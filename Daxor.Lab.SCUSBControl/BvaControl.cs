﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;


namespace Daxor.Lab.SCUSBControl
{
    static class Constants
    {
        public const ushort REPORT_SIZE=32;
        public const ushort  USB_VENDOR_ID=0x1FC9; 	// Vendor ID
        public const ushort  USB_PROD_ID=0x8044;	// Product ID
    }
    
    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct USBCmdPacket
    {
        public ushort cmd;
        public ushort seq; //command sequence number.
        public ushort arglen;
        public fixed byte arg[Constants.REPORT_SIZE - 6];

        public BVACommand Command
        {
            get
            {
                return (BVACommand)(cmd & ~(ushort)BVAPacketType.PacketFlags);
            }
        }
        
        
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct USBCmdResponsePacket
    {
        public ushort cmd;
        public ushort seq;
        public ushort status;
        public ushort arglen;
        public fixed byte arg[Constants.REPORT_SIZE - 8];

        public bool IsValidRespose(BVACommand cmdType)
        {
            ushort flag = (ushort)BVAPacketType.CommandResponse;
            return (cmd & flag) == flag;
        }

        public BVACommand Command
        {
            get
            {
                return (BVACommand)(cmd & ~(ushort)BVAPacketType.PacketFlags);
            }
        }
    }

    public enum BVAPacketType
    {
        CommandResponse=(1<<15), //packet is in response to a command
        CommandAsync=(1<<14), //packet is an async command from the device.
        PacketFlags=CommandResponse | CommandAsync
    }

    public enum BVACommand
    {
        // no argument, returns 2 byte firmware version of sample changer
        CMD_GET_ADAPTER_VERSION = 0x1,
        // argument of 4 byteS, number of micro-seconds (max 500) to pulse motor for
        CMD_PULSE_MOTOR = 0x2,
        //no arguments, returns 2 byte containing position of motor
        CMD_READ_POSITION = 0x3,
        //no args, start the motor
        CMD_START_MOTOR = 0x4,
        //no args, stop the motor
        CMD_STOP_MOTOR = 0x5,
        //no args, returns the PORT bits (SAFE,BOT,AUX,STROBE_LL)
        CMD_READ_PORTS = 0x6

    }

    public enum BVAStatus
    {
        CMD_STATUS_SUCCESS = 0x00,
        CMD_STATUS_TIMEOUT = 0x01,
        CMD_STATUS_INVALID_PARAMETER = 0x02,
        CMD_STATUS_FAILED = 0x03,
    }


    /// <summary>
    /// Main C# interface to USB Bva controller board
    /// </summary>
    public class BvaControl : USBDevice
    {
        byte _nextSeq = 0;
        public BvaControl() :
            base(Constants.USB_VENDOR_ID,Constants.USB_PROD_ID)     
        {
        
        }
        
        /// <summary>
        /// Verify the command returned is what was expected
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="resp"></param>
        void verifyCommandResponse(USBCmdPacket cmd, USBCmdResponsePacket resp)
        {

            if (!resp.IsValidRespose(cmd.Command))
                throw new Exception(string.Format("Invalid response. Expecting response for command {0}.", cmd.Command));

            if (resp.Command != cmd.Command)
                throw new Exception(string.Format("Received response for wrong command. Expected {0}, received 0x{1:X}.",
                    cmd.Command,
                    resp.Command
                    ));

            if(resp.seq!=cmd.seq)
                throw new Exception(string.Format("Received response for wrong sequence number. Received {0}, expected {1}",resp.seq,cmd.seq));

            switch((BVAStatus)resp.status)
            {
                case BVAStatus.CMD_STATUS_SUCCESS:
                    break;
                case BVAStatus.CMD_STATUS_TIMEOUT:
                    throw new Exception(string.Format("Command {0} timed-out", cmd.Command));
                case BVAStatus.CMD_STATUS_INVALID_PARAMETER:
                    throw new ArgumentException(string.Format("Invalid argument was specified for command {0}",cmd.Command));
                case BVAStatus.CMD_STATUS_FAILED:
                    throw new Exception(string.Format("Command {0} failed to execute", cmd.Command));
                default:
                    throw new Exception(string.Format("Unknown status was returend for command {0}", cmd.Command));
            }
        }

        /// <summary>
        /// The the adapter's current firmware revision
        /// </summary>
        /// <returns></returns>
        unsafe public ushort GetFirmware()
        {
            USBCmdResponsePacket response =SendCommand(BVACommand.CMD_GET_ADAPTER_VERSION, null);
            ushort ret =  (ushort)(response.arg[0] | response.arg[1] << 8);
            return ret;

        } 

        /// <summary>
        /// Pulse the motor for uSecs micro-seconds
        /// </summary>
        /// <param name="uSecs"></param>
        unsafe public void PulseMotor(UInt32 uSecs)
        {
            byte[] arg=new byte[4];
            
            arg[0] = (byte)uSecs;
            arg[1] = (byte)(uSecs >> 8);
            arg[2] = (byte)(uSecs >> 16);
            arg[3] = (byte)(uSecs >> 24);
            USBCmdResponsePacket response = SendCommand(BVACommand.CMD_PULSE_MOTOR, arg);
        }

        /// <summary>
        /// Start the motor 
        /// </summary>
        unsafe public void StartMotor()
        {
            USBCmdResponsePacket response = SendCommand(BVACommand.CMD_START_MOTOR, null);

        }

        /// <summary>
        /// Stop the motor.
        /// </summary>
        unsafe public void StopMotor()
        {
            USBCmdResponsePacket response = SendCommand(BVACommand.CMD_START_MOTOR, null);

        }

        /// <summary>
        /// Read the current motor position.
        /// </summary>
        /// <returns>Returns a position value of range 1-25.</returns>
        unsafe public ushort ReadPosition()
        {
            USBCmdResponsePacket response = SendCommand(BVACommand.CMD_READ_POSITION, null);
            if(response.arglen!=2)
                throw new Exception(string.Format("Returned argument length is invalid for cmd {0}",response.Command));

            ushort ret =(ushort)( response.arg[0] | (response.arg[1] << 8) );
            return ret;
        }
        /// <summary>
        /// Read the port bytes (BOT,AuxIn,Safety,Strobe_LL)
        /// </summary>
        /// <returns></returns>
        unsafe public byte ReadPorts()
        {
            USBCmdResponsePacket response = SendCommand(BVACommand.CMD_READ_PORTS, null);
            if (response.arglen != 1)
                throw new Exception(string.Format("Returned argument length is invalid for cmd {0}", response.Command));

            return (response.arg[0]);
        }

        /// <summary>
        /// Generic function for sending, receiving, and verifying device commands.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        unsafe public USBCmdResponsePacket SendCommand(BVACommand command, byte[] args)
        {
            USBCmdPacket cmd = new USBCmdPacket();
            cmd.cmd = (ushort)command;
            cmd.seq = _nextSeq++;
            if (args != null)
            {
                Marshal.Copy(args, 0, (IntPtr)cmd.arg, args.Length);
                cmd.arglen = (ushort)args.Length;
            }

            byte[] outBound = StructureToByteArray(cmd);
            byte[] inBound = new byte[32];
            this.genericCmd(outBound, inBound, 2000);
            object obj = new USBCmdResponsePacket();
            ByteArrayToStructure(inBound, ref obj);
            USBCmdResponsePacket response = (USBCmdResponsePacket)obj;
            verifyCommandResponse(cmd, response);
            return response;
        }
    }
}
