﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Threading;

namespace Daxor.Lab.SCUSBControl
{
    /// <summary>
    /// C# P/Invoke layer for USB HID functions
    /// </summary>
    public class USBDevice
    {
        ushort _vid = 0;
        ushort _pid = 0;
        

        const string HidDll = "daxor.lab.scusbhid.dll";
        IntPtr _deviceHandle;
        #region HID PInvoke
        [DllImport(HidDll, CallingConvention = CallingConvention.Cdecl)]
        static extern IntPtr HIDFind(ushort vid, ushort pid);

        [DllImport(HidDll, CallingConvention = CallingConvention.Cdecl)]
        static extern UInt32 HIDReadDataSync(IntPtr handle,UInt32 dwTimeout,
            UInt32 dwLengthToRead, byte[] pucData);

        [DllImport(HidDll, CallingConvention = CallingConvention.Cdecl)]
        static extern UInt32 HIDWriteData(IntPtr handle,UInt32 length, byte[] buffer);

        [DllImport(HidDll, CallingConvention = CallingConvention.Cdecl)]
        static extern void HIDMarkDeviceCommDead(IntPtr handle);

        #endregion

        #region ctor
        public USBDevice(ushort vid,ushort pid)
        {
            _vid = vid;
            _pid = pid;
        }
        #endregion
        #region USB Command- base
        protected bool genericCmd(byte[] toDevice, byte[] fromDevice, UInt32 timeoutMs)
        {
            lock (this)
            {
                if (!bConnected)
                    throw new Exception("Not connected to usb device.");

                UInt32 wRv, rRv;

                byte[] writeBuf = new byte[toDevice.Length + 1];
                byte[] readBuf = new byte[fromDevice.Length + 1];

                writeBuf[0] = readBuf[0] = 0;
                toDevice.CopyTo(writeBuf, 1);

                wRv = HIDWriteData(_deviceHandle, (uint)writeBuf.Length, writeBuf);
                if (wRv == 0)
                {
                    throw new InvalidOperationException("HIDWriteData failure");
                }

                rRv = HIDReadDataSync(_deviceHandle, timeoutMs, (uint)readBuf.Length, readBuf);
                if (rRv == 0)
                {
                    throw new InvalidOperationException("HIDReadDataSync failure");
                }

                Array.Copy(readBuf, 1, fromDevice, 0, readBuf.Length - 1);
                return true;
            }
        }
        protected void genericCmdNoResponse(byte[] toDevice)
        {
            UInt32 wRv;

            wRv = HIDWriteData(_deviceHandle,(uint)toDevice.Length, toDevice);
            if (wRv == 0)
            {
                throw new InvalidOperationException("HIDWriteData failure");
            }
        }
        #endregion
        #region Marshalling
        protected static byte[] StructureToByteArray(object obj)
        {

            int len = Marshal.SizeOf(obj);

            byte[] arr = new byte[len];

            IntPtr ptr = Marshal.AllocHGlobal(len);

            Marshal.StructureToPtr(obj, ptr, true);

            Marshal.Copy(ptr, arr, 0, len);

            Marshal.FreeHGlobal(ptr);

            return arr;

        }
        protected static void ByteArrayToStructure(byte[] bytearray, ref object obj)
        {

            int len = Marshal.SizeOf(obj);

            IntPtr i = Marshal.AllocHGlobal(len);

            Marshal.Copy(bytearray, 0, i, len);

            obj = Marshal.PtrToStructure(i, obj.GetType());

            Marshal.FreeHGlobal(i);

        }
        #endregion
        #region Public API
        bool bConnected;

        /// <summary>
        /// Disconnect from the Device
        /// </summary>
        public void Disconnect()
        {
            lock (this)
            {

                HIDMarkDeviceCommDead(_deviceHandle);
                bConnected = false;
            }
        }

        /// <summary>
        /// Connect to the Device
        /// </summary>
        /// <returns>true on successful connect, otherwise false</returns>
        public bool Connect()
        {
            lock (this)
            {
                if (bConnected)
                {
                    return true;
                }
                _deviceHandle = HIDFind(_vid, _pid);
                if (_deviceHandle != IntPtr.Zero)
                    bConnected = true;

                return bConnected;
            }
        }
        void CheckConnectStatus()
        {
            if (!bConnected)
                throw new InvalidOperationException("Device not connected.\n");
        }
        #endregion
    }
}
