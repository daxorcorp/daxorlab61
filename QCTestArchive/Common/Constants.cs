﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QCTestArchive.Common
{
    public static class Constants
    {
        public const string QCSourcesOnly = "QC Sources Only";

        public const string FullQCTest = "Full QC Test";

        public const string StandardsOnly = "Compare Standards Only";

        public const string ContaminationOnly = "Contamination QC Only";
    }
}
