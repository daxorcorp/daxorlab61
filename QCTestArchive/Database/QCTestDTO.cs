﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QCTestArchive.Classes;
using Daxor.Lab.Infrastructure.Constants;

namespace QCTestArchive.Database
{
    public class QCTestDTO
    {
        public int TestId { get; set; }

        public int StatusId { get; set; }

        public string StatusName { get; set; }

        public DateTime TestDate { get; set; }

        public string TestType { get; set; }

        public string Analyst { get; set; }

        public string Comment { get; set; }

        public string UniqueSystemId { get; set; }

        public double BackgroundCount { get; set; }

        public double BackgroundTime { get; set; }

        public int Voltage { get; set; }

        public int CoarseGain { get; set; }

        public double FineGain { get; set; }

        public int ConversionGain { get; set; }

        public bool Pass { get; set; }

        public string Reason { get; set; }

        public string InjectateLotNumber { get; set; }

        public string HospitalName { get; set; }

        public string DepartmentName { get; set; }

        private List<QCSampleDTO> _samples;
        public List<QCSampleDTO> Samples 
        {
            get
            {
                if (_samples == null)
                {
                    _samples = new List<QCSampleDTO>();
                }

                return _samples;
            }
        }

        private List<FailureDetails> _standardsFailureDetails;
        public List<FailureDetails> StandardsFailureDetails
        {
            get
            {
                if (_standardsFailureDetails == null)
                {
                    _standardsFailureDetails = new List<FailureDetails>();
                }

                return _standardsFailureDetails;
            }

        }

        public QCTestDTO() {}

        public QCTestDTO(GetQCTestResult record)
        {
            Analyst = record.ANALYST_NAME;
            BackgroundCount = (double)record.BACKGROUND;
            BackgroundTime = (double)record.BACKGROUND_TIME;
            CoarseGain = (int)record.COARSE_GAIN;
            Comment = record.COMMENT;
            ConversionGain = (int)record.CONVERSION_GAIN;
            DepartmentName = record.DEPARTMENT_NAME;
            FineGain = (double)record.FINE_GAIN;
            HospitalName = record.HOSPITAL_NAME;
            InjectateLotNumber = record.INJECTATE_LOT_NUMBER;
            Pass = record.PASS == 'T' ? true : false;
            Reason = record.REASON;
            StatusId = (int)record.STATUS_ID;
            TestDate = record.ANALYSIS_DATE == null ? DateTime.Parse(DomainConstants.NullDateTime) : DateTime.Parse(record.ANALYSIS_DATE.ToString());
            TestId = (int)record.TEST_ID;
            TestType = record.QC_TEST_TYPE;
            UniqueSystemId = record.UNIT_ID;
            Voltage = (int)record.VOLTAGE;
        }
    }
}
