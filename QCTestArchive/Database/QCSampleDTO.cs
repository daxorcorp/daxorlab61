﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QCTestArchive.Database
{
    public class QCSampleDTO
    {
        public int TestId { get; set; }

        public int SampleId { get; set; }
        public int Position
        {
            get;
            set;
        }

        public double PresetLiveTime { get; set; }

        public double LiveTime { get; set; }

        public double RealTime { get; set; }

        public int Counts { get; set; }

        public double FWHM { get; set; }

        public double FWTM { get; set; }

        public double Centroid { get; set; }

        public bool Pass { get; set; }

        public int SortOrder { get; set; }
        
        public string PositionDescription { get; set; }

        public string SerialNumber { get; set; }

        public double NominalActivity { get; set; }

        public QCSampleDTO(){}

        public QCSampleDTO(GetQCTestSamplesResult record)
        {
            Centroid = (double)record.CENTROID;
            Counts = (int)record.INTEGRAL;
            FWHM = (double)record.FWHM;
            FWTM = (double)record.FWTM;
            LiveTime = (double)record.LIVE_TIME;
            Pass = record.PASS == 'T' ? true : false;
            Position = (int)record.WHEEL_POSITION;
            PresetLiveTime = (int)record.COUNT_TIME;
            RealTime = (double)record.REAL_TIME;
            SampleId = (int)record.SAMPLE_ID;
            TestId = (int)record.TEST_ID;
            PositionDescription = record.POSITION_DESCRIPTION;
            SortOrder = record.SORT_ORDER == null ? 0 : (int)record.SORT_ORDER;
            SerialNumber = record.SERIAL_NUMBER;
            NominalActivity = (double)record.ACTIVITY;
        }
    }
}
