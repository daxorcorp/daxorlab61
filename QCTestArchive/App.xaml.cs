﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Logging;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;

namespace QCTestArchive
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {        public IUnityContainer Container { get; set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            DaxorLabBootstrapper dxrBootstrapper = null;
            try
            {
                //SplashView.ShowAsync();

                //Run BootStrapper
                dxrBootstrapper = new DaxorLabBootstrapper();
                dxrBootstrapper.Run();

                ShutdownMode = ShutdownMode.OnMainWindowClose;
                AppDomain.CurrentDomain.UnhandledException += (s, exArgs) => HandleException(exArgs.ExceptionObject);
            }
            catch (Exception ex)
            {
                HandleException(ex);
            }
            finally
            {
                //SplashView.CloseAsync();
            }
        }

        void HandleException(Object exception)
        {
            Exception ex = exception as Exception;
            if (ex == null) return;

            //string message = "An unhandled exception occurred, and the application is terminating. \nFor more information, see your Application log."; ;
            string message;
            IMessageBoxDispatcher messageBox = Container.Resolve<IMessageBoxDispatcher>();

            //Should be configured somewhere for multi language support.
            string allEndings = "\nIf the problem persists, please call DAXOR Customer Support at 1-888-774-3268.";

            //Examine the problem
            if (ex is System.Data.SqlClient.SqlException)
                message = "A connection to the database could not be established. [Error: SYS3]";
            else
                message = "A general error has occurred and the application will close. [Error: SYS2]";

            messageBox.ShowMessageBox(MessageBoxCategory.Error, String.Concat(message, allEndings), "Alert", MessageBoxChoiceSet.Ok);

            var logger = Container.Resolve<ILoggerFacade>();
            logger.Log("Unhandled Exception: " + ex.Message, Category.Exception, Priority.High);
            Environment.Exit(1);
        }
    }
}

