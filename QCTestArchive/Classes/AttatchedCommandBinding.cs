﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace QCTestArchive.Classes
{
    public class AttachedCommandBinding
    {
        #region RowActivated Command

        public static ICommand GetRowActivatedCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(RowActivatedCommandProperty);
        }

        public static void SetRowActivatedCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(RowActivatedCommandProperty, value);
        }

        // Using a DependencyProperty as the backing store for RowActivatedCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RowActivatedCommandProperty =
            DependencyProperty.RegisterAttached("RowActivatedCommand", typeof(ICommand),
            typeof(AttachedCommandBinding), new FrameworkPropertyMetadata(CommandChanged));


        private static void CommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement fe = d as FrameworkElement;
            fe.AddHandler(FrameworkElement.MouseLeftButtonDownEvent, new RoutedEventHandler(MouseLeftButtonDownHandler), true);
        }


        private static void MouseLeftButtonDownHandler(object sender, RoutedEventArgs args)
        {
            args.Handled = true;
            DependencyObject d = sender as DependencyObject;

            if (d == null) return;
            FrameworkElement fe = args.OriginalSource as FrameworkElement;
            GridViewRow gRow = fe.ParentOfType<GridViewRow>();

            if (gRow != null)
            {
                ICommand cmd = GetRowActivatedCommand(d);
                if (cmd != null && cmd.CanExecute(gRow.Item))
                {
                    //VirtualKeyboardManager.CloseActiveKeyboard();
                    cmd.Execute(gRow.Item);
                }
            }
        }
        #endregion
    }
}
