﻿using System;
using System.Collections.Generic;
using System.IO;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Shell.IntegrationTests.Stubs;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Rhino.Mocks.Constraints;

namespace Daxor.Lab.Shell.IntegrationTests.When_performing_the_automatic_backup
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_the_backup_fails
    {
        private ISettingsManager _stubSettingsManager;
        private IMessageBoxDispatcher _stubMessageBoxDispatcher;
        private ILoggerFacade _stubLogger;
        private IMessageManager _stubMessageManager;

        [TestInitialize]
        public void TestInitialize()
        {
            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();
            _stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            _stubMessageManager = MockRepository.GenerateStub<IMessageManager>();
            _stubMessageManager.Expect(x => x.GetMessage(MessageKeys.SysAutomaticBackupFailure))
                .Return(new Message(MessageKeys.SysAutomaticBackupFailure, "two", "three", "four", MessageType.Error));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_last_backup_time_is_updated()
        {
            var mockSettingsManager = MockRepository.GenerateMock<ISettingsManager>();
            mockSettingsManager.Expect(x => x.SetSetting(SettingKeys.SystemLastAutomatedBackup, DateTime.Now)).IgnoreArguments().Constraints(Is.Equal(SettingKeys.SystemLastAutomatedBackup), Is.TypeOf<DateTime>());

            var viewModel = new StubAutomaticBackupViewModelThatThrowsAnException(_stubLogger, _stubMessageBoxDispatcher, mockSettingsManager, _stubMessageManager);
            viewModel.BeginBackup();

            mockSettingsManager.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_temporary_files_are_deleted()
        {
            var mockFileManager = MockRepository.GenerateMock<IFileManager>();
            mockFileManager.Expect(x => x.GetFilesInDirectory(null, null)).IgnoreArguments().Repeat.Once().Constraints(Is.Anything(), Text.Contains("*.tmp")).Return(new List<FileInfo>());
            mockFileManager.Expect(x => x.GetFilesInDirectory(null, null)).IgnoreArguments().Repeat.Once().Constraints(Is.Anything(), Text.Contains("*.bak")).Return(new List<FileInfo>());
            mockFileManager.Expect(x => x.DirectoryExists(null)).IgnoreArguments().Return(true);
            SafeFileOperations.FileManager = mockFileManager;

            var viewModel = new StubAutomaticBackupViewModelThatThrowsAnException(_stubLogger, _stubMessageBoxDispatcher, _stubSettingsManager, _stubMessageManager);
            viewModel.BeginBackup();

            mockFileManager.VerifyAllExpectations();
            SafeFileOperations.FileManager = null;
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_correct_error_message_is_displayed()
        {
            var message = new Message(MessageKeys.SysAutomaticBackupFailure, "description", "resolution", "SYS 22", MessageType.Error);
            var mockMessageManager = MockRepository.GenerateMock<IMessageManager>();
            mockMessageManager.Expect(x => x.GetMessage(MessageKeys.SysAutomaticBackupFailure)).Return(message);

            var mockMessageBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMessageBoxDispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Error, message.FormattedMessage, "Automatic Backup", MessageBoxChoiceSet.Ok));

            var viewModel = new StubAutomaticBackupViewModelThatThrowsAnException(_stubLogger, mockMessageBoxDispatcher, _stubSettingsManager, mockMessageManager);
            viewModel.BeginBackup();

            mockMessageBoxDispatcher.VerifyAllExpectations();
            mockMessageManager.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_correct_error_message_is_logged()
        {
            var message = new Message(MessageKeys.SysAutomaticBackupFailure, "description", "resolution", "SYS 22", MessageType.Error);
            var mockMessageManager = MockRepository.GenerateMock<IMessageManager>();
            mockMessageManager.Expect(x => x.GetMessage(MessageKeys.SysAutomaticBackupFailure)).Return(message);

            var mockLogger = MockRepository.GenerateMock<ILoggerFacade>();
            mockLogger.Expect(x => x.Log(message.FormattedMessage, Category.Info, Priority.High));
            mockLogger.Expect(x => x.Log(StubAutomaticBackupViewModelThatThrowsAnException.ExceptionMessage, Category.Exception, Priority.High));

            var viewModel = new StubAutomaticBackupViewModelThatThrowsAnException(mockLogger, _stubMessageBoxDispatcher, _stubSettingsManager, mockMessageManager);
            viewModel.BeginBackup();

            mockLogger.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
