﻿using System;
using System.Collections.Generic;
using System.IO;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Shell.IntegrationTests.Stubs;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Rhino.Mocks.Constraints;

namespace Daxor.Lab.Shell.IntegrationTests.When_performing_the_automatic_backup
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_the_custom_location_does_not_exist
    {
        private const string CustomDirectory = @"C:\some_bad_location";
        private const string DefaultDirectory = @"C:\temp\";
        private const int NumberOfBackupsToKeep = 10;

        private ISettingsManager _stubSettingsManager;
        private IMessageBoxDispatcher _stubMessageBoxDispatcher;
        private ILoggerFacade _stubLogger;
        private IBackupServiceFactory _stubBackupServiceFactory;
        private IMessageManager _stubMessageManager;

        [TestInitialize]
        public void TestInitialize()
        {
            _stubMessageManager = MockRepository.GenerateStub<IMessageManager>();
            _stubMessageManager.Expect(x => x.GetMessage("something")).IgnoreArguments().Return(new Message("one", "two", "three", "four", MessageType.Alert));
            
            _stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            
            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();
            
            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            SetupSettingsManager(_stubSettingsManager);
            
            _stubBackupServiceFactory = MockRepository.GenerateStub<IBackupServiceFactory>();
            _stubBackupServiceFactory.Expect(x => x.CreateDaxorLabBackupService()).Return(new StubBackupService());
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_backup_is_in_the_default_location()
        {
            var viewModel = new StubAutomaticBackupViewModelThatDoesNotDeleteTempFiles(_stubLogger, _stubMessageBoxDispatcher, _stubSettingsManager, _stubBackupServiceFactory, _stubMessageManager);
            viewModel.BeginBackup();

            var now = DateTime.Now;
            var fileName = now.Month + "_" + now.Day + "_" + now.Year + "_DaxorSuite.zip";

            Assert.IsTrue(File.Exists(DefaultDirectory + fileName));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_correct_message_is_used()
        {
            if (SafeFileOperations.FileManager.GetType() != typeof(SystemFileManager))
                Assert.Fail("Wrong FileManager the type is" + SafeFileOperations.FileManager.GetType());

            var message = new Message(MessageKeys.SysAutomaticBackupLocationNotFound, "Could not find backup location", "resolution", "SYS 22", MessageType.Error);
            var mockMessageManager = MockRepository.GenerateMock<IMessageManager>();
            mockMessageManager.Expect(x => x.GetMessage(MessageKeys.SysAutomaticBackupLocationNotFound)).Return(message);

            var displayMessage = String.Format(message.FormattedMessage, CustomDirectory);
            var mockMessageBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMessageBoxDispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Warning, displayMessage, "Automatic Backup", MessageBoxChoiceSet.Ok));

            var viewModel = new StubAutomaticBackupViewModelThatDoesNotDeleteTempFiles(_stubLogger, mockMessageBoxDispatcher, _stubSettingsManager, _stubBackupServiceFactory, mockMessageManager);
            viewModel.BeginBackup();

            mockMessageManager.VerifyAllExpectations();
            mockMessageBoxDispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_error_is_logged()
        {
            var message = new Message(MessageKeys.SysAutomaticBackupLocationNotFound, "Could not find backup location", "resolution", "SYS 22", MessageType.Error);
            _stubMessageManager = MockRepository.GenerateMock<IMessageManager>();
            _stubMessageManager.Expect(x => x.GetMessage(MessageKeys.SysAutomaticBackupLocationNotFound)).Return(message);
            
            var displayMessage = String.Format(message.FormattedMessage, CustomDirectory);
            var mockLogger = MockRepository.GenerateMock<ILoggerFacade>();
            mockLogger.Expect(x => x.Log(displayMessage, Category.Warn, Priority.High));

            var viewModel = new StubAutomaticBackupViewModelThatDoesNotDeleteTempFiles(mockLogger, _stubMessageBoxDispatcher, _stubSettingsManager, _stubBackupServiceFactory, _stubMessageManager);
            viewModel.BeginBackup();

            mockLogger.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_last_automatic_backup_time_is_set()
        {
            var mockSettingsManager = MockRepository.GenerateMock<ISettingsManager>();
            SetupSettingsManager(mockSettingsManager);
            mockSettingsManager.Expect(x => x.SetSetting(SettingKeys.SystemLastAutomatedBackup, DateTime.Now)).IgnoreArguments().Constraints(Is.Equal(SettingKeys.SystemLastAutomatedBackup), Is.TypeOf<DateTime>());

            var viewModel = new StubAutomaticBackupViewModelThatDoesNotDeleteTempFiles(_stubLogger, _stubMessageBoxDispatcher, mockSettingsManager, _stubBackupServiceFactory, _stubMessageManager);
            viewModel.BeginBackup();

            mockSettingsManager.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_temporary_files_are_deleted()
        {
            var fileNames = new List<string> { DefaultDirectory + "create.bak", DefaultDirectory + "something.tmp" };
            CreateFiles(fileNames);

            var viewModel = new StubAutomcaticBackupViewModelThatMocksPurge(_stubLogger, _stubMessageBoxDispatcher, _stubSettingsManager, _stubBackupServiceFactory, _stubMessageManager);
            viewModel.BeginBackup();

            Assert.IsTrue(AreTempFilesDeleted(DefaultDirectory));
            DeleteFiles(fileNames);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_old_backups_are_purged()
        {
            const string oldFileName = DefaultDirectory + "old.zip";
            const string wayOldFileName = DefaultDirectory + "wayOld.zip";
            const string newHotnessFileName = DefaultDirectory + "newHotness.zip";
            
            var expiredTime = DateTime.Now.AddDays(-(NumberOfBackupsToKeep + 1));
            var fileNames = new List<string> { oldFileName, wayOldFileName, newHotnessFileName };
            CreateFiles(fileNames);

            var files = new List<FileInfo>{
                new FileInfo(oldFileName) { CreationTime = expiredTime },
                new FileInfo(wayOldFileName) { CreationTime = expiredTime },
                new FileInfo(newHotnessFileName) { CreationTime = DateTime.Now },
            };

            var fileManager = MockRepository.GenerateMock<IFileManager>();
            fileManager.Expect(x => x.DirectoryExists(DefaultDirectory)).Return(true);
            fileManager.Expect(x => x.DeleteFile(oldFileName));
            fileManager.Expect(x => x.DeleteFile(wayOldFileName));
            fileManager.Expect(x => x.DeleteFile(newHotnessFileName)).Repeat.Never();
            fileManager.Expect(x => x.GetFilesInDirectory(DefaultDirectory, null)).IgnoreArguments().Constraints(Is.Equal(DefaultDirectory), Is.Anything()).Return(files);
            SafeFileOperations.FileManager = fileManager;

            var viewModel = new StubAutomaticBackupViewModelThatDoesNotDeleteTempFiles(_stubLogger, _stubMessageBoxDispatcher, _stubSettingsManager, _stubBackupServiceFactory, _stubMessageManager);
            viewModel.BeginBackup();

            fileManager.VerifyAllExpectations();
            DeleteFiles(fileNames);
            SafeFileOperations.FileManager = null;
        }

        private static void DeleteFiles(IEnumerable<string> fileNames)
        {
            foreach (var name in fileNames)
                File.Delete(name);
        }

        private static bool AreTempFilesDeleted(string directory)
        {
            if (Directory.GetFiles(directory, "*.tmp").Length > 0)
                return false;
            if (Directory.GetFiles(directory, "*.bak").Length > 0)
                return false;
            return true;
        }

        private static void CreateFiles(IEnumerable<string> fileNames)
        {
            foreach (var name in fileNames)
                File.Create(name).Close();
        }

        private static void SetupSettingsManager(ISettingsManager settingsManager)
        {
            settingsManager.Expect(x => x.GetSetting<int>(SettingKeys.SystemNumberOfBackupsToKeep)).Return(NumberOfBackupsToKeep);
            settingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemAutomaticBackupLocation)).Return(CustomDirectory);
            settingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemDefaultAutomaticBackupLocation)).Return(DefaultDirectory);
        }
    }
    // ReSharper restore InconsistentNaming
}
