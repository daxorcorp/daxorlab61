﻿using System;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Shell.IntegrationTests.Stubs;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Rhino.Mocks.Constraints;

namespace Daxor.Lab.Shell.IntegrationTests.When_performing_the_automatic_backup
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_neither_location_can_be_found
    {
        private IMessageBoxDispatcher _stubMessageBoxDispatcher;
        private ISettingsManager _stubSettingsManager;
        private ILoggerFacade _stubLogger;
        private IBackupServiceFactory _stubBackupServiceFactory;
        private IMessageManager _stubMessageManager;
        private readonly Message _message = new Message(MessageKeys.SysAutomaticBackupLocationNotFound, "Could not find {0}", "resolution", "SYS 22", MessageType.Error);

        [TestInitialize]
        public void TestInitialize()
        {
            _stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();

            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            SetupSettingsManager(_stubSettingsManager);
            
            _stubMessageManager = MockRepository.GenerateStub<IMessageManager>();
            _stubMessageManager.Expect(x => x.GetMessage(MessageKeys.SysDefaultBackupLocationNotFound)).Return(_message);
            
            _stubBackupServiceFactory = MockRepository.GenerateStub<IBackupServiceFactory>();
            _stubBackupServiceFactory.Expect(x => x.CreateDaxorLabBackupService()).Return(new StubBackupService());

            var stubFileManager = MockRepository.GenerateStub<IFileManager>();
            stubFileManager.Expect(x => x.DirectoryExists(@"C:\temp\")).Return(false);
            stubFileManager.Expect(x => x.DirectoryExists(@"C:\somethingElse\")).Return(false);
            SafeFileOperations.FileManager = stubFileManager;
        }

        [TestCleanup]
        public void TestCleanup()
        {
            SafeFileOperations.FileManager = null;
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_backup_is_not_created()
        {
            var mockBackupService = MockRepository.GenerateMock<IBackupService>();
            mockBackupService.Expect(x => x.Backup(null)).IgnoreArguments().Repeat.Never();
            _stubBackupServiceFactory.Expect(x => x.CreateDaxorLabBackupService()).Return(mockBackupService);

            var viewModel = new StubAutomaticBackupViewModelThatDoesNotDeleteTempFiles(_stubLogger, _stubMessageBoxDispatcher, _stubSettingsManager, _stubBackupServiceFactory, _stubMessageManager);
            viewModel.BeginBackup();

            mockBackupService.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_last_automated_backup_time_is_not_updated()
        {
            var mockSettingsManager = MockRepository.GenerateMock<ISettingsManager>();
            SetupSettingsManager(mockSettingsManager);
            mockSettingsManager.Expect(x => x.SetSetting(SettingKeys.SystemLastAutomatedBackup, DateTime.Now)).Repeat.Never().Constraints(Is.Equal(SettingKeys.SystemLastAutomatedBackup), Is.TypeOf<DateTime>());

            var viewModel = new StubAutomaticBackupViewModelThatDoesNotDeleteTempFiles(_stubLogger, _stubMessageBoxDispatcher, mockSettingsManager, _stubBackupServiceFactory, _stubMessageManager);
            viewModel.BeginBackup();

            mockSettingsManager.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_correct_message_is_displayed_to_the_user()
        {
            var messageShown = String.Format(_message.FormattedMessage, @"C:\somethingElse\");

            var mockMessageManager = MockRepository.GenerateMock<IMessageManager>();
            mockMessageManager.Expect(x => x.GetMessage(MessageKeys.SysDefaultBackupLocationNotFound)).Return(_message);

            var mockMessageBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMessageBoxDispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Error, messageShown, "Automatic Backup", MessageBoxChoiceSet.Ok));

            var viewModel = new StubAutomaticBackupViewModelThatDoesNotDeleteTempFiles(_stubLogger, mockMessageBoxDispatcher, _stubSettingsManager, _stubBackupServiceFactory, mockMessageManager);
            viewModel.BeginBackup();

            mockMessageBoxDispatcher.VerifyAllExpectations();
            mockMessageManager.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_correct_message_is_logged()
        {
            var message = _stubMessageManager.GetMessage(MessageKeys.SysDefaultBackupLocationNotFound).FormattedMessage;
            message = String.Format(message, @"C:\somethingElse\");

            var mockLogger = MockRepository.GenerateMock<ILoggerFacade>();
            mockLogger.Expect(x => x.Log(message, Category.Exception, Priority.High));

            var viewModel = new StubAutomaticBackupViewModelThatDoesNotDeleteTempFiles(mockLogger, _stubMessageBoxDispatcher, _stubSettingsManager, _stubBackupServiceFactory, _stubMessageManager);
            viewModel.BeginBackup();

            mockLogger.VerifyAllExpectations();
        }

        private void SetupSettingsManager(ISettingsManager settingsManager)
        {
            settingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemDefaultAutomaticBackupLocation)).Return(@"C:\temp\");
            settingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemAutomaticBackupLocation)).Return(@"C:\somethingElse\");
        }
    }
    // ReSharper restore InconsistentNaming
}
