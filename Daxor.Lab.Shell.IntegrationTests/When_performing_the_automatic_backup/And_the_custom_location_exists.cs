﻿using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Shell.IntegrationTests.Stubs;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Rhino.Mocks.Constraints;
using System;
using System.IO;

namespace Daxor.Lab.Shell.IntegrationTests.When_performing_the_automatic_backup
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_the_custom_location_exists
    {
        private ILoggerFacade _stubLogger;
        private IMessageBoxDispatcher _stubMessageBoxDispatcher;
        private ISettingsManager _stubSettingsManager;
        private IBackupServiceFactory _backupServiceFactory;

        [TestInitialize]
        public void TestInitialize()
        {
            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();
            _stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();

            _backupServiceFactory = MockRepository.GenerateStub<IBackupServiceFactory>();
            _backupServiceFactory.Expect(x => x.CreateDaxorLabBackupService()).Return(new StubBackupService());
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_setting_is_updated()
        {
            var mockSettingsManager = MockRepository.GenerateMock<ISettingsManager>();
            mockSettingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemAutomaticBackupLocation)).Return(@"C:\temp\");
            mockSettingsManager.Expect(x => x.SetSetting(SettingKeys.SystemLastAutomatedBackup, DateTime.Now)).IgnoreArguments().Constraints(Is.Equal(SettingKeys.SystemLastAutomatedBackup), Is.TypeOf<DateTime>());

            var viewModel = new StubAutomcaticBackupViewModelThatMocksPurge(_stubLogger, _stubMessageBoxDispatcher, mockSettingsManager, _backupServiceFactory);
            viewModel.BeginBackup();

            mockSettingsManager.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_the_file_is_saved_to_the_correct_location()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemAutomaticBackupLocation)).Return(@"C:\temp\");
            _stubSettingsManager.Expect(x => x.GetSetting<int>(SettingKeys.SystemNumberOfBackupsToKeep)).Return(1);

            var viewModel = new StubAutomcaticBackupViewModelThatMocksPurge(_stubLogger, _stubMessageBoxDispatcher, _stubSettingsManager, _backupServiceFactory);
            viewModel.BeginBackup();

            var now = DateTime.Now;
            var fileName = now.Month + "_" + now.Day + "_" + now.Year + "_DaxorSuite.zip";

            Assert.IsTrue(File.Exists(@"C:\temp\" + fileName));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_there_are_no_temporary_files()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemAutomaticBackupLocation)).Return(@"C:\temp\");
            _stubSettingsManager.Expect(x => x.GetSetting<int>(SettingKeys.SystemNumberOfBackupsToKeep)).Return(1);
            CreateTempFiles(@"C:\temp\");
            
            var viewModel = new StubAutomcaticBackupViewModelThatMocksPurge(_stubLogger, _stubMessageBoxDispatcher, _stubSettingsManager, _backupServiceFactory);
            viewModel.BeginBackup();
            
            Assert.IsTrue(AreThereNoTempFilesInDirectory(@"C:\temp\"));
            DeleteTempFiles(@"C:\temp\");
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_old_backups_are_purged()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemAutomaticBackupLocation)).Return(@"C:\temp\");
            _stubSettingsManager.Expect(x => x.GetSetting<int>(SettingKeys.SystemNumberOfBackupsToKeep)).Return(1);

            var viewModel = new StubAutomcaticBackupViewModelThatMocksPurge(_stubLogger, _stubMessageBoxDispatcher, _stubSettingsManager, _backupServiceFactory);
            viewModel.BeginBackup();
            
            Assert.IsTrue(viewModel.WasPurgeCalled);
        }

        private static void CreateTempFiles(string path)
        {
            File.Create(path + "something.bak").Close();
            File.Create(path + "other.tmp").Close();
        }

        private static bool AreThereNoTempFilesInDirectory(string path)
        {
            var files = Directory.GetFiles(path, "*.bak");
            if (files.Length != 0)
                return false;

            files = Directory.GetFiles(path, "*.tmp");

            return files.Length == 0;
        }

        private static void DeleteTempFiles(string path)
        {
            File.Delete(path + "something.bak");
            File.Delete(path + "other.tmp");
        }
    }
    // ReSharper restore InconsistentNaming
}
