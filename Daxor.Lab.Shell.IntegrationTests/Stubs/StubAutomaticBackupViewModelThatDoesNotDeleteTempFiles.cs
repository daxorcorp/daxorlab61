﻿using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.Factories;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Shell.ViewModels;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Logging;
using NSubstitute;

namespace Daxor.Lab.Shell.IntegrationTests.Stubs
{
    public class StubAutomaticBackupViewModelThatDoesNotDeleteTempFiles : AutomaticBackupViewModel
    {
        public StubAutomaticBackupViewModelThatDoesNotDeleteTempFiles(ILoggerFacade logger, IMessageBoxDispatcher messageBoxDispatcher, ISettingsManager settingsManager, IBackupServiceFactory backupServiceFactory)
            : base(logger, messageBoxDispatcher, settingsManager, backupServiceFactory, null, new ZipFileFactory())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public StubAutomaticBackupViewModelThatDoesNotDeleteTempFiles(ILoggerFacade logger, IMessageBoxDispatcher messageBoxDispatcher, ISettingsManager settingsManager, IBackupServiceFactory backupServiceFactory, IMessageManager messageManager)
            : base(logger, messageBoxDispatcher, settingsManager, backupServiceFactory, messageManager, new ZipFileFactory())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        protected override void CleanUpTempFiles()
        {
        }
    }
}
