﻿using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Shell.ViewModels;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Logging;
using System;
using NSubstitute;

namespace Daxor.Lab.Shell.IntegrationTests.Stubs
{
    public class StubAutomaticBackupViewModelThatThrowsAnException : AutomaticBackupViewModel
    {
        public const string ExceptionMessage = "Oh snap";
        public StubAutomaticBackupViewModelThatThrowsAnException(ILoggerFacade logger, IMessageBoxDispatcher messageBoxDispatcher, ISettingsManager settingsManager, IMessageManager messageManager)
            : base(logger, messageBoxDispatcher, settingsManager, null, messageManager, Substitute.For<IZipFileFactory>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        protected override void BackupTask(System.Threading.CancellationTokenSource tokenSource)
        {
            throw new Exception(ExceptionMessage);
        }
    }
}
