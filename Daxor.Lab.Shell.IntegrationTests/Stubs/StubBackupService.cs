﻿using Daxor.Lab.DatabaseServices.Interfaces;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System.IO;

namespace Daxor.Lab.Shell.IntegrationTests.Stubs
{
    public class StubBackupService : IBackupService
    {
        public void Backup(string filePath)
        {
            File.Create(filePath).Close();
        }

        public ServerMessageEventHandler BackupCompletedHandler { get; set; }
        public PercentCompleteEventHandler PercentCompleteHandler { get; set; }
    }
}
