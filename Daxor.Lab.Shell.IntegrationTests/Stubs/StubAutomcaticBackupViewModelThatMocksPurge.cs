﻿using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.Factories;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Shell.ViewModels;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Logging;
using NSubstitute;
using Rhino.Mocks;

namespace Daxor.Lab.Shell.IntegrationTests.Stubs
{
    public class StubAutomcaticBackupViewModelThatMocksPurge : AutomaticBackupViewModel
    {
        public bool WasPurgeCalled { get; private set; }

        public StubAutomcaticBackupViewModelThatMocksPurge(ILoggerFacade customLoggerFacade, IMessageBoxDispatcher messageBoxDispatcher, ISettingsManager settingsManager, IBackupServiceFactory serviceFactory)
            : base(customLoggerFacade, messageBoxDispatcher, settingsManager, serviceFactory, MockRepository.GenerateStub<IMessageManager>(), new ZipFileFactory())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
            WasPurgeCalled = false;
        }

        public StubAutomcaticBackupViewModelThatMocksPurge(ILoggerFacade customLoggerFacade, IMessageBoxDispatcher messageBoxDispatcher, ISettingsManager settingsManager, IBackupServiceFactory serviceFactory, IMessageManager messageManager)
            : base(customLoggerFacade, messageBoxDispatcher, settingsManager, serviceFactory, messageManager, Substitute.For<IZipFileFactory>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
            WasPurgeCalled = false;
        }

        protected override void PurgeOldBackups()
        {
            WasPurgeCalled = true;
        }
    }
}
