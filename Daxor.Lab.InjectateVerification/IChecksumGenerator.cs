﻿//-----------------------------------------------------------------------
// <copyright file="IChecksumGenerator.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Daxor.Lab.InjectateVerification
{
    /// <summary>
    /// Exposes methods that compute a checksum of a given value and provide a display name for
    /// the checksum generator class that implements this interface.
    /// </summary>
    public interface IChecksumGenerator
    {
        #region DisplayName property
        /// <summary>
        /// Gets the full-text name of this checksum generator
        /// </summary>
        /// <remarks>
        /// This is used for populating UI-related elements and for referring to 
        /// IChecksumGenerators by a string name
        /// </remarks>
        string DisplayName { get; }
        #endregion

        #region GenerateChecksum()
        /// <summary>
        /// Generates a checksum based on the given input value
        /// </summary>
        /// <param name="inputValue">
        /// Value for which the checksum is computed
        /// </param>
        /// <returns>
        /// Checksum corresponding to the input value
        /// </returns>
        ulong GenerateChecksum(ulong inputValue);
        #endregion
    }
}
