﻿//-----------------------------------------------------------------------
// <copyright file="Alphanumeric56KeyRepresentation.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Daxor.Lab.InjectateVerification
{
    /// <summary>
    /// Represents an object that can convert to/from a 56-value alphanumeric represenation.
    /// </summary>
    /// <remarks>
    /// The letters 'O' and 'I' have been removed as they may be mistaken for lowercase 'o' or 
    /// zero, and lowercase 'l' or one respectively. It can be argued that letters whose lower-
    /// and uppercase counterparts that look similar (e.g., cC, sS, vV, wW, xX, zZ) be removed
    /// as well.
    /// </remarks>
    public class Alphanumeric56KeyRepresentation : KeyRepresentation
    {
        #region DisplayName property
        /// <summary>
        /// Returns the full-text name of this key representation
        /// </summary>
        public override string DisplayName
        {
            get
            {
                return "Alphanumeric (56 character)";
            }
        }
        #endregion

        #region RepresentationAsString property
        /// <summary>
        /// Defines the characters (and their ordering) for the 56-character key representation
        /// </summary>
        public override string RepresentationAsString
        {
            get
            {
                return "abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789";
            }
        }
        #endregion
    }
}
