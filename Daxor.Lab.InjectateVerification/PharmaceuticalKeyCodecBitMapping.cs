﻿using System;
using System.Collections.Generic;

namespace Daxor.Lab.InjectateVerification
{
    /// <summary>
    /// Represents a one-to-one mapping of original bit order to an alternate bit order. This is 
    /// effectively used to "scramble" the bits of a Pharmaceutical key. For example, instead of
    /// having the bits in order (54321) they could be in an alternate order (42531). Therefore, 
    /// if the bit mapping were not correct (or known) for a given pharmaceutical key, it could 
    /// not be decoded correctly.
    /// </summary>
    public class PharmaceuticalKeyCodecBitMapping
    {
        #region Private fields

        private List<byte> _bitMapping;

        #endregion

        #region Ctors

        /// <summary>
        /// Initializes a new instance of the PharmaceuticalKeyCodecBitMapping class with a given 
        /// pharmaceutical key configuration instance and an empty bit mapping.
        /// </summary>
        public PharmaceuticalKeyCodecBitMapping()
        {
            BitMapping = new List<byte>();
        }

        /// <summary>
        /// Initializes a new instance of the PharmaceuticalKeyCodecBitMapping class with a given 
        /// pharmaceutical key configuration instance and a given bit mapping.
        /// </summary>
        /// <param name="bitMapping">
        /// Instance of a list containing the ordering of bits (i.e., bit mapping)
        /// </param>
        public PharmaceuticalKeyCodecBitMapping(List<byte> bitMapping)
        {
            BitMapping = bitMapping;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the bit mapping list instance
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// Given list instance is null
        /// </exception>
        public List<byte> BitMapping
        {
            get { return _bitMapping; }

            set
            {
                if (value == null)
                    throw new InvalidOperationException("BitMapping instance cannot be null");

                _bitMapping = value;
            }
        }

        #endregion

        /// <summary>
        /// Applies this instance's bit mapping to a given value by moving each bit into the
        /// position indicated by the mapping.
        /// </summary>
        /// <param name="value">
        /// Value to which the bit mapping should be applied
        /// </param>
        /// <param name="numberOfBitsInKey">
        /// Number of bits in the key to which the mapping will be applied
        /// </param>
        /// <returns>Remapped value</returns>
        /// <exception cref="InvalidOperationException">
        /// This instance's mapping is not complete (see EnsureMappingIsComplete())
        /// </exception>
        public ulong ApplyMapping(ulong value, uint numberOfBitsInKey)
        {
            EnsureMappingIsComplete(numberOfBitsInKey);

            ulong valueToReturn = 0L;
            for (var i = 0; i < BitMapping.Count; i++)
            {
                var ithBit = value.GetBitAtPosition(i);
                valueToReturn = valueToReturn.PlaceBitAtPosition(ithBit, BitMapping[i]);
            }

            return valueToReturn;
        }

        /// <summary>
        /// Remove this instance's bit mapping from a given value by the bits in positions
        /// indicated by the mapping to the original positions.
        /// </summary>
        /// <param name="mappedValue">
        /// Value from which the bit mapping should be removed
        /// </param>
        /// <param name="numberOfBitsInKey">
        /// Number of bits in the key to which the mapping will be applied
        /// </param>
        /// <returns>
        /// Value with bit mapping removed
        /// </returns>
        /// <exception cref="ArgumentException">
        /// This instance's mapping is not complete (see EnsureMappingIsComplete())
        /// </exception>
        public ulong RemoveMapping(ulong mappedValue, uint numberOfBitsInKey)
        {
            EnsureMappingIsComplete(numberOfBitsInKey);

            ulong valueToReturn = 0L;
            for (var i = 0; i < BitMapping.Count; i++)
            {
                var bitAtMappedPosition = mappedValue.GetBitAtPosition(BitMapping[i]);
                valueToReturn = valueToReturn.PlaceBitAtPosition(bitAtMappedPosition, i);
            }

            return valueToReturn;
        }

        /// <summary>
        /// Checks the current bit mapping instance to ensure that all bits are mapped and that
        /// no extraneous bits are included in the mapping.
        /// </summary>
        /// <param name="numberOfBitsInKey">
        /// Number of bits in the key to which the mapping will be applied
        /// </param>
        /// <exception cref="ArgumentException">
        /// Bit position expected to be in the mapping is absent; for example, if there are five
        /// bits in the key and the mapping is [3, 1, 0, 2], an exception will be thrown because
        /// '4' is not present in the mapping
        /// </exception>
        /// <exception cref="InvalidOperationException">
        /// Bit mapping contains more entries than there are bits to map
        /// </exception>
        public void EnsureMappingIsComplete(uint numberOfBitsInKey)
        {
            if (BitMapping.Count != numberOfBitsInKey)
            {
                var message = "Number of bit mappings (" + BitMapping.Count +
                                 ") is incorrect; expecting " + numberOfBitsInKey;
                throw new InvalidOperationException(message);
            }

            for (var i = 0; i < numberOfBitsInKey; i++)
            {
                if (!BitMapping.Contains((byte) i))
                {
                    var message = "Mapping for bit '" + i + "' is missing from the bit mapping";
                    throw new InvalidOperationException(message);
                }
            }
        }
    }
}
