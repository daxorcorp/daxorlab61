﻿//-----------------------------------------------------------------------
// <copyright file="PharmaceuticalKeyCodecHelper.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Daxor.Lab.InjectateVerification
{
    using System;
    using System.Collections.Generic;
    using Daxor.Lab.Infrastructure.DomainModels;

    /// <summary>
    /// Supporting class used by the PharmaceuticalKeyCodec class. 
    /// </summary>
    /// <remarks>
    /// This class contains methods to allow the codec code to be cleaner; otherwise, most of the 
    /// codec class code would consist of supporting methods (which in turn exist to prevent the 
    /// Encode() and Decode() methods being hundreds of lines long, and thus harder to maintain and
    /// test).
    /// </remarks>
    public class PharmaceuticalKeyCodecHelper
    {
        #region Private member variables
        /// <summary>
        /// Private member variable for the pharmaceutical key codec configuration instance
        /// </summary>
        private PharmaceuticalKeyCodecConfiguration _configuration;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the PharmaceuticalKeyCodecHelper class
        /// </summary>
        /// <param name="configuration">
        /// Key codec configuration to be used with the methods in this class
        /// </param>
        public PharmaceuticalKeyCodecHelper(PharmaceuticalKeyCodecConfiguration configuration)
        {
            CodecConfiguration = configuration;
        }
        #endregion

        #region Property -- CodecConfiguration
        /// <summary>
        /// Gets or sets the PharmaceuticalKeyCodecConfiguration instance associated with this
        /// class instance
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// Given value to set the instance is null
        /// </exception>
        public PharmaceuticalKeyCodecConfiguration CodecConfiguration
        {
            get
            {
                return _configuration;
            }

            set
            {
                EnsureNonNullKeyCodecConfiguration(value);
                _configuration = value;
            }
        }
        #endregion

        #region EnsureNonNull- methods
        #region EnsureNonNullPharmaceutical()
        /// <summary>
        /// Ensures that the given Pharmaceutical instance is non-null.
        /// </summary>
        /// <param name="pharmaceutical">
        /// Pharmaceutical instance to verify
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Given Pharmaceutical instance is null
        /// </exception>
        /// <remarks>
        /// This method is used by other methods in this class. It exists as its own method to
        /// reduce code duplication in several class methods that check for non-null instances
        /// before continuing.
        /// </remarks>
        public static void EnsureNonNullPharmaceutical(Pharmaceutical pharmaceutical)
        {
            if (pharmaceutical == null)
            {
                throw new ArgumentNullException("pharmaceutical");
            }
        }
        #endregion

        #region EnsureNonNullKeyCodecConfiguration()
        /// <summary>
        /// Ensures that the given PharmaceuticalKeyCodecConfiguration instance is non-null.
        /// </summary>
        /// <param name="configuration">
        /// PharmaceuticalKeyCodecConfiguration instance to verify
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Given PharmaceuticalKeyCodecConfiguration instance is null
        /// </exception>
        /// <remarks>
        /// This method is used by other methods in this class. It exists as its own method to
        /// reduce code duplication in several class methods that check for non-null instances
        /// before continuing.
        /// </remarks>
        public static void EnsureNonNullKeyCodecConfiguration(PharmaceuticalKeyCodecConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException("configuration");
        }
        #endregion
        #endregion

        #region Checksum computation methods
        #region ComputeChecksum(pharm)
        /// <summary>
        /// Computes the checksum for a given value where the key codec configuration defines (1) 
        /// the type of checksum generated and (2) the number of bits in the checksum. If the
        /// number of bits in the generated checksum exceeds the number of allowed bits, only the
        /// number of allowed bits (starting from the least-significant) will be kept.
        /// </summary>
        /// <param name="pharmaceutical">
        /// Pharmaceutical instance for which the checksum will be computed
        /// </param>
        /// <returns>
        /// Numeric checksum for the given numeric pharmaceutical value
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// Pharmaceutical instance is null
        /// </exception>'
        /// <exception cref="ApplicationException">
        /// Pharmaceutical could not be converted to a value so that the checksum could be
        /// computed
        /// </exception>
        public ulong ComputeChecksum(Pharmaceutical pharmaceutical)
        {
            // We can't proceed without a valid Pharmaceutical instance
            PharmaceuticalKeyCodecHelper.EnsureNonNullPharmaceutical(pharmaceutical);

            // Simply convert the pharmaceutical to a value, then use the other ComputeChecksum().
            ulong pharmaceuticalValue = ConvertPharmaceuticalToValue(pharmaceutical);
            return ComputeChecksum(pharmaceuticalValue);
        }
        #endregion

        #region ComputeChecksum(ulong)
        /// <summary>
        /// Computes the checksum for a given value where the key codec configuration defines (1) 
        /// the type of checksum generated and (2) the number of bits in the checksum. If the
        /// number of bits in the generated checksum exceeds the number of allowed bits, only the
        /// number of allowed bits (starting from the least-significant) will be kept.
        /// </summary>
        /// <param name="pharmaceuticalValue">
        /// Numeric representation of a pharmaceutical for which the checksum will be computed
        /// </param>
        /// <returns>
        /// Numeric checksum for the given numeric pharmaceutical value
        /// </returns>
        public ulong ComputeChecksum(ulong pharmaceuticalValue)
        {
            ulong checksum = CodecConfiguration.ChecksumGenerator.GenerateChecksum(pharmaceuticalValue);
            uint numberOfCheckBits = CodecConfiguration.KeyConfiguration.NumberOfChecksumBits;
            return checksum & ((uint)Math.Pow(2, numberOfCheckBits) - 1);
        }
        #endregion
        #endregion

        #region PadLeft()
        /// <summary>
        /// Pads "zeroes" on the left end of the given pharmaceutical key if that key is less than
        /// the required key length.
        /// </summary>
        /// <param name="pharmaceuticalKey">
        /// Pharmaceutical key to pad (if needed)
        /// </param>
        /// <returns>
        /// Given pharmaceutical key left-padded with "zeroes", where the actual character being
        /// padded depends on how zero is represented in the given configuration's 
        /// KeyRepresentation instance
        /// </returns>
        /// <example>
        /// For hexadecimal key representation, if the encoded key is "419ec2" and the desired 
        /// number of characters is 8, the returned key will be "00419ec2".
        /// </example>
        public string PadLeft(string pharmaceuticalKey)
        {
            // Find the number of characters missing on the left side
            int numberOfCharsRequired = (int) CodecConfiguration.GetNumberOfCharactersInKey();
            int numberOfCharsToPad = numberOfCharsRequired - pharmaceuticalKey.Length;
            
            // See if any padding is necessary.
            if (numberOfCharsToPad <= 0)
            {
                return pharmaceuticalKey;
            }

            // Left-pad "0"
            KeyRepresentation representation = CodecConfiguration.Representation;
            for (int i = 0; i < numberOfCharsToPad; i++)
            {
                pharmaceuticalKey = representation.ConvertToRepresentation(0) + pharmaceuticalKey;
            }

            return pharmaceuticalKey;
        }
        #endregion

        #region Convert to/from Pharmaceutical
        #region ConvertPharmaceuticalToValue()
        /// <summary>
        /// Converts a Pharmaceutical instance into a numeric value based on the given key codec
        /// configuration
        /// </summary>
        /// <param name="pharmaceutical">
        /// Pharmaceutical instance to convert
        /// </param>
        /// <returns>
        /// Numeric equivalent of the given Pharmaceutical instance
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// Given Pharmaceutical instance is null
        /// </exception>
        /// <exception cref="ApplicationException">
        /// The date of manufacture for the given pharmaceutical occurs before the starting date of
        /// manufacuture set in the given key codec configuration
        /// </exception>
        /// <exception cref="ApplicationException">
        /// The batch ID for the given pharmaceutical occurs before the batch ID set in the given 
        /// key codec configuration
        /// </exception>
        /// <exception cref="ApplicationException">
        /// The serialization ID for the given pharmaceutical is beyond the maximum serialization
        /// ID set in the given key codec configuration
        /// </exception>
        /// <exception cref="ApplicationException">
        /// The pharmaceutical type is not recognized
        /// </exception>
        /// <exception cref="ApplicationException">
        /// The pharmaceutical manufacturer is not recognized
        /// </exception>
        /// <exception cref="ApplicationException">
        /// The pharmaceutical brand is not recognized
        /// </exception>
        public ulong ConvertPharmaceuticalToValue(Pharmaceutical pharmaceutical)
        {
            // We can't proceed without a valid Pharmaceutical
            EnsureNonNullPharmaceutical(pharmaceutical);

            // Get all the values to encode
            uint dateOfManufacturerToEncode = GetDateOfManufactureToEncode(pharmaceutical);
            uint batchIDToEncode = GetBatchIDToEncode(pharmaceutical);
            uint serializationIDToEncode = GetSerializationIDToEncode(pharmaceutical);
            uint pharmaceuticalTypeToEncode = PharmaceuticalEnumerationEncodings.GetTypeEncoding(pharmaceutical.Type);
            uint pharmaceuticalManufacturerToEncode = PharmaceuticalEnumerationEncodings.GetManufacturerEncoding(pharmaceutical.Manufacturer);
            uint pharmaceuticalBrandToEncode = PharmaceuticalEnumerationEncodings.GetBrandEncoding(pharmaceutical.Brand);

            // Encode the value
            List<uint> valuesToEncode = new List<uint>
            {
                dateOfManufacturerToEncode,
                batchIDToEncode,
                serializationIDToEncode,
                pharmaceuticalTypeToEncode,
                pharmaceuticalManufacturerToEncode,
                pharmaceuticalBrandToEncode
            };
            ulong encodingWithoutChecksum = EncodeValues(valuesToEncode, GetFieldWidths());
            ulong encodingWithChecksum = ApplyChecksumToEncodedValue(encodingWithoutChecksum);
            ulong fullyEncodedValue = CodecConfiguration.BitMapping.ApplyMapping(
                encodingWithChecksum, 
                CodecConfiguration.KeyConfiguration.GetNumberOfBitsUsedForKey());

            return fullyEncodedValue;
        }
        #endregion

        #region ConvertValueToPharmaceutical()
        /// <summary>
        /// Converts a numeric value into a Pharmaceutical instance based on the given key codec
        /// configuration
        /// </summary>
        /// <param name="fullyEncodedValue">
        /// Numeric equivalent of the a Pharmaceutical
        /// </param>
        /// <returns>
        /// Pharmaceutical equivalent of the given numerically encoded instance
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Decoded batch ID is not valid for a Pharmaceutical
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Decoded serialization ID is not valid for a Pharmaceutical
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Decoded pharmaceutical type is not known
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Decoded pharmaceutical manufacturer is not known
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Decoded pharmaceutical brand is not known
        /// </exception>
        public Pharmaceutical ConvertValueToPharmaceutical(ulong fullyEncodedValue)
        {
            // Remove the bit mapping and checksum, leaving just the encoded pharmaceutical.
            ulong encodingWithChecksum = CodecConfiguration.BitMapping.RemoveMapping(
                fullyEncodedValue,
                CodecConfiguration.KeyConfiguration.GetNumberOfBitsUsedForKey());
            ulong encodingWithoutChecksum = RemoveChecksumFromEncodedValue(encodingWithChecksum);

            // Parse the encoded value to get the individual fields
            List<uint> fieldWidths = GetFieldWidths();
            List<uint> decodedValues = DecodeValues(encodingWithoutChecksum, fieldWidths);

            // Build out the Pharmaceutical instance
            Pharmaceutical p = new Pharmaceutical();
            PharmaceuticalKeyConfiguration keyConfig = CodecConfiguration.KeyConfiguration;

            p.DateOfManufacture = keyConfig.StartingDateOfManufacture.AddDays(decodedValues[0]);
            p.BatchId = (int)(keyConfig.StartingBatchId + decodedValues[1]);
            p.SerializationId = (int)decodedValues[2];
            p.Type = PharmaceuticalEnumerationEncodings.GetType(decodedValues[3]);
            p.Manufacturer = PharmaceuticalEnumerationEncodings.GetManufacturer(decodedValues[4]);
            p.Brand = PharmaceuticalEnumerationEncodings.GetBrand(decodedValues[5]);

            // Perform some final consistency checks.
            EnsureBatchIDIsWithinRange(p.BatchId);
            EnsureSerializationIDIsWithinRange(p.SerializationId);
            EnsureDateOfManufactureIsWithinRange(p.DateOfManufacture);

            return p;
        }
        #endregion
        #endregion

        #region Apply/remove checksum
        #region ApplyChecksumToEncodedValue()
        /// <summary>
        /// Computes the checksum for the encoded value and sets the appropriate bits
        /// </summary>
        /// <param name="encodedValue">
        /// Value for which the checksum is to be computed
        /// </param>
        /// <returns>
        /// Given encoded value with the appropriate bits set for the checksum
        /// </returns>
        protected ulong ApplyChecksumToEncodedValue(ulong encodedValue)
        {
            ulong checksum = ComputeChecksum(encodedValue);
            checksum <<= (int)PharmaceuticalKeyConfiguration.GetMinimumNumberOfBitsRequired();
            return encodedValue |= checksum;
        }
        #endregion

        #region RemoveChecksumFromEncodedValue()
        /// <summary>
        /// Removes the checksum bits from the encoded value
        /// </summary>
        /// <param name="encodedValueWithChecksum">
        /// Encoded value from which the checksum bits should be removed (i.e., cleared)
        /// </param>
        /// <returns>'
        /// Encoded value with the checksum bits cleared
        /// </returns>
        protected ulong RemoveChecksumFromEncodedValue(ulong encodedValueWithChecksum)
        {
            ulong allButChecksumBitmask = (ulong)Math.Pow(
                2,
                PharmaceuticalKeyConfiguration.GetMinimumNumberOfBitsRequired()) - 1;
            return encodedValueWithChecksum & allButChecksumBitmask;
        }
        #endregion
        #endregion

        #region Encode/decode values
        #region EncodeValues()
        /// <summary>
        /// Constructs an encoding based on the given list of values to encode and the lengths of 
        /// their respective fields (in bits)
        /// </summary>
        /// <param name="valuesToEncode">
        /// List of values to encode
        /// </param>
        /// <param name="fieldWidths">
        /// Lengths of the fields (in bits)
        /// </param>
        /// <returns>
        /// Encoded value
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// Either or both parameters are null
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Given lists have different counts of items
        /// </exception>
        protected ulong EncodeValues(List<uint> valuesToEncode, List<uint> fieldWidths)
        {
            if (valuesToEncode == null)
            {
                throw new ArgumentNullException("The list of values to encode cannot be null");
            }

            if (fieldWidths == null)
            {
                throw new ArgumentNullException("The list of field widths cannot be null");
            }

            if (valuesToEncode.Count != fieldWidths.Count)
            {
                throw new ArgumentException(
                    "valuesToEncode",
                    "Values to encode and field widths lists have differing counts of items");
            }

            ulong valueToReturn = 0L;
            uint shiftAmount = 0;
            for (int i = 0; i < valuesToEncode.Count; i++)
            {
                valueToReturn |= ((ulong) valuesToEncode[i]) << (int)shiftAmount;
                shiftAmount += fieldWidths[i];
            }

            return valueToReturn;
        }
        #endregion

        #region DecodeValues()
        /// <summary>
        /// Deconstructs an encoding based on the given value to decode and a list of field lengths 
        /// (in bits)
        /// </summary>
        /// <param name="valueToDecode">
        /// Value to decode
        /// </param>
        /// <param name="fieldWidths">
        /// List of field lengths (in bits)
        /// </param>
        /// <returns>
        /// List of decoded values
        /// </returns>
        protected List<uint> DecodeValues(ulong valueToDecode, List<uint> fieldWidths)
        {
            if (fieldWidths == null)
            {
                throw new ArgumentNullException("The list of field widths cannot be null");
            }

            uint shiftAmount = 0;
            List<uint> decodedValues = new List<uint>();
            foreach (uint fieldWidth in fieldWidths)
            {
                ulong bitmask = (ulong)Math.Pow(2, fieldWidth) - 1;
                ulong shiftedBitmask = bitmask << (int)shiftAmount;
                ulong decodedValue = valueToDecode & shiftedBitmask;
                ulong shiftedDecodedValue = decodedValue >> (int)shiftAmount;
                decodedValues.Add((uint)shiftedDecodedValue);

                shiftAmount += fieldWidth;
            }

            return decodedValues;
        }
        #endregion
        #endregion

        #region Get*ToEncode() methods
        #region GetBatchIDToEncode()
        /// <summary>
        /// Computes the batch ID to encode by comparing the given pharmaceutical's batch ID with 
        /// the starting batch ID found in this class' PharmaceuticalKeyCodecConfiguration instance
        /// </summary>
        /// <param name="pharmaceutical">
        /// Pharmaceutical for which the batch ID encoding is to be computed
        /// </param>
        /// <returns>
        /// Batch ID encoding for the given pharmaceutical
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// Given Pharmaceutical instance is null
        /// </exception>
        /// <exception cref="ApplicationException">
        /// Given Pharmaceutical's batch ID is less than the starting batch ID given in this class' 
        /// PharmaceuticalKeyCodecConfiguration instance
        /// </exception>
        /// <exception cref="ApplicationException">
        /// Given Pharmaceutical's batch ID is greater than the maximum batch ID allowed for the
        /// given key configuration
        /// </exception>
        protected uint GetBatchIDToEncode(Pharmaceutical pharmaceutical)
        {
            EnsureNonNullPharmaceutical(pharmaceutical);
            EnsureBatchIDIsWithinRange(pharmaceutical.BatchId);

            PharmaceuticalKeyConfiguration keyConfiguration = CodecConfiguration.KeyConfiguration;
            int batchIDToEncode = pharmaceutical.BatchId - (int)keyConfiguration.StartingBatchId;
            
            return (uint) batchIDToEncode;
        }
        #endregion

        #region GetSerializationIDToEncode()
        /// <summary>
        /// Computes the serialization ID to encode based on the given Pharmaceutical's 
        /// serialization ID
        /// </summary>
        /// <param name="pharmaceutical">
        /// Pharmaceutical for which the serialization ID encoding is to be computed
        /// </param>
        /// <returns>
        /// Serialization ID encoding for the given pharmaceutical
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// Given Pharmaceutical instance is null
        /// </exception>
        /// <exception cref="ApplicationException">
        /// Given Pharmaceutical's serialization ID is greater than the maximum batch ID allowed 
        /// for the given key configuration
        /// </exception>
        protected uint GetSerializationIDToEncode(Pharmaceutical pharmaceutical)
        {
            EnsureNonNullPharmaceutical(pharmaceutical);
            EnsureSerializationIDIsWithinRange(pharmaceutical.SerializationId);

            return (uint)pharmaceutical.SerializationId;
        }
        #endregion

        #region GetDateOfManufactureToEncode()
        /// <summary>
        /// Computes the date of manufacture to encode by comparing the given pharmaceutical's
        /// date of manufacture with the starting date of manufacture found in this class'
        /// PharmaceuticalKeyCodecConfiguration instance
        /// </summary>
        /// <param name="pharmaceutical">
        /// Pharmaceutical for which the date of manufacture encoding is to be computed
        /// </param>
        /// <returns>
        /// Number of days between the starting date of manufacture and the given pharmaceutical's
        /// date of manufacture
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// Given Pharmaceutical instance is null
        /// </exception>
        /// <exception cref="ApplicationException">
        /// Given Pharmaceutical's date of manufacture occurs before the starting date of 
        /// manufacture given in this class' PharmaceuticalKeyCodecConfiguration instance
        /// </exception>
        protected uint GetDateOfManufactureToEncode(Pharmaceutical pharmaceutical)
        {
            EnsureNonNullPharmaceutical(pharmaceutical);

            // Find the number of days that have elapsed since the starting date of manufacture
            PharmaceuticalKeyConfiguration keyConfiguration = CodecConfiguration.KeyConfiguration;
            TimeSpan daysFromStartingDate = pharmaceutical.DateOfManufacture - keyConfiguration.StartingDateOfManufacture;
            int dateOfManufacturerToEncode = daysFromStartingDate.Days;
            if (dateOfManufacturerToEncode < 0)
            {
                throw new ApplicationException("The date of manufacture (" +
                    pharmaceutical.DateOfManufacture.ToString() +
                    ") cannot occur before the starting date (" +
                    keyConfiguration.StartingDateOfManufacture.ToString() +
                    ")");
            }

            return (uint) dateOfManufacturerToEncode;
        }
        #endregion
        #endregion

        #region GetFieldWidths()
        /// <summary>
        /// Returns a list of field widths (in bits) in order starting with the right-most (i.e.,
        /// on the least-significant side) field
        /// </summary>
        /// <returns>
        /// List of field widths (in bits)
        /// </returns>
        protected List<uint> GetFieldWidths()
        {
            List<uint> fieldWidths = new List<uint>
            {
                CodecConfiguration.KeyConfiguration.GetNumberOfBitsForDateOfManufacture(),
                CodecConfiguration.KeyConfiguration.GetNumberOfBitsForBatchId(),
                CodecConfiguration.KeyConfiguration.GetNumberOfBitsForSerializationId(),
                CodecConfiguration.KeyConfiguration.GetNumberOfBitsForType(),
                CodecConfiguration.KeyConfiguration.GetNumberOfBitsForManufacturer(),
                CodecConfiguration.KeyConfiguration.GetNumberOfBitsForBrand()
            };
            return fieldWidths;
        }
        #endregion

        #region Ensure*IsWithinRange() methods
        #region EnsureBatchIDIsWithinRange()
        /// <summary>
        /// Determines if the given batch ID is within range (i.e., between the starting batch ID
        /// and maximum batch ID (inclusive) specified by this class' key configuration)
        /// </summary>
        /// <param name="batchIDToValidate">
        /// Batch ID to validate
        /// </param>
        /// <exception cref="ApplicationException">
        /// Given batch ID is less than the starting batch ID
        /// </exception>
        /// <exception cref="ApplicationException">
        /// Given batch ID is greater than the maximum batch ID supported
        /// </exception>
        protected void EnsureBatchIDIsWithinRange(int batchIDToValidate)
        {
            PharmaceuticalKeyConfiguration keyConfiguration = CodecConfiguration.KeyConfiguration;
            int batchIDToEncode = batchIDToValidate - (int)keyConfiguration.StartingBatchId;

            // The given batch ID is less than starting batch ID.
            if (batchIDToEncode < 0)
            {
                throw new ApplicationException("The batch ID (" + batchIDToValidate +
                    ") cannot be less than the starting batch ID (" +
                    keyConfiguration.StartingBatchId + ")");
            }

            // The given batch ID is greater than the ending batch ID
            if (batchIDToValidate > keyConfiguration.GetMaxBatchIdSupported())
            {
                throw new ApplicationException("The batch ID (" + batchIDToValidate + 
                    ") is too large to be encoded; the largest accepted is " +
                    keyConfiguration.GetMaxBatchIdSupported());
            }
        }
        #endregion

        #region EnsureSerializationIDIsWithinRange()
        /// <summary>
        /// Determines if the given serialization ID is within range (i.e., less than or equal to
        /// the maximum seriaizliation ID allowed)
        /// </summary>
        /// <param name="serializationIDToValidate">
        /// Serialization ID to validate
        /// </param>
        /// <exception cref="ApplicationException">
        /// Given serialization ID is greater than the maximum serialization ID supported
        /// </exception>
        /// <remarks>
        /// This method does not ensure that the given serialization ID to validate is negative as
        /// the Pharmaceutical class prevents such an assignment.
        /// </remarks>
        protected void EnsureSerializationIDIsWithinRange(int serializationIDToValidate)
        {
            // The given batch ID is greater than the ending batch ID
            if (serializationIDToValidate > PharmaceuticalKeyConfiguration.GetMaxSerializationIdSupported())
            {
                throw new ApplicationException("The serialization ID (" + serializationIDToValidate +
                    ") is too large to be encoded; the largest accepted is " +
                    PharmaceuticalKeyConfiguration.GetMaxSerializationIdSupported());
            }
        }
        #endregion

        #region EnsureDateOfManufactureIsWithinRange()
        /// <summary>
        /// Determines if the given date of manufacture is within range (i.e., between the starting
        /// date of manufacture and the ending date of manufacture (inclusive) specified by this 
        /// class' key configuration)
        /// </summary>
        /// <param name="dateToValidate">
        /// Date of manufacture to validate
        /// </param>
        /// <exception cref="ApplicationException">
        /// Given date of manufacture is earlier than the starting date of manufacture
        /// </exception>
        /// <exception cref="ApplicationException">
        /// Given date of manufacture is later than the ending date of manufacture
        /// </exception>
        protected void EnsureDateOfManufactureIsWithinRange(DateTime dateToValidate)
        {
            // Find the number of days that have elapsed since the starting date of manufacture
            PharmaceuticalKeyConfiguration keyConfiguration = CodecConfiguration.KeyConfiguration;
            TimeSpan daysFromStartingDate = dateToValidate - keyConfiguration.StartingDateOfManufacture;
            int dateOfManufacturerToEncode = daysFromStartingDate.Days;
            
            // Date of manufacture cannot be before the starting date of manufacture
            if (dateOfManufacturerToEncode < 0)
            {
                throw new ApplicationException("The date of manufacture (" + dateToValidate +
                    ") cannot occur before the starting date (" +
                    keyConfiguration.StartingDateOfManufacture.ToString() + ")");
            }

            // Date of manufacture cannot occur after last possible starting date of manufacture
            TimeSpan daysFromEndingDate = dateToValidate - keyConfiguration.GetEndingDateOfManufacture();
            if (daysFromEndingDate.Days > 0)
            {
                throw new ApplicationException("The date of manufacture (" + dateToValidate +
                    ") cannot be encoded; the latest date accepted is " +
                    keyConfiguration.GetEndingDateOfManufacture());
            }
        }
        #endregion
        #endregion
    }
}
