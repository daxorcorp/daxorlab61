﻿using System;
using System.Globalization;
using System.Text;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;

namespace Daxor.Lab.InjectateVerification
{
    /// <summary>
    /// Represents a lot number for a Pharmaceutical. The lot number's format is defined by a 
    /// clinical/manufacturing entity.
    /// </summary>
    public class PharmaceuticalLotNumberService : ILotNumberGenerator
    {
        #region Constants
        
        public static readonly int DigitsForBatchId = 4;

        public static readonly int DigitsForDayComponent = 2;

        public static readonly int DigitsForMonthComponent = 2;

        public static readonly int DigitsForYearComponent = 2;

        public static readonly char BatchIdSeparator = '-';

        public static readonly string DaxorLotNumberPrefix = "D";

        public static readonly string IsoTexLotNumberPrefix = "V";

        public static readonly string UndefinedLotNumberPrefix = "?";

        public static readonly string LotNumberOutOfRangeExceptionMessage =
            "Unable to use the given Pharmaceutical's batch ID ({0}), " +
            "as the current lot number scheme only supports lot numbers between " +
            "{1} and {2}";

        public static readonly string UndefinedFieldExceptionMessage = "The following fields must be defined to " +
                        "generate the lot number: {0}";
        #endregion

        #region Private member variables

        private readonly PharmaceuticalKeyConfiguration _configuration;
        
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the PharmaceuticalLotNumberService class
        /// </summary>
        /// <param name="configuration">
        /// PharmaceuticalKeyConfiguration instance used to verify that a lot number can be
        /// constructed from a given Pharmaceutical instance
        /// </param>
        /// <exception cref="ArgumentNullException">If the given configuration is null</exception>
        public PharmaceuticalLotNumberService(PharmaceuticalKeyConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException("configuration");

            _configuration = configuration;
        }

        #endregion

        #region Methods
        
        /// <summary>
        /// Returns the single-character prefix based on the manufacturer of a given Pharmaceutical 
        /// instance
        /// </summary>
        /// <param name="pharmaceutical">
        /// Pharmaceutical whose manufacturer field will be used to determine the prefix
        /// </param>
        /// <returns>
        /// A single character pertaining to the manufacturer; if the pharmaceutical does not have
        /// a corresponding prefix character, then the result is UndefinedLotNumberPrefix
        /// </returns>
        /// <remarks>
        /// This method is used by the Generate() method.
        /// </remarks>
        /// <exception cref="ArgumentNullException">
        /// Given Pharmaceutical instance is null
        /// </exception>
        public static string GetLotPrefix(Pharmaceutical pharmaceutical)
        {
            if (pharmaceutical == null)
                throw new ArgumentNullException("pharmaceutical");

            if (pharmaceutical.Manufacturer == Pharmaceutical.PharmaceuticalManufacturer.Daxor)
                return DaxorLotNumberPrefix;

            if (pharmaceutical.Manufacturer == Pharmaceutical.PharmaceuticalManufacturer.IsoTex)
                return IsoTexLotNumberPrefix;
            
            return UndefinedLotNumberPrefix;
        }

        /// <summary>
        /// Constructs a lot number for the given Pharmaceutical
        /// </summary>
        /// <param name="pharmaceutical">
        /// Pharmaceutical on which the returned lot number will be based
        /// </param>
        /// <returns>
        /// Lot number based for the given Pharmaceutical
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// Either or both of the method arguments are null
        /// </exception>
        /// <exception cref="InvalidOperationException">
        /// All fields for the given Pharmaceutical are not defined
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// The batch ID for the given Pharmaceutical exceeds the maximum supported batch ID (as 
        /// defined in PharmaceuticalConfiguration)
        /// </exception>
        /// <remarks>
        /// The lot number gets its own class because the Pharmaceutical can have any batch ID. 
        /// However, this lot number representation scheme is limited to a certain number of 
        /// batches.
        /// </remarks>
        public string Generate(Pharmaceutical pharmaceutical)
        {
            EnsureAllFieldsAreDefined(pharmaceutical);

            var yearIntPart = pharmaceutical.DateOfManufacture.Year % (int)Math.Pow(10, DigitsForYearComponent);
            var yearPart = yearIntPart.ToString(CultureInfo.InvariantCulture).PadLeft(DigitsForYearComponent, '0');
            var dayPart = pharmaceutical.DateOfManufacture.Day.ToString(CultureInfo.InvariantCulture).PadLeft(DigitsForDayComponent, '0');
            var monthPart = pharmaceutical.DateOfManufacture.Month.ToString(CultureInfo.InvariantCulture).PadLeft(DigitsForMonthComponent, '0');
            var batchIdPart = pharmaceutical.BatchId.ToString(CultureInfo.InvariantCulture).PadLeft(DigitsForBatchId, '0');

            var lot = new StringBuilder();
            lot.Append(GetLotPrefix(pharmaceutical));
            lot.Append(yearPart.ToString(CultureInfo.InvariantCulture));
            lot.Append(dayPart);
            lot.Append(monthPart);
            lot.Append(BatchIdSeparator);
            lot.Append(batchIdPart);

            return lot.ToString();
        }
        
        #endregion

        #region Helper Methods
        
        private void EnsureAllFieldsAreDefined(Pharmaceutical pharmaceutical)
        {
            if (pharmaceutical == null)
            {
                throw new ArgumentNullException("pharmaceutical");
            }

            if (pharmaceutical.BatchId > _configuration.GetMaxBatchIdSupported())
            {
                throw new ArgumentOutOfRangeException(String.Format(LotNumberOutOfRangeExceptionMessage,
                                                                    pharmaceutical.BatchId,
                                                                    _configuration.StartingBatchId,
                                                                    _configuration.GetMaxBatchIdSupported()),
                                                      (Exception) null);
            }

            var fieldsToDefine = new StringBuilder();
            if (!pharmaceutical.HasDateOfManufacture())
                fieldsToDefine.Append("DateOfManufacture ");

            if (!pharmaceutical.HasBatchId())
                fieldsToDefine.Append("BatchID ");

            if (!pharmaceutical.HasManufacturer())
                fieldsToDefine.Append("Manufacturer ");

            if (!pharmaceutical.HasType())
                fieldsToDefine.Append("Type ");

            if (!pharmaceutical.HasBrand())
                fieldsToDefine.Append("Brand ");

            if (fieldsToDefine.ToString() != String.Empty)
                throw new InvalidOperationException(String.Format(UndefinedFieldExceptionMessage,
                                                                  fieldsToDefine));
        }
        
        #endregion
    }
}