﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Daxor.Lab.InjectateVerification
{
    /// <summary>
    /// Extension methods for making bitwise operations involving unsigned longs
    /// more readable
    /// </summary>
    public static class UnsignedLongBitwiseOperationExtensions
    {
        /// <summary>
        /// Number of bits in an unsigned long value
        /// </summary>
        public const int NumberOfBitsForUnsignedLong = sizeof (ulong)*8;

        /// <summary>
        /// Gets the bit value (true/false) of a given position in a given unsigned long 
        /// </summary>
        /// <param name="value">Value of the unsigned long</param>
        /// <param name="position">Desired bit position (0 == least significant bit,
        /// sizeof(ulong)*8 - 1 == most significant bit)</param>
        /// <returns>Bit value (true for set, false for cleared)</returns>
        /// <exception cref="ArgumentOutOfRangeException">If position is negative
        /// or beyond the number of bits in a ulong</exception>
        public static bool GetBitAtPosition(this ulong value, int position)
        {
            if (position < 0 || position >= NumberOfBitsForUnsignedLong)
                throw new ArgumentOutOfRangeException("position");

            return value.ToBits().ElementAt(position);
        }

        /// <summary>
        /// Sets or clears the bit at the given position, returning the modified value
        /// </summary>
        /// <param name="value">Value of the unsigned long</param>
        /// <param name="bitValue">Bit value (true for set, false for cleared)</param>
        /// <param name="position">Desired bit position (0 == least significant bit,
        /// sizeof(ulong)*8 - 1 == most significant bit)</param>
        /// <returns>New unsigned long value with the given bit set or cleared</returns>
        /// <exception cref="ArgumentOutOfRangeException">If position is negative
        /// or beyond the number of bits in a ulong</exception>
        public static ulong PlaceBitAtPosition(this ulong value, bool bitValue, int position)
        {
            if (position < 0 || position >= NumberOfBitsForUnsignedLong)
                throw new ArgumentOutOfRangeException("position");

            var bits = new List<bool>(value.ToBits());
            bits[position] = bitValue;
            return bits.ToUnsignedLong();
        }

        /// <summary>
        /// Returns a list of "bits" for the given value where the first element
        /// in the list represents the least-significant bit of the given value
        /// </summary>
        /// <param name="value">Value to use for the bit list</param>
        /// <returns>List of booleans representing bit states</returns>
        /// <remarks>The list is the backward version of the binary equivalent
        /// of the number so that code using the list will be more readable</remarks>
        public static IEnumerable<bool> ToBits(this ulong value)
        {
            var bitList = new List<bool>();
            for (var i = 0; i < NumberOfBitsForUnsignedLong; i++)
            {
                var bitMask = ((ulong)1) << i;
                bitList.Add(((value & bitMask) >> i) != 0);
            }
            return bitList;
        }

        /// <summary>
        /// Returns an unsigned long corresponding to the given bit pattern,
        /// where the first bit is the least significant bit
        /// </summary>
        /// <param name="bits">Bit pattern to convert</param>
        /// <returns>Unsigned long representing the given bits</returns>
        /// <exception cref="ArgumentException">If there are too many bits to
        /// fit in an unsigned long</exception>
        public static ulong ToUnsignedLong(this IEnumerable<bool> bits)
        {
            var bitsAsArray = bits as bool[] ?? bits.ToArray();
            var numBits = bitsAsArray.Count();
            if (numBits > NumberOfBitsForUnsignedLong)
                throw new ArgumentException("Maximum number of bits is " + NumberOfBitsForUnsignedLong, "bits");

            ulong valueToReturn = 0;
            for (var i = 0; i < numBits; i++)
            {
                if (!bitsAsArray.ElementAt(i)) continue;
                valueToReturn |= ((ulong)1) << i;
            }

            return valueToReturn;
        }
    }
}
