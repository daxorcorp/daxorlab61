﻿using System;

namespace Daxor.Lab.InjectateVerification
{
    /// <summary>
    /// Represents a container of several class instances needed for the PharmaceuticalKeyCodec to
    /// work properly. The encoding and decoding of a Pharmaceutical depends on (1) how the key is 
    /// represented as a string (e.g., hexadecimal, alphanumeric), (2) what kind of checksum the 
    /// key will use for internal validation, (3) the ordering of the bits for the key, and (4) 
    /// specific constraints of the Pharmaceutical's key.
    /// </summary>
    /// <seealso cref="IChecksumGenerator"/>
    /// <seealso cref="KeyRepresentation"/>
    /// <seealso cref="BitMapping"/>
    /// <seealso cref="PharmaceuticalKeyConfiguration"/>
    public class PharmaceuticalKeyCodecConfiguration
    {
        #region Constants
        
        public const string ToStringFormat =
                "[checksumgenerator=\"{0}\"; keyrepresentation=\"{1}\"; bitmapping=\"{2}\"; keyconfig=\"{3}\"]";
        
        #endregion

        #region Fields

        private readonly PharmaceuticalKeyCodecBitMapping _bitMapping;
        private readonly IChecksumGenerator _checksumGenerator;
        private readonly PharmaceuticalKeyConfiguration _keyConfiguration;
        private readonly KeyRepresentation _representation;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of the PharmaceuticalKeyCodecConfiguration class.
        /// </summary>
        /// <param name="checksumGenerator">
        /// Instance of an IChecksumGenerator to be used by the codec
        /// </param>
        /// <param name="keyRepresentation">
        /// Instance of a KeyRepresentation to be used by the codec
        /// </param>
        /// <param name="pharmaceuticalKeyCodecBitMapping">
        /// Instance of a PharmaceuticalKeyCodecBitMapping to be used by the codec
        /// </param>
        /// <param name="pharmaceuticalKeyConfiguration">
        /// Instance of the PharmaceuticalKeyConfiguration to be used by the codec
        /// </param>
        /// <exception cref="ArgumentNullException">If any of the arguments are null</exception>
        public PharmaceuticalKeyCodecConfiguration(
            IChecksumGenerator checksumGenerator,
            KeyRepresentation keyRepresentation,
            PharmaceuticalKeyCodecBitMapping pharmaceuticalKeyCodecBitMapping,
            PharmaceuticalKeyConfiguration pharmaceuticalKeyConfiguration)
        {
            if (checksumGenerator == null) 
                throw new ArgumentNullException("checksumGenerator");
            if (keyRepresentation == null) 
                throw new ArgumentNullException("keyRepresentation");
            if (pharmaceuticalKeyCodecBitMapping == null) 
                throw new ArgumentNullException("pharmaceuticalKeyCodecBitMapping");
            if (pharmaceuticalKeyConfiguration == null) 
                throw new ArgumentNullException("pharmaceuticalKeyConfiguration");

            _representation = keyRepresentation;
            _checksumGenerator = checksumGenerator;
            _bitMapping = pharmaceuticalKeyCodecBitMapping;
            _keyConfiguration = pharmaceuticalKeyConfiguration;
        }
        
        #endregion

        #region Properties
        
        /// <summary>
        /// Gets or sets the PharmaceuticalKeyCodecBitMapping instance for this codec configuration
        /// instance
        /// </summary>
        /// <remarks>
        /// This property was not named "PharmaceuticalKeyCodecBitMapping", as that is the name of 
        /// the type of instance returned.
        /// </remarks>
        public PharmaceuticalKeyCodecBitMapping BitMapping { get { return _bitMapping; } }
        
        /// <summary>
        /// Gets or sets the IChecksumGenerator instance for this codec configuration instance
        /// </summary>
        public IChecksumGenerator ChecksumGenerator { get { return _checksumGenerator; } }

        /// <summary>
        /// Gets or sets the PharmaceuticalKeyConfiguration instance for this codec configuration 
        /// instance
        /// </summary>
        /// <remarks>
        /// This property was not named "PharmaceuticalKeyConfiguration", as that is the name of 
        /// the type of instance returned.
        /// </remarks>
        public PharmaceuticalKeyConfiguration KeyConfiguration { get { return _keyConfiguration; } }

        /// <summary>
        /// Gets or sets the KeyRepresentation for this codec configuration instance
        /// </summary>
        /// <remarks>
        /// This property was not named "KeyRepresentation", as that is the name of the type of 
        /// instance returned.
        /// </remarks>
        public KeyRepresentation Representation { get { return _representation; } }

        #endregion

        /// <summary>
        /// Determines the number of characters needed for the required pharmaceutical fields plus
        /// the checksum
        /// </summary>
        /// <returns>
        /// Number of characters needed in the product key given the number of bits required and
        /// key representation
        /// </returns>
        public uint GetNumberOfCharactersInKey()
        {
            // If there are no characters in the alphabet, the key is implicitly zero 
            // characters in length.
            if (Representation.RepresentationSize == 0)
                return 0;

            var numBitsUsed = KeyConfiguration.GetNumberOfBitsUsedForKey();
            var numBitsPerChar = Math.Log(Representation.RepresentationSize, 2);
            var numCharsInKey = (uint)Math.Ceiling(numBitsUsed / numBitsPerChar);

            return numCharsInKey;
        }

        /// <summary>
        /// Returns a string containing human-readable state information about this key codec
        /// configuration instance.
        /// </summary>
        /// <returns>
        /// String containing state information about this key codec configuration
        /// </returns>
        /// <remarks>
        /// Even thought it's impossible for ToString to be called when any of the properties are false
        /// the MS Test Runner will still choke if this if check isn't here.
        /// </remarks>
        public override string ToString()
        {
            if (Representation == null || ChecksumGenerator == null ||
                BitMapping == null || KeyConfiguration == null)
            {
                return "Not breaking the Unit Test";
            }

            return String.Format(ToStringFormat, ChecksumGenerator.DisplayName, 
                Representation.DisplayName, BitMapping, KeyConfiguration);
        }
    }
}
