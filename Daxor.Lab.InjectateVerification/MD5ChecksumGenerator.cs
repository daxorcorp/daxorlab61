﻿using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace Daxor.Lab.InjectateVerification
{
    /// <summary>
    /// Represents a checksum generator that always returns zero (i.e., there is no checksum).
    /// </summary>
    // ReSharper disable InconsistentNaming
    public class MD5ChecksumGenerator : IChecksumGenerator
    // ReSharper restore InconsistentNaming
    {
        /// <summary>
        /// Gets the full-text name of this checksum generator
        /// </summary>
        public string DisplayName
        {
            get { return "Message-Digest algorithm 5"; }
        }

        /// <summary>
        /// Generates a checksum based on the given input value
        /// </summary>
        /// <param name="inputValue">
        /// Value for which the checksum is computed
        /// </param>
        /// <returns>
        /// The leading 64 bits of the MD5 hash of the input value
        /// </returns>
        public ulong GenerateChecksum(ulong inputValue)
        {
            var hashGenerator = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            var inputValueAsByteArray = Encoding.UTF8.GetBytes(inputValue.ToString(CultureInfo.InvariantCulture));
            var hashData = hashGenerator.ComputeHash(inputValueAsByteArray);

            return BitConverter.ToUInt64(hashData, 0);
        }
    }
}
