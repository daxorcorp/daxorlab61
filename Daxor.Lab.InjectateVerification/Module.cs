﻿//-----------------------------------------------------------------------
// <copyright file="InjectateVerificationModule.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.InjectateVerification
{
    /// <summary>
    /// Module that facilitates injectate verification
    /// </summary>
    public class Module : IModule
    {
        #region Constants
        /// <summary>
        /// Value in the settings DB corresponding to the alphanumeric-56 key representation
        /// </summary>
        private const string Alphanumeric56RepresentationString = "Alphanumeric 56";

        /// <summary>
        /// Value in the settings DB corresponding to the MD5 checksum generator
        /// </summary>
        // ReSharper disable once InconsistentNaming
        private const string MD5ChecksumGeneratorString = "MD5";

        /// <summary>
        /// First ASCII value used for interpretting the bit mapping string retrieved
        /// from the settings DB.
        /// </summary>
        private static readonly int StartingBitMappingAsciiValue = 33;

		/// <summary>
		/// Name of this Module
		/// </summary>
	    private const string ModuleName = "Injectate Verification";

        #endregion

        #region Fields
        private readonly IUnityContainer _container;
        private readonly ISettingsManager _settingsManager;
        
        #endregion

        #region __Constructor
        /// <summary>
        /// Initializes a new instance of the InjectateVerificationModule class
        /// </summary>
        /// <param name="container">
        /// Instance of an IoC container
        /// </param>
        /// <param name="settingsManager">
        /// Instance of a settings manager
        /// </param>
        public Module(IUnityContainer container, ISettingsManager settingsManager)
        {
            _container = container;
            _settingsManager = settingsManager;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets this instance's IoC container instance
        /// </summary>
        private IUnityContainer Container
        {
            get 
            { 
                return _container; 
            }
        }

        private ISettingsManager SettingsManager
        {
            get { return _settingsManager; }
        }

        #endregion

        #region Initialize()
        /// <summary>
        /// Initializes this module by (1) registering the configuration instance for the 
        /// PharmaceuticalKeyService class, (2) registering the PharmaceuticalKeyService class
        /// with the IPharmaceuticalKeyDecoder interface, and (3) registering the 
        /// PharmaceuticalLotNumberService class with the ILotNumberGenerator interface.
        /// </summary>
        /// <seealso cref="IPharmaceuticalKeyDecoder"/>
        /// <seealso cref="PharmaceuticalKeyCodecConfiguration"/>
        /// <seealso cref="PharmaceuticalKeyService"/>
        public void Initialize()
        {
	        RegisterPharmaceuticalKeyCodecConfiguration();
	        RegisterPharmaceuticalKeyConfiguration();
	        RegisterPharmaceuticalKeyService();
	        RegisterPharmaceuticalLotNumberService();
        }

        #endregion

        #region __Configuration-loading methods
        
        #region GetKeyLength()
        /// <summary>
        /// Gets the expected length of a pharmaceutical key string to use for injectate 
        /// verification from the settings manager.
        /// </summary>
        /// <returns>
        /// Length of the pharmaceutical key string
        /// </returns>
        private uint GetKeyLength()
        {
            return (uint) SettingsManager.GetSetting<int>(SettingKeys.InjectateVerificationKeyLength);
        }
        #endregion

        #region GetStartingBatchID()
        /// <summary>
        /// Gets the starting batch ID to use for injectate verification from the settings
        /// manager.
        /// </summary>
        /// <returns>
        /// Starting batch ID
        /// </returns>
        // ReSharper disable once InconsistentNaming
        private uint GetStartingBatchID()
        {
            return (uint) SettingsManager.GetSetting<int>(SettingKeys.InjectateVerificationStartingBatchId);
        }
        #endregion

        #region GetStartingDateOfManufacture()
        /// <summary>
        /// Gets the starting date of manufacture to use for injectate verification from the
        /// settings manager.
        /// </summary>
        /// <returns>
        /// Starting date of manufacture
        /// </returns>
        private DateTime GetStartingDateOfManufacture()
        {
            return SettingsManager.GetSetting<DateTime>(SettingKeys.InjectateVerificationStartingDateOfManufacturing);
        }
        #endregion

        #region GetKeyRepresentation()
        /// <summary>
        /// Gets the key representation to use for injectate verification from the settings
        /// manager.
        /// </summary>
        /// <returns>
        /// KeyRepresentation instance
        /// </returns>
        /// <exception cref="ApplicationException">
        /// Desired key representation name retrieved from the settings manager is not recognized
        /// </exception>
        private KeyRepresentation GetKeyRepresentation()
        {
            var desiredKeyRepresentation = SettingsManager.GetSetting<string>(SettingKeys.InjectateVerificationKeyRepresentation);
            KeyRepresentation keyRepresentationInstance;
            switch (desiredKeyRepresentation)
            {
                case Alphanumeric56RepresentationString: keyRepresentationInstance = new Alphanumeric56KeyRepresentation();
                    break;
                default:
                    throw new ApplicationException("Unrecognized key representation '" + 
                        desiredKeyRepresentation + "' retrieved from settings manager while " +
                        "initializing the injectate verification module");
            }

            return keyRepresentationInstance;
        }
        #endregion

        #region GetKeyConfiguration()
        /// <summary>
        /// Gets the key configuration to use with injectate verification
        /// </summary>
        /// <returns>
        /// PharmaceuticalKeyConfiguration instance
        /// </returns>
        /// <exception cref="ApplicationException">
        /// Number of values to represented cannot be determined
        /// </exception>
        /// <exception cref="ApplicationException">
        /// Number of bits needed for checksum is computed to be less than zero
        /// </exception>
        protected virtual PharmaceuticalKeyConfiguration GetKeyConfiguration()
        {
            // Calculate how many variables can be represented using the default key
            // representation and the default key length.
            ulong numberOfTotalValues = (ulong)Math.Pow(
                GetKeyRepresentation().RepresentationSize,
                GetKeyLength());
            if (numberOfTotalValues < 1)
            {
                throw new ApplicationException("Unable to determine the number of " +
                    "total values to represent using the '" +
                    GetKeyRepresentation().DisplayName + "' representation with a default " +
                    "key length of " + GetKeyLength());
            }

            // Calculate how many checksum bits will fit into the desired key length after
            // accounting for the number of bits to store the required parts.
            uint numberOfBitsInKey = (uint)Convert.ToInt32(Math.Floor(Math.Log(numberOfTotalValues, 2)));
            int numberOfBitsForChecksum = (int)numberOfBitsInKey -
                (int)PharmaceuticalKeyConfiguration.GetMinimumNumberOfBitsRequired();
            if (numberOfBitsForChecksum < 0)
            {
                throw new ApplicationException("Unable to represent the minimum set of " +
                    "required fields (needing " +
                    PharmaceuticalKeyConfiguration.GetMinimumNumberOfBitsRequired() + " bit(s)) " +
                    "with only " + numberOfBitsInKey + " bit(s), which is based on a key " +
                    "length of " + GetKeyLength() + " characters");
            }

            return new PharmaceuticalKeyConfiguration(
                GetStartingDateOfManufacture(),
                GetStartingBatchID(),
                (uint)numberOfBitsForChecksum);
        }
        #endregion

        #region GetChecksumGenerator()
        /// <summary>
        /// Gets the checksum generator to use for injectate verification
        /// </summary>
        /// <returns>
        /// IChecksumGenerator instance
        /// </returns>
        /// <exception cref="ApplicationException">
        /// Desired checksum generator name retrieved from the settings manager is not recognized
        /// </exception>
        private IChecksumGenerator GetChecksumGenerator()
        {
            var desiredChecksum = SettingsManager.GetSetting<string>(SettingKeys.InjectateVerificationChecksum);
            IChecksumGenerator generatorInstance;
            switch (desiredChecksum)
            {
                case MD5ChecksumGeneratorString: generatorInstance = new MD5ChecksumGenerator();
                    break;
                default:
                    throw new ApplicationException("Unrecognized checksum generator '" +
                        desiredChecksum + "' retrieved from settings manager while " +
                        "initializing the injectate verification module");
            }

            return generatorInstance;
        }
        #endregion

        #region GetCodecConfiguration()
        /// <summary>
        /// Gets the pharmaceutical key codec configuration to use for injectate verification
        /// </summary>
        /// <returns>
        /// PharmaceuticalKeyCodecConfiguration instance
        /// </returns>
        protected virtual PharmaceuticalKeyCodecConfiguration GetCodecConfiguration()
        {
            return new PharmaceuticalKeyCodecConfiguration(
                GetChecksumGenerator(),
                GetKeyRepresentation(),
                GetBitMapping(),
                GetKeyConfiguration());
        }
        #endregion

        #region GetBitMapping()
        /// <summary>
        /// Gets the pharmaceutical key codec bit mapping to use for injectate verification
        /// from the settings manage.
        /// </summary>
        /// <returns>
        /// PharmaceuticalKeyCodecBitMapping instance
        /// </returns>
        /// <exception cref="ApplicationException">
        /// Any value in the bit ordering retrieved from the settings manager cannot be
        /// converted to an appropriate byte value.
        /// </exception>
        private PharmaceuticalKeyCodecBitMapping GetBitMapping()
        {
            var bitOrder = SettingsManager.GetSetting<string>(SettingKeys.InjectateVerificationBitMapping);
            var byteList = new List<byte>();
            for (var i = 0; i < bitOrder.Length; i++)
            {
                // Encoding cipher: Add 'StartingBitMappingAsciiValue' to the true value, then
                // convert into a character. For example, starting value of 33 means that
                // a byte value of zero will be interpretted as '!', one as '"', two as '#', etc.
                var adjustedValue = bitOrder[i] - StartingBitMappingAsciiValue;

                if (adjustedValue < 0)
                    throw new ApplicationException("Invalid bit position value '" +
                        bitOrder[i] + "' (value = " + adjustedValue + ") at index " + i + 
                        " retrieved from settings manager while initializing the injectate verification module");
                if (adjustedValue > byte.MaxValue)
                    throw new ApplicationException("Too-high bit position value '" +
                        bitOrder[i] + "' (value = " + adjustedValue + ") at index " + i +
                        " retrieved from settings manager while initializing the injectate verification module");
                byteList.Add((byte)adjustedValue);
            }

            return new PharmaceuticalKeyCodecBitMapping(byteList);
        }
        #endregion

        #endregion

        #region Initialize helpers

        private void RegisterPharmaceuticalLotNumberService()
        {
            try
            {
                Container.RegisterType<ILotNumberGenerator, PharmaceuticalLotNumberService>(
                    new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the Pharmaceutical Lot Number Service");
            }
        }

        private void RegisterPharmaceuticalKeyService()
        {
            try
            {
                Container.RegisterType<IPharmaceuticalKeyDecoder, PharmaceuticalKeyService>(
                    new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the Pharmaceutical Key Service");
            }
        }

        private void RegisterPharmaceuticalKeyConfiguration()
        {
            try
            {
                Container.RegisterInstance(typeof(PharmaceuticalKeyConfiguration), GetKeyConfiguration(),
                    new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the Pharmaceutical Key Configuration");
            }
        }

        private void RegisterPharmaceuticalKeyCodecConfiguration()
        {
            try
            {
                Container.RegisterInstance(typeof(PharmaceuticalKeyCodecConfiguration), GetCodecConfiguration(),
                    new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the Pharmaceutical Key Codec Configuration");
            }
        }

        #endregion
    }
}
