﻿//-----------------------------------------------------------------------
// <copyright file="KeyRepresentation.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Daxor.Lab.InjectateVerification
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Abstract class that represents an object that can convert to/from a representation. 
    /// Conversions from a representation yield a numeric result, but conversions to a 
    /// representation can be implicitly numeric (e.g. hexadecimal) or alphanumeric (e.g., 
    /// a-z0-9).
    /// </summary>
    /// <remarks>
    /// This class isn't called PharmaceuticalKeyRepresentation because a key representation isn't
    /// restricted to only work with Pharmaceuticals.
    /// </remarks>
    public abstract class KeyRepresentation
    {
        #region __Member variables
        /// <summary>
        /// Private member variable for storing the number-to-character lookup
        /// </summary>
        private Dictionary<ulong, char> _representationLookup = new Dictionary<ulong, char>();
        #endregion

        #region __Constructor
        /// <summary>
        /// Initializes a new instance of the KeyRepresentation class, which initializes the 
        /// representation lookup and ensures unique values in the lookup
        /// </summary>
        /// <exception cref="ArgumentException">
        /// Value in the representation lookup is already present
        /// </exception>
        public KeyRepresentation()
        {
            InitializeRepresentationLookup();
            
            // Note: The Dictionary class already ensures that keys are unique, so we don't need 
            // to check the keys.
            EnsureUniqueValues();
        }
        #endregion

        #region __Properties
        #region RepresentationSize
        /// <summary>
        /// Gets the "base" of this key represenation (i.e., the number of characters
        /// represented)
        /// </summary>
        public uint RepresentationSize
        {
            get { return (uint) RepresenationLookup.Count; }
        }
        #endregion

        #region DisplayName
        /// <summary>
        /// Gets the full-text name of this key representation
        /// </summary>
        /// <remarks>
        /// Subclasses implement this to help in populating UI-related elements and for referring 
        /// to KeyRepresentation by a string name
        /// </remarks>
        public abstract string DisplayName
        {
            get;
        }
        #endregion

        #region RepresentationLookup
        /// <summary>
        /// Gets the key representation lookup structure
        /// </summary>
        protected Dictionary<ulong, char> RepresenationLookup
        {
            get { return _representationLookup; }
        }
        #endregion

        #region RepresentationAsString
        /// <summary>
        /// Gets the represented characters as a string
        /// </summary>
        /// <example>
        /// The method can return "0123456789abcdef" for hexadecimal.
        /// </example>
        /// <remarks>
        /// Subclasses implement this as a means of specifying the characters (and inherently)
        /// the order of those characters without having to access the representation data
        /// structure directly.
        /// </remarks>
        public abstract string RepresentationAsString
        {
            get;
        }
        #endregion
        #endregion

        #region __Convert to/from methods
        #region ConvertToRepresentation()
        /// <summary>
        /// Converts a numeric key into its representation equivalent
        /// </summary>
        /// <param name="keyToRepresent">
        /// Key to represent
        /// </param>
        /// <returns>
        /// Representation of the given key
        /// </returns>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Key to represent is negative
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Key to represent contains one or more values that are not in the corresponding 
        /// character representation
        /// </exception> 
        public string ConvertToRepresentation(ulong keyToRepresent)
        {
            // Negative values aren't allowed, as it cannot be assumed that the underlying 
            // representation supports negative numbers.
            if (keyToRepresent < 0L)
            {
                throw new ArgumentOutOfRangeException("Key to represent '" + 
                    keyToRepresent.ToString() + "' cannot be based on a negative value");
            }

            StringBuilder representation = new StringBuilder();

            // The standard conversion loop won't execute with a value of 0L, so it must be 
            // assigned separately.
            if (keyToRepresent == 0L)
            {
                char zeroChar = GetRepresentationCharacter(0L);
                representation.Insert(0, zeroChar);
            }

            // Use the standard method of converting from decimal to another base. Divide by the 
            // alphabet size, then store the remainders in reverse order. Continue this process 
            // until the working value becomes zero.
            while (keyToRepresent > 0L)
            {
                ulong remainder = keyToRepresent % RepresentationSize;
                char remainderChar = GetRepresentationCharacter(remainder);
                representation.Insert(0, remainderChar);
                keyToRepresent /= RepresentationSize;
            }

            return representation.ToString();
        }
        #endregion

        #region ConvertFromRepresentation()
        /// <summary>
        /// Converts a key from its representation to its numeric equivalent
        /// </summary>
        /// <param name="keyToConvert">
        /// Key to convert
        /// </param>
        /// <returns>
        /// Numeric version of the given key
        /// </returns>
        /// <exception cref="ArgumentException">
        /// Key to convert is an empty string
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Key to convert contains a character that is not in this key representation
        /// </exception>
        public ulong ConvertFromRepresentation(string keyToConvert)
        {
            // Zero-length strings are not assumed to be "0"
            if (keyToConvert.Length == 0)
            {
                throw new ArgumentException(
                    "keyToConvert",
                    "Unable to create a numeric value from an empty " + "string");
            }
            
            // Use the standard method of converting from another base to decimal. Multiply by k^n,
            // where k is the alphabet size and n is the number of slots away from the least 
            // significant digit.
            ulong valueToReturn = 0L;
            double power = 0;
            for (int i = keyToConvert.Length - 1; i >= 0; i--, power++)
            {
                ulong currCharValue = GetValueOfCharacter(keyToConvert[i]);
                double powerOfCount = Math.Pow(RepresenationLookup.Count, power);
                valueToReturn += currCharValue * (ulong)powerOfCount;
            }

            return valueToReturn;
        }
        #endregion
        #endregion

        #region __Methods used by constructor
        #region InitializeRepresentationLookup()
        /// <summary>
        /// Populates the representation lookup structure by adding number-to-character 
        /// relationships.
        /// </summary>
        protected void InitializeRepresentationLookup()
        {
            string alphabetChars = RepresentationAsString;
            int numCharacters = alphabetChars.Length;
            ulong key = 0L;

            // Initialize the dictionary
            RepresenationLookup.Clear();
            for (int i = 0; i < numCharacters; i++)
            {
                // NOTE: Although Dictionary<T>.Add() throws exceptions, the arguments passed to it
                //       will not trigger them.
                RepresenationLookup.Add(key, alphabetChars[i]);
                key++;
            }
        }
        #endregion

        #region EnsureUniqueValues()
        /// <summary>
        /// Ensures that all values in the representation lookup are unique
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// Value in the representation lookup is already present
        /// </exception>
        /// <remarks>
        /// The Dictionary class already ensures that keys are unique, so we don't need to check 
        /// the keys.
        /// </remarks>
        protected void EnsureUniqueValues()
        {
            Dictionary<char, ulong> tempDictionary = new Dictionary<char, ulong>();
            foreach (KeyValuePair<ulong, char> pair in RepresenationLookup)
            {
                try
                {
                    // NOTE: Although Dictionary<T>.Add() throws exceptions, the arguments passed to 
                    //       it will not trigger them.
                    tempDictionary.Add(pair.Value, pair.Key);
                }
                catch (ArgumentException ae)
                {
                    throw new InvalidOperationException("Character value '" + pair.Value + 
                        "' already exists; duplicate character values are not allowed " +
                        "(subexception: " + ae.ToString() + ")");
                }
            }
        }
        #endregion
        #endregion

        #region __Get methods
        #region GetRepresentationCharacter()
        /// <summary>
        /// Returns the character representation of a given numeric value for this key
        /// representation
        /// </summary>
        /// <param name="value">
        /// Numeric value to represent
        /// </param>
        /// <returns>
        /// Character corresponding to the given value
        /// </returns>
        /// <exception cref="ArgumentException">
        /// Value does not have a corresponding character representation
        /// </exception>
        protected char GetRepresentationCharacter(ulong value)
        {
            char charToReturn;

            // Inform the caller if there is no such value.
            if (!RepresenationLookup.TryGetValue(value, out charToReturn))
            {
                string message = "Numeric value '" + value + "' has no equivalent " + 
                    "in this key represenation";
                throw new ArgumentException("value", message);
            }

            return RepresenationLookup[value];
        }
        #endregion

        #region GetValueofCharacter()
        /// <summary>
        /// Returns the numeric representation of the given character value for this key 
        /// representation
        /// </summary>
        /// <param name="c">
        /// Character being represented
        /// </param>
        /// <returns>
        /// Number value corresponding to the given character
        /// </returns>
        /// <exception cref="ArgumentException">
        /// Character has no corresponding numeric value
        /// </exception>
        protected ulong GetValueOfCharacter(char c)
        {
            ulong value = 0L;
            bool charFound = false;

            foreach (KeyValuePair<ulong, char> pair in RepresenationLookup)
            {
                if (pair.Value == c)
                {
                    value = pair.Key;
                    charFound = true;
                    break;
                }
            }

            if (!charFound)
            {
                string message = "There is no value for the character '" + c + "'";
                throw new ArgumentException("c", message);
            }

            return value;
        }
        #endregion
        #endregion
    }
}