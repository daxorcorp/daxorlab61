﻿//-----------------------------------------------------------------------
// <copyright file="PharmaceuticalEnumerationEncodings.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Daxor.Lab.InjectateVerification
{
    using System;
    using System.Collections.Generic;
    using Daxor.Lab.Infrastructure.DomainModels;

    /// <summary>
    /// This class provides the encodings of the enumerations found in the Pharmaceutical class.
    /// </summary>
    /// <remarks>
    /// This class is used by PharmaceuticalKeyServiceHelper.
    /// </remarks>
    public static class PharmaceuticalEnumerationEncodings
    {
        #region __Methods that return lookups (Dictionaries)
        #region GetBrandEncodingLookup()
        /// <summary>
        /// Returns a Dictionary of uint/Pharmaceutical.PharmaceuticalBrand pairs, where the uint 
        /// is the encoding corresponding to the brand enumeration member.
        /// </summary>
        /// <returns>
        /// Dictionary of uint/Pharmaceutical.PharmaceuticalBrand pairs
        /// </returns>
        public static Dictionary<uint, Pharmaceutical.PharmaceuticalBrand> GetBrandEncodingLookup()
        {
            Dictionary<uint, Pharmaceutical.PharmaceuticalBrand> lookup = new Dictionary<uint, Pharmaceutical.PharmaceuticalBrand>();
            lookup.Add(0, Pharmaceutical.PharmaceuticalBrand.Volumex);
            lookup.Add(1, Pharmaceutical.PharmaceuticalBrand.Volumidine);
            return lookup;
        }
        #endregion

        #region GetManufacturerEncodingLookup()
        /// <summary>
        /// Returns a Dictionary of uint/Pharmaceutical.PharmaceuticalManufacturer pairs, where 
        /// the uint is the encoding corresponding to the manufacturer enumeration 
        /// member.
        /// </summary>
        /// <returns>
        /// Dictionary of uint/Pharmaceutical.PharmaceuticalManufacturer pairs
        /// </returns>
        public static Dictionary<uint, Pharmaceutical.PharmaceuticalManufacturer> GetManufacturerEncodingLookup()
        {
            Dictionary<uint, Pharmaceutical.PharmaceuticalManufacturer> lookup = new Dictionary<uint, Pharmaceutical.PharmaceuticalManufacturer>();
            lookup.Add(0, Pharmaceutical.PharmaceuticalManufacturer.Daxor);
            lookup.Add(1, Pharmaceutical.PharmaceuticalManufacturer.IsoTex);
            return lookup;
        }
        #endregion

        #region GetTypeEncodingLookup()
        /// <summary>
        /// Returns a Dictionary of uint/Pharmaceutical.PharmaceuticalType pairs, where the 
        /// uint is the encoding corresponding to the type enumeration member.
        /// </summary>
        /// <returns>
        /// Dictionary of uint/Pharmaceutical.PharmaceuticalType pairs
        /// </returns>
        public static Dictionary<uint, Pharmaceutical.PharmaceuticalType> GetTypeEncodingLookup()
        {
            Dictionary<uint, Pharmaceutical.PharmaceuticalType> lookup = new Dictionary<uint, Pharmaceutical.PharmaceuticalType>();
            lookup.Add(1, Pharmaceutical.PharmaceuticalType.Injectate);
            lookup.Add(0, Pharmaceutical.PharmaceuticalType.Standard);
            return lookup;
        }
        #endregion
        #endregion

        #region __Methods that return encodings given an enum value
        #region GetBrandEncoding()
        /// <summary>
        /// Returns the encoding corresponding to the given Pharmaceutical.PharmaceuticalBrand 
        /// enumeration value
        /// </summary>
        /// <param name="brand">
        /// Member of the Pharmaceutical.PharmaceuticalBrand enumeration
        /// </param>
        /// <returns>
        /// Encoding corresponding to the given value (if it exists); an exception is thrown if the
        /// enumeration value does not have an encoding equivalent
        /// </returns>
        /// <exception cref="ApplicationException">
        /// Given brand does not have an encoding equivalent
        /// </exception>
        public static uint GetBrandEncoding(Pharmaceutical.PharmaceuticalBrand brand)
        {
            Dictionary<uint, Pharmaceutical.PharmaceuticalBrand> lookup = GetBrandEncodingLookup();
            foreach (KeyValuePair<uint, Pharmaceutical.PharmaceuticalBrand> pair in lookup)
            {
                if (pair.Value == brand)
                {
                    return pair.Key;
                }
            }

            throw new ApplicationException("Brand " + brand.ToString() + " does not have " +
                "an equivalent encoding");
        }
        #endregion

        #region GetManufacturerEncoding()
        /// <summary>
        /// Returns the encoding corresponding to the given 
        /// Pharmaceutical.PharmaceuticalManufacturer enumeration value
        /// </summary>
        /// <param name="manufacturer">
        /// Member of the Pharmaceutical.PharmaceuticalManufacturer enumeration
        /// </param>
        /// <returns>
        /// Encoding corresponding to the given value (if it exists); an exception is thrown if the
        /// enumeration value does not have an encoding equivalent
        /// </returns>
        /// <exception cref="ApplicationException">
        /// Given manufacturer does not have an encoding equivalent
        /// </exception>
        public static uint GetManufacturerEncoding(Pharmaceutical.PharmaceuticalManufacturer manufacturer)
        {
            Dictionary<uint, Pharmaceutical.PharmaceuticalManufacturer> lookup = GetManufacturerEncodingLookup();
            foreach (KeyValuePair<uint, Pharmaceutical.PharmaceuticalManufacturer> pair in lookup)
            {
                if (pair.Value == manufacturer)
                {
                    return pair.Key;
                }
            }

            throw new ApplicationException("Manufacturer " + manufacturer.ToString() + 
                " does not have an equivalent encoding");
        }
        #endregion

        #region GetTypeEncoding()
        /// <summary>
        /// Returns the encoding corresponding to the given Pharmaceutical.PharmaceuticalType 
        /// enumeration value
        /// </summary>
        /// <param name="type">
        /// Member of the Pharmaceutical.PharmaceuticalType enumeration
        /// </param>
        /// <returns>
        /// Encoding corresponding to the given value (if it exists); an exception is thrown if the
        /// enumeration value does not have an encoding equivalent
        /// </returns>
        /// <exception cref="ApplicationException">
        /// Given type does not have an encoding equivalent
        /// </exception>
        public static uint GetTypeEncoding(Pharmaceutical.PharmaceuticalType type)
        {
            Dictionary<uint, Pharmaceutical.PharmaceuticalType> lookup = GetTypeEncodingLookup();
            foreach (KeyValuePair<uint, Pharmaceutical.PharmaceuticalType> pair in lookup)
            {
                if (pair.Value == type)
                {
                    return pair.Key;
                }
            }

            throw new ApplicationException("Type " + type.ToString() + " does not have " +
                "an equivalent encoding");
        }
        #endregion
        #endregion

        #region __Methods that return an enum value given an encoding
        #region GetBrand()
        /// <summary>
        /// Gets a Pharmaceutical.PharmaceuticalBrand based on its encoding
        /// </summary>
        /// <param name="encodedBrand">
        /// Encoded brand
        /// </param>
        /// <returns>
        /// Member of the Pharmaceutical.PharmaceuticalBrand enumeration; if the brand cannot be
        /// decoded, returns Pharmaceutical.PharmaceuticalBrand.Undefined
        /// </returns>
        public static Pharmaceutical.PharmaceuticalBrand GetBrand(uint encodedBrand)
        {
            Dictionary<uint, Pharmaceutical.PharmaceuticalBrand> brands = GetBrandEncodingLookup();
            Pharmaceutical.PharmaceuticalBrand targetBrand;
            if (brands.TryGetValue(encodedBrand, out targetBrand))
            {
                return targetBrand;
            }

            return Pharmaceutical.PharmaceuticalBrand.Undefined;
        }
        #endregion

        #region GetManufacturer()
        /// <summary>
        /// Gets a Pharmaceutical.PharmaceuticalManufacturer based on its encoding
        /// </summary>
        /// <param name="encodedManufacturer">
        /// Encoded manufacturer
        /// </param>
        /// <returns>
        /// Member of the Pharmaceutical.PharmaceuticalManufacturer enumeration; if the 
        /// manufacturer cannot be decoded, returns 
        /// Pharmaceutical.PharmaceuticalManufacturer.Undefined
        /// </returns>
        public static Pharmaceutical.PharmaceuticalManufacturer GetManufacturer(uint encodedManufacturer)
        {
            Dictionary<uint, Pharmaceutical.PharmaceuticalManufacturer> manufacturers = GetManufacturerEncodingLookup();
            Pharmaceutical.PharmaceuticalManufacturer targetManufacturer;
            if (manufacturers.TryGetValue(encodedManufacturer, out targetManufacturer))
            {
                return targetManufacturer;
            }

            return Pharmaceutical.PharmaceuticalManufacturer.Undefined;
        }
        #endregion

        #region GetType()
        /// <summary>
        /// Gets a Pharmaceutical.PharmaceuticalType based on its encoding
        /// </summary>
        /// <param name="encodedType">
        /// Encoded type
        /// </param>
        /// <returns>
        /// Member of the Pharmaceutical.PharmaceuticalType enumeration; if the type cannot be
        /// decoded, returns Pharmaceutical.PharmaceuticalType.Undefined
        /// </returns>
        public static Pharmaceutical.PharmaceuticalType GetType(uint encodedType)
        {
            Dictionary<uint, Pharmaceutical.PharmaceuticalType> types = GetTypeEncodingLookup();
            Pharmaceutical.PharmaceuticalType targetType;
            if (types.TryGetValue(encodedType, out targetType))
            {
                return targetType;
            }

            return Pharmaceutical.PharmaceuticalType.Undefined;
        }
        #endregion
        #endregion
    }
}