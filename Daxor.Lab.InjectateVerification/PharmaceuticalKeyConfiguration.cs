﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.InjectateVerification
{
    /// <summary>
    /// Represents a configuration for a Pharmaceutical, where the batch ID and date of manufacture 
    /// can start with a given number or given date respectively, and a key can have a specified
    /// number of bits reserved for the checksum.
    /// </summary>
    public class PharmaceuticalKeyConfiguration
    {
        #region Constants
        
        public const uint BatchIDsSupported = 10000;

        public const uint SerializationIDsSupported = 10000;

        public const uint BitsInContainer = sizeof(ulong) * 8;

        public const uint YearsSupported = 8;

        public const uint DaysSupported = 366 * YearsSupported;

        public const string FormattedToString =
            "[startingbatchid=\"{0}\"; startingdateofmanufacture=\"{1}\"; numchecksumbits=\"{2}\"]";
        
        #endregion

        #region Private member variables

        private uint _numberOfChecksumBits;
        private const string UndefinedEnumName = "Undefined";

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the PharmaceuticalKeyConfiguration class with a given 
        /// starting date of manufacture, starting batch ID, number of checksum bits, and scanner
        /// delimiter
        /// </summary>
        /// <param name="startingDateOfManufacture">
        /// Date from which the date of manufacture will be indexed
        /// </param>
        /// <param name="startingBatchId">
        /// Value from which the batch ID will be indexed
        /// </param>
        /// <param name="numChecksumBits">
        /// Number of bits occupied by the checksum
        /// </param>
        /// <seealso cref="Pharmaceutical"/>
        /// <exception cref="InvalidOperationException">
        /// Given number of checksum bits exceeds what is allowed by the container
        /// </exception>
        public PharmaceuticalKeyConfiguration(
            DateTime startingDateOfManufacture,
            uint startingBatchId,
            uint numChecksumBits)
        {
            StartingDateOfManufacture = startingDateOfManufacture;
            StartingBatchId = startingBatchId;
            NumberOfChecksumBits = numChecksumBits;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the number of bits reserved for the checksum
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Given number of checksum bits is negative
        /// </exception>
        /// <exception cref="InvalidOperationException">
        /// Given number of checksum bits exceeds what is allowed by the container
        /// </exception>
        public uint NumberOfChecksumBits
        {
            get
            {
                return _numberOfChecksumBits;
            }

            set
            {
                var numberOfBitsRemaining = BitsInContainer - GetMinimumNumberOfBitsRequired();
                if (value > numberOfBitsRemaining)
                {
                    throw new InvalidOperationException("Only " + numberOfBitsRemaining + " bit(s) can be used " +
                        "for a checksum, therefore the requested amount (" + value + ") is too high");
                }
                _numberOfChecksumBits = value;
            }
        }

        /// <summary>
        /// Gets or sets the starting batch ID
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Given starting batch ID is less than zero
        /// </exception>
        public uint StartingBatchId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the starting date of manufacture
        /// </summary>
        public DateTime StartingDateOfManufacture
        {
            get;
            set;
        }

        #endregion

        #region Static methods
        
        /// <summary>
        /// Computes the minimum number of bits required to represent the required fields for a
        /// Pharmaceutical (i.e., date of manufacture, batch ID, serialization ID, type, 
        /// manufacturer, and brand).
        /// </summary>
        /// <returns>
        /// Minimum number of bits required
        /// </returns>
        public static uint GetMinimumNumberOfBitsRequired()
        {
            var bitsForDateOfManufacture = ComputeNumberOfBitsToRepresent(DaysSupported);
            var bitsForBatchId = ComputeNumberOfBitsToRepresent(BatchIDsSupported);
            var bitsForSerializationId = ComputeNumberOfBitsToRepresent(SerializationIDsSupported);
            var bitsForType = ComputeNumberOfBitsToRepresent((uint) GetNumberOfTypesSupported());
            var bitsForManufacturer =ComputeNumberOfBitsToRepresent((uint) GetNumberOfManufacturersSupported());
            var bitsForBrand = ComputeNumberOfBitsToRepresent((uint) GetNumberOfBrandsSupported());

            return bitsForDateOfManufacture + bitsForBatchId + bitsForSerializationId +
                bitsForType + bitsForManufacturer + bitsForBrand;
        }

        /// <summary>
        /// Computes the minimum number of bits to represent the given value
        /// </summary>
        /// <param name="value"></param>
        /// <returns>Minimum number of bits required to represent</returns>
        public static uint ComputeNumberOfBitsToRepresent(uint value)
        {
            return Convert.ToUInt32(Math.Ceiling(Math.Log(value, 2)));
        }
        
        /// <summary>
        /// Computes the highest serialization ID supported by the pharmaceutical encoding
        /// </summary>
        /// <returns>
        /// Maximum serialization ID supported
        /// </returns>
        public static int GetMaxSerializationIdSupported()
        {
            return (int)SerializationIDsSupported - 1;
        }
        
        /// <summary>
        /// Returns the number of enumeration members excluding the member that
        /// represents an undefined state
        /// </summary>
        /// <param name="enumeration">Enumeration to evaluate</param>
        /// <returns>Number of defined enumeration members; zero if the enumeration
        /// has no memebers</returns>
        /// <exception cref="ArgumentNullException">If the given type is null</exception>
        /// <exception cref="ArgumentException">If the given type is not an enumeration</exception>
        public static int GetNumberOfDefinedEnumerations(Type enumeration)
        {
            if (enumeration == null)
                throw new ArgumentNullException("enumeration");
            var enumerationMemberNames = Enum.GetNames(enumeration);
            var definedMembers = from b in enumerationMemberNames where b != UndefinedEnumName select b;

            return definedMembers.Count();
        }

        /// <summary>
        /// Computes the number of brands supported by the pharmaceutical encoding and lot number
        /// schemes. This value is based on the number of items in 
        /// Pharmaceutical.PharmaceuticalBrand, excluding the "undefined" member of that 
        /// enumeration.
        /// </summary>
        /// <returns>
        /// Number of brands supported
        /// </returns>
        public static int GetNumberOfBrandsSupported()
        {
            return GetNumberOfDefinedEnumerations(typeof (Pharmaceutical.PharmaceuticalBrand));
        }
        
        /// <summary>
        /// Computes the number of manufacturers supported by the pharmaceutical encoding and lot 
        /// number schemes. This value is based on the number of items in 
        /// Pharmaceutical.PharmaceuticalManufacturer, excluding the "undefined" member of that 
        /// enumeration.
        /// </summary>
        /// <returns>
        /// Number of manufacturers supported
        /// </returns>
        public static int GetNumberOfManufacturersSupported()
        {
            return GetNumberOfDefinedEnumerations(typeof(Pharmaceutical.PharmaceuticalManufacturer));
        }
        
        /// <summary>
        /// Computes the number of types supported by the pharmaceutical encoding and lot number 
        /// schemes. This value is based on the number of items in 
        /// Pharmaceutical.PharmaceuticalType, excluding the "undefined" member of that 
        /// enumeration.
        /// </summary>
        /// <returns>
        /// Number of types supported
        /// </returns>
        public static int GetNumberOfTypesSupported()
        {
            return GetNumberOfDefinedEnumerations(typeof (Pharmaceutical.PharmaceuticalType));
        }
        
        #endregion

        #region Instance methods
        
        /// <summary>
        /// Computes the ending date of manufacture based on the starting date and the number of
        /// days supported by the current key encoding scheme
        /// </summary>
        /// <returns>
        /// Ending date of manufacture
        /// </returns>
        public DateTime GetEndingDateOfManufacture()
        {
            return StartingDateOfManufacture.AddDays(DaysSupported);
        }
        
        /// <summary>
        /// Computes the highest batch ID currently supported
        /// </summary>
        /// <returns>
        /// Number of batches supported
        /// </returns>
        public uint GetMaxBatchIdSupported()
        {
            return StartingBatchId + BatchIDsSupported - 1;
        }
        
        /// <summary>
        /// Computes the number of bits used for storing the required components of the key and the
        /// checksum
        /// </summary>
        /// <returns>
        /// Number of bits in the entire key
        /// </returns>
        public uint GetNumberOfBitsUsedForKey()
        {
            return GetMinimumNumberOfBitsRequired() + NumberOfChecksumBits;
        }

        /// <summary>
        /// Computes the number of bits used for storing the batch ID
        /// </summary>
        /// <returns>
        /// Number of bits needed to store the batch ID
        /// </returns>
        public uint GetNumberOfBitsForBatchId()
        {
            return ComputeNumberOfBitsToRepresent(BatchIDsSupported);
        }

        /// <summary>
        /// Computes the number of bits used for storing the brand
        /// </summary>
        /// <returns>
        /// Number of bits needed to store the brand
        /// </returns>
        public uint GetNumberOfBitsForBrand()
        {
            return ComputeNumberOfBitsToRepresent((uint)GetNumberOfBrandsSupported());
        }

        /// <summary>
        /// Computes the number of bits used for storing the date of manufacturer
        /// </summary>
        /// <returns>
        /// Number of bits needed to store the date of manufacture
        /// </returns>
        public uint GetNumberOfBitsForDateOfManufacture()
        {
            return ComputeNumberOfBitsToRepresent(DaysSupported);
        }

        /// <summary>
        /// Computes the number of bits used for storing the manufacturer
        /// </summary>
        /// <returns>
        /// Number of bits needed to store the manufacturer
        /// </returns>
        public uint GetNumberOfBitsForManufacturer()
        {
            return ComputeNumberOfBitsToRepresent((uint)GetNumberOfManufacturersSupported());
        }

        /// <summary>
        /// Computes the number of bits used for storing the serialization ID
        /// </summary>
        /// <returns>
        /// Number of bits needed to store the serialization ID
        /// </returns>
        public uint GetNumberOfBitsForSerializationId()
        {
            return ComputeNumberOfBitsToRepresent(SerializationIDsSupported);
        }
        
        /// <summary>
        /// Gets the number of bits used for storing the type
        /// </summary>
        /// <returns>
        /// Number of bits needed to store the type
        /// </returns>
        public uint GetNumberOfBitsForType()
        {
            return ComputeNumberOfBitsToRepresent((uint)GetNumberOfTypesSupported());
        }
        
        #endregion

        /// <summary>
        /// Overloaded. Converts the value of this instance to String.
        /// </summary>
        /// <returns>
        /// A string containing values for the starting batch ID, starting date of manufacture,
        /// and number of bits reserved for the checksum
        /// </returns>
        public override string ToString()
        {
            return String.Format(FormattedToString,
                                 StartingBatchId, StartingDateOfManufacture.ToString(CultureInfo.InvariantCulture),
                                 NumberOfChecksumBits);
        }

    }
}
