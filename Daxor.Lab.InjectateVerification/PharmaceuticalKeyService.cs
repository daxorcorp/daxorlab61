﻿//-----------------------------------------------------------------------
// <copyright file="PharmaceuticalKeyService.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Daxor.Lab.InjectateVerification
{
    using System;
    using Daxor.Lab.Infrastructure.DomainModels;
    using Daxor.Lab.Infrastructure.Interfaces;

    /// <summary>
    /// Class that is capable of encoding and decoding Pharmaceutical values to and from
    /// string-based representations.
    /// </summary>
    public class PharmaceuticalKeyService : IPharmaceuticalKeyDecoder
    {
        #region __Private member variables
        /// <summary>
        /// Private member variable representing the key codec configuration
        /// </summary>
        private readonly PharmaceuticalKeyCodecConfiguration _configuration;

        /// <summary>
        /// Private member variable representing the key codec helper
        /// </summary>
        private readonly PharmaceuticalKeyCodecHelper _helper;
        #endregion

        #region __Constructor
        /// <summary>
        /// Initializes a new instance of the PharmaceuticalKeyService class with a given key codec
        /// configuration
        /// </summary>
        /// <param name="configuration">
        /// PharmaceuticalKeyCodecConfiguration instance used to decode/encode a Pharmaceutical
        /// </param>
        public PharmaceuticalKeyService(PharmaceuticalKeyCodecConfiguration configuration)
        {
            _configuration = configuration;
            _helper = new PharmaceuticalKeyCodecHelper(Configuration);
        }
        #endregion

        #region __Properties
        #region Configuration
        /// <summary>
        /// Gets the current PharmaceuticalKeyCodecConfiguration instance
        /// </summary>
        private PharmaceuticalKeyCodecConfiguration Configuration
        {
            get { return _configuration; }
        }
        #endregion

        #region Helper
        /// <summary>
        /// Gets the current PharmaceuticalKeyCodecHelper instance
        /// </summary>
        private PharmaceuticalKeyCodecHelper Helper
        {
            get { return _helper; }
        }
        #endregion
        #endregion

        #region Decode()
        /// <summary>
        /// Decodes a given pharmaceutical key string
        /// </summary>
        /// <param name="pharmaceuticalKey">
        /// Pharmaceutical key to decode
        /// </param>
        /// <returns>
        /// Pharmaceutical instance whose fields are set to the values present in the 
        /// pharmaceutical key string
        /// </returns>
        /// <exception cref="ApplicationException">
        /// Decoded pharmaceutical could not be re-encoded to verify the checksum
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Decoded pharmaceutical type is not known
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Decoded pharmaceutical manufacturer is not known
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Decoded pharmaceutical brand is not known
        /// </exception>
        /// <exception cref="ArgumentException">
        /// Checksum of given pharmaceutical key does not have a correct checksum
        /// </exception>
        /// <exception cref="ArgumentNullException">
        /// PharmaceuticalKeyCodecConfiguration instance is null
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Decoded batch ID is not valid for a Pharmaceutical
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Decoded serialization ID is not valid for a Pharmaceutical
        /// </exception>
        public Pharmaceutical Decode(string pharmaceuticalKey)
        {
            // We can't proceed without valid references
            PharmaceuticalKeyCodecHelper.EnsureNonNullKeyCodecConfiguration(Configuration);

            // Instantiate the helper class
            ulong pharmValue = Configuration.Representation.ConvertFromRepresentation(pharmaceuticalKey);
            Pharmaceutical decodedPharmaceutical = Helper.ConvertValueToPharmaceutical(pharmValue);

            // Encode the constructed pharmaceutical and see if you get the original one back
            string testPharmaceuticalKey = Encode(decodedPharmaceutical);
            if (pharmaceuticalKey != testPharmaceuticalKey)
            {
                throw new ArgumentException(
                    "pharmaceuticalKey", 
                    "The pharmaceutical checksum is not correct");
            }

            return decodedPharmaceutical;
        }
        #endregion

        #region Encode()
        /// <summary>
        /// Encodes a given pharmaceutical
        /// </summary>
        /// <param name="pharmaceutical">
        /// Pharmaceutical instance to encode
        /// </param>
        /// <returns>
        /// The given pharmaceutical represented in the desired key representation (string). This 
        /// value is also left-padded to ensure that it has a minimum key length (which is 
        /// specified in the key codec configuration).
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// Pharmaceutical instance is null
        /// </exception>
        /// <exception cref="ApplicationException">
        /// Pharmaceutical could not be converted to a numeric equivalent
        /// </exception>
        private string Encode(Pharmaceutical pharmaceutical)
        {
            // We can't proceed without valid references
            PharmaceuticalKeyCodecHelper.EnsureNonNullPharmaceutical(pharmaceutical);
            PharmaceuticalKeyCodecHelper.EnsureNonNullKeyCodecConfiguration(Configuration);

            // Convert the pharmaceutical to a value, encode that value, then return the left-padded
            // result.
            ulong pharmaceuticalValue = Helper.ConvertPharmaceuticalToValue(pharmaceutical);
            string pharmaceuticalKey = Configuration.Representation.ConvertToRepresentation(pharmaceuticalValue);
            return Helper.PadLeft(pharmaceuticalKey);
        }
        #endregion
    }
}
