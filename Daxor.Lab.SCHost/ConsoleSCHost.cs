﻿using System;
using System.ServiceModel;
using Daxor.Lab.SCService;
using Daxor.Lab.SCContracts;

namespace Daxor.Lab.SCHost
{
    /// <summary>
    /// SampleChanger console service host. 
    /// ServiceHost reads config settings such as address, binding and service contract from App.config file.
    /// </summary>
    internal class ConsoleSCHost
    {
        static void Main(string[] args)
        {
            Console.Title = "SampleChangerService Host";
            Console.ForegroundColor = ConsoleColor.Blue;
            try
            {
                ISampleChangerService scService = new SampleChangerService();
                using (ServiceHost host = new ServiceHost(scService))
                {
                    host.Opened += host_Opened;
                    host.Closing += host_Closing;
                    host.Closed += host_Closed;
                    host.Faulted += host_Faulted;
                  
                    host.Open();
                    
                    Console.WriteLine("SampleChanger Service is up and running " + DateTime.Now.ToLongTimeString());
                    Console.WriteLine("Press Any Key to terminate the service.");
                    Console.ReadLine();

                    if (scService != null)
                        scService.Terminate();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception. Message " + ex.Message);
                Console.WriteLine("Press Any Key to continue...");
                Console.ReadKey();

            }

        }

        static void host_Opened(object sender, EventArgs e)
        {
            Console.WriteLine("SampleChanger service channel has opened.");
        }
        static void host_Faulted(object sender, EventArgs e)
        {
            System.Console.WriteLine("SampleChanger service channel has faulted.");
        }
        static void host_Closed(object sender, EventArgs e)
        {
            System.Console.WriteLine("SampleChanger service channel has closed.");
        }
        static void host_Closing(object sender, EventArgs e)
        {
            System.Console.WriteLine("SampleChanger service channel is closing.");
        }
    }
}
