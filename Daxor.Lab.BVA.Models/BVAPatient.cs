﻿using System;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.Infrastructure.Utilities;

namespace Daxor.Lab.BVA.Models
{
    public sealed class BVAPatient : PatientBase
    {
        MeasurementSystem _mSystem = MeasurementSystem.Metric;
        string _pacNumber;
        private double _heightInCm = -1f;
        private double _weightInKg = -1f;

        #region Ctor

        private BVAPatient(Guid patientID)
            : base(patientID)
        {
            HeightInCm = 0f;
            WeightInKg = 0f;
        }

        #endregion

        #region Static Creators

        public static BVAPatient CreateDefaultModel()
        {
            return new BVAPatient(Guid.NewGuid());
        }
        public static BVAPatient PatientRecordToModel(PatientRecord patient)
        {
            BVAPatient pModel = new BVAPatient(patient.PID);

            pModel.FirstName = patient.FirstName;
            pModel.LastName = patient.LastName;
            pModel.MiddleName = patient.MiddleName;
            pModel.Gender = (GenderType)patient.Gender;
            pModel.HospitalPID = patient.PatientHospitalId;
            pModel.DateOfBirth = patient.DOB;

            return pModel;
        }

        #endregion

        #region Properties

        public string PacNumber
        {
            get { return _pacNumber; }
            set { base.SetValue<string>(ref _pacNumber, "PacNumber", value); }
        }

        public MeasurementSystem MeasurementSystem
        {
            get { return _mSystem; }
            set
            {
                if (base.SetValue<MeasurementSystem>(ref _mSystem, "MeasurementSystem", value))
                    FirePropertyChanged("HeightInCurrentMeasurementSystem", "WeightInCurrentMeasurementSystem");
            }
        }
        public double DeviationFromIdealWeight { get; set; }
        
        public double HeightInCm
        {
            get { return _heightInCm; }
            set
            {
                if (base.SetValue<double>(ref _heightInCm, "HeightInCm", value))
                    FirePropertyChanged("HeightInCurrentMeasurementSystem");
            }
        }
        public double WeightInKg
        {
            get { return _weightInKg; }
            set
            {
                if (base.SetValue<double>(ref _weightInKg, "WeightInKg", value))
                    FirePropertyChanged("WeightInCurrentMeasurementSystem");
            }
        }
        public double HeightInInches
        {
            get
            {
                return UnitsConversionHelper.ConvertHeightToEnglish(_heightInCm);
            }
        }
        public double WeightInLb
        {
            get
            {
                return UnitsConversionHelper.ConvertWeightToEnglish(_weightInKg);
            }
        }
        public double HeightInCurrentMeasurementSystem
        {
            get
            {
                if (MeasurementSystem == Infrastructure.DomainModels.MeasurementSystem.English)
                    return HeightInInches;

                return HeightInCm;
            }
        }
        public double WeightInCurrentMeasurementSystem
        {
            get
            {
                if (MeasurementSystem == Infrastructure.DomainModels.MeasurementSystem.English)
                    return WeightInLb;

                return WeightInKg;
            }
        }

        #endregion
    }
}