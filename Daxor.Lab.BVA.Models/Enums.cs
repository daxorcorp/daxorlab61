﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.BVA.Models
{
    /// <summary>
    /// Types of TrainingTests
    /// </summary>
    public enum TestMode
    {
        Automatic = 0,
        Manual = 1,
        Training = 2,   // These enums need to match the values in tlkpTEST_MODE if 
        VOPS = 3,       // we are going to just use the ToString() method to pass them around
        TrainingHighStd = 4,
        TrainingReverseSlope = 5,
        TrainingHighSlopeTest = 6,
    }

    /// <summary>
    /// Volume type enum. Used to distinguish between different volume types, such as
    /// </summary>
    public enum VolumeType
    {
        WholeBlood = 0,
        Plasma = 1,
        RedCell = 2,
        Unknown = -1,
    }
  
    /// <summary>
    /// BloodType measured or ideal
    /// </summary>
    public enum BloodType
    {
        Measured = 0,
        Ideal = 1,
        Unknown = -1,
    }


    /// <summary>
    /// Hematocrit fill type
    /// </summary>
    public enum HematocritFillType
    {
        OnePerTest = 2,
        OnePerSample = 1,
        TwoPerSample = 0,
        Unknown = -1,
    }

    /// <summary>
    /// Sample types for BV Sample
    /// </summary>
    public enum SampleType
    {
        Background = 0,
        Standard = 1,
        Baseline = 2,
        Sample1 = 3,
        Sample2 = 4,
        Sample3 = 5,
        Sample4 = 6,
        Sample5 = 7,
        Sample6 = 8,

        Undefined = 9,
    }
    public enum SampleRange
    {
        A,
        B,
        Undefined
    }

    public enum SampleState
    {
        Initialized,
        AcquiringCounts,
        AcquiredCounts,
    }
}
