﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.BVA.Models
{
    /// <summary>
    /// Maps Position to Type/Range and vice versa
    /// </summary>
    public class SampleLayoutSchema : ISampleLayoutSchema
    {
        #region Fields

        protected readonly List<SampleLayoutItem> _layoutItems = new List<SampleLayoutItem>();
        protected readonly int _id;

        #endregion

        public SampleLayoutSchema(int id)
        {
            SchemaID = id;
        }

        public int GetPosition(SampleType type, SampleRange range)
        {
            int foundPosition = -1;
            if (_layoutItems == null)
                return foundPosition;

            var sPosition = (from p in _layoutItems
                             where p.Type == type && p.Range == range
                             select p).FirstOrDefault();
            if (sPosition != null)
                foundPosition = sPosition.Position;

            return foundPosition;
        }
        public SampleLayoutItem GetLayoutItem(int position)
        {
            var sPosition = (from p in _layoutItems
                             where p.Position == position 
                             select p).FirstOrDefault();
            return sPosition;
        }

        public IEnumerable<SampleLayoutItem> SampleLayoutItems
        {
            get { return _layoutItems; }
        }
        public int SchemaID
        {
            get;
            protected set;
        }

        public virtual bool TryAddLayoutItem(SampleLayoutItem item)
        {
            if (_layoutItems.Any(i => i.Position == item.Position))
                return false;
            if (item.Range == SampleRange.Undefined && item.Type == SampleType.Undefined)
                return false;

            _layoutItems.Add(item);
            return true;
        }
    }

    /// <summary>
    /// Holds position number, type associated with position and range associated with position
    /// </summary>
    public class SampleLayoutItem
    {
        private int _position = Int32.MinValue;
        private SampleType _type = SampleType.Undefined;
        private SampleRange _range = SampleRange.Undefined;

        public int Position
        {
            get { return _position; }
            set { _position = value; }
        }
        public SampleType Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public SampleRange Range
        {
            get { return _range; }
            set { _range = value; }
        }
    }
}
