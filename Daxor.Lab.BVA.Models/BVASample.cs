﻿using System;
using System.Data.Linq;
using Daxor.Lab.DataObjects.DataRecord;
using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.BVA.Models
{
    public sealed class BVASample : SampleBase
    {
        #region Fields

        int _piTimeSeconds;                             //Post Injection Time in seconds
        string _fullName;                               //Pat-1A, Pat-1B, Std-B, Std-A and Ctl-A

        bool _isCountExcluded;                          //True if counts are excluded; false otherwise
        bool _isHematocritExcluded;                     //True is Hematocrit is excluded; false otherwise
        bool _isExcluded;                               //True if the whole sample is excluded, false otherwise

        double _hematocrit = 0;                        //Hematocrit for a given sample
        double _fwhm;                                   //Full Width at Half Max
        double _fwtm;                                   //Full Width at Tenth Max
        double _centroid;                               //Centroid
        int _counts = 0;                                //Integral (sum of counts) in ROI

        SampleType _sType = SampleType.Undefined;       //Sample type enum
        SampleRange _sRange = SampleRange.Undefined;    //Sample range (A, B)

        #endregion

        #region Ctor

        public BVASample(Guid id, int position, int roiLowBound, int roiHighBound)
            : base(id, position)
        {
            this.ROILowBound = roiLowBound;
            this.ROIHighBound = roiHighBound;

            this.Counts = -1;
            this.Hematocrit = -1;
            this.PostInjectionTimeInSeconds = -1;
        }

        #endregion

        #region Static

        public static BVASample SampleRecordToModel(BvaSampleRecord sample)
        {
            BVASample sampleModel = new BVASample(sample.SampleId, sample.PositionRecord.Position, sample.RoiLowBound, sample.RoiHighBound);
            sampleModel.IsCountExcluded = sample.IsCountExcluded;
            sampleModel.IsHematocritExcluded = sample.IsHematocritExcluded;
            sampleModel.IsExcluded = sample.IsSampleExcluded;

            sampleModel.Centroid = sample.Centroid;
            sampleModel.Counts = sample.Counts;
            sampleModel.FullWidthHalfMax = sample.FWHM;
            sampleModel.FullWidthTenthMax = sample.FWTM;
            sampleModel.Hematocrit = sample.Hematocrit;

            sampleModel.LiveTime = sample.LiveTime;
            sampleModel.PostInjectionTimeInSeconds = sample.TimePostInjection;
            sampleModel.PresetLiveTime = (int)sample.PresetLiveTime; // Why is it a double in DTO and database
            sampleModel.RealTime = sample.RealTime;
            sampleModel.FullName = sample.PositionRecord.ShortFriendlyName;
            sampleModel.DisplayName = sample.PositionRecord.ShortFriendlyName;

            sampleModel.SampleHeaderTimestamp = sample.SampleHeaderTimestamp;
            sampleModel.BvaSampleTimestamp = sample.BvaSampleTimestamp;
            sampleModel.SpectrumArray = sample.Spectrum;

            return sampleModel;
        }
        public static BvaSampleRecord ModelToSampleRecord(BVASample samp)
        {
            BvaSampleRecord sampDTO = new BvaSampleRecord();
            sampDTO.SampleId = samp.SampleID;
            //sampDTO.TestId = samp.TestId;
            sampDTO.LiveTime = samp.LiveTime;
            sampDTO.RealTime = samp.RealTime;
            sampDTO.Centroid = samp.Centroid;
            sampDTO.Counts = (int)samp.Counts;
            sampDTO.FWHM = samp.FullWidthHalfMax;
            sampDTO.FWTM = samp.FullWidthTenthMax;
            sampDTO.Hematocrit = samp.Hematocrit;

            sampDTO.IsCountExcluded = samp.IsCountExcluded;
            sampDTO.IsHematocritExcluded = samp.IsHematocritExcluded;
            sampDTO.IsSampleExcluded = samp.IsExcluded;
            sampDTO.PositionRecord.Position = samp.Position;
            sampDTO.TimePostInjection = samp.PostInjectionTimeInSeconds;
            sampDTO.PresetLiveTime = samp.PresetLiveTime;
            sampDTO.RoiHighBound = samp.ROIHighBound;
            sampDTO.RoiLowBound = samp.ROILowBound;

            sampDTO.SampleHeaderTimestamp = samp.SampleHeaderTimestamp;
            sampDTO.BvaSampleTimestamp = samp.BvaSampleTimestamp;

            if (samp.SpectrumArray != null)
            {
                sampDTO.Spectrum = samp.SpectrumArray;
            }

            return sampDTO;
        }

        #endregion

        #region Properties

        public Guid TestId { get; set; }

        public string FullName
        {
            get { return _fullName; }
            set { base.SetValue<string>(ref _fullName, "FullName", value); }
        }
        public SampleType Type
        {
            get { return _sType; }
            set { base.SetValue<SampleType>(ref _sType, "Type", value); }
        }
        public SampleRange Range
        {
            get { return _sRange; }
            set { base.SetValue<SampleRange>(ref _sRange, "Range", value); }
        }
        public bool IsCountExcluded
        {
            get { return _isCountExcluded; }
            set { base.SetValue<bool>(ref _isCountExcluded, "IsCountExcluded", value); }
        }
        public bool IsHematocritExcluded
        {
            get { return _isHematocritExcluded; }
            set { base.SetValue<bool>(ref _isHematocritExcluded, "IsHematocritExcluded", value); }
        }
        public bool IsExcluded
        {
            get { return _isExcluded; }
            set { base.SetValue<bool>(ref _isExcluded, "IsExcluded", value); }
        }
        public double Hematocrit
        {
            get { return _hematocrit; }
            set { base.SetValue<double>(ref _hematocrit, "Hematocrit", value); }
        }

        public double FullWidthHalfMax
        {
            get { return _fwhm; }
            set { base.SetValue<double>(ref _fwhm, "FullWidthHalfMax", value); }
        }
        public double FullWidthTenthMax
        {
            get { return _fwtm; }
            set { base.SetValue<double>(ref _fwtm, "FullWidthTenthMax", value); }
        }
        public double Centroid
        {
            get { return _centroid; }
            set { base.SetValue<double>(ref _centroid, "Centroid", value); }
        }
        public int Counts
        {
            get { return _counts; }
            set { base.SetValue<int>(ref _counts, "Counts", value); }
        }

        public int ROIHighBound
        {
            get;
            set;
        }
        public int ROILowBound
        {
            get;
            set;
        }

        //public int ROIHighBoundChannel
        //{
        //    get;
        //    set;
        //}
        //public int ROILowBoundChannel
        //{
        //    get;
        //    set;
        //}
     
        public int PostInjectionTimeInSeconds
        {
            get { return _piTimeSeconds; }
            set { base.SetValue<int>(ref _piTimeSeconds, "PostInjectionTimeInSeconds", value); }
        }

        public Binary SampleHeaderTimestamp { get; set; }
        public Binary BvaSampleTimestamp { get; set; }

        #endregion
    }
}
