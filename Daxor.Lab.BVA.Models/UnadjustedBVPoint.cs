﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.Base;

namespace Daxor.Lab.BVA.Models
{
    public class UnadjustedBVPoint : ValidatableObject
    {
        public event EventHandler UnadjustedBloodVolumeShouldChange;

        public BVACompositeSample BloodVolumeSample { get; set; }
        public int UnadjustedBloodVolume { get; set; }

        public UnadjustedBVPoint(BVACompositeSample bvSample)
        {
            this.BloodVolumeSample = bvSample;
        }

        protected virtual void OnUnadjustedBloodVolumeShouldChanged()
        {
            var handler = UnadjustedBloodVolumeShouldChange;
            if (handler != null)
                handler(this, new EventArgs());
        }
    }
}
