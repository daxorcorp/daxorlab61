﻿using Daxor.Lab.Infrastructure.Base;

namespace Daxor.Lab.BVA.Models
{
    /// <summary>
    /// Blood Volume
    /// </summary>
    public sealed class BVAVolume : ValidatableObject
    {
        #region Fields
       
        int _redCellCount;
        int _plasmaCount;
       
        #endregion

        #region Properties

        public BloodType Type
        {
            set;
            get;
        }
        public int RedCellCount
        {
            get { return _redCellCount; }
            set
            {
                if (base.SetValue(ref _redCellCount, "RedCellCount", value))
                    FirePropertyChanged("WholeCount");
            }
        }
        public int PlasmaCount
        {
            get { return _plasmaCount; }
            set
            {
                 if (base.SetValue(ref _plasmaCount, "PlasmaCount", value))
                    FirePropertyChanged("WholeCount");
            }
        }
        public int WholeCount
        {
            get { return _redCellCount + _plasmaCount; }
        }

        #endregion
    }
}
