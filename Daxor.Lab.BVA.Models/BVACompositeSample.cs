﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.Infrastructure.Base;

namespace Daxor.Lab.BVA.Models
{
    /// <summary>
    /// Sample that is composed of multiple(two) samples
    /// (i.e. Standard - is composed of StandardA and StandardB
    /// (i.e. Sample1 - is composed of Sample1A and Sample1B)
    /// </summary>
    public sealed class BVACompositeSample : ValidatableObject
    {
        #region Fields

        const int VALUE_DOES_NOT_EXIST = 0;

        private readonly BVATest _test;
        private BVASample _sampleA, _sampleB;
        
        double _unadjustedBloodVolume = VALUE_DOES_NOT_EXIST;
        static List<String> _propertiesAffectUnadjustedBloodVolume = new List<string>() { "AverageHematocrit", "PostInjectionTimeInSeconds", "IsWholeSampleExcluded", "AverageCounts" };
            
        #endregion

        #region Ctor

        public BVACompositeSample(BVATest test, BVASample sampleA, BVASample sampleB) : this(test, new[] { sampleA, sampleB }) { }
        public BVACompositeSample(BVATest test, IEnumerable<BVASample> samples)
        {
            if(samples == null)
                throw new ArgumentNullException("samples cannot be null");
            if (samples.Count() == 0)
                throw new ArgumentOutOfRangeException("samples list cannot be empty");

             _test = test;
            var sampleA = (from s in samples where s.Range == SampleRange.A select s).FirstOrDefault();
            var sampleB = (from s in samples where s.Range == SampleRange.B select s).FirstOrDefault();

            BVACompositeSample.VerifySampleTypeAndRangeMatch(sampleA, sampleB);

            this.SampleA = sampleA;
            this.SampleB = sampleB;
        }

        #endregion

        #region Properties

        public BVATest Test
        {
            get { return _test; }
        }
        public BVASample SampleA
        {
            get { return _sampleA; }
            set
            {
                if (_sampleA == value || value == null)
                    return;

                if (_sampleA != null) _sampleA.PropertyChanged -= Sample_PropertyChanged;

                _sampleA = value;

                if (_sampleA != null) _sampleA.PropertyChanged += Sample_PropertyChanged;

                FirePropertyChanged("SampleA");
            }
        }
        public BVASample SampleB
        {
            get { return _sampleB; }
            set
            {
                if (_sampleB == value || value == null)
                    return;

                if (_sampleB != null) _sampleB.PropertyChanged -= Sample_PropertyChanged;

                _sampleB = value;

                if (_sampleB != null) _sampleB.PropertyChanged += Sample_PropertyChanged;


                FirePropertyChanged("SampleB");
            }
        }

        public double AverageHematocrit
        {
            get
            {
                int goodSamples = 0;
                double totalHematocrit = 0;

                if (_sampleA != null && !_sampleA.IsHematocritExcluded && _sampleA.Hematocrit > 0)
                {
                    goodSamples++;
                    totalHematocrit += _sampleA.Hematocrit;
                }

                if (_sampleB != null && !_sampleB.IsHematocritExcluded && _sampleB.Hematocrit > 0)
                {
                    goodSamples++;
                    totalHematocrit += _sampleB.Hematocrit;
                }

                if (goodSamples == 0)
                    return -1;

                return totalHematocrit / goodSamples;  // Do not round.  Rounding causes the results for unadjusted blood
                                                       // volumes to be different from WinBVA 5.4

            }
        }

        public int PostInjectionTimeInSeconds
        {
            get
            {
                if (_sampleA == null)
                    return VALUE_DOES_NOT_EXIST;

                return _sampleA.PostInjectionTimeInSeconds;
            }
        }
        
        public double UnadjustedBloodVolume
        {
            get { return _unadjustedBloodVolume; }
            set
            {
                if (value > 0)
                    base.SetValue<Double>(ref _unadjustedBloodVolume, "UnadjustedBloodVolume", value);
                else
                    base.SetValue<Double>(ref _unadjustedBloodVolume, "UnadjustedBloodVolume", VALUE_DOES_NOT_EXIST);
            }
        }
        public bool IsWholeSampleExcluded
        {
            get
            {
                if (SampleA == null)
                    return false;
                else if (SampleA.IsExcluded && SampleB == null)
                    return true;
                else if (SampleA.IsExcluded && SampleB.IsExcluded)
                    return true;
                else
                    return false;
            }
            set
            {
                //Exclude both points
                SampleA.IsExcluded = value;
                SampleB.IsExcluded = value;
            }
        }
        public bool AreBothCountsExcluded
        {

            //Have to have both sample in order to have both counts excluded
            get
            {
                return _sampleA != null && _sampleB != null && _sampleA.IsCountExcluded && _sampleB.IsCountExcluded;
            }

        }
        public bool AreBothHematocritsExcluded
        {
            get
            {
                return _sampleA != null && _sampleB != null && _sampleA.IsHematocritExcluded && _sampleB.IsHematocritExcluded;
            }
        }
        
        public double AverageCounts
        {
            get
            {
                int goodSamples = 0;
                double totalCounts = 0;

                if (_sampleA != null && !_sampleA.IsCountExcluded && !_sampleA.IsCounting && _sampleA.Counts > 0)
                {
                    goodSamples++;
                    totalCounts += _sampleA.Counts;
                }

                if (_sampleB != null && !_sampleB.IsCountExcluded && !_sampleB.IsCounting && _sampleB.Counts > 0)
                {
                    goodSamples++;
                    totalCounts += _sampleB.Counts;
                }

                if (goodSamples == 0)
                    return 0;

                //return Math.Round((double)totalCounts / goodSamples);

                //Not rounding here .5 stays. AverageCounts used in calculations without .5 we are off from 5.4 ASK. Don't change.
                return (double)totalCounts / goodSamples;
            }

        }
        public SampleType SampleType
        {
            get
            {
                if (_sampleA == null)
                    return Models.SampleType.Undefined;

                return _sampleA.Type;
            }
        }
      
        
        #endregion

        #region Static Verifiers

        private static void VerifySampleTypeAndRangeMatch(BVASample sampleA, BVASample sampleB)
        {
            ContractHelper.ArgumentNotNull(sampleA, "sampleA");
            if (sampleB == null)
                return;

            if (sampleA.Type == SampleType.Undefined)
                throw new Exception("SampleA contains unsupported type: " + sampleA.Type.ToString());
            
            if (sampleB.Type == SampleType.Undefined)
                throw new Exception("SampleB is of unsupported type: " + sampleB.Type.ToString());

            if (sampleA.Range == sampleB.Range)
                throw new Exception("SampleA and sampleB are of identical range: " + sampleA.Range.ToString());

            if (sampleA.Range == SampleRange.Undefined)
                throw new Exception("SampleA contains unsupported range: " + sampleA.Type.ToString());

            if (sampleB.Range == SampleRange.Undefined)
                throw new Exception("SampleB is of unsupported range: " + sampleB.Type.ToString());

         
        }
        public static List<String> PropertiesInfluenceUnadjustedBloodVolume
        {
            get { return _propertiesAffectUnadjustedBloodVolume; }
        }

        #endregion

        #region Helpers

        private void Sample_PropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == "Counts")
                FirePropertyChanged("AverageCounts");
            else if (args.PropertyName == "IsCountExcluded")
            {
                FirePropertyChanged("AverageCounts");
                FirePropertyChanged("AreBothCountsExcluded");
            }
            else if (args.PropertyName == "Hematocrit")
                FirePropertyChanged("AverageHematocrit");
            else if (args.PropertyName == "IsHematocritExcluded")
                FirePropertyChanged("AreBothHematocritsExcluded");
            else if (args.PropertyName == "IsExcluded")
                FirePropertyChanged("IsWholeSampleExcluded");
            else if (args.PropertyName == "PostInjectionTimeInSeconds")
                FirePropertyChanged(args.PropertyName);
        }

        #endregion
    }
}
