﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq;
using System.Linq;
using Daxor.Lab.Database.CommonBLLs;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.DataObjects;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.SettingsManager;

namespace Daxor.Lab.BVA.Models
{
    /// <summary>
    /// BVATest is a BVA Bounded Context entity.
    /// </summary>
    public class BVATest : TestBase<BVASample>
    {
        #region Const

        public const int MAX_PROTOCOL_COUNT = 6;
        public const int MIN_PROTOCOL_COUNT = 1;

        public const int MAX_SAMPLES_IN_SAMPLE_SET = 2;
        public const int MIN_SAMPLES_IN_SAMPLE_SET = 1;

        public const int ROI_LOW_BOUND = 308;          //Must be retrieved from DB/XML/ETC
        public const int ROI_HIGH_BOUND = 420;         //Must be retrieved from DB/XML/ETC

        public const int MANUAL_TEST_BACKGROUND_ACQUISITION_COUNT = 60;

        #endregion

        #region Fields

        string _refPhysician;       //Referring Physician - a person who is going to be notified about the dtoTest results
        string _ccReportTo;         //CC Report to - another person where the dtoTest results will go to
        string _injectateLot;       //Injectate lot used for the dtoTest
        string _pacsID;             //PACS id
        string _injectateKey;       //Barcode for the injectate
        string _standardAKey;       //Barcode for the standard A
        string _standardBKey;       //Barcode for the standard B
        string _testID2;            //Another id used to identify dtoTest

        double _injectateDose;      //Radioactivity does in microCuries used to injectate a patient with.
        int _protocol = -1;         //Number of samples (A&B parts act as a single sample)
        bool _hasGoodBackground;
        double _regStandardDev;
        
        TubeTypeRecord _tube;
        HematocritFillType _hematocritFill = HematocritFillType.Unknown;
        TestMode _mode;                                 //Mode of the dtoTest: Real, Training, VOPS.
        BVAPatient _patient;                            //Patient that a dtoTest belongs to

        int _roiLowBound;                                //Left most(LOW) bound of region of interest (ROI)
        int _roiHighBound;                               //Right most(HIGH) bound of region of interest(ROI)
        int _refVolume;                                  //Reference volume
        int _durationAdjustedBackgroundCounts = 0;       //Duration adjusted background counts
        int _considerationId;                            //Amputee, etc.
        string _tubeTypeDescription = String.Empty;      //Tube type, will be enum of different tubes; tube type affects results.
        double _anticoagulantFactor = -1;                //Tube type anticoagulant factor

       
        double _transudationRateResult;
        double _stdDevResult;
        double _normalizeHematocritResult;
        string _injectateDoseUnits = "microCuries";

        bool _hasSufficientData;
        List<BVACompositeSample> _compositeSamples = new List<BVACompositeSample>();
        Dictionary<BloodType, BVAVolume> _volumes;

        static List<ISampleLayoutSchema> _schemas = new List<ISampleLayoutSchema>();

        object _concurrencyObject;

        #endregion

        #region Ctor

        private BVATest(Guid testID) : base(testID)
        {
            this.TestType = TestType.BVA;
            this.TestStatus = Infrastructure.Entities.TestStatus.Pending;
            this.Patient = BVAPatient.CreateDefaultModel();
            this.BackgroundCount = -1;
            this.SampleDurationInSeconds = -1;

            _compositeSamples = new List<BVACompositeSample>();

            //Define dictionary of volumes: Measured and Ideals
            _volumes = new Dictionary<BloodType, BVAVolume>();
            _volumes[BloodType.Measured] = new BVAVolume() { Type = BloodType.Measured, RedCellCount = 0, PlasmaCount = 0 };
            _volumes[BloodType.Ideal] = new BVAVolume() { Type = BloodType.Ideal, RedCellCount = 0, PlasmaCount = 0 };
       }

        #endregion

        #region Properties

        public BVAPatient Patient
        {
            get { return _patient; }
            set
            {
                BVAPatient prevPatient = _patient;
                if (base.SetValue<BVAPatient>(ref _patient, "Patient", value))
                {
                    //Dispose of prev patient
                    if (prevPatient != null)
                    {
                        prevPatient.Dispose();
                        prevPatient = null;
                    }
                }
            }
        }

        public ISampleLayoutSchema CurrentSampleSchema
        {
            get
            {
                return (from schema in SampleLayoutSchemas
                        where schema.SchemaID == this.SampleLayoutId
                        select schema).FirstOrDefault();
            }
        }
        public IEnumerable<BVASample> StandardSamples
        {
            get
            {
                return from sample in base.Samples
                       where sample.Type == SampleType.Standard
                       select sample;
            }
        }
        public IEnumerable<BVASample> BaselineSamples
        {
            get
            {
                return from sample in base.Samples
                       where sample.Type == SampleType.Baseline
                       select sample;
            }
        }
        public IEnumerable<BVASample> PatientSamples
        {
            get
            {
                return from sample in base.Samples
                       where (sample.Type != SampleType.Background &&
                              sample.Type != SampleType.Baseline &&
                              sample.Type != SampleType.Standard)
                       select sample;
            }
        }
       
        public IEnumerable<BVACompositeSample> CompositeSamples
        {
            get { return _compositeSamples; }
        } 
        public IEnumerable<BVACompositeSample> PatientCompositeSamples
        {
            get
            {
                return (from cSample in CompositeSamples
                        where cSample.SampleType != SampleType.Baseline &&
                              cSample.SampleType != SampleType.Background &&
                              cSample.SampleType != SampleType.Standard
                        select cSample);
            }
        }
        
        public BVACompositeSample StandardCompositeSample
        {
            get
            {
                return (from s in CompositeSamples where s.SampleType == SampleType.Standard select s).FirstOrDefault();
            }
        }
        public BVACompositeSample PatientBaselineCompositeSample
        {
            get
            {
                return (from s in CompositeSamples where s.SampleType == SampleType.Baseline select s).FirstOrDefault();
            }
        }
        public BVACompositeSample PatientFirstCompositeSample
        {
            get
            {
                return (from s in PatientCompositeSamples where s.SampleType == SampleType.Sample1 select s).FirstOrDefault();
            }
        }

        public IDictionary<BloodType, BVAVolume> Volumes
        {
            get { return _volumes; }
        }

        public int DurationAdjustedBackgroundCounts
        {
            get { return _durationAdjustedBackgroundCounts; }
            private set { base.SetValue<int>(ref _durationAdjustedBackgroundCounts, "DurationAdjustedBackgroundCounts", value); }
        }
        public int RejectedPatientCompositeSamplesCount
        {
            get
            {
                return (from cSample in PatientCompositeSamples
                        where cSample.IsWholeSampleExcluded == true
                        select cSample).Count();
            }
        }
        public int BothCountsExcludedPatientCompositeSampleCount
        {
            get
            {
                return (from cSample in PatientCompositeSamples
                        where cSample.AreBothCountsExcluded == true
                        select cSample).Count();
            }
        }
      
        public int Protocol
        {
            get { return _protocol; }
            set
            {
                //Have to build samples based on protocol then fire that sample are built
                if (value != _protocol)
                {
                    this.BuildPatientSamplesBasedOnProtocol(value);
                    this.UpdateSamplesHematocritBasedOnFill();
                }

                base.SetValue(ref _protocol, "Protocol", value);
            }
        }
        public int RefVolume
        {
            get { return _refVolume; }
            set { base.SetValue<int>(ref _refVolume, "RefVolume", value); }
        }
        public int ROILowBound
        {
            get { return _roiLowBound; }
            set { base.SetValue<int>(ref _roiLowBound, "ROILowBound", value); }
        }
        public int ROIHighBound
        {
            get { return _roiHighBound; }
            set { base.SetValue<int>(ref _roiHighBound, "ROIHighBound", value); }
        }
        public int ConsiderationID
        {
            get { return _considerationId; }
            set { base.SetValue<int>(ref _considerationId, "ConsiderationID", value); }
        }

        public double TransudationRateResult
        {
            get { return _transudationRateResult; }
            set { base.SetValue<double>(ref _transudationRateResult, "TransudationRateResult", value); }
        }
        public double StandardDevResult
        {
            get { return _stdDevResult; }
            set { base.SetValue<double>(ref _stdDevResult, "StandardDevResult", value); }
        }
    
        public double RegressionStandardDevResult
        {
            get { return _stdDevResult; }
            set { base.SetValue<double>(ref _regStandardDev, "RegressionStandardDevResult", value); }
        }
        public double NormalizeHematocritResult
        {
            get { return _normalizeHematocritResult; }
            set { base.SetValue<double>(ref _normalizeHematocritResult, "NormalizeHematocritResult", value); }
        }
       
        public double PatientHematocritResult
        {
            get
            {
                if (HematocritFill == HematocritFillType.OnePerTest)
                    return PatientFirstCompositeSample.SampleA.Hematocrit;

                var goodHematocritSampels = from s in Samples
                                            where s.Type != SampleType.Background && 
                                                  s.Type != SampleType.Standard &&
                                                  s.IsExcluded == false && s.IsHematocritExcluded == false && s.Hematocrit != 0
                                            select s;

                if (goodHematocritSampels.Count() > 0)
                    return goodHematocritSampels.Average(s => s.Hematocrit);

                return 0;
            }
        }

        public double InjectateDose
        {
            get { return _injectateDose; }
            set
            {
                if (base.SetValue<double>(ref _injectateDose, "InjectateDose", value))
                    if (this.TestStatus == Infrastructure.Entities.TestStatus.Pending)
                        this.SampleDurationInSeconds = value != 0 ? (int)((30.0 / _injectateDose) * 60) : 0;
            }
        }
        public double AnticoagulantFactor
        {
            get { return _anticoagulantFactor; }
            set { base.SetValue<double>(ref _anticoagulantFactor, "AnticoagulantFactor", value); }
        }
    
        public string RefPhysician
        {
            get { return _refPhysician; }
            set { base.SetValue<string>(ref _refPhysician, "RefPhysician", value); }
        }
        public string CcReportTo
        {
            get { return _ccReportTo; }
            set { base.SetValue<string>(ref _ccReportTo, "CcReportTo", value); }
        }
        public string InjectateLot
        {
            get { return _injectateLot; }
            set { base.SetValue<string>(ref _injectateLot, "InjectateLot", value); }
        }
        public string TestID2
        {
            get { return _testID2; }
            set { base.SetValue<string>(ref _testID2, "TestID2", value); }
        }
        public string InjectateKey
        {
            get { return _injectateKey; }
            set { base.SetValue<string>(ref _injectateKey, "InjectateKey", value); }
        }
        public string StandardAKey
        {
            get { return _standardAKey; }
            set { base.SetValue<string>(ref _standardAKey, "StandardAKey", value); }
        }
        public string StandardBKey
        {
            get { return _standardBKey; }
            set { base.SetValue<string>(ref _standardBKey, "StandardBKey", value); }
        }
        public string PacsID
        {
            get { return _pacsID; }
            set { base.SetValue<string>(ref _pacsID, "PacsID", value); }
        }
        public string TubeTypeDescription
        {
            get { return _tube != null ? _tube.Description : String.Empty; }
        }
        public string InjectateDoseUnits
        {
            get { return _injectateDoseUnits; }
            set { base.SetValue<string>(ref _injectateDoseUnits, "InjectateDoseUnits", value); }
        }
        public HematocritFillType HematocritFill
        {
            get { return _hematocritFill; }
            set
            {
                //Update each sample hematocrit
                if (base.SetValue<HematocritFillType>(ref _hematocritFill, "HematocritFill", value))
                {
                    //update hematocrit based on hematocritFill
                    UpdateSamplesHematocritBasedOnFill();
                }
            }
        }

        public TestMode Mode
        {
            get { return _mode; }
            set
            {
                if (base.SetValue<TestMode>(ref _mode, "Mode", value))
                {
                    DisableOrEnableUserRequiredRules(_mode == TestMode.Manual);
                    DisableOrEnableTestStartRequiredRules(_mode == TestMode.Manual);
                    EnsureInjectateVerificationEnabledBasedOnMode(_mode);
                }
            }
        }
        public TubeTypeRecord TubeType
        {
            get { return _tube; }
            set { base.SetValue<TubeTypeRecord>(ref _tube, "TubeType", value); FirePropertyChanged("TubeTypeDescription"); }
        }

        public object ConcurrencyObject
        {
            get { return _concurrencyObject; }
            set { _concurrencyObject = value; }
        }
        public bool HasSufficientData
        {
            get { return _hasSufficientData; }
            set { base.SetValue<bool>(ref _hasSufficientData, "HasSufficientData", value); }
        }
        public bool HasGoodBackground
        {
            get { return _hasGoodBackground; }
            set { base.SetValue(ref _hasGoodBackground, "HasGoodBackground", value); }
        }

        #endregion

        #region Static

        public static BVATest CreateDefaultModel(int sampleLayoutId, string uniqueSystemId)
        {
            BVATest mTest = new BVATest(Guid.NewGuid());
            mTest.SystemID = uniqueSystemId;
            mTest.SampleLayoutId = sampleLayoutId;
            mTest.BuildBaselineAndStandardSamples();
            Dictionary<string, Binary> temp = new Dictionary<string,Binary>();
            mTest.ConcurrencyObject = (object)temp;
            mTest.Protocol = SystemSettingsManager.Manager.GetSetting<int>("BVA_DEFAULT_PROTOCOL");
            mTest.HematocritFill = (HematocritFillType)SystemSettingsManager.Manager.GetSetting<int>("BVA_DEFAULT_HCT_FILL_TYPE");
            mTest.TubeType = TubeTypeDataAccess.Select(SystemSettingsManager.Manager.GetSetting<int>("BVA_DEFAULT_TUBE_TYPE"));
            return mTest;
        }
        public static BVATest TestRecordToModel(BvaTestRecord dtoTest)
        {
            if (dtoTest == null)
                throw new ArgumentNullException("dtoTest");

            BVATest mTest = new BVATest(dtoTest.TestId);
            mTest.Patient = BVAPatient.PatientRecordToModel(dtoTest.Patient);

            mTest.Patient.HeightInCm = dtoTest.PatientHeight;
            mTest.Patient.WeightInKg = dtoTest.PatientWeight;
            mTest.Patient.MeasurementSystem = dtoTest.MeasurementSystem.Trim() == DomainConstants.USMeasurementSystem ? MeasurementSystem.English : MeasurementSystem.Metric;

            mTest.SampleLayoutId = dtoTest.SampleLayoutId;
            mTest.TestStatus = (Daxor.Lab.Infrastructure.Entities.TestStatus)dtoTest.StatusId;
            mTest.Mode = (TestMode)dtoTest.TestMode;

            mTest.SystemID = dtoTest.UniqueSystemId;
            mTest.TestDate = dtoTest.TestDate;
            mTest.Comments = dtoTest.Comment;
            mTest.ConsiderationID = dtoTest.ConsiderationId;
            mTest.SampleDurationInSeconds = dtoTest.SampleDurationInSeconds; 
            mTest.BackgroundCount = dtoTest.BackgroundCount;
            mTest.BackgroundTimeSec = dtoTest.BackgroundTime;
            mTest.RefVolume = dtoTest.ReferenceVolume;

            mTest.Analyst = dtoTest.Analyst;
            mTest.InjectateKey = dtoTest.InjectateBarcode;
            mTest.StandardAKey = dtoTest.StandardABarcode;
            mTest.StandardBKey = dtoTest.StandardBBarcode;
            mTest.TestID2 = dtoTest.ID2;
            mTest.Location = dtoTest.Location;
            mTest.InjectateDose = dtoTest.Dose;
            mTest.InjectateLot = dtoTest.InjectateLot;
            mTest.RefPhysician = dtoTest.ReferringPhysician;
            mTest.CcReportTo = dtoTest.CCReportTo;
            mTest.PacsID = dtoTest.PACS;
            //mTest.DurationInSeconds = dtoTest.Duration;

            mTest.HematocritFill = (HematocritFillType)dtoTest.HematocritFillType;
            mTest.TubeType = dtoTest.TubeTypeRecord;
            mTest.ConcurrencyObject = dtoTest.ConcurrencyObject;
            mTest.HasSufficientData = dtoTest.HasSufficientData;

            //Add samples first then update
            dtoTest.Samples.ForEach((sampleDTO) =>
            {
                BVASample sample = BVASample.SampleRecordToModel((BvaSampleRecord)sampleDTO);
                mTest.AssignSampleTypeAndRangeToSample(sample);
                mTest.samples.Add(sample);
            });

            //Build Composite Samples
            var allSamples = from sample in mTest.Samples group sample by sample.Type;
            allSamples.ToList().ForEach((group) => { mTest._compositeSamples.Add(new BVACompositeSample(mTest, group)); });
            
            mTest.Protocol = dtoTest.BloodDrawCount;
            
            return mTest;
        }
        public static BvaTestRecord ModelToTestRecord(BVATest test)
        {
            BvaTestRecord dto = new BvaTestRecord();
                
            // Copy patient
            dto.Patient = new PatientRecord();

            // Only copy the DOB to the DTO if it's valid.
            if ((test.Patient.DateOfBirth.HasValue) && (test.Patient.DateOfBirth != DomainConstants.InvalidDateOfBirth))
                dto.Patient.DOB = (DateTime)test.Patient.DateOfBirth;

            dto.ID2 = test.TestID2 ?? String.Empty;
            dto.Patient.FirstName = test.Patient.FirstName??String.Empty;
            dto.Patient.LastName = test.Patient.LastName??String.Empty;
            dto.Patient.MiddleName = test.Patient.MiddleName??String.Empty;
            dto.Patient.PatientHospitalId = test.Patient.HospitalPID??String.Empty;
            dto.Patient.PID = test.Patient.DaxorInternalPID;
            dto.MeasurementSystem = test.Patient.MeasurementSystem == MeasurementSystem.English ? "US" : "METRIC";
          
            dto.PID = test.Patient.DaxorInternalPID;
            dto.Patient.Gender = (Sex)test.Patient.Gender; 
            dto.PatientHeight = test.Patient.HeightInCm;
            dto.PatientWeight = test.Patient.WeightInKg;

            dto.Analyst = test.Analyst ?? String.Empty;
            dto.BackgroundCount = test.BackgroundCount;
                //  Sample duration *must* be set before the background time is set
            dto.SampleDurationInSeconds = test.SampleDurationInSeconds;
            dto.BackgroundTime = test.BackgroundTimeSec;
            dto.CCReportTo = test.CcReportTo ?? String.Empty;
            dto.Comment = test.Comments ?? String.Empty;
            dto.ConsiderationId = test.ConsiderationID;

            dto.Dose = test.InjectateDose;
            //dto.Duration = test.DurationInSeconds;
            dto.BloodDrawCount = test.Protocol;

            dto.HematocritFillType = (int)test.HematocritFill;

            dto.InjectateBarcode = test.InjectateKey ?? String.Empty;
            dto.InjectateLot = test.InjectateLot??String.Empty;

            dto.Location = test.Location ?? String.Empty;
            dto.PACS = test.PacsID ?? String.Empty;
            dto.ReferenceVolume = test.RefVolume;
            dto.ReferringPhysician = test.RefPhysician ?? String.Empty;
            dto.StandardABarcode = test.StandardAKey ?? String.Empty;
            dto.StandardBBarcode = test.StandardBKey ?? String.Empty;
            
            if (test.Mode == TestMode.Manual)
                test.TestStatus = Infrastructure.Entities.TestStatus.Completed;

            dto.StatusId = (int)test.TestStatus;
            dto.TestDate = test.TestDate;
            dto.TestId = test.TestID;
            dto.TestMode = (int)test.Mode;
            dto.TestType = (int)test.TestType;
            dto.TubeId = test.TubeType.TubeId;
            dto.TestDate = test.TestDate;
            dto.TestMode = (int)test.Mode;
            dto.HasSufficientData = test.HasSufficientData;
            dto.ConcurrencyObject = test.ConcurrencyObject;

            dto.SampleLayoutId = test.SampleLayoutId;
            dto.Samples = new List<BvaSampleRecord>();

            foreach (BVASample samp in test.Samples)
            {
                BvaSampleRecord sampDTO = BVASample.ModelToSampleRecord(samp);
                dto.Samples.Add(sampDTO);
            }

            dto.StatusId = (int)test.TestStatus;
            dto.UniqueSystemId = test.SystemID;
            
            return dto;
        }
        
        public static List<ISampleLayoutSchema> SampleLayoutSchemas
        {
            get { return _schemas; }
        }

        #endregion

        #region PrivateMethods

        //Builds Standard, Baseline, etc.
        void BuildBaselineAndStandardSamples()
        {
            var baselineAndStandardSamples = (from s in CurrentSampleSchema.SampleLayoutItems
                                              where s.Type == SampleType.Baseline || s.Type == SampleType.Standard
                                              select s).ToList();

            if (baselineAndStandardSamples == null)
                throw new Exception("Failed to build Baseline and/or Standard Samples");

            baselineAndStandardSamples.ForEach(
                (sPosition) =>
                {
                    BVASample sample = new BVASample(Guid.NewGuid(), sPosition.Position, ROI_LOW_BOUND, ROI_HIGH_BOUND) { Type = sPosition.Type, Range = sPosition.Range,
                                                                                                                          DisplayName = sPosition.Type.ToString() + sPosition.Range};
                    this.samples.Add(sample);
                });


            _compositeSamples.Add(new BVACompositeSample(this, StandardSamples));
            _compositeSamples.Add(new BVACompositeSample(this, BaselineSamples));
        }
        void BuildPatientSamplesBasedOnProtocol(int newProtocol)
        {
            if (newProtocol < MIN_PROTOCOL_COUNT) return;

            //Current samples
            var groupedCurrentPatientSamples = from p in PatientSamples
                                               group p by p.Type;

            //Number of patient samples groups (Sample1, Sample2, Sample3, etc.)
            int numExistingPatientSamplesGroups = groupedCurrentPatientSamples.Count();

            //Determine what needs to be done (add more samples or remove some extra ones)
            int numSamplesGroupsRequired = newProtocol - numExistingPatientSamplesGroups;
            if (newProtocol == numExistingPatientSamplesGroups)
                return;

            else if (newProtocol > numExistingPatientSamplesGroups)
            {
                if (numSamplesGroupsRequired == newProtocol)
                {
                    //None of the patient samples exist
                    //We need to create numSampleFromHere
                    var sampleTypesToCreate = Enumerable.Range((int)SampleType.Sample1, numSamplesGroupsRequired);
                    sampleTypesToCreate.ToList().ForEach((typeAsInt) =>
                    {
                        //Create 2 samples per type, side A and B
                        int actualSamplePosition_sideA = this.CurrentSampleSchema.GetPosition((SampleType)typeAsInt, SampleRange.A);
                        int actualSamplePosition_sideB = this.CurrentSampleSchema.GetPosition((SampleType)typeAsInt, SampleRange.B);

                        BVASample sampleA = new BVASample(Guid.NewGuid(), actualSamplePosition_sideA, ROI_LOW_BOUND, ROI_HIGH_BOUND)
                        {
                            Type = (SampleType)typeAsInt,
                            Range = SampleRange.A,
                            DisplayName = ((SampleType)typeAsInt).ToString() + SampleRange.A.ToString()
                        };
                        
                        BVASample sampleB = new BVASample(Guid.NewGuid(), actualSamplePosition_sideB, ROI_LOW_BOUND, ROI_HIGH_BOUND)
                        {
                            Type = (SampleType)typeAsInt,
                            Range = SampleRange.B,
                            DisplayName = ((SampleType)typeAsInt).ToString() + SampleRange.B.ToString()
                        };
                        
                        samples.Add(sampleA);
                        samples.Add(sampleB);

                        _compositeSamples.Add(new BVACompositeSample(this, sampleA, sampleB));
                    });
                }
                else
                {

                    //Protocol value has been changed, we need to add additional samples
                    var highestGroup = (from gSample in groupedCurrentPatientSamples
                                        orderby gSample.Key descending
                                        select gSample).FirstOrDefault();
                    SampleType hType = highestGroup.Key;

                    //We need to create numSampleFromHere
                    var sampleTypesToCreate = Enumerable.Range((int)hType + 1, numSamplesGroupsRequired);


                    sampleTypesToCreate.ToList().ForEach((typeAsInt) =>
                    {
                        //Create 2 samples per type, side A and B
                        int actualSamplePosition_sideA = this.CurrentSampleSchema.GetPosition((SampleType)typeAsInt, SampleRange.A);
                        int actualSamplePosition_sideB = this.CurrentSampleSchema.GetPosition((SampleType)typeAsInt, SampleRange.B);

                        BVASample sampleA = new BVASample(Guid.NewGuid(), actualSamplePosition_sideA, ROI_LOW_BOUND, ROI_HIGH_BOUND)
                        {
                            Type = (SampleType)typeAsInt,
                            Range = SampleRange.A,
                            DisplayName = ((SampleType)typeAsInt).ToString() + SampleRange.A.ToString()
                        };
                        
                        BVASample sampleB = new BVASample(Guid.NewGuid(), actualSamplePosition_sideB, ROI_LOW_BOUND, ROI_HIGH_BOUND)
                        {
                            Type = (SampleType)typeAsInt,
                            Range = SampleRange.B,
                            DisplayName = ((SampleType)typeAsInt).ToString() + SampleRange.B.ToString()
                        };
                        
                        samples.Add(sampleA);
                        samples.Add(sampleB);

                        _compositeSamples.Add(new BVACompositeSample(this, sampleA, sampleB));
                    });
                }
            }
            else
            {
                //Start Cutting from the end.
                //Protocol value has been changed, we need to add additional samples
                var highestGroup = (from gSample in groupedCurrentPatientSamples
                                    orderby gSample.Key descending
                                    select gSample).FirstOrDefault();
                SampleType hType = highestGroup.Key;

                //We need to create numSampleFromHere
                var sampleGroupTypesToRemove = Enumerable.Range((int)hType + numSamplesGroupsRequired + 1, Math.Abs(numSamplesGroupsRequired));

                samples.RemoveAll((sample) => sampleGroupTypesToRemove.Contains((int)sample.Type));
                _compositeSamples.RemoveAll((cSample) => sampleGroupTypesToRemove.Contains((int)cSample.SampleType));
            }

        }
        void AssignSampleTypeAndRangeToSample(BVASample sample)
        {
            ContractHelper.ArgumentNotNull(sample, "BVASampleModel");

            if (CurrentSampleSchema == null)
                throw new Exception("BVATestModel. AssignSampleTypeAndRange() failed. SampleSchema cannot be null.");

            SampleLayoutItem pInfo = (from i in CurrentSampleSchema.SampleLayoutItems
                                        where i.Position == sample.Position
                                        select i).FirstOrDefault();
            if (pInfo != null)
            {
                sample.Type = pInfo.Type;
                sample.Range = pInfo.Range;
                sample.DisplayName = pInfo.Type.ToString() + pInfo.Range.ToString();
            }
        }
        void DisableOrEnableUserRequiredRules(bool disable)
        {
            ////disable user required rules
            //var uRequiredRulesToExecuteTest = from r in RuleEngine.Rules.AsParallel()
            //                                  where r.Severity == Infrastructure.RuleFramework.RuleSeverity.UserRequired
            //                                  select r;
            //foreach (var rule in uRequiredRulesToExecuteTest)
            //{
            //    if (rule.TypeOfEntity == typeof(BVAPatient))
            //    {
            //        if (!disable)
            //            this.Patient.RemoveIgnoreRule(rule.PropertyName, rule.RuleID);
            //        else
            //            this.Patient.AddIgnoreRule(rule.PropertyName, rule.RuleID);
            //    }
            //    else if (rule.TypeOfEntity == typeof(BVATest))
            //    {
            //        if (!disable)
            //            this.RemoveIgnoreRule(rule.PropertyName, rule.RuleID);
            //        else
            //            this.AddIgnoreRule(rule.PropertyName, rule.RuleID);
            //    }
            //}
        }
        void DisableOrEnableTestStartRequiredRules(bool disable)
        {
            //var uRequiredRulesToExecuteTest = from r in RuleEngine.Rules.AsParallel()
            //                                  where (r.PropertyName == "InjectateLot" || r.PropertyName == "SampleDurationInSeconds") &&
            //                                  r.Severity == Infrastructure.RuleFramework.RuleSeverity.SystemRequired
            //                                  select r;
            //foreach (var rule in uRequiredRulesToExecuteTest)
            //{
            //    if (rule.TypeOfEntity == this.GetType())
            //    {
            //        if (!disable)
            //            this.RemoveIgnoreRule(rule.PropertyName, rule.RuleID);
            //        else
            //            this.AddIgnoreRule(rule.PropertyName, rule.RuleID);
            //    }
            //}
        }
        void EnsureInjectateVerificationEnabledBasedOnMode(TestMode mode)
        {
            //if (mode != TestMode.VOPS)
            //    return;

            //var uInjectateRules = from r in RuleEngine.Rules.AsParallel()
            //                      where r.PropertyName == "InjectateKey" || r.PropertyName == "StandardAKey" || r.PropertyName == "StandardBKey"
            //                      select r;

            ////Add ignore rules
            //if (mode == TestMode.VOPS || mode == TestMode.Manual)
            //    uInjectateRules.ForAll((r) => this.AddIgnoreRule(r.PropertyName, r.RuleID));

            ////Remove ignore rules
            //else
            //    uInjectateRules.ForAll((r) => this.RemoveIgnoreRule(r.PropertyName, r.RuleID));
        }
        
        #endregion

        #region PublicMethods

        /// <summary>
        /// Business logic to decide if a test can start
        /// </summary>
        /// <returns></returns>
        public bool CanStartExecution()
        {
            ////Cannot start the test if the mode of the test is not either Automatic or VOPS and status is not Pending
            //if (this.Mode != TestMode.Automatic && this.Mode != TestMode.VOPS && this.TestStatus != Infrastructure.Entities.TestStatus.Pending)
            //    return false;

            ////Check test's broken rules
            //bool hasAnyTestInvalidRequiredRules = this.BrokenRules.Any(r => !r.IsValid && r.IsEnabled &&
            //    (r.Severity == Infrastructure.RuleFramework.RuleSeverity.SystemRequired ||
            //     r.Severity == Infrastructure.RuleFramework.RuleSeverity.UserRequired));
            
            //if (hasAnyTestInvalidRequiredRules)
            //    return false;

            ////Check Patient's broken rules
            //bool hasAnyPatientInvalidRequiredRules = this.Patient.BrokenRules.Any(r => !r.IsValid && r.IsEnabled &&
            //    (r.Severity == Infrastructure.RuleFramework.RuleSeverity.SystemRequired ||
            //     r.Severity == Infrastructure.RuleFramework.RuleSeverity.UserRequired));
            
            //if (hasAnyPatientInvalidRequiredRules)
            //    return false;

            return true;

        }

        /// <summary>
        /// Updates hematocrits of each sample based on the fill
        /// </summary>
        public void UpdateSamplesHematocritBasedOnFill()
        {
            HematocritFillType newHematocritFill = this.HematocritFill;
            if (newHematocritFill == HematocritFillType.Unknown || newHematocritFill == HematocritFillType.TwoPerSample)
            {
                return;
            }
            //One per sample
            else if (newHematocritFill == HematocritFillType.OnePerSample)
            {
                foreach (var compositeSample in this.CompositeSamples)
                    compositeSample.SampleB.Hematocrit = compositeSample.SampleA.Hematocrit;
            }
            //One per Test
            else
            {

                double patientSample1HematocritA = 0;
                if (PatientFirstCompositeSample != null && PatientFirstCompositeSample.SampleA != null)
                {
                    PatientFirstCompositeSample.SampleB.Hematocrit = PatientFirstCompositeSample.SampleA.Hematocrit;
                    patientSample1HematocritA = PatientFirstCompositeSample.SampleA.Hematocrit;
                }


                //Go through baseline samples
                foreach (var baselineSample in this.BaselineSamples)
                {
                    baselineSample.Hematocrit = patientSample1HematocritA;
                    baselineSample.IsHematocritExcluded = false;
                }

                //Go through each patient samples
                foreach (var ptnSample in this.PatientSamples)
                {
                    ptnSample.Hematocrit = patientSample1HematocritA;
                    ptnSample.IsHematocritExcluded = false;
                }

            }
        }

        public BVACompositeSample FindCompositeSampleAfter(BVACompositeSample currentSample)
        {
            //Return null if null was given
            if (currentSample == null)
                return currentSample;

            if(_compositeSamples == null)
                return null;

            int indexOfCurrent = _compositeSamples.IndexOf(currentSample);
            if (indexOfCurrent < 0)
                return null;

            return _compositeSamples.ElementAtOrDefault(indexOfCurrent + 1);
        }
        public BVACompositeSample FindCompositeSampleBefore(BVACompositeSample currentSample)
        {
            //Return null if null was given
            if (currentSample == null)
                return currentSample;

            if (_compositeSamples == null)
                return null;

            int indexOfCurrent = _compositeSamples.IndexOf(currentSample);
            if (indexOfCurrent <= 0)
                return null;

            return _compositeSamples.ElementAtOrDefault(indexOfCurrent - 1);
        }

        public void ComputeTestAdjustedBackgroundCounts()
        {
            int adjustedCounts = 0;
            if (this.Mode == TestMode.Manual)
                adjustedCounts = this.BackgroundCount;
            else
            {
                if (this.BackgroundTimeSec > 0)
                {
                    double cps = this.BackgroundCount / (double)this.BackgroundTimeSec;
                    adjustedCounts = (int)Math.Round(this.SampleDurationInSeconds * cps, 0);
                }
            }

            this.DurationAdjustedBackgroundCounts = adjustedCounts;
        }
        public void ClearBarcodes()
        {
            this.InjectateLot = String.Empty;
            this.InjectateKey = String.Empty;
            this.StandardAKey = String.Empty;
            this.StandardBKey = String.Empty;
        }
        
        #endregion

        #region Override

        protected override void FirePropertyChanged(string propertyName)
        {
            base.FirePropertyChanged(propertyName);

            //Find if background counts or background acquisition time has changed.
            if (propertyName == "BackgroundCount" || propertyName == "BackgroundTimeSec" || propertyName == "SampleDurationInSeconds")
            {
                ComputeTestAdjustedBackgroundCounts();
            }
            else if (propertyName == "TestStatus" && this.TestStatus != Infrastructure.Entities.TestStatus.Pending)
            {
                DisableOrEnableUserRequiredRules(true);
            }
            else if (propertyName == "Mode")
            {
              
            }
        }
        protected override void OnDispose()
        {
            base.OnDispose();

            if (Patient != null)
                Patient.Dispose();
        }

        //protected override void RulesCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        //{
        //    base.RulesCollectionChanged(sender, e);

        //    //Deal with injectate verification or LotNumber
        //    //if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
        //    //{

        //    //    bool contains = e.NewItems
        //    //}
        //    //else if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
        //    //{

        //    //}
        //}
        #endregion
    }
}
