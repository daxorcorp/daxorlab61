﻿namespace Daxor.Lab.QC.Interfaces
{
    /// <summary>
    /// Destination view keys
    /// </summary>
    public static class DestinationViewKeys
    {
        public const string QcTestSelection = "selection";
        public const string QcTestSetup = "setup";
        public const string QcTestResults = "results";
    }
}
