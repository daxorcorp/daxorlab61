﻿using System;
using System.Globalization;
using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Interfaces
{
    public static class QcTestNameConverter
    {
        public static string Convert(string value)
        {
            if (value == null)
                throw new ArgumentNullException("value");

            TestType typeToCreate;
            if (!Enum.TryParse(value.ToString(CultureInfo.InvariantCulture), out typeToCreate))
                throw new ArgumentOutOfRangeException("value");

            switch (typeToCreate)
            {
                case TestType.Contamination:
                    return QcTypes.Contamination;
                case TestType.Standards:
                    return QcTypes.Standards;
                case TestType.Full:
                    return QcTypes.FullQc;
                case TestType.Linearity:
                case TestType.LinearityWithoutCalibration:
                    return QcTypes.Linearity;
                default:
                    return QcTypes.Unknown;
            }
        }

        public static string Convert(TestType value)
        {
            return Convert(value.ToString());
        }
    }
}
