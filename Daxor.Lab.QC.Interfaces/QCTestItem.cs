﻿using System;

namespace Daxor.Lab.QC.Interfaces
{
    /// <summary>
    /// Test item model used to display item on the grid.
    /// </summary>
    public class QcTestItem
    {
        private string _type;

        public Guid TestId { get; set; }
        public string Status { get; set; }
        public string Result { get; set; }
   
        public string Analyst { get; set; }
        public string Type
        {
            get { return _type; }
            set { _type = QcTestNameConverter.Convert(value); }
        }

        public DateTime TestDate { get; set; }
        public bool? IsPassed { get; set; }
      

        public string TestStatusDescription { get { return Status; } }

    }
}
