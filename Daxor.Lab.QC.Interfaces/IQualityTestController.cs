﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Daxor.Lab.QC.Core;

namespace Daxor.Lab.QC.Interfaces
{
    public interface IQualityTestController : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets RunningTest
        /// </summary>
        QcTest RunningTest { get; }

        /// <summary>
        /// Gets SelectedTest
        /// </summary>
        QcTest SelectedTest { get; }
        
        /// <summary>
        /// Gets SaveTestCommand.
        /// </summary>
        ICommand SaveTestCommand { get; }

        /// <summary>
        /// Gets SelectTestCommand.
        /// </summary>
        ICommand SelectTestCommand { get; }

        /// <summary>
        /// Gets StartTestExecutionCommand
        /// </summary>
        ICommand StartSelectedTestExecutionCommand { get; }

        /// <summary>
        /// Gets AbortTestExecutionCommand
        /// </summary>
        ICommand AbortTestExecutionCommand { get; }

        /// <summary>
        /// Gets BeginNewTestCommand
        /// </summary>
        ICommand BeginNewTestCommand { get; }

        /// <summary>
        /// Select test fiendly name
        /// </summary>
        String SelectedTestFriendlyName { get; }

        /// <summary>
        /// Navigate to a view key
        /// </summary>
        /// <param name="viewKey"></param>
        void Navigate(string viewKey);


        /// <summary>
        /// Data service for QC
        /// </summary>
        IQualityTestDataService DataService { get; }

        event EventHandler TestItemsNeedRefresh;
    }
}
