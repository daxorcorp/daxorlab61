﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core;

namespace Daxor.Lab.QC.Interfaces
{
    /// <summary>
    /// Quality control data service
    /// </summary>
    public interface IQualityTestDataService
    {
        IQueryable<QcTestItem> GetTestItems();
        ICalibrationCurve GetCurrentCalibrationCurve();
        IEnumerable<QcSource> GetSources();

        void SaveTest(QcTest test);
        
        QcTest LoadTest(Guid testId);
        bool DeleteTest(Guid testId);
    }
}
