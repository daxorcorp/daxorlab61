﻿using Daxor.Lab.Domain.Interfaces;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.QC.Interfaces
{
	public interface ICalibrationCurveDataService
	{
		ICalibrationCurve GetSpecificCalibrationCurve(int calibrationCurveId, ILoggerFacade logger);
		int SaveCalibrationCurve(ICalibrationCurve curve, ILoggerFacade logger);
	}
}
