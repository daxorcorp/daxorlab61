﻿namespace Daxor.Lab.QC.Interfaces
{
	public static class QcTypes
	{
		public const string FullQc = "Full QC";
		public const string Contamination = "Contamination";
		public const string Standards = "Standards";
		public const string Linearity = "Linearity";
		public const string Unknown = "Unknown";
	}
}
