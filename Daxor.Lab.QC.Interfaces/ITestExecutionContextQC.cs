﻿using System;
using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.QC.Interfaces
{
    /// <summary>
    /// Provides a context for executing (i.e., processing) of samples on a test.
    /// </summary>
    public interface ITestExecutionContextQc
    {
        #region Events

        /// <summary>
        /// Occurs when one or more parameters pertaining to the execution of a test 
        /// have been updated.
        /// </summary>
        event EventHandler<ExecutionParametersUpdatedEventArgs> ExecutionParametersUpdated;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the ISample that represents the background radiation sample.
        /// </summary>
        ISample BackgroundSample { get; }

        /// <summary>
        /// Gets the ID of the test associated with this execution context.
        /// </summary>
        Guid ExecutingTestId { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Cancels the execution of a test.
        /// </summary>
        void CancelExecution();

        /// <summary>
        /// Provides the next sample to execute.
        /// </summary>
        /// <returns>The sample to execute</returns>
        ISample GetNextSampleToExecute();

        /// <summary>
        /// Overrides background acquisition and uses the specified number of 
        /// counts as background.
        /// </summary>
        /// <param name="counts">Background count to use as the overriding value</param>
        void OverrideBackground(int counts);

        /// <summary>
        /// Processes the executing sample.
        /// </summary>
        void ProcessExecutingSample(ISample sample);

        /// <summary>
        /// Starts the execution of a test.
        /// </summary>
        void StartExecution();

        /// <summary>
        /// Processes the test background.
        /// </summary>
        void ProcessBackground(int counts, int acqTimeInSeconds);

        #endregion
    }

}
