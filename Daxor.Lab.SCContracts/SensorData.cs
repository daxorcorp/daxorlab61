﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections;

namespace Daxor.Lab.SCContracts
{
    /// <summary>
    /// Raw sensor data read by sample changer
    /// </summary>
    [DataContract(Name = "SensorData", Namespace = "http://daxor.com/sc/2010/08/raw")]
    public class SensorData : IExtensibleDataObject
    {
        // Part of IExtensibleDataObject interface. The property
        // holds data from future versions of the class for backward
        // compatibility.
        private ExtensionDataObject extensionDataObject_value;
        public ExtensionDataObject ExtensionData
        {
            get { return extensionDataObject_value; }
            set { extensionDataObject_value = value; }
        }

        public static SensorData ConvertFrom(BitArray pinArray)
        {
            if (pinArray == null || pinArray.Length < 17)
                return null;

            SensorData s =  new SensorData()
            {
                Pin10x8  = pinArray[10],
                Pin11x16 = pinArray[11],
                Pin12x4  = pinArray[12],
                Pin13x2  = pinArray[13],
                Pin15x1  = pinArray[15],
            };

            s.PositionInBinaryFormat = (s.Pin11x16 ? 1 : 0).ToString() + (s.Pin10x8 ? 1 : 0).ToString() +(s.Pin12x4 ? 1 : 0).ToString() + (s.Pin13x2 ? 1 : 0).ToString() + (s.Pin15x1 ? 1 : 0).ToString();
            s.PositionInDecimal = (s.Pin11x16 ? 1 : 0) * 16 + (s.Pin10x8 ? 1 : 0) * 8 + (s.Pin12x4 ? 1 : 0) * 4 + (s.Pin13x2 ? 1 : 0) * 2 + (s.Pin15x1 ? 1 : 0) * 1;

            return s;
        }

        [DataMember]
        public Boolean Pin10x8;

        [DataMember]
        public Boolean Pin11x16;

        [DataMember]
        public Boolean Pin12x4;

        [DataMember]
        public Boolean Pin13x2;

        [DataMember]
        public Boolean Pin15x1;

        [DataMember]
        public String PositionInBinaryFormat;

        [DataMember]
        public Int32 PositionInDecimal;
    }
}
