﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Daxor.Lab.SCContracts
{
    public interface ISampleChangerServiceCallbackEvents
    {
        [OperationContract(IsOneWay = true)]
        void OnPositionChangeBeginCallbackEvent(int OldPosition, int NewPosition);

        [OperationContract(IsOneWay = true)]
        void OnPositionChangeEndCallbackEvent(int NewPosition);

        [OperationContract(IsOneWay = true)]
        void OnPositionSeekBeginCallbackEvent(int CurrentPostion, int SeekPosition);

        [OperationContract(IsOneWay = true)]
        void OnPositionSeekEndCallbackEvent(int NewPosition);

        [OperationContract(IsOneWay = true)]
        void OnPositionSeekCancelCallbackEvent(int CurrentPostion, int SeekPosition);

        [OperationContract(IsOneWay = true)]
        void OnChangeErrorCallbackEvent(int ErrorCode, string Message);
    }
}
