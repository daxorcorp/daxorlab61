﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Daxor.Lab.SCContracts
{
    [ServiceContract(Namespace = "http://daxor.com/sc/2010/08")]
    public interface ISubscriber
    {
        [OperationContract]
        void Subscribe();

        [OperationContract]
        void Unsubscribe();
    }
}
