﻿using System;
using System.ServiceModel;

namespace Daxor.Lab.SCContracts
{
    [ServiceContract(Namespace = "http://daxor.com/sc/2010/08", 
        CallbackContract = typeof(ISampleChangerServiceCallbackEvents), 
        SessionMode = SessionMode.Required)]
    public interface ISampleChangerService : ISubscriber
    {
        //Moves sample changer one position for a wheel based sample changer
        //or move 1 sample for pick and place unit
        [OperationContract(IsOneWay = true)]
        void PulseSampleChanger();

        //Gets current position
        [OperationContract(IsOneWay = false)]
        int GetCurrentPosition();

        //Gets current position
        [OperationContract(IsOneWay = false)]
        SensorData GetRawSensorData();

        //Starts moving sample changer until final position is reached.
        [OperationContract(IsOneWay = true)]
        void MoveToPosition(int GoalPosition);

        //Stops sample changer
        [OperationContract(IsOneWay = true)]
        void StopSampleChanger();

        //Terminates sample changer threads
        [OperationContract(IsOneWay = true)]
        void Terminate();

        //Determine if sample changer is moving
        [OperationContract(IsOneWay = false)]
        bool IsMoving();

        //Check if sample changer is Open
        [OperationContract(IsOneWay = false)]
        bool IsOpened();

        //Gets/sets sleepTime used by PositionMonitor
        [OperationContract(IsOneWay = false)]
        TimeSpan GetSleepTime();

        [OperationContract(IsOneWay = false)]
        double GetTestDelay();

        [OperationContract(IsOneWay = true)]
        void SetTestDelay(double tDelay);
        [OperationContract(IsOneWay = true)]
        void SetSleepTime(TimeSpan tSpan);

        //Gets time when last position was attained
        [OperationContract(IsOneWay = false)]
        DateTime GetTimeLastPositionAttained();

        //Get the version of sample changer service
        [OperationContract(IsOneWay = false)]
        string GetVersion();

        //Determines if the sample changer has power
        [OperationContract(IsOneWay = false)]
        bool IsPoweredUp();
    }
}
