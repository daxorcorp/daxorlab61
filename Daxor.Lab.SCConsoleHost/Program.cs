﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.SCContracts;
using Daxor.Lab.SCService;
using System.ServiceModel;

namespace Daxor.Lab.SCConsoleHost
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "SampleChangerService Host";
            Console.ForegroundColor = ConsoleColor.Blue;
            ISampleChangerService smcService = null;
            ServiceHost host=null;
            try
            {
                smcService = new SampleChangerService();
                host = new ServiceHost(smcService);
                {
                    host.Opened += host_Opened;
                    host.Closing += host_Closing;
                    host.Closed += host_Closed;
                    host.Faulted += host_Faulted;

                    host.Open();

                    Console.WriteLine("SampleChanger Service is up and running " + DateTime.Now.ToLongTimeString());
                    Console.WriteLine("Press Any Key to terminate the service.");
                    Console.ReadLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception. Message " + ex.Message);
                Console.WriteLine("Press Any Key to continue...");
                Console.ReadKey();

            }
            finally
            {
                if (host != null)
                    host.Close();
                Console.WriteLine("Sample Changer Service is being shut down. Wait.");
                if (smcService != null)
                    smcService.Terminate();
            }

        }

        static void host_Opened(object sender, EventArgs e)
        {
            Console.WriteLine("SampleChanger Service channel has opened.");
        }
        static void host_Faulted(object sender, EventArgs e)
        {
            System.Console.WriteLine("SampleChanger Service channel has faulted.");
        }
        static void host_Closed(object sender, EventArgs e)
        {
            System.Console.WriteLine("SampleChanger Service channel has closed.");
        }
        static void host_Closing(object sender, EventArgs e)
        {
            System.Console.WriteLine("SampleChanger Service channel is closing.");
        }
    }
}
