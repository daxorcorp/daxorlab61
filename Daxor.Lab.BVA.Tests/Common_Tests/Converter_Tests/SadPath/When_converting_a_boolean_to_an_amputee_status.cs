﻿using System;
using Daxor.Lab.BVA.Common.Converters;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.BVA.Tests.Common_Tests.Converter_Tests.SadPath
{
	[TestClass]
	public class When_converting_a_boolean_to_an_amputee_status
	{
		private static BooleanToAmputeeStatusConverter _converter;

		[ClassInitialize]
		public static void TestClassInitialize(TestContext context)
		{
			_converter = new BooleanToAmputeeStatusConverter();
		}

		[TestMethod]
		public void And_the_value_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => _converter.Convert(null, null, null, null), "value");
		}

		[TestMethod]
		public void And_the_value_is_not_a_boolean_then_an_exception_is_thrown()
		{
			AssertEx.Throws<NotSupportedException>(() => _converter.Convert(DateTime.Now, null, null, null));
		}

		[TestMethod]
		public void And_converting_back_and_the_value_is_null_then_an_exception_is_thrown()
		{
			var converter = new BooleanToAmputeeStatusConverter();
			AssertEx.ThrowsArgumentNullException(() => converter.ConvertBack(null, null, null, null), "value");
		}

		[TestMethod]
		public void And_converting_back_and_the_value_is_not_a_boolean_then_an_exception_is_thrown()
		{
			AssertEx.Throws<NotSupportedException>(() => _converter.ConvertBack(DateTime.Now, null, null, null));
		}
	}
}
