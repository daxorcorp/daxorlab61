﻿using Daxor.Lab.BVA.Common.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.BVA.Tests.Common_Tests.Converter_Tests
{
	[TestClass]
	public class When_converting_an_amputee_ideals_correction_factor_to_a_formatted_amputee_ideals_correction_factor
	{
		private AmputeeIdealsCorrectionFactorToFormattedAmputeeIdealsCorrectionFactorConverter _converter;

		[TestInitialize]
		public void Init()
		{
			_converter = new AmputeeIdealsCorrectionFactorToFormattedAmputeeIdealsCorrectionFactorConverter();
		}

		[TestMethod]
		public void And_the_reduction_percentage_is_0_then_0_percent_is_returned()
		{
			Assert.AreEqual("0%", _converter.Convert(0, null, null, null));
		}

		[TestMethod]
		public void And_the_reduction_percentage_is_greater_than_0_then_an_equal_negative_percentage_is_returned()
		{
			for (var reductionPercentage = 1; reductionPercentage < 10; reductionPercentage++)
			{
				Assert.AreEqual("-" + reductionPercentage + "%", _converter.Convert(reductionPercentage, null, null, null));
			}
		}

		[TestMethod]
		public void And_the_reduction_percentage_is_negative_1_then_the_empty_string_is_returned()
		{
			Assert.AreEqual(string.Empty, _converter.Convert(-1, null, null, null));
		}
	}
}
