﻿using Daxor.Lab.BVA.Common.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.BVA.Tests.Common_Tests.Converter_Tests
{
	[TestClass]
	public class When_converting_amputee_status_to_an_Ideal_label
	{
		private const string AmputeeIdealLabel = "Ideal*";
		private const string NonAmputeeIdealLabel = "Ideal";

		private AmputeeStatusToIdealLabelConverter _converter;

		[TestInitialize]
		public void Initialize()
		{
			_converter = new AmputeeStatusToIdealLabelConverter();
		}

		[TestMethod]
		public void And_the_status_is_true_then_the_amputee_Ideal_label_is_returned()
		{
			Assert.AreEqual(AmputeeIdealLabel, _converter.Convert(true, null, null, null));
		}

		[TestMethod]
		public void And_the_status_is_false_then_the_nonAmputee_Ideal_label_is_returned()
		{
			Assert.AreEqual(NonAmputeeIdealLabel, _converter.Convert(false, null, null, null));
		}
	}
}
