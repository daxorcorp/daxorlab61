﻿using Daxor.Lab.BVA.Common.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.BVA.Tests.Common_Tests.Converter_Tests
{
	[TestClass]
	public class When_converting_a_boolean_to_an_amputee_status
	{
		private BooleanToAmputeeStatusConverter _converter;

		[TestInitialize]
		public void Initialize()
		{
			_converter = new BooleanToAmputeeStatusConverter();
		}

		[TestMethod]
		public void And_the_value_is_true_then_Amputee_is_returned()
		{
			Assert.AreEqual("Amputee", (string) _converter.Convert(true, null, null, null));
		}

		[TestMethod]
		public void And_the_value_is_false_then_Non_amputee_is_returned()
		{
			Assert.AreEqual("Non-amputee", (string) _converter.Convert(false, null, null, null));
		}

		[TestMethod]
		public void And_converting_back_and_the_value_is_amputee_then_true_is_returned()
		{
			Assert.IsTrue((bool) _converter.ConvertBack("Amputee", null, null, null));
		}

		[TestMethod]
		public void And_converting_back_and_the_value_is_Non_amputee_then_false_is_returned()
		{
			Assert.IsFalse((bool) _converter.ConvertBack("Non-amputee", null, null, null));
		}
	}
}
