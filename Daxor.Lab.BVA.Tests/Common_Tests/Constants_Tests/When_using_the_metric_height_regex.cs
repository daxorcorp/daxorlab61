using System.Globalization;
using System.Text.RegularExpressions;
using Daxor.Lab.BVA.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Common_Tests.Constants_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_metric_height_regex
    {
        [TestMethod]
        public void Then_the_entire_range_of_valid_heights_match()
        {
            for (var i = 0.3m; i <= 253.7m; i += 0.01m)
                Assert.IsTrue(Regex.IsMatch(i.ToString(CultureInfo.InvariantCulture), Constants.MetricHeightRegEx), "No match on " + i);
        }

        [TestMethod]
        public void And_the_height_has_three_decimal_points_then_it_does_not_match()
        {
            Assert.IsFalse(Regex.IsMatch("33.453", Constants.MetricHeightRegEx));
        }

        [TestMethod]
        public void And_the_height_is_zero_then_it_matches()
        {
            Assert.IsTrue(Regex.IsMatch("0", Constants.MetricHeightRegEx), "No match on 0");
            Assert.IsTrue(Regex.IsMatch("0.0", Constants.MetricHeightRegEx), "No match on 0.0");
            Assert.IsTrue(Regex.IsMatch("0.00", Constants.MetricHeightRegEx), "No match on 0.00");
        }

        [TestMethod]
        public void And_the_height_is_above_the_maximum_then_it_does_not_match()
        {
            Assert.IsFalse(Regex.IsMatch("254", Constants.MetricHeightRegEx), "Match on 254");
            Assert.IsFalse(Regex.IsMatch("253.71", Constants.MetricHeightRegEx), "Match on 253.71");
            Assert.IsFalse(Regex.IsMatch("253.8", Constants.MetricHeightRegEx), "Match on 253.8");
        }

        [TestMethod]
        public void And_the_height_is_below_the_minimum_then_it_does_not_match()
        {
            Assert.IsFalse(Regex.IsMatch("0.029", Constants.MetricHeightRegEx), "Match on 0.029");
            Assert.IsFalse(Regex.IsMatch("0.02", Constants.MetricHeightRegEx), "Match on 0.02");
            Assert.IsFalse(Regex.IsMatch("0.01", Constants.MetricHeightRegEx), "Match on 0.01");
        }
    }
    // ReSharper restore InconsistentNaming
}
