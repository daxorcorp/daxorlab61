using System.Globalization;
using System.Text.RegularExpressions;
using Daxor.Lab.BVA.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Common_Tests.Constants_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_English_weight_regex
    {
        [TestMethod]
        public void Then_the_entire_range_of_valid_weights_match()
        {
            for (var i = 0.02m; i <= 1430m; i += 0.01m)
                Assert.IsTrue(Regex.IsMatch(i.ToString(CultureInfo.InvariantCulture), Constants.EnglishWeightRegEx), "No match on " + i);
        }

        [TestMethod]
        public void And_the_weight_has_three_decimal_points_then_it_does_not_match()
        {
            Assert.IsFalse(Regex.IsMatch("33.453", Constants.EnglishWeightRegEx));
        }

        [TestMethod]
        public void And_the_weight_is_zero_then_it_matches()
        {
            Assert.IsTrue(Regex.IsMatch("0", Constants.EnglishWeightRegEx), "No match on 0");
            Assert.IsTrue(Regex.IsMatch("0.0", Constants.EnglishWeightRegEx), "No match on 0.0");
            Assert.IsTrue(Regex.IsMatch("0.00", Constants.EnglishWeightRegEx), "No match on 0.00");
        }

        [TestMethod]
        public void And_the_weight_is_above_the_maximum_then_it_does_not_match()
        {
            Assert.IsFalse(Regex.IsMatch("1431", Constants.EnglishWeightRegEx), "Match on 1431");
            Assert.IsFalse(Regex.IsMatch("1430.01", Constants.EnglishWeightRegEx), "Match on 1430.01");
            Assert.IsFalse(Regex.IsMatch("1430.1", Constants.EnglishWeightRegEx), "Match on 1430.1");
        }

        [TestMethod]
        public void And_the_weight_is_below_the_minimum_then_it_does_not_match()
        {
            Assert.IsFalse(Regex.IsMatch("0.01", Constants.EnglishWeightRegEx));
        }
    }
    // ReSharper restore InconsistentNaming
}
