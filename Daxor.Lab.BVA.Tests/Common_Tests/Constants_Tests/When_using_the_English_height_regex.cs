using System.Globalization;
using System.Text.RegularExpressions;
using Daxor.Lab.BVA.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Common_Tests.Constants_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_English_height_regex
    {
        [TestMethod]
        public void Then_the_entire_range_of_valid_heights_match()
        {
            for (var i = 0.1m; i <= 99.9m; i += 0.1m)
                Assert.IsTrue(Regex.IsMatch(i.ToString(CultureInfo.InvariantCulture), Constants.EnglishHeightRegEx), "No match on " + i);
        }

        [TestMethod]
        public void And_the_height_has_two_decimal_points_then_it_does_not_match()
        {
            Assert.IsFalse(Regex.IsMatch("33.45", Constants.EnglishHeightRegEx));
        }

        [TestMethod]
        public void And_the_height_is_zero_then_it_matches()
        {
            Assert.IsTrue(Regex.IsMatch("0", Constants.EnglishHeightRegEx), "No match on 0");
            Assert.IsTrue(Regex.IsMatch("0.0", Constants.EnglishHeightRegEx), "No match on 0.0");
        }

        [TestMethod]
        public void And_the_height_is_above_the_maximum_then_it_does_not_match()
        {
            Assert.IsFalse(Regex.IsMatch("100", Constants.EnglishHeightRegEx), "Match on 100");
            Assert.IsFalse(Regex.IsMatch("100.0", Constants.EnglishHeightRegEx), "Match on 100.0");
            Assert.IsFalse(Regex.IsMatch("101", Constants.EnglishHeightRegEx), "Match on 101");
        }

        [TestMethod]
        public void And_the_height_is_below_the_minimum_then_it_does_not_match()
        {
            Assert.IsFalse(Regex.IsMatch("0.09", Constants.EnglishHeightRegEx));
        }
    }
    // ReSharper restore InconsistentNaming
}
