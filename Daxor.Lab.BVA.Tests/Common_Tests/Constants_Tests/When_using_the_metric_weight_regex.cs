using System.Globalization;
using System.Text.RegularExpressions;
using Daxor.Lab.BVA.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Common_Tests.Constants_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_metric_weight_regex
    {
        [TestMethod]
        public void Then_the_entire_range_of_valid_weights_match()
        {
            for (var i = 0.01m; i <= 648.64m; i += 0.01m)
                Assert.IsTrue(Regex.IsMatch(i.ToString(CultureInfo.InvariantCulture), Constants.MetricWeightRegEx), "No match on " + i);
        }

        [TestMethod]
        public void And_the_weight_has_three_decimal_points_then_it_does_not_match()
        {
            Assert.IsFalse(Regex.IsMatch("33.453", Constants.MetricWeightRegEx));
        }

        [TestMethod]
        public void And_the_weight_is_zero_then_it_matches()
        {
            Assert.IsTrue(Regex.IsMatch("0", Constants.MetricWeightRegEx), "No match on 0");
            Assert.IsTrue(Regex.IsMatch("0.0", Constants.MetricWeightRegEx), "No match on 0.0");
            Assert.IsTrue(Regex.IsMatch("0.00", Constants.MetricWeightRegEx), "No match on 0.00");
        }

        [TestMethod]
        public void And_the_weight_is_above_the_maximum_then_it_does_not_match()
        {
            Assert.IsFalse(Regex.IsMatch("649", Constants.MetricWeightRegEx), "Match on 649");
            Assert.IsFalse(Regex.IsMatch("648.65", Constants.MetricWeightRegEx), "Match on 648.65");
            Assert.IsFalse(Regex.IsMatch("648.7", Constants.MetricWeightRegEx), "Match on 648.7");
        }

        [TestMethod]
        public void And_the_weight_is_below_the_minimum_then_it_does_not_match()
        {
            Assert.IsFalse(Regex.IsMatch("0.001", Constants.MetricWeightRegEx));
        }
    }
    // ReSharper restore InconsistentNaming
}
