﻿using System;
using Daxor.Lab.BVA.Reports;
using Daxor.Lab.BVA.Reports.Interfaces;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.Module_Tests.SadPath
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_the_BVA_module
	{
		private BvaModuleThatDoesNotAddResourceDictionaries _module;
		private IUnityContainer _container;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_container = Substitute.For<IUnityContainer>();
		}

		[TestMethod]
		public void And_an_error_occurs_while_adding_the_resource_dictionary_then_the_correct_module_load_exception_is_thrown()
		{
			_module = new BvaModuleThatDoesNotAddResourceDictionaries(_container) {ThrowExceptionOnAddToMergedDictionary = true};

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA", ex.ModuleName);
				Assert.AreEqual("add the resource dictionary", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_audit_trail_data_access_then_the_correct_module_load_exception_is_thrown()
		{
			_module = new BvaModuleThatDoesNotAddResourceDictionaries(_container) { ThrowExceptionOnRegisterAuditTrailDataAccess = true };

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA", ex.ModuleName);
				Assert.AreEqual("initialize the Audit Trail Data Access", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_patient_repository_then_the_correct_module_load_exception_is_thrown()
		{
			_module = new BvaModuleThatDoesNotAddResourceDictionaries(_container) { ThrowExceptionOnRegisterPatientRepository = true };

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA", ex.ModuleName);
				Assert.AreEqual("initialize the Patient Repository", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_sample_repository_then_the_correct_module_load_exception_is_thrown()
		{
			_module = new BvaModuleThatDoesNotAddResourceDictionaries(_container) { ThrowExceptionOnRegisterSampleRepository = true };

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA", ex.ModuleName);
				Assert.AreEqual("initialize the Sample Repository", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_test_info_repository_then_the_correct_module_load_exception_is_thrown()
		{
			_module = new BvaModuleThatDoesNotAddResourceDictionaries(_container) { ThrowExceptionOnRegisterTestInfoRepository = true };

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA", ex.ModuleName);
				Assert.AreEqual("initialize the BVA Test Information Repository", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_test_repository_then_the_correct_module_load_exception_is_thrown()
		{
			_module = new BvaModuleThatDoesNotAddResourceDictionaries(_container) { ThrowExceptionOnRegisterTestRepository = true };

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA", ex.ModuleName);
				Assert.AreEqual("initialize the BVA Test Repository", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_configuring_module_alerts_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<IRuleEngine>(Arg.Any<string>())).Throw(new Exception());
			_module = new BvaModuleThatDoesNotAddResourceDictionaries(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA", ex.ModuleName);
				Assert.AreEqual("configure the Module Alerts", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_module_scoped_popup_controller_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(
				c =>
					c.RegisterType<IPopupController, ModuleScopedPopupController>(Arg.Any<string>(),
						Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new BvaModuleThatDoesNotAddResourceDictionaries(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA", ex.ModuleName);
				Assert.AreEqual("register the Popup Controller", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_BVA_result_alert_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(
				c =>
					c.RegisterType<IBvaResultAlertService, BvaResultAlertService>(Arg.Any<string>(),
						Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new BvaModuleThatDoesNotAddResourceDictionaries(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA", ex.ModuleName);
				Assert.AreEqual("initialize the BVA Result Alert Service", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_PDF_printer_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IPdfPrinter, PdfPrinter>()).Throw(new Exception());
			_module = new BvaModuleThatDoesNotAddResourceDictionaries(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA", ex.ModuleName);
				Assert.AreEqual("initialize the PDF Printer", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_BVA_module_navigator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(
				c =>
					c.RegisterType<IAppModuleNavigator, BVAModuleNavigator>(Arg.Any<string>(),
						Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new BvaModuleThatDoesNotAddResourceDictionaries(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA", ex.ModuleName);
				Assert.AreEqual("initialize the BVA Module Navigator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_resolving_and_initializing_the_app_module_navigator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<IAppModuleNavigator>(Arg.Any<string>())).Throw(new Exception());
			_module = new BvaModuleThatDoesNotAddResourceDictionaries(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA", ex.ModuleName);
				Assert.AreEqual("initialize the App Module Navigator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_resolving_and_initializing_the_popup_controller_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<IPopupController>(Arg.Any<string>())).Throw(new Exception());
			_module = new BvaModuleThatDoesNotAddResourceDictionaries(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA", ex.ModuleName);
				Assert.AreEqual("initialize the Popup Controller", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}
	}
	// ReSharper restore InconsistentNaming
}