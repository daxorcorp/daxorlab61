﻿using System;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.BVA.Tests.Module_Tests
{
	internal class BvaModuleThatDoesNotAddResourceDictionaries : Module
	{
		public BvaModuleThatDoesNotAddResourceDictionaries(IUnityContainer container) : base(container)
		{
			ThrowExceptionOnAddToMergedDictionary = false;
			ThrowExceptionOnRegisterAuditTrailDataAccess = false;
			ThrowExceptionOnRegisterPatientRepository = false;
			ThrowExceptionOnRegisterSampleRepository = false;
			ThrowExceptionOnRegisterTestInfoRepository = false;
			ThrowExceptionOnRegisterTestRepository = false;
		}

		public bool ThrowExceptionOnAddToMergedDictionary { get; set; }
		public bool ThrowExceptionOnRegisterAuditTrailDataAccess { get; set; }
		public bool ThrowExceptionOnRegisterPatientRepository { get; set; }
		public bool ThrowExceptionOnRegisterSampleRepository { get; set; }
		public bool ThrowExceptionOnRegisterTestInfoRepository { get; set; }
		public bool ThrowExceptionOnRegisterTestRepository { get; set; }

		protected override void AddResourceDictionaryToMergedDictionaries()
		{
			if (ThrowExceptionOnAddToMergedDictionary)
				throw new Exception();
		}

		protected override void RegisterAuditTrailDataAccess()
		{
			if (ThrowExceptionOnRegisterAuditTrailDataAccess)
				throw new Exception();
		}

		protected override void RegisterPatientRepository()
		{
			if (ThrowExceptionOnRegisterPatientRepository)
				throw new Exception();
		}

		protected override void RegisterSampleRepository()
		{
			if (ThrowExceptionOnRegisterSampleRepository)
				throw new Exception();
		}

		protected override void RegisterTestInfoRepository()
		{
			if (ThrowExceptionOnRegisterTestInfoRepository)
				throw new Exception();
		}

		protected override void RegisterTestRepository()
		{
			if (ThrowExceptionOnRegisterTestRepository)
				throw new Exception();
		}
	}
}
