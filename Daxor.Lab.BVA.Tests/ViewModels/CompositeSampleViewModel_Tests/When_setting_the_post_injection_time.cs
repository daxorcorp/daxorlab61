﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.ViewModels.CompositeSampleViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_post_injection_time
    {
        [TestMethod]
        public void Then_both_sample_A_and_sample_B_have_their_post_injection_times_set()
        {
            var sampleA = new BVASample(1, null) { Range = SampleRange.A, Type = SampleType.Sample1 };
            var sampleB = new BVASample(1, null) { Range = SampleRange.B, Type = SampleType.Sample1 };
            var compositeSample = new BVACompositeSample(null, sampleA, sampleB, null);
            
            var compositeSampleViewModel = new CompositeSampleViewModel(null, compositeSample, null, null, null)
                {
                    PostInjectionTimeInSeconds = "45"
                };

            Assert.AreEqual(45, compositeSampleViewModel.SampleA.PostInjectionTimeInSeconds);
            Assert.AreEqual(45, compositeSampleViewModel.SampleB.PostInjectionTimeInSeconds);
        }
    }
    // ReSharper restore InconsistentNaming
}
