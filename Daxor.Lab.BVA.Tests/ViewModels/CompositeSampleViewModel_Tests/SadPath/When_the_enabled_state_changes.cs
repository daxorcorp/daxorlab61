﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.ViewModels.CompositeSampleViewModel_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_enabled_state_changes
    {
        [TestMethod]
        public void And_the_composite_sample_is_null_then_no_exception_is_thrown()
        {
            var aSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.A);
            var bSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.B);
            var compositeSample = new BVACompositeSample(null, new List<BVASample> { aSideSample, bSideSample }, null) { IsEvaluationEnabled = false };
            // ReSharper disable UseObjectOrCollectionInitializer (not testing ctor, but the property)
            var viewModel = new SadPathCompositeSampleViewModel(null, compositeSample, false);
            // ReSharper restore UseObjectOrCollectionInitializer

            viewModel.SetCompositeSampleInstance(null);
            viewModel.IsEnabled = true;
        }
    }
    // ReSharper restore InconsistentNaming
}
