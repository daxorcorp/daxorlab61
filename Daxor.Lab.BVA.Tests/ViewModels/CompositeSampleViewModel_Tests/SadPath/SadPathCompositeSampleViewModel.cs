using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Infrastructure.Interfaces;

namespace Daxor.Lab.BVA.Tests.ViewModels.CompositeSampleViewModel_Tests.SadPath
{
    internal class SadPathCompositeSampleViewModel : CompositeSampleViewModel
    {
        internal SadPathCompositeSampleViewModel(IMeasuredCalcEngineService mCalcEngine, BVACompositeSample cSample, bool isEnabled = true) : 
            base(mCalcEngine, cSample, null, null, null, isEnabled)
        { }

        internal void SetCompositeSampleInstance(BVACompositeSample compositeSample)
        {
            CompositeSample = compositeSample;
        }
    }
}