﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests;
using Daxor.Lab.BVA.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.ViewModels.CompositeSampleViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_is_enabled_state_changes
    {
        [TestMethod]
        public void And_the_view_model_is_being_enabled_then_the_evaluation_for_the_composite_sample_is_also_enabled()
        {
            var aSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.A);
            var bSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.B);
            var compositeSample = new BVACompositeSample(null, new List<BVASample> { aSideSample, bSideSample }, null) {IsEvaluationEnabled = false};
            // ReSharper disable UseObjectOrCollectionInitializer (not testing ctor, but the property)
            var viewModel = new CompositeSampleViewModel(null, compositeSample, null, null, null, false);
            // ReSharper restore UseObjectOrCollectionInitializer

            viewModel.IsEnabled = true;

            Assert.IsTrue(compositeSample.IsEvaluationEnabled, "Evaluation should be enabled if the view model is enabled");
        }

        [TestMethod]
        public void And_the_view_model_is_being_disabled_then_the_evaluation_for_the_composite_sample_is_also_disabled()
        {
            var aSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.A);
            var bSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.B);
            var compositeSample = new BVACompositeSample(null, new List<BVASample> { aSideSample, bSideSample }, null) { IsEvaluationEnabled = true };
            // ReSharper disable UseObjectOrCollectionInitializer (not testing ctor, but the property)
            var viewModel = new CompositeSampleViewModel(null, compositeSample, null, null, null);
            // ReSharper restore UseObjectOrCollectionInitializer

            viewModel.IsEnabled = false;

            Assert.IsFalse(compositeSample.IsEvaluationEnabled, "Evaluation should be disabled if the view model is disabled");
        }
    }
    // ReSharper restore InconsistentNaming
}
