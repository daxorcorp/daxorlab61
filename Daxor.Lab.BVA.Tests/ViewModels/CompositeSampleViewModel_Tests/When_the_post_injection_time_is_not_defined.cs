﻿using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Practices.Composite.Logging;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.CompositeSampleViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_post_injection_time_is_not_defined
    {
        [TestMethod]
        [TestCategory("BVA")]
        [TestCategory("ViewModel")]
        public void Then_the_unadjusted_blood_volume_is_not_computed()
        {
            //
            // Arrange
            //
            var mockMeasuredCalcEngine = MockRepository.GenerateStrictMock<IMeasuredCalcEngineService>();
            var selectedTest = TestFactory.CreateBvaTest(3);  // 3-sample test
            selectedTest.Status = TestStatus.Completed;

            // Set up conditions that will allow the sample to even be considered for updating
            var sample1A = (from c in selectedTest.CompositeSamples
                            where c.SampleA.Type == SampleType.Sample1
                            select c).FirstOrDefault();
            if (sample1A == null) Assert.Fail("Could not get Sample 1A from the BVATest instance");
            sample1A.SampleA.Hematocrit = 0.45;
            sample1A.SampleB.Hematocrit = 0.45;
            selectedTest.StandardCompositeSample.SampleA.Counts = 100;
            selectedTest.StandardCompositeSample.SampleB.Counts = 100;
            
            // ReSharper disable UnusedVariable (not using because we're affecting a sample in the view model to trigger the change)
            var compositeSampleVm = new CompositeSampleViewModel(mockMeasuredCalcEngine, sample1A, null, new EmptyLogger(), null);
            // ReSharper restore UnusedVariable

            //
            // Act
            //
            sample1A.SampleA.Counts = 100;   // Make some change that forces the view model to re-evaluate itself

            //
            // Assert
            //
            // (GetOnlyUBVResult() should not be called)
            mockMeasuredCalcEngine.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("BVA")]
        [TestCategory("ViewModel")]
        public void And_theres_already_an_unadjusted_blood_volume_then_the_unadjusted_blood_volume_is_set_to_zero()
        {
            //
            // Arrange
            //
            var mockMeasuredCalcEngine = MockRepository.GenerateStrictMock<IMeasuredCalcEngineService>();
            mockMeasuredCalcEngine.Expect(e => e.GetOnlyUbvResult(0, 0, 0, 0, 0, null)).IgnoreArguments().Return(
                new UnadjustedBloodVolumeResult("hello", 123));
            var selectedTest = TestFactory.CreateBvaTest(3);  // 3-sample test
            selectedTest.Status = TestStatus.Completed;

            // Set up conditions that will allow the sample to even be considered for updating
            var sample1A = (from c in selectedTest.CompositeSamples
                            where c.SampleA.Type == SampleType.Sample1
                            select c).FirstOrDefault();
            if (sample1A == null) Assert.Fail("Could not get Sample 1A from the BVATest instance");
            sample1A.SampleA.Hematocrit = 0.45;
            sample1A.SampleB.Hematocrit = 0.45;
            sample1A.SampleA.PostInjectionTimeInSeconds = 600;  // Set the value once so a mock blood volume is computed
            selectedTest.StandardCompositeSample.SampleA.Counts = 100;
            selectedTest.StandardCompositeSample.SampleB.Counts = 100;

            // ReSharper disable UnusedVariable (not using because we're affecting a sample in the view model to trigger the change)
            var compositeSampleVm = new CompositeSampleViewModel(mockMeasuredCalcEngine, sample1A, null, new EmptyLogger(), null);
            // ReSharper restore UnusedVariable

            //
            // Act
            //
            sample1A.SampleA.Counts = 100;   // Make some change that forces the view model to re-evaluate itself
            sample1A.SampleA.PostInjectionTimeInSeconds = -1;   // Make another change that should set the blood volume to zero

            //
            // Assert
            //
            // (GetOnlyUBVResult() should not be called more than once)
            mockMeasuredCalcEngine.VerifyAllExpectations();
            Assert.AreEqual(0, sample1A.UnadjustedBloodVolume);
        }
    }
    // ReSharper restore InconsistentNaming
}
