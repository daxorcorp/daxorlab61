﻿using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests.Stubs;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests.SadPath
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_saving_a_test_on_the_background_thread_and_an_exception_is_thrown
	{
	    private const string ErrorMessage = "Failed while saving test. Message";

		private IAppModuleNavigator _mockNavigator;
		private IMessageBoxDispatcher _mockDispatcher;
		private IBloodVolumeTestController _mockTestController;
		private IPatientSpecificFieldCache _stubCache;
		private IEventAggregator _mockEventAggregator;
		private ILoggerFacade _mockLogger;
	    private IMessageManager _stubMessageManager;
	    private BusyStateChanged _stubBusyStateChanged;

	    private TestSetupViewModelThatThrowsExceptionOnSave _vm;

		[TestInitialize]
		public void Initialize()
		{
			_mockNavigator = Substitute.For<IAppModuleNavigator>();
			_mockDispatcher = Substitute.For<IMessageBoxDispatcher>();

            _stubBusyStateChanged = Substitute.For<BusyStateChanged>();
            
            _mockEventAggregator = Substitute.For<IEventAggregator>();
			_mockEventAggregator.GetEvent<TestSetupTabItemAdded>().Returns(new TestSetupTabItemAdded());
            _mockEventAggregator.GetEvent<BusyStateChanged>().Returns(_stubBusyStateChanged);
			
            _mockLogger = Substitute.For<ILoggerFacade>();
			
            _mockTestController = Substitute.For<IBloodVolumeTestController>();
			_mockTestController.EventAggregator.Returns(_mockEventAggregator);
			_mockTestController.Logger.Returns(_mockLogger);
			
            _stubCache = Substitute.For<IPatientSpecificFieldCache>();

            var stubMessage = Substitute.For<Message>("AKey", "Something", "Something", "Something", MessageType.Error);
		    stubMessage.FormattedMessage.Returns(ErrorMessage);
		    _stubMessageManager = Substitute.For<IMessageManager>();
		    _stubMessageManager.GetMessage(MessageKeys.BvaSaveTestFailed).Returns(stubMessage);

			_vm = new TestSetupViewModelThatThrowsExceptionOnSave(_mockNavigator, _mockDispatcher, _mockTestController, null, _stubCache, _stubMessageManager);
		}

		[TestMethod]
		public void Then_the_logger_logs_an_error_message()
		{
			_vm.NavigateToTestSelection.Execute(null);

            _mockLogger.Received(1).Log(ErrorMessage, Category.Exception, Priority.High);
		}

		[TestMethod]
		public void Then_a_messagebox_is_shown_to_the_user()
		{
			_vm.NavigateToTestSelection.Execute(null);

			_mockDispatcher.Received(1)
                .ShowMessageBox(MessageBoxCategory.Error, ErrorMessage,
					TestSetupViewModel.BvaTestCreationHeader, MessageBoxChoiceSet.Ok);
		}

	    [TestMethod]
	    public void Then_the_navigator_returns_to_the_test_selection_view()
	    {
            _vm.NavigateToTestSelection.Execute(null);

            _mockNavigator.Received().ActivateView(BVAModuleViewKeys.TestSelection);
	    }

        [TestMethod]
        public void Then_the_busy_indicator_is_disabled()
        {
            _vm.NavigateToTestSelection.Execute(null);

            _mockEventAggregator.Received().GetEvent<BusyStateChanged>();
            _stubBusyStateChanged.Received(1).Publish(Arg.Any<BusyPayload>());
        }
	}
	// ReSharper restore InconsistentNaming
}
