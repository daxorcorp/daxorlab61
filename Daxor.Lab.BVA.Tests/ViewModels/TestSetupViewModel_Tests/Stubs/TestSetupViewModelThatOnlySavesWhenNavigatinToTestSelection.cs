﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.TestHelpers;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests.Stubs
{
	public class TestSetupViewModelThatOnlySavesWhenNavigatinToTestSelection : TestSetupViewModel
	{

		public TestSetupViewModelThatOnlySavesWhenNavigatinToTestSelection(IAppModuleNavigator localNavigator, 
                                                                           IMessageBoxDispatcher messageBoxDispatcher, 
                                                                           IBloodVolumeTestController controller, 
                                                                           TestAnalysisViewModel analysisVm, 
                                                                           IPatientSpecificFieldCache patientSpecificFieldCache,
                                                                           IMessageManager messageManager) 
                                                                           : base(localNavigator, messageBoxDispatcher, controller, analysisVm, patientSpecificFieldCache, messageManager)
		{
            TaskScheduler = new CurrentThreadTaskScheduler();
		}


		protected override void ExecuteNavigateToTestSelection(object arg)
		{
			SaveTestOnBackgroundThread();
		}
	}
}
