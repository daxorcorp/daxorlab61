﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests.Stubs
{
    public class TestSetupViewModelThatDoesNotCloseKeyboardAndNavigate : TestSetupViewModel
    {
        public bool WasKeyboardClosed { get; private set; }

        public TestSetupViewModelThatDoesNotCloseKeyboardAndNavigate(IAppModuleNavigator localNavigator, IMessageBoxDispatcher messageBoxDispatcher, IBloodVolumeTestController controller, TestAnalysisViewModel analysisVm, IPatientSpecificFieldCache patientSpecificFieldCache) : base(localNavigator, messageBoxDispatcher, controller, analysisVm, patientSpecificFieldCache, null)
        {
            WasKeyboardClosed = false;
        }

        protected override void CloseKeyboard()
        {
            WasKeyboardClosed = true;
        }
    }
}
