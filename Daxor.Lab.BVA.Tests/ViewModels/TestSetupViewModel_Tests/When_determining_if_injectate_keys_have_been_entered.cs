using System;
using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests.Stubs;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_injectate_keys_have_been_entered
    {
        private ILoggerFacade _stubLogger;
        private IEventAggregator _stubEventAggregator;
        private IAppModuleNavigator _stubNavigator;
        private IMessageBoxDispatcher _stubMessageBoxDispatcher;
        private IPatientSpecificFieldCache _stubPatientSpecificFieldCache;
        private IBloodVolumeTestController _stubController;
        private BVAPatient _patient;
        private BVATest _test;
        private TestSetupViewModel _stubViewModel;

        [TestInitialize]
        public void TestInitialize()
        {
            _stubNavigator = MockRepository.GenerateStub<IAppModuleNavigator>();
            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();
            _stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            _stubPatientSpecificFieldCache = MockRepository.GenerateStub<IPatientSpecificFieldCache>();

            _patient = new BVAPatient(null);
            _test = new BVATest(null) { Patient = _patient };

            _stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            _stubEventAggregator.Expect(x => x.GetEvent<TestSetupTabItemAdded>()).Return(new TestSetupTabItemAdded());

            _stubController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            _stubController.Expect(x => x.SelectedTest).Return(_test);
            _stubController.Expect(x => x.Logger).Return(_stubLogger);
            _stubController.Expect(x => x.EventAggregator).Return(_stubEventAggregator);

            _stubViewModel = new TestSetupViewModelThatDoesNotCloseKeyboardAndNavigate(_stubNavigator,
                _stubMessageBoxDispatcher, _stubController, null, _stubPatientSpecificFieldCache);
        }

        [TestMethod]
        public void And_only_injectate_key_is_present_then_keys_have_been_entered()
        {
            _test.InjectateKey = "123";
            Assert.IsTrue(_stubViewModel.InjectateKeysHaveBeenEntered());
        }

        [TestMethod]
        public void And_only_the_standard_A_key_is_present_then_keys_have_been_entered()
        {
            _test.StandardAKey = "123";
            Assert.IsTrue(_stubViewModel.InjectateKeysHaveBeenEntered());
        }

        [TestMethod]
        public void And_only_the_standard_B_key_is_present_then_keys_have_been_entered()
        {
            _test.StandardBKey = "123";
            Assert.IsTrue(_stubViewModel.InjectateKeysHaveBeenEntered());
        }

        [TestMethod]
        public void And_the_injectate_key_and_the_standard_A_key_are_present_then_keys_have_been_entered()
        {
            _test.InjectateKey = "345";
            _test.StandardAKey = "123";
            Assert.IsTrue(_stubViewModel.InjectateKeysHaveBeenEntered());

        }

        [TestMethod]
        public void And_the_injectate_key_and_the_standard_B_key_are_present_then_keys_have_been_entered()
        {
            _test.InjectateKey = "123";
            _test.StandardBKey = "345";
            Assert.IsTrue(_stubViewModel.InjectateKeysHaveBeenEntered());
        }

        [TestMethod]
        public void And_both_standard_keys_are_present_then_keys_have_been_entered()
        {
            _test.StandardAKey = "123";
            _test.StandardBKey = "345";
            Assert.IsTrue(_stubViewModel.InjectateKeysHaveBeenEntered());
        }

        [TestMethod]
        public void And_all_keys_are_present_then_keys_have_been_entered()
        {
            _test.InjectateKey = "012";
            _test.StandardAKey = "123";
            _test.StandardBKey = "345";
            Assert.IsTrue(_stubViewModel.InjectateKeysHaveBeenEntered());
        }

        [TestMethod]
        public void And_all_keys_are_null_then_keys_have_not_been_entered()
        {
            Assert.IsFalse(_stubViewModel.InjectateKeysHaveBeenEntered());
        }

        [TestMethod]
        public void And_all_keys_are_empty_then_keys_have_not_been_entered()
        {
            _test.InjectateKey = String.Empty;
            _test.StandardAKey = String.Empty;
            _test.StandardBKey = String.Empty;
            Assert.IsFalse(_stubViewModel.InjectateKeysHaveBeenEntered());
        }
    }
    // ReSharper restore InconsistentNaming
}
