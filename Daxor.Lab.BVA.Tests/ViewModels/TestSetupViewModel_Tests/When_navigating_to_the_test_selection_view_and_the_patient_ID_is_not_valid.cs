using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests.Stubs;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_navigating_to_the_test_selection_view_and_the_patient_ID_is_not_valid
    {
        private ILoggerFacade _stubLogger;
        private IEventAggregator _stubEventAggregator;
        private IAppModuleNavigator _stubNavigator;
        private IMessageBoxDispatcher _stubMessageBoxDispatcher;
        private IPatientSpecificFieldCache _stubPatientSpecificFieldCache;
        private IBloodVolumeTestController _stubController;
        private BVAPatient _patient;
        private BVATest _test;

        [TestInitialize]
        public void TestInitialize()
        {
            _stubNavigator = MockRepository.GenerateStub<IAppModuleNavigator>();
            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();
            _stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            _stubPatientSpecificFieldCache = MockRepository.GenerateStub<IPatientSpecificFieldCache>();

            _patient = new BVAPatient(null);
            _test = new BVATest(null) {Patient = _patient};

            _stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            _stubEventAggregator.Expect(x => x.GetEvent<TestSetupTabItemAdded>()).Return(new TestSetupTabItemAdded());

            _stubController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            _stubController.Expect(x => x.SelectedTest).Return(_test);
            _stubController.Expect(x => x.Logger).Return(_stubLogger);
            _stubController.Expect(x => x.EventAggregator).Return(_stubEventAggregator);
        }

        [TestMethod]
        public void And_the_patient_id_is_empty_then_the_keyboard_is_closed()
        {
            _patient.HospitalPatientId = "";

            var stubViewModel = new TestSetupViewModelThatDoesNotCloseKeyboardAndNavigate(_stubNavigator,
                _stubMessageBoxDispatcher, _stubController, null, _stubPatientSpecificFieldCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.IsTrue(stubViewModel.WasKeyboardClosed);
        }

        [TestMethod]
        public void And_the_patient_id_is_empty_then_the_user_is_taken_to_the_test_selection_screen()
        {
            _patient.HospitalPatientId = "";

            var mockNavigator = MockRepository.GenerateMock<IAppModuleNavigator>();
            mockNavigator.Expect(x => x.ActivateView(BVAModuleViewKeys.TestSelection));

            var stubViewModel = new TestSetupViewModelThatDoesNotCloseKeyboardAndNavigate(mockNavigator,
                _stubMessageBoxDispatcher, _stubController, null, _stubPatientSpecificFieldCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            mockNavigator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_patient_id_is_empty_then_a_message_box_is_never_shown()
        {
            _patient.HospitalPatientId = "";

            var mockMessageBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMessageBoxDispatcher.Expect( x => x.ShowMessageBox(MessageBoxCategory.Error, null, null, MessageBoxChoiceSet.Close))
                                    .IgnoreArguments()
                                    .Return(MessageBoxReturnResult.Cancel)
                                    .Repeat
                                    .Never();
            
            var stubViewModel = new TestSetupViewModelThatDoesNotCloseKeyboardAndNavigate(_stubNavigator,
                mockMessageBoxDispatcher, _stubController, null, _stubPatientSpecificFieldCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            mockMessageBoxDispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_patient_id_is_null_then_the_keyboard_is_closed()
        {
            _patient.HospitalPatientId = null;

            var stubViewModel = new TestSetupViewModelThatDoesNotCloseKeyboardAndNavigate(_stubNavigator,
                _stubMessageBoxDispatcher, _stubController, null, _stubPatientSpecificFieldCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.IsTrue(stubViewModel.WasKeyboardClosed);
        }

        [TestMethod]
        public void And_the_patient_id_is_null_then_the_user_is_taken_to_the_test_selection_screen()
        {
            _patient.HospitalPatientId = null;

            var mockNavigator = MockRepository.GenerateMock<IAppModuleNavigator>();
            mockNavigator.Expect(x => x.ActivateView(BVAModuleViewKeys.TestSelection));

            var stubViewModel = new TestSetupViewModelThatDoesNotCloseKeyboardAndNavigate(mockNavigator,
                _stubMessageBoxDispatcher, _stubController, null, _stubPatientSpecificFieldCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            mockNavigator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_patient_id_is_null_then_a_message_box_is_never_shown()
        {
            _patient.HospitalPatientId = null;

            var mockMessageBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMessageBoxDispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Error, null, null, MessageBoxChoiceSet.Close))
                                    .IgnoreArguments()
                                    .Return(MessageBoxReturnResult.Cancel)
                                    .Repeat
                                    .Never();

            var stubViewModel = new TestSetupViewModelThatDoesNotCloseKeyboardAndNavigate(_stubNavigator,
                mockMessageBoxDispatcher, _stubController, null, _stubPatientSpecificFieldCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            mockMessageBoxDispatcher.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
