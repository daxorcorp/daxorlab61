using System;
using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests.Stubs;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_navigating_to_the_test_selection_view_and_no_keys_are_present_and_the_test_is_pending
    {
        private ILoggerFacade _stubLogger;
        private IEventAggregator _stubEventAggregator;
        private IAppModuleNavigator _stubNavigator;
        private IMessageBoxDispatcher _stubMessageBoxDispatcher;
        private IPatientSpecificFieldCache _stubPatientSpecificFieldCache;
        private IBloodVolumeTestController _stubController;
        private BVAPatient _patient;
        private BVATest _test;

        [TestInitialize]
        public void TestInitialize()
        {
            _stubNavigator = MockRepository.GenerateStub<IAppModuleNavigator>();
            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();

            _stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();

            _stubPatientSpecificFieldCache = MockRepository.GenerateStub<IPatientSpecificFieldCache>();

            _patient = new BVAPatient(null) { HospitalPatientId = "45" };
            _test = new BVATest(null) { Patient = _patient };

            _stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            _stubEventAggregator.Expect(x => x.GetEvent<TestSetupTabItemAdded>()).Return(new TestSetupTabItemAdded());
            _stubEventAggregator.Expect(x => x.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());

            _stubController = CreateStubController(_stubLogger, _stubEventAggregator, _test);
        }

        [TestMethod]
        public void Then_the_patient_cache_service_is_used_to_prompt_the_user_if_needed()
        {
            var mockPatientCache = MockRepository.GenerateMock<IPatientSpecificFieldCache>();
            mockPatientCache.Expect(x => x.RestoreOrOverrideBasedOnUserChoice(_patient, _patient));
            SetupMessageBoxDispatcher(_stubMessageBoxDispatcher, 0);

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, _stubMessageBoxDispatcher,
                _stubController, null, mockPatientCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            mockPatientCache.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_the_correct_message_is_shown_to_the_user_about_saving_the_test()
        {
            const string message = "Do you want to save your changes?";
            var choices = new[] { "Save", "Discard", "Cancel" };

            var mockMessageBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMessageBoxDispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Question, message,
                "BVA Test Creation", choices)).Return(1);

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, mockMessageBoxDispatcher,
                _stubController, null, _stubPatientSpecificFieldCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            mockMessageBoxDispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_user_chose_to_cancel_navigation_then_the_keyboard_is_still_active()
        {
            SetupMessageBoxDispatcher(_stubMessageBoxDispatcher, 2);

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, _stubMessageBoxDispatcher,
                _stubController, null, _stubPatientSpecificFieldCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.IsFalse(stubViewModel.WasKeyboardClosed);
        }

        [TestMethod]
        public void And_the_user_chose_to_discard_changes_and_the_DOB_is_invalid_then_the_DOB_is_set_to_null()
        {
            SetupMessageBoxDispatcher(_stubMessageBoxDispatcher, 1);

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, _stubMessageBoxDispatcher,
                _stubController, null, _stubPatientSpecificFieldCache);

            _patient.DateOfBirth = DomainConstants.InvalidDateOfBirth;

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.IsTrue(_patient.DateOfBirth == null);
        }

        [TestMethod]
        public void And_the_user_chose_to_discard_changes_and_the_DOB_is_valid_then_the_DOB_left_unchanged()
        {
            var DOB = DateTime.Now;

            SetupMessageBoxDispatcher(_stubMessageBoxDispatcher, 1);

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, _stubMessageBoxDispatcher,
                _stubController, null, _stubPatientSpecificFieldCache);

            _patient.DateOfBirth = DOB;

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.AreEqual(DOB, _patient.DateOfBirth);
        }

        [TestMethod]
        public void And_the_user_chose_to_discard_changes_then_the_keyboard_is_closed_and_the_user_is_navigated_to_the_test_selection_region()
        {
            var DOB = DateTime.Now;

            SetupMessageBoxDispatcher(_stubMessageBoxDispatcher, 1);

            var mockNavigator = MockRepository.GenerateMock<IAppModuleNavigator>();
            mockNavigator.Expect(x => x.ActivateView(BVAModuleViewKeys.TestSelection));

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(mockNavigator, _stubMessageBoxDispatcher,
                _stubController, null, _stubPatientSpecificFieldCache);

            _patient.DateOfBirth = DOB;

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.IsTrue(stubViewModel.WasKeyboardClosed);
            mockNavigator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_user_chose_to_save_changes_then_the_keyboard_is_closed()
        {
            var DOB = DateTime.Now;

            SetupMessageBoxDispatcher(_stubMessageBoxDispatcher, 0);

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, _stubMessageBoxDispatcher,
                _stubController, null, _stubPatientSpecificFieldCache);

            _patient.DateOfBirth = DOB;

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.IsTrue(stubViewModel.WasKeyboardClosed);
        }

        [TestMethod]
        public void And_the_user_chose_to_save_changes_and_the_DOB_is_invalid_then_the_DOB_is_set_to_null()
        {
            var DOB =DomainConstants.InvalidDateOfBirth;

            SetupMessageBoxDispatcher(_stubMessageBoxDispatcher, 0);

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, _stubMessageBoxDispatcher,
                _stubController, null, _stubPatientSpecificFieldCache);

            _patient.DateOfBirth = DOB;

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.IsTrue(_patient.DateOfBirth == null);
        }

        [TestMethod]
        public void And_the_user_chose_to_save_changes_and_the_DOB_is_valid_then_the_DOB_left_unchanged()
        {
            var DOB = DateTime.Now;

            SetupMessageBoxDispatcher(_stubMessageBoxDispatcher, 0);

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, _stubMessageBoxDispatcher,
                _stubController, null, _stubPatientSpecificFieldCache);

            _patient.DateOfBirth = DOB;

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.AreEqual(DOB, _patient.DateOfBirth);
        }

        [TestMethod]
        public void And_the_user_chose_to_save_changes_to_a_manual_test_then_the_test_is_marked_as_completed()
        {
            var DOB = DateTime.Now;
            _test.Mode = TestMode.Manual;
            _test.Status = TestStatus.Running;

            SetupMessageBoxDispatcher(_stubMessageBoxDispatcher, 0);

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, _stubMessageBoxDispatcher,
                _stubController, null, _stubPatientSpecificFieldCache);

            _patient.DateOfBirth = DOB;

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.AreEqual(TestStatus.Completed, _test.Status);
        }

        private IBloodVolumeTestController CreateStubController(ILoggerFacade logger, IEventAggregator eventAggregator, BVATest test)
        {
            var stubDataService = MockRepository.GenerateStub<IBVADataService>();
            stubDataService.Expect(x => x.SelectPatient(null, null)).IgnoreArguments().Return(_patient);

            var stubController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            stubController.Expect(x => x.SelectedTest).Return(test);
            stubController.Expect(x => x.Logger).Return(logger);
            stubController.Expect(x => x.EventAggregator).Return(eventAggregator);
            stubController.Expect(x => x.DataService).Return(stubDataService);
            return stubController;
        }

        private static void SetupMessageBoxDispatcher(IMessageBoxDispatcher dispatcher, int choiceIndex)
        {
            dispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Question, "", "", new[] { "something" })).IgnoreArguments().Return(choiceIndex);
        }
    }
    // ReSharper restore InconsistentNaming
}
