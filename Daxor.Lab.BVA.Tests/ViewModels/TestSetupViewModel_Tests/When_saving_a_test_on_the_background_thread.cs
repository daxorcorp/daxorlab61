﻿using System;
using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests.Stubs;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.MessageManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_saving_a_test_on_the_background_thread
	{
		private IAppModuleNavigator _mockNavigator;
		private IMessageBoxDispatcher _mockDispatcher;
		private IBloodVolumeTestController _mockTestController;
		private IPatientSpecificFieldCache _stubCache;
		private IEventAggregator _mockEventAggregator;
		private ILoggerFacade _mockLogger;
	    private IMessageManager _stubMessageManager;
	    private BusyStateChanged _stubBusyStateChanged;
	    private IBVADataService _stubBvaDataService;
	    private Guid _stubTestInternalId;
	    private Guid _stubPatientInternalId;
	    private BVATest _stubBvaTest;

		private TestSetupViewModelThatOnlySavesWhenNavigatinToTestSelection _vm;

		[TestInitialize]
		public void Initialize()
		{
            _stubTestInternalId = Guid.NewGuid();
            _stubPatientInternalId = Guid.NewGuid();

		    _stubMessageManager = Substitute.For<IMessageManager>();

			_mockNavigator = Substitute.For<IAppModuleNavigator>();
			
            _mockDispatcher = Substitute.For<IMessageBoxDispatcher>();

		    _stubBusyStateChanged = Substitute.For<BusyStateChanged>();
			
            _mockEventAggregator = Substitute.For<IEventAggregator>();
			_mockEventAggregator.GetEvent<TestSetupTabItemAdded>().Returns(new TestSetupTabItemAdded());
			_mockEventAggregator.GetEvent<BusyStateChanged>().Returns(_stubBusyStateChanged);
			
            _mockLogger = Substitute.For<ILoggerFacade>();

		    _stubBvaDataService = Substitute.For<IBVADataService>();

            _stubBvaTest = BvaTestFactory.CreateBvaTest(3,10);
		    
            _mockTestController = Substitute.For<IBloodVolumeTestController>();
			_mockTestController.EventAggregator.Returns(_mockEventAggregator);
			_mockTestController.Logger.Returns(_mockLogger);
		    _mockTestController.DataService.Returns(_stubBvaDataService);
		    _mockTestController.SelectedTest.Returns(_stubBvaTest);
			
            _stubCache = Substitute.For<IPatientSpecificFieldCache>();

			_vm = new TestSetupViewModelThatOnlySavesWhenNavigatinToTestSelection(_mockNavigator, _mockDispatcher, _mockTestController, null, _stubCache, _stubMessageManager);
		}

		[TestMethod]
		public void Then_the_selected_test_is_retrieved_from_the_data_service()
		{
            _stubBvaTest.InternalId = _stubTestInternalId;

			_vm.NavigateToTestSelection.Execute(null);

		    _stubBvaDataService.Received().SelectTest(_stubTestInternalId);
		}

	    [TestMethod]
	    public void Then_the_data_service_is_used_to_save_the_test()
	    {
            _vm.NavigateToTestSelection.Execute(null);

            _stubBvaDataService.Received().SaveTest(_stubBvaTest);
	    }

	    [TestMethod]
	    public void And_the_selected_tests_patient_is_the_same_as_the_patient_on_the_controllers_selected_test_then_orphaned_patients_are_not_removed()
	    {
            _stubBvaTest.InternalId = _stubTestInternalId;
	        _stubBvaTest.Patient.InternalId = _stubPatientInternalId;
	        _stubBvaDataService.SelectTest(_stubTestInternalId).Returns(_stubBvaTest);

            _vm.NavigateToTestSelection.Execute(null);

	        _stubBvaDataService.DidNotReceive().AreThereAnyTestsThatRanOnPatient(_stubPatientInternalId);
	    }

        [TestMethod]
        public void And_the_selected_tests_patient_is_different_than_the_patient_on_the_controllers_selected_test_and_there_are_other_tests_for_the_patient_then_the_patient_is_not_deleted()
        {
            _stubBvaTest.InternalId = _stubTestInternalId;
            _stubBvaTest.Patient.InternalId = _stubPatientInternalId;

            var differentSelectedTest = BvaTestFactory.CreateBvaTest(3, 10);
            differentSelectedTest.Patient.InternalId = Guid.NewGuid();

            _stubBvaDataService.SelectTest(_stubTestInternalId).Returns(differentSelectedTest);
            _stubBvaDataService.AreThereAnyTestsThatRanOnPatient(differentSelectedTest.Patient.InternalId)
                .Returns(true);

            _vm.NavigateToTestSelection.Execute(null);

            _stubBvaDataService.Received().AreThereAnyTestsThatRanOnPatient(differentSelectedTest.Patient.InternalId);
            _stubBvaDataService.DidNotReceive().DeletePatient(differentSelectedTest.Patient.InternalId);
        }

        [TestMethod]
        public void And_the_selected_tests_patient_is_different_than_the_patient_on_the_controllers_selected_test_and_there_are_not_other_tests_for_the_patient_then_the_patient_is_deleted()
        {
            _stubBvaTest.InternalId = _stubTestInternalId;
            _stubBvaTest.Patient.InternalId = _stubPatientInternalId;

            var differentSelectedTest = BvaTestFactory.CreateBvaTest(3, 10);
            differentSelectedTest.Patient.InternalId = Guid.NewGuid();

            _stubBvaDataService.SelectTest(_stubTestInternalId).Returns(differentSelectedTest);
            _stubBvaDataService.AreThereAnyTestsThatRanOnPatient(differentSelectedTest.Patient.InternalId)
                .Returns(false);

            _vm.NavigateToTestSelection.Execute(null);

            _stubBvaDataService.Received().AreThereAnyTestsThatRanOnPatient(differentSelectedTest.Patient.InternalId);
            _stubBvaDataService.Received().DeletePatient(differentSelectedTest.Patient.InternalId);
        }

		[TestMethod]
		public void And_the_test_is_saved_then_no_message_is_shown()
		{
			_vm.NavigateToTestSelection.Execute(null);

			_mockDispatcher.DidNotReceive()
				.ShowMessageBox(MessageBoxCategory.Error, Arg.Any<string>(), TestSetupViewModel.BvaTestCreationHeader,
					MessageBoxChoiceSet.Ok);
		}

		[TestMethod]
		public void Then_the_test_selection_view_is_activated()
		{
			_vm.NavigateToTestSelection.Execute(null);

			_mockNavigator.Received(1).ActivateView(BVAModuleViewKeys.TestSelection);
		}

		[TestMethod]
		public void Then_a_busy_state_changed_event_is_published()
		{
			_vm.NavigateToTestSelection.Execute(null);

			_mockEventAggregator.Received(1).GetEvent<BusyStateChanged>();
            _stubBusyStateChanged.Received(1).Publish(Arg.Any<BusyPayload>());
		}
	}
	// ReSharper restore InconsistentNaming
}
