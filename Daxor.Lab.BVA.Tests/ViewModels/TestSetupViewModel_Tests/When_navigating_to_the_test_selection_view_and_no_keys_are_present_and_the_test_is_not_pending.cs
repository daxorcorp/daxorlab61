using System;
using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests.Stubs;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_navigating_to_the_test_selection_view_and_no_keys_are_present_and_the_test_is_not_pending
    {
        private ILoggerFacade _stubLogger;
        private IEventAggregator _stubEventAggregator;
        private IAppModuleNavigator _stubNavigator;
        private IMessageBoxDispatcher _stubMessageBoxDispatcher;
        private IPatientSpecificFieldCache _stubPatientSpecificFieldCache;
        private IBloodVolumeTestController _stubController;
        private BVAPatient _patient;
        private BVATest _test;

        [TestInitialize]
        public void TestInitialize()
        {
            _stubNavigator = MockRepository.GenerateStub<IAppModuleNavigator>();
            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();

            _stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();

            _stubPatientSpecificFieldCache = MockRepository.GenerateStub<IPatientSpecificFieldCache>();

            _patient = new BVAPatient(null) { HospitalPatientId = "45" };
            _test = new BVATest(null) { Patient = _patient, Status = TestStatus.Completed};

            _stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            _stubEventAggregator.Expect(x => x.GetEvent<TestSetupTabItemAdded>()).Return(new TestSetupTabItemAdded());
            _stubEventAggregator.Expect(x => x.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());

            _stubController = CreateStubController(_stubLogger, _stubEventAggregator, _test);
        }

        [TestMethod]
        public void Then_the_patient_cache_service_is_used_to_prompt_the_user_if_needed()
        {
            var mockPatientCache = MockRepository.GenerateMock<IPatientSpecificFieldCache>();
            mockPatientCache.Expect(x => x.RestoreOrOverrideBasedOnUserChoice(_patient, _patient));

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, _stubMessageBoxDispatcher,
                _stubController, null, mockPatientCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            mockPatientCache.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_the_keyboard_is_closed()
        {
            var DOB = DateTime.Now;

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, _stubMessageBoxDispatcher,
                _stubController, null, _stubPatientSpecificFieldCache);

            _patient.DateOfBirth = DOB;

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.IsTrue(stubViewModel.WasKeyboardClosed);
        }

        [TestMethod]
        public void And_the_DOB_is_invalid_then_the_DOB_is_set_to_null()
        {
            var DOB = DomainConstants.InvalidDateOfBirth;

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, _stubMessageBoxDispatcher,
                _stubController, null, _stubPatientSpecificFieldCache);

            _patient.DateOfBirth = DOB;

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.IsTrue(_patient.DateOfBirth == null);
        }

        [TestMethod]
        public void And_the_DOB_is_valid_then_the_DOB_left_unchanged()
        {
            var DOB = DateTime.Now;

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, _stubMessageBoxDispatcher,
                _stubController, null, _stubPatientSpecificFieldCache);

            _patient.DateOfBirth = DOB;

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.AreEqual(DOB, _patient.DateOfBirth);
        }

        [TestMethod]
        public void And_the_test_is_manual_then_the_test_is_marked_as_completed()
        {
            var DOB = DateTime.Now;
            _test.Mode = TestMode.Manual;
            _test.Status = TestStatus.Running;

            var stubViewModel = new TestSetupViewModelThatDoesNotHaveKeys(_stubNavigator, _stubMessageBoxDispatcher,
                _stubController, null, _stubPatientSpecificFieldCache);

            _patient.DateOfBirth = DOB;

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.AreEqual(TestStatus.Completed, _test.Status);
        }

        private IBloodVolumeTestController CreateStubController(ILoggerFacade logger, IEventAggregator eventAggregator, BVATest test)
        {
            var stubDataService = MockRepository.GenerateStub<IBVADataService>();
            stubDataService.Expect(x => x.SelectPatient(null, null)).IgnoreArguments().Return(_patient);

            var stubController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            stubController.Expect(x => x.SelectedTest).Return(test);
            stubController.Expect(x => x.Logger).Return(logger);
            stubController.Expect(x => x.EventAggregator).Return(eventAggregator);
            stubController.Expect(x => x.DataService).Return(stubDataService);
            return stubController;
        }
    }
    // ReSharper restore InconsistentNaming
}
