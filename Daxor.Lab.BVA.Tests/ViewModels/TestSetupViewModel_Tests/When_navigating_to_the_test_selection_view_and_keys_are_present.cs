using System;
using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests.Stubs;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSetupViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_navigating_to_the_test_selection_view_and_keys_are_present
    {
        private ILoggerFacade _stubLogger;
        private IEventAggregator _stubEventAggregator;
        private IAppModuleNavigator _stubNavigator;
        private IMessageBoxDispatcher _stubMessageBoxDispatcher;
        private IPatientSpecificFieldCache _stubPatientSpecificFieldCache;
        private IBloodVolumeTestController _stubController;
        private BVAPatient _patient;
        private BVATest _test;

        [TestInitialize]
        public void TestInitialize()
        {
            _stubNavigator = MockRepository.GenerateStub<IAppModuleNavigator>();
            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();

            _stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            SetupMessageBoxDispatcher(_stubMessageBoxDispatcher);

            _stubPatientSpecificFieldCache = MockRepository.GenerateStub<IPatientSpecificFieldCache>();

            _patient = new BVAPatient(null) {HospitalPatientId = "45"};
            _test = new BVATest(null) { Patient = _patient };

            _stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            _stubEventAggregator.Expect(x => x.GetEvent<TestSetupTabItemAdded>()).Return(new TestSetupTabItemAdded());
            _stubEventAggregator.Expect(x => x.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());

            _stubController = CreateStubController(_stubLogger, _stubEventAggregator, _test);
        }

        [TestMethod]
        public void Then_the_correct_message_is_shown_to_the_user_about_keys_being_cleared()
        {
            const string InjectateLot = "123ABC";
            _test.InjectateLot = InjectateLot;

            var mockMessageBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();

            var stubViewModel = new TestSetupViewModelWithInjectateKeys(_stubNavigator,
                mockMessageBoxDispatcher, _stubController, null, _stubPatientSpecificFieldCache);

            mockMessageBoxDispatcher.Expect(
                d => d.ShowMessageBox(MessageBoxCategory.Question, String.Format(TestSetupViewModel.EnteredKeysFormatString, InjectateLot),
                    stubViewModel.DisplayName,
                        new[] {TestSetupViewModel.ClearKeysAndSaveChoice, TestSetupViewModel.CancelChoice})).Return(1);

            stubViewModel.NavigateToTestSelection.Execute(null);

            mockMessageBoxDispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_user_chose_to_clear_keys_and_the_test_is_manual_then_the_test_is_marked_as_completed()
        {
            _test.Mode = TestMode.Manual;
            _test.Status = TestStatus.Undefined;

            var stubViewModel = new TestSetupViewModelWithInjectateKeys(_stubNavigator,
                _stubMessageBoxDispatcher, _stubController, null, _stubPatientSpecificFieldCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.IsTrue(_test.Status == TestStatus.Completed);
        }

        [TestMethod]
        public void And_the_user_chose_to_clear_keys_then_the_busy_indicator_is_activated_before_saving()
        {
            var mockEventAggregator = MockRepository.GenerateMock<IEventAggregator>();
            mockEventAggregator.Expect(x => x.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());
            mockEventAggregator.Expect(x => x.GetEvent<TestSetupTabItemAdded>()).Return(new TestSetupTabItemAdded());

            var stubController = CreateStubController(_stubLogger, mockEventAggregator, _test);

            var stubViewModel = new TestSetupViewModelWithInjectateKeys(_stubNavigator,
                _stubMessageBoxDispatcher, stubController, null, _stubPatientSpecificFieldCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            mockEventAggregator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_user_chose_to_clear_keys_then_the_keys_are_cleared_from_the_test_before_saving()
        {
            _test.InjectateLot = "123";
            _test.InjectateKey = "234";
            _test.StandardAKey = "345";
            _test.StandardBKey = "456";

            var stubViewModel = new TestSetupViewModelWithInjectateKeys(_stubNavigator,
                _stubMessageBoxDispatcher, _stubController, null, _stubPatientSpecificFieldCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.IsTrue(_test.InjectateKey == string.Empty);
            Assert.IsTrue(_test.InjectateLot == string.Empty);
            Assert.IsTrue(_test.StandardAKey == string.Empty);
            Assert.IsTrue(_test.StandardBKey == string.Empty);
        }

        [TestMethod]
        public void And_the_user_chose_to_clear_keys_then_the_virtual_keyboard_is_closed()
        {
            var stubViewModel = new TestSetupViewModelWithInjectateKeys(_stubNavigator,
                _stubMessageBoxDispatcher, _stubController, null, _stubPatientSpecificFieldCache);

            stubViewModel.NavigateToTestSelection.Execute(null);

            Assert.IsTrue(stubViewModel.WasKeyboardClosed);
        }

        private IBloodVolumeTestController CreateStubController(ILoggerFacade logger, IEventAggregator eventAggregator, BVATest test)
        {
            var stub = MockRepository.GenerateStub<IBloodVolumeTestController>();
            stub.Expect(x => x.SelectedTest).Return(test);
            stub.Expect(x => x.Logger).Return(logger);
            stub.Expect(x => x.EventAggregator).Return(eventAggregator);
            return stub;
        }

        private void SetupMessageBoxDispatcher(IMessageBoxDispatcher dispatcher)
        {
            dispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Question, "", "", new[] { "something" })).IgnoreArguments().Return(0);
        }
    }
    // ReSharper restore InconsistentNaming
}
