﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Tests.ViewModels.PatientDemographicsVM_Tests.Stubs;
using Daxor.Lab.Domain.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.PatientDemographicsVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_BVATestSelected_event_is_published
    {
        [TestMethod]
        [TestCategory("BVA")]
        [TestCategory("ViewModel")]
        public void Then_the_selected_test_is_that_events_payload()
        {
            //
            // Arrange
            //
            var eventAggregator = new EventAggregator();
            var stubController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            stubController.Stub(c => c.EventAggregator).Return(eventAggregator);

            var stubRuleEngine = MockRepository.GenerateStub<IRuleEngine>();
            stubRuleEngine.Expect(x => x.Rules).Return(new List<IRule>());

            var viewModel = new PatientDemographicsViewModelThatSubscribesToBvaTestSelectedEvent(stubController, stubRuleEngine);


            //
            // Act
            //
            var selectedTest = new BVATest(null) {Patient = new BVAPatient(null)};
            eventAggregator.GetEvent<BvaTestSelected>().Publish(selectedTest);


            //
            // Assert
            //
            var currentTest = viewModel.CurrentTest;
            Assert.AreEqual(currentTest.GetHashCode(), selectedTest.GetHashCode(),
                "Current test doesn't match the selected test.");
        }
    }
    // ReSharper restore InconsistentNaming
}
