using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.BVA.Tests.ViewModels.PatientDemographicsVM_Tests.Stubs;
using Daxor.Lab.Domain.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.PatientDemographicsVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_whether_patient_specific_data_is_locked
    {
        private const string hospitalPatientIdForRunningTest = "something";

        [TestMethod]
        public void And_there_is_no_running_BVA_test_then_patient_data_is_not_locked()
        {
            var stubExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubExecutionController.Expect(x => x.IsExecuting).Return(false);

            var patientDemographicsViewModel = new PatientDemographicsViewModelThatDoesNotUseBaseInitialize(stubExecutionController);

            Assert.IsFalse(patientDemographicsViewModel.IsPatientSpecificDataLocked);
        }

        [TestMethod]
        public void And_the_patient_for_the_running_test_matches_the_patient_for_the_current_test_then_the_data_is_locked()
        {
            var runningTest = BvaTestFactory.CreateBvaTest(3, 120);
            runningTest.Patient.HospitalPatientId = hospitalPatientIdForRunningTest;

            var pendingTestWithSamePatient = BvaTestFactory.CreateBvaTest(3, 120);
            pendingTestWithSamePatient.Patient.HospitalPatientId = hospitalPatientIdForRunningTest;

            var stubRuleEngine = MockRepository.GenerateStub<IRuleEngine>();
            stubRuleEngine.Stub(x => x.Rules).Return(new List<IRule>());

            var patientDemographicsViewModel = new PatientDemographicsViewModelThatDoesNotUseBaseInitialize(CreateStubExecutionControllerWithRunningTest(runningTest), 
                                                                                                            CreateStubTestControllerWithDataService(), stubRuleEngine)
            {
                CurrentTest = pendingTestWithSamePatient,
            };
            
            Assert.IsTrue(patientDemographicsViewModel.IsPatientSpecificDataLocked);
        }

        [TestMethod]
        public void And_the_patient_for_the_running_test_is_different_than_the_patient_for_the_current_test_then_the_data_is_not_locked()
        {
            var runningTest = BvaTestFactory.CreateBvaTest(3, 120);
            runningTest.Patient.HospitalPatientId = hospitalPatientIdForRunningTest;

            var pendingTestWithDifferentHospitalPatientId = BvaTestFactory.CreateBvaTest(3, 120);
            pendingTestWithDifferentHospitalPatientId.Patient.HospitalPatientId = hospitalPatientIdForRunningTest +"Different";

            var stubRuleEngine = MockRepository.GenerateStub<IRuleEngine>();
            stubRuleEngine.Stub(x => x.Rules).Return(new List<IRule>());

            var patientDemographicsViewModel = new PatientDemographicsViewModelThatDoesNotUseBaseInitialize(CreateStubExecutionControllerWithRunningTest(runningTest),
                                                                                                            CreateStubTestControllerWithDataService(), stubRuleEngine)
            {
                CurrentTest = pendingTestWithDifferentHospitalPatientId
            };

            Assert.IsFalse(patientDemographicsViewModel.IsPatientSpecificDataLocked);
        }

        [TestMethod]
        public void And_the_running_test_is_not_a_BVA_test_then_the_data_is_not_locked()
        {
            var stubExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubExecutionController.Expect(x => x.IsExecuting).Return(true);
            stubExecutionController.Expect(x => x.GetExecutingTestType()).Return(typeof (string));

            var patientDemographicsViewModel =
                new PatientDemographicsViewModelThatDoesNotUseBaseInitialize(stubExecutionController);

            Assert.IsFalse(patientDemographicsViewModel.IsPatientSpecificDataLocked);
        }

        private static IBVADataService CreateBvaDataServiceWithEmptyHospitalPatientIdItemList()
        {
            var dataService = MockRepository.GenerateStub<IBVADataService>();
            dataService.Expect(x => x.FindHospitalPatientIdItems(null))
                .IgnoreArguments()
                .Return(new List<HospitalPatientIdItem>().AsQueryable());
            return dataService;
        }

        private static ITestExecutionController CreateStubExecutionControllerWithRunningTest(ITest test)
        {
            var stubExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubExecutionController.Expect(x => x.IsExecuting).Return(true);
            stubExecutionController.Expect(x => x.ExecutingTest).Return(test);
            stubExecutionController.Expect(x => x.GetExecutingTestType()).Return(typeof(BVATest));
            return stubExecutionController;
        }
        
        private static IBloodVolumeTestController CreateStubTestControllerWithDataService()
        {
            var stubTestController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            stubTestController.Expect(x => x.DataService).Return(CreateBvaDataServiceWithEmptyHospitalPatientIdItemList());
            return stubTestController;
        }
    }
    // ReSharper restore InconsistentNaming
}
