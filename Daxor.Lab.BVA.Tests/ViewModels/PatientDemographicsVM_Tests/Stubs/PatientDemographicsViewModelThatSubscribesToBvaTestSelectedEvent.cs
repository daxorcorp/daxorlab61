﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.BVA.Tests.ViewModels.PatientDemographicsVM_Tests.Stubs
{
    internal class PatientDemographicsViewModelThatSubscribesToBvaTestSelectedEvent : PatientDemographicsViewModel
    {
        public PatientDemographicsViewModelThatSubscribesToBvaTestSelectedEvent(IBloodVolumeTestController controller, IRuleEngine ruleEngine)
            : base(controller, null,
                ruleEngine, null, null, null, null, null, null)
        {
        }

        protected override void InitializeViewModel()
        {
            SubscribeToBvaTestSelectedEvent();
        }

        protected override void FirePropertyChanged(string propertyName)
        {
            // Do nothing
        }

        protected override void SetCurrentPatientBasedOnTest(BVATest test)
        {
            // Do nothing
        }

        protected override void InitializeTestPropertyObserver(BVATest test)
        {
            // Do nothing
        }

        protected override void SetHospitalPatientIdBasedOnCurrentTest()
        {
            // Do nothing
        }
    }
}