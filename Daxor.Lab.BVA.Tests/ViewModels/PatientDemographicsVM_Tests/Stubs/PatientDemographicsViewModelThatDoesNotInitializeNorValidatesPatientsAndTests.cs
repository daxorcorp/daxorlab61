﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.PatientDemographicsVM_Tests.Stubs
{
	public class PatientDemographicsViewModelThatDoesNotInitializeNorValidatesPatientsAndTests : PatientDemographicsViewModel
	{
		private BVATest _test;

		public PatientDemographicsViewModelThatDoesNotInitializeNorValidatesPatientsAndTests()
			: base(Substitute.For<IBloodVolumeTestController>(), null, null, null, null, null, null, null, null)
		{
		}

		protected override void InitializeViewModel()
		{
		}

		public override BVAPatient CurrentPatient { get; set; }

		public override BVATest CurrentTest
		{
			get { return _test; }
			set
			{
				_test = value;
				CurrentPatient = _test.Patient;
			}
		}
	}
}
