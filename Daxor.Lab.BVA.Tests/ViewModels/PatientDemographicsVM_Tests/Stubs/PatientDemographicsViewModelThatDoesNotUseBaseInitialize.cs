using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Presentation.Commands;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.PatientDemographicsVM_Tests.Stubs
{
    internal class PatientDemographicsViewModelThatDoesNotUseBaseInitialize : PatientDemographicsViewModel
    {
        internal PatientDemographicsViewModelThatDoesNotUseBaseInitialize(ITestExecutionController testExecutionController)
            : base(MockRepository.GenerateStub<IBloodVolumeTestController>(),
                null, null, null, testExecutionController, null, null, null, null)
        {
        }

        public PatientDemographicsViewModelThatDoesNotUseBaseInitialize(ITestExecutionController testExecutionController, IBloodVolumeTestController testController, IRuleEngine ruleEngine)
            : base(testController,
                MockRepository.GenerateStub<IAppModuleNavigator>(), 
                ruleEngine, MockRepository.GenerateStub<IMessageBoxDispatcher>(), testExecutionController, MockRepository.GenerateStub<IIdealsCalcEngineService>(), MockRepository.GenerateStub<IPatientSpecificFieldCache>(), MockRepository.GenerateStub<ISettingsManager>(), null)
        {
        }

        protected override void InitializeViewModel()
        {
            NavigateToTestParametersDelegateCommand = new DelegateCommand<object>(o => { });
            HospitalPatientIdItemList = new ThreadSafeObservableCollection<HospitalPatientIdItem>();
        }
    }
}