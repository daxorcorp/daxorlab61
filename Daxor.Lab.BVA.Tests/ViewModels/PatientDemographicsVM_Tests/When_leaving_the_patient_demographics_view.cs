using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Tests.ViewModels.PatientDemographicsVM_Tests.Stubs;
using Daxor.Lab.Domain.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.PatientDemographicsVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_leaving_the_patient_demographics_view
    {
        [TestMethod]
        public void Then_the_patient_specific_field_cache_is_used_to_determine_if_user_intervention_is_required()
        {
            var selectedPatient = new BVAPatient(Guid.NewGuid(), null);
            var currentPatient = new BVAPatient(Guid.NewGuid(), null);
            var currentTest = new BVATest(null);
            
            var bvaTestController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            bvaTestController.Expect(c => c.Logger).Return(MockRepository.GenerateStub<ILoggerFacade>());
            var bvaDataService = MockRepository.GenerateStub<IBVADataService>();
            bvaTestController.Expect(c => c.DataService).Return(bvaDataService);
            bvaTestController.Expect(c => c.SelectedTest).Return(currentTest);
            bvaDataService.Expect(s => s.SelectPatient("whatever", null)).IgnoreArguments().Return(selectedPatient);
            
            var dummyController = MockRepository.GenerateStub<ITestExecutionController>();
            var mockPatientSpecificFieldCache = MockRepository.GenerateMock<IPatientSpecificFieldCache>();
            mockPatientSpecificFieldCache.Expect(c => c.RestoreOrOverrideBasedOnUserChoice(selectedPatient, currentPatient));
            
            // ReSharper disable once UnusedVariable
            var patientDemographicsViewModel = new PatientDemographicsViewModelThatSkipsPatientSetup(dummyController, bvaTestController, mockPatientSpecificFieldCache)
            {
                CurrentPatient = currentPatient,
                IsActive = false
            };

            mockPatientSpecificFieldCache.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
