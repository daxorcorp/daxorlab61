﻿using System.Collections.Generic;
using System.Globalization;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.ViewModels;

namespace Daxor.Lab.BVA.Tests.ViewModels
{
    internal static class BloodPointViewModelFactory
    {
        internal static IEnumerable<UnadjustedCompositeBloodPointViewModel> CreateBloodVolumePointViewModels(int numberOfBloodVolumePoints)
        {
            var viewModels = new List<UnadjustedCompositeBloodPointViewModel>();
            for (var i = 1; i <= numberOfBloodVolumePoints; i++)
                viewModels.Add(new UnadjustedCompositeBloodPointViewModel(
                                   CreateCompositeSample(SampleType.Sample1 + i - 1), i.ToString(CultureInfo.InvariantCulture)));

            return viewModels;
        }

        private static BVACompositeSample CreateCompositeSample(SampleType sampleType)
        {
            return new BVACompositeSample(null,
                                          new BVASample(1, null) { Range = SampleRange.A, Type = sampleType },
                                          new BVASample(2, null) { Range = SampleRange.B, Type = sampleType },
                                          null) { UnadjustedBloodVolume = 10 };
        }
    }
}