﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSampleValuesVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_BVATestSelected_event_is_raised
    {
        [TestMethod]
        [TestCategory("BVA")]
        [TestCategory("ViewModel")]
        public void Then_the_selected_test_is_that_events_payload()
        {
            //
            // Arrange
            //
            var eventAggregator = new EventAggregator();
            var viewModel = new TestSampleValuesVmThatSubscribesToBvaTestSelectedEvent(eventAggregator);

            
            //
            // Act
            //
            var selectedTest = new BVATest(null) {Patient = new BVAPatient(null)};
            eventAggregator.GetEvent<BvaTestSelected>().Publish(selectedTest);


            //
            // Assert
            //
            var currentTest = viewModel.CurrentTest;
            Assert.AreEqual(currentTest.GetHashCode(), selectedTest.GetHashCode(),
                "Current test doesn't match the selected test.");
        }

        [TestMethod]
        [TestCategory("BVA")]
        [TestCategory("ViewModel")]
        public void And_ideal_volumes_can_be_computed_then_the_ideal_volumes_are_computed()
        {
            //
            // Arrange
            //
            const int expectedPlasmaVolume = 100;
            const int expectedRedCellVolume = 200;
            const double expectedWeightDeviation = 0.1;
            var stubIdealsCalcEngineService = MockRepository.GenerateStub<IIdealsCalcEngineService>();
            stubIdealsCalcEngineService.Expect(s => s.GetIdeals(0, 0, 0)).IgnoreArguments().Return(
                new IdealsCalcEngineResults(300, expectedPlasmaVolume, expectedRedCellVolume, expectedWeightDeviation));
            
            var eventAggregator = new EventAggregator();
            var viewModel = new TestSampleValuesVmThatSubscribesToBvaTestSelectedEvent(eventAggregator, stubIdealsCalcEngineService);


            //
            // Act
            //
            var selectedTest = new BVATest(null) { Patient = new BVAPatient(null) };
            eventAggregator.GetEvent<BvaTestSelected>().Publish(selectedTest);


            //
            // Assert
            //
            Assert.AreEqual(expectedWeightDeviation, viewModel.CurrentTest.Patient.DeviationFromIdealWeight,
                            "Deviation from ideal weight is not as expected");
            Assert.AreEqual(expectedPlasmaVolume, viewModel.CurrentTest.Volumes[BloodType.Ideal].PlasmaCount,
                            "Ideal plasma volume is not as expected");
            Assert.AreEqual(expectedRedCellVolume, viewModel.CurrentTest.Volumes[BloodType.Ideal].RedCellCount,
                            "Ideal plasma volume is not as expected");
        }
    }
    // ReSharper restore InconsistentNaming

    internal class TestSampleValuesVmThatSubscribesToBvaTestSelectedEvent : BVA.ViewModels.TestSampleValuesViewModel
    {
        private bool TryToComputeIdeals { get; set; }

        public TestSampleValuesVmThatSubscribesToBvaTestSelectedEvent(IEventAggregator eventAggregator)
            : base(null, null, eventAggregator, null, null, null, null, null, null, null)
        {
            TryToComputeIdeals = false;
        }

        public TestSampleValuesVmThatSubscribesToBvaTestSelectedEvent(IEventAggregator eventAggregator, IIdealsCalcEngineService calcEngine)
            : base(null, null, eventAggregator, null, calcEngine, null, null, null, null, null)
        {
            TryToComputeIdeals = true;
        }

        protected override void InitializeViewModel()
        {
            SubscribeToBvaTestSelectedEvent();
            InitializeCommands();
        }

        protected override void InitializeTestPropertyObserver(BVATest test)
        {
            // Do nothing
        }

        protected override void FirePropertyChanged(string propertyName)
        {
            // Do nothing
        }

        protected override void TryUpdateIdealVolumes()
        {
            if (TryToComputeIdeals)
                base.TryUpdateIdealVolumes();
        }
    }
}
