﻿using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSampleValuesVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_post_injection_time_is_not_defined
    {
        [TestMethod]
        [TestCategory("BVA")]
        [TestCategory("ViewModel")]
        public void Then_the_unadjusted_blood_volume_is_not_computed()
        {
            //
            // Arrange
            //
            var mockMeasuredCalcEngine = MockRepository.GenerateMock<IMeasuredCalcEngineService>();
            // Any time GetAllResults() is called in the manner we don't expect, return a non-null result so UpdateMeasuredVolumes() can continue
            mockMeasuredCalcEngine.Stub(e => e.GetAllResults(Arg<MeasuredCalcEngineParams>.Matches(a => a.Points.Length != 1))).Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, 0, 0));
            // We expect to only have one point to compute; we have to return a non-null result to UpdateMeasuredVolumes() can continue
            mockMeasuredCalcEngine.Expect(e => e.GetAllResults(Arg<MeasuredCalcEngineParams>.Matches(a => a.Points.Length == 1))).Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, 0, 0));
            var viewModel = new TestSampleValuesVmForTestingEmptyPostInjectionTimes(mockMeasuredCalcEngine);
            
            var selectedTest = TestFactory.CreateBvaTest(3);  // 3-sample test
            selectedTest.Status = TestStatus.Completed;
            selectedTest.Patient = new BVAPatient(null);
            viewModel.CurrentTest = selectedTest;

            // Find one sample, and give it a positive post-injection time
            var sample1A = (from c in selectedTest.CompositeSamples
                            where c.SampleA.Type == SampleType.Sample1
                            select c.SampleA).FirstOrDefault();
            if (sample1A == null) Assert.Fail("Could not get Sample 1A from the BVATest instance");
            sample1A.PostInjectionTimeInSeconds = 600;


            //
            // Act
            //
            viewModel.UpdateMeasuredVolumes();

            //
            // Assert
            //
            mockMeasuredCalcEngine.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming

    internal class TestSampleValuesVmForTestingEmptyPostInjectionTimes : TestSampleValuesViewModel
    {
        public TestSampleValuesVmForTestingEmptyPostInjectionTimes(IMeasuredCalcEngineService measuredCalcEngineService)
            : base(null, null, null, null, null, measuredCalcEngineService, null, null, null, null)
        {
        }

        public void UpdateMeasuredVolumes()
        {
            TryUpdateMeasuredVolumes();
        }

        protected override void InitializeViewModel()
        {
            InitializeCommands();
        }

        protected override void InitializeTestPropertyObserver(BVATest test)
        {
            // Do nothing
        }

        protected override void FirePropertyChanged(string propertyName)
        {
            // Do nothing
        }

        protected override void TryUpdateIdealVolumes()
        {
            // Do nothing
        }
    }
}
