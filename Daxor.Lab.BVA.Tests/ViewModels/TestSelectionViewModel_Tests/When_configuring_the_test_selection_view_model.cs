﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.Stubs;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_configuring_the_test_selection_view_model
    {
        private ObservableCollection<BVATestItem> BvaTestList;

        private ILoggerFacade _stubLogger;
        private ISettingsManager _stubSettingsManager;
        private IEventAggregator _stubEventAggregator;
        private ITestExecutionController _stubTestExeController;
        private IBloodVolumeTestController _stubController;

        [TestInitialize]
        public void TestInitialize()
        {
            BvaTestList = new ObservableCollection<BVATestItem>(new List<BVATestItem>{ new BVATestItem(TestFactory.CreateBvaTest(3)),
                                          new BVATestItem(TestFactory.CreateBvaTest(3)),
                                          new BVATestItem(TestFactory.CreateBvaTest(3))});

            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();
            _stubTestExeController = MockRepository.GenerateStub<ITestExecutionController>();

            _stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            _stubEventAggregator.Expect(x => x.GetEvent<BvaTestSaved>()).Return(new BvaTestSaved());
            _stubEventAggregator.Expect(x => x.GetEvent<ScrollViewerPageInfoChanged>())
                .Return(new ScrollViewerPageInfoChanged());

            _stubController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            _stubController.Expect(c => c.DataService.AreBvaTestTablesAreOnline()).Return(true);
            _stubController.Expect(c => c.DataService.SelectBVATestItems()).Return(BvaTestList);

            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _stubSettingsManager.Expect(c => c.GetSetting<String>(SettingKeys.BvaTestId2Label)).Return("I'm anID");
        }

        [TestMethod]
        public void And_the_background_task_is_started_then_the_test_items_list_is_retrieved()
        {
            var testSelectionViewModel = new TestSelectionViewModelThatDoesNotGetOrProcessBvaTestItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                                                     _stubEventAggregator, _stubTestExeController);
            Assert.IsTrue(testSelectionViewModel.GetBvaTestItemsListWasCalled);
        }

        [TestMethod]
        public void And_the_background_task_is_started_then_the_test_items_are_processed()
        {
            var testSelectionViewModel = new TestSelectionViewModelThatDoesNotGetOrProcessBvaTestItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                                                     _stubEventAggregator, _stubTestExeController);
            Assert.IsTrue(testSelectionViewModel.ProcessBvaTestItemsListWasCalled);
        }

        [TestMethod]
        public void And_the_view_model_is_successfully_configured_then_the_test_info_items_are_set_correctly()
        {
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                                                     _stubEventAggregator, _stubTestExeController);
            var observedCollection = testSelectionViewModel.TestInfoItems.SourceCollection as ObservableCollection<BVATestItem>;
            CollectionAssert.AreEqual(BvaTestList, observedCollection);
        }

        [TestMethod]
        public void And_the_view_model_is_successfully_configured_and_the_test_items_have_been_retrieved_then_the_initial_loading_is_finished()
        {
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                                                     _stubEventAggregator, _stubTestExeController);
            Assert.IsFalse(testSelectionViewModel.IsInitialDataLoading);
        }

        [TestMethod]
        public void And_the_view_model_is_successfully_configured_then_there_is_a_single_search_filter()
        {
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                                                     _stubEventAggregator, _stubTestExeController);
            Assert.AreEqual(1, testSelectionViewModel.TestInfoItems.FilterDescriptors.Count);
        }
    }
    // ReSharper restore InconsistentNaming

}
