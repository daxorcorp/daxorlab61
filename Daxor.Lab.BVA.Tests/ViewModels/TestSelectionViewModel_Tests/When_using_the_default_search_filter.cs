﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.Stubs;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_default_search_filter
    {
        private ObservableCollection<BVATestItem> _bvaTestList;
        private BVATestItem _bvaTestToFilter;

        private ILoggerFacade _stubLogger;
        private ISettingsManager _stubSettingsManager;
        private IEventAggregator _stubEventAggregator;
        private ITestExecutionController _stubTestExeController;
        private IBloodVolumeTestController _stubController;

        [TestInitialize]
        public void TestInitialize()
        {
            _bvaTestList = new ObservableCollection<BVATestItem>(new List<BVATestItem>{ new BVATestItem(TestFactory.CreateBvaTest(3)),
                                          new BVATestItem(TestFactory.CreateBvaTest(3)),
                                          new BVATestItem(TestFactory.CreateBvaTest(3))});

            _bvaTestToFilter = _bvaTestList[0];

            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();
            _stubTestExeController = MockRepository.GenerateStub<ITestExecutionController>();

            _stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            _stubEventAggregator.Expect(x => x.GetEvent<BvaTestSaved>()).Return(new BvaTestSaved());

            _stubController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            _stubController.Expect(c => c.DataService.AreBvaTestTablesAreOnline()).Return(true);
            _stubController.Expect(c => c.DataService.SelectBVATestItems()).Return(_bvaTestList);

            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _stubSettingsManager.Expect(c => c.GetSetting<String>(SettingKeys.BvaTestId2Label)).Return("I'm anID");
        }

        [TestMethod]
        public void And_the_search_text_is_null_then_a_matching_test_is_found()
        {
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                                                     _stubEventAggregator, _stubTestExeController)
            {
                SearchText = null
            };

            Assert.IsTrue(testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter));
        }

        [TestMethod]
        public void And_the_search_text_is_empty_then_a_matching_test_is_found()
        {
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                                         _stubEventAggregator, _stubTestExeController)
            {
                SearchText = String.Empty
            };

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsTrue(filterResult);
        }
        
        [TestMethod]
        public void And_the_search_text_is_whitespace_then_a_matching_test_is_found()
        {
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController)
            {
                SearchText = "  "
            };

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsTrue(filterResult);
        }
        
        // Patient name (matches, matches regardless of case, doesn't match)
        [TestMethod]
        public void And_the_search_text_matches_the_patient_name_exactly_then_a_matching_test_is_found()
        {
            const string testPatientName = "Phillip Andreason";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.PatientFullName = testPatientName;

            testSelectionViewModel.SearchText = testPatientName;

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsTrue(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_matches_the_patient_name_but_is_a_different_case_then_a_matching_test_is_found()
        {
            const string testPatientName = "Phillip Andreason";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.PatientFullName = testPatientName;

            testSelectionViewModel.SearchText = "PhIlLiP aNdReAsOn";

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsTrue(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_does_not_match_the_patient_name_then_no_matching_test_is_found()
        {
            const string testPatientName = "Phillip Andreason";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.PatientFullName = testPatientName;

            testSelectionViewModel.SearchText = "Cameron Presley";

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsFalse(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_matches_the_hospital_ID_exactly_then_a_matching_test_is_found()
        {
            const string testHospitalId = "123abc";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.PatientHospitalID = testHospitalId;

            testSelectionViewModel.SearchText = testHospitalId;

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsTrue(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_matches_the_hospital_ID_but_is_a_different_case_then_a_matching_test_is_found()
        {
            const string testHospitalId = "123abc";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.PatientHospitalID = testHospitalId;

            testSelectionViewModel.SearchText = "123AbC";

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsTrue(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_does_not_match_the_hospital_ID_then_no_matching_test_is_found()
        {
            const string testHospitalId = "123abc";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.PatientHospitalID = testHospitalId;

            testSelectionViewModel.SearchText = "WrongID";

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsFalse(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_matches_the_ID2_exactly_then_a_matching_test_is_found()
        {
            const string testId2 = "123abc";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.TestID2 = testId2;

            testSelectionViewModel.SearchText = testId2;

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsTrue(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_matches_the_ID2_but_is_a_different_case_then_a_matching_test_is_found()
        {
            const string testId2 = "123abc";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.TestID2 = testId2;

            testSelectionViewModel.SearchText = "123AbC";

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsTrue(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_does_not_match_the_ID2_then_no_matching_test_is_found()
        {
            const string testId2 = "123abc";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.TestID2 = testId2;

            testSelectionViewModel.SearchText = "WrongID";

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsFalse(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_matches_the_test_status_description_exactly_then_a_matching_test_is_found()
        {
            const string testStatusDescriptionToCheck = "123abc";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.TestStatusDescription = testStatusDescriptionToCheck;

            testSelectionViewModel.SearchText = testStatusDescriptionToCheck;

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsTrue(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_matches_the_test_status_description_but_is_a_different_case_then_a_matching_test_is_found()
        {
            const string testStatusDescriptionToCheck = "123abc";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.TestStatusDescription = testStatusDescriptionToCheck;

            testSelectionViewModel.SearchText = "123AbC";

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsTrue(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_does_not_match_the_test_status_description_then_no_matching_test_is_found()
        {
            const string testStatusDescriptionToCheck = "123abc";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.TestStatusDescription = testStatusDescriptionToCheck;

            testSelectionViewModel.SearchText = "WrongID";

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsFalse(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_matches_the_date_of_birth_exactly_then_a_matching_test_is_found()
        {
            const string testPatientDateOfBirth = "02/25/2014";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.PatientDOB = DateTime.Parse(testPatientDateOfBirth);

            testSelectionViewModel.SearchText = testPatientDateOfBirth;

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsTrue(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_does_not_match_the_date_of_birth_then_no_matching_test_is_found()
        {
            const string testPatientDateOfBirth = "02/25/2014";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.PatientDOB = DateTime.Parse(testPatientDateOfBirth);

            testSelectionViewModel.SearchText = "04/26/1981";

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsFalse(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_matches_the_test_create_date_exactly_then_a_matching_test_is_found()
        {
            const string testCreateDateToCheck = "02/25/2014 10:24PM";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.CreatedDate = DateTime.Parse(testCreateDateToCheck);

            testSelectionViewModel.SearchText = "02/25/2014 10:24P";

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsTrue(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_matches_the_test_create_date_but_is_a_different_case_then_a_matching_test_is_found()
        {
            const string testCreateDateToCheck = "02/25/2014 10:24PM";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.CreatedDate = DateTime.Parse(testCreateDateToCheck);

            testSelectionViewModel.SearchText = "02/25/2014 10:24p";

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsTrue(filterResult);
        }

        [TestMethod]
        public void And_the_search_text_does_not_match_the_test_create_date_then_no_matching_test_is_found()
        {
            const string testCreateDateToCheck = "02/25/2014 10:24PM";
            var testSelectionViewModel = new TestSelectionViewModelThatProcessesANonNullBvaItemsList(_stubController, _stubLogger, _stubSettingsManager,
                                                                                             _stubEventAggregator, _stubTestExeController);

            _bvaTestToFilter.CreatedDate = DateTime.Parse(testCreateDateToCheck);

            testSelectionViewModel.SearchText = "04/26/1981 10:24P";

            var filterResult = testSelectionViewModel.TestInfoItems.Filter.Invoke(_bvaTestToFilter);

            Assert.IsFalse(filterResult);
        }
    }
    // ReSharper restore InconsistentNaming
}
