﻿using System.Threading;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Telerik.Windows.Data;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.Stubs
{
    internal class TestSelectionViewModelThatDoesNotGetOrProcessBvaTestItemsList : TestSelectionViewModel
    {
        public bool GetBvaTestItemsListWasCalled;
        public bool ProcessBvaTestItemsListWasCalled;
        public bool EvaluateCommandCanExecuteWasCalled;

        public TestSelectionViewModelThatDoesNotGetOrProcessBvaTestItemsList(IBloodVolumeTestController controller,
                                                                             ILoggerFacade logger,
                                                                             ISettingsManager settingsManager,
                                                                             IEventAggregator eventAggregator,
                                                                             ITestExecutionController testExeController)
                                                                             : base(null, testExeController, eventAggregator,
                                                                                    null, controller, null, logger,
                                                                                    null, settingsManager, null)
        {
        }

        protected override void UpdateSearchFilterPredicate()
        {
        }

        protected override void ConfigureViewModel()
        {
            base.ConfigureViewModel();
            Thread.Sleep(300);
        }

        protected override QueryableCollectionView GetBvaTestItemsList(string testRetrievalErrorMessage)
        {
            GetBvaTestItemsListWasCalled = true;
            return null;
        }

        protected override void ProcessBvaTestItemsList(string testRetrievalErrorMessage, QueryableCollectionView result)
        {
            ProcessBvaTestItemsListWasCalled = true;
        }

        protected override void EvaluateScrollCommandCanExecute()
        {
            EvaluateCommandCanExecuteWasCalled = true;
        }
    }
}
