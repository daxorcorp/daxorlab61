﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.Stubs
{
    internal class TestSelectionViewModelThatDoesNotUpdateSearchFilterOrConfigureViewModel : TestSelectionViewModel
    {
        public bool UpdateSearchFilterPredicateWasCalled;
        public bool ConfigureViewModelWasCalled;

        public TestSelectionViewModelThatDoesNotUpdateSearchFilterOrConfigureViewModel(IBloodVolumeTestController controller,
                                                                                       ILoggerFacade logger,
                                                                                       ISettingsManager settingsManager,
                                                                                       IEventAggregator eventAggregator,
                                                                                       ITestExecutionController testExeController)
                                                                                       : base(null, testExeController, eventAggregator,
                                                                                              null, controller, null, logger,
                                                                                              null, settingsManager, null)
        {
        }

        protected override void UpdateSearchFilterPredicate()
        {
            UpdateSearchFilterPredicateWasCalled = true;
        }

        protected override void ConfigureViewModel()
        {
            ConfigureViewModelWasCalled = true;
        }
    }
}
