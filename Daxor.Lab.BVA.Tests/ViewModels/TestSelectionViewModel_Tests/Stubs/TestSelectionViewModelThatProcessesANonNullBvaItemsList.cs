﻿using Daxor.Lab.BVA.Common.SearchingAndFiltering;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.Stubs
{
    class TestSelectionViewModelThatProcessesANonNullBvaItemsList : TestSelectionViewModel
    {
        public const string TestRetrievalErrorMessage = "This is the right error message!";

        public TestSelectionViewModelThatProcessesANonNullBvaItemsList(IBloodVolumeTestController controller,
                                                                       ILoggerFacade logger,
                                                                       ISettingsManager settingsManager,
                                                                       IEventAggregator eventAggregator,
                                                                       ITestExecutionController testExeController)
                                                                       : base(null, testExeController, eventAggregator,
                                                                              null, controller, null, logger,
                                                                              null, settingsManager, null)
        {
        }

        protected override void ConfigureViewModel()
        {
            var collection = GetBvaTestItemsList(TestRetrievalErrorMessage);
            ProcessBvaTestItemsList(TestRetrievalErrorMessage, collection);
        }

        public PredicateFilterDescriptor<BVATestItem> SearchFilter
        {
            get { return _searchFilter; }
        }
    }
}
