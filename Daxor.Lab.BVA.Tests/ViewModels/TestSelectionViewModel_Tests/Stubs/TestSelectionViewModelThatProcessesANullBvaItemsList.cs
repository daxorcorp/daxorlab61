﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.Stubs
{
    internal class TestSelectionViewModelThatProcessesANullBvaItemsList : TestSelectionViewModel
    {
        public const string TestRetrievalErrorMessage = "This is the right error message!";

        public TestSelectionViewModelThatProcessesANullBvaItemsList(IBloodVolumeTestController controller,
                                                                    ILoggerFacade logger,
                                                                    ISettingsManager settingsManager,
                                                                    IEventAggregator eventAggregator,
                                                                    ITestExecutionController testExeController,
                                                                    IMessageManager messageManager)
                                                                    : base(null, testExeController, eventAggregator,
                                                                        null, controller, null, logger,
                                                                        null, settingsManager, messageManager)
        {
        }

        protected override void UpdateSearchFilterPredicate()
        {
        }

        protected override void ConfigureViewModel()
        {
            ProcessBvaTestItemsList(TestRetrievalErrorMessage, null);
        }
    }
}
