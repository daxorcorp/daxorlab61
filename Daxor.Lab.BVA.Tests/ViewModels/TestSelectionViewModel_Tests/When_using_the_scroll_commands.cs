﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.Stubs;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_scroll_commands
    {
        private IEventAggregator _stubEventAggregator;
        private IBloodVolumeTestController _stubBloodVolumeTestController;
        private ISettingsManager _stubSettingsManager;
        private ILoggerFacade _stubLogger;
        private ITestExecutionController _stubTestExecutionController;
        private TestSelectionViewModel _testSelectionViewModel;
        private ScrollViewerScrollChanged _stubEvent;

        [TestInitialize]
        public void Initialize()
        {
            _stubEvent = Substitute.For<ScrollViewerScrollChanged>();
            
            _stubEventAggregator = Substitute.For<IEventAggregator>();
            _stubEventAggregator.GetEvent<BvaTestSaved>().Returns(new BvaTestSaved());
            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Returns(new ScrollViewerPageInfoChanged());
            _stubEventAggregator.GetEvent<ScrollViewerScrollChanged>().Returns(_stubEvent);

            _stubBloodVolumeTestController = Substitute.For<IBloodVolumeTestController>();
            _stubBloodVolumeTestController.DataService.AreBvaTestTablesAreOnline().Returns(true);

            _stubSettingsManager = Substitute.For<ISettingsManager>();
            _stubLogger = Substitute.For<ILoggerFacade>();
            _stubTestExecutionController = Substitute.For<ITestExecutionController>();

            _testSelectionViewModel = new TestSelectionViewModelThatDoesNotGetOrProcessBvaTestItemsList(_stubBloodVolumeTestController, _stubLogger, _stubSettingsManager, _stubEventAggregator, _stubTestExecutionController);
        }

        [TestMethod]
        public void And_the_data_is_still_loading_then_the_scroll_page_top_command_is_disabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = true;
            _testSelectionViewModel.StartingIndex = "2";

            Assert.IsFalse(_testSelectionViewModel.ScrollPageTopCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_data_is_finished_loading_and_the_starting_index_is_one_then_the_scroll_page_top_command_is_disabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.StartingIndex = "1";

            Assert.IsFalse(_testSelectionViewModel.ScrollPageTopCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_data_is_finished_loading_and_the_starting_index_is_a_dash_then_the_scroll_page_top_command_is_disabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.StartingIndex = "-";

            Assert.IsFalse(_testSelectionViewModel.ScrollPageTopCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_data_is_finished_loading_and_the_starting_index_is_zero_then_the_scroll_page_top_command_is_disabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.StartingIndex = "0";

            Assert.IsFalse(_testSelectionViewModel.ScrollPageTopCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_data_is_finished_loading_and_the_starting_index_is_not_1_or_dash_or_zero_then_the_scroll_page_top_command_is_enabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.StartingIndex = "10";

            Assert.IsTrue(_testSelectionViewModel.ScrollPageTopCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_page_top_command_is_enabled_then_executing_it_publishes_the_scroll_viewer_event_with_the_correct_payload()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.StartingIndex = "10";

            _testSelectionViewModel.ScrollPageTopCommand.Execute(new object());

            _stubEventAggregator.Received().GetEvent<ScrollViewerScrollChanged>();
            _stubEvent.Received().Publish(ScrollViewerScrollChangedEventPayload.ScrollTop);
        }

        [TestMethod]
        public void And_the_data_is_still_loading_then_the_scroll_page_up_command_is_disabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = true;
            _testSelectionViewModel.StartingIndex = "2";

            Assert.IsFalse(_testSelectionViewModel.ScrollPageUpCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_data_is_finished_loading_and_the_starting_index_is_one_then_the_scroll_page_up_command_is_disabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.StartingIndex = "1";

            Assert.IsFalse(_testSelectionViewModel.ScrollPageUpCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_data_is_finished_loading_and_the_starting_index_is_a_dash_then_the_scroll_page_up_command_is_disabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.StartingIndex = "-";

            Assert.IsFalse(_testSelectionViewModel.ScrollPageUpCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_data_is_finished_loading_and_the_starting_index_is_zero_then_the_scroll_page_up_command_is_disabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.StartingIndex = "0";

            Assert.IsFalse(_testSelectionViewModel.ScrollPageUpCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_data_is_finished_loading_and_the_starting_index_is_not_1_or_dash_or_zero_then_the_scroll_page_up_command_is_enabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.StartingIndex = "10";

            Assert.IsTrue(_testSelectionViewModel.ScrollPageUpCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_page_up_command_is_enabled_then_executing_it_publishes_the_scroll_viewer_event_with_the_correct_payload()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.StartingIndex = "10";

            _testSelectionViewModel.ScrollPageUpCommand.Execute(new object());

            _stubEventAggregator.Received().GetEvent<ScrollViewerScrollChanged>();
            _stubEvent.Received().Publish(ScrollViewerScrollChangedEventPayload.ScrollPageUp);
        }

        [TestMethod]
        public void And_the_data_is_still_loading_then_the_scroll_page_down_command_is_disabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = true;
            _testSelectionViewModel.EndingIndex = "2";
            _testSelectionViewModel.TotalItemsInScrollViewer = "10";

            Assert.IsFalse(_testSelectionViewModel.ScrollPageDownCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_data_is_finished_loading_and_the_ending_index_is_equal_to_the_total_items_in_the_scroll_viewer_then_the_scroll_page_down_command_is_disabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.EndingIndex = "10";
            _testSelectionViewModel.TotalItemsInScrollViewer = "10";

            Assert.IsFalse(_testSelectionViewModel.ScrollPageDownCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_data_is_finished_loading_and_the_ending_index_is_not_equal_to_the_total_items_in_the_scroll_viewer_then_the_scroll_page_down_command_is_enabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.EndingIndex = "2";
            _testSelectionViewModel.TotalItemsInScrollViewer = "10";

            Assert.IsTrue(_testSelectionViewModel.ScrollPageDownCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_page_down_command_is_enabled_then_executing_it_publishes_the_scroll_viewer_event_with_the_correct_payload()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.EndingIndex = "2";
            _testSelectionViewModel.StartingIndex = "10";

            _testSelectionViewModel.ScrollPageDownCommand.Execute(new object());

            _stubEventAggregator.Received().GetEvent<ScrollViewerScrollChanged>();
            _stubEvent.Received().Publish(ScrollViewerScrollChangedEventPayload.ScrollPageDown);
        }

        [TestMethod]
        public void And_the_data_is_still_loading_then_the_scroll_page_bottom_command_is_disabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = true;
            _testSelectionViewModel.EndingIndex = "2";
            _testSelectionViewModel.TotalItemsInScrollViewer = "10";

            Assert.IsFalse(_testSelectionViewModel.ScrollPageBottomCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_data_is_finished_loading_and_the_ending_index_is_equal_to_the_total_items_in_the_scroll_viewer_then_the_scroll_page_bottom_command_is_disabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.EndingIndex = "10";
            _testSelectionViewModel.TotalItemsInScrollViewer = "10";

            Assert.IsFalse(_testSelectionViewModel.ScrollPageBottomCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_data_is_finished_loading_and_the_ending_index_is_not_equal_to_the_total_items_in_the_scroll_viewer_then_the_scroll_page_bottom_command_is_enabled()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.EndingIndex = "2";
            _testSelectionViewModel.TotalItemsInScrollViewer = "10";

            Assert.IsTrue(_testSelectionViewModel.ScrollPageBottomCommand.CanExecute(new object()));
        }

        [TestMethod]
        public void And_the_page_bottom_command_is_enabled_then_executing_it_publishes_the_scroll_viewer_event_with_the_correct_payload()
        {
            _testSelectionViewModel.IsInitialDataLoading = false;
            _testSelectionViewModel.EndingIndex = "2";
            _testSelectionViewModel.StartingIndex = "10";

            _testSelectionViewModel.ScrollPageBottomCommand.Execute(new object());

            _stubEventAggregator.Received().GetEvent<ScrollViewerScrollChanged>();
            _stubEvent.Received().Publish(ScrollViewerScrollChangedEventPayload.ScrollBottom);
        }
    }
    // ReSharper restore RedundantArgumentName
}