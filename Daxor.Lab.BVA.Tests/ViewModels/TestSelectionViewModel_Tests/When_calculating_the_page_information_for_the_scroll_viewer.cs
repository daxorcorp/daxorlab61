﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.Stubs;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_calculating_the_page_information_for_the_scroll_viewer
    {
        private IEventAggregator _stubEventAggregator;
        private IBloodVolumeTestController _stubBloodVolumeTestController;
        private ISettingsManager _stubSettingsManager;
        private ILoggerFacade _stubLogger;
        private ITestExecutionController _stubTestExecutionController;
        private ScrollViewerPageInfoChangedPayload _stubPayload;
        private TestSelectionViewModelThatDoesNotGetOrProcessBvaTestItemsList _testSelectionViewModel;

        [TestInitialize]
        public void Initialize()
        {
            var stubEvent = new ScrollViewerPageInfoChanged();
            _stubEventAggregator = Substitute.For<IEventAggregator>();
            _stubEventAggregator.GetEvent<BvaTestSaved>().Returns(new BvaTestSaved());
            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Returns(stubEvent);

            _stubBloodVolumeTestController = Substitute.For<IBloodVolumeTestController>();
            _stubBloodVolumeTestController.DataService.AreBvaTestTablesAreOnline().Returns(true);

            _stubSettingsManager = Substitute.For<ISettingsManager>();
            _stubLogger = Substitute.For<ILoggerFacade>();
            _stubTestExecutionController = Substitute.For<ITestExecutionController>();

            _testSelectionViewModel = new TestSelectionViewModelThatDoesNotGetOrProcessBvaTestItemsList(_stubBloodVolumeTestController, _stubLogger, _stubSettingsManager, _stubEventAggregator, _stubTestExecutionController);
            _stubPayload = new ScrollViewerPageInfoChangedPayload(0, 0, 0, 0 , 0, AppModuleIds.BloodVolumeAnalysis);
        }

        [TestMethod]
        public void And_a_scroll_event_occurs_then_each_of_the_scroll_commands_are_reevaluated()
        {
            Assert.IsFalse(_testSelectionViewModel.EvaluateCommandCanExecuteWasCalled);

            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.IsTrue(_testSelectionViewModel.EvaluateCommandCanExecuteWasCalled);
        }

        [TestMethod]
        public void And_the_item_count_is_zero_then_the_starting_index_is_the_string_0()
        {
            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("0", _testSelectionViewModel.StartingIndex);
        }

        [TestMethod]
        public void And_the_item_count_is_zero_then_the_ending_index_is_the_string_0()
        {
            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("0", _testSelectionViewModel.EndingIndex);
        }

        [TestMethod]
        public void And_the_item_count_is_zero_then_the_total_search_items_is_the_string_0()
        {
            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("0", _testSelectionViewModel.TotalSearchItems);
        }

        [TestMethod]
        public void And_the_item_count_is_zero_then_the_total_items_in_the_scroll_viewer_is_the_string_0()
        {
            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("0", _testSelectionViewModel.TotalItemsInScrollViewer);
        }

        [TestMethod]
        public void And_the_total_item_count_is_zero_then_the_total_items_in_the_scroll_viewer_source_collection_is_the_string_0()
        {
            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("0", _testSelectionViewModel.TotalItemsInScrollviewerSourceCollection);
        }

        // PAndreason 3/31/2015
        // this is brittle ... 
        // Hypothesis: in real execution vertical offset is never greater than or equal to item height...
        [TestMethod]
        public void And_the_item_count_is_one_and_the_extent_height_is_greater_than_one_and_the_vertical_offset_is_greater_than_one_then_the_starting_index_is_the_string_1()
        {
            _stubPayload.ItemCount = 1;
            _stubPayload.ScrollViewerExtentHeight = 10;
            _stubPayload.ScrollViewerVerticalOffset = 2;

            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("1", _testSelectionViewModel.StartingIndex);
        }

        [TestMethod]
        public void And_the_item_count_is_one_and_the_extent_height_is_greater_than_one_and_the_viewport_height_is_greater_than_one_and_the_vertical_offset_is_greater_than_one_then_the_ending_index_is_the_string_1()
        {
            _stubPayload.ItemCount = 1;
            _stubPayload.ScrollViewerViewportHeight = 10;
            _stubPayload.ScrollViewerExtentHeight = 10;
            _stubPayload.ScrollViewerVerticalOffset = 2;

            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("1", _testSelectionViewModel.EndingIndex);
        }

        [TestMethod]
        public void And_the_item_count_is_1_then_the_total_search_items_is_the_string_1()
        {
            _stubPayload.ItemCount = 1;

            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("1", _testSelectionViewModel.TotalSearchItems);
        }

        [TestMethod]
        public void And_the_item_count_is_1_then_the_total_items_in_the_scroll_viewer_is_the_string_1()
        {
            _stubPayload.ItemCount = 1;

            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("1", _testSelectionViewModel.TotalItemsInScrollViewer);
        }

        [TestMethod]
        public void And_the_total_item_count_is_1_then_the_total_items_in_the_scroll_viewer_source_collection_is_the_string_1()
        {
            _stubPayload.TotalItemCount = 1;

            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("1", _testSelectionViewModel.TotalItemsInScrollviewerSourceCollection);
        }

        [TestMethod]
        public void And_the_item_count_is_two_and_the_extent_height_is_greater_than_one_and_the_vertical_offset_is_greater_than_one_then_the_starting_index_is_the_string_1()
        {
            _stubPayload.ItemCount = 2;
            _stubPayload.ScrollViewerExtentHeight = 10;
            _stubPayload.ScrollViewerVerticalOffset = 2;

            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("1", _testSelectionViewModel.StartingIndex);
        }

        [TestMethod]
        public void And_the_item_count_is_two_and_the_extent_height_is_greater_than_one_and_the_viewport_height_is_greater_than_one_and_the_vertical_offset_is_greater_than_one_then_the_ending_index_is_the_string_2()
        {
            _stubPayload.ItemCount = 2;
            _stubPayload.ScrollViewerViewportHeight = 10;
            _stubPayload.ScrollViewerExtentHeight = 10;
            _stubPayload.ScrollViewerVerticalOffset = 2;

            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("2", _testSelectionViewModel.EndingIndex);
        }

        [TestMethod]
        public void And_the_item_count_is_2_then_the_total_search_items_is_the_string_2()
        {
            _stubPayload.ItemCount = 2;

            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("2", _testSelectionViewModel.TotalSearchItems);
        }

        [TestMethod]
        public void And_the_item_count_is_2_then_the_total_items_in_the_scroll_viewer_is_the_string_2()
        {
            _stubPayload.ItemCount = 2;

            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("2", _testSelectionViewModel.TotalItemsInScrollViewer);
        }

        [TestMethod]
        public void And_the_total_item_count_is_2_then_the_total_items_in_the_scroll_viewer_source_collection_is_the_string_2()
        {
            _stubPayload.TotalItemCount = 2;

            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("2", _testSelectionViewModel.TotalItemsInScrollviewerSourceCollection);
        }

        // PAndreason 3/31/2015
        // This creates the situation where the number of items in the collection extends past the end of the screen
        [TestMethod]
        public void And_the_item_count_is_12_and_the_extent_height_is_greater_than_one_and_the_viewport_height_is_greater_than_one_and_the_vertical_offset_is_greater_than_one_then_the_ending_index_is_the_string_8()
        {
            _stubPayload.ItemCount = 12;
            _stubPayload.ScrollViewerViewportHeight = 1;
            _stubPayload.ScrollViewerExtentHeight = 3;
            _stubPayload.ScrollViewerVerticalOffset = 1;

            _stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

            Assert.AreEqual("8", _testSelectionViewModel.EndingIndex);
        }
    }
    // ReSharper restore RedundantArgumentName
}