using System;
using System.Collections.ObjectModel;
using Daxor.Lab.BVA.Common.SearchingAndFiltering;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.Stubs;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Telerik.Windows.Data;
using TestExecutionEventArgs = Daxor.Lab.Domain.Eventing.TestExecutionEventArgs;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_a_running_test_completes
    {
        private static IBVADataService _stubBvaDataService;
        private static ISettingsManager _dummySettingsManager;
        private static IEventAggregator _stubEventAggregator;
        private static ITestExecutionController _stubTestExecutionController;

        private IBloodVolumeTestController _stubBloodVolumeTestController;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();

            _stubBvaDataService = MockRepository.GenerateStub<IBVADataService>();
            _stubBvaDataService.Expect(s => s.AreBvaTestTablesAreOnline()).Return(true);

            _stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            _stubEventAggregator.Expect(a => a.GetEvent<BvaTestSaved>()).Return(new BvaTestSaved());
            _stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _stubBloodVolumeTestController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            _stubBloodVolumeTestController.Expect(c => c.DataService).Return(_stubBvaDataService);
        }

        [TestMethod]
        public void Then_the_item_in_the_test_list_is_also_marked_as_completed()
        {
            var executingTest = new BVATest(null) { InternalId = Guid.NewGuid() };
            var testItemForExecutingTest = new BVATestItem
            {
                TestID = executingTest.InternalId,
                Status = TestStatus.Running
            };

            // ReSharper disable once UnusedVariable
            var vm = new TestSelectionViewModelThatDoesNotUpdateSearchFilterOrConfigureViewModel(
                    _stubBloodVolumeTestController,
                    null, _dummySettingsManager, _stubEventAggregator, _stubTestExecutionController)
                {
                    TestInfoItems = new QueryableCollectionView(new ObservableCollection<BVATestItem> { testItemForExecutingTest })
                };

            // Act
            _stubTestExecutionController.Raise(c => c.TestCompleted += null, this,
                new TestExecutionEventArgs(executingTest, executingTest.InternalId));

            Assert.AreEqual(TestStatus.Completed, testItemForExecutingTest.Status);
        }

        [TestMethod]
        public void And_the_tests_were_filtered_out_then_the_item_in_the_test_list_is_also_marked_as_completed()
        {
            var executingTest = new BVATest(null) { InternalId = Guid.NewGuid() };

            var testItemForExecutingTest = new BVATestItem
            {
                TestID = executingTest.InternalId,
                Status = TestStatus.Running
            };

            var vm = new TestSelectionViewModelThatDoesNotUpdateSearchFilterOrConfigureViewModel(
                    _stubBloodVolumeTestController,
                    null, _dummySettingsManager, _stubEventAggregator, _stubTestExecutionController)
                {
                    TestInfoItems = new QueryableCollectionView(new ObservableCollection<BVATestItem> { testItemForExecutingTest })
                };

            var searchFilter = new PredicateFilterDescriptor<BVATestItem> { Predicate = item => false };
            vm.TestInfoItems.FilterDescriptors.Clear();
            vm.TestInfoItems.FilterDescriptors.Add(searchFilter);

            // Act
            _stubTestExecutionController.Raise(c => c.TestCompleted += null, this,
                new TestExecutionEventArgs(executingTest, executingTest.InternalId));

            Assert.AreEqual(TestStatus.Completed, testItemForExecutingTest.Status);
        }

        [TestMethod]
        public void Then_the_item_in_the_test_list_is_something_also_marked_as_completed()
        {
            var executingTest = new BVATest(null) { InternalId = Guid.NewGuid(), HasSufficientData = true };

            _stubBloodVolumeTestController.Expect(t => t.RunningTest).Return(executingTest);

            var testItemForExecutingTest = new BVATestItem
            {
                TestID = executingTest.InternalId,
                Status = TestStatus.Running,
                HasSufficientData = false
            };

            var vm = new TestSelectionViewModelThatDoesNotUpdateSearchFilterOrConfigureViewModel(
                    _stubBloodVolumeTestController,
                    null, _dummySettingsManager, _stubEventAggregator, _stubTestExecutionController)
                {
                    TestInfoItems = new QueryableCollectionView(new ObservableCollection<BVATestItem> { testItemForExecutingTest })
                };

            var searchFilter = new PredicateFilterDescriptor<BVATestItem> { Predicate = item => false };
            vm.TestInfoItems.FilterDescriptors.Clear();
            vm.TestInfoItems.FilterDescriptors.Add(searchFilter);
            
            // Act
            _stubTestExecutionController.Raise(c => c.TestCompleted += null, this,
                new TestExecutionEventArgs(executingTest, executingTest.InternalId));

            Assert.IsTrue(testItemForExecutingTest.HasSufficientData);
        }

        [TestMethod]
        public void And_the_tests_were_filtered_out_then_the_item_in_the_test_list_is_something_also_marked_as_completed()
        {
            var executingTest = new BVATest(null) { InternalId = Guid.NewGuid(), HasSufficientData = true };

            _stubBloodVolumeTestController.Expect(t => t.RunningTest).Return(executingTest);

            var testItemForExecutingTest = new BVATestItem
            {
                TestID = executingTest.InternalId,
                Status = TestStatus.Running,
                HasSufficientData = false
            };

            // ReSharper disable once UnusedVariable
            var vm = new TestSelectionViewModelThatDoesNotUpdateSearchFilterOrConfigureViewModel(
                    _stubBloodVolumeTestController,
                    null, _dummySettingsManager, _stubEventAggregator, _stubTestExecutionController)
                {
                    TestInfoItems = new QueryableCollectionView(new ObservableCollection<BVATestItem> { testItemForExecutingTest })
                };

            // Act
            _stubTestExecutionController.Raise(c => c.TestCompleted += null, this,
                new TestExecutionEventArgs(executingTest, executingTest.InternalId));

            Assert.IsTrue(testItemForExecutingTest.HasSufficientData);
        }
    }
    // ReSharper restore InconsistentNaming
}
