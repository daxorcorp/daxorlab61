﻿using System;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_instantiating_the_test_selection_view_model
    {
        [TestMethod]
        public void And_all_the_tables_needed_for_creating_the_test_list_are_not_available_then_an_exception_is_thrown()
        {
            var stubController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            stubController.Expect(c => c.DataService.AreBvaTestTablesAreOnline()).Return(false);

            var stubLogger = MockRepository.GenerateStub<ILoggerFacade>();

            // ReSharper disable once UnusedVariable
            AssertEx.Throws<ApplicationException>(() =>
            {
                var ignored = new TestSelectionViewModel(null, null, null, null, stubController, null, stubLogger, null,
                    null, null);
            });
        }
    }
    // ReSharper restore InconsistentNaming
}
