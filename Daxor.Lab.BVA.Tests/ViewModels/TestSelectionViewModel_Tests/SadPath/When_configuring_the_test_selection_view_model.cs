﻿using System;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.Stubs;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_configuring_the_test_selection_view_model
    {
        private ILoggerFacade _stubLogger;
        private ISettingsManager _stubSettingsManager;
        private IEventAggregator _stubEventAggregator;
        private ITestExecutionController _stubTestExeController;
        private IBloodVolumeTestController _stubController;

        [TestInitialize]
        public void TestInitialize()
        {
            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();
            _stubTestExeController = MockRepository.GenerateStub<ITestExecutionController>();

            _stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            _stubEventAggregator.Expect(x => x.GetEvent<BvaTestSaved>()).Return(new BvaTestSaved());

            _stubController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            _stubController.Expect(c => c.DataService.AreBvaTestTablesAreOnline()).Return(true);

            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _stubSettingsManager.Expect(c => c.GetSetting<String>(SettingKeys.BvaTestId2Label)).Return("I'm anID");
        }

        [TestMethod]
        public void And_GetBvaTestItemsList_returns_a_null_list_then_an_exception_is_thrown()
        {
            // ReSharper disable once UnusedVariable
            AssertEx.Throws<ApplicationException>(() =>
            {
                var testSelectionViewModel = new TestSelectionViewModelThatProcessesANullBvaItemsList(_stubController,
                    _stubLogger, _stubSettingsManager,
                    _stubEventAggregator, _stubTestExeController, null);
            });
        }
    }
    // ReSharper restore InconsistentNaming

}
