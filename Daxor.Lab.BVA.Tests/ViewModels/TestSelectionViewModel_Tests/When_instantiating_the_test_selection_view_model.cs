﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.Stubs;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_instantiating_the_test_selection_view_model
    {
        private ILoggerFacade _stubLogger;
        private ISettingsManager _stubSettingsManager;
        private IEventAggregator _stubEventAggregator;
        private ITestExecutionController _stubTestExeController;
        private IBloodVolumeTestController _stubController;

        [TestInitialize]
        public void TestInitialize()
        {
            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();
            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _stubTestExeController = MockRepository.GenerateStub<ITestExecutionController>();

            _stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            _stubEventAggregator.Expect(x => x.GetEvent<BvaTestSaved>()).Return(new BvaTestSaved());

            _stubController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            _stubController.Expect(c => c.DataService.AreBvaTestTablesAreOnline()).Return(true);
        }

        [TestMethod]
        public void And_all_the_tables_needed_for_creating_the_test_list_are_available_then_the_search_filter_is_updated()
        {
            var testSelectionViewModel = new TestSelectionViewModelThatDoesNotUpdateSearchFilterOrConfigureViewModel(_stubController, _stubLogger, _stubSettingsManager, 
                                                                                                                     _stubEventAggregator, _stubTestExeController);

            Assert.IsTrue(testSelectionViewModel.UpdateSearchFilterPredicateWasCalled);
        }

        [TestMethod]
        public void And_all_the_tables_needed_for_creating_the_test_list_are_available_then_view_model_is_configured()
        {
            var testSelectionViewModel = new TestSelectionViewModelThatDoesNotUpdateSearchFilterOrConfigureViewModel(_stubController, _stubLogger, _stubSettingsManager,
                                                                                                                     _stubEventAggregator, _stubTestExeController);

            Assert.IsTrue(testSelectionViewModel.ConfigureViewModelWasCalled);
        }
    }
    // ReSharper restore InconsistentNaming

}
