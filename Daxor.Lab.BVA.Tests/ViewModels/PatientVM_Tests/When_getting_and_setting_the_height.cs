using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Domain.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.ViewModels.PatientVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_and_setting_the_height
    {
        [TestMethod]
        public void And_using_metric_then_the_height_is_rounded_to_2_decimal_places()
        {
            var patient = new BVAPatient(null) {HeightInCm = 69.1234, MeasurementSystem = MeasurementSystem.Metric};
            var patientViewModel = new PatientViewModel(patient);

            Assert.AreEqual("69.12", patientViewModel.Height);
        }

        [TestMethod]
        public void And_using_metric_and_the_patient_height_is_in_English_then_the_height_is_converted_to_metric()
        {
            var patient = new BVAPatient(null) { HeightInCm = 69.1234, MeasurementSystem = MeasurementSystem.English };
            var patientViewModel = new PatientViewModel(patient) {MeasurementSystem = MeasurementSystem.Metric};

            Assert.AreEqual("69.12", patientViewModel.Height);
        }
    }
    // ReSharper restore InconsistentNaming
}
