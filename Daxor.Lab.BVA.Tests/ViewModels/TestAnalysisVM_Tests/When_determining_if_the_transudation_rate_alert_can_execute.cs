﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Reports.Interfaces;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Evaluation;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestAnalysisVM_Tests
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_determining_if_the_transudation_rate_alert_can_execute
	{
		private TestAnalysisViewModel _viewModel;

		private const string PropertyName = "TransudationRateResult";

		private IRule _rule;
		private IRuleEngine _ruleEngine;
		private ITestExecutionController _testExecutionController;

		[TestInitialize]
		public void BeforeEachTest()
		{
			var aMessageToReturn = new Message("key", "description", "resolution", "ID", MessageType.Alert);

			var stubMessageManager = Substitute.For<IMessageManager>();
			stubMessageManager.GetMessage(Arg.Any<string>()).Returns(aMessageToReturn);

			var stubEventAggregator = Substitute.For<IEventAggregator>();
			stubEventAggregator.GetEvent<ViewReportExecuteChanged>().Returns(new ViewReportExecuteChanged());
			stubEventAggregator.GetEvent<BvaTestSelected>().Returns(new BvaTestSelected());
			stubEventAggregator.GetEvent<AppPartActivationChanged>().Returns(new AppPartActivationChanged());
			stubEventAggregator.GetEvent<TestStarted>().Returns(new TestStarted());
			stubEventAggregator.GetEvent<TestAborted>().Returns(new TestAborted());
			stubEventAggregator.GetEvent<TestCompleted>().Returns(new TestCompleted());
			stubEventAggregator.GetEvent<SampleChangerPositionChangeCompleted>()
				.Returns(new SampleChangerPositionChangeCompleted());

			_testExecutionController = Substitute.For<ITestExecutionController>();

			_viewModel = new TestAnalysisViewModel(Substitute.For<IPopupController>(),
				stubEventAggregator,
				_testExecutionController, Substitute.For<ILoggerFacade>(),
				Substitute.For<IMessageBoxDispatcher>(), Substitute.For<IRegionManager>(),
				Substitute.For<IBloodVolumeTestController>(), Substitute.For<IIdealsCalcEngineService>(),
				Substitute.For<IBvaResultAlertService>(), Substitute.For<IBestFitLinePointsCalculator>(),
				Substitute.For<IAutomaticPointExclusionService>(), Substitute.For<IStarBurnWrapper>(),
				Substitute.For<IAuditTrailDataAccess>(), Substitute.For<IFolderBrowser>(), stubMessageManager,
				Substitute.For<ISettingsManager>(), Substitute.For<IZipFileFactory>(), Substitute.For<IPdfPrinter>())
			{
				CurrentTest = BvaTestFactory.CreateBvaTest(3, 10)
			};

			_rule = Substitute.For<IRule>();
			_ruleEngine = Substitute.For<IRuleEngine>();
		}

		[TestMethod]
		public void And_the_transudation_rate_is_normal_then_false_is_returned()
		{
			_viewModel.CurrentTest.RuleEngine = _ruleEngine;

			Assert.IsFalse(_viewModel.TransudationRateAlertCommand.CanExecute(null));
		}

		[TestMethod]
		public void And_the_transudation_rate_is_negative_then_true_is_returned()
		{
			_rule.PropertyName.Returns(PropertyName);
			_rule.RuleId.Returns(MessageKeys.BvaTransudationReverse);

			_ruleEngine.GetBrokenRulesOnModel(Arg.Any<BVATest>()).Returns(new List<IRule>
			{
				_rule
			});

			_viewModel.CurrentTest.RuleEngine = _ruleEngine;

			Assert.IsTrue(_viewModel.TransudationRateAlertCommand.CanExecute(null));
		}
	}
	// ReSharper restore InconsistentNaming
}