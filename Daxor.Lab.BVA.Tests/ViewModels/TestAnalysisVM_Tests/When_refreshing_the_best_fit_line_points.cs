﻿using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Domain.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Rhino.Mocks.Constraints;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestAnalysisVM_Tests
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable PossibleMultipleEnumeration
    [TestClass]
    public class When_refreshing_the_best_fit_line_points
    {
        private IEnumerable<BVACompositeSample> _compositeSamples;
        private IEnumerable<UnadjustedCompositeBloodPointViewModel> _viewModels;

            // Code that should be executed before running the first test in this class.
        [TestInitialize]
        public void TestsInitialize()
        {
            _viewModels = BloodPointViewModelFactory.CreateBloodVolumePointViewModels(5);
            _compositeSamples = _viewModels.Select(v => v.ObservableCompositePatientSample);
        }
        
        [TestMethod]
        public void Then_whole_sample_exclusions_are_not_considered()
        {
            // Mark the last sample as excluded to ensure it's not used in the computation
            _compositeSamples.Last().IsWholeSampleExcluded = true;
            var expectedCompositeSamples = _compositeSamples.Take(4);
            
            var mockCalc = MockRepository.GenerateMock<IBestFitLinePointsCalculator>();
            mockCalc.Expect(c => c.CalculateBestFitLinePoints(null, 0, 0)).IgnoreArguments()
                .Constraints(
                    List.ContainsAll(expectedCompositeSamples), // 1st param (list of points to use in the calculation)
                    Is.Anything(),                              // 2nd param (transudation rate)
                    Is.Anything());                             // 3rd param (stddev)

            var vm = new TestAnalysisViewModelWithNoSettingsReference(mockCalc)
            {
                CurrentTest = new BVATest(null) {Mode = TestMode.Automatic, Status = TestStatus.Completed},
                MeasuredVolume = new BVAVolume(null)
            };
            vm.UnadjustedBloodVolumePoints.AddRange(_viewModels);
            
            vm.RefreshBestFitLinePoints();

            mockCalc.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_unadjusted_blood_volumes_less_than_1_mL_are_not_considered()
        {
            _compositeSamples.Reverse().Skip(1).First().UnadjustedBloodVolume = 0;
            _compositeSamples.Last().UnadjustedBloodVolume = -1;

            var expectedCompositeSamples = _compositeSamples.Take(3);

            var mockCalc = MockRepository.GenerateMock<IBestFitLinePointsCalculator>();
            mockCalc.Expect(c => c.CalculateBestFitLinePoints(null, 0, 0)).IgnoreArguments()
                .Constraints(
                    List.ContainsAll(expectedCompositeSamples), 
                    Is.Anything(), 
                    Is.Anything());

            var vm = new TestAnalysisViewModelWithNoSettingsReference(mockCalc)
            {
                CurrentTest = new BVATest(null) { Mode = TestMode.Automatic, Status = TestStatus.Completed },
                MeasuredVolume = new BVAVolume(null)
            };
            vm.UnadjustedBloodVolumePoints.AddRange(_viewModels);

            vm.RefreshBestFitLinePoints();

            mockCalc.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_those_points_are_based_on_the_best_fit_line_points_calculator()
        {
            var stubCalc = MockRepository.GenerateStub<IBestFitLinePointsCalculator>();
            var stubPoints = new[]
                {
                    new BestFitLinePoint {DependentValue = 10, IndependentValue = 20},
                    new BestFitLinePoint {DependentValue = 30, IndependentValue = 40}
                };
            stubCalc.Expect(c => c.CalculateBestFitLinePoints(null, 0, 0)).IgnoreArguments().Return(stubPoints);

            var vm = new TestAnalysisViewModelWithNoSettingsReference(stubCalc)
            {
                CurrentTest = new BVATest(null) { Mode = TestMode.Automatic, Status = TestStatus.Completed },
                MeasuredVolume = new BVAVolume(null)
            };
            vm.UnadjustedBloodVolumePoints.AddRange(_viewModels);

            vm.RefreshBestFitLinePoints();

            CollectionAssert.AreEqual(stubPoints, vm.BestFitLinePoints);
        }

        [TestMethod]
        public void And_points_already_exist_then_those_points_are_replaced_by_new_points()
        {
            var stubCalc = MockRepository.GenerateStub<IBestFitLinePointsCalculator>();
            var firstStubPoints = new[]
                {
                    new BestFitLinePoint {DependentValue = 10, IndependentValue = 20},
                    new BestFitLinePoint {DependentValue = 30, IndependentValue = 40}
                };
            var secondStubPoints = new[]
                {
                    new BestFitLinePoint {DependentValue = 50, IndependentValue = 60},
                    new BestFitLinePoint {DependentValue = 70, IndependentValue = 80}
                };
            stubCalc.Expect(c => c.CalculateBestFitLinePoints(null, 0, 0))
                    .IgnoreArguments()
                    .Return(firstStubPoints)
                    .Repeat.Once();
            stubCalc.Expect(c => c.CalculateBestFitLinePoints(null, 0, 0))
                    .IgnoreArguments()
                    .Return(secondStubPoints)
                    .Repeat.Once();

            var vm = new TestAnalysisViewModelWithNoSettingsReference(stubCalc)
            {
                CurrentTest = new BVATest(null) { Mode = TestMode.Automatic, Status = TestStatus.Completed },
                MeasuredVolume = new BVAVolume(null)
            };
            vm.UnadjustedBloodVolumePoints.AddRange(_viewModels);

            vm.RefreshBestFitLinePoints();
            vm.RefreshBestFitLinePoints();

            CollectionAssert.AreEqual(secondStubPoints, vm.BestFitLinePoints); 
        }
    }
    // ReSharper restore InconsistentNaming
    // ReSharper restore PossibleMultipleEnumeration
}
