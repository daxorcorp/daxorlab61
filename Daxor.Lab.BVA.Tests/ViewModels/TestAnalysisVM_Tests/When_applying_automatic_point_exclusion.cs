﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestAnalysisVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_applying_automatic_point_exclusion
    {
        //[TestMethod]
        //public void And_the_service_is_disabled_then_the_service_is_not_invoked()
        //{
        //    var mockPointExclusionService = MockRepository.GenerateMock<IAutomaticPointExclusionService>();
        //    mockPointExclusionService.Expect(s => s.IsEnabled).Return(false);
        //    var viewModel = new TestAnalysisViewModelWithNoSettingsReference(mockPointExclusionService);

        //    viewModel.ApplyAutomaticPointExclusion();

        //    mockPointExclusionService.VerifyAllExpectations();
        //}

        //[TestMethod]
        //public void And_the_service_is_enabled_then_the_service_is_invoked_correctly()
        //{
        //    var bvaTest = TestFactory.CreateBvaTest(3);

        //    var stubLogger = MockRepository.GenerateStub<ILoggerFacade>();
        //    var mockPointExclusionService = MockRepository.GenerateMock<IAutomaticPointExclusionService>();
        //    mockPointExclusionService.Expect(s => s.IsEnabled).Return(true);
        //    mockPointExclusionService.Expect(s => s.ExcludePoints(bvaTest));
        //    var viewModel = new TestAnalysisViewModelWithNoSettingsReference(mockPointExclusionService, stubLogger)
        //        {
        //            CurrentTest = bvaTest
        //        };

        //    viewModel.ApplyAutomaticPointExclusion();

        //    mockPointExclusionService.VerifyAllExpectations();
        //}

        //[TestMethod]
        //public void Then_the_service_metadata_is_logged()
        //{
        //    var bvaTest = TestFactory.CreateBvaTest(3);
        //    var stubPointExclusionService = MockRepository.GenerateStub<IAutomaticPointExclusionService>();
        //    var expectedMetadata = new AutomaticPointExclusionMetadata
        //        {
        //            NumberOfPointsExcluded = 1,
        //            WasServiceRunToCompletion = true
        //        };
        //    stubPointExclusionService.Expect(s => s.IsEnabled).Return(true);
        //    stubPointExclusionService.Expect(s => s.ExcludePoints(null)).IgnoreArguments().Return(expectedMetadata);
        //    var mockLogger = MockRepository.GenerateMock<ILoggerFacade>();
        //    mockLogger.Expect(l => l.Log(Arg<string>.Is.Equal("Automatic point exclusion metadata: " + expectedMetadata), Arg<Category>.Is.Equal(Category.Info), Arg<Priority>.Is.Anything));
        //    var viewModel = new TestAnalysisViewModelWithNoSettingsReference(stubPointExclusionService, mockLogger)
        //    {
        //        CurrentTest = bvaTest
        //    };

        //    viewModel.ApplyAutomaticPointExclusion();

        //    mockLogger.VerifyAllExpectations();
        //}
    }
    // ReSharper restore InconsistentNaming
}
