﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Reports.Interfaces;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestAnalysisVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_exporting_a_BVA_test
    {
        private TestAnalysisViewModel _viewModel;
        private ITestExecutionController _testExecutionController;

        [TestInitialize]
        public void BeforeEachTest()
        {
            var aMessageToReturn = new Message("key", "description", "resolution", "ID", MessageType.Alert);

            var stubMessageManager = Substitute.For<IMessageManager>();
            stubMessageManager.GetMessage(Arg.Any<string>()).Returns(aMessageToReturn);

            var stubEventAggregator = Substitute.For<IEventAggregator>();
            stubEventAggregator.GetEvent<ViewReportExecuteChanged>().Returns(new ViewReportExecuteChanged());
            stubEventAggregator.GetEvent<BvaTestSelected>().Returns(new BvaTestSelected());
            stubEventAggregator.GetEvent<AppPartActivationChanged>().Returns(new AppPartActivationChanged());
            stubEventAggregator.GetEvent<TestStarted>().Returns(new TestStarted());
            stubEventAggregator.GetEvent<TestAborted>().Returns(new TestAborted());
            stubEventAggregator.GetEvent<TestCompleted>().Returns(new TestCompleted());
            stubEventAggregator.GetEvent<SampleChangerPositionChangeCompleted>()
                .Returns(new SampleChangerPositionChangeCompleted());

            _testExecutionController = Substitute.For<ITestExecutionController>();

            _viewModel = new TestAnalysisViewModel(Substitute.For<IPopupController>(),
                stubEventAggregator,
                _testExecutionController, Substitute.For<ILoggerFacade>(),
                Substitute.For<IMessageBoxDispatcher>(), Substitute.For<IRegionManager>(),
                Substitute.For<IBloodVolumeTestController>(), Substitute.For<IIdealsCalcEngineService>(),
                Substitute.For<IBvaResultAlertService>(), Substitute.For<IBestFitLinePointsCalculator>(),
                Substitute.For<IAutomaticPointExclusionService>(), Substitute.For<IStarBurnWrapper>(),
                Substitute.For<IAuditTrailDataAccess>(), Substitute.For<IFolderBrowser>(), stubMessageManager,
                Substitute.For<ISettingsManager>(), Substitute.For<IZipFileFactory>(), Substitute.For<IPdfPrinter>())
            {
                CurrentTest = BvaTestFactory.CreateBvaTest(3, 10)
            };
        }

        [TestMethod]
        public void And_a_test_is_currently_executing_then_the_test_can_be_exported()
        {
            _testExecutionController.IsExecuting.Returns(true);
            Assert.IsTrue(_viewModel.ExportReportCommand.CanExecute(null));
        }

        [TestMethod]
        public void And_no_test_is_currently_executing_then_the_test_can_be_exported()
        {
            _testExecutionController.IsExecuting.Returns(false);
            Assert.IsTrue(_viewModel.ExportReportCommand.CanExecute(null));
        }
    }
    // ReSharper restore RedundantArgumentName
}