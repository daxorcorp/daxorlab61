﻿using System;
using System.Collections.Generic;
using System.Text;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Domain.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestAnalysisVM_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_refreshing_the_best_fit_line_points
    {
        [TestMethod]
        public void And_there_is_no_current_test_then_the_existing_best_fit_points_are_not_cleared()
        {
            // Arrange: set up points that should not be cleared
            var vm = new TestAnalysisViewModelWithNoSettingsReference((IBestFitLinePointsCalculator) null);
            vm.BestFitLinePoints.CollectionChanged += (sender, args) => Assert.Fail("BestFitLinePoints was changed");

            // Act
            vm.RefreshBestFitLinePoints();
        }
        
        [TestMethod]
        public void And_the_current_test_is_not_manual_but_not_completed_then_the_existing_best_fit_points_are_not_cleared()
        {
            // ReSharper disable ConvertToConstant.Local
            var currentMode = TestMode.Automatic;
            var currentStatus = TestStatus.Undefined;
            // ReSharper restore ConvertToConstant.Local

            var allowedModes = new List<string>();
            var stubBestFitLineCalculator = MockRepository.GenerateStub<IBestFitLinePointsCalculator>();
            var vm = new TestAnalysisViewModelWithNoSettingsReference(stubBestFitLineCalculator);
            // Store any mode/status combination that ended up clearing the points
            vm.BestFitLinePoints.CollectionChanged += (sender, args) => 
                allowedModes.Add("Mode: " + currentMode + "; Status: " + currentStatus);
            vm.MeasuredVolume = new BVAVolume(null);

            // Act: try refreshing the points for all valid mode/status combinations
            var combinationsToCheck = GetTestModeAndStatusCombinations();
            foreach (var combo in combinationsToCheck)
            {
                vm.CurrentTest = new BVATest(null) {Mode = combo.Item1, Status = combo.Item2};
                vm.RefreshBestFitLinePoints();
            }

            var allowedModeStringList = new StringBuilder();
            foreach (var entry in allowedModes) allowedModeStringList.Append(entry + "\n");

            Assert.AreEqual(0, allowedModes.Count, "The following combinations did not force a return: " + allowedModeStringList);
        }

        [TestMethod]
        public void And_the_current_test_supports_best_fit_line_computation_but_the_calculator_instance_is_null_then_the_existing_best_fit_points_are_not_cleared()
        {
            // Arrange: set up points that should not be cleared
            var vm = new TestAnalysisViewModelWithNoSettingsReference((IBestFitLinePointsCalculator) null);
            vm.BestFitLinePoints.CollectionChanged += (sender, args) => Assert.Fail("BestFitLinePoints was changed");
            
            // A BVA test that is ready for best-fit computation
            vm.CurrentTest = new BVATest(null) { Mode = TestMode.Automatic, Status = TestStatus.Completed };

            // Act
            vm.RefreshBestFitLinePoints();
        }

        [TestMethod]
        public void And_the_current_test_supports_best_fit_line_computation_but_the_measured_volume_is_null_then_the_existing_best_fit_points_are_not_cleared()
        {
            // Arrange: set up points that should not be cleared
            var stubBestFitLineCalculator = MockRepository.GenerateStub<IBestFitLinePointsCalculator>();
            var vm = new TestAnalysisViewModelWithNoSettingsReference(stubBestFitLineCalculator);
            vm.BestFitLinePoints.CollectionChanged += (sender, args) => Assert.Fail("BestFitLinePoints was changed");

            // A BVA test that is ready for best-fit computation
            vm.CurrentTest = new BVATest(null) { Mode = TestMode.Automatic, Status = TestStatus.Completed };

            // Act
            vm.RefreshBestFitLinePoints();
        }

        [TestMethod]
        public void And_conditions_support_best_fit_line_computation_but_the_best_fit_calc_throws_an_exception_then_the_best_fit_line_points_are_cleared()
        {
            // Arrange
            var throwingCalcService = MockRepository.GenerateStub<IBestFitLinePointsCalculator>();
            throwingCalcService.Expect(c => c.CalculateBestFitLinePoints(null, 0, 0))
                           .IgnoreArguments()
                           .Throw(new Exception());
            var vm = new TestAnalysisViewModelWithNoSettingsReference(throwingCalcService)
                {
                    CurrentTest = new BVATest(null) {Mode = TestMode.Automatic, Status = TestStatus.Completed},
                    MeasuredVolume = new BVAVolume(null)
                };
            vm.UnadjustedBloodVolumePoints.AddRange(BloodPointViewModelFactory.CreateBloodVolumePointViewModels(2));

            // Act
            vm.RefreshBestFitLinePoints();

            // Assert
            Assert.AreEqual(0, vm.BestFitLinePoints.Count, "BestFitLinePoints not cleared after exception thrown");
        }

        private IEnumerable<Tuple<TestMode, TestStatus>> GetTestModeAndStatusCombinations()
        {
            // ReSharper disable LoopCanBeConvertedToQuery
            var combinations = new List<Tuple<TestMode, TestStatus>>();
            foreach (var mode in Enum.GetValues(typeof(TestMode)))
            {
                if ((TestMode)mode == TestMode.Manual) continue;
                foreach (var status in Enum.GetValues(typeof (TestStatus)))
                {
                    if ((TestStatus) status == TestStatus.Completed) continue;
                    combinations.Add(new Tuple<TestMode, TestStatus>((TestMode) mode, (TestStatus) status));
                }
            }
            // ReSharper restore LoopCanBeConvertedToQuery

            return combinations;
        }
    }
    // ReSharper restore InconsistentNaming
}
