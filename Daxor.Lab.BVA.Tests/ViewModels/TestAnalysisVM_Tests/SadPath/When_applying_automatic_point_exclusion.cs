﻿using System;
using System.Threading;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestAnalysisVM_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_applying_automatic_point_exclusion
    {
        //[TestMethod]
        //public void And_the_service_instance_is_null_then_a_warning_is_logged()
        //{
        //    var bvaTest = TestFactory.CreateBvaTest(3);
        //    var mockLogger = MockRepository.GenerateMock<ILoggerFacade>();
        //    mockLogger.Expect(l => l.Log(Arg<string>.Is.Anything, Arg<Category>.Is.Equal(Category.Warn), Arg<Priority>.Is.Anything));
        //    var viewModel = new TestAnalysisViewModelWithNoSettingsReference(null, mockLogger)
        //        {
        //            CurrentTest = bvaTest
        //        };

        //    viewModel.ApplyAutomaticPointExclusion();

        //    mockLogger.VerifyAllExpectations();
        //}

        //[TestMethod]
        //public void And_the_current_test_is_null_then_a_warning_is_logged()
        //{
        //    var stubPointExclusionService = MockRepository.GenerateStub<IAutomaticPointExclusionService>();
        //    stubPointExclusionService.Expect(s => s.IsEnabled).Return(true);
        //    var mockLogger = MockRepository.GenerateMock<ILoggerFacade>();
        //    mockLogger.Expect(l => l.Log(Arg<string>.Is.Anything, Arg<Category>.Is.Equal(Category.Warn), Arg<Priority>.Is.Anything));
        //    var viewModel = new TestAnalysisViewModelWithNoSettingsReference(stubPointExclusionService, mockLogger);

        //    viewModel.ApplyAutomaticPointExclusion();

        //    mockLogger.VerifyAllExpectations();
        //}

        //[TestMethod]
        //public void And_the_the_point_exclusion_service_throws_an_exception_then_the_exception_is_logged()
        //{
        //    var bvaTest = TestFactory.CreateBvaTest(3);
        //    var stubPointExclusionService = MockRepository.GenerateStub<IAutomaticPointExclusionService>();
        //    stubPointExclusionService.Expect(s => s.IsEnabled).Return(true);
        //    stubPointExclusionService.Expect(s => s.ExcludePoints(null)).IgnoreArguments().Throw(new Exception());
        //    var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
        //    stubEventAggregator.Expect(a => a.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());
        //    var stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
        //    var mockLogger = MockRepository.GenerateMock<ILoggerFacade>();
        //    mockLogger.Expect(l => l.Log(Arg<string>.Is.Anything, Arg<Category>.Is.Equal(Category.Exception), Arg<Priority>.Is.Anything));
        //    var viewModel = new TestAnalysisViewModelWithNoSettingsReference(stubPointExclusionService, stubMessageBoxDispatcher, mockLogger, stubEventAggregator)
        //        {
        //            CurrentTest = bvaTest
        //        };

        //    viewModel.ApplyAutomaticPointExclusion();

        //    mockLogger.VerifyAllExpectations();
        //}

        //[TestMethod]
        //public void And_the_the_point_exclusion_service_throws_an_exception_then_a_message_box_is_shown()
        //{
        //    var bvaTest = TestFactory.CreateBvaTest(3);
        //    var stubPointExclusionService = MockRepository.GenerateStub<IAutomaticPointExclusionService>();
        //    stubPointExclusionService.Expect(s => s.IsEnabled).Return(true);
        //    stubPointExclusionService.Expect(s => s.ExcludePoints(null)).IgnoreArguments().Throw(new Exception());
        //    var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
        //    stubEventAggregator.Expect(a => a.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());
        //    var stubLogger = MockRepository.GenerateStub<ILoggerFacade>();
        //    var mockMessageBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
        //    var viewModel = new TestAnalysisViewModelWithNoSettingsReference(stubPointExclusionService, mockMessageBoxDispatcher, stubLogger, stubEventAggregator)
        //    {
        //        CurrentTest = bvaTest
        //    };
        //    mockMessageBoxDispatcher.Expect(
        //        d => d.ShowMessageBox(MessageBoxCategory.Error,
        //                              viewModel.AutomaticPointExclusionErrorMessage,
        //                              TestAnalysisViewModel.AutomaticPointExclusionCaption,
        //                              MessageBoxChoiceSet.OK)).Return(MessageBoxReturnResult.OK);
        //    viewModel.ApplyAutomaticPointExclusion();

        //    mockMessageBoxDispatcher.VerifyAllExpectations();
        //}

        //[TestMethod]
        //public void And_the_the_point_exclusion_service_returns_null_metadata_then_an_exception_is_logged()
        //{
        //    var bvaTest = TestFactory.CreateBvaTest(3);
        //    var stubPointExclusionService = MockRepository.GenerateStub<IAutomaticPointExclusionService>();
        //    stubPointExclusionService.Expect(s => s.IsEnabled).Return(true);
        //    stubPointExclusionService.Expect(s => s.ExcludePoints(null)).IgnoreArguments().Return(null);
        //    var mockLogger = MockRepository.GenerateMock<ILoggerFacade>();
        //    mockLogger.Expect(l => l.Log(Arg<string>.Is.Anything, Arg<Category>.Is.Equal(Category.Exception), Arg<Priority>.Is.Anything));
        //    var viewModel = new TestAnalysisViewModelWithNoSettingsReference(stubPointExclusionService, mockLogger)
        //    {
        //        CurrentTest = bvaTest
        //    };

        //    viewModel.ApplyAutomaticPointExclusion();

        //    mockLogger.VerifyAllExpectations();
        //}
    }
    // ReSharper restore InconsistentNaming
}
