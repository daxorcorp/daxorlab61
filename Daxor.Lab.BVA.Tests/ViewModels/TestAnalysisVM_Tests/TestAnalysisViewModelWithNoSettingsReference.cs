using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestAnalysisVM_Tests
{
    internal class TestAnalysisViewModelWithNoSettingsReference : TestAnalysisViewModel
    {
        public TestAnalysisViewModelWithNoSettingsReference(IBestFitLinePointsCalculator calc)
            : base(null, null, null, new EmptyLogger(), null, null, null, null, null, calc, null, null, null, null, null, null, null, null)
        {
        }

        public TestAnalysisViewModelWithNoSettingsReference(IAutomaticPointExclusionService pointExclusionService)
			: base(null, null, null, null, null, null, null, null, null, null, pointExclusionService, null, null, null, null, null, null, null)
        {
        }

        public TestAnalysisViewModelWithNoSettingsReference(IAutomaticPointExclusionService pointExclusionService,
                                                            ILoggerFacade logger)
			: base(null, null, null, logger, null, null, null, null, null, null, pointExclusionService, null, null, null, null, null, null, null)
        {
            
        }

        public TestAnalysisViewModelWithNoSettingsReference(IAutomaticPointExclusionService pointExclusionService,
                                                    IMessageBoxDispatcher messageBoxDispatcher, ILoggerFacade logger, IEventAggregator eventAggregator)
			: base(null, eventAggregator, null, logger, messageBoxDispatcher, null, null, null, null, null, pointExclusionService, null, null, null, null, null, null, null)
        {

        }

        protected override void InitializeViewModel()
        {
            AutomaticPointExclusionErrorMessage = "Automatic point exclusion did not complete successfully.\n\nThe original exclusion state of the samples has been restored. Please call DAXOR Customer Support at 1-888-774-3268. [Error: BVA 34]";
            VolumesMatrix =
                new MatrixVolumesViewModel(
                    new WholeBloodViewModel {Type = BloodType.Measured, PlasmaCount = 0, RedCellCount = 0},
                    new WholeBloodViewModel {Type = BloodType.Ideal, PlasmaCount = 0, RedCellCount = 0});
        }

        protected override void InitializeTestPropertyObserver(BVATest test)
        {
            // Do nothing
        }

        protected override void UpdateMeasuredAndIdealVolumes(BVATest test)
        {
            // Do nothing
        }

        protected override void FirePropertyChanged(string propertyName)
        {
            // Do nothing
        }

        protected override void RefreshBloodVolumeAlert()
        {
            // Do nothing
        }

        protected override void RefreshTransudationAlerts()
        {
            // Do nothing
        }

        protected override void RefreshStandardDeviationAlert()
        {
            // Do nothing
        }
    }
}