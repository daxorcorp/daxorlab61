﻿using System;
using System.Collections.Generic;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestReviewVM_Tests
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_the_BVA_Test_Saved_event_is_raised_for_an_amputee
	{
		private TestReviewViewModel _viewModel;

		private IAuditTrailDataAccess _mockAuditTrailDataAccess;
		private IEventAggregator _stubEventAggregator;

		private BvaTestSaved _testSavedEvent;
		private Guid _fakeGuid;

		private const string _fakeAnalyst = "fakeAnalyst";

		private const int _fakeNewCorrectionFactor = 5;

		[TestInitialize]
		public void Init()
		{
			_fakeGuid = Guid.NewGuid();
			_mockAuditTrailDataAccess = Substitute.For<IAuditTrailDataAccess>();
			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_testSavedEvent = new BvaTestSaved();

			_stubEventAggregator.GetEvent<AlertViolationChanged>().Returns(new AlertViolationChanged());
			_stubEventAggregator.GetEvent<BvaTestSaved>().Returns(_testSavedEvent);
			_stubEventAggregator.GetEvent<BvaTestSelected>().Returns(new BvaTestSelected());
			_stubEventAggregator.GetEvent<TestAborted>().Returns(new TestAborted());
			_stubEventAggregator.GetEvent<TestCompleted>().Returns(new TestCompleted());
			_stubEventAggregator.GetEvent<TestStarted>().Returns(new TestStarted());

			_viewModel = new TestReviewViewModel(Substitute.For<IAppModuleNavigator>(),
				Substitute.For<IBloodVolumeTestController>(), Substitute.For<ITestExecutionController>(),
				Substitute.For<IMessageBoxDispatcher>(),
				_mockAuditTrailDataAccess, Substitute.For<ISettingsManager>(), Substitute.For<IMessageManager>(),
				Substitute.For<ILoggerFacade>(), Substitute.For<INavigationCoordinator>(),
				_stubEventAggregator) { CurrentTest = BvaTestFactory.CreateBvaTest(5, 60) };

			_viewModel.CurrentTest.Analyst = _fakeAnalyst;
			_viewModel.CurrentTest.InternalId = _fakeGuid;
			_viewModel.CurrentTest.Patient.IsAmputee = true;
		}

		[TestMethod]
		public void And_the_ID_of_the_test_does_not_match_the_current_test_ID_then_the_audit_trail_is_not_modified()
		{
			//Act
			_testSavedEvent.Publish(Guid.NewGuid());

			//Assert
			_mockAuditTrailDataAccess.DidNotReceive().Insert(Arg.Any<AuditTrailRecord>());
		}

		[TestMethod]
		public void And_the_audit_trail_includes_an_entry_for_amputee_status_then_the_audit_trail_is_not_modified()
		{
			//Arrange
			_mockAuditTrailDataAccess.SelectList(_fakeGuid).Returns(new List<AuditTrailRecord> { new AuditTrailRecord { ChangeField = "Amputee Status" } });

			//Act
			_testSavedEvent.Publish(_fakeGuid);

			//Assert
			_mockAuditTrailDataAccess.DidNotReceive().Insert(Arg.Any<AuditTrailRecord>());
		}

		[TestMethod]
		public void And_the_audit_trail_does_not_include_an_entry_for_amputee_status_then_an_entry_for_amputee_access_is_added()
		{
			//Arrange
			_mockAuditTrailDataAccess.SelectList(_fakeGuid).Returns(new List<AuditTrailRecord>());

			//Act
			_testSavedEvent.Publish(_fakeGuid);

			//Assert
			var expectedAuditTrailRecord = new AuditTrailRecord
			{
				TestId = _fakeGuid,
				ChangeTable = "tblPATIENT",
				ChangeTime = DateTime.Now.ToString("M/d/yyyy H:mm:ss"),
				OldValue = "Non-amputee",
				NewValue = "Amputee",
				ChangedBy = _fakeAnalyst,
				ChangeField = "Amputee Status"
			};

			_mockAuditTrailDataAccess.Received(1).Insert(Arg.Is<AuditTrailRecord>(actualAuditTrailRecord => AreTwoAuditTrailRecordsEqual(actualAuditTrailRecord, expectedAuditTrailRecord)));
		}

		[TestMethod]
		public void And_the_audit_trail_does_not_include_an_entry_for_amputee_status_and_the_correction_factor_has_not_been_changed_then_only_an_entry_for_amputee_status_is_added()
		{
			//Arrange
			_mockAuditTrailDataAccess.SelectList(_fakeGuid).Returns(new List<AuditTrailRecord>());

			//Act
			_testSavedEvent.Publish(_fakeGuid);

			//Assert
			_mockAuditTrailDataAccess.DidNotReceive().Insert(Arg.Is<AuditTrailRecord>(r => r.ChangeField == "Amputee Ideals Correction Factor"));
		}

		[TestMethod]
		public void And_the_audit_trail_does_not_include_an_entry_for_amputee_status_and_the_correction_factor_has_been_changed_then_an_entry_for_the_correction_factor_is_added()
		{
			//Arrange
			_mockAuditTrailDataAccess.SelectList(_fakeGuid).Returns(new List<AuditTrailRecord>());
			_viewModel.CurrentTest.AmputeeIdealsCorrectionFactor = _fakeNewCorrectionFactor;

			//Act
			_testSavedEvent.Publish(_fakeGuid);

			//Assert
			var expectedAuditTrailRecord = new AuditTrailRecord
			{
				TestId = _fakeGuid,
				ChangeTable = "tblBVA_TEST",
				ChangeTime = DateTime.Now.ToString("M/d/yyyy H:mm:ss"),
				OldValue = "0%",
				NewValue = "-" + _fakeNewCorrectionFactor + "%",
				ChangedBy = _fakeAnalyst,
				ChangeField = "Amputee Ideals Correction Factor"
			};

			_mockAuditTrailDataAccess.Received(1).Insert(Arg.Is<AuditTrailRecord>(actualAuditTrailRecord => AreTwoAuditTrailRecordsEqual(actualAuditTrailRecord, expectedAuditTrailRecord)));
		}

		private bool AreTwoAuditTrailRecordsEqual(AuditTrailRecord left, AuditTrailRecord right)
		{
			return left.ChangeField == right.ChangeField && left.TestId == right.TestId && left.ChangeTable == right.ChangeTable
			       && !string.IsNullOrEmpty(left.ChangeTime) && !string.IsNullOrEmpty(right.ChangeTime) &&
			       left.OldValue == right.OldValue && left.NewValue == right.NewValue && left.ChangedBy == right.ChangedBy;
		}
	}
	// ReSharper restore InconsistentNaming
}