﻿using Daxor.Lab.BVA.Common;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestReviewVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_overriding_the_QC_due_notice
    {
        [TestMethod]
        [RequirementValue("{0} QC due notice overridden by the analyst.")]
        public void Then_the_message_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), Constants.QCDueNoticeOverriddenFormatMessage);
        }
    }
    // ReSharper restore InconsistentNaming
}
