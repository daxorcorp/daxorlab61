﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestReviewVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_BVATestSelectedEvent_is_raised
    {
        [TestMethod]
        [TestCategory("BVA")]
        [TestCategory("ViewModel")]
        public void Then_the_selected_test_is_that_events_payload()
        {
            //
            // Arrange
            //
            var eventAggregator = new EventAggregator();
            var viewModel = new TestReviewVmThatSubscribesToBvaTestSelectedEvent(eventAggregator);


            //
            // Act
            //
            var selectedTest = new BVATest(null) {Patient = new BVAPatient(null)};
            eventAggregator.GetEvent<BvaTestSelected>().Publish(selectedTest);


            //
            // Assert
            //
            var currentTest = viewModel.CurrentTest;
            Assert.AreEqual(currentTest.GetHashCode(), selectedTest.GetHashCode(),
                "Current test doesn't match the selected test.");
        }
    }
    // ReSharper restore InconsistentNaming

    internal class TestReviewVmThatSubscribesToBvaTestSelectedEvent : TestReviewViewModel
    {
        public TestReviewVmThatSubscribesToBvaTestSelectedEvent(IEventAggregator eventAggregator)
            : base(null, null, null, null, null, null, null, null, null, eventAggregator)
        {
            InitializeDelegateCommands();
        }

        protected override void InitializeViewModel()
        {
            SubscribeToBvaTestSelectedEvent();
        }

        protected override void InitializeTestAndPatientPropertyObservers(BVATest test)
        {
            // Do nothing
        }

        protected override void UpdateSampleValueList()
        {
            // Do nothing
        }
    }
}
