﻿using System;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestReviewVM_Tests
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_the_BVA_Test_Saved_event_is_raised_for_a_non_amputee
	{
		private TestReviewViewModel _viewModel;

		private IAuditTrailDataAccess _mockAuditTrailDataAccess;
		private IEventAggregator _stubEventAggregator;

		private BvaTestSaved _testSavedEvent;

		private Guid _fakeGuid;

		[TestInitialize]
		public void Init()
		{
			_fakeGuid = Guid.NewGuid();
			_mockAuditTrailDataAccess = Substitute.For<IAuditTrailDataAccess>();
			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_testSavedEvent = new BvaTestSaved();

			_stubEventAggregator.GetEvent<AlertViolationChanged>().Returns(new AlertViolationChanged());
			_stubEventAggregator.GetEvent<BvaTestSaved>().Returns(_testSavedEvent);
			_stubEventAggregator.GetEvent<BvaTestSelected>().Returns(new BvaTestSelected());
			_stubEventAggregator.GetEvent<TestAborted>().Returns(new TestAborted());
			_stubEventAggregator.GetEvent<TestCompleted>().Returns(new TestCompleted());
			_stubEventAggregator.GetEvent<TestStarted>().Returns(new TestStarted());

			_viewModel = new TestReviewViewModel(Substitute.For<IAppModuleNavigator>(),
				Substitute.For<IBloodVolumeTestController>(), Substitute.For<ITestExecutionController>(),
				Substitute.For<IMessageBoxDispatcher>(),
				_mockAuditTrailDataAccess, Substitute.For<ISettingsManager>(), Substitute.For<IMessageManager>(),
				Substitute.For<ILoggerFacade>(), Substitute.For<INavigationCoordinator>(),
				_stubEventAggregator) {CurrentTest = BvaTestFactory.CreateBvaTest(5, 60)};

			_viewModel.CurrentTest.InternalId = _fakeGuid;
			_viewModel.CurrentTest.Patient.IsAmputee = false;
		}

		[TestMethod]
		public void And_the_ID_matches_the_current_test_then_the_audit_trail_is_not_modified()
		{
			//Act
			_testSavedEvent.Publish(_fakeGuid);

			//Assert
			_mockAuditTrailDataAccess.DidNotReceive().Insert(Arg.Any<AuditTrailRecord>());
		}
	}
	// ReSharper restore InconsistentNaming
}