using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestParametersVM_Tests
{
    internal class TestParametersVmWithCustomController : TestParametersViewModel
    {
        public TestParametersVmWithCustomController(IBloodVolumeTestController controller)
            : base(null, controller, null, null, null, null, null, Substitute.For<ISettingsManager>(), Substitute.For<IEventAggregator>(), null)
        {
        }

        public TestParametersVmWithCustomController(IBloodVolumeTestController controller, ISettingsManager settingsManager, IEventAggregator eventAggregator)
            : base(null, controller, null, null, null, null, null, settingsManager, eventAggregator, null)
        {
        }

        protected override void EnsureDefaultSelectionValues()
        {
            // Do nothing
        }
    }
}