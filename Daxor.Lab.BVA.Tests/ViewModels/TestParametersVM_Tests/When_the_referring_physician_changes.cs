using System.Collections;
using System.Linq;
using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestParametersVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_referring_physician_changes
    {
        [TestMethod]
        public void And_there_is_no_test_then_the_referring_physicians_list_is_empty()
        {
            // Arrange
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            stubBvaTest.RefPhysician = "Susan";

            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(),Substitute.For<ISettingsManager>(), CreateBasicStubAggregator())
            {
                ReferringPhysicians = { "Joe", "Bob" },
                CurrentTest = stubBvaTest
            };
            viewModel.CurrentTest = null;
            
            // Act
            stubBvaTest.RefPhysician = "";

            // Assert
            Assert.AreEqual(0, viewModel.ReferringPhysicians.Count);
        }
        
        [TestMethod]
        public void And_the_new_name_is_empty_then_the_referring_physicians_list_is_empty()
        {
            // Arrange
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            stubBvaTest.RefPhysician = "Susan";

            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(), Substitute.For<ISettingsManager>(), CreateBasicStubAggregator())
            {
                ReferringPhysicians = { "Joe", "Bob" },
                CurrentTest = stubBvaTest
            };

            // Act
            stubBvaTest.RefPhysician = "";

            // Assert
            Assert.AreEqual(0, viewModel.ReferringPhysicians.Count);
        }

        [TestMethod]
        public void And_the_referring_physicians_list_had_items_then_the_list_is_repopulated()
        {
            // Arrange
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            stubBvaTest.RefPhysician = "Susan";

            var stubSettingsManager = Substitute.For<ISettingsManager>();
            var expectedPhysicians = new[] {"Jimbo", "Jimmy"};
            stubSettingsManager.GetSetting<IEnumerable>(SettingKeys.BvaReferringPhysicians).Returns(expectedPhysicians.ToList());

            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(), stubSettingsManager, CreateBasicStubAggregator())
            {
                ReferringPhysicians = { "Joe", "Bob" },
                CurrentTest = stubBvaTest
            };

            // Act
            stubBvaTest.RefPhysician = "Jim";

            // Assert
            CollectionAssert.AreEqual(expectedPhysicians, viewModel.ReferringPhysicians);
        }

        [TestMethod]
        public void Then_the_physicians_specialty_is_updated()
        {
            // Arrange
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            stubBvaTest.RefPhysician = "Susan";

            var mockDataService = Substitute.For<IBVADataService>();
            
            var controller = Substitute.For<IBloodVolumeTestController>();
            controller.DataService.Returns(mockDataService);
            
            // ReSharper disable once UnusedVariable
            var viewModel = new TestParametersVmWithCustomController(controller,Substitute.For<ISettingsManager>(), CreateBasicStubAggregator())
            {
                CurrentTest = stubBvaTest
            };

            // Act
            stubBvaTest.RefPhysician = "Jim";

            // Assert
            mockDataService.Received().GetMostRecentSpecialty("Jim");
        }

        private static IEventAggregator CreateBasicStubAggregator()
        {
            var dummyEventAggregator = Substitute.For<IEventAggregator>();
            dummyEventAggregator.GetEvent<BvaTestSelected>().Returns(new BvaTestSelected());
            dummyEventAggregator.GetEvent<TestSetupTabItemAdded>().Returns(new TestSetupTabItemAdded());

            return dummyEventAggregator;
        }
    }
    // ReSharper restore InconsistentNaming
}
