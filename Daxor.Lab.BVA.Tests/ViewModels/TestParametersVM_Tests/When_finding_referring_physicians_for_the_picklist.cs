using System.Collections;
using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestParametersVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_finding_referring_physicians_for_the_picklist
    {
        [TestMethod]
        public void Then_the_referring_physicians_setting_is_used()
        {
            // Arrange
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            stubBvaTest.RefPhysician = "Susan";

            var mockSettingsManager = Substitute.For<ISettingsManager>();
            
            // ReSharper disable once UnusedVariable
            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(), mockSettingsManager, CreateBasicStubAggregator())
            {
                CurrentTest = stubBvaTest
            };

            // Act
            stubBvaTest.RefPhysician = "Jim";

            // Assert
            mockSettingsManager.Received().GetSetting<IEnumerable>(SettingKeys.BvaReferringPhysicians);
        }

        [TestMethod]
        public void Then_matches_at_the_beginning_of_the_names_are_returned()
        {
            // Arrange
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            stubBvaTest.RefPhysician = "Susan";

            var physicians = new[] {"Jimbo", "Jimmy", "Alice"};
            var stubSettingsManager = Substitute.For<ISettingsManager>();
            stubSettingsManager.GetSetting<IEnumerable>(SettingKeys.BvaReferringPhysicians).Returns(physicians);

            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(), stubSettingsManager, CreateBasicStubAggregator())
            {
                CurrentTest = stubBvaTest
            };

            // Act
            stubBvaTest.RefPhysician = "Jim";

            // Assert
            var expectedPhysicians = new[] {"Jimbo", "Jimmy"};
            CollectionAssert.AreEqual(expectedPhysicians, viewModel.ReferringPhysicians);
        }

        [TestMethod]
        public void Then_matches_in_the_middle_of_the_names_are_returned()
        {
            // Arrange
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            stubBvaTest.RefPhysician = "Susan";

            var physicians = new[] { "Jimbo", "Jimmy", "Kimmy", "Alice" };
            var stubSettingsManager = Substitute.For<ISettingsManager>();
            stubSettingsManager.GetSetting<IEnumerable>(SettingKeys.BvaReferringPhysicians).Returns(physicians);

            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(), stubSettingsManager, CreateBasicStubAggregator())
            {
                CurrentTest = stubBvaTest
            };

            // Act
            stubBvaTest.RefPhysician = "im";

            // Assert
            var expectedPhysicians = new[] { "Jimbo", "Jimmy", "Kimmy" };
            CollectionAssert.AreEqual(expectedPhysicians, viewModel.ReferringPhysicians);
        }

        [TestMethod]
        public void Then_matches_at_the_end_of_the_names_are_returned()
        {
            // Arrange
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            stubBvaTest.RefPhysician = "Susan";

            var physicians = new[] { "Jimbo", "Jimmy", "Kimmy", "Alice" };
            var stubSettingsManager = Substitute.For<ISettingsManager>();
            stubSettingsManager.GetSetting<IEnumerable>(SettingKeys.BvaReferringPhysicians).Returns(physicians);

            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(), stubSettingsManager, CreateBasicStubAggregator())
            {
                CurrentTest = stubBvaTest
            };

            // Act
            stubBvaTest.RefPhysician = "my";

            // Assert
            var expectedPhysicians = new[] { "Jimmy", "Kimmy" };
            CollectionAssert.AreEqual(expectedPhysicians, viewModel.ReferringPhysicians);
        }

        [TestMethod]
        public void Then_case_does_not_matter()
        {
            // Arrange
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            stubBvaTest.RefPhysician = "Susan";

            var physicians = new[] { "Imala", "Jimmy", "Maxim", "Alice" };
            var stubSettingsManager = Substitute.For<ISettingsManager>();
            stubSettingsManager.GetSetting<IEnumerable>(SettingKeys.BvaReferringPhysicians).Returns(physicians);

            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(), stubSettingsManager, CreateBasicStubAggregator())
            {
                CurrentTest = stubBvaTest
            };

            // Act
            stubBvaTest.RefPhysician = "IM";

            // Assert
            var expectedPhysicians = new[] { "Imala", "Jimmy", "Maxim" };
            CollectionAssert.AreEqual(expectedPhysicians, viewModel.ReferringPhysicians);
        }

        private static IEventAggregator CreateBasicStubAggregator()
        {
            var dummyEventAggregator = Substitute.For<IEventAggregator>();
            dummyEventAggregator.GetEvent<BvaTestSelected>().Returns(new BvaTestSelected());
            dummyEventAggregator.GetEvent<TestSetupTabItemAdded>().Returns(new TestSetupTabItemAdded());

            return dummyEventAggregator;
        }
    }
    // ReSharper restore InconsistentNaming
}
