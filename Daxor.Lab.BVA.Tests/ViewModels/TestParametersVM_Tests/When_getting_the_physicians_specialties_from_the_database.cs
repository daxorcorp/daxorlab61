using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestParametersVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_physicians_specialties_from_the_database
    {
        [TestMethod]
        public void Then_the_settings_manager_is_used()
        {
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            var stubController = Substitute.For<IBloodVolumeTestController>();
            var stubSettingsManager = Substitute.For<ISettingsManager>();
            var viewModel = new TestParametersVmWithCustomController(stubController, stubSettingsManager, CreateBasicStubAggregator())
            {
                CurrentTest = stubBvaTest
            };

            // ReSharper disable once UnusedVariable
            var unused = viewModel.PhysiciansSpecialties;

            stubSettingsManager.Received().GetSetting<IEnumerable>(SettingKeys.BvaPhysiciansSpecialties);
        }

        private static IEventAggregator CreateBasicStubAggregator()
        {
            var dummyEventAggregator = Substitute.For<IEventAggregator>();
            dummyEventAggregator.GetEvent<BvaTestSelected>().Returns(new BvaTestSelected());
            dummyEventAggregator.GetEvent<TestSetupTabItemAdded>().Returns(new TestSetupTabItemAdded());

            return dummyEventAggregator;
        }
    }
    // ReSharper restore InconsistentNaming
}
