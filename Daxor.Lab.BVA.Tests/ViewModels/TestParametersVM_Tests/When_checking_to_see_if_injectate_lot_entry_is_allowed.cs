﻿using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestParametersVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_checking_to_see_if_injectate_lot_entry_is_allowed
    {
        private BVATest _fakeBvaTest;
        private ISettingsManager _stubSettingsManager;
        private IEventAggregator _stubEventAggregator;
        private IBloodVolumeTestController _stubBloodVolumeTestController;

        [TestInitialize]
        public void Initialize()
        {
            _fakeBvaTest = BvaTestFactory.CreateBvaTest(6, 10);
            _stubSettingsManager = Substitute.For<ISettingsManager>();
            _stubBloodVolumeTestController = Substitute.For<IBloodVolumeTestController>();
            _stubEventAggregator = Substitute.For<IEventAggregator>();

            _stubEventAggregator.GetEvent<BvaTestSelected>().Returns(new BvaTestSelected());
            _stubEventAggregator.GetEvent<TestSetupTabItemAdded>().Returns(new TestSetupTabItemAdded());
            _stubEventAggregator.GetEvent<ScannerInputReceived>().Returns(new ScannerInputReceived());

        }

        [TestMethod]
        public void And_the_status_is_pending_for_any_test_type_then_injectate_lot_entry_is_allowed()
        {
            _stubSettingsManager.GetSetting<bool>(SettingKeys.SystemInjectateVerificationEnabled).Returns(false);

            var testModes = new[] {TestMode.Automatic, TestMode.Manual, TestMode.VOPS};
            _fakeBvaTest.Status = TestStatus.Pending;

            foreach (var mode in testModes)
            {
                _fakeBvaTest.Mode = mode;

                var viewModel = new TestParametersVmWithCustomController(_stubBloodVolumeTestController, _stubSettingsManager, _stubEventAggregator)
                {
                    CurrentTest = _fakeBvaTest,
                    IsActive = true
                };

                Assert.IsTrue(viewModel.InjectateLotEntryRequired, "Pending test in mode " + mode);
            }
        }

        [TestMethod]
        public void And_the_test_is_manual_for_any_status_then_injectate_lot_entry_is_allowed()
        {
            _stubSettingsManager.GetSetting<bool>(SettingKeys.SystemInjectateVerificationEnabled).Returns(false);
            var testStatuses = new[] { TestStatus.Aborted, TestStatus.Completed, TestStatus.Deleted, TestStatus.Running, TestStatus.Undefined };

            _fakeBvaTest.Mode = TestMode.Manual;
            foreach (var status in testStatuses)
            {
                _fakeBvaTest.Status = status;
                
                var viewModel = new TestParametersVmWithCustomController(_stubBloodVolumeTestController,
                    _stubSettingsManager, _stubEventAggregator)
                {
                    CurrentTest = _fakeBvaTest,
                    IsActive = true
                };

                Assert.IsTrue(viewModel.InjectateLotEntryRequired, "Manual test with status " + status);
            }
            
            _stubSettingsManager.GetSetting<bool>(SettingKeys.SystemInjectateVerificationEnabled).Returns(true);
            foreach (var status in testStatuses)
            {
                _fakeBvaTest.Status = status;

                var viewModel = new TestParametersVmWithCustomController(_stubBloodVolumeTestController,
                    _stubSettingsManager, _stubEventAggregator)
                {
                    CurrentTest = _fakeBvaTest,
                    IsActive = true
                };

                Assert.IsTrue(viewModel.InjectateLotEntryRequired, "Manual test with status " + status);
            }
        }

        [TestMethod]
        public void And_injectate_verification_is_enabled_for_any_test_mode_other_than_manual_and_for_any_status_then_injectate_lot_entry_is_prohibited()
        {
            _stubSettingsManager.GetSetting<bool>(SettingKeys.SystemInjectateVerificationEnabled).Returns(true);
            var testStatuses = new[] { TestStatus.Aborted, TestStatus.Completed, TestStatus.Deleted, TestStatus.Running, TestStatus.Undefined, TestStatus.Pending };
            var testModes = new[] { TestMode.Automatic, TestMode.VOPS };

            foreach (var status in testStatuses)
            {
                _fakeBvaTest.Status = status;

                foreach (var mode in testModes)
                {
                    _fakeBvaTest.Mode = mode;

                    var viewModel = new TestParametersVmWithCustomController(_stubBloodVolumeTestController,
                        _stubSettingsManager, _stubEventAggregator)
                    {
                        CurrentTest = _fakeBvaTest,
                        IsActive = true
                    };

                    Assert.IsFalse(viewModel.InjectateLotEntryRequired, "Test status is '" + status + "', test mode is '" + mode + "'");
                }
            }
        }

        [TestMethod]
        public void And_the_test_status_is_something_other_than_pending_and_not_manual_then_injectate_lot_entry_is_prohibited()
        {
            _stubSettingsManager.GetSetting<bool>(SettingKeys.SystemInjectateVerificationEnabled).Returns(false);
            var testStatuses = new[] { TestStatus.Aborted, TestStatus.Completed, TestStatus.Deleted, TestStatus.Running, TestStatus.Undefined };
            var testModes = new[] { TestMode.Automatic, TestMode.VOPS };

            foreach (var status in testStatuses)
            {
                _fakeBvaTest.Status = status;

                foreach (var mode in testModes)
                {
                    _fakeBvaTest.Mode = mode;

                    var viewModel = new TestParametersVmWithCustomController(_stubBloodVolumeTestController,
                        _stubSettingsManager, _stubEventAggregator)
                    {
                        CurrentTest = _fakeBvaTest,
                        IsActive = true
                    };

                    Assert.IsFalse(viewModel.InjectateLotEntryRequired, "Test status is '" + status + "', test mode is '" + mode + "'");
                }
            }
        }
    }
    // ReSharper restore InconsistentNaming
}