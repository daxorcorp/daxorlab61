using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections;
using System.Linq;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestParametersVM_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_physicians_specialty_field
    {
        [TestMethod]
        public void Then_a_normal_value_can_be_retrieved_and_set()
        {
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            stubBvaTest.PhysiciansSpecialty = "ICU";

            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(), Substitute.For<ISettingsManager>(), CreateBasicStubController())
            {
                CurrentTest = stubBvaTest
            };

            Assert.AreEqual("ICU", viewModel.CurrentTest.PhysiciansSpecialty);
        }

        [TestMethod]
        public void Then_setting_a_normal_value_raises_PropertyChanged()
        {
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            stubBvaTest.PhysiciansSpecialty = "ICU";

            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(), Substitute.For<ISettingsManager>(), CreateBasicStubController())
            {
                CurrentTest = stubBvaTest
            };

            var wasRaised = false;
            viewModel.CurrentTest.PropertyChanged += (sender, args) => { wasRaised = args.PropertyName == "PhysiciansSpecialty"; };

            viewModel.CurrentTest.PhysiciansSpecialty = "Cardiology";

            Assert.IsTrue(wasRaised);

        }

        [TestMethod]
        public void Then_the_initial_physicians_specialty_is_null()
        {
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(), Substitute.For<ISettingsManager>(), CreateBasicStubController())
            {
                CurrentTest = stubBvaTest
            };

            Assert.AreEqual(null, viewModel.CurrentTest.PhysiciansSpecialty);
        }

        [TestMethod]
        public void Then_setting_the_specialty_list_fires_PropertyChanged_on_the_list()
        {
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);

            var priorSpecialties = (new[] { "A" }).ToArray();
            var expectedSpecialties = (new[] { "A", "B" }).ToArray();
            var stubSettingsManager = Substitute.For<ISettingsManager>();
            stubSettingsManager.GetSetting<IEnumerable>(SettingKeys.BvaPhysiciansSpecialties).Returns(priorSpecialties, expectedSpecialties.ToList());

            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(), stubSettingsManager, CreateBasicStubController())
            {
                CurrentTest = stubBvaTest
            };

            var wasRaised = false;
            viewModel.PropertyChanged += (sender, args) => { wasRaised = args.PropertyName == "PhysiciansSpecialties"; };

            stubSettingsManager.SetSetting(SettingKeys.BvaPhysiciansSpecialties, "A|B");
            stubSettingsManager.SettingsChanged += Raise.EventWith(stubSettingsManager, new SettingsChangedEventArgs(new[] { SettingKeys.BvaPhysiciansSpecialties }));

            Assert.IsTrue(wasRaised);
        }

        [TestMethod]
        public void And_the_specialty_collection_setting_changes_then_the_physician_specialty_list_matches_the_changed_setting()
        {
            // Arrange
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);

            var priorSpecialties = (new[] {"A"}).ToArray();
            var expectedSpecialties = (new[] {"A", "B"}).ToArray();
            var stubSettingsManager = Substitute.For<ISettingsManager>();
            stubSettingsManager.GetSetting<IEnumerable>(SettingKeys.BvaPhysiciansSpecialties).Returns(priorSpecialties, expectedSpecialties.ToList());

            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(), stubSettingsManager, CreateBasicStubController())
            {
                CurrentTest = stubBvaTest
            };

            CollectionAssert.AreEqual(priorSpecialties, viewModel.PhysiciansSpecialties);
            
            // Act
            stubSettingsManager.SetSetting(SettingKeys.BvaPhysiciansSpecialties, "A|B");
            stubSettingsManager.SettingsChanged += Raise.EventWith(stubSettingsManager, new SettingsChangedEventArgs(new[] { SettingKeys.BvaPhysiciansSpecialties }));

            // Assert
            CollectionAssert.AreEqual(expectedSpecialties, viewModel.PhysiciansSpecialties);
        }
        
        private static IEventAggregator CreateBasicStubController()
        {
            var dummyEventAggregator = Substitute.For<IEventAggregator>();
            dummyEventAggregator.GetEvent<BvaTestSelected>().Returns(new BvaTestSelected());
            dummyEventAggregator.GetEvent<TestSetupTabItemAdded>().Returns(new TestSetupTabItemAdded());

            return dummyEventAggregator;
        }
    }
    // ReSharper restore InconsistentNaming
}
