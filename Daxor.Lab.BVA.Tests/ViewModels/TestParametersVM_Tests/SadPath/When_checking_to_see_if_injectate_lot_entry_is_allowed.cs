﻿using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestParametersVM_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_checking_to_see_if_injectate_lot_entry_is_allowed
    {
        private IEventAggregator _stubEventAggregator;
        private IBloodVolumeTestController _stubBloodVolumeTestController;

        [TestInitialize]
        public void Initialize()
        {
            _stubBloodVolumeTestController = Substitute.For<IBloodVolumeTestController>();
            _stubEventAggregator = Substitute.For<IEventAggregator>();

            _stubEventAggregator.GetEvent<BvaTestSelected>().Returns(new BvaTestSelected());
            _stubEventAggregator.GetEvent<TestSetupTabItemAdded>().Returns(new TestSetupTabItemAdded());
            _stubEventAggregator.GetEvent<ScannerInputReceived>().Returns(new ScannerInputReceived());

            _stubBloodVolumeTestController.EventAggregator.Returns(_stubEventAggregator);
        }

        [TestMethod]
        public void And_the_test_is_null_then_the_injectate_lot_field_is_disabled()
        {
            var viewModel = new TestParametersVmWithCustomController(_stubBloodVolumeTestController, Substitute.For<ISettingsManager>(), _stubEventAggregator)
            {
                CurrentTest = null,
                IsActive = true
            };

            Assert.IsFalse(viewModel.InjectateLotEntryRequired);
        }
    }
    // ReSharper restore RedundantArgumentName
}