using System;
using System.Collections;
using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Tests.ViewModels.TestParametersVM_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_finding_referring_physicians_for_the_picklist
    {
        [TestMethod]
        public void And_the_settings_manager_throws_an_exception_then_physicians_list_is_empty()
        {
            // Arrange
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            stubBvaTest.RefPhysician = "Susan";

            var stubSettingsManager = Substitute.For<ISettingsManager>();
            stubSettingsManager.GetSetting<IEnumerable>(SettingKeys.BvaReferringPhysicians).Returns(args => { throw new Exception(); });

            var viewModel = new TestParametersVmWithCustomController(Substitute.For<IBloodVolumeTestController>(), stubSettingsManager, CreateBasicStubAggregator())
            {
                CurrentTest = stubBvaTest
            };

            // Act
            stubBvaTest.RefPhysician = "IM";

            // Assert
            Assert.AreEqual(0, viewModel.ReferringPhysicians.Count);
        }

        [TestMethod]
        public void And_the_settings_manager_throws_an_exception_then_a_message_is_logged()
        {
            // Arrange
            var stubBvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            stubBvaTest.RefPhysician = "Susan";

            var stubSettingsManager = Substitute.For<ISettingsManager>();
            stubSettingsManager.GetSetting<IEnumerable>(SettingKeys.BvaReferringPhysicians).Returns(args => { throw new Exception(); });

            var mockLogger = Substitute.For<ILoggerFacade>();
            var testController = Substitute.For<IBloodVolumeTestController>();
            testController.Logger.Returns(mockLogger);
// ReSharper disable once UnusedVariable
            var viewModel = new TestParametersVmWithCustomController(testController, stubSettingsManager, CreateBasicStubAggregator())
            {
                CurrentTest = stubBvaTest
            };

            // Act
            stubBvaTest.RefPhysician = "IM";

            // Assert
            mockLogger.Received().Log(Arg.Is<string>(s => s.StartsWith("Unable to load referring physicians from settings DB")), 
                Category.Exception, Priority.High); 
        }

        private static IEventAggregator CreateBasicStubAggregator()
        {
            var dummyEventAggregator = Substitute.For<IEventAggregator>();
            dummyEventAggregator.GetEvent<BvaTestSelected>().Returns(new BvaTestSelected());
            dummyEventAggregator.GetEvent<TestSetupTabItemAdded>().Returns(new TestSetupTabItemAdded());

            return dummyEventAggregator;
        }
    }
    // ReSharper restore InconsistentNaming
}
