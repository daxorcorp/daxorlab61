﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.BvaRuleEngine_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_the_baseline_to_background_count_ratio
    {
        [TestMethod]
        [RequirementValue(2)]
        public void Then_the_maximum_allowable_ratio_matches_requirements()
        {
            // Baseline cannot be less than half the background
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), BvaDomainConstants.MaximumMultipleOfBaselineForBackgroundCounts);
        }
    }
    // ReSharper restore InconsistentNaming
}
