﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.BvaRuleEngine_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_transudation_rate_rules
    {
        [TestMethod]
        [RequirementValue(StaticRuleEvaluatorsTestsHelper.MinimumHighRate)]
        public void Then_the_minimum_high_transudation_rate_boundary_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsDouble(), BvaDomainConstants.MinimumHighTransudationRate);
        }

        [TestMethod]
        [RequirementValue(StaticRuleEvaluatorsTestsHelper.MaximumHighRate)]
        public void Then_the_maximum_high_transudation_rate_boundary_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsDouble(), BvaDomainConstants.MaximumHighTransudationRate);
        }
    }
    // ReSharper restore InconsistentNaming
}
