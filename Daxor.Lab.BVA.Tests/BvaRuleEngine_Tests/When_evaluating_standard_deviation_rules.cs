﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.BvaRuleEngine_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_standard_deviation_rules
    {
        [TestMethod]
        [RequirementValue(0.039)]
        public void Then_the_maximum_standard_deviation_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsDouble(), BvaDomainConstants.MaximumBloodVolumePointStandardDeviation);
        }
    }
    // ReSharper restore InconsistentNaming
}
