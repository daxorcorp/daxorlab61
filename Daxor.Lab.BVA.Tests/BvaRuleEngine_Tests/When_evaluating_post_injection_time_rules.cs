﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.BvaRuleEngine_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_post_injection_time_rules
    {
        [TestMethod]
        [RequirementValue(75*60)]
        public void Then_the_maximum_post_injection_time_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), BvaDomainConstants.MaximumPostInjectionTimeInSeconds);
        }
    }
    // ReSharper restore InconsistentNaming
}
