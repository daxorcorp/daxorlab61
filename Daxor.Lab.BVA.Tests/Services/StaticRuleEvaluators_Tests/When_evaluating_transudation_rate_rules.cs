﻿using Daxor.Lab.BVA.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_transudation_rate_rules
    {
        [TestMethod]
        public void And_the_transudation_rate_is_normal_then_the_rules_work_correctly()
        {
            var bvTest = new BVATest(null);
            var expectedRuleState = new TransudationRuleEvaluationState
            {
                IsNotHigh = true,
                IsNotUnusuallyHigh = true,
                IsPositive = true
            };

            bvTest.TransudationRateResult = 0.001;
            TransudationRuleEvaluationState observedRuleState = StaticRuleEvaluatorsTestsHelper.EvalulateRules(bvTest);

            Assert.AreEqual(expectedRuleState, observedRuleState, StaticRuleEvaluatorsTestsHelper.GenerateFailureString(observedRuleState, expectedRuleState));
        }

        [TestMethod]
        public void And_the_transudation_rate_is_at_the_lower_end_for_high_then_the_rules_work_correctly()
        {
            var bvTest = new BVATest(null);
            var expectedRuleState = new TransudationRuleEvaluationState
            {
                IsNotHigh = false,
                IsNotUnusuallyHigh = true,
                IsPositive = true
            };

            bvTest.TransudationRateResult = StaticRuleEvaluatorsTestsHelper.MinimumHighRate;
            var observedRuleState = StaticRuleEvaluatorsTestsHelper.EvalulateRules(bvTest);

            Assert.AreEqual(expectedRuleState, observedRuleState, StaticRuleEvaluatorsTestsHelper.GenerateFailureString(observedRuleState, expectedRuleState));
        }

        [TestMethod]
        public void And_the_transudation_rate_is_high_then_the_rules_work_correctly()
        {
            var bvTest = new BVATest(null);
            var expectedRuleState = new TransudationRuleEvaluationState
            {
                IsNotHigh = false,
                IsNotUnusuallyHigh = true,
                IsPositive = true
            };

            bvTest.TransudationRateResult = StaticRuleEvaluatorsTestsHelper.MinimumHighRate + 0.0001;
            var observedRuleState = StaticRuleEvaluatorsTestsHelper.EvalulateRules(bvTest);

            Assert.AreEqual(expectedRuleState, observedRuleState, StaticRuleEvaluatorsTestsHelper.GenerateFailureString(observedRuleState, expectedRuleState));
        }

        [TestMethod]
        public void And_the_transudation_rate_is_at_the_upper_end_for_high_then_the_rules_work_correctly()
        {
            var bvTest = new BVATest(null);
            var expectedRuleState = new TransudationRuleEvaluationState
            {
                IsNotHigh = false,
                IsNotUnusuallyHigh = true,
                IsPositive = true
            };

            bvTest.TransudationRateResult = StaticRuleEvaluatorsTestsHelper.MaximumHighRate;
            var observedRuleState = StaticRuleEvaluatorsTestsHelper.EvalulateRules(bvTest);

            Assert.AreEqual(expectedRuleState, observedRuleState, StaticRuleEvaluatorsTestsHelper.GenerateFailureString(observedRuleState, expectedRuleState));
        }

        [TestMethod]
        public void And_the_transudation_rate_is_unusually_high_then_the_rules_work_correctly()
        {
            var bvTest = new BVATest(null);
            var expectedRuleState = new TransudationRuleEvaluationState
            {
                IsNotHigh = true,
                IsNotUnusuallyHigh = false,
                IsPositive = true
            };

            bvTest.TransudationRateResult = StaticRuleEvaluatorsTestsHelper.MaximumHighRate + 0.0001;
            var observedRuleState = StaticRuleEvaluatorsTestsHelper.EvalulateRules(bvTest);

            Assert.AreEqual(expectedRuleState, observedRuleState, StaticRuleEvaluatorsTestsHelper.GenerateFailureString(observedRuleState, expectedRuleState));
        }
    }
    // ReSharper restore InconsistentNaming
}
