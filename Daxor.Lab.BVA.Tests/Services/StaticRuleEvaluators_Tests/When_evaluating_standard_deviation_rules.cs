﻿using Daxor.Lab.BVA.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_standard_deviation_rules
    {
        [TestMethod]
        public void And_a_positive_or_negative_standard_deviation_is_less_than_the_limit_then_the_rule_is_satisfied()
        {
            var bvTest = new BVATest(null) {StandardDevResult = 0.01};

            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardDeviationIsNotGreaterThanOrEqualTo(bvTest, StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit),
                "BV Test with stddev of {0} failed to satisfy the rule with limit value of {1}", bvTest.StandardDevResult,
                StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit);
            bvTest.StandardDevResult *= -1;
            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardDeviationIsNotGreaterThanOrEqualTo(bvTest, StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit),
                "BV Test with stddev of {0} failed to satisfy the rule with limit value of {1}", bvTest.StandardDevResult,
                StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit);
        }

        [TestMethod]
        public void And_a_positive_or_negative_standard_deviation_is_at_the_limit_then_the_rule_is_broken()
        {
            var bvTest = new BVATest(null) {StandardDevResult = StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit};

            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardDeviationIsNotGreaterThanOrEqualTo(bvTest, StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit),
                "BV Test with stddev of {0} failed to satisfy the rule with limit value of {1}", bvTest.StandardDevResult,
                StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit);
            bvTest.StandardDevResult *= -1;
            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardDeviationIsNotGreaterThanOrEqualTo(bvTest, StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit),
                "BV Test with stddev of {0} failed to satisfy the rule with limit value of {1}", bvTest.StandardDevResult,
                StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit);
        }

        [TestMethod]
        public void And_a_positive_or_negative_standard_deviation_is_over_the_limit_then_the_rule_is_broken()
        {
            var bvTest = new BVATest(null)
                             {StandardDevResult = StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit + 0.001};

            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardDeviationIsNotGreaterThanOrEqualTo(bvTest, StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit),
                "BV Test with stddev of {0} failed to break the rule with limit value of {1}", bvTest.StandardDevResult,
                StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit);
            bvTest.StandardDevResult *= -1;
            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardDeviationIsNotGreaterThanOrEqualTo(bvTest, StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit),
                "BV Test with stddev of {0} failed to break the rule with limit value of {1}", bvTest.StandardDevResult,
                StaticRuleEvaluatorsTestsHelper.StandardDeviationLimit);
        }
    }
    // ReSharper restore InconsistentNaming
}
