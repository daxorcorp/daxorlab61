﻿using Daxor.Lab.BVA.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_count_difference_rules
    {
        [TestMethod]
        public void And_standards_differ_acceptably_then_the_rule_is_satisfied()
        {
            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B);
            var compSample = new BVACompositeSample(null, standardA, standardB, null)
                                                {SampleA = {Counts = 15000}, SampleB = {Counts = 14225}};

            // A vs B under diff
            Assert.IsTrue(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardAandStandardBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Standard A counts are {0} and Standard B counts are {1}",
                compSample.SampleA.Counts, compSample.SampleB.Counts);

            // B vs A under diff
            compSample.SampleA.Counts = 14225;
            compSample.SampleB.Counts = 15000;
            Assert.IsTrue(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardAandStandardBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Standard A counts are {0} and Standard B counts are {1}",
                compSample.SampleA.Counts, compSample.SampleB.Counts);

            // A vs B exact diff
            compSample.SampleA.Counts = 15000;
            compSample.SampleB.Counts = 14223;
            Assert.IsTrue(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Sample A counts are {0} and Sample B counts are {1}",
                compSample.SampleA.Counts, compSample.SampleB.Counts);
        }

        [TestMethod]
        public void And_standards_differ_too_much_then_the_rule_is_broken()
        {
            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B);
            var compSample = new BVACompositeSample(null, standardA, standardB, null) { SampleA = { Counts = 15000 }, SampleB = { Counts = 14200 } };

            // A vs B over diff
            Assert.IsFalse(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardAandStandardBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Standard A counts are {0} and Standard B counts are {1}",
                compSample.SampleA.Counts, compSample.SampleB.Counts);

            // B vs A over diff
            compSample.SampleA.Counts = 14200;
            compSample.SampleB.Counts = 15000;
            Assert.IsFalse(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardAandStandardBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Standard A counts are {0} and Standard B counts are {1}",
                compSample.SampleA.Counts, compSample.SampleB.Counts);
        }

        [TestMethod]
        public void And_patient_samples_differ_acceptably_then_the_rule_is_satisfied()
        {
            var sampleA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Sample1, SampleRange.A);
            var sampleB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Sample1, SampleRange.B);
            var compSample = new BVACompositeSample(null, sampleA, sampleB, null) { SampleA = { Counts = 8560 }, SampleB = { Counts = 8041 } };

            // A vs B under diff
            Assert.IsTrue(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Sample A counts are {0} and Sample B counts are {1}",
                compSample.SampleA.Counts, compSample.SampleB.Counts);

            // B vs A under diff
            compSample.SampleA.Counts = 8041;
            compSample.SampleB.Counts = 8560;
            Assert.IsTrue(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Sample A counts are {0} and Sample B counts are {1}",
                compSample.SampleA.Counts, compSample.SampleB.Counts);

            // A vs B exact diff
            compSample.SampleA.Counts = 8560;
            compSample.SampleB.Counts = 8040;
            Assert.IsTrue(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Sample A counts are {0} and Sample B counts are {1}",
                compSample.SampleA.Counts, compSample.SampleB.Counts);
        }

        [TestMethod]
        public void And_patient_samples_differ_too_much_then_the_rule_is_broken()
        {
            var sampleA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Sample1, SampleRange.A);
            var sampleB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Sample1, SampleRange.B);
            var compSample = new BVACompositeSample(null, sampleA, sampleB, null)
                                                {SampleA = {Counts = 8560}, SampleB = {Counts = 8039}};

            // A vs B over diff
            Assert.IsFalse(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Sample A counts are {0} and Sample B counts are {1}",
                compSample.SampleA.Counts, compSample.SampleB.Counts);

            // B vs A over diff
            compSample.SampleA.Counts = 8039;
            compSample.SampleB.Counts = 8560;
            Assert.IsFalse(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Sample A counts are {0} and Sample B counts are {1}",
                compSample.SampleA.Counts, compSample.SampleB.Counts);
        }
    }
    // ReSharper restore InconsistentNaming
}
