using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Services.RuleEvaluators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_sample_counts_as_a_multiple_of_baseline_counts
    {
        private const int IgnoredMultiplyingFactor = 0;
        private const bool IgnoredValidatingSampleA = true;
        private const int IgnoredSampleCount = 10;
        private const bool UseASideValidation = true;
        private const bool UseBSideValidation = !UseASideValidation;
        private const int BaselineCount = 50;

        [TestMethod]
        public void And_the_validating_sample_is_null_then_the_rule_is_satisfied()
        {
            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(IgnoredValidatingSampleA, null, IgnoredMultiplyingFactor);
            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_a_background_sample_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Background);

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(IgnoredValidatingSampleA, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_a_standard_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Standard);

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(IgnoredValidatingSampleA, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_a_baseline_sample_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Baseline);

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(IgnoredValidatingSampleA, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_an_A_side_patient_sample_that_is_still_being_counted_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Sample1);
            sample.SampleA.IsCounting = true;

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(IgnoredValidatingSampleA, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_a_B_side_patient_sample_that_is_still_being_counted_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Sample1);
            sample.SampleB.IsCounting = true;

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(IgnoredValidatingSampleA, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_an_A_side_patient_sample_that_has_been_excluded_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Sample1);
            sample.SampleA.IsCountExcluded = true;

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(UseASideValidation, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_a_B_side_patient_sample_that_has_been_excluded_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Sample1);
            sample.SampleB.IsCountExcluded = true;

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(UseBSideValidation, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_an_A_side_patient_sample_that_has_negative_counts_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Sample1);
            sample.SampleA.Counts = -1;

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(UseASideValidation, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_a_B_side_patient_sample_that_has_negative_counts_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Sample1);
            sample.SampleB.Counts = -1;

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(UseBSideValidation, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_has_a_zero_baseline_average_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Baseline);

            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = 0;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = 0;

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(UseBSideValidation, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_has_a_negative_baseline_average_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Baseline);

            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = -1;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = -1;

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(UseBSideValidation, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_has_been_wholly_excluded_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Baseline);
            sample.IsWholeSampleExcluded = true;

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(UseBSideValidation, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_validating_sample_A_and_the_sample_counts_are_less_than_the_average_baseline_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(BaselineCount, IgnoredSampleCount, SampleType.Sample1);
            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = BaselineCount + 1;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = BaselineCount + 1;

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(UseASideValidation, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_validating_sample_B_and_the_sample_counts_are_less_than_the_average_baseline_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, BaselineCount, SampleType.Sample1);
            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = BaselineCount + 1;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = BaselineCount + 1;

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(UseBSideValidation, sample, IgnoredMultiplyingFactor);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_sample_count_difference_is_less_than_a_given_multiple_then_the_rule_is_broken()
        {
            const int tooSmallMultiple = 2;
            const int tooSmallCount = BaselineCount * tooSmallMultiple;

            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(tooSmallCount, tooSmallCount, SampleType.Sample1);
            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = BaselineCount;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = BaselineCount;

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(IgnoredValidatingSampleA, 
                sample, tooSmallMultiple + 1);

            Assert.IsFalse(observedResult);
        }

        [TestMethod]
        public void And_sample_count_difference_is_not_less_than_a_given_multiple_then_the_rule_is_satisfied()
        {
            const int multiple = 3;
            const int largeEnoughCount = BaselineCount * multiple;

            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(largeEnoughCount, largeEnoughCount, SampleType.Sample1);
            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = BaselineCount;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = BaselineCount;

            var observedResult = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(IgnoredValidatingSampleA, 
                sample, multiple - 1);

            Assert.IsTrue(observedResult);
        }
    }
    // ReSharper restore InconsistentNaming
}
