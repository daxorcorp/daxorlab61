﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests
{
    internal static class StaticRuleEvaluatorsTestsHelper
    {
        internal const double MinimumHighRate = 0.004;
        internal const double MaximumHighRate = 0.005;
        internal const double StandardDeviationLimit = 0.1;

        internal static BVASample CreateBvaSample(int position, SampleType type, SampleRange range,
            SampleExecutionStatus execStatus = SampleExecutionStatus.Undefined, bool isExcluded = false)
        {
            var sample = new BVASample(position, null)
                             {Type = type, Range = range, ExecutionStatus = execStatus, IsCountExcluded = isExcluded};

            return sample;
        }

        internal static TransudationRuleEvaluationState EvalulateRules(BVATest bvTest)
        {
            var state = new TransudationRuleEvaluationState
            {
                IsPositive = BVA.Services.RuleEvaluators.StaticRuleEvaluators.
                    TransudationRateIsPositive(bvTest),
                IsNotHigh = BVA.Services.RuleEvaluators.StaticRuleEvaluators.
                    TransudationRateIsNotBetweenInclusive(bvTest, MinimumHighRate, MaximumHighRate),
                IsNotUnusuallyHigh = BVA.Services.RuleEvaluators.StaticRuleEvaluators.
                    TransudationRateIsNotGreaterThan(bvTest, MaximumHighRate)
            };

            return state;
        }

        internal static string GenerateFailureString(TransudationRuleEvaluationState observed, TransudationRuleEvaluationState expected)
        {
            var msg = new StringBuilder();

            if (observed.IsNotHigh != expected.IsNotHigh)
            {
                msg.Append(expected.IsNotHigh
                               ? "Rule claims the rate is high when it shouldn't"
                               : "Rule claims the rate isn't high when it should");
                msg.Append("; ");
            }
            if (observed.IsNotUnusuallyHigh != expected.IsNotUnusuallyHigh)
            {
                msg.Append(expected.IsNotUnusuallyHigh
                               ? "Rule claims that the rate is unusually high when it shouldn't"
                               : "Rule claims that the rate isn't unusually high when it should");
                msg.Append("; ");
            }
            if (observed.IsPositive != expected.IsPositive)
            {
                msg.Append(expected.IsPositive
                               ? "Rule claims that the rate is negative when it shouldn't"
                               : "Rule claims that the rate isn't negative when it should");
                msg.Append("; ");
            }

            return msg.ToString();
        }

        internal static BVACompositeSample CreateBaselineCompositeSampleWhereTestHasBackgroundOf50(int baselineACounts, int baselineBCounts)
        {
            var baselineASample = CreateBvaSample(13, SampleType.Baseline, SampleRange.A);
            baselineASample.Counts = baselineACounts;
            var baselineBSample = CreateBvaSample(13, SampleType.Baseline, SampleRange.B);
            baselineBSample.Counts = baselineBCounts;
            var bvaTest = new BVATest(null) { BackgroundCount = 50, BackgroundTimeInSeconds = 10, SampleDurationInSeconds = 10 };
            bvaTest.ComputeTestAdjustedBackgroundCounts();
            return new BVACompositeSample(bvaTest, new List<BVASample> { baselineASample, baselineBSample }, null);
        }

        internal static BVACompositeSample CreateCompositeSample(int countsA, int countsB, SampleType sampleType)
        {
            var bvaTest = BvaTestFactory.CreateBvaTest(6, 2);
            bvaTest.ComputeTestAdjustedBackgroundCounts();

            if (sampleType == SampleType.Background)
            {
                var bvaSampleA = CreateBvaSample(13, sampleType, SampleRange.A);
                bvaSampleA.Counts = countsA;

                var bvaSampleB = CreateBvaSample(13, sampleType, SampleRange.B);
                bvaSampleB.Counts = countsB;

                return new BVACompositeSample(bvaTest, new List<BVASample>{bvaSampleA, bvaSampleB}, null);
            }

            var sample = (from c in bvaTest.CompositeSamples where c.SampleType == sampleType select c).FirstOrDefault();
            if (sample == null)
                throw new NotSupportedException("Cannot find the first composite sample in the BVA test");
            sample.SampleA.Counts = countsA;
            sample.SampleB.Counts = countsB;

            return sample;
        }
    }

    internal class TransudationRuleEvaluationState
    {
        public bool IsPositive { get; set; }
        public bool IsNotHigh { get; set; }
        public bool IsNotUnusuallyHigh { get; set; }

        public override bool Equals(Object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            var state = (TransudationRuleEvaluationState)obj;
            return (IsPositive == state.IsPositive) &&
                (IsNotHigh == state.IsNotHigh) &&
                (IsNotUnusuallyHigh == state.IsNotUnusuallyHigh);
        }

        public override int GetHashCode()
        {
            // ReSharper disable BaseObjectGetHashCodeCallInGetHashCode
            return base.GetHashCode();
            // ReSharper restore BaseObjectGetHashCodeCallInGetHashCode
        }
    }
}
