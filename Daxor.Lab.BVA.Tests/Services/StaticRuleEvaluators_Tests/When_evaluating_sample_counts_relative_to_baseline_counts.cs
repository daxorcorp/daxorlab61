using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Services.RuleEvaluators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_sample_counts_relative_to_baseline_counts
    {
        private const bool IgnoredValidatingSampleA = true;
        private const int IgnoredSampleCount = 10;
        private const bool UseASideValidation = true;
        private const bool UseBSideValidation = !UseASideValidation;
        private const int BaselineCount = 50;

        [TestMethod]
        public void And_the_validating_sample_is_null_then_the_rule_is_satisfied()
        {
            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(IgnoredValidatingSampleA, null);
            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_a_background_sample_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Background);

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(IgnoredValidatingSampleA, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_a_standard_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Standard);

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(IgnoredValidatingSampleA, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_a_baseline_sample_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Baseline);

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(IgnoredValidatingSampleA, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_an_A_side_patient_sample_that_is_still_being_counted_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Sample1);
            sample.SampleA.IsCounting = true;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(IgnoredValidatingSampleA, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_a_B_side_patient_sample_that_is_still_being_counted_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Sample1);
            sample.SampleB.IsCounting = true;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(IgnoredValidatingSampleA, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_an_A_side_patient_sample_that_has_been_excluded_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Sample1);
            sample.SampleA.IsCountExcluded = true;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(UseASideValidation, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_a_B_side_patient_sample_that_has_been_excluded_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Sample1);
            sample.SampleB.IsCountExcluded = true;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(UseBSideValidation, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_an_A_side_patient_sample_that_has_negative_counts_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Sample1);
            sample.SampleA.Counts = -1;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(UseASideValidation, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_is_a_B_side_patient_sample_that_has_negative_counts_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Sample1);
            sample.SampleB.Counts = -1;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(UseBSideValidation, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_has_a_zero_baseline_average_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Baseline);

            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = 0;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = 0;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(UseBSideValidation, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_has_a_negative_baseline_average_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Baseline);

            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = -1;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = -1;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(UseBSideValidation, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_the_validating_sample_has_been_wholly_excluded_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, IgnoredSampleCount, SampleType.Baseline);
            sample.IsWholeSampleExcluded = true;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(UseBSideValidation, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_validating_sample_A_and_its_counts_exceed_the_average_baseline_counts_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(BaselineCount + 1, IgnoredSampleCount, SampleType.Sample1);
            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = BaselineCount;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = BaselineCount;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(UseASideValidation, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_validating_sample_A_and_its_counts_are_less_than_the_average_baseline_counts_then_the_rule_is_broken()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(BaselineCount - 1, IgnoredSampleCount, SampleType.Sample1);
            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = BaselineCount;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = BaselineCount;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(UseASideValidation, sample);

            Assert.IsFalse(observedResult);
        }

        [TestMethod]
        public void And_validating_sample_A_and_its_counts_equal_the_average_baseline_counts_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(BaselineCount, IgnoredSampleCount, SampleType.Sample1);
            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = BaselineCount;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = BaselineCount;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(UseASideValidation, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_validating_sample_B_and_its_counts_exceed_the_average_baseline_counts_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, BaselineCount + 1, SampleType.Sample1);
            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = BaselineCount;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = BaselineCount;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(UseBSideValidation, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_validating_sample_B_and_its_counts_equal_the_average_baseline_counts_then_the_rule_is_satisfied()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, BaselineCount, SampleType.Sample1);
            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = BaselineCount;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = BaselineCount;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(UseBSideValidation, sample);

            Assert.IsTrue(observedResult);
        }

        [TestMethod]
        public void And_validating_sample_B_and_its_counts_are_less_than_the_average_baseline_counts_then_the_rule_is_broken()
        {
            var sample = StaticRuleEvaluatorsTestsHelper.CreateCompositeSample(IgnoredSampleCount, BaselineCount - 1, SampleType.Sample1);
            sample.Test.PatientBaselineCompositeSample.SampleA.Counts = BaselineCount;
            sample.Test.PatientBaselineCompositeSample.SampleB.Counts = BaselineCount;

            var observedResult = StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(UseBSideValidation, sample);

            Assert.IsFalse(observedResult);
        }
    }
    // ReSharper restore InconsistentNaming
}
