﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.Domain.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_standards_are_not_less_than_count_rules
    {
        [TestMethod]
        public void And_either_A_or_B_sample_has_less_then_a_given_number_of_counts_then_the_rule_is_broken()
        {
            const int ComparisonValue = 100;

            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A, SampleExecutionStatus.Completed);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B, SampleExecutionStatus.Completed);
            var compSample = new BVACompositeSample(null, standardA, standardB, null)
                                 {SampleA = {Counts = ComparisonValue - 1}};

            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                true, compSample, ComparisonValue), 
                "Rule satisfied when Standard A count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);

            compSample.SampleB.Counts = ComparisonValue - 1;
            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                false, compSample, ComparisonValue), 
                "Rule satisfied when Standard B count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);
        }

        [TestMethod]
        public void And_either_A_or_B_sample_has_less_then_a_given_number_of_counts_and_the_status_is_undefined_then_the_rule_is_broken()
        {
            const int ComparisonValue = 100;

            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B);
            var compSample = new BVACompositeSample(null, standardA, standardB, null) { SampleA = { Counts = ComparisonValue - 1 } };

            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                true, compSample, ComparisonValue),
                "Rule satisfied when Standard A (status undefined) count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);

            compSample.SampleB.Counts = ComparisonValue - 1;
            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                false, compSample, ComparisonValue),
                "Rule satisfied when Standard B (status undefined) count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);
        }

        [TestMethod]
        public void And_either_A_or_B_sample_has_less_then_a_given_number_of_counts_and_the_status_is_idle_then_the_rule_is_broken()
        {
            const int ComparisonValue = 100;

            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A, SampleExecutionStatus.Idle);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B, SampleExecutionStatus.Idle);
            var compSample = new BVACompositeSample(null, standardA, standardB, null) { SampleA = { Counts = ComparisonValue - 1 } };

            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                true, compSample, ComparisonValue),
                "Rule satisfied when Standard A (status idle) count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);

            compSample.SampleB.Counts = ComparisonValue - 1;
            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                false, compSample, ComparisonValue),
                "Rule satisfied when Standard B (status idle) count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);
        }

        [TestMethod]
        public void And_either_A_or_B_sample_has_less_then_a_given_number_of_counts_and_the_status_is_in_flight_then_the_rule_is_broken()
        {
            const int ComparisonValue = 100;

            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A, SampleExecutionStatus.InFlight);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B, SampleExecutionStatus.InFlight);
            var compSample = new BVACompositeSample(null, standardA, standardB, null) { SampleA = { Counts = ComparisonValue - 1 } };

            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                true, compSample, ComparisonValue),
                "Rule satisfied when Standard A (status in-flight) count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);

            compSample.SampleB.Counts = ComparisonValue - 1;
            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                false, compSample, ComparisonValue),
                "Rule satisfied when Standard B (status in-flight) count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);
        }

        [TestMethod]
        public void And_either_A_or_B_sample_has_less_then_a_given_number_of_counts_and_the_status_is_completed_then_the_rule_is_broken()
        {
            const int ComparisonValue = 100;

            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A, SampleExecutionStatus.Completed);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B, SampleExecutionStatus.Completed);
            var compSample = new BVACompositeSample(null, standardA, standardB, null) { SampleA = { Counts = ComparisonValue - 1 } };

            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                true, compSample, ComparisonValue),
                "Rule satisfied when Standard A (status completed) count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);

            compSample.SampleB.Counts = ComparisonValue - 1;
            Assert.IsFalse(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                false, compSample, ComparisonValue),
                "Rule satisfied when Standard B (status completed) count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);
        }

        [TestMethod]
        public void And_either_A_or_B_sample_has_exactly_the_given_number_of_counts_then_the_rule_is_satisfied()
        {
            const int ComparisonValue = 100;

            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A, SampleExecutionStatus.Completed);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B, SampleExecutionStatus.Completed);
            var compSample = new BVACompositeSample(null, standardA, standardB, null) { SampleA = { Counts = ComparisonValue } };

            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                true, compSample, ComparisonValue),
                "Rule broken when Standard A count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);

            compSample.SampleB.Counts = ComparisonValue;
            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                false, compSample, ComparisonValue),
                "Rule broken when Standard B count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);
        }

        [TestMethod]
        public void And_either_A_or_B_sample_has_more_then_a_given_number_of_counts_then_the_rule_is_satisfied()
        {
            const int ComparisonValue = 100;

            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A, SampleExecutionStatus.Completed);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B, SampleExecutionStatus.Completed);
            var compSample = new BVACompositeSample(null, standardA, standardB, null)
                                 {SampleA = {Counts = ComparisonValue + 1}};

            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                true, compSample, ComparisonValue),
                "Rule broken when Standard A count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);

            compSample.SampleB.Counts = ComparisonValue + 1;
            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                false, compSample, ComparisonValue),
                "Rule broken when Standard B count is {0} when but should be less than {1}",
                compSample.SampleA.Counts, ComparisonValue);
        }
    }
    // ReSharper restore InconsistentNaming
}
