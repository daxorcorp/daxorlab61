﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_standard_deviation_rules
    {

        [TestMethod]
        public void And_StandardDeviationIsLessThanOrEqualTo_is_given_a_null_test_instance_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardDeviationIsNotGreaterThanOrEqualTo(null, 0), "test");
        }

        [TestMethod]
        public void And_the_standard_devation_is_NaN_then_the_rule_is_satisfied()
        {
            var bvTest = new BVATest(null) { StandardDevResult = double.NaN };

            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardDeviationIsNotGreaterThanOrEqualTo(bvTest, double.NaN),
                "BV Test with stddev of NaN failed to satisfy the rule");
        }
    }
    // ReSharper restore InconsistentNaming
}
