﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Services.RuleEvaluators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_post_injection_time_rules
    {
        [TestMethod]
        public void And_testing_not_greater_than_and_the_sample_is_null_then_the_rule_is_satisfied()
        {
            var result = StaticRuleEvaluators.PostInjectionTimeIsNotGreaterThan(null, 1);
            Assert.IsTrue(result, "Post injection time not greater than value rule should pass if the sample is null");
        }

        [TestMethod]
        public void And_testing_not_greater_than_and_the_post_injection_time_is_negative_then_the_rule_is_satisfied()
        {
            var aSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.A);
            var bSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.B);
            aSideSample.PostInjectionTimeInSeconds = -1;
            var compositeSample = new BVACompositeSample(null, new List<BVASample> { aSideSample, bSideSample }, null);
            var result = StaticRuleEvaluators.PostInjectionTimeIsNotGreaterThan(compositeSample, 1);
            Assert.IsTrue(result, "Post injection time not greater than value rule should pass if Sample A's post-injection time is -1");
        }

        [TestMethod]
        public void And_testing_not_greater_than_and_the_whole_sample_is_excluded_then_the_rule_is_satisfied()
        {
            var aSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.A);
            var bSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.B);
            var compositeSample = new BVACompositeSample(null, new List<BVASample> { aSideSample, bSideSample }, null)
                {IsWholeSampleExcluded = true};
            var result = StaticRuleEvaluators.PostInjectionTimeIsNotGreaterThan(compositeSample, 1);
            Assert.IsTrue(result, "Post injection time not greater than value rule should pass if the composite sample is excluded");
        }
    }
    // ReSharper restore InconsistentNaming
}
