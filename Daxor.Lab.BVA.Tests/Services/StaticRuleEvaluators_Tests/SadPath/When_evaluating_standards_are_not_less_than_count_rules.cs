﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.Domain.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_standards_are_not_less_than_count_rules
    {
        [TestMethod]
        public void And_given_a_null_sample_then_the_rule_is_satisfied()
        {
            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                true, null, 0.0));
        }

        [TestMethod]
        public void And_given_a_sample_that_isnt_a_standard_then_the_rule_is_satisfied()
        {
            var sampleA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Sample1, SampleRange.A);
            var sampleB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Sample1, SampleRange.B);
            var compSample = new BVACompositeSample(null, sampleA, sampleB, null);

            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                true, compSample, 0.0));
        }

        [TestMethod]
        public void And_either_A_or_B_sample_is_acquiring_then_the_rule_is_satisfied()
        {
            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A, SampleExecutionStatus.Acquiring);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B, SampleExecutionStatus.Acquiring);
            var compSample = new BVACompositeSample(null, standardA, standardB, null);
            
            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                true, compSample, 0.0), "Rule broken when Standard A isn't completed");

            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                false, compSample, 0.0), "Rule broken when Standard B isn't completed");
        }

        [TestMethod]
        public void And_either_A_or_B_sample_is_excluded_then_the_rule_is_satisfied()
        {
            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A, SampleExecutionStatus.Completed);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B, SampleExecutionStatus.Completed);
            var compSample = new BVACompositeSample(null, standardA, standardB, null) { SampleA = { IsCountExcluded = true } };

            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                true, compSample, 0.0), "Rule broken when Standard A is excluded");

            compSample.SampleB.IsCountExcluded = true;
            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                false, compSample, 0.0), "Rule broken when Standard A is excluded");
        }

        [TestMethod]
        public void And_either_A_or_B_sample_has_no_counts_then_the_rule_is_satisfied()
        {
            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A, SampleExecutionStatus.Completed);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B, SampleExecutionStatus.Completed);
            var compSample = new BVACompositeSample(null, standardA, standardB, null) { SampleA = { Counts = 0 } };

            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                true, compSample, 0.0), "Rule broken when Standard A has no counts");

            compSample.SampleB.Counts = 0;
            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                false, compSample, 0.0), "Rule broken when Standard B has no counts");
        }

        [TestMethod]
        public void And_either_A_or_B_sample_has_negative_counts_then_the_rule_is_satisfied()
        {
            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A, SampleExecutionStatus.Completed);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B, SampleExecutionStatus.Completed);
            var compSample = new BVACompositeSample(null, standardA, standardB, null) { SampleA = { Counts = -2 } };

            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                true, compSample, 0.0), "Rule broken when Standard A has negative counts");

            compSample.SampleB.Counts = -2;
            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.StandardCountsAreNotLessThan(
                false, compSample, 0.0), "Rule broken when Standard B has negative counts");
        }
    }
    // ReSharper restore InconsistentNaming
}
