﻿using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Services.RuleEvaluators;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_transudation_rate_rules
    {
        [TestMethod]
        public void And_the_transudation_rate_is_negative_then_the_rules_work_correctly()
        {
            var bvTest = new BVATest(null);
            var expectedRuleState = new TransudationRuleEvaluationState
            {
                IsNotHigh = true,
                IsNotUnusuallyHigh = true,
                IsPositive = false
            };

            bvTest.TransudationRateResult = -0.1;
            var observedRuleState = StaticRuleEvaluatorsTestsHelper.EvalulateRules(bvTest);

            Assert.AreEqual(expectedRuleState, observedRuleState, StaticRuleEvaluatorsTestsHelper.GenerateFailureString(observedRuleState, expectedRuleState));
        }

        [TestMethod]
        public void And_the_transudation_rate_is_zero_then_the_rules_work_correctly()
        {
            var bvTest = new BVATest(null);
            var expectedRuleState = new TransudationRuleEvaluationState
            {
                IsNotHigh = true,
                IsNotUnusuallyHigh = true,
                IsPositive = true
            };

            bvTest.TransudationRateResult = 0;
            TransudationRuleEvaluationState observedRuleState = StaticRuleEvaluatorsTestsHelper.EvalulateRules(bvTest);

            Assert.AreEqual(expectedRuleState, observedRuleState, StaticRuleEvaluatorsTestsHelper.GenerateFailureString(observedRuleState, expectedRuleState));
        }

        [TestMethod]
        public void And_the_range_values_are_out_of_order_then_an_exception_is_thrown()
        {
            var bvTest = new BVATest(null);
            AssertEx.Throws<ArgumentOutOfRangeException>(()=>StaticRuleEvaluators.TransudationRateIsNotBetweenInclusive(bvTest, 5, 4));
        }

        [TestMethod]
        public void And_TransudationRateIsNotBetweenInclusive_is_given_a_null_test_instance_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>StaticRuleEvaluators.TransudationRateIsNotBetweenInclusive(null, 0, 1), "test");
        }

        [TestMethod]
        public void And_TransudationRateIsPositive_is_given_a_null_test_instance_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>StaticRuleEvaluators.TransudationRateIsPositive(null), "test");
        }

        [TestMethod]
        public void And_TransudationRateIsLessThanOrEqualTo_is_given_a_null_test_instance_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>StaticRuleEvaluators.TransudationRateIsNotGreaterThan(null, 0), "test");
        }
    }
    // ReSharper restore InconsistentNaming
}
