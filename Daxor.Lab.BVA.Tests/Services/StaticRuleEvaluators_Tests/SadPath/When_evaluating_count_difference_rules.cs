﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_count_difference_rules
    {
        [TestMethod]
        public void And_given_a_null_sample_then_the_rule_is_satisfied()
        {
            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(null));
        }

        [TestMethod]
        public void And_given_a_background_sample_then_the_rule_is_satisfied()
        {
            var sampleA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Background, SampleRange.A);
            var sampleB= StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Background, SampleRange.B);
            var compSample = new BVACompositeSample(null, new List<BVASample> { sampleA, sampleB }, null);

            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(compSample));
        }

        [TestMethod]
        public void And_given_wholly_exluded_sample_then_the_rule_is_satisfied()
        {
            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B);
            var compSample = new BVACompositeSample(null, standardA, standardB, null) {IsWholeSampleExcluded = true};

            Assert.IsTrue(BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(compSample));
        }

        [TestMethod]
        public void And_either_sample_is_counting_then_the_rule_is_satisfied()
        {
            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B);
            var compSample = new BVACompositeSample(null, standardA, standardB, null)
                                                {SampleA = {IsCounting = true}, SampleB = {IsCounting = false}};

            Assert.IsTrue(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Sample A is counting but Sample B is not");

            compSample.SampleA.IsCounting = false;
            compSample.SampleB.IsCounting = true;
            Assert.IsTrue(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Sample B is counting but Sample A is not");
        }

        [TestMethod]
        public void And_either_sample_is_excluded_then_the_rule_is_satisfied()
        {
            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B);
            var compSample = new BVACompositeSample(null, standardA, standardB, null)
                                                {
                                                    SampleA = {IsCountExcluded = true},
                                                    SampleB = {IsCountExcluded = false}
                                                };

            Assert.IsTrue(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Sample A is excluded but Sample B is not");

            compSample.SampleA.IsCountExcluded = false;
            compSample.SampleB.IsCountExcluded = true;
            Assert.IsTrue(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Sample B is excluded but Sample A is not");
        }

        [TestMethod]
        public void And_either_sample_has_undefined_counts_then_the_rule_is_satisfied()
        {
            var standardA = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(3, SampleType.Standard, SampleRange.A);
            var standardB = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(4, SampleType.Standard, SampleRange.B);
            var compSample = new BVACompositeSample(null, standardA, standardB, null)
                                                {SampleA = {Counts = -1}, SampleB = {Counts = 1}};

            Assert.IsTrue(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Sample A has undefined count value");

            compSample.SampleA.Counts = 1;
            compSample.SampleB.Counts = -1;
            Assert.IsTrue(
                BVA.Services.RuleEvaluators.StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(
                    compSample), "Rule broken when Sample B has undefined count value");
        }
    }
    // ReSharper restore InconsistentNaming
}
