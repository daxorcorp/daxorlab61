﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Services.RuleEvaluators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Tests.Services.StaticRuleEvaluators_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_post_injection_time_rules
    {
        [TestMethod]
        public void And_testing_not_greater_than_and_the_time_is_less_than_expected_then_the_rule_is_satisfied()
        {
            var aSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.A);
            var bSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.B);
            aSideSample.PostInjectionTimeInSeconds = 100;
            var compositeSample = new BVACompositeSample(null, new List<BVASample> { aSideSample, bSideSample }, null);
            const int comparisonValue = 101;
            var result = StaticRuleEvaluators.PostInjectionTimeIsNotGreaterThan(compositeSample, comparisonValue);
            Assert.IsTrue(result, "Post injection time not greater than value rule should pass if comparison value {0} is less than post-injection time {1}",
                comparisonValue, aSideSample.PostInjectionTimeInSeconds);
        }

        [TestMethod]
        public void And_testing_not_greater_than_and_the_time_is_equal_to_expected_then_the_rule_is_satisfied()
        {
            var aSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.A);
            var bSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.B);
            aSideSample.PostInjectionTimeInSeconds = 100;
            var compositeSample = new BVACompositeSample(null, new List<BVASample> {aSideSample, bSideSample}, null);
            const int comparisonValue = 100;
            var result = StaticRuleEvaluators.PostInjectionTimeIsNotGreaterThan(compositeSample, comparisonValue);
            Assert.IsTrue(result, "Post injection time not greater than value rule should pass if comparison value {0} is equal to post-injection time {1}",
                comparisonValue, aSideSample.PostInjectionTimeInSeconds);
        }

        [TestMethod]
        public void And_testing_not_greater_than_and_the_time_is_greater_than_expected_then_the_rule_is_broken()
        {
            var aSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.A);
            var bSideSample = StaticRuleEvaluatorsTestsHelper.CreateBvaSample(13, SampleType.Sample1, SampleRange.B);
            aSideSample.PostInjectionTimeInSeconds = 100;
            var compositeSample = new BVACompositeSample(null, new List<BVASample> { aSideSample, bSideSample }, null);
            const int comparisonValue = 99;
            var result = StaticRuleEvaluators.PostInjectionTimeIsNotGreaterThan(compositeSample, comparisonValue);
            Assert.IsFalse(result, "Post injection time not greater than value rule should break if comparison value {0} is greater than post-injection time {1}",
                comparisonValue, aSideSample.PostInjectionTimeInSeconds);
        }
    }
    // ReSharper restore InconsistentNaming
}


