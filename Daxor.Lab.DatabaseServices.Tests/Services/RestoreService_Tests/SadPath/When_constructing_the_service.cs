﻿using Daxor.Lab.DatabaseServices.Services;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.SqlServer.Management.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.DatabaseServices.Tests.Services.RestoreService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_the_service
    {
        private const string ValidString = "something";
        private static ISettingsManager _settingsManager;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _settingsManager = MockRepository.GenerateStub<ISettingsManager>();
        }

        [TestMethod]
        public void And_the_settings_manager_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(()=>new RestoreService(null, ValidString), "settingsManager");
        }

        [TestMethod]
        public void And_the_database_name_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(()=>new RestoreService(_settingsManager, null), "databaseName");
        }

        [TestMethod]
        public void And_the_database_name_is_whitespace_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<InvalidArgumentException>(()=>new RestoreService(_settingsManager, "   "));
        }
    }
    // ReSharper restore InconsistentNaming
}
