﻿using Daxor.Lab.DatabaseServices.Factories;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.SqlServer.Management.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.DatabaseServices.Tests.Factories.RestoreServiceFactory_Tests.SadPath.When_creating_the_service
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_the_qc_service
    {
        [TestMethod]
        public void And_the_database_name_is_null_then_an_exception_is_thrown()
        {
            var stubSettings = MockRepository.GenerateStub<ISettingsManager>();
            var factory = new RestoreServiceFactory(stubSettings);

            AssertEx.ThrowsArgumentNullException(()=>factory.CreateQcArchiveRestoreService(), "databaseName");
        }

        [TestMethod]
        public void And_the_database_name_is_whitespace_then_an_exception_is_thrown()
        {
            var stubSettings = MockRepository.GenerateStub<ISettingsManager>();
            stubSettings.Expect(x => x.GetSetting<string>(SettingKeys.SystemQcDatabaseArchiveDatabaseName)).Return("  ");

            var factory = new RestoreServiceFactory(stubSettings);
            AssertEx.Throws<InvalidArgumentException>(()=>factory.CreateQcArchiveRestoreService());
        }
    }
    // ReSharper restore InconsistentNaming
}
