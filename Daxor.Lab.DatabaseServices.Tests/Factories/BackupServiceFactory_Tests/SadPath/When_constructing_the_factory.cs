﻿using Daxor.Lab.DatabaseServices.Factories;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.DatabaseServices.Tests.Factories.BackupServiceFactory_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_the_factory
    {
        [TestMethod]
        public void And_the_settings_manager_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(()=>new BackupServiceFactory(null), "settingsManager");
        }
    }
    // ReSharper restore InconsistentNaming
}
