﻿using Daxor.Lab.DatabaseServices.Factories;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.SqlServer.Management.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.DatabaseServices.Tests.Factories.BackupServiceFactory_Tests.SadPath.When_creating_the_service
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_the_daxor_service
    {
        [TestMethod]
        public void And_the_database_name_is_null_then_an_exception_is_thrown()
        {
            var stubSettings = MockRepository.GenerateStub<ISettingsManager>();
            var factory = new BackupServiceFactory(stubSettings);
            AssertEx.ThrowsArgumentNullException(()=>factory.CreateDaxorLabBackupService(), "databaseName");
        }

        [TestMethod]
        public void And_the_database_name_is_whitespace_then_an_exception_is_thrown()
        {
            var stubSettings = MockRepository.GenerateStub<ISettingsManager>();
            stubSettings.Expect(x => x.GetSetting<string>(SettingKeys.SystemDatabaseName)).Return("  ");
            var factory = new BackupServiceFactory(stubSettings);
            AssertEx.Throws<InvalidArgumentException>(()=>factory.CreateDaxorLabBackupService());
        }
    }
    // ReSharper restore InconsistentNaming
}
