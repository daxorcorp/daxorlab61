﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Daxor.Lab.Help.Common;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Reports;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

[assembly: InternalsVisibleTo("Daxor.Lab.Help.Tests")]
namespace Daxor.Lab.Help.Services
{
	public class LomSignoffFormGenerator : IDocumentGenerator
	{
		#region Constants

		private const int NumberOfCharactersPerLine = 120;
		private const int NumberOfSignatureRows = 26;

		#endregion

		#region Fields

		private readonly ISettingsManager _settingsManager;

		private string _hospitalName;
		private string _departmentAddress;
		private string _departmentName;
		private string _departmentPhoneNumber;
		private string _departmentDirector;

		private LomSignoffFormReport _report;
		private const string LineBreakWithSevenSpaces = "\n       ";

		#endregion

		#region Ctor

		public LomSignoffFormGenerator(ISettingsManager settingsManager)
		{
			if (settingsManager == null)
				throw new ArgumentNullException("settingsManager");
			
			_settingsManager = settingsManager;
		}

		#endregion

		#region Properties

		public string Statement1
		{
			get
			{
				return "1.    " + _departmentName + " testing personnel's authentic signatures and initials.";
			}
		}

		public string Statement2
		{
			get
			{
				return "2.    Acknowledgement that the " + _departmentName +
					   " testing personnel have read and are familiar with the contents of the Laboratory Operations Manual.";
			}
		}

		public string Statement3
		{
			get
			{
				return "3.    That all work done in the " + _departmentName + " is done in accordance with these procedures.";
			}
		}

		#endregion

		#region Public API

		public void Generate()
		{
			_hospitalName = _settingsManager.GetSetting<string>(SettingKeys.SystemHospitalName);
			_departmentAddress = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentAddress);
			_departmentName = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentName);
			_departmentPhoneNumber = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentPhoneNumber);
			_departmentDirector = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentDirector);

			_report = new LomSignoffFormReport();

			AddHospitalInformation();
			AddStatementTextToDocument();
			AddEmptyRowsToSignatureTable();
			SavePdfRenderingToFile();
		}

		#endregion

		#region Helpers

		protected virtual void AddStatementTextToDocument()
		{
			_report.textBoxDisclaimer1.Value = AddLineBreaksAndIndent(Statement1);
			_report.textBoxDisclaimer2.Value = AddLineBreaksAndIndent(Statement2);
			_report.textBoxDisclaimer3.Value = AddLineBreaksAndIndent(Statement3);
		}

		internal string AddLineBreaksAndIndent(string statementText)
		{
			if (statementText.Length <= NumberOfCharactersPerLine) return statementText;

			var theStringThatWillFitOnThePage = statementText.Substring(0, NumberOfCharactersPerLine);
			var indexOfLastWhiteSpaceBeforeLineBreak = theStringThatWillFitOnThePage.LastIndexOf(" ", StringComparison.Ordinal);

			return statementText.Substring(0, indexOfLastWhiteSpaceBeforeLineBreak) + LineBreakWithSevenSpaces + statementText.Substring(indexOfLastWhiteSpaceBeforeLineBreak + 1);
		}

		protected virtual void AddEmptyRowsToSignatureTable()
		{
			var rows = Enumerable.Range(1, NumberOfSignatureRows).Select(n => new { RowValue = string.Empty }).ToList();
			_report.tableSignatoryArea.DataSource = rows;
		}

		protected virtual void AddHospitalInformation()
		{
			_report.textBoxHospitalName.Value = _hospitalName;
			_report.textBoxHospitalAddress.Value = _departmentAddress;
			_report.textBoxDepartmentName.Value = _departmentName;
			_report.textBoxHospitalPhone.Value = _departmentPhoneNumber;
			_report.textBoxDepartmentDirector.Value = _departmentDirector;
		}
		
		protected virtual void SavePdfRenderingToFile()
		{
			var targetDirectory = Path.GetDirectoryName(HelpConstants.BvaManualSignoffFormFileName);

			if (targetDirectory != null && !Directory.Exists(targetDirectory))
				Directory.CreateDirectory(targetDirectory);

			var reportProcessor = new ReportProcessor();

			var result = reportProcessor.RenderReport("PDF", new InstanceReportSource { ReportDocument = _report }, null);

			using (var fs = new FileStream(HelpConstants.BvaManualSignoffFormFileName, FileMode.Create))
			{
				fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
				fs.Flush();
			}
		}

		#endregion
	}
}
