﻿namespace Daxor.Lab.Help.Services.Navigation
{
	public static class HelpModuleViewKeys
	{
		public const string MainView = "MainView";
		public const string DocumentView = "DocumentView";
		public const string Empty = "Empty";
	}
}
