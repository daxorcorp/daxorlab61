﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Views;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.Help.Services.Navigation
{
	public class HelpModuleNavigator : IAppModuleNavigator
	{
		#region Fields

		private readonly string _moduleKey;
		private readonly IUnityContainer _container;
		private readonly IRegionManager _regionManager;
		private readonly IEventAggregator _eventAggregator;
		private readonly Dictionary<string, FrameworkElement> _views = new Dictionary<string, FrameworkElement>();
		// ReSharper disable once InconsistentNaming
		protected string _currentViewKey = HelpModuleViewKeys.Empty;

		#endregion

		#region Ctor

		/// <summary>
		/// Creates a new HelpModuleNavigator instance and registers with the global
		/// navigation coordiator
		/// </summary>
		/// <param name="container">IoC container</param>
		/// <param name="eventAggregator">Event aggregator</param>
		/// <param name="regionManager">View region manager</param>
		/// <param name="navigationCoordinator">Navigation coordinator</param>
		public HelpModuleNavigator(IUnityContainer container, IEventAggregator eventAggregator,
								 IRegionManager regionManager, INavigationCoordinator navigationCoordinator)
		{
			_container = container;
			_eventAggregator = eventAggregator;
			_regionManager = regionManager;
			_moduleKey = AppModuleIds.Help;

			// Register myself with a global application coordinator
			navigationCoordinator.RegisterAppModuleNavigator(_moduleKey, this);

			NavigationCoordinator = navigationCoordinator;
		}

		#endregion

		#region Properties
		
		/// <summary>
		/// Determines if the Help module is the active application module
		/// </summary>
		public bool IsActive
		{
			get { return NavigationCoordinator.ActiveAppModuleKey.Equals(_moduleKey); }
		}

		/// <summary>
		/// Gets or sets the global navigation coordinator
		/// </summary>
		public INavigationCoordinator NavigationCoordinator
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets or sets the top-level WPF element for the Help module
		/// Note: This is inherited from IAppModuleNavigator
		/// </summary>
		public FrameworkElement RootViewHost
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets or sets the lower-level WPF element for the Help module
		/// </summary>
		public FrameworkElement DocumentViewHost
		{
			get; 
			private set; 
		}

		/// <summary>
		/// Gets the unique app module key for the Help module
		/// </summary>
		public string AppModuleKey
		{
			get { return _moduleKey; }
		}

		/// <summary>
		/// Gets the collection of views associated with the Help module
		/// </summary>
		public Dictionary<string, FrameworkElement> Views
		{
			get { return _views; }
		}

		/// <summary>
		/// Gets the key for the active view for the Help module
		/// </summary>
		public string CurrentViewKey
		{
			get { return _currentViewKey; }
		}

		#endregion

		#region Methods

		/// <summary>
		/// Creates the main view and registers it with the collection of views, then adds
		/// that view to the workspace region collection of views.
		/// </summary>
		public void Initialize()
		{
			RootViewHost = _container.Resolve<HelpMainView>();
			_views.Add(HelpModuleViewKeys.MainView, RootViewHost);
			_regionManager.Regions[RegionNames.WorkspaceRegion].Add(RootViewHost);

			DocumentViewHost = _container.Resolve<HelpDocumentView>();
			_views.Add(HelpModuleViewKeys.DocumentView, DocumentViewHost);
			_regionManager.Regions[RegionNames.WorkspaceRegion].Add(DocumentViewHost);
		}

		/// <summary>
		/// Determines whether the current module can be activated
		/// </summary>
		/// <returns>True if the module can be activated; false otherwise</returns>
		public bool CanActivate()
		{
			return true;
		}

		/// <summary>
		/// Determines whether the current module can be deactivated
		/// </summary>
		/// <returns>True if the module can be deactivated; false otherwise</returns>
		public bool CanDeactivate()
		{
			return true;
		}

		/// <summary>
		/// Activates a view for the module. On first activiation, show the main view; otherwise,
		/// show whatever view was last active/current. On any activation, the BVA Manual sign
		/// off sheet is dynamically created in case a location setting changed.
		/// </summary>
		/// <param name="displayViewOption">Option for choosing which view to use (ignored)</param>
		/// <param name="payload">Additional payload for view activation (ignored)</param>
		public void Activate(DisplayViewOption displayViewOption, object payload)
		{
			// Let the main menu know this module should be a part of the menu
			_eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(ApplicationParts.MainMenuDropDown, true));

			ActivateView(_currentViewKey == HelpModuleViewKeys.Empty ? HelpModuleViewKeys.MainView : _currentViewKey, payload);
		}

		/// <summary>
		/// Deactivates the view in the workspace region and sets the current view to empty.
		/// The Manual sign off sheet is trashed so a new one can be created later.
		/// If the current view is already empty, nothing happens.
		/// </summary>
		/// <param name="onDeactivateCallback">Additional handler to be invoked post-decativation</param>
		public void Deactivate(Action onDeactivateCallback)
		{
			// Run storyboard, and on storyboard completion remove the view
			if (_currentViewKey != HelpModuleViewKeys.Empty)
			{
				var workspaceRegion = _regionManager.Regions[RegionNames.WorkspaceRegion];
				workspaceRegion.Deactivate(_views[_currentViewKey]);
				_currentViewKey = HelpModuleViewKeys.Empty;
			}

			_eventAggregator.GetEvent<HelpViewChanged>().Publish(HelpModuleViewKeys.Empty);

			onDeactivateCallback();
		}

		/// <summary>
		/// Activates a view within the Help module based on the given key/identifier
		/// </summary>
		/// <param name="viewKey">Unique identifier for the requested view</param>
		public void ActivateView(string viewKey)
		{
			ActivateView(viewKey, null);
		}

		/// <summary>
		/// Activates the view corresponding to the given key/identifier. If the view is already
		/// active or the requested view cannot be activated, nothing happens. If changing views,
		/// the current view is deactivated first. If the given view key has not yet been used,
		/// it will be added to the collection of view keys.
		/// </summary>
		/// <param name="viewKey">Key/identifier of the requested view</param>
		/// <param name="payload">Additional activation payload (ignored)</param>
		public virtual void ActivateView(string viewKey, object payload)
		{
			if (IsViewIneligbleForActiviation(viewKey))
				return;

			if (_currentViewKey != HelpModuleViewKeys.Empty)
				DeactivateCurrentView();

			AddAndActivateView(viewKey, payload);

			_eventAggregator.GetEvent<ActiveViewTitleChanged>().Publish("Help");
		}

		#endregion

		#region Helper methods

		private bool IsViewIneligbleForActiviation(string viewKey)
		{
			var isViewEmpty = viewKey.Equals(HelpModuleViewKeys.Empty);
			var isMissingFromViewCollection = !_views.ContainsKey(viewKey);

			if (isViewEmpty || isMissingFromViewCollection) return true;

			return _regionManager.Regions[RegionNames.WorkspaceRegion].ActiveViews.Contains(_views[viewKey]);
		}

		private void DeactivateCurrentView()
		{
			var workspaceRegion = _regionManager.Regions[RegionNames.WorkspaceRegion];
			workspaceRegion.Deactivate(_views[_currentViewKey]);
			_currentViewKey = HelpModuleViewKeys.Empty;
			_eventAggregator.GetEvent<HelpViewChanged>().Publish(HelpModuleViewKeys.Empty);
		}

		private void AddAndActivateView(string viewKey, object payload)
		{
			var region = _regionManager.Regions[RegionNames.WorkspaceRegion];
			var requestedView = _views[viewKey];

			// Add view to the region
			if (!region.Views.Contains(requestedView))
				region.Add(requestedView);

			// If activating the DocumentView, it needs a document
			if (viewKey == HelpModuleViewKeys.DocumentView && payload is FileInfo)
				_eventAggregator.GetEvent<HelpDocumentSelected>().Publish((FileInfo) payload);

			region.Activate(requestedView);
			_eventAggregator.GetEvent<HelpViewChanged>().Publish(viewKey);

			_currentViewKey = viewKey;
		}

		#endregion
	}
}