﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Help.Common;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Models;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Help.Services
{
	public class HelpDocumentManager : IHelpDocumentManager
	{
		#region Fields

		private readonly SettingObserver<ISettingsManager> _settingsManager;

		private readonly IDocumentGenerator _bvaSignoffDocumentGenerator;
		private readonly IFileManager _fileManager;
		private readonly ILoggerFacade _logger;

		#endregion

		#region Ctor

		public HelpDocumentManager(IFileManager fileManager, IDocumentGenerator bvaSignoffDocumentGenerator, ILoggerFacade logger, ISettingsManager settingsManager)
		{
			if (fileManager == null)
				throw new ArgumentNullException("fileManager");
			
			if (bvaSignoffDocumentGenerator == null)
				throw new ArgumentNullException("bvaSignoffDocumentGenerator");

			if (logger == null)
				throw new ArgumentNullException("logger");

			if (settingsManager == null)
				throw new ArgumentNullException("settingsManager");

			_fileManager = fileManager;
			_bvaSignoffDocumentGenerator = bvaSignoffDocumentGenerator;
			_logger = logger;
			_settingsManager = new SettingObserver<ISettingsManager>(settingsManager);

			_settingsManager.RegisterHandler(SettingKeys.SystemDepartmentAddress, s => CreateManualSignoffSheet());
			_settingsManager.RegisterHandler(SettingKeys.SystemDepartmentDirector, s => CreateManualSignoffSheet());
			_settingsManager.RegisterHandler(SettingKeys.SystemDepartmentName, s => CreateManualSignoffSheet());
			_settingsManager.RegisterHandler(SettingKeys.SystemDepartmentPhoneNumber, s => CreateManualSignoffSheet());
			_settingsManager.RegisterHandler(SettingKeys.SystemHospitalName, s => CreateManualSignoffSheet());
		}

		#endregion

		#region Methods

		#region Document Category Methods

		public IEnumerable<DocumentCategory> RetrieveHelpDocumentCategories(string filePath)
		{
			var collection = new List<DocumentCategory>();
			DocumentCategory disclaimer = null;

			if (!_fileManager.DirectoryExists(filePath))
				return collection;

			foreach (var directory in _fileManager.GetSubDirectoriesInDirectory(filePath))
			{
				var files = directory.GetFiles().Where(file => file.Extension == HelpConstants.PdfFileExtension);

				var newCategory = new DocumentCategory(directory.Name, files);

				if (newCategory.Name != HelpConstants.DisclaimerName)
					collection.Add(newCategory);
				else
					disclaimer = newCategory;
			}

			if (disclaimer != null)
				collection.Add(disclaimer);

			return collection;
		}

		#endregion

		#region Create/Delete Document Methods

		public void CreateManualSignoffSheet()
		{
			try
			{
				_bvaSignoffDocumentGenerator.Generate();
			}
			catch (Exception ex)
			{
				_logger.Log("Unable to generate the LOM sign-off PDF: " + ex.Message, Category.Exception, Priority.High);
			}
		}

		#endregion

		#endregion
	}
}
