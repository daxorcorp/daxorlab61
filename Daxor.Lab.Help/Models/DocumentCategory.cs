﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Daxor.Lab.Help.Models
{
	public class DocumentCategory
	{
		public string Name { get; private set; }
		public IEnumerable<FileInfo> Files { get; private set; }

		public DocumentCategory(string name, IEnumerable<FileInfo> files)
		{
			Name = name;

			Files = files;
		}

		#region Equality

		protected bool Equals(DocumentCategory other)
		{
			var namesEqual = Name == other.Name;
			var fileCountsEqual = Files.Count() == other.Files.Count();

			if (!namesEqual || !fileCountsEqual)
				return false;

			for (var index = 0; index < Files.Count(); index++)
			{
				if (Files.ToList()[index].FullName != other.Files.ToList()[index].FullName)
					return false;
			}

			return true;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != GetType()) return false;
			return Equals((DocumentCategory) obj);
		}

		public override int GetHashCode()
		{
			return (Name != null ? Name.GetHashCode() : 0);
		}

		#endregion
	}
}
