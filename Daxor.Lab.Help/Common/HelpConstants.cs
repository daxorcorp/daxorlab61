﻿namespace Daxor.Lab.Help.Common
{
	public static class HelpConstants
	{
		public static string BvaManualSignoffFormName = "BVA-100 Operations Manual Sign-off Form.pdf";
		public static string BvaManualSignoffFormFileName = @"C:\ProgramData\Daxor\Lab\Help\Process Forms\BVA-100 Operations Manual Sign-off Form.pdf";
		public static string DisclaimerName = "Disclaimer";
		public static string DisclaimerText = "Help Documents are current as of the most recent service date. To check for additional updates, contact DAXOR Customer Service at 1-866-548-7282.";
		public static string DocumentPath = @"C:\ProgramData\Daxor\Lab\Help";
		public static string EmptyWatermarkText = "";
		public static string NoDocumentsFoundText = "No Help Documents Found";
		public static string PdfFileExtension = ".pdf";
	}
}
