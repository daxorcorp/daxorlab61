﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Daxor.Lab.Help.Common.Converters
{
	public class FileNameToFileNameWithoutExtensionConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var file = value as string;

			if (file == null)
				return "";

			var stopIndex = file.LastIndexOf('.');

			return stopIndex < 0 ? file : file.Substring(0, stopIndex);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
