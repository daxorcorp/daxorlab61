﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Data;

namespace Daxor.Lab.Help.Common.Converters
{
	public class HelpDocumentsToWatermarkTextConverter : IValueConverter
	{
		/// <summary>
		/// Converts an IEnumerable of DocumentCategories to the appropriate
		/// text for the Help category watermark.
		/// </summary>
		/// <param name="value">Value to convert (e.g., a list of files)</param>
		/// <param name="targetType">Type this method returns (i.e., string)</param>
		/// <param name="parameter">Converter parameter (ignored)</param>
		/// <param name="culture">Culture parameter (ignored)</param>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (!(value is IEnumerable<FileInfo>))
				return HelpConstants.EmptyWatermarkText;

			var documents = value as IEnumerable<FileInfo>;

			return documents.Any() ? HelpConstants.EmptyWatermarkText : HelpConstants.NoDocumentsFoundText;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
