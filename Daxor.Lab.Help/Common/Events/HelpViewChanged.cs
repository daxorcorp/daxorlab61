﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Help.Common.Events
{
	public class HelpViewChanged : CompositePresentationEvent<string> {}
}
