﻿using System.IO;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Help.Common.Events
{
    public class HelpDocumentSelected : CompositePresentationEvent<FileInfo> {}
}
