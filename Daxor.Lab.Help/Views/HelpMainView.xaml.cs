﻿using System;
using System.Windows.Controls;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.Practices.Composite.Events;
using IActiveAware = Microsoft.Practices.Composite.IActiveAware;

namespace Daxor.Lab.Help.Views
{
    /// <summary>
    /// Interaction logic for HelpMainView.xaml
    /// </summary>
    public partial class HelpMainView : IActiveAware
    {
        private ScrollViewer _scrollViewer;
        private readonly IEventAggregator _eventAggregator;
        private bool _isRegisteredToScrollChangedEvent;

        #region Ctor

        public HelpMainView()
        {
            InitializeComponent();
        }

        public HelpMainView(HelpMainViewModel vm, IEventAggregator eventAggregator)
            : this()
        {
            _eventAggregator = eventAggregator;
            DataContext = vm;
            SubscribeToEvents();
        }

        #endregion

        #region Private Helper Methods

        void OnHelpCategoriesListLayoutUpdated()
        {
            var listBox = uiHelpDocumentsList;
            _scrollViewer = VisualHelperEx.FindVisualChild<ScrollViewer>(listBox);

            if (_scrollViewer == null)
                return;

            _eventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(new ScrollViewerPageInfoChangedPayload(listBox.Items.Count, listBox.Items.Count, _scrollViewer.ExtentHeight, _scrollViewer.ViewportHeight, _scrollViewer.VerticalOffset, AppModuleIds.Help));

            if (_isRegisteredToScrollChangedEvent) return;

            _scrollViewer.ScrollChanged += (s, e2) => _eventAggregator.GetEvent<ScrollViewerPageInfoChanged>()
                .Publish(new ScrollViewerPageInfoChangedPayload(listBox.Items.Count, listBox.Items.Count, _scrollViewer.ExtentHeight, _scrollViewer.ViewportHeight, _scrollViewer.VerticalOffset, AppModuleIds.Help));
            _isRegisteredToScrollChangedEvent = true;
        }

        private void SubscribeToEvents()
        {
            uiHelpCategoriesList.LayoutUpdated += (s, e2) => OnHelpCategoriesListLayoutUpdated();
        }

        private void OnScrollViewerScrollChanged(ScrollViewerScrollChangedEventPayload scrollEvent)
        {
            switch (scrollEvent)
            {
                case ScrollViewerScrollChangedEventPayload.ScrollTop:
                    _scrollViewer.ScrollToTop();
                    break;
                case ScrollViewerScrollChangedEventPayload.ScrollPageUp:
                    _scrollViewer.PageUp();
                    break;
                case ScrollViewerScrollChangedEventPayload.ScrollPageDown:
                    _scrollViewer.PageDown();
                    break;
                case ScrollViewerScrollChangedEventPayload.ScrollBottom:
                    _scrollViewer.ScrollToBottom();
                    break;
            }
        }

        #endregion

        #region IActive

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                var vmAware = DataContext as IActiveAware;
                if (vmAware != null)
                    vmAware.IsActive = value;

                if (value) _eventAggregator.GetEvent<ScrollViewerScrollChanged>().Subscribe(OnScrollViewerScrollChanged);
                else _eventAggregator.GetEvent<ScrollViewerScrollChanged>().Unsubscribe(OnScrollViewerScrollChanged);
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
