﻿using System.Windows.Controls;
using System.Windows.Input;
using Daxor.Lab.Help.ViewModels;
using Telerik.Windows.Controls;

namespace Daxor.Lab.Help.Views
{
	/// <summary>
	/// Interaction logic for HelpDocumentView.xaml
	/// </summary>
	public partial class HelpDocumentView
	{
		#region Fields

		private readonly DelegateCommand _goToFirstPageCommand;
		private readonly DelegateCommand _goToLastPageCommand;
		private readonly DelegateCommand _printAllCommand;
		private readonly DelegateCommand _printPageCommand;

		#endregion

		#region Ctor

		public HelpDocumentView()
		{
			_goToFirstPageCommand = new DelegateCommand(GoToFirstPageCommandExecute);
			_goToLastPageCommand = new DelegateCommand(GoToLastPageCommandExecute);
			_printAllCommand = new DelegateCommand(PrintAllCommandExecute);
			_printPageCommand = new DelegateCommand(PrintPageCommandExecute);

			InitializeComponent();
		}

		public HelpDocumentView(HelpDocumentViewModel vm) : this()
		{
			DataContext = vm;
		}

		#endregion

		#region Commands

		public ICommand GoToFirstPageCommand
		{
			get { return _goToFirstPageCommand; }
		}
		void GoToFirstPageCommandExecute(object arg)
		{
			((HelpDocumentViewModel) DataContext).GoToFirstPage(PdfViewer);
		}

		public ICommand GoToLastPageCommand
		{
			get { return _goToLastPageCommand; }
		}

		void GoToLastPageCommandExecute(object arg)
		{
			((HelpDocumentViewModel) DataContext).GoToLastPage(PdfViewer);
		}

		private void CurrentPageEnter(object sender, KeyEventArgs e)
		{
			((HelpDocumentViewModel) DataContext).GoToPageEntered((TextBox) sender, e);
		}

		public ICommand PrintAllCommand
		{
			get { return _printAllCommand; }
		}

		void PrintAllCommandExecute(object arg)
		{
			((HelpDocumentViewModel) DataContext).PrintAll(PdfViewer);
		}

		public ICommand PrintPageCommand
		{
			get { return _printPageCommand; }
		}

		void PrintPageCommandExecute(object arg)
		{
			((HelpDocumentViewModel) DataContext).PrintCurrentPage(PdfViewer);
		}

		#endregion
	}
}
