﻿using System;
using System.IO;
using System.Windows.Controls;
using System.Windows.Input;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Unity;
using Telerik.Windows.Controls;
using Telerik.Windows.Documents.Fixed.Print;
using IActiveAware = Microsoft.Practices.Composite.IActiveAware;
using ViewModelBase = Daxor.Lab.Infrastructure.Base.ViewModelBase;

namespace Daxor.Lab.Help.ViewModels
{
	public class HelpDocumentViewModel : ViewModelBase, IActiveAware
	{
		#region Fields

		private readonly IAppModuleNavigator _navigator;
		private readonly IEventAggregator _eventAggregator;
		
		private readonly DelegateCommand _goToPreviousScreenCommand;

		private FileInfo _selectedDocument;

		#endregion

		#region Ctor

		public HelpDocumentViewModel([Dependency(AppModuleIds.Help)] IAppModuleNavigator appModuleNavigator, IEventAggregator eventAggregator)
		{
			if (appModuleNavigator == null)
				throw new ArgumentNullException("appModuleNavigator");
			if (eventAggregator == null)
				throw new ArgumentNullException("eventAggregator");

			_eventAggregator = eventAggregator;
			_navigator = appModuleNavigator;

			_goToPreviousScreenCommand = new DelegateCommand(GoToPreviousScreenCommandExecute);

			SubscribeToAggregateEvents();
		}

		#endregion

		#region Properties

		public FileInfo SelectedDocument
		{
			get { return _selectedDocument; }
			set
			{
				SetValue(ref _selectedDocument, "SelectedDocument", value);
				FirePropertyChanged("SelectedDocument");
			}
		}

		#endregion

		#region Commands

		public ICommand GoToPreviousScreenCommand
		{
			get { return _goToPreviousScreenCommand; }
		}

		private void GoToPreviousScreenCommandExecute(object unusedButNeededPlaceholder)
		{
			SelectedDocument = null;
			_navigator.ActivateView(HelpModuleViewKeys.MainView);
		}

		public void GoToFirstPage(FixedDocumentViewerBase pdfViewer)
		{
			ThrowExceptionIfNull(pdfViewer);
			pdfViewer.GoToPage(1);
		}

		public void GoToLastPage(FixedDocumentViewerBase pdfViewer)
		{
			ThrowExceptionIfNull(pdfViewer);
			pdfViewer.GoToPage(pdfViewer.Document.Pages.Count);
		}

		public void GoToPageEntered(TextBox textBox, KeyEventArgs args)
		{
			if (textBox == null || args == null) return;

			if (args.Key != Key.Enter) return;

			var bindingExpression = textBox.GetBindingExpression(TextBox.TextProperty);
			if (bindingExpression != null) bindingExpression.UpdateSource();
		}

		public void PrintAll(FixedDocumentViewerBase pdfViewer)
		{
			ThrowExceptionIfNull(pdfViewer);
			var settings = new PrintSettings { UseDefaultPrinter = true };

			pdfViewer.Print(settings);
		}

		public void PrintCurrentPage(FixedDocumentViewerBase pdfViewer)
		{
			ThrowExceptionIfNull(pdfViewer);
			var dialog = new PrintDialog
			{
				PageRangeSelection = PageRangeSelection.UserPages,
				PageRange = new PageRange(pdfViewer.CurrentPageNumber)
			};

			var settings = new PrintSettings { UseDefaultPrinter = true };

			pdfViewer.Print(dialog, settings);
		}

		#endregion

		#region Events

		private void SubscribeToAggregateEvents()
		{
			_eventAggregator.GetEvent<HelpDocumentSelected>().Subscribe(info => { SelectedDocument = info; });
			_eventAggregator.GetEvent<HelpViewChanged>().Subscribe(viewKey =>
			{
				if (viewKey == HelpModuleViewKeys.Empty)
					SelectedDocument = null;
			});
		}

		private void ThrowExceptionIfNull(FixedDocumentViewerBase pdfViewer)
		{
			if (pdfViewer == null) throw new ArgumentNullException("pdfViewer");
		}

		#endregion

		#region IActiveAware Members

		public bool IsActive { get; set; }
		public event EventHandler IsActiveChanged = delegate { };

		#endregion
	}
}
