﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Daxor.Lab.Help.Common;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Models;
using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using IActiveAware = Microsoft.Practices.Composite.IActiveAware;
using ViewModelBase = Daxor.Lab.Infrastructure.Base.ViewModelBase;

namespace Daxor.Lab.Help.ViewModels
{
	public class HelpMainViewModel : ViewModelBase, IActiveAware
	{
		#region Fields

		private readonly IAppModuleNavigator _appModuleNavigator;
		private readonly IEventAggregator _eventAggregator;
	    private readonly IMessageBoxDispatcher _messageBoxDispatcher;

		private readonly DelegateCommand<object> _scrollPageTopCommand;
		private readonly DelegateCommand<object> _scrollPageUpCommand;
		private readonly DelegateCommand<object> _scrollPageDownCommand;
		private readonly DelegateCommand<object> _scrollPageBottomCommand; 
		private readonly DelegateCommand<FileInfo> _selectHelpDocumentCommand;
		private readonly DelegateCommand<object> _showDisclaimerCommand; 

		private List<DocumentCategory> _categories;
		private DocumentCategory _selectedDocumentCategory;
		private FileInfo _selectedFile;

		private bool _isInitialDataLoading;
		private string _startingIndex = "1";
		private string _endingIndex;
		private string _totalItemsInScrollViewer;
		private string _totalItemsInScrollviewerSourceCollection;

		#endregion

		#region Ctor

		public HelpMainViewModel([Dependency(AppModuleIds.Help)] IAppModuleNavigator appModuleNavigator, IHelpDocumentManager helpDocumentManager, 
																 IEventAggregator eventAggregator, IMessageBoxDispatcher messageBoxDispatcher)
		{
		    IsInitialDataLoading = true;

			_selectedDocumentCategory = null;

			if (helpDocumentManager == null)
				throw new ArgumentNullException("helpDocumentManager");

			if (appModuleNavigator == null)
				throw new ArgumentNullException("appModuleNavigator");

			if (eventAggregator == null)
				throw new ArgumentNullException("eventAggregator");

			if (messageBoxDispatcher == null)
				throw new ArgumentNullException("messageBoxDispatcher");

			_appModuleNavigator = appModuleNavigator;
			_eventAggregator = eventAggregator;
		    _messageBoxDispatcher = messageBoxDispatcher;

			_selectHelpDocumentCommand = new DelegateCommand<FileInfo>(SelectHelpDocumentCommandExecute);

			_scrollPageTopCommand = new DelegateCommand<object>(ScrollPageTopCommandExecute, ScrollPageTopCommandCanExecute);
			_scrollPageUpCommand = new DelegateCommand<object>(ScrollPageUpCommandExecute, ScrollPageUpCommandCanExecute);
			_scrollPageDownCommand = new DelegateCommand<object>(ScrollPageDownCommandExecute, ScrollPageDownCommandCanExecute);
			_scrollPageBottomCommand = new DelegateCommand<object>(ScrollPageBottomCommandExecute, ScrollPageBottomCommandCanExecute);

			_showDisclaimerCommand = new DelegateCommand<object>(ShowDisclaimerCommandExecute);

			SubscribeToAggregateEvents();

            helpDocumentManager.CreateManualSignoffSheet();

			Categories = helpDocumentManager.RetrieveHelpDocumentCategories(HelpConstants.DocumentPath).ToList();

			IsInitialDataLoading = false;
		}

		#endregion
		
		#region Properties

		public bool IsInitialDataLoading
		{
			get { return _isInitialDataLoading; }
			set
			{
				SetValue(ref _isInitialDataLoading, "IsInitialDataLoading", value);
				FirePropertyChanged("IsInitialDataLoading");
			}
		}

		public List<DocumentCategory> Categories
		{
			get { return _categories; }
			set
			{
				SetValue(ref _categories, "Categories", value);
				FirePropertyChanged("Categories");
			}
		}

		public DocumentCategory SelectedCategory
		{
			get
			{
				return _selectedDocumentCategory;
			}
			set
			{
				SetValue(ref _selectedDocumentCategory, "SelectedCategory", value);
				FirePropertyChanged("SelectedCategory");
			}
		}

		public FileInfo SelectedFile
		{
			get { return _selectedFile; }
			set
			{
				SetValue(ref _selectedFile, "SelectedFile", value);
				FirePropertyChanged("SelectedFile");
				SelectHelpDocumentCommand.Execute(SelectedFile);
			}
		}

		public string StartingIndex
		{
			get { return _startingIndex; }
			set { SetValue(ref _startingIndex, "StartingIndex", value); }
		}

		public string EndingIndex
		{
			get { return _endingIndex; }
			set { SetValue(ref _endingIndex, "EndingIndex", value); }
		}

		public string TotalItemsInScrollViewer
		{
			get { return _totalItemsInScrollViewer; }
			set { SetValue(ref _totalItemsInScrollViewer, "TotalItemsInScrollViewer", value); }
		}

		public string TotalItemsInScrollviewerSourceCollection
		{
			get { return _totalItemsInScrollviewerSourceCollection; }
			set { SetValue(ref _totalItemsInScrollviewerSourceCollection, "TotalItemsInScrollviewerSourceCollection", value); }
		}

		#endregion

		#region Commands

		public ICommand SelectHelpDocumentCommand
		{
			get { return _selectHelpDocumentCommand; }
		}

		private void SelectHelpDocumentCommandExecute(FileInfo selectedDocument)
		{
			if (_selectedFile == null)
				return;

			_appModuleNavigator.ActivateView(HelpModuleViewKeys.DocumentView, _selectedFile);
		}

		public ICommand ShowDisclaimerCommand
		{
			get { return _showDisclaimerCommand; }
		}

		private void ShowDisclaimerCommandExecute(object unusedButNeededPlaceholder)
		{
			_messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information, HelpConstants.DisclaimerText, HelpConstants.DisclaimerName, MessageBoxChoiceSet.Ok);
		}

		#region Page Navigation

		public ICommand ScrollPageTopCommand
		{
			get { return _scrollPageTopCommand; }
		}

		private void ScrollPageTopCommandExecute(object unusedButNeededPlaceholder)
		{
			_eventAggregator.GetEvent<ScrollViewerScrollChanged>().Publish(ScrollViewerScrollChangedEventPayload.ScrollTop);
		}
		
		private bool ScrollPageTopCommandCanExecute(object arg)
		{
			return !IsInitialDataLoading && StartingIndex != "1" && StartingIndex != "-" && StartingIndex != "0";
		}

		public ICommand ScrollPageUpCommand
		{
			get { return _scrollPageUpCommand; }
		}
		
		private void ScrollPageUpCommandExecute(object unusedButNeededPlaceholder)
		{
			_eventAggregator.GetEvent<ScrollViewerScrollChanged>().Publish(ScrollViewerScrollChangedEventPayload.ScrollPageUp);
		}
		
		private bool ScrollPageUpCommandCanExecute(object arg)
		{
			return !IsInitialDataLoading && StartingIndex != "1" && StartingIndex != "-" && StartingIndex != "0";
		}

		public ICommand ScrollPageDownCommand
		{
			get { return _scrollPageDownCommand; }
		}

		private void ScrollPageDownCommandExecute(object unusedButNeededPlaceholder)
		{
			_eventAggregator.GetEvent<ScrollViewerScrollChanged>().Publish(ScrollViewerScrollChangedEventPayload.ScrollPageDown);
		}
		
		private bool ScrollPageDownCommandCanExecute(object arg)
		{
			return !IsInitialDataLoading && EndingIndex != TotalItemsInScrollViewer;
		}

		public ICommand ScrollPageBottomCommand
		{
			get { return _scrollPageBottomCommand; }
		}

		private void ScrollPageBottomCommandExecute(object unusedButNeededPlaceholder)
		{
			_eventAggregator.GetEvent<ScrollViewerScrollChanged>().Publish(ScrollViewerScrollChangedEventPayload.ScrollBottom);
		}

		bool ScrollPageBottomCommandCanExecute(object arg)
		{
			return !IsInitialDataLoading && EndingIndex != TotalItemsInScrollViewer;
		}

		#endregion

		#endregion

		#region Helpers

		protected virtual void EvaluateScrollCommandCanExecute()
		{
			_scrollPageBottomCommand.RaiseCanExecuteChanged();
			_scrollPageDownCommand.RaiseCanExecuteChanged();
			_scrollPageTopCommand.RaiseCanExecuteChanged();
			_scrollPageUpCommand.RaiseCanExecuteChanged();
		}

		private void CalculatePageInfoForScrollViewer(ScrollViewerPageInfoChangedPayload scrollViewerPageInfo)
		{
			var itemCount = scrollViewerPageInfo.ItemCount;
			var totalItemCount = scrollViewerPageInfo.TotalItemCount;
			var extentHeight = scrollViewerPageInfo.ScrollViewerExtentHeight;
			var pageHeight = scrollViewerPageInfo.ScrollViewerViewportHeight;
			var verticalOffset = scrollViewerPageInfo.ScrollViewerVerticalOffset;

			var itemHeight = extentHeight / itemCount;

			StartingIndex = ((int) Math.Round(verticalOffset / itemHeight) + 1).ToString(CultureInfo.InvariantCulture);
			var endingIndex = ((int) Math.Round((verticalOffset + pageHeight) / itemHeight));

			// ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
			if (endingIndex > itemCount)
				EndingIndex = itemCount.ToString(CultureInfo.InvariantCulture);
			else
				EndingIndex = ((int) Math.Round((verticalOffset + pageHeight) / itemHeight)).ToString(CultureInfo.InvariantCulture);

			TotalItemsInScrollViewer = itemCount.ToString(CultureInfo.InvariantCulture);
			TotalItemsInScrollviewerSourceCollection = totalItemCount.ToString(CultureInfo.InvariantCulture);

			if (itemCount == 0)
			{
				EndingIndex = "0";
				StartingIndex = "0";
			}
		}

		#endregion

		#region Events

		void OnScrollViewerPageInfoChanged(ScrollViewerPageInfoChangedPayload scrollViewerPageInfo)
		{
			if (scrollViewerPageInfo.AppModuleId != AppModuleIds.Help) return;

			CalculatePageInfoForScrollViewer(scrollViewerPageInfo);
			EvaluateScrollCommandCanExecute();
		}

		public void SubscribeToAggregateEvents()
		{
			_eventAggregator.GetEvent<HelpViewChanged>().Subscribe(viewName =>
			{
				SelectedFile = null;

				switch (viewName)
				{
					case HelpModuleViewKeys.Empty:
						SelectedCategory = null;
						SelectedFile = null;
						break;
					case HelpModuleViewKeys.MainView:
						SelectedCategory = Categories.FirstOrDefault();
						break;
				}
			});

			_eventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Subscribe(OnScrollViewerPageInfoChanged);
		}

		#endregion

		#region IActiveAware Members

		public bool IsActive { get; set; }
		public event EventHandler IsActiveChanged = delegate { };

		#endregion
	}
}
