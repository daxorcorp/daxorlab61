﻿using System.Collections.Generic;
using Daxor.Lab.Help.Models;

namespace Daxor.Lab.Help.Interfaces
{
	public interface IHelpDocumentManager
	{
		IEnumerable<DocumentCategory> RetrieveHelpDocumentCategories(string filePath);
		void CreateManualSignoffSheet();
	}
}
