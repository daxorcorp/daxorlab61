﻿namespace Daxor.Lab.Help.Interfaces
{
	public interface IDocumentGenerator
	{
		void Generate();
	}
}
