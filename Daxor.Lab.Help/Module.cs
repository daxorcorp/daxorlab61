﻿using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Services;
using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageBox;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.Help
{
	public class Module : AppModule
	{
		private const string ModuleName = "Help";
		
		/// <summary>
		/// This is the constructor for the help module.  It's
		/// name is "Help" and is displayed fourth on the main
		/// page and in the navigation menu.
		/// </summary>
		/// <param name="container">UnityContainer</param>
		public Module(IUnityContainer container)
			: base(AppModuleIds.Help, container)
		{
			ShortDisplayName = "Help";
			FullDisplayName = "Help";
			DisplayOrder = 4;
			IsConfigurable = false;
		}

		public override void InitializeAppModule()
		{
			RegisterLomSignoffFormGenerator();
			RegisterSystemFileGenerator();
			RegisterHelpDocumentManager();
			RegisterMessageBoxDispatcher();
			RegisterHelpModuleNavigator();
			InitializeAppModuleNavigator();
		}

	    private void InitializeAppModuleNavigator()
	    {
	        try
	        {
	            UnityContainer.Resolve<IAppModuleNavigator>(UniqueAppId).Initialize();
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Help Module Navigator");
	        }
	    }

	    private void RegisterHelpModuleNavigator()
	    {
	        try
	        {
	            UnityContainer.RegisterType<IAppModuleNavigator, HelpModuleNavigator>(UniqueAppId,
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "register the Help Module Navigator");
	        }
	    }

	    private void RegisterMessageBoxDispatcher()
	    {
	        try
	        {
	            UnityContainer.RegisterType<IMessageBoxDispatcher, XamlMessageBoxDispatcher>();
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Message Box Dispatcher");
	        }
	    }

	    private void RegisterHelpDocumentManager()
	    {
	        try
	        {
	            UnityContainer.RegisterType<IHelpDocumentManager, HelpDocumentManager>();
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Help Document Manager");
	        }
	    }

	    private void RegisterSystemFileGenerator()
	    {
	        try
	        {
	            UnityContainer.RegisterType<IFileManager, SystemFileManager>();
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the System File Manager");
	        }
	    }

	    private void RegisterLomSignoffFormGenerator()
	    {
	        try
	        {
	            UnityContainer.RegisterType<IDocumentGenerator, LomSignoffFormGenerator>();
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the LOM Signoff Form Generator");
	        }
	    }
	}
}
