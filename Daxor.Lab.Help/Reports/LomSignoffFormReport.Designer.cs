namespace Daxor.Lab.Help.Reports
{
    partial class LomSignoffFormReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBoxHospitalName = new Telerik.Reporting.TextBox();
            this.textBoxHospitalAddress = new Telerik.Reporting.TextBox();
            this.textBoxHospitalPhone = new Telerik.Reporting.TextBox();
            this.textBoxDepartmentName = new Telerik.Reporting.TextBox();
            this.textBoxDepartmentDirector = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBoxDisclaimer1 = new Telerik.Reporting.TextBox();
            this.textBoxDisclaimer2 = new Telerik.Reporting.TextBox();
            this.textBoxDisclaimer3 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.tableSignatoryArea = new Telerik.Reporting.Table();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox3
            // 
            this.textBox3.CanGrow = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.62499666213989258D), Telerik.Reporting.Drawing.Unit.Inch(0.24000000953674316D));
            this.textBox3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "DATE";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.124983549118042D), Telerik.Reporting.Drawing.Unit.Inch(0.24000000953674316D));
            this.textBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "NAME";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1249833106994629D), Telerik.Reporting.Drawing.Unit.Inch(0.24000000953674316D));
            this.textBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "SIGNATURE";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.6249966025352478D), Telerik.Reporting.Drawing.Unit.Inch(0.24000000953674316D));
            this.textBox9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.StyleName = "";
            this.textBox9.Value = "INITIALS";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(3.3599998950958252D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxHospitalName,
            this.textBoxHospitalAddress,
            this.textBoxHospitalPhone,
            this.textBoxDepartmentName,
            this.textBoxDepartmentDirector,
            this.textBox1,
            this.textBox2,
            this.textBoxDisclaimer1,
            this.textBoxDisclaimer2,
            this.textBoxDisclaimer3});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBoxHospitalName
            // 
            this.textBoxHospitalName.Docking = Telerik.Reporting.DockingStyle.Top;
            this.textBoxHospitalName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBoxHospitalName.Name = "textBoxHospitalName";
            this.textBoxHospitalName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxHospitalName.Style.Font.Name = "Arial";
            this.textBoxHospitalName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxHospitalName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxHospitalName.Value = "Hostpital Name";
            // 
            // textBoxHospitalAddress
            // 
            this.textBoxHospitalAddress.Docking = Telerik.Reporting.DockingStyle.Top;
            this.textBoxHospitalAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxHospitalAddress.Name = "textBoxHospitalAddress";
            this.textBoxHospitalAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxHospitalAddress.Style.Font.Name = "Arial";
            this.textBoxHospitalAddress.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxHospitalAddress.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxHospitalAddress.Value = "Hospital Address";
            // 
            // textBoxHospitalPhone
            // 
            this.textBoxHospitalPhone.Docking = Telerik.Reporting.DockingStyle.Top;
            this.textBoxHospitalPhone.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.4791666567325592D));
            this.textBoxHospitalPhone.Name = "textBoxHospitalPhone";
            this.textBoxHospitalPhone.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxHospitalPhone.Style.Font.Name = "Arial";
            this.textBoxHospitalPhone.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxHospitalPhone.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxHospitalPhone.Value = "Department Phone Number";
            // 
            // textBoxDepartmentName
            // 
            this.textBoxDepartmentName.Docking = Telerik.Reporting.DockingStyle.Top;
            this.textBoxDepartmentName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.71875D));
            this.textBoxDepartmentName.Name = "textBoxDepartmentName";
            this.textBoxDepartmentName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxDepartmentName.Style.Font.Name = "Arial";
            this.textBoxDepartmentName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxDepartmentName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxDepartmentName.Value = "DepartmentName";
            // 
            // textBoxDepartmentDirector
            // 
            this.textBoxDepartmentDirector.Docking = Telerik.Reporting.DockingStyle.Top;
            this.textBoxDepartmentDirector.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.95833331346511841D));
            this.textBoxDepartmentDirector.Name = "textBoxDepartmentDirector";
            this.textBoxDepartmentDirector.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxDepartmentDirector.Style.Font.Name = "Arial";
            this.textBoxDepartmentDirector.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxDepartmentDirector.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxDepartmentDirector.Value = "Department Director";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.440000057220459D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.4999604225158691D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Arial";
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Daxor BVA-100 Laboratory Operations Manual";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.9199999570846558D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.4999604225158691D), Telerik.Reporting.Drawing.Unit.Inch(0.24400000274181366D));
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "The signatures below are evidence of:";
            // 
            // textBoxDisclaimer1
            // 
            this.textBoxDisclaimer1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(2.4000000953674316D));
            this.textBoxDisclaimer1.Name = "textBoxDisclaimer1";
            this.textBoxDisclaimer1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.4999208450317383D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
            this.textBoxDisclaimer1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxDisclaimer1.Value = "textBoxDisclaimer1";
            // 
            // textBoxDisclaimer2
            // 
            this.textBoxDisclaimer2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(2.6400787830352783D));
            this.textBoxDisclaimer2.Name = "textBoxDisclaimer2";
            this.textBoxDisclaimer2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.4999604225158691D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
            this.textBoxDisclaimer2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxDisclaimer2.Value = "textBoxDisclaimer2";
            // 
            // textBoxDisclaimer3
            // 
            this.textBoxDisclaimer3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(2.8801577091217041D));
            this.textBoxDisclaimer3.Name = "textBoxDisclaimer3";
            this.textBoxDisclaimer3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.4999208450317383D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
            this.textBoxDisclaimer3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxDisclaimer3.Value = "textBoxDisclaimer3";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(2D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.tableSignatoryArea});
            this.detail.Name = "detail";
            // 
            // tableSignatoryArea
            // 
            this.tableSignatoryArea.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.62499672174453735D)));
            this.tableSignatoryArea.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.124983549118042D)));
            this.tableSignatoryArea.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.124983549118042D)));
            this.tableSignatoryArea.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.62499666213989258D)));
            this.tableSignatoryArea.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24000000953674316D)));
            this.tableSignatoryArea.Body.SetCellContent(0, 0, this.textBox4);
            this.tableSignatoryArea.Body.SetCellContent(0, 1, this.textBox6);
            this.tableSignatoryArea.Body.SetCellContent(0, 2, this.textBox8);
            this.tableSignatoryArea.Body.SetCellContent(0, 3, this.textBox10);
            tableGroup1.Name = "tableGroup";
            tableGroup1.ReportItem = this.textBox3;
            tableGroup2.Name = "tableGroup1";
            tableGroup2.ReportItem = this.textBox5;
            tableGroup3.Name = "tableGroup2";
            tableGroup3.ReportItem = this.textBox7;
            tableGroup4.Name = "group";
            tableGroup4.ReportItem = this.textBox9;
            this.tableSignatoryArea.ColumnGroups.Add(tableGroup1);
            this.tableSignatoryArea.ColumnGroups.Add(tableGroup2);
            this.tableSignatoryArea.ColumnGroups.Add(tableGroup3);
            this.tableSignatoryArea.ColumnGroups.Add(tableGroup4);
            this.tableSignatoryArea.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox6,
            this.textBox8,
            this.textBox10,
            this.textBox3,
            this.textBox5,
            this.textBox7,
            this.textBox9});
            this.tableSignatoryArea.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.tableSignatoryArea.Name = "tableSignatoryArea";
            tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup5.Name = "detailTableGroup";
            this.tableSignatoryArea.RowGroups.Add(tableGroup5);
            this.tableSignatoryArea.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.4999604225158691D), Telerik.Reporting.Drawing.Unit.Inch(0.48000001907348633D));
            this.tableSignatoryArea.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.62499666213989258D), Telerik.Reporting.Drawing.Unit.Inch(0.24000000953674316D));
            this.textBox4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Value = "=Fields.RowValue";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.124983549118042D), Telerik.Reporting.Drawing.Unit.Inch(0.24000000953674316D));
            this.textBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1249833106994629D), Telerik.Reporting.Drawing.Unit.Inch(0.24000000953674316D));
            this.textBox8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.6249966025352478D), Telerik.Reporting.Drawing.Unit.Inch(0.24000000953674316D));
            this.textBox10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.StyleName = "";
            // 
            // LomSignoffSheetReport
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail});
            this.Name = "LomSignoffSheetReport";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.5D), Telerik.Reporting.Drawing.Unit.Inch(0.5D), Telerik.Reporting.Drawing.Unit.Inch(0.5D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.5D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        public Telerik.Reporting.TextBox textBoxHospitalName;
        public Telerik.Reporting.TextBox textBoxHospitalAddress;
        public Telerik.Reporting.TextBox textBoxHospitalPhone;
        public Telerik.Reporting.TextBox textBoxDepartmentName;
        public Telerik.Reporting.TextBox textBoxDepartmentDirector;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        public Telerik.Reporting.TextBox textBoxDisclaimer1;
        public Telerik.Reporting.TextBox textBoxDisclaimer2;
        public Telerik.Reporting.TextBox textBoxDisclaimer3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        public Telerik.Reporting.Table tableSignatoryArea;
    }
}