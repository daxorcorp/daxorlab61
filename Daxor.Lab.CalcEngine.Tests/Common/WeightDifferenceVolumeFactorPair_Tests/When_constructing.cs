﻿using Daxor.Lab.CalcEngine.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.CalcEngine.Tests.Common.WeightDifferenceVolumeFactorPair_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing
    {
        [TestMethod]
        [TestCategory("CalcEngine")]
        public void Then_the_ctor_sets_the_weight_correctly()
        {
            const double ExpectedWeightDiff = 110f;

            var wdvfPair = new WeightDifferenceVolumeFactorPair(ExpectedWeightDiff, 0f);

            Assert.AreEqual(ExpectedWeightDiff, wdvfPair.WeightDifference, "Constructor isn't setting the weight difference");
        }

        [TestMethod]
        [TestCategory("CalcEngine")]
        public void Then_the_ctor_sets_the_volume_factor_correctly()
        {
            const double ExpectedVolumeFactor = 110f;

            var wdvfPair = new WeightDifferenceVolumeFactorPair(0f, ExpectedVolumeFactor);

            Assert.AreEqual(ExpectedVolumeFactor, wdvfPair.VolumeFactor, "Constructor isn't seem to be setting the volume factor");
        }
    }
    // ReSharper restore InconsistentNaming
}
