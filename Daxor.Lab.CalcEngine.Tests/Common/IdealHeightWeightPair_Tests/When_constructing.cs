﻿using Daxor.Lab.CalcEngine.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.CalcEngine.Tests.Common.IdealHeightWeightPair_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing
    {
        [TestMethod]
        [TestCategory("CalcEngine")]
        public void Then_the_ctor_sets_the_height_correctly()
        {
            const double expectedHeight = 110f;
            
            var hwPair = new IdealHeightWeightPair(expectedHeight, 0f);

            Assert.AreEqual(expectedHeight, hwPair.HeightInCm, "Constructor isn't setting the height");
        }

        [TestMethod]
        [TestCategory("CalcEngine")]
        public void Then_the_ctor_sets_the_weight_correctly()
        {
            const double expectedIdealWeight = 110f;

            var hwPair = new IdealHeightWeightPair(0f, expectedIdealWeight);

            Assert.AreEqual(expectedIdealWeight, hwPair.IdealWeightInKg, "Constructor isn't setting the ideal weight");
        }
    }
    // ReSharper restore InconsistentNaming
}
