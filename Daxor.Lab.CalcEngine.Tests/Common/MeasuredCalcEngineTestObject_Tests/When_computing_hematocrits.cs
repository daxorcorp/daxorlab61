﻿using Daxor.Lab.CalcEngine.Common;
using Daxor.Lab.Infrastructure.DomainModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.CalcEngine.Tests.Common.MeasuredCalcEngineTestObject_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_hematocrits
    {
        [TestMethod]
        [TestCategory("CalcEngine")]
        public void Then_the_baseline_hematocrits_are_not_used()
        {
            const double expectedAvg = 0.5;
            
            // Baseline hematocrit should be ignored
            var engineParams = new MeasuredCalcEngineParams {BaselineHematocrit = 0.2, Points = new BloodVolumePoint[1]};
            var point = new BloodVolumePoint("123", false, false, false, 0, 0, expectedAvg, 0);
            engineParams.Points[0] = point;

            var testObj = new MeasuredCalcEngineTestObject(engineParams);
            var observedAvg = testObj.ComputeTestHematocrit();

            Assert.AreEqual(expectedAvg, observedAvg);
        }
    }
    // ReSharper restore InconsistentNaming
}
