﻿using System;
using Daxor.Lab.CalcEngine.Common;
using Daxor.Lab.CalcEngine.Services;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.CalcEngine.Tests.Module_Tests.SadPath
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_the_Calc_Engine_module
	{
		private Module _module;

		private IUnityContainer _container;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_container = Substitute.For<IUnityContainer>();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_ideal_blood_volume_XML_reader_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IdealBloodVolumeXMLReader, IdealBloodVolumeXMLReader>(Arg.Any<InjectionConstructor>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Calculation Engine", ex.ModuleName);
				Assert.AreEqual("initialize the Ideal Blood Volume XML Reader", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_ideals_calc_engine_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IIdealsCalcEngineService, IdealsCalcEngineService>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Calculation Engine", ex.ModuleName);
				Assert.AreEqual("initialize the Ideals Calculation Engine", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_measured_calc_engine_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IMeasuredCalcEngineService, MeasuredCalcEngineService>()).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Calculation Engine", ex.ModuleName);
				Assert.AreEqual("initialize the Measured Calculation Engine", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}
	}
	// ReSharper restore InconsistentNaming
}