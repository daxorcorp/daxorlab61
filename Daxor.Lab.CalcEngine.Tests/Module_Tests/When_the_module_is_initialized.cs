﻿using Daxor.Lab.CalcEngine.Common;
using Daxor.Lab.CalcEngine.Services;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Rhino.Mocks.Constraints;

namespace Daxor.Lab.CalcEngine.Tests.Module_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_module_is_initialized
    {
        #region Delegate/helpers for Do() handler

        // Set to true if RegisterType() is called with InjectionConstructor argument
        private bool _hasInjectionConstructor;
        
        // This mimics IUnityContainer.RegisterType
        private delegate IUnityContainer RegisterTypeDelegate(params InjectionMember[] injectionMembers);

        // Sets flag if the method call arguments are as expected.
        private IUnityContainer DummyRegisterType(params InjectionMember[] injectionMembers)
        {
            if (injectionMembers.Length > 0)
            {
                if (injectionMembers[0].GetType() == typeof(InjectionConstructor))
                    _hasInjectionConstructor = true;
            }

            return null;
        }

        #endregion

        [TestMethod]
        [TestCategory("CalcEngine")]
        public void Then_the_XML_reader_is_registered()
        {
            // NOTE: We cannot test whether the correct string argument (being injected into the
            // constructor by Unity is correct because (1) no property is exposed on the 
            // InjectionConstructor instance that gives you access to the InjectionConstructor
            // constructor args, and (2) that argument changes every time the test suite runs
            // because the assembly path includes a time stamp. [GMazeroff 10/25/2011]

            // I spent lots of time trying to figure out why the following won't allow the 
            // test to pass. The assembly version/key is the same. The type being passed in is
            // exactly as expected (verified using a hand-rolled mock). [GMazeroff 10/25/2011]
            //
            //mockContainer
            //    .Expect(c => c.RegisterType<IdealBloodVolumeXMLReader, IdealBloodVolumeXMLReader>(new InjectionConstructor("whatever")))
            //    .IgnoreArguments()
            //    .Constraints(Is.TypeOf<InjectionConstructor>());

            // Arrange
            var mockContainer = MockRepository.GenerateMock<IUnityContainer>();
            
            _hasInjectionConstructor = false;
            mockContainer
                .Expect(c => c.RegisterType<IdealBloodVolumeXMLReader, IdealBloodVolumeXMLReader>(
                    new InjectionConstructor("whatever")))
                .IgnoreArguments()
                .Do(new RegisterTypeDelegate(DummyRegisterType));

            // Act
            IModule calcModule = new Module(mockContainer);
            calcModule.Initialize();
            
            // Assert
            Assert.IsTrue(_hasInjectionConstructor);
            mockContainer.VerifyAllExpectations();            
        }

        [TestMethod]
        [TestCategory("CalcEngine")]
        public void Then_the_IdealsCalcEngineService_is_registered()
        {
            // Arrange
            IUnityContainer mockContainer = MockRepository.GenerateMock<IUnityContainer>();
            mockContainer
                .Expect(c => c.RegisterType<IIdealsCalcEngineService, IdealsCalcEngineService>(new ContainerControlledLifetimeManager()))
                .Constraints(
                    Is.TypeOf<ContainerControlledLifetimeManager>(), // lifetime manager
                    Is.Anything());                                  // params

            // Act
            IModule calcModule = new Module(mockContainer);
            calcModule.Initialize();

            // Assert
            mockContainer.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("CalcEngine")]
        public void Then_the_MeasuredCalcEngineService_is_registered()
        {
            // Arrange
            var mockContainer = MockRepository.GenerateMock<IUnityContainer>();
            mockContainer
                .Expect(c => c.RegisterType<IMeasuredCalcEngineService, MeasuredCalcEngineService>());

            // Act
            IModule calcModule = new Module(mockContainer);
            calcModule.Initialize();

            // Assert
            mockContainer.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
