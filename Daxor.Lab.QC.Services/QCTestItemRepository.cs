﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Domain;
using Daxor.Lab.QC.Core;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Database.DataAccess;

namespace Daxor.Lab.QC.Services
{
    public class QCTestItemRepository : IRepository<QCTestItem>
    {
        public IQueryable<QCTestItem> SelectAll()
        {
            return QCTestItemsDataAccess.Select().Select((s) => DataConverter.ToTestItem(s));
        }

        public QCTestItem Select(Guid id)
        {
            throw new NotImplementedException();
        }

        public void Save(QCTestItem item)
        {
            throw new NotImplementedException();
        }

        public void Delete(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
