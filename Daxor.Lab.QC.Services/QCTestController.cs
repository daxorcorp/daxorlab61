﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Factories;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.QC.Services.TestExecutionContext;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.QC.Services
{
    /// <summary>
    /// QualityTest controller
    /// </summary>
    public class QcTestController : ObservableObject, IQualityTestController
    {
        #region Fields

        private readonly IUnityContainer _container;
        private readonly IEventAggregator _eventAggregator;
        private readonly IAppModuleNavigator _navigator;
        private readonly ISchemaProvider _sampleSchemaProvider;
        private readonly IQualityTestDataService _dataService;
        private readonly IMessageBoxDispatcher _msgBox;
        private readonly ILoggerFacade _logger;
        private readonly ITestExecutionController _exeController;
        private readonly IEnumerable<IConfigurator> _testConfigurators;
        private readonly ISettingsManager _settingsManager;
        private readonly SettingObserver<ISettingsManager> _settingObserver;
        private readonly DelegateCommand<Object> _startSelectedTestExecutionCommand;
        private readonly DelegateCommand<Object> _abortTestExecutionCommand;
        private readonly DelegateCommand<Guid> _selectTestCommand;
        private readonly DelegateCommand<QcTest> _saveTestCommand;
        private readonly DelegateCommand<TestType> _beginNewTestCommand;
        private readonly IMessageManager _messageManager;

        string _sTestFriendlyName;
        BackgroundTaskManager<QcTest> _testSelector;
        Int32 _testItemsCounts;

        QcTest _rTest;
        QcTest _sTest;

        #endregion

        #region Ctor

        public QcTestController([Dependency(AppModuleIds.QualityControl)] IAppModuleNavigator localNavigator, [Dependency(AppModuleIds.QualityControl)] IRuleEngine ruleEngine, 
                                     IEventAggregator eventAggregator, IUnityContainer container,
                                     ISchemaProvider sampleSchemaProvider, IQualityTestDataService qcDataService, IEnumerable<IConfigurator> configurators,
                                     ITestExecutionController exeController, IMessageBoxDispatcher msgBox, ILoggerFacade logger, 
                                     ISettingsManager settingsManager, IMessageManager messageManager)
        {
            _navigator = localNavigator;
            RuleEngine = ruleEngine;
            _eventAggregator = eventAggregator;
            _sampleSchemaProvider = sampleSchemaProvider;
            _dataService = qcDataService;
            _testConfigurators = configurators;
            _msgBox = msgBox;
            _exeController = exeController;
            _logger = logger;
            _container = container;
            _settingsManager = settingsManager;
            _messageManager = messageManager;
            _settingObserver = new SettingObserver<ISettingsManager>(_settingsManager);

            _startSelectedTestExecutionCommand = new DelegateCommand<Object>(StartSelectedTestExecutionCommandExecute, StartSelectedTestExecutionCommandCanExecute);
            _abortTestExecutionCommand = new DelegateCommand<Object>(AbortTestExecutionCommandExecute, AbortTestExecutionCommandCanExecute);
            _selectTestCommand = new DelegateCommand<Guid>(SelectTestCommandExecute, SelectTestCommandCanExecute);
            _saveTestCommand = new DelegateCommand<QcTest>(SaveTestCommandExecute, SaveTestCommandCanExecute);
            _beginNewTestCommand = new DelegateCommand<TestType>(BeginNewTestCommandExecute, BeginNewTestCommandCanExecute);

            WireSettingsObservers();
        }

        #endregion

        #region Properties

        public QcTest RunningTest
        {
            get { return _rTest; }
            set { base.SetValue(ref _rTest, "RunningTest", value); }
        }

        public QcTest SelectedTest
        {
            get { return _sTest; }
            set
            {
                if (base.SetValue(ref _sTest, "SelectedTest", value))
                {
                    SelectedTestFriendlyName = QcTestNameConverter.Convert(_sTest.QCType);
                }

                _startSelectedTestExecutionCommand.RaiseCanExecuteChanged();
                _abortTestExecutionCommand.RaiseCanExecuteChanged();
            }
        }

        public IQualityTestDataService DataService
        {
            get { return _dataService; }
        }

        public Int32 TestItemsCount
        {
            get { return _testItemsCounts; }
            private set { base.SetValue(ref _testItemsCounts, "TestItemsCount", value); }
        }
        
        #endregion

        #region Events

        public event EventHandler TestItemsNeedRefresh;

        #endregion

        #region Commands

        public ICommand SaveTestCommand
        {
            get { return _saveTestCommand; }
        }

        public ICommand SelectTestCommand
        {
            get { return _selectTestCommand; }
        }

        public ICommand StartSelectedTestExecutionCommand
        {
            get { return _startSelectedTestExecutionCommand; }
        }

        public ICommand BeginNewTestCommand
        {
            get { return _beginNewTestCommand; }
        }

        public ICommand AbortTestExecutionCommand
        {
            get { return _abortTestExecutionCommand; }
        }

        #endregion

        #region Methods

        public void Navigate(string key)
        {
            _navigator.ActivateView(key);
        }

        public String SelectedTestFriendlyName
        {
            get { return _sTestFriendlyName; }
            private set { base.SetValue(ref _sTestFriendlyName, "SelectedTestFriendlyName", value); }
        }

        #endregion

        #region Private properties

        private IRuleEngine RuleEngine { get; set; }

        private ITestExecutionContext RunningContext { get; set; }
        
        #endregion

        #region Test execution controller event handlers

        void OnErrorOccurred(object sender, TestExecutionEventArgs e)
        {
            UnWireExecutionControllerEvents();
            if (RunningTest != null && RunningTest.InternalId == e.ExecutingTestId)
            {
                RunningTest.Status = TestStatus.Aborted;
                SaveTestCommandExecute(RunningTest);
            }
            if (_navigator.IsActive)
            {
                ReEvaluateExecutionCommands();
                string message = _messageManager.GetMessage(MessageKeys.QcTestExecutionFailed).FormattedMessage;
                _msgBox.ShowMessageBox(MessageBoxCategory.Error, message, "Test Execution Error", new[] { "Return to QC Test List" });
                _logger.Log(message, Category.Info, Priority.Low);
            }

            RunningContext = null;
            FireTestItemsNeedRefresh();
        }

        void OnTestStarted(object sender, TestExecutionEventArgs e)
        {
            ReEvaluateExecutionCommands();
            if (RunningTest != null && RunningTest.InternalId == e.ExecutingTestId)
            {
                RunningTest.Status = TestStatus.Running;
                SaveTestCommandExecute(RunningTest);
            }
            FireTestItemsNeedRefresh();
        }

        void OnTestCompleted(object sender, TestExecutionEventArgs e)
        {
            UnWireExecutionControllerEvents();
            if (RunningTest != null && RunningTest.InternalId == e.ExecutingTestId)
            {
                RunningTest.Status = TestStatus.Completed;
                SaveTestCommandExecute(RunningTest);

                //Updating the calibration curve v.0.0.1 --> need to improve on that.
                if (RunningTest.IsPassed)
                {
                    if (RunningTest is QcFullTest || RunningTest is QcLinearityTest)
                        _settingsManager.SetSetting(SettingKeys.QcLastSuccessfulFullQcOrLinearityDate, DateTime.Now);

                    _settingsManager.SetSetting(SettingKeys.QcLastQcTestPassed, true);

                    if (RunningTest is QcCalibrationTest || RunningTest is QcFullTest || RunningTest is QcLinearityTest)
                    {
                        _dataService.SaveTest(RunningTest);
                        _container.RegisterInstance(new CalibrationFunction(RunningTest.CalibrationCurve.Slope, RunningTest.CalibrationCurve.Offset));

                        RunningTest.EndFineGain = RunningTest.CurrentFineGain;

                        _settingsManager.SetSetting(SettingKeys.SystemCurrentCalibrationCurveId, RunningTest.CalibrationCurve.CurveId);
                        _settingsManager.SetSetting(SettingKeys.SystemDetectorFineGain, RunningTest.CurrentFineGain);
                    }
                }
                else
                {
                    var runningQcFullTest = RunningTest as QcFullTest;
                    if (runningQcFullTest != null)
                    {
                        //  If a test fails but the calibration passes, we want to save the successful calibration curve
                        var calibrationSubTest = (from r in runningQcFullTest.SubTests
                                                  where r.QCType == TestType.Calibration
                                                  select r).FirstOrDefault<QcTest>();

                        if (calibrationSubTest != null && calibrationSubTest.IsPassed)
                        {
                            _dataService.SaveTest(RunningTest);
                            _container.RegisterInstance(new CalibrationFunction(RunningTest.CalibrationCurve.Slope, RunningTest.CalibrationCurve.Offset));

                            RunningTest.EndFineGain = RunningTest.CurrentFineGain;

                            _settingsManager.SetSetting(SettingKeys.SystemCurrentCalibrationCurveId, RunningTest.CalibrationCurve.CurveId);
                            _settingsManager.SetSetting(SettingKeys.SystemDetectorFineGain, RunningTest.CurrentFineGain);
                        }

                        var linearitySubTest = (from r in runningQcFullTest.SubTests
                                                where r.QCType == TestType.LinearityWithoutCalibration
                                                select r).FirstOrDefault<QcTest>();

                        if (linearitySubTest != null && linearitySubTest.IsPassed)
                        {
                            _settingsManager.SetSetting(SettingKeys.QcLastSuccessfulFullQcOrLinearityDate, DateTime.Now);
                        }

                    }

                    if (RunningTest.Status == TestStatus.Completed)
                        _settingsManager.SetSetting(SettingKeys.QcLastQcTestPassed, false);
                }
            }

            _eventAggregator.GetEvent<QCTestDueChanged>().Publish(null);
            ReEvaluateExecutionCommands();
            RunningContext = null;
            RunningTest = null;
            _beginNewTestCommand.RaiseCanExecuteChanged();

            FireTestItemsNeedRefresh();
            CommandManager.InvalidateRequerySuggested();
        }

        void OnTestAborted(object sender, TestExecutionEventArgs e)
        {
            UnWireExecutionControllerEvents();
            ReEvaluateExecutionCommands();
            if (RunningTest != null && RunningTest.InternalId == e.ExecutingTestId)
            {
                RunningTest.Status = TestStatus.Aborted;
                SaveTestCommandExecute(RunningTest);
            }

            RunningContext = null;
            RunningTest = null;
            _beginNewTestCommand.RaiseCanExecuteChanged();
            _selectTestCommand.RaiseCanExecuteChanged();

            FireTestItemsNeedRefresh();
        }

        void OnTestFinished(ITest test)
        {
            _eventAggregator.GetEvent<TestCompleted>().Unsubscribe(OnTestFinished);
            _eventAggregator.GetEvent<TestAborted>().Unsubscribe(OnTestFinished);

            if (!_exeController.DidDetectorBecomeDisconnected)
                _msgBox.ShowMessageBox(MessageBoxCategory.Information,
                        new SubViews.RelocateStandardsContent(), "Relocate Standards",
                        MessageBoxChoiceSet.Close);
        }

        #endregion

        #region Command delegates
        
        void StartSelectedTestExecutionCommandExecute(Object nullObject)
        {
            try
            {
                if (!IsTestSetupComplete()) return;

                SetExecutionContext();
                WireUpToExecutionControllerEvents();
                SaveTestCommandExecute(SelectedTest);
                RunningTest = SelectedTest;
                _beginNewTestCommand.RaiseCanExecuteChanged();
                _exeController.Start(RunningContext);
                _navigator.ActivateView(DestinationViewKeys.QcTestResults);
                SubscribeToAggregateEvents();
            }
            catch (Exception ex)
            {
                UnWireExecutionControllerEvents();
                RunningTest = null;
                RunningContext = null;
                _logger.Log(ex.Message, Category.Exception, Priority.High);

                var message = _messageManager.GetMessage(MessageKeys.QcTestStartFailed).FormattedMessage;
                _msgBox.ShowMessageBox(MessageBoxCategory.Error, message, "QC Test Cannot Be Started", new[] { "Return to QC Test List" });
                _logger.Log(message, Category.Info, Priority.Low);
            }
            finally
            {
                ReEvaluateExecutionCommands();
            }
        }

        bool StartSelectedTestExecutionCommandCanExecute(Object nullObject)
        {
            var canStart = false;

            if (SelectedTest != null)
            {
                var brokenRules = SelectedTest.RuleEngine.GetBrokenRulesOnModel(SelectedTest);
                canStart = brokenRules.Count(rule => rule.IsEnabled) == 0;
            }

            return SelectedTest != null && SelectedTest.Status == TestStatus.Pending
                   && !_exeController.IsExecuting && RunningContext == null && canStart;
        }

        void AbortTestExecutionCommandExecute(Object test)
        {
            try
            {
                var message = "Are you sure you want to abort this test?";
                var choices = new[] { "Abort Test", "Cancel" };
                var result = _msgBox.ShowMessageBox(MessageBoxCategory.Question, message, "Abort Test", choices);

                if (choices[result] == "Abort Test")
                {
                    Task.Factory.StartNew(objectState =>
                    {
                        var exeController = (ITestExecutionController)objectState;

                        _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Aborting QC test..."));
                        exeController.Abort();
                        Thread.Sleep(2000);

                        return _exeController.IsExecuting;

                    }, _exeController).ContinueWith(output =>
                    {
                        _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false, "Aborting QC test..."));
                        _beginNewTestCommand.RaiseCanExecuteChanged();
                        _selectTestCommand.RaiseCanExecuteChanged();

                        if (!output.Result) return;

                        // Tried to abort, but the test controller is still executing
                        message = _messageManager.GetMessage(MessageKeys.QcAbortTestTimedOut).FormattedMessage;
                        _msgBox.ShowMessageBox(MessageBoxCategory.Warning, message, "Aborting QC Test", MessageBoxChoiceSet.Close);
                        _logger.Log(message, Category.Info, Priority.Low);
                    }, TaskScheduler.FromCurrentSynchronizationContext());
                }
            }
            catch (Exception ex)
            {
                _logger.Log("Aborting QCTest. Exception: " + ex.Message, Category.Exception, Priority.High);
            }
            finally
            {
                _abortTestExecutionCommand.RaiseCanExecuteChanged();
                _startSelectedTestExecutionCommand.RaiseCanExecuteChanged();
            }
        }

        bool AbortTestExecutionCommandCanExecute(Object test)
        {
            bool canExecute;

            try
            {
                canExecute = SelectedTest != null && SelectedTest.Status == TestStatus.Running && _exeController.IsExecuting;
            }
            catch
            {
                canExecute = false;
            }
            return canExecute;
        }

		//Stacy: This command is used when creating a new test before running it.
		//			Change name to CreateNewTestCommand?

        void BeginNewTestCommandExecute(TestType qcType)
        {
            try
            {
                var test = QcTestFactory.CreateTest(qcType, _sampleSchemaProvider, RuleEngine);
                test.InjectateVerificationEnabled = _settingsManager.GetSetting<Boolean>(SettingKeys.SystemInjectateVerificationEnabled);
                test.SystemId = _settingsManager.GetSetting<String>(SettingKeys.SystemUniqueSystemId);
                test.CalibrationCurve = _dataService.GetCurrentCalibrationCurve();

                //Builds samples and assign sources
                test.BuildSamples();
                test.AssignSourcesToSamples(_dataService.GetSources());

                SelectedTest = test;
                test.StartFineGain = _settingsManager.GetSetting<double>(SettingKeys.SystemDetectorFineGain);
                test.Voltage = _settingsManager.GetSetting<int>(SettingKeys.SystemDetectorHighVoltage);

                //Navigate to test setup view
                Navigate(DestinationViewKeys.QcTestSetup);
            }
            catch (ArgumentException ex)
            {
                _logger.Log(ex.Message, Category.Exception, Priority.High);
            }
            catch (Exception ex)
            {
                _logger.Log("Exception: " + ex.Message + "Local stack trace: " + ex.StackTrace, Category.Exception, Priority.High);
            }
        }

        bool BeginNewTestCommandCanExecute(TestType qcType)
        {
            return RunningTest == null;
        }

        public void SaveTestCommandExecute(QcTest test)
        {
            try
            {
                test.EndFineGain = test.CurrentFineGain;
                test.Date = DateTime.Now;
                _dataService.SaveTest(test);
                TestItemsCount = _dataService.GetTestItems().Count();
            }
            catch
            {
                var message = _messageManager.GetMessage(MessageKeys.QcTestSaveTestFailed).FormattedMessage;
                _msgBox.ShowMessageBox(MessageBoxCategory.Error, message, "Error Saving A Test", new[] { "Proceed" });
                _logger.Log(message, Category.Info, Priority.Low);
            }
        }

        static bool SaveTestCommandCanExecute(QcTest test)
        {
            return test != null;
        }

        void SelectTestCommandExecute(Guid testId)
        {
            _testSelector = new BackgroundTaskManager<QcTest>(
                () =>
                {
                    QcTest sTest;
                    try
                    {
                        if (_exeController.IsExecuting && RunningContext != null && RunningTest != null && RunningTest.InternalId == testId)
                            sTest = RunningTest;
                        else
                            sTest = _dataService.LoadTest(testId);

                        sTest.RuleEngine = RuleEngine;

                        if (sTest.Status != TestStatus.Aborted)
                        {
                            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true) { Message = "Loading Quality Control Test..." });
                            Thread.Sleep(1000);
                        }

                    }
                    catch (Exception ex)
                    {
                        sTest = null;
                        _logger.Log(String.Format("Failed SelectQualityControlTest with ID'{0}'. Exception: {1}", testId, ex.Message), Category.Exception, Priority.High);
                    }

                    return sTest;

                }, sTest =>
                {
                    _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));

                    if (sTest == null)
                    {
                        var message = _messageManager.GetMessage(MessageKeys.QcLoadTestFailed).FormattedMessage;
                        _msgBox.ShowMessageBox(MessageBoxCategory.Error, message, "Error Retrieving Test", new[] { "Return to QC Test List" });
                        _logger.Log(message, Category.Info, Priority.Low);
                    }

                    else
                    {
                        //Dealing with aborted tests
                        var result = -1;
                        var choices = new[] { "Delete Test", "Open Test" };
                        if (sTest.Status == TestStatus.Aborted)
                        {
                            result = _msgBox.ShowMessageBox(MessageBoxCategory.Question,
                                "Would you like to delete this aborted test?", "Aborted QC Test Options", choices);
                        }

                        //Deal with the user input
                        if (result != -1 && choices[result] == "Delete Test")
                        {
                            _dataService.DeleteTest(sTest.InternalId);
                            FireTestItemsNeedRefresh();
                            return;
                        }

                        SelectedTest = sTest;
                        _eventAggregator.GetEvent<AppModuleModelChanged>().Publish(new ModelChangedPayload(AppModuleIds.QualityControl, SelectedTest));
                        Navigate(DestinationViewKeys.QcTestResults);
                    }
                });

            _testSelector.RunBackgroundTask();
        }

        static bool SelectTestCommandCanExecute(Guid testId)
        {
            return true;
        }

        
        #endregion

        #region Helpers

        protected virtual void FireTestItemsNeedRefresh()
        {
            var handler = TestItemsNeedRefresh;
            if (handler != null)
                handler(this, null);
        }

        protected virtual void SubscribeToAggregateEvents()
        {
            _eventAggregator.GetEvent<TestCompleted>().Subscribe(OnTestFinished, ThreadOption.UIThread, false, t => t is QcTest);
            _eventAggregator.GetEvent<TestAborted>().Subscribe(OnTestFinished, ThreadOption.UIThread, false, t => t is QcTest);
        }

        protected virtual void SetExecutionContext()
        {
            SelectedTest.ConfigureForExecution(_testConfigurators);

            var detector = _container.Resolve<IDetector>();
            RunningContext = new QualityControlExecutionContext(SelectedTest, detector, _logger, SaveTestCommand.Execute,
                _settingsManager);
        }

        protected virtual bool IsTestSetupComplete()
        {
            var choices = new[] { "Start Test", "Cancel" };
            var msgBoxResult = _msgBox.ShowMessageBox(MessageBoxCategory.Information,
                new SubViews.VerifyQCTestSetupContent(SelectedTest), "Verify Test Setup",
                choices);
            return msgBoxResult == 0;
        }

        /// <summary>
        /// Wires to settings manager and listening for setting updates, modifying currently visible test
        /// </summary>
        void WireSettingsObservers()
        {
            _settingObserver.RegisterHandler(SettingKeys.SystemInjectateVerificationEnabled, s =>
            {
                if (SelectedTest != null && SelectedTest.Status == TestStatus.Pending)
                {
                    SelectedTest.InjectateVerificationEnabled = s.GetSetting<Boolean>(SettingKeys.SystemInjectateVerificationEnabled);
                }
            });
        }

        void ReEvaluateExecutionCommands()
        {
            _abortTestExecutionCommand.RaiseCanExecuteChanged();
            _startSelectedTestExecutionCommand.RaiseCanExecuteChanged();
            _eventAggregator.GetEvent<QCTestReEvaluateExecutionCommands>().Publish(null);
        }

        void UnWireExecutionControllerEvents()
        {
            if (_exeController == null)
                return;

            _exeController.TestAborted -= OnTestAborted;
            _exeController.TestCompleted -= OnTestCompleted;
            _exeController.TestStarted -= OnTestStarted;
            _exeController.ErrorOccurred -= OnErrorOccurred;
        }

        protected virtual void WireUpToExecutionControllerEvents()
        {
            if (_exeController == null)
                return;

            _exeController.TestAborted += OnTestAborted;
            _exeController.TestCompleted += OnTestCompleted;
            _exeController.TestStarted += OnTestStarted;
            _exeController.ErrorOccurred += OnErrorOccurred;
        }

        #endregion
    }
}
