﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Factories;

namespace Daxor.Lab.QC.Services
{
	//Converts DTO to Models, Enum, etc.
	internal static class QcDataMapper
	{
		#region Test mappings

		public static QCTestRecord ToTestRecord(QcTest testModel)
		{
			if (testModel == null)
				return null;

			var qcRecord = new QCTestRecord
			{
				CalibrationCurveId = testModel.CalibrationCurve.CurveId,
				Analyst = testModel.Analyst ?? String.Empty,
				TestType = Convert.ToInt32(testModel.QCType),
				QCTestType = testModel.QCType.ToString(),
				SampleLayoutId = testModel.SampleLayoutId,
				UniqueSystemId = testModel.SystemId ?? String.Empty,
				TestId = testModel.InternalId,
				TestDate = testModel.Date,
				InjectateLot = testModel.InjectateLot ?? String.Empty,
				BackgroundCount = testModel.BackgroundCount,
				BackgroundTime = testModel.BackgroundTimeInSeconds,
				Comment = testModel.Comments ?? String.Empty,
				StatusId = Convert.ToInt32(testModel.Status),
				StandardABarcode = null,
				StandardBBarcode = null,
				InitialFineGain = testModel.StartFineGain,
				EndFineGain = testModel.EndFineGain,
				HighVoltage = testModel.Voltage,
				Pass = testModel.IsPassed,
					
				Samples = new List<QCSampleRecord>(),
			};

			//Deal with barcodes
			if (!String.IsNullOrEmpty(testModel.StandardBKey) || !String.IsNullOrEmpty(testModel.StandardAKey))
			{
				qcRecord.StandardABarcode = testModel.StandardAKey ?? String.Empty;
				qcRecord.StandardBBarcode = testModel.StandardBKey ?? String.Empty;
			}

			//Add samples for DTO
			foreach (var qcSampleRecord in testModel.Samples.Select(ToSampleRecord))
			{
				qcSampleRecord.TestId = testModel.InternalId;
				qcSampleRecord.PositionRecord.LayoutID = testModel.SampleLayoutId;
				qcSampleRecord.SampleLayoutId = testModel.SampleLayoutId;
				qcRecord.Samples.Add(qcSampleRecord);
			}

			return qcRecord;
		}

		#endregion

		#region Sample mappings

		public static QCSampleRecord ToSampleRecord(QcSample sampleModel)
		{
			var sampleRecord = new QCSampleRecord
			{
				SampleId = sampleModel.InternalId,
				CountLimit = sampleModel.PresetCountsLimit,
				Centroid = Double.IsInfinity(sampleModel.Centroid) ? -1 : sampleModel.Centroid, 
				Counts = sampleModel.CountsInRegionOfInterest,
				LiveTime = sampleModel.Spectrum.LiveTimeInSeconds,
				FWHM = Double.IsInfinity(sampleModel.FullWidthHalfMax) ? -1 : sampleModel.FullWidthHalfMax, 
				FWTM = Double.IsInfinity(sampleModel.FullWidthTenthMax) ? -1 : sampleModel.FullWidthTenthMax, 
				Pass = sampleModel.IsPassed,
				PresetLiveTime = sampleModel.PresetLiveTimeInSeconds,
				RealTime = sampleModel.Spectrum.RealTimeInSeconds,
				Spectrum = sampleModel.Spectrum.SpectrumArray,
				SerialNumber = String.IsNullOrEmpty(sampleModel.SerialNumber)? null : sampleModel.SerialNumber,
				PositionRecord = new PositionRecord { Position = sampleModel.Position },
				RoiLowBound = sampleModel.StartChannel,
				RoiHighBound = sampleModel.EndChannel,
				ErrorDetails = sampleModel.ErrorDetails 
			};

			//Determine if a sample contains QCSource
			if (sampleModel.IsSource)
			{
				sampleRecord.Isotope = sampleModel.Source.Isotope.Name.ToString();
				sampleRecord.SourceTOC = sampleModel.Source.TimeOfCalibration;
				sampleRecord.Activity = sampleModel.Source.Activity;
				sampleRecord.Accuracy = sampleModel.Source.Accuracy;
				sampleRecord.HalfLife = sampleModel.Source.Isotope.HalfLife;
				sampleRecord.SerialNumber = sampleModel.Source.SerialNumber;
			}
			else
			{
				sampleRecord.Isotope = null;
				sampleRecord.Activity = 0;
				sampleRecord.Accuracy = 0;
				sampleRecord.HalfLife = 0;
				sampleRecord.SerialNumber = null;
			}

			return sampleRecord;
		}
		public static QcSample ToSampleModel(QCSampleRecord sampleRecord)
		{
			if (sampleRecord == null)
				throw new ArgumentNullException("sampleRecord");

			IsotopeType iType;
			if (!Enum.TryParse(sampleRecord.Isotope, out iType))
			{
				iType = IsotopeType.Empty;
			}
			 
			//we have a sample that is also a source
			QcSource src = null;
			if (!String.IsNullOrWhiteSpace(sampleRecord.Isotope))
			{
				src = new QcSource(IsotopeFactory.CreateIsotope(iType), sampleRecord.SourceTOC, 
								   sampleRecord.PositionRecord.Position, sampleRecord.Activity, 
								   sampleRecord.Accuracy, sampleRecord.SerialNumber, sampleRecord.CountLimit);
			}

			//Resulting sample

			//Sample is not a source
			var model = src == null ? new QcSample(IsotopeFactory.CreateIsotope(iType)) : new QcSample(src);

			model.Position = sampleRecord.PositionRecord.Position;
			model.Centroid = sampleRecord.Centroid;
			model.CountsInRegionOfInterest = sampleRecord.Counts;
			
			//Need to change
			model.CountsInWholeSpectrum = sampleRecord.Counts;
			model.StartChannel = sampleRecord.RoiLowBound;
			model.EndChannel = sampleRecord.RoiHighBound;
			model.FullWidthHalfMax = sampleRecord.FWHM;
			model.FullWidthTenthMax = sampleRecord.FWTM;
			model.InternalId = sampleRecord.SampleId;
			model.IsPassed = sampleRecord.Pass;
			model.Spectrum.LiveTimeInSeconds = sampleRecord.LiveTime;
			model.IsEvaluationCompleted = true;

			model.Spectrum.SpectrumArray = sampleRecord.Spectrum;
			model.SerialNumber = sampleRecord.SerialNumber;
			model.ErrorDetails = sampleRecord.ErrorDetails;

			return model;
		}

		#endregion

		#region Helpers mappings

		public static SampleType ToModelSampleType(PositionRecord pRecord)
		{
			var sType = SampleType.Contamination;

			switch (pRecord.SampleFunction)
			{
				case "Background":
					sType = SampleType.Background;
					break;
				case "QC":
					Enum.TryParse(String.Concat(pRecord.SampleFunction, pRecord.SampleNumber), out sType);
					break;
				case "QCStandard":
					switch (pRecord.Range)
					{
						case "A":
							sType = SampleType.StandardA;
							break;
						case "B":
							sType = SampleType.StandardB;
							break;
					}
					break;
				default:
					sType = SampleType.Contamination;
					break;
			}

			return sType;
		}

		#endregion

		#region Sources mappings

		public static QcSource ToSourceModel(QCSourceRecord qcSourceRecord)
		{
			if (qcSourceRecord == null)
				return null;

			IsotopeType isotopeTypeInSource;
			if(!Enum.TryParse(qcSourceRecord.Isotope, out isotopeTypeInSource)){
				isotopeTypeInSource = IsotopeType.Empty;
			}

			var source = new QcSource(IsotopeFactory.CreateIsotope(isotopeTypeInSource), qcSourceRecord.SourceTOC, qcSourceRecord.Position, qcSourceRecord.Activity, qcSourceRecord.Accuracy, qcSourceRecord.SerialNumber, qcSourceRecord.CountLimit);
			return source;
		}
		public static QCSourceRecord ToSourceRecord(QcSource qcSourceModel)
		{
			if (qcSourceModel == null)
				return null;

			//Description and sourceID is not neccessary, should be removed from the database
			var sourceRecord = new QCSourceRecord
			{
				Accuracy =  qcSourceModel.Accuracy,
				Activity = qcSourceModel.Activity,
				CountLimit = qcSourceModel.CountLimit,
				HalfLife = qcSourceModel.Isotope.HalfLife,
				Isotope = qcSourceModel.Isotope.Name.ToString(),
				SerialNumber = qcSourceModel.SerialNumber,
				Position = qcSourceModel.SamplePosition,
				SourceTOC = qcSourceModel.TimeOfCalibration,
			};

			return sourceRecord;
		}

		#endregion

		#region Barcode mappings

		public static IEnumerable<BarCodeRecord> ToBarcodeRecords(QcTest testModel)
		{
			var barcodes = new List<BarCodeRecord>();
			if (testModel is QcFullTest || testModel is QcStandardsTest)
			{
				var hasAtLeastOneKey = !String.IsNullOrEmpty(testModel.StandardAKey) || !String.IsNullOrEmpty(testModel.StandardBKey);

				if (!hasAtLeastOneKey) return barcodes;

				var standards = from s in testModel.Samples
					where s.TypeOfSample == SampleType.StandardA || s.TypeOfSample == SampleType.StandardB
					select s;
				standards.ToList().ForEach(q => barcodes.Add(new BarCodeRecord
				{
					PositionId = q.Position,
					TestId = testModel.InternalId,
					BID = (q.TypeOfSample == SampleType.StandardA) ? testModel.StandardAKey : testModel.StandardBKey
				}));
			}
			else
			{
				barcodes.Clear();
			}

			return barcodes;
		}

		#endregion
	}
}
