﻿using System;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.QC.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.QC.Services
{
	public class CalibrationCurveDataService : ICalibrationCurveDataService
	{
		private readonly ICalibrationCurveDataAccess _calibrationCurveDataAccess;
		private readonly IEventAggregator _eventAggregator;

		private ICalibrationCurve _curve;

		public CalibrationCurveDataService(ICalibrationCurveDataAccess calibrationCurveDataAccess, IEventAggregator eventAggregator, ILoggerFacade logger)
		{
			if (calibrationCurveDataAccess == null)
				throw new ArgumentNullException("calibrationCurveDataAccess");
			if (eventAggregator == null)
				throw new ArgumentNullException("eventAggregator");
			if (logger == null)
				throw new ArgumentNullException("logger");

			_calibrationCurveDataAccess = calibrationCurveDataAccess;
			_curve = GetCurrentCalibrationCurve(logger);
			_eventAggregator = eventAggregator;

			_eventAggregator.GetEvent<GetCalibrationCurveRequest>().Subscribe(e =>
			{
				_eventAggregator.GetEvent<CalibrationCurveCalculated>().Publish(_curve);
			});
		}

		public ICalibrationCurve GetCurrentCalibrationCurve(ILoggerFacade logger)
		{
			var calRecord = _calibrationCurveDataAccess.SelectCurrent(logger);
			_curve = new CalibrationCurve(calRecord.CalibrationCurveId, calRecord.Slope, calRecord.OffsetValue);
			return _curve;
		}

		public ICalibrationCurve GetSpecificCalibrationCurve(int calibrationCurveId, ILoggerFacade logger)
		{
			var calRecord = _calibrationCurveDataAccess.Select(calibrationCurveId, logger);
			_curve = new CalibrationCurve(calRecord.CalibrationCurveId, calRecord.Slope, calRecord.OffsetValue);
			return _curve;
		}

		public int SaveCalibrationCurve(ICalibrationCurve curve, ILoggerFacade logger)
		{
			var curveRecord = _calibrationCurveDataAccess.Select(curve.CurveId, logger);
			if (curveRecord == null)
			{
				var newCurveRecord = _calibrationCurveDataAccess.Insert(new CalibrationCurveRecord {Slope = curve.Slope, OffsetValue = curve.Offset}, logger);

				if (newCurveRecord == null)
					return curve.CurveId;
				//else
				_curve = new CalibrationCurve(newCurveRecord.CalibrationCurveId, newCurveRecord.Slope, newCurveRecord.OffsetValue);
				return _curve.CurveId;
			}
			// ReSharper disable once RedundantIfElseBlock
			else
				return curveRecord.CalibrationCurveId;
		}
	}
}
