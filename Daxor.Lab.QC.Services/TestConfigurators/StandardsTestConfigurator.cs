using System;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.QC.Services.TestConfigurators
{
    /// <summary>
    /// Configurator for Standards Test
    /// </summary>
    public class StandardsTestConfigurator : TestConfigurator<QcStandardsTest>
    {
        #region Fields
        private readonly IQualityTestDataService _qualityTestDataService;
        private readonly ISettingsManager _settingsManager;

        #endregion

        #region Ctor
        public StandardsTestConfigurator(IQualityTestDataService qualityTestDataService, ISettingsManager settingsManager)
        {
            _qualityTestDataService = qualityTestDataService;
            _settingsManager = settingsManager;
        }

        #endregion
        protected override void ConfigureTest(QcStandardsTest test)
        {
            test.MinimumAllowableCountPerMinute = _settingsManager.GetSetting<Int32>(SettingKeys.QcStandardsMinCpm);
            test.StandardsDeviationMultiplier   = _settingsManager.GetSetting<Double>(SettingKeys.QcStandardsStdDevDifferenceMultiplier);
            test.SampleDurationInSeconds        = _settingsManager.GetSetting<Int32>(SettingKeys.QcStandardsCountTimeInSeconds);
            test.CalibrationCurve               = _qualityTestDataService.GetCurrentCalibrationCurve();
            test.SpectroscopyService                   = new Domain.Services.SpectroscopyService();
        }
    }
}