using System;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.QC.Services.TestConfigurators
{
    /// <summary>
    /// Configurator for Calibration Test
    /// </summary>
    public class CalibrationTestConfigurator : TestConfigurator<QcCalibrationTest>
    {
        private readonly IDetector _detector;
        private readonly IPeakFinder _peakFinder;
        private readonly IQualityTestDataService _qualityTestDataService;
        private readonly ISettingsManager _settingsManager;
        private readonly IFineGainAdjustmentService _fineGainAdjustmentService;
        private readonly IIsotopeMatchingService _isotopeMatchingService;
        private readonly ISpectroscopyService _spectroscopyService;
        private readonly IQcCalibrationTestPayloadProcessor _payloadProcessor;

        public CalibrationTestConfigurator(IDetector detector, IPeakFinder peakFinder, 
                                           IQualityTestDataService qualityTestDataService, ISettingsManager settingsManager, 
                                           IFineGainAdjustmentService fineGainAdjustmentService, IIsotopeMatchingService isotopeMatchingService,
                                           ISpectroscopyService spectroscopyService, IQcCalibrationTestPayloadProcessor payloadProcessor)
        {
            _detector = detector;
            _peakFinder = peakFinder;
            _qualityTestDataService = qualityTestDataService;
            _settingsManager = settingsManager;
            _fineGainAdjustmentService = fineGainAdjustmentService;
            _isotopeMatchingService = isotopeMatchingService;
            _spectroscopyService = spectroscopyService;
            _payloadProcessor = payloadProcessor;
        }
       
        protected override void ConfigureTest(QcCalibrationTest test)
        {
            test.MaxAllowableFineGainAdjustments = _settingsManager.GetSetting<Int32>(SettingKeys.QcMaxNumberOfFineGainAdjustments);
            test.SetupCountTimeSecs = _settingsManager.GetSetting<Int32>(SettingKeys.QcSetupSampleCountTimeInSeconds);
            test.SampleDurationInSeconds = _settingsManager.GetSetting<Int32>(SettingKeys.QcSetupSampleCountTimeInSeconds);
            test.SetupPrecisionInkeV = _settingsManager.GetSetting<double>(SettingKeys.QcSetupCentroidPrecisionInkeV);

            test.MinimumPassingSetupFullWidthHalfMax = _settingsManager.GetSetting<double>(SettingKeys.QcCalibrationTestMinPassingFwhm);
            test.MaximumPassingSetupFullWidthHalfMax = _settingsManager.GetSetting<double>(SettingKeys.QcCalibrationTestMaxPassingFwhm);
            test.SetupSourceCountThreshold = _settingsManager.GetSetting<int>(SettingKeys.QcCalibrationTestMinCounts);
            test.SetupSourceCountThresholdAcquisitionTimeInSeconds = _settingsManager.GetSetting<int>(SettingKeys.QcSetupSampleThresholdCountTimeInSeconds);

            test.MaxFineGainAllowed = _settingsManager.GetSetting<Double>(SettingKeys.QcCalibrationTestMaxFineGain);
            test.MinFineGainAllowed = _settingsManager.GetSetting<Double>(SettingKeys.QcCalibrationTestMinFineGain);

            test.StartFineGain = _detector.FineGain;
            test.CurrentFineGain = test.StartFineGain;
            test.SetupPeakEnergy = _settingsManager.GetSetting<double>(SettingKeys.QcCs137Centroid);
            test.CalibrationPeakEnergy = _settingsManager.GetSetting<double>(SettingKeys.QcBa133Centroid);
            test.PeakFinder = _peakFinder;
            test.CalibrationCurve = _qualityTestDataService.GetCurrentCalibrationCurve(); //Should get from settings
            test.SpectroscopyService = _spectroscopyService;
            test.FineGainAdjustmentService = _fineGainAdjustmentService;
            test.FineGainAdjustmentService.InitialFineGainAdjustment = _settingsManager.GetSetting<double>(SettingKeys.QcInitialFineGainAdjustment);
            test.IsotopeMatchingService = _isotopeMatchingService;
            test.PayloadProcessor = _payloadProcessor;
        }
    }
}