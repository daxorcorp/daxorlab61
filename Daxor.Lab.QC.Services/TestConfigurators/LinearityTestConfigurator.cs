﻿using System;
using Daxor.Lab.QC.Core;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.QC.Services.TestConfigurators
{
    /// <summary>
    /// Configurator for Linearity Test
    /// </summary>
    public class LinearityTestConfigurator : TestConfigurator<QcLinearityWithoutCalibrationTest>
    {
        #region
        private readonly IQualityTestDataService _qualityTestDataService;
        private readonly ISettingsManager _settingsManager;

        #endregion

        #region Ctor
        public LinearityTestConfigurator(IQualityTestDataService qualityTestDataService, ISettingsManager settingsManager)
        {
            _qualityTestDataService = qualityTestDataService;
            _settingsManager = settingsManager;
        }

        #endregion

        protected override void ConfigureTest(QcLinearityWithoutCalibrationTest test)
        {
            test.SampleDurationInSeconds = _settingsManager.GetSetting<Int32>(SettingKeys.QcLinearityCountTimeInSeconds);
            test.MinimumPassingFullWidthHalfMax = _settingsManager.GetSetting<double>(SettingKeys.QcLinearityMinPassingFwhm);
            test.MaximumPassingFullWidthHalfMax = _settingsManager.GetSetting<double>(SettingKeys.QcLinearityMaxPassingFwhm);

            test.ExpectedCentroid = _settingsManager.GetSetting<double>(SettingKeys.QcBa133Centroid);
            test.MaximumCentroidDifferenceInChannels = _settingsManager.GetSetting<double>(SettingKeys.QcResolutionCentroidTolerance);

            test.EstimatedQC2ExecutionTimeInSeconds = _settingsManager.GetSetting<int>(SettingKeys.QcEstimatedExecutionTimeForQc2);
            test.EstimatedQC3ExecutionTimeInSeconds = _settingsManager.GetSetting<int>(SettingKeys.QcEstimatedExecutionTimeForQc3);
            test.EstimatedQC4ExecutionTimeInSeconds = _settingsManager.GetSetting<int>(SettingKeys.QcEstimatedExecutionTimeForQc4);

            test.MinimumCounts = _settingsManager.GetSetting<int>(SettingKeys.QcLinearityMinCounts);
            test.CalibrationCurve = _qualityTestDataService.GetCurrentCalibrationCurve();
            test.SpectroscopyService = new Domain.Services.SpectroscopyService();
        }
    }

}
