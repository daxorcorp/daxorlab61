using System;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.QC.Services.TestConfigurators
{
    /// <summary>
    /// Configurator for Contamination Test
    /// </summary>
    public class ContaminationTestConfigurator : TestConfigurator<QcContaminationTest>
    {
        #region Fields
        private readonly IQualityTestDataService _qualityTestDataService;
        private readonly ISettingsManager _settingsManager;

        #endregion


        #region Ctor
        public ContaminationTestConfigurator(IQualityTestDataService qualityTestDataService, ISettingsManager settingsManager)
        {
            _qualityTestDataService = qualityTestDataService;
            _settingsManager = settingsManager;
        }

        #endregion
        protected override void ConfigureTest(QcContaminationTest test)
        {
            test.ContaminationMultiplier = _settingsManager.GetSetting<Double>(SettingKeys.QcContaminationBackgroundMultiplier);
            test.DefaultContaminationCpm = _settingsManager.GetSetting<Double>(SettingKeys.QcContaminationMaxCpm);
            test.SampleDurationInSeconds = _settingsManager.GetSetting<Int32>(SettingKeys.QcContaminationCountTimeInSeconds);
            test.CalibrationCurve       = _qualityTestDataService.GetCurrentCalibrationCurve(); //Should get from settings
            test.SpectroscopyService           = new Domain.Services.SpectroscopyService();
        }
    }
}