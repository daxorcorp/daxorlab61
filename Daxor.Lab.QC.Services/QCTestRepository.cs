﻿using System;
using System.Linq;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Factories;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Interfaces;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.QC.Services
{
	/// <summary>
	/// Existing implementation with LINQ-SQL --> move to NHibernate
	/// 
	/// ASK
	/// </summary>
	public class QcTestRepository : IRepository<QcTest>
	{
		private readonly ICalibrationCurveDataService _calibrationCurveDataService;
		private readonly ISchemaProvider _schemaProvider;
		private readonly ILoggerFacade _logger;

		public QcTestRepository(ISchemaProvider schemaProvider, ILoggerFacade logger, ICalibrationCurveDataService calibrationCurveDataService)
		{
			_schemaProvider = schemaProvider;
			_logger = logger;
			_calibrationCurveDataService = calibrationCurveDataService;
		}

		public IQueryable<QcTest> SelectAll()
		{
			throw new NotSupportedException();
		}

		public QcTest Select(Guid testId)
		{
			//Invalid testID
			if(testId == null || testId == Guid.Empty)
				throw new ArgumentOutOfRangeException("testId", @"cannot be null or empty");

			var record = QCTestDataAccess.Select(testId);
			if (record == null)
				throw new Exception(String.Format("QCTest with ID '{0}' does not exist.", testId));
			   
			TestType typeToCreate;
			if (!Enum.TryParse(record.QCTestType, out typeToCreate))
				throw new Exception("Unknown test type.");
				
			var model = QcTestFactory.CreateTest(typeToCreate, _schemaProvider, record.UniqueSystemId, record.TestId, null);
			model.BuildSamples();
			   
			model.Status = (TestStatus)record.StatusId;

			//Load calibration curve based on ID
			model.CalibrationCurve = _calibrationCurveDataService.GetSpecificCalibrationCurve(record.CalibrationCurveId, _logger);

			var samplesRecords = QCSampleDataAccess.SelectList(record.TestId).ToList();

			var query = from s in model.Samples
						join r in samplesRecords on s.Position equals r.PositionRecord.Position into outer
						from r in outer.DefaultIfEmpty()
						select new { SampleModel = s, SampleRecord = r };

			foreach (var pair in query)
			{
				pair.SampleModel.InternalId = pair.SampleRecord.SampleId;
				model.ProcessSample(QcDataMapper.ToSampleModel(pair.SampleRecord));
			}

			model.Analyst = record.Analyst;
			model.StandardAKey = record.StandardABarcode;
			model.StandardBKey = record.StandardBBarcode;
			model.InjectateLot = record.InjectateLot;
			model.SampleLayoutId = record.SampleLayoutId;
			model.BackgroundCount = record.BackgroundCount;
			model.BackgroundTimeInSeconds = record.BackgroundTime;
			model.Voltage = record.HighVoltage;
			model.StartFineGain = record.InitialFineGain;
			model.EndFineGain = record.EndFineGain;
			model.Comments = record.Comment;
			model.Date = record.TestDate;

			return model;
		}

		public void Save(QcTest item)
		{
		    if (item == null)
		        throw new ArgumentNullException("item");

			//Insert curve id
			if (item.CalibrationCurve != null)
				item.CalibrationCurve.CurveId = _calibrationCurveDataService.SaveCalibrationCurve(item.CalibrationCurve, _logger);

			//Decide whether to update or initial commit
			var test = QCTestDataAccess.Select(item.InternalId);
			if (test == null)
			{
				var qcDataRecord = QcDataMapper.ToTestRecord(item);

				//Inserting test record
				QCTestDataAccess.Insert(qcDataRecord, null, _logger);

				//Inserting sample records
				qcDataRecord.Samples.ForEach(record => QCSampleDataAccess.Insert(record, null, _logger));
				  
			}
			else
			{
				//Update
				var qcDataRecord = QcDataMapper.ToTestRecord(item);
				QCTestDataAccess.Update(qcDataRecord, null, _logger);
				qcDataRecord.Samples.ForEach(record => QCSampleDataAccess.Update(record, null, _logger));
			}
		}

		public void Delete(Guid id)
		{
			 QCTestDataAccess.Delete(id);
		}
	}
}
