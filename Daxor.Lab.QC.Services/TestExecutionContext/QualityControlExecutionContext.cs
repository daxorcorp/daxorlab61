﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Domain.Services;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Logging;

[assembly: InternalsVisibleTo("Daxor.Lab.QC.Services.Tests")]
namespace Daxor.Lab.QC.Services.TestExecutionContext
{
    /// <summary>
    /// Full quality control test execution context
    /// </summary>
    internal class QualityControlExecutionContext : TestExecutionContext<QcTest>
    {
        #region Fields
        
        private readonly Action<QcTest> _saveTestAction;
        private readonly TestExecutionContextMetadata _contextMetadata;
        private readonly ILoggerFacade _logger;
        private int _numberOfFailedButRepeats;
        private readonly ISettingsManager _settingsManager;
        private readonly double _initialFineGain;

        #endregion

        #region Ctor
        public QualityControlExecutionContext(QcTest test, IDetector detector, ILoggerFacade logger, Action<QcTest> saveTestAction, ISettingsManager settingsManager)
            : base(test, detector)
        {
            if (saveTestAction == null)
                throw new ArgumentNullException("saveTestAction");

            if (detector == null)
                throw new ArgumentNullException("detector");

            if (logger == null)
                throw new ArgumentNullException("logger");

            if (test == null)
                throw new ArgumentNullException("test");

            _saveTestAction = saveTestAction;

            _logger = logger;
            _settingsManager = settingsManager;

            _initialFineGain = detector.FineGain;
         
            //Provide background if available
            BackgroundSample = (from s in ExecutingTestOfSpecificType.Samples where s.TypeOfSample == SampleType.Background select s).FirstOrDefault();
            RunningSampleIndex = BackgroundSample != null ? 1 : 0;

            _contextMetadata = new TestExecutionContextMetadata(Infrastructure.Constants.AppModuleIds.QualityControl)
                                   {
                                       DisplayTestName = "Quality Control Test",
                                       FriendlyExecutingSampleName = BackgroundSample != null ? BackgroundSample.DisplayName : string.Empty
                                   };
            Metadata = _contextMetadata;
        }

        #endregion

        #region Properties

        protected int RunningSampleIndex { get; set; }

        #endregion

        #region ITestExectuionContext implementation

        /// <summary>
        /// Gets the collection of samples (excluding the background sample), 
        /// ordered by how they should be counted during the test
        /// </summary>
        public override IEnumerable<ISample> OrderedSamplesExcludingBackground
        {
            get
            {
                return (from s in ExecutingTestOfSpecificType.Samples where s.TypeOfSample != SampleType.Background select s).ToList();
            }
        }

        public override ISample GetNextSampleToExecute()
        {
            //Implies we never had any invokers
            QcSample nextSampleToExecute = null;

            if (ExecutingSample == null)
                nextSampleToExecute = ExecutingTestOfSpecificType.Samples.ElementAt(RunningSampleIndex);
            else if(RunningSampleIndex >= 0)
            {
                var qcSample = ExecutingSample as QcSample;

                if (qcSample == null)
                    throw new NotSupportedException("ExecutingSample (as a QCSample) was null");
                
                if (qcSample.IsEvaluationCompleted)
                    RunningSampleIndex++;

                nextSampleToExecute = RunningSampleIndex >= ExecutingTestOfSpecificType.Samples.Count() ? null : ExecutingTestOfSpecificType.Samples.ElementAt(RunningSampleIndex);
                
            }

            //Figure out sample acquisition time; if two or more tests share the sample, acquisition time will be the largest
            if (nextSampleToExecute != null)
            {
                double presetLiveTime = ExecutingTestOfSpecificType.GetDesiredExecutionTimeInSeconds(nextSampleToExecute);
                // ReSharper disable CompareOfFloatsByEqualityOperator
                if (presetLiveTime == Double.MinValue)
                    throw new NotSupportedException("Someone failed to implement GetDesiredExecutionTimeInSeconds accurately");
                // ReSharper restore CompareOfFloatsByEqualityOperator

                nextSampleToExecute.PresetLiveTimeInSeconds = (int)presetLiveTime;
            }

            _contextMetadata.FriendlyExecutingSampleName = nextSampleToExecute != null ? nextSampleToExecute.DisplayName : String.Empty;
            return nextSampleToExecute;
        }

        public override int ComputeSampleAcquisitionTimeInMilliseconds(ISample sample)
        {
            var qcSample = sample as QcSample;
            if (qcSample == null)
                throw new NotSupportedException("Given sample is not a QC sample");

            return ExecutingTestOfSpecificType.GetEstimatedExecutionTimeInSeconds(qcSample) *
                   (int) TimeConstants.MillisecondsPerSecond;
        }

        public override void ProcessBackground(int integral, int acqusitionTimeInSeconds, bool isAcquisitionCompleted, Spectrum spectrum = null, bool isOverridden = false)
        {
            if (isOverridden)
                throw new NotSupportedException("Background override is not supported in QC Test");

            ProcessSpectrum(BackgroundSample, spectrum, isAcquisitionCompleted);
            ExecutingTestOfSpecificType.BackgroundCount = integral;
            ExecutingTestOfSpecificType.BackgroundTimeInSeconds = acqusitionTimeInSeconds;
           
            if (isAcquisitionCompleted)
                ExecutingTestOfSpecificType.EvaluateSample((QcSample)BackgroundSample);
        }

        public override void PerformAcquisition(ISample sample, Action<ISample> acquisitionCompletedCallback)
        {
            if (!HasAcquisitionStarted)
            {
                if (sample == null)
                    throw new ArgumentNullException("sample");

                if (acquisitionCompletedCallback == null)
                    throw new ArgumentNullException("acquisitionCompletedCallback");

                const int minimumLiveTimeInSeconds = 0;
                
                if (sample.PresetLiveTimeInSeconds <= minimumLiveTimeInSeconds)
                    throw new ArgumentOutOfRangeException("sample", string.Format("Preset live time must be >= {0}", minimumLiveTimeInSeconds));
                
                AcquisitionCompletedCallback = acquisitionCompletedCallback;
                try
                {
                    Detector.StopAcquiring();
                    Detector.ResetSpectrum();
                    Detector.StartAcquiring(sample.PresetLiveTimeInSeconds);
                }
                catch (Exception ex)
                {
                    _logger.Log(ex.Message, Category.Exception, Priority.High);
                    throw;
                }
                ExecutingSample = sample;

                HasAcquisitionStarted = true;

                _logger.Log("QualityControlExecutionContext.PerformAcquisition() -- acquisition had not yet started. Acquisition " +
                    "has been initiated on sample " + ExecutingSample.DisplayName + " for " + sample.PresetLiveTimeInSeconds + " seconds.",
                    Category.Info, Priority.High);
            }
            else
            {
                var qcSample = sample as QcSample;
                if (qcSample == null)
                    throw new NotSupportedException("Sample being acquired (as a QCSample) was null");

                // Something went wrong with acquisition (maybe aborted), so call back
                // saying we weren't successful.
                //------
                // Note from Geoff: Andrey and I put this in as the logic seems sound.
                // However, I think there's a race condition where IsAcquiring is false
                // (because the detector is truly done), but HasAcquisitionCompleted
                // is false. Logging more information shows that AcquisitionCompleted
                // is getting fired, and the implementation of both of these properties
                // relies on hardware calls.
                //------
                //if (!Detector.IsAcquiring && !Detector.HasAcquisitionCompleted)
                //{
                //    _logger.Log("QualityControlExecutionContext.PerformAcquisition() -- acquisition was in progress but something isn't right. " +
                //        "The detector is no longer acquiring but the reports that acquisition hasn't completed. Here's what is known about the detector: " + Detector.ToString(), Category.Info, Priority.High);

                //    _acquisitionCompletedCallback(qcSample, successfulAcquisiton);
                //    HasAcquisitionStarted = false;
                //    return;
                //}

                // No problems, process as usual.
                ProcessSpectrum(sample, Detector.Spectrum);

                qcSample.IsEvaluationCompleted = false;

                // Done acquiring, so do final processing and call back saying we're done.
                bool presetCountLimitHasBeenReached = (qcSample.PresetCountsLimit > 0 && qcSample.PresetCountLimitReached);

                try
                {
                    if (Detector.HasAcquisitionCompleted || presetCountLimitHasBeenReached)
                    {
                        //If Preset was reached on a given sample
                        if (presetCountLimitHasBeenReached)
                        {
                            try
                            {
                                Detector.StopAcquiring();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("An error occurred while telling the detector to stop acquiring.", ex);
                            }
                        }

                        //Get current fine gain
                        double prevFineGain = ExecutingTestOfSpecificType.CurrentFineGain;

                        //Process and evaluate
                        ProcessSpectrum(qcSample, Detector.Spectrum, true);
                        var eResult = ExecutingTestOfSpecificType.EvaluateSample(qcSample);

                        //If failed without continue, don't proceed
                        if (eResult.Result == QcSampleEvaluationResult.Failed)
                            RunningSampleIndex = -1;
                        //If failed, we need to repeat but with new hardware settings
                        else if (eResult.Result == QcSampleEvaluationResult.FailedButRepeat)
                            qcSample.IsEvaluationCompleted = false;
                        //If failed but we need to go on to the next sample
                        else if (eResult.Result == QcSampleEvaluationResult.FailedButContinue)
                            qcSample.IsEvaluationCompleted = true;

                        if (eResult.Result == QcSampleEvaluationResult.FailedButRepeat)
                        {
                            _numberOfFailedButRepeats++;
                            _logger.Log(CreateFormattedCalibrationSampleLogMessage(prevFineGain, qcSample,
                                    ExecutingTestOfSpecificType.CurrentFineGain), Category.Info, Priority.Low);
                        }
                        else
                            _numberOfFailedButRepeats = 0;

                        // Fine gain has changed, let's update the hardware
                        if (Math.Abs(prevFineGain - ExecutingTestOfSpecificType.CurrentFineGain) > 1E-300)
                        {
                            try
                            {
                                Detector.FineGain = ExecutingTestOfSpecificType.CurrentFineGain;
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("An error occurred while setting the fine gain.", ex);
                            }
                        }

                        _logger.Log("QualityControlExecutionContext.PerformAcquisition() -- the sample '" + sample.DisplayName +
                            "' finished processing, so the acquisition-completed callback is being invoked.", Category.Info, Priority.High);
                        AcquisitionCompletedCallback(sample);

                        HasAcquisitionStarted = false;
                    }
                }
                catch (Exception ex)
                {
                    
                    _logger.Log(ex.Message, Category.Exception, Priority.High);
                    throw;
                }
            }

        }
        protected override void ProcessSpectrum(ISample sample, Spectrum spectrum, bool isAcquisitionCompleted = false)
        {
            base.ProcessSpectrum(sample, spectrum, isAcquisitionCompleted);

            var qSample = sample as QcSample;
            if (qSample == null)
                throw new NotSupportedException("Cannot process QC Test with non QC samples");

            ExecutingTestOfSpecificType.ProcessSample(qSample);
         
            _contextMetadata.FriendlyExecutingSampleName = qSample.DisplayName;
            _contextMetadata.UpdatedSampleCounts = qSample.CountsInRegionOfInterest;

            //Save a test if acquisition is completed
            if (isAcquisitionCompleted)
            {
                qSample.ExecutionStatus = SampleExecutionStatus.Completed;
                _saveTestAction(ExecutingTestOfSpecificType);
            }
        }
        protected override void OnExecutingTestStatusChanged()
        {
            switch (ExecutingTestStatus)
            {
                case TestStatus.Aborted:
#if DEBUG
                    _logger.Log("Quality Control Execution Context status is being marked as aborted. Stack trace: " + new StackTrace(),
                        Category.Info, Priority.High);
#endif
                    ExecutingTestOfSpecificType.Status = TestStatus.Aborted;
                    if(RunningSampleIndex > 0)
                    {
                        QcSample qcSample = ExecutingTestOfSpecificType.Samples.ElementAt(RunningSampleIndex);
                        qcSample.ExecutionStatus = SampleExecutionStatus.Completed;
                        qcSample.IsPassed = null; // We don't know whether the sample passed or failed
                    }
                    if (BackgroundSample != null)
                    {
                        var qcSample = BackgroundSample as QcSample;
                        if (qcSample == null)
                            throw new NotSupportedException("Backgrounds sample (as a QCSample) was null");

                        if (qcSample.ExecutionStatus != SampleExecutionStatus.Completed)
                        {
                            qcSample.ExecutionStatus = SampleExecutionStatus.Completed;
                            qcSample.IsPassed = null; // We don't know whether the sample passed or failed
                        }
                    }
                    try
                    {
                        if (Detector.IsAcquiring)
                            Detector.StopAcquiring();
                    }
                    catch (Exception ex)
                    {
                        _logger.Log(ex.Message, Category.Exception, Priority.High);
                    }
                    Detector.FineGain = _initialFineGain;
                    _saveTestAction(ExecutingTestOfSpecificType);
                    break;
                case TestStatus.Completed:
                    ExecutingTestOfSpecificType.Status = TestStatus.Completed;
                    _saveTestAction(ExecutingTestOfSpecificType);
                    break;
                case TestStatus.Running:
                    ExecutingTestOfSpecificType.Status = TestStatus.Running;
                    _saveTestAction(ExecutingTestOfSpecificType);
                    break;
            }
        }

        #endregion

        private string CreateFormattedCalibrationSampleLogMessage(double previousFineGain, QcSample qcSample, double newFineGain)
        {
            return String.Format("CALIBRATION | Current Fine Gain: {0} | Centroid: {1} keV | New Fine Gain: {2} | Pass {3} of {4}",
                previousFineGain, qcSample.Centroid, newFineGain, _numberOfFailedButRepeats, 
                _settingsManager.GetSetting<int>(SettingKeys.QcMaxNumberOfFineGainAdjustments));
        }
    }
}
