﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.QC.Services
{
	public class QualityTestDataService : IQualityTestDataService
	{
		#region Fields

		private readonly ICalibrationCurveDataAccess _calibrationCurveDataAccess;
		private readonly ILoggerFacade _logger;
		private readonly IRepository<QcTest> _tRepostiory;
		private readonly ISettingsManager _settingsManager;

		#endregion

		#region Ctor

		public QualityTestDataService(IRepository<QcTest> tRepostiory, ILoggerFacade logger, ISettingsManager settingsManager, ICalibrationCurveDataAccess calibrationCurveDataAccess)
		{
			_tRepostiory = tRepostiory;
			_logger = logger;
			_settingsManager = settingsManager;
			_calibrationCurveDataAccess = calibrationCurveDataAccess;
		}

		#endregion

		public IQueryable<QcTestItem> GetTestItems()
		{
			var result = (from r in QCTestItemsDataAccess.Select()
							select new QcTestItem
							{
								Analyst = r.ANALYST,
								TestDate = r.TEST_DATE,
								Type = r.QC_TEST_TYPE,
								TestId = r.TEST_ID,
								Result = (r.STATUS_NAME=="Running") ? "Pending" : r.PASS.ToString() == "T" ? "Passed" : "Failed",
								Status = r.STATUS_NAME,
								IsPassed = (r.STATUS_NAME=="Running") ? (bool?)null : r.PASS.ToString() == "T"

							}).AsQueryable();
			return result;
		}
		public ICalibrationCurve GetCurrentCalibrationCurve()
		{
			try
			{
				var curveRecord = _calibrationCurveDataAccess.Select(_settingsManager.GetSetting<Int32>(SettingKeys.SystemCurrentCalibrationCurveId), _logger);
				return new CalibrationCurve(curveRecord.CalibrationCurveId, curveRecord.Slope, curveRecord.OffsetValue);
			}
			catch (Exception ex)
			{
				_logger.Log("QC Test Data Service: Failed on GetCurrentCalibrationCurve(); exception message: " + ex.Message +
					"; stack trace: " + ex.StackTrace, Category.Exception, Priority.High);
				throw;
			}
		}

		public void SaveTest(QcTest test)
		{
			try
			{
				_tRepostiory.Save(test);
				_logger.Log(String.Format("QCTest ID '{0}' saved.", test.InternalId), Category.Info, Priority.None);
			}
			catch (Exception ex)
			{
				_logger.Log(String.Format("QC Test Data Service: Failed to save QCTest ID '{0}'; exception message: {1}; stack trace: {2}", test != null ? test.InternalId.ToString() : "no_id_available", ex.Message, ex.StackTrace), Category.Exception, Priority.High);
				throw;
			}
		}
		public QcTest LoadTest(Guid testId)
		{
			QcTest loadedTest;
			try
			{
				loadedTest = _tRepostiory.Select(testId);
				_logger.Log(String.Format("QCTest ID '{0}' loaded.", loadedTest.InternalId), Category.Info, Priority.None);
			}
			catch (Exception ex)
			{
				_logger.Log(String.Format("QC Test Data Service: Failed to load QCTest ID '{0}'; exception message: {1}; stack trace: {2}", testId, ex.Message, ex.StackTrace), Category.Exception, Priority.High);
				throw;
			}

			return loadedTest;
		}
		public bool DeleteTest(Guid testId)
		{
			bool deleteResult;
			try
			{
				_tRepostiory.Delete(testId);
				deleteResult = true;
				_logger.Log(String.Format("QCTest ID '{0}' deleted.", testId != Guid.Empty ? testId.ToString() : "no_id_available"), Category.Info, Priority.None);
			}
			catch (Exception ex)
			{
				deleteResult = false;
				_logger.Log(String.Format("QC Test Data Service: Failed to delete QCTest ID '{0}'; exception message: {1}; stack trace: {2}",
					testId, ex.Message, ex.StackTrace), Category.Exception, Priority.High);
			}

			return deleteResult;

		}

		public IEnumerable<QcSource> GetSources()
		{
			try
			{
				var sources = new List<QcSource>();
				//Loads sources every time
				sources.AddRange(QCSourceDataAccess.SelectList().Select(QcDataMapper.ToSourceModel));

				return sources;
			}
			catch (Exception ex)
			{
				_logger.Log(String.Format("QC Test Data Service: GetSources() failed; exception message {0}; stack trace: {1}", ex.Message, ex.StackTrace),
					Category.Exception, Priority.High);
			}

			return null;
		}
	}
}
