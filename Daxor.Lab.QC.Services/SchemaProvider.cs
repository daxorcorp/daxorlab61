﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.QC.Services
{
    public class SchemasProvider : ISchemaProvider
    {
        readonly Dictionary<TestType, SamplesSchema> _schemasCache = new Dictionary<TestType, SamplesSchema>();
        readonly ILoggerFacade _logger;

        public SchemasProvider(ILoggerFacade logger)
        {
            _logger = logger;
        }

        public SamplesSchema GetSamplesLayoutSchema(TestType tType)
        {
           //1. Try to get schema from cache
           //2. Try to load schema from dataSource
           //3. If all fails return null
            SamplesSchema schema = null;
            try
            {
                if (!_schemasCache.TryGetValue(tType, out schema))
                {
                    schema = LoadSampleLayoutSchemas(tType);
                    _schemasCache.Add(tType, schema);
                }
            }
            catch (Exception ex)
            {
                _logger.Log(String.Format("Exception GetSamplesLayoutSchema({0}). Message {1}", tType, ex.Message), Category.Exception, Priority.High);
            }

            return schema;
        }

        #region Helpers

        SamplesSchema LoadSampleLayoutSchemas(TestType tType)
        {
            var items = new Dictionary<int, SampleSchemaItem>();
            var schemaId = -1;
            Func<IEnumerable<PositionRecord>> schemaLoader;
           
            switch (tType)
            {
                case TestType.Calibration:
                    schemaLoader = () => QCTestPositionSchemaAccess.SelectList(QCTestPositionSchemaAccess.QCTestType.Calibration);
                    break;
                case TestType.Contamination:
                    schemaLoader = () => QCTestPositionSchemaAccess.SelectList(QCTestPositionSchemaAccess.QCTestType.ContaminationFull);
                    break;
                case TestType.Linearity:
                      schemaLoader = () => QCTestPositionSchemaAccess.SelectList(QCTestPositionSchemaAccess.QCTestType.Linearity);
                    break;
                case TestType.LinearityWithoutCalibration:
                    schemaLoader = () => QCTestPositionSchemaAccess.SelectList(QCTestPositionSchemaAccess.QCTestType.Linearity);
                    break;
                case TestType.Standards:
                    schemaLoader = () => QCTestPositionSchemaAccess.SelectList(QCTestPositionSchemaAccess.QCTestType.Standards);
                    break;

                default: 
                    schemaLoader = null; 
                    break;
            }

            //If loader was not provided
            if (schemaLoader == null)
                return null;

            foreach (var record in schemaLoader())
            {
                items.Add(record.Position, new SampleSchemaItem
                {
                    Position = record.Position,
                    FriendlyName = record.ShortFriendlyName,
                    Type = QcDataMapper.ToModelSampleType(record),
                });
                schemaId = record.LayoutID;
            }

            return new SamplesSchema(schemaId, items);
        }
     
        #endregion
    }
}
