﻿using System.Collections;
using System.Collections.Generic;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Services.TestConfigurators;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.QC.Services
{
    /// <summary>
    /// IEnumerable of configurators
    /// </summary>
    public class TestConfiguratorCollection : IEnumerable<IConfigurator>
    {
        private readonly List<IConfigurator> _configurators;

        public TestConfiguratorCollection(IUnityContainer serviceLocator)
        {
            //Default configurators
            _configurators = new List<IConfigurator>
                {
                    serviceLocator.Resolve<ContaminationTestConfigurator>(),
                    serviceLocator.Resolve<CalibrationTestConfigurator>(),
                    serviceLocator.Resolve<StandardsTestConfigurator>(),
                    serviceLocator.Resolve<LinearityTestConfigurator>()
                };
        }

        public IEnumerator<IConfigurator> GetEnumerator()
        {
            return _configurators.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _configurators.GetEnumerator();
        }
    }
}
