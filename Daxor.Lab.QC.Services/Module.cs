﻿using System.Collections.Generic;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Interfaces;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.QC.Services
{
	/// <summary>
	/// Quality Control Services
	/// </summary>
	public class Module : IModule
	{
		private const string ModuleName = "Quality Control Services";

		private readonly IUnityContainer _container;

		public Module(IUnityContainer container)
		{
			_container = container;
		}

		public void Initialize()
		{
			RegisterSchemaProvider();
			RegisterCalibrationCurveService();
			RegisterQcTestRepository();
			RegisterQualityTestDataService();
			RegisterIsotopeMatchingService();
			RegisterFineGainAdjustmentService();
			RegisterCalibrationCurveDataAccess();
            RegisterTestConfiguratorCollection();
			RegisterPeakFinder();
			RegisterQcTestController();
			RegisterQcCalibrationTestPayloadProcessor();
		}

	    private void RegisterQcCalibrationTestPayloadProcessor()
	    {
	        try
	        {
	            _container.RegisterType<IQcCalibrationTestPayloadProcessor, QcCalibrationTestPayloadProcessor>(
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Quality Control Calibration Test Payload Processor");
	        }
	    }

	    private void RegisterQcTestController()
	    {
	        try
	        {
	            _container.RegisterType<IQualityTestController, QcTestController>(new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Quality Control Test Controller");
	        }
	    }

	    private void RegisterPeakFinder()
	    {
	        try
	        {
	            _container.RegisterType<IPeakFinder, CanberraPeakFinder>();
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Canberra Peak Finder");
	        }
	    }

	    private void RegisterTestConfiguratorCollection()
	    {
	        try
	        {
	            _container.RegisterType<IEnumerable<IConfigurator>, TestConfiguratorCollection>();
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Test Configurator Collection");
	        }
	    }

	    private void RegisterCalibrationCurveDataAccess()
	    {
	        try
	        {
	            _container.RegisterType<ICalibrationCurveDataAccess, CalibrationCurveDataAccess>(
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Calibration Curve Data Access");
	        }
	    }

	    private void RegisterFineGainAdjustmentService()
	    {
	        try
	        {
	            _container.RegisterType<IFineGainAdjustmentService, FineGainAdjustmentService>(
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Fine Gain Adjustment Service");
	        }
	    }

	    private void RegisterIsotopeMatchingService()
	    {
	        try
	        {
	            _container.RegisterType<IIsotopeMatchingService, IsotopeMatchingService>(
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Isotope Matching Service");
	        }
	    }

	    private void RegisterQualityTestDataService()
	    {
	        try
	        {
	            _container.RegisterType<IQualityTestDataService, QualityTestDataService>(
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Quality Test Data Service");
	        }
	    }

	    private void RegisterQcTestRepository()
	    {
	        try
	        {
	            _container.RegisterType<IRepository<QcTest>, QcTestRepository>();
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Quality Control Test Repository");
	        }
	    }

	    private void RegisterCalibrationCurveService()
	    {
	        try
	        {
	            _container.RegisterType<ICalibrationCurveDataService, CalibrationCurveDataService>(
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Calibration Curve Data Service");
	        }
	    }

	    private void RegisterSchemaProvider()
	    {
	        try
	        {
	            _container.RegisterType<ISchemaProvider, SchemasProvider>(new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Schema Provider");
	        }
	    }
	}
}
