using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Models;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Common;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Utility.IntegrationTests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_QC_settings
    {
        private static List<SettingBase> _allSettings;

        [ClassInitialize]
        public static void TestsClassInitialize(TestContext context)
        {
            var settingsManager = new SystemSettingsManager(new EmptyLogger(), new SettingsManagerDataService());
            SettingsDataAccessLayer.Initialize(settingsManager);

            _allSettings = SettingsDataAccessLayer.GetAllSettings();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("Settings.xml")]
        public void Then_the_calibration_centroid_precision_is_accessible_by_service_personnel_only()
        {
            var calibrationCentroidPrecisionSetting = (from s in _allSettings where s.Key == SettingKeys.QcSetupCentroidPrecisionInkeV select s)
                    .FirstOrDefault();
            if (calibrationCentroidPrecisionSetting == null) Assert.Fail("Cannot find setting key '" + SettingKeys.QcSetupCentroidPrecisionInkeV + "'");

            Assert.AreEqual(AuthorizationLevel.Service, calibrationCentroidPrecisionSetting.ModifyPermission);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("Settings.xml")]
        public void Then_the_calibration_centroid_precision_is_a_double()
        {
            var calibrationCentroidPrecisionSetting = (from s in _allSettings where s.Key == SettingKeys.QcSetupCentroidPrecisionInkeV select s)
                    .FirstOrDefault();
            if (calibrationCentroidPrecisionSetting == null) Assert.Fail("Cannot find setting key '" + SettingKeys.QcSetupCentroidPrecisionInkeV + "'");

            Assert.AreEqual(typeof(Double), calibrationCentroidPrecisionSetting.ValueType);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("Settings.xml")]
        [RequirementValue("Centroid precision (keV)")]
        public void Then_the_calibration_centroid_precision_English_description_matches_requirements()
        {
            var calibrationCentroidPrecisionSetting = (from s in _allSettings where s.Key == SettingKeys.QcSetupCentroidPrecisionInkeV select s)
                    .FirstOrDefault();
            if (calibrationCentroidPrecisionSetting == null) Assert.Fail("Cannot find setting key '" + SettingKeys.QcSetupCentroidPrecisionInkeV + "'");

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), calibrationCentroidPrecisionSetting.Header);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("Settings.xml")]
        public void Then_the_resolution_centroid_tolerance_is_accessible_by_service_personnel_only()
        {
            var resolutionCentroidPrecisionSetting = (from s in _allSettings where s.Key == SettingKeys.QcResolutionCentroidTolerance select s)
                    .FirstOrDefault();
            if (resolutionCentroidPrecisionSetting == null) Assert.Fail("Cannot find setting key '" + SettingKeys.QcResolutionCentroidTolerance + "'");

            Assert.AreEqual(AuthorizationLevel.Service, resolutionCentroidPrecisionSetting.ModifyPermission);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("Settings.xml")]
        public void Then_the_resolution_centroid_tolerance_is_a_double()
        {
            var resolutionCentroidPrecisionSetting = (from s in _allSettings where s.Key == SettingKeys.QcResolutionCentroidTolerance select s)
                    .FirstOrDefault();
            if (resolutionCentroidPrecisionSetting == null) Assert.Fail("Cannot find setting key '" + SettingKeys.QcResolutionCentroidTolerance + "'");

            Assert.AreEqual(typeof(Double), resolutionCentroidPrecisionSetting.ValueType);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("Settings.xml")]
        [RequirementValue("QC resolution test centroid tolerance (keV)")]
        public void Then_the_resolution_centroid_tolerance_English_description_matches_requirements()
        {
            var resolutionCentroidPrecisionSetting = (from s in _allSettings where s.Key == SettingKeys.QcResolutionCentroidTolerance select s)
                    .FirstOrDefault();
            if (resolutionCentroidPrecisionSetting == null) Assert.Fail("Cannot find setting key '" + SettingKeys.QcResolutionCentroidTolerance + "'");

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), resolutionCentroidPrecisionSetting.Header);
        }
    }
    // ReSharper restore InconsistentNaming
}