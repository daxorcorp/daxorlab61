﻿using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Utility;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Database.IntegrationTests.DataAccess_Tests.PositionListDataAccess_Tests
{
	// ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_sample_position_list_for_layout_1
    {
        private IEnumerable<PositionRecord> _positionRecords;
        
        [TestInitialize]
        public void BeforeEachTest()
        {
            DatabaseSettingsHelper.LINQConnectionString = @"Data Source=(local);Initial Catalog=DaxorLab;Persist Security Info=True;User ID=DaxorDBO;Password=daxor";

            _positionRecords = PositionListDataAccess.SelectList().Where(x => x.LayoutID == 1).Select(x => x);
        }

        [TestMethod]
        public void Then_there_are_25_positions()
        {
            Assert.AreEqual(25, _positionRecords.Count(x => x.LayoutID == 1));
        }

        [TestMethod]
        public void Then_the_positon_1_name_is_Standard_A()
        {
            Assert.AreEqual("Standard A", _positionRecords.First(r => r.Position == 1).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_1_QC_name_is_Standard_A_Carrier()
        {
            Assert.AreEqual("Standard A Carrier", _positionRecords.First(r => r.Position == 1).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_2_name_is_Baseline_A()
        {
            Assert.AreEqual("Baseline A", _positionRecords.First(r => r.Position == 2).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_2_QC_name_is_Baseline_A_Carrier()
        {
            Assert.AreEqual("Baseline A Carrier", _positionRecords.First(r => r.Position == 2).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_3_name_is_Sample_1A()
        {
            Assert.AreEqual("Sample 1A", _positionRecords.First(r => r.Position == 3).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_3_QC_name_is_Carrier_1A()
        {
            Assert.AreEqual("Carrier 1A", _positionRecords.First(r => r.Position == 3).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_4_name_is_Sample_2A()
        {
            Assert.AreEqual("Sample 2A", _positionRecords.First(r => r.Position == 4).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_4_QC_name_is_Carrier_2A()
        {
            Assert.AreEqual("Carrier 2A", _positionRecords.First(r => r.Position == 4).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_5_name_is_Sample_3A()
        {
            Assert.AreEqual("Sample 3A", _positionRecords.First(r => r.Position == 5).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_5_QC_name_is_Carrier_3A()
        {
            Assert.AreEqual("Carrier 3A", _positionRecords.First(r => r.Position == 5).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_6_name_is_Sample_4A()
        {
            Assert.AreEqual("Sample 4A", _positionRecords.First(r => r.Position == 6).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_6_QC_name_is_Carrier_4A()
        {
            Assert.AreEqual("Carrier 4A", _positionRecords.First(r => r.Position == 6).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_7_name_is_Sample_5A()
        {
            Assert.AreEqual("Sample 5A", _positionRecords.First(r => r.Position == 7).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_7_QC_name_is_Carrier_5A()
        {
            Assert.AreEqual("Carrier 5A", _positionRecords.First(r => r.Position == 7).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_8_name_is_Sample_6A()
        {
            Assert.AreEqual("Sample 6A", _positionRecords.First(r => r.Position == 8).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_8_QC_name_is_Carrier_6A()
        {
            Assert.AreEqual("Carrier 6A", _positionRecords.First(r => r.Position == 8).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_9_name_is_Standard_A()
        {
            Assert.AreEqual("Standard A", _positionRecords.First(r => r.Position == 9).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_9_QC_name_is_Standard_A_Carrier()
        {
            Assert.AreEqual("Standard A Carrier", _positionRecords.First(r => r.Position == 9).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_10_name_is_QC_1()
        {
            Assert.AreEqual("QC 1", _positionRecords.First(r => r.Position == 10).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_10_QC_name_is_QC_1()
        {
            Assert.AreEqual("QC 1", _positionRecords.First(r => r.Position == 10).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_11_name_is_QC_2()
        {
            Assert.AreEqual("QC 2", _positionRecords.First(r => r.Position == 11).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_11_QC_name_is_QC_2()
        {
            Assert.AreEqual("QC 2", _positionRecords.First(r => r.Position == 11).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_12_name_is_QC_3()
        {
            Assert.AreEqual("QC 3", _positionRecords.First(r => r.Position == 12).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_12_QC_name_is_QC_3()
        {
            Assert.AreEqual("QC 3", _positionRecords.First(r => r.Position == 12).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_13_name_is_QC_4()
        {
            Assert.AreEqual("QC 4", _positionRecords.First(r => r.Position == 13).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_13_QC_name_is_QC_4()
        {
            Assert.AreEqual("QC 4", _positionRecords.First(r => r.Position == 13).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_14_name_is_Standard_B()
        {
            Assert.AreEqual("Standard B", _positionRecords.First(r => r.Position == 14).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_14_QC_name_is_Standard_B_Carrier()
        {
            Assert.AreEqual("Standard B Carrier", _positionRecords.First(r => r.Position == 14).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_15_name_is_Sample_6B()
        {
            Assert.AreEqual("Sample 6B", _positionRecords.First(r => r.Position == 15).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_15_QC_name_is_Carrier_6B()
        {
            Assert.AreEqual("Carrier 6B", _positionRecords.First(r => r.Position == 15).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_16_name_is_Sample_5B()
        {
            Assert.AreEqual("Sample 5B", _positionRecords.First(r => r.Position == 16).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_16_QC_name_is_Carrier_5B()
        {
            Assert.AreEqual("Carrier 5B", _positionRecords.First(r => r.Position == 16).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_17_name_is_Sample_4B()
        {
            Assert.AreEqual("Sample 4B", _positionRecords.First(r => r.Position == 17).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_17_QC_name_is_Carrier_4B()
        {
            Assert.AreEqual("Carrier 4B", _positionRecords.First(r => r.Position == 17).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_18_name_is_Sample_3B()
        {
            Assert.AreEqual("Sample 3B", _positionRecords.First(r => r.Position == 18).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_18_QC_name_is_Carrier_3B()
        {
            Assert.AreEqual("Carrier 3B", _positionRecords.First(r => r.Position == 18).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_19_name_is_Sample_2B()
        {
            Assert.AreEqual("Sample 2B", _positionRecords.First(r => r.Position == 19).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_19_QC_name_is_Carrier_2B()
        {
            Assert.AreEqual("Carrier 2B", _positionRecords.First(r => r.Position == 19).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_20_name_is_Sample_1B()
        {
            Assert.AreEqual("Sample 1B", _positionRecords.First(r => r.Position == 20).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_20_QC_name_is_Carrier_1B()
        {
            Assert.AreEqual("Carrier 1B", _positionRecords.First(r => r.Position == 20).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_21_name_is_Baseline_B()
        {
            Assert.AreEqual("Baseline B", _positionRecords.First(r => r.Position == 21).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_21_QC_name_is_Baseline_B_Carrier()
        {
            Assert.AreEqual("Baseline B Carrier", _positionRecords.First(r => r.Position == 21).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_22_name_is_Standard_B()
        {
            Assert.AreEqual("Standard B", _positionRecords.First(r => r.Position == 22).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_22_QC_name_is_Standard_B_Carrier()
        {
            Assert.AreEqual("Standard B Carrier", _positionRecords.First(r => r.Position == 22).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_23_name_is_Empty_Carrier()
        {
            Assert.AreEqual("Empty Carrier", _positionRecords.First(r => r.Position == 23).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_23_QC_name_is_Empty_Carrier()
        {
            Assert.AreEqual("Empty Carrier", _positionRecords.First(r => r.Position == 23).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_24_name_is_Background()
        {
            Assert.AreEqual("Background", _positionRecords.First(r => r.Position == 24).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_24_QC_name_is_Background()
        {
            Assert.AreEqual("Background", _positionRecords.First(r => r.Position == 24).QcShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_25_name_is_Empty_Carrier()
        {
            Assert.AreEqual("Empty Carrier", _positionRecords.First(r => r.Position == 25).ShortFriendlyName);
        }

        [TestMethod]
        public void Then_the_position_25_QC_name_is_Empty_Carrier()
        {
            Assert.AreEqual("Empty Carrier", _positionRecords.First(r => r.Position == 25).QcShortFriendlyName);
        }
    }
    // ReSharper restore InconsistentNaming
}