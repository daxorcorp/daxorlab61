﻿using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Database.SMO;
using Daxor.Lab.Infrastructure.Constants;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Database.IntegrationTests.DataAccess_Tests.SettingsManagerDataService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_bringing_the_databases_online
    {
        private static Server _server;
        private ISettingsManagerDataService _databaseStatusService;

        [TestInitialize]
        public void TestInitialize()
        {
            _server = GetConfiguredServer();
            TakeDatabasesOffline();
            _databaseStatusService = new SettingsManagerDataService();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_DaxorLab_database_is_online()
        {
            _databaseStatusService.BringAllDatabasesOnline();

            Assert.AreEqual(_server.Databases[Constants.DaxorDatabase].Status, DatabaseStatus.Normal);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_QC_archive_database_is_online()
        {
            _databaseStatusService.BringAllDatabasesOnline();

            Assert.AreEqual(_server.Databases[Constants.QcDatabase].Status, DatabaseStatus.Normal);
        }

        private void TakeDatabasesOffline()
        {
            _server.KillAllProcesses(Constants.QcDatabase);
            _server.KillAllProcesses(Constants.DaxorDatabase);
            _server.Databases[Constants.QcDatabase].SetOffline();
            _server.Databases[Constants.DaxorDatabase].SetOffline();
        }

        private static Server GetConfiguredServer()
        {
            var connection = new ServerConnection
            {
                ServerInstance = Constants.DatabaseServer,
                LoginSecure = false,
                Login = Constants.UserName,
                Password = DomainConstants.DaxorDatabasePassword,
                DatabaseName = "master" // You cannot restore a database that you are connected to
            };
            return new Server(connection) { LoginMode = ServerLoginMode.Mixed };
        }
    }
    // ReSharper restore InconsistentNaming
}
