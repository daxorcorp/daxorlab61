﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Services;
using Daxor.Lab.MainMenu.Views;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.MainMenu.Services.Navigation
{
    /// <summary>
    /// Main Menu module navigator. (Very simple)
    /// </summary>
    public class MMModuleNavigator : IAppModuleNavigator
    {
        #region FIELDS

        private readonly string _moduleKey;
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;
        private readonly IEventAggregator _eventAggregator;
        private readonly ILoggerFacade _customLoggerFacade;

        private readonly Dictionary<string, FrameworkElement> _views =
            new Dictionary<string, FrameworkElement>();

        //Contains a single view
        private string _currentViewKey = MMModuleViewKeys.Empty;

        #endregion

        #region CTOR

        public MMModuleNavigator(IUnityContainer container, IEventAggregator eventAggregator,
                                 IRegionManager regionManager, INavigationCoordinator navigationCoordinator,
                                 ILoggerFacade customLoggerFacade)
        {
            _container = container;
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _customLoggerFacade = customLoggerFacade;

            //Set my local application key
            _moduleKey = AppModuleIds.MainMenu;

            //Register myself with a global application coordinator
            navigationCoordinator.RegisterAppModuleNavigator(_moduleKey, this);

            NavigationCoordinator = navigationCoordinator;
        }

        #endregion

        #region PROPERTIES
       
        public bool IsActive
        {
            get
            {
                return NavigationCoordinator.ActiveAppModuleKey.Equals(_moduleKey);
            }
        }

        public INavigationCoordinator NavigationCoordinator
        {
            get;
            private set;
        }

        public FrameworkElement RootViewHost
        {
            get;
            private set;
        }
        public string AppModuleKey
        {
            get { return _moduleKey; }
        }

        #endregion

        #region METHODS

        public bool CanDeactivate()
        {
            return true;
        }
        public bool CanActivate()
        {
            return true;
        }

        public void Activate(DisplayViewOption displayViewOption, object payload)
        {
            _eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(ApplicationParts.ShutdownButton, true));
            
            if (_currentViewKey == MMModuleViewKeys.Empty)
            {
                ActivateView(MMModuleViewKeys.MainMenu, payload);
            }
            else
            {
                //Activate last accessed view
                ActivateView(_currentViewKey, payload);
            }

        }
        public void Deactivate(Action onDeactivateCallback)
        {
            //Run storyboard, and on storyboard complete remove the view;
            if (_currentViewKey != MMModuleViewKeys.Empty)
            {
                IRegion workspaceRegion = _regionManager.Regions[RegionNames.WorkspaceRegion];
                workspaceRegion.Deactivate(_views[_currentViewKey]);
                _currentViewKey = MMModuleViewKeys.Empty;
                _customLoggerFacade.Log("Deactivate ViewName - " + _currentViewKey, Microsoft.Practices.Composite.Logging.Category.Info, Microsoft.Practices.Composite.Logging.Priority.None);
            }
            onDeactivateCallback();
        }

        public void ActivateView(string viewKey)
        {
            ActivateView(viewKey, null);
        }
        public void ActivateView(string viewKey, object payload)
        {
            if (_currentViewKey != MMModuleViewKeys.Empty)
            {
                IRegion workspaceRegion = _regionManager.Regions[RegionNames.WorkspaceRegion];
                workspaceRegion.Deactivate(_views[_currentViewKey]);
                _currentViewKey = MMModuleViewKeys.Empty;

                ActivateView(viewKey, payload);
            }
            else
            {
                //Add view to the region
                IRegion workspaceRegion = _regionManager.Regions[RegionNames.WorkspaceRegion];
                FrameworkElement requestedView = _views[viewKey];

                //Add view to the workspace region
                if (!workspaceRegion.Views.Contains(_views[viewKey]))
                {
                    workspaceRegion.Add(_views[viewKey]);
                }

                workspaceRegion.Activate(_views[viewKey]);
                _currentViewKey = viewKey;

                _eventAggregator.GetEvent<ActiveViewTitleChanged>().Publish("Main Menu");
                _customLoggerFacade.Log("ActivateView ViewName - " + _currentViewKey, 
                    Microsoft.Practices.Composite.Logging.Category.Info, Microsoft.Practices.Composite.Logging.Priority.None);
            }
        }
        public void Initialize()
        {
            RootViewHost = (FrameworkElement)_container.Resolve<MainMenuView>();
            _views.Add(MMModuleViewKeys.MainMenu, RootViewHost);
            _regionManager.Regions[RegionNames.WorkspaceRegion].Add(RootViewHost);
        }

        #endregion
    }
}
