﻿using System;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.MainMenu.Services.Navigation;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.MainMenu
{
    public class Module : AppModule
    {
        public Module(IUnityContainer container)
            : base(AppModuleIds.MainMenu, container)
        {
            ShortDisplayName = "Main Menu";
            DisplayOrder = 0;
            BuildDate = DateTime.Now.AddDays(-999);
        }

        public override void InitializeAppModule()
        {
            // Register MM navigator with Unity, followed by view initialization
            UnityContainer.RegisterType<IAppModuleNavigator, MMModuleNavigator>(UniqueAppId, new ContainerControlledLifetimeManager());

	        InitializeAppModuleNavigator();
	        ActivateAppModule();
        }

        private void ActivateAppModule()
        {
            // Activate main menu module through navigation coordinator
            try
            {
                UnityContainer.Resolve<INavigationCoordinator>().ActivateAppModule(UniqueAppId, null, "");
            }
            catch
            {
                throw new ModuleLoadException(ShortDisplayName, "use the Navigation Coordinator to activate the module");
            }
        }

        private void InitializeAppModuleNavigator()
        {
            try
            {
                UnityContainer.Resolve<IAppModuleNavigator>(UniqueAppId).Initialize();
            }
            catch
            {
                throw new ModuleLoadException(ShortDisplayName, "initialize the Module Navigator");
            }
        }
    }
}
