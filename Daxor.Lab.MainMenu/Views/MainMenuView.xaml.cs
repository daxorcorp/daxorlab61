﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Daxor.Lab.MainMenu.ViewModels;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.MainMenu.Views
{
    /// <summary>
    /// Interaction logic for MainMenuView.xaml
    /// </summary>
    public partial class MainMenuView : UserControl, IActiveAware
    {
        #region Ctor

        public MainMenuView()
        {
            InitializeComponent();
          
        }

        public MainMenuView(MainMenuViewModel vm)
            : this()
        {
            DataContext = vm;
        }

        #endregion

        #region IActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                IActiveAware vmAware = DataContext as IActiveAware;
                if (vmAware != null)
                {
                    vmAware.IsActive = value;
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
