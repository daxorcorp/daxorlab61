﻿using System;
using System.Collections.Specialized;
using System.Linq;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MainMenu.Models;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.MainMenu.ViewModels
{
    /// <summary>
    /// MainMenuViewModel is incharge of MainMenuView.
    /// </summary>
    public class MainMenuViewModel : ViewModelBase, IActiveAware
    {
        //Thread safe observable collection
        private readonly AppModuleCollection _menuItems = new AppModuleCollection();
        private readonly IAppModuleInfoCatalog _appModuleCatalog;
        private readonly ILoggerFacade _logger;

        public AppModuleCollection MenuItems
        {
            get { return _menuItems; }
        }

        public MainMenuViewModel(IAppModuleInfoCatalog appModuleCatalog, ILoggerFacade customLoggerFacade)
        {
            _appModuleCatalog = appModuleCatalog;
            _appModuleCatalog.Items.CollectionChanged += AppModuleInfoCatalog_CollectionChanged;
            _logger = customLoggerFacade;
           
            AddExistingAppModules();
        }

        #region Helpers
        //Fires when appModuleInfo items collection has changed
        private void AppModuleInfoCatalog_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //Added
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (IAppModuleInfo appInfo in e.NewItems)
                {
                    _logger.Log("AppModuleInfoCatalog_CollectionChanged Added - " + appInfo.ShortDisplayName, Microsoft.Practices.Composite.Logging.Category.Info, Microsoft.Practices.Composite.Logging.Priority.None);
                    //Skip menu, should not be display
                    if (appInfo.AppModuleKey == AppModuleIds.MainMenu) continue;

                    MenuItems.Add(appInfo);
                }

                SortMenuItemsByDisplayOrder();
            }

            //Removed
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (IAppModuleInfo appInfo in e.OldItems)
                {
                    MenuItems.Remove(appInfo);
                    _logger.Log("AppModuleInfoCatalog_CollectionChanged Removed - " + appInfo.ShortDisplayName, Microsoft.Practices.Composite.Logging.Category.Info, Microsoft.Practices.Composite.Logging.Priority.None);
                }
            }
        }
        private void AddExistingAppModules()
        {
            if (_appModuleCatalog.Items.Count > 0 && _menuItems.Count == 0)
            {
                foreach (IAppModuleInfo appModule in _appModuleCatalog.Items)
                {
                    //Skip Menu
                    if (appModule.AppModuleKey == AppModuleIds.MainMenu) continue;

                    _menuItems.Add(appModule);
                }

                SortMenuItemsByDisplayOrder();
            }
        }
        private void SortMenuItemsByDisplayOrder()
        {
            var sortedCollection = (from app in MenuItems
                                    orderby app.DisplayOrder
                                    select app).ToList();
            MenuItems.Clear();
            sortedCollection.ForEach((item) => MenuItems.Add(item));
        }

        #endregion

        #region IActiveAware Members
        private bool _isActive;
        public bool IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
                _logger.Log("MainMenuViewModel: MainMenuViewModel active: " + _isActive, Category.Debug, Priority.Low);
                IsActiveChanged(this, EventArgs.Empty);
            }
        }

        public event EventHandler IsActiveChanged = delegate { };
        #endregion
    }
}
