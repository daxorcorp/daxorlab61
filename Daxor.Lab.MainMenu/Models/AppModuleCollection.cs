﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.Infrastructure.Interfaces;

namespace Daxor.Lab.MainMenu.Models
{
    public class AppModuleCollection : ThreadSafeObservableCollection<IAppModuleInfo>
    {
        public AppModuleCollection()
        {
        }
    }
}
