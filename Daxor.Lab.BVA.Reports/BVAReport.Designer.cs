namespace Daxor.Lab.BVA.Reports
{
    partial class BVAReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BVAReport));
			this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
			this.textBoxReportType = new Telerik.Reporting.TextBox();
			this.textBoxReportStatus = new Telerik.Reporting.TextBox();
			this.panelPatientDemographics = new Telerik.Reporting.Panel();
			this.shape1 = new Telerik.Reporting.Shape();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBox31 = new Telerik.Reporting.TextBox();
			this.textBoxAccession = new Telerik.Reporting.TextBox();
			this.textBox33 = new Telerik.Reporting.TextBox();
			this.textBoxTestID2 = new Telerik.Reporting.TextBox();
			this.textBox35 = new Telerik.Reporting.TextBox();
			this.textBox36 = new Telerik.Reporting.TextBox();
			this.textBoxPatientHospitalID = new Telerik.Reporting.TextBox();
			this.textBoxPatientName = new Telerik.Reporting.TextBox();
			this.textBoxReferringMD = new Telerik.Reporting.TextBox();
			this.textBoxComments = new Telerik.Reporting.TextBox();
			this.textBox41 = new Telerik.Reporting.TextBox();
			this.textBox42 = new Telerik.Reporting.TextBox();
			this.textBox43 = new Telerik.Reporting.TextBox();
			this.textBox44 = new Telerik.Reporting.TextBox();
			this.textBoxPatientDOB = new Telerik.Reporting.TextBox();
			this.textBoxPatientGender = new Telerik.Reporting.TextBox();
			this.textBoxPatientHeight = new Telerik.Reporting.TextBox();
			this.textBoxPatientWeight = new Telerik.Reporting.TextBox();
			this.textBox49 = new Telerik.Reporting.TextBox();
			this.textBox50 = new Telerik.Reporting.TextBox();
			this.textBoxDateAnalyzed = new Telerik.Reporting.TextBox();
			this.textBoxAmputeeStatus = new Telerik.Reporting.TextBox();
			this.textBoxCCTo = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBoxDeviationFromIdealWeight = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBoxAnalyst = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBoxInjectateLot = new Telerik.Reporting.TextBox();
			this.textBoxLocation = new Telerik.Reporting.TextBox();
			this.textBoxLocationLabel = new Telerik.Reporting.TextBox();
			this.panelHospitalInformation = new Telerik.Reporting.Panel();
			this.textBoxHospitalName = new Telerik.Reporting.TextBox();
			this.textBoxHospitalAddress = new Telerik.Reporting.TextBox();
			this.textBoxHospitalPhone = new Telerik.Reporting.TextBox();
			this.textBoxHospitalDepartment = new Telerik.Reporting.TextBox();
			this.textBoxHospitalDepartmentDirector = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.pictureBoxIcon = new Telerik.Reporting.PictureBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBoxBVAVersion = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBoxUnitID = new Telerik.Reporting.TextBox();
			this.textBox21 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// pageHeaderSection1
			// 
			this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(3.5225620269775391D);
			this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxReportType,
            this.textBoxReportStatus,
            this.panelPatientDemographics,
            this.panelHospitalInformation,
            this.textBox5,
            this.pictureBoxIcon});
			this.pageHeaderSection1.Name = "pageHeaderSection1";
			// 
			// textBoxReportType
			// 
			this.textBoxReportType.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1145834922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.77083331346511841D));
			this.textBoxReportType.Name = "textBoxReportType";
			this.textBoxReportType.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
			this.textBoxReportType.Style.Font.Name = "Times New Roman";
			this.textBoxReportType.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxReportType.Value = "= IIf(PageNumber =1, \"PHYSICIAN\", IIf(PageNumber=2,\"ANALYST\", \"\"))";
			// 
			// textBoxReportStatus
			// 
			this.textBoxReportStatus.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1145834922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.57291668653488159D));
			this.textBoxReportStatus.Name = "textBoxReportStatus";
			this.textBoxReportStatus.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
			this.textBoxReportStatus.Style.Font.Name = "Times New Roman";
			this.textBoxReportStatus.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxReportStatus.Value = "-";
			// 
			// panelPatientDemographics
			// 
			this.panelPatientDemographics.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.shape1,
            this.textBox12,
            this.textBox31,
            this.textBoxAccession,
            this.textBox33,
            this.textBoxTestID2,
            this.textBox35,
            this.textBox36,
            this.textBoxPatientHospitalID,
            this.textBoxPatientName,
            this.textBoxReferringMD,
            this.textBoxComments,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBoxPatientDOB,
            this.textBoxPatientGender,
            this.textBoxPatientHeight,
            this.textBoxPatientWeight,
            this.textBox49,
            this.textBox50,
            this.textBoxDateAnalyzed,
            this.textBoxAmputeeStatus,
            this.textBoxCCTo,
            this.textBox2,
            this.textBoxDeviationFromIdealWeight,
            this.textBox3,
            this.textBoxAnalyst,
            this.textBox6,
            this.textBox1,
            this.textBoxInjectateLot,
            this.textBoxLocation,
            this.textBoxLocationLabel});
			this.panelPatientDemographics.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(1.375D));
			this.panelPatientDemographics.Name = "panelPatientDemographics";
			this.panelPatientDemographics.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.899960994720459D), Telerik.Reporting.Drawing.Unit.Inch(2.1475620269775391D));
			// 
			// shape1
			// 
			this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
			this.shape1.Name = "shape1";
			this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.PolygonShape(4, 45D, 0);
			this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7963333129882812D), Telerik.Reporting.Drawing.Unit.Inch(1.8454786539077759D));
			this.shape1.Stretch = true;
			// 
			// textBox12
			// 
			this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9259593904716894E-05D));
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7916667461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.3020833432674408D));
			this.textBox12.Style.BackgroundColor = System.Drawing.Color.Black;
			this.textBox12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox12.Style.Color = System.Drawing.Color.White;
			this.textBox12.Style.Font.Bold = true;
			this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox12.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox12.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox12.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox12.Value = "Patient Demographics";
			// 
			// textBox31
			// 
			this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.4583333432674408D));
			this.textBox31.Name = "textBox31";
			this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2124208211898804D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox31.Style.Font.Bold = true;
			this.textBox31.Style.Font.Name = "Times New Roman";
			this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox31.Value = "Patient Name:";
			// 
			// textBoxAccession
			// 
			this.textBoxAccession.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2239586114883423D), Telerik.Reporting.Drawing.Unit.Inch(0.93765765428543091D));
			this.textBoxAccession.Name = "textBoxAccession";
			this.textBoxAccession.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2655458450317383D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxAccession.Style.Font.Name = "Times New Roman";
			this.textBoxAccession.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxAccession.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxAccession.TextWrap = false;
			this.textBoxAccession.Value = "-";
			// 
			// textBox33
			// 
			this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.4947917461395264D), Telerik.Reporting.Drawing.Unit.Inch(1.1773198843002319D));
			this.textBox33.Name = "textBox33";
			this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.736221969127655D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox33.Style.Font.Bold = true;
			this.textBox33.Style.Font.Name = "Times New Roman";
			this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox33.Value = "Weight:";
			// 
			// textBoxTestID2
			// 
			this.textBoxTestID2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.93765765428543091D));
			this.textBoxTestID2.Name = "textBoxTestID2";
			this.textBoxTestID2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2124208211898804D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxTestID2.Style.Font.Bold = true;
			this.textBoxTestID2.Style.Font.Name = "Times New Roman";
			this.textBoxTestID2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxTestID2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxTestID2.Value = "Accession:";
			// 
			// textBox35
			// 
			this.textBox35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0625D), Telerik.Reporting.Drawing.Unit.Inch(1.1773198843002319D));
			this.textBox35.Name = "textBox35";
			this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1499208211898804D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox35.Style.Font.Bold = true;
			this.textBox35.Style.Font.Name = "Times New Roman";
			this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox35.Value = "Referring MD:";
			// 
			// textBox36
			// 
			this.textBox36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.7000001668930054D));
			this.textBox36.Name = "textBox36";
			this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2124208211898804D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox36.Style.Font.Bold = true;
			this.textBox36.Style.Font.Name = "Times New Roman";
			this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox36.Value = "Comments:";
			// 
			// textBoxPatientHospitalID
			// 
			this.textBoxPatientHospitalID.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2239586114883423D), Telerik.Reporting.Drawing.Unit.Inch(0.69799548387527466D));
			this.textBoxPatientHospitalID.Name = "textBoxPatientHospitalID";
			this.textBoxPatientHospitalID.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2655458450317383D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxPatientHospitalID.Style.Font.Name = "Times New Roman";
			this.textBoxPatientHospitalID.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxPatientHospitalID.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPatientHospitalID.TextWrap = false;
			this.textBoxPatientHospitalID.Value = "-";
			// 
			// textBoxPatientName
			// 
			this.textBoxPatientName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2239586114883423D), Telerik.Reporting.Drawing.Unit.Inch(0.4583333432674408D));
			this.textBoxPatientName.Name = "textBoxPatientName";
			this.textBoxPatientName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5925502777099609D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxPatientName.Style.Font.Name = "Times New Roman";
			this.textBoxPatientName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
			this.textBoxPatientName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxPatientName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPatientName.TextWrap = false;
			this.textBoxPatientName.Value = "-";
			// 
			// textBoxReferringMD
			// 
			this.textBoxReferringMD.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2239586114883423D), Telerik.Reporting.Drawing.Unit.Inch(1.1773198843002319D));
			this.textBoxReferringMD.Name = "textBoxReferringMD";
			this.textBoxReferringMD.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2655458450317383D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxReferringMD.Style.Font.Name = "Times New Roman";
			this.textBoxReferringMD.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxReferringMD.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxReferringMD.TextWrap = false;
			this.textBoxReferringMD.Value = "-";
			// 
			// textBoxComments
			// 
			this.textBoxComments.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2239586114883423D), Telerik.Reporting.Drawing.Unit.Inch(1.7000001668930054D));
			this.textBoxComments.Multiline = true;
			this.textBoxComments.Name = "textBoxComments";
			this.textBoxComments.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.5265412330627441D), Telerik.Reporting.Drawing.Unit.Inch(0.44756188988685608D));
			this.textBoxComments.Style.Font.Name = "Times New Roman";
			this.textBoxComments.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxComments.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBoxComments.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBoxComments.Value = " ";
			// 
			// textBox41
			// 
			this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.4895832538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.93765765428543091D));
			this.textBox41.Name = "textBox41";
			this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.74984234571456909D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox41.Style.Font.Bold = true;
			this.textBox41.Style.Font.Name = "Times New Roman";
			this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox41.Value = "Height:";
			// 
			// textBox42
			// 
			this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.4895832538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.4583333432674408D));
			this.textBox42.Name = "textBox42";
			this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.74984234571456909D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox42.Style.Font.Bold = true;
			this.textBox42.Style.Font.Name = "Times New Roman";
			this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox42.Value = "DOB:";
			// 
			// textBox43
			// 
			this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.4895832538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.69799548387527466D));
			this.textBox43.Name = "textBox43";
			this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.74984234571456909D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox43.Style.Font.Bold = true;
			this.textBox43.Style.Font.Name = "Times New Roman";
			this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox43.Value = "Gender:";
			// 
			// textBox44
			// 
			this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.72924548387527466D));
			this.textBox44.Name = "textBox44";
			this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2124208211898804D), Telerik.Reporting.Drawing.Unit.Inch(0.1770833283662796D));
			this.textBox44.Style.Font.Bold = true;
			this.textBox44.Style.Font.Name = "Times New Roman";
			this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox44.Value = "ID:";
			// 
			// textBoxPatientDOB
			// 
			this.textBoxPatientDOB.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.239504337310791D), Telerik.Reporting.Drawing.Unit.Inch(0.4618174135684967D));
			this.textBoxPatientDOB.Name = "textBoxPatientDOB";
			this.textBoxPatientDOB.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2104955911636353D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxPatientDOB.Style.Font.Name = "Times New Roman";
			this.textBoxPatientDOB.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxPatientDOB.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPatientDOB.TextWrap = false;
			this.textBoxPatientDOB.Value = "-";
			// 
			// textBoxPatientGender
			// 
			this.textBoxPatientGender.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.239504337310791D), Telerik.Reporting.Drawing.Unit.Inch(0.69799548387527466D));
			this.textBoxPatientGender.Name = "textBoxPatientGender";
			this.textBoxPatientGender.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.92067527770996094D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxPatientGender.Style.Font.Name = "Times New Roman";
			this.textBoxPatientGender.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxPatientGender.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPatientGender.TextWrap = false;
			this.textBoxPatientGender.Value = "-";
			// 
			// textBoxPatientHeight
			// 
			this.textBoxPatientHeight.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.239504337310791D), Telerik.Reporting.Drawing.Unit.Inch(0.93765765428543091D));
			this.textBoxPatientHeight.Name = "textBoxPatientHeight";
			this.textBoxPatientHeight.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.92067527770996094D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxPatientHeight.Style.Font.Name = "Times New Roman";
			this.textBoxPatientHeight.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxPatientHeight.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPatientHeight.TextWrap = false;
			this.textBoxPatientHeight.Value = "-";
			// 
			// textBoxPatientWeight
			// 
			this.textBoxPatientWeight.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.239504337310791D), Telerik.Reporting.Drawing.Unit.Inch(1.1773198843002319D));
			this.textBoxPatientWeight.Name = "textBoxPatientWeight";
			this.textBoxPatientWeight.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.92067527770996094D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxPatientWeight.Style.Font.Name = "Times New Roman";
			this.textBoxPatientWeight.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxPatientWeight.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPatientWeight.TextWrap = false;
			this.textBoxPatientWeight.Value = "-";
			// 
			// textBox49
			// 
			this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.4829316139221191D), Telerik.Reporting.Drawing.Unit.Inch(0.4583333432674408D));
			this.textBox49.Name = "textBox49";
			this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.85999995470047D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox49.Style.Font.Bold = true;
			this.textBox49.Style.Font.Name = "Times New Roman";
			this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox49.Value = "Analyzed On:";
			// 
			// textBox50
			// 
			this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.2128810882568359D), Telerik.Reporting.Drawing.Unit.Inch(0.69799548387527466D));
			this.textBox50.Name = "textBox50";
			this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1300506591796875D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox50.Style.Font.Bold = true;
			this.textBox50.Style.Font.Name = "Times New Roman";
			this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox50.Value = "Type:";
			// 
			// textBoxDateAnalyzed
			// 
			this.textBoxDateAnalyzed.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3477587699890137D), Telerik.Reporting.Drawing.Unit.Inch(0.4583333432674408D));
			this.textBoxDateAnalyzed.Name = "textBoxDateAnalyzed";
			this.textBoxDateAnalyzed.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4600000381469727D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
			this.textBoxDateAnalyzed.Style.Font.Name = "Times New Roman";
			this.textBoxDateAnalyzed.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxDateAnalyzed.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxDateAnalyzed.TextWrap = false;
			this.textBoxDateAnalyzed.Value = "-";
			// 
			// textBoxAmputeeStatus
			// 
			this.textBoxAmputeeStatus.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3477587699890137D), Telerik.Reporting.Drawing.Unit.Inch(0.69799548387527466D));
			this.textBoxAmputeeStatus.Name = "textBoxAmputeeStatus";
			this.textBoxAmputeeStatus.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4600000381469727D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
			this.textBoxAmputeeStatus.Style.Font.Name = "Times New Roman";
			this.textBoxAmputeeStatus.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxAmputeeStatus.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxAmputeeStatus.TextWrap = false;
			this.textBoxAmputeeStatus.Value = "-";
			// 
			// textBoxCCTo
			// 
			this.textBoxCCTo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2239586114883423D), Telerik.Reporting.Drawing.Unit.Inch(1.4169820547103882D));
			this.textBoxCCTo.Name = "textBoxCCTo";
			this.textBoxCCTo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7400000095367432D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxCCTo.Style.Font.Name = "Times New Roman";
			this.textBoxCCTo.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxCCTo.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxCCTo.TextWrap = false;
			this.textBoxCCTo.Value = "-";
			// 
			// textBox2
			// 
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.06770864874124527D), Telerik.Reporting.Drawing.Unit.Inch(1.4169820547103882D));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1499208211898804D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox2.Style.Font.Bold = true;
			this.textBox2.Style.Font.Name = "Times New Roman";
			this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox2.Value = "CC:";
			// 
			// textBoxDeviationFromIdealWeight
			// 
			this.textBoxDeviationFromIdealWeight.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.7300000190734863D), Telerik.Reporting.Drawing.Unit.Inch(1.4199999570846558D));
			this.textBoxDeviationFromIdealWeight.Name = "textBoxDeviationFromIdealWeight";
			this.textBoxDeviationFromIdealWeight.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
			this.textBoxDeviationFromIdealWeight.Style.Font.Name = "Times New Roman";
			this.textBoxDeviationFromIdealWeight.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxDeviationFromIdealWeight.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxDeviationFromIdealWeight.TextWrap = false;
			this.textBoxDeviationFromIdealWeight.Value = "-";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.7699999809265137D), Telerik.Reporting.Drawing.Unit.Inch(1.4199999570846558D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9500000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox3.Style.Font.Bold = true;
			this.textBox3.Style.Font.Name = "Times New Roman";
			this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox3.Value = "Deviation from Ideal Weight:";
			// 
			// textBoxAnalyst
			// 
			this.textBoxAnalyst.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3482975959777832D), Telerik.Reporting.Drawing.Unit.Inch(0.93765765428543091D));
			this.textBoxAnalyst.Name = "textBoxAnalyst";
			this.textBoxAnalyst.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4600000381469727D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
			this.textBoxAnalyst.Style.Font.Name = "Times New Roman";
			this.textBoxAnalyst.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxAnalyst.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxAnalyst.TextWrap = false;
			this.textBoxAnalyst.Value = "-";
			// 
			// textBox6
			// 
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.2128810882568359D), Telerik.Reporting.Drawing.Unit.Inch(0.93765765428543091D));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1300506591796875D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox6.Style.Font.Bold = true;
			this.textBox6.Style.Font.Name = "Times New Roman";
			this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox6.Value = "Analyst:";
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.2128810882568359D), Telerik.Reporting.Drawing.Unit.Inch(1.1773198843002319D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1300506591796875D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox1.Style.Font.Bold = true;
			this.textBox1.Style.Font.Name = "Times New Roman";
			this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox1.Value = "Injectate Lot:";
			// 
			// textBoxInjectateLot
			// 
			this.textBoxInjectateLot.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3482975959777832D), Telerik.Reporting.Drawing.Unit.Inch(1.1773198843002319D));
			this.textBoxInjectateLot.Name = "textBoxInjectateLot";
			this.textBoxInjectateLot.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4600000381469727D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
			this.textBoxInjectateLot.Style.Font.Name = "Times New Roman";
			this.textBoxInjectateLot.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxInjectateLot.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxInjectateLot.TextWrap = false;
			this.textBoxInjectateLot.Value = "-";
			// 
			// textBoxLocation
			// 
			this.textBoxLocation.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3482975959777832D), Telerik.Reporting.Drawing.Unit.Inch(1.4169820547103882D));
			this.textBoxLocation.Name = "textBoxLocation";
			this.textBoxLocation.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4600000381469727D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
			this.textBoxLocation.Style.Font.Name = "Times New Roman";
			this.textBoxLocation.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxLocation.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxLocation.TextWrap = false;
			this.textBoxLocation.Value = "-";
			// 
			// textBoxLocationLabel
			// 
			this.textBoxLocationLabel.CanGrow = false;
			this.textBoxLocationLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.4829316139221191D), Telerik.Reporting.Drawing.Unit.Inch(1.4169820547103882D));
			this.textBoxLocationLabel.Name = "textBoxLocationLabel";
			this.textBoxLocationLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.86000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
			this.textBoxLocationLabel.Style.Font.Bold = true;
			this.textBoxLocationLabel.Style.Font.Name = "Times New Roman";
			this.textBoxLocationLabel.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxLocationLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxLocationLabel.Value = "LOCATION:";
			// 
			// panelHospitalInformation
			// 
			this.panelHospitalInformation.Anchoring = Telerik.Reporting.AnchoringStyles.Left;
			this.panelHospitalInformation.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxHospitalName,
            this.textBoxHospitalAddress,
            this.textBoxHospitalPhone,
            this.textBoxHospitalDepartment,
            this.textBoxHospitalDepartmentDirector});
			this.panelHospitalInformation.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.1875D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.panelHospitalInformation.Name = "panelHospitalInformation";
			this.panelHospitalInformation.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4541666507720947D), Telerik.Reporting.Drawing.Unit.Inch(1.2916666269302368D));
			// 
			// textBoxHospitalName
			// 
			this.textBoxHospitalName.Docking = Telerik.Reporting.DockingStyle.Top;
			this.textBoxHospitalName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBoxHospitalName.Name = "textBoxHospitalName";
			this.textBoxHospitalName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4541666507720947D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxHospitalName.Style.Font.Name = "Times New Roman";
			this.textBoxHospitalName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxHospitalName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxHospitalName.Value = "Hospital Name";
			// 
			// textBoxHospitalAddress
			// 
			this.textBoxHospitalAddress.Docking = Telerik.Reporting.DockingStyle.Top;
			this.textBoxHospitalAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxHospitalAddress.Name = "textBoxHospitalAddress";
			this.textBoxHospitalAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4541666507720947D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxHospitalAddress.Style.Font.Name = "Times New Roman";
			this.textBoxHospitalAddress.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxHospitalAddress.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxHospitalAddress.Value = "Department Address";
			// 
			// textBoxHospitalPhone
			// 
			this.textBoxHospitalPhone.Docking = Telerik.Reporting.DockingStyle.Top;
			this.textBoxHospitalPhone.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.4791666567325592D));
			this.textBoxHospitalPhone.Name = "textBoxHospitalPhone";
			this.textBoxHospitalPhone.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4541666507720947D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxHospitalPhone.Style.Font.Name = "Times New Roman";
			this.textBoxHospitalPhone.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxHospitalPhone.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxHospitalPhone.Value = "Department Phone Number";
			// 
			// textBoxHospitalDepartment
			// 
			this.textBoxHospitalDepartment.Docking = Telerik.Reporting.DockingStyle.Top;
			this.textBoxHospitalDepartment.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.71875D));
			this.textBoxHospitalDepartment.Name = "textBoxHospitalDepartment";
			this.textBoxHospitalDepartment.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4541666507720947D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxHospitalDepartment.Style.Font.Name = "Times New Roman";
			this.textBoxHospitalDepartment.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxHospitalDepartment.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxHospitalDepartment.Value = "Department";
			// 
			// textBoxHospitalDepartmentDirector
			// 
			this.textBoxHospitalDepartmentDirector.Docking = Telerik.Reporting.DockingStyle.Top;
			this.textBoxHospitalDepartmentDirector.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.95833331346511841D));
			this.textBoxHospitalDepartmentDirector.Name = "textBoxHospitalDepartmentDirector";
			this.textBoxHospitalDepartmentDirector.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4541666507720947D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxHospitalDepartmentDirector.Style.Font.Name = "Times New Roman";
			this.textBoxHospitalDepartmentDirector.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxHospitalDepartmentDirector.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxHospitalDepartmentDirector.Value = "Department Director";
			// 
			// textBox5
			// 
			this.textBox5.Format = "{0}";
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1145834922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7000001668930054D), Telerik.Reporting.Drawing.Unit.Inch(0.39958333969116211D));
			this.textBox5.Style.Font.Bold = true;
			this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox5.Value = "Blood Volume Analysis\r\n";
			// 
			// pictureBoxIcon
			// 
			this.pictureBoxIcon.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.pictureBoxIcon.MimeType = "image/jpeg";
			this.pictureBoxIcon.Name = "pictureBoxIcon";
			this.pictureBoxIcon.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8125D), Telerik.Reporting.Drawing.Unit.Inch(0.8125D));
			this.pictureBoxIcon.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBoxIcon.Value = ((object)(resources.GetObject("pictureBoxIcon.Value")));
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.64583331346511841D);
			this.detail.Name = "detail";
			this.detail.Style.Visible = true;
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.3333333432674408D);
			this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17,
            this.textBoxBVAVersion,
            this.textBox20,
            this.textBoxUnitID,
            this.textBox21});
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// textBox17
			// 
			this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64583331346511841D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox17.Style.Font.Name = "Times New Roman";
			this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox17.Value = "BVA-100:";
			// 
			// textBoxBVAVersion
			// 
			this.textBoxBVAVersion.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.64591217041015625D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBoxBVAVersion.Name = "textBoxBVAVersion";
			this.textBoxBVAVersion.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.85609245300292969D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxBVAVersion.Style.Font.Name = "Times New Roman";
			this.textBoxBVAVersion.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBoxBVAVersion.Value = "v1.2.0.0";
			// 
			// textBox20
			// 
			this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.9429168701171875D), Telerik.Reporting.Drawing.Unit.Inch(0.00041675567626953125D));
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.54158782958984375D), Telerik.Reporting.Drawing.Unit.Inch(0.23916657269001007D));
			this.textBox20.Style.Font.Name = "Times New Roman";
			this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox20.Value = "Unit ID:";
			// 
			// textBoxUnitID
			// 
			this.textBoxUnitID.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.4845836162567139D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBoxUnitID.Name = "textBoxUnitID";
			this.textBoxUnitID.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.8499999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
			this.textBoxUnitID.Style.Font.Name = "Times New Roman";
			this.textBoxUnitID.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBoxUnitID.Value = "-";
			// 
			// textBox21
			// 
			this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7819075584411621D), Telerik.Reporting.Drawing.Unit.Inch(0.00041675567626953125D));
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.9894256591796875D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox21.Style.Font.Name = "Times New Roman";
			this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox21.Value = "=\'Page \' + PageNumber + \' of \' + PageCount";
			// 
			// BVAReport
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
			this.Name = "BVAReport";
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.37999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.9416670799255371D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.TextBox textBox17;
        public Telerik.Reporting.TextBox textBoxBVAVersion;
        private Telerik.Reporting.TextBox textBox20;
        public Telerik.Reporting.TextBox textBoxUnitID;
        private Telerik.Reporting.TextBox textBox21;
        public Telerik.Reporting.TextBox textBoxReportStatus;
        private Telerik.Reporting.Panel panelPatientDemographics;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox31;
        public Telerik.Reporting.TextBox textBoxAccession;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        public Telerik.Reporting.TextBox textBoxPatientHospitalID;
        public Telerik.Reporting.TextBox textBoxPatientName;
        public Telerik.Reporting.TextBox textBoxReferringMD;
        public Telerik.Reporting.TextBox textBoxComments;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        public Telerik.Reporting.TextBox textBoxPatientDOB;
        public Telerik.Reporting.TextBox textBoxPatientGender;
        public Telerik.Reporting.TextBox textBoxPatientHeight;
        public Telerik.Reporting.TextBox textBoxPatientWeight;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        public Telerik.Reporting.TextBox textBoxDateAnalyzed;
        public Telerik.Reporting.TextBox textBoxAmputeeStatus;
        public Telerik.Reporting.TextBox textBoxCCTo;
        private Telerik.Reporting.TextBox textBox2;
        public Telerik.Reporting.TextBox textBoxDeviationFromIdealWeight;
        private Telerik.Reporting.TextBox textBox3;
        public Telerik.Reporting.TextBox textBoxAnalyst;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox1;
        public Telerik.Reporting.TextBox textBoxInjectateLot;
        public Telerik.Reporting.TextBox textBoxLocation;
        private Telerik.Reporting.Panel panelHospitalInformation;
        public Telerik.Reporting.TextBox textBoxHospitalName;
        public Telerik.Reporting.TextBox textBoxHospitalAddress;
        public Telerik.Reporting.TextBox textBoxHospitalPhone;
        public Telerik.Reporting.TextBox textBoxHospitalDepartment;
        public Telerik.Reporting.TextBox textBoxHospitalDepartmentDirector;
        private Telerik.Reporting.TextBox textBox5;
        public Telerik.Reporting.TextBox textBoxTestID2;
        public Telerik.Reporting.TextBox textBoxLocationLabel;
        public Telerik.Reporting.PictureBox pictureBoxIcon;
        public Telerik.Reporting.DetailSection detail;
        public Telerik.Reporting.TextBox textBoxReportType;
        public Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        public Telerik.Reporting.PageFooterSection pageFooterSection1;
    }
}