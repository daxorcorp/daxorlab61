﻿using System;
using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Interfaces;

namespace Daxor.Lab.BVA.Reports.ReportModels
{
    public class BvaTestReportModel
    {
        #region Fields

        private readonly BVATest _bvaTest;
        public readonly IIdealsCalcEngineService CalcEngine;
        
        #endregion

        #region Ctor

        public BvaTestReportModel(BVATest test, IIdealsCalcEngineService calcEngine, IEnumerable<AuditTrailRecord> auditTrailRecordList)
        {
            _bvaTest = test;
            CalcEngine = calcEngine;
            AuditRecordList = auditTrailRecordList;
        }

        #endregion

        #region Properties

        public IEnumerable<AuditTrailRecord> AuditRecordList { get; set; }
        public BVAPatient Patient { get { return _bvaTest.Patient; } }
        public TestMode Mode { get { return _bvaTest.Mode; } }
        public TestStatus Status { get { return _bvaTest.Status; } }
        // ReSharper disable once InconsistentNaming
        public string TestID2 { get { return _bvaTest.TestID2; } }
        public string Analyst { get { return _bvaTest.Analyst; } }
        public string CcReportTo { get { return _bvaTest.CcReportTo; } }
        public string Comments { get { return _bvaTest.Comments; } }
        public DateTime Date { get { return _bvaTest.Date; } }
        public string InjectateLot { get { return _bvaTest.InjectateLot; } }
        public string Location { get { return _bvaTest.Location; } }
        public string RefPhysician { get { return _bvaTest.RefPhysician; } }
        public string SystemId { get { return _bvaTest.SystemId; } }
        public bool ShowAuditTrail { get; set; }

        #endregion

        #region Methods

        public bool HeightInRange()
        {
            return (_bvaTest.Patient.HeightInCm <= CalcEngine.MaximumHeightInCm && _bvaTest.Patient.HeightInCm >= CalcEngine.MinimumHeightInCm);
        }

        public bool IsTotalBloodVolumeInRange()
        {
            return (_bvaTest.Volumes[BloodType.Measured].WholeCount < BvaDomainConstants.MaximumBloodVolumeInmL) &&
                                    (_bvaTest.Volumes[BloodType.Measured].WholeCount > BvaDomainConstants.MinimumBloodVolumeInmL);
        }

        #endregion
    }
}
