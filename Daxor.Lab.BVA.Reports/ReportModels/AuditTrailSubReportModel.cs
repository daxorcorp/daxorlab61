﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.Database.DataRecord;
using System.Collections.Generic;
using System.Xml.Linq;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.BVA.Reports.ReportModels
{
    public class AuditTrailSubReportModel
    {
        #region Fields

        private readonly BVATest _bvaTest;

        #endregion

        #region Ctor

        public AuditTrailSubReportModel(BVATest test, IEnumerable<AuditTrailRecord> auditTrailRecordList)
        {
            _bvaTest = test;
            AuditRecordList = auditTrailRecordList;
        }

        #endregion

        #region Properties

        public IEnumerable<AuditTrailRecord> AuditRecordList { get; set; }

        #endregion

        #region Public API

        public string DetermineWatermarkText()
        {
            switch (_bvaTest.Mode)
            {
                case TestMode.Manual:
                    return "MANUAL\nTEST";
                case TestMode.VOPS:
                    return "VOPS\nTEST";
                case TestMode.TrainingHighSlopeTest:
                case TestMode.TrainingHighStd:
                case TestMode.Training:
                case TestMode.TrainingReverseSlope:
                    return "Training Test\nCounts Are Simulated";
                default:
                    switch (_bvaTest.Status)
                    {
                        case TestStatus.Aborted:
                            return "ABORTED";
                        case TestStatus.Pending:
                        case TestStatus.Running:
                        case TestStatus.Undefined:
                        case TestStatus.Deleted:
                            return "PRELIMINARY";
                        default:
                            return "";
                    }
            }
        }

        public bool IsTrainingTest
        {
            get
            {
                switch (_bvaTest.Mode)
                {
                    case TestMode.TrainingHighSlopeTest:
                    case TestMode.TrainingHighStd:
                    case TestMode.Training:
                    case TestMode.TrainingReverseSlope:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public XElement GenerateAuditTrailReportElement()
        {
            var xElem = new XElement("AuditTrailReport");

            var list = AuditRecordList;

            foreach (var record in list)
            {
                xElem.Add(new XElement("AuditTrail", new XAttribute("Date", record.ChangeTime),
                    new XElement("ChangedBy", record.ChangedBy),
                    new XElement("ChangeField", record.ChangeField),
                    new XElement("ChangeTable", record.ChangeTable),
                    new XElement("OldValue", record.OldValue),
                    new XElement("NewValue", record.NewValue),
                    new XElement("PositionData", record.PositionData),
                    new XElement("SampleId", record.SampleId)));
            }

            return xElem;
        }

        #endregion
    }
}
