﻿using System;
using System.Globalization;
using System.Text;
using System.Xml.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.BVA.Reports.ReportModels
{
    public class PhysiciansSubReportModel
    {
        #region Fields

        private readonly IIdealsCalcEngineService _calcEngine;
        private readonly BVATest _bvaTest;
        private readonly ISettingsManager _settingsManager;

        private string _normalizedHct;
        private string _peripheralHct;
        private string _transudationRate;
        private string _measuredWholeCountMlkgAnalysisElement;
        private string _idealWholeCountMlkgAnalysisElement;
        private string _measuredRbCountMlkgAnalysisElement;
        private string _idealRbCountMlkgAnalysisElement;
        private string _measuredPlasmaMlkgAnalysisElement;
        private string _idealPlasmaMlkgAnalysisElement;
        private string _wholeBloodCondition;
        private string _plasmaCondition;
        private string _redCellCondition;

        #endregion

        #region Ctor

        public PhysiciansSubReportModel(BVATest test, IIdealsCalcEngineService calcEngine, ISettingsManager settingsManager)
        {
            _bvaTest = test;
            _calcEngine = calcEngine;
            _settingsManager = settingsManager;
            CalculateDeviations();
        }

        #endregion

        #region Properties

        public ISettingsManager SettingsManager { get { return _settingsManager; } }
        public double WholeBloodDeviation { get; set; }
        public double PlasmaDeviation { get; set; }
        public double RbcDeviation { get; set; }
        public double WholeBloodPercentDeviation { get; set; }
        public double PlasmaPercentDeviation { get; set; }
        public double RbcPercentDeviation { get; set; }
        public bool ShowFindings { get; set; }

        #endregion

        #region Methods

        public string DetermineWatermarkText()
        {
            switch (_bvaTest.Mode)
            {
                case TestMode.Manual:
                    return "MANUAL\nTEST";
                case TestMode.VOPS:
                    return "VOPS\nTEST";
                case TestMode.TrainingHighSlopeTest:
                case TestMode.TrainingHighStd:
                case TestMode.Training:
                case TestMode.TrainingReverseSlope:
                    return "Training Test\nCounts Are Simulated";
                default:
                    switch (_bvaTest.Status)
                    {
                        case TestStatus.Aborted:
                            return "ABORTED";
                        case TestStatus.Pending:
                        case TestStatus.Running:
                        case TestStatus.Undefined:
                        case TestStatus.Deleted:
                            return "PRELIMINARY";
                        default:
                            return "";
                    }
            }
        }

        public bool IsTrainingTest
        {
            get
            {
                switch (_bvaTest.Mode)
                {
                    case TestMode.TrainingHighSlopeTest:
                    case TestMode.TrainingHighStd:
                    case TestMode.Training:
                    case TestMode.TrainingReverseSlope:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IsGenderMale()
        {
            return _bvaTest.Patient.Gender == GenderType.Male;
        }

        public string GenerateNormalizedHct()
        {
            if (_normalizedHct != null) return _normalizedHct;

            _normalizedHct = Math.Abs(_bvaTest.NormalizeHematocritResult) < 0 || !IsTotalBloodVolumeInRange() || !IsWeightDeviationInRange() || !IsHeightInRange()
                ? "**" : Math.Round(_bvaTest.NormalizeHematocritResult, 1, MidpointRounding.AwayFromZero) + "%";

            return _normalizedHct;
        }

        public string GeneratePeripheralHct()
        {
            if (_peripheralHct != null) return _peripheralHct;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _peripheralHct = _bvaTest.PatientHematocritResult == -1 || Double.IsNaN(_bvaTest.PatientHematocritResult) || !IsTotalBloodVolumeInRange()
                ? "**" : Math.Round(_bvaTest.PatientHematocritResult, 1, MidpointRounding.AwayFromZero) + "%";

            return _peripheralHct;
        }

        public string GenerateTransudationRate()
        {
            if (_transudationRate != null) return _transudationRate;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (_bvaTest.TransudationRateResult == 0 || Double.IsNaN(_bvaTest.TransudationRateResult) || !IsTotalBloodVolumeInRange())
                _transudationRate = "**";
            else if (_bvaTest.TransudationRateResult > 0)
                _transudationRate = Math.Round((_bvaTest.TransudationRateResult * 100), 2, MidpointRounding.AwayFromZero).ToString("#0.00");
            else if (_bvaTest.TransudationRateResult > -0.0005)
                _transudationRate = Math.Round((_bvaTest.TransudationRateResult * 100), 2, MidpointRounding.AwayFromZero).ToString("#0.00") + "*";
            else
                _transudationRate = "*";

            return _transudationRate;
        }

        public string GenerateMeasuredWholeCountMlkgAnalysisElement()
        {
            if (_measuredWholeCountMlkgAnalysisElement != null) return _measuredWholeCountMlkgAnalysisElement;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _measuredWholeCountMlkgAnalysisElement = Math.Abs(_bvaTest.Volumes[BloodType.Measured].WholeCount) < -1 || _bvaTest.Patient.WeightInKg == 0 || !IsTotalBloodVolumeInRange()
                ? "**" : Math.Round((_bvaTest.Volumes[BloodType.Measured].WholeCount / _bvaTest.Patient.WeightInKg), 1, MidpointRounding.AwayFromZero).ToString("#0.0");

            return _measuredWholeCountMlkgAnalysisElement;
        }

        public string GenerateIdealWholeCountMlkgAnalysisElement()
        {
            if (_idealWholeCountMlkgAnalysisElement != null) return _idealWholeCountMlkgAnalysisElement;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _idealWholeCountMlkgAnalysisElement = Math.Abs(_bvaTest.Volumes[BloodType.Ideal].WholeCount) < -1 || _bvaTest.Patient.WeightInKg == 0 || !IsWeightDeviationInRange() || !IsHeightInRange()
                ? "**" : Math.Round((_bvaTest.Volumes[BloodType.Ideal].WholeCount / _bvaTest.Patient.WeightInKg), 1, MidpointRounding.AwayFromZero).ToString("#0.0");

            return _idealWholeCountMlkgAnalysisElement;
        }

        public string GenerateMeasuredRbCountMlkgAnalysisElement()
        {
            if (_measuredRbCountMlkgAnalysisElement != null) return _measuredRbCountMlkgAnalysisElement;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _measuredRbCountMlkgAnalysisElement = Math.Abs(_bvaTest.Volumes[BloodType.Measured].RedCellCount) < -1 || _bvaTest.Patient.WeightInKg == 0 || !IsTotalBloodVolumeInRange()
                ? "**" : Math.Round((_bvaTest.Volumes[BloodType.Measured].RedCellCount / _bvaTest.Patient.WeightInKg), 1, MidpointRounding.AwayFromZero).ToString("#0.0");

            return _measuredRbCountMlkgAnalysisElement;
        }

        public string GenerateIdealRbCountMlkgAnalysisElement()
        {
            if (_idealRbCountMlkgAnalysisElement != null) return _idealRbCountMlkgAnalysisElement;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _idealRbCountMlkgAnalysisElement = Math.Abs(_bvaTest.Volumes[BloodType.Ideal].RedCellCount) < -1 || _bvaTest.Patient.WeightInKg == 0 || !IsWeightDeviationInRange() || !IsHeightInRange()
                ? "**" : Math.Round((_bvaTest.Volumes[BloodType.Ideal].RedCellCount / _bvaTest.Patient.WeightInKg), 1, MidpointRounding.AwayFromZero).ToString("#0.0");

            return _idealRbCountMlkgAnalysisElement;
        }

        public string GenerateMeasuredPlasmaMlkgAnalysisElement()
        {
            if (_measuredPlasmaMlkgAnalysisElement != null) return _measuredPlasmaMlkgAnalysisElement;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _measuredPlasmaMlkgAnalysisElement = Math.Abs(_bvaTest.Volumes[BloodType.Measured].PlasmaCount) < -1 || _bvaTest.Patient.WeightInKg == 0 || !IsTotalBloodVolumeInRange()
                ? "**" : Math.Round((_bvaTest.Volumes[BloodType.Measured].PlasmaCount / _bvaTest.Patient.WeightInKg), 1, MidpointRounding.AwayFromZero).ToString("#0.0");

            return _measuredPlasmaMlkgAnalysisElement;
        }

        public string GenerateIdealPlasmaMlkgAnalysisElement()
        {
            if (_idealPlasmaMlkgAnalysisElement != null) return _idealPlasmaMlkgAnalysisElement;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _idealPlasmaMlkgAnalysisElement = Math.Abs(_bvaTest.Volumes[BloodType.Ideal].PlasmaCount) < -1 || _bvaTest.Patient.WeightInKg == 0 || !IsWeightDeviationInRange() || !IsHeightInRange()
                ? "**" : Math.Round((_bvaTest.Volumes[BloodType.Ideal].PlasmaCount / _bvaTest.Patient.WeightInKg), 1, MidpointRounding.AwayFromZero).ToString("#0.0");

            return _idealPlasmaMlkgAnalysisElement;
        }

        public string AddTechnicalNotesData()
        {
            var technicalNotes = new StringBuilder();

            technicalNotes.Append("Technical Notes: This Blood Volume Analysis was performed by the radiopharmaceutical tracer dilution technique utilizing a calibrated dose of ");
            technicalNotes.Append(_settingsManager.GetSetting<string>(SettingKeys.BvaInjectateDescription));
            technicalNotes.Append(", separate timed blood samples, measured hematocrits and a regression analysis to time zero.  Corrections for plasma packing (0.99) and the ratio of whole body");
            technicalNotes.Append(" hematocrit to venous hematocrit (ƒ=0.91) are incorporated.  Sample analysis consists of an evaluation of separate blood volume collection points compared to a");
            technicalNotes.Append(" matched standard with mathematical evaluation of consistency.  Sample analysis evaluation is reported as");
            technicalNotes.Append(" acceptable or unacceptable with a standard deviation of less than 3.9% being acceptable.");
            technicalNotes.Append(" The sample analysis of this report is ");

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if ((_bvaTest.StandardDevResult != -1.0) && (!double.IsNaN(_bvaTest.StandardDevResult)) && IsTotalBloodVolumeInRange())
            {
                technicalNotes.Append(StandardDeviationAsARoundedPercent(_bvaTest.StandardDevResult).ToString("#0.000") + "% and is ");
                technicalNotes.Append(Math.Abs(_bvaTest.StandardDevResult) < .039 ? "acceptable." : "not acceptable.");
            }
            else
                technicalNotes.Append("**.");

            technicalNotes.Append("  nHct is the theoretical hematocrit that would result if the patient's plasma volume were adjusted to achieve a normal total blood volume.");
            technicalNotes.Append("  Ideal Values based on Feldschuh, J and Enson, Y. Prediction of the normal blood volume: Relation of blood volume to body habitus.  Circulation. 1977;56:605-612.");
			//The space in front of amputee guidance is so there's room for the gray square symbol
			technicalNotes.Append("\nKey: ** = data cannot be calculated, - = data missing, * = data out of range,        = see amputee guidance.");
            return technicalNotes.ToString();
        }

        public string AddReportFindingsData()
		{
			var reportFindings = new StringBuilder();
			
            if (!IsTotalBloodVolumeInRange() && _bvaTest.Volumes[BloodType.Measured].WholeCount < BvaDomainConstants.MinimumBloodVolumeInmL)
                reportFindings.Append("Measured blood volume is below system limit of 1,700 mL.  ");

            if (!IsTotalBloodVolumeInRange() && _bvaTest.Volumes[BloodType.Measured].WholeCount > BvaDomainConstants.MaximumBloodVolumeInmL)
                reportFindings.Append("Measured blood volume exceeds system limit of 14,500 mL.  ");

            if (_bvaTest.Status == TestStatus.Completed && _bvaTest.Volumes[BloodType.Measured].WholeCount == 0)
                reportFindings.Append("Measured blood volume is not available.  Please review test data.  ");

            if (Math.Round(_bvaTest.Patient.DeviationFromIdealWeight, 1, MidpointRounding.AwayFromZero) > BvaDomainConstants.MaximumWeightDeviation)
                reportFindings.Append("Ideal blood volume is not available as patient weight deviation is above system maximum of " + BvaDomainConstants.MaximumWeightDeviation.ToString(CultureInfo.InvariantCulture) + "%. ");

            if (Math.Round(_bvaTest.Patient.DeviationFromIdealWeight, 1, MidpointRounding.AwayFromZero) < BvaDomainConstants.MinimumWeightDeviation)
                reportFindings.Append("Ideal blood volume is not available as patient weight deviation is below system minimum of " + BvaDomainConstants.MinimumWeightDeviation.ToString(CultureInfo.InvariantCulture) + "%. ");

            if (Math.Round(_bvaTest.Patient.HeightInCm, 2, MidpointRounding.AwayFromZero) < Math.Round(_calcEngine.MinimumHeightInCm, 2, MidpointRounding.AwayFromZero))
                reportFindings.Append("Ideal blood volume is not available as patient height is below system minimum of " + _calcEngine.MinimumHeightInCm.ToString(CultureInfo.InvariantCulture) + " cm or " +
                    Math.Round(UnitsConversionHelper.ConvertHeightToEnglish(_calcEngine.MinimumHeightInCm), 2, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture) + " in. ");

            if (Math.Round(_bvaTest.Patient.HeightInCm, 2, MidpointRounding.AwayFromZero) > Math.Round(_calcEngine.MaximumHeightInCm, 2, MidpointRounding.AwayFromZero))
                reportFindings.Append("Ideal blood volume is not available as patient height is above system maximum of " + _calcEngine.MaximumHeightInCm.ToString(CultureInfo.InvariantCulture) + " cm or " +
                    Math.Round(UnitsConversionHelper.ConvertHeightToEnglish(_calcEngine.MaximumHeightInCm), 2, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture) + " in. ");

            if (reportFindings.Length != 0)
				return reportFindings.ToString();

			if (_bvaTest.Patient.IsAmputee && (_bvaTest.AmputeeIdealsCorrectionFactor == 0 || _bvaTest.AmputeeIdealsCorrectionFactor == -1))
				reportFindings.Append("AMPUTEE PATIENT: Ideal TBV & RCV corrected by 0% as per interpreting physician guidance.  ");
			else if (_bvaTest.Patient.IsAmputee)
				reportFindings.Append("AMPUTEE PATIENT: Ideal TBV & RCV corrected by -" + _bvaTest.AmputeeIdealsCorrectionFactor + "% as per interpreting physician guidance.  ");

            reportFindings.Append("Blood Volume Analysis result indicates patient's total blood volume is ");

            string volemic;
            var formattedWholeBloodCondition = GenerateWholeBloodCondition().ToLower();

            if (formattedWholeBloodCondition == "normal")
                volemic = " volemic";
            else
                volemic = WholeBloodDeviation > 0 ? "ly hypervolemic" : "ly hypovolemic";

            reportFindings.Append(formattedWholeBloodCondition + volemic);
            reportFindings.Append(", with ");
            reportFindings.Append(formattedWholeBloodCondition == "extreme" ? "an " : "a ");

            reportFindings.Append(formattedWholeBloodCondition);
            reportFindings.Append(" ");
            reportFindings.Append(DetermineExcessOrDeficit(WholeBloodDeviation));
            reportFindings.Append(" of ");
            reportFindings.Append(WholeBloodDeviation > 0 ? "+" + Math.Round(WholeBloodDeviation, 1, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture) : Math.Round(WholeBloodDeviation, 1, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture));
            reportFindings.Append(" mL or ");

            if (WholeBloodDeviation > 0)
                reportFindings.Append("+");

            reportFindings.Append(Math.Round(WholeBloodPercentDeviation, 1, MidpointRounding.AwayFromZero).ToString("#0.0"));
            reportFindings.Append("% from ideal; plasma volume is ");
            var formattedPlasmaCondition = GeneratePlasmaCondition().ToLower();

            if (formattedPlasmaCondition == "normal")
                volemic = " volemic";
            else
                volemic = PlasmaDeviation > 0 ? "ly hypervolemic" : "ly hypovolemic";

            reportFindings.Append(formattedPlasmaCondition + volemic);
            reportFindings.Append(", with ");
            reportFindings.Append(formattedPlasmaCondition == "extreme" ? "an " : "a ");

            reportFindings.Append(formattedPlasmaCondition);
            reportFindings.Append(" ");
            reportFindings.Append(DetermineExcessOrDeficit(PlasmaDeviation));
            reportFindings.Append(" of ");
            reportFindings.Append(PlasmaDeviation > 0 ? "+" + Math.Round(PlasmaDeviation, 1, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture) : Math.Round(PlasmaDeviation, 1, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture));
            reportFindings.Append(" mL or ");

            if (PlasmaDeviation > 0)
                reportFindings.Append("+");

            reportFindings.Append(Math.Round(PlasmaPercentDeviation, 1, MidpointRounding.AwayFromZero).ToString("#0.0"));
            reportFindings.Append("% from ideal; ");
            reportFindings.Append("red blood cell volume is ");

            var formattedRedCondition = GenerateRedCellCondition().ToLower();

            if (formattedRedCondition == "normal")
                volemic = " volemic";
            else
                volemic = RbcDeviation > 0 ? "ly hypervolemic" : "ly hypovolemic";

            reportFindings.Append(formattedRedCondition + volemic);
            reportFindings.Append(", with ");

            reportFindings.Append(formattedRedCondition == "extreme" ? "an " : "a ");

            reportFindings.Append(formattedRedCondition);
            reportFindings.Append(" ");
            reportFindings.Append(DetermineExcessOrDeficit(RbcDeviation));
            reportFindings.Append(" of ");
            reportFindings.Append(RbcDeviation > 0 ?
                "+" + Math.Round(RbcDeviation, 1, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture) :
                Math.Round(RbcDeviation, 1, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture));
            reportFindings.Append(" mL or ");

            if (RbcDeviation > 0)
                reportFindings.Append("+");

            reportFindings.Append(Math.Round(RbcPercentDeviation, 1, MidpointRounding.AwayFromZero).ToString("#0.0"));
            reportFindings.Append("%");

            return reportFindings.ToString();
        }

        public XElement GenerateAdditionalAnalysisElement()
        {
            var xElem = new XElement("AdditionalAnalysis");
            xElem.Add(new XElement("NormalizedHCT", new XAttribute("Units", "%"), GenerateNormalizedHct()));

            xElem.Add(new XElement("PeripheralVenousHct", new XAttribute("Units", "%"), GeneratePeripheralHct()));

            if (_bvaTest.Patient.Gender == GenderType.Male)
            {
                xElem.Add(new XElement("NormalizedHCTRange", new XAttribute("Units", "%"), _settingsManager.GetSetting<string>(SettingKeys.BvaNormalizedHematocritRangeMale).TrimEnd('%')));
                xElem.Add(new XElement("PeripheralVenousHctRange", new XAttribute("Units", "%"), _settingsManager.GetSetting<string>(SettingKeys.BvaHematocritRangeMale).TrimEnd('%')));
            }
            else
            {
                xElem.Add(new XElement("NormalizedHCTRange", new XAttribute("Units", "%"), _settingsManager.GetSetting<string>(SettingKeys.BvaNormalizedHematocritRangeFemale).TrimEnd('%')));
                xElem.Add(new XElement("PeripheralVenousHctRange", new XAttribute("Units", "%"), _settingsManager.GetSetting<string>(SettingKeys.BvaHematocritRangeFemale).TrimEnd('%')));
            }
            xElem.Add(new XElement("mLkgAnalysis",
                    new XElement("TotalVolume", new XAttribute("Units", "mL/kg"), GenerateMeasuredWholeCountMlkgAnalysisElement()),
                    new XElement("TotalVolumeIdeal", new XAttribute("Units", "mL/kg"), GenerateIdealWholeCountMlkgAnalysisElement()),
                    new XElement("RBCVolume", new XAttribute("Units", "mL/kg"), GenerateMeasuredRbCountMlkgAnalysisElement()),
                    new XElement("RBCVolumeIdeal", new XAttribute("Units", "mL/kg"), GenerateIdealRbCountMlkgAnalysisElement()),
                    new XElement("PlasmaVolume", new XAttribute("Units", "mL/kg"), GenerateMeasuredPlasmaMlkgAnalysisElement()),
                    new XElement("PlasmaVolumeIdeal", new XAttribute("Units", "mL/kg"), GenerateIdealPlasmaMlkgAnalysisElement())));

            xElem.Add(new XElement("Transudation", new XAttribute("Units", "%/min"), GenerateTransudationRate()));
            xElem.Add(new XElement("AlbuminTransudationReferenceRange", new XAttribute("Units", "%/min"),
                new XElement("Normal", "0 to 0.25"),
                new XElement("Elevated", "0.25 to 0.4"),
                new XElement("High", "0.4 to 0.5"),
                new XElement("VeryHigh", ">0.5")));
            return xElem;
        }

        public XElement GeneratePhysiciansReportElement()
        {
            var xElem = new XElement("PhysiciansData");

            xElem.Add(new XElement("ReportFindings", AddReportFindingsData()));
            xElem.Add(new XElement("TechnicalNotes", AddTechnicalNotesData()));

            return xElem;
        }

        #endregion

        #region Private Helper Methods

        private bool IsTotalBloodVolumeInRange()
        {
            return (_bvaTest.Volumes[BloodType.Measured].WholeCount < BvaDomainConstants.MaximumBloodVolumeInmL) &&
                                    (_bvaTest.Volumes[BloodType.Measured].WholeCount > BvaDomainConstants.MinimumBloodVolumeInmL);
        }

        private bool IsWeightDeviationInRange()
        {
            return (_bvaTest.Patient.DeviationFromIdealWeight >= BvaDomainConstants.MinimumWeightDeviation &&
                        _bvaTest.Patient.DeviationFromIdealWeight <= BvaDomainConstants.MaximumWeightDeviation);
        }

        private bool IsHeightInRange()
        {
            return (_bvaTest.Patient.HeightInCm <= _calcEngine.MaximumHeightInCm && _bvaTest.Patient.HeightInCm >= _calcEngine.MinimumHeightInCm);
        }

        private string GenerateWholeBloodCondition()
        {
            return _wholeBloodCondition ?? (_wholeBloodCondition = GetCondition(WholeBloodPercentDeviation.ToString(CultureInfo.InvariantCulture), "Blood"));
        }

        private void CalculateDeviations()
        {
            WholeBloodDeviation = _bvaTest.Volumes[BloodType.Measured].WholeCount - _bvaTest.Volumes[BloodType.Ideal].WholeCount;
            if (_bvaTest.Volumes[BloodType.Ideal].WholeCount != 0)
            {
                WholeBloodPercentDeviation = (WholeBloodDeviation / _bvaTest.Volumes[BloodType.Ideal].WholeCount) * 100;
            }

            PlasmaDeviation = _bvaTest.Volumes[BloodType.Measured].PlasmaCount - _bvaTest.Volumes[BloodType.Ideal].PlasmaCount;
            if (_bvaTest.Volumes[BloodType.Ideal].PlasmaCount != 0)
            {
                PlasmaPercentDeviation = (PlasmaDeviation / _bvaTest.Volumes[BloodType.Ideal].PlasmaCount) * 100;
            }

            RbcDeviation = _bvaTest.Volumes[BloodType.Measured].RedCellCount - _bvaTest.Volumes[BloodType.Ideal].RedCellCount;
            if (_bvaTest.Volumes[BloodType.Ideal].RedCellCount != 0)
            {
                RbcPercentDeviation = (RbcDeviation / _bvaTest.Volumes[BloodType.Ideal].RedCellCount) * 100;
            }
        }

        private static string GetCondition(string deviationString, string bloodPart)
        {
            // if no deviation (**), return **
            if (deviationString == "-     ")
            {
                return "**";
            }

            var conditionArray = new[] { "Normal", "Mild", "Moderate", "Severe", "Extreme", "Extreme" };

            var deviationIndex = 0;
            var roundedDeviation = Math.Abs(Math.Round(double.Parse(deviationString), 1, MidpointRounding.AwayFromZero));

            switch (bloodPart)
            {
                case "Blood":
                case "Plasma":
                    if (roundedDeviation <= 8)
                    {
                        deviationIndex = 0; // Normal
                    }
                    else if (roundedDeviation <= 16)
                    {
                        deviationIndex = 1; // Mild
                    }
                    else if (roundedDeviation <= 24)
                    {
                        deviationIndex = 2; // Moderate
                    }
                    else if (roundedDeviation <= 32)
                    {
                        deviationIndex = 3; // Severe
                    }
                    else
                    {
                        deviationIndex = 4; // Extreme
                    }

                    break;
                case "Red":
                    if (roundedDeviation <= 10)
                    {
                        deviationIndex = 0; // Normal
                    }
                    else if (roundedDeviation <= 20)
                    {
                        deviationIndex = 1; // Mild
                    }
                    else if (roundedDeviation <= 30)
                    {
                        deviationIndex = 2; // Moderate
                    }
                    else if (roundedDeviation <= 40)
                    {
                        deviationIndex = 3; // Severe
                    }
                    else // > 40
                    {
                        deviationIndex = 4; // Extreme
                    }

                    break;
            }

            if (deviationIndex > 5)
            {
                deviationIndex = 5;
            }

            return conditionArray[deviationIndex];
        }

        private static string DetermineExcessOrDeficit(double deviation)
        {
            return deviation > 0 ? "excess" : "deficit";
        }

        private string GeneratePlasmaCondition()
        {
            return _plasmaCondition ?? (_plasmaCondition = GetCondition(PlasmaPercentDeviation.ToString(CultureInfo.InvariantCulture), "Plasma"));
        }

        private string GenerateRedCellCondition()
        {
            return _redCellCondition ?? (_redCellCondition = GetCondition(RbcPercentDeviation.ToString(CultureInfo.InvariantCulture), "Red"));
        }

        public static double StandardDeviationAsARoundedPercent(double standardDeviationResult)
        {
            var standardDeviationPercent = standardDeviationResult * 100;
            return Math.Round(standardDeviationPercent, 3, MidpointRounding.AwayFromZero);
        }

        #endregion
    }
}
