﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Reports.Common;
using Daxor.Lab.BVA.Services.RuleEvaluators;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Daxor.Lab.SettingsManager.Interfaces;
using Telerik.Reporting.Charting;
using Telerik.Reporting.Charting.Styles;
using Chart = Telerik.Reporting.Chart;

namespace Daxor.Lab.BVA.Reports.ReportModels
{
    public class AnalystSubReportModel
    {
        #region Fields

        private readonly ISettingsManager _settingsManager;
        private readonly BVATest _bvaTest;
        private string _transudationRate;
        private string _standardDeviationText;
        private string _measuredWholeBloodVolume;
        private string _transudationSlope;

        private const string CountsEnteredManuallyString = "Counts entered manually";
        private const string MinuteUnitsString = " min.";
        private const string SecondsUnitString = " sec.";
        private const string OneLowStandardString = "1 Low Standard";
        private const string TwoLowStandardsString = "2 Low Standards";


        #endregion

        #region Ctor

        public AnalystSubReportModel(BVATest test, IEnumerable<AuditTrailRecord> auditTrailRecordList, ISettingsManager settingsManager)
        {
            _bvaTest = test;
            _settingsManager = settingsManager;
            AuditRecordList = auditTrailRecordList;
        }

        #endregion

        #region Properties

        public IEnumerable<AuditTrailRecord> AuditRecordList { get; set; }

        #endregion

        #region Public API

        public string GenerateTransudationRate()
        {
            if (_transudationRate != null) return _transudationRate;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (_bvaTest.TransudationRateResult == 0 || Double.IsNaN(_bvaTest.TransudationRateResult) || !IsTotalBloodVolumeInRange())
                _transudationRate = "**";
            else if (_bvaTest.TransudationRateResult > 0)
                _transudationRate = Math.Round((_bvaTest.TransudationRateResult * 100), 2, MidpointRounding.AwayFromZero).ToString("#0.00");
            else if (_bvaTest.TransudationRateResult > -0.0005)
                _transudationRate = Math.Round((_bvaTest.TransudationRateResult * 100), 2, MidpointRounding.AwayFromZero).ToString("#0.00") + "*";
            else
                _transudationRate = "*";

            return _transudationRate;
        }

        public string GenerateStandardDeviationText()
        {
            if (_standardDeviationText != null) return _standardDeviationText;

            _standardDeviationText = Double.IsNaN(_bvaTest.StandardDevResult) || !IsTotalBloodVolumeInRange()
                ? "**" : GetStandardDeviationText(_bvaTest.StandardDevResult);

            return _standardDeviationText;
        }

        public string GenerateFinalBloodVolume()
        {
            var measuredWholeBloodVolume = GenerateMeasuredWholeBloodVolumeText();
            return measuredWholeBloodVolume != "**" && measuredWholeBloodVolume != "-" ?
                measuredWholeBloodVolume + " mL" : measuredWholeBloodVolume;
        }

        public bool IsTotalBloodVolumeInRange()
        {
            return (_bvaTest.Volumes[BloodType.Measured].WholeCount < BvaDomainConstants.MaximumBloodVolumeInmL) &&
                                    (_bvaTest.Volumes[BloodType.Measured].WholeCount > BvaDomainConstants.MinimumBloodVolumeInmL);
        }

        public string GenerateTransudationSlope()
        {
            if (_transudationSlope != null) return _transudationSlope;

            if (IsTotalBloodVolumeInRange())
            {
                _transudationSlope = _bvaTest.TransudationRateResult > 0 ? Math.Round(_bvaTest.TransudationRateResult * 100, 2, MidpointRounding.AwayFromZero).ToString("#0.00") + " %/min" :
                    _bvaTest.TransudationRateResult > -0.0005 ? Math.Round(_bvaTest.TransudationRateResult * 100, 2, MidpointRounding.AwayFromZero).ToString("#0.00") + " %/min*" : "*";
            }
            else
            {
                _transudationSlope = "**";
            }

            return _transudationSlope;
        }

        public string DetermineWatermarkText()
        {
            switch (_bvaTest.Mode)
            {
                case TestMode.Manual:
                    return "MANUAL\nTEST";
                case TestMode.VOPS:
                    return "VOPS\nTEST";
                case TestMode.TrainingHighSlopeTest:
                case TestMode.TrainingHighStd:
                case TestMode.Training:
                case TestMode.TrainingReverseSlope:
                    return "Training Test\nCounts Are Simulated";
                default:
                    switch (_bvaTest.Status)
                    {
                        case TestStatus.Aborted:
                            return "ABORTED";
                        case TestStatus.Pending:
                        case TestStatus.Running:
                        case TestStatus.Undefined:
                        case TestStatus.Deleted:
                            return "PRELIMINARY";
                        default:
                            return "";
                    }
            }
        }

        public bool IsTrainingTest
        {
            get
            {
                switch (_bvaTest.Mode)
                {
                    case TestMode.TrainingHighSlopeTest:
                    case TestMode.TrainingHighStd:
                    case TestMode.Training:
                    case TestMode.TrainingReverseSlope:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public double StandardDeviationAsRoundedmL()
        {
            return Math.Round((_bvaTest.StandardDevResult * _bvaTest.Volumes[BloodType.Measured].WholeCount), 1, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Creates and populates the TechniciansReport element on the xml report based on the BV test associated with
        /// the report controller.
        /// </summary>
        /// <returns>A refference to the TechniciansReport element.</returns>
        public XElement GenerateAnalystReportElement()
        {
            var xElem = new XElement("AnalystsData");
            xElem.Add(new XElement("Dose", new XAttribute("Units", "μCi"), _bvaTest.InjectateDose.ToString(CultureInfo.InvariantCulture)));
            xElem.Add(new XElement("Duration", new XAttribute("Units", "Seconds"), _bvaTest.SampleDurationInSeconds.ToString(CultureInfo.InvariantCulture)));
            xElem.Add(new XElement("RoomBackgroundCount", new XAttribute("Units", "Counts"), _bvaTest.BackgroundCount != -1 ?
                (_bvaTest.Mode == TestMode.Manual ? _bvaTest.BackgroundCount.ToString(CultureInfo.InvariantCulture) :
                Math.Round((_bvaTest.BackgroundCount / _bvaTest.BackgroundTimeInSeconds) * _bvaTest.SampleDurationInSeconds, 0, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture)) :
                "-"));
            xElem.Add(new XElement("StandardDeviation", new XAttribute("Units", "%"), double.IsNaN(_bvaTest.StandardDevResult) ? "0" :
                (IsTotalBloodVolumeInRange() ? StandardDeviationAsARoundedPercent(_bvaTest.StandardDevResult).ToString(CultureInfo.InvariantCulture) : "**")));
            xElem.Add(new XElement("StandardDeviation", new XAttribute("Units", "mL"), double.IsNaN(_bvaTest.StandardDevResult) ? "**" :
                (IsTotalBloodVolumeInRange() ? StandardDeviationAsRoundedmL().ToString(CultureInfo.InvariantCulture) : "**")));
            xElem.Add(new XElement("TestMode", _bvaTest.Mode.ToString()));
            xElem.Add(new XElement("Tube", _bvaTest.Tube.Name));

            return xElem;
        }

        /// <summary>
        /// Creates and populates the PatientSampleCounts element on the xml report based on the BV test associated with
        /// the report controller.
        /// </summary>
        /// <returns>A refference to the PatientSampleCounts element.</returns>
        public XElement GeneratePatientSampleCountsElement()
        {
            var xElem = new XElement("PatientSampleCounts");

            var fields = from s in _bvaTest.CompositeSamples
                         orderby s.SampleType
                         select new PatientSampleCount
                         {
                             // ReSharper disable CompareOfFloatsByEqualityOperator
                             SampleName = s.SampleType.ToString(),
                             SampleTime = s.PostInjectionTimeInSeconds == 0 || s.PostInjectionTimeInSeconds == -1 ? "-" : ReportUtilities.SecondsToMinutesAndSeconds(s.PostInjectionTimeInSeconds),
                             SampleHctA = s.SampleA.Hematocrit == 0 || s.SampleA.Hematocrit == -1 ? "-" : s.SampleA.Hematocrit.ToString(CultureInfo.InvariantCulture),
                             SampleHctAExcluded = s.SampleA.IsHematocritExcluded.ToString(),
                             SampleHctB = s.SampleB.Hematocrit == 0 || s.SampleB.Hematocrit == -1 ? "-" : s.SampleB.Hematocrit.ToString(CultureInfo.InvariantCulture),
                             SampleHctBExcluded = s.SampleB.IsHematocritExcluded.ToString(),
                             SampleAvgHct = AverageHematocrits(s.SampleA.Hematocrit, s.SampleB.Hematocrit) == 0 || AverageHematocrits(s.SampleA.Hematocrit, s.SampleB.Hematocrit) == -1 ? "-" : AverageHematocrits(s.SampleA.Hematocrit, s.SampleB.Hematocrit).ToString(CultureInfo.InvariantCulture),
                             SampleCountA = s.SampleA.Counts == 0 || s.SampleA.Counts == -1 ? "-" : s.SampleA.Counts.ToString(CultureInfo.InvariantCulture),
                             SampleCountAExcluded = s.SampleA.IsCountExcluded.ToString(),
                             SampleCountAAutoExcluded = s.SampleA.IsCountAutoExcluded.ToString(),
                             SampleCountB = s.SampleB.Counts == 0 || s.SampleB.Counts == -1 ? "-" : s.SampleB.Counts.ToString(CultureInfo.InvariantCulture),
                             SampleCountBExcluded = s.SampleB.IsCountExcluded.ToString(),
                             SampleCountBAutoExcluded = s.SampleB.IsCountAutoExcluded.ToString(),
                             SampleAvgCount = AverageSampleCounts(s.SampleA.Counts, s.SampleB.Counts) == -1 ? "-" : AverageSampleCounts(s.SampleA.Counts, s.SampleB.Counts).ToString(CultureInfo.InvariantCulture),
                             SampleUnadjVolume = s.UnadjustedBloodVolume == -1 ? "-" : s.UnadjustedBloodVolume.ToString(CultureInfo.InvariantCulture),
                             Excluded = s.IsWholeSampleExcluded
                             // ReSharper restore CompareOfFloatsByEqualityOperator
                         };
            foreach (var row in fields)
            {
                xElem.Add(new XElement(row.SampleName,
                new XElement("Time", row.SampleTime),
                new XElement("HctA", new XAttribute("Units", "%"), new XAttribute("Excluded", row.SampleHctAExcluded), row.SampleHctA),
                new XElement("HctB", new XAttribute("Units", "%"), new XAttribute("Excluded", row.SampleHctBExcluded), row.SampleHctB),
                new XElement("AvgHct", new XAttribute("Units", "%"), row.SampleAvgHct),
                new XElement("CountA", new XAttribute("Excluded", row.SampleCountAExcluded), new XAttribute("AutoExcluded", row.SampleCountAAutoExcluded), row.SampleCountA),
                new XElement("CountB", new XAttribute("Excluded", row.SampleCountBExcluded), new XAttribute("AutoExcluded", row.SampleCountBAutoExcluded), row.SampleCountB),
                new XElement("AvgCount", row.SampleAvgCount),
                new XElement("UnadjVolume", new XAttribute("Units", "mL"), row.SampleUnadjVolume),
                new XElement("Excluded", row.Excluded)
                ));
            }

            return xElem;
        }

        public void MakeRegressionGraph(Chart regressionChart)
        {
            var bloodVolumePointSeries = CreateBloodVolumePointSeries();
            var timeZeroPointSeries = CreateTimeZeroPointSeries();
            var usableCompositeSamples = GetUsableCompositeSamples().ToArray();
            var lastPoint = GetLastUsableBvaCompositeSample(usableCompositeSamples);
            var sampleWithLowestVolume = GetSampleWithLowestVolume(usableCompositeSamples);
            var sampleWithHighestVolume = GetSampleWithHighestVolume(usableCompositeSamples);

            var maxXValue = Constants.ChartPaddingInSeconds;
            if (lastPoint != null)
                maxXValue += ReportUtilities.SecondsToMinutes(lastPoint.PostInjectionTimeInSeconds);

            var yValueOfLastPointOnRegressionLine = ComputeYValueOfLastPointOnRegressionLine(maxXValue);
            var regressionLineSeries = CreateRegressionLineSeries(yValueOfLastPointOnRegressionLine, maxXValue);

            FormatRegressionChart(regressionChart, sampleWithHighestVolume, yValueOfLastPointOnRegressionLine, sampleWithLowestVolume);

            if (_bvaTest.Volumes[BloodType.Measured].WholeCount > 0)
                regressionChart.Series.AddRange(new[] { timeZeroPointSeries, regressionLineSeries, bloodVolumePointSeries });
        }

        public string GenerateAnalystTestModeString()
        {
            return _bvaTest.Mode == TestMode.Manual ? CountsEnteredManuallyString : String.Empty;
        }

        public string GenerateBackgroundCountText()
        {
            if (_bvaTest.Mode == TestMode.Manual)
                return _bvaTest.BackgroundCount != -1 ? _bvaTest.BackgroundCount.ToString(CultureInfo.InvariantCulture) : "-";
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            return _bvaTest.BackgroundTimeInSeconds != 0 && _bvaTest.BackgroundCount != -1 ?
                Math.Round((_bvaTest.BackgroundCount / _bvaTest.BackgroundTimeInSeconds) * _bvaTest.SampleDurationInSeconds, 0, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture) : "-";
        }

        public string GenerateSampleAcquisitionTimeText()
        {
            if (_bvaTest.Mode == TestMode.Manual)
                return "N/A";

            return (_bvaTest.SampleDurationInSeconds / 60) + MinuteUnitsString + (_bvaTest.SampleDurationInSeconds % 60) + SecondsUnitString;
        }

        public string GenerateDoseText()
        {
            return _bvaTest.InjectateDose > 0 ? _bvaTest.InjectateDose + " " + _bvaTest.InjectateDoseUnits : "-";
        }

        public string GenerateTubeTypeText()
        {
            return _bvaTest.Tube.Name + @" (" + Math.Round(_bvaTest.Tube.AnticoagulantFactor, 2, MidpointRounding.AwayFromZero).ToString("#0.00") + @")";
        }

        public List<PatientSampleCount> GenerateSampleCountsFieldList()
        {
            var fields = from s in _bvaTest.CompositeSamples
                         orderby s.SampleType
                         select new PatientSampleCount
                         {
                             // ReSharper disable CompareOfFloatsByEqualityOperator
                             SampleAId = s.SampleA.InternalId,
                             SampleBId = s.SampleB.InternalId,
                             SampleName = s.SampleType.GetStringValue(),
                             SampleTime = s.PostInjectionTimeInSeconds == -1 ? (s.SampleType == SampleType.Standard || s.SampleType == SampleType.Baseline) ? string.Empty : "-" : ReportUtilities.SecondsToMinutesAndSeconds(s.PostInjectionTimeInSeconds),
                             SampleHctA = FormatHematocritString(s, SampleRange.A),
                             SampleHctB = FormatHematocritString(s, SampleRange.B),
                             SampleAvgHct = FormatAverageHematocritString(s),
                             SampleCountA = FormatSampleCountString(s, SampleRange.A),
                             SampleCountB = FormatSampleCountString(s, SampleRange.B),
                             SampleAvgCount = s.AverageCounts == 0 ? "-" : Math.Round(s.AverageCounts, 0, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture),
                             SampleUnadjVolume = s.UnadjustedBloodVolume == 0 || s.UnadjustedBloodVolume == -1 ? s.SampleType == SampleType.Standard || s.SampleType == SampleType.Baseline ? string.Empty : "**" : s.UnadjustedBloodVolume.ToString(CultureInfo.InvariantCulture),
                             ExcludedText = s.IsWholeSampleExcluded ? "Excluded" : string.Empty,
                             Excluded = s.IsWholeSampleExcluded
                             // ReSharper restore CompareOfFloatsByEqualityOperator
                         };

            var fieldList = fields.ToList();
            while (fieldList.Count() < 8)
            {
                // Add dummies to format the report
                fieldList.Add(new PatientSampleCount());
            }

            return fieldList;
        }

        public string GenerateLowStandardsWarningText()
        {
            var x = from s in _bvaTest.CompositeSamples
                    where s.SampleType == SampleType.Standard
                    && (s.SampleA.Counts < _settingsManager.GetSetting<int>(SettingKeys.BvaMinimumStandardCpm) ||
                        s.SampleB.Counts < _settingsManager.GetSetting<int>(SettingKeys.BvaMinimumStandardCpm))
                    select s;

            var y = x.FirstOrDefault();
            if (y != null)
            {
                if ((y.SampleA.Counts > -1 && y.SampleA.Counts < _settingsManager.GetSetting<int>(SettingKeys.BvaMinimumStandardCpm) &&
                    (y.SampleB.Counts > _settingsManager.GetSetting<int>(SettingKeys.BvaMinimumStandardCpm) || y.SampleB.Counts == -1) ||
                    (y.SampleB.Counts > -1 && y.SampleB.Counts < _settingsManager.GetSetting<int>(SettingKeys.BvaMinimumStandardCpm) &&
                    (y.SampleA.Counts > _settingsManager.GetSetting<int>(SettingKeys.BvaMinimumStandardCpm) || y.SampleA.Counts == -1))))
                {
                     return OneLowStandardString;
                }
                
                if (y.SampleA.Counts > -1 && y.SampleA.Counts < _settingsManager.GetSetting<int>(SettingKeys.BvaMinimumStandardCpm) &&
                    y.SampleB.Counts > -1 && y.SampleB.Counts < _settingsManager.GetSetting<int>(SettingKeys.BvaMinimumStandardCpm))
                {
                    return TwoLowStandardsString;
                }
            }

            return String.Empty;
        }

        #endregion

        #region Private Helper Methods
        
        private string GetStandardDeviationText(double standardDeviationResult)
        {
            var standardDeviationText = new StringBuilder();

            standardDeviationText.Append(StandardDeviationAsARoundedPercent(standardDeviationResult).ToString("#0.000") + "%");
            standardDeviationText.Append(" or ");
            standardDeviationText.Append(StandardDeviationAsRoundedmL().ToString("#0.0"));
            standardDeviationText.Append(" mL");

            return standardDeviationText.ToString();
        }

        private static double StandardDeviationAsARoundedPercent(double standardDeviationResult)
        {
            var standardDeviationPercent = standardDeviationResult * 100;
            return Math.Round(standardDeviationPercent, 3, MidpointRounding.AwayFromZero);
        }

        private string GenerateMeasuredWholeBloodVolumeText()
        {
            if (_measuredWholeBloodVolume != null) return _measuredWholeBloodVolume;

                if (Math.Abs(_bvaTest.Volumes[BloodType.Measured].WholeCount) < 0)
                    _measuredWholeBloodVolume = "**";
                else if (_bvaTest.Volumes[BloodType.Measured].WholeCount < BvaDomainConstants.MinimumBloodVolumeInmL)
                    _measuredWholeBloodVolume = "<" + BvaDomainConstants.MinimumBloodVolumeInmL;
                else if (_bvaTest.Volumes[BloodType.Measured].WholeCount > BvaDomainConstants.MaximumBloodVolumeInmL)
                    _measuredWholeBloodVolume = ">" + BvaDomainConstants.MaximumBloodVolumeInmL;
                else
                    _measuredWholeBloodVolume = _bvaTest.Volumes[BloodType.Measured].WholeCount.ToString(CultureInfo.InvariantCulture);

                return _measuredWholeBloodVolume;
        }

        private static double AverageHematocrits(double sampleAhct, double sampleBhct)
        {
            // ReSharper disable CompareOfFloatsByEqualityOperator
            if (sampleAhct == -1 && sampleBhct == -1)
                return -1;

            if (sampleAhct == -1 && sampleBhct != -1)
                return sampleBhct;

            if (sampleAhct != -1 && sampleBhct == -1)
                return sampleAhct;

            return Math.Round((sampleAhct + sampleBhct) / 2, 1, MidpointRounding.AwayFromZero);
            // ReSharper restore CompareOfFloatsByEqualityOperator
        }

        private static int AverageSampleCounts(int sampleACounts, int sampleBCounts)
        {
            if (sampleACounts == -1 && sampleBCounts == -1)
                return -1;

            if (sampleACounts == -1 && sampleBCounts != -1)
                return sampleBCounts;

            if (sampleACounts != -1 && sampleBCounts == -1)
                return sampleACounts;

            return (int)Math.Round(((double)(sampleACounts + sampleBCounts) / 2), 0, MidpointRounding.AwayFromZero);
        }

        private ChartSeries CreateBloodVolumePointSeries()
        {
            var bloodVolumePointSeries = new ChartSeries { Type = ChartSeriesType.Point, DefaultLabelValue = string.Empty };

            foreach (var sample in _bvaTest.CompositeSamples)
            {
                if (sample.UnadjustedBloodVolume <= 0) continue;

                ChartSeriesItem pointItem;

                if (sample.IsWholeSampleExcluded)
                {
                    pointItem = new ChartSeriesItem(sample.UnadjustedBloodVolume, string.Empty, System.Drawing.Color.White);
                    pointItem.Appearance.FillStyle.FillType = FillType.Image;
                    pointItem.Appearance.FillStyle.FillSettings.BackgroundImage =
                        @"C:\ProgramData\Daxor\Lab\Images\diagonalImage.jpg";
                    pointItem.Appearance.Border.Color = System.Drawing.Color.Black;
                }
                else
                {
                    pointItem = new ChartSeriesItem(sample.UnadjustedBloodVolume, /*, sample.UnadjustedBloodVolume.ToString()*/
                                                    string.Empty, System.Drawing.Color.White);
                    pointItem.Appearance.FillStyle.FillType = FillType.Solid;
                    pointItem.Appearance.Border.Color = System.Drawing.Color.Black;
                }

                pointItem.XValue = ReportUtilities.SecondsToMinutes(sample.PostInjectionTimeInSeconds);
                bloodVolumePointSeries.AddItem(pointItem);
            }
            return bloodVolumePointSeries;
        }

        private ChartSeries CreateTimeZeroPointSeries()
        {
            var timeZeroPointSeries = new ChartSeries { Type = ChartSeriesType.Point, DefaultLabelValue = string.Empty };
            timeZeroPointSeries.Appearance.TextAppearance.TextProperties.Font = new System.Drawing.Font("Times New Roman", 6.0f);

            var timeZeroPoint = new ChartSeriesItem(_bvaTest.Volumes[BloodType.Measured].WholeCount, string.Empty,
                                                    System.Drawing.Color.White);
            timeZeroPoint.Appearance.FillStyle.FillType = FillType.Image;
            timeZeroPoint.Appearance.FillStyle.FillSettings.BackgroundImage = @"C:\ProgramData\Daxor\Lab\Images\squareImage.jpg";
            timeZeroPoint.Appearance.Border.Color = System.Drawing.Color.White;


            timeZeroPointSeries.AddItem(timeZeroPoint);
            return timeZeroPointSeries;
        }

        private IEnumerable<BVACompositeSample> GetUsableCompositeSamples()
        {
            return from sample in _bvaTest.CompositeSamples
                   // ReSharper disable once CompareOfFloatsByEqualityOperator
                   where sample.UnadjustedBloodVolume != 0
                   select sample;
        }

        private BVACompositeSample GetLastUsableBvaCompositeSample(IEnumerable<BVACompositeSample> usableCompositeSamples)
        {
            var bvaCompositeSamples = usableCompositeSamples as BVACompositeSample[] ?? usableCompositeSamples.ToArray();
            return (from sample in bvaCompositeSamples
                    // ReSharper disable once CompareOfFloatsByEqualityOperator
                    where ReportUtilities.SecondsToMinutes(sample.PostInjectionTimeInSeconds) == bvaCompositeSamples.Max(point => ReportUtilities.SecondsToMinutes(point.PostInjectionTimeInSeconds))
                    select sample).FirstOrDefault();
        }

        private static BVACompositeSample GetSampleWithHighestVolume(IEnumerable<BVACompositeSample> usableCompositeSamples)
        {
            var bvaCompositeSamples = usableCompositeSamples as BVACompositeSample[] ?? usableCompositeSamples.ToArray();
            return (from sample in bvaCompositeSamples
                    // ReSharper disable once CompareOfFloatsByEqualityOperator
                    where sample.UnadjustedBloodVolume == bvaCompositeSamples.Max(point => point.UnadjustedBloodVolume)
                    select sample).FirstOrDefault();
        }

        private static BVACompositeSample GetSampleWithLowestVolume(IEnumerable<BVACompositeSample> usableCompositeSamples)
        {
            var bvaCompositeSamples = usableCompositeSamples as BVACompositeSample[] ?? usableCompositeSamples.ToArray();
            return (from sample in bvaCompositeSamples
                    // ReSharper disable once CompareOfFloatsByEqualityOperator
                    where sample.UnadjustedBloodVolume == bvaCompositeSamples.Min(point => point.UnadjustedBloodVolume)
                    select sample).FirstOrDefault();
        }

        private double ComputeYValueOfLastPointOnRegressionLine(double maxXValue)
        {
            return (Math.Exp((_bvaTest.TransudationRateResult * (maxXValue - Constants.ChartPaddingInSeconds)) +
                             Math.Log(_bvaTest.Volumes[BloodType.Measured].WholeCount)));
        }

        private ChartSeries CreateRegressionLineSeries(double yValueOfLastPointOnRegressionLine, double maxXValue)
        {
            var regressionLineSeries = new ChartSeries { Type = ChartSeriesType.Line };
            regressionLineSeries.Appearance.LineSeriesAppearance.Color = System.Drawing.Color.Black;

            var lowestPointOfRegressionLine = new ChartSeriesItem(_bvaTest.Volumes[BloodType.Measured].WholeCount,
                                                                  _bvaTest.Volumes[BloodType.Measured].WholeCount.ToString(
                                                                      CultureInfo.InvariantCulture), System.Drawing.Color.Blue);

            lowestPointOfRegressionLine.Label.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.Black;
            lowestPointOfRegressionLine.Label.TextBlock.Appearance.TextProperties.Font =
                new System.Drawing.Font("Times New Roman", 10.0f);

            regressionLineSeries.AddItem(lowestPointOfRegressionLine);

            var highestPointOfRegressionLine = new ChartSeriesItem(yValueOfLastPointOnRegressionLine, string.Empty);
            highestPointOfRegressionLine.Label.Appearance.Visible = false;
            highestPointOfRegressionLine.XValue = maxXValue;
            regressionLineSeries.AddItem(highestPointOfRegressionLine);
            return regressionLineSeries;
        }

        private void FormatRegressionChart(Chart regressionChart, BVACompositeSample sampleWithHighestVolume,
                                   double yValueOfLastPointOnRegressionLine, BVACompositeSample sampleWithLowestVolume)
        {
            regressionChart.PlotArea.YAxis.MaxValue =
                Math.Max(sampleWithHighestVolume.UnadjustedBloodVolume, yValueOfLastPointOnRegressionLine) + 200;
            regressionChart.PlotArea.YAxis.MinValue = Math.Min(sampleWithLowestVolume.UnadjustedBloodVolume - 400,
                                                               (_bvaTest.Volumes[BloodType.Measured].WholeCount - 400));
            regressionChart.PlotArea.YAxis.LabelStep =
                (int)Math.Ceiling((regressionChart.PlotArea.YAxis.MaxValue - regressionChart.PlotArea.YAxis.MinValue) / 20);
            regressionChart.PlotArea.YAxis.Appearance.TextAppearance.TextProperties.Font =
                new System.Drawing.Font("Times New Roman", 8.0f);
            regressionChart.PlotArea.XAxis.Appearance.TextAppearance.TextProperties.Font =
                new System.Drawing.Font("Times New Roman", 8.0f);

            regressionChart.Series.Clear();
        }

        private static string FormatSampleCountString(BVACompositeSample compositeSample, SampleRange sampleRange)
        {
            var bvaSample = sampleRange == SampleRange.A ? compositeSample.SampleA : compositeSample.SampleB;

            var countValue = bvaSample.Counts.ToString(CultureInfo.InvariantCulture);
            var leftMarker = string.Empty;
            var rightMarker = string.Empty;
            var dataOutOfRangeMarker = string.Empty;

            if (bvaSample.Counts == Constants.EmptyCount || bvaSample.IsCounting)
                return Constants.MissingValueMarker;

            if (compositeSample.SampleType == SampleType.Standard)
            {
                var isStandardCountAboveMinimum = StaticRuleEvaluators.StandardCountsAreNotLessThan(sampleRange == SampleRange.A, compositeSample,
                                                                      BvaDomainConstants.MinimumStandardCounts);
                var isStandardCountDifferenceAcceptable = StaticRuleEvaluators.StandardAandStandardBCountDifferenceIsNotGreaterThanAllowableDifference(
                                                                    compositeSample);
                if ((!isStandardCountAboveMinimum || !isStandardCountDifferenceAcceptable) && !bvaSample.IsCountExcluded)
                    dataOutOfRangeMarker = Constants.LowStandardCountMarker;
                if (bvaSample.IsCountExcluded)
                {
                    leftMarker = Constants.CountExcludedLeftMarker;
                    rightMarker = Constants.CountExcludedRightMarker;
                }
            }
            else
            {
                var otherSample = sampleRange == SampleRange.A ? compositeSample.SampleB : compositeSample.SampleA;
                var isBvaSampleExcludedOrEmpty = bvaSample.IsCountExcluded || bvaSample.IsCountAutoExcluded || bvaSample.Counts == Constants.EmptyCount;
                var isOtherBvaSampleExcludedOrEmpty = otherSample.IsCountExcluded || otherSample.IsCountAutoExcluded || otherSample.Counts == Constants.EmptyCount;
                var isCountDifferenceAcceptable = StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(compositeSample);
                var isCountFarEnoughFromBaseline = StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(sampleRange == SampleRange.A,
                                                                                       compositeSample, BvaDomainConstants.MinimumMultipleOfBaselineForSampleCounts);
                if ((!isCountDifferenceAcceptable || !isCountFarEnoughFromBaseline) && !isBvaSampleExcludedOrEmpty && !isOtherBvaSampleExcludedOrEmpty)
                    dataOutOfRangeMarker = Constants.BeyondAllowableCountDifferenceMarker;
                if (bvaSample.IsCountAutoExcluded && bvaSample.IsCountExcluded)
                {
                    leftMarker = Constants.CountAutoExcludedLeftMarker;
                    rightMarker = Constants.CountAutoExcludedRightMarker;
                }
                else if (bvaSample.IsCountExcluded)
                {
                    leftMarker = Constants.CountExcludedLeftMarker;
                    rightMarker = Constants.CountExcludedRightMarker;
                }
            }

            return string.Format("{0}{1}{2}{3}", leftMarker, countValue, rightMarker, dataOutOfRangeMarker);
        }

        private static string FormatHematocritString(BVACompositeSample compositeSample, SampleRange sampleRange)
        {
            var bvaSample = sampleRange == SampleRange.A ? compositeSample.SampleA : compositeSample.SampleB;

            if (bvaSample.Type == SampleType.Standard)
                return string.Empty;
            if (bvaSample.Hematocrit < 0)
                return Constants.MissingValueMarker;

            var dataOutOfRangeMarker = string.Empty;
            var leftMarker = string.Empty;
            var rightMarker = string.Empty;

            var hematocritValue = Math.Round(bvaSample.Hematocrit, 1, MidpointRounding.AwayFromZero).ToString("F1");
            var isHematocritDifferenceAcceptable = StaticRuleEvaluators.HematocritsDoNotDifferByMoreThanTwoPoints(sampleRange == SampleRange.A, compositeSample);
            var isHematocritDifferenceFromTestAverageAcceptable = StaticRuleEvaluators.HematocritsDoNotDifferFromTestAverageHematocritByMoreThan(
                    sampleRange == SampleRange.A, compositeSample, BvaDomainConstants.MaximumDifferenceFromAverageHematocrit);
            var isHematocritAboveMinimum = StaticRuleEvaluators.HematocritIsNotLessThan(sampleRange == SampleRange.A, compositeSample, BvaDomainConstants.MinimumHematocrit);
            var isHematocritBelowMaximum = StaticRuleEvaluators.HematocritIsNotGreaterThan(sampleRange == SampleRange.A, compositeSample, BvaDomainConstants.MaximumHematocrit);

            var isHematocritDataOutOfRange = !isHematocritDifferenceAcceptable ||
                                             !isHematocritDifferenceFromTestAverageAcceptable
                                             || !isHematocritAboveMinimum || !isHematocritBelowMaximum;
            if (isHematocritDataOutOfRange)
                dataOutOfRangeMarker = Constants.HematocritOutOfRangeMarker;
            else if (bvaSample.IsHematocritExcluded)
            {
                leftMarker = Constants.HematocritExcludedLeftMarker;
                rightMarker = Constants.HematocritExcludedRightMarker;
            }

            return string.Format("{0}{1}{2}{3}", leftMarker, hematocritValue, rightMarker, dataOutOfRangeMarker);
        }

        private static string FormatAverageHematocritString(BVACompositeSample compositeSample)
        {
            if (compositeSample.SampleType == SampleType.Standard)
                return string.Empty;
            return compositeSample.AverageHematocrit < 0 ? Constants.MissingValueMarker :
                Math.Round(compositeSample.AverageHematocrit, 1, MidpointRounding.AwayFromZero).ToString("F1");
        }

        #endregion
    }
}
