﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Interfaces;
using System;
using System.Globalization;
using System.Xml.Linq;
using Telerik.Reporting.Charting;
using Telerik.Reporting.Charting.Styles;
using Chart = Telerik.Reporting.Chart;

namespace Daxor.Lab.BVA.Reports.ReportModels
{
    public class BloodVolumeAnalysisSubReportQuickReportModel
    {
        #region Fields

        private readonly BVATest _bvaTest;
        private readonly IIdealsCalcEngineService _calcEngine;
        private string _measuredWholeBloodVolume;
        private string _measuredPlasmaVolume;
        private string _measuredRedCellVolume;
        private string _idealRedBloodCount;
        private string _idealPlasmaVolume;
        private string _idealWholeBloodCount;
        private string _totalExcessDeficit;
        private string _wholeBloodCondition;
        private string _redCellCondition;
        private string _rBcExcessDeficit;
        private string _plasmaCondition;
        private string _plasmaExcessDeficit;
        private string _totalDeviationFromIdeal;
        private string _rBcDeviationFromIdeal;
        private string _plasmaDeviationFromIdeal;
		private string _normalizedHct;
		private string _peripheralHct;
		private string _transudationRate;

        #endregion

        #region Ctor

		public BloodVolumeAnalysisSubReportQuickReportModel(BVATest test, IIdealsCalcEngineService calcEngine)
        {
            _bvaTest = test;
            _calcEngine = calcEngine;
            CalculateDeviations();
        }

        #endregion

        #region Properties

		internal BVATest Test { get { return _bvaTest; } }
        public IIdealsCalcEngineService CalcEngine { get { return _calcEngine; } }
        public double WholeBloodDeviation { get; set; }
        public double PlasmaDeviation { get; set; }
        public double RbcDeviation { get; set; }
        public double WholeBloodPercentDeviation { get; set; }
        public double PlasmaPercentDeviation { get; set; }
        public double RbcPercentDeviation { get; set; }

        #endregion

        #region Public API

        public string GenerateMeasuredPlasmaVolumeText()
        {
            var plasmaVolume = GenerateMeasuredPlasmaVolume();
            return plasmaVolume != "**" && plasmaVolume != "-" ? plasmaVolume + " mL" : plasmaVolume;
        }

        public string GenerateMeasuredRedCellVolumeText()
        {
            var measuredRedCellVolume = GenerateMeasuredRedCellVolume();
            return measuredRedCellVolume != "**" && measuredRedCellVolume != "-" ? measuredRedCellVolume + " mL" : measuredRedCellVolume;
        }

        public string GenerateMeasuredWholeBloodVolumeText()
        {
            var measuredWholeBloodVolume = GenerateMeasuredWholeBloodVolume();
            return measuredWholeBloodVolume != "**" && measuredWholeBloodVolume != "-" ? measuredWholeBloodVolume + " mL" : measuredWholeBloodVolume;
        }

        public string GenerateIdealPlasmaVolumeText()
        {
            var idealPlasmaVolume = GenerateIdealPlasmaVolume();
            return idealPlasmaVolume != "*" && idealPlasmaVolume != "**" ? idealPlasmaVolume + " mL" : idealPlasmaVolume;
        }

        public bool IsIdealPlasmaCountOutOfRange()
        {
            return Math.Abs(_bvaTest.Volumes[BloodType.Ideal].PlasmaCount) <= 0;
        }

        public string GenerateIdealRedBloodCellCountText()
        {
            var idealRedCellCount = GenerateIdealRedBloodCellCount();
            return idealRedCellCount != "*" && idealRedCellCount != "**" ? idealRedCellCount + " mL" : idealRedCellCount;
        }

        public bool IsIdealRedCellCountOutOfRange()
        {
            return Math.Abs(_bvaTest.Volumes[BloodType.Ideal].RedCellCount) <= 0;
        }

        public string GenerateIdealWholeBloodCountText()
        {
            var idealWholeBloodCount = GenerateIdealWholeBloodCount();
            return idealWholeBloodCount != "*" && idealWholeBloodCount != "**" ? idealWholeBloodCount + " mL" : idealWholeBloodCount;
        }

        public bool IsIdealWholeBloodCountOutOfRange()
        {
            return Math.Abs(_bvaTest.Volumes[BloodType.Ideal].WholeCount) <= 0;
        }

        public bool IsWeightDeviationInRange()
        {
            return (_bvaTest.Patient.DeviationFromIdealWeight >= BvaDomainConstants.MinimumWeightDeviation &&
                        _bvaTest.Patient.DeviationFromIdealWeight <= BvaDomainConstants.MaximumWeightDeviation);
        }

        public bool IsHeightInRange()
        {
            return (_bvaTest.Patient.HeightInCm <= CalcEngine.MaximumHeightInCm && _bvaTest.Patient.HeightInCm >= CalcEngine.MinimumHeightInCm);
        }

        public string GenerateTotalExessDeficitText()
        {
            var totalExcessDeficit = GenerateTotalExessDeficit();
            var wholeBloodCondition = GenerateWholeBloodCondition();

            return !totalExcessDeficit.StartsWith("*") ? totalExcessDeficit + "% " + (WholeBloodDeviation > 0 ? wholeBloodCondition + " Excess"
                : wholeBloodCondition + " Deficit") : totalExcessDeficit;
        }

        public bool IsTotalExcessDeficitOutOfRange()
        {
            var totalExcessDeficit = GenerateTotalExessDeficit();
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            return (WholeBloodPercentDeviation == 0 && _bvaTest.Volumes[BloodType.Ideal].WholeCount == 0) || totalExcessDeficit.StartsWith("*");
        }

        public string GenerateRbcExcessDeficitText()
        {
            var rbcExcessDeficit = GenerateRbcExcessDeficit();

            return !rbcExcessDeficit.StartsWith("*") ?
                rbcExcessDeficit + "% " + (RbcDeviation > 0 ? GenerateRedCellCondition() + " Excess" : GenerateRedCellCondition() + " Deficit") : rbcExcessDeficit;
        }

        public bool IsRbcExcessDeficitOutOfRange()
        {
            var rbcExcessDeficit = GenerateRbcExcessDeficit();
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            return (RbcPercentDeviation == 0 && _bvaTest.Volumes[BloodType.Ideal].RedCellCount == 0) || rbcExcessDeficit.StartsWith("*");
        }

        public string GeneratePlasmaExcessDeficitText()
        {
            var plasmaExcessDeficit = GeneratePlasmaExcessDeficit();

            return !plasmaExcessDeficit.StartsWith("*") ? 
                plasmaExcessDeficit + "% " + (PlasmaDeviation > 0 ? GeneratePlasmaCondition() + " Excess" : GeneratePlasmaCondition() + " Deficit") 
                :plasmaExcessDeficit;
        }

        public bool IsPlasmaExcessDeficitOutOfRange()
        {
            var plasmaExcessDeficit = GeneratePlasmaExcessDeficit();

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            return (PlasmaPercentDeviation == 0 && _bvaTest.Volumes[BloodType.Ideal].PlasmaCount == 0) || plasmaExcessDeficit.StartsWith("*");
        }

        public bool IsTotalBloodVolumeInRange()
        {
            return (_bvaTest.Volumes[BloodType.Measured].WholeCount < BvaDomainConstants.MaximumBloodVolumeInmL) &&
                                    (_bvaTest.Volumes[BloodType.Measured].WholeCount > BvaDomainConstants.MinimumBloodVolumeInmL);
        }

        public string GenerateTotalDeviationFromIdealText()
        {
            var totalDeviationFromIdeal = GenerateTotalDeviationFromIdeal();

            return totalDeviationFromIdeal == "**" ? totalDeviationFromIdeal : totalDeviationFromIdeal + " mL";
        }

        public string GenerateRbcDeviationFromIdealText()
        {
            var rbcDeviationFromIdeal = GenerateRbcDeviationFromIdeal();

            return rbcDeviationFromIdeal == "**" ? rbcDeviationFromIdeal : rbcDeviationFromIdeal + " mL";
        }

        public string GeneratePlasmaDeviationFromIdealText()
        {
            var plasmaDeviationFromIdeal = GeneratePlasmaDeviationFromIdeal();

            return plasmaDeviationFromIdeal == "**" ? plasmaDeviationFromIdeal : plasmaDeviationFromIdeal + " mL";
        }

        public bool AreBloodVolumesOutOfRange()
        {
            return _bvaTest.Volumes[BloodType.Ideal].WholeCount <= 0 || !IsTotalBloodVolumeInRange();
        }

        public XElement GenerateBloodVolumeAnalysisElement()
        {
            var xElem = new XElement("BloodVolumeAnalysis");

            xElem.Add(new XElement("TotalBloodVolume",
            new XElement("BVAResult", new XAttribute("Units", "mL"), GenerateMeasuredWholeBloodVolume()),
            new XElement("PatientIdeal", new XAttribute("Units", "mL"), GetIdealBvStringForReport("Blood")),
            new XElement("DeviationFromIdeal", new XAttribute("Units", "mL"), double.IsNaN(WholeBloodDeviation) ? "-" : GenerateTotalDeviationFromIdeal()),
            new XElement("ExcessDeficit", new XAttribute("Units", "%"), new XAttribute("ExcessDeficitCategory", GenerateWholeBloodCondition()
                + " " + (_bvaTest.Volumes[BloodType.Ideal].WholeCount > 0 ? (WholeBloodDeviation > 0 ? " Excess" : " Deficit") : "**")), GenerateTotalExessDeficit())));

            xElem.Add(new XElement("RedBloodCellVolume",
            new XElement("BVAResult", new XAttribute("Units", "mL"), GenerateMeasuredRedCellVolume()),
            new XElement("IdealVolume", new XAttribute("Units", "mL"), GetIdealBvStringForReport("Red")),
            new XElement("DeviationFromIdeal", new XAttribute("Units", "mL"), double.IsNaN(RbcDeviation) ? "-" : GenerateRbcDeviationFromIdeal()),
            new XElement("ExcessDeficit", new XAttribute("Units", "%"), new XAttribute("ExcessDeficitCategory", GenerateRedCellCondition()
                + " " + (_bvaTest.Volumes[BloodType.Ideal].RedCellCount > 0 ? (RbcDeviation > 0 ? " Excess" : " Deficit") : "**")), GenerateRbcExcessDeficit())));

            xElem.Add(new XElement("PlasmaVolume",
            new XElement("BVAResult", new XAttribute("Units", "mL"), GenerateMeasuredPlasmaVolume()),
            new XElement("IdealVolume", new XAttribute("Units", "mL"), GetIdealBvStringForReport("Plasma")),
            new XElement("DeviationFromIdeal", new XAttribute("Units", "mL"), double.IsNaN(PlasmaDeviation) ? "-" : GeneratePlasmaDeviationFromIdeal()),
            new XElement("ExcessDeficit", new XAttribute("Units", "%"), new XAttribute("ExcessDeficitCategory", GeneratePlasmaCondition()
                + " " + (_bvaTest.Volumes[BloodType.Ideal].PlasmaCount > 0 ? (PlasmaDeviation > 0 ? " Excess" : " Deficit") : "**")), GeneratePlasmaExcessDeficit())));

	        string correctionFactor;

	        if (Test.AmputeeIdealsCorrectionFactor == 0)
		        correctionFactor = "0";
			else if (Test.PatientIsAmputee && Test.AmputeeIdealsCorrectionFactor == -1)
				correctionFactor = "Unselected";
	        else
		        correctionFactor = "-" + Test.AmputeeIdealsCorrectionFactor;

			xElem.Add(new XElement("AmputeeIdealsCorrectionFactor", new XAttribute("Units", "%"), correctionFactor));

            xElem.Add(new XElement("BloodVolumeInterpretationGuidelines",
                    new XElement("BVPVDeviation", new XAttribute("Units", "%"),
                        new XElement("Normal", "0 to 8"), new XElement("Mild", ">8 to 16"), new XElement("Moderate", ">16 to 24"), new XElement("Severe", ">24 to 32"), new XElement("Extreme", ">32")),
                    new XElement("RCVDeviation", new XAttribute("Units", "%"),
                        new XElement("Normal", "0 to 10"), new XElement("Mild", ">10 to 20"), new XElement("Moderate", ">20 to 30"), new XElement("Severe", ">30 to 40"), new XElement("Extreme", ">40"))));

            return xElem;
        }

        public void MakeBloodBarsChart(Chart chartBloodBars)
        {
            chartBloodBars.PlotArea.YAxis.AxisMode = ChartYAxisMode.Extended;

            chartBloodBars.PlotArea.XAxis.AutoScale = false;
            chartBloodBars.PlotArea.XAxis.Items.Clear();
            var bvaLabel = new ChartAxisItem("BVA (mL)");
            ChartAxisItem idealLabel;

            if (IsWeightDeviationInRange() && IsHeightInRange())
            {
                idealLabel = new ChartAxisItem("Ideal (mL)");
            }
            else
            {
                idealLabel = new ChartAxisItem("*Ideal (mL)");
            }

            bvaLabel.TextBlock.Appearance.TextProperties.Font = new System.Drawing.Font("Times New Roman", 8.0f);
            idealLabel.TextBlock.Appearance.TextProperties.Font = new System.Drawing.Font("Times New Roman", 8.0f);
            chartBloodBars.PlotArea.XAxis.Items.Add(bvaLabel);
            chartBloodBars.PlotArea.XAxis.Items.Add(idealLabel);

            chartBloodBars.Appearance.BarWidthPercent = 25;
            chartBloodBars.PlotArea.XAxis.Appearance.MajorTick.Length = 0;

            chartBloodBars.Series.Clear();
            chartBloodBars.Series.Add(new ChartSeries { Name = "BVA" });
            chartBloodBars.Series.Add(new ChartSeries { Name = "Ideal" });
            chartBloodBars.Series.Add(new ChartSeries { Name = "Total" });

            chartBloodBars.PlotArea.YAxis.Visible = ChartAxisVisibility.False;

            var redCellsMeasured = _bvaTest.Volumes[BloodType.Measured].RedCellCount;
            var redCellsIdeal = _bvaTest.Volumes[BloodType.Ideal].RedCellCount;
            var plasmaMeasured = _bvaTest.Volumes[BloodType.Measured].PlasmaCount;
            var plasmaIdeal = _bvaTest.Volumes[BloodType.Ideal].PlasmaCount;


            //  It doesn't matter if the values are 0 or not.  You *have* to have the 0 columns if the chart is to work properly.  If you don't 
            //  have the 0 column, the graph will be off
            //  *PAndreason 2/19/2013 - Not sure what the above means . . . not adding the columns in the weight deviation out of range case works for me.
            chartBloodBars.Series[0].Items.Add(new ChartSeriesItem(redCellsMeasured, redCellsMeasured.ToString(CultureInfo.InvariantCulture)));

            chartBloodBars.Series[1].Items.Add(new ChartSeriesItem(plasmaMeasured, plasmaMeasured.ToString(CultureInfo.InvariantCulture)));

            chartBloodBars.Series[2].Items.Add(new ChartSeriesItem(0, (redCellsMeasured + plasmaMeasured).ToString(CultureInfo.InvariantCulture)));
            if (IsWeightDeviationInRange() && IsHeightInRange())
            {
                chartBloodBars.Series[2].Items.Add(new ChartSeriesItem(0, (redCellsIdeal + plasmaIdeal).ToString(CultureInfo.InvariantCulture)));
                chartBloodBars.Series[1].Items.Add(new ChartSeriesItem(plasmaIdeal, plasmaIdeal.ToString(CultureInfo.InvariantCulture)));
                chartBloodBars.Series[0].Items.Add(new ChartSeriesItem(redCellsIdeal, redCellsIdeal.ToString(CultureInfo.InvariantCulture)));
            }

            chartBloodBars.DefaultType = ChartSeriesType.StackedBar;

            chartBloodBars.Series[0].Appearance.LabelAppearance.LabelLocation = StyleSeriesItemLabel.ItemLabelLocation.Outside;
            chartBloodBars.Series[0].Appearance.LabelAppearance.Position.AlignedPosition = AlignedPositions.Right;
            chartBloodBars.Series[0].Appearance.TextAppearance.TextProperties.Color = System.Drawing.Color.Black;
            chartBloodBars.Series[0].Appearance.TextAppearance.TextProperties.Font = new System.Drawing.Font("Times New Roman", 8.0f);
            chartBloodBars.Series[0].Appearance.BarWidthPercent = 25;
            chartBloodBars.Series[0].Appearance.Border.Color = System.Drawing.Color.Black;

            chartBloodBars.Series[1].Appearance.LabelAppearance.LabelLocation = StyleSeriesItemLabel.ItemLabelLocation.Outside;
            chartBloodBars.Series[1].Appearance.LabelAppearance.Position.AlignedPosition = AlignedPositions.Right;
            chartBloodBars.Series[1].Appearance.TextAppearance.TextProperties.Color = System.Drawing.Color.Black;
            chartBloodBars.Series[1].Appearance.TextAppearance.TextProperties.Font = new System.Drawing.Font("Times New Roman", 8.0f);
            chartBloodBars.Series[1].Appearance.BarWidthPercent = 25;
            chartBloodBars.Series[1].Appearance.Border.Color = System.Drawing.Color.Black;

            chartBloodBars.Series[2].Appearance.LabelAppearance.LabelLocation = StyleSeriesItemLabel.ItemLabelLocation.Outside;
            chartBloodBars.Series[2].Appearance.TextAppearance.TextProperties.Font = new System.Drawing.Font("Times New Roman", 8.0f);
            chartBloodBars.Series[2].Appearance.TextAppearance.Dimensions.Margins = "0, 0, 0, 0";
            chartBloodBars.Series[2].Appearance.TextAppearance.Dimensions.Paddings = "0, 0, 0, 0";
            chartBloodBars.Series[2].Appearance.TextAppearance.TextProperties.Color = System.Drawing.Color.Black;
        }

        #endregion

        #region Private helper methods

        private string GenerateMeasuredPlasmaVolume()
        {
            if (_measuredPlasmaVolume != null) return _measuredPlasmaVolume;

            if (Math.Abs(_bvaTest.Volumes[BloodType.Measured].PlasmaCount) < 0)
                _measuredPlasmaVolume = "**";
            else if (_bvaTest.Volumes[BloodType.Measured].WholeCount < BvaDomainConstants.MinimumBloodVolumeInmL ||
                     _bvaTest.Volumes[BloodType.Measured].WholeCount > BvaDomainConstants.MaximumBloodVolumeInmL)
                _measuredPlasmaVolume = "-";
            else
                _measuredPlasmaVolume = _bvaTest.Volumes[BloodType.Measured].PlasmaCount.ToString(CultureInfo.InvariantCulture);

            return _measuredPlasmaVolume;
        }

        private string GenerateMeasuredRedCellVolume()
        {
            if (_measuredRedCellVolume != null) return _measuredRedCellVolume;

            if (Math.Abs(_bvaTest.Volumes[BloodType.Measured].RedCellCount) < 0)
                _measuredRedCellVolume = "**";
            else if (_bvaTest.Volumes[BloodType.Measured].WholeCount < BvaDomainConstants.MinimumBloodVolumeInmL ||
                     _bvaTest.Volumes[BloodType.Measured].WholeCount > BvaDomainConstants.MaximumBloodVolumeInmL)
                _measuredRedCellVolume = "-";
            else
                _measuredRedCellVolume = _bvaTest.Volumes[BloodType.Measured].RedCellCount.ToString(CultureInfo.InvariantCulture);

            return _measuredRedCellVolume;
        }

        private string GenerateMeasuredWholeBloodVolume()
        {
            if (_measuredWholeBloodVolume != null) return _measuredWholeBloodVolume;

            if (Math.Abs(_bvaTest.Volumes[BloodType.Measured].WholeCount) < 0)
                _measuredWholeBloodVolume = "**";
            else if (_bvaTest.Volumes[BloodType.Measured].WholeCount < BvaDomainConstants.MinimumBloodVolumeInmL)
                _measuredWholeBloodVolume = "<" + BvaDomainConstants.MinimumBloodVolumeInmL;
            else if (_bvaTest.Volumes[BloodType.Measured].WholeCount > BvaDomainConstants.MaximumBloodVolumeInmL)
                _measuredWholeBloodVolume = ">" + BvaDomainConstants.MaximumBloodVolumeInmL;
            else
                _measuredWholeBloodVolume = _bvaTest.Volumes[BloodType.Measured].WholeCount.ToString(CultureInfo.InvariantCulture);

            return _measuredWholeBloodVolume;
        }

        private string GenerateIdealPlasmaVolume()
        {
            if (_idealPlasmaVolume != null) return _idealPlasmaVolume;

            if (IsWeightDeviationInHighRange() && Math.Abs(_bvaTest.Volumes[BloodType.Ideal].PlasmaCount) > 0)
                _idealPlasmaVolume = "*" + _bvaTest.Volumes[BloodType.Ideal].PlasmaCount;
            else if (Math.Abs(_bvaTest.Volumes[BloodType.Ideal].PlasmaCount) > 0)
                _idealPlasmaVolume = _bvaTest.Volumes[BloodType.Ideal].PlasmaCount.ToString(CultureInfo.InvariantCulture);
            else
                _idealPlasmaVolume = "**";

            return _idealPlasmaVolume;
        }

        private bool IsWeightDeviationInHighRange()
        {
            return _bvaTest.Patient.DeviationFromIdealWeight <= BvaDomainConstants.MaximumWeightDeviation &&
                    _bvaTest.Patient.DeviationFromIdealWeight > BvaDomainConstants.HighWeightDeviationThreshold;
        }

        private string GenerateIdealRedBloodCellCount()
        {
            if (_idealRedBloodCount != null) return _idealRedBloodCount;

            if (IsWeightDeviationInHighRange() && Math.Abs(_bvaTest.Volumes[BloodType.Ideal].RedCellCount) > 0)
                _idealRedBloodCount = "*" + _bvaTest.Volumes[BloodType.Ideal].RedCellCount;
            else if (Math.Abs(_bvaTest.Volumes[BloodType.Ideal].RedCellCount) > 0)
                _idealRedBloodCount = _bvaTest.Volumes[BloodType.Ideal].RedCellCount.ToString(CultureInfo.InvariantCulture);
            else
                _idealRedBloodCount = "**";

            return _idealRedBloodCount;
        }

        private string GenerateIdealWholeBloodCount()
        {
            if (_idealWholeBloodCount != null) return _idealWholeBloodCount;

            if (IsWeightDeviationInHighRange() && Math.Abs(_bvaTest.Volumes[BloodType.Ideal].WholeCount) > 0)
                _idealWholeBloodCount = "*" + _bvaTest.Volumes[BloodType.Ideal].WholeCount;
            else if (Math.Abs(_bvaTest.Volumes[BloodType.Ideal].WholeCount) > 0)
                _idealWholeBloodCount = _bvaTest.Volumes[BloodType.Ideal].WholeCount.ToString(CultureInfo.InvariantCulture);
            else
                _idealWholeBloodCount = "**";

            return _idealWholeBloodCount;
        }

        public string GenerateTotalExessDeficit()
        {
            if (_totalExcessDeficit != null) return _totalExcessDeficit;

            // ReSharper disable CompareOfFloatsByEqualityOperator
            if (WholeBloodPercentDeviation == 0 && _bvaTest.Volumes[BloodType.Ideal].WholeCount == 0)
            {
                _totalExcessDeficit = "**";
            }
            else
            {
                if (Math.Abs(WholeBloodPercentDeviation) != 100)
                {
                    if (IsTotalBloodVolumeInRange())
                    {
                        if (WholeBloodPercentDeviation > 0)
                            _totalExcessDeficit = "+" + Math.Round(WholeBloodPercentDeviation, 1, MidpointRounding.AwayFromZero).ToString("#0.0");
                        else
                            _totalExcessDeficit = Math.Round(WholeBloodPercentDeviation, 1, MidpointRounding.AwayFromZero).ToString("#0.0");
                    }
                    else
                    {
                        _totalExcessDeficit = "** **";
                    }
                }
                else
                {
                    _totalExcessDeficit = "*";
                }
            }
            // ReSharper restore CompareOfFloatsByEqualityOperator

            return _totalExcessDeficit;
        }

        private void CalculateDeviations()
        {
            WholeBloodDeviation = _bvaTest.Volumes[BloodType.Measured].WholeCount - _bvaTest.Volumes[BloodType.Ideal].WholeCount;
            if (_bvaTest.Volumes[BloodType.Ideal].WholeCount != 0)
            {
                WholeBloodPercentDeviation = (WholeBloodDeviation / _bvaTest.Volumes[BloodType.Ideal].WholeCount) * 100;
            }

            PlasmaDeviation = _bvaTest.Volumes[BloodType.Measured].PlasmaCount - _bvaTest.Volumes[BloodType.Ideal].PlasmaCount;
            if (_bvaTest.Volumes[BloodType.Ideal].PlasmaCount != 0)
            {
                PlasmaPercentDeviation = (PlasmaDeviation / _bvaTest.Volumes[BloodType.Ideal].PlasmaCount) * 100;
            }

            RbcDeviation = _bvaTest.Volumes[BloodType.Measured].RedCellCount - _bvaTest.Volumes[BloodType.Ideal].RedCellCount;
            if (_bvaTest.Volumes[BloodType.Ideal].RedCellCount != 0)
            {
                RbcPercentDeviation = (RbcDeviation / _bvaTest.Volumes[BloodType.Ideal].RedCellCount) * 100;
            }
        }

        private string GenerateWholeBloodCondition()
        {
            return _wholeBloodCondition ?? (_wholeBloodCondition = GetCondition(WholeBloodPercentDeviation.ToString(CultureInfo.InvariantCulture), "Blood"));
        }

        private string GenerateRedCellCondition()
        {
            return _redCellCondition ?? (_redCellCondition = GetCondition(RbcPercentDeviation.ToString(CultureInfo.InvariantCulture), "Red"));
        }

        private string GeneratePlasmaCondition()
        {
            return _plasmaCondition ?? (_plasmaCondition = GetCondition(PlasmaPercentDeviation.ToString(CultureInfo.InvariantCulture), "Plasma"));
        }

        private static string GetCondition(string deviationString, string bloodPart)
        {
            // if no deviation (**), return **
            if (deviationString == "-     ")
            {
                return "**";
            }

            var conditionArray = new[] { "Normal", "Mild", "Moderate", "Severe", "Extreme", "Extreme" };

            var deviationIndex = 0;
            var roundedDeviation = Math.Abs(Math.Round(double.Parse(deviationString), 1, MidpointRounding.AwayFromZero));

            switch (bloodPart)
            {
                case "Blood":
                case "Plasma":
                    if (roundedDeviation <= 8)
                    {
                        deviationIndex = 0; // Normal
                    }
                    else if (roundedDeviation <= 16)
                    {
                        deviationIndex = 1; // Mild
                    }
                    else if (roundedDeviation <= 24)
                    {
                        deviationIndex = 2; // Moderate
                    }
                    else if (roundedDeviation <= 32)
                    {
                        deviationIndex = 3; // Severe
                    }
                    else
                    {
                        deviationIndex = 4; // Extreme
                    }

                    break;
                case "Red":
                    if (roundedDeviation <= 10)
                    {
                        deviationIndex = 0; // Normal
                    }
                    else if (roundedDeviation <= 20)
                    {
                        deviationIndex = 1; // Mild
                    }
                    else if (roundedDeviation <= 30)
                    {
                        deviationIndex = 2; // Moderate
                    }
                    else if (roundedDeviation <= 40)
                    {
                        deviationIndex = 3; // Severe
                    }
                    else // > 40
                    {
                        deviationIndex = 4; // Extreme
                    }

                    break;
            }

            if (deviationIndex > 5)
            {
                deviationIndex = 5;
            }

            return conditionArray[deviationIndex];
        }

        public string GenerateRbcExcessDeficit()
        {
            if (_rBcExcessDeficit != null) return _rBcExcessDeficit;

            // ReSharper disable CompareOfFloatsByEqualityOperator
            if (RbcPercentDeviation == 0 && _bvaTest.Volumes[BloodType.Ideal].RedCellCount == 0)
            {
                _rBcExcessDeficit = "**";
            }
            else
            {
                if (Math.Abs(RbcPercentDeviation) != 100)
                {
                    if (IsTotalBloodVolumeInRange())
                    {
                        if (RbcPercentDeviation > 0)
                            _rBcExcessDeficit = "+" + Math.Round(RbcPercentDeviation, 1, MidpointRounding.AwayFromZero).ToString("#0.0");
                        else
                            _rBcExcessDeficit = Math.Round(RbcPercentDeviation, 1, MidpointRounding.AwayFromZero).ToString("#0.0");
                    }
                    else
                    {
                        _rBcExcessDeficit = "** **";
                    }
                }
                else
                {
                    _rBcExcessDeficit = "*";
                }
            }
            // ReSharper restore CompareOfFloatsByEqualityOperator

            return _rBcExcessDeficit;
        }

        public string GeneratePlasmaExcessDeficit()
        {
            if (_plasmaExcessDeficit != null) return _plasmaExcessDeficit;

            // ReSharper disable CompareOfFloatsByEqualityOperator
            if (PlasmaPercentDeviation == 0 && _bvaTest.Volumes[BloodType.Ideal].PlasmaCount == 0)
            {
                _plasmaExcessDeficit = "**";
            }
            else
            {
                if (Math.Abs(PlasmaPercentDeviation) != 100)
                {
                    if (IsTotalBloodVolumeInRange())
                    {
                        if (PlasmaPercentDeviation > 0)
                            _plasmaExcessDeficit = "+" + Math.Round(PlasmaPercentDeviation, 1, MidpointRounding.AwayFromZero).ToString("#0.0");
                        else
                            _plasmaExcessDeficit = Math.Round(PlasmaPercentDeviation, 1, MidpointRounding.AwayFromZero).ToString("#0.0");
                    }
                    else
                    {
                        _plasmaExcessDeficit = "** **";
                    }
                }
                else
                {
                    _plasmaExcessDeficit = "*";
                }
            }
            // ReSharper restore CompareOfFloatsByEqualityOperator

            return _plasmaExcessDeficit;
        }

        private string GenerateTotalDeviationFromIdeal()
        {
            if (_totalDeviationFromIdeal != null) return _totalDeviationFromIdeal;

            if (_bvaTest.Volumes[BloodType.Ideal].WholeCount > 0)
            {
                if (IsTotalBloodVolumeInRange())
                {
                    if (WholeBloodDeviation > 0)
                        _totalDeviationFromIdeal = "+" + WholeBloodDeviation;
                    else
                        _totalDeviationFromIdeal = WholeBloodDeviation.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    _totalDeviationFromIdeal = "+-   ";
                }
            }
            else
                _totalDeviationFromIdeal = "**";

            return _totalDeviationFromIdeal;
        }

        private string GenerateRbcDeviationFromIdeal()
        {
            if (_rBcDeviationFromIdeal != null) return _rBcDeviationFromIdeal;

            if (_bvaTest.Volumes[BloodType.Ideal].RedCellCount > 0)
            {
                if (IsTotalBloodVolumeInRange())
                {
                    if (RbcDeviation > 0)
                        _rBcDeviationFromIdeal = "+" + RbcDeviation;
                    else
                        _rBcDeviationFromIdeal = RbcDeviation.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    _rBcDeviationFromIdeal = "+-   ";
                }
            }
            else
                _rBcDeviationFromIdeal = "**";

            return _rBcDeviationFromIdeal;
        }

        private string GeneratePlasmaDeviationFromIdeal()
        {
            if (_plasmaDeviationFromIdeal != null) return _plasmaDeviationFromIdeal;

            if (_bvaTest.Volumes[BloodType.Ideal].PlasmaCount > 0)
            {
                if (IsTotalBloodVolumeInRange())
                {
                    if (PlasmaDeviation > 0)
                        _plasmaDeviationFromIdeal = "+" + PlasmaDeviation;
                    else
                        _plasmaDeviationFromIdeal = PlasmaDeviation.ToString(CultureInfo.InvariantCulture);
                }
                else
                {
                    _plasmaDeviationFromIdeal = "+-   ";
                }
            }
            else
                _plasmaDeviationFromIdeal = "**";

            return _plasmaDeviationFromIdeal;
        }

        private string GetIdealBvStringForReport(string bloodPart)
        {
            bool weightDeviationGreaterThanSupported = _bvaTest.Patient.DeviationFromIdealWeight > BvaDomainConstants.MaximumWeightDeviation;
            bool weightDeviationLessThanSupported = _bvaTest.Patient.DeviationFromIdealWeight < BvaDomainConstants.MinimumWeightDeviation;

            if (weightDeviationGreaterThanSupported)
                return "Ideal blood volume not available as patient's weight deviation is greater than the supported maximum of +350%.";
            if (weightDeviationLessThanSupported)
                return "Ideal blood volume not available as patient's weight deviation is less than the supported minimum of -40%.";
            if (_bvaTest.Patient.HeightInCm < _calcEngine.MinimumHeightInCm)
                return "Ideal blood volume not available as patient's height is less than the supported minimum of " + _calcEngine.MinimumHeightInCm;
            if (_bvaTest.Patient.HeightInCm > _calcEngine.MaximumHeightInCm)
                return "Ideal blood volume not available as patient's height is greater than the supported maximum of " + _calcEngine.MaximumHeightInCm;

            if (IsWeightDeviationInRange())
            {
                switch (bloodPart)
                {
                    case "Blood":
                        return GenerateIdealWholeBloodCount();
                    case "Red":
                        return GenerateIdealRedBloodCellCount();
                    case "Plasma":
                        return GenerateIdealPlasmaVolume();
                }
            }

            return "**";
        }

		public bool IsGenderMale()
		{
			return _bvaTest.Patient.Gender == GenderType.Male;
		}

		public string GenerateNormalizedHct()
		{
			if (_normalizedHct != null) return _normalizedHct;

			_normalizedHct = Math.Abs(_bvaTest.NormalizeHematocritResult) < 0 || !IsTotalBloodVolumeInRange() || !IsWeightDeviationInRange() || !IsHeightInRange()
				? "**" : Math.Round(_bvaTest.NormalizeHematocritResult, 1, MidpointRounding.AwayFromZero) + "%";

			return _normalizedHct;
		}

		public string GeneratePeripheralHct()
		{
			if (_peripheralHct != null) return _peripheralHct;

			// ReSharper disable once CompareOfFloatsByEqualityOperator
			_peripheralHct = _bvaTest.PatientHematocritResult == -1 || Double.IsNaN(_bvaTest.PatientHematocritResult) || !IsTotalBloodVolumeInRange()
				? "**" : Math.Round(_bvaTest.PatientHematocritResult, 1, MidpointRounding.AwayFromZero) + "%";

			return _peripheralHct;
		}

		public string GenerateTransudationRate()
		{
			if (_transudationRate != null) return _transudationRate;

			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if (_bvaTest.TransudationRateResult == 0 || Double.IsNaN(_bvaTest.TransudationRateResult) || !IsTotalBloodVolumeInRange())
				_transudationRate = "**";
			else if (_bvaTest.TransudationRateResult > 0)
				_transudationRate = Math.Round((_bvaTest.TransudationRateResult * 100), 2, MidpointRounding.AwayFromZero).ToString("#0.00");
			else if (_bvaTest.TransudationRateResult > -0.0005)
				_transudationRate = Math.Round((_bvaTest.TransudationRateResult * 100), 2, MidpointRounding.AwayFromZero).ToString("#0.00") + "*";
			else
				_transudationRate = "*";

			return _transudationRate;
		}

        #endregion
    }
}
