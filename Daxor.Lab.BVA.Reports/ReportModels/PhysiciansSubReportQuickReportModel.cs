﻿using System;
using System.Globalization;
using System.Text;
using System.Xml.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.BVA.Reports.ReportModels
{
    public class PhysiciansSubReportQuickReportModel
    {
        #region Fields

        private readonly IIdealsCalcEngineService _calcEngine;
        private readonly BVATest _bvaTest;
        private readonly ISettingsManager _settingsManager;
        private string _normalizedHct;
        private string _peripheralHct;
        private string _transudationRate;
        private string _measuredWholeCountMlkgAnalysisElement;
        private string _idealWholeCountMlkgAnalysisElement;
        private string _measuredRbCountMlkgAnalysisElement;
        private string _idealRbCountMlkgAnalysisElement;
        private string _measuredPlasmaMlkgAnalysisElement;
        private string _idealPlasmaMlkgAnalysisElement;
        private string _wholeBloodCondition;
        private string _plasmaCondition;
        private string _redCellCondition;

        #endregion

        #region Ctor

		public PhysiciansSubReportQuickReportModel(BVATest test, IIdealsCalcEngineService calcEngine, ISettingsManager settingsManager)
        {
            _bvaTest = test;
            _calcEngine = calcEngine;
            _settingsManager = settingsManager;
            CalculateDeviations();
        }

        #endregion

        #region Properties

        public ISettingsManager SettingsManager { get { return _settingsManager; } }
        public double WholeBloodDeviation { get; set; }
        public double PlasmaDeviation { get; set; }
        public double RbcDeviation { get; set; }
        public double WholeBloodPercentDeviation { get; set; }
        public double PlasmaPercentDeviation { get; set; }
        public double RbcPercentDeviation { get; set; }
        public bool ShowFindings { get; set; }

        #endregion

        #region Methods

        public string DetermineWatermarkText()
        {
            switch (_bvaTest.Mode)
            {
                case TestMode.Manual:
                    return "MANUAL\nTEST";
                case TestMode.VOPS:
                    return "VOPS\nTEST";
                case TestMode.TrainingHighSlopeTest:
                case TestMode.TrainingHighStd:
                case TestMode.Training:
                case TestMode.TrainingReverseSlope:
                    return "Training Test\nCounts Are Simulated";
                default:
                    switch (_bvaTest.Status)
                    {
                        case TestStatus.Aborted:
                            return "ABORTED";
                        case TestStatus.Pending:
                        case TestStatus.Running:
                        case TestStatus.Undefined:
                        case TestStatus.Deleted:
                            return "PRELIMINARY";
                        default:
                            return "";
                    }
            }
        }

        public bool IsTrainingTest
        {
            get
            {
                switch (_bvaTest.Mode)
                {
                    case TestMode.TrainingHighSlopeTest:
                    case TestMode.TrainingHighStd:
                    case TestMode.Training:
                    case TestMode.TrainingReverseSlope:
                        return true;
                    default:
                        return false;
                }
            }
        }

        public bool IsGenderMale()
        {
            return _bvaTest.Patient.Gender == GenderType.Male;
        }

        public string GenerateNormalizedHct()
        {
            if (_normalizedHct != null) return _normalizedHct;

            _normalizedHct = Math.Abs(_bvaTest.NormalizeHematocritResult) < 0 || !IsTotalBloodVolumeInRange() || !IsWeightDeviationInRange() || !IsHeightInRange()
                ? "**" : Math.Round(_bvaTest.NormalizeHematocritResult, 1, MidpointRounding.AwayFromZero) + "%";

            return _normalizedHct;
        }

        public string GeneratePeripheralHct()
        {
            if (_peripheralHct != null) return _peripheralHct;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _peripheralHct = _bvaTest.PatientHematocritResult == -1 || Double.IsNaN(_bvaTest.PatientHematocritResult) || !IsTotalBloodVolumeInRange()
                ? "**" : Math.Round(_bvaTest.PatientHematocritResult, 1, MidpointRounding.AwayFromZero) + "%";

            return _peripheralHct;
        }

        public string GenerateTransudationRate()
        {
            if (_transudationRate != null) return _transudationRate;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (_bvaTest.TransudationRateResult == 0 || Double.IsNaN(_bvaTest.TransudationRateResult) || !IsTotalBloodVolumeInRange())
                _transudationRate = "**";
            else if (_bvaTest.TransudationRateResult > 0)
                _transudationRate = Math.Round((_bvaTest.TransudationRateResult * 100), 2, MidpointRounding.AwayFromZero).ToString("#0.00");
            else if (_bvaTest.TransudationRateResult > -0.0005)
                _transudationRate = Math.Round((_bvaTest.TransudationRateResult * 100), 2, MidpointRounding.AwayFromZero).ToString("#0.00") + "*";
            else
                _transudationRate = "*";

            return _transudationRate;
        }

        public string GenerateMeasuredWholeCountMlkgAnalysisElement()
        {
            if (_measuredWholeCountMlkgAnalysisElement != null) return _measuredWholeCountMlkgAnalysisElement;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _measuredWholeCountMlkgAnalysisElement = Math.Abs(_bvaTest.Volumes[BloodType.Measured].WholeCount) < -1 || _bvaTest.Patient.WeightInKg == 0 || !IsTotalBloodVolumeInRange()
                ? "**" : Math.Round((_bvaTest.Volumes[BloodType.Measured].WholeCount / _bvaTest.Patient.WeightInKg), 1, MidpointRounding.AwayFromZero).ToString("#0.0");

            return _measuredWholeCountMlkgAnalysisElement;
        }

        public string GenerateIdealWholeCountMlkgAnalysisElement()
        {
            if (_idealWholeCountMlkgAnalysisElement != null) return _idealWholeCountMlkgAnalysisElement;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _idealWholeCountMlkgAnalysisElement = Math.Abs(_bvaTest.Volumes[BloodType.Ideal].WholeCount) < -1 || _bvaTest.Patient.WeightInKg == 0 || !IsWeightDeviationInRange() || !IsHeightInRange()
                ? "**" : Math.Round((_bvaTest.Volumes[BloodType.Ideal].WholeCount / _bvaTest.Patient.WeightInKg), 1, MidpointRounding.AwayFromZero).ToString("#0.0");

            return _idealWholeCountMlkgAnalysisElement;
        }

        public string GenerateMeasuredRbCountMlkgAnalysisElement()
        {
            if (_measuredRbCountMlkgAnalysisElement != null) return _measuredRbCountMlkgAnalysisElement;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _measuredRbCountMlkgAnalysisElement = Math.Abs(_bvaTest.Volumes[BloodType.Measured].RedCellCount) < -1 || _bvaTest.Patient.WeightInKg == 0 || !IsTotalBloodVolumeInRange()
                ? "**" : Math.Round((_bvaTest.Volumes[BloodType.Measured].RedCellCount / _bvaTest.Patient.WeightInKg), 1, MidpointRounding.AwayFromZero).ToString("#0.0");

            return _measuredRbCountMlkgAnalysisElement;
        }

        public string GenerateIdealRbCountMlkgAnalysisElement()
        {
            if (_idealRbCountMlkgAnalysisElement != null) return _idealRbCountMlkgAnalysisElement;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _idealRbCountMlkgAnalysisElement = Math.Abs(_bvaTest.Volumes[BloodType.Ideal].RedCellCount) < -1 || _bvaTest.Patient.WeightInKg == 0 || !IsWeightDeviationInRange() || !IsHeightInRange()
                ? "**" : Math.Round((_bvaTest.Volumes[BloodType.Ideal].RedCellCount / _bvaTest.Patient.WeightInKg), 1, MidpointRounding.AwayFromZero).ToString("#0.0");

            return _idealRbCountMlkgAnalysisElement;
        }

        public string GenerateMeasuredPlasmaMlkgAnalysisElement()
        {
            if (_measuredPlasmaMlkgAnalysisElement != null) return _measuredPlasmaMlkgAnalysisElement;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _measuredPlasmaMlkgAnalysisElement = Math.Abs(_bvaTest.Volumes[BloodType.Measured].PlasmaCount) < -1 || _bvaTest.Patient.WeightInKg == 0 || !IsTotalBloodVolumeInRange()
                ? "**" : Math.Round((_bvaTest.Volumes[BloodType.Measured].PlasmaCount / _bvaTest.Patient.WeightInKg), 1, MidpointRounding.AwayFromZero).ToString("#0.0");

            return _measuredPlasmaMlkgAnalysisElement;
        }

        public string GenerateIdealPlasmaMlkgAnalysisElement()
        {
            if (_idealPlasmaMlkgAnalysisElement != null) return _idealPlasmaMlkgAnalysisElement;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            _idealPlasmaMlkgAnalysisElement = Math.Abs(_bvaTest.Volumes[BloodType.Ideal].PlasmaCount) < -1 || _bvaTest.Patient.WeightInKg == 0 || !IsWeightDeviationInRange() || !IsHeightInRange()
                ? "**" : Math.Round((_bvaTest.Volumes[BloodType.Ideal].PlasmaCount / _bvaTest.Patient.WeightInKg), 1, MidpointRounding.AwayFromZero).ToString("#0.0");

            return _idealPlasmaMlkgAnalysisElement;
        }

        public XElement GeneratePhysiciansReportElement()
        {
            var xElem = new XElement("PhysiciansData");
            return xElem;
        }

        #endregion

        #region Private Helper Methods

        private bool IsTotalBloodVolumeInRange()
        {
            return (_bvaTest.Volumes[BloodType.Measured].WholeCount < BvaDomainConstants.MaximumBloodVolumeInmL) &&
                                    (_bvaTest.Volumes[BloodType.Measured].WholeCount > BvaDomainConstants.MinimumBloodVolumeInmL);
        }

        private bool IsWeightDeviationInRange()
        {
            return (_bvaTest.Patient.DeviationFromIdealWeight >= BvaDomainConstants.MinimumWeightDeviation &&
                        _bvaTest.Patient.DeviationFromIdealWeight <= BvaDomainConstants.MaximumWeightDeviation);
        }

        private bool IsHeightInRange()
        {
            return (_bvaTest.Patient.HeightInCm <= _calcEngine.MaximumHeightInCm && _bvaTest.Patient.HeightInCm >= _calcEngine.MinimumHeightInCm);
        }

        private string GenerateWholeBloodCondition()
        {
            return _wholeBloodCondition ?? (_wholeBloodCondition = GetCondition(WholeBloodPercentDeviation.ToString(CultureInfo.InvariantCulture), "Blood"));
        }

        private void CalculateDeviations()
        {
            WholeBloodDeviation = _bvaTest.Volumes[BloodType.Measured].WholeCount - _bvaTest.Volumes[BloodType.Ideal].WholeCount;
            if (_bvaTest.Volumes[BloodType.Ideal].WholeCount != 0)
            {
                WholeBloodPercentDeviation = (WholeBloodDeviation / _bvaTest.Volumes[BloodType.Ideal].WholeCount) * 100;
            }

            PlasmaDeviation = _bvaTest.Volumes[BloodType.Measured].PlasmaCount - _bvaTest.Volumes[BloodType.Ideal].PlasmaCount;
            if (_bvaTest.Volumes[BloodType.Ideal].PlasmaCount != 0)
            {
                PlasmaPercentDeviation = (PlasmaDeviation / _bvaTest.Volumes[BloodType.Ideal].PlasmaCount) * 100;
            }

            RbcDeviation = _bvaTest.Volumes[BloodType.Measured].RedCellCount - _bvaTest.Volumes[BloodType.Ideal].RedCellCount;
            if (_bvaTest.Volumes[BloodType.Ideal].RedCellCount != 0)
            {
                RbcPercentDeviation = (RbcDeviation / _bvaTest.Volumes[BloodType.Ideal].RedCellCount) * 100;
            }
        }

        private static string GetCondition(string deviationString, string bloodPart)
        {
            // if no deviation (**), return **
            if (deviationString == "-     ")
            {
                return "**";
            }

            var conditionArray = new[] { "Normal", "Mild", "Moderate", "Severe", "Extreme", "Extreme" };

            var deviationIndex = 0;
            var roundedDeviation = Math.Abs(Math.Round(double.Parse(deviationString), 1, MidpointRounding.AwayFromZero));

            switch (bloodPart)
            {
                case "Blood":
                case "Plasma":
                    if (roundedDeviation <= 8)
                    {
                        deviationIndex = 0; // Normal
                    }
                    else if (roundedDeviation <= 16)
                    {
                        deviationIndex = 1; // Mild
                    }
                    else if (roundedDeviation <= 24)
                    {
                        deviationIndex = 2; // Moderate
                    }
                    else if (roundedDeviation <= 32)
                    {
                        deviationIndex = 3; // Severe
                    }
                    else
                    {
                        deviationIndex = 4; // Extreme
                    }

                    break;
                case "Red":
                    if (roundedDeviation <= 10)
                    {
                        deviationIndex = 0; // Normal
                    }
                    else if (roundedDeviation <= 20)
                    {
                        deviationIndex = 1; // Mild
                    }
                    else if (roundedDeviation <= 30)
                    {
                        deviationIndex = 2; // Moderate
                    }
                    else if (roundedDeviation <= 40)
                    {
                        deviationIndex = 3; // Severe
                    }
                    else // > 40
                    {
                        deviationIndex = 4; // Extreme
                    }

                    break;
            }

            if (deviationIndex > 5)
            {
                deviationIndex = 5;
            }

            return conditionArray[deviationIndex];
        }

        private static string DetermineExcessOrDeficit(double deviation)
        {
            return deviation > 0 ? "excess" : "deficit";
        }

        private string GeneratePlasmaCondition()
        {
            return _plasmaCondition ?? (_plasmaCondition = GetCondition(PlasmaPercentDeviation.ToString(CultureInfo.InvariantCulture), "Plasma"));
        }

        private string GenerateRedCellCondition()
        {
            return _redCellCondition ?? (_redCellCondition = GetCondition(RbcPercentDeviation.ToString(CultureInfo.InvariantCulture), "Red"));
        }

        public static double StandardDeviationAsARoundedPercent(double standardDeviationResult)
        {
            var standardDeviationPercent = standardDeviationResult * 100;
            return Math.Round(standardDeviationPercent, 3, MidpointRounding.AwayFromZero);
        }

        #endregion
    }
}
