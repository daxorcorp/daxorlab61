﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Xml.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Reports.Common;
using Daxor.Lab.BVA.Reports.Interfaces;
using Daxor.Lab.BVA.Reports.ReportModels;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.RocketDivisionKey;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.ReportViewer.SubViewModels;
using Daxor.Lab.ReportViewer.SubViews;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using RocketDivision.StarBurnX;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;
using Telerik.Reporting.Processing;
using SubReport = Telerik.Reporting.SubReport;

namespace Daxor.Lab.BVA.Reports.ReportControllers
{
	public class BvaReportController : ReportControllerBase
	{
		#region Fields

		BVAReport _bvaReport;

		SubReport_BloodVolumeAnalysis _bloodVolumeAnalysisSubReport;
		readonly Unit _nameFont = Unit.Cm(14 * 0.03528);
		Unit _unitX = Unit.Inch(0);
		Unit _unitY = Unit.Inch(0);

		private const string DefaultAmputeeGuidanceDocumentFilepath =
			@"C:\ProgramData\Daxor\Lab\Help\BVA-100 Operations Manual\Post-Procedure 4 - Amputee Guidance.pdf";

		private TaskScheduler _taskScheduler;
		private bool _printing;
		private bool _useIdealDispatcher = true;
		private readonly BVATest _test;
		private readonly IEnumerable<AuditTrailRecord> _auditTrailRecordList;
		private readonly IFolderBrowser _folderBrowser;
		private readonly IIdealsCalcEngineService _calcEngine;
		private readonly ILoggerFacade _logger;
		private readonly IMessageBoxDispatcher _messageBoxDispatcher;
		private readonly IMessageManager _messageManager;
		private readonly IPdfPrinter _pdfPrinter;
		private readonly IStarBurnWrapper _opticalDriveWrapper;
		private readonly IZipFileFactory _zipFileFactory;

		#endregion

		#region Ctor

		public BvaReportController(ILoggerFacade logger, IMessageBoxDispatcher messageBoxDispatcher,
			IRegionManager regionManager, IIdealsCalcEngineService calcEngine, IEventAggregator eventAggregator,
			BVATest test, IFolderBrowser folderBrowser, IMessageManager messageManager, IStarBurnWrapper driveWrapper,
			ISettingsManager settingsManager, IAuditTrailDataAccess auditTrailDataAccess, IZipFileFactory zipFileFactory,
			IPdfPrinter pdfPrinter)
		{
			_logger = logger;
			_test = test;
			SettingsManager = settingsManager;
			_messageBoxDispatcher = messageBoxDispatcher;
			_folderBrowser = folderBrowser;
			RegionManager = regionManager;
			EventAggregator = eventAggregator;
			_messageManager = messageManager;
			_opticalDriveWrapper = driveWrapper;
			_calcEngine = calcEngine;
			_zipFileFactory = zipFileFactory;
			_auditTrailRecordList = auditTrailDataAccess.SelectList(test.InternalId).ToArray();
			_pdfPrinter = pdfPrinter;

			BvaReportModel = new BvaTestReportModel(test, calcEngine, _auditTrailRecordList) { ShowAuditTrail = true };
			ShowFindings = true;
			PrintReportType = Enums.ReportType.FullReport;
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the scheduler for running asynchronous tasks. By default, this is the regular thread pool,
		/// but is overridable such that other schedulers can be used (i.e., for unit testing).
		/// </summary>
		/// <remarks>
		/// When using a custom scheduler, the IdealDispatcher (for running code on the UI thread) will not be
		/// used, so that the executing code will run under the custom scheduler.
		/// </remarks>
		/// 
		/// v6.05  2019-12-18  Lee Muller added this and CASE statement above, for the new [Summary] report button.

		public TaskScheduler TaskScheduler
		{
			get
			{
				return _taskScheduler ?? (_taskScheduler = TaskScheduler.Default);
			}
			set
			{
				_taskScheduler = value;
				_useIdealDispatcher = false;
			}
		}

		private bool ShowFindings { get; set; }

		private BvaTestReportModel BvaReportModel { get; set; }

		private Enums.ReportType PrintReportType { get; set; }

		private AuditTrailSubReportController AuditReportController { get; set; }

		private BloodVolumeAnalysisSubReportController BloodVolumeAnalysisController { get; set; }

		private AnalystSubReportController AnalystReportController { get; set; }

		private PhysiciansSubReportController PhysiciansReportController { get; set; }

		#endregion

		#region IReport Members

		public override IReportDocument ReportToView
		{
			//  v6.04.n		Always show the full report
			get { return _bvaReport; }
		}

		/// <summary>
		/// Loads a BVA report based on the BV test associated with the report contoller and provides the ReportProcessor with
		/// the report that was generated.
		/// </summary>
		public override void PrintReport()
		{
			_printing = true;
			var printReportVm = new PrintReportViewModel(SettingsManager);
			var printReportDialog = new PrintReportDialogInterface(printReportVm);

			var choices = new[] { "Print", "Cancel" };
			var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information, printReportDialog, "Print BVA Report", choices);

			if (choices[result] == "Cancel")
				return;

			DoEvents();
			ShowFindings = printReportVm.IncludeFindings;

			BvaReportModel.ShowAuditTrail = printReportVm.IncludeAuditTrail;

			switch (printReportVm.SelectedReportStyle)
			{
				case PrintReportViewModel.ReportStyle.Full:
					PrintReportType = Enums.ReportType.FullReport;
					break;

				case PrintReportViewModel.ReportStyle.Physicians:
					PrintReportType = Enums.ReportType.PhysiciansReport;
					break;

				case PrintReportViewModel.ReportStyle.Analysts:
					PrintReportType = Enums.ReportType.AnalystsReport;
					break;

				// v6.05  2019-12-18  Lee Muller added this and CASE statement above, for the new [Summary] report button.
				case PrintReportViewModel.ReportStyle.Summary:
					PrintReportType = Enums.ReportType.SummaryReport;

					break;
			}

			LoadReportData();

			DoEvents();

			var standardPrintController =
				new StandardPrintController();
			var settings = new PrinterSettings();

			var proc = new ReportProcessor { PrintController = standardPrintController };

			DoEvents();

			proc.PrintReport(new InstanceReportSource { ReportDocument = _bvaReport }, settings);

			if (_test.Patient.IsAmputee)
				_pdfPrinter.Print(DefaultAmputeeGuidanceDocumentFilepath);

			_printing = false;
		}

		/// <summary>
		/// Loads a BVA report based on the BV test associated with the report controller, in both PDF and xml format, and exports 
		/// the reports to the specified location in a zipped, password protected file.
		/// </summary>
		public override void ExportReport()
		{
			var fileName = BuildFileName(BvaReportModel.Patient.HospitalPatientId);
			var zipFilePath = SettingsManager.GetSetting<string>(SettingKeys.SystemTempFilePath);

			var messageBoxChoices = new[] { Constants.ExportChoiceText, Constants.CancelChoiceText };
			var choiceIndex = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Undefined, _folderBrowser,
				Constants.ExportMessageBoxCaption, messageBoxChoices);

			if (messageBoxChoices[choiceIndex] == Constants.CancelChoiceText)
				return;

			if (!_folderBrowser.IsPathValid())
			{
				var errorMessage = _messageManager.GetMessage(MessageKeys.BvaExportPathInvalid).FormattedMessage;
				_messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, errorMessage, Constants.ExportMessageBoxCaption,
					MessageBoxChoiceSet.Close);
				_logger.Log(errorMessage, Category.Info, Priority.Low);
				return;
			}

			LoadReportData();

			if (!_folderBrowser.IsOpticalDrive)
			{
				var currentPath = _folderBrowser.CurrentPath;
				if (!currentPath.EndsWith(Constants.FolderSeparator))
					currentPath += Constants.FolderSeparator;

				zipFilePath = currentPath;
			}

			var tokenSource = new CancellationTokenSource();
			var token = tokenSource.Token;
			var exportTask = new Task(() => ExportTask(fileName, _folderBrowser, zipFilePath, tokenSource), token);
			exportTask.ContinueWith(result => OnExportTaskFaulted(result.Exception), token, TaskContinuationOptions.OnlyOnFaulted, TaskScheduler);
			exportTask.ContinueWith(result => OnExportTaskCompleted(), token, TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler);
			exportTask.Start(TaskScheduler);
		}

		protected virtual void OnExportTaskCompleted()
		{
			DisableBusyIndicator();

			IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(
				MessageBoxCategory.Information, "The export has completed successfully.",
				Constants.ExportMessageBoxCaption, MessageBoxChoiceSet.Close));
		}

		protected virtual void OnExportTaskFaulted(Exception faultingException)
		{
			var errorMessage = _messageManager.GetMessage(MessageKeys.BvaExportUnsuccessful);

			DisableBusyIndicator();
			if (_useIdealDispatcher)
				IdealDispatcher.BeginInvoke(() => DisplayExportErrorMessage(errorMessage));
			else
				DisplayExportErrorMessage(errorMessage);

			var exceptionToLog = faultingException.InnerException ?? faultingException;
			var logMessage = String.Format("{0}; inner exception: {1}", errorMessage.FormattedMessage, exceptionToLog.Message);
			_logger.Log(logMessage, Category.Exception, Priority.High);
		}

		protected virtual void ExportTask(string fileName, IFolderBrowser folderBrowser, string zipFilePath, CancellationTokenSource cancellationTokenSource)
		{
			EventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Exporting Report..."));
			var tempPath = SettingsManager.GetSetting<string>(SettingKeys.SystemTempFilePath);

			SavePdfRenderingToFile(fileName, tempPath);
			SaveReportXmlToFile(fileName, tempPath);
			SaveZipFile(fileName, zipFilePath, tempPath);

			if (_folderBrowser.IsOpticalDrive)
				BurnToOpticalMedia(fileName, cancellationTokenSource);
			else
				CopyToNonOpticalMedia(fileName, zipFilePath, tempPath);

			CleanUpAfterExport(fileName, tempPath);
		}

		private void CopyToNonOpticalMedia(string fileName, string zipFilePath, string tempPath)
		{
			SafeFileOperations.Copy(tempPath + fileName + ".zip", zipFilePath + fileName + ".zip");
		}

		protected virtual void CleanUpAfterExport(string fileName, string tempPath)
		{
			SafeFileOperations.Delete(tempPath, fileName + ".*");
		}

		protected virtual void SaveReportXmlToFile(string fileName, string tempPath)
		{
			var root = new XElement("report",
				GetHeaderElement(),
				GetPatientDemographicsElement(),
				BloodVolumeAnalysisController.GetBloodVolumeAnalysisElement(),
				PhysiciansReportController.GetAdditionalAnalysisElement(),
				PhysiciansReportController.GetPhysiciansReportElement(),
				AnalystReportController.GetAnalystReportElement(),
				AnalystReportController.GetPatientSampleCountsElement(),
				AuditReportController.GetAuditTrailReportElement()
				);

			root.Save(tempPath + fileName + ".xml");
		}

		protected virtual void SavePdfRenderingToFile(string fileName, string tempPath)
		{
			var renderedReport = new ReportProcessor().RenderReport("PDF", new InstanceReportSource { ReportDocument = _bvaReport }, null);
			using (var fs = new FileStream(tempPath + fileName + ".pdf", FileMode.Create))
				fs.Write(renderedReport.DocumentBytes, 0, renderedReport.DocumentBytes.Length);
		}

		private void BurnToOpticalMedia(string fileName, CancellationTokenSource cancellationTokenSource)
		{
			var dataBurner = _opticalDriveWrapper.CreateDataBurner();
			dataBurner.OnProgress += OnDataBurnerProgress;

			var dataFolder = (IDataFolder) dataBurner;
			dataFolder.AddFile(
				SettingsManager.GetSetting<string>(SettingKeys.SystemTempFilePath) +
				fileName + ".zip");

			try
			{
				dataBurner.Burn(false, RocketDivisionKey.DEF_VOLUME_NAME, "StarBurnX",
					"StarBurnX Data Burner");
			}
			catch (Exception ex)
			{
				Message errorAlert;
				string message;
				switch (ex.Message)
				{
					case "Device is not ready:Device is not ready":
						errorAlert =
							_messageManager.GetMessage(MessageKeys.BvaExportDeviceNotReady);
						message = errorAlert.FormattedMessage;
						break;
					case "Disk readonly: The media disk is not recordable":
						errorAlert =
							_messageManager.GetMessage(MessageKeys.BvaExportDiscReadonly);
						message = errorAlert.FormattedMessage;
						break;
					case
						"Disc is finalized:The disc is finalized! Note that is not possible to write data on the finalized disc!"
						:
						errorAlert =
							_messageManager.GetMessage(MessageKeys.BvaExportDiscFinalized);
						message = errorAlert.FormattedMessage;
						break;
					default:
						message = ex.Message;
						_logger.Log(ex.Message, Category.Exception, Priority.High);
						break;
				}
				if (_useIdealDispatcher)
					IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error,
						message, Constants.ExportMessageBoxCaption, MessageBoxChoiceSet.Close));
				else
					_messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error,
						message, Constants.ExportMessageBoxCaption, MessageBoxChoiceSet.Close);

				_logger.Log(message, Category.Info, Priority.Low);
				dataBurner.OnProgress -= OnDataBurnerProgress;
				cancellationTokenSource.Cancel();
			}

			dataBurner.OnProgress -= OnDataBurnerProgress;
			EventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
			dataBurner.Drive.Eject();
		}

		protected virtual void SaveZipFile(string fileName, string zipFilePath, string tempPath)
		{

			var zipFile = _zipFileFactory.CreateZipFileWithPassword(SettingsManager.GetSetting<string>(SettingKeys.SystemPasswordUnsecureExport));

			zipFile.AddFileToRoot(tempPath + fileName + ".pdf");
			zipFile.AddFileToRoot(tempPath + fileName + ".xml");
			zipFile.Save(tempPath + fileName + ".zip");
		}

		#endregion

		#region Report building methods

		/// <summary>
		/// Loads the specified BVA report based on the BV test associated with the report controller formatted
		/// according to the type of report requested.
		/// </summary>
		public override void LoadReportData()
		{
			_bvaReport = new BVAReport();
			LoadPageHeaderData();
			BloodVolumeAnalysisController = new BloodVolumeAnalysisSubReportController(new BloodVolumeAnalysisSubReportModel(_test, _calcEngine), _logger);
			_bloodVolumeAnalysisSubReport = BloodVolumeAnalysisController.LoadBloodVolumeAnalysis();

			var subReports = new List<SubReport>();

			// v6.05  2019-12-18  Lee Muller added CASE statement for the new [Summary] report button.

			switch (PrintReportType)
			{
				case Enums.ReportType.FullReport:
					if (BvaReportModel.ShowAuditTrail)
					{
						var buildReport =
							Task.Factory.StartNew(() =>
								{
									PhysiciansReportController = new PhysiciansSubReportController(new PhysiciansSubReportModel(_test, _calcEngine, SettingsManager) { ShowFindings = ShowFindings });
									subReports.Add(PhysiciansReportController.LoadPhysicianReportData(_bloodVolumeAnalysisSubReport, ref _unitX, ref _unitY, _printing));
									return subReports;
								})
							.ContinueWith(x =>
								{
									subReports = x.Result;
									AnalystReportController = new AnalystSubReportController(new AnalystSubReportModel(_test, _auditTrailRecordList, SettingsManager), _logger);
									subReports.Add(AnalystReportController.LoadAnalystReportData(_bloodVolumeAnalysisSubReport, ref _unitX, ref _unitY, _printing));
									return subReports;
								})
							.ContinueWith(y =>
								{
									subReports = y.Result;
									AuditReportController = new AuditTrailSubReportController(new AuditTrailSubReportModel(_test, _auditTrailRecordList));
									subReports.Add(AuditReportController.LoadAuditTrailData(ref _unitX, ref _unitY));
									return subReports;
								});

						subReports = buildReport.Result;
					}
					else
					{
						Task<List<SubReport>> buildReport =
							Task.Factory.StartNew(() =>
							{
								PhysiciansReportController = new PhysiciansSubReportController(new PhysiciansSubReportModel(_test, _calcEngine, SettingsManager) { ShowFindings = ShowFindings });
								subReports.Add(PhysiciansReportController.LoadPhysicianReportData(_bloodVolumeAnalysisSubReport, ref _unitX, ref _unitY, _printing));
								return subReports;
							})
							.ContinueWith(x =>
							{
								subReports = x.Result;
								AnalystReportController = new AnalystSubReportController(new AnalystSubReportModel(_test, _auditTrailRecordList, SettingsManager), _logger);
								subReports.Add(AnalystReportController.LoadAnalystReportData(_bloodVolumeAnalysisSubReport, ref _unitX, ref _unitY, _printing));
								return subReports;
							});

						subReports = buildReport.Result;
					}
					break;

				case Enums.ReportType.AnalystsReport:
					_bvaReport.textBoxReportType.Value = @"ANALYST";
					AnalystReportController = new AnalystSubReportController(new AnalystSubReportModel(_test, _auditTrailRecordList, SettingsManager), _logger);
					subReports.Add(AnalystReportController.LoadAnalystReportData(_bloodVolumeAnalysisSubReport, ref _unitX, ref _unitY, _printing));

					break;

				case Enums.ReportType.PhysiciansReport:
					PhysiciansReportController = new PhysiciansSubReportController(new PhysiciansSubReportModel(_test, _calcEngine, SettingsManager) { ShowFindings = ShowFindings });
					subReports.Add(PhysiciansReportController.LoadPhysicianReportData(_bloodVolumeAnalysisSubReport, ref _unitX, ref _unitY, _printing));

					break;

				// v6.05  2019-12-18  Lee Muller added this CASE statement and above, for the new [Summary] report button.
				case Enums.ReportType.SummaryReport:
					// v6.05 Until we get some of the Summary Report constructed, Just have it run something which already exists, then replace that.
					PhysiciansReportController = new PhysiciansSubReportController(new PhysiciansSubReportModel(_test, _calcEngine, SettingsManager) { ShowFindings = ShowFindings });
					subReports.Add(PhysiciansReportController.LoadPhysicianReportData(_bloodVolumeAnalysisSubReport, ref _unitX, ref _unitY, _printing));

					break;

				default:

					return;
			}

			foreach (var subReport in subReports)
				_bvaReport.detail.Items.Add(subReport);

			_unitX = Unit.Inch(0);
			_unitY = Unit.Inch(0);
		}

		/// <summary>
		/// Populates the header and footer of the report based on the BV test associated with
		/// the report controller.
		/// </summary>
		private void LoadPageHeaderData()
		{
			_bvaReport.pictureBoxIcon.Visible = SettingsManager.GetSetting<bool>(SettingKeys.BvaReportLogoEnabled);
			_bvaReport.textBoxHospitalName.Value = SettingsManager.GetSetting<string>(SettingKeys.SystemHospitalName);
			_bvaReport.textBoxHospitalAddress.Value = SettingsManager.GetSetting<string>(SettingKeys.SystemDepartmentAddress);
			_bvaReport.textBoxHospitalDepartment.Value = SettingsManager.GetSetting<string>(SettingKeys.SystemDepartmentName);
			_bvaReport.textBoxHospitalDepartmentDirector.Value = SettingsManager.GetSetting<string>(SettingKeys.SystemDepartmentDirector);
			_bvaReport.textBoxHospitalPhone.Value = SettingsManager.GetSetting<string>(SettingKeys.SystemDepartmentPhoneNumber);

			if (BvaReportModel.Mode != TestMode.Manual)
			{
				switch (BvaReportModel.Status)
				{
					case TestStatus.Aborted:
						_bvaReport.textBoxReportStatus.Value = @"ABORTED REPORT";
						break;
					case TestStatus.Completed:
						_bvaReport.textBoxReportStatus.Value = @"FINAL REPORT";
						break;
					default:
						_bvaReport.textBoxReportStatus.Value = @"PRELIMINARY REPORT";
						break;
				}
			}
			else
			{
				_bvaReport.textBoxReportStatus.Value = @"MANUAL REPORT";
			}

			_bvaReport.textBoxAccession.Value = string.IsNullOrEmpty(BvaReportModel.TestID2) ? "-" : BvaReportModel.TestID2;
			ReportUtilities.BoldIfInAuditTrail(_bvaReport.textBoxAccession, "ID2", BvaReportModel.AuditRecordList);
			_bvaReport.textBoxAnalyst.Value = string.IsNullOrEmpty(BvaReportModel.Analyst) ? "-" : BvaReportModel.Analyst;
			ReportUtilities.BoldIfInAuditTrail(_bvaReport.textBoxAnalyst, "ANALYST", BvaReportModel.AuditRecordList);
			_bvaReport.textBoxCCTo.Value = string.IsNullOrEmpty(BvaReportModel.CcReportTo) ? "-" : BvaReportModel.CcReportTo;
			ReportUtilities.BoldIfInAuditTrail(_bvaReport.textBoxCCTo, "CC_REPORT_TO", BvaReportModel.AuditRecordList);
			_bvaReport.textBoxComments.Value = BvaReportModel.Comments.Truncate(100);
			ReportUtilities.BoldIfInAuditTrail(_bvaReport.textBoxComments, "COMMENT", BvaReportModel.AuditRecordList);
			_bvaReport.textBoxDateAnalyzed.Value = BvaReportModel.Date.ToString("g", CultureInfo.CreateSpecificCulture("en-us"));
			ReportUtilities.BoldIfInAuditTrail(_bvaReport.textBoxDateAnalyzed, "TEST_DATE", BvaReportModel.AuditRecordList);
			if (BvaReportModel.Patient.HeightInCm > 0 && BvaReportModel.Patient.WeightInKg > 0 && BvaReportModel.HeightInRange())
			{
				if (Math.Round(BvaReportModel.Patient.DeviationFromIdealWeight, 1, MidpointRounding.AwayFromZero) > BvaDomainConstants.MaximumWeightDeviation
					|| Math.Round(BvaReportModel.Patient.DeviationFromIdealWeight, 1, MidpointRounding.AwayFromZero) < BvaDomainConstants.MinimumWeightDeviation)
				{
					_bvaReport.textBoxDeviationFromIdealWeight.Value = BvaReportModel.Patient.DeviationFromIdealWeight > 0 ?
							"*+" + Math.Round(BvaReportModel.Patient.DeviationFromIdealWeight, 1, MidpointRounding.AwayFromZero) + "%" :
							Math.Round(BvaReportModel.Patient.DeviationFromIdealWeight, 1, MidpointRounding.AwayFromZero) + "%";
				}
				else
				{
					_bvaReport.textBoxDeviationFromIdealWeight.Value =
						BvaReportModel.Patient.DeviationFromIdealWeight > 0 ? "+" + Math.Round(BvaReportModel.Patient.DeviationFromIdealWeight, 1, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture) + "%" :
						Math.Round(BvaReportModel.Patient.DeviationFromIdealWeight, 1, MidpointRounding.AwayFromZero) + "%";
				}
			}
			else
			{
				_bvaReport.textBoxDeviationFromIdealWeight.Value = @"**";
			}

			_bvaReport.textBoxInjectateLot.Value = string.IsNullOrEmpty(BvaReportModel.InjectateLot) ? "-" : BvaReportModel.InjectateLot;

			ReportUtilities.BoldIfInAuditTrail(_bvaReport.textBoxInjectateLot, "INJECTATE_LOT", BvaReportModel.AuditRecordList);

			_bvaReport.textBoxLocation.Value = string.IsNullOrEmpty(BvaReportModel.Location) ? "-" : BvaReportModel.Location;

			ReportUtilities.BoldIfInAuditTrail(_bvaReport.textBoxLocation, "LOCATION", BvaReportModel.AuditRecordList);

			if (BvaReportModel.Patient.DateOfBirth != null)
			{
				_bvaReport.textBoxPatientDOB.Value = DateTime.Parse(BvaReportModel.Patient.DateOfBirth.ToString()).ToString("M/d/yyyy") + " (" +
					ReportUtilities.DobToAge(DateTime.Parse(BvaReportModel.Patient.DateOfBirth.ToString()), DateTime.Parse(BvaReportModel.Date.ToString(CultureInfo.InvariantCulture))) + @" y/o)";
			}

			ReportUtilities.BoldIfInAuditTrail(_bvaReport.textBoxPatientDOB, "DOB", BvaReportModel.AuditRecordList);

			_bvaReport.textBoxPatientGender.Value = BvaReportModel.Patient.Gender.ToString();

			ReportUtilities.BoldIfInAuditTrail(_bvaReport.textBoxPatientGender, "GENDER", BvaReportModel.AuditRecordList);

			_bvaReport.textBoxAmputeeStatus.Value = ReportUtilities.BoolToAmputeeStatus(BvaReportModel.Patient.IsAmputee);

			switch (BvaReportModel.Patient.MeasurementSystem)
			{
				case MeasurementSystem.English:
					// ReSharper disable once CompareOfFloatsByEqualityOperator
					_bvaReport.textBoxPatientHeight.Value = BvaReportModel.Patient.HeightInInches == 0 ? "-" : Math.Round(BvaReportModel.Patient.HeightInInches, 2, MidpointRounding.AwayFromZero).ToString("F2") + " " + UnitOfMeasurementConstants.USUnitOfLength;

					// ReSharper disable once CompareOfFloatsByEqualityOperator
					_bvaReport.textBoxPatientWeight.Value = BvaReportModel.Patient.WeightInLb == 0 ? "-" : Math.Round(BvaReportModel.Patient.WeightInLb, 2, MidpointRounding.AwayFromZero).ToString("F2") + " " + UnitOfMeasurementConstants.USUnitOfMass;
					break;
				case MeasurementSystem.Metric:
					// ReSharper disable once CompareOfFloatsByEqualityOperator
					_bvaReport.textBoxPatientHeight.Value = BvaReportModel.Patient.HeightInCm == 0 ? "-" : Math.Round(BvaReportModel.Patient.HeightInCm, 2, MidpointRounding.AwayFromZero).ToString("F2") + " " + UnitOfMeasurementConstants.MetricUnitOfLength;

					// ReSharper disable once CompareOfFloatsByEqualityOperator
					_bvaReport.textBoxPatientWeight.Value = BvaReportModel.Patient.WeightInKg == 0 ? "-" : Math.Round(BvaReportModel.Patient.WeightInKg, 2, MidpointRounding.AwayFromZero).ToString("F2") + " " + UnitOfMeasurementConstants.MetricUnitOfMass;
					break;
			}

			ReportUtilities.BoldIfInAuditTrail(_bvaReport.textBoxPatientHeight, "PATIENT_HEIGHT", BvaReportModel.AuditRecordList);

			ReportUtilities.BoldIfInAuditTrail(_bvaReport.textBoxPatientWeight, "PATIENT_WEIGHT", BvaReportModel.AuditRecordList);

			_bvaReport.textBoxPatientHospitalID.Value = BvaReportModel.Patient.HospitalPatientId;

			ReportUtilities.BoldIfInAuditTrail(_bvaReport.textBoxPatientHospitalID, "PATIENT_HOSPITAL_ID", BvaReportModel.AuditRecordList);

			_bvaReport.textBoxPatientName.Value = BvaReportModel.Patient.FullName == string.Empty ? "-" : BvaReportModel.Patient.FullName;

			_bvaReport.textBoxPatientName.Style.Font.Size = _nameFont;

			_bvaReport.textBoxReferringMD.Value = string.IsNullOrEmpty(BvaReportModel.RefPhysician) ? "-" : BvaReportModel.RefPhysician;

			ReportUtilities.BoldIfInAuditTrail(_bvaReport.textBoxReferringMD, "REFERRING_PHYSICIAN", BvaReportModel.AuditRecordList);

			_bvaReport.textBoxUnitID.Value = (BvaReportModel.SystemId == null) ? string.Empty : BvaReportModel.SystemId.ToString(CultureInfo.InvariantCulture);

			_bvaReport.textBoxBVAVersion.Value = SettingsManager.GetSetting<string>(SettingKeys.BvaModuleVersion);

			_bvaReport.textBoxTestID2.Value = SettingsManager.GetSetting<string>(SettingKeys.BvaTestId2Label) + @":";

			_bvaReport.textBoxLocationLabel.Value = SettingsManager.GetSetting<string>(SettingKeys.BvaLocationLabel) + @":";
		}

		#endregion

		#region XML report builder methods

		/// <summary>
		/// Creates and populates the HeaderData element on the xml report based on the BV test associated with
		/// the report controller.
		/// </summary>
		/// <returns>A refference to the HeaderData element.</returns>
		private XElement GetHeaderElement()
		{
			var xElem = new XElement("HeaderData");

			xElem.Add(new XElement("HospitalName", SettingsManager.GetSetting<string>(SettingKeys.SystemHospitalName)));
			xElem.Add(new XElement("DepartmentName", SettingsManager.GetSetting<string>(SettingKeys.SystemDepartmentName)));
			xElem.Add(new XElement("DepartmentAddress", SettingsManager.GetSetting<string>(SettingKeys.SystemDepartmentAddress)));
			xElem.Add(new XElement("DepartmentDirector", SettingsManager.GetSetting<string>(SettingKeys.SystemDepartmentDirector)));
			xElem.Add(new XElement("DepartmentPhoneNumber", SettingsManager.GetSetting<string>(SettingKeys.SystemDepartmentPhoneNumber)));

			string status;

			switch (BvaReportModel.Status)
			{
				case TestStatus.Aborted:
					status = "ABORTED REPORT";
					break;
				case TestStatus.Completed:
					status = "FINAL REPORT";
					break;
				default:
					status = "PRELIMINARY REPORT";
					break;
			}

			xElem.Add(new XElement("Status", status));
			xElem.Add(new XElement("UniqueSystemID", BvaReportModel.SystemId));
			xElem.Add(new XElement("ApplicationVersionNumber", SettingsManager.GetSetting<string>(SettingKeys.BvaModuleVersion)));
			xElem.Add(new XElement("DeviceModality", SettingsManager.GetSetting<string>(SettingKeys.SystemDeviceModality)));

			return xElem;
		}

		/// <summary>
		/// Creates and populates the PatientDemographics element on the xml report based on the BV test associated with
		/// the report controller.
		/// </summary>
		/// <returns>A refference to the PatientDemographics element.</returns>
		private XElement GetPatientDemographicsElement()
		{
			var xElem = new XElement("PatientDemographics");

			if (BvaReportModel.Patient.FullName != null)
				xElem.Add(new XElement("FullName", BvaReportModel.Patient.FullName));
			if (BvaReportModel.Patient.FirstName != null)
				xElem.Add(new XElement("FirstName", BvaReportModel.Patient.FirstName));
			if (BvaReportModel.Patient.MiddleName != null)
				xElem.Add(new XElement("MiddleName", BvaReportModel.Patient.MiddleName));
			if (BvaReportModel.Patient.LastName != null)
				xElem.Add(new XElement("LastName", BvaReportModel.Patient.LastName));

			xElem.Add(new XElement("ID", BvaReportModel.Patient.HospitalPatientId));
			xElem.Add(new XElement(SettingsManager.GetSetting<string>(SettingKeys.BvaTestId2Label), BvaReportModel.TestID2));
			xElem.Add(new XElement("ReferringMD", BvaReportModel.RefPhysician));
			xElem.Add(new XElement("CC", BvaReportModel.CcReportTo));

			if (BvaReportModel.Comments != null)
				xElem.Add(new XElement("Comment", BvaReportModel.Comments));
			if (BvaReportModel.Patient.DateOfBirth != null)
				xElem.Add(new XElement("DOB", ((DateTime) (BvaReportModel.Patient.DateOfBirth)).ToString("d")));

			xElem.Add(new XElement("Gender", BvaReportModel.Patient.Gender));
			switch (BvaReportModel.Patient.MeasurementSystem)
			{
				// ReSharper disable CompareOfFloatsByEqualityOperator
				case MeasurementSystem.English:
					xElem.Add(new XElement("Height", new XAttribute("Units", UnitOfMeasurementConstants.USUnitOfLength), BvaReportModel.Patient.HeightInInches == 0 ? "-" : Math.Round(BvaReportModel.Patient.HeightInInches, 2, MidpointRounding.AwayFromZero).ToString("f2")));
					xElem.Add(new XElement("Weight", new XAttribute("Units", UnitOfMeasurementConstants.USUnitOfMass), BvaReportModel.Patient.WeightInLb == 0 ? "-" : Math.Round(BvaReportModel.Patient.WeightInLb, 2, MidpointRounding.AwayFromZero).ToString("f2")));
					break;
				case MeasurementSystem.Metric:
					xElem.Add(new XElement("Height", new XAttribute("Units", UnitOfMeasurementConstants.MetricUnitOfLength), BvaReportModel.Patient.HeightInCm == 0 ? "-" : Math.Round(BvaReportModel.Patient.HeightInCm, 2, MidpointRounding.AwayFromZero).ToString("f2")));
					xElem.Add(new XElement("Weight", new XAttribute("Units", UnitOfMeasurementConstants.MetricUnitOfMass), BvaReportModel.Patient.WeightInKg == 0 ? "-" : Math.Round(BvaReportModel.Patient.WeightInKg, 2, MidpointRounding.AwayFromZero).ToString("f2")));
					break;
				// ReSharper restore CompareOfFloatsByEqualityOperator
			}
			xElem.Add(new XElement("AmputeeStatus", BvaReportModel.Patient.IsAmputee ? "Amputee" : "Non-amputee"));

			xElem.Add(new XElement("DeviationFromIdealWeight", new XAttribute("Units", "%"), Math.Round(BvaReportModel.Patient.DeviationFromIdealWeight, 1, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture)));

			xElem.Add(new XElement("AnalyzedOn", BvaReportModel.Date.ToString("g",
			   CultureInfo.CreateSpecificCulture("en-us"))));

			// ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
			if (BvaReportModel.Patient.DateOfBirth != null)
			{
				xElem.Add(new XElement("Age", ReportUtilities.DobToAge(DateTime.Parse(BvaReportModel.Patient.DateOfBirth.ToString()), DateTime.Parse(BvaReportModel.Date.ToString(CultureInfo.InvariantCulture)))));
			}
			else
			{
				xElem.Add(new XElement("Age", string.Empty));
			}

			xElem.Add(new XElement("Analyst", BvaReportModel.Analyst));
			xElem.Add(new XElement("InjectateLot", BvaReportModel.InjectateLot));
			xElem.Add(new XElement(SettingsManager.GetSetting<string>(SettingKeys.BvaLocationLabel), BvaReportModel.Location));


			return xElem;
		}

		#endregion

		#region Helper Methods

		[SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
		public void DoEvents()
		{
			var frame = new DispatcherFrame();
			Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
				new DispatcherOperationCallback(ExitFrame), frame);
			Dispatcher.PushFrame(frame);
		}

		public object ExitFrame(object f)
		{
			((DispatcherFrame) f).Continue = false;

			return null;
		}

		void OnDataBurnerProgress(int percent, int timeRemaining)
		{
			EventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true) { PercentageCompleted = percent, Message = "Burning CD" });
		}

		private void DisplayExportErrorMessage(Message errorMessage)
		{
			_messageBoxDispatcher.ShowMessageBox(
				MessageBoxCategory.Error,
				errorMessage.FormattedMessage,
				Constants.ExportMessageBoxCaption, MessageBoxChoiceSet.Close);
		}

		private void DisableBusyIndicator()
		{
			var busyStateChangedEvent = EventAggregator.GetEvent<BusyStateChanged>();
			if (busyStateChangedEvent != null) busyStateChangedEvent.Publish(new BusyPayload(false));
		}

		#endregion
	}
}
