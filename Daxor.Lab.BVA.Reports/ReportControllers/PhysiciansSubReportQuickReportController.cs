﻿using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Reports.ReportModels;
using Daxor.Lab.SettingsManager.Common;
using System.Xml.Linq;
using Daxor.Lab.SettingsManager.Interfaces;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;

namespace Daxor.Lab.BVA.Reports.ReportControllers
{
	public class PhysiciansSubReportQuickReportController
	{
		#region Fields

		private readonly PhysiciansSubReportQuickReportModel _physiciansSubReportModel;
		private readonly SubReport_PhysicianQuickReport _physiciansSubreport;
		private readonly ISettingsManager _settingsManager;

		#endregion

		#region Ctor

		public PhysiciansSubReportQuickReportController(PhysiciansSubReportQuickReportModel physiciansSubReportModel)
		{
			_physiciansSubReportModel = physiciansSubReportModel;
			_physiciansSubreport = new SubReport_PhysicianQuickReport();
			_settingsManager = _physiciansSubReportModel.SettingsManager;
		}

		#endregion

		#region Public API

		/// <summary>
		/// Creates and populates the Physicians' sub report based on the BV test associated with
		/// the report controller.
		/// </summary>
		public SubReport LoadPhysicianReportData(SubReport_BloodVolumeAnalysisQuickReport bloodVolumeAnalysisSubReport, ref Unit unitX, ref Unit unitY, bool printing)
		{
			_physiciansSubreport.textBoxWatermark1.Value = _physiciansSubReportModel.DetermineWatermarkText();

			if (_physiciansSubReportModel.IsTrainingTest)
				_physiciansSubreport.textBoxWatermark1.Style.Font.Size = new Unit(50);

			var bloodVolumeReportSource = new InstanceReportSource { ReportDocument = bloodVolumeAnalysisSubReport };
			_physiciansSubreport.BloodVolumeAnalysisSubreport1.ReportSource = bloodVolumeReportSource;

			var subReport = new SubReport
			{
				Location = new PointU(unitX, unitY),
				Size = new SizeU(Unit.Inch(7.94), _physiciansSubreport.detail1.Height)
			};

			unitY = unitY.Add(_physiciansSubreport.detail1.Height);
			subReport.ReportSource = new InstanceReportSource { ReportDocument = _physiciansSubreport };
			return subReport;
		}
		#endregion
	}
}
