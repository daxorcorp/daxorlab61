﻿using System;
using System.Drawing;
using System.Xml.Linq;
using Daxor.Lab.BVA.Reports.ReportModels;
using Microsoft.Practices.Composite.Logging;
using Telerik.Reporting.Charting.Styles;
using Telerik.Reporting.Drawing;

namespace Daxor.Lab.BVA.Reports.ReportControllers
{
    public class BloodVolumeAnalysisSubReportController
	{
		#region Fields

		private readonly BloodVolumeAnalysisSubReportModel _bloodVolumeAnalysisSubReportModel;
		private readonly Color _amputeeWarningColor = Color.LightGray;
        private readonly ILoggerFacade _logger;
        private readonly SubReport_BloodVolumeAnalysis _bloodVolumeAnalysisReport;

        #endregion

        #region Ctor

        public BloodVolumeAnalysisSubReportController(BloodVolumeAnalysisSubReportModel bloodVolumeAnalysisSubReportModel, ILoggerFacade logger)
        {
            _bloodVolumeAnalysisSubReportModel = bloodVolumeAnalysisSubReportModel;
            _logger = logger;
            _bloodVolumeAnalysisReport = new SubReport_BloodVolumeAnalysis();
        }

        #endregion

		#region Properties

	    private bool PatientIsAmputee
	    {
		    get { return _bloodVolumeAnalysisSubReportModel.Test.Patient.IsAmputee; }
	    }

		#endregion

		#region Public API

		/// <summary>
        /// Creates and populates the BloodVolumeAnalysis sub report based on the BV bloodVolumeAnalysisSubReportModel associated with
        /// the report controller.
        /// </summary>
        /// <returns>A refference to the BloodVolume Analysis sub report.</returns>
        public SubReport_BloodVolumeAnalysis LoadBloodVolumeAnalysis()
        {
            _bloodVolumeAnalysisReport.textBoxBVAPlasmaVolume.Value = _bloodVolumeAnalysisSubReportModel.GenerateMeasuredPlasmaVolumeText();
            _bloodVolumeAnalysisReport.textBoxBVARBCVolume.Value = _bloodVolumeAnalysisSubReportModel.GenerateMeasuredRedCellVolumeText();
            _bloodVolumeAnalysisReport.textBoxTotalBloodVolumeResults.Value = _bloodVolumeAnalysisSubReportModel.GenerateMeasuredWholeBloodVolumeText();

            _bloodVolumeAnalysisReport.textBoxIdealPlasmaVolume.Value =
                _bloodVolumeAnalysisSubReportModel.GenerateIdealPlasmaVolumeText();

            if (_bloodVolumeAnalysisSubReportModel.IsIdealPlasmaCountOutOfRange())
                _bloodVolumeAnalysisReport.textBoxIdealPlasmaVolume.Style.TextAlign = HorizontalAlign.Center;

            _bloodVolumeAnalysisReport.textBoxIdealRBC.Value = _bloodVolumeAnalysisSubReportModel.GenerateIdealRedBloodCellCountText();

            if (_bloodVolumeAnalysisSubReportModel.IsIdealRedCellCountOutOfRange())
                _bloodVolumeAnalysisReport.textBoxIdealRBC.Style.TextAlign = HorizontalAlign.Center;

            _bloodVolumeAnalysisReport.textBoxTotalPatientIdeal.Value = _bloodVolumeAnalysisSubReportModel.GenerateIdealWholeBloodCountText();

            if (_bloodVolumeAnalysisSubReportModel.IsIdealWholeBloodCountOutOfRange())
                _bloodVolumeAnalysisReport.textBoxTotalPatientIdeal.Style.TextAlign = HorizontalAlign.Center;

            _bloodVolumeAnalysisReport.textBoxTotalExcessDeficit.Value = _bloodVolumeAnalysisSubReportModel.GenerateTotalExessDeficitText();

            if (_bloodVolumeAnalysisSubReportModel.IsTotalExcessDeficitOutOfRange())
                _bloodVolumeAnalysisReport.textBoxTotalExcessDeficit.Style.TextAlign = HorizontalAlign.Center;

            _bloodVolumeAnalysisReport.textBoxRBCExcessDeficit.Value = _bloodVolumeAnalysisSubReportModel.GenerateRbcExcessDeficitText();

            if (_bloodVolumeAnalysisSubReportModel.IsRbcExcessDeficitOutOfRange())
                _bloodVolumeAnalysisReport.textBoxRBCExcessDeficit.Style.TextAlign = HorizontalAlign.Center;

            _bloodVolumeAnalysisReport.textBoxPlasmaExcessDeficit.Value = _bloodVolumeAnalysisSubReportModel.GeneratePlasmaExcessDeficitText();

            if (_bloodVolumeAnalysisSubReportModel.IsPlasmaExcessDeficitOutOfRange())
                _bloodVolumeAnalysisReport.textBoxPlasmaExcessDeficit.Style.TextAlign = HorizontalAlign.Center;

            #region BV Deviation from Ideal

            _bloodVolumeAnalysisReport.textBoxTotalDeviationFromIdeal.Value = _bloodVolumeAnalysisSubReportModel.GenerateTotalDeviationFromIdealText();
            _bloodVolumeAnalysisReport.textBoxRBCDeviationFromIdeal.Value = _bloodVolumeAnalysisSubReportModel.GenerateRbcDeviationFromIdealText();
            _bloodVolumeAnalysisReport.textBoxPlasmaDeviationFromIdeal.Value = _bloodVolumeAnalysisSubReportModel.GeneratePlasmaDeviationFromIdealText();

            if (_bloodVolumeAnalysisSubReportModel.AreBloodVolumesOutOfRange())
            {
                _bloodVolumeAnalysisReport.textBoxTotalDeviationFromIdeal.Style.TextAlign = HorizontalAlign.Center;
                _bloodVolumeAnalysisReport.textBoxRBCDeviationFromIdeal.Style.TextAlign = HorizontalAlign.Center;
                _bloodVolumeAnalysisReport.textBoxPlasmaDeviationFromIdeal.Style.TextAlign = HorizontalAlign.Center;
            }

            #endregion

            #region Blood Chart

            try
            {
                if (_bloodVolumeAnalysisSubReportModel.IsTotalBloodVolumeInRange())
                    _bloodVolumeAnalysisSubReportModel.MakeBloodBarsChart(_bloodVolumeAnalysisReport.chartBloodBars);
                else
                    _bloodVolumeAnalysisReport.chartBloodBars.PlotArea.EmptySeriesMessage.TextBlock.Text = @"Out Of Range";
            }
            catch (Exception ex)
            {
                _logger.Log(ex.Message, Category.Exception, Priority.High);
            }

            #endregion

			if (PatientIsAmputee)
			{
				_bloodVolumeAnalysisReport.textBoxIdealPlasmaVolume.Style.BackgroundColor = _amputeeWarningColor;
				_bloodVolumeAnalysisReport.textBoxIdealRBC.Style.BackgroundColor = _amputeeWarningColor;
				_bloodVolumeAnalysisReport.textBoxPlasmaDeviationFromIdeal.Style.BackgroundColor = _amputeeWarningColor;
				_bloodVolumeAnalysisReport.textBoxPlasmaExcessDeficit.Style.BackgroundColor = _amputeeWarningColor;
				_bloodVolumeAnalysisReport.textBoxRBCDeviationFromIdeal.Style.BackgroundColor = _amputeeWarningColor;
				_bloodVolumeAnalysisReport.textBoxRBCExcessDeficit.Style.BackgroundColor = _amputeeWarningColor;
				_bloodVolumeAnalysisReport.textBoxTotalDeviationFromIdeal.Style.BackgroundColor = _amputeeWarningColor;
				_bloodVolumeAnalysisReport.textBoxTotalExcessDeficit.Style.BackgroundColor = _amputeeWarningColor;
				_bloodVolumeAnalysisReport.textBoxTotalPatientIdeal.Style.BackgroundColor = _amputeeWarningColor;

				//_bloodVolumeAnalysisReport.chartBloodBars.PlotArea.Appearance.FillStyle.MainColor = _amputeeWarningColor;

				_bloodVolumeAnalysisReport.chartBloodBars.PlotArea.Appearance.FillStyle.FillType = FillType.Image;
				_bloodVolumeAnalysisReport.chartBloodBars.PlotArea.Appearance.FillStyle.FillSettings.BackgroundImage =
					@"C:\ProgramData\Daxor\Lab\Images\AmputeeBackground.png";
				_bloodVolumeAnalysisReport.chartBloodBars.PlotArea.Appearance.FillStyle.FillSettings.ImageDrawMode =
					ImageDrawMode.Stretch;
			}
			else
			{
				_bloodVolumeAnalysisReport.chartBloodBars.PlotArea.Appearance.FillStyle.FillType = FillType.Solid;
				_bloodVolumeAnalysisReport.chartBloodBars.PlotArea.Appearance.FillStyle.MainColor = Color.White;
			}

            return _bloodVolumeAnalysisReport;
        }

        /// <summary>
        /// Creates and populates the BloodVolumeAnalysis element on the xml reprot based on the BV bloodVolumeAnalysisSubReportModel associated with
        /// the report controller.
        /// </summary>
        /// <returns>A refference to the BloodVolumeAnalysis element.</returns>
        public XElement GetBloodVolumeAnalysisElement()
        {
            return _bloodVolumeAnalysisSubReportModel.GenerateBloodVolumeAnalysisElement();
        }

        #endregion
    }
}
