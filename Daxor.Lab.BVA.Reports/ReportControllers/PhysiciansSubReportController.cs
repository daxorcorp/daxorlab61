﻿using Daxor.Lab.BVA.Reports.ReportModels;
using Daxor.Lab.SettingsManager.Common;
using System.Xml.Linq;
using Daxor.Lab.SettingsManager.Interfaces;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;

namespace Daxor.Lab.BVA.Reports.ReportControllers
{
	public class PhysiciansSubReportController
	{
		#region Fields

		private readonly PhysiciansSubReportModel _physiciansSubReportModel;
		private readonly SubReport_Physician _physiciansSubreport;
		private readonly ISettingsManager _settingsManager;

		#endregion

		#region Ctor

		public PhysiciansSubReportController(PhysiciansSubReportModel physiciansSubReportModel)
		{
			_physiciansSubReportModel = physiciansSubReportModel;
			_physiciansSubreport = new SubReport_Physician();
			_settingsManager = _physiciansSubReportModel.SettingsManager;
		}

		#endregion

		#region Public API

		/// <summary>
		/// Creates and populates the Physicians' sub report based on the BV test associated with
		/// the report controller.
		/// </summary>
		public SubReport LoadPhysicianReportData(SubReport_BloodVolumeAnalysis bloodVolumeAnalysisSubReport, ref Unit unitX, ref Unit unitY, bool printing)
		{
			_physiciansSubreport.textBoxWatermark.Value = _physiciansSubReportModel.DetermineWatermarkText();

			if (_physiciansSubReportModel.IsTrainingTest)
				_physiciansSubreport.textBoxWatermark.Style.Font.Size = new Unit(50);

			var bloodVolumeReportSource = new InstanceReportSource { ReportDocument = bloodVolumeAnalysisSubReport };
			_physiciansSubreport.BloodVolumeAnalysisSubreport.ReportSource = bloodVolumeReportSource;

			_physiciansSubreport.textBoxNormalizedHct.Value = _physiciansSubReportModel.GenerateNormalizedHct();

			if (_physiciansSubReportModel.IsGenderMale())
			{
				_physiciansSubreport.textBoxNormalNHct.Value = _settingsManager.GetSetting<string>(SettingKeys.BvaNormalizedHematocritRangeMale);
				_physiciansSubreport.textBoxNormalPVHct.Value = _settingsManager.GetSetting<string>(SettingKeys.BvaHematocritRangeMale);
				_physiciansSubreport.textBoxNormalGender.Value = @"Normal Male";

			}
			else
			{
				_physiciansSubreport.textBoxNormalNHct.Value = _settingsManager.GetSetting<string>(SettingKeys.BvaNormalizedHematocritRangeFemale);
				_physiciansSubreport.textBoxNormalPVHct.Value = _settingsManager.GetSetting<string>(SettingKeys.BvaHematocritRangeFemale);
				_physiciansSubreport.textBoxNormalGender.Value = @"Normal Female";
			}

			_physiciansSubreport.textBoxPeripheralHct.Value = _physiciansSubReportModel.GeneratePeripheralHct();
			_physiciansSubreport.textBoxTransudation.Value = _physiciansSubReportModel.GenerateTransudationRate();
			_physiciansSubreport.textBoxTotalVolumeMLkg.Value = _physiciansSubReportModel.GenerateMeasuredWholeCountMlkgAnalysisElement();
			_physiciansSubreport.textBoxTotalVolumeMLkgIdeal.Value = _physiciansSubReportModel.GenerateIdealWholeCountMlkgAnalysisElement();
			_physiciansSubreport.textBoxRBCMLkg.Value = _physiciansSubReportModel.GenerateMeasuredRbCountMlkgAnalysisElement();
			_physiciansSubreport.textBoxRBCMLKgIdeal.Value = _physiciansSubReportModel.GenerateIdealRbCountMlkgAnalysisElement();
			_physiciansSubreport.textBoxPlasmaMLkg.Value = _physiciansSubReportModel.GenerateMeasuredPlasmaMlkgAnalysisElement();
			_physiciansSubreport.textBoxPlasmaMLkgIdeal.Value = _physiciansSubReportModel.GenerateIdealPlasmaMlkgAnalysisElement();

			_physiciansSubreport.textBoxTechnicalNotes.Value = _physiciansSubReportModel.AddTechnicalNotesData();
			_physiciansSubreport.textBoxReportFindings.Value = _physiciansSubReportModel.AddReportFindingsData();

			if (printing)
				_physiciansSubreport.amputeeColorExample.Location = new PointU(Unit.Inch(3.425D), Unit.Inch(6.65D));

			if (!_physiciansSubReportModel.ShowFindings)
				_physiciansSubreport.panel_ReportFindings.Visible = false;

			var subReport = new SubReport
			{
				Location = new PointU(unitX, unitY),
				Size = new SizeU(Unit.Inch(7.94), _physiciansSubreport.detail.Height)
			};

			unitY = unitY.Add(_physiciansSubreport.detail.Height);
			subReport.ReportSource = new InstanceReportSource { ReportDocument = _physiciansSubreport };
			return subReport;
		}

		/// <summary>
		/// Creates and populates the AdditionalAnalysis element on the xml report based on the BV test associated with
		/// the report controller.
		/// </summary>
		/// <returns>A refference to the AddiditonalAnalysis xml element.</returns>
		public XElement GetAdditionalAnalysisElement()
		{
			return _physiciansSubReportModel.GenerateAdditionalAnalysisElement();
		}

		/// <summary>
		/// Creates and populates the PhysiciansReport element on the xml report based on the BV test associated with
		/// the report controller.
		/// </summary>
		/// <returns>A refference to the PhysiciansReport element.</returns>
		public XElement GetPhysiciansReportElement()
		{
			return _physiciansSubReportModel.GeneratePhysiciansReportElement();
		}

		#endregion
	}
}
