﻿using System;
using System.Drawing;
using System.Xml.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Reports.ReportModels;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Telerik.Reporting.Charting.Styles;
using Telerik.Reporting.Drawing;
using Telerik.Reporting.Expressions;

namespace Daxor.Lab.BVA.Reports.ReportControllers
{
    public class BloodVolumeAnalysisSubReportQuickReportController
	{
		#region Fields

		private readonly BloodVolumeAnalysisSubReportQuickReportModel _bloodVolumeAnalysisSubReportQuickReportModel;
		private readonly Color _amputeeWarningColor = Color.LightGray;
		private readonly ILoggerFacade _logger;
		private readonly SubReport_BloodVolumeAnalysisQuickReport _bloodVolumeAnalysisReportQuickReport;

        #endregion

        #region Ctor

        public BloodVolumeAnalysisSubReportQuickReportController(BloodVolumeAnalysisSubReportQuickReportModel bloodVolumeAnalysisSubReportQuickReportModel, ILoggerFacade logger)
        {
            _bloodVolumeAnalysisSubReportQuickReportModel = bloodVolumeAnalysisSubReportQuickReportModel;
            _logger = logger;
			_bloodVolumeAnalysisReportQuickReport = new SubReport_BloodVolumeAnalysisQuickReport();
        }

        #endregion

		#region Properties

	    private bool PatientIsAmputee
	    {
		    get { return _bloodVolumeAnalysisSubReportQuickReportModel.Test.Patient.IsAmputee; }
	    }

		#endregion

		#region Public API

		/// <summary>
        /// Creates and populates the BloodVolumeAnalysis sub report based on the BV bloodVolumeAnalysisSubReportModel associated with
        /// the report controller
        /// </summary>
        /// <returns>A refference to the BloodVolume Analysis sub report.</returns>
        public SubReport_BloodVolumeAnalysisQuickReport LoadBloodVolumeAnalysis()
		{

			_bloodVolumeAnalysisReportQuickReport.textBoxNormalizedHct.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GenerateNormalizedHct();
			_bloodVolumeAnalysisReportQuickReport.textBoxPeripheralHct.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GeneratePeripheralHct();
			_bloodVolumeAnalysisReportQuickReport.textBoxAlbumanTransudation.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GenerateTransudationRate();

            _bloodVolumeAnalysisReportQuickReport.textBoxBVAPlasmaVolumeQR.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GenerateMeasuredPlasmaVolumeText();
            _bloodVolumeAnalysisReportQuickReport.textBoxBVARBCVolumeQR.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GenerateMeasuredRedCellVolumeText();
            _bloodVolumeAnalysisReportQuickReport.textBoxTotalBloodVolumeResultsQR.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GenerateMeasuredWholeBloodVolumeText();

			//_bloodVolumeAnalysisReportQuickReport.textBoxTotalExcessDeficitQR.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GenerateTotalExessDeficitText();
			_bloodVolumeAnalysisReportQuickReport.textBoxTotalExcessDeficitQR.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GenerateTotalExessDeficit() + "%";

			//_bloodVolumeAnalysisReportQuickReport.textBoxRBCExcessDeficitQR.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GenerateRbcExcessDeficitText();
			_bloodVolumeAnalysisReportQuickReport.textBoxRBCExcessDeficitQR.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GenerateRbcExcessDeficit() + "%";

			//_bloodVolumeAnalysisReportQuickReport.textBoxPlasmaExcessDeficitQR.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GeneratePlasmaExcessDeficitText();
			_bloodVolumeAnalysisReportQuickReport.textBoxPlasmaExcessDeficitQR.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GeneratePlasmaExcessDeficit() + "%";

            if (_bloodVolumeAnalysisSubReportQuickReportModel.IsTotalExcessDeficitOutOfRange())
                _bloodVolumeAnalysisReportQuickReport.textBoxTotalExcessDeficitQR.Style.TextAlign = HorizontalAlign.Center;

			// Shawn Urban added per Micheal's request for a flag column. This grabs the value and formats to a double to see what category it fits in.
			var totalExcessDeficit = _bloodVolumeAnalysisSubReportQuickReportModel.GenerateTotalExessDeficit();
			_bloodVolumeAnalysisReportQuickReport.totalBloodVolumeFlag.Value = DetermineFlag(totalExcessDeficit);

			var rBcExcessDeficitQr = _bloodVolumeAnalysisSubReportQuickReportModel.GenerateRbcExcessDeficit();
			_bloodVolumeAnalysisReportQuickReport.redBloodCellVolumeFlag.Value = DetermineFlag(rBcExcessDeficitQr);

			var plasmaExcessDeficitQr = _bloodVolumeAnalysisSubReportQuickReportModel.GeneratePlasmaExcessDeficit();
			_bloodVolumeAnalysisReportQuickReport.plasmaVolumeFlag.Value = DetermineFlag(plasmaExcessDeficitQr);

			

            if (_bloodVolumeAnalysisSubReportQuickReportModel.IsRbcExcessDeficitOutOfRange())
                _bloodVolumeAnalysisReportQuickReport.textBoxRBCExcessDeficitQR.Style.TextAlign = HorizontalAlign.Center;

            

            if (_bloodVolumeAnalysisSubReportQuickReportModel.IsPlasmaExcessDeficitOutOfRange())
                _bloodVolumeAnalysisReportQuickReport.textBoxPlasmaExcessDeficitQR.Style.TextAlign = HorizontalAlign.Center;

            #region BV Deviation from Ideal

            _bloodVolumeAnalysisReportQuickReport.textBoxTotalDeviationFromIdealQR.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GenerateTotalDeviationFromIdealText();
            _bloodVolumeAnalysisReportQuickReport.textBoxRBCDeviationFromIdealQR.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GenerateRbcDeviationFromIdealText();
            _bloodVolumeAnalysisReportQuickReport.textBoxPlasmaDeviationFromIdealQR.Value = _bloodVolumeAnalysisSubReportQuickReportModel.GeneratePlasmaDeviationFromIdealText();

            if (_bloodVolumeAnalysisSubReportQuickReportModel.AreBloodVolumesOutOfRange())
            {
                _bloodVolumeAnalysisReportQuickReport.textBoxTotalDeviationFromIdealQR.Style.TextAlign = HorizontalAlign.Center;
                _bloodVolumeAnalysisReportQuickReport.textBoxRBCDeviationFromIdealQR.Style.TextAlign = HorizontalAlign.Center;
                _bloodVolumeAnalysisReportQuickReport.textBoxPlasmaDeviationFromIdealQR.Style.TextAlign = HorizontalAlign.Center;
            }

            #endregion

			if (PatientIsAmputee)
			{
                _bloodVolumeAnalysisReportQuickReport.textBoxPlasmaDeviationFromIdealQR.Style.BackgroundColor = _amputeeWarningColor;
                _bloodVolumeAnalysisReportQuickReport.textBoxPlasmaExcessDeficitQR.Style.BackgroundColor = _amputeeWarningColor;
                _bloodVolumeAnalysisReportQuickReport.textBoxRBCDeviationFromIdealQR.Style.BackgroundColor = _amputeeWarningColor;
                _bloodVolumeAnalysisReportQuickReport.textBoxRBCExcessDeficitQR.Style.BackgroundColor = _amputeeWarningColor;
                _bloodVolumeAnalysisReportQuickReport.textBoxTotalDeviationFromIdealQR.Style.BackgroundColor = _amputeeWarningColor;
                _bloodVolumeAnalysisReportQuickReport.textBoxTotalExcessDeficitQR.Style.BackgroundColor = _amputeeWarningColor;

				//_bloodVolumeAnalysisReportQuickReport.chartBloodBars.PlotArea.Appearance.FillStyle.MainColor = _amputeeWarningColor;
			}
			else
			{
				return _bloodVolumeAnalysisReportQuickReport;
			}

            return _bloodVolumeAnalysisReportQuickReport;
        }

        /// <summary>
        /// Creates and populates the BloodVolumeAnalysis element on the xml reprot based on the BV bloodVolumeAnalysisSubReportModel associated with
        /// the report controller.
        /// </summary>
        /// <returns>A refference to the BloodVolumeAnalysis element.</returns>
        public XElement GetBloodVolumeAnalysisElement()
        {
            return _bloodVolumeAnalysisSubReportQuickReportModel.GenerateBloodVolumeAnalysisElement();
        }

		

        #endregion

		#region Methods
		private string DetermineFlag(string field)
		{
			var result = "";
			var fieldTrimmed = field.TrimStart('+', '-');
			var totalExcessDeficitParsed = Double.Parse(fieldTrimmed);
			if (totalExcessDeficitParsed >= 8){ result = "High"; }
			else if (totalExcessDeficitParsed >= -8){ result = "Low"; }
			else{ result = " "; }
			return result;
		}
		#endregion
	}
}
