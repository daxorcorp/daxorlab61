﻿using Daxor.Lab.BVA.Reports.ReportModels;
using System.Linq;
using System.Xml.Linq;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;

namespace Daxor.Lab.BVA.Reports.ReportControllers
{
    public class AuditTrailSubReportController
    {
        #region Fields

        private readonly AuditTrailSubReportModel _auditTrailSubReportModel;
        private readonly SubReport_AuditTrail _auditTrailReport;

        #endregion

        #region Ctor

        public AuditTrailSubReportController(AuditTrailSubReportModel auditTrailSubReportModel)
        {
            _auditTrailSubReportModel = auditTrailSubReportModel;
            _auditTrailReport = new SubReport_AuditTrail();
        }

        #endregion

        #region Public API

        /// <summary>Creates and populates the audit trail sub report based on the BV test associated with
        /// the report controller.
        /// </summary>
        public SubReport LoadAuditTrailData(ref Unit unitX, ref Unit unitY)
        {
            _auditTrailReport.tableAuditTrail.DataSource = _auditTrailSubReportModel.AuditRecordList.ToList();

            _auditTrailReport.textBoxWatermark.Value = _auditTrailSubReportModel.DetermineWatermarkText();

            if (_auditTrailSubReportModel.IsTrainingTest)
                _auditTrailReport.textBoxWatermark.Style.Font.Size = new Unit(50);

            var subReport = new SubReport
            {
                Location = new PointU(unitX, unitY),
                Size = new SizeU(Unit.Inch(7.94), _auditTrailReport.detail.Height)
            };

            unitY = unitY.Add(_auditTrailReport.detail.Height);
            subReport.ReportSource = new InstanceReportSource {ReportDocument = _auditTrailReport};
            return subReport;
        }

        /// <summary>
        /// Creates and populates the AuditTrailReport element on the xml report based on the BV test associated with
        /// the report controller.
        /// </summary>
        /// <returns>A refference to the AuditTrailReport element.</returns>
        public XElement GetAuditTrailReportElement()
        {
            return _auditTrailSubReportModel.GenerateAuditTrailReportElement();
        }

        #endregion
    }
}
