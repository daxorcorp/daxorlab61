﻿using Daxor.Lab.BVA.Reports.Common;
using Daxor.Lab.BVA.Reports.ReportModels;
using Microsoft.Practices.Composite.Logging;
using System;
using System.Linq;
using System.Xml.Linq;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;
using Unit = Telerik.Reporting.Drawing.Unit;

namespace Daxor.Lab.BVA.Reports.ReportControllers
{
    public class AnalystSubReportController
    {
        #region Fields

        private readonly AnalystSubReportModel _analystSubReportModel;
        private readonly SubReport_Analyst _analystsSubReport;
        private readonly ILoggerFacade _logger;

        private const string OutOfRangeString = "Out Of Range";

        #endregion

        #region Ctor

        public AnalystSubReportController(AnalystSubReportModel analystSubReportModel, ILoggerFacade logger)
        {
            _analystSubReportModel = analystSubReportModel;
            _logger = logger;
            _analystsSubReport = new SubReport_Analyst();
        }

        #endregion

        #region Public API
        
        /// <summary>Creates and populates the analysts' sub report based on the BV test associated with
        /// the report controller.
        /// </summary>
        public SubReport LoadAnalystReportData(SubReport_BloodVolumeAnalysis bloodVolumeAnalysisSubReport, ref Unit unitX, ref Unit unitY, bool printing)
        {
            var bloodVolumeReportSource = new InstanceReportSource {ReportDocument = bloodVolumeAnalysisSubReport};
            _analystsSubReport.BloodVolumeAnalysisSubReport.ReportSource = bloodVolumeReportSource;
            
            _analystsSubReport.textBoxTransudation.Value = _analystSubReportModel.GenerateTransudationRate();
            _analystsSubReport.textBoxStandardDeviation.Value = _analystSubReportModel.GenerateStandardDeviationText();
            _analystsSubReport.textBoxFinalBloodVolume.Value = _analystSubReportModel.GenerateFinalBloodVolume();
            _analystsSubReport.textBoxSlope.Value = _analystSubReportModel.GenerateTransudationSlope();

			if (printing)
				_analystsSubReport.amputeeColorExample.Location = new PointU(Unit.Inch(1.95D), Unit.Inch(6.76D));

            try
            {
                if (_analystSubReportModel.IsTotalBloodVolumeInRange())
                {
                    _analystsSubReport.regChart.ChartTitle.Visible = false;
                    _analystsSubReport.regChart.PlotArea.Visible = true;
                    _analystSubReportModel.MakeRegressionGraph(_analystsSubReport.regChart);
                }
                else
                {
                    _analystsSubReport.regChart.ChartTitle.TextBlock.Text = OutOfRangeString;
                }
            }
            catch (Exception ex)
            {
                _logger.Log(ex.Message, Category.Exception, Priority.High);
            }

            #region Patient Sample Counts Section

            #region Background/Dose Data

            _analystsSubReport.textBoxTestMode.Value = _analystSubReportModel.GenerateAnalystTestModeString();
            _analystsSubReport.textBoxBackgroundCount.Value = _analystSubReportModel.GenerateBackgroundCountText();
            _analystsSubReport.textBoxSampleAcquisitionTime.Value = _analystSubReportModel.GenerateSampleAcquisitionTimeText();
            _analystsSubReport.textBoxDose.Value = _analystSubReportModel.GenerateDoseText();
            _analystsSubReport.textBoxTubeType.Value = _analystSubReportModel.GenerateTubeTypeText();
            _analystsSubReport.textBoxWatermark.Value = _analystSubReportModel.DetermineWatermarkText();

            if (_analystSubReportModel.IsTrainingTest)
            {
                _analystsSubReport.textBoxWatermark.Style.Font.Size = new Unit(50);
            }

            ReportUtilities.BoldIfInAuditTrail(_analystsSubReport.textBoxBackgroundCount, "BACKGROUND_COUNT", _analystSubReportModel.AuditRecordList);
            ReportUtilities.BoldIfInAuditTrail(_analystsSubReport.textBoxTubeType, "TUBE_ID", _analystSubReportModel.AuditRecordList);
            ReportUtilities.BoldIfInAuditTrail(_analystsSubReport.textBoxDose, "DOSE", _analystSubReportModel.AuditRecordList);

            #endregion

            #region Patient Samples

            _analystsSubReport.textBoxLowStandardsWarning.Value = _analystSubReportModel.GenerateLowStandardsWarningText();

            var fieldList = _analystSubReportModel.GenerateSampleCountsFieldList();

            _analystsSubReport.tableSampleCounts.DataSource = fieldList;

            foreach (var a in fieldList)
            {
                a.SampleCountAInAuditTrail = IsSampleFieldInAuditTrail(a.SampleAId, "COUNTS");
                a.SampleCountBInAuditTrail = IsSampleFieldInAuditTrail(a.SampleBId, "COUNTS");
                a.SampleHctAInAuditTrail = IsSampleFieldInAuditTrail(a.SampleAId, "HEMATOCRIT");
                a.SampleHctBInAuditTrail = IsSampleFieldInAuditTrail(a.SampleBId, "HEMATOCRIT");

                a.SampleTimeInAuditTrail = IsSampleFieldInAuditTrail(a.SampleAId, "TIME_POST_INJECTION") ||
                    IsSampleFieldInAuditTrail(a.SampleBId, "TIME_POST_INJECTION");
            }

            #endregion

            #endregion

            var subReport = new SubReport
            {
                Location = new PointU(unitX, unitY),
                Size = new SizeU(Unit.Inch(7.94), _analystsSubReport.detail.Height)
            };

            unitY = unitY.Add(_analystsSubReport.detail.Height);
            subReport.ReportSource = new InstanceReportSource {ReportDocument = _analystsSubReport};
            return subReport;
        }

        public XElement GetPatientSampleCountsElement()
        {
            return _analystSubReportModel.GeneratePatientSampleCountsElement();
        }

        public XElement GetAnalystReportElement()
        {
            return _analystSubReportModel.GenerateAnalystReportElement();
        }
        
        #endregion

        #region Private Methods

        private bool IsSampleFieldInAuditTrail(Guid guid, string fieldName)
        {
            var field = from record in _analystSubReportModel.AuditRecordList
                        where record.NotFriendlyChangeField == fieldName && record.SampleId == guid
                        select record;

            return field.Any();
        }
      
        #endregion
    }
}
