namespace Daxor.Lab.BVA.Reports
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for BloodVolumeAnalysisSubReport.
    /// </summary>
    public partial class SubReport_BloodVolumeAnalysisQuickReport : Telerik.Reporting.Report
    {
        public SubReport_BloodVolumeAnalysisQuickReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}