﻿using System.IO;
using Daxor.Lab.BVA.Reports.Interfaces;
using Telerik.Windows.Controls;
using Telerik.Windows.Documents.Fixed.FormatProviders;
using Telerik.Windows.Documents.Fixed.FormatProviders.Pdf;
using Telerik.Windows.Documents.Fixed.Print;

namespace Daxor.Lab.BVA.Reports
{
	public class PdfPrinter : IPdfPrinter
	{
		/// <summary>
		/// Prints the pdf given in the filePath using Telerik's RadPdfViewer.
		/// Uses the default printer.
		/// </summary>
		/// <param name="filePath">Path to the pdf file to print.</param>
		/// <remarks>This method will run in the background w/o user prompts.</remarks>
		public void Print(string filePath)
		{
			var fileInfo = new FileInfo(filePath);
			var stream = fileInfo.OpenRead();
			var provider = new PdfFormatProvider(stream, FormatProviderSettings.ReadOnDemand);
			var document = provider.Import();
			var viewer = new RadPdfViewer { Document = document };
			var settings = new PrintSettings { UseDefaultPrinter = true };
			viewer.Print(settings);
		}
	}
}
