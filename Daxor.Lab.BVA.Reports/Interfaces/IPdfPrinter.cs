﻿namespace Daxor.Lab.BVA.Reports.Interfaces
{
	public interface IPdfPrinter
	{
		void Print(string filePath);
	}
}
