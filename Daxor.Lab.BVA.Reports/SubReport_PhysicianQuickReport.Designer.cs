namespace Daxor.Lab.BVA.Reports
{
	partial class SubReport_PhysicianQuickReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			Telerik.Reporting.InstanceReportSource instanceReportSource1 = new Telerik.Reporting.InstanceReportSource();
			this.detail1 = new Telerik.Reporting.DetailSection();
			this.BloodVolumeAnalysisSubreport1 = new Telerik.Reporting.SubReport();
			this.textBoxWatermark1 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail1
			// 
			this.detail1.Height = Telerik.Reporting.Drawing.Unit.Inch(1.9200390577316284D);
			this.detail1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.BloodVolumeAnalysisSubreport1,
            this.textBoxWatermark1});
			this.detail1.Name = "detail1";
			this.detail1.PageBreak = Telerik.Reporting.PageBreak.None;
			// 
			// BloodVolumeAnalysisSubreport1
			// 
			this.BloodVolumeAnalysisSubreport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.019999999552965164D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.BloodVolumeAnalysisSubreport1.Name = "BloodVolumeAnalysisSubreport1";
			instanceReportSource1.ReportDocument = null;
			this.BloodVolumeAnalysisSubreport1.ReportSource = instanceReportSource1;
			this.BloodVolumeAnalysisSubreport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.880000114440918D), Telerik.Reporting.Drawing.Unit.Inch(1.9199995994567871D));
			// 
			// textBoxWatermark1
			// 
			this.textBoxWatermark1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D));
			this.textBoxWatermark1.Name = "textBoxWatermark1";
			this.textBoxWatermark1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.5D), Telerik.Reporting.Drawing.Unit.Inch(1.9199999570846558D));
			this.textBoxWatermark1.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.textBoxWatermark1.Style.Font.Bold = true;
			this.textBoxWatermark1.Style.Font.Name = "Times New Roman";
			this.textBoxWatermark1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(50D);
			this.textBoxWatermark1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxWatermark1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxWatermark1.Value = "";
			// 
			// SubReport_PhysicianQuickReport
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail1});
			this.Name = "SubReport_Physician";
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.9000000953674316D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

		public Telerik.Reporting.TextBox textBoxWatermark1;
        public Telerik.Reporting.SubReport BloodVolumeAnalysisSubreport1;
		public Telerik.Reporting.DetailSection detail1;
		public Telerik.Reporting.TextBox textBoxPeripheralHct;
		public Telerik.Reporting.TextBox textBoxNormalizedHct;
		public Telerik.Reporting.TextBox textBoxNormalPVHct;
		public Telerik.Reporting.TextBox textBoxNormalNHct;
		public Telerik.Reporting.TextBox textBoxNormalGender;
		public Telerik.Reporting.Panel panel_ReportFindings;
    }
}