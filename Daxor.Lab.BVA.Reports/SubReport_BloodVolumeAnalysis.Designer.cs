using System.Drawing;

namespace Daxor.Lab.BVA.Reports
{
    partial class SubReport_BloodVolumeAnalysis
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			Telerik.Reporting.Charting.Palette palette1 = new Telerik.Reporting.Charting.Palette();
			Telerik.Reporting.Charting.PaletteItem paletteItem1 = new Telerik.Reporting.Charting.PaletteItem();
			Telerik.Reporting.Charting.PaletteItem paletteItem2 = new Telerik.Reporting.Charting.PaletteItem();
			Telerik.Reporting.Charting.Styles.ChartPaddingsLegend chartPaddingsLegend1 = new Telerik.Reporting.Charting.Styles.ChartPaddingsLegend();
			Telerik.Reporting.Charting.Styles.ChartMarginsPlotArea chartMarginsPlotArea1 = new Telerik.Reporting.Charting.Styles.ChartMarginsPlotArea();
			Telerik.Reporting.Charting.Styles.ChartPaddings chartPaddings1 = new Telerik.Reporting.Charting.Styles.ChartPaddings();
			Telerik.Reporting.Charting.Styles.ChartPaddings chartPaddings2 = new Telerik.Reporting.Charting.Styles.ChartPaddings();
			Telerik.Reporting.Charting.Styles.ChartPaddings chartPaddings3 = new Telerik.Reporting.Charting.Styles.ChartPaddings();
			Telerik.Reporting.Charting.ChartAxisItem chartAxisItem1 = new Telerik.Reporting.Charting.ChartAxisItem();
			Telerik.Reporting.Charting.ChartAxisItem chartAxisItem2 = new Telerik.Reporting.Charting.ChartAxisItem();
			Telerik.Reporting.Charting.ChartAxisItem chartAxisItem3 = new Telerik.Reporting.Charting.ChartAxisItem();
			Telerik.Reporting.Charting.ChartAxisItem chartAxisItem4 = new Telerik.Reporting.Charting.ChartAxisItem();
			Telerik.Reporting.Charting.ChartAxisItem chartAxisItem5 = new Telerik.Reporting.Charting.ChartAxisItem();
			Telerik.Reporting.Charting.ChartAxisItem chartAxisItem6 = new Telerik.Reporting.Charting.ChartAxisItem();
			Telerik.Reporting.Charting.ChartAxisItem chartAxisItem7 = new Telerik.Reporting.Charting.ChartAxisItem();
			Telerik.Reporting.Charting.ChartAxisItem chartAxisItem8 = new Telerik.Reporting.Charting.ChartAxisItem();
			this.detail = new Telerik.Reporting.DetailSection();
			this.panelBloodVolumeAnalysisResults = new Telerik.Reporting.Panel();
			this.chartBloodBars = new Telerik.Reporting.Chart();
			this.shape4 = new Telerik.Reporting.Shape();
			this.textBoxPlasmaDeviationFromIdeal = new Telerik.Reporting.TextBox();
			this.textBoxRBCDeviationFromIdeal = new Telerik.Reporting.TextBox();
			this.textBoxTotalDeviationFromIdeal = new Telerik.Reporting.TextBox();
			this.textBoxIdealPlasmaVolume = new Telerik.Reporting.TextBox();
			this.textBoxIdealRBC = new Telerik.Reporting.TextBox();
			this.textBoxTotalPatientIdeal = new Telerik.Reporting.TextBox();
			this.textBoxBVAPlasmaVolume = new Telerik.Reporting.TextBox();
			this.textBoxBVARBCVolume = new Telerik.Reporting.TextBox();
			this.textBoxTotalBloodVolumeResults = new Telerik.Reporting.TextBox();
			this.textBox19 = new Telerik.Reporting.TextBox();
			this.textBox18 = new Telerik.Reporting.TextBox();
			this.textBox16 = new Telerik.Reporting.TextBox();
			this.textBox15 = new Telerik.Reporting.TextBox();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBoxPlasmaExcessDeficit = new Telerik.Reporting.TextBox();
			this.textBoxRBCExcessDeficit = new Telerik.Reporting.TextBox();
			this.panel2 = new Telerik.Reporting.Panel();
			this.shape5 = new Telerik.Reporting.Shape();
			this.shape2 = new Telerik.Reporting.Shape();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.textBox61 = new Telerik.Reporting.TextBox();
			this.textBox63 = new Telerik.Reporting.TextBox();
			this.textBox64 = new Telerik.Reporting.TextBox();
			this.textBox65 = new Telerik.Reporting.TextBox();
			this.textBox66 = new Telerik.Reporting.TextBox();
			this.textBox67 = new Telerik.Reporting.TextBox();
			this.textBox68 = new Telerik.Reporting.TextBox();
			this.textBox69 = new Telerik.Reporting.TextBox();
			this.textBox70 = new Telerik.Reporting.TextBox();
			this.textBox71 = new Telerik.Reporting.TextBox();
			this.textBoxTotalExcessDeficit = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(2.1749603748321533D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panelBloodVolumeAnalysisResults});
			this.detail.Name = "detail";
			// 
			// panelBloodVolumeAnalysisResults
			// 
			this.panelBloodVolumeAnalysisResults.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.chartBloodBars,
            this.shape4,
            this.textBoxPlasmaDeviationFromIdeal,
            this.textBoxRBCDeviationFromIdeal,
            this.textBoxTotalDeviationFromIdeal,
            this.textBoxIdealPlasmaVolume,
            this.textBoxIdealRBC,
            this.textBoxTotalPatientIdeal,
            this.textBoxBVAPlasmaVolume,
            this.textBoxBVARBCVolume,
            this.textBoxTotalBloodVolumeResults,
            this.textBox19,
            this.textBox18,
            this.textBox16,
            this.textBox15,
            this.textBox14,
            this.textBox13,
            this.textBox11,
            this.textBox7,
            this.textBoxPlasmaExcessDeficit,
            this.textBoxRBCExcessDeficit,
            this.panel2,
            this.textBoxTotalExcessDeficit});
			this.panelBloodVolumeAnalysisResults.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D));
			this.panelBloodVolumeAnalysisResults.Name = "panelBloodVolumeAnalysisResults";
			this.panelBloodVolumeAnalysisResults.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8400001525878906D), Telerik.Reporting.Drawing.Unit.Inch(2.1249604225158691D));
			// 
			// chartBloodBars
			// 
			this.chartBloodBars.Appearance.BarWidthPercent = new decimal(new int[] {
            50,
            0,
            0,
            0});
			this.chartBloodBars.Appearance.Border.Color = System.Drawing.Color.White;
			this.chartBloodBars.BitmapResolution = 96F;
			this.chartBloodBars.ChartTitle.Appearance.Visible = false;
			this.chartBloodBars.ChartTitle.Visible = false;
			paletteItem1.MainColor = System.Drawing.Color.Red;
			paletteItem1.Name = "RedCells";
			paletteItem1.SecondColor = System.Drawing.Color.Red;
			paletteItem2.MainColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			paletteItem2.Name = "Plasma";
			paletteItem2.SecondColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			palette1.Items.AddRange(new Telerik.Reporting.Charting.PaletteItem[] {
            paletteItem1,
            paletteItem2});
			palette1.Name = "Palette1";
			this.chartBloodBars.CustomPalettes.AddRange(new Telerik.Reporting.Charting.Palette[] {
            palette1});
			this.chartBloodBars.DefaultType = Telerik.Reporting.Charting.ChartSeriesType.StackedBar;
			this.chartBloodBars.ImageFormat = System.Drawing.Imaging.ImageFormat.Emf;
			chartPaddingsLegend1.Top = new Telerik.Reporting.Charting.Styles.Unit(10D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.chartBloodBars.Legend.Appearance.Dimensions.Paddings = chartPaddingsLegend1;
			this.chartBloodBars.Legend.Appearance.ItemTextAppearance.TextProperties.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.chartBloodBars.Legend.Appearance.Visible = false;
			this.chartBloodBars.Legend.Visible = false;
			this.chartBloodBars.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5208334922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.33337274193763733D));
			this.chartBloodBars.Name = "chartBloodBars";
			this.chartBloodBars.PlotArea.Appearance.Border.Color = System.Drawing.Color.Transparent;
			chartMarginsPlotArea1.Bottom = new Telerik.Reporting.Charting.Styles.Unit(15D, Telerik.Reporting.Charting.Styles.UnitType.Percentage);
			chartMarginsPlotArea1.Left = new Telerik.Reporting.Charting.Styles.Unit(15D, Telerik.Reporting.Charting.Styles.UnitType.Percentage);
			chartMarginsPlotArea1.Right = new Telerik.Reporting.Charting.Styles.Unit(10D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartMarginsPlotArea1.Top = new Telerik.Reporting.Charting.Styles.Unit(10D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.chartBloodBars.PlotArea.Appearance.Dimensions.Margins = chartMarginsPlotArea1;
			chartPaddings1.Bottom = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings1.Left = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings1.Right = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings1.Top = new Telerik.Reporting.Charting.Styles.Unit(10D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.chartBloodBars.PlotArea.Appearance.Dimensions.Paddings = chartPaddings1;
			this.chartBloodBars.PlotArea.Appearance.FillStyle.FillType = Telerik.Reporting.Charting.Styles.FillType.Solid;
			this.chartBloodBars.PlotArea.Appearance.FillStyle.MainColor = System.Drawing.Color.White;
			this.chartBloodBars.PlotArea.Appearance.FillStyle.SecondColor = System.Drawing.Color.White;
			this.chartBloodBars.PlotArea.Appearance.SeriesPalette = "Palette1";
			this.chartBloodBars.PlotArea.EmptySeriesMessage.Appearance.Visible = true;
			this.chartBloodBars.PlotArea.EmptySeriesMessage.TextBlock.Appearance.TextProperties.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold);
			this.chartBloodBars.PlotArea.EmptySeriesMessage.TextBlock.Text = "Insufficient Data";
			this.chartBloodBars.PlotArea.EmptySeriesMessage.Visible = true;
			this.chartBloodBars.PlotArea.XAxis.Appearance.LabelAppearance.Position.AlignedPosition = Telerik.Reporting.Charting.Styles.AlignedPositions.Left;
			this.chartBloodBars.PlotArea.XAxis.Appearance.MajorGridLines.Color = System.Drawing.Color.Black;
			this.chartBloodBars.PlotArea.XAxis.Appearance.MajorGridLines.Visible = false;
			this.chartBloodBars.PlotArea.XAxis.Appearance.MajorTick.Color = System.Drawing.Color.Black;
			this.chartBloodBars.PlotArea.XAxis.Appearance.TextAppearance.Border.Color = System.Drawing.Color.Transparent;
			chartPaddings2.Left = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings2.Top = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.chartBloodBars.PlotArea.XAxis.Appearance.TextAppearance.Dimensions.Paddings = chartPaddings2;
			this.chartBloodBars.PlotArea.XAxis.Appearance.TextAppearance.Position.AlignedPosition = Telerik.Reporting.Charting.Styles.AlignedPositions.Center;
			this.chartBloodBars.PlotArea.XAxis.Appearance.TextAppearance.TextProperties.Color = System.Drawing.Color.Black;
			this.chartBloodBars.PlotArea.XAxis.Appearance.TextAppearance.TextProperties.Font = new System.Drawing.Font("Times New Roman", 7F);
			this.chartBloodBars.PlotArea.XAxis.AutoScale = false;
			this.chartBloodBars.PlotArea.XAxis.AxisLabel.Appearance.Border.Color = System.Drawing.Color.Black;
			this.chartBloodBars.PlotArea.XAxis.AxisLabel.Appearance.Dimensions.AutoSize = false;
			this.chartBloodBars.PlotArea.XAxis.AxisLabel.Appearance.Dimensions.Height = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.chartBloodBars.PlotArea.XAxis.AxisLabel.Appearance.Dimensions.Width = new Telerik.Reporting.Charting.Styles.Unit(1D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.chartBloodBars.PlotArea.XAxis.AxisLabel.Appearance.Visible = true;
			this.chartBloodBars.PlotArea.XAxis.AxisLabel.TextBlock.Appearance.Border.Color = System.Drawing.Color.Black;
			this.chartBloodBars.PlotArea.XAxis.AxisLabel.TextBlock.Appearance.Position.AlignedPosition = Telerik.Reporting.Charting.Styles.AlignedPositions.Center;
			this.chartBloodBars.PlotArea.XAxis.AxisLabel.TextBlock.Appearance.Shadow.Color = System.Drawing.Color.Transparent;
			this.chartBloodBars.PlotArea.XAxis.AxisLabel.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.Black;
			this.chartBloodBars.PlotArea.XAxis.AxisLabel.TextBlock.Appearance.TextProperties.Font = new System.Drawing.Font("Times New Roman", 8F);
			this.chartBloodBars.PlotArea.XAxis.AxisLabel.TextBlock.Visible = false;
			this.chartBloodBars.PlotArea.XAxis.AxisLabel.Visible = true;
			this.chartBloodBars.PlotArea.XAxis.DataLabelsColumn = "Series";
			this.chartBloodBars.PlotArea.XAxis.MinValue = 1D;
			this.chartBloodBars.PlotArea.YAxis.Appearance.LabelAppearance.Border.Color = System.Drawing.Color.Transparent;
			this.chartBloodBars.PlotArea.YAxis.Appearance.LabelAppearance.FillStyle.FillType = Telerik.Reporting.Charting.Styles.FillType.Solid;
			this.chartBloodBars.PlotArea.YAxis.Appearance.LabelAppearance.FillStyle.MainColor = System.Drawing.Color.White;
			this.chartBloodBars.PlotArea.YAxis.Appearance.LabelAppearance.FillStyle.SecondColor = System.Drawing.Color.Black;
			this.chartBloodBars.PlotArea.YAxis.Appearance.MajorGridLines.Color = System.Drawing.Color.Black;
			this.chartBloodBars.PlotArea.YAxis.Appearance.MajorGridLines.Visible = false;
			this.chartBloodBars.PlotArea.YAxis.Appearance.MajorTick.Color = System.Drawing.Color.Black;
			this.chartBloodBars.PlotArea.YAxis.Appearance.MinorGridLines.Color = System.Drawing.Color.Black;
			this.chartBloodBars.PlotArea.YAxis.Appearance.MinorGridLines.Visible = false;
			this.chartBloodBars.PlotArea.YAxis.Appearance.MinorTick.Color = System.Drawing.Color.Black;
			this.chartBloodBars.PlotArea.YAxis.Appearance.MinorTick.Visible = false;
			this.chartBloodBars.PlotArea.YAxis.Appearance.TextAppearance.Border.Color = System.Drawing.Color.White;
			chartPaddings3.Bottom = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings3.Left = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings3.Right = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings3.Top = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.chartBloodBars.PlotArea.YAxis.Appearance.TextAppearance.Dimensions.Paddings = chartPaddings3;
			this.chartBloodBars.PlotArea.YAxis.Appearance.TextAppearance.Position.AlignedPosition = Telerik.Reporting.Charting.Styles.AlignedPositions.Center;
			this.chartBloodBars.PlotArea.YAxis.Appearance.TextAppearance.TextProperties.Color = System.Drawing.Color.Black;
			this.chartBloodBars.PlotArea.YAxis.Appearance.TextAppearance.TextProperties.Font = new System.Drawing.Font("Times New Roman", 7F);
			this.chartBloodBars.PlotArea.YAxis.AxisLabel.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.Black;
			this.chartBloodBars.PlotArea.YAxis.AxisLabel.TextBlock.Appearance.TextProperties.Font = new System.Drawing.Font("Times New Roman", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.chartBloodBars.PlotArea.YAxis.AxisMode = Telerik.Reporting.Charting.ChartYAxisMode.Extended;
			this.chartBloodBars.PlotArea.YAxis.LogarithmBase = 2D;
			this.chartBloodBars.PlotArea.YAxis.MaxValue = 100D;
			this.chartBloodBars.PlotArea.YAxis.Step = 10D;
			this.chartBloodBars.PlotArea.YAxis2.AutoScale = false;
			chartAxisItem2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			chartAxisItem3.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
			chartAxisItem4.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
			chartAxisItem5.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
			chartAxisItem6.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
			chartAxisItem7.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
			chartAxisItem8.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
			this.chartBloodBars.PlotArea.YAxis2.Items.AddRange(new Telerik.Reporting.Charting.ChartAxisItem[] {
            chartAxisItem1,
            chartAxisItem2,
            chartAxisItem3,
            chartAxisItem4,
            chartAxisItem5,
            chartAxisItem6,
            chartAxisItem7,
            chartAxisItem8});
			this.chartBloodBars.SeriesPalette = "Palette1";
			this.chartBloodBars.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2435925006866455D), Telerik.Reporting.Drawing.Unit.Inch(1.7123826742172241D));
			this.chartBloodBars.Style.BackgroundColor = System.Drawing.Color.Transparent;
			this.chartBloodBars.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
			this.chartBloodBars.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.chartBloodBars.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.chartBloodBars.Style.Font.Name = "Times New Roman";
			this.chartBloodBars.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.chartBloodBars.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// shape4
			// 
			this.shape4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
			this.shape4.Name = "shape4";
			this.shape4.ShapeType = new Telerik.Reporting.Drawing.Shapes.PolygonShape(4, 45D, 0);
			this.shape4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7963333129882812D), Telerik.Reporting.Drawing.Unit.Inch(1.8249603509902954D));
			this.shape4.Stretch = true;
			// 
			// textBoxPlasmaDeviationFromIdeal
			// 
			this.textBoxPlasmaDeviationFromIdeal.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3333332538604736D), Telerik.Reporting.Drawing.Unit.Inch(1.0438684225082398D));
			this.textBoxPlasmaDeviationFromIdeal.Name = "textBoxPlasmaDeviationFromIdeal";
			this.textBoxPlasmaDeviationFromIdeal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.78537750244140625D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxPlasmaDeviationFromIdeal.Style.Font.Name = "Times New Roman";
			this.textBoxPlasmaDeviationFromIdeal.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBoxPlasmaDeviationFromIdeal.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxPlasmaDeviationFromIdeal.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPlasmaDeviationFromIdeal.Value = "-";
			// 
			// textBoxRBCDeviationFromIdeal
			// 
			this.textBoxRBCDeviationFromIdeal.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3333332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.84378975629806519D));
			this.textBoxRBCDeviationFromIdeal.Name = "textBoxRBCDeviationFromIdeal";
			this.textBoxRBCDeviationFromIdeal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.78537750244140625D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxRBCDeviationFromIdeal.Style.Font.Name = "Times New Roman";
			this.textBoxRBCDeviationFromIdeal.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBoxRBCDeviationFromIdeal.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxRBCDeviationFromIdeal.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxRBCDeviationFromIdeal.Value = "-";
			// 
			// textBoxTotalDeviationFromIdeal
			// 
			this.textBoxTotalDeviationFromIdeal.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3333332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.64587306976318359D));
			this.textBoxTotalDeviationFromIdeal.Name = "textBoxTotalDeviationFromIdeal";
			this.textBoxTotalDeviationFromIdeal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.78537750244140625D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxTotalDeviationFromIdeal.Style.Font.Name = "Times New Roman";
			this.textBoxTotalDeviationFromIdeal.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBoxTotalDeviationFromIdeal.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxTotalDeviationFromIdeal.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxTotalDeviationFromIdeal.Value = "-";
			// 
			// textBoxIdealPlasmaVolume
			// 
			this.textBoxIdealPlasmaVolume.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5104167461395264D), Telerik.Reporting.Drawing.Unit.Inch(1.0438684225082398D));
			this.textBoxIdealPlasmaVolume.Name = "textBoxIdealPlasmaVolume";
			this.textBoxIdealPlasmaVolume.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.75D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxIdealPlasmaVolume.Style.Font.Name = "Times New Roman";
			this.textBoxIdealPlasmaVolume.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBoxIdealPlasmaVolume.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxIdealPlasmaVolume.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxIdealPlasmaVolume.Value = "-";
			// 
			// textBoxIdealRBC
			// 
			this.textBoxIdealRBC.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5104167461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.84378975629806519D));
			this.textBoxIdealRBC.Name = "textBoxIdealRBC";
			this.textBoxIdealRBC.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.75D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxIdealRBC.Style.Font.Name = "Times New Roman";
			this.textBoxIdealRBC.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBoxIdealRBC.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxIdealRBC.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxIdealRBC.Value = "-";
			// 
			// textBoxTotalPatientIdeal
			// 
			this.textBoxTotalPatientIdeal.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5104167461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.64587306976318359D));
			this.textBoxTotalPatientIdeal.Name = "textBoxTotalPatientIdeal";
			this.textBoxTotalPatientIdeal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.75D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBoxTotalPatientIdeal.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBoxTotalPatientIdeal.Style.Font.Name = "Times New Roman";
			this.textBoxTotalPatientIdeal.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBoxTotalPatientIdeal.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxTotalPatientIdeal.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxTotalPatientIdeal.Value = "-";
			// 
			// textBoxBVAPlasmaVolume
			// 
			this.textBoxBVAPlasmaVolume.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7499605417251587D), Telerik.Reporting.Drawing.Unit.Inch(1.0438684225082398D));
			this.textBoxBVAPlasmaVolume.Name = "textBoxBVAPlasmaVolume";
			this.textBoxBVAPlasmaVolume.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72503918409347534D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxBVAPlasmaVolume.Style.Font.Name = "Times New Roman";
			this.textBoxBVAPlasmaVolume.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBoxBVAPlasmaVolume.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxBVAPlasmaVolume.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxBVAPlasmaVolume.Value = "-";
			// 
			// textBoxBVARBCVolume
			// 
			this.textBoxBVARBCVolume.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7499605417251587D), Telerik.Reporting.Drawing.Unit.Inch(0.84378975629806519D));
			this.textBoxBVARBCVolume.Name = "textBoxBVARBCVolume";
			this.textBoxBVARBCVolume.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72503918409347534D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxBVARBCVolume.Style.Font.Name = "Times New Roman";
			this.textBoxBVARBCVolume.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBoxBVARBCVolume.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxBVARBCVolume.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxBVARBCVolume.Value = "-";
			// 
			// textBoxTotalBloodVolumeResults
			// 
			this.textBoxTotalBloodVolumeResults.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7499605417251587D), Telerik.Reporting.Drawing.Unit.Inch(0.64587306976318359D));
			this.textBoxTotalBloodVolumeResults.Name = "textBoxTotalBloodVolumeResults";
			this.textBoxTotalBloodVolumeResults.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72503918409347534D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxTotalBloodVolumeResults.Style.Font.Name = "Times New Roman";
			this.textBoxTotalBloodVolumeResults.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBoxTotalBloodVolumeResults.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxTotalBloodVolumeResults.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBoxTotalBloodVolumeResults.Value = "-";
			// 
			// textBox19
			// 
			this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1145833358168602D), Telerik.Reporting.Drawing.Unit.Inch(1.0438684225082398D));
			this.textBox19.Name = "textBox19";
			this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5456757545471191D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox19.Style.Font.Name = "Times New Roman";
			this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox19.Value = "Plasma Volume";
			// 
			// textBox18
			// 
			this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1145833358168602D), Telerik.Reporting.Drawing.Unit.Inch(0.84378975629806519D));
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5456757545471191D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox18.Style.Font.Name = "Times New Roman";
			this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox18.Value = "Red Blood Cell Volume";
			// 
			// textBox16
			// 
			this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1145833358168602D), Telerik.Reporting.Drawing.Unit.Inch(0.64587306976318359D));
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3000000715255737D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox16.Style.Font.Name = "Times New Roman";
			this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox16.Value = "Total Blood Volume";
			// 
			// textBox15
			// 
			this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.1875D), Telerik.Reporting.Drawing.Unit.Inch(0.24579448997974396D));
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5272808074951172D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox15.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox15.Style.Font.Bold = true;
			this.textBox15.Style.Font.Name = "Times New Roman";
			this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox15.Value = "Excess/Deficit %";
			// 
			// textBox14
			// 
			this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3333332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.2500397264957428D));
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.78537750244140625D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox14.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox14.Style.Font.Bold = true;
			this.textBox14.Style.Font.Name = "Times New Roman";
			this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox14.Value = "Deviation From Ideal";
			// 
			// textBox13
			// 
			this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5104167461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.2500397264957428D));
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.75D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox13.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox13.Style.Font.Bold = true;
			this.textBox13.Style.Font.Name = "Times New Roman";
			this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox13.Value = "Patient Ideal";
			// 
			// textBox11
			// 
			this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7499605417251587D), Telerik.Reporting.Drawing.Unit.Inch(0.2500397264957428D));
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72503918409347534D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox11.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox11.Style.Font.Bold = true;
			this.textBox11.Style.Font.Name = "Times New Roman";
			this.textBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
			this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox11.TextWrap = true;
			this.textBox11.Value = "BVA Result";
			// 
			// textBox7
			// 
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7916667461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.3020833432674408D));
			this.textBox7.Style.BackgroundColor = System.Drawing.Color.Black;
			this.textBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox7.Style.Color = System.Drawing.Color.White;
			this.textBox7.Style.Font.Bold = true;
			this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox7.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox7.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox7.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox7.TextWrap = false;
			this.textBox7.Value = "Blood Volume Analysis Results";
			// 
			// textBoxPlasmaExcessDeficit
			// 
			this.textBoxPlasmaExcessDeficit.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.1875D), Telerik.Reporting.Drawing.Unit.Inch(1.0438684225082398D));
			this.textBoxPlasmaExcessDeficit.Name = "textBoxPlasmaExcessDeficit";
			this.textBoxPlasmaExcessDeficit.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5272808074951172D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxPlasmaExcessDeficit.Style.Font.Name = "Times New Roman";
			this.textBoxPlasmaExcessDeficit.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBoxPlasmaExcessDeficit.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxPlasmaExcessDeficit.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPlasmaExcessDeficit.Value = "-";
			// 
			// textBoxRBCExcessDeficit
			// 
			this.textBoxRBCExcessDeficit.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.1875D), Telerik.Reporting.Drawing.Unit.Inch(0.84378975629806519D));
			this.textBoxRBCExcessDeficit.Name = "textBoxRBCExcessDeficit";
			this.textBoxRBCExcessDeficit.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5272808074951172D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxRBCExcessDeficit.Style.Font.Name = "Times New Roman";
			this.textBoxRBCExcessDeficit.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBoxRBCExcessDeficit.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxRBCExcessDeficit.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxRBCExcessDeficit.Value = "-";
			// 
			// panel2
			// 
			this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.shape5,
            this.shape2,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox6,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox61,
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.textBox69,
            this.textBox70,
            this.textBox71});
			this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0416666679084301D), Telerik.Reporting.Drawing.Unit.Inch(1.1666666269302368D));
			this.panel2.Name = "panel2";
			this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.6374607086181641D), Telerik.Reporting.Drawing.Unit.Inch(0.91874980926513672D));
			this.panel2.Style.Font.Name = "Times New Roman";
			// 
			// shape5
			// 
			this.shape5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.13746070861816406D), Telerik.Reporting.Drawing.Unit.Inch(0.19583384692668915D));
			this.shape5.Name = "shape5";
			this.shape5.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.497916579246521D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
			// 
			// shape2
			// 
			this.shape2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.008293628692627D), Telerik.Reporting.Drawing.Unit.Inch(0.19583351910114288D));
			this.shape2.Name = "shape2";
			this.shape2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6648203134536743D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7082939147949219D), Telerik.Reporting.Drawing.Unit.Inch(0.09587319940328598D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.4082157611846924D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox1.Style.Font.Italic = true;
			this.textBox1.Style.Font.Name = "Times New Roman";
			this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox1.Value = "Blood Volume Interpretation Guideline";
			// 
			// textBox2
			// 
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.13746070861816406D), Telerik.Reporting.Drawing.Unit.Inch(0.50000065565109253D));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.497916579246521D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox2.Style.Font.Name = "Times New Roman";
			this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox2.Value = "BV, PV Deviation (� %):";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.13746054470539093D), Telerik.Reporting.Drawing.Unit.Inch(0.70000046491622925D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4895833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox3.Style.Font.Name = "Times New Roman";
			this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox3.Value = "RCV Deviation (� %):";
			// 
			// textBox4
			// 
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.764543890953064D), Telerik.Reporting.Drawing.Unit.Inch(0.30000019073486328D));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.599999725818634D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox4.Style.Font.Name = "Times New Roman";
			this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox4.Value = "Normal";
			// 
			// textBox6
			// 
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.4187107086181641D), Telerik.Reporting.Drawing.Unit.Inch(0.30000019073486328D));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.599999725818634D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox6.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox6.Style.Font.Name = "Times New Roman";
			this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox6.Value = "Mild";
			// 
			// textBox8
			// 
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0729167461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.29992198944091797D));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.74984234571456909D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox8.Style.Font.Name = "Times New Roman";
			this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox8.Value = "Moderate";
			// 
			// textBox9
			// 
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.8645439147949219D), Telerik.Reporting.Drawing.Unit.Inch(0.30000019073486328D));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.599999725818634D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox9.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox9.Style.Font.Name = "Times New Roman";
			this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox9.Value = "Severe";
			// 
			// textBox10
			// 
			this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5187110900878906D), Telerik.Reporting.Drawing.Unit.Inch(0.30000019073486328D));
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.599999725818634D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox10.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox10.Style.Font.Name = "Times New Roman";
			this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox10.Value = "Extreme";
			// 
			// textBox61
			// 
			this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7395833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.50000065565109253D));
			this.textBox61.Name = "textBox61";
			this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65208309888839722D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox61.Style.Font.Name = "Times New Roman";
			this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox61.Value = "0 to 8";
			// 
			// textBox63
			// 
			this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.3958332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.50000065565109253D));
			this.textBox63.Name = "textBox63";
			this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65208309888839722D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox63.Style.Font.Name = "Times New Roman";
			this.textBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox63.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox63.Value = ">8 to 16";
			// 
			// textBox64
			// 
			this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0833332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.50000065565109253D));
			this.textBox64.Name = "textBox64";
			this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65208309888839722D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox64.Style.Font.Name = "Times New Roman";
			this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox64.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox64.Value = ">16 to 24";
			// 
			// textBox65
			// 
			this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.875D), Telerik.Reporting.Drawing.Unit.Inch(0.50000065565109253D));
			this.textBox65.Name = "textBox65";
			this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65208309888839722D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox65.Style.Font.Name = "Times New Roman";
			this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox65.Value = ">24 to 32";
			// 
			// textBox66
			// 
			this.textBox66.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.4895834922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.50000065565109253D));
			this.textBox66.Name = "textBox66";
			this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65208309888839722D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox66.Style.Font.Name = "Times New Roman";
			this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox66.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox66.Value = ">32";
			// 
			// textBox67
			// 
			this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.4895834922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.71875D));
			this.textBox67.Name = "textBox67";
			this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65208309888839722D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox67.Style.Font.Name = "Times New Roman";
			this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox67.Value = ">40";
			// 
			// textBox68
			// 
			this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.875D), Telerik.Reporting.Drawing.Unit.Inch(0.71875D));
			this.textBox68.Name = "textBox68";
			this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65208309888839722D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox68.Style.Font.Name = "Times New Roman";
			this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox68.Value = ">30 to 40";
			// 
			// textBox69
			// 
			this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.0729167461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.71875D));
			this.textBox69.Name = "textBox69";
			this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65208309888839722D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox69.Style.Font.Name = "Times New Roman";
			this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox69.Value = ">20 to 30";
			// 
			// textBox70
			// 
			this.textBox70.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.3958332538604736D), Telerik.Reporting.Drawing.Unit.Inch(0.71875D));
			this.textBox70.Name = "textBox70";
			this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65208309888839722D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox70.Style.Font.Name = "Times New Roman";
			this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox70.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox70.Value = ">10 to 20";
			// 
			// textBox71
			// 
			this.textBox71.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7395833730697632D), Telerik.Reporting.Drawing.Unit.Inch(0.71875D));
			this.textBox71.Name = "textBox71";
			this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.65208309888839722D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox71.Style.Font.Name = "Times New Roman";
			this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox71.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox71.Value = "0 to 10";
			// 
			// textBoxTotalExcessDeficit
			// 
			this.textBoxTotalExcessDeficit.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.1875D), Telerik.Reporting.Drawing.Unit.Inch(0.64587306976318359D));
			this.textBoxTotalExcessDeficit.Name = "textBoxTotalExcessDeficit";
			this.textBoxTotalExcessDeficit.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5272808074951172D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxTotalExcessDeficit.Style.Font.Name = "Times New Roman";
			this.textBoxTotalExcessDeficit.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBoxTotalExcessDeficit.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxTotalExcessDeficit.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxTotalExcessDeficit.Value = "-";
			// 
			// SubReport_BloodVolumeAnalysis
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
			this.Name = "SubReport_BloodVolumeAnalysis";
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.9000000953674316D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.Panel panelBloodVolumeAnalysisResults;
        private Telerik.Reporting.Shape shape4;
        public Telerik.Reporting.TextBox textBoxPlasmaDeviationFromIdeal;
        public Telerik.Reporting.TextBox textBoxRBCDeviationFromIdeal;
        public Telerik.Reporting.TextBox textBoxTotalDeviationFromIdeal;
        public Telerik.Reporting.TextBox textBoxIdealPlasmaVolume;
        public Telerik.Reporting.TextBox textBoxIdealRBC;
        public Telerik.Reporting.TextBox textBoxTotalPatientIdeal;
        public Telerik.Reporting.TextBox textBoxBVAPlasmaVolume;
        public Telerik.Reporting.TextBox textBoxBVARBCVolume;
        public Telerik.Reporting.TextBox textBoxTotalBloodVolumeResults;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox7;
        public Telerik.Reporting.Chart chartBloodBars;
        public Telerik.Reporting.TextBox textBoxPlasmaExcessDeficit;
        public Telerik.Reporting.TextBox textBoxRBCExcessDeficit;
        private Telerik.Reporting.Panel panel2;
        private Telerik.Reporting.Shape shape5;
        private Telerik.Reporting.Shape shape2;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        public Telerik.Reporting.TextBox textBox61;
        public Telerik.Reporting.TextBox textBox63;
        public Telerik.Reporting.TextBox textBox64;
        public Telerik.Reporting.TextBox textBox65;
        public Telerik.Reporting.TextBox textBox66;
        public Telerik.Reporting.TextBox textBox67;
        public Telerik.Reporting.TextBox textBox68;
        public Telerik.Reporting.TextBox textBox69;
        public Telerik.Reporting.TextBox textBox70;
        public Telerik.Reporting.TextBox textBox71;
        public Telerik.Reporting.TextBox textBoxTotalExcessDeficit;
		public Telerik.Reporting.DetailSection detail;
    }
}