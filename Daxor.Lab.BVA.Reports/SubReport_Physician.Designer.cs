namespace Daxor.Lab.BVA.Reports
{
    partial class SubReport_Physician
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SubReport_Physician));
			this.detail = new Telerik.Reporting.DetailSection();
			this.BloodVolumeAnalysisSubreport = new Telerik.Reporting.SubReport();
			this.shape6 = new Telerik.Reporting.Shape();
			this.shape8 = new Telerik.Reporting.Shape();
			this.shape7 = new Telerik.Reporting.Shape();
			this.shape3 = new Telerik.Reporting.Shape();
			this.textBoxTechnicalNotes = new Telerik.Reporting.TextBox();
			this.textBoxTotalVolumeMLkgIdeal = new Telerik.Reporting.TextBox();
			this.textBoxRBCMLKgIdeal = new Telerik.Reporting.TextBox();
			this.textBoxPlasmaMLkgIdeal = new Telerik.Reporting.TextBox();
			this.textBoxPlasmaMLkg = new Telerik.Reporting.TextBox();
			this.textBoxRBCMLkg = new Telerik.Reporting.TextBox();
			this.textBoxTotalVolumeMLkg = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.shape1 = new Telerik.Reporting.Shape();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.shape9 = new Telerik.Reporting.Shape();
			this.shape10 = new Telerik.Reporting.Shape();
			this.textBox26 = new Telerik.Reporting.TextBox();
			this.textBox27 = new Telerik.Reporting.TextBox();
			this.textBoxNormalGender = new Telerik.Reporting.TextBox();
			this.textBox28 = new Telerik.Reporting.TextBox();
			this.textBox29 = new Telerik.Reporting.TextBox();
			this.textBox30 = new Telerik.Reporting.TextBox();
			this.textBox32 = new Telerik.Reporting.TextBox();
			this.textBox37 = new Telerik.Reporting.TextBox();
			this.panel_ReportFindings = new Telerik.Reporting.Panel();
			this.shape11 = new Telerik.Reporting.Shape();
			this.textBox51 = new Telerik.Reporting.TextBox();
			this.textBoxReportFindings = new Telerik.Reporting.TextBox();
			this.textBox52 = new Telerik.Reporting.TextBox();
			this.textBoxPeripheralHct = new Telerik.Reporting.TextBox();
			this.textBoxNormalizedHct = new Telerik.Reporting.TextBox();
			this.textBoxNormalPVHct = new Telerik.Reporting.TextBox();
			this.textBoxNormalNHct = new Telerik.Reporting.TextBox();
			this.textBox53 = new Telerik.Reporting.TextBox();
			this.textBoxTransudation = new Telerik.Reporting.TextBox();
			this.textBox54 = new Telerik.Reporting.TextBox();
			this.shape12 = new Telerik.Reporting.Shape();
			this.textBox55 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.textBox24 = new Telerik.Reporting.TextBox();
			this.textBox25 = new Telerik.Reporting.TextBox();
			this.shape13 = new Telerik.Reporting.Shape();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBoxWatermark = new Telerik.Reporting.TextBox();
			this.amputeeColorExample = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox44 = new Telerik.Reporting.TextBox();
			this.textBox45 = new Telerik.Reporting.TextBox();
			this.textBox46 = new Telerik.Reporting.TextBox();
			this.textBox47 = new Telerik.Reporting.TextBox();
			this.textBox48 = new Telerik.Reporting.TextBox();
			this.textBox49 = new Telerik.Reporting.TextBox();
			this.textBox50 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(6.9331765174865723D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.BloodVolumeAnalysisSubreport,
            this.shape6,
            this.shape8,
            this.shape7,
            this.shape3,
            this.textBoxTechnicalNotes,
            this.textBoxTotalVolumeMLkgIdeal,
            this.textBoxRBCMLKgIdeal,
            this.textBoxPlasmaMLkgIdeal,
            this.textBoxPlasmaMLkg,
            this.textBoxRBCMLkg,
            this.textBoxTotalVolumeMLkg,
            this.textBox5,
            this.shape1,
            this.textBox22,
            this.shape9,
            this.shape10,
            this.textBox26,
            this.textBox27,
            this.textBoxNormalGender,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox32,
            this.textBox37,
            this.panel_ReportFindings,
            this.textBox52,
            this.textBoxPeripheralHct,
            this.textBoxNormalizedHct,
            this.textBoxNormalPVHct,
            this.textBoxNormalNHct,
            this.textBox53,
            this.textBoxTransudation,
            this.textBox54,
            this.shape12,
            this.textBox55,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.shape13,
            this.textBox12,
            this.textBoxWatermark,
            this.amputeeColorExample,
            this.textBox3,
            this.textBox2,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50});
			this.detail.Name = "detail";
			this.detail.PageBreak = Telerik.Reporting.PageBreak.None;
			// 
			// BloodVolumeAnalysisSubreport
			// 
			this.BloodVolumeAnalysisSubreport.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.019999999552965164D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.BloodVolumeAnalysisSubreport.Name = "BloodVolumeAnalysisSubreport";
			this.BloodVolumeAnalysisSubreport.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.880000114440918D), Telerik.Reporting.Drawing.Unit.Inch(2.1700000762939453D));
			// 
			// shape6
			// 
			this.shape6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.072499990463256836D), Telerik.Reporting.Drawing.Unit.Inch(2.9617455005645752D));
			this.shape6.Name = "shape6";
			this.shape6.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.58746075630188D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
			// 
			// shape8
			// 
			this.shape8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.3433337211608887D), Telerik.Reporting.Drawing.Unit.Inch(2.9617455005645752D));
			this.shape8.Name = "shape8";
			this.shape8.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
			// 
			// shape7
			// 
			this.shape7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8537499904632568D), Telerik.Reporting.Drawing.Unit.Inch(2.9617455005645752D));
			this.shape7.Name = "shape7";
			this.shape7.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2917060852050781D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
			// 
			// shape3
			// 
			this.shape3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.019999999552965164D), Telerik.Reporting.Drawing.Unit.Inch(2.5199999809265137D));
			this.shape3.Name = "shape3";
			this.shape3.ShapeType = new Telerik.Reporting.Drawing.Shapes.PolygonShape(4, 45D, 0);
			this.shape3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.769289493560791D), Telerik.Reporting.Drawing.Unit.Inch(1.6020046472549439D));
			this.shape3.Stretch = true;
			// 
			// textBoxTechnicalNotes
			// 
			this.textBoxTechnicalNotes.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.019999999552965164D), Telerik.Reporting.Drawing.Unit.Inch(5.7399997711181641D));
			this.textBoxTechnicalNotes.Name = "textBoxTechnicalNotes";
			this.textBoxTechnicalNotes.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7672457695007324D), Telerik.Reporting.Drawing.Unit.Inch(1.1831766366958618D));
			this.textBoxTechnicalNotes.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.textBoxTechnicalNotes.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.textBoxTechnicalNotes.Style.Font.Name = "Times New Roman";
			this.textBoxTechnicalNotes.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBoxTechnicalNotes.Value = resources.GetString("textBoxTechnicalNotes.Value");
			// 
			// textBoxTotalVolumeMLkgIdeal
			// 
			this.textBoxTotalVolumeMLkgIdeal.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5412497520446777D), Telerik.Reporting.Drawing.Unit.Inch(3.4617455005645752D));
			this.textBoxTotalVolumeMLkgIdeal.Name = "textBoxTotalVolumeMLkgIdeal";
			this.textBoxTotalVolumeMLkgIdeal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999960660934448D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxTotalVolumeMLkgIdeal.Style.Font.Name = "Times New Roman";
			this.textBoxTotalVolumeMLkgIdeal.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxTotalVolumeMLkgIdeal.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxTotalVolumeMLkgIdeal.Value = "-";
			// 
			// textBoxRBCMLKgIdeal
			// 
			this.textBoxRBCMLKgIdeal.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5412497520446777D), Telerik.Reporting.Drawing.Unit.Inch(3.8992455005645752D));
			this.textBoxRBCMLKgIdeal.Name = "textBoxRBCMLKgIdeal";
			this.textBoxRBCMLKgIdeal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999960660934448D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxRBCMLKgIdeal.Style.Font.Name = "Times New Roman";
			this.textBoxRBCMLKgIdeal.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxRBCMLKgIdeal.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxRBCMLKgIdeal.Value = "-";
			// 
			// textBoxPlasmaMLkgIdeal
			// 
			this.textBoxPlasmaMLkgIdeal.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5412497520446777D), Telerik.Reporting.Drawing.Unit.Inch(3.6700789928436279D));
			this.textBoxPlasmaMLkgIdeal.Name = "textBoxPlasmaMLkgIdeal";
			this.textBoxPlasmaMLkgIdeal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999960660934448D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxPlasmaMLkgIdeal.Style.Font.Name = "Times New Roman";
			this.textBoxPlasmaMLkgIdeal.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxPlasmaMLkgIdeal.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPlasmaMLkgIdeal.Value = "-";
			// 
			// textBoxPlasmaMLkg
			// 
			this.textBoxPlasmaMLkg.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9474999904632568D), Telerik.Reporting.Drawing.Unit.Inch(3.6700789928436279D));
			this.textBoxPlasmaMLkg.Name = "textBoxPlasmaMLkg";
			this.textBoxPlasmaMLkg.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999960660934448D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxPlasmaMLkg.Style.Font.Name = "Times New Roman";
			this.textBoxPlasmaMLkg.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxPlasmaMLkg.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPlasmaMLkg.Value = "-";
			// 
			// textBoxRBCMLkg
			// 
			this.textBoxRBCMLkg.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9474999904632568D), Telerik.Reporting.Drawing.Unit.Inch(3.8992455005645752D));
			this.textBoxRBCMLkg.Name = "textBoxRBCMLkg";
			this.textBoxRBCMLkg.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999960660934448D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxRBCMLkg.Style.Font.Name = "Times New Roman";
			this.textBoxRBCMLkg.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxRBCMLkg.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxRBCMLkg.Value = "-";
			// 
			// textBoxTotalVolumeMLkg
			// 
			this.textBoxTotalVolumeMLkg.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9474999904632568D), Telerik.Reporting.Drawing.Unit.Inch(3.4617455005645752D));
			this.textBoxTotalVolumeMLkg.Name = "textBoxTotalVolumeMLkg";
			this.textBoxTotalVolumeMLkg.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999960660934448D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxTotalVolumeMLkg.Style.Font.Name = "Times New Roman";
			this.textBoxTotalVolumeMLkg.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxTotalVolumeMLkg.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxTotalVolumeMLkg.Value = "-";
			// 
			// textBox5
			// 
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.21999979019165D), Telerik.Reporting.Drawing.Unit.Inch(5.5399999618530273D));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5456758737564087D), Telerik.Reporting.Drawing.Unit.Inch(0.18958282470703125D));
			this.textBox5.Style.Font.Name = "Times New Roman";
			this.textBox5.Value = "Date";
			// 
			// shape1
			// 
			this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.21999979019165D), Telerik.Reporting.Drawing.Unit.Inch(5.5100002288818359D));
			this.shape1.Name = "shape1";
			this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7999998331069946D), Telerik.Reporting.Drawing.Unit.Inch(0.079166412353515625D));
			// 
			// textBox22
			// 
			this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.019999999552965164D), Telerik.Reporting.Drawing.Unit.Inch(2.2200000286102295D));
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7916667461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.3020833432674408D));
			this.textBox22.Style.BackgroundColor = System.Drawing.Color.Black;
			this.textBox22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox22.Style.Color = System.Drawing.Color.White;
			this.textBox22.Style.Font.Bold = true;
			this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox22.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox22.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox22.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox22.TextWrap = false;
			this.textBox22.Value = "Additional Analysis";
			// 
			// shape9
			// 
			this.shape9.Angle = 15D;
			this.shape9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.6558332443237305D), Telerik.Reporting.Drawing.Unit.Inch(2.5659122467041016D));
			this.shape9.Name = "shape9";
			this.shape9.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.NS);
			this.shape9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.19162750244140625D), Telerik.Reporting.Drawing.Unit.Inch(1.5D));
			// 
			// shape10
			// 
			this.shape10.Angle = 15D;
			this.shape10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.145416259765625D), Telerik.Reporting.Drawing.Unit.Inch(2.5763289928436279D));
			this.shape10.Name = "shape10";
			this.shape10.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.NS);
			this.shape10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.19162750244140625D), Telerik.Reporting.Drawing.Unit.Inch(1.5D));
			// 
			// textBox26
			// 
			this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.3954166173934937D), Telerik.Reporting.Drawing.Unit.Inch(3.0659122467041016D));
			this.textBox26.Name = "textBox26";
			this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.599999725818634D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox26.Style.Font.Bold = true;
			this.textBox26.Style.Font.Name = "Times New Roman";
			this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox26.Value = "Patient Result";
			// 
			// textBox27
			// 
			this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.051666658371686935D), Telerik.Reporting.Drawing.Unit.Inch(3.4617455005645752D));
			this.textBox27.Name = "textBox27";
			this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5456758737564087D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox27.Style.Font.Name = "Times New Roman";
			this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox27.Value = "Peripheral Venous Hct";
			// 
			// textBoxNormalGender
			// 
			this.textBoxNormalGender.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.0829164981842041D), Telerik.Reporting.Drawing.Unit.Inch(3.0659122467041016D));
			this.textBoxNormalGender.Name = "textBoxNormalGender";
			this.textBoxNormalGender.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.599999725818634D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBoxNormalGender.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBoxNormalGender.Style.Font.Bold = true;
			this.textBoxNormalGender.Style.Font.Name = "Times New Roman";
			this.textBoxNormalGender.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxNormalGender.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBoxNormalGender.Value = "Normal Male";
			// 
			// textBox28
			// 
			this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9474999904632568D), Telerik.Reporting.Drawing.Unit.Inch(3.0659122467041016D));
			this.textBox28.Name = "textBox28";
			this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.599999725818634D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox28.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox28.Style.Font.Bold = true;
			this.textBox28.Style.Font.Name = "Times New Roman";
			this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox28.Value = "Patient Result";
			// 
			// textBox29
			// 
			this.textBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8641664981842041D), Telerik.Reporting.Drawing.Unit.Inch(3.6700789928436279D));
			this.textBox29.Name = "textBox29";
			this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.20617198944091797D));
			this.textBox29.Style.Font.Name = "Times New Roman";
			this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox29.Value = "Plasma Volume";
			// 
			// textBox30
			// 
			this.textBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8641664981842041D), Telerik.Reporting.Drawing.Unit.Inch(3.8992455005645752D));
			this.textBox30.Name = "textBox30";
			this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90000009536743164D), Telerik.Reporting.Drawing.Unit.Inch(0.20617198944091797D));
			this.textBox30.Style.Font.Name = "Times New Roman";
			this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox30.Value = "RBC Volume";
			// 
			// textBox32
			// 
			this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8641664981842041D), Telerik.Reporting.Drawing.Unit.Inch(3.4617455005645752D));
			this.textBox32.Name = "textBox32";
			this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.90000039339065552D), Telerik.Reporting.Drawing.Unit.Inch(0.20617198944091797D));
			this.textBox32.Style.Font.Name = "Times New Roman";
			this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox32.Value = "Total Volume";
			// 
			// textBox37
			// 
			this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.3745837211608887D), Telerik.Reporting.Drawing.Unit.Inch(3.0659122467041016D));
			this.textBox37.Name = "textBox37";
			this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.74000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox37.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox37.Style.Font.Bold = true;
			this.textBox37.Style.Font.Name = "Times New Roman";
			this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox37.Value = "Patient Result";
			// 
			// panel_ReportFindings
			// 
			this.panel_ReportFindings.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.shape11,
            this.textBox51,
            this.textBoxReportFindings});
			this.panel_ReportFindings.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.019999999552965164D), Telerik.Reporting.Drawing.Unit.Inch(4.179999828338623D));
			this.panel_ReportFindings.Name = "panel_ReportFindings";
			this.panel_ReportFindings.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.93325519561767578D));
			// 
			// shape11
			// 
			this.shape11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
			this.shape11.Name = "shape11";
			this.shape11.ShapeType = new Telerik.Reporting.Drawing.Shapes.PolygonShape(4, 45D, 0);
			this.shape11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.769289493560791D), Telerik.Reporting.Drawing.Unit.Inch(0.61458301544189453D));
			this.shape11.Stretch = true;
			// 
			// textBox51
			// 
			this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox51.Name = "textBox51";
			this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7916667461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.30208459496498108D));
			this.textBox51.Style.BackgroundColor = System.Drawing.Color.Black;
			this.textBox51.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox51.Style.Color = System.Drawing.Color.White;
			this.textBox51.Style.Font.Bold = true;
			this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox51.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox51.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox51.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox51.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox51.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox51.TextWrap = false;
			this.textBox51.Value = "Report Findings";
			// 
			// textBoxReportFindings
			// 
			this.textBoxReportFindings.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.069289125502109528D), Telerik.Reporting.Drawing.Unit.Inch(0.34791755676269531D));
			this.textBoxReportFindings.Name = "textBoxReportFindings";
			this.textBoxReportFindings.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.53749877214431763D));
			this.textBoxReportFindings.Style.Font.Name = "Times New Roman";
			this.textBoxReportFindings.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBoxReportFindings.Value = "";
			// 
			// textBox52
			// 
			this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.051666658371686935D), Telerik.Reporting.Drawing.Unit.Inch(3.6700789928436279D));
			this.textBox52.Name = "textBox52";
			this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5456758737564087D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox52.Style.Font.Name = "Times New Roman";
			this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox52.Value = "Normalized Hct (nHct)";
			// 
			// textBoxPeripheralHct
			// 
			this.textBoxPeripheralHct.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.3954166173934937D), Telerik.Reporting.Drawing.Unit.Inch(3.4617455005645752D));
			this.textBoxPeripheralHct.Name = "textBoxPeripheralHct";
			this.textBoxPeripheralHct.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999960660934448D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxPeripheralHct.Style.Font.Name = "Times New Roman";
			this.textBoxPeripheralHct.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxPeripheralHct.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPeripheralHct.Value = "-";
			// 
			// textBoxNormalizedHct
			// 
			this.textBoxNormalizedHct.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.3954166173934937D), Telerik.Reporting.Drawing.Unit.Inch(3.6700789928436279D));
			this.textBoxNormalizedHct.Name = "textBoxNormalizedHct";
			this.textBoxNormalizedHct.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999960660934448D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxNormalizedHct.Style.Font.Name = "Times New Roman";
			this.textBoxNormalizedHct.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxNormalizedHct.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxNormalizedHct.Value = "-";
			// 
			// textBoxNormalPVHct
			// 
			this.textBoxNormalPVHct.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.0829164981842041D), Telerik.Reporting.Drawing.Unit.Inch(3.4617455005645752D));
			this.textBoxNormalPVHct.Name = "textBoxNormalPVHct";
			this.textBoxNormalPVHct.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999960660934448D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxNormalPVHct.Style.Font.Name = "Times New Roman";
			this.textBoxNormalPVHct.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxNormalPVHct.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxNormalPVHct.Value = "-";
			// 
			// textBoxNormalNHct
			// 
			this.textBoxNormalNHct.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.0829164981842041D), Telerik.Reporting.Drawing.Unit.Inch(3.6700789928436279D));
			this.textBoxNormalNHct.Name = "textBoxNormalNHct";
			this.textBoxNormalNHct.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999960660934448D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxNormalNHct.Style.Font.Name = "Times New Roman";
			this.textBoxNormalNHct.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxNormalNHct.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxNormalNHct.Value = "-";
			// 
			// textBox53
			// 
			this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.5724997520446777D), Telerik.Reporting.Drawing.Unit.Inch(3.0659122467041016D));
			this.textBox53.Name = "textBox53";
			this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.599999725818634D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox53.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox53.Style.Font.Bold = true;
			this.textBox53.Style.Font.Name = "Times New Roman";
			this.textBox53.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox53.Value = "Patient Ideal";
			// 
			// textBoxTransudation
			// 
			this.textBoxTransudation.CanGrow = false;
			this.textBoxTransudation.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.3745837211608887D), Telerik.Reporting.Drawing.Unit.Inch(3.4617455005645752D));
			this.textBoxTransudation.Name = "textBoxTransudation";
			this.textBoxTransudation.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.74000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxTransudation.Style.Font.Name = "Times New Roman";
			this.textBoxTransudation.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxTransudation.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxTransudation.Value = "-";
			// 
			// textBox54
			// 
			this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.019999999552965164D), Telerik.Reporting.Drawing.Unit.Inch(5.0999999046325684D));
			this.textBox54.Name = "textBox54";
			this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox54.Style.Font.Bold = true;
			this.textBox54.Style.Font.Name = "Times New Roman";
			this.textBox54.Value = "Additional Comments:";
			// 
			// shape12
			// 
			this.shape12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.019999999552965164D), Telerik.Reporting.Drawing.Unit.Inch(5.5100002288818359D));
			this.shape12.Name = "shape12";
			this.shape12.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4588329792022705D), Telerik.Reporting.Drawing.Unit.Inch(0.079166412353515625D));
			// 
			// textBox55
			// 
			this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.019999999552965164D), Telerik.Reporting.Drawing.Unit.Inch(5.5399999618530273D));
			this.textBox55.Name = "textBox55";
			this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5456758737564087D), Telerik.Reporting.Drawing.Unit.Inch(0.21041615307331085D));
			this.textBox55.Style.Font.Name = "Times New Roman";
			this.textBox55.Value = "Physician Signature";
			// 
			// textBox23
			// 
			this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.18708331882953644D), Telerik.Reporting.Drawing.Unit.Inch(2.7013289928436279D));
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.25208345055580139D));
			this.textBox23.Style.Font.Bold = true;
			this.textBox23.Style.Font.Name = "Times New Roman";
			this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox23.Value = "Hematocrit Analysis";
			// 
			// textBox24
			// 
			this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.2495832443237305D), Telerik.Reporting.Drawing.Unit.Inch(2.7117455005645752D));
			this.textBox24.Name = "textBox24";
			this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.406092643737793D), Telerik.Reporting.Drawing.Unit.Inch(0.25208345055580139D));
			this.textBox24.Style.Font.Bold = true;
			this.textBox24.Style.Font.Name = "Times New Roman";
			this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox24.Value = "mL/kg Analysis";
			// 
			// textBox25
			// 
			this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.4475002288818359D), Telerik.Reporting.Drawing.Unit.Inch(2.5242455005645752D));
			this.textBox25.Name = "textBox25";
			this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1060926914215088D), Telerik.Reporting.Drawing.Unit.Inch(0.45200476050376892D));
			this.textBox25.Style.Font.Bold = true;
			this.textBox25.Style.Font.Name = "Times New Roman";
			this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox25.Value = "Albumin Transudation Analysis/Slope (%/min)";
			// 
			// shape13
			// 
			this.shape13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3299999237060547D), Telerik.Reporting.Drawing.Unit.Inch(5.5100002288818359D));
			this.shape13.Name = "shape13";
			this.shape13.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2583332061767578D), Telerik.Reporting.Drawing.Unit.Inch(0.079166412353515625D));
			// 
			// textBox12
			// 
			this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3299999237060547D), Telerik.Reporting.Drawing.Unit.Inch(5.5399999618530273D));
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89984256029129028D), Telerik.Reporting.Drawing.Unit.Inch(0.18958282470703125D));
			this.textBox12.Style.Font.Name = "Times New Roman";
			this.textBox12.Value = "Time";
			// 
			// textBoxWatermark
			// 
			this.textBoxWatermark.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1545833349227905D), Telerik.Reporting.Drawing.Unit.Inch(0.15625D));
			this.textBoxWatermark.Name = "textBoxWatermark";
			this.textBoxWatermark.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.5D), Telerik.Reporting.Drawing.Unit.Inch(1.9199999570846558D));
			this.textBoxWatermark.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.textBoxWatermark.Style.Font.Bold = true;
			this.textBoxWatermark.Style.Font.Name = "Times New Roman";
			this.textBoxWatermark.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(50D);
			this.textBoxWatermark.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxWatermark.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxWatermark.Value = "";
			// 
			// amputeeColorExample
			// 
			this.amputeeColorExample.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(6.7899999618530273D));
			this.amputeeColorExample.Name = "amputeeColorExample";
			this.amputeeColorExample.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.15583319962024689D), Telerik.Reporting.Drawing.Unit.Inch(0.10000037401914597D));
			this.amputeeColorExample.Style.BackgroundColor = System.Drawing.Color.LightGray;
			this.amputeeColorExample.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.amputeeColorExample.Style.Font.Bold = true;
			this.amputeeColorExample.Style.Font.Name = "Times New Roman";
			this.amputeeColorExample.Value = "";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.2870445251464844D), Telerik.Reporting.Drawing.Unit.Inch(3.5346622467041016D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53958255052566528D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox3.Style.Font.Name = "Times New Roman";
			this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox3.Value = "Elevated:";
			// 
			// textBox2
			// 
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9328780174255371D), Telerik.Reporting.Drawing.Unit.Inch(3.5346622467041016D));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70000028610229492D), Telerik.Reporting.Drawing.Unit.Inch(0.20617198944091797D));
			this.textBox2.Style.Font.Name = "Times New Roman";
			this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox2.Value = "0.25 to 0.4";
			// 
			// textBox44
			// 
			this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.2245445251464844D), Telerik.Reporting.Drawing.Unit.Inch(2.9617455005645752D));
			this.textBox44.Name = "textBox44";
			this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3290481567382813D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox44.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox44.Style.Font.Bold = true;
			this.textBox44.Style.Font.Name = "Times New Roman";
			this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox44.Value = "Reference Range";
			// 
			// textBox45
			// 
			this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.2870445251464844D), Telerik.Reporting.Drawing.Unit.Inch(3.3575789928436279D));
			this.textBox45.Name = "textBox45";
			this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53958255052566528D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox45.Style.Font.Name = "Times New Roman";
			this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox45.Value = "Normal:";
			// 
			// textBox46
			// 
			this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9328780174255371D), Telerik.Reporting.Drawing.Unit.Inch(3.3575789928436279D));
			this.textBox46.Name = "textBox46";
			this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70000028610229492D), Telerik.Reporting.Drawing.Unit.Inch(0.20617198944091797D));
			this.textBox46.Style.Font.Name = "Times New Roman";
			this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox46.Value = "0 to 0.25";
			// 
			// textBox47
			// 
			this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.2870445251464844D), Telerik.Reporting.Drawing.Unit.Inch(3.7117455005645752D));
			this.textBox47.Name = "textBox47";
			this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53958255052566528D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox47.Style.Font.Name = "Times New Roman";
			this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox47.Value = "High:";
			// 
			// textBox48
			// 
			this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9328780174255371D), Telerik.Reporting.Drawing.Unit.Inch(3.7117455005645752D));
			this.textBox48.Name = "textBox48";
			this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80000066757202148D), Telerik.Reporting.Drawing.Unit.Inch(0.20617198944091797D));
			this.textBox48.Style.Font.Name = "Times New Roman";
			this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox48.Value = "0.4 to 0.5";
			// 
			// textBox49
			// 
			this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.7141280174255371D), Telerik.Reporting.Drawing.Unit.Inch(3.9096622467041016D));
			this.textBox49.Name = "textBox49";
			this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1105340719223023D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox49.Style.Font.Name = "Times New Roman";
			this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox49.Value = "Very High:";
			// 
			// textBox50
			// 
			this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9328780174255371D), Telerik.Reporting.Drawing.Unit.Inch(3.8992455005645752D));
			this.textBox50.Name = "textBox50";
			this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80000066757202148D), Telerik.Reporting.Drawing.Unit.Inch(0.20617198944091797D));
			this.textBox50.Style.Font.Name = "Times New Roman";
			this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox50.Value = ">0.5";
			// 
			// SubReport_Physician
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
			this.Name = "SubReport_Physician";
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.9200000762939453D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        public Telerik.Reporting.TextBox textBoxTechnicalNotes;
        public Telerik.Reporting.TextBox textBoxTotalVolumeMLkgIdeal;
        public Telerik.Reporting.TextBox textBoxRBCMLKgIdeal;
        public Telerik.Reporting.TextBox textBoxPlasmaMLkgIdeal;
        public Telerik.Reporting.TextBox textBoxPlasmaMLkg;
        public Telerik.Reporting.TextBox textBoxRBCMLkg;
        public Telerik.Reporting.TextBox textBoxTotalVolumeMLkg;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.Shape shape6;
        private Telerik.Reporting.Shape shape7;
        private Telerik.Reporting.Shape shape8;
        private Telerik.Reporting.Shape shape9;
        private Telerik.Reporting.Shape shape10;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        public Telerik.Reporting.TextBox textBoxNormalGender;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox32;
		private Telerik.Reporting.TextBox textBox37;
        public Telerik.Reporting.Panel panel_ReportFindings;
        private Telerik.Reporting.Shape shape11;
        private Telerik.Reporting.TextBox textBox51;
        public Telerik.Reporting.TextBox textBoxReportFindings;
        private Telerik.Reporting.TextBox textBox52;
        public Telerik.Reporting.TextBox textBoxPeripheralHct;
        public Telerik.Reporting.TextBox textBoxNormalizedHct;
        public Telerik.Reporting.TextBox textBoxNormalPVHct;
        public Telerik.Reporting.TextBox textBoxNormalNHct;
        private Telerik.Reporting.TextBox textBox53;
        public Telerik.Reporting.TextBox textBoxTransudation;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.Shape shape12;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.Shape shape3;
        public Telerik.Reporting.TextBox textBoxWatermark;
        private Telerik.Reporting.Shape shape13;
        private Telerik.Reporting.TextBox textBox12;
        public Telerik.Reporting.SubReport BloodVolumeAnalysisSubreport;
		public Telerik.Reporting.DetailSection detail;
		internal Telerik.Reporting.TextBox amputeeColorExample;
		private Telerik.Reporting.TextBox textBox3;
		private Telerik.Reporting.TextBox textBox2;
		private Telerik.Reporting.TextBox textBox44;
		private Telerik.Reporting.TextBox textBox45;
		private Telerik.Reporting.TextBox textBox46;
		private Telerik.Reporting.TextBox textBox47;
		private Telerik.Reporting.TextBox textBox48;
		private Telerik.Reporting.TextBox textBox49;
		private Telerik.Reporting.TextBox textBox50;
    }
}