using System.Drawing;

namespace Daxor.Lab.BVA.Reports
{
    partial class SubReport_BloodVolumeAnalysisQuickReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.detail = new Telerik.Reporting.DetailSection();
			this.panelBloodVolumeAnalysisResults = new Telerik.Reporting.Panel();
			this.textBoxPlasmaDeviationFromIdealQR = new Telerik.Reporting.TextBox();
			this.textBoxRBCDeviationFromIdealQR = new Telerik.Reporting.TextBox();
			this.textBoxTotalDeviationFromIdealQR = new Telerik.Reporting.TextBox();
			this.textBoxBVAPlasmaVolumeQR = new Telerik.Reporting.TextBox();
			this.textBoxBVARBCVolumeQR = new Telerik.Reporting.TextBox();
			this.textBoxTotalBloodVolumeResultsQR = new Telerik.Reporting.TextBox();
			this.textBox19 = new Telerik.Reporting.TextBox();
			this.textBox18 = new Telerik.Reporting.TextBox();
			this.textBox16 = new Telerik.Reporting.TextBox();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.panel2 = new Telerik.Reporting.Panel();
			this.textBoxNormalizedHct = new Telerik.Reporting.TextBox();
			this.textBoxPeripheralHct = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.panel3 = new Telerik.Reporting.Panel();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBoxTotalExcessDeficitQR = new Telerik.Reporting.TextBox();
			this.textBoxPlasmaExcessDeficitQR = new Telerik.Reporting.TextBox();
			this.textBox15 = new Telerik.Reporting.TextBox();
			this.textBoxRBCExcessDeficitQR = new Telerik.Reporting.TextBox();
			this.panel4 = new Telerik.Reporting.Panel();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.textBoxAlbumanTransudation = new Telerik.Reporting.TextBox();
			this.panel1 = new Telerik.Reporting.Panel();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.totalBloodVolumeFlag = new Telerik.Reporting.TextBox();
			this.redBloodCellVolumeFlag = new Telerik.Reporting.TextBox();
			this.plasmaVolumeFlag = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(3.4999997615814209D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panelBloodVolumeAnalysisResults});
			this.detail.Name = "detail";
			// 
			// panelBloodVolumeAnalysisResults
			// 
			this.panelBloodVolumeAnalysisResults.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxPlasmaDeviationFromIdealQR,
            this.textBoxRBCDeviationFromIdealQR,
            this.textBoxTotalDeviationFromIdealQR,
            this.textBoxBVAPlasmaVolumeQR,
            this.textBoxBVARBCVolumeQR,
            this.textBoxTotalBloodVolumeResultsQR,
            this.textBox19,
            this.textBox18,
            this.textBox16,
            this.textBox14,
            this.textBox11,
            this.panel2,
            this.textBox1,
            this.textBoxTotalExcessDeficitQR,
            this.textBoxPlasmaExcessDeficitQR,
            this.textBox15,
            this.textBoxRBCExcessDeficitQR,
            this.panel4,
            this.textBox13,
            this.totalBloodVolumeFlag,
            this.redBloodCellVolumeFlag,
            this.plasmaVolumeFlag});
			this.panelBloodVolumeAnalysisResults.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9387494325637817E-05D));
			this.panelBloodVolumeAnalysisResults.Name = "panelBloodVolumeAnalysisResults";
			this.panelBloodVolumeAnalysisResults.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.899960994720459D), Telerik.Reporting.Drawing.Unit.Inch(3.4999210834503174D));
			// 
			// textBoxPlasmaDeviationFromIdealQR
			// 
			this.textBoxPlasmaDeviationFromIdealQR.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9999611377716064D), Telerik.Reporting.Drawing.Unit.Inch(1.1269785165786743D));
			this.textBoxPlasmaDeviationFromIdealQR.Name = "textBoxPlasmaDeviationFromIdealQR";
			this.textBoxPlasmaDeviationFromIdealQR.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3812503814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxPlasmaDeviationFromIdealQR.Style.Font.Name = "Arial";
			this.textBoxPlasmaDeviationFromIdealQR.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxPlasmaDeviationFromIdealQR.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxPlasmaDeviationFromIdealQR.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPlasmaDeviationFromIdealQR.Value = "-";
			// 
			// textBoxRBCDeviationFromIdealQR
			// 
			this.textBoxRBCDeviationFromIdealQR.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9999611377716064D), Telerik.Reporting.Drawing.Unit.Inch(0.97689980268478394D));
			this.textBoxRBCDeviationFromIdealQR.Name = "textBoxRBCDeviationFromIdealQR";
			this.textBoxRBCDeviationFromIdealQR.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3812503814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxRBCDeviationFromIdealQR.Style.Font.Name = "Arial";
			this.textBoxRBCDeviationFromIdealQR.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxRBCDeviationFromIdealQR.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxRBCDeviationFromIdealQR.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxRBCDeviationFromIdealQR.Value = "-";
			// 
			// textBoxTotalDeviationFromIdealQR
			// 
			this.textBoxTotalDeviationFromIdealQR.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9999611377716064D), Telerik.Reporting.Drawing.Unit.Inch(0.80003929138183594D));
			this.textBoxTotalDeviationFromIdealQR.Name = "textBoxTotalDeviationFromIdealQR";
			this.textBoxTotalDeviationFromIdealQR.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3812503814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.17678172886371613D));
			this.textBoxTotalDeviationFromIdealQR.Style.Font.Name = "Arial";
			this.textBoxTotalDeviationFromIdealQR.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxTotalDeviationFromIdealQR.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxTotalDeviationFromIdealQR.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxTotalDeviationFromIdealQR.Value = "-";
			// 
			// textBoxBVAPlasmaVolumeQR
			// 
			this.textBoxBVAPlasmaVolumeQR.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.9854955673217773D), Telerik.Reporting.Drawing.Unit.Inch(1.1160885095596314D));
			this.textBoxBVAPlasmaVolumeQR.Name = "textBoxBVAPlasmaVolume";
			this.textBoxBVAPlasmaVolumeQR.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.900000274181366D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxBVAPlasmaVolumeQR.Style.Font.Name = "Arial";
			this.textBoxBVAPlasmaVolumeQR.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxBVAPlasmaVolumeQR.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxBVAPlasmaVolumeQR.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxBVAPlasmaVolumeQR.Value = "-";
			// 
			// textBoxBVARBCVolumeQR
			// 
			this.textBoxBVARBCVolumeQR.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.9854955673217773D), Telerik.Reporting.Drawing.Unit.Inch(0.966009795665741D));
			this.textBoxBVARBCVolumeQR.Name = "textBoxBVARBCVolume";
			this.textBoxBVARBCVolumeQR.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.900000274181366D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxBVARBCVolumeQR.Style.Font.Name = "Arial";
			this.textBoxBVARBCVolumeQR.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxBVARBCVolumeQR.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxBVARBCVolumeQR.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxBVARBCVolumeQR.Value = "-";
			// 
			// textBoxTotalBloodVolumeResultsQR
			// 
			this.textBoxTotalBloodVolumeResultsQR.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.9854955673217773D), Telerik.Reporting.Drawing.Unit.Inch(0.81593102216720581D));
			this.textBoxTotalBloodVolumeResultsQR.Name = "textBoxTotalBloodVolumeResultsQR";
			this.textBoxTotalBloodVolumeResultsQR.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.900000274181366D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxTotalBloodVolumeResultsQR.Style.Font.Name = "Arial";
			this.textBoxTotalBloodVolumeResultsQR.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxTotalBloodVolumeResultsQR.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxTotalBloodVolumeResultsQR.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBoxTotalBloodVolumeResultsQR.Value = "-";
			// 
			// textBox19
			// 
			this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D), Telerik.Reporting.Drawing.Unit.Inch(1.1160885095596314D));
			this.textBox19.Name = "textBox19";
			this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5854167938232422D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox19.Style.Font.Bold = true;
			this.textBox19.Style.Font.Name = "Arial";
			this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox19.Value = "Plasma Volume";
			// 
			// textBox18
			// 
			this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D), Telerik.Reporting.Drawing.Unit.Inch(0.966009795665741D));
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5854167938232422D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox18.Style.Font.Bold = true;
			this.textBox18.Style.Font.Name = "Arial";
			this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox18.Value = "Red Blood Cell Volume";
			// 
			// textBox16
			// 
			this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D), Telerik.Reporting.Drawing.Unit.Inch(0.81593102216720581D));
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5854169130325317D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox16.Style.Font.Bold = true;
			this.textBox16.Style.Font.Name = "Arial";
			this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox16.Value = "Total Blood Volume";
			// 
			// textBox14
			// 
			this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9999611377716064D), Telerik.Reporting.Drawing.Unit.Inch(0.64587312936782837D));
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3812503814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.15408754348754883D));
			this.textBox14.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox14.Style.Font.Bold = true;
			this.textBox14.Style.Font.Name = "Arial";
			this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox14.Value = "Deviation From Ideal";
			// 
			// textBox11
			// 
			this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.9854955673217773D), Telerik.Reporting.Drawing.Unit.Inch(0.64587306976318359D));
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.900000274181366D), Telerik.Reporting.Drawing.Unit.Inch(0.15408754348754883D));
			this.textBox11.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox11.Style.Font.Bold = true;
			this.textBox11.Style.Font.Name = "Arial";
			this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox11.TextWrap = true;
			this.textBox11.Value = "Result";
			// 
			// panel2
			// 
			this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxNormalizedHct,
            this.textBoxPeripheralHct,
            this.textBox2,
            this.textBox3,
            this.textBox5,
            this.textBox4,
            this.panel3});
			this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9378803194267675E-05D), Telerik.Reporting.Drawing.Unit.Inch(1.7899211645126343D));
			this.panel2.Name = "panel2";
			this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8998820781707764D), Telerik.Reporting.Drawing.Unit.Inch(1.7100000381469727D));
			this.panel2.Style.Font.Name = "Times New Roman";
			// 
			// textBoxNormalizedHct
			// 
			this.textBoxNormalizedHct.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.985456109046936D), Telerik.Reporting.Drawing.Unit.Inch(0.75007885694503784D));
			this.textBoxNormalizedHct.Name = "textBoxNormalizedHct";
			this.textBoxNormalizedHct.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88541668653488159D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxNormalizedHct.Style.Font.Name = "Arial";
			this.textBoxNormalizedHct.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBoxNormalizedHct.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxNormalizedHct.Value = "-";
			// 
			// textBoxPeripheralHct
			// 
			this.textBoxPeripheralHct.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.985456109046936D), Telerik.Reporting.Drawing.Unit.Inch(0.596515953540802D));
			this.textBoxPeripheralHct.Name = "textBoxPeripheralHct";
			this.textBoxPeripheralHct.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88541668653488159D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxPeripheralHct.Style.Font.Name = "Arial";
			this.textBoxPeripheralHct.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPeripheralHct.Value = "-";
			// 
			// textBox2
			// 
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.39996054768562317D), Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5854167938232422D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox2.Style.Font.Bold = true;
			this.textBox2.Style.Font.Name = "Arial";
			this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox2.Value = "Peripheral Hct :";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.39996054768562317D), Telerik.Reporting.Drawing.Unit.Inch(0.75007885694503784D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5854167938232422D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox3.Style.Font.Bold = true;
			this.textBox3.Style.Font.Name = "Arial";
			this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox3.Value = "Normalized Hct (nHct) :";
			// 
			// textBox5
			// 
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.29996064305305481D), Telerik.Reporting.Drawing.Unit.Inch(1.2100000381469727D));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1853771209716797D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
			this.textBox5.Style.Font.Bold = false;
			this.textBox5.Style.Font.Italic = false;
			this.textBox5.Style.Font.Name = "Arial";
			this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox5.Value = "The normalized Hct is the peripheral Hct which would be observed if patient volum" +
    "e is corrected to ideal by adjusting the plasma volume.";
			// 
			// textBox4
			// 
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.29996064305305481D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1854169368743896D), Telerik.Reporting.Drawing.Unit.Inch(0.30000010132789612D));
			this.textBox4.Style.Font.Bold = true;
			this.textBox4.Style.Font.Italic = true;
			this.textBox4.Style.Font.Name = "Arial";
			this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox4.Value = "Hct Analysis";
			// 
			// panel3
			// 
			this.panel3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.29996064305305481D), Telerik.Reporting.Drawing.Unit.Inch(0.49386787414550781D));
			this.panel3.Name = "panel3";
			this.panel3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.50000017881393433D));
			this.panel3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.panel3.Style.Font.Name = "Times New Roman";
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Inch(3.9378803194267675E-05D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4999215602874756D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
			this.textBox1.Style.Font.Bold = true;
			this.textBox1.Style.Font.Italic = true;
			this.textBox1.Style.Font.Name = "Arial";
			this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox1.Value = "Blood Volume Analysis Results";
			// 
			// textBoxTotalExcessDeficitQR
			// 
			this.textBoxTotalExcessDeficitQR.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.481290340423584D), Telerik.Reporting.Drawing.Unit.Inch(0.80003947019577026D));
			this.textBoxTotalExcessDeficitQR.Name = "textBoxTotalExcessDeficitQR";
			this.textBoxTotalExcessDeficitQR.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999603271484375D), Telerik.Reporting.Drawing.Unit.Inch(0.16589139401912689D));
			this.textBoxTotalExcessDeficitQR.Style.Font.Name = "Arial";
			this.textBoxTotalExcessDeficitQR.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxTotalExcessDeficitQR.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxTotalExcessDeficitQR.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxTotalExcessDeficitQR.Value = "-";
			// 
			// textBoxPlasmaExcessDeficitQR
			// 
			this.textBoxPlasmaExcessDeficitQR.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.481290340423584D), Telerik.Reporting.Drawing.Unit.Inch(1.1269785165786743D));
			this.textBoxPlasmaExcessDeficitQR.Name = "textBoxPlasmaExcessDeficitQR";
			this.textBoxPlasmaExcessDeficitQR.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999603271484375D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxPlasmaExcessDeficitQR.Style.Font.Name = "Arial";
			this.textBoxPlasmaExcessDeficitQR.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxPlasmaExcessDeficitQR.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxPlasmaExcessDeficitQR.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPlasmaExcessDeficitQR.Value = "-";
			// 
			// textBox15
			// 
			this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.481290340423584D), Telerik.Reporting.Drawing.Unit.Inch(0.64587312936782837D));
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999603271484375D), Telerik.Reporting.Drawing.Unit.Inch(0.15408754348754883D));
			this.textBox15.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox15.Style.Font.Bold = true;
			this.textBox15.Style.Font.Name = "Arial";
			this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox15.Value = "Excess/Deficit %";
			// 
			// textBoxRBCExcessDeficitQR
			// 
			this.textBoxRBCExcessDeficitQR.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.481290340423584D), Telerik.Reporting.Drawing.Unit.Inch(0.97281217575073242D));
			this.textBoxRBCExcessDeficitQR.Name = "textBoxRBCExcessDeficitQR";
			this.textBoxRBCExcessDeficitQR.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999603271484375D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxRBCExcessDeficitQR.Style.Font.Name = "Arial";
			this.textBoxRBCExcessDeficitQR.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxRBCExcessDeficitQR.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxRBCExcessDeficitQR.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxRBCExcessDeficitQR.Value = "-";
			// 
			// panel4
			// 
			this.panel4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox12,
            this.textBox17,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBoxAlbumanTransudation,
            this.panel1});
			this.panel4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4D), Telerik.Reporting.Drawing.Unit.Inch(1.7899211645126343D));
			this.panel4.Name = "panel4";
			this.panel4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.6895837783813477D), Telerik.Reporting.Drawing.Unit.Inch(1.7099608182907105D));
			this.panel4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.panel4.Style.Font.Name = "Times New Roman";
			// 
			// textBox6
			// 
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.49166679382324219D), Telerik.Reporting.Drawing.Unit.Inch(0.010934829711914063D));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1854169368743896D), Telerik.Reporting.Drawing.Unit.Inch(0.20999994874000549D));
			this.textBox6.Style.Font.Bold = true;
			this.textBox6.Style.Font.Italic = true;
			this.textBox6.Style.Font.Name = "Arial";
			this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox6.Value = "Albumin Transudation";
			// 
			// textBox7
			// 
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.6099211573600769D), Telerik.Reporting.Drawing.Unit.Inch(0.58954781293869019D));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5900000333786011D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox7.Style.Font.Bold = true;
			this.textBox7.Style.Font.Italic = false;
			this.textBox7.Style.Font.Name = "Arial";
			this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox7.Value = "\tAnalysis Slope ( %/min )";
			// 
			// textBox8
			// 
			this.textBox8.Angle = 0D;
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.49166679382324219D), Telerik.Reporting.Drawing.Unit.Inch(0.95960617065429688D));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1000005006790161D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox8.Style.Font.Bold = true;
			this.textBox8.Style.Font.Name = "Arial";
			this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox8.Value = "Reference Range";
			// 
			// textBox9
			// 
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.49166679382324219D), Telerik.Reporting.Drawing.Unit.Inch(1.109684944152832D));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox9.Style.Font.Bold = false;
			this.textBox9.Style.Font.Name = "Arial";
			this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox9.Value = "Normal :";
			// 
			// textBox10
			// 
			this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.49166679382324219D), Telerik.Reporting.Drawing.Unit.Inch(1.2597637176513672D));
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox10.Style.Font.Bold = false;
			this.textBox10.Style.Font.Name = "Arial";
			this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox10.Value = "Elevated :";
			// 
			// textBox12
			// 
			this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.49166679382324219D), Telerik.Reporting.Drawing.Unit.Inch(1.4098424911499023D));
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox12.Style.Font.Bold = false;
			this.textBox12.Style.Font.Name = "Arial";
			this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox12.Value = "High :";
			// 
			// textBox17
			// 
			this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.49166679382324219D), Telerik.Reporting.Drawing.Unit.Inch(1.5599212646484375D));
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox17.Style.Font.Bold = false;
			this.textBox17.Style.Font.Name = "Arial";
			this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox17.Value = "Very High :";
			// 
			// textBox20
			// 
			this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2917455434799194D), Telerik.Reporting.Drawing.Unit.Inch(1.109684944152832D));
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox20.Style.Font.Bold = false;
			this.textBox20.Style.Font.Name = "Arial";
			this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox20.Value = "0 to 0.25";
			// 
			// textBox21
			// 
			this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2917455434799194D), Telerik.Reporting.Drawing.Unit.Inch(1.2597637176513672D));
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox21.Style.Font.Bold = false;
			this.textBox21.Style.Font.Name = "Arial";
			this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox21.Value = "0.25 to 0.4";
			// 
			// textBox22
			// 
			this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2917455434799194D), Telerik.Reporting.Drawing.Unit.Inch(1.4098424911499023D));
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox22.Style.Font.Bold = false;
			this.textBox22.Style.Font.Name = "Arial";
			this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox22.Value = "0.4 to 0.5";
			// 
			// textBox23
			// 
			this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.2917455434799194D), Telerik.Reporting.Drawing.Unit.Inch(1.5600000619888306D));
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox23.Style.Font.Bold = false;
			this.textBox23.Style.Font.Name = "Arial";
			this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox23.Value = ">0.5";
			// 
			// textBoxAlbumanTransudation
			// 
			this.textBoxAlbumanTransudation.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.58954781293869019D));
			this.textBoxAlbumanTransudation.Name = "textBoxAlbumanTransudation";
			this.textBoxAlbumanTransudation.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.57175636291503906D), Telerik.Reporting.Drawing.Unit.Inch(0.15000002086162567D));
			this.textBoxAlbumanTransudation.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBoxAlbumanTransudation.Style.Font.Name = "Arial";
			this.textBoxAlbumanTransudation.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBoxAlbumanTransudation.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBoxAlbumanTransudation.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxAlbumanTransudation.Value = "-";
			// 
			// panel1
			// 
			this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.49166679382324219D), Telerik.Reporting.Drawing.Unit.Inch(0.49386787414550781D));
			this.panel1.Name = "panel1";
			this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.3354167938232422D), Telerik.Reporting.Drawing.Unit.Inch(0.35416683554649353D));
			this.panel1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.panel1.Style.Font.Name = "Times New Roman";
			// 
			// textBox13
			// 
			this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.2000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.64587312936782837D));
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2999998331069946D), Telerik.Reporting.Drawing.Unit.Inch(0.15408754348754883D));
			this.textBox13.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox13.Style.Font.Bold = true;
			this.textBox13.Style.Font.Name = "Arial";
			this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox13.Value = "Flag";
			// 
			// totalBloodVolumeFlag
			// 
			this.totalBloodVolumeFlag.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.2000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.80003929138183594D));
			this.totalBloodVolumeFlag.Name = "totalBloodVolumeFlag";
			this.totalBloodVolumeFlag.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2999998331069946D), Telerik.Reporting.Drawing.Unit.Inch(0.16589148342609406D));
			this.totalBloodVolumeFlag.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.totalBloodVolumeFlag.Style.Font.Bold = false;
			this.totalBloodVolumeFlag.Style.Font.Name = "Arial";
			this.totalBloodVolumeFlag.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.totalBloodVolumeFlag.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.totalBloodVolumeFlag.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.totalBloodVolumeFlag.Value = "-";
			// 
			// redBloodCellVolumeFlag
			// 
			this.redBloodCellVolumeFlag.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.2000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(0.96872454881668091D));
			this.redBloodCellVolumeFlag.Name = "redBloodCellVolumeFlag";
			this.redBloodCellVolumeFlag.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2999998331069946D), Telerik.Reporting.Drawing.Unit.Inch(0.15408754348754883D));
			this.redBloodCellVolumeFlag.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.redBloodCellVolumeFlag.Style.Font.Bold = false;
			this.redBloodCellVolumeFlag.Style.Font.Name = "Arial";
			this.redBloodCellVolumeFlag.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.redBloodCellVolumeFlag.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.redBloodCellVolumeFlag.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.redBloodCellVolumeFlag.Value = "-";
			// 
			// plasmaVolumeFlag
			// 
			this.plasmaVolumeFlag.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.2000002861022949D), Telerik.Reporting.Drawing.Unit.Inch(1.1228909492492676D));
			this.plasmaVolumeFlag.Name = "plasmaVolumeFlag";
			this.plasmaVolumeFlag.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2999998331069946D), Telerik.Reporting.Drawing.Unit.Inch(0.15408754348754883D));
			this.plasmaVolumeFlag.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.plasmaVolumeFlag.Style.Font.Bold = false;
			this.plasmaVolumeFlag.Style.Font.Name = "Arial";
			this.plasmaVolumeFlag.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.plasmaVolumeFlag.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.plasmaVolumeFlag.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.plasmaVolumeFlag.Value = "-";
			// 
			// SubReport_BloodVolumeAnalysisQuickReport
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
			this.Name = "SubReport_BloodVolumeAnalysis";
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Style.Font.Name = "Arial";
			this.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.9000000953674316D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        public Telerik.Reporting.DetailSection detail;
		private Telerik.Reporting.Panel panelBloodVolumeAnalysisResults;
        public Telerik.Reporting.TextBox textBoxPlasmaDeviationFromIdealQR;
        public Telerik.Reporting.TextBox textBoxRBCDeviationFromIdealQR;
		public Telerik.Reporting.TextBox textBoxTotalDeviationFromIdealQR;
        public Telerik.Reporting.TextBox textBoxBVAPlasmaVolumeQR;
        public Telerik.Reporting.TextBox textBoxBVARBCVolumeQR;
        public Telerik.Reporting.TextBox textBoxTotalBloodVolumeResultsQR;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox15;
		private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox11;
        public Telerik.Reporting.TextBox textBoxPlasmaExcessDeficitQR;
        public Telerik.Reporting.TextBox textBoxRBCExcessDeficitQR;
        private Telerik.Reporting.Panel panel2;
        public Telerik.Reporting.TextBox textBoxNormalizedHct;
        public Telerik.Reporting.TextBox textBoxPeripheralHct;
        public Telerik.Reporting.TextBox textBoxTotalExcessDeficitQR;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
		private Telerik.Reporting.TextBox textBox5;
		private Telerik.Reporting.Panel panel3;
		private Telerik.Reporting.Panel panel4;
		private Telerik.Reporting.TextBox textBox6;
		private Telerik.Reporting.TextBox textBox7;
		public Telerik.Reporting.TextBox textBoxAlbumanTransudation;
		private Telerik.Reporting.TextBox textBox8;
		private Telerik.Reporting.TextBox textBox9;
		private Telerik.Reporting.TextBox textBox10;
		private Telerik.Reporting.TextBox textBox12;
		private Telerik.Reporting.TextBox textBox17;
		private Telerik.Reporting.TextBox textBox20;
		private Telerik.Reporting.TextBox textBox21;
		private Telerik.Reporting.TextBox textBox22;
		private Telerik.Reporting.TextBox textBox23;
		private Telerik.Reporting.TextBox textBox13;
		public Telerik.Reporting.TextBox totalBloodVolumeFlag;
		public Telerik.Reporting.TextBox redBloodCellVolumeFlag;
		public Telerik.Reporting.TextBox plasmaVolumeFlag;
		private Telerik.Reporting.Panel panel1;
    }
}