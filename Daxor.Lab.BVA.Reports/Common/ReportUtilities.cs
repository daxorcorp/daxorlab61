﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Daxor.Lab.Database.DataRecord;
using Telerik.Reporting;

namespace Daxor.Lab.BVA.Reports.Common
{
    public class ReportUtilities
    {
        /// <summary>
        /// Finds if a field is mentioned in the audit trail and changes the specified text box value to bold if it is found.
        /// </summary>
        /// <param name="textBox">The text box on the report to format as bold.</param>
        /// <param name="fieldName">The name of the field as described in the audit trail.</param>
        /// <param name="auditRecordList"></param>
        public static void BoldIfInAuditTrail(TextBox textBox, string fieldName, IEnumerable<AuditTrailRecord> auditRecordList)
        {
            var field = from a in auditRecordList
                        where a.NotFriendlyChangeField == fieldName
                        select a;

            if (field.Any())
                textBox.Style.Font.Bold = true;
        }


        /// <summary>
        /// Determines the patients age based on the date of birth and the time of the test.
        /// </summary>
        /// <param name="dob">A DateTime representing the patient's date of birth.</param>
        /// <param name="timeOfTest">A DateTime representing the time the test was run.</param>
        /// <returns>A refference to the string that was created.</returns>
        public static string DobToAge(DateTime dob, DateTime timeOfTest)
        {
            if (dob != null)
            {
                var age = timeOfTest.Year - dob.Year;
                if (dob > timeOfTest.AddYears(-age))
                    age--;

                return age.ToString(CultureInfo.InvariantCulture);
            }

            return "-";
        }

        public static string SecondsToMinutesAndSeconds(int secondsInput)
        {
            var minutes = secondsInput / 60;
            var seconds = secondsInput % 60;

            return minutes + ":" + seconds.ToString("00");
        }

        //** PAndreason & PPresley 11/1/2013
        //This may not be doing what we expect!!
        public static double SecondsToMinutes(int seconds)
        {
            return Math.Round((seconds / 60.0), 0, MidpointRounding.AwayFromZero);
        }

	    public static string BoolToAmputeeStatus(bool isAmputee)
	    {
		    return isAmputee ? "Amputee" : "Non-amputee";
	    }
    }
}
