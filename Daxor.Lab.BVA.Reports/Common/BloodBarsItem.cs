﻿namespace Daxor.Lab.BVA.Reports.Common
{
    public class BloodBarsItem
    {
        public string Series { get; set; }

        public int Value { get; set; }

        public string Type { get; set; }
    }
}
