﻿namespace Daxor.Lab.BVA.Reports.Common
{
    public static class Constants
    {
        public const int EmptyCount = -1;
        public const string CountExcludedLeftMarker = "[";
        public const string CountExcludedRightMarker = "]";
        public const string HematocritExcludedLeftMarker = "[";
        public const string HematocritExcludedRightMarker = "]";
        public const string CountAutoExcludedLeftMarker = "<";
        public const string CountAutoExcludedRightMarker = ">";
        public const string LowStandardCountMarker = "*";
        public const string BeyondAllowableCountDifferenceMarker = "*";
        public const string HematocritOutOfRangeMarker = "*";
        public const string MissingValueMarker = "-";
        public const string FolderSeparator = @"\";
        public const double ChartPaddingInSeconds = 5;
        public const string ExportChoiceText = "Export";
        public const string CancelChoiceText = "Cancel";
        public const string ExportMessageBoxCaption = "Export Blood Volume Analysis Report";
    }
}
