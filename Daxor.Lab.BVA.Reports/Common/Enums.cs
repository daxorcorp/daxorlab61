﻿namespace Daxor.Lab.BVA.Reports.Common
{
    public class Enums
    {
        public enum ReportType
        {
            SummaryReport,			//v6.0.5	Muller	added for the new page 1
			FullReport,
            PhysiciansReport,
            AnalystsReport
        }
    }
}
