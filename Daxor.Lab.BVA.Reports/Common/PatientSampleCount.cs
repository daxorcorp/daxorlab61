﻿using System;

namespace Daxor.Lab.BVA.Reports.Common
{
    public class PatientSampleCount
    {
        public Guid SampleAId { get; set; }

        public Guid SampleBId { get; set; }

        public string SampleName { get; set; }

        public string SampleTime { get; set; }

        public bool SampleTimeInAuditTrail { get; set; }

        public string SampleHctA { get; set; }

        public string SampleHctAExcluded { get; set; }

        public bool SampleHctAInAuditTrail { get; set; }

        public string SampleHctB { get; set; }

        public string SampleHctBExcluded { get; set; }

        public bool SampleHctBInAuditTrail { get; set; }

        public string SampleAvgHct { get; set; }

        public string SampleCountA { get; set; }

        public string SampleCountAExcluded { get; set; }

        public string SampleCountAAutoExcluded { get; set; }

        public bool SampleCountAInAuditTrail { get; set; }

        public string SampleCountB { get; set; }

        public string SampleCountBExcluded { get; set; }

        public string SampleCountBAutoExcluded { get; set; }

        public bool SampleCountBInAuditTrail { get; set; }

        public string SampleAvgCount { get; set; }

        public string SampleUnadjVolume { get; set; }

        public string ExcludedText { get; set; }

        public bool Excluded { get; set; }
    }
}