namespace Daxor.Lab.BVA.Reports
{
    partial class SubReport_Analyst
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.Drawing.FormattingRule formattingRule49 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule50 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule51 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule52 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule53 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule54 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule55 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule56 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule57 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule58 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule59 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule60 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule61 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule62 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule63 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Drawing.FormattingRule formattingRule64 = new Telerik.Reporting.Drawing.FormattingRule();
			Telerik.Reporting.Charting.Styles.ChartPaddings chartPaddings13 = new Telerik.Reporting.Charting.Styles.ChartPaddings();
			Telerik.Reporting.Charting.Styles.ChartMargins chartMargins4 = new Telerik.Reporting.Charting.Styles.ChartMargins();
			Telerik.Reporting.Charting.Styles.ChartPaddings chartPaddings14 = new Telerik.Reporting.Charting.Styles.ChartPaddings();
			Telerik.Reporting.Charting.Styles.ChartPaddings chartPaddings15 = new Telerik.Reporting.Charting.Styles.ChartPaddings();
			Telerik.Reporting.Charting.Styles.ChartPaddings chartPaddings16 = new Telerik.Reporting.Charting.Styles.ChartPaddings();
			Telerik.Reporting.Charting.ChartSeries chartSeries7 = new Telerik.Reporting.Charting.ChartSeries();
			Telerik.Reporting.Charting.ChartSeries chartSeries8 = new Telerik.Reporting.Charting.ChartSeries();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBox28 = new Telerik.Reporting.TextBox();
			this.textBox26 = new Telerik.Reporting.TextBox();
			this.textBox24 = new Telerik.Reporting.TextBox();
			this.textBox34 = new Telerik.Reporting.TextBox();
			this.textBox32 = new Telerik.Reporting.TextBox();
			this.textBox72 = new Telerik.Reporting.TextBox();
			this.textBox30 = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBox58 = new Telerik.Reporting.TextBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.textBox51 = new Telerik.Reporting.TextBox();
			this.shape6 = new Telerik.Reporting.Shape();
			this.textBox52 = new Telerik.Reporting.TextBox();
			this.textBox57 = new Telerik.Reporting.TextBox();
			this.textBox55 = new Telerik.Reporting.TextBox();
			this.textBox56 = new Telerik.Reporting.TextBox();
			this.textBoxStandardDeviation = new Telerik.Reporting.TextBox();
			this.shape9 = new Telerik.Reporting.Shape();
			this.textBox54 = new Telerik.Reporting.TextBox();
			this.textBox53 = new Telerik.Reporting.TextBox();
			this.shape7 = new Telerik.Reporting.Shape();
			this.textBoxTransudation = new Telerik.Reporting.TextBox();
			this.textBox50 = new Telerik.Reporting.TextBox();
			this.textBox49 = new Telerik.Reporting.TextBox();
			this.textBox48 = new Telerik.Reporting.TextBox();
			this.textBox47 = new Telerik.Reporting.TextBox();
			this.textBox46 = new Telerik.Reporting.TextBox();
			this.textBox45 = new Telerik.Reporting.TextBox();
			this.textBox44 = new Telerik.Reporting.TextBox();
			this.textBox43 = new Telerik.Reporting.TextBox();
			this.textBox42 = new Telerik.Reporting.TextBox();
			this.shape10 = new Telerik.Reporting.Shape();
			this.shape8 = new Telerik.Reporting.Shape();
			this.textBox41 = new Telerik.Reporting.TextBox();
			this.panel1 = new Telerik.Reporting.Panel();
			this.shape1 = new Telerik.Reporting.Shape();
			this.textBoxWatermark = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.shape3 = new Telerik.Reporting.Shape();
			this.textBox36 = new Telerik.Reporting.TextBox();
			this.textBox37 = new Telerik.Reporting.TextBox();
			this.textBoxTubeType = new Telerik.Reporting.TextBox();
			this.textBoxBackgroundCount = new Telerik.Reporting.TextBox();
			this.textBoxTestMode = new Telerik.Reporting.TextBox();
			this.textBox38 = new Telerik.Reporting.TextBox();
			this.textBoxSampleAcquisitionTime = new Telerik.Reporting.TextBox();
			this.textBox39 = new Telerik.Reporting.TextBox();
			this.textBox40 = new Telerik.Reporting.TextBox();
			this.textBoxDose = new Telerik.Reporting.TextBox();
			this.tableSampleCounts = new Telerik.Reporting.Table();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.textBox25 = new Telerik.Reporting.TextBox();
			this.textBox27 = new Telerik.Reporting.TextBox();
			this.textBox29 = new Telerik.Reporting.TextBox();
			this.textBox31 = new Telerik.Reporting.TextBox();
			this.textBox33 = new Telerik.Reporting.TextBox();
			this.textBox35 = new Telerik.Reporting.TextBox();
			this.textBox59 = new Telerik.Reporting.TextBox();
			this.textBox73 = new Telerik.Reporting.TextBox();
			this.textBoxLowStandardsWarning = new Telerik.Reporting.TextBox();
			this.regChart = new Telerik.Reporting.Chart();
			this.textBox60 = new Telerik.Reporting.TextBox();
			this.textBoxFinalBloodVolume = new Telerik.Reporting.TextBox();
			this.textBoxSlope = new Telerik.Reporting.TextBox();
			this.textBox62 = new Telerik.Reporting.TextBox();
			this.BloodVolumeAnalysisSubReport = new Telerik.Reporting.SubReport();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.amputeeColorExample = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// textBox12
			// 
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0729166269302368D), Telerik.Reporting.Drawing.Unit.Inch(0.3489583432674408D));
			this.textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox12.Style.Font.Bold = true;
			this.textBox12.Style.Font.Name = "Times New Roman";
			this.textBox12.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox12.Value = "Sample";
			// 
			// textBox17
			// 
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.95833355188369751D), Telerik.Reporting.Drawing.Unit.Inch(0.3489583432674408D));
			this.textBox17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox17.Style.Font.Bold = true;
			this.textBox17.Style.Font.Name = "Times New Roman";
			this.textBox17.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox17.Value = "Time";
			// 
			// textBox28
			// 
			this.textBox28.Name = "textBox28";
			this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.4791666567325592D), Telerik.Reporting.Drawing.Unit.Inch(0.3489583432674408D));
			this.textBox28.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox28.Style.Font.Bold = true;
			this.textBox28.Style.Font.Name = "Times New Roman";
			this.textBox28.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox28.StyleName = "";
			this.textBox28.Value = "Hct-A (%)";
			// 
			// textBox26
			// 
			this.textBox26.Name = "textBox26";
			this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.50000011920928955D), Telerik.Reporting.Drawing.Unit.Inch(0.3489583432674408D));
			this.textBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox26.Style.Font.Bold = true;
			this.textBox26.Style.Font.Name = "Times New Roman";
			this.textBox26.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox26.StyleName = "";
			this.textBox26.Value = "Hct-B (%)";
			// 
			// textBox24
			// 
			this.textBox24.Name = "textBox24";
			this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.46874997019767761D), Telerik.Reporting.Drawing.Unit.Inch(0.3489583432674408D));
			this.textBox24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox24.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox24.Style.Font.Bold = true;
			this.textBox24.Style.Font.Name = "Times New Roman";
			this.textBox24.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox24.StyleName = "";
			this.textBox24.Value = "Avg Hct";
			// 
			// textBox34
			// 
			this.textBox34.Name = "textBox34";
			this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.3489583432674408D));
			this.textBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox34.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox34.Style.Font.Bold = true;
			this.textBox34.Style.Font.Name = "Times New Roman";
			this.textBox34.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox34.StyleName = "";
			this.textBox34.Value = "Count-A";
			// 
			// textBox32
			// 
			this.textBox32.Name = "textBox32";
			this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.3489583432674408D));
			this.textBox32.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox32.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox32.Style.Font.Bold = true;
			this.textBox32.Style.Font.Name = "Times New Roman";
			this.textBox32.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox32.StyleName = "";
			this.textBox32.Value = "Count-B";
			// 
			// textBox72
			// 
			this.textBox72.Name = "textBox72";
			this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.21874992549419403D), Telerik.Reporting.Drawing.Unit.Inch(0.3489583432674408D));
			this.textBox72.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox72.Style.Font.Bold = true;
			this.textBox72.Style.Font.Name = "Times New Roman";
			this.textBox72.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox72.StyleName = "";
			// 
			// textBox30
			// 
			this.textBox30.Name = "textBox30";
			this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49999997019767761D), Telerik.Reporting.Drawing.Unit.Inch(0.3489583432674408D));
			this.textBox30.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox30.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox30.Style.Font.Bold = true;
			this.textBox30.Style.Font.Name = "Times New Roman";
			this.textBox30.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox30.StyleName = "";
			this.textBox30.Value = "Avg     Count";
			// 
			// textBox20
			// 
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.9800000786781311D), Telerik.Reporting.Drawing.Unit.Inch(0.3489583432674408D));
			this.textBox20.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox20.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox20.Style.Font.Bold = true;
			this.textBox20.Style.Font.Name = "Times New Roman";
			this.textBox20.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox20.Value = "Unadjusted Volume (mL)";
			// 
			// textBox58
			// 
			this.textBox58.Name = "textBox58";
			this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.699999988079071D), Telerik.Reporting.Drawing.Unit.Inch(0.3489583432674408D));
			this.textBox58.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox58.Style.Font.Bold = true;
			this.textBox58.Style.Font.Name = "Times New Roman";
			this.textBox58.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox58.StyleName = "";
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(7.0284123420715332D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox51,
            this.shape6,
            this.textBox52,
            this.textBox57,
            this.textBox55,
            this.textBox56,
            this.textBoxStandardDeviation,
            this.shape9,
            this.textBox54,
            this.textBox53,
            this.shape7,
            this.textBoxTransudation,
            this.textBox50,
            this.textBox49,
            this.textBox48,
            this.textBox47,
            this.textBox46,
            this.textBox45,
            this.textBox44,
            this.textBox43,
            this.textBox42,
            this.shape10,
            this.shape8,
            this.textBox41,
            this.panel1,
            this.regChart,
            this.textBox60,
            this.textBoxFinalBloodVolume,
            this.textBoxSlope,
            this.textBox62,
            this.BloodVolumeAnalysisSubReport,
            this.textBox1,
            this.amputeeColorExample,
            this.textBox2,
            this.textBox3});
			this.detail.Name = "detail";
			this.detail.PageBreak = Telerik.Reporting.PageBreak.None;
			// 
			// textBox51
			// 
			this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.041661936789751053D), Telerik.Reporting.Drawing.Unit.Inch(6.6201176643371582D));
			this.textBox51.Name = "textBox51";
			this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7916665077209473D), Telerik.Reporting.Drawing.Unit.Inch(0.3125D));
			this.textBox51.Style.Font.Name = "Times New Roman";
			this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox51.Value = "Report Key:           = sample excluded,  Bold = audit trail,  ** = data cannot b" +
    "e calculated,   * = data out of range,   [ ] = entry excluded,   - = data missin" +
    "g,";
			// 
			// shape6
			// 
			this.shape6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.019999980926513672D), Telerik.Reporting.Drawing.Unit.Inch(5.2700786590576172D));
			this.shape6.Name = "shape6";
			this.shape6.ShapeType = new Telerik.Reporting.Drawing.Shapes.PolygonShape(4, 45D, 0);
			this.shape6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7963333129882812D), Telerik.Reporting.Drawing.Unit.Inch(1.3562101125717163D));
			this.shape6.Stretch = true;
			this.shape6.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
			// 
			// textBox52
			// 
			this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.041661936789751053D), Telerik.Reporting.Drawing.Unit.Inch(6.8617458343505859D));
			this.textBox52.Name = "textBox52";
			this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8958334922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.1666666716337204D));
			this.textBox52.Style.Font.Name = "Times New Roman";
			this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox52.TextWrap = false;
			this.textBox52.Value = "Ideal Values based on Feldschuh, J and Enson, Y. Prediction of the normal blood v" +
    "olume: Relation of blood volume to body habitus.  Circulation. 1977;56:605-612.";
			// 
			// textBox57
			// 
			this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.562495231628418D), Telerik.Reporting.Drawing.Unit.Inch(6.0909123420715332D));
			this.textBox57.Name = "textBox57";
			this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999960660934448D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox57.Style.Font.Name = "Times New Roman";
			this.textBox57.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox57.Value = "<3.9%";
			// 
			// textBox55
			// 
			this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.4479118585586548D), Telerik.Reporting.Drawing.Unit.Inch(5.6742458343505859D));
			this.textBox55.Name = "textBox55";
			this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.849999725818634D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox55.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox55.Style.Font.Bold = true;
			this.textBox55.Style.Font.Name = "Times New Roman";
			this.textBox55.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox55.Value = "Acceptance Range";
			// 
			// textBox56
			// 
			this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.40624523162841797D), Telerik.Reporting.Drawing.Unit.Inch(5.6742458343505859D));
			this.textBox56.Name = "textBox56";
			this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.599999725818634D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox56.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox56.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox56.Style.Font.Bold = true;
			this.textBox56.Style.Font.Name = "Times New Roman";
			this.textBox56.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox56.Value = "Patient Result";
			// 
			// textBoxStandardDeviation
			// 
			this.textBoxStandardDeviation.CanGrow = false;
			this.textBoxStandardDeviation.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.072911940515041351D), Telerik.Reporting.Drawing.Unit.Inch(6.0909123420715332D));
			this.textBoxStandardDeviation.Name = "textBoxStandardDeviation";
			this.textBoxStandardDeviation.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4061712026596069D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxStandardDeviation.Style.Font.Name = "Times New Roman";
			this.textBoxStandardDeviation.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxStandardDeviation.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxStandardDeviation.Value = "-";
			// 
			// shape9
			// 
			this.shape9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.083328604698181152D), Telerik.Reporting.Drawing.Unit.Inch(5.7159123420715332D));
			this.shape9.Name = "shape9";
			this.shape9.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2062499523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
			this.shape9.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
			// 
			// textBox54
			// 
			this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.20832855999469757D), Telerik.Reporting.Drawing.Unit.Inch(5.3096623420715332D));
			this.textBox54.Name = "textBox54";
			this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1060926914215088D), Telerik.Reporting.Drawing.Unit.Inch(0.39992141723632812D));
			this.textBox54.Style.Font.Bold = true;
			this.textBox54.Style.Font.Name = "Times New Roman";
			this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox54.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox54.Value = "Standard Deviation";
			// 
			// textBox53
			// 
			this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.72680997848510742D), Telerik.Reporting.Drawing.Unit.Inch(6.6548404693603516D));
			this.textBox53.Name = "textBox53";
			this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.1458333283662796D), Telerik.Reporting.Drawing.Unit.Inch(0.0798611119389534D));
			this.textBox53.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox53.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox53.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox53.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox53.Value = "";
			// 
			// shape7
			// 
			this.shape7.Angle = 15D;
			this.shape7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.218745231628418D), Telerik.Reporting.Drawing.Unit.Inch(5.3200793266296387D));
			this.shape7.Name = "shape7";
			this.shape7.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.NS);
			this.shape7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.19162750244140625D), Telerik.Reporting.Drawing.Unit.Inch(1.2479158639907837D));
			// 
			// textBoxTransudation
			// 
			this.textBoxTransudation.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5833287239074707D), Telerik.Reporting.Drawing.Unit.Inch(5.9867458343505859D));
			this.textBoxTransudation.Name = "textBoxTransudation";
			this.textBoxTransudation.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59999960660934448D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBoxTransudation.Style.Font.Name = "Times New Roman";
			this.textBoxTransudation.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxTransudation.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxTransudation.Value = "-";
			// 
			// textBox50
			// 
			this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.0100002288818359D), Telerik.Reporting.Drawing.Unit.Inch(6.3899998664855957D));
			this.textBox50.Name = "textBox50";
			this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80000066757202148D), Telerik.Reporting.Drawing.Unit.Inch(0.20617198944091797D));
			this.textBox50.Style.Font.Name = "Times New Roman";
			this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox50.Value = ">0.5";
			// 
			// textBox49
			// 
			this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.7899999618530273D), Telerik.Reporting.Drawing.Unit.Inch(6.4000000953674316D));
			this.textBox49.Name = "textBox49";
			this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1105340719223023D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox49.Style.Font.Name = "Times New Roman";
			this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox49.Value = "Very High:";
			// 
			// textBox48
			// 
			this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.0100002288818359D), Telerik.Reporting.Drawing.Unit.Inch(6.1999998092651367D));
			this.textBox48.Name = "textBox48";
			this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80000066757202148D), Telerik.Reporting.Drawing.Unit.Inch(0.20617198944091797D));
			this.textBox48.Style.Font.Name = "Times New Roman";
			this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox48.Value = "0.4 to 0.5";
			// 
			// textBox47
			// 
			this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3600001335144043D), Telerik.Reporting.Drawing.Unit.Inch(6.1999998092651367D));
			this.textBox47.Name = "textBox47";
			this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53958255052566528D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox47.Style.Font.Name = "Times New Roman";
			this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox47.Value = "High:";
			// 
			// textBox46
			// 
			this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.0100002288818359D), Telerik.Reporting.Drawing.Unit.Inch(5.8499999046325684D));
			this.textBox46.Name = "textBox46";
			this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70000028610229492D), Telerik.Reporting.Drawing.Unit.Inch(0.20617198944091797D));
			this.textBox46.Style.Font.Name = "Times New Roman";
			this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox46.Value = "0 to 0.25";
			// 
			// textBox45
			// 
			this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3600001335144043D), Telerik.Reporting.Drawing.Unit.Inch(5.8499999046325684D));
			this.textBox45.Name = "textBox45";
			this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53958255052566528D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox45.Style.Font.Name = "Times New Roman";
			this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox45.Value = "Normal:";
			// 
			// textBox44
			// 
			this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3020787239074707D), Telerik.Reporting.Drawing.Unit.Inch(5.4499998092651367D));
			this.textBox44.Name = "textBox44";
			this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3290481567382813D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox44.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox44.Style.Font.Bold = true;
			this.textBox44.Style.Font.Name = "Times New Roman";
			this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox44.Value = "Reference Range";
			// 
			// textBox43
			// 
			this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5833287239074707D), Telerik.Reporting.Drawing.Unit.Inch(5.6013293266296387D));
			this.textBox43.Name = "textBox43";
			this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.599999725818634D), Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D));
			this.textBox43.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox43.Style.Font.Bold = true;
			this.textBox43.Style.Font.Name = "Times New Roman";
			this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox43.Value = "Patient Result";
			// 
			// textBox42
			// 
			this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.5416622161865234D), Telerik.Reporting.Drawing.Unit.Inch(5.2575793266296387D));
			this.textBox42.Name = "textBox42";
			this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1060926914215088D), Telerik.Reporting.Drawing.Unit.Inch(0.42075476050376892D));
			this.textBox42.Style.Font.Bold = true;
			this.textBox42.Style.Font.Name = "Times New Roman";
			this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox42.Value = "Albumin Transudation Analysis/Slope (%/min)";
			// 
			// shape10
			// 
			this.shape10.Angle = 15D;
			this.shape10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.4166622161865234D), Telerik.Reporting.Drawing.Unit.Inch(5.3200793266296387D));
			this.shape10.Name = "shape10";
			this.shape10.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.NS);
			this.shape10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.19162750244140625D), Telerik.Reporting.Drawing.Unit.Inch(1.2624994516372681D));
			// 
			// shape8
			// 
			this.shape8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.6041622161865234D), Telerik.Reporting.Drawing.Unit.Inch(5.6534123420715332D));
			this.shape8.Name = "shape8";
			this.shape8.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1229164600372314D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
			// 
			// textBox41
			// 
			this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.019999999552965164D), Telerik.Reporting.Drawing.Unit.Inch(4.96999979019165D));
			this.textBox41.Name = "textBox41";
			this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7899999618530273D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
			this.textBox41.Style.BackgroundColor = System.Drawing.Color.Black;
			this.textBox41.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox41.Style.Color = System.Drawing.Color.White;
			this.textBox41.Style.Font.Bold = true;
			this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox41.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox41.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox41.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox41.TextWrap = false;
			this.textBox41.Value = "Sample Data Analysis";
			// 
			// panel1
			// 
			this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.shape1,
            this.textBoxWatermark,
            this.textBox5,
            this.shape3,
            this.textBox36,
            this.textBox37,
            this.textBoxTubeType,
            this.textBoxBackgroundCount,
            this.textBoxTestMode,
            this.textBox38,
            this.textBoxSampleAcquisitionTime,
            this.textBox39,
            this.textBox40,
            this.textBoxDose,
            this.tableSampleCounts,
            this.textBoxLowStandardsWarning});
			this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.019999999552965164D), Telerik.Reporting.Drawing.Unit.Inch(9.9341077586245774E-09D));
			this.panel1.Name = "panel1";
			this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8125D), Telerik.Reporting.Drawing.Unit.Inch(2.7249221801757812D));
			// 
			// shape1
			// 
			this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.34999999403953552D));
			this.shape1.Name = "shape1";
			this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.PolygonShape(4, 45D, 0);
			this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7963333129882812D), Telerik.Reporting.Drawing.Unit.Inch(2.3749220371246338D));
			this.shape1.Stretch = true;
			// 
			// textBoxWatermark
			// 
			this.textBoxWatermark.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.35007879137992859D));
			this.textBoxWatermark.Name = "textBoxWatermark";
			this.textBoxWatermark.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8125D), Telerik.Reporting.Drawing.Unit.Inch(1.9166666269302368D));
			this.textBoxWatermark.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.textBoxWatermark.Style.Font.Bold = true;
			this.textBoxWatermark.Style.Font.Name = "Times New Roman";
			this.textBoxWatermark.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(50D);
			this.textBoxWatermark.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxWatermark.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxWatermark.Value = "";
			// 
			// textBox5
			// 
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7899999618530273D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
			this.textBox5.Style.BackgroundColor = System.Drawing.Color.Black;
			this.textBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox5.Style.Color = System.Drawing.Color.White;
			this.textBox5.Style.Font.Bold = true;
			this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox5.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox5.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox5.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox5.TextWrap = false;
			this.textBox5.Value = "Patient Sample Counts";
			// 
			// shape3
			// 
			this.shape3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.90625D));
			this.shape3.Name = "shape3";
			this.shape3.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5270838737487793D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
			// 
			// textBox36
			// 
			this.textBox36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.091106414794921875D), Telerik.Reporting.Drawing.Unit.Inch(0.96875D));
			this.textBox36.Name = "textBox36";
			this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5291666984558106D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox36.Style.Font.Name = "Times New Roman";
			this.textBox36.Value = "Room Background Count:";
			// 
			// textBox37
			// 
			this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.091106414794921875D), Telerik.Reporting.Drawing.Unit.Inch(1.1145833730697632D));
			this.textBox37.Name = "textBox37";
			this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.38124999403953552D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox37.Style.Font.Name = "Times New Roman";
			this.textBox37.Value = "Tube:";
			// 
			// textBoxTubeType
			// 
			this.textBoxTubeType.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.47243505716323853D), Telerik.Reporting.Drawing.Unit.Inch(1.1145833730697632D));
			this.textBoxTubeType.Name = "textBoxTubeType";
			this.textBoxTubeType.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9999210834503174D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBoxTubeType.Style.Font.Name = "Times New Roman";
			this.textBoxTubeType.Value = "-";
			// 
			// textBoxBackgroundCount
			// 
			this.textBoxBackgroundCount.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6203517913818359D), Telerik.Reporting.Drawing.Unit.Inch(0.96875011920928955D));
			this.textBoxBackgroundCount.Name = "textBoxBackgroundCount";
			this.textBoxBackgroundCount.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.46658745408058167D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBoxBackgroundCount.Style.Font.Name = "Times New Roman";
			this.textBoxBackgroundCount.Value = "-";
			// 
			// textBoxTestMode
			// 
			this.textBoxTestMode.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5911064147949219D), Telerik.Reporting.Drawing.Unit.Inch(0.96875D));
			this.textBoxTestMode.Name = "textBoxTestMode";
			this.textBoxTestMode.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9832544326782227D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBoxTestMode.Style.Font.Name = "Times New Roman";
			this.textBoxTestMode.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxTestMode.Value = "";
			// 
			// textBox38
			// 
			this.textBox38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.6848559379577637D), Telerik.Reporting.Drawing.Unit.Inch(0.96875D));
			this.textBox38.Name = "textBox38";
			this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7062500715255737D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox38.Style.Font.Name = "Times New Roman";
			this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox38.Value = "Sample Acquisition Time:";
			// 
			// textBoxSampleAcquisitionTime
			// 
			this.textBoxSampleAcquisitionTime.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3931903839111328D), Telerik.Reporting.Drawing.Unit.Inch(0.96875D));
			this.textBoxSampleAcquisitionTime.Name = "textBoxSampleAcquisitionTime";
			this.textBoxSampleAcquisitionTime.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1290876865386963D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBoxSampleAcquisitionTime.Style.Font.Name = "Times New Roman";
			this.textBoxSampleAcquisitionTime.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxSampleAcquisitionTime.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBoxSampleAcquisitionTime.Value = "-";
			// 
			// textBox39
			// 
			this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.6848559379577637D), Telerik.Reporting.Drawing.Unit.Inch(1.1145833730697632D));
			this.textBox39.Name = "textBox39";
			this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2583333253860474D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox39.Style.Font.Name = "Times New Roman";
			this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox39.Value = "Isotope: Iodine-131";
			// 
			// textBox40
			// 
			this.textBox40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.9348559379577637D), Telerik.Reporting.Drawing.Unit.Inch(1.1145833730697632D));
			this.textBox40.Name = "textBox40";
			this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.45412778854370117D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
			this.textBox40.Style.Font.Name = "Times New Roman";
			this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox40.Value = "Dose:";
			// 
			// textBoxDose
			// 
			this.textBoxDose.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3931903839111328D), Telerik.Reporting.Drawing.Unit.Inch(1.1145833730697632D));
			this.textBoxDose.Name = "textBoxDose";
			this.textBoxDose.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1290876865386963D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBoxDose.Style.Font.Name = "Times New Roman";
			this.textBoxDose.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxDose.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBoxDose.Value = "-";
			// 
			// tableSampleCounts
			// 
			this.tableSampleCounts.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0729186534881592D)));
			this.tableSampleCounts.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.95833432674407959D)));
			this.tableSampleCounts.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.47916704416275024D)));
			this.tableSampleCounts.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.50000101327896118D)));
			this.tableSampleCounts.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.468750536441803D)));
			this.tableSampleCounts.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D)));
			this.tableSampleCounts.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D)));
			this.tableSampleCounts.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.21874992549419403D)));
			this.tableSampleCounts.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.50000017881393433D)));
			this.tableSampleCounts.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.98000055551528931D)));
			this.tableSampleCounts.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.699999988079071D)));
			this.tableSampleCounts.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19270829856395721D)));
			this.tableSampleCounts.Body.SetCellContent(0, 0, this.textBox21);
			this.tableSampleCounts.Body.SetCellContent(0, 1, this.textBox22);
			this.tableSampleCounts.Body.SetCellContent(0, 9, this.textBox23);
			this.tableSampleCounts.Body.SetCellContent(0, 4, this.textBox25);
			this.tableSampleCounts.Body.SetCellContent(0, 3, this.textBox27);
			this.tableSampleCounts.Body.SetCellContent(0, 2, this.textBox29);
			this.tableSampleCounts.Body.SetCellContent(0, 8, this.textBox31);
			this.tableSampleCounts.Body.SetCellContent(0, 6, this.textBox33);
			this.tableSampleCounts.Body.SetCellContent(0, 5, this.textBox35);
			this.tableSampleCounts.Body.SetCellContent(0, 10, this.textBox59);
			this.tableSampleCounts.Body.SetCellContent(0, 7, this.textBox73);
			tableGroup37.ReportItem = this.textBox12;
			tableGroup38.ReportItem = this.textBox17;
			tableGroup39.Name = "Group3";
			tableGroup39.ReportItem = this.textBox28;
			tableGroup40.Name = "Group2";
			tableGroup40.ReportItem = this.textBox26;
			tableGroup41.Name = "Group1";
			tableGroup41.ReportItem = this.textBox24;
			tableGroup42.Name = "Group6";
			tableGroup42.ReportItem = this.textBox34;
			tableGroup43.Name = "Group5";
			tableGroup43.ReportItem = this.textBox32;
			tableGroup44.Name = "Group8";
			tableGroup44.ReportItem = this.textBox72;
			tableGroup45.Name = "Group4";
			tableGroup45.ReportItem = this.textBox30;
			tableGroup46.ReportItem = this.textBox20;
			tableGroup47.Name = "Group7";
			tableGroup47.ReportItem = this.textBox58;
			this.tableSampleCounts.ColumnGroups.Add(tableGroup37);
			this.tableSampleCounts.ColumnGroups.Add(tableGroup38);
			this.tableSampleCounts.ColumnGroups.Add(tableGroup39);
			this.tableSampleCounts.ColumnGroups.Add(tableGroup40);
			this.tableSampleCounts.ColumnGroups.Add(tableGroup41);
			this.tableSampleCounts.ColumnGroups.Add(tableGroup42);
			this.tableSampleCounts.ColumnGroups.Add(tableGroup43);
			this.tableSampleCounts.ColumnGroups.Add(tableGroup44);
			this.tableSampleCounts.ColumnGroups.Add(tableGroup45);
			this.tableSampleCounts.ColumnGroups.Add(tableGroup46);
			this.tableSampleCounts.ColumnGroups.Add(tableGroup47);
			this.tableSampleCounts.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox25,
            this.textBox27,
            this.textBox29,
            this.textBox31,
            this.textBox33,
            this.textBox35,
            this.textBox59,
            this.textBox73,
            this.textBox12,
            this.textBox17,
            this.textBox28,
            this.textBox26,
            this.textBox24,
            this.textBox34,
            this.textBox32,
            this.textBox72,
            this.textBox30,
            this.textBox20,
            this.textBox58});
			this.tableSampleCounts.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.3541666567325592D));
			this.tableSampleCounts.Name = "tableSampleCounts";
			tableGroup48.Groupings.Add(new Telerik.Reporting.Grouping(""));
			tableGroup48.Name = "DetailGroup";
			this.tableSampleCounts.RowGroups.Add(tableGroup48);
			this.tableSampleCounts.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.4779224395751953D), Telerik.Reporting.Drawing.Unit.Inch(0.54166662693023682D));
			this.tableSampleCounts.ItemDataBinding += new System.EventHandler(this.tableSampleCounts_ItemDataBinding);
			this.tableSampleCounts.ItemDataBound += new System.EventHandler(this.tableSampleCounts_ItemDataBound);
			// 
			// textBox21
			// 
			formattingRule49.Filters.Add(new Telerik.Reporting.Filter("=Fields.Excluded", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule49.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule49.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule49.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox21.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule49});
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0729166269302368D), Telerik.Reporting.Drawing.Unit.Inch(0.1927083283662796D));
			this.textBox21.Style.Font.Name = "Times New Roman";
			this.textBox21.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.02500000037252903D);
			this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox21.Value = "=Fields.SampleName";
			// 
			// textBox22
			// 
			formattingRule50.Filters.Add(new Telerik.Reporting.Filter("=Fields.Excluded", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule50.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule50.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule51.Filters.Add(new Telerik.Reporting.Filter("=SampleTimeInAuditTrail", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule51.Style.Font.Bold = true;
			this.textBox22.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule50,
            formattingRule51});
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.95833367109298706D), Telerik.Reporting.Drawing.Unit.Inch(0.19270831346511841D));
			this.textBox22.Style.Font.Name = "Times New Roman";
			this.textBox22.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.02500000037252903D);
			this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox22.Value = "=Fields.SampleTime";
			// 
			// textBox23
			// 
			formattingRule52.Filters.Add(new Telerik.Reporting.Filter("=Fields.Excluded", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule52.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule52.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			formattingRule52.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox23.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule52});
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.9800000786781311D), Telerik.Reporting.Drawing.Unit.Inch(0.19270829856395721D));
			this.textBox23.Style.Font.Name = "Times New Roman";
			this.textBox23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.02500000037252903D);
			this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox23.Value = "=Fields.SampleUnadjVolume";
			// 
			// textBox25
			// 
			formattingRule53.Filters.Add(new Telerik.Reporting.Filter("=Fields.Excluded", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule53.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule53.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox25.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule53});
			this.textBox25.Name = "textBox25";
			this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.46874997019767761D), Telerik.Reporting.Drawing.Unit.Inch(0.19270829856395721D));
			this.textBox25.Style.Font.Name = "Times New Roman";
			this.textBox25.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.02500000037252903D);
			this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox25.StyleName = "";
			this.textBox25.Value = "=Fields.SampleAvgHct";
			// 
			// textBox27
			// 
			formattingRule54.Filters.Add(new Telerik.Reporting.Filter("=Fields.Excluded", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule54.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule54.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule55.Filters.Add(new Telerik.Reporting.Filter("=SampleHctBInAuditTrail", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule55.Style.Font.Bold = true;
			this.textBox27.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule54,
            formattingRule55});
			this.textBox27.Name = "textBox27";
			this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.50000011920928955D), Telerik.Reporting.Drawing.Unit.Inch(0.19270829856395721D));
			this.textBox27.Style.Font.Name = "Times New Roman";
			this.textBox27.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.02500000037252903D);
			this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox27.StyleName = "";
			this.textBox27.Value = "=Fields.SampleHctB";
			// 
			// textBox29
			// 
			formattingRule56.Filters.Add(new Telerik.Reporting.Filter("=Fields.Excluded", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule56.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule56.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule57.Filters.Add(new Telerik.Reporting.Filter("=SampleHctAInAuditTrail", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule57.Style.Font.Bold = true;
			this.textBox29.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule56,
            formattingRule57});
			this.textBox29.Name = "textBox29";
			this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.4791666567325592D), Telerik.Reporting.Drawing.Unit.Inch(0.19270829856395721D));
			this.textBox29.Style.Font.Name = "Times New Roman";
			this.textBox29.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.02500000037252903D);
			this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox29.StyleName = "";
			this.textBox29.Value = "=Fields.SampleHctA";
			// 
			// textBox31
			// 
			formattingRule58.Filters.Add(new Telerik.Reporting.Filter("=Fields.Excluded", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule58.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule58.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox31.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule58});
			this.textBox31.Name = "textBox31";
			this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49999997019767761D), Telerik.Reporting.Drawing.Unit.Inch(0.19270829856395721D));
			this.textBox31.Style.Font.Name = "Times New Roman";
			this.textBox31.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.02500000037252903D);
			this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox31.StyleName = "";
			this.textBox31.Value = "=Fields.SampleAvgCount";
			// 
			// textBox33
			// 
			formattingRule59.Filters.Add(new Telerik.Reporting.Filter("=Fields.Excluded", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule59.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule59.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule60.Filters.Add(new Telerik.Reporting.Filter("=SampleCountBInAuditTrail", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule60.Style.Font.Bold = true;
			this.textBox33.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule59,
            formattingRule60});
			this.textBox33.Name = "textBox33";
			this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.19270829856395721D));
			this.textBox33.Style.Font.Name = "Times New Roman";
			this.textBox33.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.02500000037252903D);
			this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox33.StyleName = "";
			this.textBox33.Value = "=Fields.SampleCountB";
			// 
			// textBox35
			// 
			formattingRule61.Filters.Add(new Telerik.Reporting.Filter("=Fields.Excluded", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule61.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule61.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule62.Filters.Add(new Telerik.Reporting.Filter("=SampleCountAInAuditTrail", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule62.Style.Font.Bold = true;
			this.textBox35.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule61,
            formattingRule62});
			this.textBox35.Name = "textBox35";
			this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.19270829856395721D));
			this.textBox35.Style.Font.Name = "Times New Roman";
			this.textBox35.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.02500000037252903D);
			this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox35.StyleName = "";
			this.textBox35.Value = "=Fields.SampleCountA";
			// 
			// textBox59
			// 
			formattingRule63.Filters.Add(new Telerik.Reporting.Filter("=Fields.Excluded", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule63.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule63.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule63.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox59.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule63});
			this.textBox59.Name = "textBox59";
			this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.699999988079071D), Telerik.Reporting.Drawing.Unit.Inch(0.19270829856395721D));
			this.textBox59.Style.Font.Name = "Times New Roman";
			this.textBox59.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.02500000037252903D);
			this.textBox59.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox59.StyleName = "";
			this.textBox59.Value = "=Fields.ExcludedText";
			// 
			// textBox73
			// 
			formattingRule64.Filters.Add(new Telerik.Reporting.Filter("Fields.Excluded", Telerik.Reporting.FilterOperator.Equal, "true"));
			formattingRule64.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			formattingRule64.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox73.ConditionalFormatting.AddRange(new Telerik.Reporting.Drawing.FormattingRule[] {
            formattingRule64});
			this.textBox73.Name = "textBox73";
			this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.21874992549419403D), Telerik.Reporting.Drawing.Unit.Inch(0.19270829856395721D));
			this.textBox73.Style.Font.Name = "Times New Roman";
			this.textBox73.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.02500000037252903D);
			this.textBox73.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox73.StyleName = "";
			// 
			// textBoxLowStandardsWarning
			// 
			this.textBoxLowStandardsWarning.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8306896686553955D), Telerik.Reporting.Drawing.Unit.Inch(1.1145833730697632D));
			this.textBoxLowStandardsWarning.Name = "textBoxLowStandardsWarning";
			this.textBoxLowStandardsWarning.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5665878057479858D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBoxLowStandardsWarning.Style.Font.Name = "Times New Roman";
			this.textBoxLowStandardsWarning.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxLowStandardsWarning.Value = "";
			// 
			// regChart
			// 
			this.regChart.Appearance.Border.Color = System.Drawing.Color.White;
			this.regChart.BitmapResolution = 96F;
			this.regChart.ChartTitle.Appearance.Position.AlignedPosition = Telerik.Reporting.Charting.Styles.AlignedPositions.Center;
			this.regChart.ChartTitle.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.Red;
			this.regChart.ChartTitle.TextBlock.Appearance.TextProperties.Font = new System.Drawing.Font("Times New Roman", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.regChart.ChartTitle.TextBlock.Text = "Insufficient Data";
			this.regChart.ImageFormat = System.Drawing.Imaging.ImageFormat.Emf;
			chartPaddings13.Bottom = new Telerik.Reporting.Charting.Styles.Unit(2D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings13.Left = new Telerik.Reporting.Charting.Styles.Unit(3D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings13.Right = new Telerik.Reporting.Charting.Styles.Unit(2D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings13.Top = new Telerik.Reporting.Charting.Styles.Unit(10D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.regChart.Legend.Appearance.Dimensions.Paddings = chartPaddings13;
			this.regChart.Legend.Appearance.ItemTextAppearance.TextProperties.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.regChart.Legend.Appearance.Visible = false;
			this.regChart.Legend.Visible = false;
			this.regChart.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.3521573543548584D), Telerik.Reporting.Drawing.Unit.Inch(5.3200793266296387D));
			this.regChart.Name = "regChart";
			this.regChart.PlotArea.Appearance.Border.Color = System.Drawing.Color.Transparent;
			chartMargins4.Bottom = new Telerik.Reporting.Charting.Styles.Unit(15D, Telerik.Reporting.Charting.Styles.UnitType.Percentage);
			chartMargins4.Left = new Telerik.Reporting.Charting.Styles.Unit(15D, Telerik.Reporting.Charting.Styles.UnitType.Percentage);
			chartMargins4.Right = new Telerik.Reporting.Charting.Styles.Unit(10D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartMargins4.Top = new Telerik.Reporting.Charting.Styles.Unit(10D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.regChart.PlotArea.Appearance.Dimensions.Margins = chartMargins4;
			chartPaddings14.Bottom = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings14.Left = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings14.Right = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings14.Top = new Telerik.Reporting.Charting.Styles.Unit(10D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.regChart.PlotArea.Appearance.Dimensions.Paddings = chartPaddings14;
			this.regChart.PlotArea.Appearance.FillStyle.FillType = Telerik.Reporting.Charting.Styles.FillType.Solid;
			this.regChart.PlotArea.Appearance.FillStyle.MainColor = System.Drawing.Color.White;
			this.regChart.PlotArea.Appearance.FillStyle.SecondColor = System.Drawing.Color.White;
			this.regChart.PlotArea.EmptySeriesMessage.TextBlock.Text = "Insufficient Data";
			this.regChart.PlotArea.Visible = false;
			this.regChart.PlotArea.XAxis.Appearance.LabelAppearance.Position.AlignedPosition = Telerik.Reporting.Charting.Styles.AlignedPositions.Left;
			this.regChart.PlotArea.XAxis.Appearance.MajorGridLines.Color = System.Drawing.Color.Black;
			this.regChart.PlotArea.XAxis.Appearance.MajorGridLines.Visible = false;
			this.regChart.PlotArea.XAxis.Appearance.MajorTick.Color = System.Drawing.Color.Black;
			this.regChart.PlotArea.XAxis.Appearance.TextAppearance.Border.Color = System.Drawing.Color.Transparent;
			chartPaddings15.Left = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartPaddings15.Top = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.regChart.PlotArea.XAxis.Appearance.TextAppearance.Dimensions.Paddings = chartPaddings15;
			this.regChart.PlotArea.XAxis.Appearance.TextAppearance.TextProperties.Color = System.Drawing.Color.Black;
			this.regChart.PlotArea.XAxis.Appearance.TextAppearance.TextProperties.Font = new System.Drawing.Font("Arial", 7F);
			this.regChart.PlotArea.XAxis.AxisLabel.Appearance.Border.Color = System.Drawing.Color.Black;
			this.regChart.PlotArea.XAxis.AxisLabel.Appearance.Dimensions.AutoSize = false;
			this.regChart.PlotArea.XAxis.AxisLabel.Appearance.Dimensions.Height = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.regChart.PlotArea.XAxis.AxisLabel.Appearance.Dimensions.Width = new Telerik.Reporting.Charting.Styles.Unit(1D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.regChart.PlotArea.XAxis.AxisLabel.Appearance.Visible = true;
			this.regChart.PlotArea.XAxis.AxisLabel.TextBlock.Appearance.Border.Color = System.Drawing.Color.Black;
			this.regChart.PlotArea.XAxis.AxisLabel.TextBlock.Appearance.Shadow.Color = System.Drawing.Color.Transparent;
			this.regChart.PlotArea.XAxis.AxisLabel.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.Black;
			this.regChart.PlotArea.XAxis.AxisLabel.TextBlock.Appearance.TextProperties.Font = new System.Drawing.Font("Arial", 8F);
			this.regChart.PlotArea.XAxis.AxisLabel.Visible = true;
			this.regChart.PlotArea.XAxis.LayoutMode = Telerik.Reporting.Charting.Styles.ChartAxisLayoutMode.Normal;
			this.regChart.PlotArea.XAxis.MinValue = 1D;
			this.regChart.PlotArea.YAxis.Appearance.LabelAppearance.Border.Color = System.Drawing.Color.Transparent;
			this.regChart.PlotArea.YAxis.Appearance.LabelAppearance.FillStyle.FillType = Telerik.Reporting.Charting.Styles.FillType.Solid;
			this.regChart.PlotArea.YAxis.Appearance.LabelAppearance.FillStyle.MainColor = System.Drawing.Color.White;
			this.regChart.PlotArea.YAxis.Appearance.LabelAppearance.FillStyle.SecondColor = System.Drawing.Color.Black;
			this.regChart.PlotArea.YAxis.Appearance.MajorGridLines.Color = System.Drawing.Color.Black;
			this.regChart.PlotArea.YAxis.Appearance.MajorGridLines.Visible = false;
			this.regChart.PlotArea.YAxis.Appearance.MajorTick.Color = System.Drawing.Color.Black;
			this.regChart.PlotArea.YAxis.Appearance.MinorGridLines.Color = System.Drawing.Color.Black;
			this.regChart.PlotArea.YAxis.Appearance.MinorGridLines.Visible = false;
			this.regChart.PlotArea.YAxis.Appearance.MinorTick.Color = System.Drawing.Color.Black;
			this.regChart.PlotArea.YAxis.Appearance.MinorTick.Visible = false;
			this.regChart.PlotArea.YAxis.Appearance.TextAppearance.Border.Color = System.Drawing.Color.White;
			chartPaddings16.Top = new Telerik.Reporting.Charting.Styles.Unit(0D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			this.regChart.PlotArea.YAxis.Appearance.TextAppearance.Dimensions.Paddings = chartPaddings16;
			this.regChart.PlotArea.YAxis.Appearance.TextAppearance.TextProperties.Color = System.Drawing.Color.Black;
			this.regChart.PlotArea.YAxis.Appearance.TextAppearance.TextProperties.Font = new System.Drawing.Font("Arial", 7F);
			this.regChart.PlotArea.YAxis.AutoScale = false;
			this.regChart.PlotArea.YAxis.AxisLabel.TextBlock.Appearance.TextProperties.Color = System.Drawing.Color.Black;
			this.regChart.PlotArea.YAxis.AxisLabel.TextBlock.Appearance.TextProperties.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.regChart.PlotArea.YAxis.LabelStep = 5;
			this.regChart.PlotArea.YAxis.LogarithmBase = 2D;
			this.regChart.PlotArea.YAxis.MaxItemsCount = 6;
			this.regChart.PlotArea.YAxis.MaxValue = 2.47588007857076E+27D;
			this.regChart.PlotArea.YAxis.MinValue = 2D;
			this.regChart.PlotArea.YAxis.Step = 5D;
			this.regChart.PlotArea.YAxis2.AutoScale = false;
			this.regChart.PlotArea.YAxis2.MaxValue = 6D;
			chartSeries7.Appearance.PointDimentions.AutoSize = false;
			chartSeries7.Appearance.PointDimentions.Height = new Telerik.Reporting.Charting.Styles.Unit(15D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartSeries7.Appearance.PointDimentions.Width = new Telerik.Reporting.Charting.Styles.Unit(15D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartSeries7.Name = "Series 1";
			chartSeries7.Type = Telerik.Reporting.Charting.ChartSeriesType.Point;
			chartSeries8.Appearance.FillStyle.MainColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(254)))), ((int)(((byte)(122)))));
			chartSeries8.Appearance.PointDimentions.AutoSize = false;
			chartSeries8.Appearance.PointDimentions.Height = new Telerik.Reporting.Charting.Styles.Unit(15D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartSeries8.Appearance.PointDimentions.Width = new Telerik.Reporting.Charting.Styles.Unit(15D, Telerik.Reporting.Charting.Styles.UnitType.Pixel);
			chartSeries8.Name = "Series 2";
			chartSeries8.Type = Telerik.Reporting.Charting.ChartSeriesType.Line;
			this.regChart.Series.AddRange(new Telerik.Reporting.Charting.ChartSeries[] {
            chartSeries7,
            chartSeries8});
			this.regChart.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1311435699462891D), Telerik.Reporting.Drawing.Unit.Inch(1.0725002288818359D));
			this.regChart.Style.BackgroundColor = System.Drawing.Color.Transparent;
			this.regChart.Style.Font.Name = "Times New Roman";
			this.regChart.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.regChart.Style.Visible = true;
			// 
			// textBox60
			// 
			this.textBox60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.3854119777679443D), Telerik.Reporting.Drawing.Unit.Inch(6.4034123420715332D));
			this.textBox60.Name = "textBox60";
			this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1313673257827759D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox60.Style.Font.Name = "Times New Roman";
			this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox60.Value = "Final Blood Volume:";
			// 
			// textBoxFinalBloodVolume
			// 
			this.textBoxFinalBloodVolume.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.5104119777679443D), Telerik.Reporting.Drawing.Unit.Inch(6.3929958343505859D));
			this.textBoxFinalBloodVolume.Name = "textBoxFinalBloodVolume";
			this.textBoxFinalBloodVolume.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80000066757202148D), Telerik.Reporting.Drawing.Unit.Inch(0.20617198944091797D));
			this.textBoxFinalBloodVolume.Style.Font.Name = "Times New Roman";
			this.textBoxFinalBloodVolume.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBoxFinalBloodVolume.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxFinalBloodVolume.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxFinalBloodVolume.Value = "";
			// 
			// textBoxSlope
			// 
			this.textBoxSlope.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.6979117393493652D), Telerik.Reporting.Drawing.Unit.Inch(6.4034123420715332D));
			this.textBoxSlope.Name = "textBoxSlope";
			this.textBoxSlope.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80000066757202148D), Telerik.Reporting.Drawing.Unit.Inch(0.20617198944091797D));
			this.textBoxSlope.Style.Font.Name = "Times New Roman";
			this.textBoxSlope.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBoxSlope.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxSlope.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxSlope.Value = "";
			// 
			// textBox62
			// 
			this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.281245231628418D), Telerik.Reporting.Drawing.Unit.Inch(6.4034123420715332D));
			this.textBox62.Name = "textBox62";
			this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.42303404211997986D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox62.Style.Font.Name = "Times New Roman";
			this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox62.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox62.Value = "Slope:";
			// 
			// BloodVolumeAnalysisSubReport
			// 
			this.BloodVolumeAnalysisSubReport.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.019999999552965164D), Telerik.Reporting.Drawing.Unit.Inch(2.7400000095367432D));
			this.BloodVolumeAnalysisSubReport.Name = "BloodVolumeAnalysisSubReport";
			this.BloodVolumeAnalysisSubReport.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9099998474121094D), Telerik.Reporting.Drawing.Unit.Inch(2.1700000762939453D));
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.72680997848510742D), Telerik.Reporting.Drawing.Unit.Inch(6.7347798347473145D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.1065182685852051D), Telerik.Reporting.Drawing.Unit.Inch(0.1268870085477829D));
			this.textBox1.Style.Font.Name = "Times New Roman";
			this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox1.Value = "< >= entry auto-excluded,        = see amputee guidance.";
			// 
			// amputeeColorExample
			// 
			this.amputeeColorExample.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2D), Telerik.Reporting.Drawing.Unit.Inch(6.7699999809265137D));
			this.amputeeColorExample.Name = "amputeeColorExample";
			this.amputeeColorExample.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.1458333283662796D), Telerik.Reporting.Drawing.Unit.Inch(0.0798611119389534D));
			this.amputeeColorExample.Style.BackgroundColor = System.Drawing.Color.LightGray;
			this.amputeeColorExample.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.amputeeColorExample.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
			this.amputeeColorExample.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.amputeeColorExample.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.amputeeColorExample.Value = "";
			// 
			// textBox2
			// 
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.0100002288818359D), Telerik.Reporting.Drawing.Unit.Inch(6.03000020980835D));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70000028610229492D), Telerik.Reporting.Drawing.Unit.Inch(0.20617198944091797D));
			this.textBox2.Style.Font.Name = "Times New Roman";
			this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox2.Value = "0.25 to 0.4";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3600001335144043D), Telerik.Reporting.Drawing.Unit.Inch(6.03000020980835D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53958255052566528D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox3.Style.Font.Name = "Times New Roman";
			this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox3.Value = "Elevated:";
			// 
			// SubReport_Analyst
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
			this.Name = "SubReport_Analyst";
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.9412498474121094D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        public Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        public Telerik.Reporting.TextBox textBoxStandardDeviation;
        private Telerik.Reporting.Shape shape9;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.Shape shape7;
        public Telerik.Reporting.TextBox textBoxTransudation;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.Shape shape10;
        private Telerik.Reporting.Shape shape8;
        private Telerik.Reporting.TextBox textBox41;
        public Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.Shape shape3;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        public Telerik.Reporting.TextBox textBoxTubeType;
        public Telerik.Reporting.TextBox textBoxBackgroundCount;
        public Telerik.Reporting.TextBox textBoxTestMode;
        private Telerik.Reporting.TextBox textBox38;
        public Telerik.Reporting.TextBox textBoxSampleAcquisitionTime;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox40;
        public Telerik.Reporting.TextBox textBoxDose;
        public Telerik.Reporting.Table tableSampleCounts;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox5;
        public Telerik.Reporting.Chart regChart;
        private Telerik.Reporting.Shape shape6;
        public Telerik.Reporting.TextBox textBoxWatermark;
        public Telerik.Reporting.TextBox textBoxLowStandardsWarning;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox60;
        public Telerik.Reporting.TextBox textBoxFinalBloodVolume;
        public Telerik.Reporting.TextBox textBoxSlope;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox72;
        public Telerik.Reporting.SubReport BloodVolumeAnalysisSubReport;
		private Telerik.Reporting.TextBox textBox1;
		internal Telerik.Reporting.TextBox amputeeColorExample;
		private Telerik.Reporting.TextBox textBox2;
		private Telerik.Reporting.TextBox textBox3;
    }
}