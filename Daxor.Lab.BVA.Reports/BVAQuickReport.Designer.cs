namespace Daxor.Lab.BVA.Reports
{
    partial class BVAQuickReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
			this.panelPatientDemographics = new Telerik.Reporting.Panel();
			this.textBox31 = new Telerik.Reporting.TextBox();
			this.textBoxAccession = new Telerik.Reporting.TextBox();
			this.textBoxTestID2 = new Telerik.Reporting.TextBox();
			this.textBoxDateAnalyzed = new Telerik.Reporting.TextBox();
			this.textBoxPhysicianName = new Telerik.Reporting.TextBox();
			this.textBox50 = new Telerik.Reporting.TextBox();
			this.textBoxInterpetingTech = new Telerik.Reporting.TextBox();
			this.textBoxVolumexLot = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBoxPatientName = new Telerik.Reporting.TextBox();
			this.textBoxPatientID = new Telerik.Reporting.TextBox();
			this.textBoxDOB = new Telerik.Reporting.TextBox();
			this.textBoxAmputeeStatus = new Telerik.Reporting.TextBox();
			this.textBox15 = new Telerik.Reporting.TextBox();
			this.textBox16 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.textBox24 = new Telerik.Reporting.TextBox();
			this.textBoxGender = new Telerik.Reporting.TextBox();
			this.textBoxHeight = new Telerik.Reporting.TextBox();
			this.textBoxWeight = new Telerik.Reporting.TextBox();
			this.textBoxDeviationFromIdealWt = new Telerik.Reporting.TextBox();
			this.shape4 = new Telerik.Reporting.Shape();
			this.textBox49 = new Telerik.Reporting.TextBox();
			this.textBoxReportName = new Telerik.Reporting.TextBox();
			this.shape2 = new Telerik.Reporting.Shape();
			this.detail = new Telerik.Reporting.DetailSection();
			this.HctBarChart = new Telerik.Reporting.Panel();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.textBox18 = new Telerik.Reporting.TextBox();
			this.shape5 = new Telerik.Reporting.Shape();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.shape6 = new Telerik.Reporting.Shape();
			this.shape7 = new Telerik.Reporting.Shape();
			this.textBoxHospitalAddress = new Telerik.Reporting.TextBox();
			this.textBoxHospitalPhone = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.shape3 = new Telerik.Reporting.Shape();
			this.objectDataSource2 = new Telerik.Reporting.ObjectDataSource();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBoxBVAVersion = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBoxUnitID = new Telerik.Reporting.TextBox();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBoxHospitalDepartmentDirector = new Telerik.Reporting.TextBox();
			this.textBoxHospitalDepartment = new Telerik.Reporting.TextBox();
			this.textBoxHospitalName = new Telerik.Reporting.TextBox();
			this.shape1 = new Telerik.Reporting.Shape();
			this.objectDataSource1 = new Telerik.Reporting.ObjectDataSource();
			this.entityDataSource1 = new Telerik.Reporting.EntityDataSource();
			this.objectDataSource3 = new Telerik.Reporting.ObjectDataSource();
			this.objectDataSource4 = new Telerik.Reporting.ObjectDataSource();
			this.objectDataSource5 = new Telerik.Reporting.ObjectDataSource();
			this.BloodVolumeAnalysisResults = new Telerik.Reporting.ObjectDataSource();
			this.graph1 = new Telerik.Reporting.Graph();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// pageHeaderSection1
			// 
			this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(3.5020442008972168D);
			this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panelPatientDemographics,
            this.textBoxReportName});
			this.pageHeaderSection1.Name = "pageHeaderSection1";
			// 
			// panelPatientDemographics
			// 
			this.panelPatientDemographics.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox31,
            this.textBoxAccession,
            this.textBoxTestID2,
            this.textBoxDateAnalyzed,
            this.textBoxPhysicianName,
            this.textBox50,
            this.textBoxInterpetingTech,
            this.textBoxVolumexLot,
            this.textBox6,
            this.textBox5,
            this.textBox14,
            this.textBox8,
            this.textBox9,
            this.textBoxPatientName,
            this.textBoxPatientID,
            this.textBoxDOB,
            this.textBoxAmputeeStatus,
            this.textBox15,
            this.textBox16,
            this.textBox23,
            this.textBox24,
            this.textBoxGender,
            this.textBoxHeight,
            this.textBoxWeight,
            this.textBoxDeviationFromIdealWt,
            this.shape4,
            this.textBox49});
			this.panelPatientDemographics.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.6791667938232422D));
			this.panelPatientDemographics.Name = "panelPatientDemographics";
			this.panelPatientDemographics.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9000000953674316D), Telerik.Reporting.Drawing.Unit.Inch(1.3522018194198608D));
			this.panelPatientDemographics.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.panelPatientDemographics.Style.BorderColor.Left = System.Drawing.Color.Black;
			this.panelPatientDemographics.Style.BorderColor.Right = System.Drawing.Color.Black;
			this.panelPatientDemographics.Style.Color = System.Drawing.Color.Black;
			this.panelPatientDemographics.Style.LineColor = System.Drawing.Color.Black;
			// 
			// textBox31
			// 
			this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.307995468378067D), Telerik.Reporting.Drawing.Unit.Inch(0.60106915235519409D));
			this.textBox31.Name = "textBox31";
			this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2499986886978149D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox31.Style.Font.Bold = true;
			this.textBox31.Style.Font.Name = "Arial";
			this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox31.Value = "Ordering Physician :";
			// 
			// textBoxAccession
			// 
			this.textBoxAccession.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1383953094482422D), Telerik.Reporting.Drawing.Unit.Inch(0.45099037885665894D));
			this.textBoxAccession.Name = "textBoxAccession";
			this.textBoxAccession.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3420006036758423D), Telerik.Reporting.Drawing.Unit.Inch(0.15287311375141144D));
			this.textBoxAccession.Style.Font.Name = "Arial";
			this.textBoxAccession.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxAccession.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxAccession.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxAccession.TextWrap = false;
			this.textBoxAccession.Value = "-";
			// 
			// textBoxTestID2
			// 
			this.textBoxTestID2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.00431489944458D), Telerik.Reporting.Drawing.Unit.Inch(0.45099037885665894D));
			this.textBoxTestID2.Name = "textBoxTestID2";
			this.textBoxTestID2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1339212656021118D), Telerik.Reporting.Drawing.Unit.Inch(0.15287311375141144D));
			this.textBoxTestID2.Style.Font.Bold = true;
			this.textBoxTestID2.Style.Font.Name = "Arial";
			this.textBoxTestID2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxTestID2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxTestID2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxTestID2.Value = "Accession  :";
			// 
			// textBoxDateAnalyzed
			// 
			this.textBoxDateAnalyzed.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1382365226745605D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBoxDateAnalyzed.Name = "textBoxDateAnalyzed";
			this.textBoxDateAnalyzed.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3420792818069458D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxDateAnalyzed.Style.Font.Name = "Arial";
			this.textBoxDateAnalyzed.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxDateAnalyzed.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxDateAnalyzed.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxDateAnalyzed.TextWrap = false;
			this.textBoxDateAnalyzed.Value = "-";
			// 
			// textBoxPhysicianName
			// 
			this.textBoxPhysicianName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.5581527948379517D), Telerik.Reporting.Drawing.Unit.Inch(0.60106915235519409D));
			this.textBoxPhysicianName.Name = "textBoxPhysicianName";
			this.textBoxPhysicianName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2288472652435303D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxPhysicianName.Style.Font.Name = "Arial";
			this.textBoxPhysicianName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxPhysicianName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxPhysicianName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPhysicianName.TextWrap = false;
			this.textBoxPhysicianName.Value = "-";
			// 
			// textBox50
			// 
			this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.00431489944458D), Telerik.Reporting.Drawing.Unit.Inch(0.15007877349853516D));
			this.textBox50.Name = "textBox50";
			this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1339213848114014D), Telerik.Reporting.Drawing.Unit.Inch(0.15362714231014252D));
			this.textBox50.Style.Font.Bold = true;
			this.textBox50.Style.Font.Name = "Arial";
			this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox50.Value = "Technologist :";
			// 
			// textBoxInterpetingTech
			// 
			this.textBoxInterpetingTech.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1383953094482422D), Telerik.Reporting.Drawing.Unit.Inch(0.15007877349853516D));
			this.textBoxInterpetingTech.Name = "textBoxInterpetingTech";
			this.textBoxInterpetingTech.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3420003652572632D), Telerik.Reporting.Drawing.Unit.Inch(0.15362714231014252D));
			this.textBoxInterpetingTech.Style.Font.Name = "Arial";
			this.textBoxInterpetingTech.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxInterpetingTech.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxInterpetingTech.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxInterpetingTech.TextWrap = false;
			this.textBoxInterpetingTech.Value = "-";
			// 
			// textBoxVolumexLot
			// 
			this.textBoxVolumexLot.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1382365226745605D), Telerik.Reporting.Drawing.Unit.Inch(0.30378469824790955D));
			this.textBoxVolumexLot.Name = "textBoxVolumexLot";
			this.textBoxVolumexLot.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3420003652572632D), Telerik.Reporting.Drawing.Unit.Inch(0.14712689816951752D));
			this.textBoxVolumexLot.Style.Font.Name = "Arial";
			this.textBoxVolumexLot.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxVolumexLot.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxVolumexLot.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxVolumexLot.TextWrap = false;
			this.textBoxVolumexLot.Value = "-";
			// 
			// textBox6
			// 
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.00431489944458D), Telerik.Reporting.Drawing.Unit.Inch(0.30378484725952148D));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1339213848114014D), Telerik.Reporting.Drawing.Unit.Inch(0.14712674915790558D));
			this.textBox6.Style.Font.Bold = true;
			this.textBox6.Style.Font.Name = "Arial";
			this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox6.Value = "Volumex Lot :";
			// 
			// textBox5
			// 
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.307995468378067D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2499986886978149D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox5.Style.Font.Bold = true;
			this.textBox5.Style.Font.Name = "Arial";
			this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox5.Value = "Patient Name :";
			// 
			// textBox14
			// 
			this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.307995468378067D), Telerik.Reporting.Drawing.Unit.Inch(0.30015754699707031D));
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2499985694885254D), Telerik.Reporting.Drawing.Unit.Inch(0.1507541686296463D));
			this.textBox14.Style.Font.Bold = true;
			this.textBox14.Style.Font.Name = "Arial";
			this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox14.Value = "ID :";
			// 
			// textBox8
			// 
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.307995468378067D), Telerik.Reporting.Drawing.Unit.Inch(0.15007877349853516D));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2499986886978149D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox8.Style.Font.Bold = true;
			this.textBox8.Style.Font.Name = "Arial";
			this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox8.Value = "DOB :";
			// 
			// textBox9
			// 
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.307995468378067D), Telerik.Reporting.Drawing.Unit.Inch(0.45099034905433655D));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2499986886978149D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox9.Style.Font.Bold = true;
			this.textBox9.Style.Font.Name = "Arial";
			this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox9.Value = "Amputee :";
			// 
			// textBoxPatientName
			// 
			this.textBoxPatientName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.5580730438232422D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBoxPatientName.Name = "textBoxPatientName";
			this.textBoxPatientName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2289267778396606D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxPatientName.Style.Font.Name = "Arial";
			this.textBoxPatientName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxPatientName.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxPatientName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPatientName.TextWrap = false;
			this.textBoxPatientName.Value = "-";
			// 
			// textBoxPatientID
			// 
			this.textBoxPatientID.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.55807363986969D), Telerik.Reporting.Drawing.Unit.Inch(0.30015739798545837D));
			this.textBoxPatientID.Name = "textBoxPatientID";
			this.textBoxPatientID.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2289261817932129D), Telerik.Reporting.Drawing.Unit.Inch(0.1507541686296463D));
			this.textBoxPatientID.Style.Font.Name = "Arial";
			this.textBoxPatientID.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxPatientID.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxPatientID.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxPatientID.TextWrap = false;
			this.textBoxPatientID.Value = "-";
			// 
			// textBoxDOB
			// 
			this.textBoxDOB.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.5580730438232422D), Telerik.Reporting.Drawing.Unit.Inch(0.15007877349853516D));
			this.textBoxDOB.Name = "textBoxDOB";
			this.textBoxDOB.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2289263010025024D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxDOB.Style.Font.Name = "Arial";
			this.textBoxDOB.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxDOB.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxDOB.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxDOB.TextWrap = false;
			this.textBoxDOB.Value = "-";
			// 
			// textBoxAmputeeStatus
			// 
			this.textBoxAmputeeStatus.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.5580743551254273D), Telerik.Reporting.Drawing.Unit.Inch(0.45099034905433655D));
			this.textBoxAmputeeStatus.Name = "textBoxAmputeeStatus";
			this.textBoxAmputeeStatus.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2289257049560547D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxAmputeeStatus.Style.Font.Name = "Arial";
			this.textBoxAmputeeStatus.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxAmputeeStatus.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxAmputeeStatus.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxAmputeeStatus.TextWrap = false;
			this.textBoxAmputeeStatus.Value = "-";
			// 
			// textBox15
			// 
			this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.7870781421661377D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox15.Style.Font.Bold = true;
			this.textBox15.Style.Font.Name = "Arial";
			this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox15.Value = "Gender :";
			// 
			// textBox16
			// 
			this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.7870781421661377D), Telerik.Reporting.Drawing.Unit.Inch(0.15007860958576202D));
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox16.Style.Font.Bold = true;
			this.textBox16.Style.Font.Name = "Arial";
			this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox16.Value = "Ht :";
			// 
			// textBox23
			// 
			this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.7870781421661377D), Telerik.Reporting.Drawing.Unit.Inch(0.30091175436973572D));
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox23.Style.Font.Bold = true;
			this.textBox23.Style.Font.Name = "Arial";
			this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox23.Style.Font.Strikeout = false;
			this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox23.Value = "Wt :";
			// 
			// textBox24
			// 
			this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.7870781421661377D), Telerik.Reporting.Drawing.Unit.Inch(0.45099052786827087D));
			this.textBox24.Name = "textBox24";
			this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox24.Style.Font.Bold = true;
			this.textBox24.Style.Font.Name = "Arial";
			this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox24.Value = "Dev From Ideal :";
			// 
			// textBoxGender
			// 
			this.textBoxGender.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7871570587158203D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBoxGender.Name = "textBoxGender";
			this.textBoxGender.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2170791625976563D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxGender.Style.Font.Name = "Arial";
			this.textBoxGender.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxGender.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxGender.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxGender.TextWrap = false;
			this.textBoxGender.Value = "-";
			// 
			// textBoxHeight
			// 
			this.textBoxHeight.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7871570587158203D), Telerik.Reporting.Drawing.Unit.Inch(0.15007860958576202D));
			this.textBoxHeight.Name = "textBoxHeight";
			this.textBoxHeight.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2170791625976563D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxHeight.Style.Font.Name = "Arial";
			this.textBoxHeight.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxHeight.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxHeight.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxHeight.TextWrap = false;
			this.textBoxHeight.Value = "-";
			// 
			// textBoxWeight
			// 
			this.textBoxWeight.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7871570587158203D), Telerik.Reporting.Drawing.Unit.Inch(0.30091175436973572D));
			this.textBoxWeight.Name = "textBoxWeight";
			this.textBoxWeight.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2170791625976563D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxWeight.Style.Font.Name = "Arial";
			this.textBoxWeight.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxWeight.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxWeight.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxWeight.TextWrap = false;
			this.textBoxWeight.Value = "-";
			// 
			// textBoxDeviationFromIdealWt
			// 
			this.textBoxDeviationFromIdealWt.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.7871570587158203D), Telerik.Reporting.Drawing.Unit.Inch(0.45099034905433655D));
			this.textBoxDeviationFromIdealWt.Name = "textBoxDeviationFromIdealWt";
			this.textBoxDeviationFromIdealWt.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2170791625976563D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxDeviationFromIdealWt.Style.Font.Name = "Arial";
			this.textBoxDeviationFromIdealWt.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxDeviationFromIdealWt.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxDeviationFromIdealWt.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxDeviationFromIdealWt.TextWrap = false;
			this.textBoxDeviationFromIdealWt.Value = "-";
			// 
			// shape4
			// 
			this.shape4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.307995468378067D), Telerik.Reporting.Drawing.Unit.Inch(1.3001184463500977D));
			this.shape4.Name = "shape4";
			this.shape4.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.1723198890686035D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
			// 
			// textBox49
			// 
			this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.00431489944458D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox49.Name = "textBox49";
			this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1339213848114014D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox49.Style.Font.Bold = true;
			this.textBox49.Style.Font.Name = "Arial";
			this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox49.Value = "Analysis :";
			// 
			// textBoxReportName
			// 
			this.textBoxReportName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.790157675743103D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
			this.textBoxReportName.Name = "textBoxReportName";
			this.textBoxReportName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.2097640037536621D), Telerik.Reporting.Drawing.Unit.Inch(0.7119210958480835D));
			this.textBoxReportName.Style.Font.Bold = true;
			this.textBoxReportName.Style.Font.Name = "Arial";
			this.textBoxReportName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
			this.textBoxReportName.Style.Font.Strikeout = false;
			this.textBoxReportName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxReportName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBoxReportName.Value = "Blood Volume Analysis Final Report";
			// 
			// shape2
			// 
			this.shape2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.307995468378067D), Telerik.Reporting.Drawing.Unit.Inch(0.30000019073486328D));
			this.shape2.Name = "shape2";
			this.shape2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.1723203659057617D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833283662796D));
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(0.89999991655349731D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.HctBarChart});
			this.detail.Name = "detail";
			this.detail.Style.Visible = true;
			// 
			// HctBarChart
			// 
			this.HctBarChart.Name = "HctBarChart";
			// 
			// textBox13
			// 
			this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.307995468378067D), Telerik.Reporting.Drawing.Unit.Inch(1.0084110498428345D));
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5020790100097656D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox13.Style.BackgroundColor = System.Drawing.Color.Transparent;
			this.textBox13.Style.BorderColor.Default = System.Drawing.Color.Transparent;
			this.textBox13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox13.Style.Color = System.Drawing.Color.Black;
			this.textBox13.Style.Font.Bold = true;
			this.textBox13.Style.Font.Italic = false;
			this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox13.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox13.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox13.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox13.Value = "Interpreting Physician :";
			// 
			// textBox18
			// 
			this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.307995468378067D), Telerik.Reporting.Drawing.Unit.Inch(1.4145822525024414D));
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.84116655588150024D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox18.Style.BackgroundColor = System.Drawing.Color.Transparent;
			this.textBox18.Style.BorderColor.Default = System.Drawing.Color.Transparent;
			this.textBox18.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox18.Style.Color = System.Drawing.Color.Black;
			this.textBox18.Style.Font.Bold = true;
			this.textBox18.Style.Font.Italic = false;
			this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox18.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox18.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox18.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox18.Value = "Comments :";
			// 
			// shape5
			// 
			this.shape5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.8101533651351929D), Telerik.Reporting.Drawing.Unit.Inch(1.106328010559082D));
			this.shape5.Name = "shape5";
			this.shape5.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.3280830383300781D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
			// 
			// textBox22
			// 
			this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1383152008056641D), Telerik.Reporting.Drawing.Unit.Inch(1.0084110498428345D));
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.45199999213218689D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBox22.Style.BackgroundColor = System.Drawing.Color.Transparent;
			this.textBox22.Style.BorderColor.Default = System.Drawing.Color.Transparent;
			this.textBox22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox22.Style.Color = System.Drawing.Color.Black;
			this.textBox22.Style.Font.Bold = true;
			this.textBox22.Style.Font.Italic = false;
			this.textBox22.Style.Font.Name = "Arial";
			this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox22.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox22.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox22.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox22.Value = "Date :";
			// 
			// shape6
			// 
			this.shape6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.61815881729126D), Telerik.Reporting.Drawing.Unit.Inch(1.106328010559082D));
			this.shape6.Name = "shape6";
			this.shape6.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.86215674877166748D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
			// 
			// shape7
			// 
			this.shape7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1492408514022827D), Telerik.Reporting.Drawing.Unit.Inch(1.512499213218689D));
			this.shape7.Name = "shape7";
			this.shape7.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.3310751914978027D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D));
			// 
			// textBoxHospitalAddress
			// 
			this.textBoxHospitalAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.307995468378067D), Telerik.Reporting.Drawing.Unit.Inch(2.1977570056915283D));
			this.textBoxHospitalAddress.Name = "textBoxHospitalAddress";
			this.textBoxHospitalAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2939999103546143D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxHospitalAddress.Style.Font.Bold = true;
			this.textBoxHospitalAddress.Style.Font.Italic = false;
			this.textBoxHospitalAddress.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxHospitalAddress.Value = "-";
			// 
			// textBoxHospitalPhone
			// 
			this.textBoxHospitalPhone.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.307995468378067D), Telerik.Reporting.Drawing.Unit.Inch(2.3478362560272217D));
			this.textBoxHospitalPhone.Name = "textBoxHospitalPhone";
			this.textBoxHospitalPhone.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9850000143051148D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxHospitalPhone.Style.Font.Bold = true;
			this.textBoxHospitalPhone.Style.Font.Italic = false;
			this.textBoxHospitalPhone.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxHospitalPhone.Value = "-";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.40000024437904358D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7619208097457886D), Telerik.Reporting.Drawing.Unit.Inch(0.30199998617172241D));
			this.textBox3.Style.BackgroundColor = System.Drawing.Color.Transparent;
			this.textBox3.Style.BorderColor.Default = System.Drawing.Color.Transparent;
			this.textBox3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox3.Style.Color = System.Drawing.Color.Black;
			this.textBox3.Style.Font.Bold = true;
			this.textBox3.Style.Font.Italic = true;
			this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(16D);
			this.textBox3.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBox3.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox3.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox3.Value = "Hct Analysis";
			// 
			// textBox7
			// 
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.5D));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox7.Style.Font.Bold = true;
			this.textBox7.Value = "Peripheral Hct:";
			// 
			// textBox10
			// 
			this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.7000000476837158D));
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
			this.textBox10.Style.Font.Bold = true;
			this.textBox10.Value = "Normalized Hct (nHct):";
			// 
			// textBox11
			// 
			this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(2.2000000476837158D));
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.3880000114440918D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
			this.textBox11.Style.Font.Bold = true;
			this.textBox11.Style.Font.Italic = true;
			this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox11.Value = "The normalized Hct is the peripheral Hct which would be observed if patient volum" +
    "e is corrected to ideal by adjusting the plasma volume.";
			// 
			// shape3
			// 
			this.shape3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(2.9000000953674316D));
			this.shape3.Name = "shape3";
			this.shape3.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.0620002746582031D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
			// 
			// objectDataSource2
			// 
			this.objectDataSource2.DataSource = typeof(Daxor.Lab.BVA.Core.BVAVolume);
			this.objectDataSource2.Name = "objectDataSource2";
			this.objectDataSource2.Parameters.AddRange(new Telerik.Reporting.ObjectDataSourceParameter[] {
            new Telerik.Reporting.ObjectDataSourceParameter("ruleEngine", typeof(Daxor.Lab.Domain.Interfaces.IRuleEngine), null)});
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(3.5937490463256836D);
			this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17,
            this.textBoxBVAVersion,
            this.textBox20,
            this.textBoxUnitID,
            this.textBox21,
            this.textBox13,
            this.shape5,
            this.textBox22,
            this.shape6,
            this.textBox18,
            this.shape7,
            this.textBoxHospitalAddress,
            this.textBoxHospitalPhone,
            this.textBoxHospitalDepartmentDirector,
            this.textBoxHospitalDepartment,
            this.textBoxHospitalName,
            this.shape2,
            this.shape1});
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// textBox17
			// 
			this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.307995468378067D), Telerik.Reporting.Drawing.Unit.Inch(2.8666656017303467D));
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64600002765655518D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
			this.textBox17.Style.Font.Name = "Times New Roman";
			this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox17.Value = "BVA-100 :";
			// 
			// textBoxBVAVersion
			// 
			this.textBoxBVAVersion.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.95407438278198242D), Telerik.Reporting.Drawing.Unit.Inch(2.8666656017303467D));
			this.textBoxBVAVersion.Name = "textBoxBVAVersion";
			this.textBoxBVAVersion.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.85600000619888306D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
			this.textBoxBVAVersion.Style.Font.Name = "Times New Roman";
			this.textBoxBVAVersion.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBoxBVAVersion.Value = "v1.2.0.0";
			// 
			// textBox20
			// 
			this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.1272499561309814D), Telerik.Reporting.Drawing.Unit.Inch(2.8676660060882568D));
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.54199999570846558D), Telerik.Reporting.Drawing.Unit.Inch(0.23899999260902405D));
			this.textBox20.Style.Font.Name = "Times New Roman";
			this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox20.Value = "Unit ID :  ";
			// 
			// textBoxUnitID
			// 
			this.textBoxUnitID.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.6693286895751953D), Telerik.Reporting.Drawing.Unit.Inch(2.8666658401489258D));
			this.textBoxUnitID.Name = "textBoxUnitID";
			this.textBoxUnitID.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6729165315628052D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
			this.textBoxUnitID.Style.Font.Name = "Times New Roman";
			this.textBoxUnitID.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBoxUnitID.Value = " -";
			// 
			// textBox21
			// 
			this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.4913158416748047D), Telerik.Reporting.Drawing.Unit.Inch(2.8656661510467529D));
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.98900002241134644D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
			this.textBox21.Style.Font.Name = "Times New Roman";
			this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox21.Value = "=\'Page \' + PageNumber + \' of \' + PageCount";
			// 
			// textBoxHospitalDepartmentDirector
			// 
			this.textBoxHospitalDepartmentDirector.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.6722354888916016D), Telerik.Reporting.Drawing.Unit.Inch(2.3478362560272217D));
			this.textBoxHospitalDepartmentDirector.Name = "textBoxHospitalDepartmentDirector";
			this.textBoxHospitalDepartmentDirector.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8081600666046143D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxHospitalDepartmentDirector.Style.Font.Bold = true;
			this.textBoxHospitalDepartmentDirector.Style.Font.Italic = false;
			this.textBoxHospitalDepartmentDirector.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxHospitalDepartmentDirector.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxHospitalDepartmentDirector.Value = "-";
			// 
			// textBoxHospitalDepartment
			// 
			this.textBoxHospitalDepartment.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.6722354888916016D), Telerik.Reporting.Drawing.Unit.Inch(2.1977570056915283D));
			this.textBoxHospitalDepartment.Name = "textBoxHospitalDepartment";
			this.textBoxHospitalDepartment.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8081594705581665D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxHospitalDepartment.Style.Font.Bold = true;
			this.textBoxHospitalDepartment.Style.Font.Italic = false;
			this.textBoxHospitalDepartment.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxHospitalDepartment.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBoxHospitalDepartment.Value = "-";
			// 
			// textBoxHospitalName
			// 
			this.textBoxHospitalName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.307995468378067D), Telerik.Reporting.Drawing.Unit.Inch(2.0476779937744141D));
			this.textBoxHospitalName.Name = "textBoxHospitalName";
			this.textBoxHospitalName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2939999103546143D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
			this.textBoxHospitalName.Style.Font.Bold = true;
			this.textBoxHospitalName.Style.Font.Italic = false;
			this.textBoxHospitalName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBoxHospitalName.Value = "-";
			// 
			// shape1
			// 
			this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.308074951171875D), Telerik.Reporting.Drawing.Unit.Inch(1.78125D));
			this.shape1.Name = "shape1";
			this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
			this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.1723203659057617D), Telerik.Reporting.Drawing.Unit.Inch(0.0520833283662796D));
			// 
			// objectDataSource1
			// 
			this.objectDataSource1.DataMember = "Patient";
			this.objectDataSource1.DataSource = typeof(Daxor.Lab.BVA.Core.BVATest);
			this.objectDataSource1.Name = "objectDataSource1";
			// 
			// entityDataSource1
			// 
			this.entityDataSource1.Name = "entityDataSource1";
			// 
			// objectDataSource3
			// 
			this.objectDataSource3.Name = "objectDataSource3";
			// 
			// objectDataSource4
			// 
			this.objectDataSource4.Name = "objectDataSource4";
			// 
			// objectDataSource5
			// 
			this.objectDataSource5.DataSource = typeof(Daxor.Lab.BVA.Reports.SubReport_BloodVolumeAnalysisQuickReport);
			this.objectDataSource5.Name = "objectDataSource5";
			// 
			// BloodVolumeAnalysisResults
			// 
			this.BloodVolumeAnalysisResults.DataMember = "Items";
			this.BloodVolumeAnalysisResults.DataSource = typeof(Daxor.Lab.BVA.Reports.SubReport_BloodVolumeAnalysisQuickReport);
			this.BloodVolumeAnalysisResults.Name = "BloodVolumeAnalysisResults";
			// 
			// graph1
			// 
			this.graph1.Legend.Style.LineColor = System.Drawing.Color.LightGray;
			this.graph1.Legend.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.graph1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7619997262954712D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.graph1.Name = "graph1";
			this.graph1.PlotAreaStyle.LineColor = System.Drawing.Color.LightGray;
			this.graph1.PlotAreaStyle.LineWidth = Telerik.Reporting.Drawing.Unit.Inch(0D);
			this.graph1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9000002145767212D), Telerik.Reporting.Drawing.Unit.Inch(1.2000001668930054D));
			// 
			// BVAQuickReport
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
			this.Name = "BVAReport";
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.37999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Style.BorderColor.Default = System.Drawing.Color.Transparent;
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.9000000953674316D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.TextBox textBox17;
        public Telerik.Reporting.TextBox textBoxBVAVersion;
        private Telerik.Reporting.TextBox textBox20;
        public Telerik.Reporting.TextBox textBoxUnitID;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.Panel panelPatientDemographics;
        private Telerik.Reporting.TextBox textBox31;
        public Telerik.Reporting.TextBox textBoxAccession;
        public Telerik.Reporting.TextBox textBoxDateAnalyzed;
		public Telerik.Reporting.TextBox textBoxPhysicianName;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        public Telerik.Reporting.TextBox textBoxAnalyst;
        public Telerik.Reporting.TextBox textBoxInterpetingTech;
        public Telerik.Reporting.TextBox textBoxVolumexLot;
		private Telerik.Reporting.TextBox textBox6;
        public Telerik.Reporting.TextBox textBoxTestID2;
        public Telerik.Reporting.DetailSection detail;
        public Telerik.Reporting.TextBox textBoxReportType;
        public Telerik.Reporting.PageHeaderSection pageHeaderSection1;
		public Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox5;
        public Telerik.Reporting.TextBox textBoxDOB;
        public Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        public Telerik.Reporting.TextBox textBoxPatientID;
        public Telerik.Reporting.TextBox textBoxPatientName;
        public Telerik.Reporting.TextBox textBoxAmputeeStatus;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        public Telerik.Reporting.TextBox textBoxGender;
        public Telerik.Reporting.TextBox textBoxHeight;
        public Telerik.Reporting.TextBox textBoxWeight;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
		public Telerik.Reporting.TextBox textBoxDeviationFromIdealWt;
        private Telerik.Reporting.Shape shape2;
        private Telerik.Reporting.Panel HctBarChart;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.Shape shape3;
        private Telerik.Reporting.TextBox textBox13;
		private Telerik.Reporting.TextBox textBox18;
        public Telerik.Reporting.Chart chartBloodBarsQR;
        private Telerik.Reporting.Shape shape5;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.Shape shape6;
		private Telerik.Reporting.Shape shape7;
        public Telerik.Reporting.TextBox textBoxHospitalPhone;
        public Telerik.Reporting.PictureBox pictureBoxIcon;
        public Telerik.Reporting.TextBox textBoxHospitalAddress;
        public Telerik.Reporting.TextBox textBoxReportStatus;
        public Telerik.Reporting.TextBox textBoxCCTo;
        public Telerik.Reporting.TextBox textBoxComments;
        public Telerik.Reporting.TextBox textBoxDeviationFromIdealWeight;
        public Telerik.Reporting.TextBox textBoxInjectateLot;
        public Telerik.Reporting.TextBox textBoxLocation;
        public Telerik.Reporting.TextBox textBoxPatientDOB;
        public Telerik.Reporting.TextBox textBoxPatientGender;
        public Telerik.Reporting.TextBox textBoxPatientHeight;
        public Telerik.Reporting.TextBox textBoxPatientWeight;
        public Telerik.Reporting.TextBox textBoxPatientHospitalID;
        public Telerik.Reporting.TextBox textBoxReferringMD;
		public Telerik.Reporting.TextBox textBoxLocationLabel;
        private Telerik.Reporting.ObjectDataSource objectDataSource2;
        private Telerik.Reporting.ObjectDataSource objectDataSource1;
        private Telerik.Reporting.EntityDataSource entityDataSource1;
        private Telerik.Reporting.ObjectDataSource objectDataSource3;
        private Telerik.Reporting.ObjectDataSource objectDataSource4;
        private Telerik.Reporting.ObjectDataSource objectDataSource5;
        private Telerik.Reporting.ObjectDataSource BloodVolumeAnalysisResults;
        private Telerik.Reporting.Graph graph1;
		public Telerik.Reporting.TextBox textBoxHospitalDepartmentDirector;
		public Telerik.Reporting.TextBox textBoxHospitalDepartment;
		public Telerik.Reporting.TextBox textBoxReportName;
		public Telerik.Reporting.TextBox textBoxHospitalName;
		private Telerik.Reporting.Shape shape4;
		private Telerik.Reporting.Shape shape1;
    }
}