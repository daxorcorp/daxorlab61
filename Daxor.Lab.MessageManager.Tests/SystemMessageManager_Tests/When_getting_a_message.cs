﻿using System.Collections.Generic;
using Daxor.Lab.MessageManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Daxor.Lab.MessageManager.Common;

namespace Daxor.Lab.MessageManager.Tests.SystemMessageManager_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_a_message
    {
        [TestMethod]
        public void And_the_key_for_a_message_exists_then_the_corresponding_message_is_returned()
        {
            // Arrange
            const string messageId = "SOME_ID";
            var expectedMessage = new Message(messageId, null, null, null, MessageType.Undefined);
            var messageDictionary = new Dictionary<string, Message>
                {
                    {messageId, expectedMessage}
                };

            var stubMessageRepository = Substitute.For<IMessageRepository>();
            stubMessageRepository.GetMessages().Returns(messageDictionary);

            var messageManager = new SystemMessageManager(stubMessageRepository);

            // Act
            var observedMessage = messageManager.GetMessage(messageId);

            // Assert
            Assert.AreSame(expectedMessage, observedMessage);
        }
    }
    // ReSharper restore InconsistentNaming
}
