﻿using System.Collections.Generic;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MessageManager.Tests.SystemMessageManager_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_a_message
    {
        [TestMethod]
        public void And_the_given_key_does_not_exist_then_an_exception_is_thrown()
        {
            var stubMessageRepo = Substitute.For<IMessageRepository>();
            stubMessageRepo.GetMessages().Returns(new Dictionary<string, Message>());

            var messagemManager = new SystemMessageManager(stubMessageRepo);

            AssertEx.Throws<KeyNotFoundException>(()=>messagemManager.GetMessage("whatever"));
        }
    }
    // ReSharper restore InconsistentNaming
}
