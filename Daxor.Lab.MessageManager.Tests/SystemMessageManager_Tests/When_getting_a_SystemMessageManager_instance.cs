﻿using Daxor.Lab.MessageManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MessageManager.Tests.SystemMessageManager_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_a_SystemMessageManager_instance
    {
        [TestMethod]
        public void Then_the_message_repository_is_asked_for_all_messages()
        {
            // Arrange
            var mockMessageRepository = Substitute.For<IMessageRepository>();

            // Act
            var unused = new SystemMessageManager(mockMessageRepository);

            // Assert
            mockMessageRepository.Received().GetMessages();
        }
    }
    // ReSharper restore InconsistentNaming
}
