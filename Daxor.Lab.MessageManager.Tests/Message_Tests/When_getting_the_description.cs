﻿using Daxor.Lab.MessageManager.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MessageManager.Tests.Message_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_description
    {
        [TestMethod]
        public void And_the_given_description_has_backslash_ns_then_they_are_replaced_with_newlines()
        {
            var message = new Message("whatever", @"abc\n123\nxyz", "", "", MessageType.Undefined);

            var observedTokens = message.Description.Split('\n');

            var expectedTokens = new[] { "abc", "123", "xyz" };
            CollectionAssert.AreEqual(expectedTokens, observedTokens);
        }

        [TestMethod]
        public void And_the_given_description_has_backslash_doublequotes_then_they_are_replaced_with_doublequotes()
        {
            var message = new Message("whatever", "\\\"Hello\\\"", "", "", MessageType.Undefined);

            Assert.AreEqual("\"Hello\"", message.Description);
        }
    }
    // ReSharper restore InconsistentNaming
}
