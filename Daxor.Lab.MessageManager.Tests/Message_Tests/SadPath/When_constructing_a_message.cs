﻿using System;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MessageManager.Tests.Message_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_a_message
    {
        [TestMethod]
        public void And_the_key_is_null_then_an_ArgumentNullException_is_thrown()
        {
            // ReSharper disable ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(()=>new Message(null, null, null, null, MessageType.Undefined), "key");            
            // ReSharper restore ObjectCreationAsStatement
        }

        [TestMethod]
        public void And_the_key_is_the_empty_string_then_an_ArgumentNullException_is_thrown()
        {
            // ReSharper disable ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(()=>new Message(String.Empty, null, null, null, MessageType.Undefined), "key");
            // ReSharper restore ObjectCreationAsStatement
        }
    }
    // ReSharper restore InconsistentNaming
}
