﻿using System;
using Daxor.Lab.MessageManager.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MessageManager.Tests.Message_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_formatted_message
    {
        [TestMethod]
        public void And_the_message_type_is_undefined_then_the_message_is_formatted_correctly()
        {
            var message = new Message(When_constructing_a_message.MessageKey,
                                      When_constructing_a_message.MessageDescription,
                                      When_constructing_a_message.MessageResolution,
                                      When_constructing_a_message.MessageId, 
                                      MessageType.Undefined);

            var expectedFormattedMessage = String.Format("{0}\n\n{1}\n[{2}]",
                                                             When_constructing_a_message.MessageDescription,
                                                             When_constructing_a_message.MessageResolution,
                                                             When_constructing_a_message.MessageId);
            Assert.AreEqual(expectedFormattedMessage, message.FormattedMessage);
        }

        [TestMethod]
        public void And_the_message_type_is_an_alert_then_the_message_is_formatted_correctly()
        {
            var message = new Message(When_constructing_a_message.MessageKey,
                          When_constructing_a_message.MessageDescription,
                          When_constructing_a_message.MessageResolution,
                          When_constructing_a_message.MessageId,
                          MessageType.Alert);

            var expectedFormattedMessage = String.Format("{0}\n\n{1}\n[Alert: {2}]",
                                                             When_constructing_a_message.MessageDescription,
                                                             When_constructing_a_message.MessageResolution,
                                                             When_constructing_a_message.MessageId);
            Assert.AreEqual(expectedFormattedMessage, message.FormattedMessage);
        }

        [TestMethod]
        public void And_the_message_type_is_an_error_then_the_message_is_formatted_correctly()
        {
            var message = new Message(When_constructing_a_message.MessageKey,
                          When_constructing_a_message.MessageDescription,
                          When_constructing_a_message.MessageResolution,
                          When_constructing_a_message.MessageId,
                          MessageType.Error);

            var expectedFormattedMessage = String.Format("{0}\n\n{1}\n[Error: {2}]",
                                                             When_constructing_a_message.MessageDescription,
                                                             When_constructing_a_message.MessageResolution,
                                                             When_constructing_a_message.MessageId);
            Assert.AreEqual(expectedFormattedMessage, message.FormattedMessage);
        }

        [TestMethod]
        public void And_the_message_is_null_then_the_message_is_formatted_correctly()
        {
            var message = new Message(When_constructing_a_message.MessageKey,
                          When_constructing_a_message.MessageDescription,
                          null,
                          When_constructing_a_message.MessageId,
                          MessageType.Undefined);

            var expectedFormattedMessage = String.Format("{0}\n\n\n[{1}]",
                                                             When_constructing_a_message.MessageDescription,
                                                             When_constructing_a_message.MessageId);
            Assert.AreEqual(expectedFormattedMessage, message.FormattedMessage);
        }

        [TestMethod]
        public void And_the_message_is_empty_then_the_message_is_formatted_correctly()
        {
            var message = new Message(When_constructing_a_message.MessageKey,
              When_constructing_a_message.MessageDescription,
              String.Empty,
              When_constructing_a_message.MessageId,
              MessageType.Undefined);

            var expectedFormattedMessage = String.Format("{0}\n\n\n[{1}]",
                                                             When_constructing_a_message.MessageDescription,
                                                             When_constructing_a_message.MessageId);
            Assert.AreEqual(expectedFormattedMessage, message.FormattedMessage);
        }

        [TestMethod]
        public void And_the_message_is_whitespace_then_the_message_is_formatted_correctly()
        {
            var message = new Message(When_constructing_a_message.MessageKey,
              When_constructing_a_message.MessageDescription,
              "  ",
              When_constructing_a_message.MessageId,
              MessageType.Undefined);

            var expectedFormattedMessage = String.Format("{0}\n\n\n[{1}]",
                                                             When_constructing_a_message.MessageDescription,
                                                             When_constructing_a_message.MessageId);
            Assert.AreEqual(expectedFormattedMessage, message.FormattedMessage);
        }
    }
    // ReSharper restore InconsistentNaming
}
