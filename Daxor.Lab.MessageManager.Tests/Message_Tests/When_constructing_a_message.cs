﻿using Daxor.Lab.MessageManager.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MessageManager.Tests.Message_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_a_message
    {
        public const string MessageKey = "TheKey";
        public const string MessageDescription = "TheDescription";
        public const string MessageResolution = "TheResolution";
        public const string MessageId = "TheId";

        private static Message _message;
        private const MessageType MessageTypeState = MessageType.Undefined;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _message = new Message(MessageKey, MessageDescription, MessageResolution, MessageId, MessageTypeState);
        }

        [TestMethod]
        public void Then_the_key_is_set_correctly()
        {
            Assert.AreEqual(MessageKey, _message.Key);
        }

        [TestMethod]
        public void Then_the_description_is_set_correctly()
        {
            Assert.AreEqual(MessageDescription, _message.Description);
        }

        [TestMethod]
        public void Then_the_resolution_is_set_correctly()
        {
            Assert.AreEqual(MessageResolution, _message.Resolution);
        }

        [TestMethod]
        public void Then_the_message_id_is_set_correctly()
        {
            Assert.AreEqual(MessageId, _message.MessageId);
        }

        [TestMethod]
        public void Then_the_message_type_is_set_correctly()
        {
            Assert.AreEqual(MessageTypeState, _message.MessageType);
        }
    }
    // ReSharper restore InconsistentNaming
}
