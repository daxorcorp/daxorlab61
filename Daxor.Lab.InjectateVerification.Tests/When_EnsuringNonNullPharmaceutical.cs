﻿using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class When_EnsuringNonNullPharmaceutical
    {
        [TestMethod]
        public void And_the_Pharmaceutical_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>PharmaceuticalKeyCodecHelper.EnsureNonNullPharmaceutical(null), "pharmaceutical");
        }
    }
}
