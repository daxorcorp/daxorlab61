﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Daxor.Lab.Infrastructure.DomainModels;
using System.Collections.Generic;

namespace Daxor.Lab.InjectateVerification.Tests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class When_converting_value_to_pharmaceutical
    {
        [TestMethod]
        public void And_the_value_is_valid_then_the_correct_pharmaceutical_is_returned()
        {
            var myBytes = new List<byte>();
            for (var i = 0; i < 53; i++)
                myBytes.Add((byte)i);
            var configuration = new PharmaceuticalKeyCodecConfiguration(new MD5ChecksumGenerator(), new Alphanumeric56KeyRepresentation(),
                                                                                                        new PharmaceuticalKeyCodecBitMapping(myBytes), new PharmaceuticalKeyConfiguration(DateTime.Now, 0, 10));
            var target = new PharmaceuticalKeyCodecHelper(configuration);
            var expected = new Pharmaceutical(DateTime.Now, 10, 5, Pharmaceutical.PharmaceuticalManufacturer.Daxor,
                                                                Pharmaceutical.PharmaceuticalType.Injectate, Pharmaceutical.PharmaceuticalBrand.Volumex);
            var fullyEncodedValue = target.ConvertPharmaceuticalToValue(expected);
            var actual = target.ConvertValueToPharmaceutical(fullyEncodedValue);

            Assert.IsTrue(expected.BatchId == actual.BatchId
                            && expected.Brand == actual.Brand
                            && expected.DateOfManufacture.ToShortDateString() == actual.DateOfManufacture.ToShortDateString()
                            && expected.Manufacturer == actual.Manufacturer
                            && expected.SerializationId == actual.SerializationId
                            && expected.Type == actual.Type);
        }
    }
}
