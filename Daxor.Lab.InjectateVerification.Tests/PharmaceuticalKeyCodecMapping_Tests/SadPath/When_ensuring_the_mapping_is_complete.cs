﻿using System;
using System.Collections.Generic;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyCodecMapping_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_ensuring_the_mapping_is_complete
    {
        private List<byte> _mappingOrder;
        private PharmaceuticalKeyCodecBitMapping _bitMapping;

        // Code that should be executed before running each test in this class.
        [TestInitialize]
        public void TestInitialize()
        {
            _mappingOrder = new List<byte> {1, 2};
            _bitMapping = new PharmaceuticalKeyCodecBitMapping(_mappingOrder);
        }
        
        [TestMethod]
        public void And_there_are_not_enough_bits_in_the_mapping_then_an_exception_is_thrown()
        {
            var numberOfBitsInKey = (uint)_mappingOrder.Count + 1;

            AssertEx.Throws<InvalidOperationException>(()=>_bitMapping.EnsureMappingIsComplete(numberOfBitsInKey));
        }

        [TestMethod]
        public void And_there_are_too_many_bits_in_the_mapping_then_an_exception_is_thrown()
        {
            var numberOfBitsInKey = (uint)_mappingOrder.Count - 1;

            AssertEx.Throws<InvalidOperationException>(()=>_bitMapping.EnsureMappingIsComplete(numberOfBitsInKey));
        }

        // Not all bits are there
        [TestMethod]
        public void And_not_all_indices_are_in_the_bit_mapping_then_an_exception_is_thrown()
        {
            _mappingOrder = new List<byte> { 1, 1 };
            _bitMapping = new PharmaceuticalKeyCodecBitMapping(_mappingOrder);
            AssertEx.Throws<InvalidOperationException>(()=>_bitMapping.EnsureMappingIsComplete((uint)_mappingOrder.Count));
        }
}
    // ReSharper restore InconsistentNaming
}
