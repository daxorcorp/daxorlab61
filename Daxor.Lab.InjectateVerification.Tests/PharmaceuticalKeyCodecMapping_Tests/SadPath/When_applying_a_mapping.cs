﻿using System;
using System.Collections.Generic;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyCodecMapping_Tests.SadPath
{
    [TestClass]
    // ReSharper disable InconsistentNaming
    public class When_applying_a_mapping
    {
        [TestMethod]
        public void When_the_mapping_is_not_complete_then_an_exception_is_thrown()
        {
            var emptyMapping = new List<byte>();
            var mapping = new PharmaceuticalKeyCodecBitMapping(emptyMapping);
            AssertEx.Throws<InvalidOperationException>(()=>mapping.ApplyMapping(1234, (uint) emptyMapping.Count + 1));
        }
    }
    // ReSharper restore InconsistentNaming
}
