﻿using System;
using System.Collections.Generic;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyCodecMapping_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_removing_a_bit_mapping
    {
        [TestMethod]
        public void When_the_mapping_is_not_complete_then_an_exception_is_thrown()
        {
            var emptyMapping = new List<byte>();
            var mapping = new PharmaceuticalKeyCodecBitMapping(emptyMapping);
            AssertEx.Throws<InvalidOperationException>(()=>mapping.RemoveMapping(1234, (uint)emptyMapping.Count + 1));
        }
    }
    // ReSharper restore InconsistentNaming
}
