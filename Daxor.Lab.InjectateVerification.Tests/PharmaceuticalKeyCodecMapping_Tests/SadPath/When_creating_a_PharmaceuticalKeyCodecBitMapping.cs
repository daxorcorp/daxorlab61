﻿using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyCodecMapping_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_a_PharmaceuticalKeyCodecBitMapping
    {
        [TestMethod]
        public void And_the_bit_mapping_passed_to_the_constructor_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.Throws<InvalidOperationException>(
                () => { var ignored = new PharmaceuticalKeyCodecBitMapping(null); });
            // ReSharper restore UnusedVariable
        }
    }
    // ReSharper restore InconsistentNaming
}
