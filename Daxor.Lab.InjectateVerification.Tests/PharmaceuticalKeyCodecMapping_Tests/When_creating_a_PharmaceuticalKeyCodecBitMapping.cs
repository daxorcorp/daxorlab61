﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyCodecMapping_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_a_PharmaceuticalKeyCodecBitMapping
    {
        [TestMethod]
        public void And_no_bit_mapping_is_provided_then_the_BitMapping_property_is_set_to_an_empty_list()
        {
            // ReSharper disable PossibleNullReferenceException
            var bitMapping = new PharmaceuticalKeyCodecBitMapping();
            var bitMappingIsNotNull = bitMapping.BitMapping != null;
            var bitMappingIsEmpty = bitMapping.BitMapping.Count == 0;
            Assert.IsTrue(bitMappingIsNotNull && bitMappingIsEmpty);
            // ReSharper restore PossibleNullReferenceException
        }
        
        [TestMethod]
        public void And_a_bit_mapping_is_passed_to_the_constructor_then_the_BitMapping_is_that_bit_mapping()
        {
            var byteList = new List<byte> {new byte()};
            var bitMapping = new PharmaceuticalKeyCodecBitMapping(byteList);
            
            Assert.AreSame(byteList, bitMapping.BitMapping);
        }
    }
    // ReSharper restore InconsistentNaming
}