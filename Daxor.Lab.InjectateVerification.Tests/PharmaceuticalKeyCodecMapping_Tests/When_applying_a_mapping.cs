﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyCodecMapping_Tests
{
    [TestClass]
    // ReSharper disable InconsistentNaming
    public class When_applying_a_mapping
    {
        [TestMethod]
        public void And_the_mapping_is_reversed_then_applying_the_mapping_works_correctly()
        {
            var mapping = InjectateVerificationTestHelpers.GenerateIdentityMappingOfLength(InjectateVerificationTestHelpers.NumberOfBitsInKey);
            mapping.Reverse();
            var pharmaceuticalKeyCodecBitMapping = new PharmaceuticalKeyCodecBitMapping(mapping);

            const ulong valueToMap = 77; 
            const ulong expectedValue = 178;
            var observedValue = pharmaceuticalKeyCodecBitMapping.ApplyMapping(valueToMap, InjectateVerificationTestHelpers.NumberOfBitsInKey);

            Assert.AreEqual(expectedValue, observedValue);
        }

        [TestMethod]
        public void And_the_bit_mapping_is_the_identity_then_the_same_value_is_returned()
        {
            var mapping = InjectateVerificationTestHelpers.GenerateIdentityMappingOfLength(InjectateVerificationTestHelpers.NumberOfBitsInKey);
            var pharmaceuticalKeyCodecBitMapping = new PharmaceuticalKeyCodecBitMapping(mapping);

            const ulong valueToMap = 77;
            var observedValue = pharmaceuticalKeyCodecBitMapping.ApplyMapping(valueToMap, InjectateVerificationTestHelpers.NumberOfBitsInKey);

            Assert.AreEqual(valueToMap, observedValue);
        }
    }
    // ReSharper restore InconsistentNaming
}
