﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyCodecMapping_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_ensuring_the_mapping_is_complete
    {
        [TestMethod]
        public void And_the_bit_mapping_contains_all_indices_then_no_exeption_is_thrown()
        {
            var mappingOrder = new List<byte>{4,3,2,1,0};
            var bitMapping = new PharmaceuticalKeyCodecBitMapping(mappingOrder);
            bitMapping.EnsureMappingIsComplete((uint)mappingOrder.Count);
        }
    }
    // ReSharper restore InconsistentNaming
}
