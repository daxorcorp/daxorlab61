﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.InjectateVerification.Tests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class When_decoding_the_pharmaceutical_key
    {
        [TestMethod]
        public void And_the_pharmaceutical_key_is_valid_then_the_proper_pharmaceutical_is_returned()
        {
            var intArray = new[]{29, 11, 57, 21, 26, 18, 42, 50, 41, 1, 17, 48, 56, 32, 33, 5, 27, 3, 4, 23, 43, 22, 55, 14, 46, 6, 52, 31, 24, 44, 49, 28, 2, 
                                        54, 51, 30, 38, 16, 37, 47, 15, 9, 39, 13, 19, 34, 0, 45, 25, 35, 36, 20, 40, 53, 7, 10, 12, 8};

            var myBytes = intArray.Select(number => (byte) number).ToList();

            var configuration = new PharmaceuticalKeyCodecConfiguration(new MD5ChecksumGenerator(),
                                                                                                        new Alphanumeric56KeyRepresentation(),
                                                                                                        new PharmaceuticalKeyCodecBitMapping(myBytes),
                                                                                                        new PharmaceuticalKeyConfiguration(new DateTime(2010,1,1),1,15)); 
            var target = new PharmaceuticalKeyService(configuration); 
            const string pharmaceuticalKey = "DATc3GqNGk"; 
            var expected = new Pharmaceutical(new DateTime(2011, 06, 10), 499, 3, Pharmaceutical.PharmaceuticalManufacturer.IsoTex,
                                                            Pharmaceutical.PharmaceuticalType.Injectate, Pharmaceutical.PharmaceuticalBrand.Volumex);
            var actual = target.Decode(pharmaceuticalKey);

            Assert.IsTrue(expected.BatchId == actual.BatchId
                            && expected.Brand == actual.Brand
                            && expected.DateOfManufacture.ToShortDateString() == actual.DateOfManufacture.ToShortDateString()
                            && expected.Manufacturer == actual.Manufacturer
                            && expected.SerializationId == actual.SerializationId
                            && expected.Type == actual.Type);
        }
    }
}
