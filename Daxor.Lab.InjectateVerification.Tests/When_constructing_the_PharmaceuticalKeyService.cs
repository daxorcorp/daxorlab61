﻿using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class When_constructing_the_PharmaceuticalKeyService
    {
        [TestMethod]
        public void And_the_key_codec_configuration_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>new PharmaceuticalKeyService(null), "configuration");
        }
    }
}
