﻿using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyCodecHelper_Tests.SadPath
{
    [TestClass]
    // ReSharper disable InconsistentNaming
    public class When_ensuring_that_the_key_codec_configuration_is_not_null
    {
        [TestMethod]
        public void And_the_pharmaceuticalKeyCodecConfiguation_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>PharmaceuticalKeyCodecHelper.EnsureNonNullKeyCodecConfiguration(null), "configuration");
        }
    }
    // ReSharper restore InconsistentNaming
}
