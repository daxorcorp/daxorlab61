﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyConfiguration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_number_of_defined_enumerations
    {
        private enum EmptyEnum
        {
        }

        private enum OnlyUndefinedEnum
        {
        // ReSharper disable UnusedMember.Local
            Undefined
        // ReSharper restore UnusedMember.Local
        }

        [TestMethod]
        public void And_computing_the_number_of_defined_enumerations_and_there_are_no_defined_enumerations_then_zero_is_returned()
        {
            Assert.AreEqual(0, PharmaceuticalKeyConfiguration.GetNumberOfDefinedEnumerations(typeof(EmptyEnum)));
        }

        [TestMethod]
        public void And_computing_the_number_of_defined_enumerations_and_there_is_only_an_undefined_member_then_zero_is_returned()
        {
            Assert.AreEqual(0, PharmaceuticalKeyConfiguration.GetNumberOfDefinedEnumerations(typeof(OnlyUndefinedEnum)));
        }
    }
    // ReSharper restore InconsistentNaming
}