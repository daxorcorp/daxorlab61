﻿using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyConfiguration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_the_number_of_supported_entities
    {
        [TestMethod]
        [RequirementValue(2)]
        public void Then_the_number_of_brands_supported_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), PharmaceuticalKeyConfiguration.GetNumberOfBrandsSupported());
        }

        [TestMethod]
        [RequirementValue(2)]
        public void Then_the_number_of_manufacturers_supported_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), PharmaceuticalKeyConfiguration.GetNumberOfManufacturersSupported());
        }

        [TestMethod]
        [RequirementValue(2)]
        public void Then_the_number_of_pharmaceutical_types_supported_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), PharmaceuticalKeyConfiguration.GetNumberOfTypesSupported());
        }
    }
    // ReSharper restore InconsistentNaming
}