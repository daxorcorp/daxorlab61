﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyConfiguration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_number_of_checksum_bits
    {
        [TestMethod]
        public void And_the_value_is_within_range_then_the_number_of_checksum_bits_is_correct()
        {
            var ignoredStartingDateOfManufacture = new DateTime();
            const uint ignoredStartingBatchId = 100;
            const uint numChecksumBits = (uint)0;

            const uint expectedNumberOfChecksumBits = numChecksumBits + 1;
            var pharmaceuticalKeyConfiguration = new PharmaceuticalKeyConfiguration(ignoredStartingDateOfManufacture, ignoredStartingBatchId, numChecksumBits);
            pharmaceuticalKeyConfiguration.NumberOfChecksumBits = expectedNumberOfChecksumBits;

            Assert.AreEqual(expectedNumberOfChecksumBits,pharmaceuticalKeyConfiguration.NumberOfChecksumBits);
        }
    }
    // ReSharper restore InconsistentNaming
}
