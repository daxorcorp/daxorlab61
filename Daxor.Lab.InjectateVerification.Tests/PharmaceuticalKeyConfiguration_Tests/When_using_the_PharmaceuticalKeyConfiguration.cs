﻿using System;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyConfiguration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_PharmaceuticalKeyConfiguration
    {
        [TestMethod]
        public void And_computing_the_maximum_serialization_id_supported_then_the_result_is_zero_based()
        {
            const uint expectedResult = PharmaceuticalKeyConfiguration.SerializationIDsSupported - 1;
            var observedResult = (uint) PharmaceuticalKeyConfiguration.GetMaxSerializationIdSupported();
            
            Assert.AreEqual(expectedResult, observedResult);   
        }

        [TestMethod]
        public void Then_the_ending_date_of_manufacture_is_8_years_after_the_starting_date()
        {
            var today = DateTime.Today;
            const int eightYears = 366*8;
            const int startingBatchId = 0;
            const int numChecksumBits = 0;
            var pharmaceuticalKeyConfiguration = new PharmaceuticalKeyConfiguration(today, startingBatchId, numChecksumBits);

            var expectedEndingDateOfManufacture = today.AddDays(eightYears);
            var observedEndingDateOfManufacture = pharmaceuticalKeyConfiguration.GetEndingDateOfManufacture();
            Assert.AreEqual(expectedEndingDateOfManufacture,observedEndingDateOfManufacture);
        }

        [TestMethod]
        public void Then_the_max_batch_id_supported_is_based_on_the_starting_batch_id_and_the_number_of_batch_ids_supported()
        {
            const uint startingBatchId = 234;
            const uint numChecksumBits = 0;
            const uint numberOfBatchIdsSupported = PharmaceuticalKeyConfiguration.BatchIDsSupported;
            const uint expectedMaxBatchId = startingBatchId + numberOfBatchIdsSupported - 1;
            var keyConfig = new PharmaceuticalKeyConfiguration(DateTime.Now, startingBatchId, numChecksumBits);

            var observedMaxBatchId = keyConfig.GetMaxBatchIdSupported();

            Assert.AreEqual(expectedMaxBatchId, observedMaxBatchId);
        }

        [TestMethod]
        public void And_using_to_string_then_the_proper_string_is_printed()
        {
            const uint startingBatchId = 234;
            const uint numChecksumBits = 0;
            var today = DateTime.Today;
            var expectedString = String.Format(PharmaceuticalKeyConfiguration.FormattedToString,
                                 startingBatchId, today.ToString(CultureInfo.InvariantCulture),
                                 numChecksumBits);

            var keyConfig = new PharmaceuticalKeyConfiguration(today, startingBatchId, numChecksumBits); 

            Assert.AreEqual(expectedString,keyConfig.ToString());
        }
    }
    // ReSharper restore InconsistentNaming
}
