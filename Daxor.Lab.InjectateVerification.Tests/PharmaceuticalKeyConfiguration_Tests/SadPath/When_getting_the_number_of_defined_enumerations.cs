using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyConfiguration_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_number_of_defined_enumerations
    {
        [TestMethod]
        public void And_computing_the_number_of_defined_enumerations_and_the_enum_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>PharmaceuticalKeyConfiguration.GetNumberOfDefinedEnumerations(null), "enumeration");
        }

        [TestMethod]
        public void And_computing_the_number_of_defined_enumerations_and_the_type_passed_is_not_enum_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentException>(()=>PharmaceuticalKeyConfiguration.GetNumberOfDefinedEnumerations(typeof(ArgumentException)));
        }
    }
    // ReSharper restore InconsistentNaming
}
