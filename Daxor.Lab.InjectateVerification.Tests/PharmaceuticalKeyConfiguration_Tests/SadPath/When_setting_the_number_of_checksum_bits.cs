﻿using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyConfiguration_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_number_of_checksum_bits
    {
        [TestMethod]
        public void And_the_number_of_checksum_bits_is_greater_than_allowed_then_an_exception_is_thrown()
        {
            var ignoredStartingDateOfManufacture = new DateTime(); 
            const uint ignoredStartingBatchId = 100;
            var numChecksumBits = PharmaceuticalKeyConfiguration.BitsInContainer - PharmaceuticalKeyConfiguration.GetMinimumNumberOfBitsRequired();

            var pharmaceuticalKeyConfiguration = new PharmaceuticalKeyConfiguration(ignoredStartingDateOfManufacture, ignoredStartingBatchId, numChecksumBits);
            AssertEx.Throws<InvalidOperationException>(()=>pharmaceuticalKeyConfiguration.NumberOfChecksumBits += 1);
        }
    }
    // ReSharper restore InconsistentNaming
}
