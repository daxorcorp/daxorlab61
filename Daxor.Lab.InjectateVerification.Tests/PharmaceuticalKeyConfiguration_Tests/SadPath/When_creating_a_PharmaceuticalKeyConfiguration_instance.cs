﻿using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyConfiguration_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_a_PharmaceuticalKeyConfiguration_instance
    {
        [TestMethod]
        public void And_the_number_of_checksum_bits_is_too_large_then_an_exception_is_thrown()
        {
            var ignoredStartingDateOfManufacture = new DateTime();
            const uint ignoredStartingBatchId = 100;
            var numChecksumBits = PharmaceuticalKeyConfiguration.BitsInContainer - PharmaceuticalKeyConfiguration.GetMinimumNumberOfBitsRequired() + 1;
            // ReSharper disable ObjectCreationAsStatement
            AssertEx.Throws<InvalidOperationException>(()=>new PharmaceuticalKeyConfiguration(ignoredStartingDateOfManufacture, ignoredStartingBatchId, numChecksumBits));
            // ReSharper restore ObjectCreationAsStatement
        }
    }
    // ReSharper restore InconsistentNaming
}
