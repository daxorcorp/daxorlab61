using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyConfiguration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_number_of_bits_for_a_component
    {
        const uint StartingBatchId = 123;
        const uint NumChecksumBits = 10;
        private static PharmaceuticalKeyConfiguration _keyConfiguration;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _keyConfiguration = new PharmaceuticalKeyConfiguration(DateTime.Now,
                StartingBatchId, NumChecksumBits);
        }

        [TestMethod]
        public void And_the_component_is_the_entire_key_then_the_number_is_the_sum_of_all_components_plus_the_checksum()
        {
            var bitsNeededForComponents = PharmaceuticalKeyConfiguration.GetMinimumNumberOfBitsRequired();

            var expectedNumberOfBitsForKey = bitsNeededForComponents + NumChecksumBits;

            Assert.AreEqual(expectedNumberOfBitsForKey, _keyConfiguration.GetNumberOfBitsUsedForKey());
        }

        [TestMethod]
        public void And_the_component_is_the_batch_id_then_the_number_is_based_on_the_number_of_batch_ids_supported()
        {
            var expectedNumberOfBitsForBatchId =
                PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent(
                    PharmaceuticalKeyConfiguration.BatchIDsSupported);

            Assert.AreEqual(expectedNumberOfBitsForBatchId, _keyConfiguration.GetNumberOfBitsForBatchId());
        }

        [TestMethod]
        public void And_the_component_is_the_brand_then_the_number_is_based_on_the_number_of_brands_supported()
        {
            var expectedNumberOfBitsForBrand =
                    PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent(
                        (uint) PharmaceuticalKeyConfiguration.GetNumberOfBrandsSupported());

            Assert.AreEqual(expectedNumberOfBitsForBrand, _keyConfiguration.GetNumberOfBitsForBrand());
        }

        [TestMethod]
        public void And_the_component_is_the_DateOfManufacture_then_the_number_is_based_on_the_number_of_days_supported()
        {
            var expectedNumberOfBitsForDateOfManufacture =
                    PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent(
                        PharmaceuticalKeyConfiguration.DaysSupported);

            Assert.AreEqual(expectedNumberOfBitsForDateOfManufacture, _keyConfiguration.GetNumberOfBitsForDateOfManufacture());
        }

        [TestMethod]
        public void And_the_component_is_the_manufacturer_then_the_number_is_based_on_the_number_of_manufacturers_supported()
        {
            var expectedNumberOfBitsForManufacturer=
                    PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent(
                        (uint)PharmaceuticalKeyConfiguration.GetNumberOfManufacturersSupported());

            Assert.AreEqual(expectedNumberOfBitsForManufacturer, _keyConfiguration.GetNumberOfBitsForManufacturer());
        }

        [TestMethod]
        public void And_the_component_is_the_serialization_id_then_the_number_is_based_on_the_number_of_serialization_ids_supported()
        {
            var expectedNumberOfBitsForSerializationId =
                    PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent(
                        PharmaceuticalKeyConfiguration.SerializationIDsSupported);

            Assert.AreEqual(expectedNumberOfBitsForSerializationId, _keyConfiguration.GetNumberOfBitsForSerializationId());
        }

        [TestMethod]
        public void And_the_component_is_the_pharmaceutical_type_then_the_number_is_based_on_the_number_of_pharmaceutical_types_supported()
        {
            var expectedNumberOfBitsForPharmaceuticalType =
                    PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent(
                        (uint)PharmaceuticalKeyConfiguration.GetNumberOfTypesSupported());

            Assert.AreEqual(expectedNumberOfBitsForPharmaceuticalType, _keyConfiguration.GetNumberOfBitsForType());
        }
    }
    // ReSharper restore InconsistentNaming
}
