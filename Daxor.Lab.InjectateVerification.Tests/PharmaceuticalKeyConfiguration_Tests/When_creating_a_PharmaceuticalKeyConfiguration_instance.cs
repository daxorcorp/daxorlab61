﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyConfiguration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_a_PharmaceuticalKeyConfiguration_instance
    {
        private static PharmaceuticalKeyConfiguration _configuration;
        private const uint StartingBatchId = 1;
        private const uint NumberOfChecksumBits = 2;
        private static readonly DateTime StartingDateOfManufacturing = DateTime.Now;

        [ClassInitialize]
        public static void TestsClassInitialize(TestContext context)
        {
            _configuration = new PharmaceuticalKeyConfiguration(StartingDateOfManufacturing, StartingBatchId,
                                                                NumberOfChecksumBits);
        }

        [TestMethod]
        public void Then_the_starting_batch_id_is_set()
        {
            Assert.AreEqual(StartingBatchId, _configuration.StartingBatchId);
        }

        [TestMethod]
        public void Then_the_starting_date_of_manufacturing_is_set()
        {
            Assert.AreEqual(StartingDateOfManufacturing, _configuration.StartingDateOfManufacture);
        }

        [TestMethod]
        public void Then_the_number_of_checksum_bits_is_set()
        {
            Assert.AreEqual(NumberOfChecksumBits, _configuration.NumberOfChecksumBits);
        }
    }
    // ReSharper restore InconsistentNaming
}
