﻿using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyConfiguration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_PharmaceuticalKeyConfiguration_constants
    {
        [TestMethod]
        [RequirementValue(10000)]
        public void Then_the_number_of_supported_batch_IDs_matches_requirements()
        {
            Assert.AreEqual((uint) RequirementHelper.GetRequirementValueAsInt(), PharmaceuticalKeyConfiguration.BatchIDsSupported);
        }

        [TestMethod]
        [RequirementValue(10000)]
        public void Then_the_number_of_supported_serialization_IDs_matches_requirements()
        {
            Assert.AreEqual((uint) RequirementHelper.GetRequirementValueAsInt(), PharmaceuticalKeyConfiguration.SerializationIDsSupported);
        }

        [TestMethod]
        [RequirementValue(sizeof(ulong)*8)]
        public void Then_the_container_size_matches_requirements()
        {
            Assert.AreEqual((uint) RequirementHelper.GetRequirementValueAsInt(), PharmaceuticalKeyConfiguration.BitsInContainer);
        }

        [TestMethod]
        [RequirementValue(8*366)]
        public void Then_the_number_of_years_supported_matches_requirements()
        {
            Assert.AreEqual((uint) RequirementHelper.GetRequirementValueAsInt(), PharmaceuticalKeyConfiguration.DaysSupported);
        }
    }
    // ReSharper restore InconsistentNaming
}