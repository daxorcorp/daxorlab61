﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyConfiguration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_the_number_of_bits_required
    {
        [TestMethod]
        public void Then_the_minimum_number_of_bits_is_based_on_the_six_components_of_a_pharmaceutical()
        {
            var bitsForDateOfManufacture = 
                PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent(PharmaceuticalKeyConfiguration.DaysSupported);
            var bitsForBatchId = 
                PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent(PharmaceuticalKeyConfiguration.BatchIDsSupported);
            var bitsForSerializationId = 
                PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent(PharmaceuticalKeyConfiguration.SerializationIDsSupported);
            var bitsForType = 
                PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent((uint) PharmaceuticalKeyConfiguration.GetNumberOfTypesSupported());
            var bitsForManufacturer =
                PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent((uint) PharmaceuticalKeyConfiguration.GetNumberOfManufacturersSupported());
            var bitsForBrand =
                PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent((uint)PharmaceuticalKeyConfiguration.GetNumberOfBrandsSupported());

            var expectedNumberOfBits = bitsForDateOfManufacture + bitsForBatchId + bitsForSerializationId +
                                       bitsForType + bitsForManufacturer + bitsForBrand;
            var observedNumberrOfBits = PharmaceuticalKeyConfiguration.GetMinimumNumberOfBitsRequired();

            Assert.AreEqual(expectedNumberOfBits,observedNumberrOfBits);
        }

        [TestMethod]
        public void And_the_value_is_a_power_of_2_then_correct_value_is_returned()
        {
            const uint NumberOfBits = 5;
            var inputValue = (uint)Math.Pow(2, NumberOfBits);
            var observedResult = PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent(inputValue);

            Assert.AreEqual(NumberOfBits, observedResult);
        }

        [TestMethod]
        public void And_the_value_is_not_a_power_of_2_then_correct_value_is_returned()
        {
            const uint NumberOfBits = 5;
            var inputValue = (uint)Math.Pow(2, NumberOfBits) + 1;
            var observedResult = PharmaceuticalKeyConfiguration.ComputeNumberOfBitsToRepresent(inputValue);

            Assert.AreEqual(NumberOfBits + 1, observedResult);
        }
    }
    // ReSharper restore InconsistentNaming
}