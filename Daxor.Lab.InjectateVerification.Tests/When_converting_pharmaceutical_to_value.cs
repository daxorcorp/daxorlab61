﻿using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Daxor.Lab.Infrastructure.DomainModels;
using System.Collections.Generic;

namespace Daxor.Lab.InjectateVerification.Tests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class When_converting_pharmaceutical_to_value
    {
        [TestMethod]
        public void And_the_Pharmaceutical_is_null_then_an_exception_is_thrown()
        {
            var configuration = new PharmaceuticalKeyCodecConfiguration(new MD5ChecksumGenerator(), new Alphanumeric56KeyRepresentation(),
                                                                                                        new PharmaceuticalKeyCodecBitMapping(), new PharmaceuticalKeyConfiguration(DateTime.Now, 0, 10)); 
            var target = new PharmaceuticalKeyCodecHelper(configuration);
            AssertEx.ThrowsArgumentNullException(()=>target.ConvertPharmaceuticalToValue(null), "pharmaceutical"); 
        }

        [TestMethod]
        public void And_the_pharmaceutical_is_valid_then_the_proper_value_is_returned()
        {
            var myBytes = new List<byte>();
            for (var i = 0; i < 53; i++)
                myBytes.Add((byte)i);

            var configuration = new PharmaceuticalKeyCodecConfiguration(new MD5ChecksumGenerator(), new Alphanumeric56KeyRepresentation(),
                                                                                                        new PharmaceuticalKeyCodecBitMapping(myBytes), new PharmaceuticalKeyConfiguration(DateTime.Now, 0, 10));
            var target = new PharmaceuticalKeyCodecHelper(configuration);
            var pharmaceutical = new Pharmaceutical(DateTime.Now, 10, 5, Pharmaceutical.PharmaceuticalManufacturer.Daxor,
                                                                Pharmaceutical.PharmaceuticalType.Injectate, Pharmaceutical.PharmaceuticalBrand.Volumex);
            const ulong expected = 5401900962848768;
            var actual = target.ConvertPharmaceuticalToValue(pharmaceutical);
            Assert.AreEqual(expected, actual);
        }
    }
}
