﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.UlongBitwiseExtensionMethods_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_ulong_to_a_list_of_bits
    {
        [TestMethod]
        public void Then_there_are_enough_bits()
        {
            var bits = ((ulong) 12).ToBits();
            Assert.AreEqual(UnsignedLongBitwiseOperationExtensions.NumberOfBitsForUnsignedLong, bits.Count());
        }

        [TestMethod]
        public void Then_the_first_value_represents_the_least_significant_bit()
        {
            var oneAsBitList = CreateBitListWhereAllBitsAreFalse();
            oneAsBitList[0] = true;  // The number "1" as bit sequence

            var observedBits = ((ulong) 1).ToBits().ToList();
            
            CollectionAssert.AreEqual(oneAsBitList, observedBits);
        }

        [TestMethod]
        public void Then_the_pattern_of_bits_matches_the_ulong_value()
        {
            const ulong valueToConvert = 123456789123;

            var expectedBits = CreateBitListWhereAllBitsAreFalse();
            SetBits(expectedBits, new List<int> { 0, 1, 7, 9, 11, 12, 16, 19, 20, 23, 25, 26, 27, 28, 29, 31, 34, 35, 36});

            var observedBits = valueToConvert.ToBits().ToList();

            CollectionAssert.AreEqual(expectedBits, observedBits);
        }

        private List<bool> CreateBitListWhereAllBitsAreFalse()
        {
            var bitList = new List<bool>();
            for (var i = 0; i < UnsignedLongBitwiseOperationExtensions.NumberOfBitsForUnsignedLong; i++)
                bitList.Add(false);
            return bitList;
        }

        private void SetBits(IList<bool> bitList, IEnumerable<int> indicesOfBitsToSet)
        {
            foreach (var index in indicesOfBitsToSet)
                bitList[index] = true;
        }
    }
    // ReSharper restore InconsistentNaming
}
