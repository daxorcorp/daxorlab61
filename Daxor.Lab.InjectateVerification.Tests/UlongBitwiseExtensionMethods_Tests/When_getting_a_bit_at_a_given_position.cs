﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.UlongBitwiseExtensionMethods_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_a_bit_at_a_given_position
    {
        private const ulong ValueToTest = 14;

        [TestMethod]
        public void And_the_bit_is_set_then_true_is_returned()
        {
            Assert.IsTrue(ValueToTest.GetBitAtPosition(3));
        }

        [TestMethod]
        public void And_the_bit_is_cleared_then_false_is_returned()
        {
            Assert.IsFalse(ValueToTest.GetBitAtPosition(0));
        }
    }
    // ReSharper restore InconsistentNaming
}
