﻿using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.UlongBitwiseExtensionMethods_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_placing_a_bit_at_a_given_position
    {
        [TestMethod]
        public void And_the_position_is_negative_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentOutOfRangeException>(()=>((ulong) 1).PlaceBitAtPosition(true, -1));
        }

        [TestMethod]
        public void And_the_position_exceeds_the_number_of_bits_in_a_ulong_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentOutOfRangeException>(()=>((ulong)1).PlaceBitAtPosition(true, UnsignedLongBitwiseOperationExtensions.NumberOfBitsForUnsignedLong));
        }
    }
    // ReSharper restore InconsistentNaming
}
