﻿using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.UlongBitwiseExtensionMethods_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_a_bit_at_a_given_position
    {
        private const ulong ValueToTest = 1;

        [TestMethod]
        public void And_the_position_is_negative_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentOutOfRangeException>(()=>ValueToTest.GetBitAtPosition(-1));
        }

        [TestMethod]
        public void And_the_position_exceeds_the_number_of_bits_in_a_ulong_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentOutOfRangeException>(()=>ValueToTest.GetBitAtPosition(UnsignedLongBitwiseOperationExtensions.NumberOfBitsForUnsignedLong));
        }
    }
    // ReSharper restore InconsistentNaming
}
