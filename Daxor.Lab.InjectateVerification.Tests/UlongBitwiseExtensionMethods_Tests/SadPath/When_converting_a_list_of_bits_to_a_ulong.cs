﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.UlongBitwiseExtensionMethods_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_list_of_bits_to_a_ulong
    {
        [TestMethod]
        public void And_there_are_too_many_bits_then_an_exception_is_thrown()
        {
            var tooManyBits = new List<bool>();
            Enumerable.Range(0, UnsignedLongBitwiseOperationExtensions.NumberOfBitsForUnsignedLong + 1)
                .ToList().ForEach(x => tooManyBits.Add(false));

            AssertEx.Throws<ArgumentException>(()=>tooManyBits.ToUnsignedLong());
        }
    }
    // ReSharper restore InconsistentNaming
}
