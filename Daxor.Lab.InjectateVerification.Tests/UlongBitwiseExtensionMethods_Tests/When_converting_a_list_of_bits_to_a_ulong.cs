﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.UlongBitwiseExtensionMethods_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_list_of_bits_to_a_ulong
    {
        [TestMethod]
        public void And_there_are_too_few_bits_then_the_value_is_still_generated()
        {
            var oneAsBitList = CreateBitListWhereAllBitsAreTrue().ToList();
            var truncatedOneAsBitList = oneAsBitList.Take(4).ToList();

            var observedUnsignedLong = truncatedOneAsBitList.ToUnsignedLong();

            Assert.AreEqual((ulong) 15, observedUnsignedLong);
        }

        [TestMethod]
        public void Then_the_first_value_represents_the_least_significant_bit()
        {
            var oneAsBitList = CreateBitListWhereAllBitsAreFalse();
            oneAsBitList[0] = true;  // The number "1" as bit sequence

            var observedUnsignedLong = oneAsBitList.ToUnsignedLong();

            Assert.AreEqual((ulong) 1, observedUnsignedLong);
        }

        [TestMethod]
        public void Then_the_ulong_value_is_correct()
        {
            var expectedBits = CreateBitListWhereAllBitsAreFalse();
            SetBits(expectedBits, new List<int> { 0, 1, 7, 9, 11, 12, 16, 19, 20, 23, 25, 26, 27, 28, 29, 31, 34, 35, 36 });
            const ulong expectedUnsignedLong = 123456789123;

            var observedUnsignedLong = expectedBits.ToUnsignedLong();

            Assert.AreEqual(expectedUnsignedLong, observedUnsignedLong);
        }

        private List<bool> CreateBitListWhereAllBitsAreFalse()
        {
            var bitList = new List<bool>();
            for (var i = 0; i < UnsignedLongBitwiseOperationExtensions.NumberOfBitsForUnsignedLong; i++)
                bitList.Add(false);
            return bitList;
        }

        private IEnumerable<bool> CreateBitListWhereAllBitsAreTrue()
        {
            var bitList = new List<bool>();
            for (var i = 0; i < UnsignedLongBitwiseOperationExtensions.NumberOfBitsForUnsignedLong; i++)
                bitList.Add(true);
            return bitList;
        }

        private void SetBits(IList<bool> bitList, IEnumerable<int> indicesOfBitsToSet)
        {
            foreach (var index in indicesOfBitsToSet)
                bitList[index] = true;
        }
    }
    // ReSharper restore InconsistentNaming
}
