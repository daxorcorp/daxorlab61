﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.UlongBitwiseExtensionMethods_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_placing_a_bit_at_a_given_position
    {
        private const ulong startingValue = 14;

        [TestMethod]
        public void And_the_bit_is_to_be_cleared_then_the_correct_value_is_returned()
        {
            Assert.AreEqual((ulong) 15, startingValue.PlaceBitAtPosition(true, 0));
        }

        [TestMethod]
        public void And_the_bit_is_to_be_set_then_the_correct_value_is_returned()
        {
            Assert.AreEqual((ulong)12, startingValue.PlaceBitAtPosition(false, 1));
        }
    }
    // ReSharper restore InconsistentNaming
}
