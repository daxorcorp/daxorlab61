﻿using System.Collections.Generic;
using System.Linq;

namespace Daxor.Lab.InjectateVerification.Tests
{
    public static class InjectateVerificationTestHelpers
    {
        public const uint NumberOfBitsInKey = 8;

        public static List<byte> GenerateIdentityMappingOfLength(uint capacity)
        {
            var mapping = new byte[capacity];
            for (byte i = 0; i < capacity; i++)
                mapping[i] = i;

            return mapping.ToList();
        }
    }
}
