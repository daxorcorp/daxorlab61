﻿using System;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.InjectateVerification.Tests
{
	internal class ModuleThatDoesNotGetConfigurations : Module
	{
		public ModuleThatDoesNotGetConfigurations(IUnityContainer container, ISettingsManager settingsManager) : base(container, settingsManager)
		{
		}

		protected override PharmaceuticalKeyCodecConfiguration GetCodecConfiguration()
		{
			return new PharmaceuticalKeyCodecConfiguration(new MD5ChecksumGenerator(), new Alphanumeric56KeyRepresentation(), new PharmaceuticalKeyCodecBitMapping(), new PharmaceuticalKeyConfiguration(DateTime.MinValue, 0, 0));
		}

		protected override PharmaceuticalKeyConfiguration GetKeyConfiguration()
		{
			return new PharmaceuticalKeyConfiguration(DateTime.MinValue, 0, 0);
		}
	}
}
