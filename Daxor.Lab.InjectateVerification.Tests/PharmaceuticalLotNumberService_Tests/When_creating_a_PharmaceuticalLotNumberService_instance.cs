﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalLotNumberService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_a_PharmaceuticalLotNumberService_instance
    {
        [TestMethod]
        public void And_the_PharmaceuticalKeyConfiguration_instance_is_not_null_then_no_exception_is_thrown()
        {
            var config = new PharmaceuticalKeyConfiguration(DateTime.Now, 0, 10);

            // ReSharper disable UnusedVariable
            var ignored = new PharmaceuticalLotNumberService(config);
            // ReSharper restore UnusedVariable
        }
    }
    // ReSharper restore InconsistentNaming
}
