using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalLotNumberService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_lot_number_prefix
    {
        [TestMethod]
        public void And_the_Pharmaceutical_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>PharmaceuticalLotNumberService.GetLotPrefix(null), "pharmaceutical");
        }
    }
    // ReSharper restore InconsistentNaming
}
