using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalLotNumberService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_a_PharmaceuticalLotNumberService_instance
    {
        [TestMethod]
        public void And_the_PharmaceuticalKeyConfiguration_instance_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(() => {
                                                           var ignored = new PharmaceuticalLotNumberService(null);
            }, "configuration");
            // ReSharper restore UnusedVariable
        }
    }
    // ReSharper restore InconsistentNaming
}
