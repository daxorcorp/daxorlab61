using System;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalLotNumberService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_generating_the_lot_number
    {
        private static PharmaceuticalKeyConfiguration _keyConfiguration;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _keyConfiguration = new PharmaceuticalKeyConfiguration(DateTime.Now, 0, 10);
        }
        
        [TestMethod]
        public void And_the_pharmaceutical_batch_id_is_greater_than_the_max_supported_then_an_exception_is_thrown()
        {
            var lotNumberService = new PharmaceuticalLotNumberService(_keyConfiguration);
            var pharmaceutical = new Pharmaceutical 
                {BatchId = (int) _keyConfiguration.GetMaxBatchIdSupported() + 1};

            try
            {
                lotNumberService.Generate(pharmaceutical);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                var expectedMessage = String.Format(PharmaceuticalLotNumberService.LotNumberOutOfRangeExceptionMessage,
                                                    pharmaceutical.BatchId, _keyConfiguration.StartingBatchId,
                                                    _keyConfiguration.GetMaxBatchIdSupported());
                Assert.AreEqual(expectedMessage, ex.Message);
                return;
            }
            catch (Exception ex)
            {
                Assert.Fail("ArgumentOutOfRangeException not thrown -- was " + 
                    ex.GetType() + " instead");
            }

            Assert.Fail("No exception was thrown");
        }

        [TestMethod]
        public void And_the_pharmaceutical_validator_throws_an_exception_then_that_exception_is_not_caught()
        {
            var lotNumberService = new PharmaceuticalLotNumberService(_keyConfiguration);
            var pharmaceutical = new Pharmaceutical();

            AssertEx.Throws<InvalidOperationException>(()=>lotNumberService.Generate(pharmaceutical));
        }

        [TestMethod]
        public void And_Pharmaceutical_is_null_then_an_exception_is_thrown()
        {
            var lotNumberService = new PharmaceuticalLotNumberService(_keyConfiguration);

            AssertEx.ThrowsArgumentNullException(()=>lotNumberService.Generate(null), "pharmaceutical");
        }

        [TestMethod]
        public void and_the_Pharmaceutical_is_not_null_and_has_default_fields_then_an_exception_is_thrown()
        {
            var pharmaceutical = new Pharmaceutical();

            var lotNumberService = new PharmaceuticalLotNumberService(_keyConfiguration);
            try
            {
                lotNumberService.Generate(pharmaceutical);
            }
            catch (InvalidOperationException ex)
            {
                var expectedMessage = String.Format(PharmaceuticalLotNumberService.UndefinedFieldExceptionMessage, "DateOfManufacture BatchID Manufacturer Type Brand ");
                Assert.AreEqual(expectedMessage, ex.Message);
                return;
            }
            catch (Exception ex)
            {
                Assert.Fail("Wrong exception throw: Expected InvalidOperation, instead got " + ex.GetType());
            }
            Assert.Fail("No exception was thrown");
        }

        [TestMethod]
        public void and_the_Pharmaceutical_is_not_null_and_has_some_default_fields_then_an_exception_is_thrown()
        {
            var pharmaceutical = new Pharmaceutical();
            pharmaceutical.Type = Pharmaceutical.PharmaceuticalType.Injectate;
            pharmaceutical.Manufacturer = Pharmaceutical.PharmaceuticalManufacturer.Daxor;

            var lotNumberService = new PharmaceuticalLotNumberService(_keyConfiguration);

            AssertEx.Throws<InvalidOperationException>(()=>lotNumberService.Generate(pharmaceutical));
        }
    }
    // ReSharper restore InconsistentNaming
}
