using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalLotNumberService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_PharmaceuticalLotNumberService_constants
    {
        [TestMethod]
        [RequirementValue(4)]
        public void Then_the_number_of_digits_for_the_batch_id_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), PharmaceuticalLotNumberService.DigitsForBatchId);
        }

        [TestMethod]
        [RequirementValue(2)]
        public void Then_the_number_of_digits_for_the_day_component_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), PharmaceuticalLotNumberService.DigitsForDayComponent);
        }

        [TestMethod]
        [RequirementValue(2)]
        public void Then_the_number_of_digits_for_the_month_component_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), PharmaceuticalLotNumberService.DigitsForMonthComponent);
        }

        [TestMethod]
        [RequirementValue(2)]
        public void Then_the_number_of_digits_for_the_year_component_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), PharmaceuticalLotNumberService.DigitsForYearComponent);
        }

        [TestMethod]
        [RequirementValue('-')]
        public void Then_the_batch_id_separator_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsChar(), PharmaceuticalLotNumberService.BatchIdSeparator);
        }

        [TestMethod]
        [RequirementValue("D")]
        public void Then_the_Daxor_lot_number_prefix_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), PharmaceuticalLotNumberService.DaxorLotNumberPrefix);
        }

        [TestMethod]
        [RequirementValue("V")]
        public void Then_the_IsoTex_lot_number_prefix_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), PharmaceuticalLotNumberService.IsoTexLotNumberPrefix);
        }

        [TestMethod]
        [RequirementValue("?")]
        public void Then_the_Undefined_lot_number_prefix_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), PharmaceuticalLotNumberService.UndefinedLotNumberPrefix);
        }
    }
    // ReSharper restore InconsistentNaming
}
