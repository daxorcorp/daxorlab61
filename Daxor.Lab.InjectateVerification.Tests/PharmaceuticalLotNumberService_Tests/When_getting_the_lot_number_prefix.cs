using Daxor.Lab.Infrastructure.DomainModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalLotNumberService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_lot_number_prefix
    {
        private Pharmaceutical _pharmaceutical;

        // Code that should be executed before running the first test in this class.
        [TestInitialize]
        public void TestsClassInitialize()
        {
            _pharmaceutical = new Pharmaceutical();
        }

        [TestMethod]
        public void And_the_pharmaceutical_manufacturer_is_Daxor_then_the_correct_prefix_is_returned()
        {
            _pharmaceutical.Manufacturer = Pharmaceutical.PharmaceuticalManufacturer.Daxor;

            var observedReturnValue = PharmaceuticalLotNumberService.GetLotPrefix(_pharmaceutical);

            Assert.AreEqual(PharmaceuticalLotNumberService.DaxorLotNumberPrefix, observedReturnValue);
        }

        [TestMethod]
        public void And_the_pharmaceutical_manufacturer_is_IsoTex_then_the_correct_prefix_is_returned()
        {
            _pharmaceutical.Manufacturer = Pharmaceutical.PharmaceuticalManufacturer.IsoTex;

            var observedReturnValue = PharmaceuticalLotNumberService.GetLotPrefix(_pharmaceutical);

            Assert.AreEqual(PharmaceuticalLotNumberService.IsoTexLotNumberPrefix, observedReturnValue);
        }

        [TestMethod]
        public void And_the_pharmaceutical_manufacturer_is_undefined_then_the_correct_prefix_is_returned()
        {
            var observedReturnValue = PharmaceuticalLotNumberService.GetLotPrefix(_pharmaceutical);

            Assert.AreEqual(PharmaceuticalLotNumberService.UndefinedLotNumberPrefix, observedReturnValue);
        }
    }
    // ReSharper restore InconsistentNaming
}
