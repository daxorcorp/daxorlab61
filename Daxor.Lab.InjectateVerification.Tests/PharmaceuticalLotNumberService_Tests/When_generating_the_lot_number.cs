﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalLotNumberService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_generating_the_lot_number
    {
        [TestMethod]
        public void And_the_pharmaceutical_is_well_defined_then_the_proper_pharmaceutical_lot_is_returned()
        {
            var keyConfiguration = new PharmaceuticalKeyConfiguration(DateTime.Now, 0, 10);

            var lotNumberService = new PharmaceuticalLotNumberService(keyConfiguration);
            
            var pharmaceutical = new Pharmaceutical
                {
                    BatchId = 20,
                    Brand = Pharmaceutical.PharmaceuticalBrand.Volumex,
                    DateOfManufacture = new DateTime(2013, 6, 1),
                    Manufacturer = Pharmaceutical.PharmaceuticalManufacturer.Daxor,
                    Type = Pharmaceutical.PharmaceuticalType.Standard,
                    SerializationId = 1001
                };

            const string expectedLotNumer = "D130106-0020";
            string observedLotNumber = lotNumberService.Generate(pharmaceutical);

            Assert.AreEqual(expectedLotNumer, observedLotNumber, "The lot number was not properly returned.");
        }
    }

    // ReSharper restore InconsistentNaming
}
