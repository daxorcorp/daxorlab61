﻿using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class When_constructing_PharmaceuticalKeyCodecHelper
    {
        [TestMethod]
        public void And_the_PharmaceuticalConfiguration_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>new PharmaceuticalKeyCodecHelper(null), "configuration");
        }
    }
}
