﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyCodecConfiguration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_number_of_characters_in_the_key
    {
        private static IChecksumGenerator _checksumGenerator;
        private static PharmaceuticalKeyCodecBitMapping _bitMapping;
        private static PharmaceuticalKeyConfiguration _keyConfiguration;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _checksumGenerator = new MD5ChecksumGenerator();
            _bitMapping = new PharmaceuticalKeyCodecBitMapping();
            _keyConfiguration = new PharmaceuticalKeyConfiguration(DateTime.Now, 0, 10);
        }

        [TestMethod]
        public void And_the_representation_is_of_zero_length_then_there_are_zero_characters_in_the_key()
        {
            var zeroLengthKeyRepresentation = StubKeyRepresentation.Create(String.Empty);
            var keyCodecConfiguration = new PharmaceuticalKeyCodecConfiguration(_checksumGenerator, 
                zeroLengthKeyRepresentation, _bitMapping, _keyConfiguration);

            const uint expectedNumberOfCharacters = 0; 
            var observedNumberOfCharacters = keyCodecConfiguration.GetNumberOfCharactersInKey();

            Assert.AreEqual(expectedNumberOfCharacters, observedNumberOfCharacters);
        }

        [TestMethod]
        public void And_the_representation_is_of_length_ten_then_there_are_16_characters_in_the_key()
        {
            var keyRepresentationOfLengthTen = StubKeyRepresentation.Create("1234567890");
            var keyCodecConfiguration = new PharmaceuticalKeyCodecConfiguration(_checksumGenerator,
                keyRepresentationOfLengthTen, _bitMapping, _keyConfiguration);

            // This is calculated from minimum number of bits (43) plus number of checksum bits (10) 
            // divided by log base 2 of number of characters in the representation (10)
            // Math.Ceiling(53/(log2(10))) = 16
            const uint expectedNumberOfCharacters = 16;
            var observedNumberOfCharacters = keyCodecConfiguration.GetNumberOfCharactersInKey();

            Assert.AreEqual(expectedNumberOfCharacters, observedNumberOfCharacters);
        }
    }
    // ReSharper restore InconsistentNaming
}
