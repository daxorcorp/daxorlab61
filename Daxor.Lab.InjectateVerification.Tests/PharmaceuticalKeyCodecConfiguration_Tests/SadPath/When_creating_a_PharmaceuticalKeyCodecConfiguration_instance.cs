using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyCodecConfiguration_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_a_PharmaceuticalKeyCodecConfiguration_instance
    {
        private static IChecksumGenerator _checksumGenerator;
        private static KeyRepresentation _keyRepresentation;
        private static PharmaceuticalKeyCodecBitMapping _bitMapping;
        private static PharmaceuticalKeyConfiguration _keyConfiguration;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _checksumGenerator = new MD5ChecksumGenerator();
            _keyRepresentation = new Alphanumeric56KeyRepresentation();
            _bitMapping = new PharmaceuticalKeyCodecBitMapping();
            _keyConfiguration = new PharmaceuticalKeyConfiguration(DateTime.Now, 0, 10);
        }
        
        [TestMethod]
        public void And_the_checksum_generator_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var ignored = new PharmaceuticalKeyCodecConfiguration(null,
                    _keyRepresentation, _bitMapping, _keyConfiguration);
            }, "checksumGenerator");
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_the_key_representation_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var ignored = new PharmaceuticalKeyCodecConfiguration(_checksumGenerator,
                    null, _bitMapping, _keyConfiguration);
            }, "keyRepresentation");
            // ReSharper restore UnusedVariable
        }
        
        [TestMethod]
        public void And_the_bit_mapping_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var ignored = new PharmaceuticalKeyCodecConfiguration(_checksumGenerator,
                    _keyRepresentation, null, _keyConfiguration);
            }, "pharmaceuticalKeyCodecBitMapping");
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_the_key_configuration_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var ignored = new PharmaceuticalKeyCodecConfiguration(_checksumGenerator,
                    _keyRepresentation, _bitMapping, null);
            }, "pharmaceuticalKeyConfiguration");
            // ReSharper restore UnusedVariable
        }
    }
    // ReSharper restore InconsistentNaming
}
