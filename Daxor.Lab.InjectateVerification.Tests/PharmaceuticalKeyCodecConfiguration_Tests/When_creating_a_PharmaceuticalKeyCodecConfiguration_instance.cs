﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyCodecConfiguration_Tests
{
    [TestClass]
    // ReSharper disable InconsistentNaming
    public class When_creating_a_PharmaceuticalKeyCodecConfiguration_instance
    {
        private static IChecksumGenerator _checksumGenerator;
        private static KeyRepresentation _keyRepresentation;
        private static PharmaceuticalKeyCodecBitMapping _bitMapping;
        private static PharmaceuticalKeyConfiguration _keyConfiguration;
        private static PharmaceuticalKeyCodecConfiguration _keyCodecConfiguration;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _checksumGenerator = new MD5ChecksumGenerator();
            _keyRepresentation = new Alphanumeric56KeyRepresentation();
            _bitMapping = new PharmaceuticalKeyCodecBitMapping();
            _keyConfiguration = new PharmaceuticalKeyConfiguration(DateTime.Now, 0, 10);
            
            _keyCodecConfiguration = new PharmaceuticalKeyCodecConfiguration(_checksumGenerator,
                _keyRepresentation, _bitMapping, _keyConfiguration);
        }
        
        [TestMethod]
        public void Then_the_checksum_generator_is_set()
        {
            Assert.AreSame(_checksumGenerator, _keyCodecConfiguration.ChecksumGenerator);
        }

        [TestMethod]
        public void Then_the_key_representation_is_set()
        {
            Assert.AreSame(_keyRepresentation, _keyCodecConfiguration.Representation);
        }

        [TestMethod]
        public void Then_the_key_codec_bit_mapping_is_set()
        {
            Assert.AreSame(_bitMapping, _keyCodecConfiguration.BitMapping);
        }

        [TestMethod]
        public void Then_the_key_configuration_is_set()
        {
            Assert.AreSame(_keyConfiguration, _keyCodecConfiguration.KeyConfiguration);
        }
    }
    // ReSharper restore InconsistentNaming

}
