﻿namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyCodecConfiguration_Tests
{
    public class StubKeyRepresentation: KeyRepresentation
    {
        private static string _representationAsString;

        private StubKeyRepresentation(){ }

        public static StubKeyRepresentation Create(string representationAsString)
        {
            _representationAsString = representationAsString;
            return new StubKeyRepresentation();
        }

        public override string DisplayName
        {
            get
            {
                return "ISA Unit Test Key Reperesentation";
            }
        }

        public override string RepresentationAsString
        {
            get
            {
                return _representationAsString;
            }
        }
    }
}
