﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.PharmaceuticalKeyCodecConfiguration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_representing_the_PharmaceuticalKeyCodecConfiguration_as_a_string
    {
        [TestMethod]
        public void And_all_its_members_are_defined_then_the_proper_string_is_printed()
        {
            var checksumGenerator = new MD5ChecksumGenerator();
            var keyRepresentation = StubKeyRepresentation.Create(String.Empty);
            var pharmaceuticalKeyCodecBitMapping = new PharmaceuticalKeyCodecBitMapping();
            var pharmaceuticalKeyConfiguration = new PharmaceuticalKeyConfiguration(DateTime.Now,0,10); 
            var keyCodecConfiguration = new PharmaceuticalKeyCodecConfiguration(checksumGenerator, 
                keyRepresentation, pharmaceuticalKeyCodecBitMapping, pharmaceuticalKeyConfiguration);

            var expectedResult = String.Format(PharmaceuticalKeyCodecConfiguration.ToStringFormat, 
                checksumGenerator.DisplayName, keyRepresentation.DisplayName, 
                pharmaceuticalKeyCodecBitMapping, pharmaceuticalKeyConfiguration);

            var observedResult = keyCodecConfiguration.ToString();
            Assert.AreEqual(expectedResult, observedResult);
        }
    }
    // ReSharper restore InconsistentNaming
}
