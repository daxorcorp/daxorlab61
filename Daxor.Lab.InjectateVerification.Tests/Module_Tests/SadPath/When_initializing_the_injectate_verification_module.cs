﻿using System;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.InjectateVerification.Tests.Module_Tests.SadPath
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_the_Injectate_Verification_module
	{
		private ModuleThatDoesNotGetConfigurations _module;
		private ISettingsManager _settingsManager;
		private IUnityContainer _container;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_container = Substitute.For<IUnityContainer>();
			_settingsManager = Substitute.For<ISettingsManager>();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_pharmaceutical_key_codec_configuration_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c =>
					c.RegisterInstance(typeof(PharmaceuticalKeyCodecConfiguration), Arg.Any<PharmaceuticalKeyCodecConfiguration>(),
						Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new ModuleThatDoesNotGetConfigurations(_container, _settingsManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Injectate Verification", ex.ModuleName);
				Assert.AreEqual("initialize the Pharmaceutical Key Codec Configuration", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_pharmaceutical_key_configuration_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c =>
					c.RegisterInstance(typeof(PharmaceuticalKeyConfiguration), Arg.Any<PharmaceuticalKeyConfiguration>(),
						Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new ModuleThatDoesNotGetConfigurations(_container, _settingsManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Injectate Verification", ex.ModuleName);
				Assert.AreEqual("initialize the Pharmaceutical Key Configuration", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_pharmaceutical_key_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IPharmaceuticalKeyDecoder, PharmaceuticalKeyService>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new ModuleThatDoesNotGetConfigurations(_container, _settingsManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Injectate Verification", ex.ModuleName);
				Assert.AreEqual("initialize the Pharmaceutical Key Service", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_pharmaceutical_lot_number_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<ILotNumberGenerator, PharmaceuticalLotNumberService>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new ModuleThatDoesNotGetConfigurations(_container, _settingsManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Injectate Verification", ex.ModuleName);
				Assert.AreEqual("initialize the Pharmaceutical Lot Number Service", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}
	}
	// ReSharper restore InconsistentNaming
}