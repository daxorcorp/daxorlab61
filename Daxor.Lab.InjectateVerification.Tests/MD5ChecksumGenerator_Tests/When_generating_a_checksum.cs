﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.InjectateVerification.Tests.MD5ChecksumGenerator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_generating_a_checksum
    {
        private static MD5ChecksumGenerator _checksumGenerator;
        
        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _checksumGenerator = new MD5ChecksumGenerator();
        }

        [TestMethod]
        public void And_the_input_value_is_zero_then_the_correct_checksum_is_computed()
        {
            const ulong expectedChecksum = 17250428785856138703; 
            var actualChecksum = _checksumGenerator.GenerateChecksum(0);
            
            Assert.AreEqual(expectedChecksum, actualChecksum);
        }

        [TestMethod]
        public void And_the_input_value_is_the_maximum_ulong_then_the_correct_checksum_is_computed()
        {
            const ulong expectedChecksum = 8469679736696961150;
            var actualChecksum = _checksumGenerator.GenerateChecksum(ulong.MaxValue);

            Assert.AreEqual(expectedChecksum, actualChecksum);
        }
    }
    // ReSharper restore InconsistentNaming

}
