﻿using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Composite.Logging;
using RocketDivision.StarBurnX;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;

namespace Daxor.Lab.Infrastructure.UserControls
{
    /// <summary>
    /// Interaction logic for FolderBrowser.xaml
    /// </summary>
    public partial class FolderBrowser : IFolderBrowser
    {
        #region Constants
        
        private const string DriveSeparator = @":\";

        #endregion
        
        #region Fields

        private IDataBurner _dataBurner;
        private readonly ILoggerFacade _logger;
        private readonly IDriveAccessorService _driveAccessorService;

        #endregion

        #region Ctor

        public FolderBrowser(ILoggerFacade logger, IStarBurnWrapper driveWrapper, IDriveAccessorService driveAccessorService)
        {
            _logger = logger;
            _driveAccessorService = driveAccessorService;
            InitializeComponent();
            Loaded += OnFolderBrowserLoaded;

            try
            {
                _dataBurner = driveWrapper.CreateDataBurner();
            }
            catch
            {
                _logger.Log("Unable to create a DataBurner via StarBurn", Category.Exception, Priority.High);
            }
        }

        #endregion

        #region Dtor
        
        ~FolderBrowser()
        {
            if (_dataBurner != null)
            {
                Marshal.ReleaseComObject(_dataBurner);
                _dataBurner = null;
            }
        }

        #endregion

        #region Event Handlers
        void OnFolderBrowserLoaded(object sender, RoutedEventArgs e)
        {
            driveList.SelectionChanged += OnDriveListSelectionChanged;
            treeView.SelectedItemChanged += OnTreeViewSelectedItemChanged;
            AddDrives();
            AvailableText = "Select An Available Drive";
        }

        void OnTreeViewSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var treeItem = e.NewValue as TreeViewItem;
            
            if (treeItem != null)
                CurrentPath = "Path: " + treeItem.Tag;
        }
        
        void OnDriveListSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            treeView.Items.Clear();
            AvailableText = string.Empty;

            if (e.AddedItems.Count == 0)
                return;

            var item = e.AddedItems[0] as ExportDrive;
            if (item == null)
                return;

            IsOpticalDrive = item.IsOpticalDrive;
            if (item.IsOpticalDrive)
            {
                _dataBurner.LockTray = false;

                _dataBurner.Drive.DiscInfo.Refresh();
                if (_dataBurner.Drive.DiscInfo.DiscType != 4)
                    _dataBurner.Drive.Eject();

                IsOpticalDrive = true;
            }
            CurrentPath = "Path: " + item.Content;
            
            // ReSharper disable UseObjectOrCollectionInitializer
            var treeItem = new TreeViewItem();
            // ReSharper restore UseObjectOrCollectionInitializer

            treeItem.Header = item.Tag;
            treeItem.Tag = item.Tag;
            treeItem.FontWeight = FontWeights.Bold;
            treeItem.Items.Add(null);
            treeItem.IsExpanded = false;
            treeItem.Expanded += OnTreeViewItemExpanded;
            treeItem.IsSelected = true;
            treeView.Items.Add(treeItem);        
        }
        #endregion

        #region Properties

        public string DrivesTitle
        {
            get { return (string)GetValue(DrivesTitleProperty); }
            set { SetValue(DrivesTitleProperty, value); }
        }

        public static readonly DependencyProperty DrivesTitleProperty =
            DependencyProperty.Register("DrivesTitle", typeof(string), typeof(FolderBrowser), new UIPropertyMetadata("Available Drives"));

        public string CurrentPath
        {
            get 
            {
                var drivePath = (string)GetValue(CurrentPathProperty);

                if (String.IsNullOrEmpty(drivePath))
                    return String.Empty;

                if (drivePath.Contains(DriveSeparator))
                    return FormattedDrivePath(drivePath);

                return drivePath;
            }
            set { SetValue(CurrentPathProperty, value); }
        }

        public static readonly DependencyProperty CurrentPathProperty =
            DependencyProperty.Register("CurrentPath", typeof(String), typeof(FolderBrowser), new UIPropertyMetadata(String.Empty));

        public string AvailableText
        {
            get { return (String)GetValue(AvailableTextProperty); }
            set { SetValue(AvailableTextProperty, value); }
        }

        public static readonly DependencyProperty AvailableTextProperty =
            DependencyProperty.Register("AvailableText", typeof(String), typeof(FolderBrowser), new UIPropertyMetadata(String.Empty));

        public bool IsOpticalDrive { get; set; }

        #endregion

        #region Public API

        public bool IsPathValid()
        {
            return CurrentPath.Contains(DriveSeparator);
        }

        #endregion

        #region Helpers
        void AddDrives()
        {
            treeView.Items.Clear();
            driveList.Items.Clear();

            foreach (var drive in _driveAccessorService.GetAvailableDrives())
            {
                var exportDrive = new ExportDrive {Content = drive.Name, Tag = drive.Name};
                if (drive.DriveType == DriveTypeEnum.Optical)
                    exportDrive.IsOpticalDrive = true;
                if (drive.DriveType == DriveTypeEnum.Usb)
                    exportDrive.IsUsb = true;

                driveList.Items.Add(exportDrive);
            }

            noDrivesAvailable.Visibility = driveList.HasItems ? Visibility.Collapsed : Visibility.Visible;
        }

        void OnTreeViewItemExpanded(object sender, RoutedEventArgs e)
        {
            if (sender == null)
                return;

            var rootItem = (TreeViewItem)sender;

            if (rootItem.Items.Count != 1 || rootItem.Items[0] != null) 
                return;

            rootItem.Items.Clear();

            string[] directories;
            
            try
            {
                directories = Directory.GetDirectories((string)rootItem.Tag);
            }
            catch (Exception ex)
            {
                _logger.Log("Unable to get directories from " + rootItem.Tag + ": " + ex.Message, Category.Exception, Priority.High);
                return;
            }

            foreach (var dir in directories)
            {
                // ReSharper disable UseObjectOrCollectionInitializer
                var subItem = new TreeViewItem();
                // ReSharper restore UseObjectOrCollectionInitializer
                
                subItem.Style = treeView.ItemContainerStyle;
                subItem.Header = new DirectoryInfo(dir).Name;
                subItem.Tag = dir;
                try
                {
                    if (Directory.GetDirectories(dir).Length > 0)
                        subItem.Items.Add(null);
                }
                catch
                {
                    _logger.Log("Unable to add directory: " + dir, Category.Exception, Priority.High);
                    return;
                }
                subItem.Expanded += OnTreeViewItemExpanded;
                rootItem.Items.Add(subItem);
            }
        }

        private string FormattedDrivePath(string drivePath)
        {
            var driveStartIndex = drivePath.IndexOf(DriveSeparator, StringComparison.Ordinal) - 1;
            return drivePath.Substring(driveStartIndex);
        }

        #endregion
    }
}
