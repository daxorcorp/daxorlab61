﻿using System;

namespace Daxor.Lab.Infrastructure.Alerts
{
    public class AlertViolationPayload
    {
        public AlertViolationPayload(Guid alertId)
        {
            AlertId = alertId;
        }

        public Guid AlertId { get; private set; }
        public AlertViolationPayload ForModel(Object model)
        {
            UniqueModelPropertyId = AlertId + ":" + model.GetHashCode();

            return this;
        }
        public Guid TestId { get; set; }
        public string UniqueModelPropertyId { get; private set; }
        public string ErrorHeader { get; set; }
        public string Resolution { get; set; }
        public string ErrorOwner { get; set; }
        public bool RemoveAlertFromList { get; set; }
    }
}
