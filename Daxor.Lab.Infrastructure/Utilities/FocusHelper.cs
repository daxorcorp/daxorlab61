﻿using System.Windows;

namespace Daxor.Lab.Infrastructure.Utilities
{
	/// <summary>
	/// Provides ability to set the focus from the ViewModel through databinding on any UIElement.
	/// </summary>
	public static class FocusHelper
	{
		// Using a DependencyProperty as the backing store for IsFocused.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty IsFocusedProperty =
			DependencyProperty.RegisterAttached("IsFocused", typeof(bool), typeof(FocusHelper), new UIPropertyMetadata(false, null, CoerceIsFocusedPropertyCallback));

		public static bool GetIsFocused(DependencyObject obj)
		{
			return (bool)obj.GetValue(IsFocusedProperty);
		}

		public static void SetIsFocused(DependencyObject obj, bool value)
		{
			obj.SetValue(IsFocusedProperty, value);
		}

		/// <summary>
		/// Coerce value is used to set focus event if focus already exists
		/// </summary>
		/// <param name="d"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		private static object CoerceIsFocusedPropertyCallback(DependencyObject d, object value)
		{
			// Sets Focus only when 
			var uiElement = (UIElement)d;
			var setFocus = (bool)value;
			if (!uiElement.Focusable || !setFocus) return value;

			var canTouchClick = TouchClick.TouchClick.GetIsSoundEnabled(d);
			TouchClick.TouchClick.SetIsSoundEnabled(d, false);
			value = uiElement.Focus();
			TouchClick.TouchClick.SetIsSoundEnabled(d, canTouchClick);

			return value;
		}
	}
}
