﻿using System.Windows;

namespace Daxor.Lab.Infrastructure.Utilities
{
    public class StringContainer : FrameworkElement
    {
        public static readonly DependencyProperty ValueProperty =
         DependencyProperty.Register(
         "Value",
         typeof(string),
         typeof(StringContainer),
         new UIPropertyMetadata(string.Empty));

        public string Value
        {
            get { return (string)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
    }
}
