﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Infrastructure.Interfaces;
using Ionic.Zip;

namespace Daxor.Lab.Infrastructure.Utilities
{
    public class IonicZipWrapper : IIonicZipWrapper
    {
        #region Fields

        private readonly ZipFile _zipFile;
        protected bool HasZipFileBeenSaved;

        #endregion

        #region Ctor

        /// <summary>
        /// Creates a new IonicZipWrapper instance with no password
        /// </summary>
        public IonicZipWrapper()
        {
            _zipFile = new ZipFile();
        }

        /// <summary>
        /// Creates a new IonicZipWrapper instance configured with a given password
        /// </summary>
        /// <param name="password">Password for the zip file</param>
        /// <exception cref="ArgumentNullException">If the password is null</exception>
        /// <exception cref="ArgumentException">The password is null or whitespace</exception>
        public IonicZipWrapper(string password)
        {
            if (password == null)
                throw new ArgumentNullException("password");

            if (String.IsNullOrWhiteSpace(password))
                throw new ArgumentException("password cannot be whitespace");

            _zipFile = CreateConfiguredZipFile(password);
            Password = password;
        }

        #endregion

        #region Properties

        /// <summary>
        /// List of absolute file paths and names in zip file
        /// </summary>
        /// <exception cref="ObjectDisposedException">If the zip file has already been saved</exception>
        public List<string> Files
        {
            get
            {
                if (HasZipFileBeenSaved) throw new ObjectDisposedException("The zip file has already been saved");

                return _zipFile.Entries.Select(entry => entry.FileName).ToList();
            }
        }

        /// <summary>
        /// Password for the zip file
        /// </summary>
        /// <remarks>Passowrd can only be set upon construction</remarks>
        public string Password { get; private set; }

        #endregion

        #region Public APIs

        /// <summary>
        /// Adds a file to the zip folder in the root directory.
        /// </summary>
        /// <param name="filename">The full path for the file</param>
        /// <exception cref="ObjectDisposedException">If the zip file has already been saved</exception>
        public void AddFileToRoot(string filename)
        {
            if(filename == null) throw new ArgumentNullException("filename");
            if (HasZipFileBeenSaved) throw new ObjectDisposedException("The zip file has already been saved");

            AddFileToDirectory(filename, "\\");
        }

        /// <summary>
        /// Adds a file to the zip folder in the specified directory
        /// </summary>
        /// <param name="filename">Absolute path for the file</param>
        /// <param name="directory">Directory in the zip folder to add the file to</param>
        /// <remarks>If the directory doesn't exist in the zip folder, it will be created</remarks>
        /// <exception cref="ArgumentNullException">Thrown if filePath or directory is null is null</exception>
        /// <exception cref="ObjectDisposedException">If the zip file has already been saved</exception>
        public void AddFileToDirectory(string filename, string directory)
        {
            if (HasZipFileBeenSaved) throw new ObjectDisposedException("The zip file has already been saved");

            if (filename == "")
                throw new ArgumentException("filename cannot be empty");

            if (directory == null)
                throw new ArgumentNullException("directory");

            if (string.IsNullOrWhiteSpace(directory))
                throw new ArgumentException("directory cannot be whitespace");
            
            _zipFile.AddFile(filename, directory);
        }

        public void AddDirectory(string pathToDirectory, string pathToDirectoryInArchive)
        {
            if (HasZipFileBeenSaved) throw new ObjectDisposedException("The zip file has already been saved");

            if (string.IsNullOrWhiteSpace(pathToDirectoryInArchive))
                throw new ArgumentException("pathToDirectoryInArchive cannot be empty");

            if (pathToDirectory == null)
                throw new ArgumentNullException("pathToDirectory");

            if (string.IsNullOrWhiteSpace(pathToDirectory))
                throw new ArgumentException("directory cannot be whitespace");

            _zipFile.AddDirectory(pathToDirectory, pathToDirectoryInArchive);
        }

        /// <summary>
        /// Saves the zip file
        /// </summary>
        /// <param name="filename">Absolute file path to save the file to</param>
        /// <exception cref="InvalidOperationException">Thrown if no files have been added</exception>
        /// <exception cref="ArgumentNullException">Thrown if filePath is null</exception>
        /// <exception cref="ObjectDisposedException">If the zip file has already been saved</exception>
        public virtual void Save(string filename)
        {
            if (HasZipFileBeenSaved) throw new ObjectDisposedException("The zip file has already been saved");

            if (Files.Count == 0)
                throw new InvalidOperationException("Can't save a zip file with no files");
            if (filename == null)
                throw new ArgumentNullException("filename");

            _zipFile.Save(filename);
            HasZipFileBeenSaved = true;
        }

        /// <summary>
        /// Reads a zip file from disc into memory
        /// </summary>
        /// <param name="pathToZipfile">the full file path of the zip file to read into memory</param>
        /// <returns></returns>
        public virtual ZipFile Read(string pathToZipfile)
        {
            return ZipFile.Read(pathToZipfile);
        }

        /// <summary>
        /// Adds an event handler to the Ionic.Zip.SaveProgress event
        /// </summary>
        /// <param name="progressHandler">An event handle that handles the SaveProgress event</param>
        public virtual void AttachProgressHandlerToZip(EventHandler<SaveProgressEventArgs> progressHandler)
        {
            _zipFile.SaveProgress += progressHandler;
        }

        /// <summary>
        /// Adds a password to a zip file that doesn't already have one;
        /// </summary>
        /// <param name="password">the string that the password should be</param>
        public void AddPasswordToZip(string password)
        {
            _zipFile.Password = password;
            _zipFile.Encryption = EncryptionAlgorithm.PkzipWeak;
        }

        #endregion

        private static ZipFile CreateConfiguredZipFile(string password)
        {
            return new ZipFile { Password = password, Encryption = EncryptionAlgorithm.PkzipWeak };
        }
    }
}
