﻿using System;
using Daxor.Lab.Infrastructure.Interfaces;

namespace Daxor.Lab.Infrastructure.Utilities
{
    /// <summary>
    /// Contains a list of File Operations
    /// </summary>
    public static class SafeFileOperations
    {
        // ReSharper disable InconsistentNaming
        private static IFileManager _customFileManager;
        private static readonly SystemFileManager _systemFileManager = new SystemFileManager();
        // ReSharper restore InconsistentNaming

        public static IFileManager FileManager
        {
            get
            {
                // ReSharper disable ConvertIfStatementToNullCoalescingExpression
                if (_customFileManager == null)
                    _customFileManager = _systemFileManager;
                // ReSharper restore ConvertIfStatementToNullCoalescingExpression

                return _customFileManager;
            }
            set
            {
                _customFileManager = value;
            }
        }

        static public void Copy(string fromPath, string toPath)
        {
            if (!FileManager.FileExists(fromPath)) return;

            if (FileManager.FileExists(toPath))
                FileManager.RemoveReadOnlyProperty(toPath);

            FileManager.CopyFile(fromPath, toPath);
        }
        
        static public void Delete(string filePath)
        {
            FileManager.RemoveReadOnlyProperty(filePath);
            FileManager.DeleteFile(filePath);
        }

        static public void Delete(string directory, string wildCardString)
        {
            if(wildCardString == null) throw new ArgumentNullException("wildCardString");
            if (FileManager.DirectoryExists(directory))
                foreach (var info in FileManager.GetFilesInDirectory(directory, wildCardString))
                    FileManager.DeleteFile(info.FullName);
        }

        static public void DeleteDirectory(string directory)
        {
            if (FileManager.DirectoryExists(directory))
                FileManager.DeleteDirectory(directory);
        }
    }
}
