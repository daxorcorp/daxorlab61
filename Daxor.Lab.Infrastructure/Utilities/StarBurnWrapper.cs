﻿using Daxor.Lab.Infrastructure.Interfaces;
using RocketDivision.StarBurnX;

namespace Daxor.Lab.Infrastructure.Utilities
{
    public class StarBurnWrapper : IStarBurnWrapper
    {
        public DataBurner CreateDataBurner()
        {
            var starBurnX = new StarBurnX();
            starBurnX.Initialize(RocketDivisionKey.RocketDivisionKey.Key);
            return new DataBurner {Drive = starBurnX.Drives[0] };
        }
    }
}
