﻿using System;
using System.Text.RegularExpressions;

namespace Daxor.Lab.Infrastructure.Utilities
{
	public static class RegexHelper
	{
		public static bool IsValidRegex(string pattern)
		{
			if (String.IsNullOrEmpty(pattern)) return false;

			try
			{
			    // ReSharper disable once ReturnValueOfPureMethodIsNotUsed
				Regex.Match("", pattern);
			}
			catch (ArgumentException)
			{
				return false;
			}

			return true;
		}
	}
}