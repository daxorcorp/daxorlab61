﻿using System;
using Microsoft.Practices.Composite.Modularity;
using System.IO;
using System.Configuration;

namespace Daxor.Lab.Infrastructure.Utilities
{
	public class FilteredConfigurationStore : IConfigurationStore
	{
		private readonly string _sectionName;
		private readonly string _baseDirectory;

		/// <summary>
		/// Initializes a new instance of <see cref="ConfigurationStore"/>. It uses the Application's directory as the base for looking config files.
		/// </summary>
		public FilteredConfigurationStore(string sectionName)
			: this(sectionName, null)
		{
		}

		/// <summary>
		/// Initializes a new instance of <see cref="ConfigurationStore"/>.
		/// </summary>
		/// <param name="sectionName">Name of a section to be loaded</param>
		/// <param name="baseDirectory">The directory from which to start searching for the 
		/// configuration files.</param>
		public FilteredConfigurationStore(string sectionName, string baseDirectory)
		{
			_sectionName = sectionName;
			_baseDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, baseDirectory ?? string.Empty);
		}

		/// <summary>
		/// Gets the module configuration data.
		/// </summary>
		/// <returns>A <see cref="ModulesConfigurationSection"/> instance.</returns>
		public ModulesConfigurationSection RetrieveModuleConfigurationSection()
		{
			// ReSharper disable once LoopCanBeConvertedToQuery
			foreach (var fileName in Directory.GetFiles(_baseDirectory, "*.config", SearchOption.TopDirectoryOnly))
			{
				var configuration =
					GetConfiguration(Path.Combine(_baseDirectory, fileName));

				var section = (ModulesConfigurationSection)configuration.GetSection(_sectionName);

				if (section != null)
				{
					return section;
				}
			}

			return null;
		}

		private static Configuration GetConfiguration(string configFilePath)
		{
			var map = new ExeConfigurationFileMap { ExeConfigFilename = configFilePath };
			return ConfigurationManager.OpenMappedExeConfiguration(map, ConfigurationUserLevel.None);
		}
	}
}
