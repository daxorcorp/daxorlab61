﻿using System;
using System.Windows.Media.Animation;

namespace Daxor.Lab.Infrastructure.Utilities
{
    public class StoryboardEventHandlerWrapper<T>
    {
        private readonly T _state;
        private readonly Action<T> _callBack;
        private readonly Storyboard _str;

        public StoryboardEventHandlerWrapper(T state, Storyboard str, Action<T> callback = null)
        {
            _str = str;
            _state = state;
            _callBack = callback;
        }
        public void CustomHandler(object sender, EventArgs args)
        {
            _str.Completed -= CustomHandler;

            if (_callBack != null)
                _callBack(_state);
        }
    }
}
