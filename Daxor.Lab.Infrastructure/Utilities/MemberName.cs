﻿using System;
using System.Linq.Expressions;

namespace Daxor.Lab.Infrastructure.Utilities
{
	public static class MemberName
	{
		private static string GetMemberName(Expression expression)
		{
			switch (expression.NodeType)
			{
				case ExpressionType.MemberAccess:
					var memberExpression = (MemberExpression)expression;
					return memberExpression.Member.Name;

				case ExpressionType.Call:
					var callExpression = (MethodCallExpression)expression;
					return callExpression.Method.Name;

				case ExpressionType.Convert:
					var unaryExpression = (UnaryExpression)expression;
					return GetMemberName(unaryExpression.Operand);

				case ExpressionType.Constant:
				case ExpressionType.Parameter:
					return String.Empty;

				default:
					throw new ArgumentException("The expression is not a member access or method call expression");
			}
		}

		public static string AsString<T>(Expression<Func<T>> expression)
		{
			return GetMemberName(expression.Body);
		}

		public static string AsString(Expression<Action> expression)
		{
			return GetMemberName(expression.Body);
		}

		public static string AsString<T>()
		{
			return typeof(T).Name;
		}
	}
}