﻿namespace Daxor.Lab.Infrastructure.Utilities
{
    /// <summary>
    /// Utility class to convert height and weight values to/from metric and English units.
    /// </summary>
    public static class UnitsConversionHelper
    {
        #region Constants
        /// <summary>
        /// Number of kilograms per pound.
        /// </summary>
        private const double KilogramsPerPound = 0.45359237;

        /// <summary>
        /// Number of centimeters per inch.
        /// </summary>
        private const double CentimetersPerInch = 2.54;
        #endregion

        #region English to metric
        /// <summary>
        /// Converts a given height (in inches) to centimeters.
        /// </summary>
        /// <param name="heightInInches">Height in inches</param>
        /// <returns>Given height in centimeters</returns>
        public static double ConvertHeightToMetric(double heightInInches)
        {
            return heightInInches * CentimetersPerInch;
        }

        /// <summary>
        /// Converts a given weight (in pounds) to kilograms.
        /// </summary>
        /// <param name="weightInLb">Weight in pounds</param>
        /// <returns>Given weight in kilograms</returns>
        public static double ConvertWeightToMetric(double weightInLb)
        {
            return weightInLb * KilogramsPerPound;
        }
        #endregion

        #region Metric to English
        /// <summary>
        /// Converts a given height (in centimeters) to inches.
        /// </summary>
        /// <param name="heightInCm">Height in centimeters</param>
        /// <returns>Given height in inches</returns>
        public static double ConvertHeightToEnglish(double heightInCm)
        {
            return heightInCm / CentimetersPerInch;
        }

        /// <summary>
        /// Converts a given weight (in kilograms) to pounds.
        /// </summary>
        /// <param name="weightInKg">Weight in kilograms</param>
        /// <returns>Given weight in pounds</returns>
        public static double ConvertWeightToEnglish(double weightInKg)
        {
            return weightInKg / KilogramsPerPound;
        }
        #endregion
    }
}