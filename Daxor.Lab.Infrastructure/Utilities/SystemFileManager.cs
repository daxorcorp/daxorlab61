﻿using Daxor.Lab.Infrastructure.Interfaces;
using System.Collections.Generic;
using System.IO;

namespace Daxor.Lab.Infrastructure.Utilities
{
	public class SystemFileManager : IFileManager
	{
		public bool FileExists(string filePath)
		{
			return File.Exists(filePath);
		}

		public void DeleteFile(string filePath)
		{
			File.Delete(filePath);
		}

		public void CopyFile(string fromPath, string toPath)
		{
			File.Copy(fromPath, toPath, overwrite: true);
		}

		public void DeleteDirectory(string path)
		{
			Directory.Delete(path, recursive: true);
		}

		public bool DirectoryExists(string path)
		{
			return Directory.Exists(path);
		}

		public IEnumerable<FileInfo> GetFilesInDirectory(string path, string searchPattern)
		{
			return new DirectoryInfo(path).GetFiles(searchPattern);
		}

		public IEnumerable<DirectoryInfo> GetSubDirectoriesInDirectory(string path)
		{
			return new DirectoryInfo(path).GetDirectories();
		}
		
		public void RemoveReadOnlyProperty(string path)
		{
			var info = new FileInfo(path) { IsReadOnly = false };
			info.Refresh();
		}
	}
}
