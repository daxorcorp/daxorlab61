﻿using System.Windows;
using System.Windows.Media;

namespace Daxor.Lab.Infrastructure.Utilities
{
	public class VisualHelperEx
	{
		public static T FindVisualChildByName<T>(DependencyObject parent, string name) where T : DependencyObject
		{
			for (var i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
			{
				var child = VisualTreeHelper.GetChild(parent, i);
				var controlName = child.GetValue(FrameworkElement.NameProperty) as string;
				if (controlName == name)
				{
					return child as T;
				}
				var result = FindVisualChildByName<T>(child, name);
				if (result != null)
					return result;
			}
			return null;
		}

		public static TChildItem FindVisualChild<TChildItem>(DependencyObject obj) where TChildItem : DependencyObject
		{
			if (obj == null)
				return null;

			// Iterate through all immediate children
			for (var i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
			{
				var child = VisualTreeHelper.GetChild(obj, i);

				if (child is TChildItem)
					return (TChildItem) child;

				var childOfChild = FindVisualChild<TChildItem>(child);

				if (childOfChild != null)
					return childOfChild;
			}

			return null;
		}
	}
}
