﻿namespace Daxor.Lab.Infrastructure.Entities
{
    public enum TestSelectionDialogActions
    {
        OpenTest,
        DeleteTest,
    }
}