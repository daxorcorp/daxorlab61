﻿namespace Daxor.Lab.Infrastructure.Entities
{
    public enum DriveTypeEnum
    {
        Undefined = 0,
        Usb = 1,
        Optical = 2,
        Network = 3
    }
}
