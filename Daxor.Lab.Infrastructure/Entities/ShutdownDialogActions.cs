﻿namespace Daxor.Lab.Infrastructure.Entities
{
    public enum ShutdownDialogActions
    {
        CloseApplication,
        ShutdownSystem,
        RebootSystem
    }
}