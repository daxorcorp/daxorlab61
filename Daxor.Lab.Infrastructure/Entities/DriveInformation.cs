﻿namespace Daxor.Lab.Infrastructure.Entities
{
    public class DriveInformation
    {
        public string Name { get; set; }
        public DriveTypeEnum DriveType { get; set; }
    }
}
