﻿using System;
using System.Windows.Media;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Constants;

namespace Daxor.Lab.Infrastructure.SubViews
{
    /// <summary>
    /// Interaction logic for PatientComparisonSubView.xaml
    /// </summary>
    public partial class PatientComparisonSubView 
    {
        /// <summary>
        /// Creates a new PatientComparisonSubView instance and initializes it.
        /// </summary>
        public PatientComparisonSubView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Creates a new PatientComparisonSubView instance, initializes it, and sets the textboxes on the UI.
        /// </summary>
        /// <param name="intro">Introductory text</param>
        /// <param name="patientAHeader">Column header for patient A</param>
        /// <param name="patientBHeader">Column header for patient B</param>
        /// <param name="prompt">Prompt for the user describing various actions</param>
        /// <param name="patientA">Patient A</param>
        /// <param name="patientB">Patient B</param>
        public PatientComparisonSubView(string intro, string patientAHeader, string patientBHeader, string prompt,
                                        IPatient patientA, IPatient patientB) : this()
        {
            if (intro == null) throw new ArgumentNullException("intro");
            if (patientAHeader == null) throw new ArgumentNullException("patientAHeader");
            if (patientBHeader == null) throw new ArgumentNullException("patientBHeader");
            if (prompt == null) throw new ArgumentNullException("prompt");
            if (patientA == null) throw new ArgumentNullException("patientA");
            if (patientB == null) throw new ArgumentNullException("patientB");

            uiIntro.Text = intro;
            uiPatientAHeader.Text = patientAHeader;
            uiPatientBHeader.Text = patientBHeader;
            uiPrompt.Text = prompt;

            uiPatientAPatientId.Text = FormatStringIfEmpty(patientA.HospitalPatientId);
            uiPatientALastName.Text = FormatStringIfEmpty(patientA.LastName);
            uiPatientAFirstName.Text = FormatStringIfEmpty(patientA.FirstName);
            uiPatientAMiddleName.Text = FormatStringIfEmpty(patientA.MiddleName);
            uiPatientAGender.Text = patientA.Gender.ToString();
            uiPatientADateOfBirth.Text = FormatDateOfBirthString(patientA.DateOfBirth);
	        var patientAAmputeeStatus = FormatAmputeeStatus(patientA.IsAmputee);
	        uiPatientAAmputeeStatus.Text = patientAAmputeeStatus;

            uiPatientBPatientId.Text = FormatStringIfEmpty(patientB.HospitalPatientId);
            uiPatientBLastName.Text = FormatStringIfEmpty(patientB.LastName);
            uiPatientBFirstName.Text = FormatStringIfEmpty(patientB.FirstName);
            uiPatientBMiddleName.Text = FormatStringIfEmpty(patientB.MiddleName);
            uiPatientBGender.Text = patientB.Gender.ToString();
            uiPatientBDateOfBirth.Text = FormatDateOfBirthString(patientB.DateOfBirth);
			var patientBAmputeeStatus = FormatAmputeeStatus(patientB.IsAmputee);
	        uiPatientBAmputeeStatus.Text = patientBAmputeeStatus;

            uiPatientAPatientId.Foreground = uiPatientBPatientId.Foreground = ConvertStringMismatchToForeground(patientA.HospitalPatientId, patientB.HospitalPatientId);
            uiPatientALastName.Foreground = uiPatientBLastName.Foreground = ConvertStringMismatchToForeground(patientA.LastName, patientB.LastName);
            uiPatientAFirstName.Foreground = uiPatientBFirstName.Foreground = ConvertStringMismatchToForeground(patientA.FirstName, patientB.FirstName);
            uiPatientAMiddleName.Foreground = uiPatientBMiddleName.Foreground = ConvertStringMismatchToForeground(patientA.MiddleName, patientB.MiddleName);
            uiPatientAGender.Foreground = uiPatientBGender.Foreground = ConvertStringMismatchToForeground(uiPatientAGender.Text, uiPatientBGender.Text);
            uiPatientADateOfBirth.Foreground = uiPatientBDateOfBirth.Foreground = ConvertStringMismatchToForeground(uiPatientADateOfBirth.Text, uiPatientBDateOfBirth.Text);
	        uiPatientAAmputeeStatus.Foreground = uiPatientBAmputeeStatus.Foreground = ConvertStringMismatchToForeground(patientAAmputeeStatus, patientBAmputeeStatus);
        }

        /// <summary>
        /// Converts string equality into a colored brush (red for mismatch, black otherwise).
        /// </summary>
        /// <param name="string1">First string</param>
        /// <param name="string2">Second string</param>
        /// <returns>Red for mismatch, black otherwise</returns>
        private static Brush ConvertStringMismatchToForeground(string string1, string string2)
        {
            return string1 != string2 ? new SolidColorBrush(Color.FromRgb(255, 0, 0)) : new SolidColorBrush(Color.FromRgb(0, 0, 0));
        }

        /// <summary>
        /// Formats a given date of birth in MM-dd-yyyy format. If the given value is empty or
        /// invalid, appropriate string representations are returned.
        /// </summary>
        /// <param name="dateOfBirth">Date to format</param>
        /// <returns>MM-dd-yyyy formatted date; (Empty) for null values,
        /// (Invalid) for invalid values</returns>
        private static string FormatDateOfBirthString(DateTime? dateOfBirth)
        {
            var value = dateOfBirth.HasValue ? dateOfBirth.Value.Date.ToString("MM-dd-yyyy") : "(Empty)";

            if (dateOfBirth.HasValue && dateOfBirth.Value == DomainConstants.InvalidDateOfBirth)
                value = "(Invalid)";

            return value;
        }

        /// <summary>
        /// Formats names to allow for empty values.
        /// </summary>
        /// <param name="name">name to format</param>
        /// <returns>Given name unless it is null or whitespace, in which case (Empty) is returned</returns>
        private static string FormatStringIfEmpty(string name)
        {
            return String.IsNullOrWhiteSpace(name) ? "(Empty)" : name;
        }

		/// <summary>
		/// Formats a boolean amputee status into a longer string format
		/// </summary>
		/// <param name="isAmputee">Whether or not the patient is an amputee</param>
		/// <returns>"Amputee" or "Non-amputee"</returns>
	    private static string FormatAmputeeStatus(bool isAmputee)
	    {
		    return isAmputee ? "Amputee" : "Non-amputee";
	    }
    }
}
