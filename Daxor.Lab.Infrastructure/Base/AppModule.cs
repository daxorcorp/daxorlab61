﻿using System;
using System.Reflection;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.Infrastructure.Base
{
	/// <summary>
	/// Module that exposes some application-specific metadata (name, display order, etc.)
	/// and that can be a member of an module catalog.
	/// </summary>
	public abstract class AppModule : IModule
	{
		#region Ctor

		/// <summary>
		/// Creates a new AppModule instance based on the given ID and IoC container.
		/// </summary>
		/// <param name="uniqueAppId">Unique ID for the application module</param>
		/// <param name="unityContainer">IoC container for resolving objects</param>
		/// <exception cref="ArgumentNullException">If any arguments are null</exception>
		protected AppModule(string uniqueAppId, IUnityContainer unityContainer)
		{
			ContractHelper.ArgumentNotNull(uniqueAppId, "uniqueAppId");
			ContractHelper.ArgumentNotNull(unityContainer, "unityContainer");

			UnityContainer = unityContainer;

			AppModuleInfoCatalog = UnityContainer.Resolve<IAppModuleInfoCatalog>();
			Logger = UnityContainer.Resolve<ILoggerFacade>();
			RegionManager = UnityContainer.Resolve<IRegionManager>();
			UniqueAppId = uniqueAppId;
		}

		#endregion

		#region IModule

		/// <summary>
		/// Initializes the application module by deferring to InitializeAppModule(),
		/// then adding the module information to the info catalog. (If initialization
		/// was unsuccessful, the app module is flagged as disabled.)
		/// </summary>
		public virtual void Initialize()
		{
			try
			{
				InitializeAppModule();

				if ((ModuleInfo != null) && (AppModuleInfoCatalog != null))
					AppModuleInfoCatalog.Items.Add(ModuleInfo);
			}
			catch (Exception ex)
			{
				Logger.Log(String.Format("Failed to initialize the {0} module. Stack trace: {1} ", UniqueAppId, ex.StackTrace), 
					Category.Exception, Priority.High);

				if ((ModuleInfo != null) && (AppModuleInfoCatalog != null))
				{
					var moduleInfo = ModuleInfo;
					moduleInfo.IsEnabled = false;
					AppModuleInfoCatalog.Items.Add(moduleInfo);
				}

				if (ex is ModuleLoadException)
					throw;
			}
		}

		#endregion

		#region Methods

		/// <summary>
		/// Performs application-module specific initialization
		/// </summary>
		public abstract void InitializeAppModule();

		#endregion

		#region Properties 

		/// <summary>
		/// Gets or sets the application module's info catalog
		/// </summary>
		protected IAppModuleInfoCatalog AppModuleInfoCatalog { get; private set; }

		/// <summary>
		/// Gets the application module's assembly version (major.minor) 
		/// </summary>
		protected string AssemblyVersion
		{
			get
			{
				var ver = Assembly.GetExecutingAssembly().GetName().Version;
				return String.Format("Version {0}.{1}", ver.Major, ver.Minor);
			}
		}

		/// <summary>
		/// Gets or sets the application module's build date
		/// </summary>
		protected DateTime BuildDate { get; set; }

		/// <summary>
		/// Gets or sets the application's module display order (for UI positioning)
		/// </summary>
		public int DisplayOrder { get; protected set; }

		/// <summary>
		/// Gets or sets the application module's full/long display name
		/// </summary>
		public string FullDisplayName { get; protected set; }

		/// <summary>
		/// Gets or sets whether this application is configurable through settings
		/// </summary>
		public bool IsConfigurable { get; protected set; }

		/// <summary>
		/// Gets or sets the application module's logger
		/// </summary>
		protected ILoggerFacade Logger { get; private set; }

		/// <summary>
		/// Gets the application's module information (used by the app module info catalog)
		/// </summary>
		protected virtual IAppModuleInfo ModuleInfo
		{
			get
			{
				var mInfo = UnityContainer.Resolve<AppModuleInfo>(); // same as "new"
				mInfo.AppModuleKey = UniqueAppId;
				mInfo.BuildDate = BuildDate;
				mInfo.DisplayOrder = DisplayOrder;
				mInfo.FullDisplayName = FullDisplayName;
				mInfo.IsConfigurable = IsConfigurable;
				mInfo.ShortDisplayName = ShortDisplayName;

				return mInfo;
			}
		}
		
		/// <summary>
		/// Gets or sets the application module's UI region manager
		/// </summary>
		protected IRegionManager RegionManager { get; private set; }

		/// <summary>
		/// Gets or sets the application module's short display name
		/// </summary>
		public string ShortDisplayName { get; protected set; }

		/// <summary>
		/// Gets or sets the application module's identifier
		/// </summary>
		protected string UniqueAppId { get; private set; }

		/// <summary>
		/// Gets or sets the application module's IoC container instance
		/// </summary>
		protected IUnityContainer UnityContainer { get; private set; }

		#endregion
	}
}