﻿using System.ComponentModel;
using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.Infrastructure.Base
{
    /// <summary>
    /// Abstract BusinessEntityBase class used by all models.
    /// </summary>
    public abstract class ValidatableObject : ObservableObject, IDataErrorInfo
    {
        public string Error
        {
            get { return string.Empty; }
        }

        public string this[string columnName]
        {
            get { return string.Empty; }
        }
    }
}
