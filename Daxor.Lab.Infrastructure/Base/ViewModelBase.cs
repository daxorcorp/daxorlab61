﻿using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.Infrastructure.Base
{
    /// <summary>
    /// Base class for all ViewModel classes in the application.
    /// It provides support for property change notifications 
    /// and has a DisplayName property.  This class is abstract.
    /// </summary>
    public abstract class ViewModelBase : ObservableObject
    {
        #region Fields

        string _displayName;

        #endregion

        #region Ctor

        protected ViewModelBase(){}
    
        #endregion // Constructor

        #region DisplayName

        /// <summary>
        /// Returns the user-friendly name of this object.
        /// Child classes can set this property to a new value,
        /// or override it to determine the value on-demand.
        /// </summary>
        public virtual string DisplayName
        {
            get {return _displayName; }
            protected set { base.SetValue(ref _displayName, "DisplayName", value); }
        }

        #endregion
    }
}
