﻿using System;

namespace Daxor.Lab.Infrastructure.RuleFramework.Rules
{
	public class UserRequiredRule<TEntity> : Domain.Evaluation.BasicRule<TEntity> where TEntity : Domain.Evaluation.EvaluatableObject
	{
		public UserRequiredRule(string propertyName, string errorMessage, Func<TEntity, Boolean> validator) :
			base(propertyName, errorMessage, validator)
		{
				SeverityKind = RuleSeverity.UserRequired.ToString();
				IsEnabled = false;
		}
		public UserRequiredRule(string propertyName, string errorMessage, Func<TEntity, Boolean> validator, string ruleId) :
			this(propertyName, errorMessage, validator)
		{
			RuleId = ruleId;
		}

		public UserRequiredRule(string propertyName, string errorMessage, Func<TEntity, Boolean> validator, string ruleId, Boolean isEnabled = true) :
			this(propertyName, errorMessage, validator)
		{
			IsEnabled = isEnabled;
			RuleId = ruleId;
		}
	}
}