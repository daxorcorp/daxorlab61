﻿using System;

namespace Daxor.Lab.Infrastructure.RuleFramework.Rules
{
	/// <summary>
	/// Sytem value required rule
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public class SystemRequiredRule<TEntity> : Domain.Evaluation.BasicRule<TEntity> where TEntity : Domain.Evaluation.EvaluatableObject
	{
		public SystemRequiredRule(string propertyName, string errorMessage, Func<TEntity, bool> validator, string ruleId = null) :
			base(propertyName, errorMessage, validator)
		{
			SeverityKind = RuleSeverity.SystemRequired.ToString();
			RuleId = ruleId ?? Guid.NewGuid().ToString();
		}
	}
}
