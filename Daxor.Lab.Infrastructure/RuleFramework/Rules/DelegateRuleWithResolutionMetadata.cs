﻿using System;

namespace Daxor.Lab.Infrastructure.RuleFramework.Rules
{
	/// <summary>
	/// Represents a delegate (alert) rule that exposes metadata about how the alert can
	/// be resolved
	/// </summary>
	/// <typeparam name="TEntity">Entity with which the rule is associated</typeparam>
	public class DelegateRuleWithResolutionMetadata<TEntity> : Domain.Evaluation.BasicRule<TEntity>, IExposeAlertResolutionMetadata where TEntity : Domain.Evaluation.EvaluatableObject
	{
		/// <summary>
		/// Creates a new DelegateRuleWithResolutionMetadata instance based on the given rule ID,
		/// property associated with the rule, and a delegate capable of evaluating the rule.
		/// </summary>
		/// <param name="ruleId">Rule ID for the rule</param>
		/// <param name="propertyName">Class property associated with the rule</param>
		/// <param name="evaluate">Delegate capable of evaluating the rule</param>
		/// <remarks>
		/// The description of the base rule is the rule ID upon instantiation. The
		/// overridden Description property uses the metadata if available.
		/// </remarks>
		public DelegateRuleWithResolutionMetadata(string ruleId, string propertyName, Func<TEntity, bool> evaluate)
			: base(propertyName, String.Empty, evaluate)
		{
			RuleId = ruleId;
			SeverityKind = RuleSeverity.Alert.ToString();
		}

		#region Properties

		/// <summary>
		/// Gets or sets the alert resolution metadata
		/// </summary>
		AlertResolutionMetadata IExposeAlertResolutionMetadata.Metadata { get; set; }

		/// <summary>
		/// Gets or sets the description of this rule. If there is metadata that contains
		/// a description, that description will be returned instead.
		/// </summary>
		public override string BrokenRuleMessage
		{
			get
			{
				IExposeAlertResolutionMetadata exposerInstance = this;
				return exposerInstance.Metadata == null ? base.BrokenRuleMessage : exposerInstance.Metadata.ErrorMessage;
			}
			set
			{
				base.BrokenRuleMessage = value;
			}
		}

		#endregion
	}
}
