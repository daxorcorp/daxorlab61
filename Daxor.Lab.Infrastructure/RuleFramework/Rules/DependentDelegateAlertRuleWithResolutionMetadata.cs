﻿using System;

namespace Daxor.Lab.Infrastructure.RuleFramework.Rules
{
	/// <summary>
	/// Represents a delegate (alert) rule that depends on an observable object and also
	/// exposes metadata about how the alert can be resolved
	/// </summary>
	/// <typeparam name="TEntity">Entity with which the rule is associated</typeparam>
	/// <typeparam name="TDependency">Dependency used by the delegate</typeparam>
	public class DependentDelegateAlertRuleWithResolutionMetadata<TEntity, TDependency> : 
		DependentRule<TEntity, TDependency>, IExposeAlertResolutionMetadata
		where TEntity : Domain.Evaluation.EvaluatableObject
		where TDependency : class
	{
		/// <summary>
		/// Creates a new DependentDelegateAlertRuleWithResolutionMetadata instance based on
		/// the given rule ID, property associated with the rule, and a delegate capable of
		/// evaluating the rule.
		/// </summary>
		/// <param name="ruleId">Rule ID for the rule</param>
		/// <param name="propertyName">Class property associated with the rule</param>
		/// <param name="cDelegate">Delegate capable of evaluating the rule</param>
		/// <remarks>
		/// The error header and problem description of the base rule are String.Empty upon 
		/// instantiation. The overridden Description property uses the metadata if available.
		/// Also, this rule does not need the event aggregator, so null is passed to the
		/// base constructor.
		/// </remarks>
		public DependentDelegateAlertRuleWithResolutionMetadata(string ruleId, string propertyName,
			Func<TEntity, TDependency, Boolean> cDelegate)
			: base(propertyName, String.Empty, cDelegate)
		{
			RuleId = ruleId;
			SeverityKind = RuleSeverity.Alert.ToString();
		}

		#region Properties

		/// <summary>
		/// Gets or sets the alert resolution metadata
		/// </summary>
		AlertResolutionMetadata IExposeAlertResolutionMetadata.Metadata { get; set; }

		/// <summary>
		/// Gets or sets the description of this rule. If there is metadata that contains
		/// a description, that description will be returned instead.
		/// </summary>
		public override string BrokenRuleMessage
		{
			get
			{
				IExposeAlertResolutionMetadata exposerInstance = this;
				return exposerInstance.Metadata == null ? base.BrokenRuleMessage : exposerInstance.Metadata.ErrorMessage;
			}
			set
			{
				base.BrokenRuleMessage = value;
			}
		}

		#endregion
	}
}
