﻿namespace Daxor.Lab.Infrastructure.RuleFramework.Rules
{
	/// <summary>
	/// Metadata pertaining to an alert that can aid in the resolution of
	/// the alert by the user
	/// </summary>
	public class AlertResolutionMetadata
	{
		/// <summary>
		/// Creates a new AlertResolutionMetadata instance based on the given description,
		/// resolution, and resolution type.
		/// </summary>
		/// <param name="alertId"></param>
		/// <param name="errorMessage">Error message of the alert</param>
		/// <param name="resolution">Resolution of the alert</param>
		/// <param name="resolutionType">Type of resolution (e.g., string, video)</param>
		public AlertResolutionMetadata(string alertId, string errorMessage, string resolution, string resolutionType)
		{
			AlertId = alertId;
			ErrorMessage = errorMessage;
			Resolution = resolution;
			ResolutionType = resolutionType;
		}

		/// <summary>
		/// Key: AlertId of the problem that triggered the alert
		/// </summary>
		public string AlertId { get; set; }

		/// <summary>
		/// Description of the problem that triggered the alert
		/// </summary>
		public string ErrorMessage { get; set; }

		/// <summary>
		/// Information about how the alert can be resolved
		/// </summary>
		public string Resolution { get; set; }

		/// <summary>
		/// Type of resolution (e.g., string, video) as a string
		/// </summary>
		public string ResolutionType { get; set; }

		/// <summary>
		/// Type of resolution (e.g., string, video) as the corresponding enumeration value
		/// </summary>
		public RuleViolationMetadata.HelpSourceType ResolutionTypeAsEnum
		{
			get
			{
				switch (ResolutionType)
				{
					case "video": return RuleViolationMetadata.HelpSourceType.Video;
					case "website": return RuleViolationMetadata.HelpSourceType.WebSite;
					default: return RuleViolationMetadata.HelpSourceType.Text; 
				}
			}
		}
	}
}
