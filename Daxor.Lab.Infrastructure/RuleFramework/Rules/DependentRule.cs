﻿using System;

namespace Daxor.Lab.Infrastructure.RuleFramework.Rules
{
	public class DependentRule<TEntity, TDependency> : Domain.Evaluation.BasicRule<TEntity>, IDependableRule<TDependency> 
		where TEntity : Domain.Evaluation.EvaluatableObject
		where TDependency : class
	{
		/// <summary>
		/// Complex rule func
		/// </summary>
		public Func<TEntity, TDependency, Boolean> ComplexDelegate { get; private set; }

		public DependentRule(string propertyName, string errorMessage,
			Func<TEntity, TDependency, Boolean> cDelegate)
			: base(propertyName, errorMessage)
		{
			ComplexDelegate = cDelegate;
		}

		public override bool Evaluate(object model)
		{
			if (ComplexDelegate == null)
				return true;

			var result = ComplexDelegate((TEntity)model, Dependency);

			return result;
		}

		#region IDependableRule<TDependency> Members

		public TDependency Dependency { get; set; }

		#endregion
	}
}
