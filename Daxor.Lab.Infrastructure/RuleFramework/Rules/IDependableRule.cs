﻿namespace Daxor.Lab.Infrastructure.RuleFramework.Rules
{
    /// <summary>
    /// If a rule is dependent on outside entity
    /// </summary>
    /// <typeparam name="TDependecy"></typeparam>
    public interface IDependableRule<TDependecy> where TDependecy : class
    {
        TDependecy Dependency { get; set; }
    }
}
