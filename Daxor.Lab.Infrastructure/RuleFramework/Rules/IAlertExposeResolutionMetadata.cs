﻿namespace Daxor.Lab.Infrastructure.RuleFramework.Rules
{
    /// <summary>
    /// Defines a property that exposes (alert) resolution metadata
    /// </summary>
    public interface IExposeAlertResolutionMetadata
    {
        AlertResolutionMetadata Metadata { get; set; }
    }
}
