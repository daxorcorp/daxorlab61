﻿namespace Daxor.Lab.Infrastructure.RuleFramework
{
    /// <summary>
    /// Rule severity levels
    /// </summary>
    public enum RuleSeverity
    {
        SystemRequired,
        CalculationRequired,
        Alert,
        UserRequired,
        Unknown,
    }
}
