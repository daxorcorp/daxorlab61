﻿using System;
using System.Windows.Media;

namespace Daxor.Lab.Infrastructure.RuleFramework
{
    public struct RuleViolationMetadata
    {
        public VisualBrush VisualBrush { get; set; }
        public double VisualHeight { get; set; }
        public double VisualWidth { get; set; }

        public string PropertyName { get; set; }
        public string Symptoms { get; set; }
        public string Cause { get; set; }

        public Object Resolution { get; set; }
        public HelpSourceType HelpType { get; set; }

        public enum HelpSourceType
        {
            Text = 0, Video = 1, WebSite = 2
        }
        public override string ToString()
        {
            return Symptoms;
        }
    }
}
