﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Daxor.Lab.Infrastructure.RuleFramework.Rules;

namespace Daxor.Lab.Infrastructure.RuleFramework
{
	public static class ValidationHelper
	{
		#region ReadOnlyAttached Property - Will throw exception if binding two way

		private static readonly DependencyPropertyKey HasAlertViolationPropertyKey = DependencyProperty.RegisterAttachedReadOnly("HasAlertViolation", typeof(Boolean), typeof(ValidationHelper),
			new PropertyMetadata(false));
		public static readonly DependencyProperty HasAlertViolationProperty = HasAlertViolationPropertyKey.DependencyProperty;
		public static bool GetHasAlertViolation(DependencyObject obj)
		{
			return (bool)obj.GetValue(HasAlertViolationProperty);
		}
		private static void SetHasAlertViolation(DependencyObject obj, bool value)
		{
			obj.SetValue(HasAlertViolationPropertyKey, value);
		}

		
		
		private static readonly DependencyPropertyKey FirstBrokenRuleSeverityPropertyKey = DependencyProperty.RegisterAttachedReadOnly("FirstBrokenRuleSeverity", typeof(RuleSeverity), typeof(ValidationHelper),
		  new PropertyMetadata(RuleSeverity.Unknown));
		public static readonly DependencyProperty FirstBrokenRuleSeverityProperty = FirstBrokenRuleSeverityPropertyKey.DependencyProperty;
		public static RuleSeverity GetFirstBrokenRuleSeverity(DependencyObject obj)
		{
			return (RuleSeverity)obj.GetValue(FirstBrokenRuleSeverityProperty);
		}
		private static void SetFirstBrokenRuleSeverity(DependencyObject obj, RuleSeverity value)
		{
			obj.SetValue(FirstBrokenRuleSeverityPropertyKey, value);
		}


		private static readonly DependencyPropertyKey BrokenRuleIdPropertyKey = DependencyProperty.RegisterAttachedReadOnly("BrokenRuleID", typeof(String), typeof(ValidationHelper), new PropertyMetadata(null));

		// ReSharper disable InconsistentNaming
		public static readonly DependencyProperty BrokenRuleIDProperty = BrokenRuleIdPropertyKey.DependencyProperty;
		public static String GetBrokenRuleID(DependencyObject obj)
		{
			return (String)obj.GetValue(BrokenRuleIDProperty);
		}
		private static void SetBrokenRuleID(DependencyObject obj, String value)
		{
			obj.SetValue(BrokenRuleIdPropertyKey, value);
		}
		// ReSharper restore InconsistentNaming


		private static readonly DependencyPropertyKey BrokenRuleMetadataPropertyKey = DependencyProperty.RegisterAttachedReadOnly("BrokenRuleMetadata", typeof(AlertResolutionMetadata), typeof(ValidationHelper),
		 new PropertyMetadata(null));
		public static readonly DependencyProperty BrokenRuleMetadataProperty = BrokenRuleMetadataPropertyKey.DependencyProperty;
		public static AlertResolutionMetadata GetBrokenRuleMetadata(DependencyObject obj)
		{
			return (AlertResolutionMetadata)obj.GetValue(BrokenRuleMetadataProperty);
		}
		private static void SetBrokenRuleMetadata(DependencyObject obj, AlertResolutionMetadata value)
		{
			obj.SetValue(BrokenRuleMetadataPropertyKey, value);
		}
		#endregion

		public static string GetValidationProperty(DependencyObject obj)
		{
			return (string)obj.GetValue(ValidationPropertyProperty);
		}
		public static void SetValidationProperty(DependencyObject obj, string value)
		{
			obj.SetValue(ValidationPropertyProperty, value);
		}

		// Using a DependencyProperty as the backing store for ValidationProperty.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ValidationPropertyProperty =
			DependencyProperty.RegisterAttached("ValidationProperty", typeof(string), typeof(ValidationHelper), new UIPropertyMetadata(String.Empty));

		public static object GetValidationTarget(DependencyObject obj)
		{
			return obj.GetValue(ValidationTargetProperty);
		}
		public static void SetValidationTarget(DependencyObject obj, object value)
		{
			obj.SetValue(ValidationTargetProperty, value);
		}

		// Using a DependencyProperty as the backing store for ValidationTarget.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ValidationTargetProperty =
			DependencyProperty.RegisterAttached("ValidationTarget", typeof(object), typeof(ValidationHelper),
			new UIPropertyMetadata(null, ValidationTargetChanged));

		private static void ValidationTargetChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
		{
			//Tries to set first broken rule and adds handler for subsequent calls
			TrySetFirstBrokenRule(o);
			Validation.AddErrorHandler(o, EvaluateErrors);
		}

		private static void EvaluateErrors(object sender, ValidationErrorEventArgs args)
		{
			TrySetFirstBrokenRule(sender as DependencyObject);
		}

		private static void TrySetFirstBrokenRule(DependencyObject control)
		{
			var vEntity = GetValidationTarget(control) as Domain.Evaluation.EvaluatableObject;
			var validatingProperties = GetValidationProperty(control).Split('|');
			if (vEntity == null)
				return;

			//Pulling broken rule from multiple properties on the same entity
			var firstBrokenRule = vEntity.BrokenRules.Where(e => validatingProperties.Contains(e.PropertyName)).Select(e => e).FirstOrDefault();
			if (firstBrokenRule == null)
			{
				SetHasAlertViolation(control, false);
				return;
			}

			//Figure out the actual rule severity
			RuleSeverity rSeverity;
			SetFirstBrokenRuleSeverity(control,
				Enum.TryParse(firstBrokenRule.SeverityKind, out rSeverity) ? rSeverity : RuleSeverity.Unknown);

			//Sets alert violation and ruleID
			SetHasAlertViolation(control, rSeverity == RuleSeverity.Alert);
			SetBrokenRuleID(control, firstBrokenRule.RuleId);
			SetBrokenRuleMetadata(control, (firstBrokenRule as IExposeAlertResolutionMetadata) != null ? ((IExposeAlertResolutionMetadata)firstBrokenRule).Metadata : null);
		}
	}
}
