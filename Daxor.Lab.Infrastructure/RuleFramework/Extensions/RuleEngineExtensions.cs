﻿using System.Linq;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.RuleFramework.Rules;

namespace Daxor.Lab.Infrastructure.RuleFramework.Extensions
{
	/// <summary>
	/// Extension methods on the rule engine
	/// </summary>
	public static class RulesEngineExtensions
	{
		public static void InjectRuleDependency<TDepency>(this IRuleEngine ruleEngine, TDepency dependency) where TDepency : class
		{
			if (!ruleEngine.Rules.Any()) return;

			foreach (var dependableRule in ruleEngine.Rules.OfType<IDependableRule<TDepency>>())
			{
				dependableRule.Dependency = dependency;
			}
		}
	}
}
