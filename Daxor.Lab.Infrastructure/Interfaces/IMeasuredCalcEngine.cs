﻿using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.Infrastructure.Interfaces
{
	public interface IMeasuredCalcEngineService
	{
		MeasuredCalcEngineResults GetAllResults(MeasuredCalcEngineParams parms);
		UnadjustedBloodVolumeResult GetOnlyUbvResult(int refVolume, double antiCouglant, double standardCounts, int background, double controlCounts, BloodVolumePoint p);
	}
}
