﻿using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.Infrastructure.Interfaces
{
    /// <summary>
    /// Exposes a method that generates a lot number (string) based on a given Pharmaceutical
    /// instance
    /// </summary>
    public interface ILotNumberGenerator
    {
        /// <summary>
        /// Constructs a lot number for the given Pharmaceutical
        /// </summary>
        /// <param name="pharmaceutical">
        /// Pharmaceutical on which the returned lot number will be based
        /// </param>
        /// <returns>
        /// Lot number based for the given Pharmaceutical
        /// </returns>
        string Generate(Pharmaceutical pharmaceutical);
    }
}
