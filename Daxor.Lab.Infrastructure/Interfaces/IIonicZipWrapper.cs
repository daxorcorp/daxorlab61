﻿using System;
using System.Collections.Generic;
using Ionic.Zip;

namespace Daxor.Lab.Infrastructure.Interfaces
{
    public interface IIonicZipWrapper
    {
        /// <summary>
        /// List of absolute file paths and names in zip file
        /// </summary>
        List<string> Files { get; }
        
        /// <summary>
        /// Password for the zip file
        /// </summary>
        /// <remarks>Passowrd can only be set upon construction</remarks>
        string Password { get; }

        /// <summary>
        /// Adds a file to the zip folder in the root directory.
        /// </summary>
        /// <param name="filename">The full path for the file</param>
        void AddFileToRoot(string filename);

        /// <summary>
        /// Adds a file to the zip folder in the specified directory
        /// </summary>
        /// <param name="filename">Absolute path for the file</param>
        /// <param name="directory">Directory in the zip folder to add the file to</param>
        /// <remarks>If the directory doesn't exist in the zip folder, it will be created</remarks>
        /// <exception cref="ArgumentNullException">Thrown if filePath or directory is null is null</exception>
        void AddFileToDirectory(string filename, string directory);

        /// <summary>
        /// Saves the zip file
        /// </summary>
        /// <param name="filename">Absolute file path to save the file to</param>
        void Save(string filename);

        /// <summary>
        /// Reads a zip file from disc into memory
        /// </summary>
        /// <param name="pathToZipfile">the full file path of the zip file to read into memory</param>
        /// <returns></returns>
        ZipFile Read(string pathToZipfile);

        /// <summary>
        /// Attaches an event handler to the Ionic.Zip.SaveProgress event
        /// </summary>
        /// <param name="progressHandler">An event handler that handler the save progress event</param>
        void AttachProgressHandlerToZip(EventHandler<SaveProgressEventArgs> progressHandler);

        /// <summary>
        /// Adds an entire directory to an archive at a named location
        /// </summary>
        /// <param name="pathToDirectory">The path to the directory on disk</param>
        /// <param name="pathToDirectoryInArchive">The path to the location that the directory will be created in the archive</param>
        void AddDirectory(string pathToDirectory, string pathToDirectoryInArchive);

        /// <summary>
        /// Adds a password to a zip file that doesn't already have one;
        /// </summary>
        /// <param name="password">the string that the password should be</param>
        void AddPasswordToZip(string password);
    }
}
