﻿using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.Infrastructure.Interfaces
{
    public interface IIdealsCalcEngineService
    {
        /// <summary>
        /// Returns ideal TBV, PV, RCV and Weight Deviation
        /// </summary>
        /// <param name="weightKg">Weight in Kg</param>
        /// <param name="heightKg">Height in cm</param>
        /// <param name="gender">Gender of the patient(1 : male, 0 : female, -1 unknown)</param>
        /// <returns>Ideal results(IBV, IPV, IRBC and weight deviation)</returns>
        IdealsCalcEngineResults GetIdeals(double weightKg, double heightKg, int gender);

        /// <summary>
        /// Returns minimum valid heigh in cm
        /// </summary>
        double MinimumHeightInCm { get; }

        /// <summary>
        /// Returns maximum valid heigh in cm
        /// </summary>
        double MaximumHeightInCm { get; }

        /// <summary>
        /// Returns minimum valid weight in cm
        /// </summary>
        double MinimumWeightInKg { get; }


        /// <summary>
        /// Returns maximum valid weight in kg
        /// </summary>
        double MaximumWeightInKg { get; }

        double MaximumWeightDeviation { get; }

        double MinimumWeightDeviation { get; }

    }
}
