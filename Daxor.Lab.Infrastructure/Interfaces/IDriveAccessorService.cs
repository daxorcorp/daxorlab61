﻿using Daxor.Lab.Infrastructure.Entities;
using System.Collections.Generic;

namespace Daxor.Lab.Infrastructure.Interfaces
{
    public interface IDriveAccessorService
    {
        List<DriveInformation> GetAvailableDrives();
    }
}