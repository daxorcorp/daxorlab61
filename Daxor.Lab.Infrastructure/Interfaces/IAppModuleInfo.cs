﻿using System;
using System.Windows.Input;

namespace Daxor.Lab.Infrastructure.Interfaces
{
    public interface IAppModuleInfo
    {
        string AppModuleKey { get; }
        string FullDisplayName { get; set; }
        string ShortDisplayName { get; set; }
        string Version { get; set; }

        int DisplayOrder { get; }
        bool IsEnabled { get; set; }
        bool IsCurrentlyActive { get; set; }
        bool IsConfigurable { get; set; }
        
        DateTime BuildDate { get; set; }
        ICommand OpenAppModuleCommand { get; set; }
    }
}
