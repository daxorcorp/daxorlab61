﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Infrastructure.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        //Select
        IEnumerable<TEntity> SelectAll();
        TEntity Select(Guid id);

        //Update or Insert
        void Save(TEntity dto);

        //Remove
        void Delete(Guid id);
    }
    public interface ISampleRepository<TEntity> where TEntity : class
    {
        //Select
        IEnumerable<TEntity> SelectAll(Guid testId);

        TEntity Select(Guid id, ILoggerFacade logger);

        //Update or Insert
        void Save(TEntity dto, ILoggerFacade logger);

        //Remove
        void Delete(Guid id, ILoggerFacade logger);
    }
    public interface ITestInfoRepository<TEntity> where TEntity : class
    {
        //Select
        IEnumerable<TEntity> SelectAll();
    }
}
