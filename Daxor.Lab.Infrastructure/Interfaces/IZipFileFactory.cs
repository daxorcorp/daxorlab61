﻿namespace Daxor.Lab.Infrastructure.Interfaces
{
    public interface IZipFileFactory
    {
        IIonicZipWrapper CreateZipFile();
        IIonicZipWrapper CreateZipFileWithPassword(string password);
    }
}
