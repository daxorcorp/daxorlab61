﻿namespace Daxor.Lab.Infrastructure.Interfaces
{
    public interface IFolderBrowser
    {
        bool IsPathValid();

        bool IsOpticalDrive { get; set; }

        string CurrentPath { get; set; }
    }
}
