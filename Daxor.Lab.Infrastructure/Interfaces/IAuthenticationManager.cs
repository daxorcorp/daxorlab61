﻿using System;
using System.ComponentModel;
using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.Infrastructure.Interfaces
{
    public interface IAuthenticationManager : INotifyPropertyChanged
    {
        event EventHandler LoginFailed;
        event EventHandler LoginSucceeded;
        event EventHandler LogoutCompleted;

        User LoggedInUser { get; }
        bool Login(AuthorizationLevel requestedLevel, string password);
        void Logout();
    }
}
