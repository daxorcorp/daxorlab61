﻿namespace Daxor.Lab.Infrastructure.Interfaces
{
    public interface IWorkProgress
    {
        int TotalTaskTimeInSeconds { get; }
        int PercentageCompleted { get; }

        object ProgressContext { get; }
    }
}
