﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Base;
using System.Windows;

namespace Daxor.Lab.Infrastructure.Interfaces
{
    public interface IModalView
    {
        event RoutedEventHandler Loaded;
        event EventHandler Closing;

        Object State { get; set; }

        IEnumerable<String> Choices { get; }
        Int32 SelectedChoice { get; set; }
    }

    public interface IModalMessageBoxService
    {
        void Show<T>(Guid msgKey, Action<T> callback) where T : ViewModelBase;
        void Show<T>(Guid msgKey, T instanceToEdit, Action<T> callback) where T : ViewModelBase;

        void Register(Guid msgKey, Func<IModalView> builder);
    }
}
