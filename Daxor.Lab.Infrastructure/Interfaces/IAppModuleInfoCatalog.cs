﻿using System.Collections.ObjectModel;

namespace Daxor.Lab.Infrastructure.Interfaces
{
    /// <summary>
    /// Defines the behavior of a catalog of IAppModuleInfo instances
    /// </summary>
    public interface IAppModuleInfoCatalog
    {
        /// <summary>
        /// Gets the observable collection of IAppModuleInfo instances
        /// </summary>
        ObservableCollection<IAppModuleInfo> Items { get; }
    }
}
