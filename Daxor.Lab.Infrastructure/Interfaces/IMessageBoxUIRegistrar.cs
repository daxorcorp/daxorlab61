﻿using System;

namespace Daxor.Lab.Infrastructure.Interfaces
{
	public interface IMessageBoxUiRegistrar
	{
		IMessageBoxUiRegistrar RegisterUi(Guid viewId, Func<IModalView> viewBuilder);
		IModalView GenerateUi(Guid viewId);
	}
}
