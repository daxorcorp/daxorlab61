﻿//-----------------------------------------------------------------------
// <copyright file="IMessageBoxDispatcher.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Daxor.Lab.Infrastructure.DomainModels;
using System;

namespace Daxor.Lab.Infrastructure.Interfaces
{
	/// <summary>
	/// Handles the showing of a message box with specified options for the user to choose.
	/// </summary>
	public interface IMessageBoxDispatcher
	{
		/// <summary>
		/// Displays a message box of a specified category with a specified title, message, and
		/// a list of strings corresponding to options for the user to choose.
		/// </summary>
		/// <param name="category">One of the MessageBoxCategory values that categorizes the
		/// information to be displayed in the message box</param>
		/// <param name="messageContent">Content of the message</param>
		/// <param name="caption">The text to display as the title of the message box</param>
		/// <param name="choiceTexts">The strings corresponding to the choices presented to 
		/// the user</param>
		/// <returns>Index of the list item corresponding to the option chosen by the user</returns>
		int ShowMessageBox(MessageBoxCategory category, object messageContent, string caption, IEnumerable<string> choiceTexts);

		/// <summary>
		/// Displays a message box of a specified category with a specified title, message, and
		/// a predefined set of choices from which the user can choose.
		/// </summary>
		/// <param name="category">One of the MessageBoxCategory values that categorizes the
		/// information to be displayed in the message box</param>
		/// <param name="messageContent">Content of the message</param>
		/// <param name="caption">The text to display as the title of the message box</param>
		/// <param name="choiceSet">One of the MessageBoxChoiceSet values that defines the choices
		/// presented to the user</param>
		/// <returns>MessageBoxReturnResult corresponding to the choice made by the user</returns>
		MessageBoxReturnResult ShowMessageBox(MessageBoxCategory category, object messageContent, string caption, MessageBoxChoiceSet choiceSet);


		/// <summary>
		/// Displays a message box of a specified category with a specified title, message, and
		/// a predefined set of choices from which the user can choose.
		/// </summary>
		/// <param name="category">One of the MessageBoxCategory values that categorizes the
		/// information to be displayed in the message box</param>
		/// <param name="messageContent">Content of the message</param>
		/// <param name="caption">The text to display as the title of the message box</param>
		/// <param name="choiceSet">One of the MessageBoxChoiceSet values that defines the choices
		/// presented to the user</param>
		/// <param name="msgContentDataContext">Represents message content data context, if one existed</param>
		/// <returns>MessageBoxReturnResult corresponding to the choice made by the user</returns>
		MessageBoxReturnResult ShowMessageBox(MessageBoxCategory category, object messageContent, string caption, MessageBoxChoiceSet choiceSet, out object msgContentDataContext);
		MessageBoxReturnResult ShowMessageBox(MessageBoxCategory category, object messageContent, string caption, MessageBoxChoiceSet choiceSet, Action onClosing);
		MessageBoxReturnResult ShowMessageBox(MessageBoxCategory category, Guid viewId, string caption);

		int ShowMessageBox(MessageBoxCategory category, object messageContent, string caption, IEnumerable<string> choiceTexts, out object msgContentDataContext);

		/// <summary>
		/// Edited by ASK. Testing purposes only
		/// </summary>
		/// <typeparam name="T">The state of the msg box (DataContext)</typeparam>
		/// <param name="category"></param>
		/// <param name="caption"></param>
		/// <param name="modalViewId"></param>
		/// <param name="onClose"></param>
		void ShowMessageBox<T>(MessageBoxCategory category, string caption, Guid modalViewId, Action<IModalView, T> onClose) where T : class;
		void ShowMessageBox<T>(MessageBoxCategory category, string caption, Guid modalViewId, T instanceToBind, Action<IModalView, T> onClose) where T : class;
	}
}
