﻿using System.Runtime.InteropServices;
using RocketDivision.StarBurnX;

namespace Daxor.Lab.Infrastructure.Interfaces
{
    /// <summary>
    /// Defines the behavior of an entity that can provide a wrapper for
    /// working with StarBurn classes.
    /// </summary>
    public interface IStarBurnWrapper 
    {
        /// <summary>
        /// Creates a DataBurner instance that provides access to creating optical media
        /// whose Drive instance is pointed to the first optical drive on the computer
        /// </summary>
        /// <returns>DataBurner instance that communicates with StarBurn</returns>
        /// <exception cref="COMException">The first drive cannot be obtained from StarBurn</exception>
        DataBurner CreateDataBurner();
    }
}
