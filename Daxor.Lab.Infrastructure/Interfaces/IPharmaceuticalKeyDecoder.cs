﻿using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.Infrastructure.Interfaces
{

    /// <summary>
    /// Exposes a method that decodes a given pharmaceutical key (string) and represents it as a
    /// Pharmaceutical instance
    /// </summary>
    public interface IPharmaceuticalKeyDecoder
    {
        /// <summary>
        /// Decodes a given pharmaceutical key string
        /// </summary>
        /// <param name="pharmaceuticalKey">
        /// Pharmaceutical key to decode
        /// </param>
        /// <returns>
        /// Pharmaceutical instance represented by the given pharmaceutical key string
        /// </returns>
        Pharmaceutical Decode(string pharmaceuticalKey);
    }
}
