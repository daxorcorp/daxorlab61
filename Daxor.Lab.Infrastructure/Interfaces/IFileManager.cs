﻿using System.Collections.Generic;
using System.IO;

namespace Daxor.Lab.Infrastructure.Interfaces
{
    public interface IFileManager
    {
        bool FileExists(string filePath);
        void DeleteFile(string filePath);
        void CopyFile(string fromPath, string toPath);
        void DeleteDirectory(string path);
        bool DirectoryExists(string path);
        void RemoveReadOnlyProperty(string path);
        IEnumerable<FileInfo> GetFilesInDirectory(string path, string searchPattern);
	    IEnumerable<DirectoryInfo> GetSubDirectoriesInDirectory(string path);
    }
}
