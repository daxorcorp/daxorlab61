﻿namespace Daxor.Lab.Infrastructure.Constants
{
    public static class RegionNames
    {
        public const string WorkspaceRegion = "WorkspaceRegion";
        public const string HeaderRegion = "HeaderRegion";
        public const string FooterRegion = "FooterRegion";
        public const string KeyboardRegion = "KeyboardRegion";
        public const string OverlayRegion = "OverlayRegion";
        public const string ReportRegion = "ReportRegion";
    }
}
