﻿namespace Daxor.Lab.Infrastructure.Constants
{
	public static class ApplicationParts
	{
		public const string MainMenuDropDown = "MainMenuDropDownPART";
		public const string ShutdownButton = "ShutdownButtonPART";
		public const string Footer = "FooterPART";
		public const string Privacy = "PrivacyPART";
	}
}
