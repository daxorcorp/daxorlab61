﻿using System;

namespace Daxor.Lab.Infrastructure.Constants
{
	public static class DomainConstants
	{
		public const int MaxPostInjectionTime = 12;

		public const string NullDateTime = "1/1/1753";

		/// <summary>
		/// Special date representing invalid input from the user.
		/// </summary>
		public static readonly DateTime InvalidDateOfBirth = new DateTime(1, 1, 1);

		/// <summary>
		/// Imperative verb associated with excluding a sample
		/// </summary>
		public const string SampleExclude = "Exclude";

		/// <summary>
		/// Imperative verb associated with including a sample
		/// </summary>
		public const string SampleInclude = "Include";

		/// <summary>
		/// Daxor lab database password.
		/// </summary>
		public static readonly string DaxorDatabasePassword = "daxor";
	}
}
