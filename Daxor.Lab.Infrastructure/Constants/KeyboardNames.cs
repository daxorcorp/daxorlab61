﻿namespace Daxor.Lab.Infrastructure.Constants
{
    public static class KeyboardNames
    {
        public const string FullKeyboard            = "FullKeyboard";
        public const string FullSearchKeyboard      = "FullSearchKeyboard";
        public const string CountsKeyboard          = "CountsKeyboard";
        public const string BasicNumericKeyboard    = "BasicNumericKeyboard";
        public const string MesurementKeyboard      = "NumericMesurementKeyboard";
        public const string BackgroundKeyboard      = "BackgroundKeyboard";
        public const string DurationKeyboard        = "DurationKeyboard";
        public const string DateOfBirthKeyboard     = "DateOfBirthKeyboard";
        public const string HematocritKeyboard      = "HematocritKeyboard";
        public const string LoginKeyboard           = "LoginKeyboard";
    }
}
