﻿namespace Daxor.Lab.Infrastructure.Constants
{
	/// <summary>
	/// Application identifiers used for uniquely identifying Prism modules.
	/// </summary>
	public static class AppModuleIds
	{
		/// <summary>
		/// Blood volume analysis
		/// </summary>
		public const string BloodVolumeAnalysis = "BVA";

		/// <summary>
		/// Help
		/// </summary>
		public const string Help = "HELP";

		/// <summary>
		/// Main menu
		/// </summary>
		public const string MainMenu = "MAINMENU";
		
		/// <summary>
		/// Quality control
		/// </summary>
		public const string QualityControl = "QC";

		/// <summary>
		/// Utilities
		/// </summary>
		public const string Utilities = "UTILITIES";
	}
}
