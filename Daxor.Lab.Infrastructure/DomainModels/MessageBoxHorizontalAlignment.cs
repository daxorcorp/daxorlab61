﻿//-----------------------------------------------------------------------
// <copyright file="MessageBoxHorizontalAlignment.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Daxor.Lab.Infrastructure.DomainModels
{
    /// <summary>
    /// Specifies constants defining the horizontal alignment of message box buttons
    /// when using one of the MessageBoxChoiceSet-type dialogs.
    /// </summary>
    public enum MessageBoxHorizontalAlignment
    {
        /// <summary>
        /// The horizontal alignment is undefined.
        /// </summary>
        Undefined = 0,
        /// <summary>
        /// The buttons should be left-aligned.
        /// </summary>
        Left = 1,
        /// <summary>
        /// The buttons should be centered.
        /// </summary>
        Center = 2,
        /// <summary>
        /// The buttons should be right-aligned.
        /// </summary>
        Right = 3,
    }
}
