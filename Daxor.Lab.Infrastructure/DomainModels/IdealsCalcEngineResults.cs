﻿namespace Daxor.Lab.Infrastructure.DomainModels
{
    public class IdealsCalcEngineResults
    {
        public IdealsCalcEngineResults(int bloodVolume, int plasmaVolume, int redCellVolume, double weightDeviation)
        {
            IdealBloodVolume = bloodVolume;
            IdealPlasmaVolume = plasmaVolume;
            IdealRedCellVolume = redCellVolume;
            WeightDeviation = weightDeviation;
        }

        public int IdealBloodVolume { get; private set; }
        public int IdealPlasmaVolume { get; private set; }
        public int IdealRedCellVolume { get; private set; }
        public double WeightDeviation { get; private set; }
    }
}
