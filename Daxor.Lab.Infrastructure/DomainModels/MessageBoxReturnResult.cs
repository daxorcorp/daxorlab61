﻿namespace Daxor.Lab.Infrastructure.DomainModels
{
	/// <summary>
	/// Specifies constants defining which set of choices to display on an IMessageBox
	/// </summary>
	public enum MessageBoxReturnResult
	{
		/// <summary>
		/// The dialog box return value is undefined/none.
		/// </summary>
		None = 0,

		/// <summary>
		/// The dialog box return value is OK.
		/// </summary>
		Ok = 1,

		/// <summary>
		/// The dialog box return value is Close.
		/// </summary>
		Close = 2,

		/// <summary>
		/// The dialog box return value is Cancel.
		/// </summary>
		Cancel = 3,

		/// <summary>
		/// The dialog box return value is Yes.
		/// </summary>
		Yes = 4,

		/// <summary>
		/// The dialog box return value is No.
		/// </summary>
		No = 5,

		/// <summary>
		/// The dialog box return value is Start.
		/// </summary>
		Start = 6,

		/// <summary>
		/// The dialog box return value is Return.
		/// </summary>
		Return = 7,
	}
}
