﻿namespace Daxor.Lab.Infrastructure.DomainModels
{
    /// <summary>
    /// Authorization level for a specific resource; i.e. setting changed, test mode change, etc.
    /// </summary>
    public enum AuthorizationLevel
    {
        Undefined = -1,
        CustomerAdmin = 2,
        Service = 3,
        Internal = 4,
        ReadOnly = 5
    }

    /// <summary>
    /// Represents a user that is allowed to changed settings, types of test, etc.
    /// </summary>
    public class User
    {
        #region Declaration

        private readonly AuthorizationLevel _aLevel;

        #endregion

        #region Ctor

        public User(AuthorizationLevel aLevel, string passcode)
        {
            _aLevel = aLevel;
            Passcode = passcode;
        }

        #endregion

        #region Properties

        public AuthorizationLevel AuthLevel
        {
            get { return _aLevel; }
        }
        public string UserName
        {
            get
            {
                string uName;
                switch (AuthLevel)
                {
                    case AuthorizationLevel.CustomerAdmin: uName = "customer"; break;
                    case AuthorizationLevel.Service: uName = "service"; break;
                    case AuthorizationLevel.Internal: uName = "Internal"; break;
                    default: uName = "WHO_ARE_YOU"; break;
                }

                return uName;
            }
        }
        public string Passcode
        {
            get;
            private set;
        }

        #endregion

        #region StaticConverters

        public static AuthorizationLevel Int32ToAuthLevel(int authorizationLevel)
        {
            AuthorizationLevel authLevel;
            switch (authorizationLevel)
            {
                case 5: authLevel = AuthorizationLevel.ReadOnly; break;
                case 4: authLevel = AuthorizationLevel.Internal; break;
                case 3: authLevel = AuthorizationLevel.Service; break;
                case 2: authLevel = AuthorizationLevel.CustomerAdmin; break;
                default: authLevel = AuthorizationLevel.Undefined; break;
            }
            return authLevel;
        }

        #endregion
    }
}
