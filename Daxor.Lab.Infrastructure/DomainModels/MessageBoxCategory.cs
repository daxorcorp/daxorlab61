﻿//-----------------------------------------------------------------------
// <copyright file="MessageBoxCategory.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Daxor.Lab.Infrastructure.DomainModels
{
    /// <summary>
    /// Specifies constants defining the category of an IMessageBox
    /// </summary>
    public enum MessageBoxCategory
    {
        /// <summary>
        /// The message box category is undefined.
        /// </summary>
        Undefined = 0,
        /// <summary>
        /// The message box contains general information.
        /// </summary>
        Information = 1,
        /// <summary>
        /// The message box contains a question/prompt.
        /// </summary>
        Question = 2,
        /// <summary>
        /// The message box contains a warning message.
        /// </summary>
        Warning = 3,
        /// <summary>
        /// The message box contains an error message.
        /// </summary>
        Error = 4,
    }
}
