﻿namespace Daxor.Lab.Infrastructure.DomainModels
{
    public class ExportDrive
    {
        public string Content { get; set; }
        public object Tag { get; set; }
        public bool IsUsb { get; set; }
        public bool IsOpticalDrive { get; set; }

        public override string ToString()
        {
            return Content;
        }
    }
}
