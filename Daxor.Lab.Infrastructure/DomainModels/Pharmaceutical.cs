﻿using System.Globalization;
using System;
using System.Text;

namespace Daxor.Lab.Infrastructure.DomainModels
{
	/// <summary>
	/// Represents a basic pharmaceutical with a date of manufacture, batch ID, serialization ID, 
	/// manufacturer, and type (in this case, standard or injectate).
	/// </summary>
	public class Pharmaceutical
	{
		#region Constants

		/// <summary>
		/// Special value for the batch ID representing an undefined state
		/// </summary>
		public static readonly int UndefinedBatchId = -1;

		/// <summary>
		/// Special value for the serialization ID representing an undefined state
		/// </summary>
		public static readonly int UndefinedSerializationId = -1;

        #endregion

		#region Member variables

		/// <summary>
		/// Private member variable for the date of manufacture.
		/// </summary>
		private DateTime _dateOfManufacture;
		
		/// <summary>
		/// Private member variable for the batch ID
		/// </summary>
		private int _batchId;

		/// <summary>
		/// Private member variable for the serialization ID
		/// </summary>
		private int _serializationId;

		/// <summary>
		/// Private member variable for the manufacturer
		/// </summary>
		private PharmaceuticalManufacturer _manufacturer;

		/// <summary>
		/// Private member variable for the pharmaceutical type
		/// </summary>
		private PharmaceuticalType _type;

		/// <summary>
		/// Private member variable for the pharmaceutical brand
		/// </summary>
		private PharmaceuticalBrand _brand;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the Pharmaceutical class with default values (e.g., date
		/// of manufacture = 1/1/0000, batch ID = UndefinedBatchID, serialization ID = 
		/// UndefinedSerializationID, manufacturer = PharmaceuticalManufacturer.Undefined, 
		/// type = PharmaceuticalType.Undefined, brand = PharmaceuticalBrand.Undefined)
		/// </summary>
		public Pharmaceutical()
		{
			// NOTE: The member variables themselves are accessed instead of using the properties
			//       so that the undefined states can be stored. (The properties throw exceptions
			//       if you try to store such states.)
			_dateOfManufacture = new DateTime();
			_batchId = UndefinedBatchId;
			_serializationId = UndefinedSerializationId;
			_manufacturer = PharmaceuticalManufacturer.Undefined;
			_type = PharmaceuticalType.Undefined;
			_brand = PharmaceuticalBrand.Undefined;
		}

		/// <summary>
		/// Initializes a new instance of the Pharmaceutical class with instance values based on 
		/// the given parameters.
		/// </summary>
		/// <param name="dateOfManufacture">
		/// A date of manufacture value
		/// </param>
		/// <param name="batchId">
		/// A batch ID value
		/// </param>
		/// <param name="serializationId">
		/// A serialization ID value
		/// </param>
		/// <param name="manufacturer">
		/// One of the Pharmaceutical.PharmaceuticalManufacturer values
		/// </param>
		/// <param name="type">
		/// One of the Pharmaceutical.PharmaceuticalType value
		/// s</param>
		/// <param name="brand">
		/// One of the Pharmaceutical.PharmaceuticalBrand values
		/// </param>
		/// <exception cref="ArgumentOutOfRangeException">
		/// Given batch ID is out of range
		/// </exception>
		/// <exception cref="ArgumentOutOfRangeException">
		/// Given serialization ID is out of range
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Given manufacturer is not a member of the PharmaceuticalManufacturer enumeration
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Given type is not a member of the PharmaceuticalType enumeration
		/// </exception>
		/// <exception cref="ArgumentException">
		/// Given brand is not a member of the PharmaceuticalBrand enumeration
		/// </exception>
		public Pharmaceutical(
			DateTime dateOfManufacture, 
			int batchId, 
			int serializationId,
			PharmaceuticalManufacturer manufacturer, 
			PharmaceuticalType type,
			PharmaceuticalBrand brand)
		{
			DateOfManufacture = dateOfManufacture;
			BatchId = batchId;
			SerializationId = serializationId;
			Manufacturer = manufacturer;
			Type = type;
			Brand = brand;
		}

		#endregion

		#region Enums

		/// <summary>
		/// Specifies constants defining which company manufactures the pharmaceutical
		/// </summary>
		/// <remarks>
		/// This enumeration is not called "Manufacturer" because there's already a property with
		/// that name. Although it's awkward to have the name of the class in the enum name, this
		/// seems to be the best solution.
		/// </remarks>
		public enum PharmaceuticalManufacturer
		{
			/// <summary>
			/// Manufacturer is unknown
			/// </summary>
			Undefined,

			/// <summary>
			/// Iso-Tex Diagnostics
			/// </summary>
			IsoTex,

			/// <summary>
			/// DAXOR Corporation
			/// </summary>
			Daxor
		}

		/// <summary>
		/// Specifies constants defining the kind of product for the pharmaceutical
		/// </summary>
		/// <remarks>
		/// This enumeration is not called "Type" because there's already a property with that 
		/// name. Although it's awkward to have the name of the class in the enum name, this seems 
		/// to be the best solution.
		/// </remarks>
		public enum PharmaceuticalType
		{
			/// <summary>
			/// Type is not known
			/// </summary>
			Undefined,

			/// <summary>
			/// Injectable pharmaceutical
			/// </summary>
			Injectate,

			/// <summary>
			/// Pharmaceutical standard
			/// </summary>
			Standard
		}

		/// <summary>
		/// Specifies constants defining the brand for the pharmaceutical
		/// </summary>
		/// <remarks>
		/// This enumeration is not called "Brand" because there's already a property with that 
		/// name. Although it's awkward to have the name of the class in the enum name, this seems 
		/// to be the best solution.
		/// </remarks>
		public enum PharmaceuticalBrand
		{
			/// <summary>
			/// Brand is unknown
			/// </summary>
			Undefined,

			/// <summary>
			/// Volumex brand pharmaceutical
			/// </summary>
			Volumex,

			/// <summary>
			/// DAXOR-branded radiopharmaceutical
			/// </summary>
			Volumidine
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the date of manufacture in the current Pharmaceutical object
		/// </summary>
		public DateTime DateOfManufacture
		{
			get
			{
				return _dateOfManufacture;
			}

			set
			{
				SetDateOfManufacture(value);
			}
		}

		/// <summary>
		/// Gets or sets the batch ID in the current Pharmaceutical object
		/// </summary>
		/// <exception cref="ArgumentOutOfRangeException">
		/// Value to set the batch ID is a number less than zero
		/// </exception>
		public int BatchId
		{
			get
			{
				return _batchId;
			}

			set
			{
				SetBatchId(value);
			}
		}

		/// <summary>
		/// Gets or sets the serialization ID in the current Pharmaceutical object
		/// </summary>
		/// <exception cref="ArgumentOutOfRangeException">
		/// Value to set the serialization ID is a number less than zero or greater than 
		/// MaxSerializationID
		/// </exception>
		public int SerializationId
		{
			get
			{
				return _serializationId;
			}
		   
			set
			{
				SetSerializationId(value);
			}
		}

		/// <summary>
		/// Gets or sets the manufacturer in the current Pharmaceutical object
		/// </summary>
		/// <exception cref="ArgumentException">
		/// Value to set the manufacturer is not a member of 
		/// Pharmaceutical.PharmaceuticalManufacturer
		/// </exception>
		public PharmaceuticalManufacturer Manufacturer
		{
			get
			{
				return _manufacturer;
			}
			
			set
			{
				SetManufacturer(value);
			}
		}

		/// <summary>
		/// Gets or sets the pharmaceutical type in the current Pharmaceutical object
		/// </summary>
		/// <exception cref="ArgumentException">
		/// Value to set the type is not a member of Pharmaceutical.PharmaceuticalType
		/// </exception>
		public PharmaceuticalType Type
		{
			get
			{
				return _type;
			}
			
			set
			{
				SetType(value);
			}
		}

		/// <summary>
		/// Gets or sets the pharmaceutical brand in the current Pharmaceutical object
		/// </summary>
		/// <exception cref="ArgumentException">
		/// Value to set the brand is not a member of Pharmaceutical.PharmaceuticalBrand
		/// </exception>
		public PharmaceuticalBrand Brand
		{
			get
			{
				return _brand;
			}

			set
			{
				SetBrand(value);
			}
		}

		#endregion

		#region Has* bool methods

		/// <summary>
		/// Determines if the date of manufacture for this Pharmaceutical instance has been defined 
		/// (i.e., is not the default value of 1/1/0000)
		/// </summary>
		/// <returns>
		/// true if this instance has a defined date of manufacture; false otherwise
		/// </returns>
		public bool HasDateOfManufacture()
		{
			return DateOfManufacture.CompareTo(new DateTime()) != 0;
		}

		/// <summary>
		/// Determines if the batch ID for this Pharmaceutical instance has been defined (i.e., is 
		/// not UndefinedBatchID)
		/// </summary>
		/// <returns>
		/// true if this instance has a defined batch ID; false otherwise
		/// </returns>
		public bool HasBatchId()
		{
			return BatchId != UndefinedBatchId; 
		}

        /// <summary>
		/// Determines if the serialization ID for this Pharmaceutical instance has been defined 
		/// (i.e., is not UndefinedSerializationID)
		/// </summary>
		/// <returns>
		/// true if this instance has a defined serialization ID; false otherwise
		/// </returns>
		public bool HasSerializationId()
		{
			return SerializationId != UndefinedSerializationId;
		}

		/// <summary>
		/// Determines if the manufacturer for this Pharmaceutical instance has been defined (i.e.,
		/// is not PharmaceuticalManufacturer.Undefined)
		/// </summary>
		/// <returns>
		/// true if this instance has a defined manufacturer; false otherwise
		/// </returns>
		public bool HasManufacturer()
		{
			return Manufacturer != PharmaceuticalManufacturer.Undefined;
		}

		/// <summary>
		/// Determines if the type for this Pharmaceutical instance has been defined (i.e., is not 
		/// PharmaceuticalType.Undefined)
		/// </summary>
		/// <returns>
		/// true if this instance has a defined type; false otherwise
		/// </returns>
		public bool HasType()
		{
			return Type != PharmaceuticalType.Undefined;
		}

		/// <summary>
		/// Determines if the brand for this Pharmaceutical instance has been defined (i.e., is not 
		/// PharmaceuticalBrand.Undefined)
		/// </summary>
		/// <returns>
		/// true if this instance has a defined brand; false otherwise
		/// </returns>
		public bool HasBrand()
		{
			return Brand != PharmaceuticalBrand.Undefined;
		}

		#endregion

		#region ToString()
		/// <summary>
		/// Overloaded. Converts the value of this instance to String.
		/// </summary>
		/// <returns>
		/// A string containing values for the date of manufacture, batch ID, serialization ID, 
		/// manufacturer, type, and brand
		/// </returns>
		public override string ToString()
		{
			var productString = new StringBuilder();
			productString.Append("Date of Manufacture: ");
			productString.Append(DateOfManufacture.ToShortDateString());
			productString.Append("; Batch ID: ");
			productString.Append(BatchId.ToString(CultureInfo.InvariantCulture));
			productString.Append("; Serialization ID: ");
			productString.Append(SerializationId.ToString(CultureInfo.InvariantCulture));
			productString.Append("; Manufacturer: ");
			productString.Append(Enum.GetName(typeof(PharmaceuticalManufacturer), Manufacturer));
			productString.Append("; Type: ");
			productString.Append(Enum.GetName(typeof(PharmaceuticalType), Type));
			productString.Append("; Brand: ");
			productString.Append(Enum.GetName(typeof(PharmaceuticalBrand), Brand));
			return productString.ToString();
		}
		#endregion

		#region Helper methods used by properties

		/// <summary>
		/// Sets the date of manufacture member variable for this Pharmaceutical instance
		/// </summary>
		/// <param name="value">
		/// A date value
		/// </param>
		/// <exception cref="ArgumentException">
		/// Value to set the type is the default DateTime value
		/// </exception>
		/// <remarks>
		/// This method is used internally by the DateOfManufacture property.
		/// </remarks>
		private void SetDateOfManufacture(DateTime value)
		{
			if (value.ToString(CultureInfo.InvariantCulture) == new DateTime().ToString(CultureInfo.InvariantCulture))
			{
				throw new ArgumentOutOfRangeException(
					"value", 
					"Date of manufacturing cannot be the default DateTime");
			}

			_dateOfManufacture = value;
		}

		/// <summary>
		/// Sets the batch ID member variable for this Pharmaceutical instance
		/// </summary>
		/// <param name="value">
		/// A batch ID value
		/// </param>
		/// <exception cref="ArgumentException">
		/// Value to set the type is negative
		/// </exception>
		/// <remarks>
		/// This method is used internally by the BatchID property.
		/// </remarks>
		private void SetBatchId(int value)
		{
			if (value < 0)
			{
				throw new ArgumentOutOfRangeException("value", "Batch ID cannot be negative");
			}

			_batchId = value;
		}

		/// <summary>
		/// Sets the serialization ID member variable for this Pharmaceutical instance
		/// </summary>
		/// <param name="value">
		/// A serialization ID value
		/// </param>
		/// <exception cref="ArgumentOutOfRangeException">
		/// Value to set the type is negative
		/// </exception>
		/// <remarks>
		/// This method is used internally by the SerializationID property.
		/// </remarks>
		private void SetSerializationId(int value)
		{
			if (value < 0)
			{
				throw new ArgumentOutOfRangeException("value", "Serialization ID cannot be negative");
			}

			_serializationId = value;
		}

		/// <summary>
		/// Sets the manufacturer member variable for this Pharmaceutical instance
		/// </summary>
		/// <param name="value">
		/// One of the Pharmaceutical.PharmaceuticalManufacturer values
		/// </param>
		/// <exception cref="ArgumentException">
		/// Value to set the type is not a member of Pharmaceutical.PharmaceuticalManufacturer
		/// </exception>
		/// <exception cref="InvalidOperationException">
		/// Value to set the type is Pharmaceutical.PharmaceuticalManufacturer.Undefined
		/// </exception>
		/// <remarks>
		/// This method is used internally by the Manufacturer property.
		/// </remarks>
		private void SetManufacturer(PharmaceuticalManufacturer value)
		{
			if (!Enum.IsDefined(typeof(PharmaceuticalManufacturer), value))
			{
				string message = "Given manufacturer value '" + value.ToString() +
					"' is not a member of enumeration PharmaceuticalManufacturer";
				throw new ArgumentException("value", message);
			}

			if (value == PharmaceuticalManufacturer.Undefined)
			{
				throw new InvalidOperationException(
					"Manufacturer cannot explicitly be set to Undefined");
			}

			_manufacturer = value;
		}

		/// <summary>
		/// Sets the type member variable for this Pharmaceutical instance
		/// </summary>
		/// <param name="value">
		/// One of the Pharmaceutical.PharmaceuticalType values
		/// </param>
		/// <exception cref="ArgumentException">
		/// Value to set the type is not a member of Pharmaceutical.PharmaceuticalType
		/// </exception>
		/// <exception cref="InvalidOperationException">
		/// Value to set the type is Pharmaceutical.PharmaceuticalManufacturer.Undefined
		/// </exception>
		/// <remarks>
		/// This method is used internally by the Type property.
		/// </remarks>
		private void SetType(PharmaceuticalType value)
		{
			if (!Enum.IsDefined(typeof(PharmaceuticalType), value))
			{
				string message = "Given type value '" + value.ToString() +
					"' is not a member of enumeration PharmaceuticalType";
				throw new ArgumentException("value", message);
			}

			if (value == PharmaceuticalType.Undefined)
			{
				throw new InvalidOperationException(
					"Type cannot explicitly be set to Undefined");
			}

			_type = value;
		}

		/// <summary>
		/// Sets the brand member variable for this Pharmaceutical instance
		/// </summary>
		/// <param name="value">
		/// One of the Pharmaceutical.PharmaceuticalBrand values
		/// </param>
		/// <exception cref="ArgumentException">
		/// Value to set the brand is not a 
		/// member of Pharmaceutical.PharmaceuticalBrand
		/// </exception>
		/// <exception cref="InvalidOperationException">
		/// Value to set the type is Pharmaceutical.PharmaceuticalManufacturer.Undefined
		/// </exception>
		/// <remarks>
		/// This method is used internally by the Brand property.
		/// </remarks>
		private void SetBrand(PharmaceuticalBrand value)
		{
			if (!Enum.IsDefined(typeof(PharmaceuticalBrand), value))
			{
				string message = "Given brand value '" + value.ToString() +
					"' is not a member of enumeration PharmaceuticalBrand";
				throw new ArgumentException("value", message);
			}

			if (value == PharmaceuticalBrand.Undefined)
			{
				throw new InvalidOperationException(
					"Brand cannot explicitly be set to Undefined");
			}

			_brand = value;
		}

        #endregion
	}
}
