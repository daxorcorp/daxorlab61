﻿namespace Daxor.Lab.Infrastructure.DomainModels
{
	public class BloodVolumePoint
	{
		#region Properties

		public object UniqueId
		{
			get;
			private set;
		}
		public bool IsPointRejected
		{
			get;
			private set;
		}
		public int TimePostInjectionInSeconds
		{
			get;
			private set;
		}
		public double Counts
		{
			get;
			private set;
		}
		public double Hematocrit
		{
			get;
			private set;
		}
		public double UnadjustedBloodVolume
		{
			get;
			set;
		}
		public bool AreBothHematocritsExcluded
		{
			get;
			private set;
		}
		public bool AreBothCountsExcluded
		{
			get;
			private set;
		}

		public double C1TimesV1 { get; set; }
		public double C2 { get; set; }
		public double PlasmaVolume { get; set; }
		
        /// <summary>
        /// This is used by the calcUBV() method in the measured calc engine. It could be the 
        /// case that the unadjusted blood volume is out of range. This internal property will
        /// retain the "out of range" value, but the other property "UnadjustedBloodVolume" will
        /// be set to an error code.
        /// </summary>
        public double InternalUnadjustedBloodVolume { get; set; }  

		#endregion

		#region Ctor
		
		public BloodVolumePoint(object uniqueId, bool isPointReject, bool areHematocritsExcluded, bool areCountsExcl, int timePostInject,
					   double counts, double hematocrit, double unadjustedBloodVolume)
		{

			UniqueId = uniqueId;
			IsPointRejected = isPointReject;
			AreBothHematocritsExcluded = areHematocritsExcluded;
			AreBothCountsExcluded = areCountsExcl;
			TimePostInjectionInSeconds = timePostInject;
			Counts = counts;
			Hematocrit = hematocrit;
		   
			UnadjustedBloodVolume = unadjustedBloodVolume;
		}

		#endregion

		#region Equality methods

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			return obj.GetType() == GetType() && Equals((BloodVolumePoint) obj);
		}

		protected bool Equals(BloodVolumePoint other)
		{
			return Equals(UniqueId, other.UniqueId) && 
				IsPointRejected.Equals(other.IsPointRejected) && 
				TimePostInjectionInSeconds == other.TimePostInjectionInSeconds && 
				Counts.Equals(other.Counts) && Hematocrit.Equals(other.Hematocrit) && 
				UnadjustedBloodVolume.Equals(other.UnadjustedBloodVolume) && 
				AreBothHematocritsExcluded.Equals(other.AreBothHematocritsExcluded) && 
				AreBothCountsExcluded.Equals(other.AreBothCountsExcluded) && 
				C1TimesV1.Equals(other.C1TimesV1) && 
				C2.Equals(other.C2) && 
				PlasmaVolume.Equals(other.PlasmaVolume) && 
				InternalUnadjustedBloodVolume.Equals(other.InternalUnadjustedBloodVolume);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = (UniqueId != null ? UniqueId.GetHashCode() : 0);
				hashCode = (hashCode*397) ^ IsPointRejected.GetHashCode();
				hashCode = (hashCode*397) ^ TimePostInjectionInSeconds;
				hashCode = (hashCode*397) ^ Counts.GetHashCode();
				hashCode = (hashCode*397) ^ Hematocrit.GetHashCode();
				hashCode = (hashCode*397) ^ UnadjustedBloodVolume.GetHashCode();
				hashCode = (hashCode*397) ^ AreBothHematocritsExcluded.GetHashCode();
				hashCode = (hashCode*397) ^ AreBothCountsExcluded.GetHashCode();
				hashCode = (hashCode*397) ^ C1TimesV1.GetHashCode();
				hashCode = (hashCode*397) ^ C2.GetHashCode();
				hashCode = (hashCode*397) ^ PlasmaVolume.GetHashCode();
				hashCode = (hashCode*397) ^ InternalUnadjustedBloodVolume.GetHashCode();
				return hashCode;
			}
		}
		
		#endregion
	}
}