﻿using System;
namespace Daxor.Lab.Infrastructure.DomainModels
{
	public class AlertBase
	{
		public string UniqueId { get; set; }

		public string AlertType { get; set; }       
		
		public string AlertOwner { get; set; }      
		
		public int AlertIdByOwner { get; set; }     

        public Guid AlertId { get; set; }
		
        /// <remarks>
        /// This is a warning/suggestion of solution, failure, reminder, missing required field, etc.
        /// AlertMessageType is used to Style the alert appropriately. (warning icon is displayed for warnings,
        /// failure icon is displayed for failures, etc.)
        /// </remarks>
		public string AlertMessageType { get; set; }    
 
        /// <summary>
        /// Brief summary of the alert (i.e. Baseline too high, Daily QC due, Reverse Slope, etc.)
        /// </summary>
		public string AlertHeader { get; set; }      
		
		/// <summary>
        /// Detailed description of the alert with solution or suggestion
		/// </summary>
        public string AlertMessageBody { get; set; }    
	}
}

