﻿namespace Daxor.Lab.Infrastructure.DomainModels
{
	/// <summary>
	/// Specifies constants defining which set of buttons to display on an IMessageBox
	/// </summary>
	public enum MessageBoxChoiceSet
	{
		/// <summary>
		/// The message box contains an OK button.
		/// </summary>
		Ok = 0,

		/// <summary>
		/// The message box contains a Close button.
		/// </summary>
		Close = 1,

		/// <summary>
		/// The message box contains OK and Cancel buttons.
		/// </summary>
		OkCancel = 2,

		/// <summary>
		/// The message box contains Yes and No buttons.
		/// </summary>
		YesNo = 3,

		/// <summary>
		/// The message box contains Yes, No, and Cancel buttons.
		/// </summary>
		YesNoCancel = 4,

		/// <summary>
		/// The message box contains Start and Return buttons.
		/// </summary>
		StartReturn = 5,
	}
}
