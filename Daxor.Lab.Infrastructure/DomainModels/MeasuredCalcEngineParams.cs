﻿namespace Daxor.Lab.Infrastructure.DomainModels
{
	/// <summary>
	/// Parameters passed to measured calc engine.
	/// </summary>
	public class MeasuredCalcEngineParams
	{
		public int Background { get; set; }
		public double StandardCounts { get; set; }
		public double BaselineCounts { get; set; }
		public double BaselineHematocrit { get; set; }
		public double AnticoagulantFactor { get; set; }
		public int ReferenceVolume { get; set; }

		// needed to compute Normalized HCT. If NormHct is not needed pass 0 (it will return NHCT = 0)
		public int IdealTotalBloodVolume { get; set; }   
		public BloodVolumePoint[] Points { get; set; }
	}
}
