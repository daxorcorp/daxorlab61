﻿using System;
using System.Windows.Input;
using Daxor.Lab.Domain.Entities.Base;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;

namespace Daxor.Lab.Infrastructure.DomainModels
{
    public class AppModuleInfo : ObservableObject, IAppModuleInfo
    {
        #region Fields

        readonly INavigationCoordinator _navCoordinator;
        readonly ILoggerFacade _customLoggerFacade;

        bool _isEnabled = true;
        bool _isCurrentlyActive;
        int _order;
        string _fullName;
        string _shortName;
        string _version;
        string _appModuleKey;

        #endregion

        #region Ctor

        public AppModuleInfo(INavigationCoordinator navCoordinator, ILoggerFacade customLoggerFacade)
        {
            _navCoordinator = navCoordinator;
            _customLoggerFacade = customLoggerFacade;
            OpenAppModuleCommand = new DelegateCommand<object>(OnOpenAppModule, CanOpenAppModule);
        }

        #endregion

        #region Properties

        public int DisplayOrder
        {
            get {  return _order;}
            set
            {
                if (value < 0)
                    throw new NotSupportedException("Display order cannot be less than zero");

                base.SetValue(ref _order, "DisplayOrder", value);
            }
        }
        public string AppModuleKey
        {
            get { return _appModuleKey; }
            set
            {
                if (String.IsNullOrEmpty(value))
                    throw new NotSupportedException("AppModuleKey cannot be null or empty.");

                base.SetValue(ref _appModuleKey, "AppModuleKey", value);
            }
        }
        public string FullDisplayName
        {
            get { return _fullName; }
            set { base.SetValue(ref _fullName, "FullDisplayName", value); }
        }
        public string ShortDisplayName
        {
            get { return _shortName; }
            set { base.SetValue(ref _shortName, "ShortDisplayName", value); }
        }
        public string Version
        {
            get { return _version; }
            set { base.SetValue(ref _version, "Version", value); }
        }
        public DateTime BuildDate
        {
            get;
            set;
        }
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { base.SetValue(ref _isEnabled, "Version", value); CommandManager.InvalidateRequerySuggested(); }
        }
        public bool IsCurrentlyActive
        {
            get { return _isCurrentlyActive; }
            set { base.SetValue(ref _isCurrentlyActive, "IsCurrentlyActive", value); }
        }
        public ICommand OpenAppModuleCommand
        {
            get;
            set;
        }
        public bool IsConfigurable
        {
            get;
            set;
        }

        #endregion

        #region ToString()
        public override string ToString()
        {
            return ShortDisplayName.Replace("\n", " ");
        }
        #endregion

        #region Commands
        bool CanOpenAppModule(object arg)
        {
            return IsEnabled;
        }
        void OnOpenAppModule(object arg)
        {
           _customLoggerFacade.Log("OpenAppModule - " + AppModuleKey, Category.Info, Priority.None);
            if (arg is DisplayViewOption)
            {
                var option = (DisplayViewOption)arg;
                _navCoordinator.ActivateAppModule(AppModuleKey, null, option, _navCoordinator.ActiveAppModuleKey);
            }
            else
            {
                _navCoordinator.ActivateAppModule(AppModuleKey, null, _navCoordinator.ActiveAppModuleKey);
            }
        }
        #endregion
    }
}
