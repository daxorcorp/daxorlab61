﻿//-----------------------------------------------------------------------
// <copyright file="MessageBoxVerticalAlignment.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Daxor.Lab.Infrastructure.DomainModels
{
    /// <summary>
    /// Specifies constants defining the vertical alignment of message box buttons.
    /// </summary>
    public enum MessageBoxVerticalAlignment
    {
        /// <summary>
        /// The vertical alignment is undefined.
        /// </summary>
        Undefined = 0,
        /// <summary>
        /// The buttons should be top-aligned.
        /// </summary>
        Top = 1,
        /// <summary>
        /// The buttons should be centered.
        /// </summary>
        Center = 2,
        /// <summary>
        /// The buttons should be bottom-aligned.
        /// </summary>
        Bottom = 3,
    }
}
