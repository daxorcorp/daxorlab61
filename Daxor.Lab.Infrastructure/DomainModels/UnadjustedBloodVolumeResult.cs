﻿namespace Daxor.Lab.Infrastructure.DomainModels
{
    public class UnadjustedBloodVolumeResult
    {
        public object UniqueId { get; private set; }
        public double UnadjustedBloodVolume { get; private set; }

        public UnadjustedBloodVolumeResult(object uniqueId, double ubv)
        {
            UniqueId = uniqueId;
            UnadjustedBloodVolume = ubv;
        }
    }
}