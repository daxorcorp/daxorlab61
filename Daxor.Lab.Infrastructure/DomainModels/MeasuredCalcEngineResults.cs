﻿using System.Collections.Generic;

namespace Daxor.Lab.Infrastructure.DomainModels
{
	public class MeasuredCalcEngineResults
	{
		#region Props

		public double TimeZeroBloodVolume { get; private set; }
		public double TimeZeroPlasmaVolume { get; private set; }
		public double TimeZeroRedCellVolume { get; private set; }

		public double Slope { get; private set; }
		public double StdDev { get; private set; }
		public double NormStdDev { get; private set; }

		/// <summary>
		/// List of unadjusted Blood Volume Results
		/// </summary>
		public IEnumerable<UnadjustedBloodVolumeResult> UnadjustedBloodVolumeResults { get; private set; }

		/// <summary>
		/// Normalized Hematocrit. If equals to 0 that means no IdealTotalBloodVolume was passed
		/// and therefore it could not be computed
		/// </summary>
		public double NormalizedHematocrit { get; private set; }

		#endregion

		#region Ctor

		public MeasuredCalcEngineResults(IEnumerable<UnadjustedBloodVolumeResult> unadjustedBloodVolume, double timeZeroBloodVolume, double timeZeroPlasmaVolume, 
										 double timeZeroRedCellVolume, double slope, double stdDev, double normalizedHematocrit, double normStdDev = 0)
		{
			UnadjustedBloodVolumeResults = unadjustedBloodVolume;
			TimeZeroBloodVolume = timeZeroBloodVolume;
			TimeZeroPlasmaVolume = timeZeroPlasmaVolume;
			TimeZeroRedCellVolume = timeZeroRedCellVolume;
			Slope = slope;
			StdDev = stdDev;
			NormalizedHematocrit = normalizedHematocrit;
			NormStdDev = normStdDev;
		}

		#endregion
	}
}
