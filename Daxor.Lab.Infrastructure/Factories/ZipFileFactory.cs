﻿using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;

namespace Daxor.Lab.Infrastructure.Factories
{
    public class ZipFileFactory : IZipFileFactory
    {
        public IIonicZipWrapper CreateZipFile()
        {
            return new IonicZipWrapper();
        }

        public IIonicZipWrapper CreateZipFileWithPassword(string password)
        {
            return new IonicZipWrapper(password);
        }
    }
}
