﻿using System;
using System.Runtime.Serialization;

namespace Daxor.Lab.Infrastructure.Exceptions
{
    public class GammaCounterCommunicationException : Exception
    {
        public GammaCounterCommunicationException() { }
        public GammaCounterCommunicationException(string message) : base(message) { }
        public GammaCounterCommunicationException(string message, Exception inner) : base(message, inner) { }
        protected GammaCounterCommunicationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}