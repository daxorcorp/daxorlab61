﻿using System;

namespace Daxor.Lab.Infrastructure.Exceptions
{
	public class ModuleLoadException : Exception
	{
		public ModuleLoadException(string moduleName, string failingAction)
		{
			ModuleName = moduleName;
			FailingAction = failingAction;
		}

		public string ModuleName { get; set; }
		public string FailingAction { get; set; }
	}
}
