using System;
using System.Runtime.Serialization;

namespace Daxor.Lab.Infrastructure.Exceptions
{
    public class SampleChangerCommunicationException : Exception
    {
        public SampleChangerCommunicationException() { }
        public SampleChangerCommunicationException(string message) : base(message) { }
        public SampleChangerCommunicationException(string message, Exception inner) : base(message, inner) { }
        protected SampleChangerCommunicationException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}