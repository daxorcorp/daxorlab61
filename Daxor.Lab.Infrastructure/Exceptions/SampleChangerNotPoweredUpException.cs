﻿using System;
using System.Runtime.Serialization;

namespace Daxor.Lab.Infrastructure.Exceptions
{
    public class SampleChangerNotPoweredUpException : Exception
    {
        public SampleChangerNotPoweredUpException() { }
        public SampleChangerNotPoweredUpException(string message) : base(message) { }
        public SampleChangerNotPoweredUpException(string message, Exception inner) : base(message,inner) { }
        protected SampleChangerNotPoweredUpException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}