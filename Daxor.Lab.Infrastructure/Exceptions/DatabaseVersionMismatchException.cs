﻿using System;
using System.Runtime.Serialization;

namespace Daxor.Lab.Infrastructure.Exceptions
{
    /// <summary>
    /// Exception for when trying to restore a database with the wrong version
    /// </summary>
    public class DatabaseVersionMismatchException : Exception
    {
        public DatabaseVersionMismatchException() { }
        public DatabaseVersionMismatchException(string message) : base(message) { }
        public DatabaseVersionMismatchException(string message, Exception inner) : base(message, inner) { }
        protected DatabaseVersionMismatchException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
