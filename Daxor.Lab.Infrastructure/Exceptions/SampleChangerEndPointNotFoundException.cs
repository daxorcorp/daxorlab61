﻿using System;
using System.Runtime.Serialization;

namespace Daxor.Lab.Infrastructure.Exceptions
{
	public class SampleChangerEndPointNotFoundException : Exception
	{
		public SampleChangerEndPointNotFoundException() { }
		public SampleChangerEndPointNotFoundException(string message) : base(message) { }
        public SampleChangerEndPointNotFoundException(string message, Exception inner) : base(message, inner) { }
		protected SampleChangerEndPointNotFoundException(SerializationInfo info, StreamingContext context) : base (info,context) { }
	}
}
