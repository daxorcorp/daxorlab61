using System;
using System.Runtime.Serialization;

namespace Daxor.Lab.Infrastructure.Exceptions
{
    public class GammaCounterEndPointNotFoundException : Exception
    {
        public GammaCounterEndPointNotFoundException() { }
        public GammaCounterEndPointNotFoundException(string message) : base(message) { }
        public GammaCounterEndPointNotFoundException(string message, Exception inner) : base(message, inner) { }
        protected GammaCounterEndPointNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}