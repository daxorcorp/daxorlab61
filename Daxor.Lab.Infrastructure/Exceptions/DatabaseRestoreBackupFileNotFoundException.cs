﻿using System;
using System.Runtime.Serialization;

namespace Daxor.Lab.Infrastructure.Exceptions
{
    public class DatabaseRestoreBackupFileNotFoundException : Exception
    {
        public DatabaseRestoreBackupFileNotFoundException() { }
        public DatabaseRestoreBackupFileNotFoundException(string message) : base(message) { }
        public DatabaseRestoreBackupFileNotFoundException(string message, Exception inner) : base(message, inner) { }
        protected DatabaseRestoreBackupFileNotFoundException(SerializationInfo info, StreamingContext context) :base (info, context) { }
    }
}