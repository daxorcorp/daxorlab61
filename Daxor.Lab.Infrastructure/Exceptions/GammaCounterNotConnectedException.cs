﻿using System;
using System.Runtime.Serialization;

namespace Daxor.Lab.Infrastructure.Exceptions
{
    public class GammaCounterNotConnectedException : Exception
	{
		public GammaCounterNotConnectedException() { }
        public GammaCounterNotConnectedException(string message) : base(message) { }
        public GammaCounterNotConnectedException(string message, Exception inner) : base(message, inner) { }
        protected GammaCounterNotConnectedException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
