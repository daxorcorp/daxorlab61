﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace Daxor.Lab.Infrastructure.Keyboards.Common
{
	/// <summary>
	/// Event manager that adds and removes listeners for the GotFocus
	/// event on TextBox using the weak event pattern.
	/// </summary>
	public class TextBoxGotFocusEventManagerWeakEventManager : WeakEventManager
	{
		#region Static methods

		/// <summary>
		/// Adds the specified listener to the specified source for the event being managed.
		/// </summary>
		/// <param name="source">Source to attach listeners to</param>
		/// <param name="listener">Listening class</param>
		public static void AddListener(TextBoxBase source, IWeakEventListener listener)
		{
			CurrentManager.ProtectedAddListener(source, listener);
		}

		/// <summary>
		/// Removes a previously added listener from the specified source.
		/// </summary>
		/// <param name="source">Source to remove listeners from</param>
		/// <param name="listener">Listening class</param>
		public static void RemoveListener(TextBoxBase source, IWeakEventListener listener)
		{
			CurrentManager.ProtectedRemoveListener(source, listener);
		}

		/// <summary>
		/// Gets the instance of TestBoxBasePreviewGotFocusEventManager registered with WeakEventManager.
		/// If no instance has been set, this property creates and instance and sets it as the manager.
		/// </summary>
		private static TextBoxGotFocusEventManagerWeakEventManager CurrentManager
		{
			get
			{
				var managerType = typeof(TextBoxGotFocusEventManagerWeakEventManager);
				var manager = (TextBoxGotFocusEventManagerWeakEventManager)GetCurrentManager(managerType);
				if (manager != null) return manager;
				manager = new TextBoxGotFocusEventManagerWeakEventManager();
				SetCurrentManager(managerType, manager);
				return manager;
			}
		}

		#endregion

		#region WeakEventManager overrides

		/// <summary>
		/// Starts listening for the PreviewGotKeyboardFocus event on the specified
		/// source by wiring a handler/delegate for the event.
		/// </summary>
		/// <param name="source">Source to begin listening on</param>
		/// <exception cref="NullReferenceException">Given source is not of type TextBoxBase
		/// </exception>
		protected override void StartListening(object source)
		{
			var textBox = source as TextBoxBase;
			if (textBox == null)
				throw new NullReferenceException("Unable to start listening for TextBoxBase.GotFocus " +
					"because specified source ('" + source.GetType() + "') is not TextBoxBase.");

			textBox.GotFocus += OnGotFocus;
		}

	 
		/// <summary>
		/// Stops listening for the PreviewGotKeyboardFocus event on the specified
		/// source by unwiring a handler/delegate for the event.
		/// </summary>
		/// <param name="source">Source to stop listening on</param>
		/// <exception cref="NullReferenceException">Given source is not of type TextBoxBase
		/// </exception>
		protected override void StopListening(object source)
		{
			var textBox = source as TextBoxBase;
			if (textBox == null)
				throw new NullReferenceException("Unable to stop listening for TextBoxBase.GotFocus " +
					"because specified source ('" + source.GetType() + "') is not TextBoxBase.");

			textBox.GotFocus -= OnGotFocus;
		}

		#endregion

		#region Event handler

		/// <summary>
		/// Handler/delegate invoked when the event source fires GotKeyboardFocus that
		/// delivers the event to the listeners managed by this instance.
		/// </summary>
		/// <param name="sender">Sender of the event</param>
		/// <param name="e">Arguments for the event</param>
		void OnGotFocus(object sender, RoutedEventArgs e)
		{
			DeliverEvent(sender, e);
		}


		#endregion
	}
}
