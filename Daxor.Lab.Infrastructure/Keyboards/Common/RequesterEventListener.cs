﻿using System;
using System.Windows;

namespace Daxor.Lab.Infrastructure.Keyboards.Common
{
	/// <summary>
	/// Listens for events on the FrameworkElement that requests a virtual keyboard
	/// and forwards them to a specified handler.
	/// </summary>
	public class RequesterEventListener : IWeakEventListener
	{
		#region Fields

		/// <summary>
		/// Private member variable representing the handler/delegate to call when a weak
		/// event is received.
		/// </summary>
		private readonly Action<Object, EventArgs> _requesterEventHandler;

		#endregion

		#region Ctor

		/// <summary>
		/// Initializes a new instance of the RequesterEventListener class that responds 
		/// to weak events using the specified handler.
		/// </summary>
		/// <param name="requesterEventHandler">Handler/delegate that can be invoked
		/// when a weak event is received</param>
		public RequesterEventListener(Action<Object, EventArgs> requesterEventHandler)
		{
			_requesterEventHandler = requesterEventHandler;
		}

		#endregion

		#region IWeakEventListener members

		/// <summary>
		/// Receives events from the TextBoxBasePreviewGotFocusEventManager and invokes
		/// the appropriate handler/delegate.
		/// </summary>
		/// <param name="managerType">Type of WeakEventManager calling this method</param>
		/// <param name="sender">Object that originated the event</param>
		/// <param name="e">Event data</param>
		/// <returns>True if the listener handled the event (i.e., the WeakEventManager
		/// is a TextBoxPreviewGotFocusEventManager and the handler/delegate exists);
		/// false otherwise</returns>
		public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
		{
			var managerIsCorrectType = managerType == typeof(TextBoxPreviewGotKeyboardFocusEventManager);
			var handlerExists = _requesterEventHandler != null;

			if (!managerIsCorrectType || !handlerExists) return false;
			_requesterEventHandler(sender, e);
			return true;
		}

		#endregion
	}
}
