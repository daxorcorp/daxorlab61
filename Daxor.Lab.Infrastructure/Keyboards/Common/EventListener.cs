﻿using System;
using System.Windows;

namespace Daxor.Lab.Infrastructure.Keyboards.Common
{
	/// <summary>
	/// Listens for events from a specified WeakEventManager and forwards them to a 
	/// specified handler.
	/// </summary>
	public class EventListener<TManager> : IWeakEventListener where TManager : WeakEventManager
	{
		#region Fields

		/// <summary>
		/// Private member variable representing the handler/delegate to call when a weak
		/// event is received.
		/// </summary>
		private readonly Action<Object> _requesterEventHandler;

		/// <summary>
		/// Private member variable representing the WeakEventManager that this EventListener
		/// belongs to.
		/// </summary>
		private readonly Type _managerType;

		#endregion

		#region Ctor

		/// <summary>
		/// Initializes a new instance of the RequesterEventListener class that responds 
		/// to weak events using the specified handler.
		/// </summary>
		/// <param name="requesterEventHandler">Handler/delegate that can be invoked
		/// when a weak event is received</param>
		public EventListener(Action<Object> requesterEventHandler)
		{
			_requesterEventHandler = requesterEventHandler;
			_managerType = typeof(TManager);
		}

		#endregion

		#region IWeakEventListener Members
		/// <summary>
		/// Receives events from the TextBoxBasePreviewGotFocusEventManager and invokes
		/// the appropriate handler/delegate.
		/// </summary>
		/// <param name="managerType">Type of WeakEventManager calling this method</param>
		/// <param name="sender">Object that originated the event</param>
		/// <param name="e">Event data</param>
		/// <returns>True if the listener handled the event (i.e., the WeakEventManager
		/// is the same type that is associated with this instance and the 
		/// handler/delegate exists); false otherwise</returns>
		public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
		{
			var managerIsCorrectType = managerType == _managerType;
			var handlerExists = _requesterEventHandler != null;

			if (!managerIsCorrectType || !handlerExists) return false;
			_requesterEventHandler(sender);
			return true;
		}

		#endregion
	}
}
