﻿using System;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using Daxor.Lab.Infrastructure.Keyboards.PropertyObservers;
using Daxor.Lab.Infrastructure.RuleFramework;

namespace Daxor.Lab.Infrastructure.Keyboards.Common
{
    public class ObserverKeyboardUserControl : UserControl
    {
        public ObserverKeyboardUserControl()
        {
            KeyboardTextObserver = new DependencyPropertyObserver<string>(TextBox.TextProperty);
            ReadOnlyObserver = new DependencyPropertyObserver<bool>(TextBoxBase.IsReadOnlyProperty);
            WatermarkObserver = new DependencyPropertyObserver<string>(VirtualKeyboardManager.WatermarkProperty);
            TabStopObserver = new DependencyPropertyObserver<bool>(IsTabStopProperty);
            HasAlertObserver = new DependencyPropertyObserver<bool>(ValidationHelper.HasAlertViolationProperty);
        }

        /// <summary>
        /// Gets or sets the dependency property observer for the keyboard text.
        /// </summary>
        public DependencyPropertyObserver<String> KeyboardTextObserver
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the dependency property observer for the readonly flag.
        /// </summary>
        public DependencyPropertyObserver<Boolean> ReadOnlyObserver
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the dependency property observer for the watermark text.
        /// </summary>
        public DependencyPropertyObserver<String> WatermarkObserver
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the dependency property observer for the IsTabStop text.
        /// </summary>
        public DependencyPropertyObserver<Boolean> TabStopObserver
        {
            get;
            protected set;
        }

        /// <summary>
        /// Set or sets the dependency property ob
        /// </summary>
        public DependencyPropertyObserver<Boolean> HasAlertObserver
        {
            get;
            protected set;
        }

    }
}
