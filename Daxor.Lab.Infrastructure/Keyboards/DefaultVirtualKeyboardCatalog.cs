﻿using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Keyboards.Controllers;
using Daxor.Lab.Infrastructure.Keyboards.Views;

namespace Daxor.Lab.Infrastructure.Keyboards
{
	/// <summary>
	/// Represents a collection of virtual keyboards (controllers)
	/// </summary>
	public class DefaultVirtualKeyboardCatalog
	{
		private List<IVirtualKeyboard> _keyboardCatalog;

		/// <summary>
		/// Returns a collection of virtual keyboards (i.e., keyboard controllers with
		/// corresponding visuals and identifiers).
		/// </summary>
		/// <returns>Collection (IEnumerable) of virtual keyboards</returns>
		public IEnumerable<IVirtualKeyboard> GetVirtualKeyboards()
		{
			if (_keyboardCatalog != null)
				return _keyboardCatalog;

			_keyboardCatalog = new List<IVirtualKeyboard>
			{
				new KeyboardSelfClosableController(new FullKeyboardVisual(), KeyboardNames.FullKeyboard),
				new KeyboardDurationController(new DurationKeyboardVisual(), KeyboardNames.DurationKeyboard),
				new KeyboardSelfClosableController(new DateOfBirthKeyboardVisual(), KeyboardNames.DateOfBirthKeyboard),
				new KeyboardSelfClosableController(new MeasurementsKeyboardVisual(), KeyboardNames.MesurementKeyboard),
				new KeyboardSelfClosableController(new NumericKeyboardVisual(), KeyboardNames.BasicNumericKeyboard),
				new KeyboardSelfClosableController(new CountsKeyboardVisual(), KeyboardNames.CountsKeyboard),
				new KeyboardSelfClosableController(new HematocritKeyboardVisual(), KeyboardNames.HematocritKeyboard),
				new KeyboardSelfFocusableController(new BackgroundKeyboardVisual(), KeyboardNames.BackgroundKeyboard),
				new KeyboardSelfFocusableController(new FullKeyboardVisual(), KeyboardNames.FullSearchKeyboard),
				new KeyboardSelfFocusableController(new LoginKeyboardVisual(), KeyboardNames.LoginKeyboard)
			};

			return _keyboardCatalog;
		}
	}
}
