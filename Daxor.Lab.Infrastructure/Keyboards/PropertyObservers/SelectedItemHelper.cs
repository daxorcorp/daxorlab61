﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Daxor.Lab.Infrastructure.Keyboards.PropertyObservers
{
	/// <summary>
	/// Helper class that exposes an event which is fired when 
	/// VirtualKeyboardManager.PickListSelectedItemProperty has changed.
	/// </summary>
	public class PickListSelectedItemHelper : DependencyObject
	{
		#region Events

		/// <summary>
		/// Occurs when the VirtualKeyboardManager.PickListSelectedItemProperty has changed.
		/// </summary>
		public event PropertyChangedCallback PickListSelectedItemPropertyChanged;

		#endregion

		#region Dependency property: "PickListSelectedItem"

		/// <summary>
		/// Dependency property that represents the selected item on the virtual keyboard's pick list.
		/// </summary>
		public static readonly DependencyProperty PickListSelectedItemProperty =
			DependencyProperty.Register("PickListSelectedItem", typeof(Object), typeof(PickListSelectedItemHelper),
			new FrameworkPropertyMetadata(null, OnPickListSelectedItemChanged));

		/// <summary>
		/// Gets or sets the value of the PickListSelectedItem dependency property.
		/// </summary>
		/// <exception cref="InvalidOperationException">PickListSelectedItemProperty value 
		/// is invalid or PickListSelectedItemProperty does not exist</exception>
		/// <exception cref="ArgumentException">Specified value (for PickListSelectedItem.set) 
		/// is not the correct type</exception>
		public object PickListSelectedItem
		{
			get { return GetValue(PickListSelectedItemProperty); }
			set { SetValue(PickListSelectedItemProperty, value); }
		}

		#endregion

		#region BindWith / Unbind

		/// <summary>
		/// Sets up binding from the specified dependency object's 
		/// VirtualKeyboardManager.PickListSelectedItemProperty property to the dependency 
		/// property of this PickListSelectedItemHelper.
		/// </summary>
		/// <param name="dObject">Dependency object source (i.e., VirtuaKeyboardManager)</param>
		/// <remarks>
		/// 1. If this instance was already bound to another dependency object, that binding
		///    will be removed.
		/// 2. The trigger for observing the dependency property is PropertyChanged.
		/// 3. The binding is two-way.
		/// </remarks>
		/// <exception cref="ArgumentNullException">PickListSelectedItemProperty is null</exception>
		public void BindWith(DependencyObject dObject)
		{
			Unbind();

			var newBinding = new Binding
			{
				UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
				Mode = BindingMode.OneWayToSource,
				Source = dObject,
				Path = new PropertyPath(VirtualKeyboardManager.PickListSelectedItemProperty)
			};
			BindingOperations.SetBinding(this, PickListSelectedItemProperty, newBinding);
		}

		/// <summary>
		/// Removes any binding associated with this PickListSelectedItemHelper.
		/// </summary>
		/// <exception cref="ArgumentNullException">PickListSelectedItemProperty is null</exception>
		public void Unbind()
		{
			BindingOperations.ClearBinding(this, PickListSelectedItemProperty);
		}

		#endregion

		#region Event handlers

		/// <summary>
		/// Fires the PickListSelectedItemChanged event.
		/// </summary>
		/// <remarks>Invoked when VirtualKeyboardManager.PickListSelectedItemProperty on the dependency
		/// object specified via BindWith() has changed.</remarks>
		/// <param name="args">Arguments for the event</param>
		protected virtual void FirePickListSelectedItemChanged(DependencyPropertyChangedEventArgs args)
		{
			if (PickListSelectedItemPropertyChanged != null)
				PickListSelectedItemPropertyChanged(this, args);
		}

		/// <summary>
		/// Handler/delegate invoked whenever the PickListSelectedItemHelper.PickListSelectedItemProperty
		/// has changed.
		/// </summary>
		/// <remarks>Refer to how the dependency property is registered (specifically the metadata) to
		/// see how this method is invoked</remarks>
		/// <param name="dObject">Dependency object that changed (which should be 
		/// PickListSelectedItemHelper)</param>
		/// <param name="args">Arguments for the event</param>
		private static void OnPickListSelectedItemChanged(DependencyObject dObject, DependencyPropertyChangedEventArgs args)
		{
			var pickListSelectedItemHelper = dObject as PickListSelectedItemHelper;
			if (pickListSelectedItemHelper != null)
				pickListSelectedItemHelper.FirePickListSelectedItemChanged(args);
		}

		#endregion
	}
}
