﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Daxor.Lab.Infrastructure.Keyboards.PropertyObservers
{
	/// <summary>
	/// Helper class that exposes an event which is fired when the TextProperty of a 
	/// requester of a virtual keyboard (which should be TextBoxBase) has changed.
	/// </summary>
	public class KeyboardTextHelper : DependencyObject
	{
		#region Events

		/// <summary>
		/// Occurs when the TextBox.TextProperty associated with the virtual keyboard has changed.
		/// </summary>
		public event PropertyChangedCallback KeyboardTextPropertyChanged;

		#endregion

		#region Dependency property: "KeyboardText"

		/// <summary>
		/// Dependency property that represents the virtual keyboard requester's text.
		/// </summary>
		public static readonly DependencyProperty KeyboardTextProperty =
			DependencyProperty.Register("KeyboardText", typeof(string), typeof(KeyboardTextHelper), new FrameworkPropertyMetadata(String.Empty, OnKeyboardTextChanged));

		/// <summary>
		/// Gets or sets the value of the KeyboardText dependency property.
		/// </summary>
		/// <exception cref="InvalidOperationException">KeyboardTextProperty value is invalid or 
		/// KeyboardTextProperty does not exist</exception>
		/// <exception cref="ArgumentException">Specified value (for KeyboardText.set) is not the 
		/// correct type</exception>
		public string KeyboardText
		{
			get { return (string)GetValue(KeyboardTextProperty); }
			set { SetValue(KeyboardTextProperty, value); }
		}

		#endregion

		#region BindWith / Unbind

		/// <summary>
		/// Sets up binding from the specified dependency object's TextBox.TextProperty
		/// property to the dependency property of this KeyboardTextHelper.
		/// </summary>
		/// <param name="dObject">Dependency object source (i.e., TextBox that requested
		/// the virtual keyboard)</param>
		/// <param name="mode"></param>
		/// <remarks>
		/// 1. If this instance was already bound to another dependency object, that binding
		///    will be removed.
		/// 2. The trigger for observing the dependency property is PropertyChanged.
		/// 3. The binding is one-way (keyboard requester to this class).
		/// </remarks>
		/// <exception cref="ArgumentNullException">KeyboardTextProperty is null</exception>
		public void BindWith(DependencyObject dObject, BindingMode mode = BindingMode.OneWay)
		{
			Unbind();

			var newBinding = new Binding
			{
				UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
				Mode = mode,
				Source = dObject,
				Path = new PropertyPath(TextBox.TextProperty)
			};
			BindingOperations.SetBinding(this, KeyboardTextProperty, newBinding);
		}

		/// <summary>
		/// Removes any binding associated with this KeyboardTextHelper.
		/// </summary>
		/// <exception cref="ArgumentNullException">KeyboardTextProperty is null</exception>
		public void Unbind()
		{
			BindingOperations.ClearBinding(this, KeyboardTextProperty);
		}

		#endregion

		#region Event handlers

		/// <summary>
		/// Raises the KeyboardTextPropertyChanged event.
		/// </summary>
		/// <remarks>Invoked when TextBoxBase.TextProperty on the dependency object specified
		/// via BindWith() has changed.</remarks>
		/// <param name="args">Arguments for the event</param>
		protected virtual void FireKeyboardTextPropertyChanged(DependencyPropertyChangedEventArgs args)
		{
			if (KeyboardTextPropertyChanged != null)
				KeyboardTextPropertyChanged(this, args);
		}

		/// <summary>
		/// Handler/delegate invoked whenever KeyboardTextHelper.KeyboardTextProperty has changed.
		/// </summary>
		/// <remarks>Refer to how the dependency property is registered (specifically the metadata) to
		/// see how this method is invoked</remarks>
		/// <param name="dObject">Dependecy object that changed (which should be KeyboardTextHelper)</param>
		/// <param name="args">Arguments for the event</param>
		private static void OnKeyboardTextChanged(DependencyObject dObject, DependencyPropertyChangedEventArgs args)
		{
			var keyboardTextHelper = dObject as KeyboardTextHelper;
			if (keyboardTextHelper != null)
				keyboardTextHelper.FireKeyboardTextPropertyChanged(args);
		}

		#endregion
	}
}
