﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Daxor.Lab.Infrastructure.Keyboards.PropertyObservers
{
	/// <summary>
	/// Represents a strongly-typed dependency property observer that can observe
	/// changes in a specified dependency property on a specified dependency object.
	/// </summary>
	/// <typeparam name="T">Dependency property's type</typeparam>
	public class DependencyPropertyObserver<T> : DependencyObject
	{
		#region Fields

		/// <summary>
		/// Private member variable representing the property to be observed.
		/// </summary>
		private readonly DependencyProperty _propertyToObserve;

		#endregion

		#region Ctor

		/// <summary>
		/// Creates a new DependencyPropertyObserver that is capable of observing
		/// the specified dependency property.
		/// </summary>
		/// <param name="propertyToObserve">Dependency property for this instance to 
		/// observe</param>
		/// <exception cref="ArgumentNullException">Specified dependency property 
		/// instance is null</exception>
		public DependencyPropertyObserver(DependencyProperty propertyToObserve)
		{
			if (propertyToObserve == null)
				throw new ArgumentNullException("propertyToObserve");

			_propertyToObserve = propertyToObserve;
		}

		#endregion

		#region Dependency property: "Value"

		/// <summary>
		/// Dependency property that represents the dependency property this class observes.
		/// </summary>
		public static readonly DependencyProperty ValueProperty =
			DependencyProperty.Register("Value", typeof(T), typeof(DependencyPropertyObserver<T>), new UIPropertyMetadata(default(T)));

		/// <summary>
		/// Gets or sets the value of the ValueProperty dependency property.
		/// </summary>
		/// <exception cref="InvalidOperationException">ValueProperty value is invalid or 
		/// ValueProperty does not exist</exception>
		/// <exception cref="ArgumentException">Specified value (for Value.set) is not the 
		/// correct type</exception>
		public T Value
		{
			get { return (T)GetValue(ValueProperty); }
			set { SetValue(ValueProperty, value); }
		}

		#endregion

		#region Methods

		/// <summary>
		/// Starts observing the dependency property (on the specified dependency object)
		/// associated with this instance.
		/// </summary>
		/// <param name="dObject">Dependency object that has a dependency property of
		/// the type specified when this class was instantiated.</param>
		/// <remarks>
		/// 1. If this instance was already observing another dependency object, that object
		///    will stop being observed.
		/// 2. The trigger for observing the dependency property is PropertyChanged.
		/// 3. The binding is two-way.
		/// </remarks>
		/// <exception cref="ArgumentNullException">ValueProperty is null</exception>
		public void Start(DependencyObject dObject)
		{
			Stop();
			Start(dObject, BindingMode.TwoWay);
		}
		public void Start(DependencyObject dObject, BindingMode mode)
		{
			var newBinding = new Binding
			{
				UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
				Mode = mode,
				Source = dObject,
				Path = new PropertyPath(_propertyToObserve)
			};
			BindingOperations.SetBinding(this, ValueProperty, newBinding);
		}

		/// <summary>
		/// Stops observing the dependency property associated with this instance.
		/// </summary>
		/// <exception cref="ArgumentNullException">ValueProperty is null</exception>
		public void Stop()
		{
			BindingOperations.ClearBinding(this, ValueProperty);
		}

		#endregion
	}
}
