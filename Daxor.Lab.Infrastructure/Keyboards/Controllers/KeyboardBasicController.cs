﻿using Daxor.Lab.Infrastructure.Keyboards.Views;

namespace Daxor.Lab.Infrastructure.Keyboards.Controllers
{
	/// <summary>
	/// Virtual keyboard controller that handles physical and virtual input for the
	/// basic (full) keyboard.
	/// </summary>
	public class KeyboardBasicController : VirtualKeyboardControllerBase
	{
		/// <summary>
		/// Creates a new virtual basic keyboard controller for the specified keyboard
		/// view and specified keyboard identifier.
		/// </summary>
		/// <seealso cref="VirtualKeyboardControllerBase"/>
		/// <param name="visual">Keyboard view associated with this controller</param>
		/// <param name="uniqueId">Keyboard identifier of the virtual keyboard controlled
		/// by this controller</param>
		public KeyboardBasicController(IKeyboardView visual, object uniqueId) : base(visual, uniqueId) { }
	}
}
