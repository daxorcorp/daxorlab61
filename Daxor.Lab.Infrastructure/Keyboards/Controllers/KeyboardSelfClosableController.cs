﻿using System.Windows.Input;
using Daxor.Lab.Infrastructure.Keyboards.Views;

namespace Daxor.Lab.Infrastructure.Keyboards.Controllers
{
	/// <summary>
	/// Virtual keyboard controller that handles physical and virtual input for a
	/// self-closable (i.e., keyboard with a close button) keyboard.
	/// </summary>
	public class KeyboardSelfClosableController : VirtualKeyboardControllerBase
	{
		#region Ctor

		/// <summary>
		/// Creates a new virtual self-closable keyboard controller for the specified keyboard
		/// view and specified keyboard identifier.
		/// </summary>
		/// <seealso cref="VirtualKeyboardControllerBase"/>
		/// <param name="keyboard">Keyboard view associated with this controller</param>
		/// <param name="uniqueId">Keyboard identifier of the virtual keyboard controlled
		/// by this controller</param>
		public KeyboardSelfClosableController(IKeyboardView keyboard, string uniqueId) : base(keyboard, uniqueId) { }

		#endregion

		#region Overrides

		/// <summary>
		/// Processes the specified input from the virtual keyboard.
		/// </summary>
		/// <param name="singleCharacterInput">Single character of text entered via the virtual keyboard</param>
		/// <param name="keyPressed">Specific key (e.g., escape) pressed on the virtual keyboard</param>
		/// <remarks>
		/// If Key.Escape is specified, the RequestToClose event will be fired. Otherwise
		/// VirtualKeyboardControllerBase.ProcessVirtualKeyboardKeyPress() processes the input.
		/// </remarks>
		protected override void ProcessVirtualKeyboardKeyPress(string singleCharacterInput, Key? keyPressed)
		{
			if ((keyPressed != null) && (keyPressed.Value == Key.Return))
				FireRequestToClose();
			else
				base.ProcessVirtualKeyboardKeyPress(singleCharacterInput, keyPressed);
		}

		#endregion
	}
}
