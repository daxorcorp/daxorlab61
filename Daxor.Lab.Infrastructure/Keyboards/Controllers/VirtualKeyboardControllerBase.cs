﻿using System;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Daxor.Lab.Infrastructure.Keyboards.PropertyObservers;
using Daxor.Lab.Infrastructure.Keyboards.Views;

namespace Daxor.Lab.Infrastructure.Keyboards.Controllers
{
	/// <summary>
	/// Abstract virtual keyboard controller that provides a core set of functionality
	/// for a virtual keyboard (controller), mainly consisting of handling physical
	/// or virtual input.
	/// </summary>
	abstract public class VirtualKeyboardControllerBase : IVirtualKeyboard
	{
		#region Fields

		/// <summary>
		/// Private member variable representing the TextBox that requested the virtual keyboard.
		/// </summary>
		private TextBox _requesterAsTextBox;

		/// <summary>
		/// Dependency observer that listens for changes in the input format (i.e., regular
		/// expression) associated with the virtual keyboard.
		/// </summary>
		private readonly DependencyPropertyObserver<string> _inputFormatObserver;

		#endregion

		#region Ctor

		/// <summary>
		/// Creates a new virtual keyboard controller for the specified keyboard view and
		/// specified keyboard identifier.
		/// </summary>
		/// <param name="keyboardView">Keyboard view associated with this controller</param>
		/// <param name="keyboardId">Keyboard identifier of the virtual keyboard controlled
		/// by this controller</param>
		/// <exception cref="ArgumentNullException">If either of the specified parameters
		/// is null</exception>
		protected VirtualKeyboardControllerBase(IKeyboardView keyboardView, object keyboardId)
		{
			if (keyboardView == null)
				throw new ArgumentNullException("keyboardView", @"Failed to intialize keyboard controller; the keyboard view cannot be null.");
			if (keyboardId == null)
				throw new ArgumentNullException("keyboardId", @"Failed to initialize keyboard controller; the keyboard ID cannot be null.");

			_inputFormatObserver = new DependencyPropertyObserver<string>(VirtualKeyboardManager.InputFormatProperty);
			KeyboardID = keyboardId;
			VisualView = keyboardView;
		}

		#endregion

		#region User32.dll -- for handling caps-lock synchronization

		/// <summary>
		/// Synthesizes a keystroke
		/// </summary>
		/// <param name="bVk">Virtual key code</param>
		/// <param name="bScan">Hardware scan code for the key</param>
		/// <param name="dwFlags">Flags that control aspects of function operation</param>
		/// <param name="dwExtraInfo">Additional value associated with the keystroke</param>
		[DllImport("user32.dll")]
		protected static extern void keybd_event(byte bVk, byte bScan, uint dwFlags, UIntPtr dwExtraInfo);

		/// <summary>
		/// Key code for Caps Lock.
		/// </summary>
		internal const byte CapsLockKey = 0x14;

		/// <summary>
		/// Scan code is preceded by a prefix byte having the value 0xE0 (224).
		/// </summary>
		internal const int ExtendedKey = 0x1;

		/// <summary>
		/// Key is being released.
		/// </summary>
		internal const int KeyUpEvent = 0x2;
		
        #endregion

		#region Properties

		/// <summary>
		/// Gets or sets the TextBox that requested the virtual keyboard. Setting the TextBox
		/// instance wires/unwires the keyboard and mouse handlers for the TextBox as well as
		/// the IKeyboardView.VirtualKeyDown handler.
		/// </summary>
		/// <remarks>This should be VirtuakKeyboardControllerBase.Requester cast as a
		/// TextBox.</remarks>
		protected TextBox RequesterAsTextBox
		{
			get { return _requesterAsTextBox; }
			set
			{
				if (_requesterAsTextBox != null)
				{
					RequesterAsTextBox.PreviewKeyDown -= OnPreviewKeyDown;
					RequesterAsTextBox.PreviewMouseDoubleClick -= OnPreviewMouseDoubleClick;
					RequesterAsTextBox.PreviewMouseMove -= OnPreviewMouseMove;
					RequesterAsTextBox.PreviewMouseRightButtonUp -= OnPreviewMouseRightButtonUp;
					RequesterAsTextBox.PreviewTextInput -= OnPreviewTextInput;
					VisualView.VirtualKeyDown -= OnVirtualKeyDown;
				}

				_requesterAsTextBox = value;

				if (_requesterAsTextBox != null)
				{
					RequesterAsTextBox.PreviewKeyDown += OnPreviewKeyDown;
					RequesterAsTextBox.PreviewMouseMove += OnPreviewMouseMove;
					RequesterAsTextBox.PreviewMouseDoubleClick += OnPreviewMouseDoubleClick;
					RequesterAsTextBox.PreviewMouseRightButtonUp += OnPreviewMouseRightButtonUp;
					RequesterAsTextBox.PreviewTextInput += OnPreviewTextInput;
					VisualView.VirtualKeyDown += OnVirtualKeyDown;
				}
			}
		}

		/// <summary>
		/// Gets the validation regular expression based on the input format property
		/// observer.
		/// </summary>
		/// <remarks>Returns string.Empty if the input format property observer instance
		/// is null</remarks>
		protected virtual string ValidationRegEx
		{
			get
			{
				if (_inputFormatObserver == null)
					return string.Empty;
				return _inputFormatObserver.Value;
			}
		}

		#endregion

		#region IVirtualKeyboard members

		#region Properties

		/// <summary>
		/// Gets or sets the identifier of the keyboard controlled by this controller.
		/// </summary>
		public object KeyboardID
		{
			get;
			protected set;
		}

		/// <summary>
		/// Gets or sets the FrameworkElement that requested the virtual keyboard.
		/// </summary>
		public FrameworkElement Requester
		{
			get;
			protected set;
		}

		/// <summary>
		/// Gets or sets the view for the virtual keyboard controlled by this controller.
		/// </summary>
		public IKeyboardView VisualView
		{
			get;
			protected set;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Activates (shows) the virtual keyboard invoked by a given FrameworkElement.
		/// </summary>
		/// <param name="requester">UI element that requested the keyboard to be activated</param>
		/// <exception cref="ArgumentNullException">Specified FrameworkElement is null</exception>
		/// <remarks>Closes the active keyboard if present and if the requester is different
		/// than the one that invoked the active keyboard</remarks>
		public virtual void Activate(FrameworkElement requester)
		{
			if (requester == null)
				throw new ArgumentNullException("requester", @"Virtual keyboard activation failed; requesting FrameworkElement cannot be null.");

			// Close the keyboard if there's a keyboard open and the requester is different.
			var isRequestingTextBoxDifferent = !Equals(RequesterAsTextBox, requester as TextBox);
			var isKeyboardActive = false;
			if (VisualView != null)
				isKeyboardActive = VisualView.IsOpened;

			if (isRequestingTextBoxDifferent && isKeyboardActive)
				Deactivate();

			_inputFormatObserver.Start(requester);
			Requester = requester;
			RequesterAsTextBox = requester as TextBox;

			// The requester has focus, so move it's caret to the end before showing the keyboard
			// ReSharper disable once PossibleNullReferenceException
			RequesterAsTextBox.CaretIndex = RequesterAsTextBox.Text.Length;

			if (VisualView != null)
				VisualView.Show(requester);
		}

		/// <summary>
		/// Deactivates (closes) the virtual keyboard.
		/// </summary>
		public virtual void Deactivate()
		{
			_inputFormatObserver.Stop();
			Requester = null;
			RequesterAsTextBox = null;

			if (VisualView != null)
				VisualView.Close();
		}

		#endregion

		#region Events

		/// <summary>
		/// Occurs when the virtual keyboard is requesting to be closed.
		/// </summary>
		public event EventHandler RequestToClose;

		#endregion

		#endregion

		#region Event handlers

		/// <summary>
		/// Handler/delegate for the UIElement.PreviewKeyDown event that stops the 
		/// event tunneling only if the key input was processed successfully.
		/// </summary>
		/// <param name="sender">Sender of the event</param>
		/// <param name="e">Event data</param>
		/// <seealso cref="VirtualKeyboardControllerBase.ProcessPhysicalKeyboardKeyPress"/>
		internal void OnPreviewKeyDown(object sender, KeyEventArgs e)
		{
			e.Handled = !ProcessPhysicalKeyboardKeyPress(null, e.Key);
		}

		/// <summary>
		/// Handler/delegate for the UIElement.MouseDoubleClick event that stops the
		/// event tunneling.
		/// </summary>
		/// <param name="sender">Sender of the event</param>
		/// <param name="e">Event data</param>
		internal void OnPreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;
		}

		/// <summary>
		/// Handler/delegate for the UIElement.PreviewMouseMove event that stops the
		/// event tunneling.
		/// </summary>
		/// <param name="sender">Sender of the event</param>
		/// <param name="e">Event data</param>
		internal void OnPreviewMouseMove(object sender, MouseEventArgs e)
		{
			e.Handled = true;
		}

		/// <summary>
		/// Handler/delegate for the UIElement.PreviewMouseRightButtonUp event that stops
		/// the event tunneling.
		/// </summary>
		/// <param name="sender">Sender of the event</param>
		/// <param name="e">Event data</param>
		internal void OnPreviewMouseRightButtonUp(object sender, MouseButtonEventArgs e)
		{
			e.Handled = true;
		}

		/// <summary>
		/// Handler/delegate for the UIElement.PreviewTextInput event that stops the event
		/// tunneling only if the text input was processed successfully.
		/// </summary>
		/// <seealso cref="VirtualKeyboardControllerBase.ProcessPhysicalKeyboardKeyPress"/>
		/// <param name="sender">Sender of the event</param>
		/// <param name="e">Event data</param>
		internal void OnPreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			e.Handled = !ProcessPhysicalKeyboardKeyPress(e.Text, null);
		}

		/// <summary>
		/// Handler/delegate for the IKeyboardView.VirtualKeyDown event that processes the
		/// given key.
		/// </summary>
		/// <seealso cref="VirtualKeyboardControllerBase.ProcessPhysicalKeyboardKeyPress"/>
		/// <param name="sender">Sender of the event</param>
		/// <param name="e">Event data</param>
		internal void OnVirtualKeyDown(object sender, VirtualKeyDownArgs e)
		{
			ProcessVirtualKeyboardKeyPress(e.KeyContent, e.ActionKey);
		}

		#endregion

		#region Protected helper methods

		#region Process keys with/without selection

		/// <summary>
		/// Performs a backspace operation on the specified text given a specified selection of text.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="selectionStart">Character index for the beginning of the selection</param>
		/// <param name="selectionLength">Number of characters in the current selection</param>
		/// <param name="newCaretIndex">Location of the caret index after the backspace has been
		/// performed</param>
		/// <returns>Modified string where the selected text has been removed and an updated 
		/// caret position (zero if text could not be modified)</returns>
		/// <remarks>If the selected text could not be removed, a diagnostic message is written
		/// to the debug console.</remarks>
		protected virtual string ProcessBackKeyWithSelection(string text, int selectionStart, int selectionLength, out int newCaretIndex)
		{
			text = text.Remove(selectionStart, selectionLength);

			newCaretIndex = selectionStart;
			return text;
		}

		/// <summary>
		/// Performs a backspace operation on the specified text at a specified index.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="currentCaretIndex">Caret's position within the string</param>
		/// <param name="newCaretIndex">Location of the caret index after the backspace has been
		/// performed</param>
		/// <returns>Modified string where the character to the left of the caret has 
		/// been removed and an updated caret position (zero if text could not be modified)</returns>
		/// <remarks>
		/// (1) No change is made to the text or caret index if the caret position
		/// is at the beginning of the string.
		/// (2) If the selected text could not be modified, a diagnostic message is written
		/// to the debug console.
		/// </remarks>
		protected virtual string ProcessBackKeyWithoutSelection(string text, int currentCaretIndex, out int newCaretIndex)
		{
			// Caret is already at the left-most position, so there's nothing to "back out."
			if (currentCaretIndex < 1)
			{
				newCaretIndex = currentCaretIndex;
				return text;
			}

			text = text.Remove(currentCaretIndex - 1, 1);

			newCaretIndex = currentCaretIndex - 1;
			return text;
		}

		/// <summary>
		/// Performs a clear operation on the specified text.
		/// </summary>
		/// <param name="text">Text to be cleared</param>
		/// <param name="newCaretIndex">Location of the caret index after the clear has been
		/// performed</param>
		/// <returns>Empty string with a caret position of zero.</returns>
		protected virtual string ProcessClearKey(string text, out int newCaretIndex)
		{
			newCaretIndex = 0;
			return string.Empty;
		}

		/// <summary>
		/// Performs a delete operation on the specified text given a specified selection of text.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="selectionStart">Character index for the beginning of the selection</param>
		/// <param name="selectionLength">Number of characters in the current selection</param>
		/// <param name="newCaretIndex">Location of the caret index after the deletion has been
		/// performed</param>
		/// <returns>Modified string where the selected text has been removed and an updated 
		/// caret position (zero if text could not be modified)</returns>
		/// <remarks>If the selected text could not be removed, a diagnostic message is written
		/// to the debug console.</remarks>
		protected virtual string ProcessDeleteKeyWithSelection(string text, int selectionStart, int selectionLength, out int newCaretIndex)
		{
			text = text.Remove(selectionStart, selectionLength);

			newCaretIndex = selectionStart;
			return text;
		}

		/// <summary>
		/// Performs a delete operation on the specified text at a specified index.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="currentCaretIndex">Caret's position within the string</param>
		/// <param name="newCaretIndex">Location of the caret index after the backspace has been
		/// performed</param>
		/// <returns>Modified string where the character to the right of the caret has 
		/// been removed and an updated caret position (zero if text could not be modified)
		/// </returns>
		/// <remarks>No change is made to the text or caret index if the caret position
		/// is at the end of the string.</remarks>
		protected virtual string ProcessDeleteKeyWithoutSelection(string text, int currentCaretIndex, out int newCaretIndex)
		{
			// Caret is already at the right-most position, so there's nothing to delete.
			if (currentCaretIndex >= text.Length)
			{
				newCaretIndex = currentCaretIndex;
				return text;
			}

			text = text.Remove(currentCaretIndex, 1);

			newCaretIndex = currentCaretIndex;
			return text;
		}

		/// <summary>
		/// Adds a space to the specified text given a specified selection of text.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="selectionStart">Character index for the beginning of the selection</param>
		/// <param name="selectionLength">Number of characters in the current selection</param>
		/// <param name="newCaretIndex">Location of the caret index after the space has been
		/// added</param>
		/// <returns>Modified string where the selected text has been replaced with a space and
		/// an updated caret position (zero if text could not be modified)</returns>
		/// <remarks>If the selected text could not be modified, a diagnostic message is written
		/// to the debug console.</remarks>
		protected virtual string ProcessSpaceKeyWithSelection(string text, int selectionStart, int selectionLength, out int newCaretIndex)
		{
			text = text.Remove(selectionStart, selectionLength).Insert(selectionStart, " ");

			newCaretIndex = selectionStart + 1;
			return text;
		}

		/// <summary>
		/// Adds a space to the specified text at a specified index.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="currentCaretIndex">Caret's position within the string</param>
		/// <param name="newCaretIndex">Location of the caret index after the space has been
		/// added</param>
		/// <returns>Modified string where a space has been added to the right of the caret
		/// and an updated caret position (zero if text could not be modified)</returns>
		/// <remarks>If the selected text could not be modified, a diagnostic message is written
		/// to the debug console.</remarks>
		protected virtual string ProcessSpaceKeyWithoutSelection(string text, int currentCaretIndex, out int newCaretIndex)
		{
			text = text.Insert(currentCaretIndex, " ");

			newCaretIndex = currentCaretIndex + 1;
			return text;
		}

		#endregion

		#region Process single character with/without selection

		/// <summary>
		/// Adds a single character to the specified text given a specified selection of text.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="singleCharacterInput">Single character to be added</param>
		/// <param name="selectionStart">Character index for the beginning of the selection</param>
		/// <param name="selectionLength">Number of characters in the current selection</param>
		/// <param name="newCaretIndex">Location of the caret index after the character has been
		/// added</param>
		/// <returns>Modified string where the selected text has been replaced with a character and
		/// an updated caret position (zero if text could not be modified)</returns>
		/// <remarks>If the selected text could not be modified, a diagnostic message is written
		/// to the debug console.</remarks>
		protected virtual string ProcessSingleCharacterWithSelection(string text, string singleCharacterInput,
			int selectionStart, int selectionLength, out int newCaretIndex)
		{
			text = text.Remove(selectionStart, selectionLength).Insert(selectionStart, singleCharacterInput);

			newCaretIndex = selectionStart + singleCharacterInput.Length;
			return text;
		}

		/// <summary>
		/// Adds a single character to the specified text at a specified index.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="singleCharacterInput">Single character to be added</param>
		/// <param name="currentCaretIndex">Caret's position within the string</param>
		/// <param name="newCaretIndex">Location of the caret index after the character has been
		/// added</param>
		/// <returns>Modified string where the character has been added to the right of the caret
		/// and an updated caret position (zero if text could not be modified)</returns>
		/// <remarks>If the selected text could not be modified, a diagnostic message is written
		/// to the debug console.</remarks>
		protected virtual string ProcessSingleCharacterWithoutSelection(string text, string singleCharacterInput,
			int currentCaretIndex, out int newCaretIndex)
		{
			text = text.Insert(currentCaretIndex, singleCharacterInput);

			newCaretIndex = currentCaretIndex + singleCharacterInput.Length;
			return text;
		}

		#endregion

		#region Virtual helper methods

		/// <summary>
		/// Raises the RequestToCloseEvent.
		/// </summary>
		protected virtual void FireRequestToClose()
		{
			if (RequestToClose != null)
				RequestToClose(this, EventArgs.Empty);
		}

		/// <summary>
		/// Constructs the string resulting from the specified single-character input or key
		/// pressed with respect to the current state of the text box and its caret associated
		/// with this virtual keyboard.
		/// </summary>
		/// <param name="singleCharInput">Single character to be added</param>
		/// <param name="keyPressed">Key pressed</param>
		/// <param name="pendingCaretIndex">New caret position within the pending keyboard
		/// value</param>
		/// <returns>Text box string modified by the given character or key</returns>
		/// <remarks>Most of the work is done by the virtual Process* methods.</remarks>
		protected virtual string GetPendingKeyboardValue(string singleCharInput, Key? keyPressed, out int pendingCaretIndex)
		{
			string updatedKeyboardValue = RequesterAsTextBox.Text;
			int caretIndex = RequesterAsTextBox.CaretIndex;
			int selectionStart = RequesterAsTextBox.SelectionStart;
			int selectionLength = RequesterAsTextBox.SelectionLength;

			// Special key.
			if (keyPressed.HasValue && string.IsNullOrEmpty(singleCharInput))
			{
				switch (keyPressed.Value)
				{
					case Key.Back:
						updatedKeyboardValue = selectionLength > 0 ? ProcessBackKeyWithSelection(updatedKeyboardValue, selectionStart, selectionLength, out caretIndex) : ProcessBackKeyWithoutSelection(updatedKeyboardValue, caretIndex, out caretIndex);
						break;

					case Key.Clear:
						updatedKeyboardValue = ProcessClearKey(updatedKeyboardValue, out caretIndex);
						break;

					case Key.Delete:
						updatedKeyboardValue = selectionLength > 0 ? ProcessDeleteKeyWithSelection(updatedKeyboardValue, selectionStart, selectionLength, out caretIndex) : ProcessDeleteKeyWithoutSelection(updatedKeyboardValue, caretIndex, out caretIndex);
						break;

					case Key.Space:
						updatedKeyboardValue = selectionLength > 0 ? ProcessSpaceKeyWithSelection(updatedKeyboardValue, selectionStart, selectionLength, out caretIndex) : ProcessSpaceKeyWithoutSelection(updatedKeyboardValue, caretIndex, out caretIndex);
						break;
				}
			}
			// No special key; just single character change.
			else if (!string.IsNullOrEmpty(singleCharInput))
			{
				updatedKeyboardValue = selectionLength > 0 ? ProcessSingleCharacterWithSelection(updatedKeyboardValue, singleCharInput, selectionStart, selectionLength, out caretIndex) : ProcessSingleCharacterWithoutSelection(updatedKeyboardValue, singleCharInput, caretIndex, out caretIndex);
			}

			pendingCaretIndex = caretIndex;

			return updatedKeyboardValue;
		}

		/// <summary>
		/// Deals with the specified input from the physical keyboard.
		/// </summary>
		/// <param name="singleCharInput">Single character of text entered via the physical keyboard</param>
		/// <param name="keyPressed">Specific key (e.g., escape) pressed on the physical keyboard</param>
		/// <returns>True if (1) specified key is Key.Escape or Key.Capital, (2) if the pending keyboard
		/// value based on the specified input matches the keyboard's regular expression; false 
		/// otherwise</returns>
		/// <remarks>Does not change the caret position</remarks>
		protected virtual bool ProcessPhysicalKeyboardKeyPress(string singleCharInput, Key? keyPressed)
		{
			var isKeypressValid = true;
			int newCaretIndex;
			if (keyPressed.HasValue)
			{
				switch (keyPressed.Value)
				{
					case Key.Escape: FireRequestToClose(); break;
					case Key.Capital: 
						// ReSharper disable once CanBeReplacedWithTryCastAndCheckForNull
						if (VisualView is IRefreshableKeyboardView)
							((IRefreshableKeyboardView)VisualView).Refresh(); 
						break;
					case Key.Enter:
						// ReSharper disable once CanBeReplacedWithTryCastAndCheckForNull
						if (VisualView is IRefreshableKeyboardView)
							((IRefreshableKeyboardView)VisualView).Refresh();
						break;
					case Key.Space: singleCharInput = " "; break;
				}
			}

			if (!string.IsNullOrEmpty(singleCharInput))
				isKeypressValid = IsValidInputValue(GetPendingKeyboardValue(singleCharInput, keyPressed, out newCaretIndex));

			return isKeypressValid;
		}

		/// <summary>
		/// Processes the specified input from the virtual keyboard.
		/// </summary>
		/// <param name="singleCharInput">Single character of text entered via the virtual keyboard</param>
		/// <param name="keyPressed">Specific key (e.g., escape) pressed on the virtual keyboard</param>
		/// <remarks>
		/// (1) The following actions are taken if a Key is specified:
		///     Key.Capital -- toggles caps lock on the physical keyboard
		///     Key.End -- moves the caret to the right-most end of the string
		///     Key.Home -- moves the caret to the left-most end of the string
		///     Key.Left -- moves the caret left on position (if possible)
		///     Key.Right -- moves the caret right on position (if possible)
		///     Key.Tab -- invokes VirtualKeyboardManager.MoveNextFocus()
		/// (2) Unless Key.Capital is specified, the text box's text and caret position
		/// will be updated based on the specified input/key.
		/// </remarks>
		protected virtual void ProcessVirtualKeyboardKeyPress(string singleCharInput, Key? keyPressed)
		{
			Action systemKeyAction = null;
			int newCaretIndex;
			var newTextValue = GetPendingKeyboardValue(singleCharInput, keyPressed, out newCaretIndex);

			if (keyPressed.HasValue)
			{
				switch (keyPressed.Value)
				{
					case Key.Capital:
						systemKeyAction = () =>
						{
							keybd_event(CapsLockKey, 0x45, ExtendedKey, (UIntPtr)0);
							keybd_event(CapsLockKey, 0x45, ExtendedKey | KeyUpEvent, (UIntPtr)0);
						};
						break;

					case Key.End:
						newCaretIndex = RequesterAsTextBox.Text.Length;
						break;

					case Key.F1:
						// ReSharper disable once AssignNullToNotNullAttribute
						systemKeyAction = () => _requesterAsTextBox.RaiseEvent(new KeyEventArgs(Keyboard.PrimaryDevice,
							PresentationSource.FromVisual(_requesterAsTextBox), 0, Key.F1) { RoutedEvent = Keyboard.KeyDownEvent });
						break;

					case Key.Home:
						newCaretIndex = 0;
						break;

					case Key.Left:
						newCaretIndex = RequesterAsTextBox.CaretIndex - 1 > 0 ? --RequesterAsTextBox.CaretIndex : 0;
						break;

					case Key.Right:
						newCaretIndex = RequesterAsTextBox.CaretIndex + 1 < RequesterAsTextBox.Text.Length ?
							++RequesterAsTextBox.CaretIndex : RequesterAsTextBox.Text.Length;
						break;

					case Key.Tab:
						systemKeyAction = VirtualKeyboardManager.MoveNextFocus;
						break;
				}
			}

			if (systemKeyAction != null)
				systemKeyAction();
			else
				UpdateTextAndCaret(newTextValue, newCaretIndex);
		}

		#endregion

		#region Non-virtual helper methods

		/// <summary>
		/// Determines if the specified string value matches the validation regular expression
		/// associated with this virtual keyboard.
		/// </summary>
		/// <param name="value">Input value to validate</param>
		/// <returns>True if the value matches the validation regular expression (also true if 
		/// (1) the validation regular expression is null/empty, and (2) if the specified value 
		/// is null/whitespace); false otherwise</returns>
		protected bool IsValidInputValue(string value)
		{
			if (string.IsNullOrEmpty(ValidationRegEx) || string.IsNullOrWhiteSpace(value))
				return true;

			return Regex.IsMatch(value, ValidationRegEx);
		}

		/// <summary>
		/// Sets the text and caret index of this virtual keyboard's requester (TextBox)
		/// to the specified values if the specified text value is valid.
		/// </summary>
		/// <param name="newTextValue">Text for the text box</param>
		/// <param name="newCaretIndex">Caret position for the text box</param>
		/// <remarks>Writes information to the debugging console of the text or caret
		/// position could not be set</remarks>
		protected void UpdateTextAndCaret(string newTextValue, int newCaretIndex)
		{
			if (!IsValidInputValue(newTextValue) || RequesterAsTextBox == null) return;

			if (RequesterAsTextBox.Text != newTextValue)
				RequesterAsTextBox.SetCurrentValue(TextBox.TextProperty, newTextValue);

			RequesterAsTextBox.CaretIndex = newCaretIndex;
		}

		#endregion

		#endregion
	}
}
