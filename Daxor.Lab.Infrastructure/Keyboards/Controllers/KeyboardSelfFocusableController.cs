﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Daxor.Lab.Infrastructure.Keyboards.Views;

namespace Daxor.Lab.Infrastructure.Keyboards.Controllers
{
	/// <summary>
	/// Virtual keyboard controller that handles physical and virtual input for a
	/// self-focusable (i.e., the keyboard itself has a textbox with input focus)
	/// keyboard.
	/// </summary>
	public class KeyboardSelfFocusableController : KeyboardSelfClosableController
	{
		#region Ctor

		/// <summary>
		/// Creates a new virtual self-focusable keyboard controller for the specified 
		/// keyboard view and specified keyboard identifier.
		/// </summary>
		/// <seealso cref="VirtualKeyboardControllerBase"/>
		/// <param name="view">Keyboard view associated with this controller</param>
		/// <param name="uniqueId">Keyboard identifier of the virtual keyboard controlled
		/// by this controller</param>
		public KeyboardSelfFocusableController(IKeyboardView view, string uniqueId) : base(view, uniqueId){}

		#endregion

		#region Overrides

		/// <summary>
		/// Activates the virtual keyboard invoked by a given FrameworkElement.
		/// </summary>
		/// <param name="requester">UI element that requested the keyboard to be activated</param>
		public override void Activate(FrameworkElement requester)
		{
			// Run through the base activation.
			base.Activate(requester);

			// Determine if the focused element after the base activation. It should be the textbox
			// on the keyboard view rather than the element that invoked this keyboard.
			// ReSharper disable once AssignNullToNotNullAttribute
			var focusedElement = FocusManager.GetFocusedElement(VisualView as DependencyObject);
			if ((focusedElement != null) && (!Equals(focusedElement, requester)))
				RequesterAsTextBox = focusedElement as TextBox;
		}

		/// <summary>
		/// Deals with the specified input from the physical keyboard.
		/// </summary>
		/// <param name="singleCharInput">Single character of text entered via the physical keyboard</param>
		/// <param name="keyPressed">Specific key (e.g., escape) pressed on the physical keyboard</param>
		/// <returns>True if the keypress should be let through for normal key event processing;
		/// false if the keypress has already been handled by this controller and should not
		/// be let through for further event processing</returns>
		/// <remarks>
		/// This method causes the physical Tab key to be ignored.
		/// </remarks>
		protected override bool ProcessPhysicalKeyboardKeyPress(string singleCharInput, Key? keyPressed)
		{
			// Ignores physical Tab key processing
			if (keyPressed.HasValue && (keyPressed.Value == Key.Tab))
				return false;

			return base.ProcessPhysicalKeyboardKeyPress(singleCharInput, keyPressed);
		}

		#endregion
	}
}
