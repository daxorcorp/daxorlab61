﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Daxor.Lab.Infrastructure.Keyboards.Views;
using System.Text.RegularExpressions;

namespace Daxor.Lab.Infrastructure.Keyboards.Controllers
{
	/// <summary>
	/// Virtual keyboard controller that handles physical and virtual input for the
	/// duration keyboard.
	/// </summary>
	public class KeyboardDurationController : VirtualKeyboardControllerBase
	{
		#region Constants

		/// <summary>
		/// Regular expression for invalid duration characters.
		/// </summary>
		private const string InvalidDurationCharactersRegex = "[^0-9:]";

		/// <summary>
		/// Format string for zero minutes and some number of seconds.
		/// </summary>
		private const string SecondsFormatString = "0:{0:00}";

		/// <summary>
		/// String used to separate minutes and seconds.
		/// </summary>
		private const string TimeSeparator = ":";

		/// <summary>
		/// Character used to separate minutes and seconds.
		/// </summary>
		private const char TimeSeparatorChar = ':';

		/// <summary>
		/// Regular expression for valid duration characters.
		/// </summary>
		private const string ValidDurationCharactersRegex = "[0-9:]";

		/// <summary>
		/// Zero minutes and zero seconds.
		/// </summary>
		private const string ZeroTimeValue = "0:00";

		#endregion

		#region Ctor

		/// <summary>
		/// Creates a new virtual duration keyboard controller for the specified keyboard
		/// view and specified keyboard identifier.
		/// </summary>
		/// <seealso cref="VirtualKeyboardControllerBase"/>
		/// <param name="keyboardView">Keyboard view associated with this controller</param>
		/// <param name="keyboardId">Keyboard identifier of the virtual keyboard controlled
		/// by this controller</param>
		public KeyboardDurationController(IKeyboardView keyboardView, string keyboardId)
			: base(keyboardView, keyboardId) { }

		#endregion

		#region Key handling overrides

		#region Back key

		/// <summary>
		/// Processes the back key for the specified text given a specified selection of text
		/// by preventing modification to the text or caret position.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="selectionStart">Character index for the beginning of the selection</param>
		/// <param name="selectionLength">Number of characters in the current selection</param>
		/// <param name="newCaretIndex">Location of the caret index after the backspace has been
		/// performed</param>
		/// <returns>Specified (unchanged) text and a caret index of selectionStart</returns>
		/// <remarks>Because this controller takes extra steps to deal with manipulating the
		/// duration value that contains a time separator (":"), modifying selected text is
		/// not supported.</remarks>
		protected override string ProcessBackKeyWithSelection(string text, int selectionStart, int selectionLength, out int newCaretIndex)
		{
			newCaretIndex = selectionStart;
			return text;
		}

		/// <summary>
		/// Performs a backspace operation on the specified text at a specified index.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="currentCaretIndex">Caret's position within the string</param>
		/// <param name="newCaretIndex">Location of the caret index after the backspace has been
		/// performed</param>
		/// <returns>Modified string where the character to the left of the caret has 
		/// been removed and an updated caret position (unchanged if the text was not
		/// modified)</returns>
		/// <remarks>
		/// (1) No change is made to the text or caret index if the caret position
		/// is at the beginning of the string.
		/// (2) If the caret is to the right of the time separator, this method attempts
		/// to remove the value to the left of the caret. Ex: "12:|34" results in
		/// "1:|34" rather than "12|34".
		/// (3) No change is made if the backspace operation makes no change to the
		/// string.
		/// (4) If the backspace operation leaves the duration as "0:00", the
		/// duration is set to string.Empty.
		/// </remarks>
		protected override string ProcessBackKeyWithoutSelection(string text, int currentCaretIndex, out int newCaretIndex)
		{
			// The caret can be anywhere in the string, so separate the string into two parts.
			// Ex: (Where "|" is the caret) 12:3|4 --> "12:3" and "4"
			string textToTheLeft;
			string textToTheRight;
			StringSplit(text, currentCaretIndex, out textToTheLeft, out textToTheRight);

			var length = textToTheLeft.Length;
			if (length == 0)
			{
				// Nothing to delete; ex: "|12:34"
				newCaretIndex = currentCaretIndex;
				return text;
			}

			// Can't delete the time separator, so prepare to look at the digit to the left of it
			// by removing the separator. (Ex:  12:|34 --> 12|34)
			if (textToTheLeft.EndsWith(TimeSeparator))
				textToTheLeft = textToTheLeft.Substring(0, textToTheLeft.Length - 1);

			// Remove the right-most value. (Ex: 12|34 --> 1|34)
			if (textToTheLeft.Length > 0)
				textToTheLeft = textToTheLeft.Substring(0, textToTheLeft.Length - 1);

			var newValue = textToTheLeft + textToTheRight;

			var formattedValue = FormatValueWithTimeSeparator(newValue);
			if (currentCaretIndex == text.Length)   // Backspacing at the far-right end of the string
				// Backspace just left an empty time; zero out and return.
				if (formattedValue == ZeroTimeValue)
				{
					newCaretIndex = 0;
					formattedValue = string.Empty;
					return formattedValue;
				}
				else
				{
					newCaretIndex = formattedValue.Length;
				}
			else if (formattedValue == text)        // Backspace did nothing to the string.
				newCaretIndex = currentCaretIndex;
			else if (formattedValue.Length == text.Length)
				newCaretIndex = currentCaretIndex;
			else
			{
				newCaretIndex = currentCaretIndex - 1 < 0 ? 0 : currentCaretIndex - 1;
			}
			return formattedValue;
		}

		#endregion

		#region Delete key

		/// <summary>
		/// Processes the delete key for the specified text given a specified selection of text
		/// by preventing modification to the text or caret position.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="selectionStart">Character index for the beginning of the selection</param>
		/// <param name="selectionLength">Number of characters in the current selection</param>
		/// <param name="newCaretIndex">Location of the caret index after the deletion has been
		/// performed</param>
		/// <returns>Specified (unchanged) text and a caret index of selectionStart</returns>
		/// <remarks>Because this controller takes extra steps to deal with manipulating the
		/// duration value that contains a time separator (":"), modifying selected text is
		/// not supported.</remarks>
		protected override string ProcessDeleteKeyWithSelection(string text, int selectionStart, int selectionLength, out int newCaretIndex)
		{
			newCaretIndex = selectionStart;
			return text;
		}

		/// <summary>
		/// Performs a delete operation on the specified text at a specified index.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="currentCaretIndex">Caret's position within the string</param>
		/// <param name="newCaretIndex">Location of the caret index after the backspace has been
		/// performed</param>
		/// <returns>Modified string where the character to the right of the caret has 
		/// been removed and an updated caret position (unchanged if the text was not
		/// modified)</returns>
		/// <remarks>
		/// (1) No change is made to the text or caret index if the caret position
		/// is at the end of the string.
		/// (2) If the caret is to the left of the time separator, this method attempts
		/// to remove the value to the right of the caret. Ex: "12|:34" results in
		/// "1|:24" rather than "12|34".
		/// </remarks>
		protected override string ProcessDeleteKeyWithoutSelection(string text, int currentCaretIndex, out int newCaretIndex)
		{
			var length = text.Length;
			if (currentCaretIndex == length)
			{
				// Nothing to delete
				// Ex: (Where "|" is the caret) 12:34|
				newCaretIndex = currentCaretIndex;
				return text;
			}

			// Move the caret over one position. Ex: 12:|34 --> 12:3|4
			newCaretIndex = currentCaretIndex + 1;

			// Can't backspace the time separator, so prepare to look at the digit to the right of it.
			// Ex: 12:|34 --> 12:3|4
			if (text.Substring(newCaretIndex - 1, 1) == TimeSeparator)
				newCaretIndex++;

			var formattedValue = ProcessBackKeyWithoutSelection(text, newCaretIndex, out newCaretIndex);

			// Handles "|3:21" --> "0:|21" case.
			if (!text.StartsWith("0") && formattedValue.StartsWith("0"))
				newCaretIndex = 2;

			return formattedValue;
		}

		#endregion

		#region Space key

		/// <summary>
		/// Processes the space key for the specified text given a specified selection of text
		/// by preventing modification to the text or caret position.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="selectionStart">Character index for the beginning of the selection</param>
		/// <param name="selectionLength">Number of characters in the current selection</param>
		/// <param name="newCaretIndex">Location of the caret index after the space has been
		/// added</param>
		/// <returns>Specified (unchanged) text and a caret index of selectionStart</returns>
		/// <remarks>Whitespace is not part of a valid duration string.</remarks>
		protected override string ProcessSpaceKeyWithSelection(string text, int selectionStart, int selectionLength, out int newCaretIndex)
		{
			newCaretIndex = selectionStart;
			return text;
		}

		/// <summary>
		/// Processes the space key for the specified text at a specified index by preventing
		/// modification to the text or caret position.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="currentCaretIndex">Caret's position within the string</param>
		/// <param name="newCaretIndex">Location of the caret index after the space has been
		/// added</param>
		/// <returns>Specified (unchanged) text and a caret index of selectionStart</returns>
		/// <remarks>Whitespace is not part of a valid duration string.</remarks>
		protected override string ProcessSpaceKeyWithoutSelection(string text, int currentCaretIndex, out int newCaretIndex)
		{
			newCaretIndex = currentCaretIndex;
			return text;
		}

		#endregion

		#region Single character

		/// <summary>
		/// Processes a single character for the specified text given a specified selection of text
		/// by preventing modification to the text or caret position.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="singleCharacterInput">Single character to be added</param>
		/// <param name="selectionStart">Character index for the beginning of the selection</param>
		/// <param name="selectionLength">Number of characters in the current selection</param>
		/// <param name="newCaretIndex">Location of the caret index after the character has been
		/// added</param>
		/// <returns>Specified (unchanged) text and a caret index of selectionStart</returns>
		/// <remarks>Because this controller takes extra steps to deal with manipulating the
		/// duration value that contains a time separator (":"), modifying selected text is
		/// not supported.</remarks>
		protected override string ProcessSingleCharacterWithSelection(string text, string singleCharacterInput,
			int selectionStart, int selectionLength, out int newCaretIndex)
		{
			newCaretIndex = selectionStart;
			return text;
		}

		/// <summary>
		/// Adds a single character to the specified text at a specified index.
		/// </summary>
		/// <param name="text">Text to be modified</param>
		/// <param name="singleCharacterInput">Single character to be added</param>
		/// <param name="currentCaretIndex">Caret's position within the string</param>
		/// <param name="newCaretIndex">Location of the caret index after the character has been
		/// added</param>
		/// <returns>Modified string where the character has been added to the right of the caret
		/// and an updated caret position (unchanged if the text was not
		/// modified)</returns>
		/// <remarks>If the selected text could not be modified, a diagnostic message is written
		/// to the debug console.</remarks>
		protected override string ProcessSingleCharacterWithoutSelection(string text, string singleCharacterInput,
			int currentCaretIndex, out int newCaretIndex)
		{
			var caretSpan = text.Length - currentCaretIndex;

			text = text.Insert(currentCaretIndex, singleCharacterInput);

			var formattedValue = FormatValueWithTimeSeparator(text);
			newCaretIndex = formattedValue.Length - caretSpan;
			return formattedValue;
		}

		#endregion

		#region Physical/virtual key press

		/// <summary>
		/// Deals with the specified input from the physical keyboard.
		/// </summary>
		/// <param name="singleCharInput">Single character of text entered via the physical keyboard</param>
		/// <param name="keyPressed">Specific key (e.g., escape) pressed on the physical keyboard</param>
		/// <returns>True if the keypress should be let through for normal key event processing;
		/// false if the keypress has already been handled by this controller and should not
		/// be let through for further event processing</returns>
		/// <remarks>
		/// (1) The following actions are taken if a Key is specified:
		///     Key.End -- moves the caret to the right-most end of the string
		///     Key.Escape -- fires the RequestToClose event
		///     Key.Home -- moves the caret to the left-most end of the string
		///     Key.Left -- moves the caret left on position (if possible)
		///     Key.Right -- moves the caret right on position (if possible)
		/// (2) Keys other than the ones mentioned above will be evaluated as 
		///     potential input to the duration keyboard.
		/// (3) If the input does not yield a valid duration value, the caret
		///     position and text values will remain unchanged.
		/// </remarks>
		protected override bool ProcessPhysicalKeyboardKeyPress(string singleCharInput, Key? keyPressed)
		{
			var letKeyDownEventContinue = false;
			var needToGetPendingValue = true;
			var newCaretIndex = 0;

			if (keyPressed.HasValue)
			{
				// The name of the key is used for getting the pending value. As this keyboard
				// deals with numbers, special consideration is needed for those keys
				// (e.g., Keys.D0 through Keys.D9, Keys.Numpad0 through Keys.Numpad9).
				var converter = new KeyConverter();
				string keyAsString;
				try
				{
					keyAsString = converter.ConvertToString(keyPressed);
				}
				catch
				{
					keyAsString = string.Empty;
				}

				switch (keyPressed.Value)
				{
					case Key.End:
						newCaretIndex = RequesterAsTextBox.Text.Length;
						needToGetPendingValue = false;
						break;

					case Key.Return:
						FireRequestToClose();
						return true; // No further processing needed because the keyboard will be closing.

					case Key.Home:
						newCaretIndex = 0;
						needToGetPendingValue = false;
						break;

					case Key.Left:
						newCaretIndex = RequesterAsTextBox.CaretIndex - 1 > 0 ? --RequesterAsTextBox.CaretIndex : 0;
						needToGetPendingValue = false;
						break;

					case Key.Right:
						newCaretIndex = RequesterAsTextBox.CaretIndex + 1 < RequesterAsTextBox.Text.Length ?
							++RequesterAsTextBox.CaretIndex : RequesterAsTextBox.Text.Length;
						needToGetPendingValue = false;
						break;

					case Key.Tab:
						letKeyDownEventContinue = true;
						needToGetPendingValue = false;
						break;

					default:
						// Ignore anything that's not a single character or one of the numeric
						// keypad digits.
						// ReSharper disable once PossibleNullReferenceException
						if (keyAsString.StartsWith("NumPad"))
							singleCharInput = keyAsString.Substring(6, 1);
						else if (keyAsString.Length == 1)
							singleCharInput = keyAsString;
						break;
				}
			}

			if (!needToGetPendingValue)
			{
				UpdateTextAndCaret(RequesterAsTextBox.Text, newCaretIndex);
				return letKeyDownEventContinue;
			}

			var newTextValue = GetPendingKeyboardValue(singleCharInput, keyPressed, out newCaretIndex);
			if (IsValidInputValue(newTextValue))
				UpdateTextAndCaret(newTextValue, newCaretIndex);

			return false;
		}

		/// <summary>
		/// Processes the specified input from the virtual keyboard.
		/// </summary>
		/// <param name="singleCharacterInput">Single character of text entered via the virtual keyboard</param>
		/// <param name="keyPressed">Specific key (e.g., escape) pressed on the virtual keyboard</param>
		/// <remarks>
		/// If Key.Escape is specified, the RequestToClose event will be fired. Otherwise
		/// VirtualKeyboardControllerBase.ProcessVirtualKeyboardKeyPress() processes the input.
		/// </remarks>
		protected override void ProcessVirtualKeyboardKeyPress(string singleCharacterInput, Key? keyPressed)
		{
			if ((keyPressed != null) && ((keyPressed.Value == Key.Escape) || (keyPressed.Value == Key.Return)))
				FireRequestToClose();
			else
				base.ProcessVirtualKeyboardKeyPress(singleCharacterInput, keyPressed);
		}

		#endregion

		#endregion

		#region Private helper methods

		/// <summary>
		/// Formats the specified value so that the digits represent a valid duration.
		/// </summary>
		/// <param name="value">Value to format</param>
		/// <returns>Formatted/valid version of the given value</returns>
		/// <remarks>
		/// 1. The given value may or may not contain a time separator. That is,
		///    "1234" and "12:34" both result in the same output from this method.
		/// 2. Any non-numeric or non-colon characters will be ignored.</remarks>
		private string FormatValueWithTimeSeparator(string value)
		{
			string valueWithSeparator;

			if (Regex.IsMatch(value, InvalidDurationCharactersRegex))
			{
				var newString = value.TakeWhile(c => Regex.IsMatch(c.ToString(CultureInfo.InvariantCulture), ValidDurationCharactersRegex));
				var sb = new StringBuilder();
				sb.Append(newString);
				value = sb.ToString();
			}

			if (!value.Contains(TimeSeparator))
			{
				// Value to format doesn't have a separator already; ex: "23" instead of "0:23"
				if (value.Length < 3)
				{
					// One- or two-digit value means we just have seconds to format.
					int valueAsInt;
					int.TryParse(value, out valueAsInt);
					valueWithSeparator = String.Format(SecondsFormatString, valueAsInt);
				}
				else
				{
					// Two- or more-digit value means we have minutes and seconds to format.
					var length = value.Length;
					valueWithSeparator = value.Substring(0, length - 2) + TimeSeparator + value.Substring(length - 2, 2);
				}
			}
			else
			{
				// Value to format does have a separator; however, that separator could be anywhere in the
				// value, so we need to put it in the right place.

				// Strip out the separator and leading zeroes.
				var trimmedValueWithoutSeparatorAsInt = RemoveTimeSeparatorAndTrim(value);
				var trimmedValueWithoutSeparator = trimmedValueWithoutSeparatorAsInt.ToString(CultureInfo.InvariantCulture);

				var length = trimmedValueWithoutSeparator.Length;

				if (trimmedValueWithoutSeparator.Length < 3)
				{
					// One- or two-digit value means we just have seconds to format.
					valueWithSeparator = String.Format(SecondsFormatString, trimmedValueWithoutSeparatorAsInt);
				}
				else
				{
					// Two- or more-digit value means we have minutes and seconds to format.
					var numMinutesChars = length - 2;
					var minutesPart = trimmedValueWithoutSeparator.Substring(0, numMinutesChars);
					var secondsPart = trimmedValueWithoutSeparator.Substring(numMinutesChars, 2);
					valueWithSeparator = minutesPart + TimeSeparator + secondsPart;
				}
			}

			return valueWithSeparator;
		}

		/// <summary>
		/// Removes the time separator from the specified value.
		/// </summary>
		/// <param name="value">Value from which to remove the time separator</param>
		/// <returns>Value with the time separator removed</returns>
		private static string RemoveTimeSeparator(string value)
		{
			var valueWithoutSeparator = new StringBuilder();
			foreach (var c in value.Where(c => c != TimeSeparatorChar))
			    valueWithoutSeparator.Append(c);

			return valueWithoutSeparator.ToString();
		}

		/// <summary>
		/// Removes the time separator as well as any leading zeroes from the specified value.
		/// </summary>
		/// <param name="value">Value from which to remove the time separator and leading zeroes</param>
		/// <returns>Value with the time separator and leading zeroes removed</returns>
		private static int RemoveTimeSeparatorAndTrim(string value)
		{
			var valueWithoutSeparator = RemoveTimeSeparator(value);
			int valueWithoutSeparatorAsInt;
			int.TryParse(valueWithoutSeparator, out valueWithoutSeparatorAsInt);
			return valueWithoutSeparatorAsInt;
		}

		/// <summary>
		/// Splits a specified string into left and right components based on a given split point (index).
		/// </summary>
		/// <param name="stringToSplit">String to split</param>
		/// <param name="splitPoint">Index at which the split should take place</param>
		/// <param name="leftPart">Characters to the left of the split point</param>
		/// <param name="rightPart">Characters to the right of the split point</param>
		private static void StringSplit(string stringToSplit, int splitPoint, out string leftPart, out string rightPart)
		{
			leftPart = stringToSplit.Substring(0, splitPoint);
			rightPart = stringToSplit.Substring(splitPoint);
		}

		#endregion
	}
}
