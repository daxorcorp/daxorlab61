﻿using System;
using System.Windows;
using Daxor.Lab.Infrastructure.Keyboards.Views;

namespace Daxor.Lab.Infrastructure.Keyboards
{
	/// <summary>
	/// Represents a virtual (i.e., on-screen) keyboard.
	/// </summary>
	public interface IVirtualKeyboard
	{
		#region Properties
		
		/// <summary>
		/// Gets the unique identifier for the virtual keyboard.
		/// </summary>
		// ReSharper disable once InconsistentNaming
		object KeyboardID { get; }                         
		
		/// <summary>
		/// Gets the UI element that requested the keyboard to be activated.
		/// </summary>
		FrameworkElement Requester { get; }
	  
		/// <summary>
		/// Gets the keyboard view (i.e., visual) for the virtual keyboard.
		/// </summary>
		IKeyboardView VisualView { get; }

		#endregion

		#region Methods
		
		/// <summary>
		/// Activates the virtual keyboard invoked by a given FrameworkElement.
		/// </summary>
		/// <param name="requester">UI element that requested the keyboard to be activated</param>
		void Activate(FrameworkElement requester);
		
		/// <summary>
		/// Deactivates the virtual keyboard.
		/// </summary>
		void Deactivate();
	   
		#endregion

		#region Events

		/// <summary>
		/// Occurs when the virtual keyboard is requesting to be closed.
		/// </summary>
		event EventHandler RequestToClose;

		#endregion
	}
}
