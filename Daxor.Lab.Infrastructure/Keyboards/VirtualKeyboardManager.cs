﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Keyboards.Common;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using System.Windows.Controls;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events.Payloads;

namespace Daxor.Lab.Infrastructure.Keyboards
{
	/// <summary>
	/// Provides services for managing virtual (i.e., on-screen) keyboards. Such services include 
	/// exposing attached properties, registering keyboards, and opening/closing keyboards.
	/// </summary>
	public static class VirtualKeyboardManager
	{
		#region Fields

		/// <summary>
		/// Private instance of a listener that can respond to the requesting FrameworkElement
		/// getting keyboard focus.
		/// </summary>
		private static readonly RequesterEventListener RequesterPreviewGotFocusListener = new RequesterEventListener(
			(requester, e) =>
			{
				OpenKeyboard(requester, e);
				var txtBox = requester as TextBox;
				if (txtBox == null) return;
				txtBox.CaretIndex = txtBox.Text.Length;
				txtBox.ScrollToHorizontalOffset(Double.MaxValue);
			});

		/// <summary>
		/// Private flag that prevents multiple calls to DeactivateKeyboard() on the
		/// same keyboard from causing problems.
		/// </summary>
		private static bool _isProcessingDeactivation;

		/// <summary>
		/// Private object used for preventing multiple threads from trying to deal with
		/// the keyboard at the same time.
		/// </summary>
		private static readonly object CriticalSectionLock = new object();

		/// <summary>
		/// Private instance of the active (last known) virtual keyboard.
		/// </summary>
		private static IVirtualKeyboard _activeVirtualKeyboard;

		/// <summary>
		/// Private IRegion instance associated with the virtual keyboard.
		/// </summary>
		private static IRegion _keyboardRegion;

		/// <summary>
		/// Private dictionary of keyboards (identifier/IVirtualKeyboard pairs) registered with the 
		/// virtual keyboard manager.
		/// </summary>
		private static readonly Dictionary<object, IVirtualKeyboard> RegisteredKeyboards = new Dictionary<object, IVirtualKeyboard>();

		/// <summary>
		/// Private TraversalRequest instance that defines how the keyboard focus should change when
		/// MoveNextFocus() is called.
		/// </summary>
		private static readonly TraversalRequest TraversalRequest = new TraversalRequest(FocusNavigationDirection.Next);

		#endregion

		#region Properties

		/// <summary>
		/// Gets or sets the event aggregator.
		/// </summary>
		private static IEventAggregator EventAggregator
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the keyboard region (IRegion).
		/// </summary>
		/// <exception cref="NotSupportedException">Caller attempts to set the keyboard region when
		/// one has already been defined</exception>
		private static IRegion KeyboardRegion
		{
			get
			{
				return _keyboardRegion;
			}
			set
			{
				if (_keyboardRegion != null)
					throw new NotSupportedException("Unable to set the keyboard region because it has already been defined.");

				_keyboardRegion = value;
			}
		}

		#endregion

		#region Attached Properties

		#region CloseKeyboardCommand

		/// <summary>
		/// Dependency property that pertains to an ICommand that should be executed when the
		/// virtual keyboard is closed.
		/// </summary>
		public static readonly DependencyProperty CloseKeyboardCommandProperty =
			DependencyProperty.RegisterAttached("CloseKeyboardCommand", typeof(ICommand), typeof(VirtualKeyboardManager), new UIPropertyMetadata(null));

		/// <summary>
		/// Gets the ICommand associated with the keyboard.
		/// </summary>
		/// <param name="obj">Requester of the virtual keyboard</param>
		/// <returns>ICommand associated with the keyboard</returns>
		/// <exception cref="InvalidOperationException">CloseKeyboardCommandProperty value is 
		/// invalid or CloseKeyboardCommandProperty does not exist</exception>
		public static ICommand GetCloseKeyboardCommand(DependencyObject obj)
		{
			return (ICommand)obj.GetValue(CloseKeyboardCommandProperty);
		}

		/// <summary>
		/// Sets the ICommand associated with the keyboard.
		/// </summary>
		/// <param name="obj">Requester of the virtual keyboard</param>
		/// <param name="value">ICommand to use with the keyboard</param>
		/// <exception cref="InvalidOperationException">CloseKeyboardCommandProperty value is 
		/// invalid or CloseKeyboardCommandProperty does not exist</exception>
		/// <exception cref="ArgumentException">Specified value is not the correct type</exception>
		public static void SetCloseKeyboardCommand(DependencyObject obj, ICommand value)
		{
			obj.SetValue(CloseKeyboardCommandProperty, value);
		}

		#endregion

		#region InputFormat

		/// <summary>
		/// Dependency property that pertains to the input format (i.e., regular expression) for the value
		/// returned by the keyboard.
		/// </summary>
		public static readonly DependencyProperty InputFormatProperty = DependencyProperty.RegisterAttached(
			"InputFormat", 
			typeof(string), 
			typeof(VirtualKeyboardManager), 
			new UIPropertyMetadata(string.Empty));

		/// <summary>
		/// Gets the input format (i.e., regular expression) associated with the keyboard.
		/// </summary>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <returns>Input format associated with the keyboard</returns>
		/// <exception cref="InvalidOperationException">InputFormatProperty value is 
		/// invalid or InputFormatProperty does not exist</exception>
		public static string GetInputFormat(DependencyObject requester)
		{
			return (string)requester.GetValue(InputFormatProperty);
		}

		/// <summary>
		/// Sets the input format (i.e., regular expression) associated with the keyboard.
		/// </summary>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <param name="inputFormat">Input format to use with the keyboard</param>
		/// <exception cref="InvalidOperationException">InputFormatProperty value is 
		/// invalid or InputFormatProperty does not exist</exception>
		/// <exception cref="ArgumentException">Specified value is not the correct type</exception>
		public static void SetInputFormat(DependencyObject requester, string inputFormat)
		{
			requester.SetValue(InputFormatProperty, inputFormat);
		}

		#endregion

		#region KeyboardID

		/// <summary>
		/// Dependency property that pertains to identifier of the virtual keyboard to be displayed.
		/// </summary>
		// ReSharper disable InconsistentNaming
		public static readonly DependencyProperty KeyboardIDProperty = DependencyProperty.RegisterAttached(
			"KeyboardID", 
			typeof(object), 
			typeof(VirtualKeyboardManager), 
			new FrameworkPropertyMetadata(null, OnKeyboardIDChange));

		/// <summary>
		/// Gets the identifier of the virtual keyboard to be displayed.
		/// </summary>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <returns>Identifier corresponding to the virtual keyboard to be displayed</returns>
		/// <exception cref="InvalidOperationException">KeyboardIDProperty value is 
		/// invalid or KeyboardIDProperty does not exist</exception>
		public static object GetKeyboardID(DependencyObject requester)
		{
			return requester.GetValue(KeyboardIDProperty);
		}

		/// <summary>
		/// Sets the identifier of the virtual keyboard to be displayed.
		/// </summary>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <param name="id">Identifier for the virtual keyboard to display</param>
		/// <exception cref="InvalidOperationException">KeyboardIDProperty value is 
		/// invalid or KeyboardIDProperty does not exist</exception>
		/// <exception cref="ArgumentException">Specified value is not the correct type</exception>
		public static void SetKeyboardID(DependencyObject requester, object id)
		{
			requester.SetValue(KeyboardIDProperty, id);
		}

		/// <summary>
		/// Handles the event where the keyboard identifier has changed.
		/// </summary>
		/// <remarks>Metadata gets fired when the KeyboardID attached dependency property
		/// is changed. (Currently this happens at compile-time.)</remarks>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <param name="args">Arguments for the event</param>
		public static void OnKeyboardIDChange(DependencyObject requester, DependencyPropertyChangedEventArgs args)
		{
			var textBoxBase = requester as TextBoxBase;

			// Using a weak event manager to keep a weak reference of our got-focus-listener. 
			// This way the TextBoxBase instance that requests/invokes a keyboard can be 
			// garbage-collected.
			if (textBoxBase != null)
				TextBoxPreviewGotKeyboardFocusEventManager.AddListener(textBoxBase, RequesterPreviewGotFocusListener);
		}
		// ReSharper restore InconsistentNaming

		#endregion

		#region PickListItemsSource

		/// <summary>
		/// Dependency property that pertains to the IEnumerable items source for the virtual
		/// keyboard's pick list.
		/// </summary>
		public static readonly DependencyProperty PickListItemsSourceProperty = DependencyProperty.RegisterAttached(
			"PickListItemsSource", 
			typeof(IEnumerable), 
			typeof(VirtualKeyboardManager), 
			new FrameworkPropertyMetadata(null));

		/// <summary>
		/// Gets the IEnumerable items source for the virtual keyboard's pick list.
		/// </summary>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <returns>IEnumerable items source for the virtual keyboard's pick list</returns>
		/// <exception cref="InvalidOperationException">PickListItemsSourceProperty value is 
		/// invalid or PickListItemsSourceProperty does not exist</exception>
		public static IEnumerable GetPickListItemsSource(DependencyObject requester)
		{
			return (IEnumerable)requester.GetValue(PickListItemsSourceProperty);
		}

		/// <summary>
		/// Sets the IEnumerable items source for the virtual keyboard's pick list.
		/// </summary>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <param name="itemsSource">IEnumerable items source for the virtual keyboard's pick list</param>
		/// <exception cref="InvalidOperationException">PickListItemsSourceProperty value is 
		/// invalid or PickListItemsSourceProperty does not exist</exception>
		/// <exception cref="ArgumentException">Specified value is not the correct type</exception>
		public static void SetPickListItemsSource(DependencyObject requester, IEnumerable itemsSource)
		{
			requester.SetValue(PickListItemsSourceProperty, itemsSource);
		}

		#endregion

		#region PickListSelectedItem

		/// <summary>
		/// Dependency property that pertains to the selected item for the virtual keyboard's
		/// pick list.
		/// </summary>
		public static readonly DependencyProperty PickListSelectedItemProperty = DependencyProperty.RegisterAttached(
			"PickListSelectedItem", 
			typeof(object), 
			typeof(VirtualKeyboardManager), 
			new FrameworkPropertyMetadata(null));

		/// <summary>
		/// Gets the selected item for the virtual keyboard's pick list.
		/// </summary>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <returns>Selected item for the virtual keyboard's pick list</returns>
		/// <exception cref="InvalidOperationException">PickListSelectedItemProperty value is 
		/// invalid or PickListSelectedItemProperty does not exist</exception>
		public static object GetPickListSelectedItem(DependencyObject requester)
		{
			return requester.GetValue(PickListSelectedItemProperty);
		}

		/// <summary>
		/// Sets the selected item for the virtual keyboard's pick list.
		/// </summary>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <param name="selectedItem">Select item for the virtual keyboard's pick list</param>
		/// <exception cref="InvalidOperationException">PickListSelectedItemProperty value is 
		/// invalid or PickListSelectedItemProperty does not exist</exception>
		/// <exception cref="ArgumentException">Specified value is not the correct type</exception>
		public static void SetPickListSelectedItem(DependencyObject requester, object selectedItem)
		{
			requester.SetValue(PickListSelectedItemProperty, selectedItem);
		}

		#endregion

		#region Title

		/// <summary>
		/// Dependency property that pertains to the title for the virtual keyboard.
		/// </summary>
		public static readonly DependencyProperty TitleProperty = DependencyProperty.RegisterAttached(
			"Title",
			typeof(string),
			typeof(VirtualKeyboardManager),
			new FrameworkPropertyMetadata(String.Empty));

		/// <summary>
		/// Gets the title associated with the virtual keyboard.
		/// </summary>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <returns>Title associated with the virtual keyboard</returns>
		/// <exception cref="InvalidOperationException">TitleProperty value is invalid or 
		/// TitleProperty does not exist</exception>
		public static string GetTitle(DependencyObject requester)
		{
			return (string)requester.GetValue(TitleProperty);
		}

		/// <summary>
		/// Sets the title associated with the virtual keyboard.
		/// </summary>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <param name="title">Title to use with the virtual keyboard</param>
		/// <exception cref="InvalidOperationException">TitleProperty value is 
		/// invalid or TitleProperty does not exist</exception>
		/// <exception cref="ArgumentException">Specified value is not the correct type</exception>
		public static void SetTitle(DependencyObject requester, string title)
		{
			requester.SetValue(TitleProperty, title);
		}

		#endregion

		#region Watermark

		/// <summary>
		/// Dependency property that pertains to the watermark for the virutal keyboard's text box.
		/// </summary>
		public static readonly DependencyProperty WatermarkProperty = DependencyProperty.RegisterAttached(
			"Watermark",
			typeof(string),
			typeof(VirtualKeyboardManager),
			new FrameworkPropertyMetadata(String.Empty));

		/// <summary>
		/// Gets the watermark associated with the virtual keyboard's text box.
		/// </summary>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <returns>Watermark associated with the keyboard's text box</returns>
		/// <exception cref="InvalidOperationException">WatermarkProperty value is invalid or 
		/// WatermarkProperty does not exist</exception>
		public static string GetWatermark(DependencyObject requester)
		{
			return (string)requester.GetValue(WatermarkProperty);
		}

		/// <summary>
		/// Sets the watermark associated with the virtual keyboard's text box.
		/// </summary>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <param name="watermark">Watermark to use with the virtual keyboard's text box</param>
		/// <exception cref="InvalidOperationException">WatermarkProperty value is 
		/// invalid or WatermarkProperty does not exist</exception>
		/// <exception cref="ArgumentException">Specified value is not the correct type</exception>
		public static void SetWatermark(DependencyObject requester, string watermark)
		{
			requester.SetValue(WatermarkProperty, watermark);
		}

		#endregion

		#region IsKeyboardOpen

		public static bool GetIsKeyboardOpen(DependencyObject obj)
		{
			return (bool)obj.GetValue(IsKeyboardOpenProperty);
		}

		public static void SetIsKeyboardOpen(DependencyObject obj, bool value)
		{
			obj.SetValue(IsKeyboardOpenProperty, value);
		}

		// Using a DependencyProperty as the backing store for IsKeyboardOpen.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty IsKeyboardOpenProperty = DependencyProperty.RegisterAttached(
			"IsKeyboardOpen", 
			typeof(bool), 
			typeof(VirtualKeyboardManager), 
			new UIPropertyMetadata(false));

		#endregion

		#endregion

		#region Methods

		/// <summary>
		/// Initializes the virtual keyboard manager with the specified event aggregator and
		/// keyboard region
		/// </summary>
		/// <param name="eventAggregator">IEventAggregator instance</param>
		/// <param name="keyboardRegion">IRegion instance associated with the virtual keyboard</param>
		public static void Initialize(IEventAggregator eventAggregator, IRegion keyboardRegion)
		{
			KeyboardRegion = keyboardRegion;
			EventAggregator = eventAggregator;
		}

		/// <summary>
		/// Deactivates the active keyboard (if present).
		/// </summary>
		public static void CloseActiveKeyboard()
		{
			if (_activeVirtualKeyboard != null)
				DeactivateKeyboard(ref _activeVirtualKeyboard);
		}

		/// <summary>
		/// Determines if there is an active keyboard (i.e., a keyboard that is open).
		/// </summary>
		/// <returns></returns>
		public static bool HasAnyOpenKeyboards()
		{
			return _activeVirtualKeyboard != null;
		}

		/// <summary>
		/// Moves the input focus of the requester/invoker to be the next element based on the 
		/// TraversalRequest associated with the virtual keyboard manager.
		/// </summary>
		/// <remarks>The focus does not change if there is no keyboard active or if there is
		/// no reference to a UIElement that requested a keyboard.</remarks>
		public static void MoveNextFocus()
		{
			var isKeyboardOpen = _activeVirtualKeyboard != null;
			var hasRequester = _activeVirtualKeyboard != null && _activeVirtualKeyboard.Requester != null;
			if (!isKeyboardOpen || !hasRequester) return;
			TraversalRequest.Wrapped = false;
			_activeVirtualKeyboard.Requester.MoveFocus(TraversalRequest);
		}

		/// <summary>
		/// Registers a given collection of virtual keyboards with the virtual keyboard manager 
		/// and adds each keyboard to the keyboard region.
		/// </summary>
		/// <param name="keyboardsToRegister">IEnumerable collection of IVirtualKeyboards to register</param>
		/// <exception cref="ArgumentNullException">Given collection is null</exception>
		/// <exception cref="ArgumentNullException">IVirtualKeyboard instance within the given
		/// collection is null</exception>
		/// <exception cref="NotSupportedException">Given keyboard instance is already registered</exception>
		/// <exception cref="NullReferenceException">Keyboard region has not yet been set</exception>
		/// <remarks>If the specified IEnumerable instance is null, this method returns immediately.</remarks>
		public static void RegisterKeyboards(IEnumerable<IVirtualKeyboard> keyboardsToRegister)
		{
			if (keyboardsToRegister == null)
				return;

			foreach (var keyboard in keyboardsToRegister)
			{
				if (keyboard == null)
					throw new InvalidOperationException("Keyboard registration failed; the registering keyboard cannot be null.");

				if (keyboard.KeyboardID == null)
                    throw new InvalidOperationException("Keyboard registration failed; the registering keyboard cannot have a null identifier.");

				// Can't register the same keyboard name twice.
				if (RegisteredKeyboards.ContainsKey(keyboard.KeyboardID))
					throw new NotSupportedException("Keyboard registration failed for keyboard.Identifier='" + 
						keyboard.KeyboardID + "'. " + "The registering keyboard identifier is already associated with another keyboard.");

				if (KeyboardRegion == null)
					throw new NullReferenceException("Keyboard registration failed; the keyboard region must be set prior to any keyboard registration.");

				// Otherwise add the keyboard to the dictionary.
				RegisteredKeyboards.Add(keyboard.KeyboardID, keyboard);

				// Add the view to the region.
				KeyboardRegion.Add(keyboard.VisualView);

				// The VirtualKeyboardManager should know if the keyboard is requesting to be closed.
				keyboard.RequestToClose += OnRequestToClose;
			}
		}

		/// <summary>
		/// Attempts to open/show a virtual keyboard.
		/// </summary>
		/// <param name="requester">DependencyObject that requested the virtual keyboard</param>
		/// <param name="e"></param>
		/// <remarks>The parameter is 'object' instead of 'DependencyObject' because this method
		/// is a delegate for RequesterEventListener.</remarks>
		/// <exception cref="ArgumentNullException">Requester is null</exception>
		/// <exception cref="NotSupportedException">Requester is not a DependencyObject</exception>
		/// <exception cref="NotSupportedException">Requester's keyboard identifier is null</exception>
		/// <exception cref="NotSupportedException">Requester's keyboard identifier is not in
		/// the list of registered keyboards</exception>
		public static void OpenKeyboard(object requester, EventArgs e)
		{
			if (requester == null)
				throw new ArgumentNullException("requester");
			
			var requesterDepObj = requester as DependencyObject;
			if (requesterDepObj == null)
				throw new NotSupportedException("Unable to open keyboard for requester '" + requester +
					"' because it is not a DependencyObject.");

			// ReSharper disable once InconsistentNaming
			var requestedKeyboardID = GetKeyboardID(requesterDepObj);

			if (requestedKeyboardID == null)
				throw new NotSupportedException("Unable to open keyboard for requester '" + requesterDepObj +
					"' because its keyboard ID is null.");

			// This could happen if invoker requested keyboard with incorrect ID
			// or the keyboard/ID is not yet registered.
			if (!RegisteredKeyboards.ContainsKey(requestedKeyboardID))
				throw new NotSupportedException("Unable to open keyboard (ID = '" + requestedKeyboardID +
					"') because it is not in the list of registered keyboards.");

			ShowVirtualKeyboard(RegisteredKeyboards[requestedKeyboardID], requesterDepObj);
			if (requestedKeyboardID.ToString() != KeyboardNames.BackgroundKeyboard) return;
			var args = (RoutedEventArgs)e;
			args.Handled = true;
		}

		#endregion

		#region Private Helper Methods

		/// <summary>
		/// Deactivates a specified virtual keyboard by giving its requester the input focus
		/// (if possible), firing a VirtualKeyboardActiveStatusChanged event (via the event
		/// aggregator), and deactivating the keyboard region.</summary>
		/// <param name="keyboard">IVirtualKeyboard instance to deactivate</param>
		/// <exception cref="ArgumentNullException">IVirtualKeyboard instance is null</exception>
		private static void DeactivateKeyboard(ref IVirtualKeyboard keyboard)
		{
			if (keyboard == null)
				throw new ArgumentNullException("keyboard");

			lock(CriticalSectionLock)
			{
				if ((keyboard != null) && (!_isProcessingDeactivation))
				{
					_isProcessingDeactivation = true;

					EventAggregator.GetEvent<VirtualKeyboardActiveStatusChanged>().Publish(new VirtualKeyboardActiveStatusPayload(keyboard.KeyboardID, false));
					keyboard.Deactivate();
					KeyboardRegion.Deactivate(keyboard.VisualView);
					keyboard = null;

					_isProcessingDeactivation = false;
				}
			} 
		}

		/// <summary>
		/// Handles the RequestToClose event by deactivating the current keyboard.
		/// </summary>
		/// <param name="sender">Sender of the event</param>
		/// <param name="e">Arguments for the event</param>
		private static void OnRequestToClose(object sender, EventArgs e)
		{
			var keyboard = sender as IVirtualKeyboard;
			// ReSharper disable once PossibleNullReferenceException
			if (keyboard.Requester != null)
				SetIsKeyboardOpen(keyboard.Requester, false);

			DeactivateKeyboard(ref keyboard);
		}

		/// <summary>
		/// Shows the specified keyboard invoked by the specified requester and sets the 
		/// appropriate focus.
		/// </summary>
		/// <remarks>If a virtual keyboard is already active and a different one is requested,
		/// the active keyboard will be changed and the VirtualKeyboardActiveStatusChanged event
		/// will be fired via the event aggregator.</remarks>
		/// <param name="requestedKeyboard">IVirtualKeyboard instance to show</param>
		/// <param name="requester">Requester of the virtual keyboard</param>
		/// <exception cref="ArgumentNullException">Specified IVirtualKeyboard instance is null</exception>
		/// <exception cref="ArgumentNullException">Specified DependencyObject instance is null</exception>
		private static void ShowVirtualKeyboard(IVirtualKeyboard requestedKeyboard, DependencyObject requester)
		{
			if (requestedKeyboard == null)
				throw new ArgumentNullException("requestedKeyboard");

			if (requester == null)
				throw new ArgumentNullException("Unable to show virtual keyboard (ID = '" + requestedKeyboard.KeyboardID +
					"') because its requester is null.");

			// Requester wants something other than the active virtual keyboard.
			if (_activeVirtualKeyboard != requestedKeyboard)
			{
				// Some keyboard is already visible. First hide that one, and then show the new one.
				if (_activeVirtualKeyboard != null)
					DeactivateKeyboard(ref _activeVirtualKeyboard);

				KeyboardRegion.Activate(requestedKeyboard.VisualView);
				requestedKeyboard.Activate((FrameworkElement)requester);
				
				// Update the active virtual keyboard.
				_activeVirtualKeyboard = requestedKeyboard;
				SetIsKeyboardOpen(requester, true);

				// Send an event that new keyboard is active.
				EventAggregator.GetEvent<VirtualKeyboardActiveStatusChanged>().Publish(new VirtualKeyboardActiveStatusPayload(requestedKeyboard.KeyboardID, true));
			}
			// Requester wants the same virtual keyboard as the one that's active.
			else
			{
				// Same keyboard, different requester.
				if (Equals(_activeVirtualKeyboard.Requester, requester)) return;
				requestedKeyboard.Activate((FrameworkElement)requester);
				SetIsKeyboardOpen(requester, true);
			}
		}

		#endregion
	}
}
