﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Daxor.Lab.Infrastructure.Keyboards.Views
{
    /// <summary>
    /// Interaction logic for the pick list visual.
    /// </summary>
    public partial class PickList : UserControl
    {
        #region Ctor

        /// <summary>
        /// Creates a new instance of the PickList class by initializing its components
        /// and setting up the PreviewMouseLeftButtonUp handler.
        /// </summary>
        public PickList()
        {
            InitializeComponent();
            
            PickListContentControl.PreviewMouseLeftButtonUp += new MouseButtonEventHandler(PickListContentControl_PreviewMouseLeftButtonUp);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the pick list item that was most recently clicked.
        /// </summary>
        public object LastClickedItem
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the ListBox that is displaying the pick list items.
        /// </summary>
        public ListBox ItemsControl
        {
            get
            {
                return PickListContentControl;
            }
        }

        #endregion

        #region Dependency property -- TitleProperty

        /// <summary>
        /// Gets or sets the value of the TitleProperty dependency property.
        /// </summary>
        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        /// <summary>
        /// Dependency property that pertains to a string that is the title of the pick list.
        /// </summary>
        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(PickList), new UIPropertyMetadata(string.Empty));
        
        #endregion

        #region Methods

        /// <summary>
        /// Resets the pick list by clearing the last clicked item and the selected item
        /// on the items control.
        /// </summary>
        public void Reset()
        {
            LastClickedItem = null;
            ItemsControl.SelectedItem = null;
        }
        
        /// <summary>
        /// Fires the ItemClicked event.
        /// </summary>
        protected virtual void FireItemClicked()
        {
            EventHandler hander = ItemClicked;
            if (hander != null)
                hander(this, EventArgs.Empty);
        }

        /// <summary>
        /// Handles the PreviewMouseLeftButtonUp event by attempting to find (within the visual
        /// tree) the ListBoxItem clicked and setting that as the selected item and firing
        /// ItemClicked.
        /// </summary>
        /// <param name="sender">Sender of the PreviewMouseLeftButtonUp event</param>
        /// <param name="e">Arguments for the PreviewMouseLeftButtonUp event</param>
        /// <remarks>
        /// The SelectedItem has to be set manually to keep the ListBoxItem unfocusable.
        /// </remarks>
        protected void PickListContentControl_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            // ItemsControl used to be PickListContentControl

            UIElement elem = ItemsControl.InputHitTest(e.GetPosition(ItemsControl)) as UIElement;

            // Work our way up the visual stree until we find the ListBoxItem the user clicked.
            while ((elem != null) && (elem != ItemsControl))
            {
                if (elem is ListBoxItem)
                {
                    LastClickedItem = ((ListBoxItem)elem).Content;
                    ItemsControl.SelectedItem = LastClickedItem;
                    e.Handled = true;
                    FireItemClicked();
                    return;
                }

                elem = VisualTreeHelper.GetParent(elem) as UIElement;
            }
        }

        /// <summary>
        /// Handles the Clicked event for the ListBox by stopping the event.
        /// </summary>
        /// <param name="sender">Sender of the Clicked event</param>
        /// <param name="e">Arguments for the Clicked event</param>
        private void PickList_Clicked(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        #endregion

        #region events

        /// <summary>
        /// Occurs when an item in the pick list is clicked.
        /// </summary>
        public event EventHandler ItemClicked;

        #endregion
    }
}
