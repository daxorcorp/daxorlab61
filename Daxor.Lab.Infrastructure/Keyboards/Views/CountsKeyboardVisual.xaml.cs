﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Daxor.Lab.Infrastructure.Controls;
using Daxor.Lab.Infrastructure.Keyboards.Common;
using Daxor.Lab.Infrastructure.Keyboards.PropertyObservers;
using Daxor.Lab.Infrastructure.RuleFramework;
using System.Windows.Automation.Peers;

namespace Daxor.Lab.Infrastructure.Keyboards.Views
{
    /// <summary>
    /// Interaction logic for the counts keyboard visual.
    /// </summary>
    public partial class CountsKeyboardVisual : ObserverKeyboardUserControl, IKeyboardView
    {
        #region Ctor

        /// <summary>
        /// Creates a new instance of the CountsKeyboardVisual class by 
        /// initializing its components and setting up the dependency property
        /// observers.
        /// </summary>
        public CountsKeyboardVisual() : base()
        {
            InitializeComponent();

            ExclusionObserver = new DependencyPropertyObserver<bool>(SampleValueTextBox.IsExcludedProperty);
            uiSlider.IsOpened = false;
        }

        #endregion

        #region DependencyPropertyObservers

        /// <summary>
        /// Gets or sets the dependency observer for the is-excluded flag.
        /// </summary>
        public DependencyPropertyObserver<bool> ExclusionObserver
        {
            get;
            private set;
        }

        #endregion

        #region IKeyboardView Members

        /// <summary>
        /// Gets the status of whether the keyboard view is opened (i.e., visible).
        /// </summary>
        public bool IsOpened
        {
            get
            {
                return uiSlider.IsOpened;
            }
        }

        /// <summary>
        /// Closes the keyboard view by closing the slider, clearing the focus, and stopping
        /// all property observers.
        /// </summary>
        public void Close()
        {
            uiSlider.IsOpened = false;

            Keyboard.ClearFocus();
            Keyboard.Focus(this);

            ExclusionObserver.Stop();
            KeyboardTextObserver.Stop();
            ReadOnlyObserver.Stop();
            WatermarkObserver.Stop();
            TabStopObserver.Stop();
            HasAlertObserver.Stop();
        }

        /// <summary>
        /// Shows the keyboard view, which was invoked by a given requester, and starts the
        /// dependency property observers.
        /// </summary>
        /// <param name="requester">FrameworkElement that requested the keyboard to be shown</param>
        public void Show(FrameworkElement requester)
        {
            if (IsOpened) return;

            uiSlider.IsOpened = true;

            ExclusionObserver.Start(requester);
            KeyboardTextObserver.Start(requester);
            ReadOnlyObserver.Start(requester);
            WatermarkObserver.Start(requester);
            TabStopObserver.Start(requester);
            HasAlertObserver.Start(requester, System.Windows.Data.BindingMode.OneWay);
        }

        /// <summary>
        /// Occurs when a "key" on the keyboard view is invoked.
        /// </summary>
        public event EventHandler<VirtualKeyDownArgs> VirtualKeyDown;

        #endregion

        #region Helpers

        /// <summary>
        /// Fires the VirtualKeyDown event based on the specified character/key.
        /// </summary>
        /// <param name="singleCharInput">Single character of text entered via the virtual keyboard</param>
        /// <param name="keyPressed">Specific key (e.g., escape) pressed on the virtual keyboard</param>
        /// <remarks>
        /// <returns>Arguments for the KeyDownEvent based on the specified values; null if
        /// no handler is present to receive the arguments</returns>
        protected virtual VirtualKeyDownArgs FireVirtualKeyDown(string singleCharacterInput, Key? keyPressed)
        {
            EventHandler<VirtualKeyDownArgs> handler = VirtualKeyDown;
            if (handler != null)
            {
                VirtualKeyDownArgs args = new VirtualKeyDownArgs(singleCharacterInput, keyPressed);
                handler(this, args);
                return args;
            }
            return null;
        }

        /// <summary>
        /// Handles the click event for the buttons on the virtual keyboard and firing
        /// the VirtualKeyDown event if appropriate.
        /// </summary>
        /// <param name="sender">Sender of the click event</param>
        /// <param name="e">Arguments for the click event</param>
        private void ButtonBaseKeyPressed(object sender, RoutedEventArgs e)
        {
            ButtonBase button = e.OriginalSource as ButtonBase;
            if (button != null)
            {
                string singleCharacterInput = string.Empty;
                Key? keyPressed = null;

                // Create either a single character input or Key instance.
                if ((button.Tag != null) && (!string.IsNullOrEmpty(button.Tag.ToString())))
                {
                    Key tempKey;
                    if (Enum.TryParse<System.Windows.Input.Key>(button.Tag.ToString(), out tempKey))
                        keyPressed = tempKey;
                }
                else
                    singleCharacterInput = button.Content.ToString();

                // Fire the event only if value is not empty or key has value
                if (!string.IsNullOrEmpty(singleCharacterInput) || keyPressed.HasValue)
                    FireVirtualKeyDown(singleCharacterInput, keyPressed);
            }
        }

        #endregion
    }
}