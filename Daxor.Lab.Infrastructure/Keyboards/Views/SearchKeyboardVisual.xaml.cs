﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Daxor.Lab.Infrastructure.Controls;
using Daxor.Lab.Infrastructure.Keyboards.Common;

namespace Daxor.Lab.Infrastructure.Keyboards.Views
{
    /// <summary>
    /// Interaction logic for the search keyboard visual.
    /// </summary>
    public partial class SearchKeyboardVisual : ObserverKeyboardUserControl, IKeyboardView
    {
        #region Ctor

        /// <summary>
        /// Creates a new instance of the SearchKeyboardVisual class by 
        /// initializing its components and setting up the dependency property
        /// observers.
        /// </summary>
        public SearchKeyboardVisual()
        {
            InitializeComponent();

            uiMainKeyboardSlider.IsOpened = false;
        }

        #endregion

        #region IKeyboardView Members

        /// <summary>
        /// Gets the status of whether the keyboard view is opened (i.e., visible).
        /// </summary>
        public bool IsOpened
        {
            get
            {
                return uiMainKeyboardSlider.IsOpened;
            }
        }

        /// <summary>
        /// Closes the keyboard view by closing the slider, clearing the focus, and stopping
        /// all property observers.
        /// </summary>
        public void Close()
        {
            if (!IsOpened) return;

            Keyboard.ClearFocus();
            Keyboard.Focus(this);

            KeyboardTextObserver.Stop();
            WatermarkObserver.Stop();

            uiMainKeyboardSlider.IsOpened = false;
        }

        /// <summary>
        /// Refreshes the keyboard view by setting the caps-lock toggle and
        /// snyching the keyboard keys shown in the UI.
        /// </summary>
        public void Refresh()
        {
            uiCapsToggle.IsChecked = Keyboard.IsKeyToggled(Key.CapsLock);
            SyncKeyboardKeys();
        }

        /// <summary>
        /// Shows the keyboard view, starts the dependency property observers, sets the focused
        /// element, and sets the caret position.
        /// </summary>
        /// <param name="requester">FrameworkElement that requested the keyboard to be shown</param>
        public void Show(FrameworkElement requester)
        {
            if (IsOpened) return;

            KeyboardTextObserver.Start(requester);
            WatermarkObserver.Start(requester);

            uiCapsToggle.IsChecked = Keyboard.IsKeyToggled(Key.CapsLock);
            SyncKeyboardKeys();

            uiInputTextBox.CaretIndex = KeyboardTextObserver.Value.Length;
            uiMainKeyboardSlider.IsOpened = true;

            FocusManager.SetFocusedElement(this, uiInputTextBox);
            uiInputTextBox.Focus();
        }

        /// <summary>
        /// Occurs when a "key" on the keyboard view is invoked.
        /// </summary>
        public event EventHandler<VirtualKeyDownArgs> VirtualKeyDown;       
        
        #endregion

        #region Helpers

        /// <summary>
        /// Handles the click event for the buttons on the virtual keyboard and firing
        /// the VirtualKeyDown event if appropriate.
        /// </summary>
        /// <param name="sender">Sender of the click event</param>
        /// <param name="e">Arguments for the click event</param>
        private void ButtonBaseKeyPressed(object sender, RoutedEventArgs e)
        {
            ButtonBase button = e.OriginalSource as ButtonBase;
            if (button != null)
            {
                // If button had a command on it ignore all this logic and let the command to execute
                if (button.Command != null) return;

                string singleCharacterInput = string.Empty;
                Key? keyPressed = null;

                // Create either a single character input or Key instance.
                if ((button.Tag != null) && (!string.IsNullOrEmpty(button.Tag.ToString())))
                {
                    Key tempKey;
                    if (Enum.TryParse<System.Windows.Input.Key>(button.Tag.ToString(), out tempKey))
                        keyPressed = tempKey;
                }
                else
                    singleCharacterInput = button.Content.ToString();

                // Fire the event only if value is not empty or key has value
                if (!string.IsNullOrEmpty(singleCharacterInput) || keyPressed.HasValue)
                {
                    FireVirtualKeyDown(singleCharacterInput, keyPressed);

                    if (keyPressed.HasValue)
                        SyncKeyboardKeys();
                }
            }
        }

        /// <summary>
        /// Fires the VirtualKeyDown event based on the specified character/key.
        /// </summary>
        /// <param name="singleCharInput">Single character of text entered via the virtual keyboard</param>
        /// <param name="keyPressed">Specific key (e.g., escape) pressed on the virtual keyboard</param>
        /// <remarks>
        /// <returns>Arguments for the KeyDownEvent based on the specified values; null if
        /// no handler is present to receive the arguments</returns>
        protected virtual VirtualKeyDownArgs FireVirtualKeyDown(string singleCharacterInput, Key? keyPressed)
        {
            EventHandler<VirtualKeyDownArgs> handler = VirtualKeyDown;
            if (handler != null)
            {
                VirtualKeyDownArgs args = new VirtualKeyDownArgs(singleCharacterInput, keyPressed);
                handler(this, args);
                return args;
            }
            return null;
        }

        /// <summary>
        /// Sets the letter-based buttons on the UI to lowercase letters.
        /// </summary>
        void MakeKeyboardLowerCase()
        {
            A_Key.Content = "a";
            B_Key.Content = "b";
            C_Key.Content = "c";
            D_Key.Content = "d";
            E_Key.Content = "e";
            F_Key.Content = "f";
            G_Key.Content = "g";
            H_Key.Content = "h";
            I_Key.Content = "i";
            J_Key.Content = "j";
            K_Key.Content = "k";
            L_Key.Content = "l";
            M_Key.Content = "m";
            N_Key.Content = "n";
            O_Key.Content = "o";
            P_Key.Content = "p";
            Q_Key.Content = "q";
            R_Key.Content = "r";
            S_Key.Content = "s";
            T_Key.Content = "t";
            U_Key.Content = "u";
            V_Key.Content = "v";
            W_Key.Content = "w";
            X_Key.Content = "x";
            Y_Key.Content = "y";
            Z_Key.Content = "z";
        }

        /// <summary>
        /// Sets the letter-based buttons on the UI to symbols.
        /// </summary>
        void MakeKeyboardSymbol()
        {
            A_Key.Content = "!";
            B_Key.Content = "@";
            C_Key.Content = "#";
            D_Key.Content = "$";
            E_Key.Content = "%";
            F_Key.Content = "_";
            G_Key.Content = "^";
            H_Key.Content = "&";
            I_Key.Content = "*";
            J_Key.Content = "(";
            K_Key.Content = ")";
            L_Key.Content = "+";
            M_Key.Content = "=";
            N_Key.Content = "'";
            O_Key.Content = "\"";
            P_Key.Content = ":";
            Q_Key.Content = ";";
            R_Key.Content = "<";
            S_Key.Content = ">";
            T_Key.Content = "?";
            U_Key.Content = "[";
            V_Key.Content = "]";
            W_Key.Content = "{";
            X_Key.Content = "}";
            Y_Key.Content = "`";
            Z_Key.Content = "~";
        }

        /// <summary>
        /// Sets the letter-based buttons on the UI to uppercase letters.
        /// </summary>
        void MakeKeyboardUpperCase()
        {
            A_Key.Content = "A";
            B_Key.Content = "B";
            C_Key.Content = "C";
            D_Key.Content = "D";
            E_Key.Content = "E";
            F_Key.Content = "F";
            G_Key.Content = "G";
            H_Key.Content = "H";
            I_Key.Content = "I";
            J_Key.Content = "J";
            K_Key.Content = "K";
            L_Key.Content = "L";
            M_Key.Content = "M";
            N_Key.Content = "N";
            O_Key.Content = "O";
            P_Key.Content = "P";
            Q_Key.Content = "Q";
            R_Key.Content = "R";
            S_Key.Content = "S";
            T_Key.Content = "T";
            U_Key.Content = "U";
            V_Key.Content = "V";
            W_Key.Content = "W";
            X_Key.Content = "X";
            Y_Key.Content = "Y";
            Z_Key.Content = "Z";
        }

        /// <summary>
        /// Determines which state the keyboard keys should be in (lower, upper, symbol)
        /// and sets the UI accordingly.
        /// </summary>
        void SyncKeyboardKeys()
        {
            bool symbolKeysDisplayed = (bool)SymbolToggle.IsChecked;
            bool capsKeysDisplayed = (bool)uiCapsToggle.IsChecked;

            if (SymbolToggle.IsChecked.HasValue && SymbolToggle.IsChecked.Value)
            {
                MakeKeyboardSymbol();
            }
            else
            {
                if (capsKeysDisplayed)
                    MakeKeyboardUpperCase();
                else
                    MakeKeyboardLowerCase();
            }

            uiInputTextBox.Focus();
        }

        #endregion
    }
}
