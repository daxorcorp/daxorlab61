﻿using System;
using System.Windows;

namespace Daxor.Lab.Infrastructure.Keyboards.Views
{
    /// <summary>
    /// Represents a view (i.e., visual) for a virtual keyboard.
    /// </summary>
    public interface IKeyboardView
    {
        #region Properties

        /// <summary>
        /// Gets the status of whether the keyboard view is opened (i.e., visible).
        /// </summary>
        bool IsOpened { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Closes the keyboard view.
        /// </summary>
        void Close();

        /// <summary>
        /// Shows the keyboard view, which was invoked by a given requester
        /// </summary>
        /// <param name="requester">FrameworkElement that requested the keyboard to be shown</param>
        void Show(FrameworkElement requester);

        #endregion

        #region Events

        /// <summary>
        /// Occurs when a "key" on the keyboard view is invoked.
        /// </summary>
        event EventHandler<VirtualKeyDownArgs> VirtualKeyDown;

        #endregion
    }
}
