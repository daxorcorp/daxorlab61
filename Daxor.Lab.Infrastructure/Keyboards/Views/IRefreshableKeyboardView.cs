﻿namespace Daxor.Lab.Infrastructure.Keyboards.Views
{
    /// <summary>
    /// Keyboard View that allows for refreshing
    /// </summary>
    public interface IRefreshableKeyboardView
    {
        /// <summary>
        /// Refreshes the keyboard view.
        /// </summary>
        void Refresh();
    }
}
