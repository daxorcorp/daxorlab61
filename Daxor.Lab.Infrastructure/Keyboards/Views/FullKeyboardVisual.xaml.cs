﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using Daxor.Lab.Infrastructure.Controls;
using Daxor.Lab.Infrastructure.Keyboards.Common;
using Daxor.Lab.Infrastructure.Keyboards.PropertyObservers;
using Daxor.Lab.Infrastructure.RuleFramework;
using System.Windows.Media.Animation;
using System.Threading.Tasks;
using Daxor.Lab.Infrastructure.Threading;

namespace Daxor.Lab.Infrastructure.Keyboards.Views
{
    /// <summary>
    /// Interaction logic for full keyboard visual.
    /// </summary>
    public partial class FullKeyboardVisual : ObserverKeyboardUserControl, IKeyboardView, IRefreshableKeyboardView
    {
        #region Fields

        object _selectedItem;
        ListCollectionView _itemsCollectionView;

        #endregion

        #region Ctor

        /// <summary>
        /// Creates a new instance of the FullKeyboardVisual class by 
        /// initializing its components and setting up the dependency property
        /// observers/helpers.
        /// </summary>
        public FullKeyboardVisual()
        {
            InitializeComponent();

            KeyboardTextHelper = new KeyboardTextHelper();
            SelectedItemHelper = new PickListSelectedItemHelper();
            FocusableObserver = new DependencyPropertyObserver<Boolean>(FrameworkElement.FocusableProperty);
            SelectedItemHelper.PickListSelectedItemPropertyChanged += new PropertyChangedCallback(SelectedItemHelper_PickListSelectedItemPropertyChanged);
            
            uiMainKeyboardSlider.IsOpened = false;
            uiPickListSlider.IsOpened = false;
        }

        
        /// <summary>
        /// Closes pick list and tabs to next field when pick list item is selected.
        /// </summary>
        void SelectedItemHelper_PickListSelectedItemPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue == null)
            {
                    //  We have a new selection.  This event always get's fired in pairs.
                    //  We want to handle the first event
                uiPickListSlider.IsOpened = false;
                VirtualKeyboardManager.MoveNextFocus();
            }
        }

        #endregion

        #region Properties

        // Don't think this is used. GMazeroff 6/21/2011
        public object InternalSelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                SelectedItemHelper.PickListSelectedItem = _selectedItem;
                FireVirtualKeyDown(string.Empty, Key.End);
            }
        }

        /// <summary>
        /// Gets or sets the ListCollectionView of pick list items.
        /// </summary>
        /// <remarks>
        /// When setting the ItemsCollectionView, any associated filter is reset.
        /// </remarks>
        public ListCollectionView ItemsCollectionView
        {
            get { return _itemsCollectionView; }
            private set
            {
                _itemsCollectionView = value;
            }
        }

        /// <summary>
        /// Gets or sets the keyboard text helper.
        /// </summary>
        public KeyboardTextHelper KeyboardTextHelper
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the pick list selected item helper.
        /// </summary>
        public PickListSelectedItemHelper SelectedItemHelper
        {
            get;
            set;
        }
        
        ///// <summary>
        ///// Gets or sets the dependency property observer for focusable
        ///// </summary>
        public DependencyPropertyObserver<Boolean> FocusableObserver
        {
            get;
            private set;
        }

      
        #endregion

        #region IKeyboardView Members

        /// <summary>
        /// Gets the status of whether the keyboard view is opened (i.e., visible).
        /// </summary>
        public bool IsOpened
        {
            get
            {
                return uiMainKeyboardSlider.IsOpened;
            }
        }

        /// <summary>
        /// Closes the keyboard view by closing the slider, clearing the focus, and stopping
        /// all property observers.
        /// </summary>
        public void Close()
        {
            if (!IsOpened) return;

            ItemsCollectionView = null;
            Keyboard.ClearFocus();
            Keyboard.Focus(this);
            
            KeyboardTextHelper.KeyboardTextPropertyChanged -= OnKeyboardTextPropertyChanged;
            KeyboardTextHelper.Unbind();
            FocusableObserver.Stop();
            WatermarkObserver.Stop();
            TabStopObserver.Stop();
            HasAlertObserver.Stop();
            
            uiPickListSlider.IsOpened = false;
            uiMainKeyboardSlider.IsOpened = false;
        }

        /// <summary>
        /// Refreshes the keyboard view by setting the caps-lock toggle,
        /// snyching the keyboard keys shown in the UI, and changing the
        /// pick list slider state if it has changed.
        /// </summary>
        public void Refresh()
        {
            uiCapsToggle.IsChecked = Keyboard.IsKeyToggled(Key.CapsLock);
            SyncKeyboardKeys();

            bool currentSliderState = uiPickListSlider.IsOpened;
            bool newSliderState = ItemsCollectionView != null && !ItemsCollectionView.IsEmpty;

            if (currentSliderState != newSliderState)
                uiPickListSlider.IsOpened = newSliderState;
        }

        /// <summary>
        /// Shows the keyboard view, starts the dependency property observers, sets up the pick list,
        /// and sets the state of the keyboard (caps-lock).
        /// </summary>
        /// <param name="requester">FrameworkElement that requested the keyboard to be shown</param>
        public void Show(FrameworkElement requester)
        {
            if (IsOpened) return;
        
            KeyboardTextHelper.KeyboardTextPropertyChanged += OnKeyboardTextPropertyChanged;
            
            // Set up custom binding with the requester.
            FocusableObserver.Start(requester);

            BindingMode bModeWithObserver = FocusableObserver.Value ? BindingMode.OneWay : BindingMode.TwoWay;

            KeyboardTextHelper.BindWith(requester, bModeWithObserver);
            SelectedItemHelper.BindWith(requester);
         
            WatermarkObserver.Start(requester);
            TabStopObserver.Start(requester);
            HasAlertObserver.Start(requester, BindingMode.OneWay);

            uiPickListControl.ItemsSource = VirtualKeyboardManager.GetPickListItemsSource(requester);
            ItemsCollectionView = (ListCollectionView)CollectionViewSource.GetDefaultView(VirtualKeyboardManager.GetPickListItemsSource(requester));
           
            uiCapsToggle.IsChecked = Keyboard.IsKeyToggled(Key.CapsLock);
            
            //Focus related issues
            if (!FocusableObserver.Value)
                uiInputTextBox.CaretIndex = KeyboardTextHelper.KeyboardText.Length;

            SyncKeyboardKeys();
            uiMainKeyboardSlider.IsOpened = true;
            
            if (!FocusableObserver.Value)
            {
                FocusManager.SetFocusedElement(this, uiInputTextBox);
                uiInputTextBox.Focus();
            }
        }

        /// <summary>
        /// Occurs when a "key" on the keyboard view is invoked.
        /// </summary>
        public event EventHandler<VirtualKeyDownArgs> VirtualKeyDown;

        #endregion

        #region Helpers

        /// <summary>
        /// Handles the click event for the buttons on the virtual keyboard and firing
        /// the VirtualKeyDown event if appropriate.
        /// </summary>
        /// <param name="sender">Sender of the click event</param>
        /// <param name="e">Arguments for the click event</param>
        private void ButtonBaseKeyPressed(object sender, RoutedEventArgs e)
        {
            ButtonBase button = e.OriginalSource as ButtonBase;
            if (button != null)
            {
                string singleCharacterInput = string.Empty;
                Key? keyPressed = null;

                // Create either a single character input or Key instance.
                if ((button.Tag != null) && (!string.IsNullOrEmpty(button.Tag.ToString())))
                {
                    Key tempKey;
                    if (Enum.TryParse<System.Windows.Input.Key>(button.Tag.ToString(), out tempKey))
                        keyPressed = tempKey;
                }
                else
                {
                    if (button.Content is StackPanel)
                    {
                        StackPanel temp = button.Content as StackPanel;
                        if (temp.Name == "Hyphen")
                            singleCharacterInput = "-";
                    }
                    else
                    {
                        singleCharacterInput = button.Content.ToString();
                    }
                }

                // Fire the event only if value is not empty or key has value
                if (!string.IsNullOrEmpty(singleCharacterInput) || keyPressed.HasValue)
                {
                    FireVirtualKeyDown(singleCharacterInput, keyPressed);

                    if (keyPressed.HasValue)
                        SyncKeyboardKeys();
                }
            }
        }

        /// <summary>
        /// Fires the VirtualKeyDown event based on the specified character/key.
        /// </summary>
        /// <param name="singleCharInput">Single character of text entered via the virtual keyboard</param>
        /// <param name="keyPressed">Specific key (e.g., escape) pressed on the virtual keyboard</param>
        /// <remarks>
        /// <returns>Arguments for the KeyDownEvent based on the specified values; null if
        /// no handler is present to receive the arguments</returns>
        protected virtual VirtualKeyDownArgs FireVirtualKeyDown(string singleCharacterInput, Key? keyPressed)
        {
            EventHandler<VirtualKeyDownArgs> handler = VirtualKeyDown;
            if (handler != null)
            {
                VirtualKeyDownArgs args = new VirtualKeyDownArgs(singleCharacterInput, keyPressed);
                handler(this, args);
                return args;
            }
            return null;
        }

        /// <summary>
        /// Sets the letter-based buttons on the UI to lowercase letters.
        /// </summary>
        void MakeKeyboardLowerCase()
        {
            A_Key.Content = "a";
            B_Key.Content = "b";
            C_Key.Content = "c";
            D_Key.Content = "d";
            E_Key.Content = "e";
            F_Key.Content = "f";
            G_Key.Content = "g";
            H_Key.Content = "h";
            I_Key.Content = "i";
            J_Key.Content = "j";
            K_Key.Content = "k";
            L_Key.Content = "l";
            M_Key.Content = "m";
            N_Key.Content = "n";
            O_Key.Content = "o";
            P_Key.Content = "p";
            Q_Key.Content = "q";
            R_Key.Content = "r";
            S_Key.Content = "s";
            T_Key.Content = "t";
            U_Key.Content = "u";
            V_Key.Content = "v";
            W_Key.Content = "w";
            X_Key.Content = "x";
            Y_Key.Content = "y";
            Z_Key.Content = "z";
        }

        /// <summary>
        /// Sets the letter-based buttons on the UI to symbols.
        /// </summary>
        void MakeKeyboardSymbol()
        {
            A_Key.Content = "!";
            B_Key.Content = "@";
            C_Key.Content = "#";
            D_Key.Content = "$";
            E_Key.Content = "%";
            F_Key.Content = "_";
            G_Key.Content = "^";
            H_Key.Content = "&";
            I_Key.Content = "*";
            J_Key.Content = "(";
            K_Key.Content = ")";
            L_Key.Content = "+";
            M_Key.Content = "=";
            N_Key.Content = "'";
            O_Key.Content = "\"";
            P_Key.Content = ":";
            Q_Key.Content = ";";
            R_Key.Content = "<";
            S_Key.Content = ">";
            T_Key.Content = "?";
            U_Key.Content = "[";
            V_Key.Content = "]";
            W_Key.Content = "{";
            X_Key.Content = "}";
            Y_Key.Content = "`";
            Z_Key.Content = "~";
        }

        /// <summary>
        /// Sets the letter-based buttons on the UI to uppercase letters.
        /// </summary>
        void MakeKeyboardUpperCase()
        {
            A_Key.Content = "A";
            B_Key.Content = "B";
            C_Key.Content = "C";
            D_Key.Content = "D";
            E_Key.Content = "E";
            F_Key.Content = "F";
            G_Key.Content = "G";
            H_Key.Content = "H";
            I_Key.Content = "I";
            J_Key.Content = "J";
            K_Key.Content = "K";
            L_Key.Content = "L";
            M_Key.Content = "M";
            N_Key.Content = "N";
            O_Key.Content = "O";
            P_Key.Content = "P";
            Q_Key.Content = "Q";
            R_Key.Content = "R";
            S_Key.Content = "S";
            T_Key.Content = "T";
            U_Key.Content = "U";
            V_Key.Content = "V";
            W_Key.Content = "W";
            X_Key.Content = "X";
            Y_Key.Content = "Y";
            Z_Key.Content = "Z";
        }

        /// <summary>
        /// Updates the pick list (ItemsCollectionView) if the keyboard text has changed 
        /// by applying a filter that only shows strings in the collection that start with
        /// the new keyboard value.
        /// </summary>
        /// <param name="sender">Sender of the KeyboardTextPropertyChanged event</param>
        /// <param name="e">Arugments for the event</param>
        void OnKeyboardTextPropertyChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            // Update the pick list if the keyboard text has changed.
            if (ItemsCollectionView != null)
            {
                // See if the keyboard text value has changed. If not, there's nothing to do.
                string newValue = e.NewValue.ToString();
                string oldValue = e.OldValue.ToString();
                if (newValue == oldValue) return;

                // The new filter may have changed whether the pick list should be visible or not. 
                Refresh();
            }
        }

        /// <summary>
        /// Determines which state the keyboard keys should be in (lower, upper, symbol)
        /// and sets the UI accordingly.
        /// </summary>
        void SyncKeyboardKeys()
        {
            bool symbolKeysDisplayed = (bool)SymbolToggle.IsChecked;
            bool capsKeysDisplayed = (bool)uiCapsToggle.IsChecked;

            if (SymbolToggle.IsChecked.HasValue && SymbolToggle.IsChecked.Value)
            {
                MakeKeyboardSymbol();
            }
            else
            {
                if (capsKeysDisplayed)
                    MakeKeyboardUpperCase();
                else
                    MakeKeyboardLowerCase();
            }

            //Check to see if Focusable
            if (uiInputTextBox.IsFocused && !FocusableObserver.Value)
                uiInputTextBox.Focus();
        }

        #endregion
    }
}
