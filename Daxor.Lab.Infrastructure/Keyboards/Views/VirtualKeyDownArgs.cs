﻿using System;
using System.Windows.Input;

namespace Daxor.Lab.Infrastructure.Keyboards.Views
{
    /// <summary>
    /// Provides data for the IKeyboardView.VirtualKeyDown event.
    /// </summary>
    public class VirtualKeyDownArgs : EventArgs
    {
        /// <summary>
        /// Creates a new VirtualKeyDownArgs instance using the specified key content,
        /// key, and associated item.
        /// </summary>
        /// <param name="keyContent">Content of the key</param>
        /// <param name="actionKey">Special key value (e.g., backspace)</param>
        /// <param name="item">Associated item</param>
        public VirtualKeyDownArgs(string keyContent, Key? actionKey, object item = null)
        {
            KeyContent = keyContent;
            ActionKey = actionKey;
            Item = item;
        }

        /// <summary>
        /// Gets or sets the key value (e.g., backspace) corresponding to the virtual key.
        /// </summary>
        public Key? ActionKey { get; private set; }

        /// <summary>
        /// Gets or sets the item associated with the virtual key press.
        /// </summary>
        public object Item { get; private set; }

        /// <summary>
        /// Gets or sets the string equivalent of the virtual key.
        /// </summary>
        public string KeyContent { get; private set; }
    }
}
