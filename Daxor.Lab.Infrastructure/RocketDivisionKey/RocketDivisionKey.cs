﻿namespace Daxor.Lab.Infrastructure.RocketDivisionKey
{
	public static class RocketDivisionKey
	{
		public static string Key = "BHsMeSl3DnVfB28HbTBcAnwFYQZ+OFoEZAxoYX4fRXSYYVB02SqyLlJqC19lQootMhAnUSRH/AQZCVwYa/9r3mrV8o/JuF/O+bKOjNn5P+XCp8ycmfbxj2bewtQszUzC8as/x72wZuWfmvWdVtTa64GRf42v6zfUqt4Fjvrhtq33gXaB//FoiofFtaIk5+zw14Bv8++gi9/x21De74v1nS2bl6mXyv/uoPXb6N2TvtlV6f+GotVn/RKi39EyeS5eHEfYI88q4EWFcIsyuVQcUqUfyiORY0ZtVhykcu5uY3hFJAERrx1Mb75HF3RSToF5J39OEQU2Z1PIWntyy2/5RWA6CS2BPh979Ho7IaEkFFY8SMUcR3R+cAYNAV1idE82wCltK014D1pgXIYS/0CXWuVJdmrgWYwJzDgDdX2YXM1kse+7XaT46dOvM6xUyJ+MJMhw21i2yoL59VPHU8nB6D6NA4W5yZi17pCM+vqtWYOSvb7RmsU1hhaxjLX/9mLQlvZr3KvuyNZKsgrHGvxe68byc9MO1sOjLdg1xCnEQPqvgaDCeqnkzdDCvokd6n+9o6CPijzfM+OX3pv5ejfEJYBKal0yPCZaOUVkdhtIcWkBci4Fj148JRQ0rQ7saCgw5ET6OlobqzEvQ3Z47hPgVZp7jxoONl4hRg7aZkYyGSG6LtJ8u1jLZbkaYRCuIRtbFkVJIIRjUkNtdtJ+Q2QubmRlrkifQiBt8lvCMGYTiSnGQEdXuHaVaqNKZw577qeVwvEIzo7oSq7BzFXLnab0kIu+tZ7Up5zoabVQ0reUWve3miX6UqmgnTmrNPg8gR263/9/0wLXqMUfz0TeYZx2zSKofbPz8EGSir24na7EmdPj6hSp6cnzqwbMPOwhv7fXZvyS5PvpAtodwsLdSv51tRaenK2zm/XUMYaXrL8YMxDiUBQNMh6FJGoRx0KqNCoMG3Pze2lBqmP2C9gVphjWV/A/zH1hdz8ZlGuHKVYYmVwqW0xydDTRJFkVazEvbZ4kCAS0dZItrAxYBm9RTAp1TSRaMlgnXSpRJlYoQSpvFmQuWwkczVVKVRlb/xOZKyA/UzOUSg0Nk3EPVugFd8/GyCbVy+jc87L2zb/8nPKCouuwib7UbYlnw7bCx5d5vI23Zaps9IiN4KcBn23w65sWhjLevYwGrpTUvM4iqkCg0L6067Tb2NQOokfP37/ursaAJM5+zIG32Pm11TfwFYpwjQmwHckY+tn+BtvH4VS9QZg2r5Llx6mvhm+/MIjhbAcxuBBnfM5+mVSRG5ZT1iQdbIoYVV6BPI5AbjbHWs5LTTo5S8ccRU+oL58jhD8MLHYG1x9yQXdPth0JFShnWG0OXQ==";
		#region Constants

		// ReSharper disable InconsistentNaming
		public const string DIRECTORY_NODE_KEY = "$D";
		public const string ROOT_NODE_KEY = "$R";
		public const string DISC_LABEL_PREFIX = "Disc :";
		public const string DEF_VOLUME_NAME = "DATA_DISC";
		public const int DEF_PROGRESS_CAPACITY = 32000;
		public const int MODE1_LB_SIZE = 2048;  // This constant is used to calculate size of the Data track (in kb.)
		// ReSharper restore InconsistentNaming

		#endregion Constants
		}
}
