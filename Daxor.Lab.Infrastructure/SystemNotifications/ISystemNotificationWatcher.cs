﻿using System;
using System.Linq;

namespace Daxor.Lab.Infrastructure.SystemNotifications
{
    public interface ISystemNotificationWatcher
    {
        void RemoveSystemNotification(SystemNotification systemNotification);

        event EventHandler<SystemNotificationEventArgs> SystemNotificationChanged;

        IQueryable<SystemNotification> SystemNotifications { get; }
        
    }
}
