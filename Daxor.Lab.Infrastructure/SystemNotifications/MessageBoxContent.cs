﻿using System;

namespace Daxor.Lab.Infrastructure.SystemNotifications
{
    public class MessageBoxContent : SystemNotification
    {
        public MessageBoxContent(Guid mcaStatusChangedId) : base (mcaStatusChangedId)
        {

        }

        public string Caption { get; set; }

        public string Message { get; set; }

        public bool DisplayMessageBox { get; set; }
    }
}