﻿using System;
using Daxor.Lab.Infrastructure.Alerts;

namespace Daxor.Lab.Infrastructure.SystemNotifications
{
    public static class SystemNotificationArray
    {
        public enum NotificationIndex
        {
            QcDue,
            HighBackground,
            SampleChangerUnavailable,
        }

        public static SystemNotification[] Notifications = new SystemNotification[10];

        static SystemNotificationArray()
        {
            var qcDueId = new Guid("{74B0E5B2-089D-40A8-B8B2-DFC64F80743F}");
            var highBackgroundId = new Guid("{7D68F3E2-ED18-49C0-AC09-8CCAE1CF08AB}");

            Notifications[(int)NotificationIndex.QcDue] = new SystemNotification(qcDueId)
            {
                IsAlert = true,
                RemoveFromQue = false,
                SystemNotificationType = SystemNotification.QcNotificationType,
                SystemNotificationDescription = "QC Due",
                SystemNotificationData = new AlertViolationPayload(qcDueId)
                {
                    ErrorHeader = "QC Due",
                    Resolution = "A QC Test is due at this time."
                },
            };

            Notifications[(int)NotificationIndex.HighBackground] = new SystemNotification(highBackgroundId)
            {
                IsAlert = true,
                RemoveFromQue = false,
                SystemNotificationType = SystemNotification.QcNotificationType,
                SystemNotificationDescription = "High Background",
                SystemNotificationData = new AlertViolationPayload(highBackgroundId)
                {
                    ErrorHeader = "High Background",
                    Resolution = "Background has been restarted due to high counts."
                },
            };
        }

    }
}