﻿using System;

namespace Daxor.Lab.Infrastructure.SystemNotifications
{
	public class SystemNotification
	{
	    public const string UnknownNotificationType = "Unknown";

	    public const string QcNotificationType = "QC";

	    public Guid SystemNotificationId { get; set; }

		public bool IsAlert { get; set; }

		public bool RemoveFromQue { get; set; }

		public string SystemNotificationType { get; set; }

		public string SystemNotificationDescription { get; set; }

		public object SystemNotificationData { get; set; }

		public object MessageBoxContent { get; set; }

		public DateTime SystemNotificationDateTime { get; set; }

		public SystemNotification()
		{
			SystemNotificationId = Guid.NewGuid();
			SystemNotificationType = UnknownNotificationType;
			SystemNotificationDateTime = DateTime.Now;
		}

		public SystemNotification(Guid systemNotificationId)
		{
			SystemNotificationId = systemNotificationId;
			SystemNotificationType = UnknownNotificationType;
			SystemNotificationDateTime = DateTime.Now;
		}
	}
}

