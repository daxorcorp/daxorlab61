﻿using System;

namespace Daxor.Lab.Infrastructure.SystemNotifications
{
    public class SystemNotificationEventArgs : EventArgs
    {
        public SystemNotification ChangedSystemNotification { get; private set; }

        public SystemNotificationEventArgs(SystemNotification changedSystemNotification)
        {
            ChangedSystemNotification = changedSystemNotification;
        }
    }

}

