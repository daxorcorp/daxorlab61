﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Infrastructure.Events;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.SystemNotifications
{
	public class SystemNotificationWatcher : ISystemNotificationWatcher 
	{
		private readonly Dictionary<Guid, SystemNotification> _systemNotifications = new Dictionary<Guid, SystemNotification>();
 
		protected void OnSystemNotification(SystemNotification systemNotification)
		{
			if (!_systemNotifications.ContainsKey(systemNotification.SystemNotificationId))
			{
				_systemNotifications.Add(systemNotification.SystemNotificationId, systemNotification);
			}
			else
			{

				if (!systemNotification.RemoveFromQue)
				{
					_systemNotifications[systemNotification.SystemNotificationId] = systemNotification;
				}
				else
				{
					_systemNotifications.Remove(systemNotification.SystemNotificationId);
				}
			}
			
			RaiseSystemNotificationChanged(systemNotification);
		}

		public IQueryable<SystemNotification> SystemNotifications
		{
			get
			{
				return (from p in _systemNotifications
						select p.Value).AsQueryable();
			}
		}

		public event EventHandler<SystemNotificationEventArgs> SystemNotificationChanged;

		public SystemNotificationWatcher(IEventAggregator eventAggregator)
		{
			eventAggregator.GetEvent<SystemNotificationOccurred>().Subscribe(OnSystemNotification, ThreadOption.PublisherThread, false);
		}

		public void RemoveSystemNotification(SystemNotification systemNotification)
		{
			_systemNotifications.Remove(systemNotification.SystemNotificationId);
			RaiseSystemNotificationChanged(systemNotification);
		}

		protected virtual void RaiseSystemNotificationChanged(SystemNotification systemNotification)
		{
			var handler = SystemNotificationChanged;
			if (handler == null) return;

			var args = new SystemNotificationEventArgs(systemNotification);
				
			handler(this, args);
		}

	}
}
