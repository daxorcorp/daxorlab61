using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Daxor.Lab.Infrastructure.Controls
{
	/// <summary>
	///  InnerGlowChrome draws consecutive rectangles each with a 
	///  lower alpha opacity than the previous to achive a smooth 
	///  fade from the edges on in.
	/// </summary>
	public class InnerGlow : Decorator
	{
		#region DEPENDENCY PROPERTIES

		public static readonly DependencyProperty GlowCornerRadiusXProperty =
			DependencyProperty.Register("GlowCornerRadiusX", typeof(double), typeof(InnerGlow), new PropertyMetadata(0.0, OnPropertiesChanged));

		public static readonly DependencyProperty GlowCornerRadiusYProperty =
			DependencyProperty.Register("GlowCornerRadiusY", typeof(double), typeof(InnerGlow), new PropertyMetadata(0.0, OnPropertiesChanged));

		public static readonly DependencyProperty GlowColorProperty =
			DependencyProperty.Register("GlowColor", typeof(Color), typeof(InnerGlow), new PropertyMetadata(Color.FromArgb(50, 0, 0, 0), OnPropertiesChanged));

		public static readonly DependencyProperty GlowSoftnessProperty =
			DependencyProperty.Register("GlowSoftness", typeof(int), typeof(InnerGlow), new PropertyMetadata(12, OnPropertiesChanged));

		public static readonly DependencyProperty GlowGrowProperty =
			DependencyProperty.Register("GlowGrow", typeof(int), typeof(InnerGlow), new PropertyMetadata(0, OnPropertiesChanged));
		#endregion DEPENDENCY PROPERTIES

		#region Class Variables

		private readonly double[] _lookupTable = new double[181];

		private List<Pen> _pens = new List<Pen>();

		private Rect _rec;
		private double _cornerX;
		private double _cornerY;
		private double _inc;
		private int _numRects;

		#endregion

		public InnerGlow()
		{
			for (int j = 0; j < 180; j++)
			{
				double x = DegToRads(j);
				_lookupTable[j] = (Math.Cos(x) + 1) * .5;
			}
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public double GlowCornerRadiusX
		{
			get { return (double)GetValue(GlowCornerRadiusXProperty); }
			set { SetValue(GlowCornerRadiusXProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public double GlowCornerRadiusY
		{
			get { return (double)GetValue(GlowCornerRadiusYProperty); }
			set { SetValue(GlowCornerRadiusYProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public Color GlowColor
		{
			get { return (Color)GetValue(GlowColorProperty); }
			set { SetValue(GlowColorProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public int GlowSoftness
		{
			get { return (int)GetValue(GlowSoftnessProperty); }
			set { SetValue(GlowSoftnessProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public int GlowGrow
		{
			get { return (int)GetValue(GlowGrowProperty); }
			set { SetValue(GlowGrowProperty, value); }
		}
 
		#region RENDERING

		protected override void OnRender(DrawingContext drawingContext)
		{
			var rec = new Rect
			{
				X = _rec.X,
				Y = _rec.X,
				Width = Pos(ActualWidth - 2) + 1,
				Height = Pos(ActualHeight - 2) + 1
			};

			var cornerX = _cornerX;
			var cornerY = _cornerY;

			for (var i = 0; i < _numRects; i++)
			{
				drawingContext.DrawRoundedRectangle(null, _pens[i], rec, cornerX, cornerY);

				rec.X ++;
				rec.Y ++;
				rec.Width = (rec.Width - 2 < 0) ? 0 : rec.Width - 2;
				rec.Height = (rec.Height - 2 < 0) ? 0 : rec.Height - 2;
				cornerX = (cornerX - 1 > 0) ? cornerX - 1 : 0;
				cornerY = (cornerY - 1 > 0) ? cornerY - 1 : 0;                
			}
		}

		#endregion

		#region PROPERTIES CHANGED CALLBACK

		private static void OnPropertiesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var innerGlow = (InnerGlow)d;

			innerGlow.SetProperties();
		}

		private void SetProperties()
		{
			_numRects = GlowSoftness;

			_cornerX = GlowCornerRadiusX - .5;
			_cornerY = GlowCornerRadiusY - .5;

			_rec.X = .5;
			_rec.Y = .5;

			// ReSharper disable once PossibleLossOfFraction
			_inc = 180 / _numRects;

			BuildBrushes();
			InvalidateVisual();
		}

		private void BuildBrushes()
		{
			_pens = new List<Pen>();

			for (var i = 0; i < _numRects; i++)
			{
				// angle
				var x = (int)(i * _inc);

				// Get the new Alpha Value, we are using a cosine equation to get both a smooth ease out and ease in.
				// We have previously stored these values in a lookup table to speed up this loop.
				var alpha = _lookupTable[x] * GlowColor.A;

				_pens.Add(new Pen(new SolidColorBrush(Color.FromArgb((byte)alpha, GlowColor.R, GlowColor.G, GlowColor.B)), 1.0));
			}
		}

		private static double Pos(double p)
		{
			return (p < 0) ? 0 : p;
		}

		private static double DegToRads(double p)
		{
			return p * Math.PI / 180;
		}

		#endregion
	}
}