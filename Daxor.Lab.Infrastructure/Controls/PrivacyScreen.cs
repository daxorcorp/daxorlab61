﻿using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Daxor.Lab.Infrastructure.Controls
{
	// The PrivacyScreen takes in 3 brushes:
	//         1) Background Gradient
	//         2) Logo to be tiled in the background
	//         3) Logo for the top-left of screen
	//
	// After attempting different approaches, we ended up with the following 
	// approach which performed the best.  Instead of drawing directly to the 
	// screen's DrawingContext, we are instead drawing to an intermediate 
	// DrawingContext, creating a Bitmap out of that, and drawing that Bitmap
	// to the screen.  There should be no concern that a bitmap is used as an
	// intermediate step since it is regenerated whenever the layout changes.  
	public class PrivacyScreen : FrameworkElement
	{
		public static readonly DependencyProperty BackgroundProperty =
			DependencyProperty.Register("Background", typeof(Brush), typeof(PrivacyScreen), new FrameworkPropertyMetadata(OnDrawingBrushChanged));
		
		public static readonly DependencyProperty DrawingBrushProperty =
			DependencyProperty.Register("DrawingBrush", typeof(DrawingBrush), typeof(PrivacyScreen), new PropertyMetadata(OnDrawingBrushChanged));

		public static readonly DependencyProperty LogoBrushProperty =
			DependencyProperty.Register("LogoBrush", typeof(DrawingBrush), typeof(PrivacyScreen), new FrameworkPropertyMetadata(OnDrawingBrushChanged));

		private DrawingImage _tileImageSource;
		private DrawingImage _logoImageSource;
		private RenderTargetBitmap _bitmap;

		public DrawingBrush DrawingBrush
		{
			get { return (DrawingBrush)GetValue(DrawingBrushProperty); }
			set { SetValue(DrawingBrushProperty, value); }
		}

		public Brush Background
		{
			get { return (Brush)GetValue(BackgroundProperty); }
			set { SetValue(BackgroundProperty, value); }
		}
		
		public DrawingBrush LogoBrush
		{
			get { return (DrawingBrush)GetValue(LogoBrushProperty); }
			set { SetValue(LogoBrushProperty, value); }
		}

		protected override void OnRender(DrawingContext drawingContext)
		{
			if (_tileImageSource == null || _logoImageSource == null)
			{
				return;
			}

			if (ActualWidth <= 0.0 || ActualHeight <= 0.0)
			{
				return;
			}

		    var curY = -60.0;
			var curOffset = 0.0;
			var offsetInterval = DrawingBrush.Viewbox.Width * 0.74;

			var drawingVisual = new DrawingVisual();
			var dc = drawingVisual.RenderOpen();

			// Draw the Background gradient
			dc.DrawRectangle(Background, null, new Rect(0, 0, ActualWidth, ActualHeight));

			// Loop through each row, until we reach the full height
			while (curY < ActualHeight)
			{
				curOffset -= offsetInterval;
				double curX = 0;

				// Tile Logos horizontally in each row
				while (curX < ActualWidth - curOffset)
				{
					if (curX + curOffset > -240)
						dc.DrawImage(_tileImageSource, new Rect(curX + curOffset, curY, DrawingBrush.Viewbox.Width, DrawingBrush.Viewbox.Height));

					curX += DrawingBrush.Viewbox.Width + 20;
				}

				curY += DrawingBrush.Viewbox.Height - 20;
			}

			// Draw the Logo in the upper left-hand corner
			dc.DrawImage(_logoImageSource, new Rect(30, 8, LogoBrush.Viewbox.Width, LogoBrush.Viewbox.Height));

			dc.Close();

			// Generate a bitmap out of our Temporary DrawingVisual
			_bitmap = new RenderTargetBitmap((int)ActualWidth, (int)ActualHeight, 96, 96, PixelFormats.Default);
			_bitmap.Render(drawingVisual);

			// Draw the bitmap to the Screen's drawing context
			drawingContext.DrawImage(_bitmap, new Rect(0, 0, ActualWidth, ActualHeight));
		}
		
		private static void OnDrawingBrushChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var privacyScreen = (PrivacyScreen)d;
			privacyScreen.UpdateProperties();
		}

		private void UpdateProperties()
		{
			if (DrawingBrush == null || LogoBrush == null)
			{
				return;
			}

			_tileImageSource = new DrawingImage();
			_logoImageSource = new DrawingImage();

			_tileImageSource.Drawing = DrawingBrush.Drawing;
			_logoImageSource.Drawing = LogoBrush.Drawing;

			if (_tileImageSource.CanFreeze)
			{
				_tileImageSource.Freeze();
			}

			if (_logoImageSource.CanFreeze)
			{
				_logoImageSource.Freeze();
			}

			InvalidateVisual();
		}
	}
}
