﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Daxor.Lab.Infrastructure.Controls
{
	/// <summary>
	//// Custom Panel that can be animated closed and open.  
	//// When the IsOpened Property changes, it either expands or contracts
	/// </summary>
	/// 
	public enum SlideDirections
	{
		Up, Down, Left, Right
	}

	public class SlideBorder : Border
	{
		public static readonly DependencyProperty IsOpenedProperty =
			DependencyProperty.Register("IsOpened", typeof(bool), typeof(SlideBorder), new PropertyMetadata(false, OnIsOpenedPropertyChanged));

		public static readonly DependencyProperty IsAnimatedProperty =
			DependencyProperty.Register("IsAnimated", typeof(bool), typeof(SlideBorder), new PropertyMetadata(true));

		public static readonly DependencyProperty SpeedMultiplierProperty =
			DependencyProperty.Register("SpeedMultiplier", typeof(double), typeof(SlideBorder), new PropertyMetadata(1.0));

		public static readonly DependencyProperty SlideDirectionProperty =
			DependencyProperty.Register("SlideDirection", typeof(SlideDirections), typeof(SlideBorder), new PropertyMetadata(SlideDirections.Up));

		private readonly TranslateTransform _trans = new TranslateTransform();
		private string _animProperty;
		private double _amount;
		private double _opacity;
		private Visibility _visibility;
		
		public SlideBorder()
		{
			RenderTransform = _trans;

			Loaded += SlideBorder_Loaded;
		}

		#region DEPENDENCY PROPERTIES
		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public bool IsOpened
		{
			get { return (bool)GetValue(IsOpenedProperty); }
			set { SetValue(IsOpenedProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public bool IsAnimated
		{
			get { return (bool)GetValue(IsAnimatedProperty); }
			set { SetValue(IsAnimatedProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public double SpeedMultiplier
		{
			get { return (double)GetValue(SpeedMultiplierProperty); }
			set { SetValue(SpeedMultiplierProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public SlideDirections SlideDirection
		{
			get { return (SlideDirections)GetValue(SlideDirectionProperty); }
			set { SetValue(SlideDirectionProperty, value); }
		}
		#endregion DEPENDENCY PROPERTIES
		
		protected override Size MeasureOverride(Size constraint)
		{
			Size returnSize = base.MeasureOverride(constraint);
			SetupAnimationValues();
			return returnSize;
		}

		private static void OnIsOpenedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var slideBorder = (SlideBorder)d;
			slideBorder.SetupAnimationValues();
			slideBorder.PlayStoryboard();
		}

		private void SlideBorder_Loaded(object sender, RoutedEventArgs e)
		{
			if (IsOpened) return;
			Opacity = 0;
			SetupAnimationValues();
			PlayStoryboard();
		}      

		#region CALLBACKS
		private void SetupAnimationValues()
		{            
			if (SlideDirection == SlideDirections.Up)
			{
				_animProperty = "(RenderTransform).(TranslateTransform.Y)";
				_amount = IsOpened ? 0 : -ActualHeight;
			}
			else if (SlideDirection == SlideDirections.Down)
			{
				_animProperty = "(RenderTransform).(TranslateTransform.Y)";
				_amount = IsOpened ? 0 : ActualHeight;                
			}
			else if (SlideDirection == SlideDirections.Left)
			{
				_animProperty = "(RenderTransform).(TranslateTransform.X)";
				_amount = IsOpened ? 0 : ActualWidth;
			}
			else if (SlideDirection == SlideDirections.Right)
			{
				_animProperty = "(RenderTransform).(TranslateTransform.X)";
				_amount = IsOpened ? 0 : -ActualWidth;
			}

			// Set some properties that are common to all SlideDirections
			if (IsOpened)
			{
				_opacity = 1;
				_visibility = Visibility.Visible;
				Visibility = _visibility;
			}
			else
			{
				_opacity = 0;
				_visibility = Visibility.Collapsed;
			}
		}
		#endregion CALLBACKS

		#region ANIMATION
		private void PlayStoryboard()
		{
			var easeCurve = new KeySpline(0, 0, 0.19, 1);
			var duration = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(800 * 1 / SpeedMultiplier));

			var key = new SplineDoubleKeyFrame(_amount, duration, easeCurve);
			var anim = new DoubleAnimationUsingKeyFrames();

			anim.KeyFrames.Add(key);

			Storyboard.SetTarget(anim, this);
			Storyboard.SetTargetProperty(anim, new PropertyPath(_animProperty));

			var opacityAnim = new DoubleAnimationUsingKeyFrames();
			var opacKey1 = new SplineDoubleKeyFrame(_opacity, duration, easeCurve);

			opacityAnim.KeyFrames.Add(opacKey1);

			Storyboard.SetTarget(opacityAnim, this);
			Storyboard.SetTargetProperty(opacityAnim, new PropertyPath(OpacityProperty));

			var scrollStoryboard = new Storyboard();
			scrollStoryboard.Children.Add(anim);
			scrollStoryboard.Children.Add(opacityAnim);

			scrollStoryboard.FillBehavior = FillBehavior.HoldEnd;
			scrollStoryboard.Completed += ScrollStoryboard_Completed;

			scrollStoryboard.Begin(this);
		}

		private void ScrollStoryboard_Completed(object sender, EventArgs e)
		{
			Visibility = _visibility;
		}
		#endregion ANIMATION
	}
}