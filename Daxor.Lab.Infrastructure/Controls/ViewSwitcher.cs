﻿using System.Windows;
using System.Windows.Controls;

namespace Daxor.Lab.Infrastructure.Controls
{
	public class ViewSwitcher : ItemsControl
	{
		// Using a DependencyProperty as the backing store for SelectedItem.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty SelectedItemProperty =
			DependencyProperty.Register("SelectedItem", typeof(FrameworkElement), typeof(ViewSwitcher), new FrameworkPropertyMetadata(SelectedItemChanged));

		public static readonly DependencyProperty SelectedIndexProperty =
			DependencyProperty.Register("SelectedIndex", typeof(int), typeof(ViewSwitcher), new FrameworkPropertyMetadata(0));

		public static readonly DependencyProperty IsAnimatedProperty =
		   DependencyProperty.Register("IsAnimated", typeof(bool), typeof(ViewSwitcher), new FrameworkPropertyMetadata(true));

		public int SelectedIndex
		{
			get { return (int)GetValue(SelectedIndexProperty); }
			set { SetValue(SelectedIndexProperty, value); }
		}

		public bool IsAnimated
		{
			get { return (bool)GetValue(IsAnimatedProperty); }
			set { SetValue(IsAnimatedProperty, value); }
		}

		public FrameworkElement SelectedItem
		{
			get { return (FrameworkElement)GetValue(SelectedItemProperty); }
			set { SetValue(SelectedItemProperty, value); }
		}

		private static void SelectedItemChanged(DependencyObject dp, DependencyPropertyChangedEventArgs args)
		{
			var viewSwitcher = dp as ViewSwitcher;
			var newElement = (FrameworkElement)args.NewValue;
			var oldElement = (FrameworkElement)args.OldValue;

			if (oldElement != null)
				oldElement.IsHitTestVisible = false;

            if (newElement == null) return;
			newElement.IsHitTestVisible = true;

			// ReSharper disable once PossibleNullReferenceException
			viewSwitcher.SelectedIndex = viewSwitcher.Items.IndexOf(newElement);
		}
	}
}
