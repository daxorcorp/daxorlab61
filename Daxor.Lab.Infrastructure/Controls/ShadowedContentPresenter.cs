﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace Daxor.Lab.Infrastructure.Controls
{
	public class ShadowedContentPresenter : ContentPresenter
	{
		 public static readonly DependencyProperty HasShadowProperty =
			DependencyProperty.Register("HasShadow", typeof(bool), typeof(ShadowedContentPresenter), new PropertyMetadata(true));

		public static readonly DependencyProperty ShadowProperty =
			DependencyProperty.Register("Shadow", typeof(SolidColorBrush), typeof(ShadowedContentPresenter), new PropertyMetadata(new SolidColorBrush(Color.FromArgb(175, 0, 0, 0))));
		
		public static readonly DependencyProperty ShadowXOffsetProperty =
			DependencyProperty.Register("ShadowXOffset", typeof(double), typeof(ShadowedContentPresenter), new PropertyMetadata(2.0));

		 public static readonly DependencyProperty ShadowYOffsetProperty =
			DependencyProperty.Register("ShadowYOffset", typeof(double), typeof(ShadowedContentPresenter), new PropertyMetadata(2.0));

		  // Using a DependencyProperty as the backing store for IsTabStop.  This enables animation, styling, binding, etc...
		 public static readonly DependencyProperty IsTabStopProperty =
			 DependencyProperty.Register("IsTabStop", typeof(bool), typeof(ShadowedContentPresenter), new UIPropertyMetadata(true));
	 
		static ShadowedContentPresenter()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(ShadowedContentPresenter), new FrameworkPropertyMetadata((object)null));
		}

		#region DEPENDENCY PROPERTIES
		// HasShadow Property
		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Description("Enables or Disables the Shadow when content is a string")]
		public bool HasShadow
		{
			get { return (bool)GetValue(HasShadowProperty); }
			set { SetValue(HasShadowProperty, value); }
		}

		// Shadow Property
		public SolidColorBrush Shadow
		{
			get { return (SolidColorBrush)GetValue(ShadowProperty); }
			set { SetValue(ShadowProperty, value); }
		}

		// ShadowXOffset Property
		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Description("The horizontal offset of the shadow")]
		public double ShadowXOffset
		{
			get { return (double)GetValue(ShadowXOffsetProperty); }
			set { SetValue(ShadowXOffsetProperty, value); }
		}

		// ShadowYOffset Property
		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Description("The vertical offset of the shadow.")]
		public double ShadowYOffset
		{
			get { return (double)GetValue(ShadowYOffsetProperty); }
			set { SetValue(ShadowYOffsetProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Description("True if element participates in tab navigation")]
		public bool IsTabStop
		{
			get { return (bool)GetValue(IsTabStopProperty); }
			set { SetValue(IsTabStopProperty, value); }
		}
		#endregion DEPENDENCY PROPERTIES

		protected override DataTemplate ChooseTemplate()
		{
			if (Content is string && HasShadow)
			{
				// create the data template
				var shadowedDataTemplate = new DataTemplate
				{
					DataType = typeof (ShadowedContentPresenter)
				};

				// set up the stack panel
				var shadFactory = new FrameworkElementFactory(typeof(ShadowedTextBlock));                
				shadFactory.SetBinding(ShadowedTextBlock.TextProperty, new Binding("Content") { Source = this });
				shadFactory.SetBinding(ShadowedTextBlock.ShadowProperty, new Binding("Shadow") { Source = this });
				shadFactory.SetBinding(ShadowedTextBlock.ShadowXOffsetProperty, new Binding("ShadowXOffset") { Source = this });
				shadFactory.SetBinding(ShadowedTextBlock.ShadowYOffsetProperty, new Binding("ShadowYOffset") { Source = this });
				shadFactory.SetBinding(Control.IsTabStopProperty, new Binding("IsTabStop") { Source = this });
				
				// set the visual tree of the data template
				shadowedDataTemplate.VisualTree = shadFactory;

				return shadowedDataTemplate;
			}

			// create the data template
			var defaultTemplate = new DataTemplate
			{
				DataType = typeof (ShadowedContentPresenter)
			};

			// set up the stack panel
			var defaultFactory = new FrameworkElementFactory(typeof(ContentPresenter));
			defaultFactory.SetBinding(ContentProperty, new Binding("Content") { Source = this });

			// set the visual tree of the data template
			defaultTemplate.VisualTree = defaultFactory;

			return defaultTemplate;
		}
	}
}