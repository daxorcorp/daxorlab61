﻿using System.Windows;
using System.Windows.Controls;

namespace Daxor.Lab.Infrastructure.Controls
{
    public class TestSetupTabControl : TabControl
    {
        public static readonly DependencyProperty PreTabsContentProperty =
           DependencyProperty.Register("PreTabsContent", typeof(object), typeof(TestSetupTabControl), new FrameworkPropertyMetadata((object)null));

        public static readonly DependencyProperty PostTabsContentProperty =
            DependencyProperty.Register("PostTabsContent", typeof(object), typeof(TestSetupTabControl), new FrameworkPropertyMetadata((object)null));

        static TestSetupTabControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TestSetupTabControl), new FrameworkPropertyMetadata(typeof(TestSetupTabControl)));
        }

        public object PreTabsContent
        {
            get { return GetValue(PreTabsContentProperty); }
            set { SetValue(PreTabsContentProperty, value); }
        }

        public object PostTabsContent
        {
            get { return GetValue(PostTabsContentProperty); }
            set { SetValue(PostTabsContentProperty, value); }
        }
    }
}