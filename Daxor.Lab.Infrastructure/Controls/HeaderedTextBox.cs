﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System;

namespace Daxor.Lab.Infrastructure.Controls
{
    public class HeaderedTextBox : TextBox
    {
        public static readonly DependencyProperty IsRequiredProperty =
            DependencyProperty.Register("IsRequired", typeof(bool), typeof(HeaderedTextBox), new FrameworkPropertyMetadata(false));

        public static readonly DependencyProperty HeaderProperty =
            DependencyProperty.Register("Header", typeof(string), typeof(HeaderedTextBox), new FrameworkPropertyMetadata(null));

        public static readonly DependencyProperty UnitsLabelProperty =
             DependencyProperty.Register("UnitsLabel", typeof(string), typeof(HeaderedTextBox), new FrameworkPropertyMetadata(String.Empty));

        public static readonly DependencyProperty WatermarkProperty =
            DependencyProperty.Register("Watermark", typeof(string), typeof(HeaderedTextBox), new FrameworkPropertyMetadata(String.Empty));

        public static readonly DependencyProperty UnitsFontSizeProperty =
            DependencyProperty.Register("UnitsFontSize", typeof(double), typeof(HeaderedTextBox), new UIPropertyMetadata(SystemFonts.MessageFontSize));

        static HeaderedTextBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(HeaderedTextBox), new FrameworkPropertyMetadata(typeof(HeaderedTextBox)));
        }

        #region DEPENDENCY PROPERTIES
        [Description("Specifies if the textbox entry is required."), Category("Common Properties")]
        public bool IsRequired
        {
            get { return (bool)GetValue(IsRequiredProperty); }
            set { SetValue(IsRequiredProperty, value); }
        }

        [Description("Specifies Header string to be displayed above the textbox."), Category("Common Properties")]
        public string Header
        {
            get { return (string)GetValue(HeaderProperty); }
            set { SetValue(HeaderProperty, value); }
        }

        [Description("Units label to be displayed withing the textBox."), Category("Common Properties")]
        public string UnitsLabel
        {
            get { return (string)GetValue(UnitsLabelProperty); }
            set { SetValue(UnitsLabelProperty, value); }
        }

        [Description("Watermark that gets displayed if textbox is empty"), Category("Common Properties")]
        public string Watermark
        {
            get { return (string)GetValue(WatermarkProperty); }
            set { SetValue(WatermarkProperty, value); }
        }

        [Description("Gets or sets the unit font size. The default value is the system dialog font size. The font size must be a positive number."), Category("Common Properties")]
        public double UnitsFontSize
        {
            get { return (double)GetValue(UnitsFontSizeProperty); }
            set { SetValue(UnitsFontSizeProperty, value); }
        }

        #endregion DEPENDENCY PROPERTIES
    }
}
