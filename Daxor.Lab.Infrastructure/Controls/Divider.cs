using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Daxor.Lab.Infrastructure.Controls
{
	/// <summary>
	/// A Lightweight Line
	/// </summary>
	public class Divider : FrameworkElement
	{
		public static readonly DependencyProperty LineThicknessProperty =
			DependencyProperty.Register("LineThickness", typeof(double), typeof(Divider), new PropertyMetadata(1.0, OnPropertiesChanged));

		public static readonly DependencyProperty Line1Property =
			DependencyProperty.Register("Line1", typeof(Brush), typeof(Divider), new PropertyMetadata(new SolidColorBrush(Color.FromArgb(255, 80, 80, 80)), OnPropertiesChanged));

		public static readonly DependencyProperty Line2Property =
			DependencyProperty.Register("Line2", typeof(Brush), typeof(Divider), new PropertyMetadata(new SolidColorBrush(Color.FromArgb(255, 255, 255, 255)), OnPropertiesChanged));

		public static readonly DependencyProperty EndLineCapProperty =
			DependencyProperty.Register("EndLineCap", typeof(PenLineCap), typeof(Divider), new PropertyMetadata(PenLineCap.Flat, OnPropertiesChanged));
		
		public static readonly DependencyProperty StartLineCapProperty =
			DependencyProperty.Register("StartLineCap", typeof(PenLineCap), typeof(Divider), new PropertyMetadata(PenLineCap.Flat, OnPropertiesChanged));

		public static readonly DependencyProperty OrientationProperty =
			DependencyProperty.Register("Orientation", typeof(Orientation), typeof(Divider), new PropertyMetadata(Orientation.Vertical, OnPropertiesChanged));

		public static readonly DependencyProperty IsDashedLineProperty =
			DependencyProperty.Register("IsDashedLine", typeof(bool), typeof(Divider), new PropertyMetadata(false, OnPropertiesChanged));

		public static readonly DependencyProperty DashesProperty =
			DependencyProperty.Register("Dashes", typeof(string), typeof(Divider), new PropertyMetadata("0,2", OnPropertiesChanged));

		public static readonly DependencyProperty DashOffsetProperty =
			DependencyProperty.Register("DashOffset", typeof(double), typeof(Divider), new PropertyMetadata(0.5, OnPropertiesChanged));

		#region CLASS VARIABLES
		private Pen _pen1 = new Pen();
		private Pen _pen2 = new Pen();
		private double _slip = 0.5;
		#endregion

		public Divider()
		{
			UpdateProperties();
		}

		#region DEPENDENCY PROPERTIES
		public Brush Line1
		{
			get { return (Brush)GetValue(Line1Property); }
			set { SetValue(Line1Property, value); }
		}

		public Brush Line2
		{
			get { return (Brush)GetValue(Line2Property); }
			set { SetValue(Line2Property, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public double LineThickness
		{
			get { return (double)GetValue(LineThicknessProperty); }
			set { SetValue(LineThicknessProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public PenLineCap EndLineCap
		{
			get { return (PenLineCap)GetValue(EndLineCapProperty); }
			set { SetValue(EndLineCapProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public PenLineCap StartLineCap
		{
			get { return (PenLineCap)GetValue(StartLineCapProperty); }
			set { SetValue(StartLineCapProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public Orientation Orientation
		{
			get { return (Orientation)GetValue(OrientationProperty); }
			set { SetValue(OrientationProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public bool IsDashedLine
		{
			get { return (bool)GetValue(IsDashedLineProperty); }
			set { SetValue(IsDashedLineProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		[Description(@"Describes the length of alternating dashes and gaps. The values are in terms of multiples of the LineThickness. For example, an array of 1,2 specifies a dash of length (1 * LineThickness) followed by a gap of length (2 * LineThickness). 

Negative values in the array are interpreted as their absolute value. 
")]
		public string Dashes
		{
			get { return (string)GetValue(DashesProperty); }
			set { SetValue(DashesProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public double DashOffset
		{
			get { return (double)GetValue(DashOffsetProperty); }
			set { SetValue(DashOffsetProperty, value); }
		}
		#endregion DEPENDENCY PROPERTIES
		
		#region Rendering and Positioning

		protected override void OnRender(DrawingContext drawingContext)
		{                        
			if (Orientation == Orientation.Vertical)
			{
				drawingContext.DrawLine(_pen1, new Point(-_slip, 0), new Point(-_slip, ActualHeight));
				drawingContext.DrawLine(_pen2, new Point(_slip, 0), new Point(_slip, ActualHeight));
			}
			else
			{
				drawingContext.DrawLine(_pen1, new Point(0, -_slip), new Point(ActualWidth, -_slip));
				drawingContext.DrawLine(_pen2, new Point(0, _slip), new Point(ActualWidth, _slip));
			}
		}
	  
		private static void OnPropertiesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var divider = (Divider)d;

			divider.UpdateProperties();
			divider.InvalidateVisual();
		}

		private void UpdateProperties()
		{
			var stringValues = Dashes.Split(',');

			var values = stringValues.Select(Convert.ToDouble).ToList();
			_pen1 = new Pen(Line1, LineThickness);
			_pen2 = new Pen(Line2, LineThickness);

			_pen1.EndLineCap = EndLineCap;
			_pen2.EndLineCap = EndLineCap;

			_pen1.StartLineCap = StartLineCap;
			_pen2.StartLineCap = StartLineCap;

			// ReSharper disable once CompareOfFloatsByEqualityOperator
			_slip = LineThickness % 2 == 0 ? 0.0 : 0.5;

			if (IsDashedLine)
			{
				_pen1.DashStyle = new DashStyle(values, DashOffset);
				_pen2.DashStyle = new DashStyle(values, DashOffset);
			}
		}
		#endregion
	}
}