﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace Daxor.Lab.Infrastructure.Controls
{
	/// <summary>
	//// Custom Panel that can be animated closed and open.  
	//// When the IsOpened Property changes, it either expands or contracts
	/// </summary>
	/// 
	public enum ExpandDirection
	{
		Width, Height
	}

	public class ExpandableBorder : Border
	{
		#region DEPENDENCY PROPERTIES
		public static readonly DependencyProperty IsOpenedProperty =
			DependencyProperty.Register("IsOpened", typeof(bool), typeof(ExpandableBorder), new PropertyMetadata(false, OnIsOpenedPropertyChanged));

		public static readonly DependencyProperty IsAnimatedProperty =
			DependencyProperty.Register("IsAnimated", typeof(bool), typeof(ExpandableBorder), new PropertyMetadata(true));

		public static readonly DependencyProperty SpeedMultiplierProperty =
			DependencyProperty.Register("SpeedMultiplier", typeof(double), typeof(ExpandableBorder), new PropertyMetadata(100.0));

		public static readonly DependencyProperty ExpandDirectionProperty =
			DependencyProperty.Register("ExpandDirection", typeof(ExpandDirection), typeof(ExpandableBorder), new PropertyMetadata(ExpandDirection.Height));

		#endregion DEPENDENCY PROPERTIES

		private double _currSize;
		private double _destSize;
		private bool _skipCustomArrange;

		private DependencyProperty _animProperty;

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public bool IsOpened
		{
			get { return (bool)GetValue(IsOpenedProperty); }
			set { SetValue(IsOpenedProperty, value); }
		}
		
		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public bool IsAnimated
		{
			get { return (bool)GetValue(IsAnimatedProperty); }
			set { SetValue(IsAnimatedProperty, value); }
		}
		
		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public double SpeedMultiplier
		{
			get { return (double)GetValue(SpeedMultiplierProperty); }
			set { SetValue(SpeedMultiplierProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public ExpandDirection ExpandDirection
		{
			get { return (ExpandDirection)GetValue(ExpandDirectionProperty); }
			set { SetValue(ExpandDirectionProperty, value); }
		}

		#region CONSTRUCTORS
		protected override void OnInitialized(EventArgs e)
		{
			base.OnInitialized(e);            
			
			if (!IsOpened)
			{
				_skipCustomArrange = true;
				SetupAnimation();
			}
		}
		#endregion CONSTRUCTORS

		#region ARRANGE OVERRIDE
		protected override Size ArrangeOverride(Size finalSize)
		{
			if (_skipCustomArrange || _animProperty == null)
			{
				return base.ArrangeOverride(finalSize);
			}

			_skipCustomArrange = true;

			// If animation is turned off, skip all that code
			if (!IsAnimated)
			{
				SetValue(_animProperty, IsOpened ? Double.NaN : 0.0);

				return base.ArrangeOverride(finalSize);
			}

			// get the resulting finalSize to use for animating
			finalSize = base.ArrangeOverride(finalSize);

			// Setup Animation parameters
			if (ExpandDirection == ExpandDirection.Width)
			{
				_currSize = ActualWidth;

				_destSize = IsOpened ? finalSize.Width : MinWidth;
			}
			else
			{
				_currSize = ActualHeight;

				_destSize = IsOpened ? finalSize.Height : MinHeight;
			}

			// set size back to its pre-temporary value 
			// since we want to Animate to this value.
			SetValue(_animProperty, _currSize);
			PlayStoryboard();

			return finalSize;
		}
		#endregion

		#region CALLBACKS
		private static void OnIsOpenedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var expandibleBorder = (ExpandableBorder)d;
			expandibleBorder.SetupAnimation();
		}

		private void SetupAnimation()
		{
			// Set a Dependency property to either Width or Height to keep
			// upcomming code cleaner by avoiding many conditional statements.
			_animProperty = ExpandDirection == ExpandDirection.Width ? WidthProperty : HeightProperty;

			// Temporarily set the Width or Height to Auto or Zero in
			// order to get its destination size in the ArrangeOverride Method
			SetValue(_animProperty, IsOpened ? Double.NaN : 0.0);

			_skipCustomArrange = false;
			InvalidateVisual();
		}
		#endregion CALLBACKS

		#region ANIMATION
		private void PlayStoryboard()
		{
			var easeCurve = new KeySpline(0, 0, 0, 1);
			var duration = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(300));

			var key = new SplineDoubleKeyFrame(_destSize, duration, easeCurve);

			var anim = new DoubleAnimationUsingKeyFrames();            
			anim.KeyFrames.Add(key);

			Storyboard.SetTarget(anim, this);
			Storyboard.SetTargetProperty(anim, new PropertyPath(_animProperty));

			var sizeStoryboard = new Storyboard();
			sizeStoryboard.Children.Add(anim);
			sizeStoryboard.FillBehavior = FillBehavior.Stop;
			sizeStoryboard.Completed += Storyboard_Completed;

			BeginStoryboard(sizeStoryboard);
		}

		private void Storyboard_Completed(object sender, EventArgs e)
		{
			SetValue(_animProperty, IsOpened ? Double.NaN : 0.0);
		}

		#endregion ANIMATION
	}
}