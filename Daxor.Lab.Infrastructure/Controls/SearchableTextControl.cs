﻿using System;
using System.Windows.Documents;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;

namespace Daxor.Lab.Infrastructure.Controls
{
	/// <summary>
	/// Borrowed from MSDN Virtual Labs.
	/// A Searchable / Highlight Text Control
	/// This control can be used in many places, like in one DataGrid to support searching function and highlight the text
	/// 
	/// Author: Bob Bao, Chinasoft, MSDN
	/// 
	/// Modified: Added verification that control template contains required TextBlock
	/// </summary>
	public class SearchableTextControl : Control
	{
		static SearchableTextControl()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(SearchableTextControl), new FrameworkPropertyMetadata(typeof(SearchableTextControl)));
		}

		#region DPs

		public string Text
		{
			get { return (string)GetValue(TextProperty); }
			set { SetValue(TextProperty, value); }
		}
		public static readonly DependencyProperty TextProperty =
			DependencyProperty.Register("Text", typeof(string), typeof(SearchableTextControl), new UIPropertyMetadata(string.Empty,
			  UpdateControlCallBack));

		public Brush HighlightBackground
		{
			get { return (Brush)GetValue(HighlightBackgroundProperty); }
			set { SetValue(HighlightBackgroundProperty, value); }
		}
		public static readonly DependencyProperty HighlightBackgroundProperty =
			DependencyProperty.Register("HighlightBackground", typeof(Brush), typeof(SearchableTextControl), new UIPropertyMetadata(Brushes.Yellow, UpdateControlCallBack));

		public Brush HighlightForeground
		{
			get { return (Brush)GetValue(HighlightForegroundProperty); }
			set { SetValue(HighlightForegroundProperty, value); }
		}
		public static readonly DependencyProperty HighlightForegroundProperty =
			DependencyProperty.Register("HighlightForeground", typeof(Brush), typeof(SearchableTextControl), new UIPropertyMetadata(Brushes.Black, UpdateControlCallBack));

		public bool IsMatchCase
		{
			get { return (bool)GetValue(IsMatchCaseProperty); }
			set { SetValue(IsMatchCaseProperty, value); }
		}
		public static readonly DependencyProperty IsMatchCaseProperty =
			DependencyProperty.Register("IsMatchCase", typeof(bool), typeof(SearchableTextControl), new UIPropertyMetadata(true, UpdateControlCallBack));

		public bool IsHighlight
		{
			get { return (bool)GetValue(IsHighlightProperty); }
			set { SetValue(IsHighlightProperty, value); }
		}
		public static readonly DependencyProperty IsHighlightProperty =
			DependencyProperty.Register("IsHighlight", typeof(bool), typeof(SearchableTextControl), new UIPropertyMetadata(false, UpdateControlCallBack));

		public string SearchText
		{
			get { return (string)GetValue(SearchTextProperty); }
			set { SetValue(SearchTextProperty, value); }
		}
		public static readonly DependencyProperty SearchTextProperty =
			DependencyProperty.Register("SearchText", typeof(string), typeof(SearchableTextControl), new UIPropertyMetadata(string.Empty,
			  UpdateControlCallBack));

		private static void UpdateControlCallBack(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var obj = d as SearchableTextControl;
			// ReSharper disable once PossibleNullReferenceException
			obj.InvalidateVisual();
		}
		#endregion

		#region Overrides

		protected override void OnRender(DrawingContext drawingContext)
		{
			var displayTextBlock = Template.FindName("PART_InternalTextBlock", this) as TextBlock;
			if (displayTextBlock == null)
				throw new NullReferenceException("Expecting PART_InternalTextBlock in control template");

			displayTextBlock.Inlines.Clear();

			if (string.IsNullOrEmpty(Text))
			{
				displayTextBlock.Text = Text;
				base.OnRender(drawingContext);
				return;
			}
			if (!IsHighlight || String.IsNullOrEmpty(SearchText))
			{
				displayTextBlock.Text = Text;
				base.OnRender(drawingContext);
				return;
			}

			var searchstring = IsMatchCase ? SearchText : SearchText.ToUpper();
			var compareText = IsMatchCase ? Text : Text.ToUpper();
			var displayText = Text;

			Run run;
			while (!string.IsNullOrEmpty(searchstring) && compareText.IndexOf(searchstring, StringComparison.Ordinal) >= 0)
			{
				var position = compareText.IndexOf(searchstring, StringComparison.Ordinal);

				run = GenerateRun(displayText.Substring(0, position), false);
				if (run != null) displayTextBlock.Inlines.Add(run);
				run = GenerateRun(displayText.Substring(position, searchstring.Length), true);
				if (run != null) displayTextBlock.Inlines.Add(run);

				compareText = compareText.Substring(position + searchstring.Length);
				displayText = displayText.Substring(position + searchstring.Length);
			}
			run = GenerateRun(displayText, false);
			if (run != null) displayTextBlock.Inlines.Add(run);

			base.OnRender(drawingContext);
		}

		#endregion

		#region Helpers

		private Run GenerateRun(string searchedString, bool isHighlight)
		{
			if (string.IsNullOrEmpty(searchedString)) return null;
			var run = new Run(searchedString)
			{
				Background = isHighlight ? HighlightBackground : Background,
				Foreground = isHighlight ? HighlightForeground : Foreground
			};
			return run;
		}

		#endregion
	}
}
