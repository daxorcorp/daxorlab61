using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace Daxor.Lab.Infrastructure.Controls
{
	[Description("To be used as a Child of Renderer and gives you a lightweight alternative for custom looks")]
	public class RectangularShape : FrameworkElement
	{
		public static readonly DependencyProperty OnProperty =
			DependencyProperty.Register("On", typeof(bool), typeof(RectangularShape), new PropertyMetadata(true, OnOnChanged));
		
		public static readonly DependencyProperty FillProperty =
			DependencyProperty.Register("Fill", typeof(Brush), typeof(RectangularShape), new PropertyMetadata(null, OnFillChanged));

		public static readonly DependencyProperty StrokeProperty =
					DependencyProperty.Register("Stroke", typeof(Brush), typeof(RectangularShape), new PropertyMetadata(null, OnStrokeChanged));
		
		public static readonly DependencyProperty StrokeAmountProperty =
			DependencyProperty.Register("StrokeAmount", typeof(double), typeof(RectangularShape), new PropertyMetadata(0.0, OnStrokeAmountChanged));

		public static readonly DependencyProperty StrokeLeftProperty =
			DependencyProperty.Register("StrokeLeft", typeof(bool), typeof(RectangularShape), new PropertyMetadata(true, OnStrokeLeftChanged));

		public static readonly DependencyProperty StrokeTopProperty =
			DependencyProperty.Register("StrokeTop", typeof(bool), typeof(RectangularShape), new PropertyMetadata(true, OnStrokeTopChanged));

		public static readonly DependencyProperty StrokeRightProperty =
					DependencyProperty.Register("StrokeRight", typeof(bool), typeof(RectangularShape), new PropertyMetadata(true, OnStrokeRightChanged));

		public static readonly DependencyProperty StrokeBottomProperty =
					DependencyProperty.Register("StrokeBottom", typeof(bool), typeof(RectangularShape), new PropertyMetadata(true, OnStrokeBottomChanged));

		public static readonly DependencyProperty CornerRadiusProperty =
					DependencyProperty.Register("CornerRadius", typeof(CornerRadius), typeof(RectangularShape), new PropertyMetadata(new CornerRadius(0), OnCornerRadiusChanged));

		public static readonly DependencyProperty MarginLeftAsPercentProperty =
			DependencyProperty.Register("MarginLeftAsPercent", typeof(bool), typeof(RectangularShape), new PropertyMetadata(false, OnMarginLeftAsPercentageChanged));

		public static readonly DependencyProperty MarginRightAsPercentProperty =
			DependencyProperty.Register("MarginRightAsPercent", typeof(bool), typeof(RectangularShape), new PropertyMetadata(false, OnMarginRightAsPercentageChanged));

		public static readonly DependencyProperty MarginTopAsPercentProperty =
					DependencyProperty.Register("MarginTopAsPercent", typeof(bool), typeof(RectangularShape), new PropertyMetadata(false, OnMarginTopAsPercentageChanged));

		public static readonly DependencyProperty MarginBottomAsPercentProperty =
			DependencyProperty.Register("MarginBottomAsPercent", typeof(bool), typeof(RectangularShape), new PropertyMetadata(false, OnMarginBottomAsPercentageChanged));
		
		static RectangularShape()
		{
			OpacityProperty.OverrideMetadata(typeof(RectangularShape), new FrameworkPropertyMetadata(OnOpacityChanged));
			MarginProperty.OverrideMetadata(typeof(RectangularShape), new FrameworkPropertyMetadata(OnMarginChanged));
		}

		public event EventHandler PropertiesChanged = delegate { };

		public Brush FillInternal { get; set; }

		public Brush StrokeInternal { get; set; }
		
		public Pen StrokePen { get; set; }
		
		public bool IsSimple { get; set; }
		
		public Thickness MarginInternal { get; set; }
		
		public bool ShouldDisplay { get; set; }
		
		public Size AvailableSize { get; set; }
		
		public double StrokeOffsetLeft { get; set; }
		
		public double StrokeOffsetTop { get; set; }
		
		public double StrokeOffsetRight { get; set; }
		
		public double StrokeOffsetBottom { get; set; }
		
		public Rect RenderRect { get; set; }
		
		public PathGeometry Geometry { get; set; }

		#region DEPENDENCY PROPERTIES   
		[Category("Appearance")]
		public bool On
		{
			get { return (bool)GetValue(OnProperty); }
			set { SetValue(OnProperty, value); }
		}

		public Brush Fill
		{
			get { return (Brush)GetValue(FillProperty); }
			set { SetValue(FillProperty, value); }
		}

		public Brush Stroke
		{
			get { return (Brush)GetValue(StrokeProperty); }
			set { SetValue(StrokeProperty, value); }
		}

		[Category("Appearance")]
		public double StrokeAmount
		{
			get { return (double)GetValue(StrokeAmountProperty); }
			set { SetValue(StrokeAmountProperty, value); }
		}

		[Category("Appearance")]
		public bool StrokeLeft
		{
			get { return (bool)GetValue(StrokeLeftProperty); }
			set { SetValue(StrokeLeftProperty, value); }
		}

		[Category("Appearance")]
		public bool StrokeTop
		{
			get { return (bool)GetValue(StrokeTopProperty); }
			set { SetValue(StrokeTopProperty, value); }
		}

		[Category("Appearance")]
		public bool StrokeRight
		{
			get { return (bool)GetValue(StrokeRightProperty); }
			set { SetValue(StrokeRightProperty, value); }
		}

		[Category("Appearance")]
		public bool StrokeBottom
		{
			get { return (bool)GetValue(StrokeBottomProperty); }
			set { SetValue(StrokeBottomProperty, value); }
		}

		[Category("Appearance")]
		public CornerRadius CornerRadius
		{
			get { return (CornerRadius)GetValue(CornerRadiusProperty); }
			set { SetValue(CornerRadiusProperty, value); }
		}

		[Category("Layout")]
		public bool MarginLeftAsPercent
		{
			get { return (bool)GetValue(MarginLeftAsPercentProperty); }
			set { SetValue(MarginLeftAsPercentProperty, value); }
		}

		[Category("Layout")]
		public bool MarginRightAsPercent
		{
			get { return (bool)GetValue(MarginRightAsPercentProperty); }
			set { SetValue(MarginRightAsPercentProperty, value); }
		}

		[Category("Layout")]
		public bool MarginTopAsPercent
		{
			get { return (bool)GetValue(MarginTopAsPercentProperty); }
			set { SetValue(MarginTopAsPercentProperty, value); }
		}

		[Category("Layout")]
		public bool MarginBottomAsPercent
		{
			get { return (bool)GetValue(MarginBottomAsPercentProperty); }
			set { SetValue(MarginBottomAsPercentProperty, value); }
		}

		#endregion

		public void SizeHasChanged()
		{
			CalcPercentageMargins();
			RectOrGeometry();
			Notify();
		}

		#region DEPENDENCY PROPERTY CALLBACKS

		protected static void OnOnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.ShouldItDisplay();
			rectangularShape.Notify();
		}

		protected static void OnFillChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.ApplyOpacityToFill();
			rectangularShape.ShouldItDisplay();
			rectangularShape.Notify();
		}        

		protected static void OnStrokeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;           
			rectangularShape.ApplyOpacityToStroke();
			rectangularShape.UpdateStrokePen();
			rectangularShape.CalcStrokeOffsets();
			rectangularShape.ShouldItDisplay();
			rectangularShape.Notify();            
		}        

		protected static void OnStrokeAmountChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.UpdateStrokePen();
			rectangularShape.CalcStrokeOffsets();
			rectangularShape.RectOrGeometry();                        
			rectangularShape.Notify();
		}
		
		protected static void OnStrokeLeftChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.CalcStrokeOffsets();
			rectangularShape.RectOrGeometry();            
			rectangularShape.Notify();
		}

		protected static void OnStrokeTopChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.CalcStrokeOffsets();
			rectangularShape.RectOrGeometry();            
			rectangularShape.Notify();
		}

		protected static void OnStrokeRightChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.CalcStrokeOffsets();
			rectangularShape.RectOrGeometry();            
			rectangularShape.Notify();
		}

		protected static void OnStrokeBottomChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.CalcStrokeOffsets();
			rectangularShape.RectOrGeometry();
			rectangularShape.Notify();
		}

		protected static void OnCornerRadiusChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.RectOrGeometry();            
			rectangularShape.Notify();
		}

		protected static void OnMarginChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.CalcPercentageMargins();
			rectangularShape.RectOrGeometry();            
			rectangularShape.Notify();
		}

		protected static void OnOpacityChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.ApplyOpacityToFill();
			rectangularShape.ApplyOpacityToStroke();
			rectangularShape.ShouldItDisplay();
			rectangularShape.Notify();
		}

		protected static void OnMarginLeftAsPercentageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.CalcPercentageMargins();
			rectangularShape.RectOrGeometry();            
			rectangularShape.Notify();
		}

		protected static void OnMarginRightAsPercentageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.CalcPercentageMargins();
			rectangularShape.RectOrGeometry();
			rectangularShape.Notify();
		}

		protected static void OnMarginTopAsPercentageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.CalcPercentageMargins();
			rectangularShape.RectOrGeometry();
			rectangularShape.Notify();
		}

		protected static void OnMarginBottomAsPercentageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var rectangularShape = (RectangularShape)d;
			rectangularShape.CalcPercentageMargins();
			rectangularShape.RectOrGeometry();
			rectangularShape.Notify();
		}

		#endregion        

		#region OVERRIDES (Empty out Measure, Arrange, and OnRender)
		protected override Size MeasureOverride(Size availableSize)
		{
			var returnSize = new Size(0, 0);

			returnSize.Width = Double.IsInfinity(availableSize.Width) ? returnSize.Width : availableSize.Width;
			returnSize.Height = Double.IsInfinity(availableSize.Height) ? returnSize.Height : availableSize.Height;

			return returnSize;
		}

		protected override Size ArrangeOverride(Size finalSize)
		{
			return finalSize;
		}

		protected override void OnRender(DrawingContext drawingContext)
		{
		}

		#endregion

		#region PRIVATE METHODS

		private void ApplyOpacityToFill()
		{
			if (Fill == null)
			{
				return;
			}

			Brush fill = Fill.Clone();

			// Apply Opacity to Fill
			fill.Opacity = Opacity;
			fill.Freeze();

			FillInternal = fill;
		}

		private void ApplyOpacityToStroke()
		{
			if (Stroke == null)
			{
				return;
			}

			Brush stroke = Stroke.Clone();

			// Apply Opacity to Stroke
			stroke.Opacity = Opacity;
			stroke.Freeze();

			StrokeInternal = stroke;
		}

		private void ShouldItDisplay()
		{
			ShouldDisplay = true;

			if (!On)
			{
				ShouldDisplay = false;
			}

			if (Fill == null && Stroke == null)
			{
				ShouldDisplay = false;
			}

			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if (Opacity == 0)
			{
				ShouldDisplay = false;
			}
		}

		private void UpdateStrokePen()
		{
			StrokePen = new Pen(StrokeInternal, StrokeAmount)
			{
				StartLineCap = PenLineCap.Flat,
				EndLineCap = PenLineCap.Flat,
				DashCap = PenLineCap.Flat
			};
			StrokePen.Freeze();
		}        

		private void CalcStrokeOffsets()
		{
			double strokeOffset = StrokeAmount / 2;

			StrokeOffsetLeft = (StrokeLeft && StrokeAmount > 0 && Stroke != null) ? strokeOffset : 0;
			StrokeOffsetTop = (StrokeTop && StrokeAmount > 0 && Stroke != null) ? strokeOffset : 0;
			StrokeOffsetRight = (StrokeRight && StrokeAmount > 0 && Stroke != null) ? strokeOffset : 0;
			StrokeOffsetBottom = (StrokeBottom && StrokeAmount > 0 && Stroke != null) ? strokeOffset : 0;
		}

		private void CalcPercentageMargins()
		{
			Thickness margin = Margin;

			// Are Margins Percentages?
			if (MarginLeftAsPercent)
			{
				margin.Left = Margin.Left * .01 * AvailableSize.Width;
			}

			if (MarginRightAsPercent)
			{
				margin.Right = Margin.Right * .01 * AvailableSize.Width;
			}

			if (MarginTopAsPercent)
			{
				margin.Top = Margin.Top * .01 * AvailableSize.Height;
			}

			if (MarginBottomAsPercent)
			{
				margin.Bottom = Margin.Bottom * .01 * AvailableSize.Height;
			}

			MarginInternal = margin;
		}

		private void RectOrGeometry()
		{
			IsItSimple();

			if (IsSimple)
			{
				CalcRenderRect();
			}
			else
			{
				CalcGeometry();
			}
		}

		private void IsItSimple()
		{
			IsSimple = true;

			// Check if CornerRadii are equal
			// ReSharper disable CompareOfFloatsByEqualityOperator
			if (!((CornerRadius.TopLeft == CornerRadius.TopRight) && (CornerRadius.TopLeft == CornerRadius.BottomRight) && (CornerRadius.TopLeft == CornerRadius.BottomLeft)))
			// ReSharper restore CompareOfFloatsByEqualityOperator
			{
				IsSimple = false;
			}

			// ReSharper disable once CompareOfFloatsByEqualityOperator
			if (StrokeAmount == 0.0 || Stroke == null) return;
			// Check if Stroked edges are equal
			if (!(StrokeLeft == StrokeRight && StrokeLeft == StrokeTop && StrokeLeft == StrokeBottom))
			{
				IsSimple = false;
			}
		}       

		private void CalcRenderRect()
		{
			if (StrokeLeft)
			{
				double width = AvailableSize.Width - StrokeOffsetRight - MarginInternal.Right - StrokeOffsetLeft - MarginInternal.Left;
				double height = AvailableSize.Height - StrokeOffsetBottom - MarginInternal.Bottom - StrokeOffsetTop - MarginInternal.Top;

				width = (width < 0) ? 0 : width;
				height = (height < 0) ? 0 : height;

				RenderRect = new Rect(StrokeOffsetLeft + MarginInternal.Left, StrokeOffsetTop + MarginInternal.Top, width, height);
			}
			else
			{
				double width = AvailableSize.Width - MarginInternal.Right - MarginInternal.Left;
				double height = AvailableSize.Height - MarginInternal.Bottom - MarginInternal.Top;

				width = (width < 0) ? 0 : width;
				height = (height < 0) ? 0 : height;

				RenderRect = new Rect(MarginInternal.Left, MarginInternal.Top, width, height);
			}
		}

		private void CalcGeometry()
		{
			// Points Clockwise
			var pointTop1 = new Point(CornerRadius.TopLeft + StrokeOffsetLeft + MarginInternal.Left, StrokeOffsetTop + MarginInternal.Top);
			var pointTop2 = new Point(AvailableSize.Width - CornerRadius.TopRight - StrokeOffsetRight - MarginInternal.Right, StrokeOffsetTop + MarginInternal.Top);
			var pointRight1 = new Point(AvailableSize.Width - StrokeOffsetRight - MarginInternal.Right, CornerRadius.TopRight + StrokeOffsetTop + MarginInternal.Top);
			var pointRight2 = new Point(AvailableSize.Width - StrokeOffsetRight - MarginInternal.Right, AvailableSize.Height - CornerRadius.BottomRight - StrokeOffsetBottom - MarginInternal.Bottom);
			var pointBottom1 = new Point(AvailableSize.Width - CornerRadius.BottomRight - StrokeOffsetRight - MarginInternal.Right, AvailableSize.Height - StrokeOffsetBottom - MarginInternal.Bottom);
			var pointBottom2 = new Point(CornerRadius.BottomLeft + StrokeOffsetLeft + MarginInternal.Left, AvailableSize.Height - StrokeOffsetBottom - MarginInternal.Bottom);
			var pointLeft1 = new Point(StrokeOffsetLeft + MarginInternal.Left, AvailableSize.Height - CornerRadius.BottomLeft - StrokeOffsetBottom - MarginInternal.Bottom);
			var pointLeft2 = new Point(StrokeOffsetLeft + MarginInternal.Left, CornerRadius.TopLeft + StrokeOffsetTop + MarginInternal.Top);

			PathFigure myPathFigure = new PathFigure();

			// Starting Point
			myPathFigure.StartPoint = pointTop1;

			// Top Line Segment
			myPathFigure.Segments.Add(new LineSegment(pointTop2, StrokeTop));

			// Arc Top Right
			if (CornerRadius.TopRight > 0)
			{
				myPathFigure.Segments.Add(new ArcSegment(pointRight1, new Size(CornerRadius.TopRight, CornerRadius.TopRight), 45, false, /* IsLargeArc */ SweepDirection.Clockwise, StrokeTop && StrokeRight));
			}

			// Right Line Segment
			myPathFigure.Segments.Add(new LineSegment(pointRight2, StrokeRight));

			// Arc Bottom Right
			if (CornerRadius.BottomRight > 0)
			{
				myPathFigure.Segments.Add(new ArcSegment(pointBottom1, new Size(CornerRadius.BottomRight, CornerRadius.BottomRight), 45, false, /* IsLargeArc */ SweepDirection.Clockwise, StrokeBottom && StrokeRight));
			}

			// Bottom Line Segment
			myPathFigure.Segments.Add(new LineSegment(pointBottom2, StrokeBottom));

			// Arc Bottom Left
			if (CornerRadius.BottomLeft > 0)
			{
				myPathFigure.Segments.Add(new ArcSegment(pointLeft1, new Size(CornerRadius.BottomLeft, CornerRadius.BottomLeft), 45, false, /* IsLargeArc */ SweepDirection.Clockwise, StrokeBottom && StrokeLeft));
			}

			// Left Line Segment
			myPathFigure.Segments.Add(new LineSegment(pointLeft2, StrokeLeft));

			// Arc Top Left
			if (CornerRadius.TopLeft > 0)
			{
				myPathFigure.Segments.Add(new ArcSegment(pointTop1, new Size(CornerRadius.TopLeft, CornerRadius.TopLeft), 45, false, /* IsLargeArc */ SweepDirection.Clockwise, StrokeTop && StrokeLeft));
			}

			// Create a PathGeometry to contain the figure.
			Geometry = new PathGeometry();
			Geometry.Figures.Add(myPathFigure);
		}

		private void Notify()
		{
			PropertiesChanged(this, EventArgs.Empty);            
		}
		#endregion
	}
}