using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Daxor.Lab.Infrastructure.Controls
{
	/// <summary>
	///  DropShadow draws consecutive rectangles each with a 
	///  lower alpha opacity than the previous to achive a smooth 
	///  fade from the edges on out.
	/// </summary>
	public class DropShadow : Decorator
	{
		#region DEPENDENCY PROPERTIES
		public static readonly DependencyProperty ShadowCornerRadiusXProperty =
			DependencyProperty.Register("ShadowCornerRadiusX", typeof(double), typeof(DropShadow), new PropertyMetadata(0.0, OnPropertiesChanged));

		public static readonly DependencyProperty ShadowCornerRadiusYProperty =
			DependencyProperty.Register("ShadowCornerRadiusY", typeof(double), typeof(DropShadow), new PropertyMetadata(0.0, OnPropertiesChanged));

		public static readonly DependencyProperty ShadowOffsetXProperty =
			DependencyProperty.Register("ShadowOffsetX", typeof(double), typeof(DropShadow), new PropertyMetadata(4.0, OnPropertiesChanged));

		public static readonly DependencyProperty ShadowOffsetYProperty =
			DependencyProperty.Register("ShadowOffsetY", typeof(double), typeof(DropShadow), new PropertyMetadata(4.0, OnPropertiesChanged));

		public static readonly DependencyProperty ShadowColorProperty =
			DependencyProperty.Register("ShadowColor", typeof(Color), typeof(DropShadow), new PropertyMetadata(Color.FromArgb(50, 0, 0, 0), OnPropertiesChanged));

		public static readonly DependencyProperty ShadowSoftnessProperty =
			DependencyProperty.Register("ShadowSoftness", typeof(int), typeof(DropShadow), new PropertyMetadata(12, OnPropertiesChanged));

		public static readonly DependencyProperty ShadowGrowProperty =
			DependencyProperty.Register("ShadowGrow", typeof(int), typeof(DropShadow), new PropertyMetadata(0, OnPropertiesChanged));
		#endregion DEPENDENCY PROPERTIES
		
		private readonly double[] _lookupTable = new double[181];
		private List<SolidColorBrush> _brushes = new List<SolidColorBrush>();
		private Rect _rec;
		private double _cornerX;
		private double _cornerY;
		private double _inc;
		private int _numRects;

		/// <summary>
		/// Initializes a new instance of the <strong><see cref="DropShadow"/></strong> class.
		/// </summary>
		public DropShadow()
		{
			for (var j = 0; j < 180; j++)
			{
				var x = DegToRads(j);
				_lookupTable[j] = (Math.Cos(x) + 1) * .5;
			}

			Loaded += DropShadow_Loaded;
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public double ShadowCornerRadiusX
		{
			get { return (double)GetValue(ShadowCornerRadiusXProperty); }
			set { SetValue(ShadowCornerRadiusXProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public double ShadowCornerRadiusY
		{
			get { return (double)GetValue(ShadowCornerRadiusYProperty); }
			set { SetValue(ShadowCornerRadiusYProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public double ShadowOffsetX
		{
			get { return (double)GetValue(ShadowOffsetXProperty); }
			set { SetValue(ShadowOffsetXProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public double ShadowOffsetY
		{
			get { return (double)GetValue(ShadowOffsetYProperty); }
			set { SetValue(ShadowOffsetYProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public Color ShadowColor
		{
			get { return (Color)GetValue(ShadowColorProperty); }
			set { SetValue(ShadowColorProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public int ShadowSoftness
		{
			get { return (int)GetValue(ShadowSoftnessProperty); }
			set { SetValue(ShadowSoftnessProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public int ShadowGrow
		{
			get { return (int)GetValue(ShadowGrowProperty); }
			set { SetValue(ShadowGrowProperty, value); }
		}

		#region Rendering and Positioning

		/// <summary>
		/// Render callback.  
		/// </summary>
		protected override void OnRender(DrawingContext drawingContext)
		{
			var rec = new Rect
			{
				X = _rec.X,
				Y = _rec.Y,
				Width = Pos(ActualWidth) + _rec.Width,
				Height = Pos(ActualHeight) + _rec.Height
			};

			var cornerX = _cornerX;
			var cornerY = _cornerY;

			for (var i = _numRects; i > 0; i--)
			{
				drawingContext.DrawRoundedRectangle(_brushes[_numRects - i], null, rec, cornerX, cornerY);

				rec.X++;
				rec.Y++;

				if (rec.Width - 2 > 0 && rec.Height - 2 > 0)
				{
					rec.Width -= 2;
					rec.Height -= 2;
				}
				else
				{
					i = 0;
				}

				cornerX--;
				cornerY--;
			}
		}

		#endregion
		
		private static void OnPropertiesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var dropShadow = (DropShadow)d;

			dropShadow.SetProperties();
		}

		#region Initalization
		private void DropShadow_Loaded(object sender, RoutedEventArgs e)
		{
			SetProperties();
		}

		#endregion Constructors

		#region PROPERTIES CHANGED CALLBACK
		private void SetProperties()
		{
			_numRects = ShadowSoftness;

			// ReSharper disable PossibleLossOfFraction
			_cornerX = ShadowCornerRadiusX + _numRects / 2;
			_cornerY = ShadowCornerRadiusY + _numRects / 2;

			_rec.X = -((_numRects / 2) + ShadowGrow - ShadowOffsetX);
			_rec.Y = -((_numRects / 2) + ShadowGrow - ShadowOffsetY);
			_rec.Width = ((_numRects / 2) + ShadowGrow) * 2;
			_rec.Height = ((_numRects / 2) + ShadowGrow) * 2;

			_inc = 180 / _numRects;
			// ReSharper restore PossibleLossOfFraction

			BuildBrushes();
			InvalidateVisual();
		}

		private void BuildBrushes()
		{
			double lastAlpha = 0;

			_brushes = new List<SolidColorBrush>();

			for (var i = _numRects; i > 0; i--)
			{
				// angle
				var x = (int)(i * _inc);

				// Get the new Alpha Value, we are using a cosine equation to get both a smooth ease out and ease in.
				// We have previously stored these values in a lookup table to speed up this loop.
				var alpha = _lookupTable[x] * ShadowColor.A;
				alpha = alpha - lastAlpha;
				_brushes.Add(new SolidColorBrush(Color.FromArgb((byte)alpha, ShadowColor.R, ShadowColor.G, ShadowColor.B)));

				lastAlpha = alpha;
			}
		}

		private static double Pos(double p)
		{
			return (p < 0) ? 0 : p;
		}

		private static double DegToRads(double degrees)
		{
			return degrees * Math.PI / 180;
		}
		#endregion
	}
}