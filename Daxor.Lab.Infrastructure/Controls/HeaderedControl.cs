﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Daxor.Lab.Infrastructure.Controls
{
    public class HeaderedControl : HeaderedContentControl
    {
        #region DEPENDENCY PROPERTIES
        
        public static readonly DependencyProperty IsRequiredProperty = 
            DependencyProperty.Register("IsRequired", typeof(bool), typeof(HeaderedControl), new FrameworkPropertyMetadata(false));

        public static readonly DependencyProperty UnitsProperty =
            DependencyProperty.Register("Units", typeof(string), typeof(HeaderedControl));

        public static readonly DependencyProperty RequiredFillProperty =
            DependencyProperty.Register("RequiredFill", typeof(Brush), typeof(HeaderedControl), new UIPropertyMetadata(Brushes.Transparent));

        public static readonly DependencyProperty UnitsFontSizeProperty =
            DependencyProperty.Register("UnitsFontSize", typeof(double), typeof(HeaderedControl), new UIPropertyMetadata(13.333));

        #endregion

        static HeaderedControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(HeaderedControl), new FrameworkPropertyMetadata(typeof(HeaderedControl)));
        }

        public bool IsRequired
        {
            get { return (bool)GetValue(IsRequiredProperty); }
            set { SetValue(IsRequiredProperty, value); }
        }
        public string Units
        {
            get { return (string)GetValue(UnitsProperty); }
            set { SetValue(UnitsProperty, value); }
        }
        public Brush RequiredFill
        {
            get { return (Brush)GetValue(RequiredFillProperty); }
            set { SetValue(RequiredFillProperty, value); }
        }

        public double UnitsFontSize
        {
            get { return (double)GetValue(UnitsFontSizeProperty); }
            set { SetValue(UnitsFontSizeProperty, value); }
        }

    }
}