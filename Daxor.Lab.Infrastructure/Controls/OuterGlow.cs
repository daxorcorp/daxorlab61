using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Daxor.Lab.Infrastructure.Controls
{
	/// <summary>
	///  OuterGlow draws consecutive rectangles each with a 
	///  lower alpha opacity than the previous to achive a smooth 
	///  fade from the edges on out.
	/// </summary>
	public class OuterGlow : Decorator
	{
		#region DEPENDENCY PROPERTIES
		public static readonly DependencyProperty GlowCornerRadiusXProperty =
			DependencyProperty.Register("GlowCornerRadiusX", typeof(double), typeof(OuterGlow), new PropertyMetadata(0.0, OnPropertiesChanged));

		public static readonly DependencyProperty GlowCornerRadiusYProperty =
			DependencyProperty.Register("GlowCornerRadiusY", typeof(double), typeof(OuterGlow), new PropertyMetadata(0.0, OnPropertiesChanged));

		public static readonly DependencyProperty GlowColorProperty =
			DependencyProperty.Register("GlowColor", typeof(Color), typeof(OuterGlow), new PropertyMetadata(Color.FromArgb(50, 0, 0, 0), OnPropertiesChanged));

		public static readonly DependencyProperty GlowSoftnessProperty =
			DependencyProperty.Register("GlowSoftness", typeof(int), typeof(OuterGlow), new PropertyMetadata(12, OnPropertiesChanged));

		public static readonly DependencyProperty GlowGrowProperty =
			DependencyProperty.Register("GlowGrow", typeof(int), typeof(OuterGlow), new PropertyMetadata(0, OnPropertiesChanged));
		#endregion DEPENDENCY PROPERTIES

		#region CLASS VARIABLES
		private readonly double[] _lookupTable = new double[181];
		private List<SolidColorBrush> _brushes = new List<SolidColorBrush>();

		private Rect _rec;
		private double _cornerX;
		private double _cornerY;
		private double _inc;
		private int _numRects;
		#endregion

		/// <summary>
		/// Initializes a new instance of the <strong><see cref="OuterGlow"/></strong> class.
		/// </summary>
		public OuterGlow()
		{
			for (var j = 0; j < 180; j++)
			{
				var x = DegToRads(j);
				_lookupTable[j] = (Math.Cos(x) + 1) * .5;
			}

			Loaded += OuterGlow_Loaded;
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public double GlowCornerRadiusX
		{
			get { return (double)GetValue(GlowCornerRadiusXProperty); }
			set { SetValue(GlowCornerRadiusXProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public double GlowCornerRadiusY
		{
			get { return (double)GetValue(GlowCornerRadiusYProperty); }
			set { SetValue(GlowCornerRadiusYProperty, value); }
		}
	
		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public Color GlowColor
		{
			get { return (Color)GetValue(GlowColorProperty); }
			set { SetValue(GlowColorProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public int GlowSoftness
		{
			get { return (int)GetValue(GlowSoftnessProperty); }
			set { SetValue(GlowSoftnessProperty, value); }
		}

		[Category("Common Properties")]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public int GlowGrow
		{
			get { return (int)GetValue(GlowGrowProperty); }
			set { SetValue(GlowGrowProperty, value); }
		}
		
		#region Rendering and Positioning
		/// <summary>
		/// Render callback.  
		/// </summary>
		protected override void OnRender(DrawingContext drawingContext)
		{
			var rec = new Rect
			{
				X = _rec.X,
				Y = _rec.Y,
				Width = Pos(ActualWidth) + _rec.Width,
				Height = Pos(ActualHeight) + _rec.Height
			};

			double cornerX = _cornerX;
			double cornerY = _cornerY;

			for (int i = 0; i < _numRects; i++)
			{
				drawingContext.DrawRoundedRectangle(_brushes[i], null, rec, cornerX, cornerY);

				rec.X--;
				rec.Y--;
				rec.Width += 2;
				rec.Height += 2;                
				cornerX++;
				cornerY++;
			}
		}       
		#endregion
	   
		private static void OnPropertiesChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var outerGlow = (OuterGlow)d;

			outerGlow.SetProperties();
		}

		private void OuterGlow_Loaded(object sender, RoutedEventArgs e)
		{
			SetProperties();
		}

		#region PROPERTIES CHANGED CALLBACK
		private void SetProperties()
		{
			_numRects = GlowSoftness;

			_cornerX = GlowCornerRadiusX;
			_cornerY = GlowCornerRadiusY;

			_rec.X = GlowGrow;
			_rec.Y = GlowGrow;

			// ReSharper disable once PossibleLossOfFraction
			_inc = 180 / _numRects;

			BuildBrushes();
			InvalidateVisual();
		}

		private void BuildBrushes()
		{
			_brushes = new List<SolidColorBrush>();

			for (var i = 0; i < _numRects; i++)
			{
				// angle
				var x = (int)(i * _inc);

				// Get the new Alpha Value, we are using a cosine equation to get both a smooth ease out and ease in.
				// We have previously stored these values in a lookup table to speed up this loop.
				var alpha = _lookupTable[x] * GlowColor.A;

				_brushes.Add(new SolidColorBrush(Color.FromArgb((byte)alpha, GlowColor.R, GlowColor.G, GlowColor.B)));
			}
		}

		private static double Pos(double p)
		{
			return (p < 0) ? 0 : p;
		}

		private static double DegToRads(double p)
		{
			return p * Math.PI / 180;
		}

		#endregion
	}
}