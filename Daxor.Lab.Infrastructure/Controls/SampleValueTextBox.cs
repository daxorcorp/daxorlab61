using System.Windows;
using System.Windows.Controls;

namespace Daxor.Lab.Infrastructure.Controls
{
    public class SampleValueTextBox : TextBox
    {
        public static readonly DependencyProperty IsExcludedProperty =
           DependencyProperty.Register("IsExcluded", typeof(bool), typeof(SampleValueTextBox), new FrameworkPropertyMetadata(false));

        public static readonly DependencyProperty IsUpdatingProperty =
            DependencyProperty.Register("IsUpdating", typeof(bool), typeof(SampleValueTextBox), new UIPropertyMetadata(false));

        public static readonly DependencyProperty IsRequiredProperty =
            DependencyProperty.Register("IsRequired", typeof(bool), typeof(SampleValueTextBox), new UIPropertyMetadata(false));

        static SampleValueTextBox()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(SampleValueTextBox), new FrameworkPropertyMetadata(typeof(SampleValueTextBox)));
        }


        #region IsUpdating
        public bool IsUpdating
        {
            get { return (bool)GetValue(IsUpdatingProperty); }
            set { SetValue(IsUpdatingProperty, value); }
        }
        #endregion

        #region IsExcluded (Dependency Property)
        public bool IsExcluded
        {
            get { return (bool)GetValue(IsExcludedProperty); }
            set { SetValue(IsExcludedProperty, value); }
        }
        #endregion    
   
        #region IsRequired (Dependency Property)
        public bool IsRequired
        {
            get { return (bool)GetValue(IsRequiredProperty); }
            set { SetValue(IsRequiredProperty, value); }
        }
        #endregion
    }
}