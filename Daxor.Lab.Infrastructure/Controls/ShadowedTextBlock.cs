﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Daxor.Lab.Infrastructure.Controls
{
    public class ShadowedTextBlock : Control
    {
         public static readonly DependencyProperty ShadowYOffsetProperty =
             DependencyProperty.Register("ShadowYOffset", typeof(double), typeof(ShadowedTextBlock), new PropertyMetadata(2.0, OnContentChanged));

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(ShadowedTextBlock), new PropertyMetadata("Shadowed Text", OnContentChanged));

        public static readonly DependencyProperty ShadowProperty =
            DependencyProperty.Register("Shadow", typeof(SolidColorBrush), typeof(ShadowedTextBlock), new PropertyMetadata(new SolidColorBrush(Color.FromArgb(175, 0, 0, 0)), OnContentChanged));

        public static readonly DependencyProperty ShadowXOffsetProperty =
            DependencyProperty.Register("ShadowXOffset", typeof(double), typeof(ShadowedTextBlock), new PropertyMetadata(2.0, OnContentChanged));

        static ShadowedTextBlock()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ShadowedTextBlock), new FrameworkPropertyMetadata((object)null));
        }

        #region DEPENDENCY PROPERTIES
        // Text Property
        [Category("Common Properties")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("The Text that you want displayed.")]
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Shadow Property
        public SolidColorBrush Shadow
        {
            get { return (SolidColorBrush)GetValue(ShadowProperty); }
            set { SetValue(ShadowProperty, value); }
        }

        // ShadowXOffset Property
        [Category("Common Properties")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("The horizontal offset of the shadow")]
        public double ShadowXOffset
        {
            get { return (double)GetValue(ShadowXOffsetProperty); }
            set { SetValue(ShadowXOffsetProperty, value); }
        }

        // ShadowYOffset Property
        [Category("Common Properties")]
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Description("The vertical offset of the shadow.")]
        public double ShadowYOffset
        {
            get { return (double)GetValue(ShadowYOffsetProperty); }
            set { SetValue(ShadowYOffsetProperty, value); }
        }
        #endregion DEPENDENCY PROPERTIES
        
        protected override void OnRender(DrawingContext dc)
        {
            if (Text == null)
            {
                return;
            }

            var typeface = new Typeface(FontFamily, FontStyle, FontWeight, FontStretch);

            var textMain = new FormattedText(Text, new System.Globalization.CultureInfo(string.Empty), FlowDirection.LeftToRight, typeface, FontSize, Foreground);
            var textShadow = new FormattedText(Text, new System.Globalization.CultureInfo(string.Empty), FlowDirection.LeftToRight, typeface, FontSize, Shadow);

            dc.DrawText(textShadow, new Point(ShadowXOffset, ShadowYOffset));
            dc.DrawText(textMain, new Point(0, 0));

            Width = textMain.Width;
            Height = textMain.Height;
        }

        private static void OnContentChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((ShadowedTextBlock)d).InvalidateVisual();
        }
    }
}