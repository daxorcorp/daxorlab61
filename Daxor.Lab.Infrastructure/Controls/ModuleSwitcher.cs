﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Daxor.Lab.Infrastructure.Utilities;

namespace Daxor.Lab.Infrastructure.Controls
{
	public class ModuleSwitcher : ItemsControl
	{
		//Temp solution; testing only
		readonly Dictionary<RoutedEvent, Storyboard> _storyboards = new Dictionary<RoutedEvent, Storyboard>();

		#region ToSmallEvent (Attached Event)

		public static readonly RoutedEvent ToSmallEvent =
			  EventManager.RegisterRoutedEvent("ToSmall", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ModuleSwitcher));

		#endregion

		#region ToCurrentEvent (Attached Event)

		public static readonly RoutedEvent ToCurrentEvent =
			  EventManager.RegisterRoutedEvent("ToCurrent", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ModuleSwitcher));

		#endregion

		#region ToLargeEvent (Attached Event)

		public static readonly RoutedEvent ToLargeEvent =
			  EventManager.RegisterRoutedEvent("ToLarge", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ModuleSwitcher));

		#endregion

		#region CollapseEvent (Attached Event)

		public static readonly RoutedEvent ToCollapseEvent =
			  EventManager.RegisterRoutedEvent("ToCollapse", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ModuleSwitcher));

		#endregion

		#region ToVisible (Attached Event)

		public static readonly RoutedEvent ToVisibleEvent =
			EventManager.RegisterRoutedEvent("ToVisible", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ModuleSwitcher));

		#endregion

		#region CurrentModuleChangedEvent Bubble Event

		public event RoutedEventHandler CurrentModuleChanged
		{
			add { AddHandler(CurrentModuleChangedEvent, value); }
			remove { RemoveHandler(CurrentModuleChangedEvent, value); }
		}

		public static readonly RoutedEvent CurrentModuleChangedEvent =
		 EventManager.RegisterRoutedEvent("CurrentModuleChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ModuleSwitcher));

		#endregion

		#region CTOR
		static ModuleSwitcher()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(ModuleSwitcher), new FrameworkPropertyMetadata(typeof(ModuleSwitcher)));
		}
		#endregion
	   
		#region FIELDS

		public FrameworkElement CurrentActiveElement
		{
			get;
			private set;
		}
		private FrameworkElement InitialActiveElement
		{
			get;
			set;
		}

		#endregion

		#region M
		public void Navigate(FrameworkElement element)
		{
			if (Items.Contains(element))
			{
				ChangeScreens(CurrentActiveElement, element);
			}
		}
		#endregion

		#region OVERRIDES

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			ItemContainerGenerator.StatusChanged += ItemContainerGenerator_StatusChanged;
		}
		void ItemContainerGenerator_StatusChanged(object sender, EventArgs e)
		{
			var g = sender as ItemContainerGenerator;
			// ReSharper disable once PossibleNullReferenceException
			if (g.Status != System.Windows.Controls.Primitives.GeneratorStatus.ContainersGenerated) return;
			if(Items.Count > 0)
				ItemContainerGenerator.StatusChanged -= ItemContainerGenerator_StatusChanged;

			for (var i = 0; i < Items.Count; i++)
			{
				var item = g.ContainerFromIndex(i) as ContentControl;
				if (item == null) continue;
				_storyboards[ToCurrentEvent]  = (Storyboard)item.Template.Resources["ToCurrentStoryboard"];
				_storyboards[ToLargeEvent]    = (Storyboard)item.Template.Resources["ToLargeStoryboard"];
				_storyboards[ToSmallEvent]    = (Storyboard)item.Template.Resources["ToSmallStoryboard"];
				_storyboards[ToCollapseEvent] = (Storyboard)item.Template.Resources["ToCollapseStoryboard"];
			}
		}

		protected override bool IsItemItsOwnContainerOverride(object item)
		{
			return item is ContentControl && !(item is UserControl);
		}
		protected override DependencyObject GetContainerForItemOverride()
		{
			return new ContentControl(); 
		}

		
		protected override void OnItemsChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
		{
			base.OnItemsChanged(e);
			if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
			{
				foreach (FrameworkElement element in e.NewItems)
				{
					element.Loaded += (o, arg) =>
					{
						// ReSharper disable once AccessToForEachVariableInClosure
						if (Equals(element, InitialActiveElement)) return;
						var ccTemp = (ContentControl)ItemContainerGenerator.ContainerFromItem(o);
						var str = _storyboards[ToCollapseEvent].Clone();
						str.Completed += new StoryboardEventHandlerWrapper<FrameworkElement>(null, str, AnimationFinishedCallback).CustomHandler;
						str.Begin(ccTemp, ccTemp.Template);
					};
				}
			}
		}
		#endregion

		#region HELPERS

		private void ChangeScreens(UIElement oldItem, FrameworkElement newItem)
		{
			if (Items.Count == 0)
				return;

			EnableBitmapCacheComposition(oldItem);
			EnableBitmapCacheComposition(newItem);

			// Cache first activatedElement
			if (InitialActiveElement == null && oldItem == null && newItem != null)
			{
				InitialActiveElement = newItem;
				var cc = (ContentControl)ItemContainerGenerator.ContainerFromItem(newItem);

				if (cc.IsLoaded)
				{
					var ccTemp = (ContentControl)ItemContainerGenerator.ContainerFromItem(InitialActiveElement);
					Storyboard str = _storyboards[ToCurrentEvent].Clone();
					str.Completed += new StoryboardEventHandlerWrapper<FrameworkElement>(InitialActiveElement, str, AnimationFinishedCallback).CustomHandler;
					str.Begin(ccTemp, ccTemp.Template);
					
				}

				else
					cc.Loaded += (s, e) =>
					{
						var ccTemp = s as ContentControl;
						var str = _storyboards[ToCurrentEvent].Clone();
						str.Completed += new StoryboardEventHandlerWrapper<FrameworkElement>(InitialActiveElement, str, AnimationFinishedCallback).CustomHandler;
						// ReSharper disable once AssignNullToNotNullAttribute
						// ReSharper disable once PossibleNullReferenceException
						str.Begin(ccTemp, ccTemp.Template);
					};
			}
			else
			{
				if (newItem != null)
				{
					var ccTemp = (ContentControl)ItemContainerGenerator.ContainerFromItem(newItem);
					Storyboard str = _storyboards[ToCurrentEvent].Clone();
					str.Completed += new StoryboardEventHandlerWrapper<FrameworkElement>(newItem, str, AnimationFinishedCallback).CustomHandler;
					str.Begin(ccTemp, ccTemp.Template);
				}
				   
			}

			// An item that was originally activated first
			if (Equals(InitialActiveElement, newItem))
			{
				foreach (var element in Items.Cast<FrameworkElement>().Where(element => !Equals(element, InitialActiveElement)))
				{
					EnableBitmapCacheComposition(element);
					
					var ccTemp = (ContentControl)ItemContainerGenerator.ContainerFromItem(element);
					if (!ccTemp.IsLoaded) continue;
					var str = _storyboards[ToSmallEvent].Clone();
					str.Completed += new StoryboardEventHandlerWrapper<FrameworkElement>(null, str, AnimationFinishedCallback).CustomHandler;
					str.Begin(ccTemp, ccTemp.Template);
				}
			}
			else
			{
				if (oldItem != null)
				{
					var ccTemp = (ContentControl)ItemContainerGenerator.ContainerFromItem(oldItem);
					var str = _storyboards[ToLargeEvent].Clone();
					str.Completed += new StoryboardEventHandlerWrapper<FrameworkElement>(null, str, AnimationFinishedCallback).CustomHandler;
					str.Begin(ccTemp, ccTemp.Template);
				}
			}

			// Updates currently active Element
			CurrentActiveElement = newItem;
			DisableBitmapCacheComposition(newItem);
		}

		
		#endregion

		private static void EnableBitmapCacheComposition(UIElement element)
		{
			if (element == null) return;
			if (element.CacheMode != null) return;

			var cache = new BitmapCache
			{
				EnableClearType = false, 
				RenderAtScale = 1, 
				SnapsToDevicePixels = false
			};
			element.CacheMode = cache;
		}
		private static void DisableBitmapCacheComposition(UIElement element)
		{
		    element.CacheMode = null;
		}

		private void AnimationFinishedCallback(FrameworkElement navigatedView)
		{
			if (!Equals(CurrentActiveElement, navigatedView) || navigatedView == null)
				return;

			var args = new RoutedEventArgs
			{
				RoutedEvent = CurrentModuleChangedEvent
			};
			navigatedView.RaiseEvent(args);
		}
	}
}
