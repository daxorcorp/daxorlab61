﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Daxor.Lab.Infrastructure.Controls
{
	public class SmoothScrollPanel : Panel, IScrollInfo
	{
		#region DPs
		public static readonly DependencyProperty IsAnimatedProperty =
			DependencyProperty.Register("IsAnimated", typeof(bool), typeof(SmoothScrollPanel), new FrameworkPropertyMetadata(true));

		public static readonly DependencyProperty OrientationProperty =
			DependencyProperty.Register("Orientation", typeof(Orientation), typeof(SmoothScrollPanel), new PropertyMetadata(Orientation.Vertical));

		// When CurrentIndex changes the child at that index scrolls into view
		public static readonly DependencyProperty CurrentIndexProperty =
			DependencyProperty.Register("CurrentIndex", typeof(int), typeof(SmoothScrollPanel), new FrameworkPropertyMetadata(0, OnCurrentIndexChanged));
		
		public static readonly DependencyProperty StartingItemProperty =
			DependencyProperty.Register("StartingItem", typeof(int), typeof(SmoothScrollPanel));

		public static readonly DependencyProperty EndingItemProperty =
			DependencyProperty.Register("EndingItem", typeof(int), typeof(SmoothScrollPanel));

		public static readonly DependencyProperty TotalItemsProperty =
			DependencyProperty.Register("TotalItems", typeof(int), typeof(SmoothScrollPanel), new FrameworkPropertyMetadata(OnTotalItemsChanged));

			  
		#endregion

		#region Fields

		private ScrollViewer _owner;
		private Size _extent = new Size(0, 0);
		private Size _viewport = new Size(0, 0);
		private Point _offset;
		private readonly TranslateTransform _trans = new TranslateTransform();
		private enum Direction
		{
			Left, Right, Up, Down
		}
		#endregion

		public SmoothScrollPanel()
		{
			RenderTransform = _trans;
		}

		#region CLR Properties

		// Orientation Property
		[Category("Common Properties")]
		public Orientation Orientation
		{
			get { return (Orientation)GetValue(OrientationProperty); }
			set { SetValue(OrientationProperty, value); }
		}

		public int CurrentIndex
		{
			get { return (int)GetValue(CurrentIndexProperty); }
			set { SetValue(CurrentIndexProperty, value); }
		}
		public bool IsAnimated
		{
			get { return (bool)GetValue(IsAnimatedProperty); }
			set { SetValue(IsAnimatedProperty, value); }
		}

		public int StartingItem
		{
			get { return (int)GetValue(StartingItemProperty); }
			set { SetValue(StartingItemProperty, value); }
		}
		public int EndingItem
		{
			get { return (int)GetValue(EndingItemProperty); }
			set { SetValue(EndingItemProperty, value); }
		}
		public int TotalItems
		{
			get { return (int)GetValue(TotalItemsProperty); }
			set { SetValue(TotalItemsProperty, value); }
		}

		public ScrollViewer ScrollOwner
		{
			get { return _owner; }
			set { _owner = value; }
		}

		public bool CanHorizontallyScroll { get; set; }

		public bool CanVerticallyScroll { get; set; }

		public double HorizontalOffset
		{
			get { return _offset.X; }
		}
		public double VerticalOffset
		{
			get { return _offset.Y; }
		}

		public double ExtentHeight
		{
			get { return _extent.Height; }
		}
		public double ExtentWidth
		{
			get { return _extent.Width; }
		}

		public double ViewportHeight
		{
			get { return _viewport.Height; }
		}
		public double ViewportWidth
		{
			get { return _viewport.Width; }
		}

		#endregion

		public void SetHorizontalOffset(double offset)
		{
			if (offset < 0 || _viewport.Width >= _extent.Width)
			{
				offset = 0;
			}
			else
			{
				if (offset + _viewport.Width >= _extent.Width)
				{
					offset = _extent.Width - _viewport.Width;
				}
			}

			_offset.X = offset;

			if (_owner != null)
			{
				_owner.InvalidateScrollInfo();
			}

			_trans.X = -offset;
		}
		public void SetVerticalOffset(double offset)
		{
			RenderTransform = _trans;

			if (offset < 0 || _viewport.Height >= _extent.Height)
			{
				offset = 0;
			}
			else
			{
				if (offset + _viewport.Height >= _extent.Height)
				{
					offset = _extent.Height - _viewport.Height;
				}
			}

			_offset.Y = offset;

			if (_owner != null)
			{
				_owner.InvalidateScrollInfo();
			}

			_trans.Y = -offset;
			CalculatePageInfo();
		}
  
		public Rect MakeVisible(Visual visual, Rect rectangle)
		{
			// HORIZONTAL //
			if (Orientation == Orientation.Horizontal)
			{
				var runningChildWidth = 0.0;
				var prevRunningWidth = 0.0;
				var rightExtent = Math.Abs(HorizontalOffset) + _viewport.Width;
				var leftExtent = Math.Abs(HorizontalOffset);

				for (var i = 0; i < InternalChildren.Count; i++)
				{
					runningChildWidth += ((FrameworkElement)InternalChildren[i]).ActualWidth;

					if (Equals(InternalChildren[i], visual))
					{
						double newOffset;

						if (prevRunningWidth < rightExtent && runningChildWidth > rightExtent)
						{
							newOffset = runningChildWidth - _viewport.Width;
							SetHorizontalOffsetAnimated(newOffset);
							break;
						}

						if (runningChildWidth > leftExtent && prevRunningWidth < leftExtent)
						{
							newOffset = prevRunningWidth;
							SetHorizontalOffsetAnimated(newOffset);
							break;
						}
					}

					prevRunningWidth = runningChildWidth;
				}
			}

			// VERTICAL //
			if (Orientation == Orientation.Vertical)
			{
				double runningChildHeight = 0.0;
				double prevRunningHeight = 0.0;
				double bottomExtent = Math.Abs(VerticalOffset) + _viewport.Height;
				double topExtent = Math.Abs(VerticalOffset);

				for (int i = 0; i < InternalChildren.Count; i++)
				{
					runningChildHeight += ((FrameworkElement)InternalChildren[i]).ActualHeight;

					if (Equals(InternalChildren[i], visual))
					{
						double newOffset;

						if (prevRunningHeight < bottomExtent && runningChildHeight > bottomExtent)
						{
							newOffset = runningChildHeight - _viewport.Height;
							SetVerticalOffsetAnimated(newOffset);
							break;
						}

						if (runningChildHeight > topExtent && prevRunningHeight < topExtent)
						{
							newOffset = prevRunningHeight;
							SetVerticalOffsetAnimated(newOffset);
							break;
						}
					}

					prevRunningHeight = runningChildHeight;
				}
			}

			return rectangle;
		}

		public void MouseWheelUp()
		{
			SetVerticalOffsetAnimated(VerticalOffset - 43);
		}
		public void MouseWheelDown()
		{
			SetVerticalOffsetAnimated(VerticalOffset + 43);
		}
		public void MouseWheelLeft()
		{
			throw new Exception("The method or operation is not implemented.");
		}
		public void MouseWheelRight()
		{
			throw new Exception("The method or operation is not implemented.");
		}

		public void PageUp()
		{
			double newOffset = VerticalOffset - _viewport.Height;

			SetVerticalOffsetAnimated(newOffset);
		}
		public void PageDown()
		{
			// For the TestListGridView, we don't have access to individual items.  
			// So we will page down based on the ViewPort Height
			double newOffset = VerticalOffset + _viewport.Height;

			SetVerticalOffsetAnimated(newOffset);
		}

		public void ToTop()
		{
			SetVerticalOffsetAnimated(0);
		}
		public void ToBottom()
		{
			SetVerticalOffsetAnimated(_extent.Height);
		}
		public void ToLeftEnd()
		{
			SetHorizontalOffsetAnimated(0);
		}
		public void ToRightEnd()
		{
			SetHorizontalOffsetAnimated(_extent.Width);
		}
		public void PageLeft()
		{
			throw new Exception("The method or operation is not implemented.");
		}
		public void PageRight()
		{
			throw new Exception("The method or operation is not implemented.");
		}
		public void LineUp()
		{
			var newOffset = GetNewChildOffset(Direction.Up);
			SetVerticalOffsetAnimated(newOffset);
		}
		public void LineDown()
		{
			var newOffset = GetNewChildOffset(Direction.Down);
			SetVerticalOffsetAnimated(newOffset);
		}
		public void LineLeft()
		{
			var newOffset = GetNewChildOffset(Direction.Left);
			SetHorizontalOffsetAnimated(newOffset);
		}
		public void LineRight()
		{
			var newOffset = GetNewChildOffset(Direction.Right);
			SetHorizontalOffsetAnimated(newOffset);
		}

		 // Dependency Property Callback
		protected static void OnCurrentIndexChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var smoothScrollPanel = (SmoothScrollPanel)d;
			smoothScrollPanel.ScrollToItem(smoothScrollPanel.CurrentIndex);
		}
		private static void OnTotalItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
		  var smoothScrollPanel = (SmoothScrollPanel)d;
			smoothScrollPanel.CalculatePageInfo();
		}

		#region Panel Overrides
		
		protected override Size MeasureOverride(Size availableSize)
		{
			var infiniteSize = new Size(double.PositiveInfinity, double.PositiveInfinity);

			double curX = 0, curY = 0;

			// Measure differently depending on Orientation
			if (Orientation == Orientation.Horizontal)
			{
				foreach (UIElement child in Children)
				{
					 child.Measure(infiniteSize);

					curX += child.DesiredSize.Width;
					curY = Math.Max(curY, child.DesiredSize.Height);
				}
			}
			else
			{
				foreach (UIElement child in Children)
				{
					child.Measure(infiniteSize);

					curX = Math.Max(curX, child.DesiredSize.Width);



					curY += child.DesiredSize.Height;
				}
			}

			// Get the old ScrollPercentage
			var scrollPercentage = new Point
			{
				X = _offset.X/(_extent.Width - _viewport.Width),
				Y = _offset.Y/(_extent.Height - _viewport.Height)
			};

			// Update Extent and ViewPort
			var extent = new Size(curX, curY);

			if (extent != _extent)
			{
				_extent = extent;
				if (_owner != null)
				{
					_owner.InvalidateScrollInfo();
				}
			}

			if (availableSize != _viewport)
			{
				_viewport = availableSize;
				if (_owner != null)
				{
					_owner.InvalidateScrollInfo();
				}
			}

			// Set the new Scroll Offset based on ScrollPercentage and new Geometry.
			var newOffset = new Point
			{
				X = scrollPercentage.X*(_extent.Width - _viewport.Width),
				Y = scrollPercentage.Y*(_extent.Height - _viewport.Height)
			};

			if (Double.IsNaN(newOffset.X))
			{
				newOffset.X = 0;
			}

			if (Double.IsNaN(newOffset.Y))
			{
				newOffset.Y = 0;
			}

			SetHorizontalOffset(newOffset.X);
			SetVerticalOffset(newOffset.Y);

			// Return the appropriate size for the panel
			var resultSize = new Size(availableSize.Width, availableSize.Height);

			// Size to Content if content size is less than available size. 
			if (curX < availableSize.Width || Double.IsInfinity(availableSize.Width))
			{
				resultSize.Width = curX;
			}

			if (curY < availableSize.Height || Double.IsInfinity(availableSize.Width))
			{
				resultSize.Height = curY;
			}

			if (Double.IsInfinity(availableSize.Width))
			{
				resultSize.Width = 500;
			}

			if (Double.IsInfinity(availableSize.Height))
			{
				resultSize.Height = 500;
			}

			return resultSize;
		}
		protected override Size ArrangeOverride(Size finalSize)
		{
			if (Children == null || Children.Count == 0)
			{
				return finalSize;
			}

			double curX = 0, curY = 0;

			// Arrange differently depending on Orientation
			if (Orientation == Orientation.Horizontal)
			{
				foreach (UIElement child in Children)
				{
					child.Arrange(new Rect(curX, 0, child.DesiredSize.Width, finalSize.Height));

					curX += child.DesiredSize.Width;
					curY = Math.Max(curY, child.DesiredSize.Height);
				}
			}
			else
			{
				foreach (UIElement child in Children)
				{
					child.Arrange(new Rect(0, curY, finalSize.Width, child.DesiredSize.Height));

					curX = Math.Max(curX, child.DesiredSize.Width);
					curY += child.DesiredSize.Height;
				}
			}

			var extent = new Size(curX, curY);

			if (extent != _extent)
			{
				_extent = extent;
				if (_owner != null)
				{
					_owner.InvalidateScrollInfo();
				}
			}

			if (finalSize != _viewport)
			{
				_viewport = finalSize;
				if (_owner != null)
				{
					_owner.InvalidateScrollInfo();
				}
			}

			return finalSize;
		}
		
		#endregion

		#region Helpers

		private void ScrollToItem(int index)
		{
			// HORIZONTAL //
			if (Orientation == Orientation.Horizontal)
			{
				var runningChildWidth = 0.0;

				for (var i = 0; i < InternalChildren.Count; i++)
				{
					runningChildWidth += ((FrameworkElement)InternalChildren[i]).ActualWidth;

					if (i != index) continue;

					var newOffset = runningChildWidth - _viewport.Width;

					if (IsAnimated)
					{
						SetHorizontalOffsetAnimated(newOffset);
					}
					else
					{
						SetHorizontalOffset(newOffset);
					}

					break;
				}
			}

			// VERTICAL //
			else if (Orientation == Orientation.Vertical)
			{
				var runningChildHeight = 0.0;

				for (var i = 0; i < InternalChildren.Count; i++)
				{
					runningChildHeight += ((FrameworkElement)InternalChildren[i]).ActualHeight;

					if (i != index) continue;

					var newOffset = runningChildHeight - _viewport.Height;

					if (IsAnimated)
					{
						SetVerticalOffsetAnimated(newOffset);
					}
					else
					{
						SetVerticalOffset(newOffset);
					}

					break;
				}
			}
		}
		private double GetNewChildOffset(Direction direction)
		{
			if (direction == Direction.Right)
			{
				var rightExtent = Math.Abs(HorizontalOffset) + _viewport.Width;
				var runningChildWidth = 0.0;

				for (var i = 0; i < InternalChildren.Count; i++)
				{
					runningChildWidth += ((FrameworkElement)InternalChildren[i]).ActualWidth;

					if (runningChildWidth > rightExtent + 20)
					{
						break;
					}
				}

				return runningChildWidth - _viewport.Width;
			}

			if (direction == Direction.Left)
			{
				var leftExtent = Math.Abs(HorizontalOffset);
				var runningChildWidth = 0.0;

				for (var i = 0; i < InternalChildren.Count; i++)
				{
					runningChildWidth += ((FrameworkElement)InternalChildren[i]).ActualWidth;

					if (!(runningChildWidth > leftExtent - 20)) continue;
					runningChildWidth -= ((FrameworkElement)InternalChildren[i]).ActualWidth;

					// ReSharper disable once CompareOfFloatsByEqualityOperator
					if (i - 1 >= 0 && runningChildWidth == leftExtent)
					{
						runningChildWidth -= ((FrameworkElement)InternalChildren[i - 1]).ActualWidth;
					}

					break;
				}

				return runningChildWidth;
			}

			var runningChildHeight = 0.0;

			if (direction == Direction.Down)
			{
				double bottomExtent = Math.Abs(VerticalOffset) + _viewport.Height;

				for (int i = 0; i < InternalChildren.Count; i++)
				{
					runningChildHeight += ((FrameworkElement)InternalChildren[i]).ActualHeight;

					if (runningChildHeight > bottomExtent + 20)
					{
						break;
					}
				}

				return runningChildHeight - _viewport.Height;
			}

			if (direction != Direction.Up) return 0.0;
			var topExtent = Math.Abs(VerticalOffset);

			for (var i = 0; i < InternalChildren.Count; i++)
			{
				runningChildHeight += ((FrameworkElement)InternalChildren[i]).ActualHeight;

				if (!(runningChildHeight > topExtent - 20)) continue;
				runningChildHeight -= ((FrameworkElement)InternalChildren[i]).ActualHeight;

				// ReSharper disable once CompareOfFloatsByEqualityOperator
				if (i - 1 >= 0 && runningChildHeight == topExtent)
				{
					runningChildHeight -= ((FrameworkElement)InternalChildren[i - 1]).ActualHeight;
				}

				break;
			}

			return runningChildHeight;
		}

		private void SetHorizontalOffsetAnimated(double offset)
		{
			if (offset < 0 || _viewport.Width >= _extent.Width)
			{
				offset = 0;
			}
			else
			{
				if (offset + _viewport.Width >= _extent.Width)
				{
					offset = _extent.Width - _viewport.Width;
				}
			}

			_offset.X = offset;

			if (_owner != null)
			{
				_owner.InvalidateScrollInfo();
			}

			if (offset > 0) EnableBitmapCacheComposition();

			var easeCurve = new KeySpline(0.5, 0, 0, 1);
			var duration = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(800));

			var key = new SplineDoubleKeyFrame(-offset, duration, easeCurve);
			var anim = new DoubleAnimationUsingKeyFrames();

			anim.KeyFrames.Add(key);

			Storyboard.SetTarget(anim, this);
			Storyboard.SetTargetProperty(anim, new PropertyPath("(RenderTransform).(TranslateTransform.X)"));

			var opacityAnim = new DoubleAnimationUsingKeyFrames();
			var opacKey0 = new SplineDoubleKeyFrame(1, KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(000)), easeCurve);
			var opacKey1 = new SplineDoubleKeyFrame(.6, KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(300)), easeCurve);
			var opacKey2 = new SplineDoubleKeyFrame(.6, KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(400)), easeCurve);
			var opacKey3 = new SplineDoubleKeyFrame(1, KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(800)), easeCurve);

			opacityAnim.KeyFrames.Add(opacKey0);
			opacityAnim.KeyFrames.Add(opacKey1);
			opacityAnim.KeyFrames.Add(opacKey2);
			opacityAnim.KeyFrames.Add(opacKey3);

			Storyboard.SetTarget(opacityAnim, this);
			Storyboard.SetTargetProperty(opacityAnim, new PropertyPath(OpacityProperty));

			var scrollStoryboard = new Storyboard();
			Timeline.SetDesiredFrameRate(scrollStoryboard, 60);

			scrollStoryboard.Children.Add(anim);
			scrollStoryboard.Children.Add(opacityAnim);
			
			scrollStoryboard.FillBehavior = FillBehavior.Stop;
			scrollStoryboard.Completed += ScrollStoryboard_Completed;

			scrollStoryboard.Begin(this);
		  
		}
		private void SetVerticalOffsetAnimated(double offset)
		{
			if (offset < 0 || _viewport.Height >= _extent.Height)
			{
				offset = 0;
			}
			else
			{
				if (offset + _viewport.Height >= _extent.Height)
				{
					offset = _extent.Height - _viewport.Height;
				}
			}

			_offset.Y = offset;

			if (_owner != null)
			{
				_owner.InvalidateScrollInfo();
			}

			if (offset > 0) EnableBitmapCacheComposition();

			var easeCurve = new KeySpline(0.5, 0, 0, 1);
			var duration = KeyTime.FromTimeSpan(TimeSpan.FromMilliseconds(800));

			var key = new SplineDoubleKeyFrame(-offset, duration, easeCurve);
			var anim = new DoubleAnimationUsingKeyFrames
			{
				FillBehavior = FillBehavior.Stop
			};
			anim.Completed += ScrollStoryboard_Completed;

			anim.KeyFrames.Add(key);

			Timeline.SetDesiredFrameRate(anim, 60);
			_trans.BeginAnimation(TranslateTransform.YProperty, anim);
		}
		private void ScrollStoryboard_Completed(object sender, EventArgs e)
		{
			Opacity = 1;
			SetHorizontalOffset(_offset.X);
			SetVerticalOffset(_offset.Y);
			CalculatePageInfo();
			DisableBitmapCacheComposition();
		}
		private void CalculatePageInfo()
		{
			double itemHeight = _extent.Height / TotalItems;
			double pageHeight = _viewport.Height;

			StartingItem = (int)Math.Round(VerticalOffset / itemHeight) + 1;
			EndingItem = (int)Math.Round((VerticalOffset + pageHeight) / itemHeight);
		}

		private void EnableBitmapCacheComposition()
		{
			IsHitTestVisible = false;
			var cache = new BitmapCache
			{
				EnableClearType = false, 
				RenderAtScale = 1, 
				SnapsToDevicePixels = false
			};

			CacheMode = cache;
		}
		private void DisableBitmapCacheComposition()
		{
			IsHitTestVisible = true;
        	CacheMode = null;
		}
		#endregion
	}
}
