﻿using System;
using System.Windows.Controls;
using System.Windows;
using System.ComponentModel;

namespace Daxor.Lab.Infrastructure.Controls
{
	public class PickList : ListBox
	{
		private ScrollViewer _scrollViewer;

		#region Fields

		public static readonly DependencyProperty TitleProperty;
		public static readonly DependencyProperty ItemStartProperty;
		public static readonly DependencyProperty ItemEndProperty;

		#endregion

		#region Title
		[Description("The title displayed by the picklist"), Category("Common Properties")]
		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set { SetValue(TitleProperty, value); }
		}
		#endregion

		#region ItemStart
		[Description("Starting visible item of picklist"), Category("Common Properties")]
		public int ItemStart
		{
			get { return (int)GetValue(ItemStartProperty); }
			private set { SetValue(ItemStartProperty, value); }
		}
		#endregion

		#region ItemEnd
		[Description("Ending visible item of picklist"), Category("Common Properties")]
		public int ItemEnd
		{
			get { return (int)GetValue(ItemEndProperty); }
			set { SetValue(ItemEndProperty, value); }
		}
		#endregion

		/// <summary>
		/// Default constructor
		/// </summary>
		static PickList()
		{
			// Initialize as lookless control
			DefaultStyleKeyProperty.OverrideMetadata(typeof(PickList), new FrameworkPropertyMetadata(typeof(PickList)));

			//Override SelectedItem metadata; overriding SelectedItemProperty metadata extends metadata defined in superclass
			SelectedItemProperty.OverrideMetadata(typeof(PickList), new FrameworkPropertyMetadata(SelectedItemPropertyChanged));

			//Initialize dependecy property
			TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(PickList), new UIPropertyMetadata(String.Empty));
			ItemStartProperty = DependencyProperty.Register("ItemStart", typeof(int), typeof(PickList), new PropertyMetadata(0));
			ItemEndProperty = DependencyProperty.Register("ItemEnd", typeof(int), typeof(PickList), new PropertyMetadata(0));
		}

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			ItemContainerGenerator.StatusChanged += ItemContainerGenerator_StatusChanged;

			_scrollViewer = (ScrollViewer)Template.FindName("PART_SimpleScrollViewer", this);
			if (_scrollViewer == null)
				throw new NullReferenceException("PART_SimpleScrollViewer");

			_scrollViewer.ScrollChanged += ScrollViewer_ScrollChanged;
		}

		void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
		{
			var sViewer = sender as ScrollViewer;
			if (sViewer == null)
				return;
			//Snap item height, page height and ending index
			var itemHeight = sViewer.ExtentHeight / Items.Count;
			var pageHeight = sViewer.ViewportHeight;

			ItemStart = ((int)Math.Round(sViewer.VerticalOffset / itemHeight) + 1);

			if (ItemEnd > Items.Count)
				ItemEnd = Items.Count;
			else
				ItemEnd = ((int)Math.Round((sViewer.VerticalOffset + pageHeight) / itemHeight));

			if (Items.Count == 0)
			{
				ItemStart = 0;
				ItemEnd = 0;
			}
		}


		void ItemContainerGenerator_StatusChanged(object sender, EventArgs e)
		{
			var g = sender as ItemContainerGenerator;
			// ReSharper disable once PossibleNullReferenceException
			if (g.Status != System.Windows.Controls.Primitives.GeneratorStatus.ContainersGenerated) return;
			for (var i = 0; i < Items.Count; i++)
			{
				var item = g.ContainerFromIndex(i) as ListBoxItem;
				if (item != null)
					item.PreviewMouseLeftButtonDown += item_PreviewMouseLeftButtonDown;
			}
		}
		void item_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			var item = sender as ListBoxItem;
			if (item == null) return;
			item.PreviewMouseLeftButtonDown -= item_PreviewMouseLeftButtonDown;
			SelectedIndex = ItemContainerGenerator.IndexFromContainer(item);
			e.Handled = true;
		}

		static void SelectedItemPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
		{
			var list = sender as PickList;
			if (list != null && e.NewValue == null)
				list.SelectedIndex = -1;
		}
	}
}
