using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Daxor.Lab.Infrastructure.Controls
{
	/// <summary>
	/// Renders multiple RectangularShape controls lightening fast.  Add RectangularShapes to this control.
	/// </summary>
	[Description("Renders multiple RectangularShape controls lightening fast.  Add RectangularShapes to this control.")]
	public class Renderer : Panel
	{     
		public Renderer()
		{
			Focusable = false;         
		}

		#region MEASURE
		protected override Size MeasureOverride(Size availableSize)
		{
			var returnSize = new Size(0, 0);

			double maxX = 0;
			double maxY = 0;

			foreach (var child in Children)
			{
				if (child is RectangularShape) continue;
				var nonShape = (UIElement)child;
				nonShape.Measure(new Size(Double.PositiveInfinity, Double.PositiveInfinity));
				maxX = Math.Max(nonShape.DesiredSize.Width, maxX);
				maxY = Math.Max(nonShape.DesiredSize.Height, maxY);
			}
			if (HorizontalAlignment != HorizontalAlignment.Stretch)
				returnSize.Width = maxX;
			else
				returnSize.Width = Double.IsInfinity(availableSize.Width) ? maxX : availableSize.Width;

			returnSize.Height = Double.IsInfinity(availableSize.Height) ? maxY : availableSize.Height;

			return returnSize;
		}
		
		#endregion

		#region ARRANGE

		protected override Size ArrangeOverride(Size arrangeSize)
		{
			var returnSize = base.ArrangeOverride(arrangeSize);

			foreach (var shape in Children)
			{
				// ReSharper disable once CanBeReplacedWithTryCastAndCheckForNull
				if (shape is RectangularShape)
				{
					((RectangularShape)shape).AvailableSize = returnSize;
					((RectangularShape)shape).SizeHasChanged();
				}
				else
				{
					var nonShape = (UIElement)shape;
					nonShape.Arrange(new Rect(0, 0, arrangeSize.Width, arrangeSize.Height));
				}
			}         

			return returnSize;
		}

		#endregion

		protected override void OnVisualChildrenChanged(DependencyObject visualAdded, DependencyObject visualRemoved)
		{
			base.OnVisualChildrenChanged(visualAdded, visualRemoved);

			foreach (var shape in Children)
			{
				// ReSharper disable once CanBeReplacedWithTryCastAndCheckForNull
				if (!(shape is RectangularShape)) continue;
				((RectangularShape)shape).PropertiesChanged -= Shapes_Invalidated;
				((RectangularShape)shape).PropertiesChanged += Shapes_Invalidated;
			}
		}       

		#region RENDERING

		protected override void OnRender(DrawingContext drawingContext)
		{
			if ((RenderSize.Width < 4.0) || (RenderSize.Height < 4.0))
			{
				return;
			}

			foreach (var shape in Children)
			{
				// ReSharper disable once CanBeReplacedWithTryCastAndCheckForNull
				if (shape is RectangularShape)
				{
					DrawShape(drawingContext, (RectangularShape)shape);
				}
			}
		}

		private void DrawShape(DrawingContext drawingContext, RectangularShape shape)
		{
			if (shape.ShouldDisplay == false)
			{
				return;
			}

			// If the corners are uniform and all edges are stroked, then no need for a custom geometry.  
			if (shape.IsSimple)
			{                
				drawingContext.DrawRoundedRectangle(shape.FillInternal, shape.StrokePen, shape.RenderRect, shape.CornerRadius.TopLeft, shape.CornerRadius.TopLeft);               
				return;
			}

			// Need to render a custom geometry
			drawingContext.DrawGeometry(shape.FillInternal, shape.StrokePen, shape.Geometry);
		}

		#endregion

		#region CALLBACKS

		private void Shapes_Invalidated(object sender, EventArgs e)
		{
			InvalidateVisual();
			UpdateLayout();
		}
		#endregion
	}
}