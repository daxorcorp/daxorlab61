﻿using System;
using System.Windows.Data;

namespace Daxor.Lab.Infrastructure.Converters
{
    /// <summary>
    /// Note: This implementation came from StackOverflow...
    /// http://stackoverflow.com/questions/397556/how-to-bind-radiobuttons-to-an-enum
    /// 
    /// The first solution was used here (by Andrey Khomenko), and was later changed to 
    /// the more condensed version (by Geoff Mazeroff).
    /// </summary>
    public class EnumToBooleanConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converts an enumeration equality expression into a boolean result
        /// </summary>
        /// <param name="value">First enumeration value</param>
        /// <param name="targetType">(Ignored)</param>
        /// <param name="parameter">Second enumeration value</param>
        /// <param name="culture">(Ignored)</param>
        /// <returns>True if value and parameter are equal; false otherwise</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) throw new ArgumentNullException("value");
            return value.Equals(parameter);
        }
        
        /// <summary>
        /// Converts a boolean value into an enumeration value (if possible)
        /// </summary>
        /// <param name="value">Boolean value (true/false)</param>
        /// <param name="targetType">(Ignored)</param>
        /// <param name="parameter">Enumeration value to be returned if the given value parameter is true</param>
        /// <param name="culture">(Ignored)</param>
        /// <returns>parameter if value is true; Binding.DoNothing otherwise</returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) throw new ArgumentNullException("value");
            return value.Equals(true) ? parameter : Binding.DoNothing;
        }

        #endregion
    }
}
