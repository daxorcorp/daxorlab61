﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.Infrastructure.Converters
{
    public class TestStatusRunningToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var testStatus = (TestStatus)value;

            return (testStatus == TestStatus.Running) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}