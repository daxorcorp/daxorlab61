﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Data;
using Daxor.Lab.Infrastructure.Utilities;

namespace Daxor.Lab.Infrastructure.Converters
{
    public class RegExpToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var regEx = System.Convert.ToString(parameter);
            var inputString = System.Convert.ToString(value);

            if (!RegexHelper.IsValidRegex(regEx))
                return value;

            return Regex.IsMatch(inputString, regEx) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}