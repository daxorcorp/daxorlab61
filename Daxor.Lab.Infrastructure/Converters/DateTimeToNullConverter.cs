﻿using System;
using System.Windows.Data;

namespace Daxor.Lab.Infrastructure.Converters
{
    public class DateTimeToNullConverter : IValueConverter
    {
        public static readonly string EmptyDate = Constants.DomainConstants.NullDateTime;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime valueAsDateTime;
            if (value != null && DateTime.TryParse(value.ToString(), out valueAsDateTime) && !valueAsDateTime.ToShortDateString().Equals(EmptyDate))
                return valueAsDateTime;
 
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
