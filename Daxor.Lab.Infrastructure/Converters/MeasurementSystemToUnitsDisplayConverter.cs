﻿using System;
using System.Globalization;
using System.Windows.Data;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.Infrastructure.Converters
{
    public class MeasurementSystemToUnitsDisplayConverter : IValueConverter
    {
        public const string HeightParameterName = "Height";
        public const string WeightParameterName = "Weight";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            var unitsDisplayLabel = value.ToString();
            MeasurementSystem valueAsMeasurementSystem;

            if (Enum.TryParse(unitsDisplayLabel, out valueAsMeasurementSystem))
            {
                switch ((string)parameter)
                {
                    case HeightParameterName:
                        unitsDisplayLabel = valueAsMeasurementSystem == MeasurementSystem.English ? UnitOfMeasurementConstants.USUnitOfLength : UnitOfMeasurementConstants.MetricUnitOfLength;
                        break;

                    case WeightParameterName:
                        unitsDisplayLabel = valueAsMeasurementSystem == MeasurementSystem.English ? UnitOfMeasurementConstants.USUnitOfMass : UnitOfMeasurementConstants.MetricUnitOfMass;
                        break;
                }
            }

            return unitsDisplayLabel;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return MeasurementSystem.English;
            var unitsDisplayLabel = (string)value;

            if (unitsDisplayLabel != UnitOfMeasurementConstants.USUnitOfLength && 
                unitsDisplayLabel != UnitOfMeasurementConstants.USUnitOfMass &&
                unitsDisplayLabel != UnitOfMeasurementConstants.MetricUnitOfLength && 
                unitsDisplayLabel != UnitOfMeasurementConstants.MetricUnitOfMass)
                    return MeasurementSystem.English;
            
            return unitsDisplayLabel == UnitOfMeasurementConstants.USUnitOfLength || unitsDisplayLabel == UnitOfMeasurementConstants.USUnitOfMass ? 
                MeasurementSystem.English : MeasurementSystem.Metric;
        }
    }
}
