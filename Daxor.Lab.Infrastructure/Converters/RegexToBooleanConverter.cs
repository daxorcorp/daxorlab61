﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Data;
using Daxor.Lab.Infrastructure.Utilities;

namespace Daxor.Lab.Infrastructure.Converters
{
    public class RegexToBooleanConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Uses a regular expression and input to determine if a match exists
        /// </summary>
        /// <param name="value">Input that the regular expression is to be used with</param>
        /// <param name="targetType">(Ignored)</param>
        /// <param name="parameter">Regular expression</param>
        /// <param name="culture">(Ignored)</param>
        /// <returns>True if the given input matches the given regular expression; false if
        /// the regular expression is invalid or if the input isn't a match for the
        /// regular expression</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var input = value == null ? String.Empty : value.ToString();
            var regExPattern = parameter == null ? String.Empty : parameter.ToString();

            return RegexHelper.IsValidRegex(regExPattern) && Regex.IsMatch(input, regExPattern);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}