﻿using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace Daxor.Lab.Infrastructure.Converters
{
    /// <summary>
    /// Multi-value converter that returns an appropriate z-index based on where a tab is
    /// in relation to its siblings and whether or not the tab is selected/active.
    /// </summary>
    public class TabIndexToZIndexConverter : IMultiValueConverter
    {
        /// <summary>
        /// Computes a z-index based on the specified TabItem, TabControl, and number of
        /// items in the tab group. The left most tab has the highest z-index (i.e., number
        /// of tabs minus one), and the right-most tab has the lowest (i.e., zero). The selected
        /// tab has a z-index equal to the number of tabs in the group.
        /// </summary>
        /// <remarks>Source: http://stackoverflow.com/questions/4691090/how-to-change-the-overlapping-order-of-tabitems-in-wpf-tabcontrol</remarks>
        /// <param name="values">Array of values used for conversion. (1) TabItem, (2) TabControl
        /// containing the TabItem, (3) number of tabs in the group.</param>
        /// <param name="targetType">Not used</param>
        /// <param name="parameter">Not used</param>
        /// <param name="culture">Not used</param>
        /// <returns>The computed z-index of the given tab; Binding.DoNothing if the values array 
        /// contains invalid values</returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length < 3)
                return Binding.DoNothing;

            var tabItem = values[0] as TabItem;
            var tabControl = values[1] as TabControl;

            if (tabItem == null || tabControl == null || values[2] == null)
                return Binding.DoNothing;

            int tabCount;
            if (!int.TryParse(values[2].ToString(), out tabCount))
                return Binding.DoNothing;

            // If this is the selected tab, make it's z-index the highest.
            if (tabItem.IsSelected)
                return tabCount;

            // First tab has position one (not zero). This is so the ZIndex has the right-most tab
            // at Panel.ZIndex == 0.

            // When running under WPF, the dispatcher will be the synch context. When running
            // under the unit test environment, there is no synch context
            var tabIndex = System.Threading.SynchronizationContext.Current != null ?
                GetTabIndexWhenSynchronizationContextIsNotNull(tabControl, tabItem) :
                GetTabIndexWhenThereIsNoSynchronizationContext(tabControl, tabItem);

            return tabCount - (tabIndex + 1);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        private int GetTabIndexWhenSynchronizationContextIsNotNull(TabControl tabControl, TabItem tabItem)
        {
            return tabControl.ItemContainerGenerator.IndexFromContainer(tabItem);
        }

        private int GetTabIndexWhenThereIsNoSynchronizationContext(TabControl tabControl, TabItem tabItem)
        {
            return tabControl.Items.IndexOf(tabItem);
        }
    }
}
