﻿using System;
using System.Globalization;
using System.Windows.Data;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.Infrastructure.Converters
{
    /// <summary>
    /// Converts to/from a formatted time string (mmm:ss) and the corresponding number of seconds.
    /// </summary>
    public class SecondsToMinutesAndSecondsConverter : IValueConverter
    {
        #region Constants

        /// <summary>
        /// Character used to separate minutes and seconds.
        /// </summary>
        public static readonly char TimeSeparatorChar = ':';

        /// <summary>
        /// Value representing an undefined number of seconds
        /// </summary>
        public static readonly int UndefinedSeconds = -1;

        #endregion

        /// <summary>
        /// Converts a formatted time string (mmm:ss) to the corresponding number of seconds.
        /// </summary>
        /// <param name="value">Formatted time string</param>
        /// <param name="targetType">Type of the converted value (string, in this case)</param>
        /// <param name="parameter">Converter parameter (not used)</param>
        /// <param name="culture">Culture information (not used)</param>
        /// <returns>Number of seconds represented by the specified value</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return String.Empty;
            
            int seconds;
            int.TryParse(value.ToString(), out seconds);

            if (seconds == UndefinedSeconds)
                return String.Empty;

            var minutePart = seconds / (int) TimeConstants.SecondsPerMinute;
            var secondsPart = seconds % (int) TimeConstants.SecondsPerMinute;

            return String.Format("{0:0}{1}{2:00}", minutePart, TimeSeparatorChar, secondsPart);
        }

        /// <summary>
        /// Converts a number of seconds to the corresponding formatted time string (mmm:ss).
        /// </summary>
        /// <param name="value">Number of seconds</param>
        /// <param name="targetType">Type of the converted value (int, in this case)</param>
        /// <param name="parameter">Converter parameter (not used)</param>
        /// <param name="culture">Culture information (not used)</param>
        /// <returns>Formatted time string representing the specified number of seconds</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return String.Empty;

            var minutesAndSeconds = value.ToString();
            var tokens = minutesAndSeconds.Split(TimeSeparatorChar);
            if (tokens.Length != 2) return UndefinedSeconds;

            int minutesAsInt;
            int.TryParse(tokens[0], out minutesAsInt);

            int secondsAsInt;
            int.TryParse(tokens[1], out secondsAsInt);

            var totalSeconds = (minutesAsInt * (int) TimeConstants.SecondsPerMinute) + secondsAsInt;

            return totalSeconds;
        }
    }
}
