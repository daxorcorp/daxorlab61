﻿using System;
using System.Windows.Data;

namespace Daxor.Lab.Infrastructure.Converters
{
    public class ComboBoxItemValueToDefaultValueConverter : IValueConverter
    {
        public static readonly string DefaultValue = "SELECT";

        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var valueAsString = value as string;
            if (valueAsString != null && valueAsString == String.Empty)
                return DefaultValue;

            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
