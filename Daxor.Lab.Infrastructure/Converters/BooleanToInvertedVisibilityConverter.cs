﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Daxor.Lab.Infrastructure.Converters
{
	public class BooleanToInvertedVisibilityConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			// ReSharper disable RedundantComparisonWithNull
			if (value == null || !(value is bool) || ((bool) value))
			// ReSharper restore RedundantComparisonWithNull
				return Visibility.Collapsed;

			return Visibility.Visible;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotSupportedException("Not supported");
		}
	}
}