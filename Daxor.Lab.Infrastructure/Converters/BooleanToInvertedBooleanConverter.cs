﻿using System;
using System.Windows.Data;

namespace Daxor.Lab.Infrastructure.Converters
{
    public class BooleanToInvertedBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) throw new ArgumentNullException("value");
            if (!(value is bool)) throw new NotSupportedException();
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) throw new ArgumentNullException("value");
            if (!(value is bool)) throw new NotSupportedException();
            return !(bool)value;
        }
    }
}