﻿using System;
using System.Linq;
using System.Windows.Data;

namespace Daxor.Lab.Infrastructure.Converters
{
    public class UnitsToBoolConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Count() != 2)
                return false;

            var units = (string)values.ElementAt(0);
            var value = (string)values.ElementAt(1);

            if (units == null || value == null)
                return false;

            return units.Equals(value);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            //Value is either true or false
            throw new NotImplementedException();
        }

        #endregion
    }

}
