﻿using System;
using System.Windows.Data;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.Infrastructure.Converters
{
    public class GenderToBooleanConverter : IValueConverter
    {
        public static readonly string InvertGender = "Invert";

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) throw new ArgumentNullException("value");
            if (!(value is GenderType)) throw new NotSupportedException();
            var gender = (GenderType) value;

            if (parameter as string == InvertGender)
                return gender == GenderType.Female;

            return gender == GenderType.Male;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) throw new ArgumentNullException("value");
            if (!(value is bool)) throw new NotSupportedException();
            var isMale = (bool) value;

            if (parameter as string == InvertGender)
                return isMale ? GenderType.Female : GenderType.Male;

            return isMale ? GenderType.Male : GenderType.Female;
        }
    }
}
