﻿using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.Infrastructure.Interfaces;
using System.Collections.Generic;
using System.IO;

namespace Daxor.Lab.Infrastructure.Services
{
    public class DriveAccessorService : IDriveAccessorService
    {
        public List<DriveInformation> GetAvailableDrives()
        {
            var results = new List<DriveInformation>();
            foreach (var driveInfo in DriveInfo.GetDrives())
            {
                if (driveInfo.Name.Contains(@"C:\"))
                    continue;

                DriveInformation driveInformation = null;

                if (driveInfo.DriveType == DriveType.Fixed)
                    driveInformation = new DriveInformation {DriveType = DriveTypeEnum.Usb};

                if (driveInfo.IsReady && driveInfo.DriveType == DriveType.Removable)
                    driveInformation = new DriveInformation { DriveType = DriveTypeEnum.Usb };

                if (driveInfo.IsReady && driveInfo.DriveType == DriveType.Network)
                    driveInformation = new DriveInformation {DriveType = DriveTypeEnum.Network};

                if (driveInfo.DriveType == DriveType.CDRom)
                    driveInformation = new DriveInformation {DriveType = DriveTypeEnum.Optical};

                if (driveInformation == null)
                    continue;

                driveInformation.Name = driveInfo.Name;
                results.Add(driveInformation);
            }
            return results;
        }
    }
}
