using System;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Infrastructure.Services
{
    /// <summary>
    /// Abstract base logger
    /// </summary>
    public abstract class LoggerBase : ILoggerFacade
    {
        #region ILoggerFacade Members

        public void Log(string message, Category category, Priority priority)
        {
            HandleLog(message, category, priority, DateTime.Now);
        }
        #endregion

        protected virtual void HandleLog(string message, Category category, Priority priority, DateTime timeStamp)
        {
            System.Diagnostics.Debug.WriteLine("{0},{1},{2},{3}", timeStamp.ToString("MM-dd-yy - HH:mm:ss ffff"), category, priority, message);
        }
    }
}