﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Services
{
    /// <summary>
    /// Keeps track of activated/deactivated appModule.
    /// </summary>
    public class AppModuleInfoCatalog : IAppModuleInfoCatalog
    {
        private readonly ObservableCollection<IAppModuleInfo> _items;
        private readonly IEventAggregator _eventAggregator;

        public AppModuleInfoCatalog(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;

            _items = new ObservableCollection<IAppModuleInfo>();
            Items.CollectionChanged += ItemsCollectionChanged;
            SubscribeToAggregateEvents();
        }

        public ObservableCollection<IAppModuleInfo> Items
        {
            get { return _items; }
        }

        #region EventHandlers
        private void OnAppModuleActivated(string appModuleKey)
        {
            IAppModuleInfo appModule = GetModuleByKey(appModuleKey);
            if (appModule != null)
            {
                appModule.IsCurrentlyActive = true;
            }
        }

        private void OnAppModuleDeactivated(string appModuleKey)
        {
            IAppModuleInfo appModule = GetModuleByKey(appModuleKey);
            if (appModule != null)
            {
                appModule.IsCurrentlyActive = false;
            }
        }

        private void OnAppModuleDisabled(string appModuleKey)
        {
            IAppModuleInfo appModule = GetModuleByKey(appModuleKey);
            if (appModule != null)
            {
                appModule.IsEnabled = false;
            }
        }

        private void OnAppModuleEnabled(string appModuleKey)
        {
            IAppModuleInfo appModule = GetModuleByKey(appModuleKey);
            if (appModule != null)
            {
                appModule.IsEnabled = true;
            }
        }

        #endregion

        #region HELPERS

        private IAppModuleInfo GetModuleByKey(string appModuleKey)
        {
            return (from app in Items
                    where app.AppModuleKey == appModuleKey
                    select app).FirstOrDefault();
        }

        private void ItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                var groups = from m in Items
                             group m by m.DisplayOrder into groupped
                             select new
                             {
                                 groupId = groupped.Key
                             };

                if (groups.Count() < Items.Count)
                {
                    throw new NotSupportedException("AppModuleInfo: DisplayOrder property has to be unique.");
                }
            }
        }

        private void SubscribeToAggregateEvents()
        {
            _eventAggregator.GetEvent<AppModuleActivated>().Subscribe(OnAppModuleActivated, ThreadOption.UIThread, false);
            _eventAggregator.GetEvent<AppModuleDeactivated>().Subscribe(OnAppModuleDeactivated, ThreadOption.UIThread, false);
            _eventAggregator.GetEvent<AppModuleDisabled>().Subscribe(OnAppModuleDisabled, ThreadOption.UIThread, false);
            _eventAggregator.GetEvent<AppModuleEnabled>().Subscribe(OnAppModuleEnabled, ThreadOption.UIThread, false);
        }
        #endregion
    }
}
