﻿using System;
using System.IO;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Infrastructure.Services
{
    /// <summary>
	/// Text file logger
	/// </summary>
	public class CustomFileLogger : LoggerBase
	{
		private static readonly object LogLock = new object();
		private readonly string   _filePath;
		protected FileInfo File;

		public CustomFileLogger(string filePath)
		{
			_filePath = filePath;
		}

		protected override void HandleLog(string message, Category category, Priority priority, DateTime timeStamp)
		{
			base.HandleLog(message, category, priority, timeStamp);
			
			if (category == Category.Exception)
				message += "\n" + Environment.StackTrace;

			var messageHandled = false;
			FileInfo fi;
			//No one else would be able to create another fileInfo
			lock (LogLock)
				fi = new FileInfo(_filePath);
			
			if (!fi.Exists)
			{
				// Create a file to write to.
				lock (LogLock)
				{
					if (!fi.Exists)
					{
						using (StreamWriter sw = fi.CreateText())
						{
							sw.WriteLine("{0}, {1}, {2}, {3}", timeStamp.ToString("MM-dd-yy - HH:mm:ss ffff"), category, priority, "**********Log File Created**********");
							sw.WriteLine("{0}, {1}, {2}, {3}", timeStamp.ToString("MM-dd-yy - HH:mm:ss ffff"), category, priority, message);
											 
						}
						System.IO.File.SetCreationTime(_filePath, timeStamp);      
					}
					else
					{
						lock (LogLock)
						{
							using (StreamWriter sw = fi.AppendText())
								sw.WriteLine("{0}, {1}, {2}, {3}", timeStamp.ToString("MM-dd-yy - HH:mm:ss ffff"), category, priority, message);
						}

					}
				}
			}
			else
			{
				var createDate = fi.CreationTime;
				var currentDate = DateTime.Now;

				var days = (currentDate - createDate).TotalDays;
				if (days > 30)
				{
					lock (LogLock)
					{
						var s = fi.DirectoryName + "\\" + DateTime.Now.ToFileTimeUtc() + ".csv";

						fi.CopyTo(s, true);
						fi.Delete();
					}

					// recurse to create the new log file
					Log(message, category, priority);
					messageHandled = true;
				}

				if (messageHandled) return;
				lock (LogLock)
				{
					using (var sw = fi.AppendText())
						sw.WriteLine("{0}, {1}, {2}, {3}", timeStamp.ToString("MM-dd-yy - HH:mm:ss ffff"), category, priority, message);
				}
			}
		}
	}
}
