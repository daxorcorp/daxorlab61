using System;
using System.Threading;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Infrastructure.Services
{
    /// <summary>
    /// Async custom logger
    /// </summary>
    public class CustomAsyncFileLogger : CustomFileLogger
    {
        public CustomAsyncFileLogger(string filePath)
            : base(filePath)
        {
		   
        }

        protected override void HandleLog(string message, Category category, Priority priority, DateTime timeStamp)
        {

            ThreadPool.QueueUserWorkItem(LogFromThread, new LogInfo
            {
                Message = message,
                LogCategory = category,
                LogPriority = priority,
                LogTimeStamp = timeStamp
            });
        }

        private void LogFromThread(object state)
        {
            var lInfo = state as LogInfo;
            // ReSharper disable once PossibleNullReferenceException
            base.HandleLog(lInfo.Message, lInfo.LogCategory, lInfo.LogPriority, lInfo.LogTimeStamp);
        }

        private class LogInfo
        {
            public string Message { get; set; }
            public Category LogCategory { get; set; }
            public Priority LogPriority { get; set; }
            public DateTime LogTimeStamp { get; set; }
        }
    }
}