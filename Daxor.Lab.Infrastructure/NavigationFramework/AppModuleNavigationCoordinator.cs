﻿using System;
using System.Collections.Generic;
using Microsoft.Practices.Composite.Events;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Keyboards;

namespace Daxor.Lab.Infrastructure.NavigationFramework
{
	/// <summary>
	/// This class is responsible for navigation between
	/// independent modules.
	/// </summary>
	public class AppModuleNavigationCoordinator : INavigationCoordinator
	{
		#region Fields
	   
		private readonly Dictionary<string, IAppModuleNavigator> _moduleNavigators = new Dictionary<string, IAppModuleNavigator>();
		private readonly IEventAggregator _eventAggregator;
		private string _activeModuleKey;
		private string _previousModuleKey;
	   
		#endregion

		#region Ctor
	  
		public AppModuleNavigationCoordinator(IEventAggregator eventAggregator)
		{
			_eventAggregator = eventAggregator;
		}
		
		#endregion

		#region Properties
	   
		public string ActiveAppModuleKey
		{
			get { return _activeModuleKey; }
		}
		public int TotalAppModuleNavigators
		{
			get { return _moduleNavigators.Count; }
		}

		public string PreviousAppModuleKey
		{
			get { return _previousModuleKey; }
		}
	   
		#endregion

		#region Methods

		public void RegisterAppModuleNavigator(string moduleKey, IAppModuleNavigator moduleNavigator)
		{
			if (moduleKey == null)
			{
				throw new ArgumentNullException("moduleKey");
			}

			if (moduleNavigator == null)
			{
				throw new ArgumentNullException("moduleNavigator");
			}

			if (_moduleNavigators.ContainsKey(moduleKey))
			{
				throw new NotSupportedException("Failed to register navigator. Associated application key already exists.");
			}

			_moduleNavigators.Add(moduleKey, moduleNavigator);
		}
		public bool ActivateAppModule(string appKey, object payload)
		{
			return ActivateAppModule(appKey, payload, DisplayViewOption.LastAccessed);
		}
		public bool ActivateAppModule(string appKey, object payLoad, string prevAppKey)
		{
			return ActivateAppModule(appKey, payLoad, DisplayViewOption.LastAccessed, prevAppKey);
		}
		public bool ActivateAppModule(string appKey, object payload, DisplayViewOption displayOption)
		{
			return ActivateAppModule(appKey, payload, displayOption, "");
		}

		public bool ActivateAppModule(string appKey, object payload, DisplayViewOption displayOption, string prevAppKey)
		{
			if (!_moduleNavigators.ContainsKey(appKey))
				throw new ArgumentOutOfRangeException("appKey", @"ActivateApplication failed. AppKey was not found.");

			// Get from and to navigators
			IAppModuleNavigator fromAppNavigator = null;
			if (_activeModuleKey != null && _moduleNavigators.ContainsKey(_activeModuleKey))
				fromAppNavigator = _moduleNavigators[_activeModuleKey];

			var toAppNavigator = _moduleNavigators[appKey];

			// If an attempt to activate an already active application
			if (fromAppNavigator == toAppNavigator)
			{
				// ReSharper disable once PossibleNullReferenceException
				toAppNavigator.Activate(displayOption, payload);
				return true;
			}
			VirtualKeyboardManager.CloseActiveKeyboard();

			if ((fromAppNavigator != null && !fromAppNavigator.CanDeactivate()) || !toAppNavigator.CanActivate()) return false;

			if (fromAppNavigator != null)
			{
				// Publish event that a given application has been closed
				_eventAggregator.GetEvent<AppModuleDeactivated>().Publish(_activeModuleKey);

				// Close virtual keyboard
				VirtualKeyboardManager.CloseActiveKeyboard();
				fromAppNavigator.Deactivate(() => toAppNavigator.Activate(displayOption, payload));
			}
			else
				toAppNavigator.Activate(displayOption, payload);

			// Publish event that application has been opened
			_eventAggregator.GetEvent<AppModuleActivated>().Publish(appKey);

			// Capture current app key
			_activeModuleKey = appKey;

			//Capture the previous appKey
			_previousModuleKey = prevAppKey;
			return true;
		}

		#endregion
	}
}
