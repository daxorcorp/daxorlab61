﻿using System;
using System.Windows;

namespace Daxor.Lab.Infrastructure.NavigationFramework
{
    public interface IAppModuleNavigator
    {
        /// <summary>
        /// Global navigation coordinator that this IAppModuleNavigator belongs to
        /// </summary>
        INavigationCoordinator NavigationCoordinator { get; }

        /// <summary>
        /// True if a give module is active/false otherwise
        /// </summary>
        bool IsActive { get; }


        /// <summary>
        /// Unique application navigator key 
        /// </summary>
        string AppModuleKey { get; }

        /// <summary>
        /// Readonly property that returns root host of a given Navigator
        /// </summary>
        FrameworkElement RootViewHost { get; }

        /// <summary>
        /// Returns true if application can be activated, false otherwise
        /// </summary>
        /// <returns></returns>
        bool CanActivate();

        /// <summary>
        /// Returns true if application can be deactivated, false otherwise
        /// </summary>
        /// <returns></returns>
        bool CanDeactivate();

        /// <summary>
        /// Activates a view; implementer decided, called by global Navigator
        /// </summary>
        /// <param name="displayViewOption"></param>
        /// <param name="payload"></param>
        void Activate(DisplayViewOption displayViewOption, object payload); 

        /// <summary>
        /// Deactives current app view and calls onComplete callback
        /// </summary>
        /// <param name="onDeactivateCallback"></param>
        void Deactivate(Action onDeactivateCallback);                         

        /// <summary>
        /// Activates specific view based on the viewKey
        /// </summary>
        /// <param name="viewKey"></param>
        /// <param name="payload"></param>
        void ActivateView(string viewKey, object payload);

        /// <summary>
        /// Activates specific view based on the viewKey
        /// </summary>
        /// <param name="viewKey"></param>
        void ActivateView(string viewKey);

        /// <summary>
        /// Registers internal views (FrameworkElement) with unique keys.
        /// Adds Navigator's hostView to the Shell's main region (Workspace region)
        /// </summary>
        /// 
        void Initialize();
    }
}
