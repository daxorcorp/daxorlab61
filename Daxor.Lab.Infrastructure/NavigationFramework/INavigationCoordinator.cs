﻿namespace Daxor.Lab.Infrastructure.NavigationFramework
{
    public interface INavigationCoordinator
    {
        /// <summary>
        /// Currently Active Application Key
        /// </summary>
        string ActiveAppModuleKey { get; }

        string PreviousAppModuleKey { get; }
        /// <summary>
        /// Total number of module navigators
        /// </summary>
        int TotalAppModuleNavigators { get; }    

        /// <summary>
        /// Registration mechanism for app modules
        /// </summary>
        /// <param name="appModuleKey"></param>
        /// <param name="appModuleNavigator"></param>
        void RegisterAppModuleNavigator(string appModuleKey, IAppModuleNavigator appModuleNavigator);


        bool ActivateAppModule(string appModuleKey, object payload);
        bool ActivateAppModule(string appModuleKey, object payload, string previousAppModuleKey);
        bool ActivateAppModule(string appModuleKey, object payload, DisplayViewOption displayOption);
        bool ActivateAppModule(string appModuleKey, object payload, DisplayViewOption displayOption, string previousAppModuleKey);
    }
}
