﻿namespace Daxor.Lab.Infrastructure.NavigationFramework
{
    /// <summary>
    /// Specifies constants defining which view should be displayed after navigation
    /// </summary>
    public enum DisplayViewOption
    {
        /// <summary>
        /// Initial/startup view
        /// </summary>
        Default,       

        /// <summary>
        /// View that was last accessed
        /// </summary>
        LastAccessed, 

        /// <summary>
        /// View containing results
        /// </summary>
        Result,        
    }
}
