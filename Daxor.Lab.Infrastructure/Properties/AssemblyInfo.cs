using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Markup;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Daxor.Lab.Infrastructure")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Daxor.Lab.Infrastructure")]
[assembly: AssemblyCopyright("Copyright ©  2010, 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("da660271-2bd6-4323-b936-852e5819e70b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("6.1.0.0")]
[assembly: AssemblyFileVersion("6.1.0.0")]

[assembly: XmlnsDefinition("http://infra.daxor.com/winfx/2012/xaml", "Daxor.Lab.Infrastructure.Commands")]
[assembly: XmlnsDefinition("http://infra.daxor.com/winfx/2012/xaml", "Daxor.Lab.Infrastructure.Constants")]
[assembly: XmlnsDefinition("http://infra.daxor.com/winfx/2012/xaml", "Daxor.Lab.Infrastructure.Controls")]
[assembly: XmlnsDefinition("http://infra.daxor.com/winfx/2012/xaml", "Daxor.Lab.Infrastructure.UserControls")]
[assembly: XmlnsDefinition("http://infra.daxor.com/winfx/2012/xaml", "Daxor.Lab.Infrastructure.Converters")]
[assembly: XmlnsDefinition("http://infra.daxor.com/winfx/2012/xaml", "Daxor.Lab.Infrastructure.Keyboards")]
[assembly: XmlnsDefinition("http://infra.daxor.com/winfx/2012/xaml", "Daxor.Lab.Infrastructure.TouchClick")]
[assembly: XmlnsDefinition("http://infra.daxor.com/winfx/2012/xaml", "Daxor.Lab.Infrastructure.Utilities")]
[assembly: XmlnsDefinition("http://infra.daxor.com/winfx/2012/xaml", "Daxor.Lab.Infrastructure.RuleFramework")]
[assembly: XmlnsDefinition("http://infra.daxor.com/winfx/2012/xaml", "Daxor.Lab.Infrastructure.Entities")]




