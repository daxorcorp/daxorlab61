﻿using Microsoft.Practices.Composite.Presentation.Commands;

namespace Daxor.Lab.Infrastructure.Commands
{
    public static class GlobalCommands
    {
        //global shutdown commands
        public static readonly CompositeCommand ShutdownCommand = new CompositeCommand();

        //open privacy screen command
        public static readonly CompositeCommand OpenPrivacyScreenCommand = new CompositeCommand();
    }
}
