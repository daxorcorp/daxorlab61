﻿using System;
using System.Windows.Threading;
using Daxor.Lab.Infrastructure.Utilities;
using System.Windows;

namespace Daxor.Lab.Infrastructure.Threading
{
	/// <summary>
	/// Responsible for marshaling back and excuting UI changes in UI thread.
	/// </summary>
	public static class IdealDispatcher
	{
		private static Dispatcher _dispatcher;

		/// <summary>
		/// Main initialization routine; attempts to ensure that we have cached UI Dispatcher
		/// </summary>
		public static void Initialize()
		{
			if (_dispatcher == null)
				EnsureDispatcherExistence();
		}

		public static void Initialize(Dispatcher dispatcher)
		{
			ContractHelper.ArgumentNotNull(dispatcher, "dispatcher");
			_dispatcher = dispatcher;
		}

		/// <summary>
		/// Execute provided delegate asynchronously on the UI thread.
		/// </summary>
		/// <param name="uiAction">Delegate that gets executed on UI thread.</param>
		public static void BeginInvoke(Action uiAction)
		{
			if (_dispatcher == null)
				EnsureDispatcherExistence();

			//If we are already on UI thread simple call action; otherwise asynchroniously execute
			// ReSharper disable once PossibleNullReferenceException
			if (_dispatcher.CheckAccess() || DesignerPropertiesExtension.IsInDesignModeStatic)
				uiAction();
			else
				_dispatcher.BeginInvoke(uiAction);
		}


		/// <summary>
		/// Ensures that IdealDispatcher contains an instance of the UI Dispatcher.
		/// </summary>
		private static void EnsureDispatcherExistence()
		{
		   //Determine if we are in designer mode; if so simply return
			if (DesignerPropertiesExtension.IsInDesignModeStatic)
				return;

			//Attempt to use the RootVisual (Silverlight) or MainWindow(WPF) in order to retrieve the dispatcher instance.
			//This call can only succeed if invoked from the UI thread.
			try
			{
#if SILVERLIGHT
				_dispatcher = Application.Current.RootVisual.Dispatcher;
#else
				_dispatcher = Application.Current.MainWindow.Dispatcher;
#endif
			}
			catch
			{
				throw new InvalidOperationException("IdealDispatcher needs to have a cached instance of dispatcher. Initialize() or Initialize(Dispatcher) must be called from the UI thread");
			}

			if (_dispatcher == null)
				throw new InvalidOperationException("Unable to retrieve a suitable dispatcher");
		}
	}
}
