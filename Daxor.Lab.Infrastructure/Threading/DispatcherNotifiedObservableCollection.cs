﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Windows.Threading;

namespace Daxor.Lab.Infrastructure.Threading
{
	/// <summary>
	/// *** Borrowed from MVVM Cinch Framework. All credit goes to Sasha Barber.***
	/// 
	/// This class provides an ObservableCollection which supports the 
	/// Dispatcher thread marshalling for added items. 
	/// 
	/// This class does not take support any thread sycnhronization of
	/// adding items using multiple threads, that level of thread synchronization
	/// is left to the user. This class simply marshalls the CollectionChanged
	/// call to the correct Dispatcher thread
	/// </summary>
	/// <typeparam name="T">Type this collection holds</typeparam>
	public class DispatcherNotifiedObservableCollection<T> : ThreadSafeObservableCollection<T>
	{
		#region Overrides
		/// <summary>
		/// Occurs when an item is added, removed, changed, moved, 
		/// or the entire list is refreshed.
		/// </summary>
		public override event NotifyCollectionChangedEventHandler CollectionChanged;

		/// <summary>
		/// Raises the <see cref="E:System.Collections.ObjectModel.
		/// ObservableCollection`1.CollectionChanged"/> 
		/// event with the provided arguments.
		/// </summary>
		/// <param name="e">Arguments of the event being raised.</param>
		protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
		{
			var collectionChangedHandler = CollectionChanged;
			if (collectionChangedHandler == null) return;

			var dispatcher =
				(from NotifyCollectionChangedEventHandler handler in collectionChangedHandler.GetInvocationList()
					let dispatcherObject = handler.Target as DispatcherObject
					where dispatcherObject != null
					select dispatcherObject.Dispatcher).FirstOrDefault();

			if (dispatcher != null && dispatcher.CheckAccess() == false)
			{
				dispatcher.Invoke(DispatcherPriority.DataBind, (Action)(() => OnCollectionChanged(e)));
			}
			else
			{
				foreach (var handler in collectionChangedHandler.GetInvocationList())
				{
					var notificationHandler = (NotifyCollectionChangedEventHandler) handler;
					notificationHandler.Invoke(this, e);
				}
			}
		}
		#endregion
	}
}
