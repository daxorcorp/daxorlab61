﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Threading;

namespace Daxor.Lab.Infrastructure.Threading
{
    // Thread safe observable collection. Allows deletion and addition of new items
    // on a non UI thread. The actual collection has to be instantiated on the UI thread, in 
    // order for us to grab the Dispatcher.

    // Code provided by WPF Microsofot Desciple - Tamir Khason
    public class ThreadSafeObservableCollection<T> : ObservableCollection<T>
    {
        private readonly Dispatcher _dispatcher;
        private readonly ReaderWriterLock _lock;

        public ThreadSafeObservableCollection()
        {
            _dispatcher = Dispatcher.CurrentDispatcher;
            _lock = new ReaderWriterLock();
        }

        protected override void ClearItems()
        {
            if (_dispatcher.CheckAccess())
            {
                LockCookie c = _lock.UpgradeToWriterLock(-1);
                base.ClearItems();
                _lock.DowngradeFromWriterLock(ref c);
            }
            else
            {
                _dispatcher.Invoke(DispatcherPriority.DataBind, (Action)Clear);
            }
        }

        protected override void InsertItem(int index, T item)
        {
            if (_dispatcher.CheckAccess())
            {
                if (index > Count)
                {
                    return;
                }

                var c = _lock.UpgradeToWriterLock(-1);
                base.InsertItem(index, item);
                _lock.DowngradeFromWriterLock(ref c);
            }
            else
            {
                var e = new object[] { index, item };
                _dispatcher.Invoke(DispatcherPriority.DataBind, (SendOrPostCallback)delegate { InsertItemImpl(e); }, e);
            }
        }

        protected override void RemoveItem(int index)
        {
            if (_dispatcher.CheckAccess())
            {
                if (index >= Count)
                {
                    return;
                }

                var c = _lock.UpgradeToWriterLock(-1);
                base.RemoveItem(index);
                _lock.DowngradeFromWriterLock(ref c);
            }
            else
            {
                _dispatcher.Invoke(DispatcherPriority.DataBind, (SendOrPostCallback)delegate { RemoveItem(index); }, index);
            }
        }

        protected override void MoveItem(int oldIndex, int newIndex)
        {
            if (_dispatcher.CheckAccess())
            {
                if (oldIndex >= Count | newIndex >= Count | oldIndex == newIndex)
                {
                    return;
                }
                
                var c = _lock.UpgradeToWriterLock(-1);
                base.MoveItem(oldIndex, newIndex);
                _lock.DowngradeFromWriterLock(ref c);
            }
            else
            {
                object[] e = { oldIndex, newIndex };
                _dispatcher.Invoke(DispatcherPriority.DataBind, (SendOrPostCallback)delegate { MoveItemImpl(e); }, e);
            }
        }

        protected override void SetItem(int index, T item)
        {
            if (_dispatcher.CheckAccess())
            {
                var c = _lock.UpgradeToWriterLock(-1);
                base.SetItem(index, item);
                _lock.DowngradeFromWriterLock(ref c);
            }
            else
            {
                var e = new object[] { index, item };
                _dispatcher.Invoke(DispatcherPriority.DataBind, (SendOrPostCallback)delegate { SetItemImpl(e); }, e);
            }
        }

        private void InsertItemImpl(IList<object> e)
        {
            if (_dispatcher.CheckAccess())
            {
                InsertItem((int)e[0], (T)e[1]);
            }
            else
            {
                _dispatcher.Invoke(DispatcherPriority.DataBind, (SendOrPostCallback)delegate { InsertItemImpl(e); });
            }
        }

        private void MoveItemImpl(IList<object> e)
        {
            if (_dispatcher.CheckAccess())
            {
                MoveItem((int)e[0], (int)e[1]);
            }
            else
            {
                _dispatcher.Invoke(DispatcherPriority.DataBind, (SendOrPostCallback)delegate { MoveItemImpl(e); });
            }
        }

        private void SetItemImpl(IList<object> e)
        {
            if (_dispatcher.CheckAccess())
            {
                SetItem((int)e[0], (T)e[1]);
            }
            else
            {
                _dispatcher.Invoke(DispatcherPriority.DataBind, (SendOrPostCallback)delegate { SetItemImpl(e); });
            }
        }
    }
}
