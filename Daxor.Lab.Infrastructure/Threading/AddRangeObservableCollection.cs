﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Daxor.Lab.Infrastructure.Threading
{
	/// <summary>
	/// *** Borrowed from MVVM Cinch Framework. All credit goes to Sasha Barber.***
	/// 
	/// 
	/// This class provides a method to allow mutiple entries to be added
	/// to an ObservableCollection without the CollectionChanged event being fired.
	/// As this class also inherits from DispatcherNotifiedObservableCollection, it
	/// also supports the Dispatcher thread marshalling for added items. 
	/// 
	/// This class does not take support any thread sycnhronization of
	/// adding items using multiple threads, that level of thread synchronization
	/// is left to the user. This class simply marshalls the CollectionChanged
	/// call to the correct Dispatcher thread
	/// 
	/// This class was taken and subsequently modified from
	/// http://peteohanlon.wordpress.com/2008/10/22/bulk-loading-in-observablecollection/
	/// </summary>
	/// <typeparam name="T">Type this collection holds</typeparam>
	public class AddRangeObservableCollection<T> :
		DispatcherNotifiedObservableCollection<T>
	{
		#region Data
		private bool _suppressNotification;
		#endregion
		
		#region Public Methods
		/// <summary>
		/// Adds a range of items to the Collection, without firing the
		/// CollectionChanged event
		/// </summary>
		/// <param name="list">The items to add</param>
		public void AddRange(IEnumerable<T> list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}

			_suppressNotification = true;

			foreach (T item in list)
			{
				Add(item);
			}
			
			_suppressNotification = false;
			OnCollectionChanged(new
				NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
		}

		/// <summary>
		/// Adds a range of items to the Collection, without firing the
		/// CollectionChanged event
		/// </summary>
		/// <param name="list">The items to add</param>
		public void RmoveRange(IEnumerable<T> list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}

			_suppressNotification = true;

			foreach (T item in list)
			{
				Remove(item);
			}

			_suppressNotification = false;
			OnCollectionChanged(new
				NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
		}

		/// <summary>
		/// Clears the collection, without firing the
		/// CollectionChanged event, then adds a range of items to the list 
		/// publishing the CollectionChanged event on completion.
		/// </summary>
		/// <param name="list">The items to add</param>
		public void ClearAndAddRange(IEnumerable<T> list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}

			_suppressNotification = true;

			Clear();
			foreach (T item in list)
				Add(item);

			_suppressNotification = false;
			OnCollectionChanged(new
				NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
		}
		#endregion

		#region Overrides
		/// <summary>
		/// Only raise the OnCollectionChanged event if there 
		/// is currently no suppressed notification
		/// </summary>
		/// <param name="e"></param>
		protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
		{
			if (!_suppressNotification)
			{
				base.OnCollectionChanged(e);
			}
		}
		#endregion
	}
}
