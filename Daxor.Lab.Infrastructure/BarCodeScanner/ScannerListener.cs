﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Infrastructure.BarCodeScanner
{
	public class ScannerListener
	{
		#region Constants

		public static readonly uint MaxAvailableScanners = 10;
		private readonly bool _ignoreDuplicateInputReceived; //Ignores duplicate decode Window event after being reattached. Known bug.
		private const byte DataMatrixSymbologyCode = 27; //SnapiDLL code for dataMatrix bar code type

		#endregion

		#region Private member variables

		private readonly IEventAggregator _eventAggregator; //Event aggregator
		private readonly ILoggerFacade _customLoggerFacade; //Application logger
		
		private byte[] _dllBuffer;               // Managed destination buffer for SNAPI.DLL
		private GCHandle _pinnedBuffer;          // Unmanaged version of dllBuffer passed to SNAPI.DLL
		private int _currentDeviceHandle;        // Device handle for currently active scanner
		private HwndSource _source;              // Window identifier for this application
		private bool _isAttached;        // Is a scanner attached to this computer?
		private int _numberOfTimesDetached;  // How many times has a scanner been unplugged?
		private int _mostRecentScannerEvent; // Most recent message received from the scanner
		
		#endregion

		#region Constructor

		public ScannerListener(IEventAggregator eventAggregator, ILoggerFacade customLoggerFacade)
		{
		    _ignoreDuplicateInputReceived = true;
            _eventAggregator = eventAggregator;
			_customLoggerFacade = customLoggerFacade;
			InitializeWindowMessageDelegate();
		}
		#endregion

		#region WndProc()

		private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
		{
			if (msg < SnapiDll.WM_APP)
			{
				return IntPtr.Zero;
			}
			
			if (msg == SnapiDll.WM_DECODE)
			{
				if ((_numberOfTimesDetached > 0) &&
					(_mostRecentScannerEvent == SnapiDll.WM_DEVICE_NOTIFICATION) &&
					_ignoreDuplicateInputReceived)
				{
					_mostRecentScannerEvent = msg;
					return IntPtr.Zero;
				}

				// Copy hiWord-2 bytes of the decode buffer into a local buffer
				var hiWord = Marshal.ReadInt32(wParam, 0);
				var decodedData = new byte[hiWord - 2];
				Array.Copy(_dllBuffer, 2, decodedData, 0, hiWord - 2);

				// Updated the text box with the decoded value
				var enc = new System.Text.ASCIIEncoding();
				
				// Make sure only a data matrix bar code was scanned
				var codeType = _dllBuffer[0];

				_eventAggregator.GetEvent<ScannerInputReceived>().Publish(new ScannerInputPayload(codeType == DataMatrixSymbologyCode, enc.GetString(decodedData)));
			}

			else if (msg == SnapiDll.WM_DEVICE_NOTIFICATION)
			{
				int hiWord = Marshal.ReadInt32(wParam, 4);
				if (hiWord == SnapiDll.DEVICE_DISCONNECT)
				{
					AttemptToDisconnectScanner();
				}
				if (hiWord == SnapiDll.DEVICE_ATTACH)
				{
					AttemptToConnectScanner();
				}
			}

			_mostRecentScannerEvent = msg;


			return IntPtr.Zero;
		}
		#endregion

		private void InitializeWindowMessageDelegate()
		{
			// Add the hook to listen for Windows events
			_source = PresentationSource.FromVisual(Application.Current.MainWindow) as HwndSource;

		    // ReSharper disable once PossibleNullReferenceException
			_source.AddHook(WndProc);

			AttemptToConnectScanner();
		}

		private void AttemptToConnectScanner()
		{
			if (_isAttached) return;
			// Ask the driver what scanners are available
			var deviceHandles = new int[MaxAvailableScanners];
			int numberOfHandles;
			unsafe
			{
				// Initilaze SNAPI.dll to detect all the scanners attached
				SnapiDll.SNAPI_Init(_source.Handle, deviceHandles, &numberOfHandles);
			}

			// No scanners are connected, so there's nothing to do.
			if (numberOfHandles == 0)
			{
				return;
			}

			// Connect to the first scanner available
			_currentDeviceHandle = deviceHandles[0];
			var canConnect = SnapiDll.SNAPI_Connect(_currentDeviceHandle);

			if (canConnect == SnapiDll.SNAPI_NOERROR)
			{
				System.Threading.Monitor.Enter(this);

				int handle = _currentDeviceHandle;
				// The SNAPI dll expects this memory to be fixed; so we need to pin it in .NET
				_dllBuffer = new byte[SnapiDll.MAX_VIDEO_LEN];
				_pinnedBuffer = GCHandle.Alloc(_dllBuffer, GCHandleType.Pinned);

				SnapiDll.SNAPI_SetDecodeBuffer(handle, _pinnedBuffer.AddrOfPinnedObject(), SnapiDll.MAX_VIDEO_LEN);
				SnapiDll.SNAPI_SetImageBuffer(handle, _pinnedBuffer.AddrOfPinnedObject(), SnapiDll.MAX_VIDEO_LEN);
				SnapiDll.SNAPI_SetVideoBuffer(handle, _pinnedBuffer.AddrOfPinnedObject(), SnapiDll.MAX_VIDEO_LEN);
				SnapiDll.SNAPI_SetParamPersistance(handle, 1);
				System.Threading.Monitor.Exit(this);
			}

			//Notify listeners that scanner was disconnected
			_eventAggregator.GetEvent<ScannerConnectionChanged>().Publish(ScannerConnectionChangedPayload.Connected);

			_isAttached = true;
		}
		private void AttemptToDisconnectScanner()
		{
		    if (!_isAttached) return;

			try
			{
				System.Threading.Monitor.Enter(this);
				SnapiDll.SNAPI_Disconnect(_currentDeviceHandle);

				_dllBuffer = null;
				_pinnedBuffer.Free();

				System.Threading.Monitor.Exit(this);

				//Notify listeners that scanner was disconnected
				_eventAggregator.GetEvent<ScannerConnectionChanged>().Publish(ScannerConnectionChangedPayload.Disconnected);

				_isAttached = false;
				_numberOfTimesDetached++;
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message, "Unable to Disconnect Scanner Safely");
				_customLoggerFacade.Log(e.Message, Category.Exception, Priority.None);
 
			}
		}
	}
}
