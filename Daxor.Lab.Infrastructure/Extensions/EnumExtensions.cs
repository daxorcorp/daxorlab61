﻿using System;

namespace Daxor.Lab.Infrastructure.Extensions
{
    public static class EnumExtensions
    {
        public static string GetStringValue(this Enum value)
        {
            var type = value.GetType();

            var fieldInfo = type.GetField(value.ToString());
            var attributes = fieldInfo.GetCustomAttributes(typeof(StringAttribute),false) as StringAttribute[];
            if (attributes == null || attributes.Length == 0)
                return null;

            return attributes[0].StringValue;
        }
    }
}
