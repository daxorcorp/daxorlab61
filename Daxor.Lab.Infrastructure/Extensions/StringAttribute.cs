﻿using System;

namespace Daxor.Lab.Infrastructure.Extensions
{
    public class  StringAttribute : Attribute
    {
        public string StringValue { get; protected set; }

        public StringAttribute(string value)
        {
            StringValue = value;
        }
    }
}
