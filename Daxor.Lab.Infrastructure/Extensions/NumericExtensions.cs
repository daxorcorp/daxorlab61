﻿using System;

namespace Daxor.Lab.Infrastructure.Extensions
{
    public static class NumericExtensions
    {
        public static string RoundAndFormat(this double number, int decimalPlaces)
        {
            var formatString = "#0.";
            for (var i = 0; i < decimalPlaces; i++)
                formatString += "0";

            return Math.Round(number, decimalPlaces, MidpointRounding.AwayFromZero).ToString(formatString);
        }
    }
}
