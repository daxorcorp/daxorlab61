﻿using System;

namespace Daxor.Lab.Infrastructure.Extensions
{
    /// <summary>
    /// String extensions
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Converts string to all lower letters and capatilizing first letter 
        /// </summary>
        public static string ToFirstUpperInvariant(this string source)
        {
            if (string.IsNullOrEmpty(source))
                return string.Empty;

            // convert to char array of the string
            char[] letters = source.ToCharArray();

            // upper case the first char
            letters[0] = char.ToUpper(letters[0]);

            // return the array made of the new char array
            return new string(letters);
        }


        /// <summary>
        /// Extends Contains() to support StringComparison enums
        /// </summary>
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            if (string.IsNullOrEmpty(toCheck) || string.IsNullOrEmpty(source))
                return false;

            return source.IndexOf(toCheck, comp) >= 0;
        }

        /// <summary>
        /// Get a substring of the first N characters.
        /// </summary>
        public static string Truncate(this string source, int length)
        {
            if (source.Length > length)
            {
                source = source.Substring(0, length);
            }
            return source;
        }
    }
}
