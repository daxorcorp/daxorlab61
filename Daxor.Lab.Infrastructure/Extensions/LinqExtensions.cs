﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Daxor.Lab.Infrastructure.Extensions
{
    public static class LinqExtensions
    {
        public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> source, int listSize)
        {
            T[] array = null;
            var count = 0;
            
            foreach (var item in source)
            {
                if (array == null)
                    array = new T[listSize];
                
                array[count++] = item;

                if (count != listSize) continue;
                yield return new ReadOnlyCollection<T>(array);
                array = null;
                count = 0;
            }

            if (array == null) yield break;
            Array.Resize(ref array, count);
            yield return new ReadOnlyCollection<T>(array);
        }
    }
}
