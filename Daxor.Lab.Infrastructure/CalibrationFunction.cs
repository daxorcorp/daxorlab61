﻿using System;

namespace Daxor.Lab.Infrastructure
{
    public class CalibrationFunction
    {
        private readonly double _slope, _offset;

        public CalibrationFunction(double slope, double offset)
        {
            _slope = slope;
            _offset = offset;
        }

        public int GetChannel(double energyInKeV)
        {
            int channel;

            try
            {
                channel = (Int32)((energyInKeV - _offset) / _slope);
            }
            catch
            {
                channel = -1;
            }

            return channel;
        }

        public double GetEnergy(int channel)
        {
            return _slope * channel + _offset;
        }
    }
}
