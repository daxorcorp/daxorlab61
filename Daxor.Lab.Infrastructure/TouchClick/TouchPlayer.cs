﻿using System;
using System.Collections.Generic;
using System.Media;
using System.IO;

namespace Daxor.Lab.Infrastructure.TouchClick
{
	public enum Sound
	{
		Touch,
		Warning,
		Focus,
	}

	public class TouchPlayer
	{
		static readonly Dictionary<Sound, SoundPlayer> _players = new Dictionary<Sound, SoundPlayer>();
		static MemoryStream GetSoundStream(string wavFilePath)
		{
			var stream = new MemoryStream();
			using (var fs = File.OpenRead(wavFilePath))
			{
				var b = new byte[1024];
				while (fs.Read(b, 0, b.Length) > 0)
					stream.Write(b, 0, b.Length);
			}
			return stream;
		}

		public static bool IsEnabled { get; set; }
		public static void PlaySound(Sound sound)
		{
			if (!IsEnabled) return;

			SoundPlayer player;
			if (!_players.TryGetValue(sound, out player)) return;

			//Resets the position stream
			player.Stream.Position = 0;
			player.Play();
		}
		public static void InitializeSound(string wavFilePath, Sound forSound)
		{
			if (String.IsNullOrEmpty(wavFilePath)) return;

			var player = new SoundPlayer(GetSoundStream(wavFilePath)) {Stream =
			{
				Position = 0
			}};
			_players[forSound] = player;
		}
	}
}
