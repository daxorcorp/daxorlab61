﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Controls;

namespace Daxor.Lab.Infrastructure.TouchClick
{
	public class TouchClick
	{
		#region Delegates

		static readonly Delegate ButtonClickHandler = new RoutedEventHandler((o, arg) => InternalPlaySound((DependencyObject)o));
		static readonly Delegate TextBoxGotFocusHandler = new RoutedEventHandler((o, arg) => InternalPlaySound((DependencyObject)o));
		static readonly Delegate ListBoxItemClickHandler = new RoutedEventHandler((o, arg) => InternalPlaySound((DependencyObject)o));

		#endregion

		#region PlayWarningSound

		// Using a DependencyProperty as the backing store for PlayWarningSound.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty PlayWarningSoundProperty =
			DependencyProperty.RegisterAttached("PlayWarningSound", typeof(bool), typeof(TouchClick),
		   new FrameworkPropertyMetadata(false));

		public static bool GetPlayWarningSound(DependencyObject obj)
		{
			return (bool)obj.GetValue(PlayWarningSoundProperty);
		}
		public static void SetPlayWarningSound(DependencyObject obj, bool value)
		{
			obj.SetValue(PlayWarningSoundProperty, value);
		}

		#endregion

		#region PlaySoundOnButtonClick

		public static bool GetPlaySoundOnButtonClick(DependencyObject obj)
		{
			return (bool)obj.GetValue(PlaySoundOnButtonClickProperty);
		}
		public static void SetPlaySoundOnButtonClick(DependencyObject obj, bool value)
		{
			obj.SetValue(PlaySoundOnButtonClickProperty, value);
		}

		// Using a DependencyProperty as the backing store for EnableButtonSound.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty PlaySoundOnButtonClickProperty =
			DependencyProperty.RegisterAttached("PlaySoundOnButtonClick", typeof(bool), typeof(TouchClick),
		   new FrameworkPropertyMetadata(false, PlaySoundOnButtonClickChanged));

		private static void PlaySoundOnButtonClickChanged(DependencyObject control, DependencyPropertyChangedEventArgs e)
		{
			var oldValue = (bool)e.OldValue;
			var newValue = (bool)e.NewValue;
			var element = control as FrameworkElement;
			if (element == null) return;

			if (newValue && !oldValue)
				EventManager.RegisterClassHandler(typeof(ButtonBase), UIElement.MouseLeftButtonDownEvent, ButtonClickHandler);
		}

		#endregion

		#region PlaySoundOnTabItemClick

		public static bool GetPlaySoundOnTabItemClick(DependencyObject obj)
		{
			return (bool)obj.GetValue(PlaySoundOnTabItemClickProperty);
		}
		public static void SetPlaySoundOnTabItemClick(DependencyObject obj, bool value)
		{
			obj.SetValue(PlaySoundOnTabItemClickProperty, value);
		}

		// Using a DependencyProperty as the backing store for EnableButtonSound.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty PlaySoundOnTabItemClickProperty =
			DependencyProperty.RegisterAttached("PlaySoundOnTabItemClick", typeof(bool), typeof(TouchClick),
		   new FrameworkPropertyMetadata(false, PlaySoundOnTabItemClickChanged));

		private static void PlaySoundOnTabItemClickChanged(DependencyObject control, DependencyPropertyChangedEventArgs e)
		{
			var oldValue = (Boolean)e.OldValue;
			var newValue = (Boolean)e.NewValue;
			var element = control as FrameworkElement;
			if (element == null) return;

			if (newValue && !oldValue)
				EventManager.RegisterClassHandler(typeof(TabItem), UIElement.MouseLeftButtonDownEvent, ButtonClickHandler);
		}

		#endregion

		#region PlaySoundOnTextBoxFocus

		public static bool GetPlaySoundOnTextBoxFocus(DependencyObject obj)
		{
			return (bool)obj.GetValue(PlaySoundOnTextBoxFocusProperty);
		}
		public static void SetPlaySoundOnTextBoxFocus(DependencyObject obj, bool value)
		{
			obj.SetValue(PlaySoundOnTextBoxFocusProperty, value);
		}

		// Using a DependencyProperty as the backing store for EnableTextBoxSound.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty PlaySoundOnTextBoxFocusProperty =
			DependencyProperty.RegisterAttached("PlaySoundOnTextBoxFocus", typeof(bool), typeof(TouchClick),
			new FrameworkPropertyMetadata(false, OnPlaySoundOnTextBoxFocusChanged));

		private static void OnPlaySoundOnTextBoxFocusChanged(DependencyObject control, DependencyPropertyChangedEventArgs e)
		{
			var oldValue = (Boolean)e.OldValue;
			var newValue = (Boolean)e.NewValue;
			var element = control as FrameworkElement;
			if (element == null) return;

			//Registering event handler for all ListBoxItem; should get called before any other event handler
			if (newValue && !oldValue)
				EventManager.RegisterClassHandler(typeof(TextBoxBase), UIElement.PreviewGotKeyboardFocusEvent, TextBoxGotFocusHandler, true);
		}

		#endregion

		#region PlaySoundOnListBoxItemClick

		public static bool GetPlaySoundOnListBoxItemClick(DependencyObject obj)
		{
			return (bool)obj.GetValue(PlaySoundOnListBoxItemClickProperty);
		}
		public static void SetPlaySoundOnListBoxItemClick(DependencyObject obj, bool value)
		{
			obj.SetValue(PlaySoundOnListBoxItemClickProperty, value);
		}

		// Using a DependencyProperty as the backing store for EnableTextBoxSound.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty PlaySoundOnListBoxItemClickProperty =
			DependencyProperty.RegisterAttached("PlaySoundOnListBoxItemClick", typeof(bool), typeof(TouchClick),
			new FrameworkPropertyMetadata(false, OnPlaySoundOnListBoxItemClickChanged));

		private static void OnPlaySoundOnListBoxItemClickChanged(DependencyObject control, DependencyPropertyChangedEventArgs e)
		{
			var oldValue = (bool)e.OldValue;
			var newValue = (bool)e.NewValue;

			if (newValue && !oldValue)
				EventManager.RegisterClassHandler(typeof(ListBoxItem), UIElement.MouseLeftButtonDownEvent, ListBoxItemClickHandler);
		}

		#endregion

		#region TouchSoundEnabled

		public static bool GetTouchSoundEnabled(DependencyObject obj)
		{
			return (bool)obj.GetValue(TouchSoundEnabledProperty);
		}

		public static void SetTouchSoundEnabled(DependencyObject obj, bool value)
		{
			obj.SetValue(TouchSoundEnabledProperty, value);
		}

		// Using a DependencyProperty as the backing store for TouchSoundEnabled.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty TouchSoundEnabledProperty =
			DependencyProperty.RegisterAttached("TouchSoundEnabled", typeof(bool), typeof(TouchClick), new UIPropertyMetadata(false, OnTouchSoundEnabledChanged));

		private static void OnTouchSoundEnabledChanged(DependencyObject control, DependencyPropertyChangedEventArgs e)
		{
			var newValue = (bool)e.NewValue;

			SetPlaySoundOnButtonClick(control, newValue);
			SetPlaySoundOnListBoxItemClick(control, newValue);
			SetPlaySoundOnTabItemClick(control, newValue);
			SetPlaySoundOnTextBoxFocus(control, newValue);
		}

		#endregion

		#region IsSoundEnabled

		public static bool GetIsSoundEnabled(DependencyObject obj)
		{
			return (bool)obj.GetValue(IsSoundEnabledProperty);
		}
		
		public static void SetIsSoundEnabled(DependencyObject obj, bool value)
		{
			obj.SetValue(IsSoundEnabledProperty, value);
		}

		// Using a DependencyProperty as the backing store for IsSoundEnabled.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty IsSoundEnabledProperty =
			DependencyProperty.RegisterAttached("IsSoundEnabled", typeof(bool), typeof(TouchClick),
			new FrameworkPropertyMetadata(true));

		#endregion

		public static void Click()
		{
			if (TouchPlayer.IsEnabled)
				TouchPlayer.PlaySound(Sound.Touch);
		}

		#region Helpers

		static void InternalPlaySound(DependencyObject invoker)
		{
			var element = invoker as UIElement;
			if (element == null)
				return;

			if (GetIsSoundEnabled(invoker) && !element.IsFocused)
			{
				TouchPlayer.PlaySound(GetPlayWarningSound(invoker) ? Sound.Warning : Sound.Touch);
			}
		}

		#endregion

	}
}
