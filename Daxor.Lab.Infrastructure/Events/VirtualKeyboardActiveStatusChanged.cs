﻿using Daxor.Lab.Infrastructure.Events.Payloads;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// Represents a composite presentation event that is fired when the status
    /// (i.e., activated/deactivated) of the active virtual keyboard changes.
    /// </summary>
    public class VirtualKeyboardActiveStatusChanged : CompositePresentationEvent<VirtualKeyboardActiveStatusPayload> {}
}