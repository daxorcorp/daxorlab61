﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// Fired when background acquisition is aborted.
    /// (TODO: think if we can move this to CLR Events)
    /// </summary>
    public class BackgroundAcquisitionAborted : CompositePresentationEvent<object> {}
}
