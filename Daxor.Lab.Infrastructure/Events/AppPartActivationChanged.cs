﻿using Daxor.Lab.Infrastructure.Events.Payloads;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    // An event to activate specific application part.
    // Example: When BV application is activated, MainMenu Drop down should be displayed.
    public class AppPartActivationChanged : CompositePresentationEvent<ActivationChangedPayload> {}
}
