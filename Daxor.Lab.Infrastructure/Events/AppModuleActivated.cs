﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// Fire when module (BV, QC, WIPE, UTILITIES) are activated.
    /// Header module catches this event to show currently active 
    /// module with a grid dot (we might change the dot to something else).
    /// Fired by NavigationCoordinator.
    /// </summary>
    public class AppModuleActivated : CompositePresentationEvent<string> {}
}
