﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// Fired when module is navigated away from.
    /// Fired by NavigationCoordinator.
    /// </summary>
    public class AppModuleDeactivated : CompositePresentationEvent<string> {}
}
