﻿using Daxor.Lab.Infrastructure.Events.Payloads;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    public class SampleChangerPositionChangeCompleted : CompositePresentationEvent<SampleChangerPositionChangeCompletedPayload> {}
}
