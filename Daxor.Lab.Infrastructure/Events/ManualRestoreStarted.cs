﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// This event is fired once the manual restore workflow is starting. This prevents QC from trying to access the a stored procedure when it doesn't exist.
    /// </summary>
    public class ManualRestoreStarted : CompositePresentationEvent<object> {}
}
