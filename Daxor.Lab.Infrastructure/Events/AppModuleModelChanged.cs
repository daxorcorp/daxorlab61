﻿using Daxor.Lab.Infrastructure.Events.Payloads;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// This event is fired when an underlying model for a given AppModule changes.
    /// This allows passing parameters between ViewModels while creating a low coupling between themselves.
    /// In other words, ViewModels don't need to know about each other. 
    /// Each ViewModel is responsible for interpreting if it's capable of handling a given payload.
    /// 
    /// 
    /// For example: In BVAModule (TestSelectionViewModel) fires this event with new Model,
    /// where ModelInfo.NewModel is a BVATestModel object
    /// </summary>
    public class AppModuleModelChanged : CompositePresentationEvent<ModelChangedPayload> {}
}
