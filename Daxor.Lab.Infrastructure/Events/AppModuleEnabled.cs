﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// Fired when module is enabled. Navigation can occur now.
    /// </summary>
    public class AppModuleEnabled : CompositePresentationEvent<string> {}
}
