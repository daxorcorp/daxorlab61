﻿using Daxor.Lab.Infrastructure.SystemNotifications;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    public class SystemNotificationOccurred : CompositePresentationEvent<SystemNotification> {}
}
