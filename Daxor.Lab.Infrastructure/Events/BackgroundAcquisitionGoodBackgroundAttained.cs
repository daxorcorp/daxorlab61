﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    public class BackgroundAcquisitionGoodBackgroundAttained : CompositePresentationEvent<bool> {}
}
