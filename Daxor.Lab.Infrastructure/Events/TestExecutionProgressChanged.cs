﻿using Microsoft.Practices.Composite.Presentation.Events;
using Daxor.Lab.Domain.Eventing;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// Aggregate event that gets fired when a test's execution progress has changed
    /// </summary>
    public class TestExecutionProgressChanged : CompositePresentationEvent<TestExecutionProgressChangedEventArgs> {}
}
