﻿using Daxor.Lab.Infrastructure.Events.Payloads;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// Fired when a given sample acquisition has completed.
    /// </summary>
    public class SampleAcquisitionCompleted : CompositePresentationEvent<SampleAcquisitionPayload> {}
}
