﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// This event will be fired once the Manual restoration of the database has been completed.
    /// </summary>
    public class ManualRestoreCompleted : CompositePresentationEvent<object> {}
}
