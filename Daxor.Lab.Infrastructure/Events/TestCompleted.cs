﻿using Daxor.Lab.Domain.Interfaces;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// Aggregate event that gets fired when a test has completed
    /// </summary>
    public class TestCompleted : CompositePresentationEvent<ITest> {}
}
