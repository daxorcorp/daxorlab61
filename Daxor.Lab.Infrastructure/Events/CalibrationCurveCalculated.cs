﻿using Daxor.Lab.Domain.Interfaces;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
	public class CalibrationCurveCalculated : CompositePresentationEvent<ICalibrationCurve> {}
}
