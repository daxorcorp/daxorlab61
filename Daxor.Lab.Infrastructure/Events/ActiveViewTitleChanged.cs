﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// This event is fired when any active view wants to update heading Title.
    /// Payload is a string that contains new title
    /// </summary>
    public class ActiveViewTitleChanged : CompositePresentationEvent<string> {}
}
