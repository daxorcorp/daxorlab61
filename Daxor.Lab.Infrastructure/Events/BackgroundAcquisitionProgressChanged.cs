﻿using Daxor.Lab.Infrastructure.Events.Payloads;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// Background Progress Changed Aggregate Event
    /// </summary>
    public class BackgroundAcquisitionProgressChanged : CompositePresentationEvent<BackgroundAcquisitionProgressPayload> {}
}
