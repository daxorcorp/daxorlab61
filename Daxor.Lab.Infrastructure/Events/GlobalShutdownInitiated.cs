﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    public class GlobalShutdownInitiated : CompositePresentationEvent<object> {}
}
