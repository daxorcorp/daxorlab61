﻿using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Infrastructure.Events
{
    /// <summary>
    /// Fired when module is disabled. Cannot navigate to.
    /// </summary>
    public class AppModuleDisabled : CompositePresentationEvent<string> {}
}
