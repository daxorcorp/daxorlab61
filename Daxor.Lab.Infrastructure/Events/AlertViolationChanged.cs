﻿using Microsoft.Practices.Composite.Presentation.Events;
using Daxor.Lab.Infrastructure.Alerts;

namespace Daxor.Lab.Infrastructure.Events
{
    public class AlertViolationChanged : CompositePresentationEvent<AlertViolationPayload> { }
}
