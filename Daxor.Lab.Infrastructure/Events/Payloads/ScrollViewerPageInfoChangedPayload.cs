﻿namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public class ScrollViewerPageInfoChangedPayload
    {
        public ScrollViewerPageInfoChangedPayload(int itemCount, int totalItemCount, double scrollViewerExtentHeight,
            double scrollViewerViewportHeight, double scrollViewerVerticalOffset, string appModuleId)
        {
            ItemCount = itemCount;
            TotalItemCount = totalItemCount;
            ScrollViewerExtentHeight = scrollViewerExtentHeight;
            ScrollViewerViewportHeight = scrollViewerViewportHeight;
            ScrollViewerVerticalOffset = scrollViewerVerticalOffset;
            AppModuleId = appModuleId;
        }

        public int ItemCount { get; set; }
        public int TotalItemCount { get; set; }
        public double ScrollViewerExtentHeight { get; set; }
        public double ScrollViewerViewportHeight { get; set; }
        public double ScrollViewerVerticalOffset { get; set; }
        public string AppModuleId { get; set; }
    }
}