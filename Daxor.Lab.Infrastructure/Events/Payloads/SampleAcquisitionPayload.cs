﻿using System;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public class SampleAcquisitionPayload
    {
        public SampleAcquisitionPayload(ISample sample, Guid testId)
        {
            Sample = sample;
            TestId = testId;
        }

        public ISample Sample { get; private set; }
        public Guid TestId { get; private set; }
    }
}
