﻿namespace Daxor.Lab.Infrastructure.Events.Payloads
{
	public static class AlertResolutionPayload
	{
		public static readonly string ContaminationQCAlert = "Contamination QC Due";
		public static readonly string FullQCAlert = "Full QC Due";
		public static readonly string StandardsQCAlert = "Standards QC Due";
		public static readonly string LinearityQCAlert = "Linearity QC Due";
	}
}
