﻿
namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public class SampleChangerErrorOccurredPayload
    {
        public string Message { get; set; }

        public int ErrorCode { get; set; }
    }
}
