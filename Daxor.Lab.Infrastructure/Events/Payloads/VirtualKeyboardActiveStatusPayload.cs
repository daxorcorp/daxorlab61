﻿namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    /// <summary>
	/// Event payload corresponding to the VirtualKeyboardActiveStatusChanged event.
	/// </summary>
	public class VirtualKeyboardActiveStatusPayload
	{
		#region Properties

		/// <summary>
		/// Gets or sets the keyboard ID of the virtual keyboard
		/// </summary>
		// ReSharper disable once InconsistentNaming
		public object KeyboardID { get; private set; }
		
		/// <summary>
		/// Gets or sets whether the virtual keyboard is active nor not.
		/// </summary>
		public bool IsActive { get; private set; }

		#endregion

		#region Ctor

		/// <summary>
		/// Creates a new VirtualKeyboardActiveStatusPayload instance with the specified
		/// keyboard ID and active state.
		/// </summary>
		/// <param name="keyboardId">Unique identifier for the virtual keyboard whose 
		/// status is changing</param>
		/// <param name="isActive">Status of the virtual keyboard
		/// (i.e., activated/deactivated)</param>
		public VirtualKeyboardActiveStatusPayload(object keyboardId, bool isActive)
		{
			KeyboardID = keyboardId;
			IsActive = isActive;
		}

		#endregion
	}
}
