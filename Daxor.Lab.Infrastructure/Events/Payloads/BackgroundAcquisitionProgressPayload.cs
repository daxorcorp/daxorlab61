﻿using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public class BackgroundAcquisitionProgressPayload
    {
        public BackgroundAcquisitionProgressPayload(int percentCompleted, int timeElapsedInSeconds, int totalTaskTimeInSeconds,
            BackgroundAcquisitionServiceStatus status, int currentCpm, int? previousCpm,
            bool isActive, bool isCurrentCpmTooHigh, bool wasGoodBackgroundAttained)
        {
            PercentageCompleted = percentCompleted;
            TimeElapsedInSeconds = timeElapsedInSeconds;
            TotalTaskTimeInSeconds = totalTaskTimeInSeconds;
            Status = status;
            CurrentCpm = currentCpm;
            PreviousCpm = previousCpm;
            IsActive = isActive;
            IsCurrentCpmTooHigh = isCurrentCpmTooHigh;
            WasGoodBackgroundAttained = wasGoodBackgroundAttained;
        }

        public int PercentageCompleted { get; private set; }
        public int TotalTaskTimeInSeconds { get; private set; }
        public int TimeElapsedInSeconds { get; private set; }
        
        public BackgroundAcquisitionServiceStatus Status { get; private set; }

        public int CurrentCpm { get; private set; }
        public int? PreviousCpm { get; private set; }

        public bool IsActive { get; private set; }
        public bool IsCurrentCpmTooHigh { get; private set; }
        public bool WasGoodBackgroundAttained { get; private set; }
    }
}
