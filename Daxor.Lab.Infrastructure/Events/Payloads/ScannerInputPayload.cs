﻿namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public class ScannerInputPayload
    {
        public bool IsDataMatrix
        {
            get;
            private set;
        }
        public string DecodedValue
        {
            get;
            private set;
        }

        public ScannerInputPayload(bool isDataMatrix, string decodedValue)
        {
            IsDataMatrix = isDataMatrix;
            DecodedValue = decodedValue;
        }
    }
}