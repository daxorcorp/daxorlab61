﻿
namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public interface IWorkProgressPayload
    {
        int PercentageCompleted { get; }
        object ProgressContext { get; }
        int TotalTaskTimeInSeconds { get; }
    }
}
