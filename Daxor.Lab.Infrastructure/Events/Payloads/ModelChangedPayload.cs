﻿namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    /// <summary>
    /// ModelInfo contains targetAppModule name 
    /// and a NewModel.
    /// </summary>
    public class ModelChangedPayload
    {
        public ModelChangedPayload(string targetAppModuleKey, object newModel)
        {
            TargetAppModuleKey = targetAppModuleKey;
            NewModel = newModel;
        }

        public object NewModel
        {
            private set;
            get;
        }

        public string TargetAppModuleKey
        {
            private set;
            get;
        }
    }
}