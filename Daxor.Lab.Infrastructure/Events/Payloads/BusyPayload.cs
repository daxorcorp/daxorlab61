﻿using System;

namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public class BusyPayload
    {
        public BusyPayload(bool isBusy)
        {
            IsBusy = isBusy;
            IsIndetermineState = true;
        }
        public BusyPayload(bool isBusy, string message) : this(isBusy)
        {
            Message = message;
        }
        public bool IsBusy { get; private set; }
        public String Message { get; set; }

        public bool IsIndetermineState { get; set; }
        public double PercentageCompleted { get; set; }
    }
}