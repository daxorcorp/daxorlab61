﻿
namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public class SampleChangerPositionChangeCompletedPayload
    {
        public int NewPosition { get; set; }
    }
}
