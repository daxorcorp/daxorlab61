﻿namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public class ActivationChangedPayload
    {
        public ActivationChangedPayload(string partName, bool isActivated)
        {
            AppPartName = partName;
            IsActivated = isActivated;
        }

        public string AppPartName
        {
            get;
            private set;
        }

        public bool IsActivated
        {
            get;
            private set;
        }
    }
}