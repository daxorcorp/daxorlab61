﻿namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public enum ScrollViewerScrollChangedEventPayload
    {
        ScrollTop,
        ScrollPageUp,
        ScrollPageDown,
        ScrollBottom,
    }
}