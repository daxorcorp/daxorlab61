﻿
namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public class SampleChangerPositionChangeStartedPayload
    {
        public int OldPosition { get; set; }
        public int NewPosition { get; set; }
    }
}
