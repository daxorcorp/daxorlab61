﻿
namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public class SampleChangerPositionSeekCompletedPayload
    {
        public int CurrentPosition { get; set; }
    }
}
