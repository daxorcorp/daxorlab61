﻿
namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public class SampleChangerPositionSeekCancelledPayload
    {
        public int CurrentPosition { get; set; }
        public int GoalPosition { get; set; }
    }
}
