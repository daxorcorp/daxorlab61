﻿namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public enum ScannerConnectionChangedPayload
    {
        Connected,
        Disconnected
    }
}