﻿
namespace Daxor.Lab.Infrastructure.Events.Payloads
{
    public class SampleChangerPositionSeekStartedPayload
    {
        public int CurrentPosition { get; set; }
        public int GoalPosition { get; set; }
    }
}
