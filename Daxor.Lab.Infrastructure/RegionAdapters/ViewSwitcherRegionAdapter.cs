﻿using System;
using System.Windows;
using Daxor.Lab.Infrastructure.Controls;
using Microsoft.Practices.Composite.Presentation.Regions;
using Microsoft.Practices.Composite.Regions;

namespace Daxor.Lab.Infrastructure.RegionAdapters
{
    /// <summary>
    /// ViewSwitcher region adapter.
    /// </summary>
    public class ViewSwitcherRegionAdapter : RegionAdapterBase<ViewSwitcher>
    {
        #region Ctor
        public ViewSwitcherRegionAdapter(IRegionBehaviorFactory behaviorFactory)
            : base(behaviorFactory)
        {
        }
        #endregion

        #region Overrides
        protected override void Adapt(IRegion region, ViewSwitcher regionTarget)
        {
            bool itemsSourceIsSet = regionTarget.ItemsSource != null;
            if (itemsSourceIsSet)
            {
                throw new InvalidOperationException("ItemsControlHasItemsSourceException");
            }

            region.Views.CollectionChanged += (s, e) =>
            {
                // Add items
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                {
                    foreach (FrameworkElement element in e.NewItems)
                    {
                        regionTarget.Items.Add(element);
                    }
                }

                // Remove items
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
                {
                    foreach (FrameworkElement element in e.OldItems)
                    {
                        regionTarget.Items.Remove(element);
                    }

                    //If all views were removed from the region, let's set selectedItem to null
                    if (!regionTarget.HasItems)
                    {
                        regionTarget.SelectedItem = null;
                        regionTarget.IsAnimated = false;
                        regionTarget.SelectedIndex = 0;
                        regionTarget.IsAnimated = true;
                    }
                }
            };

            //region
            // Active View Change
            region.ActiveViews.CollectionChanged += (v, e) =>
            {
                // Active view has been activated
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                {
                    FrameworkElement view = (FrameworkElement)e.NewItems[0];
                    regionTarget.SelectedItem = view;  
                }


            };
        }
        protected override IRegion CreateRegion()
        {
            return new SingleActiveRegion();
        }
        #endregion
    }
}
