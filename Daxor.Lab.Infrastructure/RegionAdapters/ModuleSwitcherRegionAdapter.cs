﻿using System;
using System.Diagnostics;
using System.Windows;
using Daxor.Lab.Infrastructure.Controls;
using Microsoft.Practices.Composite.Presentation.Regions;
using Microsoft.Practices.Composite.Regions;

namespace Daxor.Lab.Infrastructure.RegionAdapters
{
    /// <summary>
    /// ModuleSwitcher region adapter.
    /// </summary>
    public class ModuleSwitcherRegionAdapter : RegionAdapterBase<ModuleSwitcher>
    {
        #region Ctor
        public ModuleSwitcherRegionAdapter(IRegionBehaviorFactory behaviorFactory)
            : base(behaviorFactory)
        {
        }
        #endregion

        #region Overrides
        protected override void Adapt(IRegion region, ModuleSwitcher regionTarget)
        {
            bool itemsSourceIsSet = regionTarget.ItemsSource != null;
            if (itemsSourceIsSet)
            {
                throw new InvalidOperationException("ItemsControlHasItemsSourceException");
            }

            region.Views.CollectionChanged += (s, e) =>
            {
                // Add
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                {
                    foreach (FrameworkElement element in e.NewItems)
                    {
                        Debug.Print("REGION_ADAPTER: Adding |{0}| to region.Views", element.GetType().Name);
                        regionTarget.Items.Add(element);
                    }
                }

                // Remove
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
                {
                    foreach (FrameworkElement element in e.OldItems)
                    {
                        Debug.Print("REGION_ADAPTER: Removing |{0}| from region.Views", element.GetType().Name);

                        regionTarget.Items.Remove(element);
                    }
                }
            };

            // Active View Change
            region.ActiveViews.CollectionChanged += (v, e) =>
            {
                // Active View has been activated
                if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                {
                    FrameworkElement view = (FrameworkElement)e.NewItems[0];
                    regionTarget.Navigate(view);
                    Debug.Print("REGION_ADAPTER: {0} has been actived.", view.GetType().Name);
                }
            };
        }
        protected override IRegion CreateRegion()
        {
            return new SingleActiveRegion();
        }
        #endregion
    }
}
