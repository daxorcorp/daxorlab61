﻿using System;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Test_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        private static Test<ISample> _stubTest;

        [TestInitialize]
        public void TestsInitialize()
        {
            _stubTest = new DummyTest();
        }

        #region Ctor

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_default_status_is_pending()
        {
            Assert.AreEqual(TestStatus.Pending, _stubTest.Status,
                "The default test status should be pending, not {0}.",
                _stubTest.Status);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_default_timestamp_is_the_current_time()
        {
            TimeSpan timeDiff = DateTime.Now - _stubTest.Date;

            Assert.IsTrue(timeDiff.TotalMilliseconds < 500,
                "The difference between expected and actual date ({0} ms) is too long.",
                timeDiff.TotalMilliseconds);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_it_is_not_transient()
        {
            Assert.IsFalse(_stubTest.IsTransient(),
                "TestBase is an entity and should have a unique ID (i.e., not be transient). It's ID was {0}.",
                _stubTest.InternalId);
        }

        #endregion
    }
    // ReSharper restore InconsistentNaming
}
