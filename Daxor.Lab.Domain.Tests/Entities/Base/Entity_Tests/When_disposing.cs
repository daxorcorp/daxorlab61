﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Base.Entity_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_disposing
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void When_disposing_then_disposing_defers_to_the_virtual_Dispose_handler()
        {
            var obsEntity = new TestEntity {OnDisposeWasCalled = false};

            obsEntity.Dispose();

            Assert.IsTrue(obsEntity.OnDisposeWasCalled, "Disposing did not defer to the OnDispose() handler.");
        }
    }
    // ReSharper restore InconsistentNaming
}
