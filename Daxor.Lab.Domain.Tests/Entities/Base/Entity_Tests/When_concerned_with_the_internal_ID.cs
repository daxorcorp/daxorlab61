﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Base.Entity_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_concerned_with_the_internal_ID
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_initial_internal_ID_is_the_empty_GUID()
        {
            var entity = new TestEntity();

            Assert.AreEqual(Guid.Empty, entity.InternalId,
                "An abstract entity should have a default internal ID of Guid.Empty instead of '{0}'",
                entity.InternalId);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_entity_has_no_internal_ID_then_its_considered_transient()
        {
            var entity = new TestEntity {InternalId = Guid.Empty};
            Assert.IsTrue(entity.IsTransient(),
                "An entity with no ID should be considered transient.");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_entity_has_an_internal_ID_then_its_not_considered_transient()
        {
            var entity = new TestEntity {InternalId = Guid.NewGuid()};
            Assert.IsFalse(entity.IsTransient(),
                "An entity with an ID ('{0}') should not be considered transient.", entity.InternalId);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_change_handler_is_called_after_the_internal_ID_is_set()
        {
            var entity = new TestEntity {ChangedHandlerCalled = false, InternalId = Guid.NewGuid()};

            Assert.IsTrue(entity.ChangedHandlerCalled,
                "The callback was not called when the entity's ID changed.");
        }
    }
    // ReSharper restore InconsistentNaming
}
