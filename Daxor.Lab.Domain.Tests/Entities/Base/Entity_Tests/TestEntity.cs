using System.Collections.Generic;
using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.Domain.Tests.Entities.Base.Entity_Tests
{
    public class TestEntity : Entity
    {
        #region Fields

        public bool ChangedHandlerCalled;
        public bool OnDisposeWasCalled;

        #endregion

        #region Ctor

        public TestEntity(bool throwExceptionOnInvalidPropertyName = false)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            ThrowExceptionOnInvalidPropertyName = throwExceptionOnInvalidPropertyName;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Properties

        public string TestProperty { get; set; }

        public string TestProperty2 { get; set; }

        protected override bool ThrowExceptionOnInvalidPropertyName { get; set; }

        public List<string> FirePropertyChangedPropertyNameArguments = new List<string>();

        #endregion

        #region Methods that expose protected behavior

        public bool TestSetValue<T>(ref T propField, string propName, T newValue)
        {
            return base.SetValue(ref propField, propName, newValue);
        }
        
        public void TestFirePropertyChanged(string propertyName)
        {
            FirePropertyChangedPropertyNameArguments.Add(propertyName);
        }

        public void TestFirePropertyChanged(params string[] propertyNames)
        {
            base.FirePropertyChanged(propertyNames);
        }

        #endregion

        #region Overridden protected methods

        protected override void FirePropertyChanged(string propertyName)
        {
            if (ThrowExceptionOnInvalidPropertyName)
                base.FirePropertyChanged(propertyName);  // Run the logic that calls VerifyPropertyName()
            else
                TestFirePropertyChanged(propertyName);
        }
        
        protected override void OnDispose()
        {
            OnDisposeWasCalled = true;
        }

        protected override void OnIdChanged()
        {
            ChangedHandlerCalled = true;
        }

        #endregion
    }
}