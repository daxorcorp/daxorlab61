﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Base.Entity_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_a_property_value
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void And_given_the_same_value_then_SetValue_returns_false()
        {
            var obsEntity = new TestEntity {TestProperty = "Whatever"};

            var propertyField = "Whatever";
            var wasSuccessful = obsEntity.TestSetValue(ref propertyField, "TestProperty", "Whatever");

            Assert.IsFalse(wasSuccessful, "SetValue<T> didn't return false when the field would be set to the same value.");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_given_a_new_value_then_SetValue_returns_true()
        {
            var obsEntity = new TestEntity {TestProperty = "Whatever"};

            var propertyField = "Whatever";
            var wasSuccessful = obsEntity.TestSetValue(ref propertyField, "TestProperty", "A new value");

            Assert.IsTrue(wasSuccessful, "SetValue<T> didn't return true when the field would be set to a new value.");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_given_a_new_value_then_SetValue_sets_the_property_field()
        {
            var obsEntity = new TestEntity {TestProperty = "Whatever"};

            var propertyField = "Whatever";
            const string newValue = "A new value";
            obsEntity.TestSetValue(ref propertyField, "TestProperty", newValue);

            Assert.AreEqual(newValue, propertyField, "SetValue<T> didn't set the property field to a new value.");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_given_a_new_value_then_SetValue_fires_PropertyChanged_correctly()
        {
            // (Rhino Mocks doesn't help here because we can't mock a protected method
            // that calls another protected method.)

            var obsEntity = new TestEntity {TestProperty = "Whatever"};

            var propertyField = "Whatever";
            const string newValue = "A new value";
            const string propertyName = "TestProperty";

            obsEntity.TestSetValue(ref propertyField, propertyName, newValue);

            Assert.AreEqual(propertyName, obsEntity.FirePropertyChangedPropertyNameArguments[0],
                "PropertyChanged was not fired, or wasn't called with the correct argument");
        }

#if DEBUG
        [TestMethod]
        [TestCategory("Domain")]
        public void When_firing_PropertyChanged_then_the_property_name_is_verified()
        {
            // Mocking can't help here because VerifyPropertyName() isn't virtual.
            // This just passes in a bogus property name which should throw an exception.

            var obsEntity = new TestEntity(true);
            var propertyField = "Whatever";
            const string bogusPropertyName = "PropertyThatDoesntExist";
            try
            {
                obsEntity.TestSetValue(ref propertyField, bogusPropertyName, "A new value");
            }
            catch
            {
                return;  // Pass
            }

            Assert.Fail("No exception was thrown because of an invalid property name.");
        }
#endif

        [TestMethod]
        [TestCategory("Domain")]
        public void And_PropertyChanged_is_fired_then_the_handler_is_called_correctly()
        {
            var obsEntity = new TestEntity(true);
            var propertyField = "Whatever";
            const string expectedPropertyName = "TestProperty";
            obsEntity.PropertyChanged += obsEntity_PropertyChanged;
            _propertyChangedHandlerUsedCorrectly = false;
            _expectedPropertyChangedHandlerEventArg = expectedPropertyName;

            obsEntity.TestSetValue(ref propertyField, expectedPropertyName, "A new value");

            Assert.IsTrue(_propertyChangedHandlerUsedCorrectly,
                "The PropertyChangedEventHandler was not invoked or was not invoked with the correct event args.");
        }

#if DEBUG
        [TestMethod]
        [TestCategory("Domain")]
        public void And_firing_PropertyChanged_on_multiple_properties_then_the_property_names_are_verified()
        {
            // Mocking can't help here because VerifyPropertyName() isn't virtual.
            // This just passes in a bogus property name which should throw an exception.

            var obsEntity = new TestEntity(true);
            const string validPropertyName = "TestProperty";
            const string bogusPropertyName = "PropertyThatDoesntExist";
            try
            {
                obsEntity.TestFirePropertyChanged(validPropertyName, bogusPropertyName);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, bogusPropertyName,
                    "The exception thrown because of an invalid property name doesn't contain the invalid property name.");
                return;  // Pass
            }

            Assert.Fail("No exception was thrown because of an invalid property name.");
        }
#endif

        [TestMethod]
        [TestCategory("Domain")]
        public void When_firing_PropertyChanged_on_multiple_properties_then_FirePropertyChanged_is_called_for_each_property()
        {
            var obsEntity = new TestEntity {TestProperty = "Whatever", TestProperty2 = "Whatever"};

            const string validPropertyName = "TestProperty";
            const string anotherValidPropertyName = "TestProperty2";

            obsEntity.TestFirePropertyChanged(validPropertyName, anotherValidPropertyName);

            var expectedProperties = new List<string> { validPropertyName, anotherValidPropertyName };
            CollectionAssert.AreEqual(expectedProperties.ToArray(), obsEntity.FirePropertyChangedPropertyNameArguments.ToArray(),
                "The list of properties that have changed ({0}) isn't correct ({1}).",
                UnitTestHelper.ListToString(obsEntity.FirePropertyChangedPropertyNameArguments),
                UnitTestHelper.ListToString(expectedProperties));
        }

        #region Helper methods

        private bool _propertyChangedHandlerUsedCorrectly;
        private string _expectedPropertyChangedHandlerEventArg;
        internal void obsEntity_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var isSenderNotNull = sender != null;
            var isPropertyNameGiven = e.PropertyName == _expectedPropertyChangedHandlerEventArg;

            _propertyChangedHandlerUsedCorrectly = isSenderNotNull && isPropertyNameGiven;
        }

        #endregion
    }
    // ReSharper restore InconsistentNaming
}
