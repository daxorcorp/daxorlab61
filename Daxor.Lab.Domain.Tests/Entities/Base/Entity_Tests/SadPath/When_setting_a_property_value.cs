﻿using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Base.Entity_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_a_property_value
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void And_firing_PropertyChanged_on_multiple_properties_then_an_exception_is_thrown_for_an_null_list_of_properties()
        {
            var obsEntity = new TestEntity(true);
            string[] nullArray = null;

            // ReSharper disable ExpressionIsAlwaysNull (have to pass a variable, otherwise the compiler doesn't know which overload to use)
            AssertEx.ThrowsArgumentNullException(()=>obsEntity.TestFirePropertyChanged(nullArray), "propertyNames");
            // ReSharper restore ExpressionIsAlwaysNull
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_firing_PropertyChanged_on_multiple_properties_then_an_exception_is_thrown_for_an_empty_list_of_properties()
        {
            var obsEntity = new TestEntity(true);
            var emptyStringArray = new string[0];
            AssertEx.Throws<ArgumentOutOfRangeException>(()=>obsEntity.TestFirePropertyChanged(emptyStringArray));
        }
    }
    // ReSharper restore InconsistentNaming
}
