﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Base.Entity_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_verifying_a_property_name
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_property_name_is_correct_then_no_exception_is_thrown()
        {
            var obsEntity = new TestEntity {TestProperty = "Whatever"};

            string propertyField = "Whatever";
            obsEntity.TestSetValue(ref propertyField, "TestProperty", "A new value");

            // If the code-under-test doesn't behave as expected, an exception will be
            // thrown, or Debug.Fail() will be called -- either of which will cause this
            // test to not pass.
        }

#if DEBUG
        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_property_name_is_invalid_then_an_exception_is_thrown()
        {
            var obsEntity = new TestEntity(true);
            string propertyField = "Whatever";
            const string bogusPropertyName = "PropertyThatDoesntExist";
            try
            {
                obsEntity.TestSetValue(ref propertyField, bogusPropertyName, "A new value");
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, bogusPropertyName,
                    "The exception thrown because of an invalid property name doesn't contain the invalid property name.");
                return;
            }

            Assert.Fail("No exception was thrown because of an invalid property name.");
        }
#endif
    }
    // ReSharper restore InconsistentNaming
}
