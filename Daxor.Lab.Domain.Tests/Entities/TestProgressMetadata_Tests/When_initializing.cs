﻿using Daxor.Lab.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.TestProgressMetadata_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_constructor_sets_the_app_module_key()
        {
            const string appModuleKey = "Whatever";
            var metadata = new TestProgressMetadata(appModuleKey);

            Assert.AreEqual(appModuleKey, metadata.AppModuleKey,
                "The AppModuleKey property didn't return ('{0}') what was passed in via the ctor",
                metadata.AppModuleKey);
        }
    }
    // ReSharper restore InconsistentNaming
}
