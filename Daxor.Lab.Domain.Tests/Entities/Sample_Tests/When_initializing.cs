﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Sample_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_sample_entity_is_not_transient()
        {
            var sample = new TestSample();
            Assert.IsFalse(sample.IsTransient(),
                "Sample is an entity and should have a unique ID (i.e., not be transient).");
        }
        
        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_spectrum_is_initialized()
        {
            var sample = new TestSample();
            Assert.IsNotNull(sample.Spectrum);
        }
    }
    // ReSharper restore InconsistentNaming
}
