﻿using System;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Sample_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_properties
    {
        private static Sample _stubSample;

        [TestInitialize]
        public void TestsMethodInitialize()
        {
            _stubSample = new TestSample();
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_display_name_can_be_set()
        {
            const string expectedName = "The Sample";

            _stubSample.DisplayName = expectedName;

            Assert.AreEqual(expectedName, _stubSample.DisplayName,
                "The expected display name ({0}) didn't match the actual display name ({1}).",
                expectedName, _stubSample.DisplayName);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_display_name_fires_PropertyChanged()
        {
            var propertyChangedWasFired = false;
            _stubSample.PropertyChanged += (sender, args) => { propertyChangedWasFired = args.PropertyName == "DisplayName"; };
            
            const string expectedName = "The Sample";

            _stubSample.DisplayName = expectedName;

            Assert.IsTrue(propertyChangedWasFired, "Setting the display name did not fire the PropertyChanged event");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_execution_status_can_be_set()
        {
            const SampleExecutionStatus expectedStatus = SampleExecutionStatus.Completed;

            _stubSample.ExecutionStatus = expectedStatus;

            Assert.AreEqual(expectedStatus, _stubSample.ExecutionStatus,
                "The expected execution status ({0}) didn't match the actual execution status ({1}).",
                expectedStatus, _stubSample.ExecutionStatus);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_execution_status_fires_PropertyChanged()
        {
            var propertyChangedWasFired = false;
            _stubSample.PropertyChanged += (sender, args) => { propertyChangedWasFired = args.PropertyName == "ExecutionStatus"; };

            const SampleExecutionStatus expectedStatus = SampleExecutionStatus.Completed;

            _stubSample.ExecutionStatus = expectedStatus;

            Assert.IsTrue(propertyChangedWasFired, "Setting the execution status did not fire the PropertyChanged event");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_position_can_be_set()
        {
            const int expectedPosition = 123;

            _stubSample.Position = expectedPosition;

            Assert.AreEqual(expectedPosition, _stubSample.Position,
                "The expected position ({0}) didn't match the actual position ({1}).",
                expectedPosition, _stubSample.Position);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_position_fires_PropertyChanged()
        {
            var propertyChangedWasFired = false;
            _stubSample.PropertyChanged += (sender, args) => { propertyChangedWasFired = args.PropertyName == "Position"; };

            const int expectedPosition = 123;

            _stubSample.Position = expectedPosition;

            Assert.IsTrue(propertyChangedWasFired, "Setting the position did not fire the PropertyChanged event");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_spectrum_array_can_be_set()
        {
            var expectedSpectrum = new UInt32[10];

            _stubSample.Spectrum.SpectrumArray = expectedSpectrum;

            Assert.AreEqual(expectedSpectrum, _stubSample.Spectrum.SpectrumArray,
                "The expected spectrum array ({0}) didn't match the actual spectrum array ({1}).",
                UnitTestHelper.ListToString(expectedSpectrum),
                UnitTestHelper.ListToString(_stubSample.Spectrum.SpectrumArray));
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_spectrum_array_fires_PropertyChanged()
        {
            var propertyChangedWasFired = false;
            _stubSample.Spectrum.PropertyChanged += (sender, args) => { propertyChangedWasFired = args.PropertyName == "SpectrumArray"; };

            var expectedSpectrum = new UInt32[10];

            _stubSample.Spectrum.SpectrumArray = expectedSpectrum;

            Assert.IsTrue(propertyChangedWasFired, "Setting the spectrum did not fire the PropertyChanged event");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_counting_flag_can_be_set()
        {
            const bool expectedIsCounting = true;

            _stubSample.IsCounting = expectedIsCounting;

            Assert.IsTrue(_stubSample.IsCounting,
                "The expected is-counting flag ({0}) didn't match the actual is-counting flag ({1}).",
                expectedIsCounting, _stubSample.IsCounting);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_counting_flag_fires_PropertyChanged()
        {
            var propertyChangedWasFired = false;
            _stubSample.PropertyChanged += (sender, args) => { propertyChangedWasFired = args.PropertyName == "IsCounting"; };

            const bool expectedIsCounting = true;

            _stubSample.IsCounting = expectedIsCounting;

            Assert.IsTrue(propertyChangedWasFired, "Setting the is-counting flag did not fire the PropertyChanged event");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_spectrum_can_be_set()
        {
            var expectedSpectrum = new Spectrum();
            _stubSample.Spectrum = expectedSpectrum;

            Assert.AreEqual(expectedSpectrum, _stubSample.Spectrum);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_spectrum_fires_PropertyChanged()
        {
            var propertyChangedWasFired = false;
            _stubSample.PropertyChanged += (sender, args) => { propertyChangedWasFired = args.PropertyName == "Spectrum"; };

            _stubSample.Spectrum = new Spectrum {RealTimeInSeconds = 45};

            Assert.IsTrue(propertyChangedWasFired, "Setting the Spectrum instance did not fire the PropertyChanged event");
        }
    }
    // ReSharper restore InconsistentNaming
}
