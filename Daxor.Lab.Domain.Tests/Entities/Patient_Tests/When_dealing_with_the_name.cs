﻿using System.Collections.Generic;
using System.ComponentModel;
using Daxor.Lab.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Patient_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_dealing_with_the_name
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_first_name_can_be_set()
        {
            const string expectedName = "Geoff";
            var patient = new Patient { FirstName = expectedName };
            Assert.AreEqual(expectedName, patient.FirstName,
                "The first name (" + patient.FirstName + ") doesn't match the expected first name ("
                + expectedName + ").");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_first_name_fires_PropertyChanged_for_the_first_name_and_the_full_name()
        {
            var patient = new Patient();
            _changedProperties = new List<string>();

            patient.PropertyChanged += patient_PropertyChanged;
            patient.FirstName = "Geoff";

            var expectedChangedProperties = new List<string> { "FirstName", "FullName" };
            CollectionAssert.AreEqual(expectedChangedProperties, _changedProperties,
                "Observed changed properties (" + UnitTestHelper.ListToString(_changedProperties) +
                ") does not match expected changed properties (" +
                UnitTestHelper.ListToString(expectedChangedProperties) + ").");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_last_name_fires_PropertyChanged_for_the_last_name_and_the_full_name()
        {
            var patient = new Patient();
            _changedProperties = new List<string>();

            patient.PropertyChanged += patient_PropertyChanged;
            patient.LastName = "Mazeroff";

            var expectedChangedProperties = new List<string> { "LastName", "FullName" };
            CollectionAssert.AreEqual(expectedChangedProperties, _changedProperties,
                "Observed changed properties (" + UnitTestHelper.ListToString(_changedProperties) +
                ") does not match expected changed properties (" +
                UnitTestHelper.ListToString(expectedChangedProperties) + ").");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_middle_name_fires_PropertyChanged_for_the_middle_name_and_the_full_name()
        {
            var patient = new Patient();
            _changedProperties = new List<string>();

            patient.PropertyChanged += patient_PropertyChanged;
            patient.MiddleName = "Alan";

            var expectedChangedProperties = new List<string> { "MiddleName", "FullName" };
            CollectionAssert.AreEqual(expectedChangedProperties, _changedProperties,
                "Observed changed properties (" + UnitTestHelper.ListToString(_changedProperties) +
                ") does not match expected changed properties (" +
                UnitTestHelper.ListToString(expectedChangedProperties) + ").");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_first_middle_and_last_names_are_present_then_the_full_name_is_correct()
        {
            var patient = new Patient { FirstName = "Geoff", MiddleName = "Alan", LastName = "Mazeroff" };
            const string expectedName = "Mazeroff, Geoff Alan";

            Assert.AreEqual(expectedName, patient.FullName,
                "The patient's full name was given as '{0}' instead of '{1}'.",
                patient.FullName, expectedName);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_first_and_last_names_are_present_then_the_full_name_is_correct()
        {
            var patient = new Patient { FirstName = "Geoff", LastName = "Mazeroff" };
            const string expectedName = "Mazeroff, Geoff";

            Assert.AreEqual(expectedName, patient.FullName,
                "The patient's full name was given as '{0}' instead of '{1}'.",
                patient.FullName, expectedName);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_only_the_first_name_is_present_then_the_full_name_is_correct()
        {
            var patient = new Patient { FirstName = "Geoff" };
            const string expectedName = "Geoff";

            Assert.AreEqual(expectedName, patient.FullName,
                "The patient's full name was given as '{0}' instead of '{1}'.",
                patient.FullName, expectedName);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_only_the_last_name_is_present_then_the_full_name_is_correct()
        {
            var patient = new Patient { LastName = "Mazeroff" };
            const string expectedName = "Mazeroff";

            Assert.AreEqual(expectedName, patient.FullName,
                "The patient's full name was given as '{0}' instead of '{1}'.",
                patient.FullName, expectedName);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_no_name_parts_are_present_then_the_full_name_is_empty()
        {
            var patient = new Patient();

            Assert.AreEqual(string.Empty, patient.FullName,
                "The patient's full name was given as '{0}' instead of an empty string.",
                patient.FullName);
        }

        #region PropertyChanged helpers

        private List<string> _changedProperties;
        private void patient_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            _changedProperties.Add(e.PropertyName);
        }

        #endregion
    }
    // ReSharper restore InconsistentNaming
}
