﻿using System;
using Daxor.Lab.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Patient_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_the_age
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_date_difference_is_less_than_an_exact_year_then_the_age_is_computed_correctly()
        {
            var dateOfBirth = new DateTime(2000, 1, 1);
            var endDate = new DateTime(2000, 12, 31);
            var patient = new Patient { DateOfBirth = dateOfBirth };

            var ageInYears = patient.GetAgeInYears(endDate);

            Assert.AreEqual(0, ageInYears,
                "Patient born on {0} did not yield the correct age based on the end date of {1}.",
                dateOfBirth, endDate);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_end_dates_month_is_smaller_with_less_than_exact_year_then_the_age_is_computed_correctly()
        {
            var dateOfBirth = new DateTime(2000, 8, 1);
            var endDate = new DateTime(2001, 5, 1);
            var patient = new Patient { DateOfBirth = dateOfBirth };

            var ageInYears = patient.GetAgeInYears(endDate);

            Assert.AreEqual(0, ageInYears,
                "Patient born on {0} did not yield the correct age based on the end date of {1}.",
                dateOfBirth, endDate);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_date_difference_is_an_exact_year_then_the_age_is_computed_correctly()
        {
            var dateOfBirth = new DateTime(2000, 1, 1);
            var endDate = new DateTime(2001, 1, 1);
            var patient = new Patient { DateOfBirth = dateOfBirth };

            var ageInYears = patient.GetAgeInYears(endDate);

            Assert.AreEqual(1, ageInYears,
                "Patient born on {0} did not yield the correct age based on the end date of {1}.",
                dateOfBirth, endDate);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_date_difference_is_more_than_an_exact_year_then_the_age_is_computed_correctly()
        {
            var dateOfBirth = new DateTime(2000, 1, 1);
            var endDate = new DateTime(2001, 2, 2);
            var patient = new Patient { DateOfBirth = dateOfBirth };

            var ageInYears = patient.GetAgeInYears(endDate);

            Assert.AreEqual(1, ageInYears,
                "Patient born on {0} did not yield the correct age based on the end date of {1}.",
                dateOfBirth, endDate);
        }
    }
    // ReSharper restore InconsistentNaming
}
