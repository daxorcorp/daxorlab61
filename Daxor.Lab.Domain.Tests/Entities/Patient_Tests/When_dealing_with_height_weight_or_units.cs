﻿using System.Collections.Generic;
using System.ComponentModel;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Patient_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_dealing_with_height_weight_or_units
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_height_in_cm_can_be_set()
        {
            const double expectedHeight = 1.23d;
            var patient = new Patient { HeightInCm = expectedHeight };
            Assert.AreEqual(expectedHeight, patient.HeightInCm,
                "The height (" + patient.Gender + " cm) doesn't match the expected height ("
                + expectedHeight + " cm).");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_height_fires_PropertyChanged_for_the_height_and_the_height_in_current_measurement_system()
        {
            var patient = new Patient();
            _changedProperties = new List<string>();

            patient.PropertyChanged += patient_PropertyChanged;
            patient.HeightInCm = 30;

            var expectedChangedProperties = new List<string> { "HeightInCm", "HeightInCurrentMeasurementSystem" };
            CollectionAssert.AreEqual(expectedChangedProperties, _changedProperties,
                "Observed changed properties (" + UnitTestHelper.ListToString(_changedProperties) +
                ") does not match expected changed properties (" +
                UnitTestHelper.ListToString(expectedChangedProperties) + ").");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_weight_fires_PropertyChanged_for_the_weight_and_the_weight_in_current_measurement_system()
        {
            var patient = new Patient();
            _changedProperties = new List<string>();

            patient.PropertyChanged += patient_PropertyChanged;
            patient.WeightInKg = 30;

            var expectedChangedProperties = new List<string> { "WeightInKg", "WeightInCurrentMeasurementSystem" };
            CollectionAssert.AreEqual(expectedChangedProperties, _changedProperties,
                "Observed changed properties (" + UnitTestHelper.ListToString(_changedProperties) +
                ") does not match expected changed properties (" +
                UnitTestHelper.ListToString(expectedChangedProperties) + ").");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_measurement_system_fires_PropertyChanged_for_measurement_system_and_the_height_and_the_weight()
        {
            var patient = new Patient();
            _changedProperties = new List<string>();

            patient.PropertyChanged += patient_PropertyChanged;
            patient.MeasurementSystem = MeasurementSystem.English;

            var expectedChangedProperties = new List<string> { "MeasurementSystem", 
                "HeightInCurrentMeasurementSystem", "WeightInCurrentMeasurementSystem" };
            CollectionAssert.AreEqual(expectedChangedProperties, _changedProperties,
                "Observed changed properties (" + UnitTestHelper.ListToString(_changedProperties) +
                ") does not match expected changed properties (" +
                UnitTestHelper.ListToString(expectedChangedProperties) + ").");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_in_English_then_the_height_in_current_measurement_system_is_correct()
        {
            var patient = new Patient { HeightInCm = 2.54, MeasurementSystem = MeasurementSystem.English };
            const double expectedHeightInInches = 1.0d;

            Assert.AreEqual(expectedHeightInInches, patient.HeightInCurrentMeasurementSystem,
                "The patient's height in inches was {0} instead of {1}.",
                patient.HeightInCurrentMeasurementSystem, expectedHeightInInches);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_in_metric_then_the_height_in_current_measurement_system_is_correct()
        {
            var patient = new Patient { HeightInCm = 2.54, MeasurementSystem = MeasurementSystem.Metric };
            const double expectedHeightInInches = 2.54d;

            Assert.AreEqual(expectedHeightInInches, patient.HeightInCurrentMeasurementSystem,
                "The patient's height in inches was {0} instead of {1}.",
                patient.HeightInCurrentMeasurementSystem, expectedHeightInInches);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_height_is_defined_then_the_height_in_inches_is_correct()
        {
            var patient = new Patient { HeightInCm = 2.54 };
            const double expectedHeightInInches = 1.0d;

            Assert.AreEqual(expectedHeightInInches, patient.HeightInInches,
                "The patient's height in inches was {0} instead of {1}.",
                patient.HeightInInches, expectedHeightInInches);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_in_English_then_the_weight_in_current_measurement_system_is_correct()
        {
            var patient = new Patient { WeightInKg = 0.45359237, MeasurementSystem = MeasurementSystem.English };
            const double expectedWeightInPounds = 1.0d;

            Assert.AreEqual(expectedWeightInPounds, patient.WeightInCurrentMeasurementSystem,
                "The patient's weight in pounds was {0} instead of {1}.",
                patient.WeightInCurrentMeasurementSystem, expectedWeightInPounds);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void When_in_metric_then_the_weight_in_current_measurement_system_is_correct()
        {
            var patient = new Patient { WeightInKg = 0.45359237, MeasurementSystem = MeasurementSystem.Metric };
            const double expectedWeightInKg = 0.45359237d;

            Assert.AreEqual(expectedWeightInKg, patient.WeightInCurrentMeasurementSystem,
                "The patient's weight in kg was {0} instead of {1}.",
                patient.WeightInCurrentMeasurementSystem, expectedWeightInKg);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_a_weight_is_defined_then_the_weight_in_pounds_is_correct()
        {
            var patient = new Patient { WeightInKg = 0.45359237 };
            const double expectedWeightInPounds = 1.0d;

            Assert.AreEqual(expectedWeightInPounds, patient.WeightInLb,
                "The patient's weight in pounds was {0} instead of {1}.",
                patient.WeightInLb, expectedWeightInPounds);
        }

        #region PropertyChanged helpers

        private List<string> _changedProperties;
        private void patient_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            _changedProperties.Add(e.PropertyName);
        }

        #endregion
    }
    // ReSharper restore InconsistentNaming
}
