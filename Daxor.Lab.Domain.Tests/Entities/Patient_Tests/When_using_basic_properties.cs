﻿using System;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Patient_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_basic_properties
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_date_of_birth_can_be_set()
        {
            var expectedDOB = DateTime.Today;
            var patient = new Patient { DateOfBirth = expectedDOB };
            Assert.AreEqual(expectedDOB, patient.DateOfBirth,
                "The date of birth (" + patient.DateOfBirth + ") doesn't match the expected date of birth ("
                + expectedDOB + ").");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_date_of_birth_fires_PropertyChanged()
        {
            var propertyChangedWasFired = false;
            var patient = new Patient();
            patient.PropertyChanged += (sender, args) => { propertyChangedWasFired = args.PropertyName == "DateOfBirth"; };
            patient.DateOfBirth = DateTime.Today;
            Assert.IsTrue(propertyChangedWasFired, "Setting the date of birth did not fire the PropertyChanged event");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_gender_can_be_set()
        {
            const GenderType expectedGender = GenderType.Female;
            var patient = new Patient { Gender = expectedGender };
            Assert.AreEqual(expectedGender, patient.Gender,
                "The gender (" + patient.Gender + ") doesn't match the expected gender ("
                + expectedGender + ").");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_gender_fires_PropertyChanged()
        {
            var propertyChangedWasFired = false;
            var patient = new Patient();
            patient.PropertyChanged += (sender, args) => { propertyChangedWasFired = args.PropertyName == "Gender"; };
            patient.Gender = GenderType.Male;
            Assert.IsTrue(propertyChangedWasFired, "Setting the gender did not fire the PropertyChanged event");
        }

	    [TestMethod]
	    [TestCategory("Domain")]
	    public void Then_the_amputee_status_can_be_set()
	    {
		    const bool expectedAmputeeStatus = true;
		    var patient = new Patient {IsAmputee = expectedAmputeeStatus};
			Assert.AreEqual(expectedAmputeeStatus, patient.IsAmputee,
				"The amputee status (" + patient.IsAmputee + ") doesn't match the expected amputee status ("
				+ expectedAmputeeStatus + ").");
	    }

		[TestMethod]
		[TestCategory("Domain")]
		public void Then_setting_the_amputee_status_fires_PropertyChanged()
		{
			var propertyChangedWasFired = false;
			var patient = new Patient();
			patient.PropertyChanged += (sender, args) =>
			{
				if (!propertyChangedWasFired)
					propertyChangedWasFired = args.PropertyName == "IsAmputee";
			};
			patient.IsAmputee = true;
			Assert.IsTrue(propertyChangedWasFired, "Setting the amputee status did not fire the PropertyChanged event");
		}

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_hospital_patient_ID_can_be_set()
        {
            const string expectedHospitalPID = "123-ABC";
            var patient = new Patient { HospitalPatientId = expectedHospitalPID };
            Assert.AreEqual(expectedHospitalPID, patient.HospitalPatientId,
                "The hospital patient ID '{0}' doesn't match the expected ID '{1}'.",
                expectedHospitalPID, patient.HospitalPatientId);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_hospital_ID_fires_PropertyChanged()
        {
            var propertyChangedWasFired = false;
            var patient = new Patient();
            patient.PropertyChanged += (sender, args) => { propertyChangedWasFired = args.PropertyName == "HospitalPatientId"; };
            patient.HospitalPatientId = "whatever";
            Assert.IsTrue(propertyChangedWasFired, "Setting the hospital ID did not fire the PropertyChanged event");
        }
    }
    // ReSharper restore InconsistentNaming
}
