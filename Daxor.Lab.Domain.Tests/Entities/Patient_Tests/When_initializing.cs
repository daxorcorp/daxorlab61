﻿using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Patient_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_patient_entity_is_not_transient()
        {
            var patient = new Patient();
            Assert.IsFalse(patient.IsTransient(),
                "Patient is an entity and should have a unique ID (i.e., not be transient).");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_default_date_of_birth_is_null()
        {
            var patient = new Patient();
            Assert.IsNull(patient.DateOfBirth, "Initial date of birth should be null.");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_default_gender_is_unknown()
        {
            var patient = new Patient();
            Assert.AreEqual(GenderType.Unknown, patient.Gender,
                "Initial gender should be unknown.");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_default_height_is_negative()
        {
            var patient = new Patient();
            Assert.IsTrue(patient.HeightInCurrentMeasurementSystem < 0,
                "Initial height should be negative.");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_default_weight_is_negative()
        {
            var patient = new Patient();
            Assert.IsTrue(patient.WeightInCurrentMeasurementSystem < 0,
                "Initial weight should be negative.");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_default_measurement_system_is_metric()
        {
            var patient = new Patient();
            Assert.AreEqual(MeasurementSystem.Metric, patient.MeasurementSystem,
                "Initial measurement system should be metric.");
        }
    }
    // ReSharper restore InconsistentNaming
}
