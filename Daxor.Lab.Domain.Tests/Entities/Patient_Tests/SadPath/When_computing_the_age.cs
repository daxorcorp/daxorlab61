﻿using System;
using Daxor.Lab.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Patient_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_the_age
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_date_of_birth_is_null_then_the_age_is_null()
        {
            var patient = new Patient();
            var age = patient.GetAgeInYears(DateTime.Now);
            Assert.IsNull(age, "Getting the age when the birthday is null should return null.");
        }
    }
    // ReSharper restore InconsistentNaming
}
