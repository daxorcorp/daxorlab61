﻿using System.Collections.Generic;
using System.ComponentModel;
using Daxor.Lab.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Spectrum_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_properties
    {
        private static Spectrum _stubSpectrum;

        [TestInitialize]
        public void TestsMethodInitialize()
        {
            _stubSpectrum = new Spectrum();
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_live_time_can_be_set()
        {
            const double expectedLiveTime = 1.23d;

            _stubSpectrum.LiveTimeInSeconds = expectedLiveTime;

            Assert.AreEqual(expectedLiveTime, _stubSpectrum.LiveTimeInSeconds,
                "The expected live time ({0}) didn't match the actual live time ({1}).",
                expectedLiveTime, _stubSpectrum.LiveTimeInSeconds);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_live_time_updates_the_dead_time()
        {
            const double expectedRealTime = 2.0d;
            const double expectedLiveTime = 1.23d;
            const double expectedDeadTime = expectedRealTime - expectedLiveTime;

            _stubSpectrum.RealTimeInSeconds = expectedRealTime;
            _stubSpectrum.LiveTimeInSeconds = expectedLiveTime;

            Assert.AreEqual(expectedDeadTime, _stubSpectrum.DeadTimeInSeconds,
                "The expected dead time ({0}) didn't match the actual dead time ({1}) when setting the live time.",
                expectedDeadTime, _stubSpectrum.DeadTimeInSeconds);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_live_time_fires_PropertyChanged()
        {
            _changedProperties = new List<string>();
            _stubSpectrum.PropertyChanged += SpectrumPropertyChanged;

            const double expectedLiveTime = 1.23d;

            _stubSpectrum.LiveTimeInSeconds = expectedLiveTime;

            var expectedChangedProperties = new List<string> { "LiveTimeInSeconds", "DeadTimeInSeconds" };
            CollectionAssert.AreEqual(expectedChangedProperties, _changedProperties,
                "Observed changed properties (" + UnitTestHelper.ListToString(_changedProperties) +
                ") does not match expected changed properties (" +
                UnitTestHelper.ListToString(expectedChangedProperties) + ").");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_real_time_can_be_set()
        {
            const double expectedRealTime = 1.23d;

            _stubSpectrum.RealTimeInSeconds = expectedRealTime;

            Assert.AreEqual(expectedRealTime, _stubSpectrum.RealTimeInSeconds,
                "The expected real time ({0}) didn't match the actual real time ({1}).",
                expectedRealTime, _stubSpectrum.RealTimeInSeconds);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_setting_the_real_time_fires_PropertyChanged()
        {
            _changedProperties = new List<string>();
            _stubSpectrum.PropertyChanged += SpectrumPropertyChanged;

            const double expectedRealTime = 1.23d;

            _stubSpectrum.RealTimeInSeconds = expectedRealTime;

            var expectedChangedProperties = new List<string> { "RealTimeInSeconds", "DeadTimeInSeconds" };
            CollectionAssert.AreEqual(expectedChangedProperties, _changedProperties,
                "Observed changed properties (" + UnitTestHelper.ListToString(_changedProperties) +
                ") does not match expected changed properties (" +
                UnitTestHelper.ListToString(expectedChangedProperties) + ").");
        }


        #region PropertyChanged helpers

        private List<string> _changedProperties;
        private void SpectrumPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            _changedProperties.Add(e.PropertyName);
        }

        #endregion    
    }
    // ReSharper restore InconsistentNaming
}
