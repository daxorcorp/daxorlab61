﻿using Daxor.Lab.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Entities.Spectrum_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_an_empty_spectrum
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void Then_it_has_zero_live_and_real_times_and_a_null_spectrum_array()
        {
            const int expectedLiveTime = 0;
            const int expectedRealTime = 0;

            var emptySpectrum = Spectrum.Empty();

            Assert.AreEqual(expectedLiveTime, emptySpectrum.LiveTimeInSeconds, "The live time for an empty spectrum should be {0} not {1}.",
                expectedLiveTime, emptySpectrum.LiveTimeInSeconds);
            Assert.AreEqual(expectedRealTime, emptySpectrum.RealTimeInSeconds, "The real time for an empty spectrum should be {0} not {1}.",
                expectedRealTime, emptySpectrum.RealTimeInSeconds);
            Assert.AreEqual(null, emptySpectrum.SpectrumArray, "The array for an empty spectrum should be null but is not.");
        }
    }
    // ReSharper restore InconsistentNaming
}
