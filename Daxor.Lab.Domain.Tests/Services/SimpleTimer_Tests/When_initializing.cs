﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Daxor.Lab.Domain.Services;

namespace Daxor.Lab.Domain.Tests.Services.SimpleTimer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        [TestMethod]
        public void The_the_internal_timer_is_set_to_the_correct_interval()
        {
            const int expectedInterval = 123;

            var timer = new SimpleTimer(expectedInterval);

            Assert.AreEqual(expectedInterval, (int)timer.InternalTimer.Interval);
        }
    }
    // ReSharper restore InconsistentNaming
}
