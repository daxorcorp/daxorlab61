﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Daxor.Lab.Domain.Services;

namespace Daxor.Lab.Domain.Tests.Services.SimpleTimer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_stop_is_called
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void When_Stop_is_called_then_the_internal_timer_is_stopped()
        {
            var timer = new SimpleTimer(10000);
            Assert.IsFalse(timer.InternalTimer.Enabled, "The internal timer is enabled before Start() was called.");

            timer.Start();
            timer.Stop();

            Assert.IsFalse(timer.InternalTimer.Enabled, "The internal timer should have been disnabled after Stop() was called.");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void When_Stop_is_called_then_the_timer_becomes_disabled()
        {
            var timer = new SimpleTimer(10000);

            timer.Start();
            timer.Stop();

            Assert.IsFalse(timer.Enabled, "The timer should have been disabled after Stop() was called.");
        }
    }
    // ReSharper restore InconsistentNaming
}
