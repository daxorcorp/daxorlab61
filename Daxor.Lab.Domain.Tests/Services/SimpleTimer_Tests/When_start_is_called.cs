﻿using Daxor.Lab.Domain.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Services.SimpleTimer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_start_is_called
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_internal_timer_is_started()
        {
            var timer = new SimpleTimer(10000);
            Assert.IsFalse(timer.InternalTimer.Enabled, "The internal timer is enabled before Start() was called.");

            timer.Start();

            Assert.IsTrue(timer.InternalTimer.Enabled, "The internal timer should have been enabled after Start() was called.");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_the_timer_becomes_enabled()
        {
            var timer = new SimpleTimer(10000);

            timer.Start();

            Assert.IsTrue(timer.Enabled, "The timer should have been enabled after Start() was called.");
        }
    }
    // ReSharper restore InconsistentNaming
}
