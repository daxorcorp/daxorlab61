﻿using System;
using System.Linq;
using System.Threading;
using Daxor.Lab.Domain.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Services.IdentityGenerator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_generating_a_new_sequential_GUID
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void And_theres_no_delay_between_generation_then_it_generates_two_unique_guids()
        {
            var guid1 = IdentityGenerator.NewSequentialGuid();
            var guid2 = IdentityGenerator.NewSequentialGuid();

            Assert.AreNotEqual(guid1, guid2, 
                "GUIDs returned should be unique but they are not '{0}' and '{1}'",
                guid1, guid2);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_theres_a_delay_between_generation_then_it_generates_two_sequential_guids()
        {
            var guid1 = IdentityGenerator.NewSequentialGuid();
            Thread.Sleep(100);
            var guid2 = IdentityGenerator.NewSequentialGuid();

            Assert.IsTrue(IsFirstGuidComponentLessThanSecondGuidComponent(guid1, guid2), 
                "Eight least-significant bytes of the first GUID '{0}' are greater than the second GUID '{1}'",
                guid1, guid2);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_a_sequential_guid_is_generated_then_it_has_the_correct_marker_bits()
        {
            var theGuid = IdentityGenerator.NewSequentialGuid();
            var eighthMostSigByte = theGuid.ToByteArray()[7];
            var actualLeadingBits = (byte)(eighthMostSigByte & 0xf0);
            const byte expectedLeadingBits = 0xc0;

            Assert.AreEqual(expectedLeadingBits, actualLeadingBits,
                "Most significant bits of the eighth byte should be {0:x}", expectedLeadingBits);
        }

        private bool IsFirstGuidComponentLessThanSecondGuidComponent(Guid guid1, Guid guid2)
        {
            var guid1Bytes = guid1.ToByteArray();
            var guid2Bytes = guid2.ToByteArray();
            var guid1part = new byte[8];
            var guid2part = new byte[8];

            // Pull out the eight least-significant bytes of each GUID
            for (int i = 0; i < 8; i++)
            {
                guid1part[i] = guid1Bytes[i + 8];
                guid2part[i] = guid2Bytes[i + 8];
            }

            // Because higher indices correspond to lower significance (i.e., big endian),
            // the individual bits need to be reversed before they can be compared.
            guid1part = guid1part.Reverse().ToArray();
            guid2part = guid2part.Reverse().ToArray();

            return BitConverter.ToUInt32(guid1part, 0) < BitConverter.ToUInt32(guid2part, 0);
        }
    }
    // ReSharper restore InconsistentNaming
}
