﻿using Daxor.Lab.Domain.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Services.UnitOfMeasurementConverter_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_English_to_metric
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void Then_inch_values_convert_to_centimeters_correctly()
        {
            var heightExemplars = MeasurementConverterTestHelper.GetHeightExemplars();
            foreach (var pair in heightExemplars)
            {
                var actualCentimeters = UnitsOfMeasurementConverter.ConvertHeightToMetric(pair.Item2);
                Assert.AreEqual(pair.Item1, actualCentimeters, MeasurementConverterTestHelper.Tolerance,
                    "{0} centimeters should be {1} centimeters, but was {2} centimeters",
                    pair.Item2, pair.Item1, actualCentimeters);
            }
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_pound_values_convert_to_kilograms_correctly()
        {
            var weightExemplars = MeasurementConverterTestHelper.GetWeightExemplars();
            foreach (var pair in weightExemplars)
            {
                var actualKilograms = UnitsOfMeasurementConverter.ConvertWeightToMetric(pair.Item2);
                Assert.AreEqual(pair.Item1, actualKilograms, MeasurementConverterTestHelper.Tolerance,
                    "{0} centimeters should be {1} kilograms, but was {2} kilograms",
                    pair.Item2, pair.Item1, actualKilograms);
            }
        }
    }
    // ReSharper restore InconsistentNaming
}
