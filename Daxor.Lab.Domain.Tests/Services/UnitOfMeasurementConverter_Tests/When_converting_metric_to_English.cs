﻿using Daxor.Lab.Domain.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Services.UnitOfMeasurementConverter_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_metric_to_English
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void Then_centimeter_values_convert_to_inches_correctly()
        {
            var heightExemplars = MeasurementConverterTestHelper.GetHeightExemplars();
            foreach (var pair in heightExemplars)
            {
                var actualInches = UnitsOfMeasurementConverter.ConvertHeightToEnglish(pair.Item1);
                Assert.AreEqual(pair.Item2, actualInches, MeasurementConverterTestHelper.Tolerance,
                    "{0} centimeters should be {1} inches, but was {2} inches",
                    pair.Item1, pair.Item2, actualInches);
            }
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void Then_kilogram_values_convert_to_pounds_correctly()
        {
            var weightExemplars = MeasurementConverterTestHelper.GetWeightExemplars();
            foreach (var pair in weightExemplars)
            {
                double actualPounds = UnitsOfMeasurementConverter.ConvertWeightToEnglish(pair.Item1);
                Assert.AreEqual(pair.Item2, actualPounds, MeasurementConverterTestHelper.Tolerance,
                    "{0} centimeters should be {1} pounds, but was {2} pounds",
                    pair.Item1, pair.Item2, actualPounds);
            }
        }
    }
    // ReSharper restore InconsistentNaming
}
