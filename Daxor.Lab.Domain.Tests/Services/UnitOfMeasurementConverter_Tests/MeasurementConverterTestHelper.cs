﻿using System;
using System.Collections.Generic;

namespace Daxor.Lab.Domain.Tests.Services.UnitOfMeasurementConverter_Tests
{
    internal static class MeasurementConverterTestHelper
    {
        internal static double Tolerance = 0.000000001;

        internal static List<Tuple<double, double>> GetWeightExemplars()
        {
            var weightExemplars = new List<Tuple<double, double>>
                                      {
                                            new Tuple<double, double>(0, 0),
                                            new Tuple<double, double>(1, 2.2046226218487758072297380134503),
                                            new Tuple<double, double>(50, 110.23113109243879036148690067251),
                                            new Tuple<double, double>(100, 220.46226218487758072297380134503)
                                      };
            return weightExemplars;
        }

        internal static List<Tuple<double, double>> GetHeightExemplars()
        {
            var heightExemplars = new List<Tuple<double, double>>
                                      {
                                          new Tuple<double, double>(0, 0),
                                          new Tuple<double, double>(1, 0.3937007874015748031496062992126),
                                          new Tuple<double, double>(50, 19.68503937007874015748031496063),
                                          new Tuple<double, double>(100, 39.37007874015748031496062992126)
                                      };
            return heightExemplars;
        }
    }
}
