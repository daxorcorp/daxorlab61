﻿using Daxor.Lab.Domain.Services;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Services.DemoSampleChanger_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_defining_core_functionality
    {
        [TestMethod]
        [TestCategory("Domain")]
        [RequirementValue(2000)]
        public void Then_the_delay_between_position_changes_matches_requirements()
        {
            var demoChanger = new DemoSampleChanger();
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), demoChanger.PositionChangeDelayInMilliseconds);
        }

        [TestMethod]
        [TestCategory("Domain")]
        [RequirementValue(24)]
        public void Then_the_initial_position_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), DemoSampleChanger.InitialPosition);
        }

        [TestMethod]
        [TestCategory("Domain")]
        [RequirementValue(25)]
        public void Then_the_number_of_positions_matches_requirements()
        {
            var demoChanger = new DemoSampleChanger();
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), demoChanger.NumberOfPositions);
        }
    }
    // ReSharper restore InconsistentNaming
}
