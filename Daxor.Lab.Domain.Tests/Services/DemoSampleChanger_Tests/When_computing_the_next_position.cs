﻿using Daxor.Lab.Domain.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Services.DemoSampleChanger_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_the_next_position
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_current_position_is_1_then_the_next_position_is_2()
        {
            var demoSC = new DemoSampleChanger();
            const int currPosition = 1;
            const int expectedNextPosition = 2;

            var actualNextPosition = demoSC.ComputeNextPosition(currPosition);

            Assert.AreEqual(expectedNextPosition, actualNextPosition);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_current_position_is_the_max_then_the_next_position_is_1()
        {
            var demoSC = new DemoSampleChanger();
            var currPosition = demoSC.NumberOfPositions;
            const int expectedNextPosition = 1;

            var actualNextPosition = demoSC.ComputeNextPosition(currPosition);

            Assert.AreEqual(expectedNextPosition, actualNextPosition);
        }
    }
    // ReSharper restore InconsistentNaming
}
