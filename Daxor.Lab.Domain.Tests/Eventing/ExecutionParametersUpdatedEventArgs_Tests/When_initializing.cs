﻿using Daxor.Lab.Domain.Eventing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Eventing.ExecutionParametersUpdatedEventArgs_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void And_args_are_given_to_the_ctor_then_each_property_is_set_correctly()
        {
            const int expectedVoltage = 1234;
            const double expectedFineGain = 1.015;
            const bool expectedIsDesiredIntegralReached = true;

            var args = new ExecutionParametersUpdatedEventArgs(expectedFineGain, expectedVoltage, expectedIsDesiredIntegralReached);

            Assert.AreEqual(expectedVoltage, args.Voltage, "The ctor set the voltage to {0} instead of {1}.",
                args.Voltage, expectedVoltage);
            Assert.AreEqual(expectedFineGain, args.FineGain, "The ctor set the fine gain to {0} instead of {1}.",
                args.FineGain, expectedFineGain);
            Assert.AreEqual(expectedIsDesiredIntegralReached, args.IsDesiredIntegralReached, "The ctor set desired integral reached flag to {0} instead of {1}.",
                args.IsDesiredIntegralReached, expectedIsDesiredIntegralReached);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_desired_integral_reached_flag_isnt_set_then_the_default_value_is_false()
        {
            var args = new ExecutionParametersUpdatedEventArgs(0, 0);

            Assert.IsFalse(args.IsDesiredIntegralReached);
        }
    }
    // ReSharper restore InconsistentNaming
}
