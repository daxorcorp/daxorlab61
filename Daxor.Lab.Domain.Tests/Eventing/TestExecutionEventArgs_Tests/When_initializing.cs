﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Eventing.TestExecutionEventArgs_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void And_args_are_given_to_the_ctor_then_each_property_is_set_correctly()
        {
            var executingTest = new DummyTest {InternalId = new Guid()};
            var expectedErrorException = new Exception("Some message");

            var args = new Domain.Eventing.TestExecutionEventArgs(executingTest, executingTest.InternalId, expectedErrorException);

            Assert.AreEqual(executingTest.InternalId, args.ExecutingTestId, "The ctor set the executing test ID to {0} instead of {1}.",
                args.ExecutingTestId, executingTest.InternalId);
            Assert.AreEqual(expectedErrorException, args.ErrorException, "The ctor set the error exception to {0} instead of {1}.",
                args.ErrorException, expectedErrorException);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_no_exception_arg_is_given_to_the_ctor_then_the_error_exception_is_null()
        {
            var executingTest = new DummyTest { InternalId = new Guid() };

            var args = new Domain.Eventing.TestExecutionEventArgs(executingTest, executingTest.InternalId);

            Assert.IsNull(args.ErrorException, "The ctor set the error exception to {0} instead of null.",
                args.ErrorException);
        }
    }
    // ReSharper restore InconsistentNaming
}
