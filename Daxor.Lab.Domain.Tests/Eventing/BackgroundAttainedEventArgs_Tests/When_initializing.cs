﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Daxor.Lab.Domain.Eventing;

namespace Daxor.Lab.Domain.Tests.Eventing.BackgroundAttainedEventArgs_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void And_args_are_given_to_the_ctor_then_each_property_is_set_correctly()
        {
            // ReSharper disable ConditionIsAlwaysTrueOrFalse
            const int expectedBackgroundCount = 1234;
            const bool expectedIsGood = true;
            const int expectedAcqTime = 2345;
            const bool expectedIsOverridden = true;

            var args = new BackgroundAttainedEventArgs(expectedBackgroundCount,
                 expectedAcqTime, expectedIsGood, expectedIsOverridden);

            Assert.AreEqual(expectedBackgroundCount, args.Count, "The ctor set the background count to {0} instead of {1}.",
                args.Count, expectedBackgroundCount);

            Assert.AreEqual(expectedIsGood, args.IsGood, "The ctor set the good background flag to {0} instead of {1}.",
                args.IsGood, expectedIsGood);
            Assert.AreEqual(expectedAcqTime, args.AcquisitionTimeInSeconds, "The ctor set the acquisition time to {0} seconds instead of {1} seconds.",
                args.AcquisitionTimeInSeconds, expectedAcqTime);
            Assert.AreEqual(expectedIsOverridden, args.IsOverridden, "The ctor set the overridden flag to {0} instead of {1}.",
                args.IsOverridden, expectedIsOverridden);
            // ReSharper restore ConditionIsAlwaysTrueOrFalse
        }
    }
    // ReSharper restore InconsistentNaming
}
