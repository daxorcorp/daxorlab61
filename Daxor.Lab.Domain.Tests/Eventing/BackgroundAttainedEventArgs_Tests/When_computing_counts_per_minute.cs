﻿using Daxor.Lab.Domain.Eventing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Domain.Tests.Eventing.BackgroundAttainedEventArgs_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_counts_per_minute
    {
        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_acquisition_time_is_zero_then_the_CPM_is_zero()
        {
            const int acqTime = 0;
            var args = new BackgroundAttainedEventArgs(0, acqTime, false, false);

            Assert.AreEqual(0, args.CountsPerMinute, "The CPM should be zero when the acquisition time is zero.");
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_the_count_is_zero_then_the_CPM_is_zero()
        {
            const int counts = 0;
            const int acqTime = 60;
            var args = new BackgroundAttainedEventArgs(counts, acqTime, false, false);

            Assert.AreEqual(0, args.CountsPerMinute, "The CPM should be zero (not {0}) when there are no counts.",
                args.CountsPerMinute);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_one_count_occurs_in_one_minute_then_the_CPM_is_one()
        {
            const int counts = 1;
            const int acqTime = 60;
            var args = new BackgroundAttainedEventArgs(counts, acqTime, false, false);

            const int expectedCpm = 1;
            Assert.AreEqual(expectedCpm, args.CountsPerMinute, "The CPM should be {0} (not {1}) when one count is observed with an acquisition time of 60 seconds.",
                expectedCpm, args.CountsPerMinute);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_one_count_occurs_in_seven_seconds_then_the_CPM_is_nine()
        {
            // This also tests whether rounding works.

            const int counts = 1;
            const int acqTime = 7;
            var args = new BackgroundAttainedEventArgs(counts, acqTime, false, false);

            const int expectedCpm = 9;
            Assert.AreEqual(expectedCpm, args.CountsPerMinute, "The CPM should be {0} (not {1}) when one count is observed with an acquisition time of 60 seconds.",
                expectedCpm, args.CountsPerMinute);
        }

        [TestMethod]
        [TestCategory("Domain")]
        public void And_one_count_occurs_in_eleven_seconds_then_the_CPM_is_five()
        {
            // This also tests whether rounding works.

            const int counts = 1;
            const int acqTime = 11;
            var args = new BackgroundAttainedEventArgs(counts, acqTime, false, false);

            const int expectedCpm = 5;
            Assert.AreEqual(expectedCpm, args.CountsPerMinute, "The CPM should be {0} (not {1}) when one count is observed with an acquisition time of 60 seconds.",
                expectedCpm, args.CountsPerMinute);
        }
    }
    // ReSharper restore InconsistentNaming
}
