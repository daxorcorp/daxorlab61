﻿using System.Collections.Generic;
using System.Text;

namespace Daxor.Lab.Domain.Tests
{
    public static class UnitTestHelper
    {
        public static string ListToString<T>(IEnumerable<T> list)
        {
            var sb = new StringBuilder();
            sb.Append("[");
            foreach (var item in list)
                sb.Append("'" + item + "', ");
            sb.Append("]");
            return sb.ToString();
        }
    }
}
