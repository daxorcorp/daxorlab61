﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Interfaces;

namespace Daxor.Lab.BVA.Services
{
    /// <summary>
    /// Represents a service that can compute the sum of squares of differences.
    /// </summary>
    public class SumOfSquaresOfDifferencesCalculator : ISumOfSquaresOfDifferencesCalculator
    {
        /// <summary>
        /// Calculates the sum of the squares of differences between the given lists' values.
        /// That is, Sum_i((list1_i - list2_i)^2).
        /// </summary>
        /// <param name="list1">First list of values</param>
        /// <param name="list2">Second list of values</param>
        /// <returns>Sum of the squares of differences for the given list values</returns>
        /// <exception cref="ArgumentException">The two lists have different numbers of elements</exception>
        /// <exception cref="ArgumentOutOfRangeException">Either of the lists have positive or negative
        /// infinity or NaN as members</exception>
        /// <exception cref="ArgumentNullException">Either list is null</exception>
        public double CalculateSumOfSquaresOfDifferences(IEnumerable<double> list1, IEnumerable<double> list2)
        {
            // Covert the list to an array for efficiency
            var list1AsArray = list1 as double[] ?? list1.ToArray();
            var list2AsArray = list2 as double[] ?? list2.ToArray();
            var listSize = list1AsArray.Count();

            // Boundary checks
            if (listSize != list2AsArray.Count())
                throw new ArgumentException("Given lists of values have differing member counts (" + listSize + " and " + list2AsArray.Count() + " respectively)");
            if (list1AsArray.Any(double.IsInfinity))
                throw new ArgumentOutOfRangeException("list1", "Infinity is not a supported value");
            if (list2AsArray.Any(double.IsInfinity))
                throw new ArgumentOutOfRangeException("list2", "Infinity is not a supported value");
            if (list1AsArray.Any(double.IsNaN))
                throw new ArgumentOutOfRangeException("list1", "NaN is not a supported value");
            if (list2AsArray.Any(double.IsNaN))
                throw new ArgumentOutOfRangeException("list2", "NaN is not a supported value");

            // Compute the result
            double sumOfSquares = 0;
            for (var i = 0; i < listSize; i++)
                sumOfSquares += Math.Pow(list1AsArray[i] - list2AsArray[i], 2);

            return sumOfSquares;
        }
    }
}
