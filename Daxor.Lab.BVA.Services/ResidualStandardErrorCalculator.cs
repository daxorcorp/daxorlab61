﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Interfaces;

namespace Daxor.Lab.BVA.Services
{
    /// <summary>
    /// Represents a service that can compute the residual standard error.
    /// </summary>
    public class ResidualStandardErrorCalculator : IResidualStandardErrorCalculator
    {
        #region Fields

        private readonly IPredictedValuesCalculator _predictedValuesCalculator;
        private readonly ISumOfSquaresOfDifferencesCalculator _sumOfSquaresOfDifferencesCalculator;

        #endregion

        #region Ctor

        /// <summary>
        /// Creates a new ResidualStandardErrorCalculator instance that uses the given services
        /// to aid in the calculation.
        /// </summary>
        /// <param name="predictedValuesCalculator">Service instance that calculates predicted values
        /// based on a linear regression</param>
        /// <param name="sumOfSquaresOfDifferencesCalculator">Service instance that calculates the
        /// sum of squares of differences of values</param>
        /// <exception cref="ArgumentNullException">If either or both of the two service instances
        /// are null</exception>
        public ResidualStandardErrorCalculator(IPredictedValuesCalculator predictedValuesCalculator,
                                               ISumOfSquaresOfDifferencesCalculator sumOfSquaresOfDifferencesCalculator)
        {
            if (predictedValuesCalculator == null)
                throw new ArgumentNullException("predictedValuesCalculator");
            if (sumOfSquaresOfDifferencesCalculator == null)
                throw new ArgumentNullException("sumOfSquaresOfDifferencesCalculator");

            _predictedValuesCalculator = predictedValuesCalculator;
            _sumOfSquaresOfDifferencesCalculator = sumOfSquaresOfDifferencesCalculator;
        }

        #endregion

        /// <summary>
        /// Calculates the residual standard error for the given set of independent (x) values, 
        /// observed values, slope, and y-intercept.
        /// </summary>
        /// <param name="independentValues">x-values for the data points</param>
        /// <param name="observedValues">y-values for the data points</param>
        /// <param name="slope">Slope of the regression line</param>
        /// <param name="yIntercept">y-intercept of the regression line</param>
        /// <returns>Calculated residual standard error</returns>
        /// <exception cref="ArgumentException">If the two lists of values have different numbers
        /// of elements</exception>
        /// <exception cref="ArgumentException">If either of the lists has fewer than three
        /// members</exception>
        /// <remarks>If any of the service dependencies throw exceptions, they will
        /// not be caught by this method.</remarks>
        public double CalculateResidualStandardError(IEnumerable<double> independentValues,
                                                        IEnumerable<double> observedValues, 
                                                        double slope, 
                                                        double yIntercept)
        {
            var indepValsAsArray = independentValues as double[] ?? independentValues.ToArray();
            var obsValuesAsArray = observedValues as double[] ?? observedValues.ToArray();
            var numValues = indepValsAsArray.Count();

            if (numValues != obsValuesAsArray.Count())
                throw new ArgumentException("Must have the same number of independent and observed values; " + numValues + " != " + obsValuesAsArray.Count());
            if (numValues < 3)
                throw new ArgumentException("There must be at least three values to compute RSE");

            var predictedValues = _predictedValuesCalculator.CalculatePredictedValues(
                                        indepValsAsArray, slope, yIntercept);
            var sumOfSquaresOfDiffs = _sumOfSquaresOfDifferencesCalculator.CalculateSumOfSquaresOfDifferences(
                                        predictedValues, obsValuesAsArray);
            return Math.Sqrt(sumOfSquaresOfDiffs/(numValues - 2));
        }
    }
}
