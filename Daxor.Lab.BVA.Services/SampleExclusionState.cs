﻿using System;

namespace Daxor.Lab.BVA.Services
{
    /// <summary>
    /// Represents the exclusion state for a BVASample
    /// </summary>
    public class SampleExclusionState
    {
        /// <summary>
        /// Gets or sets the sample ID for the BVASample whose state is being represented
        /// </summary>
        public Guid SampleId { get; set; }

        /// <summary>
        /// Gets or sets whether or not the represented sample's count is excluded
        /// </summary>
        public bool IsCountExcluded { get; set; }

        /// <summary>
        /// Gets or sets whether or not the represented sample's count is auto-excluded
        /// </summary>
        public bool IsCountAutoExcluded { get; set; }

        /// <summary>
        /// Returns a string representation of the properties of this instance
        /// </summary>
        /// <returns>String representation of the properties of this instance</returns>
        public override string ToString()
        {
            return String.Format("SampleId: {0}, IsCountExcluded: {1}, IsCountAutoExcluded: {2}", SampleId,
                                 IsCountExcluded, IsCountAutoExcluded);
        }
    }
}