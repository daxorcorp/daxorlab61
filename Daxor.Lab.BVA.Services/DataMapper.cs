﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Database;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.BVA.Services
{
	internal static class DataMapper
	{
		public static SampleLayoutItem RecordToModel(PositionRecord record)
		{
			if (record == null)
				throw new NullReferenceException("record");

			var item = new SampleLayoutItem { Position = record.Position, FriendlyName = record.ShortFriendlyName };

			switch (record.SampleFunction)
			{
				case "Background":
					item.Type = SampleType.Background;
					break;

				case "Baseline":
					item.Type = SampleType.Baseline;
					item.Range = SampleRange.Undefined;
					switch (record.Range)
					{
						case "A":
							item.Range = SampleRange.A;
							break;
						case "B":
							item.Range = SampleRange.B;
							break;
					}
					break;

				case "Standard":
					item.Type = SampleType.Standard;
					item.Range = SampleRange.Undefined;
					switch (record.Range)
					{
						case "A":
							item.Range = SampleRange.A;
							break;
						case "B":
							item.Range = SampleRange.B;
							break;
					}
					break;

				case "Sample":
					SampleType sType;
					if (!Enum.TryParse(String.Format("{0}{1}", record.SampleFunction, record.SampleNumber), out sType))
						sType = SampleType.Undefined;
					item.Type = sType;

					SampleRange sRange;
					if (!Enum.TryParse(record.Range, out sRange))
						sRange = SampleRange.Undefined;
					item.Range = sRange;
					break;

				default:
					item.Type = SampleType.Undefined;
					item.Range = SampleRange.Undefined;
					break;
			}

			return item;
		}
		public static BVATest RecordToModel(BvaTestRecord testRecord, ISampleLayoutSchema schema)
		{
			if (testRecord == null)
				throw new ArgumentNullException("testRecord");

			var testModel = new BVATest(schema, null)
							{InternalId = testRecord.TestId, Patient = RecordToModel(testRecord.Patient)};

			testModel.Patient.HeightInCm = testRecord.PatientHeight;
			testModel.Patient.WeightInKg = testRecord.PatientWeight;
			testModel.Patient.MeasurementSystem = testRecord.MeasurementSystem.Trim() == "US" ? MeasurementSystem.English : MeasurementSystem.Metric;
			testModel.Patient.IsAmputee = testRecord.Patient.IsAmputee;

			testModel.SampleLayoutId = testRecord.SampleLayoutId;
			testModel.Status = (TestStatus)testRecord.StatusId;
			testModel.Mode = (TestMode)testRecord.TestMode;

			testModel.SystemId = testRecord.UniqueSystemId;
			testModel.Date = testRecord.TestDate;
			testModel.Comments = testRecord.Comment;
			testModel.ConsiderationId = testRecord.ConsiderationId;
			
			testModel.BackgroundCount = testRecord.BackgroundCount;
			testModel.BackgroundTimeInSeconds = testRecord.BackgroundTime;
			testModel.RefVolume = testRecord.ReferenceVolume;
			testModel.AmputeeIdealsCorrectionFactor = testRecord.AmputeeIdealsCorrectionFactor;

			testModel.Analyst = testRecord.Analyst;
			testModel.InjectateKey = testRecord.InjectateBarcode;
			testModel.StandardAKey = testRecord.StandardABarcode;
			testModel.StandardBKey = testRecord.StandardBBarcode;
			testModel.TestID2 = testRecord.ID2;
			testModel.Location = testRecord.Location;
			testModel.InjectateDose = testRecord.Dose;
			testModel.SampleDurationInSeconds = testRecord.SampleDurationInSeconds;
			testModel.InjectateLot = testRecord.InjectateLot;
			testModel.RefPhysician = testRecord.ReferringPhysician;
			testModel.PhysiciansSpecialty = testRecord.PhysiciansSpecialty;
			testModel.CcReportTo = testRecord.CCReportTo;
			testModel.PacsID = testRecord.PACS;

			testModel.HematocritFill = (HematocritFillType)testRecord.HematocritFillType;
			testModel.Tube = RecordToModel(testRecord.TubeTypeRecord);
			testModel.ConcurrencyObject = testRecord.ConcurrencyObject;
			testModel.HasSufficientData = testRecord.HasSufficientData;
			testModel.Protocol = testRecord.BloodDrawCount;

			var sampleModelsAndRecords = from s in testModel.Samples
						join r in testRecord.Samples on s.Position equals r.PositionRecord.Position into outer
						from r in outer.DefaultIfEmpty()
						select new { SampleModel = s, SampleRecord = r };

			foreach (var modelRecordPair in sampleModelsAndRecords.Where(pair => pair.SampleRecord != null))
			{
				modelRecordPair.SampleModel.InternalId = modelRecordPair.SampleRecord.SampleId;
				modelRecordPair.SampleModel.BvaSampleTimestamp = modelRecordPair.SampleRecord.BvaSampleTimestamp;
				modelRecordPair.SampleModel.Centroid = modelRecordPair.SampleRecord.Centroid;
				modelRecordPair.SampleModel.Counts = modelRecordPair.SampleRecord.Counts;
				modelRecordPair.SampleModel.FullWidthHalfMax = modelRecordPair.SampleRecord.FWHM;
				modelRecordPair.SampleModel.FullWidthTenthMax = modelRecordPair.SampleRecord.FWTM;
				modelRecordPair.SampleModel.Hematocrit = modelRecordPair.SampleRecord.Hematocrit;
				modelRecordPair.SampleModel.IsCountAutoExcluded = modelRecordPair.SampleRecord.IsCountAutoExcluded;
				modelRecordPair.SampleModel.IsCountExcluded = modelRecordPair.SampleRecord.IsCountExcluded;
				modelRecordPair.SampleModel.IsHematocritExcluded = modelRecordPair.SampleRecord.IsHematocritExcluded;
				modelRecordPair.SampleModel.IsExcluded = modelRecordPair.SampleRecord.IsSampleExcluded;
				modelRecordPair.SampleModel.Spectrum.LiveTimeInSeconds = modelRecordPair.SampleRecord.LiveTime;
				modelRecordPair.SampleModel.Position = modelRecordPair.SampleRecord.PositionRecord.Position;
				modelRecordPair.SampleModel.PostInjectionTimeInSeconds = modelRecordPair.SampleRecord.TimePostInjection;
				modelRecordPair.SampleModel.PresetLiveTimeInSeconds = (int)modelRecordPair.SampleRecord.PresetLiveTime;
				modelRecordPair.SampleModel.Spectrum.RealTimeInSeconds = modelRecordPair.SampleRecord.RealTime;
				modelRecordPair.SampleModel.ROIHighBound = modelRecordPair.SampleRecord.RoiHighBound;
				modelRecordPair.SampleModel.ROILowBound = modelRecordPair.SampleRecord.RoiLowBound;
				modelRecordPair.SampleModel.SampleHeaderTimestamp = modelRecordPair.SampleRecord.SampleHeaderTimestamp;
				modelRecordPair.SampleModel.Spectrum.SpectrumArray = modelRecordPair.SampleRecord.Spectrum;
				modelRecordPair.SampleModel.TestId = modelRecordPair.SampleRecord.TestId;
			}

			return testModel;
		}

		public static BvaTestRecord ModelToRecord(BVATest test, ISettingsManager settingsManager)
		{
			if (test == null)
				throw new ArgumentNullException("test");

			var testRecord = new BvaTestRecord {Patient = new PatientRecord()}; // Copy patient

			// Only copy the DOB to the DTO if it's valid.
			if ((test.Patient.DateOfBirth.HasValue) && (test.Patient.DateOfBirth != DomainConstants.InvalidDateOfBirth))
				testRecord.Patient.DOB = test.Patient.DateOfBirth;

			testRecord.ID2 = test.TestID2 ?? String.Empty;
			testRecord.Patient.FirstName = test.Patient.FirstName ?? String.Empty;
			testRecord.Patient.LastName = test.Patient.LastName ?? String.Empty;
			testRecord.Patient.MiddleName = test.Patient.MiddleName ?? String.Empty;
			testRecord.Patient.PatientHospitalId = test.Patient.HospitalPatientId ?? String.Empty;
			testRecord.Patient.PID = test.Patient.InternalId;
			testRecord.Patient.IsAmputee = test.Patient.IsAmputee;
			testRecord.MeasurementSystem = test.Patient.MeasurementSystem == MeasurementSystem.English ? "US" : "METRIC";

			testRecord.PID = test.Patient.InternalId;
			testRecord.Patient.Gender = (Sex)test.Patient.Gender;
			testRecord.PatientHeight = test.Patient.HeightInCm;
			testRecord.PatientWeight = test.Patient.WeightInKg;

			testRecord.Analyst = test.Analyst ?? String.Empty;
			testRecord.BackgroundCount = test.BackgroundCount;

			//  Sample duration *must* be set before the background time is set
			testRecord.SampleDurationInSeconds = test.SampleDurationInSeconds;
			testRecord.BackgroundTime = test.BackgroundTimeInSeconds;
			testRecord.CCReportTo = test.CcReportTo ?? String.Empty;
			testRecord.Comment = test.Comments ?? String.Empty;
			testRecord.ConsiderationId = test.ConsiderationId;

			testRecord.Dose = test.InjectateDose;
			testRecord.BloodDrawCount = test.Protocol;

			testRecord.HematocritFillType = (int)test.HematocritFill;

			testRecord.InjectateBarcode = test.InjectateKey ?? String.Empty;
			testRecord.InjectateLot = test.InjectateLot ?? String.Empty;

			testRecord.Location = test.Location ?? String.Empty;
			testRecord.PACS = test.PacsID ?? String.Empty;
			testRecord.ReferenceVolume = test.RefVolume;
			testRecord.ReferringPhysician = test.RefPhysician ?? String.Empty;
			testRecord.PhysiciansSpecialty = test.PhysiciansSpecialty ?? String.Empty;
			testRecord.StandardABarcode = test.StandardAKey ?? String.Empty;
			testRecord.StandardBBarcode = test.StandardBKey ?? String.Empty;

			if (test.Mode == TestMode.Manual)
				test.Status = TestStatus.Completed;

			testRecord.StatusId = (int)test.Status;
			testRecord.TestDate = test.Date;
			testRecord.TestId = test.InternalId;
			testRecord.TestMode = (int)test.Mode;
			testRecord.TubeId = test.Tube.Id;
			testRecord.TestDate = test.Date;
			testRecord.TestMode = (int)test.Mode;
			testRecord.HasSufficientData = test.HasSufficientData;
			testRecord.ConcurrencyObject = test.ConcurrencyObject;
			testRecord.AmputeeIdealsCorrectionFactor = test.AmputeeIdealsCorrectionFactor;

			testRecord.SampleLayoutId = test.SampleLayoutId;
			testRecord.Samples = new List<BvaSampleRecord>();

			foreach (var sampleRecord in test.Samples.Select(s => ModelToRecord(s, settingsManager)))
				testRecord.Samples.Add(sampleRecord);

			testRecord.StatusId = (int)test.Status;
			testRecord.UniqueSystemId = test.SystemId;

			return testRecord;
		}

		public static BVASample RecordToModel(BvaSampleRecord sampleRecord)
		{
			if (sampleRecord == null)
				throw new ArgumentNullException("sampleRecord");

			var sampleModel = new BVASample(sampleRecord.SampleId, sampleRecord.PositionRecord.Position, null)
								  {
									  ROIHighBound = sampleRecord.RoiHighBound,
									  ROILowBound = sampleRecord.RoiLowBound,
									  IsCountExcluded = sampleRecord.IsCountExcluded,
									  IsCountAutoExcluded = sampleRecord.IsCountAutoExcluded,
									  IsHematocritExcluded = sampleRecord.IsHematocritExcluded,
									  IsExcluded = sampleRecord.IsSampleExcluded,
									  Centroid = sampleRecord.Centroid,
									  Counts = sampleRecord.Counts,
									  FullWidthHalfMax = sampleRecord.FWHM,
									  FullWidthTenthMax = sampleRecord.FWTM,
									  Hematocrit = sampleRecord.Hematocrit,
									  PostInjectionTimeInSeconds = sampleRecord.TimePostInjection,
									  PresetLiveTimeInSeconds = (int) sampleRecord.PresetLiveTime,
									  FullName = sampleRecord.PositionRecord.ShortFriendlyName,
									  DisplayName = sampleRecord.PositionRecord.ShortFriendlyName,
									  SampleHeaderTimestamp = sampleRecord.SampleHeaderTimestamp,
									  BvaSampleTimestamp = sampleRecord.BvaSampleTimestamp,
									  Spectrum = new Spectrum
									  {
										  LiveTimeInSeconds = sampleRecord.LiveTime,
										  RealTimeInSeconds = sampleRecord.RealTime,
										  SpectrumArray = sampleRecord.Spectrum
									  }
								  };
			sampleModel.IsCountExcluded = sampleRecord.IsCountExcluded; // IsCountAutoExcluded.set may have overridden  (Geoff)
			return sampleModel;
		}
		public static BvaSampleRecord ModelToRecord(BVASample sample, ISettingsManager settingsManager)
		{
			if (sample == null)
				throw new ArgumentNullException("sample");

			var sampleRecord = new BvaSampleRecord
							  {
								  SampleId = sample.InternalId,
								  LiveTime = sample.Spectrum.LiveTimeInSeconds,
								  RealTime = sample.Spectrum.RealTimeInSeconds,
								  Centroid = sample.Centroid,
								  Counts = sample.Counts,
								  FWHM = sample.FullWidthHalfMax,
								  FWTM = sample.FullWidthTenthMax,
								  Hematocrit = sample.Hematocrit,
								  IsCountExcluded = sample.IsCountExcluded,
								  IsCountAutoExcluded = sample.IsCountAutoExcluded,
								  IsHematocritExcluded = sample.IsHematocritExcluded,
								  IsSampleExcluded = sample.IsExcluded,
								  PositionRecord = {Position = sample.Position},
								  TimePostInjection = sample.PostInjectionTimeInSeconds,
								  PresetLiveTime = sample.PresetLiveTimeInSeconds,
								  RoiHighBound = sample.ROIHighBound,
								  RoiLowBound = sample.ROILowBound,
								  TestId = sample.TestId,
								  SampleHeaderTimestamp = sample.SampleHeaderTimestamp,
								  BvaSampleTimestamp = sample.BvaSampleTimestamp,
								  SampleLayoutId = settingsManager.GetSetting<int>(SettingKeys.SystemSampleChangerLayoutId),
							  };

			if (sample.Spectrum.SpectrumArray != null)
				sampleRecord.Spectrum = sample.Spectrum.SpectrumArray;

			return sampleRecord;
		}

		public static BVAPatient RecordToModel(PatientRecord patientRecord)
		{
			var patientModel = new BVAPatient(Guid.Empty, null);
			if (patientRecord == null) return patientModel;

			patientModel.InternalId = patientRecord.PID;
			patientModel.FirstName = patientRecord.FirstName;
			patientModel.LastName = patientRecord.LastName;
			patientModel.MiddleName = patientRecord.MiddleName;
			patientModel.Gender = Enum.IsDefined(typeof (Sex), patientRecord.Gender) ? (GenderType) patientRecord.Gender : GenderType.Unknown;
			patientModel.HospitalPatientId = patientRecord.PatientHospitalId;
			patientModel.DateOfBirth = patientRecord.DOB == new DateTime(1753,1,1) ? null : patientRecord.DOB;
			patientModel.IsAmputee = patientRecord.IsAmputee;
			
			return patientModel;
		}

		public static Tube RecordToModel(TubeTypeRecord tubeRecord)
		{
			return new Tube
			{
				Id = tubeRecord.TubeId,
				AnticoagulantFactor = tubeRecord.Anticoagulant,
				Name = tubeRecord.Description
			};
		}
		public static TubeTypeRecord ModelToRecord(Tube tube)
		{
			return new TubeTypeRecord
			{
				TubeId = tube.Id,
				Anticoagulant = tube.AnticoagulantFactor,
				Description = tube.Name
			};
		}

		public static BVATestItem RecordToModel(GetBvaTestListResult record)
		{
			return new BVATestItem
			{
				TestID = record.TEST_ID,
				WasSaved = true,
				CreatedDate = record.TEST_DATE,

				PatientDOB = record.DOB == new DateTime(1753,1,1) ? (DateTime?)null : record.DOB,
				PatientFullName = BVATestItem.FormatFullName(record.LAST_NAME, record.FIRST_NAME, record.MIDDLE_NAME),
				PatientHospitalID = record.PATIENT_HOSPITAL_ID,
				PatientPID = record.PID,

				TestID2 = record.ID2,
				Status = ( (record.STATUS_ID >= -1) && (record.STATUS_ID <= 4) ) ? (TestStatus)record.STATUS_ID : TestStatus.Undefined,
				HasSufficientData = record.HAS_SUFFICIENT_DATA == 'T',
				TestModeDescription = record.TEST_MODE_DESCRIPTION,
			};
		}
	}
}
