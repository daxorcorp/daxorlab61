﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.BVA.Services
{
    /// <summary>
    /// Represents a service that can exclude points from a blood volume test 
    /// using an algorithm.
    /// </summary>
    public class AutomaticPointExclusionService : IAutomaticPointExclusionService
    {
        #region Constants

        /// <summary>
        /// Minimum number of non-excluded individual samples remaining after point exclusion
        /// has been applied.
        /// </summary>
        public static int MinimumSampleCountAfterExclusion = 3;

        /// <summary>
        /// String format for metadata pertaining to finding a sample to exclude
        /// </summary>
        public static string FindSampleToExcludeMetadataFormatString =
            "Excluding {0}{1} yields: RSE = {2:0.00000}, StdDev = {3:0.00000}, TotalBV = {4:0.0}";

        #endregion

        #region Fields

        private readonly IMeasuredCalcEngineService _measuredCalcEngineService;
        private readonly IResidualStandardErrorCalculator _residualStandardErrorCalculator;
        private readonly ISettingsManager _settingsManager;

        #endregion

        #region Ctor

        /// <summary>
        /// Creates a new AutomaticPointExclusionService instance based on the given
        /// dependencies.
        /// </summary>
        /// <param name="measuredCalcEngineService">Calculation engine service for
        /// determining measured blood volumes</param>
        /// <param name="residualStandardErrorCalculator">Service for calculating
        /// residual standard error</param>
        /// <param name="settingsManager">Manager for system settings</param>
        /// <exception cref="ArgumentNullException">If any of the given arguments
        /// are null</exception>
        public AutomaticPointExclusionService(IMeasuredCalcEngineService measuredCalcEngineService,
            IResidualStandardErrorCalculator residualStandardErrorCalculator,
            ISettingsManager settingsManager)
        {
            if (measuredCalcEngineService == null)
                throw new ArgumentNullException("measuredCalcEngineService");
            if (residualStandardErrorCalculator == null)
                throw new ArgumentNullException("residualStandardErrorCalculator");
            if (settingsManager == null)
                throw new ArgumentNullException("settingsManager");

            _measuredCalcEngineService = measuredCalcEngineService;
            _residualStandardErrorCalculator = residualStandardErrorCalculator;
            _settingsManager = settingsManager;
            _settingsManager.SettingsChanged += OnSettingsChanged;

            SetEnabledState();
            SetStandardDeviationThreshold();
            SetMinimumPatientProtocol();
            SetMaximumNumberOfExclusions();
            SetResidualStandardErrorThreshold();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets whether this service is enabled or not.
        /// </summary>
        public bool IsEnabled { get; private set; }

        /// <summary>
        /// Gets or sets the maximum number of points that can be excluded.
        /// </summary>
        public int MaximumNumberOfExclusions { get; private set; }

        /// <summary>
        /// Gets or sets the minimum patient protocol for applying point exclusion
        /// </summary>
        public int MinimumPatientProtocol { get; private set; }

        /// <summary>
        /// Gets or sets the residual standard error threshold
        /// </summary>
        public double ResidualStandardErrorThreshold { get; private set; }

        /// <summary>
        /// Gets or sets the standard deviation threshold
        /// </summary>
        public double StandardDeviationThreshold { get; private set; }

        #endregion

        #region IAutomaticPointExclusionService.ExcludePoints(BVATest)

        /// <summary>
        /// Handles the application of the automatic point exclusion algorithm
        /// </summary>
        /// <param name="test">BVA test instance whose points are to be evaluated</param>
        /// <returns>Metadata about the exclusion process</returns>
        /// <remarks>
        /// (1) If the service is not enabled, no work is performed and appropriate
        ///     metadata is returned.
        /// (2) The actual algorithm for exclusion is handled by InternalExcludePoints().
        /// (3) If InternalExcludePoints() encounters a problem (i.e., throws an exception)
        ///     the original state of the given test's samples exclusions is restored
        ///     and appropriate metadata is returned.
        /// (4) If capturing the initial test samples' exclusion states or the restoration
        ///     of those states fails, an apporpriate exception with be thrown.
        /// </remarks>
        /// <exception cref="ArgumentNullException">Given test instance is null</exception>
        public AutomaticPointExclusionMetadata ExcludePoints(BVATest test)
        {
            if (!IsEnabled)
                return CreateFailureMetadata("Service is not enabled");
            if (test == null)
                throw new ArgumentNullException("test");

            AutomaticPointExclusionMetadata metadata;
            if (!TestMeetsInitialConditions(test, out metadata)) return metadata;

            var currentBvaTestState = CaptureBvaTestState(test);
            try
            {
                metadata = InternalExcludePoints(test);
            }
            catch (Exception e)
            {
                RestoreBvaTestState(currentBvaTestState, test);
                metadata = CreateFailureMetadata("Previous states restored because an internal exception occurred: '" + e.Message + "'; Stack trace: " + e.StackTrace);
            }

            return metadata;
        }

        #endregion

        #region Virtual helpers

        /// <summary>
        /// Creates a BVA test state pertaining to properties modified by the point exclusion service
        /// </summary>
        /// <param name="bvaTest">BVA test whose state should be captured</param>
        /// <remarks>A List of sample states is used instead of IEnumerable to force the evaluation
        /// of the LINQ query, which resolves issues where the Samples collection has null elements</remarks>
        /// <returns>A BvaTestState instance that represents the state of the given test</returns>
        /// <exception cref="ArgumentNullException">Given test instance is null</exception>
        /// <exception cref="ArgumentNullException">Given test's Samples instance is null</exception>
        /// <exception cref="NullReferenceException">Given test's Samples collection has null members</exception>
        /// <exception cref="NotSupportedException">If the measured portion of the Volumes collection 
        /// is null</exception>
        public virtual BvaTestState CaptureBvaTestState(BVATest bvaTest)
        {
            if (bvaTest == null)
                throw new ArgumentNullException("bvaTest");
            if (bvaTest.Volumes[BloodType.Measured] == null)
                throw new NotSupportedException("Measured blood type volume data of the given BVA test is null");

            var exclusionStates = from sample in bvaTest.Samples
                         select new SampleExclusionState
                             {
                                 SampleId = sample.InternalId,
                                 IsCountExcluded = sample.IsCountExcluded,
                                 IsCountAutoExcluded = sample.IsCountAutoExcluded
                             };

            return new BvaTestState
                {
                     MeasuredPlasmaVolume = bvaTest.Volumes[BloodType.Measured].PlasmaCount,
                     MeasuredRedCellVolume = bvaTest.Volumes[BloodType.Measured].RedCellCount,
                     SampleExclusionStates = exclusionStates.ToList(),
                     StandardDeviation = bvaTest.StandardDevResult,
                     TransudationRate = bvaTest.TransudationRateResult
                };
        }

        /// <summary>
        /// Restores the counts excluded and auto-excluded states for the samples in a given BVA test
        /// to the given states.
        /// </summary>
        /// <param name="bvaTestState">Collection of sample exclusion states</param>
        /// <param name="bvaTest">BVA test with samples whose states should be restored</param>
        /// <exception cref="ArgumentNullException">Given BVA test state is null</exception>
        /// <exception cref="ArgumentNullException">Given test instance is null</exception>
        /// <exception cref="ArgumentNullException">Given test's Samples instance is null</exception>
        /// <exception cref="NullReferenceException">Given collection of states has a null member</exception>
        /// <exception cref="NotSupportedException">Collection of sample exclusion states has a different 
        /// size than  the collection of BVA test's collection of samples</exception>
        /// <exception cref="NotSupportedException">Collection of states has at least one sample (ID)
        /// that doesn't exist in the BVA test's collection of samples</exception>
        /// <exception cref="NotSupportedException">If the measured portion of the Volumes collection 
        /// is null</exception>
        /// <exception cref="NotSupportedException">If the sample exclusion state list is null</exception>
        public virtual void RestoreBvaTestState(BvaTestState bvaTestState, BVATest bvaTest)
        {
            if (bvaTestState == null)
                throw new ArgumentNullException("bvaTestState");
            if (bvaTestState.SampleExclusionStates == null)
                throw new NotSupportedException("Sample exclusion state collection cannot be null");
            if (bvaTest == null)
                throw new ArgumentNullException("bvaTest");
            if (bvaTest.Volumes[BloodType.Measured] == null)
                throw new NotSupportedException("Measured blood type volume data of the given BVA test is null");
            
            var sampleIdsForTests = from s in bvaTest.Samples select s.InternalId;
            var sampleIdsForTestsAsArray = sampleIdsForTests as Guid[] ?? sampleIdsForTests.ToArray();
            var sampleIdsForExclusionStates = from s in bvaTestState.SampleExclusionStates select s.SampleId;

            if (bvaTestState.SampleExclusionStates.Count() != sampleIdsForTestsAsArray.Count())
                throw new NotSupportedException("The collection of exclusion states and BVA test samples are of different sizes");
            if (sampleIdsForExclusionStates.Except(sampleIdsForTestsAsArray).Count() != 0)
                throw new NotSupportedException("There is at least one sample ID in the exclusion states collection that isn't in the test's sample collection");

            foreach (var exclusionState in bvaTestState.SampleExclusionStates)
            {
                var bvaSample = (from s in bvaTest.Samples where s.InternalId == exclusionState.SampleId select s).FirstOrDefault();
                if (bvaSample == null) continue;
                bvaSample.IsCountAutoExcluded = exclusionState.IsCountAutoExcluded;
                bvaSample.IsCountExcluded = exclusionState.IsCountExcluded;
            }

            bvaTest.Volumes[BloodType.Measured].PlasmaCount = bvaTestState.MeasuredPlasmaVolume;
            bvaTest.Volumes[BloodType.Measured].RedCellCount = bvaTestState.MeasuredRedCellVolume;
            bvaTest.StandardDevResult = bvaTestState.StandardDeviation;
            bvaTest.TransudationRateResult = bvaTestState.TransudationRate;
        }

        /// <summary>
        /// Determines whether or not the given BVA test meets the initial conditions required
        /// to begin automatic point exclusion.
        /// </summary>
        /// <param name="bvaTest">BVA test instance to analyze</param>
        /// <param name="failureMetadata">Metadata regarding any failed condition</param>
        /// <returns>True if the given test meets all criteria; false otherwise</returns>
        protected virtual bool TestMeetsInitialConditions(BVATest bvaTest, out AutomaticPointExclusionMetadata failureMetadata)
        {
            failureMetadata = null;
            string incompleteFields;

            if (!bvaTest.HasSufficientData)
                failureMetadata = CreateFailureMetadata("Test does not have sufficient data to compute a total blood volume");
            else if (TestHasIncompleteFields(bvaTest, out incompleteFields))
                failureMetadata = CreateFailureMetadata("Test has incomplete fields: " + incompleteFields);
            else if (double.IsInfinity(bvaTest.StandardDevResult))
                failureMetadata = CreateFailureMetadata("Standard deviation of the test was infinity");
            else if (double.IsNaN(bvaTest.StandardDevResult))
                failureMetadata = CreateFailureMetadata("Standard deviation of the test was NaN");
            else if (bvaTest.StandardDevResult <= StandardDeviationThreshold)
                failureMetadata = CreateFailureMetadata("Standard deviation is at or below the threshold (" + StandardDeviationThreshold + ")");
            else if (double.IsInfinity(bvaTest.TransudationRateResult))
                failureMetadata = CreateFailureMetadata("Transudation rate of the test was infinity");
            else if (double.IsNaN(bvaTest.TransudationRateResult))
                failureMetadata = CreateFailureMetadata("Transudation rate of the test was NaN");
            else if (bvaTest.TransudationRateResult < 0)
                failureMetadata = CreateFailureMetadata("Transudation rate is negative");
            else if (bvaTest.Protocol < MinimumPatientProtocol)
                failureMetadata = CreateFailureMetadata("Patient protocol (number of composite samples) is below the threshold (" + MinimumPatientProtocol + ")");
            else if (TestContainsAtLeastOneSampleWithMismatchedExclusionStates(bvaTest))
                failureMetadata = CreateFailureMetadata("Test has at least one sample whose user-excluded and auto-excluded states do not match");
            else if (TestContainsAtLeastOneWhollyExcludedSample(bvaTest))
                failureMetadata = CreateFailureMetadata("Test has at least one sample that is wholly excluded");
            else if (TestContainsAtLeastOneSampleWithBothHematocritsExcluded(bvaTest))
                failureMetadata = CreateFailureMetadata("Test has at least one sample where both hematocrits are excluded");
            else if (TestHasTooFewPatientSamples(bvaTest, MaximumNumberOfExclusions))
                failureMetadata = CreateFailureMetadata("Test could potentially have fewer than " + MinimumSampleCountAfterExclusion +
                    " individual sample(s) remaining after possible exclusion of " + MaximumNumberOfExclusions + " sample(s)");
            else if (bvaTest.Status != TestStatus.Completed)
                failureMetadata = CreateFailureMetadata("Test is not marked as completed");

            return failureMetadata == null;
        }

        #endregion

        #region Internal worker methods

        /// <summary>
        /// Performs the actual work of finding points to exclude and excluding them until 
        /// the standard deviation is below the threshold or the maximum number of points
        /// has been reached.
        /// </summary>
        /// <param name="bvaTest">BVA test whose points should be evaluated for exclusion</param>
        /// <returns>Metadata about the exclusion results; also the given test may have one or
        /// more points auto-excluded and test properties (i.e., measured plasma and red cell volume,
        /// transudation rate, standard deviation) updated as well</returns>
        internal virtual AutomaticPointExclusionMetadata InternalExcludePoints(BVATest bvaTest)
        {
            // NOTE (gmazeroff): There may be some ways to make this method shorter. Most of the
            // standard refactoring techniques end up with nasty looking methods with ref/out
            // parameters returning non-intuitive types. This method has a lot of state to keep
            // track of throughout the algorithm.

            var bvaTestClone = CreateBvaTestClone(bvaTest);
            var calcEngineResults = _measuredCalcEngineService.GetAllResults(GenerateCalcEngineParameters(bvaTestClone));
            SetTestPropertiesBasedOnCalculationResults(bvaTestClone, calcEngineResults);
            var currentResidualStandardError = CalculateResidualStandardErrorOfTest(bvaTestClone, GenerateCollectionOfUnadjustedBloodVolumes(calcEngineResults, bvaTestClone));
            var originalResidualStandardError = currentResidualStandardError;
            var compositeCalcEngineResults = _measuredCalcEngineService.GetAllResults(GenerateCalcEngineParameters(bvaTestClone, true));
            var currentStandardDeviation = compositeCalcEngineResults.StdDev;
            var currentTransudationRate = compositeCalcEngineResults.Slope;
            var originalStandardDeviation = currentStandardDeviation;
            var originalTransudationRate = currentTransudationRate;

            var samplesToBeExcluded = new List<BVASample>();
            var numberOfPointsExcluded = 0;
            var intermediateMetadata = new List<string>();

            // Step 1: Try to exclude an individual sample
            // -------------------------------------------
            if (MaximumNumberOfExclusions == 0)
            {
                intermediateMetadata.Add("-No first individual sample evaluation done because the maximum number of exclusions is zero-");
                return CreateSuccessMetadata(numberOfPointsExcluded, currentResidualStandardError, currentStandardDeviation, currentTransudationRate, intermediateMetadata);
            }
            if (currentStandardDeviation < StandardDeviationThreshold)
            {
                intermediateMetadata.Add("-No first individual sample evaluation done because the standard deviation is below the threshold-");
                return CreateSuccessMetadata(numberOfPointsExcluded, currentResidualStandardError, currentStandardDeviation, currentTransudationRate, intermediateMetadata);
            }
            BVASample firstIndividualSampleToExclude;
            intermediateMetadata.AddRange(FindSampleToExclude(bvaTestClone, out firstIndividualSampleToExclude));
            intermediateMetadata.Add("-End of finding first individual sample-");

            firstIndividualSampleToExclude.IsCountAutoExcluded = true;
            samplesToBeExcluded.Add(firstIndividualSampleToExclude);

            calcEngineResults = _measuredCalcEngineService.GetAllResults(GenerateCalcEngineParameters(bvaTestClone));
            SetTestPropertiesBasedOnCalculationResults(bvaTestClone, calcEngineResults);
            currentResidualStandardError = CalculateResidualStandardErrorOfTest(bvaTestClone, GenerateCollectionOfUnadjustedBloodVolumes(calcEngineResults, bvaTestClone));
            calcEngineResults = _measuredCalcEngineService.GetAllResults(GenerateCalcEngineParameters(bvaTestClone, true));
            currentStandardDeviation = calcEngineResults.StdDev;  // Stddev in the UI comes from composites even though we need RSE from individuals
            currentTransudationRate = calcEngineResults.Slope;
            numberOfPointsExcluded++;

            
            // Step 2: Undo the first individual sample and see if a composite should be excluded.
            // -------------------------------------------
            if (currentStandardDeviation >= StandardDeviationThreshold)
            {
                // Temporarily undo the first exclusion.
                firstIndividualSampleToExclude.IsCountAutoExcluded = false;
                samplesToBeExcluded.Clear();
                numberOfPointsExcluded = 0;
                intermediateMetadata.Add("-Resetting previously excluded individual sample (" + 
                    firstIndividualSampleToExclude.Type + firstIndividualSampleToExclude.Range + 
                    ") to try finding a composite sample instead-");

                if (MaximumNumberOfExclusions < 2)
                {
                    intermediateMetadata.Add("-No first composite sample evaluation done because doing so would exceed the maximum number of exclusions-");
                    return CreateSuccessMetadata(numberOfPointsExcluded, originalResidualStandardError, originalStandardDeviation, currentTransudationRate, intermediateMetadata);
                }

                // Find a composite sample
                BVACompositeSample compositeSampleToExclude;
                intermediateMetadata.AddRange(FindCompositeSampleToExclude(bvaTestClone, out compositeSampleToExclude));
                intermediateMetadata.Add("-End of finding composite sample-");

                compositeSampleToExclude.IsWholeSampleExcluded = true;
                samplesToBeExcluded.Add(compositeSampleToExclude.SampleA);
                samplesToBeExcluded.Add(compositeSampleToExclude.SampleB);

                compositeCalcEngineResults = _measuredCalcEngineService.GetAllResults(GenerateCalcEngineParameters(bvaTestClone, true));
                SetTestPropertiesBasedOnCalculationResults(bvaTestClone, compositeCalcEngineResults);
                currentResidualStandardError = CalculateResidualStandardErrorOfTest(bvaTestClone, GenerateCollectionOfUnadjustedBloodVolumes(compositeCalcEngineResults, bvaTestClone, true));
                currentStandardDeviation = compositeCalcEngineResults.StdDev;
                currentTransudationRate = compositeCalcEngineResults.Slope;
                numberOfPointsExcluded += 2;

                // Undo if the standard deviation was only lowered because we excluded
                // something other than Sample 1.
                if (currentStandardDeviation < StandardDeviationThreshold && compositeSampleToExclude.SampleType != SampleType.Sample1)
                {
                    compositeSampleToExclude.IsWholeSampleExcluded = false;
                    samplesToBeExcluded.Clear();
                    numberOfPointsExcluded = 0;
                    currentStandardDeviation = originalStandardDeviation;
                    currentResidualStandardError = originalResidualStandardError;
                    currentTransudationRate = originalTransudationRate;
                    intermediateMetadata.Add("-Resetting previously excluded composite sample ("
                        + compositeSampleToExclude.SampleType + ") even though stddev is acceptable because it was not Sample 1-");
                }
            }

            // Step 3: Exclude individual samples until the standard deviation
            // is acceptable, or no more points can be excluded.
            // -------------------------------------------
            if (currentStandardDeviation >= StandardDeviationThreshold)
            {
                // Undo any composite sample exclusions
                foreach (var compositeSample in bvaTestClone.CompositeSamples)
                    compositeSample.IsWholeSampleExcluded = false;

                samplesToBeExcluded.Clear();
                numberOfPointsExcluded = 0;
                currentStandardDeviation = originalStandardDeviation;
                currentResidualStandardError = originalResidualStandardError;
                currentTransudationRate = originalTransudationRate;
                intermediateMetadata.Add("-Resetting previously excluded composite samples-");
            }

            while (numberOfPointsExcluded < MaximumNumberOfExclusions &&
                   currentStandardDeviation >= StandardDeviationThreshold)
            {
                BVASample sampleToExclude;
                intermediateMetadata.AddRange(FindSampleToExclude(bvaTestClone, out sampleToExclude));
                intermediateMetadata.Add("-End of finding individual sample-");

                sampleToExclude.IsCountAutoExcluded = true;
                samplesToBeExcluded.Add(sampleToExclude);

                calcEngineResults = _measuredCalcEngineService.GetAllResults(GenerateCalcEngineParameters(bvaTestClone));
                SetTestPropertiesBasedOnCalculationResults(bvaTestClone, calcEngineResults);
                currentResidualStandardError = CalculateResidualStandardErrorOfTest(bvaTestClone, GenerateCollectionOfUnadjustedBloodVolumes(calcEngineResults, bvaTestClone));
                calcEngineResults = _measuredCalcEngineService.GetAllResults(GenerateCalcEngineParameters(bvaTestClone, true));
                currentStandardDeviation = calcEngineResults.StdDev;  // Stddev in the UI comes from composites even though we need RSE from individuals
                currentTransudationRate = calcEngineResults.Slope;
                numberOfPointsExcluded++;
            }

            
            // Step 4: The standard deviation was still too high, so reset everything
            // and try composite samples
            // -------------------------------------------
            if (currentStandardDeviation >= StandardDeviationThreshold)
            {
                // Undo all individual sample exclusions
                foreach (var excludedSample in samplesToBeExcluded)
                    excludedSample.IsCountAutoExcluded = false;
                // Undo any composite sample exclusions
                foreach (var compositeSample in bvaTestClone.CompositeSamples)
                    compositeSample.IsWholeSampleExcluded = false;

                samplesToBeExcluded.Clear();
                numberOfPointsExcluded = 0;
                currentStandardDeviation = originalStandardDeviation;
                currentResidualStandardError = originalResidualStandardError;
                currentTransudationRate = originalTransudationRate;
                intermediateMetadata.Add("-Resetting previously excluded individual samples-");

                while (numberOfPointsExcluded < MaximumNumberOfExclusions &&
                   numberOfPointsExcluded + 2 <= MaximumNumberOfExclusions &&
                   currentStandardDeviation >= StandardDeviationThreshold)
                {
                    BVACompositeSample compositeSampleToExclude;
                    intermediateMetadata.AddRange(FindCompositeSampleToExclude(bvaTestClone, out compositeSampleToExclude));
                    intermediateMetadata.Add("-End of finding composite samples-");

                    compositeSampleToExclude.IsWholeSampleExcluded = true;
                    samplesToBeExcluded.Add(compositeSampleToExclude.SampleA);
                    samplesToBeExcluded.Add(compositeSampleToExclude.SampleB);

                    compositeCalcEngineResults = _measuredCalcEngineService.GetAllResults(GenerateCalcEngineParameters(bvaTestClone, true));
                    SetTestPropertiesBasedOnCalculationResults(bvaTestClone, compositeCalcEngineResults);
                    currentResidualStandardError = CalculateResidualStandardErrorOfTest(bvaTestClone, GenerateCollectionOfUnadjustedBloodVolumes(compositeCalcEngineResults, bvaTestClone, true));
                    currentStandardDeviation = compositeCalcEngineResults.StdDev;
                    currentTransudationRate = compositeCalcEngineResults.Slope;
                    numberOfPointsExcluded += 2;
                }
            }

            // Step 5: We've reached the end of the algorithm and still can't
            // get the standard deviation low enough. Reset any exclusions.
            if (currentStandardDeviation >= StandardDeviationThreshold)
            {
                intermediateMetadata.Add("-Reached the end of the point exclusion algorithm but cannot get the stddev low enough. No points will be excluded-");
                return CreateSuccessMetadata(0, currentResidualStandardError, currentStandardDeviation, currentTransudationRate, intermediateMetadata);
            }

            // Step 6: We've reached the end of the algorithm and the standard deviation
            // is acceptable, but the transudation rate is negative.
            if (currentStandardDeviation < StandardDeviationThreshold && currentTransudationRate < 0)
            {
                intermediateMetadata.Add("-Reached the end of the point exclusion algorithm with acceptable stddev, but the slope would be negative. No points will be excluded-");
                return CreateSuccessMetadata(0, currentResidualStandardError, currentStandardDeviation, currentTransudationRate, intermediateMetadata);
            }

            // Now that the samples to exclude have been identified, exclude them on the original test.
            foreach (var sample in samplesToBeExcluded)
            {
                var realSample = (from s in bvaTest.Samples where s.Range == sample.Range && s.Type == sample.Type select s).FirstOrDefault();
                if (realSample != null)
                    realSample.IsCountAutoExcluded = true;
            }

            return CreateSuccessMetadata(numberOfPointsExcluded, currentResidualStandardError, currentStandardDeviation, currentTransudationRate, intermediateMetadata);
        }

        /// <summary>
        /// Determines the sample in the given BVA test that yields the lowest residual
        /// standard error (RSE) when excluded from calculations. Ties for lowest RSE
        /// are handled via first-come priority.
        /// </summary>
        /// <param name="bvaTest">BVA test with candidate samples for exclusions</param>
        /// <param name="sampleToExclude">(Output parameter) Sample chosen for exclusion</param>
        /// <returns>Metadata about the computed RSE for each candidate sample</returns>
        /// <remarks>Any exceptions generated by dependent methods are not caught by
        /// this method.</remarks>
        internal virtual List<string> FindSampleToExclude(BVATest bvaTest, out BVASample sampleToExclude)
        {
            var nonExcludedPatientSamples = FindAllNonExcludedPatientSamples(bvaTest);
            var metadata = new List<string>();
            var lowestRse = double.MaxValue;
            sampleToExclude = null;

            foreach (var sample in nonExcludedPatientSamples)
            {
                sample.IsCountAutoExcluded = true;
                var calcEngineResults = _measuredCalcEngineService.GetAllResults(GenerateCalcEngineParameters(bvaTest));
                SetTestPropertiesBasedOnCalculationResults(bvaTest, calcEngineResults);
                var currentRse = CalculateResidualStandardErrorOfTest(bvaTest, GenerateCollectionOfUnadjustedBloodVolumes(calcEngineResults, bvaTest));
                var stdDevCalcEngineResults = _measuredCalcEngineService.GetAllResults(GenerateCalcEngineParameters(bvaTest, true));
                metadata.Add(String.Format(FindSampleToExcludeMetadataFormatString,
                                           sample.Type, sample.Range, currentRse, stdDevCalcEngineResults.StdDev, stdDevCalcEngineResults.TimeZeroBloodVolume));
                if (currentRse < lowestRse)
                {
                    lowestRse = currentRse;
                    sampleToExclude = sample;
                }
                sample.IsCountAutoExcluded = false; // Restore previous state
            }

            return metadata;
        }

        /// <summary>
        /// Determines the composite sample in the given BVA test that yields the lowest residual
        /// standard error (RSE) when excluded from calculations. Ties for lowest RSE
        /// are handled via first-come priority.
        /// </summary>
        /// <param name="bvaTest">BVA test with candidate samples for exclusions</param>
        /// <param name="compositeSampleToExclude">(Output parameter) Composite sample chosen for exclusion</param>
        /// <returns>Metadata about the computed RSE for each candidate composite sample</returns>
        /// <remarks>Any exceptions generated by dependent methods are not caught by
        /// this method.</remarks>
        internal virtual List<string> FindCompositeSampleToExclude(BVATest bvaTest, out BVACompositeSample compositeSampleToExclude)
        {
            var nonExcludedPatientSamples = FindAllNonExcludedCompositePatientSamples(bvaTest);
            var metadata = new List<string>();
            var lowestRse = double.MaxValue;
            compositeSampleToExclude = null;

            foreach (var sample in nonExcludedPatientSamples)
            {
                sample.IsWholeSampleExcluded = true;
                
                var calcEngineResults = _measuredCalcEngineService.GetAllResults(GenerateCalcEngineParameters(bvaTest, true));
                SetTestPropertiesBasedOnCalculationResults(bvaTest, calcEngineResults);
                var currentRse = CalculateResidualStandardErrorOfTest(bvaTest, GenerateCollectionOfUnadjustedBloodVolumes(calcEngineResults, bvaTest, true));
                metadata.Add(String.Format("Excluding composite sample {0} yields: RSE = {1:0.00000}, StdDev = {2:0.00000}, TotalBV = {3:0.0}",
                           sample.SampleType, currentRse, calcEngineResults.StdDev, calcEngineResults.TimeZeroBloodVolume));
                if (currentRse < lowestRse)
                {
                    lowestRse = currentRse;
                    compositeSampleToExclude = sample;
                }
                
                sample.IsWholeSampleExcluded = false;           // Restore previous state
            }

            return metadata;
        }

        internal static BVATest CreateBvaTestClone(BVATest bvaTest)
        {
            var clone = new BVATest(bvaTest.CurrentSampleSchema, null)
                {
                    Mode = bvaTest.Mode,
                    Protocol = bvaTest.Protocol, 
                    Tube = bvaTest.Tube, 
                    HasSufficientData = bvaTest.HasSufficientData,
                    BackgroundCount = bvaTest.BackgroundCount,
                    SampleDurationInSeconds = bvaTest.SampleDurationInSeconds,
                    BackgroundTimeInSeconds = bvaTest.BackgroundTimeInSeconds,
                    RefVolume = bvaTest.RefVolume,
                    StandardDevResult = bvaTest.StandardDevResult,
                    TransudationRateResult = bvaTest.TransudationRateResult,
                };
            clone.Volumes[BloodType.Measured].PlasmaCount = bvaTest.Volumes[BloodType.Measured].PlasmaCount;
            clone.Volumes[BloodType.Measured].RedCellCount = bvaTest.Volumes[BloodType.Measured].RedCellCount;
            foreach (var sample in bvaTest.Samples)
            {
                var originalSample = sample;
                var cloneSample = (from s in clone.Samples
                                   where s.Range == originalSample.Range && s.Type == originalSample.Type
                                   select s).FirstOrDefault();
                if (cloneSample == null) throw new NotSupportedException("Cannot find clone sample for " + sample);
                cloneSample.Counts = originalSample.Counts;
                cloneSample.ExecutionStatus = originalSample.ExecutionStatus;
                cloneSample.FullName = originalSample.FullName;
                cloneSample.Hematocrit = originalSample.Hematocrit;
                cloneSample.IsCountAutoExcluded = originalSample.IsCountAutoExcluded;
                cloneSample.IsCountExcluded = originalSample.IsCountExcluded;
                cloneSample.IsExcluded = originalSample.IsExcluded;
                cloneSample.IsHematocritExcluded = originalSample.IsHematocritExcluded;
                cloneSample.Position = originalSample.Position;
                cloneSample.PostInjectionTimeInSeconds = originalSample.PostInjectionTimeInSeconds;
                cloneSample.TestId = originalSample.TestId;
            }
            clone.ComputeTestAdjustedBackgroundCounts();
            return clone;
        }

        /// <summary>
        /// Calculates the residual standard error of the given volumes for the given BVA test
        /// using the given residual standard error calculator service.
        /// </summary>
        /// <param name="bvaTest">BVA test instance (for slope and y-intercept)</param>
        /// <param name="individualVolumes">Dictionary of sample-to-volume pairs</param>
        /// <returns>The computed residual standard error</returns>
        /// <exception cref="ArgumentNullException">If any of the arguments are null</exception>
        internal double CalculateResidualStandardErrorOfTest(BVATest bvaTest, Dictionary<BVASample, double> individualVolumes)
        {
            if (bvaTest == null)
                throw new ArgumentNullException("bvaTest");
            if (individualVolumes == null)
                throw new ArgumentNullException("individualVolumes");

            var postInjectionTimes = new List<double>();
            var sampleVolumes = new List<double>();
            var yIntercept = Math.Log(bvaTest.Volumes[BloodType.Measured].WholeCount);    // Jonathan Feldschuh's implementation -- ln(mL); original was just mL
            foreach (var individualVolume in individualVolumes.Keys)
            {
                // Jonathan Feldschuh's implementation -- minutes / ln(mL); original was seconds / mL
                postInjectionTimes.Add(individualVolume.PostInjectionTimeInSeconds / 60.0);
                sampleVolumes.Add(individualVolumes[individualVolume] > 0
                                      ? Math.Log(individualVolumes[individualVolume])
                                      : 0);
            }
            return _residualStandardErrorCalculator.CalculateResidualStandardError(postInjectionTimes, sampleVolumes, bvaTest.TransudationRateResult, yIntercept);
        }

        #region Static helpers

        /// <summary>
        /// Generates the appropriate input parameters for the measured calc engine service
        /// based on the given BVA test. Note that all individual samples are used so long
        /// as they are not wholly excluded, have counts that are not excluded and are
        /// non-negative, and have post-injection times greater than zero seconds.
        /// </summary>
        /// <param name="bvaTest">BVA test to be used for creating the calculation engine parameters</param>
        /// <param name="useCompositeSamples">True if composite samples are to be used; false if individual
        /// samples are to be used (default)</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">If the given test is null</exception>
        /// <exception cref="NotSupportedException">If BVATest.Tube is null</exception>
        internal static MeasuredCalcEngineParams GenerateCalcEngineParameters(BVATest bvaTest, bool useCompositeSamples = false)
        {
            if (bvaTest == null)
                throw new ArgumentNullException("bvaTest");
            if (bvaTest.Tube == null)
                throw new NotSupportedException("BVATest.Tube cannot be null");

            var standardA = (from s in bvaTest.Samples where s.Type == SampleType.Standard && s.Range == SampleRange.A select s).FirstOrDefault();
            var standardB = (from s in bvaTest.Samples where s.Type == SampleType.Standard && s.Range == SampleRange.B select s).FirstOrDefault();
            var baselineA = (from s in bvaTest.Samples where s.Type == SampleType.Baseline && s.Range == SampleRange.A select s).FirstOrDefault();
            var baselineB = (from s in bvaTest.Samples where s.Type == SampleType.Baseline && s.Range == SampleRange.B select s).FirstOrDefault();

            BloodVolumePoint[] pointsArray;
            if (useCompositeSamples)
                pointsArray = (from s in FindAllNonExcludedCompositePatientSamples(bvaTest)
                               where s.AverageCounts >= 0 && s.PostInjectionTimeInSeconds > 0
                               select new BloodVolumePoint(s.SampleType, s.IsWholeSampleExcluded, s.AreBothHematocritsExcluded, s.AreBothCountsExcluded,
                                                  s.PostInjectionTimeInSeconds, s.AverageCounts, s.AverageHematocrit, 0)).ToArray();
            else
            {
                pointsArray = (from s in FindAllNonExcludedPatientSamples(bvaTest)
                               where s.Counts >= 0 && s.PostInjectionTimeInSeconds > 0
                               select new BloodVolumePoint(s.InternalId, s.IsExcluded, s.IsHematocritExcluded, s.IsCountExcluded,
                                                  s.PostInjectionTimeInSeconds, s.Counts, s.Hematocrit, 0)).ToArray();
            }

            return new MeasuredCalcEngineParams
            {
                AnticoagulantFactor = bvaTest.Tube.AnticoagulantFactor,
                Background = bvaTest.DurationAdjustedBackgroundCounts,
                BaselineCounts = ComputeAverageSampleCount(baselineA, baselineB),
                BaselineHematocrit = ComputeAverageSampleHematocrit(baselineA, baselineB),
                ReferenceVolume = bvaTest.RefVolume,
                StandardCounts = ComputeAverageSampleCount(standardA, standardB),
                Points = pointsArray
            };
        }

        /// <summary>
        /// Computes the average count of the two given samples. If a sample is excluded or has
        /// counts less than or equal to zero, that sample's counts do not factor into the
        /// average. If neither sample is included nor has counts greater than zero, the 
        /// average is zero.
        /// </summary>
        /// <param name="sampleOne">First BVA sample</param>
        /// <param name="sampleTwo">Second BVA sample</param>
        /// <returns>Average count based on the summary text of this method</returns>
        /// <remarks>No rounding is performed when computing the average</remarks>
        /// <exception cref="ArgumentNullException">If either of the arguments are null</exception>
        internal static double ComputeAverageSampleCount(BVASample sampleOne, BVASample sampleTwo)
        {
            if (sampleOne == null)
                throw new ArgumentNullException("sampleOne");
            if (sampleTwo == null)
                throw new ArgumentNullException("sampleTwo");

            var numSamplesUsed = 0;
            double totalCounts = 0;

            if (!sampleOne.IsCountExcluded && sampleOne.Counts > 0)
            {
                numSamplesUsed++;
                totalCounts += sampleOne.Counts;
            }

            if (!sampleTwo.IsCountExcluded && sampleTwo.Counts > 0)
            {
                numSamplesUsed++;
                totalCounts += sampleTwo.Counts;
            }

            if (numSamplesUsed == 0)
                return 0;

            return totalCounts / numSamplesUsed;
        }

        /// <summary>
        /// Computes the average hematocrit of the two given samples. If a hematocrit is excluded 
        /// or has a value less than or equal to zero, that sample's hematocrit does not factor into 
        /// the average. If neither sample has the hematocrit included nor has hematocrits greater
        ///  than zero, the average is -1.
        /// </summary>
        /// <param name="sampleOne">First BVA sample</param>
        /// <param name="sampleTwo">Second BVA sample</param>
        /// <returns>Average hematocrit based on the summary text of this method</returns>
        /// <exception cref="ArgumentNullException">If either of the arguments are null</exception>
        internal static double ComputeAverageSampleHematocrit(BVASample sampleOne, BVASample sampleTwo)
        {
            if (sampleOne == null)
                throw new ArgumentNullException("sampleOne");
            if (sampleTwo == null)
                throw new ArgumentNullException("sampleTwo");

            var numSamplesUsed = 0;
            double totalHematocrit = 0;

            if (!sampleOne.IsHematocritExcluded && sampleOne.Hematocrit > 0)
            {
                numSamplesUsed++;
                totalHematocrit += sampleOne.Hematocrit;
            }

            if (!sampleTwo.IsHematocritExcluded && sampleTwo.Hematocrit > 0)
            {
                numSamplesUsed++;
                totalHematocrit += sampleTwo.Hematocrit;
            }

            if (numSamplesUsed == 0)
                return -1;

            return totalHematocrit / numSamplesUsed;
        }

        /// <summary>
        /// Generates a mapping of blood volume samples to their unadjusted blood volumes.
        /// </summary>
        /// <param name="calcEngineResults">Measured blood volume results for the given test</param>
        /// <param name="bvaTest">BVA test that corresponds the given results</param>
        /// <param name="useCompositeSamples">True if the calc engine results pertain to
        /// composite samples (because the internal ID mechanism is different)</param>
        /// <returns>Dictionary of samples-to-volumes</returns>
        /// <exception cref="ArgumentNullException">If either argument is null</exception>
        /// <exception cref="NotSupportedException">If the collection of unadjusted blood volumes in the
        /// calc engine results is null, if the unadjusted blood volume collection contains a sample ID 
        /// not in the given test, or if any of the unadjusted blood volumes is negative</exception>
        /// <exception cref="NullReferenceException">If the collection of unadjusted blood volumes in the
        /// calc engine results contains a null value</exception>
        internal static Dictionary<BVASample, double> GenerateCollectionOfUnadjustedBloodVolumes(MeasuredCalcEngineResults calcEngineResults, BVATest bvaTest,
            bool useCompositeSamples = false)
        {
            if (calcEngineResults == null)
                throw new ArgumentNullException("calcEngineResults");
            if (bvaTest == null)
                throw new ArgumentNullException("bvaTest");
            if (calcEngineResults.UnadjustedBloodVolumeResults == null)
                throw new NotSupportedException("Collection of unadjusted blood volume results cannot be null");

            var volumeCollection = new Dictionary<BVASample, double>();
            foreach (var volume in calcEngineResults.UnadjustedBloodVolumeResults)
            {
                BVASample targetSample = null;
                if (!useCompositeSamples)
                    targetSample = (from s in bvaTest.Samples where s.InternalId == (Guid)volume.UniqueId select s).FirstOrDefault();
                else
                {
                    var targetCompositeSample = (from s in bvaTest.CompositeSamples where s.SampleType == (SampleType)volume.UniqueId select s).FirstOrDefault();
                    // We return the A-side because BVACompositeSample isn't a subtype of any sample (it's an EvaluatableObject)
                    if (targetCompositeSample != null)
                        targetSample = targetCompositeSample.SampleA;
                }
                if (targetSample == null)
                    throw new NotSupportedException("The given test has no sample with ID '" + volume.UniqueId + "'");

                volumeCollection.Add(targetSample, volume.UnadjustedBloodVolume);
            }

            return volumeCollection;
        }

        /// <summary>
        /// Sets the standard deviation, transudation rate, measured plasma volume, and measured red cell
        /// volume on the given test based on the given measured blood volume calculation results.
        /// </summary>
        /// <param name="bvaTest">BVA test whose properties are to be set</param>
        /// <param name="calcEngineResults">Measured blood volume calculation results</param>
        /// <exception cref="ArgumentNullException">If either argument is null</exception>
        /// <exception cref="NotSupportedException">If the plasma volume, red cell volume, or
        /// standard deviation in the results are NaN</exception>
        internal static void SetTestPropertiesBasedOnCalculationResults(BVATest bvaTest, MeasuredCalcEngineResults calcEngineResults)
        {
            if (bvaTest == null)
                throw new ArgumentNullException("bvaTest");
            if (calcEngineResults == null)
                throw new ArgumentNullException("calcEngineResults");
            if (double.IsNaN(calcEngineResults.TimeZeroPlasmaVolume))
                throw new NotSupportedException("Time-zero plasma volume cannot be NaN");
            if (double.IsNaN(calcEngineResults.TimeZeroRedCellVolume))
                throw new NotSupportedException("Time-zero red cell volume cannot be NaN");
            if (double.IsNaN(calcEngineResults.StdDev))
                throw new NotSupportedException("Standard deviation cannot be NaN");

            bvaTest.StandardDevResult = calcEngineResults.StdDev;
            bvaTest.TransudationRateResult = calcEngineResults.Slope;
            bvaTest.Volumes[BloodType.Measured].PlasmaCount = (int)Math.Round(calcEngineResults.TimeZeroPlasmaVolume, 0);
            bvaTest.Volumes[BloodType.Measured].RedCellCount = (int)Math.Round(calcEngineResults.TimeZeroRedCellVolume, 0);
        }

        /// <summary>
        /// Determines whether the given sample is a patient sample (i.e., not a baseline or standard).
        /// </summary>
        /// <param name="sample">BVA sample to evaluate</param>
        /// <returns>True if the given sample is a patient sample; false otherwise</returns>
        /// <exception cref="ArgumentNullException">If the given sample is null</exception>
        internal static bool IsPatientSample(BVASample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");

            switch (sample.Type)
            {
                case SampleType.Sample1:
                case SampleType.Sample2:
                case SampleType.Sample3:
                case SampleType.Sample4:
                case SampleType.Sample5:
                case SampleType.Sample6:
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Determines whether the given composite sample is a patient sample (i.e., not a baseline or standard).
        /// </summary>
        /// <param name="sample">BVA composite sample to evaluate</param>
        /// <returns>True if the given composite sample is a patient sample; false otherwise</returns>
        /// <exception cref="ArgumentNullException">If the given sample is null</exception>
        internal static bool IsCompositePatientSample(BVACompositeSample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");

            switch (sample.SampleType)
            {
                case SampleType.Sample1:
                case SampleType.Sample2:
                case SampleType.Sample3:
                case SampleType.Sample4:
                case SampleType.Sample5:
                case SampleType.Sample6:
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Returns the collection of all non-excluded patient samples from the given BVA test.
        /// </summary>
        /// <param name="bvaTest">BVA test instance</param>
        /// <returns>Collection of all non-excluded patient samples; empty collection if 
        /// all samples are excluded</returns>
        /// <remarks>Uses IsExcluded and IsCountExcluded, not IsCountAutoExcluded</remarks>
        internal static IEnumerable<BVASample> FindAllNonExcludedPatientSamples(BVATest bvaTest)
        {
            if (bvaTest == null)
                throw new ArgumentNullException("bvaTest");

            var nonExcludedPatientSamples = from s in bvaTest.Samples
                                            where IsPatientSample(s) && !s.IsCountExcluded && !s.IsExcluded
                                            select s;
            return nonExcludedPatientSamples;
        }

        /// <summary>
        /// Returns the collection of all non-excluded composite patient samples from the given BVA test.
        /// </summary>
        /// <param name="bvaTest">BVA test instance</param>
        /// <returns>Collection of all non-excluded composite patient samples; empty collection if 
        /// all samples are excluded</returns>
        /// <remarks>Uses IsExcluded and IsCountExcluded, not IsCountAutoExcluded</remarks>
        internal static IEnumerable<BVACompositeSample> FindAllNonExcludedCompositePatientSamples(BVATest bvaTest)
        {
            if (bvaTest == null)
                throw new ArgumentNullException("bvaTest");

            var nonExcludedCompositePatientSamples = from s in bvaTest.CompositeSamples
                                            where IsCompositePatientSample(s) && !s.AreBothCountsExcluded && !s.IsWholeSampleExcluded
                                            select s;
            return nonExcludedCompositePatientSamples;
        }

        #endregion

        #endregion

        #region Private helpers

        /// <summary>
        /// Handler for ISettingsManager.SettingsChanged event that responds to settings pertaining
        /// to automatic point exclusion.
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="settingsChangedEventArgs">Arguments for the event</param>
        private void OnSettingsChanged(object sender, SettingsChangedEventArgs settingsChangedEventArgs)
        {
            if (settingsChangedEventArgs.ChangedSettingIdentifiers.Contains(SettingKeys.BvaAutomaticPointExclusionEnabled))
                SetEnabledState();
            if (settingsChangedEventArgs.ChangedSettingIdentifiers.Contains(SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions))
                SetMaximumNumberOfExclusions();
            if (settingsChangedEventArgs.ChangedSettingIdentifiers.Contains(SettingKeys.BvaAutomaticPointExclusionMinProtocol))
                SetMinimumPatientProtocol();
            if (settingsChangedEventArgs.ChangedSettingIdentifiers.Contains(SettingKeys.BvaAutomaticPointExclusionRseThreshold))
                SetResidualStandardErrorThreshold();
            if (settingsChangedEventArgs.ChangedSettingIdentifiers.Contains(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold))
                SetStandardDeviationThreshold();
        }

        /// <summary>
        /// Sets the enabled state of the service based on the system setting.
        /// </summary>
        private void SetEnabledState()
        {
            IsEnabled = _settingsManager.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled);
        }

        /// <summary>
        /// Sets the maximum number of exclusions based on the system setting.
        /// </summary>
        private void SetMaximumNumberOfExclusions()
        {
            var newMaxNumberOfExclusions = _settingsManager.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions);
            if (newMaxNumberOfExclusions < 0)
                throw new NotSupportedException("Maximum number of exclusions cannot be negative");
            MaximumNumberOfExclusions = newMaxNumberOfExclusions;
        }

        /// <summary>
        /// Sets the minimum patient protocol based on the system setting.
        /// </summary>
        private void SetMinimumPatientProtocol()
        {
            var newMinProtocol = _settingsManager.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMinProtocol);
            if (newMinProtocol < 0)
                throw new NotSupportedException("Minimum patient protocol cannot be negative");
            MinimumPatientProtocol = newMinProtocol;
        }

        /// <summary>
        /// Sets the residual standard error threshold based on the system setting.
        /// </summary>
        private void SetResidualStandardErrorThreshold()
        {
            var newRseThreshold = _settingsManager.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionRseThreshold);
            if (newRseThreshold < 0)
                throw new NotSupportedException("Residual standard error threshold cannot be negative");
            ResidualStandardErrorThreshold = newRseThreshold;
        }

        /// <summary>
        /// Sets the standard deviation threshold based on the system setting.
        /// </summary>
        private void SetStandardDeviationThreshold()
        {
            StandardDeviationThreshold = _settingsManager.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold);
        }

        /// <summary>
        /// Creates service metadata pertaining to a failure
        /// </summary>
        /// <param name="failureMessage">Message about the failure</param>
        /// <returns>Metadata corresponding to a service failure</returns>
        private static AutomaticPointExclusionMetadata CreateFailureMetadata(string failureMessage)
        {
            return new AutomaticPointExclusionMetadata
            {
                NumberOfPointsExcluded = 0,
                WasServiceRunToCompletion = false,
                ReasonServiceNotRunToCompletion = failureMessage
            };
        }

        /// <summary>
        /// Creates service metadata pertaining to a successful run of the algorithm
        /// </summary>
        /// <param name="numberOfPointsExcluded">Number of points excluded</param>
        /// <param name="lastComputedRse">Last known residual standard error</param>
        /// <param name="lastComputedStandardDeviation">Last known standard deviation</param>
        /// <param name="lastComputedTransudationRate">Last known transudation rate</param>
        /// <param name="intermediateMetadata">Collection of metadata strings that
        /// represent information captured during the algorithm's execution</param>
        /// <returns>Metadata corresponding to a successful run of the service; intermediate
        /// metadata items are separated by semicolons</returns>
        private static AutomaticPointExclusionMetadata CreateSuccessMetadata(int numberOfPointsExcluded, double lastComputedRse, 
            double lastComputedStandardDeviation, double lastComputedTransudationRate, IEnumerable<string> intermediateMetadata)
        {
            var intermediateMetadataAsString = new StringBuilder();
            foreach (var metadataString in intermediateMetadata)
                intermediateMetadataAsString.Append(metadataString + ";");

            return new AutomaticPointExclusionMetadata
                {
                    NumberOfPointsExcluded = numberOfPointsExcluded,
                    WasServiceRunToCompletion = true,
                    ReasonServiceNotRunToCompletion = string.Empty,
                    IntermediateMetadata = intermediateMetadataAsString.ToString(),
                    LastComputedResidualStandardError = lastComputedRse,
                    LastComputedStandardDeviation = lastComputedStandardDeviation,
                    LastComputedTransudationRate = lastComputedTransudationRate
                };
        }

        /// <summary>
        /// Determines whether or not the given test has any patient samples whose counts'
        /// exclusion states do not match. That is, counts were user-excluded but not
        /// auto-excluded (meaning the user chose to exclude it), or counts were 
        /// auto-excluded but not user-excluded (meaning the user undid the auto-exclusion).
        /// </summary>
        /// <param name="bvaTest">BVA test to evaluate</param>
        /// <returns>True if at least one patient sample has mismatched exclusion states</returns>
        private static bool TestContainsAtLeastOneSampleWithMismatchedExclusionStates(BVATest bvaTest)
        {
            return (from s in bvaTest.Samples where s.Type != SampleType.Standard && s.IsCountAutoExcluded != s.IsCountExcluded select s).Any();
        }

        /// <summary>
        /// Determines whether or not the given test has at least one wholly excluded
        /// (i.e., hematocrit and count) sample.
        /// </summary>
        /// <param name="bvaTest">BVA test to evaluate</param>
        /// <returns>True if at least one patient sample is wholly excluded</returns>
        private static bool TestContainsAtLeastOneWhollyExcludedSample(BVATest bvaTest)
        {
            return (from s in bvaTest.Samples where s.IsExcluded select s).Any();
        }

        /// <summary>
        /// Determines whether or not the given test has any patient samples whose A- and
        /// B-side hematocrits have been excluded/
        /// </summary>
        /// <param name="bvaTest">BVA test to evaluate</param>
        /// <returns>True if at least one A- and B-side patient sample pair where
        /// both hematocrits have been excluded; false otherwise</returns>
        private static bool TestContainsAtLeastOneSampleWithBothHematocritsExcluded(BVATest bvaTest)
        {
            return (from aSideSample in bvaTest.Samples
                    from bSideSample in bvaTest.Samples
                    where aSideSample.Range == SampleRange.A && bSideSample.Range == SampleRange.B && aSideSample.IsHematocritExcluded && bSideSample.IsHematocritExcluded
                    select aSideSample).Any();
        }

        /// <summary>
        /// Determines whether or not the given test could end up having too many points
        /// excluded.
        /// </summary>
        /// <param name="bvaTest">BVA test to evaluate</param>
        /// <param name="maximumNumberOfExclusions">Maximum number of individual samples
        /// that can be excluded</param>
        /// <returns>True if the test has too few samples; false otherwise</returns>
        private static bool TestHasTooFewPatientSamples(BVATest bvaTest, int maximumNumberOfExclusions)
        {
            return (bvaTest.Protocol * 2 - maximumNumberOfExclusions) < MinimumSampleCountAfterExclusion;
        }

        /// <summary>
        /// Determines if the given test has any incomplete fields, indicating that data entry has
        /// not yet been completed.
        /// </summary>
        /// <param name="bvaTest">BVA test to evaluate</param>
        /// <param name="incompleteFields">(Out) Summary of the incomplete fields</param>
        /// <returns>True if the test has incomplete fields; false otherwise</returns>
        private static bool TestHasIncompleteFields(BVATest bvaTest, out string incompleteFields)
        {
            var incompleteFieldsBuilder = new StringBuilder();

            var numMissingPostInjectionTimes = bvaTest.PatientCompositeSamples.Count(s => s.PostInjectionTimeInSeconds < 0);
            if (numMissingPostInjectionTimes > 0) incompleteFieldsBuilder.Append("Post-injection times (" + numMissingPostInjectionTimes + "); ");
            
            var numMissingHematocritAValues = bvaTest.PatientCompositeSamples.Count(s => s.HematocritA < 0);
            if (numMissingHematocritAValues > 0) incompleteFieldsBuilder.Append("Hematocrits (" + numMissingHematocritAValues + "); ");
            var numMissingHematocritBValues = bvaTest.HematocritFill == HematocritFillType.TwoPerSample ? bvaTest.PatientCompositeSamples.Count(s => s.HematocritB < 0) : 0;
            if (numMissingHematocritBValues > 0) incompleteFieldsBuilder.Append("B-side Hematocrits (" + numMissingHematocritBValues + "); ");
            var numMissingBaselineHematocrits = bvaTest.BaselineSamples.Count(s => s.Hematocrit < 0);
            if (numMissingBaselineHematocrits > 0) incompleteFieldsBuilder.Append("Baseline hematocrits (" + numMissingBaselineHematocrits + "); ");

            var numMissingACounts = bvaTest.PatientCompositeSamples.Count(s => s.CountsA < 0);
            if (numMissingACounts > 0) incompleteFieldsBuilder.Append("A-side counts (" + numMissingACounts + "); ");
            var numMissingBCounts = bvaTest.PatientCompositeSamples.Count(s => s.CountsB < 0);
            if (numMissingBCounts > 0) incompleteFieldsBuilder.Append("B-side counts (" + numMissingBCounts + "); ");
            if (bvaTest.BackgroundCount < 0) incompleteFieldsBuilder.Append("Background counts;");
            var numMissingStandardCounts = bvaTest.StandardSamples.Count(s => s.Counts < 0);
            if (numMissingStandardCounts > 0) incompleteFieldsBuilder.Append("Standard counts (" + numMissingStandardCounts + "); ");
            var numMissingBaselineCounts = bvaTest.BaselineSamples.Count(s => s.Counts < 0);
            if (numMissingBaselineCounts > 0) incompleteFieldsBuilder.Append("Baseline counts (" + numMissingBaselineCounts + "); ");
            
            incompleteFields = incompleteFieldsBuilder.ToString();
            return !String.IsNullOrEmpty(incompleteFields);
        }

        #endregion
    }
}
