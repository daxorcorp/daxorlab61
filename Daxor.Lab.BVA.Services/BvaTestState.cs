﻿using System.Collections.Generic;

namespace Daxor.Lab.BVA.Services
{
    /// <summary>
    /// Represents the state of several parts of the BVA test with respect to point exclusion
    /// so that the test state can be restored if needed
    /// </summary>
    public class BvaTestState
    {
        /// <summary>
        /// Gets or sets the measured plasma volume (in mL)
        /// </summary>
        public int MeasuredPlasmaVolume { get; set; }

        /// <summary>
        /// Gets or sets the measured red cell volume (in mL)
        /// </summary>
        public int MeasuredRedCellVolume { get; set; }
       
        /// <summary>
        /// Gets or sets the list of sample exclusion states
        /// </summary>
        public List<SampleExclusionState> SampleExclusionStates { get; set; }

        /// <summary>
        /// Gets or sets the standard deviation
        /// </summary>
        public double StandardDeviation { get; set; }

        /// <summary>
        /// Gets or sets the transudation rate
        /// </summary>
        public double TransudationRate { get; set; }
    }
}
