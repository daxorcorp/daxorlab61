﻿using System.Linq;
using Daxor.Lab.Domain.Evaluation;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.RuleFramework;
using Daxor.Lab.Infrastructure.RuleFramework.Rules;
using Daxor.Lab.MessageManager.Interfaces;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.BVA.Services
{
    public class BvaResultAlertService : IBvaResultAlertService
    {
        #region Private fields

        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly ILoggerFacade _logger;
        private readonly IMessageManager _messageManager;
        private const string MethodName = "BvaResultAlertService.ShowAlertMessageBox()";

        #endregion

        #region Ctor

        /// <summary>
        /// Creates a new BvaResultAlertService that facilitates displaying message boxes with
        /// information about BVA result alerts.
        /// </summary>
        /// <param name="messageBoxDispatcher">Service used to display message boxes</param>
        /// <param name="logger">Logger for providing details about errors</param>
        /// <param name="messageManager">MessageManager for providing the message to display</param>
        public BvaResultAlertService(IMessageBoxDispatcher messageBoxDispatcher, ILoggerFacade logger, IMessageManager messageManager)
        {
            _messageBoxDispatcher = messageBoxDispatcher;
            _logger = logger;
            _messageManager = messageManager;
        }

        #endregion

        /// <summary>
        /// Show a message box with information about an alert for the given entity.
        /// </summary>
        /// <param name="entity">Validatable entity with a validation error</param>
        /// <param name="propertyName">Property name that is causing the alert to exist</param>
        /// <param name="causeMessage">String to display in the "cause" section of the message
        /// (e.g., what value caused the alert to occur)</param>
        /// <remarks>This method has no effect if the following conditions exist:
        /// the argument is null, the entity has no validation errors, the entity
        /// has a null validation error, the entity's failed rule isn't of type
        /// IExposeAlertResolutionMetadata, the entity's resolution metadata is
        /// null.
        /// </remarks>
        public void ShowAlertMessageBox(EvaluatableObject entity, string propertyName, string causeMessage)
        {
            if (entity == null)
            {
                _logger.Log(MethodName + " was given a null entity", Category.Exception, Priority.High);
                return;
            }

            var brokenRule = entity.BrokenRules.FirstOrDefault(x => x.PropertyName == propertyName);
            if (brokenRule == null)
            {
                _logger.Log(MethodName + " was given an entity with no validation errors", Category.Exception, Priority.High);
                return;
            }

            var metadata = brokenRule as IExposeAlertResolutionMetadata;
            if (metadata == null)
            {
                _logger.Log(MethodName + " was given an entity whose failed rule was not of type IExposeAlertResolutionMetadata", Category.Exception, Priority.High);
                return;
            }

            var resolutionDetails = metadata.Metadata;
            if (resolutionDetails == null)
            {
                _logger.Log(MethodName + " was given an entity that has no AlertResolutionMetadata", Category.Exception, Priority.High);
                return;
            }

            var err = new RuleViolationMetadata
            {
                VisualBrush = null,
                Cause = causeMessage,
                Resolution = resolutionDetails.Resolution + " [Alert: " + _messageManager.GetMessage(resolutionDetails.AlertId).MessageId + "]",
                Symptoms = resolutionDetails.ErrorMessage,
                HelpType = resolutionDetails.ResolutionTypeAsEnum
            };

            _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information, err, "Alert", MessageBoxChoiceSet.Close);
            _logger.Log(err.Resolution.ToString(), Category.Info, Priority.Low);
        }
    }
}
