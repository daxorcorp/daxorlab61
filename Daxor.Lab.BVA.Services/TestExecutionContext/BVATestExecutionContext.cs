﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Domain.Services;
using Daxor.Lab.Infrastructure.Constants;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.BVA.Services.TestExecutionContext
{
    /// <summary>
    /// Real time test execution context.
    /// Internal no-one should have access to this
    /// </summary>
    internal class BvaTestExecutionContext : TestExecutionContext<BVATest>
    {
        #region Fields
        
        private readonly string _friendlyNameForBackgroundSample;
        readonly TestExecutionContextMetadata _metadata;

        #endregion

        #region Ctor

        public BvaTestExecutionContext(BVATest test, IDetector detector, ICalibrationCurve calibrationCurve, ILoggerFacade logger,
                                       ISpectroscopyService spectroscopyService, IBVADataService bvaDataService, 
                                       [Dependency(AppModuleIds.BloodVolumeAnalysis)] IRuleEngine ruleEngine) : base(test, detector)
        {

            if (calibrationCurve == null)
                throw new ArgumentNullException("calibrationCurve");
            if (logger == null)
                throw new ArgumentNullException("logger");
            if (spectroscopyService == null)
                throw new ArgumentNullException("spectroscopyService");
            if (bvaDataService == null)
                throw new ArgumentNullException("bvaDataService");
            if (ruleEngine == null)
                throw new ArgumentNullException("ruleEngine");

            SpectroscopyService = spectroscopyService;
            BvaDataService = bvaDataService;
            CalibrationCurve = calibrationCurve;
            Logger = logger;
            RuleEngine = ruleEngine;

            _metadata = new TestExecutionContextMetadata(AppModuleIds.BloodVolumeAnalysis)
                { DisplayTestName = "BVA Test" };
            Metadata = _metadata;

            int backroundPosition = ExecutingTestOfSpecificType.CurrentSampleSchema.GetPosition(SampleType.Background, SampleRange.Undefined);
            _friendlyNameForBackgroundSample = ExecutingTestOfSpecificType.CurrentSampleSchema.GetLayoutItem(backroundPosition).FriendlyName;
           
            // Set preset live time for every sample
            test.Samples.ToList().ForEach(s => s.PresetLiveTimeInSeconds = ExecutingTestOfSpecificType.SampleDurationInSeconds);
            BackgroundSample = new BVASample(backroundPosition, ruleEngine);
        }

        #endregion

        #region Properties

        public ICalibrationCurve CalibrationCurve { get; private set; }

        public ILoggerFacade Logger { get; private set; }

        public ISpectroscopyService SpectroscopyService { get; private set; }

        public IBVADataService BvaDataService { get; private set; }

        public IRuleEngine RuleEngine { get; private set; }

        #endregion

        #region TestExecutionContextBase overrides

        public override IEnumerable<ISample> OrderedSamplesExcludingBackground
        {
            get
            {
                return (from s in ExecutingTestOfSpecificType.Samples orderby s.Position select s).ToList();
            }
        }

        public override ISample GetNextSampleToExecute()
        {
            //Implies we never had any invokers
            if (ExecutingSample == null)
            {
                var nextSample = (from s in ExecutingTestOfSpecificType.Samples orderby s.Position select s).FirstOrDefault();
                _metadata.FriendlyExecutingSampleName = nextSample != null ? nextSample.DisplayName : String.Empty;

                return nextSample;
            }
            else
            {
                //We are look for sample which position is + 1 from ExecutingSample
                var orderedSamples = (from s in ExecutingTestOfSpecificType.Samples orderby s.Position select s).ToList();
                var indexOfExecutingSample = orderedSamples.IndexOf((BVASample)ExecutingSample);
                ISample nextSample = orderedSamples.ElementAtOrDefault(indexOfExecutingSample + 1);
                _metadata.FriendlyExecutingSampleName = nextSample != null ? nextSample.DisplayName : String.Empty;

                return nextSample;
            }
        }
        
        public override void ProcessBackground(int integral, int acqusitionTimeInSeconds, bool isAcquisitionCompleted, Spectrum spectrum = null, bool isOverridden = false)
        {
            //Someone overrode the background
            if (isOverridden)
            {
                ExecutingTestOfSpecificType.BackgroundCount = integral;
                ExecutingTestOfSpecificType.BackgroundTimeInSeconds = acqusitionTimeInSeconds;
                ExecutingTestOfSpecificType.HasGoodBackground = true;
            }
            else
            {
                ExecutingTestOfSpecificType.BackgroundCount = integral;
                ExecutingTestOfSpecificType.BackgroundTimeInSeconds = acqusitionTimeInSeconds;
                ExecutingTestOfSpecificType.HasGoodBackground = isAcquisitionCompleted;
            }

            _metadata.FriendlyExecutingSampleName = _friendlyNameForBackgroundSample;
            _metadata.UpdatedSampleCounts = ExecutingTestOfSpecificType.DurationAdjustedBackgroundCounts;
        }

        public override int ComputeSampleAcquisitionTimeInMilliseconds(ISample sample)
        {
            // For BVA tests, each item to be counted always has the same duration
            return ExecutingTestOfSpecificType.SampleDurationInSeconds * (int) TimeConstants.MillisecondsPerSecond;
        }

        public override void PerformAcquisition(ISample acquireSample, Action<ISample> completed)
        {
            _metadata.UpdatedSampleCounts = 0;
            _metadata.FriendlyExecutingSampleName = acquireSample.DisplayName;

            base.PerformAcquisition(acquireSample, completed);
        }
       
        protected override void ProcessSpectrum(ISample sample, Spectrum spectrum, bool isAcquisitionCompleted = false)
        {
            try
            {
                //Default spectrum processing
                base.ProcessSpectrum(sample, spectrum, isAcquisitionCompleted);
                
                var bvaSample = sample as BVASample;
                if (bvaSample != null)
                {
                    bvaSample.IsCounting = !isAcquisitionCompleted;

                    var lChannel = CalibrationCurve.GetChannel(BvaDomainConstants.I131LowerEnergyBound);
                    var hChannel = CalibrationCurve.GetChannel(BvaDomainConstants.I131UpperEnergyBound);

                    bvaSample.Counts = (int) SpectroscopyService.ComputeIntegral(bvaSample.Spectrum.SpectrumArray, lChannel, hChannel);
                    _metadata.UpdatedSampleCounts = bvaSample.Counts;

                    //If we are done acquiring
                    if (isAcquisitionCompleted)
                    {
                        bvaSample.FullWidthHalfMax = SpectroscopyService.ComputeFullWidthHalfMax(bvaSample.Spectrum.SpectrumArray, lChannel, hChannel);
                        bvaSample.FullWidthTenthMax = SpectroscopyService.ComputeFullWidthTenthMax(bvaSample.Spectrum.SpectrumArray, lChannel, hChannel);
                        bvaSample.Centroid = SpectroscopyService.ComputeCentroid(bvaSample.Spectrum.SpectrumArray, lChannel, hChannel);

                        bvaSample.ExecutionStatus = SampleExecutionStatus.Completed;

                        SaveTheTest("A sample (" + bvaSample.DisplayName + ") counts have been acquired for test: " + ExecutingTestOfSpecificType.InternalId);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(String.Format("BVA Test Execution Context failed to process a spectrum: {0} {1}", ex.Message, ex.StackTrace), Category.Exception, Priority.High);
                throw;
            }
        }

        protected override void OnExecutingTestStatusChanged()
        {
            switch (ExecutingTestStatus)
            {
                case TestStatus.Aborted:
#if DEBUG
                    Logger.Log("BVA Test Execution Context status is being marked as aborted. Stack trace: " + new StackTrace(),
                        Category.Info, Priority.High);
#endif
                    ExecutingTestOfSpecificType.Status = TestStatus.Aborted;
                    try
                    {
                        Detector.StopAcquiring();
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(String.Format("Could not stop aquiring: {0}\n\nStack Trace:\n{1}", ex.Message, ex.StackTrace), Category.Exception, Priority.High);
                        //throw;
                    }
                    SaveTheTest("The test: " + ExecutingTestOfSpecificType.InternalId + " has been aborted");    
                    break;
                case TestStatus.Completed:
                    ExecutingTestOfSpecificType.Status = TestStatus.Completed;
                    SaveTheTest( "The test: "+ ExecutingTestOfSpecificType.InternalId + " is now complete");
                    break;
                case TestStatus.Running:
                    ExecutingTestOfSpecificType.Status = TestStatus.Running;
                    SaveTheTest("The test: " + ExecutingTestOfSpecificType.InternalId + " has started running");
                    break;
            }
        }

        protected void SaveTheTest(string context)
        {
            Logger.Log(context, Category.Info, Priority.Low);

            BvaDataService.SaveTest(ExecutingTestOfSpecificType);
        }

        #endregion
    }
}
