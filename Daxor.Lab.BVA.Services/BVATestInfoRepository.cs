﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Database.CommonBLLs;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Database.DataAccess;

namespace Daxor.Lab.BVA.Services
{
    public class BVATestInfoRepository : ITestInfoRepository<BVATestInfoDTO>
    {
        public IEnumerable<BVATestInfoDTO> SelectAll()
        {
            return BvaTestListAccess.SelectList();
        }
    }
}
