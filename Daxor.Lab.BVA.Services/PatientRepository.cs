﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.Utility;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.BVA.Services
{
    public class PatientRepository : IRepository<PatientRecord>
    {
        private readonly ILoggerFacade _logger;

        public PatientRepository(ILoggerFacade logger)
        {
            _logger = logger;
        }
        public IEnumerable<PatientRecord> SelectAll()
        {
            return PatientDataAccess.SelectList();
        }
        public PatientRecord Select(Guid id)
        {
            return PatientDataAccess.Select(id, null);
        }
        public void Save(PatientRecord dto)
        {
            try
            {
                if (dto != null && dto.PID != Guid.Empty)
                {
                    PatientDataAccess.Update(dto, dto.Timestamp);
                }
                else
                {
                    Guid patientId = UtilityDataService.CheckPatientHospitalId(dto.PatientHospitalId);
                    if (patientId == Guid.Empty)
                    {
                        PatientDataAccess.Insert(dto, dto.Timestamp, _logger);
                    }
                    else
                    {
                        dto.PID = patientId;
                        PatientDataAccess.Update(dto, dto.Timestamp);
                    }
                }
            }
            catch
            {
            }
        }
        public void Delete(Guid id)
        {
            PatientDataAccess.Delete(id);
        }
    }
}
