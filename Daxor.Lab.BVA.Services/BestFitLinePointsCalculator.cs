﻿using System;
using System.Linq;
using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;

namespace Daxor.Lab.BVA.Services
{
    /// <summary>
    /// Represents a service that can calculate best fit line points as applicable to
    /// blood volume analysis.
    /// </summary>
    public class BestFitLinePointsCalculator : IBestFitLinePointsCalculator
    {
        /// <summary>
        /// Calculates the endpoints for a best fit line based on the given samples, 
        /// transudation rate, and total blood volume.
        /// </summary>
        /// <param name="compositeSamples">Collection of composite blood volume 
        /// samples used in the regression</param>
        /// <param name="transudationRate">Transudation rate (i.e., slope) of the
        /// corresponding test</param>
        /// <param name="totalBloodVolume">Total (i.e., time-zero) blood volume
        /// of the corresponding test</param>
        /// <returns>Two-element collection where the first point represents the
        /// graph point for the time-zero blood volume, and the second represents
        /// the graph point for the blood volume at time 't' where 't' is the
        /// number of minutes post-injection for the final sample.</returns>
        /// <exception cref="ArgumentException">
        /// There are fewer than two composite samples provided
        /// </exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Given transudation rate or total blood volume is positive or negative infinity,
        /// or NaN
        /// </exception>
        /// <exception cref="ArgumentNullException">The list of composite samples is null
        /// or if any of those samples are null</exception>
        public IEnumerable<BestFitLinePoint> CalculateBestFitLinePoints(IEnumerable<BVACompositeSample> compositeSamples, double transudationRate, double totalBloodVolume)
        {
            // ReSharper disable PossibleMultipleEnumeration
            if (compositeSamples.Count() < 2)
                throw new ArgumentException("At least two composite samples are required", "compositeSamples");
            if (double.IsInfinity(transudationRate))
                throw new ArgumentOutOfRangeException("transudationRate", "Transudation rate cannot be infinity");
            if (double.IsNaN(transudationRate))
                throw new ArgumentOutOfRangeException("transudationRate", "Transudation rate cannot be not-a-number (NaN)");
            if (double.IsInfinity(totalBloodVolume))
                throw new ArgumentOutOfRangeException("totalBloodVolume", "Total blood volume cannot be infinity");
            if (double.IsNaN(totalBloodVolume))
                throw new ArgumentOutOfRangeException("totalBloodVolume", "Total blood volume cannot be not-a-number (NaN)");
            if (compositeSamples.Any(x => x == null))
                throw new ArgumentNullException("compositeSamples", "None of the composite samples can be null");

            var highestPostInjectionTimeInMinutes = compositeSamples.Max(x => x.PostInjectionTimeInSeconds / 60);
            var secondPointDependentValue = Math.Exp(transudationRate * highestPostInjectionTimeInMinutes) * totalBloodVolume;
            // ReSharper restore PossibleMultipleEnumeration

            // Note about second point dependent value:
            //
            //     Transudation is in natural log space and blood volume is in linear space.
            //     The usual point slope formula is "y = mx + b". The following are equivalent:
            //
            //         y = e^(mx + ln(b))    ==     y = e^(mx) * b

            return new[] 
                { 
                    new BestFitLinePoint { IndependentValue = 0, DependentValue = totalBloodVolume }, 
                    new BestFitLinePoint { IndependentValue = highestPostInjectionTimeInMinutes, DependentValue = secondPointDependentValue } 
                };
        }
    }
}
