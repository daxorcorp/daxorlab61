﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.BVA.Services
{
	/// <summary>
	/// Provides CRUD operations for BVA module used with DaxorLab SQL Database
	/// </summary>
	internal class BvaDataService : IBVADataService
	{
		#region Fields

		private readonly IEventAggregator _eventAggregator;
		private readonly ILoggerFacade _logger;
		private readonly ISettingsManager _settingsManager;

		#endregion

		#region Ctor

		public BvaDataService(IEventAggregator eventAggregator, ILoggerFacade logger, ISettingsManager settingsManager)
		{
			_eventAggregator = eventAggregator;
			_logger = logger;
			_settingsManager = settingsManager;
		}

		#endregion

		#region Public API

		public void SaveSample(BvaSampleRecord samp, object concurrencyObject)
		{
			var s = samp.SampleId != Guid.Empty ? BvaSampleDataAccess.Select(samp.SampleId, concurrencyObject, _logger) : samp;

			if (s != null && s.SampleId != Guid.Empty)
				BvaSampleDataAccess.Update(samp, concurrencyObject, _logger);
			else
				BvaSampleDataAccess.Insert(samp, concurrencyObject, _logger);
		}

		public IEnumerable<BVATestItem> SelectBVATestItems()
		{
			try
			{
				var context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
				return context.GetBvaTestList().Select(DataMapper.RecordToModel).ToList();
			}
			catch (Exception ex)
			{
				_logger.Log("Exception: " + ex.Message, Category.Exception, Priority.High);
				throw;
			}
		}

		public string GetMostRecentSpecialty(string physiciansName)
		{
			try
			{
				var context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
				var record = context.GetRecentPhysiciansSpecialties(physiciansName).FirstOrDefault();
				return record==null? String.Empty:record.PHYSICIANS_SPECIALTY;
			}
			catch (Exception ex)
			{
				_logger.Log("Exception: " + ex.Message, Category.Exception, Priority.High);
				throw;
			}
		}

		public bool AreBvaTestTablesAreOnline()
		{
			try
			{
				var context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
				return context.CheckIfBVATestTablesOnline().Select(r => r.Column1 == "true" ).FirstOrDefault();
			}
			catch (Exception ex)
			{
				_logger.Log("Exception: " + ex.Message, Category.Exception, Priority.High);
				throw;
			}
		}

		public BVATestItem SelectBVATestItem(Guid testId)
		{
			try
			{
				var context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
				var x = context.GetBvaTestListItem(testId);

				var result = x.FirstOrDefault();
				if (result == null)
					return null;

				bool patientAmputeeStatus;
				var patient = context.GetPatientData(result.PID);
				try
				{
					patientAmputeeStatus = patient.FirstOrDefault().IS_AMPUTEE;
				}
				catch (NullReferenceException)
				{
					patientAmputeeStatus = false;
				}


				return new BVATestItem
				{
					CreatedDate = result.TEST_DATE,
					HasSufficientData = result.HAS_SUFFICIENT_DATA == 'T',
					PatientDOB = result.DOB,
					PatientFullName = GetFullName(result.FIRST_NAME, result.MIDDLE_NAME, result.LAST_NAME),
					PatientHospitalID = result.PATIENT_HOSPITAL_ID,
					PatientPID = result.PID,
					AmputeeStatus = patientAmputeeStatus,
					TestID = result.TEST_ID,
					Status = GetStatusFromStatusId(result.STATUS_ID),
					TestStatusDescription = BVATestItem.FormatStatusDescription(GetStatusFromStatusId(result.STATUS_ID),
					result.TEST_MODE_DESCRIPTION, result.HAS_SUFFICIENT_DATA == 'T'),
					WasSaved = true,
					TestID2 = result.ID2
				};
			}
			catch (Exception ex)
			{
				_logger.Log("Exception: " + ex.Message, Category.Exception, Priority.High);
				throw;
			}
		}

		public ISampleLayoutSchema SelectLayoutSchema(int schemaId)
		{
			try
			{
				//Must select all of them where type is equals to BVA Type
				var positions = (from x in PositionListDataAccess.SelectList()
								 where schemaId == x.LayoutID
								 select x).ToList();

				//Define sample layout schema
				SampleLayoutSchema schema = null;

				if (positions.Count > 0)
				{
					schema = new SampleLayoutSchema(schemaId);
					positions.ForEach(r => schema.TryAddLayoutItem(DataMapper.RecordToModel(r)));
				}

				return schema;

			}
			catch (Exception ex)
			{
				_logger.Log(ex.Message, Category.Exception, Priority.High);
				throw;
			}
		}

		public ISampleLayoutSchema SelectLayoutSchema()
		{
			return SelectLayoutSchema(_settingsManager.GetSetting<Int32>(SettingKeys.SystemSampleChangerLayoutId));
		}

		public BVATest SelectTest(Guid testId)
		{
			try
			{
				// BVA Test Record
				BvaTestRecord testRecord = BvaTestDataAccess.Select(testId);
				if (testRecord == null)
					return null;

				testRecord.Patient = PatientDataAccess.Select(testRecord.PID, testRecord.ConcurrencyObject);
				testRecord.TubeTypeRecord = TubeTypeDataAccess.Select(testRecord.TubeId);
				testRecord.Samples = BvaSampleDataAccess.SelectList(testRecord.TestId).ToList();

				return DataMapper.RecordToModel(testRecord, SelectLayoutSchema(testRecord.SampleLayoutId));
			}
			catch (Exception ex)
			{
				_logger.Log(ex.Message, Category.Exception, Priority.High);
				throw;
			}
		}

		public void DeleteTest(Guid testId)
		{
			try
			{
				BvaTestDataAccess.Delete(testId);
			}
			catch (Exception ex)
			{
				_logger.Log("DeleteTest() " + ex.Message, Category.Exception, Priority.High);
				throw;
			}
		}

		public void SaveTest(BVATest test, Boolean includeInjectates = false)
		{
			try
			{
				if (test == null)
					throw new ArgumentNullException("test");

				InternalSaveTest(DataMapper.ModelToRecord(test, _settingsManager));

				// This is absolutely necessary to save the barcodes properly. 
				// We need to save them *ONLY* when we start the test.  This function needs to be called 
				// AFTER the test is saved, otherwise there will be a foreign key conflict. 
				// Author: Steve Johnson
				if (includeInjectates)
				{
					//Verify that injectate key is valid
					if (!String.IsNullOrEmpty(test.InjectateKey))
						BarCodeDataAccess.Insert(new BarCodeRecord { TestId = test.InternalId, BID = test.InjectateKey, PositionId = 1 });
					if (!String.IsNullOrEmpty(test.StandardAKey))
						BarCodeDataAccess.Insert(new BarCodeRecord { TestId = test.InternalId, BID = test.StandardAKey, PositionId = 2 });
					if (!String.IsNullOrEmpty(test.StandardBKey))
						BarCodeDataAccess.Insert(new BarCodeRecord { TestId = test.InternalId, BID = test.StandardBKey, PositionId = 3 });
				}

				_eventAggregator.GetEvent<BvaTestSaved>().Publish(test.InternalId);
			}
			catch (Exception ex)
			{
				_logger.Log("SaveTest() " + ex.Message, Category.Exception, Priority.High);
				throw;
			}
		}

		public IQueryable<HospitalPatientIdItem> FindHospitalPatientIdItems(string startsWithHospitalPatientId)
		{
			return (from r in HospitalPatientIdDataAccess.SelectList()
					where r.HospitalId.StartsWith(startsWithHospitalPatientId)
					select new HospitalPatientIdItem(r.PatientId, r.HospitalId)).AsQueryable();
		}

		public BVAPatient SelectPatient(string hospitalId, object concurrencyManager)
		{
			return DataMapper.RecordToModel(PatientDataAccess.Select(hospitalId, concurrencyManager));
		}
        
		public Tube SelectDefaultTube()
		{
			try
			{
				return DataMapper.RecordToModel(TubeTypeDataAccess.Select(_settingsManager.GetSetting<Int32>(SettingKeys.BvaDefaultTubeType)));
			}
			catch (Exception ex)
			{
				_logger.Log("SelectDefaultTube() " + ex.Message, Category.Exception, Priority.High);
				throw;
			}
		}

		public IEnumerable<Tube> SelectTubes()
		{
			try
			{
				return TubeTypeDataAccess.SelectList().ToList().ConvertAll(DataMapper.RecordToModel);
			}
			catch (Exception ex)
			{
				_logger.Log("SelectTubes() " + ex.Message, Category.Exception, Priority.High);
				throw;
			}
		}

		public IQueryable<String> FindAnalysts(string startWithName)
		{
			try
			{
				if (startWithName == null)
					return null;

				var context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
				return context.GetAnalysts().Where(r => r.ANALYST.StartsWith(startWithName)).Select(r => r.ANALYST);
			}
			catch (Exception ex)
			{
				//Log exception and rethrow to the user
				_logger.Log("Exception: " + ex.Message, Category.Exception, Priority.High);
				throw;
			}
		}

	    public bool AreThereAnyTestsThatRanOnPatient(Guid internalId)
	    {
	        return BvaTestDataAccess.AreThereAnyTestsThatRanOnPatient(internalId);
	    }

	    public void DeletePatient(Guid internalId)
	    {
            PatientDataAccess.Delete(internalId);
	    }

		#endregion

		#region Private Helper Methods

		private void InternalSaveTest(BvaTestRecord test)
		{
			// Check and see if test exists
			BvaTestRecord rec = BvaTestDataAccess.Select(test.TestId);
			if (rec != null && rec.TestId != Guid.Empty)
			{
				// Update Test
				// Update samples first to prevent spurious Audit Trail records
				foreach (BvaSampleRecord samp in test.Samples)
				{
					samp.TestId = test.TestId;
					SaveSample(samp, test.ConcurrencyObject);
				}

				// Need to make sure the patient gets committed first, otherwise the BVA test can't be committed.
				var patientRecord = PatientDataAccess.Select(test.PID, test.ConcurrencyObject);
				if (patientRecord == null)
					PatientDataAccess.Insert(test.Patient, test.ConcurrencyObject, _logger);
				else
					PatientDataAccess.Update(test.Patient, test.ConcurrencyObject);

				BvaTestDataAccess.Update(test, test.ConcurrencyObject, _logger);
			}
			else
			{
				PatientRecord patRec = PatientDataAccess.Select(test.Patient.PID, test.ConcurrencyObject);

				if (patRec != null && patRec.PID != Guid.Empty)
					PatientDataAccess.Update(test.Patient, test.ConcurrencyObject);
				else
				{
					Guid patientId = UtilityDataService.CheckPatientHospitalId(test.Patient.PatientHospitalId);
					if (patientId == Guid.Empty)
						PatientDataAccess.Insert(test.Patient, test.ConcurrencyObject, _logger);
					else
					{
						test.Patient.PID = patientId;
						test.PID = patientId;

						PatientDataAccess.Update(test.Patient, test.ConcurrencyObject);
					}
				}

				BvaTestDataAccess.Insert(test, test.ConcurrencyObject, _logger);

				foreach (BvaSampleRecord samp in test.Samples)
				{
					samp.TestId = test.TestId;
					BvaSampleDataAccess.Insert(samp, test.ConcurrencyObject, _logger);
				}
			}
		 }

		private static TestStatus GetStatusFromStatusId(int statusId)
		{
			switch (statusId)
			{
				case 0:
					return TestStatus.Pending;
				case 1:
					return TestStatus.Running;
				case 2:
					return TestStatus.Aborted;
				case 3:
					return TestStatus.Deleted;
				case 4:
					return TestStatus.Completed;
			}

			return TestStatus.Undefined;
		}

		private static string GetFullName(string firstName, string middleName, string lastName)
		{
			if (!String.IsNullOrEmpty(lastName) && !String.IsNullOrEmpty(firstName))
				return (lastName + ", " + firstName + " " + middleName).Trim();

			if (String.IsNullOrEmpty(lastName) && String.IsNullOrEmpty(firstName)) return String.Empty;
			return String.IsNullOrEmpty(lastName) ? firstName : lastName;
		}

		#endregion
	}
}
