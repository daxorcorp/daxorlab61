﻿using System;
using System.Linq;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.Infrastructure.SubViews;
using Daxor.Lab.MessageManager.Interfaces;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.BVA.Services
{
	/// <summary>
	/// Represents a cache for patient-specific fields.
	/// </summary>
	public class PatientSpecificFieldCache : IPatientSpecificFieldCache
	{
		#region fields

		private string _cachedLastName;
		private string _cachedFirstName;
		private string _cachedMiddleName;
		private DateTime? _cachedDateOfBirth;
		private GenderType _cachedGenderType;
		private bool _cachedAmputeeStatus;
		private bool _isCacheInitialized;
		private PropertyObserver<BVAPatient> _patientObserverForCache;
		private readonly IMessageBoxDispatcher _messageBoxDispatcher;
		private readonly ILoggerFacade _logger;
		private readonly IMessageManager _messageManager;

		#endregion

		#region CTOR

		/// <summary>
		/// Creates a new PatientSpecificFieldCache instance.
		/// </summary>
		/// <param name="messageBoxDispatcher">Dispatcher instance for displaying message boxes.</param>
		/// <param name="logger"></param>
		/// <param name="messageManager"></param>
		public PatientSpecificFieldCache(IMessageBoxDispatcher messageBoxDispatcher, ILoggerFacade logger, IMessageManager messageManager)
		{
			if (messageBoxDispatcher == null) throw new ArgumentNullException("messageBoxDispatcher");

			_messageBoxDispatcher = messageBoxDispatcher;
			_logger = logger;
			_isCacheInitialized = false;
			_messageManager = messageManager;
		}

		#endregion

		#region properties

		/// <summary>
		/// Gets or sets whether the cached patient information differs from the actual patient information
		/// </summary>
		public bool IsDirty { get; set; }

		/// <summary>
		/// Gets or sets whether the cached patient information is for a new patient.
		/// </summary>
		public bool IsNewPatient { get; set; }

		#endregion

		#region Public API

		/// <summary>
		/// Initializes the cache with the given patient. 
		/// </summary>
		/// <param name="patient">Patient to initialize the cache </param>
		/// <param name="forceInitialization">If set to true, this will initialize the cache with the information from patient</param>
		/// <returns>Return true if the cache was initialized, false otherwise</returns>
		/// <remarks>If the forceInitialization flag is not set, the cache will only be initialized if the cache was not previously intialized.</remarks>
		public bool InitializePatientSpecificFieldCache(BVAPatient patient, bool forceInitialization = false)
		{
			if (patient == null) throw new ArgumentNullException("patient");

			if (_isCacheInitialized && !forceInitialization)
				return false;

			_cachedDateOfBirth = patient.DateOfBirth;
			_cachedFirstName = patient.FirstName;
			_cachedLastName = patient.LastName;
			_cachedMiddleName = patient.MiddleName;
			_cachedGenderType = patient.Gender;
			_cachedAmputeeStatus = patient.IsAmputee;

			_isCacheInitialized = true;
			IsDirty = false;

			_patientObserverForCache = new PropertyObserver<BVAPatient>(patient);
			_patientObserverForCache.RegisterHandler(p => p.DateOfBirth, p => { IsDirty = p.DateOfBirth != _cachedDateOfBirth; });
			_patientObserverForCache.RegisterHandler(p => p.FirstName, p => { IsDirty = p.FirstName != _cachedFirstName; });
			_patientObserverForCache.RegisterHandler(p => p.LastName, p => { IsDirty = p.LastName != _cachedLastName; });
			_patientObserverForCache.RegisterHandler(p => p.MiddleName, p => { IsDirty = p.MiddleName != _cachedMiddleName; });
			_patientObserverForCache.RegisterHandler(p => p.Gender, p => { IsDirty = p.Gender != _cachedGenderType; });
			_patientObserverForCache.RegisterHandler(p => p.IsAmputee, p => { IsDirty = p.IsAmputee != _cachedAmputeeStatus; });

			return true;
		}

		/// <summary>
		/// Overrides the cache with the given patient.
		/// </summary>
		/// <param name="patient"></param>
		public void OverridePatientSpecificFieldCache(BVAPatient patient)
		{
			InitializePatientSpecificFieldCache(patient, forceInitialization:true);
		}

		/// <summary>
		/// Pushes the fields in the cache back to the given patient.
		/// </summary>
		/// <param name="patient"></param>
		public void RestorePatientFieldsToMatchCache(BVAPatient patient)
		{
			if (patient == null) throw new ArgumentNullException("patient");

			patient.DateOfBirth = _cachedDateOfBirth;
			patient.FirstName = _cachedFirstName;
			patient.LastName = _cachedLastName;
			patient.MiddleName = _cachedMiddleName;
			patient.Gender = _cachedGenderType;
			patient.IsAmputee = _cachedAmputeeStatus;
		}

		/// <summary>
		/// Marks the cache as uninitialized.
		/// </summary>
		public void UninitializePatientSpecificFieldCache()
		{
			_isCacheInitialized = false;
		}

		/// <summary>
		/// Queries the user via messagebox to either restore the cache (i.e. undo the changes) or override the cache with new information
		/// </summary>
		/// <param name="existingPatient">Patient that exists in the database</param>
		/// <param name="enteredPatient">Patient that the user entered</param>
		public void RestoreOrOverrideBasedOnUserChoice(BVAPatient existingPatient, BVAPatient enteredPatient)
		{
			if (existingPatient == null) throw new ArgumentNullException("existingPatient");
			if (enteredPatient == null) throw new ArgumentNullException("enteredPatient");
			
			if (!IsDirty || IsNewPatient) return;

			var differingPatientInformationAlert = _messageManager.GetMessage(MessageKeys.BvaDifferingPatientInformation);
			const string caption = "Differing Patient Information";
			var escapedDescription = differingPatientInformationAlert.Description;
			var alertDescriptionComponents = escapedDescription.Split('|');
			var intro = alertDescriptionComponents.FirstOrDefault();
			var promptAndAlertId = alertDescriptionComponents.LastOrDefault() + " [Alert: " + differingPatientInformationAlert.MessageId + "]";
			const string patientAHeader = "Entered Information";
			const string patientBHeader = "Existing Information";
			var choices = new[] { "Keep Changes", "Discard Changes" };

			int result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Question,
				new PatientComparisonSubView(intro, patientAHeader, patientBHeader, promptAndAlertId, enteredPatient, existingPatient),
				caption, choices);
			
			_logger.Log(promptAndAlertId, Category.Info, Priority.Low);
			if (choices[result] == "Discard Changes")
				RestorePatientFieldsToMatchCache(enteredPatient);
			else
				OverridePatientSpecificFieldCache(enteredPatient);
		}

		#endregion
	}
}
