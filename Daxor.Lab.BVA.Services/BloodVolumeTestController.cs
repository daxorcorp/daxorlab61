﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Services.TestExecutionContext;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Windows.Input;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Infrastructure.Events;

namespace Daxor.Lab.BVA.Services
{
	/// <summary>
	/// Blood Volume Test Controllers
	/// </summary>
	internal class BloodVolumeTestController : IBloodVolumeTestController
	{
		#region Fields

		private readonly IBVADataService _dataService;
		private readonly IDetector _detector;
		private readonly IEventAggregator _eventAggregator;
		private readonly ILoggerFacade _logger;
		private readonly IRuleEngine _ruleEngine;
		private readonly ISettingsManager _settingsManager;
		private readonly ISpectroscopyService _spectroscopyService;
		private readonly ITestExecutionController _executionController;
		private ICalibrationCurve _calibrationCurve;

		private readonly DelegateCommand<BVATestItem> _selectTestCommand;
		private readonly DelegateCommand<object> _createTestCommand;

		private BVATest _runningTest;
		private BVATest _selectedTest;

		#endregion

		#region Ctor

		public BloodVolumeTestController(ITestExecutionController executionController, 
			IBVADataService dataService, ILoggerFacade logger, ISettingsManager settingsManager, IEventAggregator eventAggregator,
			[Dependency(AppModuleIds.BloodVolumeAnalysis)] IRuleEngine ruleEngine, IDetector detector, ISpectroscopyService spectroscopyService)
		{
			if (dataService == null)
				throw new ArgumentNullException("dataService");
			if (detector == null)
				throw new ArgumentNullException("detector");
			if (eventAggregator == null)
				throw new ArgumentNullException("eventAggregator");
			if (executionController == null)
				throw new ArgumentNullException("executionController");
			if (logger == null)
				throw new ArgumentNullException("logger");
			if (ruleEngine == null)
				throw new ArgumentNullException("ruleEngine");
			if (settingsManager == null)
				throw new ArgumentNullException("settingsManager");
			if (spectroscopyService == null)
				throw new ArgumentNullException("spectroscopyService");

			_calibrationCurve = new CalibrationCurve(0, 1, 0);
			_dataService = dataService;
			_detector = detector;
			_eventAggregator = eventAggregator;
			_executionController = executionController;
			_logger = logger;
			_ruleEngine = ruleEngine;
			_settingsManager = settingsManager;
			_spectroscopyService = spectroscopyService;
			
			_selectTestCommand = new DelegateCommand<BVATestItem>(SelectTestCommandExecute, SelectTestCommandCanExecute);
			_createTestCommand = new DelegateCommand<object>(CreateTestCommandExecute);
			_executionController.TestCompleted += _executionController_TestCompleted;
			_executionController.TestAborted += _executionController_TestAborted;

			_eventAggregator.GetEvent<CalibrationCurveCalculated>().Subscribe(curve => { _calibrationCurve = curve; });
		}

		#endregion

		#region Properties

		public IBVADataService DataService
		{
			get { return _dataService; }
		}
		public ILoggerFacade Logger
		{
			get { return _logger; }
		}
		public IEventAggregator EventAggregator
		{
			get { return _eventAggregator; }
		}

		public BVATest RunningTest
		{
			get { return _runningTest; }
			private set { _runningTest = value; }
		}
		public BVATest SelectedTest
		{
			get { return _selectedTest; }
			private set
			{
				if (_selectedTest == value)
					return;

				_selectedTest = value;
				
				if (RunningTest != null && RunningTest.InternalId == value.InternalId)
					_selectedTest = RunningTest;

				_eventAggregator.GetEvent<BvaTestSelected>().Publish(_selectedTest);
			}
		}

		public BVATestItem ObservingTestItem { get; private set; }

		#endregion

		#region Commands

		public ICommand SelectTestCommand
		{
			get { return _selectTestCommand; }
		}
		void SelectTestCommandExecute(BVATestItem testItem)
		{
			if (testItem == null) return;

			var test = _dataService.SelectTest(testItem.TestID);
			test.UpdateRuleEngine(_ruleEngine);
			SelectedTest = test;
		}
		bool SelectTestCommandCanExecute(BVATestItem testItem)
		{
			return testItem != null;
		}

		public ICommand CreateTestCommand
		{
			get { return _createTestCommand; }
		}
		void CreateTestCommandExecute(Object nullObject)
		{
			try
			{
				var test = new BVATest(_dataService.SelectLayoutSchema(), _ruleEngine)
							   {
								   SystemId = _settingsManager.GetSetting<String>(SettingKeys.SystemUniqueSystemId),
								   ConcurrencyObject = new Dictionary<String, Binary>(),
								   Protocol = _settingsManager.GetSetting<Int32>(SettingKeys.BvaDefaultProtocol),
								   HematocritFill =
									   (HematocritFillType)
									   _settingsManager.GetSetting<Int32>(SettingKeys.BvaDefaultHematocritFillType),
								   Tube = _dataService.SelectDefaultTube(),
								   Patient =
									   {
										   MeasurementSystem =
											   _settingsManager.GetSetting<String>(
												   SettingKeys.SystemDefaultMeasurementSystem) == "US"
												   ? MeasurementSystem.English
												   : MeasurementSystem.Metric
									   },
								   RefVolume = _settingsManager.GetSetting<Int32>(SettingKeys.BvaReferenceVolumeInMl)
							   };

				ObservingTestItem = new BVATestItem(test) {WasSaved = false};
				ObservingTestItem.BeginObserving(test);
				SelectedTest = test;
			}
			catch
			{
				ObservingTestItem = null;
				SelectedTest = null;
				throw;
			}
		}

		#endregion

		#region Public API

		//Cannot test this method because BvaTestExecutionContext has to have a real test;
		public void StartSelectedTest()
		{
			try
			{
				if (_executionController.IsExecuting) return;
				
				RunningTest = SelectedTest;

				_eventAggregator.GetEvent<GetCalibrationCurveRequest>().Publish(null);

				_executionController.Start(new BvaTestExecutionContext(RunningTest, _detector, _calibrationCurve, Logger, _spectroscopyService, DataService, _ruleEngine));
			}
			catch (Exception ex)
			{
				_logger.Log(ex.Message, Category.Exception, Priority.High);
				throw;
			}
		}

		#endregion

		#region Private Helpers

		private void _executionController_TestAborted(object sender, Domain.Eventing.TestExecutionEventArgs e)
		{
			CleanupRunningTest();
		}

		private void _executionController_TestCompleted(object sender, Domain.Eventing.TestExecutionEventArgs e)
		{
			CleanupRunningTest();
		}

		private void CleanupRunningTest()
		{
			if (_runningTest == null)
				return;

			_runningTest.Date = DateTime.Now;
			DataService.SaveTest(_runningTest);
			_runningTest = null;
		}

		#endregion
	}
}
