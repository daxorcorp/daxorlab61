﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Database.DataAccess;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.BVA.Services
{
	public class SampleRepository : ISampleRepository<BvaSampleRecord>
	{
		public IEnumerable<BvaSampleRecord> SelectAll(Guid testId)
		{
			return BvaSampleDataAccess.SelectList(testId);
		}
		public BvaSampleRecord Select(Guid id, ILoggerFacade logger)
		{
			return BvaSampleDataAccess.Select(id, null, logger);
		}
		public void Save(BvaSampleRecord sampleRecord, ILoggerFacade logger)
		{
			var s = sampleRecord.SampleId == Guid.Empty ? sampleRecord : BvaSampleDataAccess.Select(sampleRecord.SampleId, sampleRecord.BvaSampleTimestamp, logger);

			if (s != null && s.SampleId != Guid.Empty)
				BvaSampleDataAccess.Update(sampleRecord, sampleRecord.BvaSampleTimestamp, logger);

			else
				BvaSampleDataAccess.Insert(sampleRecord, sampleRecord.BvaSampleTimestamp, logger);
		}

		public void Delete(Guid id, ILoggerFacade logger)
		{
			BvaSampleDataAccess.Delete(id, logger);
		}
	}
}
