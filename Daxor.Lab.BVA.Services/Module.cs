﻿using Daxor.Lab.BVA.Services.RuleEvaluators;
using Daxor.Lab.Infrastructure.Constants;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Exceptions;

namespace Daxor.Lab.BVA.Services
{
	/// <summary>
	/// BVAService module. Provides services used by BVAModule.
	/// </summary>
	public class Module : IModule
	{
		private const string ModuleName = "BVA Services";

		private readonly IUnityContainer _container;

		public Module(IUnityContainer container)
		{
			_container = container;
		}

		public void Initialize()
		{
			RegisterPatientRepository();
			RegisterBvaDataService();
			RegisterBloodVolumeTestController();
			RegisterBestFitLinePointsCalculator();
			RegisterSumOfSquaresOfDifferencesCalculator();
			RegisterPredictedValuesCalculator();
			RegisterResidualStandardErrorCalculator();
			RegisterAutomaticPointExclusionService();
			RegisterPatientSpecificFieldCache();
			RegisterBvaRuleEngine();
			RegisterRuleEngine();
		}

	    private void RegisterRuleEngine()
	    {
	        try
	        {
	            var rEngine = (BvaRuleEngine) _container.Resolve<IRuleEngine>(AppModuleIds.BloodVolumeAnalysis);
	            rEngine.Initialize();
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the BVA Rule Engine");
	        }
	    }

	    private void RegisterBvaRuleEngine()
	    {
	        try
	        {
	            _container.RegisterType<IRuleEngine, BvaRuleEngine>(AppModuleIds.BloodVolumeAnalysis,
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "register the BVA Rule Engine");
	        }
	    }

	    private void RegisterPatientSpecificFieldCache()
	    {
	        try
	        {
	            _container.RegisterType<IPatientSpecificFieldCache, PatientSpecificFieldCache>(
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Patient-specific Field Cache");
	        }
	    }

	    private void RegisterAutomaticPointExclusionService()
	    {
	        try
	        {
	            _container.RegisterType<IAutomaticPointExclusionService, AutomaticPointExclusionService>(
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Automatic Point Exclusion Service");
	        }
	    }

	    private void RegisterResidualStandardErrorCalculator()
	    {
	        try
	        {
	            _container.RegisterType<IResidualStandardErrorCalculator, ResidualStandardErrorCalculator>(
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Residual Standard Error Calculator");
	        }
	    }

	    private void RegisterPredictedValuesCalculator()
	    {
	        try
	        {
	            _container.RegisterType<IPredictedValuesCalculator, PredictedValuesCalculator>(
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Predicted Values Calculator");
	        }
	    }

	    private void RegisterSumOfSquaresOfDifferencesCalculator()
	    {
	        try
	        {
	            _container.RegisterType<ISumOfSquaresOfDifferencesCalculator, SumOfSquaresOfDifferencesCalculator>(
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Sum of Squares of Differences Calculator");
	        }
	    }

	    private void RegisterBestFitLinePointsCalculator()
	    {
	        try
	        {
	            _container.RegisterType<IBestFitLinePointsCalculator, BestFitLinePointsCalculator>(
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Best Fit Line Points Calculator");
	        }
	    }

	    private void RegisterBloodVolumeTestController()
	    {
	        try
	        {
	            _container.RegisterType<IBloodVolumeTestController, BloodVolumeTestController>(
	                new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Blood Volume Test Controller");
	        }
	    }

	    private void RegisterBvaDataService()
	    {
	        try
	        {
	            _container.RegisterType<IBVADataService, BvaDataService>(new ContainerControlledLifetimeManager());
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the BVA Data Service");
	        }
	    }

	    private void RegisterPatientRepository()
	    {
	        try
	        {
	            _container.RegisterType(typeof (IRepository<>), typeof (PatientRepository));
	        }
	        catch
	        {
	            throw new ModuleLoadException(ModuleName, "initialize the Patient Repository");
	        }
	    }
	}
}
