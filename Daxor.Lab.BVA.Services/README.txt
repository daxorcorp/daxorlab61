﻿Daxor.Lab.BVA.Services

Contains concrete implementations of Daxor.Lab.Interfaces. 
This is a PRISM "service" module (i.e. contains no UI elements). NO ONE should make reference to this project.
BVA module should be configured such that Daxor.Lab.BVA.Service.Module gets loaded before BVA. Use dependencies tag in app.Config.

AK 