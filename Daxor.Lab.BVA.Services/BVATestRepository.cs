﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.Utility;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.BVA.Services
{
    public class BVATestRepository : IRepository<BvaTestRecord>
    {
        readonly ISampleRepository<BvaSampleRecord> _sampleRepository;
        readonly IRepository<PatientRecord> _ptnRepository;
        readonly ILoggerFacade _logger;

        public BVATestRepository(ISampleRepository<BvaSampleRecord> sampleRepository, IRepository<PatientRecord> ptnRepository, ILoggerFacade logger)
        {
            _sampleRepository = sampleRepository;
            _ptnRepository = ptnRepository;
            _logger = logger;
        }

        public IEnumerable<BvaTestRecord> SelectAll()
        {
            throw new NotImplementedException();
        }
        public BvaTestRecord Select(Guid id)
        {
            // BVA Test Record
            BvaTestRecord bvaTestRecord = BvaTestDataAccess.Select(id);

            if (bvaTestRecord == null)
                return null;

            bvaTestRecord.Patient = _ptnRepository.Select(bvaTestRecord.PID);
            bvaTestRecord.TubeTypeRecord = TubeTypeDataAccess.Select(bvaTestRecord.TubeId);

            //Fill up samples
            bvaTestRecord.Samples = _sampleRepository.SelectAll(bvaTestRecord.TestId).ToList();

            return bvaTestRecord;
        }
        public void Save(BvaTestRecord dto)
        {
            BvaTestRecord rec = Select(dto.TestId);
            if (rec != null && rec.TestId != Guid.Empty)
            {
                // Update Test
                BvaTestDataAccess.Update(dto, dto.ConcurrencyObject, _logger);
                _ptnRepository.Save(dto.Patient);
                dto.Samples.ForEach((s) => { s.TestId = dto.TestId; _sampleRepository.Save(s,_logger); });
            }
            else
            {
                PatientRecord patRec = _ptnRepository.Select(dto.Patient.PID);

                if (patRec != null && patRec.PID != Guid.Empty)
                    _ptnRepository.Save(dto.Patient);

                else
                {
                    Guid patientId = UtilityDataService.CheckPatientHospitalId(dto.Patient.PatientHospitalId);
                    if (patientId == Guid.Empty)
                        _ptnRepository.Save(dto.Patient);

                    else
                    {
                        dto.Patient.PID = patientId;
                        dto.PID = patientId;

                        _ptnRepository.Save(dto.Patient);
                    }
                }

                BvaTestDataAccess.Insert(dto, dto.ConcurrencyObject, _logger);
                dto.Samples.ForEach((s) => { s.TestId = dto.TestId; _sampleRepository.Save(s, _logger); });
            }
        }
        public void Delete(Guid id)
        {
            BvaTestDataAccess.Delete(id);
        }
    }
}
