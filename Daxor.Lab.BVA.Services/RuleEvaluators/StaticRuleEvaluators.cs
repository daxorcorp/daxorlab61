﻿using System;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Services;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Interfaces;

namespace Daxor.Lab.BVA.Services.RuleEvaluators
{
    /// <summary>
    /// Static rule evaluators for bva test
    /// </summary>
    public static class StaticRuleEvaluators 
    {
        #region Height

        /// <summary>
        /// Determines whether the height for the given BVAPatient is greater than zero
        /// (i.e., is defined)
        /// </summary>
        /// <param name="patient">BVAPatient instance to evaluate</param>
        /// <returns>True if the instance is non-null and the height is greater than zero</returns>
        public static bool HeightIsGreaterThanZero(BVAPatient patient)
        {
            return (patient != null) && (patient.HeightInCurrentMeasurementSystem > 0);
        }

        //returns false if Invalid
        public static bool HeightIsNotLessThanMinimumSupportedIdealHeight(BVAPatient patient, IIdealsCalcEngineService calsService)
        {
            // ReSharper disable CompareOfFloatsByEqualityOperator
            return (patient != null) && (patient.HeightInCm == 0 || !(patient.HeightInCm < calsService.MinimumHeightInCm));
            // ReSharper restore CompareOfFloatsByEqualityOperator
        }

        public static bool HeightIsNotGreaterThanMaximumSupportedIdealHeight(BVAPatient patient, IIdealsCalcEngineService calsService)
        {
            // ReSharper disable CompareOfFloatsByEqualityOperator
            return (patient != null) && (patient.HeightInCm == 0 || !(patient.HeightInCm > calsService.MaximumHeightInCm));
            // ReSharper restore CompareOfFloatsByEqualityOperator
        }

        #endregion

        #region Weight

        /// <summary>
        /// Determines whether the weight for the given BVAPatient is greater than zero
        /// (i.e., is defined)
        /// </summary>
        /// <param name="patient">BVAPatient instance to evaluate</param>
        /// <returns>True if the instance is non-null and the weight is greater than zero</returns>
        public static bool WeightIsGreaterThanZero(BVAPatient patient)
        {
            return (patient != null) && (patient.WeightInCurrentMeasurementSystem > 0);
        }

        public static bool WeightIsNotLessThanMinimumSupportedIdealWeight(BVAPatient patient, IIdealsCalcEngineService calsService)
        {
            // ReSharper disable CompareOfFloatsByEqualityOperator
            return (patient != null) && (patient.WeightInKg == 0 || !(patient.WeightInKg < calsService.MinimumWeightInKg));
            // ReSharper restore CompareOfFloatsByEqualityOperator
        }

        public static bool WeightIsNotGreaterThanMaximumSupportedIdealWeight(BVAPatient patient, IIdealsCalcEngineService calsService)
        {
            // ReSharper disable CompareOfFloatsByEqualityOperator
            return (patient != null) && (patient.WeightInKg == 0 || !(patient.WeightInKg > calsService.MaximumWeightInKg));
            // ReSharper restore CompareOfFloatsByEqualityOperator
        }

        #endregion

        #region Weight deviation

        /// <summary>
        /// Determines whether a given BVAPatient has a weight deviation greater
        /// than a given value.
        /// </summary>
        /// <param name="models">Array containing (1) a BVAPatient instance, 
        /// (2) an IIdealsCalcEngine instance, and (3) a comparison value (double).</param>
        /// <returns>True if (1) the given patient's weight deviation is greater than the
        /// comparison value, (2) the parameter count is not correct, (3) the given
        /// patient's weight deviation is infinity or NaN; false otherwise</returns>
        public static bool WeightDeviationIsNotLessThan(params object[] models)
        {
            if (models.Length != 3) return true;

            var bvaPatient = models[0] as BVAPatient;
            var idealCalc = models[1] as IIdealsCalcEngineService;
            var compareValue = Convert.ToDouble(models[2]);

            var weightDev = ComputeWeightDeviation(bvaPatient, idealCalc);
            if (Double.IsNaN(weightDev) || Double.IsInfinity(weightDev))
                return true;

            return !(weightDev < compareValue);
        }

        /// <summary>
        /// Determines whether a given BVAPatient has a weight deviation less than
        /// a given value.
        /// </summary>
        /// <param name="models">Array containing (1) a BVAPatient instance, 
        /// (2) an IIdealsCalcEngine instance, and (3) a comparison value (double).</param>
        /// <returns>True if (1) the given patient's weight deviation is less than the
        /// comparison value, (2) the parameter count is not correct, (3) the given
        /// patient's weight deviation is infinity or NaN; false otherwise</returns>
        public static bool WeightDeviationIsNotGreaterThan(params object[] models)
        {
            if (models.Length != 3) return true;

            var bvaPatient = models[0] as BVAPatient;
            var idealCalc = models[1] as IIdealsCalcEngineService;
            var compareValue = Convert.ToDouble(models[2]);

            var weightDev = ComputeWeightDeviation(bvaPatient, idealCalc);
            if (Double.IsNaN(weightDev) || Double.IsInfinity(weightDev))
                return true;

            return !(weightDev > compareValue);
        }

        /// <summary>
        /// Determines whether a given BVAPatient has a weight deviation that is inside
        /// the given range
        /// </summary>
        /// <param name="models">Array containing (1) a BVAPatient instance, 
        /// (2) an IIdealsCalcEngine instance, (3) a lower comparison value (double),
        /// and (4) an upper comparison value</param>
        /// <returns>True if (1) the given patient's weight deviation is outside the
        /// given range, (2) the parameter count is not correct, (3) the given
        /// patient's weight deviation is infinity or NaN; false otherwise</returns>
        public static bool WeightDeviationIsInHighReportableRange(params object[] models)
        {
            if (models.Length != 4) return true;

            var bvaPatient = models[0] as BVAPatient;
            var idealCalc = models[1] as IIdealsCalcEngineService;
            var lowerValue = Convert.ToDouble(models[2]);
            var upperValue = Convert.ToDouble(models[3]);

            var weightDev = ComputeWeightDeviation(bvaPatient, idealCalc);
            if (Double.IsNaN(weightDev) || Double.IsInfinity(weightDev))
                return true;

            return !((weightDev>lowerValue) && (weightDev <= upperValue));
        }

        /// <summary>
        /// Computes the weight deviation for a given patient given an ideals calculation
        /// engine.
        /// </summary>
        /// <param name="patient">BVAPatient whose weight deviation is to be computed</param>
        /// <param name="iCalcEngine">Instance of IIdealsCalcEngine to compute the weight
        /// deviation</param>
        /// <returns>Weight deviation computed by the IIdealsCalcEngine for the given patient
        /// </returns>
        private static double ComputeWeightDeviation(Patient patient, IIdealsCalcEngineService iCalcEngine)
        {
            if (patient == null || iCalcEngine == null || patient.Gender == GenderType.Unknown)
                return Double.NaN;

            var heightInCmRounded = Math.Round(patient.HeightInCm, 2, MidpointRounding.AwayFromZero);
            var weightInKgRounded = Math.Round(patient.WeightInKg, 2, MidpointRounding.AwayFromZero);

            return iCalcEngine.GetIdeals(weightInKgRounded, heightInCmRounded, (int)patient.Gender).WeightDeviation;
        }

        #endregion

        #region Date of birth / age

        /// <summary>
        /// Determines whether the date of birth for the specified patient occurs after
        /// the test (provided through the specified view model).
        /// </summary>
        /// <param name="patient">BVAPatient instance to evaluate</param>
        /// <param name="currentTest">View model containing a CurrentTest property
        /// that can be used to determine the test date</param>
        /// <returns>True if (1) either argument is null, (2) the date of birth is not defined
        /// for the specified patient, (3) the specified view model has a null test instance,
        /// (4) the date of birth occurs before the current test date; false otherwise</returns>
        public static bool DateOfBirthIsNotBeyondTestDate(BVAPatient patient, BVATest currentTest)
        {
            if (patient == null || !patient.DateOfBirth.HasValue || currentTest == null) return true;

            return !(patient.DateOfBirth > currentTest.Date);
        }

        /// <summary>
        /// Determines whether the date of birth for the specified patient is not set
        /// to a predefined invalid value.
        /// </summary>
        /// <param name="patient">BVAPatient instance to evaluate</param>
        /// <returns></returns>
        public static bool DateOfBirthIsValid(BVAPatient patient)
        {
            if (patient == null) return true;
            return patient.DateOfBirth != DomainConstants.InvalidDateOfBirth;
        }

        /// <summary>
        /// Determines whether the age in years (based on the test date) of the specified patient 
        /// is less than a specified value.
        /// </summary>
        /// <param name="patient">BVAPatient instance to evaluate</param>
        /// <param name="currentTest">View model containing a CurrentTest property
        /// that can be used to determine the test date</param>
        /// <param name="value"></param>
        /// <returns>True if (1) either argument is null, (2) the date of birth is not defined
        /// for the specified patient, (3) the specified view model has a null test instance,
        /// (4) the patient's age is less than the specified value; false otherwise</returns>
        public static bool PatientAgeIsLessThan(BVAPatient patient, BVATest currentTest, int value)
        {
            if (patient == null || !patient.DateOfBirth.HasValue || currentTest == null) return true;

            var patientAge = patient.GetAgeInYears(currentTest.Date);
            return patientAge < value;
        }

        #endregion

        #region Gender

        /// <summary>
        /// Determines whether the gender is defined for a given BVAPatient
        /// </summary>
        /// <param name="patient">BVAPatient instance to evaluate</param>
        /// <returns>True if the instance is non-null and the gender is not GenderType.Unknown;
        /// false otherwise</returns>
        public static bool GenderIsDefined(BVAPatient patient)
        {
            return (patient != null) && (patient.Gender != GenderType.Unknown);
        }

        #endregion

        #region InjectateEval

        //Returns true if valid
        public static bool InjectateLotIsNotEmpty(BVATest test)
        {
            if (test == null)
                return true;

            if (test.Status != TestStatus.Pending)
                return true;

            if (test.Mode != TestMode.Automatic && test.Mode != TestMode.VOPS)
                return true;

            return !String.IsNullOrEmpty(test.InjectateLot);
        }

        #endregion

        #region Post-injection

        static public bool PostInjectionTimeIsNotLessOrEqualToPreviousPostInjectionTime(BVACompositeSample sample)
        {
            #region IgnoreRules

            //Check for edge cases
            if (sample == null) return true;
            if (sample.Test == null) return true;
            if (sample.SampleType == SampleType.Background || sample.SampleType == SampleType.Baseline || sample.SampleType == SampleType.Standard) return true;
            if (sample.IsWholeSampleExcluded) return true;

            //Applied only to samples 2, 3, etc.
            if (sample.SampleType == SampleType.Sample1) return true;
            if (sample.PostInjectionTimeInSeconds <= 0) return true;

            #endregion

            var prevCompositeSample = sample.Test.FindCompositeSampleBefore(sample);
            if ((prevCompositeSample != null) && (prevCompositeSample.PostInjectionTimeInSeconds > -1))
                return !(sample.PostInjectionTimeInSeconds <= prevCompositeSample.PostInjectionTimeInSeconds);

            return true;
        }

        static public bool PostInjectionTimeIsNotMoreOrEqualToSubsequentPostInjectionTime(BVACompositeSample sample)
        {
            #region IgnoreRules

            //Check for edge cases
            if (sample == null) return true;
            if (sample.Test == null) return true;
            if (sample.SampleType == SampleType.Background || sample.SampleType == SampleType.Baseline || sample.SampleType == SampleType.Standard) return true;
            if (sample.IsWholeSampleExcluded) return true;
            if (sample.PostInjectionTimeInSeconds <= 0) return true;

            #endregion

            var subsequentCompositeSample = sample.Test.FindCompositeSampleAfter(sample);
            if ((subsequentCompositeSample != null) && (subsequentCompositeSample.PostInjectionTimeInSeconds > -1))
                return !(sample.PostInjectionTimeInSeconds >= subsequentCompositeSample.PostInjectionTimeInSeconds);

            return true;
        }

        static public bool PostInjectionTimeIsNotLessThan(BVACompositeSample sample, double value)
        {
            // Check for edge cases
            if (sample == null) return true;
            if (sample.SampleA == null) return true;
            if (sample.SampleA.PostInjectionTimeInSeconds == -1) return true;
            if (sample.IsWholeSampleExcluded) return true;

            return !(sample.SampleA.PostInjectionTimeInSeconds < value);
        }

        static public bool PostInjectionTimeIsNotGreaterThan(BVACompositeSample sample, double value)
        {
            // Check for edge cases
            if (sample == null) return true;
            if (sample.SampleA == null) return true;
            if (sample.SampleA.PostInjectionTimeInSeconds == -1) return true;
            if (sample.IsWholeSampleExcluded) return true;

            return !(sample.SampleA.PostInjectionTimeInSeconds > value);
        }

        #endregion

        #region Hematocrit

        static public bool HematocritIsNotGreaterThan(bool validatingHctA, BVACompositeSample sample, double value)
        {
            //Ignore rules
            if (sample == null || sample.SampleA == null || sample.SampleB == null) return true;
            if (sample.IsWholeSampleExcluded) return true;
            if (NotAllHematocritsAreFilled(sample.Test)) return true;
            if (validatingHctA && (sample.SampleA.IsHematocritExcluded || sample.SampleA.Hematocrit <= 0)) return true;
            if (!validatingHctA && (sample.SampleB.IsHematocritExcluded || sample.SampleB.Hematocrit <= 0)) return true;

            if (!validatingHctA && sample.SampleB.Hematocrit > value) return false;
            if (validatingHctA && sample.SampleA.Hematocrit > value) return false;

            return true;
        }

        static public bool HematocritIsNotLessThan(bool validatingHctA, BVACompositeSample sample, double value)
        {
            //Ignore rules
            if (sample == null || sample.SampleA == null || sample.SampleB == null) return true;
            if (NotAllHematocritsAreFilled(sample.Test)) return true;
            if (sample.IsWholeSampleExcluded) return true;
            if (validatingHctA && (sample.SampleA.IsHematocritExcluded || sample.SampleA.Hematocrit <= 0)) return true;
            if (!validatingHctA && (sample.SampleB.IsHematocritExcluded || sample.SampleB.Hematocrit <= 0)) return true;

            if (!validatingHctA && sample.SampleB.Hematocrit < value) return false;
            if (validatingHctA && sample.SampleA.Hematocrit < value) return false;

            return true;
        }

        static public bool HematocritsDoNotDifferByMoreThanTwoPoints(bool validatingHctA, BVACompositeSample sample)
        {
            //Ignore cases:
            if (sample == null || sample.SampleType == SampleType.Background || sample.SampleType == SampleType.Standard)
                return true;
            if (sample.SampleA == null || sample.SampleB == null)
                return true;
            if (NotAllHematocritsAreFilled(sample.Test)) return true;

            if (sample.IsWholeSampleExcluded) return true;

            double hctA;
            double hctB;
            
            // ReSharper disable CompareOfFloatsByEqualityOperator
            if (sample.IsWholeSampleExcluded)
                hctA = hctB = 0;
            else
            {

                hctA = sample.SampleA.IsHematocritExcluded || sample.SampleB.Hematocrit == -1 ? 0 : sample.SampleA.Hematocrit;
                hctB = sample.SampleB.IsHematocritExcluded || sample.SampleA.Hematocrit == -1 ? 0 : sample.SampleB.Hematocrit;
            }

            if (hctA == 0 || hctB == 0) return true;
            // ReSharper restore CompareOfFloatsByEqualityOperator

            //Evaluation, return false if invalid
            var diff = Math.Abs(sample.SampleA.Hematocrit - sample.SampleB.Hematocrit);
            return !(diff > 2);
        }

        static public bool HematocritsDoNotDifferFromTestAverageHematocritByMoreThan(bool validatingHctA, BVACompositeSample sample, double byMoreThanValue)
        {
            #region Ignore Cases

            if (sample == null) return true;
            if (NotAllHematocritsAreFilled(sample.Test)) return true;
            if (sample.SampleType == SampleType.Background || sample.SampleType == SampleType.Standard || sample.SampleType == SampleType.Baseline) return true;
            if (validatingHctA && sample.SampleA.IsHematocritExcluded) return true;
            if (!validatingHctA && sample.SampleB.IsHematocritExcluded) return true;

            if (sample.IsWholeSampleExcluded) return true;

            if (validatingHctA && (sample.SampleA == null || sample.SampleA.Hematocrit <= 0)) return true;
            if (!validatingHctA && (sample.SampleB == null || sample.SampleB.Hematocrit <= 0)) return true;

            //Ignore case where we don't have an average valid hematocrit
            if (sample.Test.PatientHematocritResult <= 0) return true;
            //No way we can be in this case, averages are all dead on
            if (sample.Test.HematocritFill == HematocritFillType.OnePerTest) return true;

            #endregion

            var averageHematocrit = sample.Test.PatientHematocritResult;

            double diff = validatingHctA ? Math.Abs(sample.SampleA.Hematocrit - averageHematocrit) : Math.Abs(sample.SampleB.Hematocrit - averageHematocrit);

            return !(diff > byMoreThanValue);
        }

        private static bool NotAllHematocritsAreFilled(BVATest bvaTest)
        {
            if (bvaTest == null)
                return true;
            if (bvaTest.PatientCompositeSamples == null)
                return true;

            return bvaTest.PatientCompositeSamples.Any(sample => sample.HematocritA < 0 || sample.HematocritB < 0);
        }

        #endregion

        #region Sample counts

        static public bool StandardCountsAreNotLessThan(bool validatingStandardA, BVACompositeSample validatingSample, double value)
        {
            // Situations where there isn't enough information to properly evaluate the rule.
            if (validatingSample == null)
                return true;
            if (validatingSample.SampleType != SampleType.Standard)
                return true;
            if (validatingSample.SampleA == null || validatingSample.SampleB == null)
                return true;
            if (validatingStandardA)
            {
                if (validatingSample.SampleA.ExecutionStatus == SampleExecutionStatus.Acquiring)
                    return true;
                if (validatingSample.SampleA.IsCountExcluded)
                    return true;
                if (validatingSample.SampleA.Counts <= 0)
                    return true;
            }
            else
            {
                if (validatingSample.SampleB.ExecutionStatus == SampleExecutionStatus.Acquiring)
                    return true;
                if (validatingSample.SampleB.IsCountExcluded)
                    return true;
                if (validatingSample.SampleB.Counts <= 0)
                    return true;
            }

            // Enough information to evaluate fully.
            if (validatingStandardA && validatingSample.SampleA.Counts < value) return false;
            if (!validatingStandardA && validatingSample.SampleB.Counts < value) return false;

            return true;
        }

        static public bool SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference(BVACompositeSample validatingSample)
        {
            // Situations where there isn't enough information to properly evaluate the rule.
            if (validatingSample == null)
                return true;
            if (validatingSample.SampleType == SampleType.Background)
                return true;
            if (validatingSample.SampleType == SampleType.Standard)
                return true;
            if (validatingSample.SampleA == null || validatingSample.SampleB == null)
                return true;
            if (validatingSample.SampleA.IsCountExcluded || validatingSample.SampleB.IsCountExcluded)
                return true;
            if (validatingSample.IsWholeSampleExcluded)
                return true;
            if (validatingSample.SampleA.IsCounting || validatingSample.SampleB.IsCounting)
                return true;
            if ((validatingSample.SampleA.Counts < 0) || (validatingSample.SampleB.Counts < 0))
                return true;

            // Enough information to evaluate fully.
            const double coeffOfVariance = Core.StatisticalConstants.PATIENT_SAMPLE_COEFFICIENT_OF_VARIANCE;

            var maximumAllowableDifference = StatisticsHelpers.ComputeAllowableDifference(validatingSample.SampleA.Counts,
                validatingSample.SampleB.Counts,
                coeffOfVariance);

            double actualCountDifference = Math.Abs(validatingSample.SampleA.Counts - validatingSample.SampleB.Counts);

            return (actualCountDifference <= maximumAllowableDifference);
        }

        static public bool StandardAandStandardBCountDifferenceIsNotGreaterThanAllowableDifference(BVACompositeSample validatingSample)
        {
            // Situations where there isn't enough information to properly evaluate the rule.
            if (validatingSample == null)
                return true;
            if (validatingSample.SampleType == SampleType.Background)
                return true;
            if (validatingSample.SampleType != SampleType.Standard)
                return true;
            if (validatingSample.SampleA == null || validatingSample.SampleB == null)
                return true;
            if (validatingSample.SampleA.IsCountExcluded || validatingSample.SampleB.IsCountExcluded)
                return true;
            if (validatingSample.IsWholeSampleExcluded)
                return true;
            if (validatingSample.SampleA.IsCounting || validatingSample.SampleB.IsCounting)
                return true;
            if ((validatingSample.SampleA.Counts < 0) || (validatingSample.SampleB.Counts < 0))
                return true;

            // Enough information to evaluate fully.
            var maximumAllowableDifference = StatisticsHelpers.ComputeAllowableDifference(validatingSample.SampleA.Counts,
                                                             validatingSample.SampleB.Counts);

            double actualCountDifference = Math.Abs(validatingSample.SampleA.Counts - validatingSample.SampleB.Counts);

            return (actualCountDifference <= maximumAllowableDifference);
        }

        static public bool SampleCountsAreNotLessThanBaselineByMultipleOf(bool validatingSampleA, BVACompositeSample validatingSample, double multipleValue)
        {

            if (ShouldIgnoreSampleForEvaluation(validatingSampleA, validatingSample))
                return true;

            var countsToEvaluate = validatingSampleA ? validatingSample.CountsA : validatingSample.CountsB;
            if (countsToEvaluate < validatingSample.Test.PatientBaselineCompositeSample.AverageCounts)
                return true;

            var diff = countsToEvaluate / validatingSample.Test.PatientBaselineCompositeSample.AverageCounts;

            return !(diff < multipleValue);
        }

        static public bool SampleCountsAreGreaterThanOrEqualToBaseline(bool validatingSampleA, BVACompositeSample validatingSample)
        {
            if (ShouldIgnoreSampleForEvaluation(validatingSampleA, validatingSample)) 
                return true;

            var countsToEvaluate = validatingSampleA ? validatingSample.CountsA : validatingSample.CountsB;

            return countsToEvaluate >= validatingSample.Test.PatientBaselineCompositeSample.AverageCounts;
        }

        private static bool ShouldIgnoreSampleForEvaluation(bool validatingSampleA, BVACompositeSample validatingSample)
        {
            if (validatingSample == null) return true;
            if (validatingSample.SampleType == SampleType.Background || validatingSample.SampleType == SampleType.Standard ||
                validatingSample.SampleType == SampleType.Baseline) return true;

            if (validatingSample.SampleA.IsCounting || validatingSample.SampleB.IsCounting) return true;
            if (validatingSampleA && validatingSample.SampleA.IsCountExcluded) return true;
            if (!validatingSampleA && validatingSample.SampleB.IsCountExcluded) return true;

            if (validatingSampleA && validatingSample.SampleA.Counts <= -1) return true;
            if (!validatingSampleA && validatingSample.SampleB.Counts <= -1) return true;

            if (validatingSample.Test.PatientBaselineCompositeSample.AverageCounts <= 0) return true;
            if (validatingSample.IsWholeSampleExcluded) return true;
            return false;
        }

        #endregion

        #region Time-zero blood volume

        public static bool BloodVolumeIsNotBelowMinimumSupportedVolume(BVAVolume volume)
        {
            if (volume == null)
                return true;

            if (volume.WholeCount == -1 || volume.WholeCount == 0) // Undefined or zero
                return true;

            return volume.WholeCount >= BvaDomainConstants.MinimumBloodVolumeInmL;
        }

        public static bool BloodVolumeIsNotAboveMaximumSupportedVolume(BVAVolume volume)
        {
            if (volume == null)
                return true;

            if (volume.WholeCount == -1 || volume.WholeCount == 0) // Undefined or zero
                return true;

            return volume.WholeCount <= BvaDomainConstants.MaximumBloodVolumeInmL;
        }

        #endregion

        #region Standard deviation and transudation rate

        /// <summary>
        /// Determines whether the standard deviation of the result for a given BV test is less
        /// than a given value.
        /// </summary>
        /// <param name="test">Test containing a computed standard deviation of the result</param>
        /// <param name="value">Comparison value</param>
        /// <returns>True if the standard deviation isn't NaN and is less than the 
        /// given value; false otherwise</returns>
        public static bool StandardDeviationIsNotGreaterThanOrEqualTo(BVATest test, double value)
        {
            if (test == null) throw new ArgumentNullException("test");
            if (double.IsNaN(test.StandardDevResult)) return true;
            return Math.Abs(test.StandardDevResult) < value;
        }

        /// <summary>
        /// Determines whether the transudation rate for a given BV test is outside a given range
        /// where the endpoints of the range are. For example 0.4 is inside the range [0.4, 0.5].
        /// </summary>
        /// <param name="test">Test containing a computed transudation rate</param>
        /// <param name="min">Minimum comparison value</param>
        /// <param name="max">Maximum comparison value</param>
        /// <returns>True if the transudation rate is outside the range; false otherwise</returns>
        public static bool TransudationRateIsNotBetweenInclusive(BVATest test, double min, double max)
        {
            if (test == null) throw new ArgumentNullException("test");
            if (min > max) throw new ArgumentOutOfRangeException("min", @"Minimum comparison value must be less than or equal to the maximum comparison value");
            return (test.TransudationRateResult < min) || (test.TransudationRateResult > max);
        }

        /// <summary>
        /// Determines whether the transudation rate for a given BV test is positive.
        /// </summary>
        /// <param name="test">Test containing a computed transudation rate</param>
        /// <returns>True if the transudation rate is zero or greater; false otherwise</returns>
        public static bool TransudationRateIsPositive(BVATest test)
        {
            if (test == null) throw new ArgumentNullException("test");
            return test.TransudationRateResult >= 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="test"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool TransudationRateIsNotGreaterThan(BVATest test, double value)
        {
            if (test == null) throw new ArgumentNullException("test");
            return test.TransudationRateResult <= value;
        }

        #endregion

        #region Sample duration

        public static bool SampleDurationIsLessThanMaximum(BVATest test)
        {
            if (test == null) throw new ArgumentNullException("test");
            return Math.Abs(test.SampleDurationInSeconds) < BvaDomainConstants.MaximumSampleCountDurationInSeconds;
        }

        #endregion

        #region General

        public static bool FieldIsNotEmpty(string property)
        {
            return !String.IsNullOrWhiteSpace(property);
        }
        public static bool FieldIsNotEmpty(Func<String> func)
        {
            return FieldIsNotEmpty(func());
        }

        #endregion
    }
}
