﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Domain.Evaluation;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.RuleFramework.Rules;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.BVA.Services.RuleEvaluators
{
    /// <summary>
    /// The rule engine used in BVAModule
    /// </summary>
    public class BvaRuleEngine : RuleEngine
    {
        #region Constants

        private readonly string[] _ruleSettingIds =
        { 
            SettingKeys.BvaAccessionFieldRequired,
            SettingKeys.BvaFirstNameFieldRequired,
            SettingKeys.BvaLastNameFieldRequired,
            SettingKeys.BvaMiddleNameFieldRequired,
            SettingKeys.BvaDateOfBirthFieldRequired,
            SettingKeys.BvaReferringPhysicianFieldRequired,
            SettingKeys.BvaCcFieldRequired,
            SettingKeys.BvaAnalystFieldRequired,
            SettingKeys.BvaLocationFieldRequired,
            SettingKeys.BvaInjectateDoseFieldRequired,
            SettingKeys.SystemInjectateVerificationEnabled,
            SettingKeys.BvaPhysiciansSpecialtyFieldRequired,
        };

        #endregion

        #region Fields

        private readonly IIdealsCalcEngineService _iCalcEngine;
        private readonly ISettingsManager _settingsManager;

        #endregion

        #region Properties

        public bool IsInitialized { get; private set; }

        #endregion

        #region Ctor

        /// <summary>
        /// Creates a new BvaRuleEngine instance based on a given ideals calculation engine.
        /// </summary>
        /// <param name="iCalcEngine">Ideals calculation engine instance</param>
        /// <param name="settingsManager"></param>
        public BvaRuleEngine(IIdealsCalcEngineService iCalcEngine, ISettingsManager settingsManager)
        {
            IsInitialized = false;
            _iCalcEngine = iCalcEngine;
            _settingsManager = settingsManager;
        }

        #endregion

        #region Init

        public void Initialize()
        {
            //Ensure only one initialization during lifespan
            if (IsInitialized)
                return;

            //Initializes all possible rules for multiple entities
            InternalAddRulesBasedOnPriority();

            _settingsManager.SettingsChanged += OnSettingsManagerSettingsChanged;
        }

        #endregion

        #region Helpers

        void InternalAddRulesBasedOnPriority()
        {
            AddSystemRules();
            AddUserRules();
            AddAlertRules();
        }

        /// <summary>
        /// Adds alert rules to the rule engine
        /// </summary>
        void AddAlertRules()
        {
            #region Sample duration alert

            AddRule(new DelegateRuleWithResolutionMetadata<BVATest>(MessageKeys.BvaSampleDurationTooLong,
                "SampleDurationInSeconds", StaticRuleEvaluators.SampleDurationIsLessThanMaximum));

            #endregion

            #region Standard deviation and transudation alerts

            AddRule(new DelegateRuleWithResolutionMetadata<BVATest>(MessageKeys.BvaStdDevExtreme, "StandardDevResult",
                m => StaticRuleEvaluators.StandardDeviationIsNotGreaterThanOrEqualTo(m, BvaDomainConstants.MaximumBloodVolumePointStandardDeviation)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVATest>(MessageKeys.BvaTransudationReverse, "TransudationRateResult",
                StaticRuleEvaluators.TransudationRateIsPositive));

            #endregion

            #region Height/weight rules

            //Height rules
            AddRule(new DelegateRuleWithResolutionMetadata<BVAPatient>(MessageKeys.BvaHeightMinSupported, "HeightInCurrentMeasurementSystem",
                m => StaticRuleEvaluators.HeightIsNotLessThanMinimumSupportedIdealHeight(m, _iCalcEngine)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVAPatient>(MessageKeys.BvaHeightMaxSupported, "HeightInCurrentMeasurementSystem",
                m => StaticRuleEvaluators.HeightIsNotGreaterThanMaximumSupportedIdealHeight(m, _iCalcEngine)));

            //Weight rules
            //Note from Geoff: I don't think the less than min/max supported rules help much for the weight.
            //  For example, if you enter the lowest height supported, then enter a weight just below the
            //  corresponding ideal weight, the deviation will be acceptable, but the alert will fire because
            //  it's less than the minimum supported weight. (11/11/2011)
            AddRule(new DelegateRuleWithResolutionMetadata<BVAPatient>(MessageKeys.BvaWeightMinDev, "WeightInCurrentMeasurementSystem",
                m => StaticRuleEvaluators.WeightDeviationIsNotLessThan(m, _iCalcEngine, BvaDomainConstants.MinimumWeightDeviation)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVAPatient>(MessageKeys.BvaWeightHighDev, "WeightInCurrentMeasurementSystem",
                m => StaticRuleEvaluators.WeightDeviationIsInHighReportableRange(m, _iCalcEngine, BvaDomainConstants.HighWeightDeviationThreshold, BvaDomainConstants.MaximumWeightDeviation)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVAPatient>(MessageKeys.BvaWeightMaxDev, "WeightInCurrentMeasurementSystem",
                m => StaticRuleEvaluators.WeightDeviationIsNotGreaterThan(m, _iCalcEngine, BvaDomainConstants.MaximumWeightDeviation)));

            #endregion

            #region Age/date-of-birth rules

            AddRule(new DelegateRuleWithResolutionMetadata<BVAPatient>(MessageKeys.BvaDateOfBirthInvalid,
                "DateOfBirth",
                StaticRuleEvaluators.DateOfBirthIsValid));
            AddRule(new DependentDelegateAlertRuleWithResolutionMetadata<BVAPatient, BVATest>(MessageKeys.BvaDateOfBirthAfterTestDate,
                "DateOfBirth",
                StaticRuleEvaluators.DateOfBirthIsNotBeyondTestDate));
            AddRule(new DependentDelegateAlertRuleWithResolutionMetadata<BVAPatient, BVATest>(MessageKeys.BvaDateOfBirthMax,
                "DateOfBirth",
                (m, currentTest) => StaticRuleEvaluators.PatientAgeIsLessThan(m, currentTest, BvaDomainConstants.MaximumAgeInYears)));
            AddRule(new DependentDelegateAlertRuleWithResolutionMetadata<BVAPatient, BVATest>(MessageKeys.BvaDateOfBirth100YearsOld,
                "DateOfBirth",
                (m, currentTest) => StaticRuleEvaluators.PatientAgeIsLessThan(m, currentTest, BvaDomainConstants.HighAgeInYearsThreshold)));

            #endregion

            #region Time-zero blood volume result rules

            AddRule(new DelegateRuleWithResolutionMetadata<BVAVolume>(MessageKeys.BvaMeasuredBloodVolumeMin,
                "WholeCount", StaticRuleEvaluators.BloodVolumeIsNotBelowMinimumSupportedVolume));
            AddRule(new DelegateRuleWithResolutionMetadata<BVAVolume>(MessageKeys.BvaMeasuredBloodVolumeMax,
                "WholeCount", StaticRuleEvaluators.BloodVolumeIsNotAboveMaximumSupportedVolume));

            #endregion

            #region Post-injection time rules

            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaPostInjTimeShort, "PostInjectionTimeInSeconds",
                cSample => StaticRuleEvaluators.PostInjectionTimeIsNotLessThan(cSample, BvaDomainConstants.MinimumPostInjectionTimeInSeconds)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaPostInjTimeLong, "PostInjectionTimeInSeconds",
                cSample => StaticRuleEvaluators.PostInjectionTimeIsNotGreaterThan(cSample, BvaDomainConstants.MaximumPostInjectionTimeInSeconds)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaPostInjTimeLessThanPrev,
                "PostInjectionTimeInSeconds", StaticRuleEvaluators.PostInjectionTimeIsNotLessOrEqualToPreviousPostInjectionTime));
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaPostInjTimeMoreThanSubsequent,
                "PostInjectionTimeInSeconds", StaticRuleEvaluators.PostInjectionTimeIsNotMoreOrEqualToSubsequentPostInjectionTime));

            #endregion

            #region Hematocrit rules

            // Hematocrit differ by two points rules
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaHematocritMismatchBetweenBandA,
                "HematocritB", cSample => StaticRuleEvaluators.HematocritsDoNotDifferByMoreThanTwoPoints(false, cSample)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaHematocritMismatchBetweenAandB,
                "HematocritA", cSample => StaticRuleEvaluators.HematocritsDoNotDifferByMoreThanTwoPoints(true, cSample)));

            // Hematocrit diff from test hematocrit rules
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaHematocritBExceedsAverage,
                "HematocritB", cSample => StaticRuleEvaluators.HematocritsDoNotDifferFromTestAverageHematocritByMoreThan(false, cSample, BvaDomainConstants.MaximumDifferenceFromAverageHematocrit)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaHematocritAExceedsAverage,
                "HematocritA", cSample => StaticRuleEvaluators.HematocritsDoNotDifferFromTestAverageHematocritByMoreThan(true, cSample, BvaDomainConstants.MaximumDifferenceFromAverageHematocrit)));

            // Hematocrit minimum/maximum rules
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaHematocritLowB,
                "HematocritB",
                cSample => StaticRuleEvaluators.HematocritIsNotLessThan(false, cSample, BvaDomainConstants.MinimumHematocrit)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaHematocritHighB,
                "HematocritB",
                cSample => StaticRuleEvaluators.HematocritIsNotGreaterThan(false, cSample, BvaDomainConstants.MaximumHematocrit)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaHematocritLowA,
                "HematocritA",
                cSample => StaticRuleEvaluators.HematocritIsNotLessThan(true, cSample, BvaDomainConstants.MinimumHematocrit)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaHematocritHighA,
                "HematocritA",
                cSample => StaticRuleEvaluators.HematocritIsNotGreaterThan(true, cSample, BvaDomainConstants.MaximumHematocrit)));

            #endregion

            #region Sample Counts Rules

            // Low standard count rules
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaStandardsLowA,
                "CountsA",
                cSample => StaticRuleEvaluators.StandardCountsAreNotLessThan(true, cSample, BvaDomainConstants.MinimumStandardCounts)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaStandardsLowB,
                "CountsB",
                cSample => StaticRuleEvaluators.StandardCountsAreNotLessThan(false, cSample, BvaDomainConstants.MinimumStandardCounts)));

            // Counts on Sample's side A and side B statistically mismatch rules
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaSamplesDiffer,
                "CountsA",
                StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference));
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaSamplesDiffer,
                "CountsB",
                StaticRuleEvaluators.SampleAandSampleBCountDifferenceIsNotGreaterThanAllowableDifference));

            // Counts on Standard's side A and side B statistically mismatch rules
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaStandardsDiffer,
                "CountsA",
                StaticRuleEvaluators.StandardAandStandardBCountDifferenceIsNotGreaterThanAllowableDifference));
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaStandardsDiffer,
                "CountsB",
                StaticRuleEvaluators.StandardAandStandardBCountDifferenceIsNotGreaterThanAllowableDifference));

            // Samples are close to the baseline rules
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaSamplesACloseToBaseline,
                "CountsA",
                cSample => StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(true, cSample, BvaDomainConstants.MinimumMultipleOfBaselineForSampleCounts)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaSamplesBCloseToBaseline,
                "CountsB",
                cSample => StaticRuleEvaluators.SampleCountsAreNotLessThanBaselineByMultipleOf(false, cSample, BvaDomainConstants.MinimumMultipleOfBaselineForSampleCounts)));

            // Sample 1 counts are less than baseline rules
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaSampleALessThanBaseline,
                "CountsA",
                cSample => StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(true, cSample)));
            AddRule(new DelegateRuleWithResolutionMetadata<BVACompositeSample>(MessageKeys.BvaSampleBLessThanBaseline,
                "CountsB",
                cSample => StaticRuleEvaluators.SampleCountsAreGreaterThanOrEqualToBaseline(false, cSample)));
            
            #endregion
        }

        /// <summary>
        /// Adds system rules to the rule engine
        /// </summary>
        void AddSystemRules()
        {
            AddRule(new SystemRequiredRule<BVAPatient>("HospitalPatientId", "Patient ID is a required field.",
               m => StaticRuleEvaluators.FieldIsNotEmpty(m.HospitalPatientId)));
            AddRule(new SystemRequiredRule<BVAPatient>("HeightInCurrentMeasurementSystem", "Height is a required field.",
               StaticRuleEvaluators.HeightIsGreaterThanZero));
            AddRule(new SystemRequiredRule<BVAPatient>("WeightInCurrentMeasurementSystem", "Weight is a required field.",
               StaticRuleEvaluators.WeightIsGreaterThanZero));
            AddRule(new SystemRequiredRule<BVAPatient>("Gender", "Gender is a required field.",
               StaticRuleEvaluators.GenderIsDefined));
            AddRule(new SystemRequiredRule<BVATest>("Protocol", "Protocol is a required field.",
               m => m.Protocol != -1));
			AddRule(new SystemRequiredRule<BVATest>("AmputeeIdealsCorrectionFactor", "Amputee Ideals Correction Factor is a required field.",
				m => m.AmputeeIdealsCorrectionFactor != -1));
            AddRule(new SystemRequiredRule<BVATest>("InjectateLot", "InjectateLot is a required field.",
               StaticRuleEvaluators.InjectateLotIsNotEmpty));
            AddRule(new SystemRequiredRule<BVATest>("SampleDurationInSeconds", "Duration is a required field.",
               m => m.SampleDurationInSeconds != -1));
        }

        /// <summary>
        /// Adds user rules to the rule engine
        /// </summary>
        void AddUserRules()
        {
            AddRule(new UserRequiredRule<BVAPatient>("FirstName", "First name is a required field.",
                                                          m => StaticRuleEvaluators.FieldIsNotEmpty(m.FirstName),
                                                          SettingKeys.BvaFirstNameFieldRequired,
                                                          _settingsManager.GetSetting<Boolean>(
                                                              SettingKeys.BvaFirstNameFieldRequired)));
            AddRule(new UserRequiredRule<BVAPatient>("LastName", "LastName name is a required field.",
                                                          m => StaticRuleEvaluators.FieldIsNotEmpty(m.LastName),
                                                          SettingKeys.BvaLastNameFieldRequired,
                                                          _settingsManager.GetSetting<Boolean>(
                                                              SettingKeys.BvaLastNameFieldRequired)));
            AddRule(new UserRequiredRule<BVAPatient>("MiddleName", "Middle name is a required field.",
                                                          m => StaticRuleEvaluators.FieldIsNotEmpty(m.MiddleName),
                                                          SettingKeys.BvaMiddleNameFieldRequired,
                                                          _settingsManager.GetSetting<Boolean>(
                                                              SettingKeys.BvaMiddleNameFieldRequired)));
            AddRule(new UserRequiredRule<BVAPatient>("DateOfBirth", "DateOfBirth name is a required field.",
                                                          m => StaticRuleEvaluators.FieldIsNotEmpty(m.DateOfBirth.HasValue
                                                                                                   ? m.DateOfBirth.Value.ToShortDateString()
                                                                                                   : null),
                                                          SettingKeys.BvaDateOfBirthFieldRequired,
                                                          _settingsManager.GetSetting<Boolean>(
                                                              SettingKeys.BvaDateOfBirthFieldRequired)));
            AddRule(new UserRequiredRule<BVATest>("TestID2", "Accession is a required field.",
                                                       m => StaticRuleEvaluators.FieldIsNotEmpty(m.TestID2),
                                                       SettingKeys.BvaAccessionFieldRequired,
                                                       _settingsManager.GetSetting<Boolean>(
                                                           SettingKeys.BvaAccessionFieldRequired)));
            AddRule(new UserRequiredRule<BVATest>("PhysiciansSpecialty", "PhysiciansSpecialty is a required field.",
                                                       m => StaticRuleEvaluators.FieldIsNotEmpty(m.PhysiciansSpecialty),
                                                       SettingKeys.BvaPhysiciansSpecialtyFieldRequired,
                                                       _settingsManager.GetSetting<Boolean>(
                                                           SettingKeys.BvaPhysiciansSpecialtyFieldRequired)));
            AddRule(new UserRequiredRule<BVATest>("RefPhysician", "RefPhysician is a required field.",
                                                       m => StaticRuleEvaluators.FieldIsNotEmpty(m.RefPhysician),
                                                       SettingKeys.BvaReferringPhysicianFieldRequired,
                                                       _settingsManager.GetSetting<Boolean>(
                                                           SettingKeys.BvaReferringPhysicianFieldRequired)));
            AddRule(new UserRequiredRule<BVATest>("CcReportTo", "CcReportTo is a required field.",
                                                       m => StaticRuleEvaluators.FieldIsNotEmpty(m.CcReportTo),
                                                       SettingKeys.BvaCcFieldRequired,
                                                       _settingsManager.GetSetting<Boolean>(
                                                           SettingKeys.BvaCcFieldRequired)));
            AddRule(new UserRequiredRule<BVATest>("Analyst", "Analyst is a required field.",
                                                       m => StaticRuleEvaluators.FieldIsNotEmpty(m.Analyst),
                                                       SettingKeys.BvaAnalystFieldRequired,
                                                       _settingsManager.GetSetting<Boolean>(
                                                           SettingKeys.BvaAnalystFieldRequired)));
            AddRule(new UserRequiredRule<BVATest>("Location", "Location is a required field.",
                                                       m => StaticRuleEvaluators.FieldIsNotEmpty(m.Location),
                                                       SettingKeys.BvaLocationFieldRequired,
                                                       _settingsManager.GetSetting<Boolean>(
                                                           SettingKeys.BvaLocationFieldRequired)));
            // ReSharper disable CompareOfFloatsByEqualityOperator
            AddRule(new UserRequiredRule<BVATest>("InjectateDose", "InjectateDose is a required field.",
                                                       m => StaticRuleEvaluators.FieldIsNotEmpty(m.InjectateDose == 0
                                                                                                ? String.Empty
                                                                                                : m.InjectateDose.
                                                                                                      ToString(CultureInfo.InvariantCulture)),
                                                       SettingKeys.BvaInjectateDoseFieldRequired,
                                                       _settingsManager.GetSetting<Boolean>(
                                                           SettingKeys.BvaInjectateDoseFieldRequired)));
            // ReSharper restore CompareOfFloatsByEqualityOperator
            AddRule(new UserRequiredRule<BVATest>("InjectateKey", "InjectateKey is a required field.",
                                                       m => StaticRuleEvaluators.FieldIsNotEmpty(m.InjectateKey),
                                                       SettingKeys.SystemInjectateVerificationEnabled,
                                                       _settingsManager.GetSetting<Boolean>(
                                                           SettingKeys.SystemInjectateVerificationEnabled)));
            AddRule(new UserRequiredRule<BVATest>("StandardAKey", "StandardAKey is a required field.",
                                                       m => StaticRuleEvaluators.FieldIsNotEmpty(m.StandardAKey),
                                                       SettingKeys.SystemInjectateVerificationEnabled,
                                                       _settingsManager.GetSetting<Boolean>(
                                                           SettingKeys.SystemInjectateVerificationEnabled)));
            AddRule(new UserRequiredRule<BVATest>("StandardBKey", "StandardBKey is a required field.",
               m => StaticRuleEvaluators.FieldIsNotEmpty(m.StandardBKey),
               SettingKeys.SystemInjectateVerificationEnabled, _settingsManager.GetSetting<Boolean>(SettingKeys.SystemInjectateVerificationEnabled)));
        }

        void OnSettingsManagerSettingsChanged(object sender, SettingsChangedEventArgs e)
        {
            var ruleIDs = from s in e.ChangedSettingIdentifiers where _ruleSettingIds.Contains(s) select s;
            // ReSharper disable PossibleMultipleEnumeration
            if (!ruleIDs.Any()) return;
            
            var ruleIDsToEnable = new List<string>();
            var ruleIDsToDisable = new List<string>();

            ruleIDs.ToList().ForEach(
                ruleId =>
                {
                    if (_settingsManager.GetSetting<bool>(ruleId))
                        ruleIDsToEnable.Add(ruleId);
                    else
                        ruleIDsToDisable.Add(ruleId);
                });
            // ReSharper restore PossibleMultipleEnumeration

            // Final step
            if (ruleIDsToEnable.Count > 0)
                EnableRules(ruleIDsToEnable);

            if (ruleIDsToDisable.Count > 0)
                DisableRules(ruleIDsToDisable);
        }

        #endregion
    }
}
