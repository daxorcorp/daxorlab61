﻿using System;
using Daxor.Lab.BVA.Models;

namespace Daxor.Lab.BVA.Services
{
    public static class HematocritEvaluator
    {
        public static Boolean Evaluate(params object[] models)
        {
            if (models == null || models.Length != 2) return true;

            Double compareValue;
            Double.TryParse((string)models[1], out compareValue);

            BVACompositeSample cSample = models[0] as BVACompositeSample;
            Double hemA = 0;
            Double hemB = 0;

            if (cSample.SampleA != null)
                hemA = cSample.SampleA.Hematocrit;
            if (cSample.SampleB != null)
                hemB = cSample.SampleB.Hematocrit;

            if (hemA == 0 || hemB == 0) return true;
            return !(Math.Abs(hemA - hemB) > compareValue);
        }
    }
}
