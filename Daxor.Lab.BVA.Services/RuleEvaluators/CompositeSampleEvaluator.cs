﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.BVA.Models;
using Daxor.Lab.BVA.ViewModels;

namespace Daxor.Lab.BVA.Services.RuleEvaluators
{
    public static class CompositeSampleEvaluator
    {
        /// <summary>
        /// Returns true if post injection times are valid, false otherwise
        /// </summary>
        /// <param name="validatingSample">Sample ViewModel being validated</param>
        /// <param name="dependencyVM">Dependency object, VM</param>
        /// <returns></returns>
        /// 
       
        static public bool ValidPostInjectionTimes(CompositeSampleViewModel validatingSample, TestSampleValuesViewModel dependencyVM)
        {
            //Ignore cases:
            if (validatingSample == null || dependencyVM == null || dependencyVM.CompositeSampleViewModels == null) 
                return true;
            if (validatingSample.Type == SampleType.Background || validatingSample.Type == SampleType.Baseline || validatingSample.Type == SampleType.Standard) 
                return true;
            if (validatingSample.IsWholeSampleExcluded) return true;

            //Get cSampelViewModel where SampleA matches sampleB
            int index = dependencyVM.CompositeSampleViewModels.IndexOf(validatingSample);

            if (validatingSample.Type == SampleType.Sample1 || String.IsNullOrEmpty(validatingSample.PostInjectionTimeInSeconds) || index <= 0) return true;
            else{
                
                CompositeSampleViewModel prevSample = dependencyVM.CompositeSampleViewModels.ElementAt(index - 1);
                if(prevSample != null){

                    //Ignore if prev Sample
                    if (prevSample.SampleA.PostInjectionTimeInSeconds == 0)
                        return true;

                    //If validating sample's post injection time is less than prevSample, we have an invalid state of the sample
                    if (validatingSample.SampleA.PostInjectionTimeInSeconds < prevSample.SampleA.PostInjectionTimeInSeconds)
                        return false;
                }
            }

            return true;
        }
        static public bool PostInjectionTimeIsNotLessThan(CompositeSampleViewModel validatingSample, double value)
        {

            return validatingSample != null && validatingSample.SampleA != null && (validatingSample.SampleA.PostInjectionTimeInSeconds == 0 || validatingSample.IsWholeSampleExcluded || !(validatingSample.SampleA.PostInjectionTimeInSeconds < value));
        }
        static public bool PostInjectionTimeIsNotGreaterThan(CompositeSampleViewModel validatingSample, double value)
        {

            return validatingSample != null && validatingSample.SampleA != null && (validatingSample.SampleA.PostInjectionTimeInSeconds == 0 || validatingSample.IsWholeSampleExcluded || !(validatingSample.SampleA.PostInjectionTimeInSeconds > value));
        }
        
        static public bool HematocritGreaterThan(bool validatingHctA, CompositeSampleViewModel validatingSample, double value)
        {
            //Ignore rules
            if (validatingSample == null || validatingSample.SampleA == null || validatingSample.SampleB == null) return true;
            if (validatingSample.IsWholeSampleExcluded) return true;

            if (validatingHctA && (validatingSample.IsSampleAHematocritExcluded || String.IsNullOrEmpty(validatingSample.HematocritA))) return true;
            if (!validatingHctA && (validatingSample.IsSampleBHematocritExcluded || String.IsNullOrEmpty(validatingSample.HematocritB)))return true;

            if (!validatingHctA && validatingSample.SampleB.Hematocrit > value) return false;
            if (validatingHctA && validatingSample.SampleA.Hematocrit > value) return false;

            return true;
        }
        static public bool HematocritLessThan(bool validatingHctA, CompositeSampleViewModel validatingSample, double value)
        {
            //Ignore rules
            if (validatingSample == null || validatingSample.SampleA == null || validatingSample.SampleB == null) return true;
            if (validatingSample.IsWholeSampleExcluded) return true;

            if (validatingHctA && (validatingSample.IsSampleAHematocritExcluded || String.IsNullOrEmpty(validatingSample.HematocritA))) return true;
            if (!validatingHctA && (validatingSample.IsSampleBHematocritExcluded || String.IsNullOrEmpty(validatingSample.HematocritB))) return true;

            if (!validatingHctA && validatingSample.SampleB.Hematocrit < value) return false;
            if (validatingHctA && validatingSample.SampleA.Hematocrit < value) return false;

            return true;
        }
        static public bool ValidHematocritsDiffFromRanges(bool validatingHctA, CompositeSampleViewModel validatingSample)
        {
            //Ignore cases:
            if (validatingSample == null || validatingSample.Type == SampleType.Background || validatingSample.Type == SampleType.Standard)
                return true;
            if (validatingSample.SampleA == null || validatingSample.SampleB == null) 
                return true;
            if (validatingSample.IsWholeSampleExcluded) return true;

            double hctA = validatingSample.SampleA.Hematocrit;
            double hctB = validatingSample.SampleB.Hematocrit;

            if (validatingSample.IsWholeSampleExcluded)
                hctA = hctB = 0;
            else
            {
                hctA = validatingSample.IsSampleAHematocritExcluded ? 0 : validatingSample.SampleA.Hematocrit;
                hctB = validatingSample.IsSampleBHematocritExcluded ? 0 : validatingSample.SampleB.Hematocrit;
            }

            if (hctA == 0 || hctB == 0) return true;

            //Evaluation, return false if invalid
            double diff = Math.Abs(validatingSample.SampleA.Hematocrit - validatingSample.SampleB.Hematocrit);
            return !(diff > 2);
        }
        static public bool ValidHematocritsDiffFromTestAverage(bool validatingHctA, CompositeSampleViewModel validatingSample, TestSampleValuesViewModel dependencyVM)
        {

            //Ignore cases; return true
            if (validatingSample == null || validatingSample.SampleA == null || validatingSample.SampleB == null ||
                dependencyVM == null || dependencyVM.CurrentTest == null)
                return true;
            if (validatingSample.Type == SampleType.Background || validatingSample.Type == SampleType.Standard)
                return true;

            if (validatingHctA && validatingSample.IsSampleAHematocritExcluded) return true;
            if (!validatingHctA && validatingSample.IsSampleBHematocritExcluded) return true;


            if (validatingHctA && validatingSample.SampleA.Hematocrit == 0) return true;
            else if (!validatingHctA && validatingSample.SampleB.Hematocrit == 0) return true;

            if (validatingSample.IsWholeSampleExcluded) return true;

            //Determine if validatingSampleViewModel contains CompositeSample of in CurrentTest
            if (!dependencyVM.CurrentTest.CompositeSamples.Contains(validatingSample.CompositeSample)) return true;

            //Ignore case where we don't have an average valid hematocrit
            if (dependencyVM.CurrentTest.PatientHematocritResult <= 0) return true;

            double diff = Math.Abs(validatingSample.CompositeSample.AverageHematocrit - dependencyVM.CurrentTest.PatientHematocritResult);
            return !(diff > 2);
        }
        
        static public bool ValidPostInjectionTimesSample2(CompositeSampleViewModel validatingSample, TestSampleValuesViewModel dependencyVM)
        {
            //Ignore cases:
            if (validatingSample == null || dependencyVM == null || dependencyVM.CompositeSampleViewModels == null)
                return true;
            if (validatingSample.Type == SampleType.Background || validatingSample.Type == SampleType.Baseline || validatingSample.Type == SampleType.Standard)
                return true;
            if (validatingSample.Type != SampleType.Sample2) return true;

            if (validatingSample.IsWholeSampleExcluded) return true;

            //Get cSampelViewModel where SampleA matches sampleB
            int index = dependencyVM.CompositeSampleViewModels.IndexOf(validatingSample);

            if (validatingSample.Type == SampleType.Sample1 || String.IsNullOrEmpty(validatingSample.PostInjectionTimeInSeconds) || index <= 0) return true;
            else
            {

                CompositeSampleViewModel prevSample = dependencyVM.CompositeSampleViewModels.ElementAt(index - 1);
                if (prevSample != null)
                {

                    //Ignore if prev Sample
                    if (prevSample.SampleA.PostInjectionTimeInSeconds == 0)
                        return true;

                    //If validating sample's post injection time is less than prevSample, we have an invalid state of the sample
                    if (validatingSample.SampleA.PostInjectionTimeInSeconds < prevSample.SampleA.PostInjectionTimeInSeconds)
                        return false;
                }
            }

            return true;
        }
        static public bool ValidPostInjectionTimesSample3(CompositeSampleViewModel validatingSample, TestSampleValuesViewModel dependencyVM)
        {
            //Ignore cases:
            if (validatingSample == null || dependencyVM == null || dependencyVM.CompositeSampleViewModels == null)
                return true;
            if (validatingSample.Type == SampleType.Background || validatingSample.Type == SampleType.Baseline || validatingSample.Type == SampleType.Standard)
                return true;
            if (validatingSample.Type != SampleType.Sample3) return true;

            if (validatingSample.IsWholeSampleExcluded) return true;

            //Get cSampelViewModel where SampleA matches sampleB
            int index = dependencyVM.CompositeSampleViewModels.IndexOf(validatingSample);

            if (validatingSample.Type == SampleType.Sample1 || String.IsNullOrEmpty(validatingSample.PostInjectionTimeInSeconds) || index <= 0) return true;
            else
            {

                CompositeSampleViewModel prevSample = dependencyVM.CompositeSampleViewModels.ElementAt(index - 1);
                if (prevSample != null)
                {

                    //Ignore if prev Sample
                    if (prevSample.SampleA.PostInjectionTimeInSeconds == 0)
                        return true;

                    //If validating sample's post injection time is less than prevSample, we have an invalid state of the sample
                    if (validatingSample.SampleA.PostInjectionTimeInSeconds < prevSample.SampleA.PostInjectionTimeInSeconds)
                        return false;
                }
            }

            return true;
        }

        #region SAMPLE COUNTS RULES
        static public bool StandardsLessThan(bool validatingStandardA, CompositeSampleViewModel validatingSample, double value)
        {
            //Ignore rules
            if (validatingSample == null || validatingSample.Type != SampleType.Standard) return true;
            if (validatingSample.SampleA == null || validatingSample.SampleB == null) return true;
            if (validatingSample.CountingSampleA || validatingSample.CountingSampleB) return true;
            
            if (validatingStandardA && (validatingSample.IsSampleACountsExcluded || String.IsNullOrEmpty(validatingSample.SampleACounts) 
                || validatingSample.SampleA.Counts == 0)) return true;            
            if (!validatingStandardA && (validatingSample.IsSampleBCountsExcluded || String.IsNullOrEmpty(validatingSample.SampleBCounts)
                || validatingSample.SampleB.Counts == 0)) return true;

            if (validatingStandardA && validatingSample.SampleA.Counts < value) return false;
            if (!validatingStandardA && validatingSample.SampleB.Counts < value) return false;

            return true;
        }

        static public bool ValidSampleCountsADiffFromSampleCountsB(bool validatingSampleA, CompositeSampleViewModel validatingSample)
        {
            //Ignore cases:
            if (validatingSample == null || validatingSample.Type == SampleType.Background) return true;
            if (validatingSample.SampleA == null || validatingSample.SampleB == null) return true;
            if (validatingSample.IsWholeSampleExcluded) return true;

            if (validatingSample.CountingSampleA || validatingSample.CountingSampleB) return true;

            double sampleA = validatingSample.SampleA.Counts;
            double sampleB = validatingSample.SampleB.Counts;

            if (validatingSample.IsWholeSampleExcluded)
                sampleA = sampleB = 0;
            else
            {
                sampleA = validatingSample.IsSampleACountsExcluded ? 0 : validatingSample.SampleA.Counts;
                sampleB = validatingSample.IsSampleBCountsExcluded ? 0 : validatingSample.SampleB.Counts;
            }

            if (sampleA == 0 || sampleB == 0) return true;

            double max = 0;
            double std = 0;
            if (sampleA > sampleB) max = sampleA;
            else max = sampleB;

            std = Math.Sqrt(max);

            //Evaluation, return false if invalid
            double diff = Math.Abs(validatingSample.SampleA.Counts - validatingSample.SampleB.Counts);
            return !(diff > 3.89 * std);
        }

        // Need to more precisely define these rules bellow (what is the rule for Baseline compared to the background
        // and rule whats the relationship between counts and baseline
        static public bool BaselineGreaterThanBackgroundByMultOf(bool validatingBslnA, CompositeSampleViewModel validatingSample, 
                                                        TestSampleValuesViewModel dependencyVM, double multipleValue)
        {
            //Ignore cases:
            if (validatingSample == null || validatingSample.Type != SampleType.Baseline || dependencyVM == null || dependencyVM.CurrentTest == null)
                return true;
            if (validatingSample.SampleA == null || validatingSample.SampleB == null)
                return true;
            if (validatingSample.CountingSampleA || validatingSample.CountingSampleB) return true;

            // we need to have a good baseline and background
            if (dependencyVM.CurrentTest.PatientBaselineResult <= 0 || dependencyVM.CurrentTest.BackgroundCount <= 0) return true;

            if (validatingBslnA && (validatingSample.IsSampleACountsExcluded || String.IsNullOrEmpty(validatingSample.SampleACounts)
              || validatingSample.SampleA.Counts == 0)) return true;
            if (!validatingBslnA && (validatingSample.IsSampleBCountsExcluded || String.IsNullOrEmpty(validatingSample.SampleBCounts)
                || validatingSample.SampleB.Counts == 0)) return true;

            //Evaluation, return false if invalid
            if (validatingBslnA)
            {
                double diff = Math.Abs(validatingSample.SampleA.Counts / dependencyVM.CurrentTest.BackgroundCount);
                return !(diff > multipleValue);
            }
            if (!validatingBslnA)
            {
                double diff = Math.Abs(validatingSample.SampleB.Counts / dependencyVM.CurrentTest.BackgroundCount);
                return !(diff > multipleValue);
            }

            return true;
        }

        static public bool SampleCountsLessThanBaselineMultiplyOf(bool validatingSampleA, CompositeSampleViewModel validatingSample, 
                                                                    TestSampleValuesViewModel dependencyVM, double multipleValue)
        {

            //Ignore cases; return true
            if (validatingSample == null || validatingSample.SampleA == null || validatingSample.SampleB == null || 
                dependencyVM == null || dependencyVM.CurrentTest == null)
                return true;
            if (validatingSample.Type == SampleType.Background || validatingSample.Type == SampleType.Standard || validatingSample.Type == SampleType.Baseline)
                return true;
            if (validatingSample.CountingSampleA || validatingSample.CountingSampleB) return true;


            if (validatingSampleA && validatingSample.IsSampleACountsExcluded) return true;
            if (!validatingSampleA && validatingSample.IsSampleBCountsExcluded) return true;


            if (validatingSampleA && validatingSample.SampleA.Counts == 0) return true;
            else if (!validatingSampleA && validatingSample.SampleB.Counts == 0) return true;

            //Determine if validatingSampleViewModel contains CompositeSample of in CurrentTest
            if (!dependencyVM.CurrentTest.CompositeSamples.Contains(validatingSample.CompositeSample)) return true;

            //Ignore case where we don't have an average valid baseline
            if (dependencyVM.CurrentTest.PatientBaselineResult <= 0) return true;

            // if whole sample is excluded return true
            if (validatingSample.IsWholeSampleExcluded) return true;


            double diff = validatingSample.CompositeSample.AverageCounts / dependencyVM.CurrentTest.PatientBaselineResult;

            return !(diff < multipleValue);
        }
        #endregion
    }
}
