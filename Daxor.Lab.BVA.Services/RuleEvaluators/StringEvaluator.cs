﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.BVA.Services
{
    public static class StringEvaluator
    {
        public static Boolean IsNotNullOrWhiteSpace(params object[] models)
        {
            if (models == null || models.Length == 0) return true;
            
            return !String.IsNullOrWhiteSpace((String)models[0]);
        }
    }
}
