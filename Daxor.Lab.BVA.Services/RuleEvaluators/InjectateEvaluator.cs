﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.BVA.Models;

namespace Daxor.Lab.BVA.Services.RuleEvaluators
{
    public static class InjectateEvaluator
    {
        //If valid returns true
        public static bool TestInjectateKeyRequired(BVATest test, string key)
        {
            if (test == null)
                return true;

            if (test.TestStatus != Infrastructure.Entities.TestStatus.Pending)
                return true;

            if (test.Mode != TestMode.Automatic)
                return true;

            return !String.IsNullOrEmpty(key);
        }

        //Returns true if valid
        public static bool TestInjectateLotRequired(BVATest test)
        {
            if (test == null)
                return true;

            if (test.TestStatus != Infrastructure.Entities.TestStatus.Pending)
                return true;

            if (test.Mode != TestMode.Automatic && test.Mode != TestMode.VOPS)
                return true;

            return !String.IsNullOrEmpty(test.InjectateLot);
        }
    }
}
