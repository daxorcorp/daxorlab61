﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Interfaces;

namespace Daxor.Lab.BVA.Services
{
    /// <summary>
    /// Represents a service that can compute predicted values based on linear equation parameters.
    /// </summary>
    public class PredictedValuesCalculator : IPredictedValuesCalculator
    {
        /// <summary>
        /// Calculates the predicted (i.e., y-values) for the given independent values (i.e., x-values),
        /// slope, and y-intercept.
        /// </summary>
        /// <param name="independentValues">Values (i.e., x-values) to be used in the linear equation</param>
        /// <param name="slope">Slope of the line</param>
        /// <param name="yIntercept">y-intercept of the line</param>
        /// <returns>List of predicted y-values that correspond to the given independent values</returns>
        /// <exception cref="ArgumentNullException">If the list of independent values is null</exception>
        /// <exception cref="ArgumentOutOfRangeException">If the list of independent values contains 
        /// infinity or NaN values, if the slope is infinity or NaN, if the y-intercept
        /// is infinity or NaN</exception>
        public IEnumerable<double> CalculatePredictedValues(IEnumerable<double> independentValues, double slope, double yIntercept)
        {
            var indepValuesAsArray = independentValues as double[] ?? independentValues.ToArray();
            if (indepValuesAsArray.Any(double.IsInfinity))
                throw new ArgumentOutOfRangeException("independentValues", "The list cannot contain infinite values");
            if (indepValuesAsArray.Any(double.IsNaN))
                throw new ArgumentOutOfRangeException("independentValues", "The list cannot contain NaN values");
            if (double.IsInfinity(slope))
                throw new ArgumentOutOfRangeException("slope", "Slope cannot be infinite");
            if (double.IsNaN(slope))
                throw new ArgumentOutOfRangeException("slope", "Slope cannot be NaN");
            if (double.IsInfinity(yIntercept))
                throw new ArgumentOutOfRangeException("yIntercept", "y-intercept cannot be infinite");
            if (double.IsNaN(yIntercept))
                throw new ArgumentOutOfRangeException("yIntercept", "y-intercept cannot be NaN");

            return indepValuesAsArray.Select(value => slope*value + yIntercept).ToList();
        }
    }
}
