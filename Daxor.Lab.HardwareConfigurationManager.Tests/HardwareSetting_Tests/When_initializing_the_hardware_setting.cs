﻿using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.Tests.HardwareSetting_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_hardware_setting
    {
        [TestMethod]
        public void Then_the_name_is_set()
        {
            var setting = new HardwareSetting("Name Here", HardwareSetting.StringType);

            Assert.AreEqual("Name Here", setting.Name);
        }

        [TestMethod]
        public void Then_the_type_is_set()
        {
            var setting = new HardwareSetting("Name Here", HardwareSetting.StringType);

            Assert.AreEqual("string", setting.Type);
        }

        [TestMethod]
        public void Then_the_four_types_are_allowed()
        {
            // ReSharper disable ObjectCreationAsStatement
            AssertEx.DoesNotThrow(() => new HardwareSetting("test", HardwareSetting.IntegerType));
            AssertEx.DoesNotThrow(() => new HardwareSetting("test", HardwareSetting.StringType));
            AssertEx.DoesNotThrow(() => new HardwareSetting("test", HardwareSetting.BooleanType));
            AssertEx.DoesNotThrow(() => new HardwareSetting("test", HardwareSetting.DoubleType));
            // ReSharper restore ObjectCreationAsStatement
        }
    }
    // ReSharper restore InconsistentNaming
}
