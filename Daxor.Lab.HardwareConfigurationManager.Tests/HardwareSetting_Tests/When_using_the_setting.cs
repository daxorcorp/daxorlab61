using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.Tests.HardwareSetting_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_setting
    {
        private HardwareSetting _setting;
            
        [TestInitialize]
        public void BeforeEachTest()
        {
            _setting = new HardwareSetting("Something", HardwareSetting.IntegerType);                
        }

        [TestMethod]
        public void Then_the_setting_value_can_be_set()
        {
            _setting.SetValue(345);
            Assert.AreEqual(345, _setting.GetValue<int>());
        }

        [TestMethod]
        public void Then_the_setting_description_can_be_set()
        {
            _setting.Description = "Desc here";
            Assert.AreEqual("Desc here", _setting.Description);
        }

        [TestMethod]
        public void Then_the_setting_description_can_be_null()
        {
            AssertEx.DoesNotThrow(() => _setting.Description = null);
        }

        [TestMethod]
        public void And_the_value_has_not_been_set_then_the_default_for_the_type_is_assumed()
        {
            var stringSetting = new HardwareSetting("test", HardwareSetting.StringType);
            Assert.AreEqual(default(string), stringSetting.GetValue<string>());

            var integerSetting = new HardwareSetting("test", HardwareSetting.IntegerType);
            Assert.AreEqual(default(int), integerSetting.GetValue<int>());

            var doubleSetting = new HardwareSetting("test", HardwareSetting.DoubleType);
            Assert.AreEqual(default(double), doubleSetting.GetValue<double>());

            var booleanSetting = new HardwareSetting("test", HardwareSetting.BooleanType);
            Assert.AreEqual(default(bool), booleanSetting.GetValue<bool>());
        }
    }
    // ReSharper restore InconsistentNaming
}
