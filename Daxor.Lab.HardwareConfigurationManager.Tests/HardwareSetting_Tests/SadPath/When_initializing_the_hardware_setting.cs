using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.Tests.HardwareSetting_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_hardware_setting
    {
        [TestMethod]
        public void And_the_name_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentException>(() => new HardwareSetting(null, HardwareSetting.StringType));
        }

        [TestMethod]
        public void And_the_name_is_empty_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentException>(() => new HardwareSetting(string.Empty, HardwareSetting.StringType));
        }

        [TestMethod]
        public void And_the_name_is_whitespace_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentException>(() => new HardwareSetting("  ", HardwareSetting.StringType));
        }

        [TestMethod]
        public void And_the_type_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentException>(() => new HardwareSetting("name", null));
        }

        [TestMethod]
        public void And_the_type_is_empty_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentException>(() => new HardwareSetting("name", string.Empty));
        }

        [TestMethod]
        public void And_the_type_is_whitespace_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentException>(() => new HardwareSetting("name", "  "));
        }

        [TestMethod]
        public void And_the_type_is_not_supported_then_an_exception_is_thrown()
        {
            // ReSharper disable ObjectCreationAsStatement
            AssertEx.Throws<ArgumentException>(() => new HardwareSetting("name", "int"));
            AssertEx.Throws<ArgumentException>(() => new HardwareSetting("name", "object"));
            // ReSharper restore ObjectCreationAsStatement
        }
    }
    // ReSharper restore InconsistentNaming
}
