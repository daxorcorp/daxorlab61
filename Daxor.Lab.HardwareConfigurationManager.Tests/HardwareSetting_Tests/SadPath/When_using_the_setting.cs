using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.Tests.HardwareSetting_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_setting
    {
        [TestMethod]
        public void And_getting_a_value_that_is_inappropriate_for_the_type_then_an_exception_is_thrown()
        {
            var stringSetting = new HardwareSetting("test", HardwareSetting.StringType);
            stringSetting.SetValue("hello");

            AssertEx.Throws<InvalidCastException>(() => stringSetting.GetValue<int>());
        }

        [TestMethod]
        public void And_setting_a_value_that_is_inappropriate_for_the_type_then_an_exception_is_thrown()
        {
            var integerSetting = new HardwareSetting("test", HardwareSetting.IntegerType);

            AssertEx.Throws<InvalidCastException>(() => integerSetting.SetValue("hello"));
        }
    }
    // ReSharper restore InconsistentNaming
}
