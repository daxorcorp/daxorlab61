using System.Collections.Generic;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.HardwareConfigurationManager.Tests.HardwareConfigurationManager_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_hardware_configuration_manager
    {
        [TestMethod]
        public void Then_the_event_logger_instance_is_set()
        {
            var stubLogger = Substitute.For<IWindowsEventLogger>();
            var stubRepository = Substitute.For<IHardwareConfigurationRepository>();

            // ReSharper disable once UnusedVariable
            var manager = new HardwareConfigurationManagerWithExposedProperties(stubLogger, stubRepository);

            Assert.AreEqual(stubLogger, manager.GetEventLogger());
        }

        [TestMethod]
        public void Then_the_configuration_repository_instance_is_set()
        {
            var stubLogger = Substitute.For<IWindowsEventLogger>();
            var stubRepository = Substitute.For<IHardwareConfigurationRepository>();

            // ReSharper disable once UnusedVariable
            var manager = new HardwareConfigurationManagerWithExposedProperties(stubLogger, stubRepository);

            Assert.AreEqual(stubRepository, manager.GetHardwareConfigurationRepository());
        }

        [TestMethod]
        public void And_the_configuration_is_read_then_a_log_entry_is_made()
        {
            var mockLogger = Substitute.For<IWindowsEventLogger>();

            // ReSharper disable once UnusedVariable
            var manager = new HardwareConfigurationManager(mockLogger, Substitute.For<IHardwareConfigurationRepository>());

            mockLogger.Received().Log("A request was made to read the hardware settings.");
        }

        [TestMethod]
        public void And_the_configuration_exists_in_then_a_corresponding_log_entry_is_made()
        {
            var stubRepository = Substitute.For<IHardwareConfigurationRepository>();
            stubRepository.Filename.Returns("whatever.xml");
            stubRepository.Exists().Returns(true);
            var mockLogger = Substitute.For<IWindowsEventLogger>();

            // ReSharper disable once UnusedVariable
            var manager = new HardwareConfigurationManager(mockLogger, stubRepository);

            mockLogger.Received().Log("'whatever.xml' exists, so it will be loaded.");
        }

        [TestMethod]
        public void And_the_configuration_doesnt_exist_then_a_corresponding_log_entry_is_made()
        {
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubRepository = Substitute.For<IHardwareConfigurationRepository>();
            stubRepository.Exists().Returns(false);
            stubRepository.Filename.Returns("whatever.xml");
            stubRepository.DefaultFilename.Returns("default.xml");

            // ReSharper disable once UnusedVariable
            var manager = new HardwareConfigurationManager(mockLogger, stubRepository);

            mockLogger.Received().Log("'whatever.xml' does not exist, so default.xml will be used as defaults.");
        }

        [TestMethod]
        public void And_the_configuration_doesnt_exist_then_a_default_configuration_is_created()
        {
            var mockRepository = Substitute.For<IHardwareConfigurationRepository>();
            mockRepository.Exists().Returns(false);

            // ReSharper disable once UnusedVariable
            var manager = new HardwareConfigurationManager(Substitute.For<IWindowsEventLogger>(), mockRepository);

            mockRepository.Received().CreateDefaultConfiguration();
        }

        [TestMethod]
        public void And_the_configuration_doesnt_exist_then_the_default_configuration_is_read()
        {
            var mockRepository = Substitute.For<IHardwareConfigurationRepository>();
            mockRepository.Exists().Returns(false);

            // ReSharper disable once UnusedVariable
            var manager = new HardwareConfigurationManager(Substitute.For<IWindowsEventLogger>(), mockRepository);

            mockRepository.Received().ReadAll();
        }

        [TestMethod]
        public void Then_each_setting_is_loaded_into_the_manager()
        {
            var manager = new HardwareConfigurationManagerWithExposedProperties(Substitute.For<IWindowsEventLogger>(), CreateFakeRepositoryThatReadsFourSettings());

            Assert.AreEqual(4, manager.GetConfiguration().Count);
        }

        [TestMethod]
        public void And_the_configuration_read_is_null_then_the_manager_has_no_settings()
        {
            var manager = new HardwareConfigurationManagerWithExposedProperties(Substitute.For<IWindowsEventLogger>(), Substitute.For<IHardwareConfigurationRepository>());

            AssertEx.IsEmpty(manager.GetConfiguration());
        }

        [TestMethod]
        public void And_there_are_no_setting_elements_in_the_file_then_the_manager_has_no_settings()
        {
            var manager = new HardwareConfigurationManagerWithExposedProperties(Substitute.For<IWindowsEventLogger>(), Substitute.For<IHardwareConfigurationRepository>());

            AssertEx.IsEmpty(manager.GetConfiguration());
        }

        private static IHardwareConfigurationRepository CreateFakeRepositoryThatReadsFourSettings()
        {
            var stubRepository = Substitute.For<IHardwareConfigurationRepository>();
            stubRepository.ReadAll().Returns(new Dictionary<string, HardwareSetting>
            {
                {"stringKey1", new HardwareSetting("stringKey1", "string")},
                {"stringKey2", new HardwareSetting("stringKey2", "string")},
                {"stringKey3", new HardwareSetting("stringKey3", "string")},
                {"stringKey4", new HardwareSetting("stringKey4", "string")}
            });

            return stubRepository;
        }

    }
    // ReSharper restore InconsistentNaming
}
