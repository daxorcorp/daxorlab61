using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.HardwareConfigurationManager.Tests.HardwareConfigurationManager_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_a_setting
    {
        [TestMethod]
        public void And_given_a_key_that_exists_then_the_setting_is_retrieved()
        {
            var setting = new HardwareSetting("test", "string");
            var stubRepository = Substitute.For<IHardwareConfigurationRepository>();
            stubRepository.ReadAll().Returns(new Dictionary<string, HardwareSetting>
            {
                { setting.Name, setting }
            });

            var manager = new HardwareConfigurationManager(Substitute.For<IWindowsEventLogger>(), stubRepository);

            Assert.AreEqual(setting, manager.GetSetting("test"));
        }
    }
    // ReSharper restore InconsistentNaming
}
