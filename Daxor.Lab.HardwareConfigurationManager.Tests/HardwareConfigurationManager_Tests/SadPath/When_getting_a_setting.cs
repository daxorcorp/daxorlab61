using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.HardwareConfigurationManager.Tests.HardwareConfigurationManager_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_a_setting
    {
        [TestMethod]
        public void And_given_a_key_that_doesnt_exist_then_an_exception_is_thrown()
        {
            var manager = new HardwareConfigurationManager(Substitute.For<IWindowsEventLogger>(), Substitute.For<IHardwareConfigurationRepository>());

            AssertEx.Throws<ArgumentException>(() => manager.GetSetting("whatever"));
        }
    }
    // ReSharper restore InconsistentNaming
}
