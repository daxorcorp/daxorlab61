using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.HardwareConfigurationManager.Tests.HardwareConfigurationManager_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_manager
    {
        [TestMethod]
        public void And_the_given_logger_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentNullException>(() => new HardwareConfigurationManager(null, Substitute.For<IHardwareConfigurationRepository>()));
        }

        [TestMethod]
        public void And_the_given_configuration_repository_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentNullException>(() => new HardwareConfigurationManager(Substitute.For<IWindowsEventLogger>(), null));
        }
    }
    // ReSharper restore InconsistentNaming
}
