using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.HardwareConfigurationManager.Tests.HardwareConfigurationManager_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_resetting_to_the_default_configuration
    {
        [TestMethod]
        public void Then_a_log_entry_stating_that_this_action_is_happening_is_made()
        {
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var manager = new HardwareConfigurationManager(mockLogger, CreateFakeRepositoryThatReadsFourSettings());

            manager.ResetToDefaultConfiguration();

            mockLogger.Received().Log("A request was made to reset the hardware settings.");
        }

        [TestMethod]
        public void Then_the_repository_is_used_to_delete_the_file()
        {
            var mockRepository = Substitute.For<IHardwareConfigurationRepository>();
            var manager = new HardwareConfigurationManager(Substitute.For<IWindowsEventLogger>(), mockRepository);

            manager.ResetToDefaultConfiguration();

            mockRepository.Received().TryAndDelete();
        }

        [TestMethod]
        public void And_the_configuration_was_deleted_then_a_log_entry_is_made()
        {
            var mockLogger = Substitute.For<IWindowsEventLogger>();

            var stubRepository = Substitute.For<IHardwareConfigurationRepository>();
            stubRepository.Filename.Returns("whatever.xml");
            stubRepository.TryAndDelete().Returns(true);

            var manager = new HardwareConfigurationManager(mockLogger, stubRepository);

            manager.ResetToDefaultConfiguration();

            mockLogger.Received().Log("'whatever.xml' has been deleted.");
        }

        [TestMethod]
        public void And_the_configuration_doesnt_exist_then_a_log_entry_is_made()
        {
            var mockLogger = Substitute.For<IWindowsEventLogger>();

            var stubRepository = Substitute.For<IHardwareConfigurationRepository>();
            stubRepository.Filename.Returns("whatever.xml");
            stubRepository.TryAndDelete().Returns(false);

            var manager = new HardwareConfigurationManager(mockLogger, stubRepository);

            manager.ResetToDefaultConfiguration();

            mockLogger.Received().Log("'whatever.xml' did not exist. No modifications made.");
        }

        [TestMethod]
        public void Then_the_repository_is_used_to_create_a_default_configuration()
        {
            var mockRepository = Substitute.For<IHardwareConfigurationRepository>();
            mockRepository.Exists().Returns(true);

            var manager = new HardwareConfigurationManager(Substitute.For<IWindowsEventLogger>(), mockRepository);

            manager.ResetToDefaultConfiguration();

            mockRepository.Received().CreateDefaultConfiguration();
        }

        [TestMethod]
        public void Then_the_repository_is_used_to_reread_the_settings()
        {
            var mockRepository = Substitute.For<IHardwareConfigurationRepository>();
            mockRepository.Exists().Returns(true);

            var manager = new HardwareConfigurationManager(Substitute.For<IWindowsEventLogger>(), mockRepository);

            manager.ResetToDefaultConfiguration();

            mockRepository.Received(2).ReadAll(); // First is during initialization
        }

        [TestMethod]
        public void Then_the_configuration_is_reset_to_the_default_settings()
        {
            var oldConfiguration = new Dictionary<string, HardwareSetting>();
            var newConfiguration = new Dictionary<string, HardwareSetting>();
            Assert.AreNotEqual(oldConfiguration, newConfiguration);

            var stubRepository = Substitute.For<IHardwareConfigurationRepository>();
            stubRepository.ReadAll().Returns(oldConfiguration, newConfiguration);

            var manager = new HardwareConfigurationManagerWithExposedProperties(Substitute.For<IWindowsEventLogger>(), stubRepository);

            manager.ResetToDefaultConfiguration();

            Assert.AreEqual(newConfiguration, manager.GetConfiguration());
        }

        [TestMethod]
        public void And_the_reset_process_completed_then_a_log_entry_is_made()
        {
            var mockLogger = Substitute.For<IWindowsEventLogger>();

            var stubRepository = Substitute.For<IHardwareConfigurationRepository>();

            var manager = new HardwareConfigurationManager(mockLogger, stubRepository);

            manager.ResetToDefaultConfiguration();

            mockLogger.Received().Log("The default configuration has been restored.");
        }

        private static IHardwareConfigurationRepository CreateFakeRepositoryThatReadsFourSettings()
        {
            var stubRepository = Substitute.For<IHardwareConfigurationRepository>();
            stubRepository.ReadAll().Returns(new Dictionary<string, HardwareSetting>
            {
                {"stringKey1", new HardwareSetting("stringKey1", "string")},
                {"stringKey2", new HardwareSetting("stringKey2", "string")},
                {"stringKey3", new HardwareSetting("stringKey3", "string")},
                {"stringKey4", new HardwareSetting("stringKey4", "string")}
            });

            return stubRepository;
        }
    }
    // ReSharper restore InconsistentNaming
}
