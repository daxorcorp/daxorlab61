﻿using System.Collections.Generic;

namespace Daxor.Lab.HardwareConfigurationManager.Tests.HardwareConfigurationManager_Tests
{
    internal class HardwareConfigurationManagerWithExposedProperties : HardwareConfigurationManager
    {
        internal HardwareConfigurationManagerWithExposedProperties(IWindowsEventLogger eventLogger, IHardwareConfigurationRepository configurationRepository)
            : base(eventLogger, configurationRepository) { }

        internal IWindowsEventLogger GetEventLogger() { return EventLogger; }

        internal IHardwareConfigurationRepository GetHardwareConfigurationRepository() { return HardwareConfigurationRepository; }

        internal Dictionary<string, HardwareSetting> GetConfiguration() { return Configuration; }
    }
}
