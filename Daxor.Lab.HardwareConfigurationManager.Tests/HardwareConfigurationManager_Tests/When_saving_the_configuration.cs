using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.HardwareConfigurationManager.Tests.HardwareConfigurationManager_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_saving_the_configuration
    {
        [TestMethod]
        public void Then_a_log_entry_is_made_to_indicate_settings_are_being_written()
        {
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var manager = new HardwareConfigurationManager(mockLogger, Substitute.For<IHardwareConfigurationRepository>());

            manager.SaveConfiguration();

            mockLogger.Received().Log("A request was made to write settings.");
        }

        [TestMethod]
        public void Then_the_repository_is_used_to_write_the_configuration()
        {
            var mockRepository = Substitute.For<IHardwareConfigurationRepository>();
            var manager = new HardwareConfigurationManagerWithExposedProperties(Substitute.For<IWindowsEventLogger>(), mockRepository);

            manager.SaveConfiguration();

            mockRepository.Received().WriteAll(manager.GetConfiguration());
        }

        [TestMethod]
        public void Then_a_log_entry_is_made_when_settings_have_been_written()
        {
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubRepository = Substitute.For<IHardwareConfigurationRepository>();
            stubRepository.Filename.Returns("whatever");
            var manager = new HardwareConfigurationManager(mockLogger, stubRepository);

            manager.SaveConfiguration();

            mockLogger.Received().Log("'whatever' has been saved.");
        }
    }
    // ReSharper restore InconsistentNaming
}
