﻿using System;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.QC.Common.Events;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.QC.Common;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;

namespace Daxor.Lab.QC.ViewModels
{
    /// <summary>
    /// The viewModel for the samples values view
    /// </summary>
    public class QCSampleValuesViewModel : ViewModelBase, IActiveAware
    {
        #region Fields
        private readonly IQualityTestController _controller;
        private readonly IEventAggregator _eventAggregator;
        private QcSample _selectedSample;
        #endregion

        #region Properties
        public IQualityTestController Controller
        {
            get { return _controller; }
        }
        public QcSample SelectedSample
        {
            get { return _selectedSample; }
            set { 
                
                //If selected sample changed, we'll fire ss event
                if (base.SetValue(ref _selectedSample, "SelectedSample", value))
                    _eventAggregator.GetEvent<SampleSelected>().Publish(_selectedSample);
            }
        }
        #endregion

        #region Ctor
        public QCSampleValuesViewModel(IQualityTestController controller, IEventAggregator eventAggregator)
        {
            DisplayName = "QC Sample Values";
            _controller = controller;
            _eventAggregator = eventAggregator;
          
        }
        #endregion

        #region IActiveAware Members

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                if (!value)
                    SelectedSample = null;
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
