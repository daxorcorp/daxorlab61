﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.QC.Common;
using Daxor.Lab.QC.Common.Events;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.QC.Reports.ReportControllers;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Regions;

namespace Daxor.Lab.QC.ViewModels
{
    /// <summary>
    /// The viewModel for the results view
    /// </summary>
    public class QCResultsViewModel : ViewModelBase, IActiveAware
    {
        #region Fields

        /// <summary>
        /// The event aggregator.
        /// </summary>
        private readonly IEventAggregator _eventAggregator;

        /// <summary>
        /// The customer controller.
        /// </summary>
        private readonly IQualityTestController _controller;

        /// <summary>
        /// The messageBoxDispatcher
        /// </summary>
        private readonly IMessageBoxDispatcher _msgBox;

        private readonly ILoggerFacade _customLoggerFacade;

        private readonly IRegionManager _regionManager;

        private readonly DispatcherNotifiedObservableCollection<EvaluationResultViewModel> _eFailures = new DispatcherNotifiedObservableCollection<EvaluationResultViewModel>();
        private PropertyObserver<QcTest> _qcTestObserver;
        private PropertyObserver<QcSample> _selectSampleObserver;

        private DelegateCommand<Object> _printReportCommand;
        private DelegateCommand<Object> _viewReportCommand;
        private DelegateCommand<Object> _exportReportCommand;
        private QcSample _displayedSample;
        private string _sDescription;
        private string _calibrationCurveFriendly;
        private readonly IStarBurnWrapper _opticalDriveWrapper;
        private readonly IFolderBrowser _folderBrowser;
        private readonly IMessageManager _messageManager;
        private readonly ISettingsManager _settingsManager;
        private bool _isReportBeingViewed;

        #endregion

        #region Ctor

        public QCResultsViewModel(IQualityTestController controller, IMessageBoxDispatcher msgBox, IEventAggregator eventAggregator,
            ILoggerFacade customLoggerFacade, IRegionManager regionManager, IStarBurnWrapper driveWrapper, IFolderBrowser folderBrowser, IMessageManager messageManager, ISettingsManager settingsManager)
        {
            _controller = controller;
            _msgBox = msgBox;
            _eventAggregator = eventAggregator;
            _customLoggerFacade = customLoggerFacade;
            _regionManager = regionManager;
            _opticalDriveWrapper = driveWrapper;
            _folderBrowser = folderBrowser;
            _messageManager = messageManager;
            _settingsManager = settingsManager;

            var ctrObserver = new PropertyObserver<IQualityTestController>(_controller);
            _qcTestObserver = new PropertyObserver<QcTest>(_controller.SelectedTest);

            ctrObserver.RegisterHandler(c => c.SelectedTest, c => OnSelectedTestChanged(c.SelectedTest));
            OnSelectedTestChanged(_controller.SelectedTest);

            _eventAggregator.GetEvent<QCTestReEvaluateExecutionCommands>().Subscribe(OnQCTestReEvaluateExecutionCommands);
            SubscribeToEvents();
            BindCommands();
        }

        #endregion

        #region Properties

        public IQualityTestController Controller
        {
            get { return _controller; }
        }
        public QcSample DisplayedSample
        {
            get { return _displayedSample; }
            private set
            {
                if (base.SetValue(ref _displayedSample, "DisplayedSample", value))
                {
                    UpdateDisplayedSampleDescription();
                    //Remove all previous observers
                    if (_selectSampleObserver != null)
                    {
                        if (_displayedSample != null)
                            _selectSampleObserver.UnregisterHandler(s => s.Spectrum.SpectrumArray);

                        _selectSampleObserver = null;
                    }

                    //Start observing selected sample, as long as it's not null
                    if (_displayedSample != null)
                    {
                        _selectSampleObserver = new PropertyObserver<QcSample>(_displayedSample);
                        _selectSampleObserver.RegisterHandler(s => s.Spectrum.SpectrumArray, DisplaySampleSpectrumArrayChangedHandler);
                    }
                }
            }
        }
        public string DisplayedSampleDescription
        {
            get { return _sDescription; }
            private set { base.SetValue(ref _sDescription, "DisplayedSampleDescription", value); }
        }

        public string DisplayedErrorDetails
        {
            get { return _errorDetails; }
            private set { base.SetValue(ref _errorDetails, "DisplayedErrorDetails", value); }
        }
        
        public bool HasAnyComments
        {
            get
            {
                if (_controller != null && _controller.SelectedTest != null)
                    return !String.IsNullOrWhiteSpace(_controller.SelectedTest.Comments);

                return false;
            }
        }
        public string CurrentCalibrationCurveFriendly
        {
            get { return _calibrationCurveFriendly; }
            private set { base.SetValue(ref _calibrationCurveFriendly, "CurrentCalibrationCurveFriendly", value); }
        }
        public ObservableCollection<EvaluationResultViewModel> Failures
        {
            get { return _eFailures; }
        }

        #endregion

        #region Helpers

        void OnSelectedTestChanged(QcTest newTest)
        {
            if (_controller == null || _controller.SelectedTest == null)
                return;

            _qcTestObserver = new PropertyObserver<QcTest>(_controller.SelectedTest);
            _qcTestObserver.RegisterHandler(c => c.Comments, c => FirePropertyChanged("HasAnyComments"));
            _qcTestObserver.RegisterHandler(c => c.CalibrationCurve, c => FormatCalibrationCurve(c.CalibrationCurve));
            _qcTestObserver.RegisterHandler(c => c.EvaluationResultCount, c =>
            {
                Failures.Clear();
                foreach (var e in _controller.SelectedTest.EvaluationResults)
                {
                    string sName = _controller.SelectedTest.Samples.Where(s => s.InternalId == e.SampleId).Select(s => s.DisplayName).FirstOrDefault();
                    Failures.Add(new EvaluationResultViewModel(sName, e));
                }
            });
           
            Failures.Clear();
            foreach (var e in _controller.SelectedTest.EvaluationResults)
            {
                var sName = _controller.SelectedTest.Samples.Where(s => s.InternalId == e.SampleId).Select(s => s.DisplayName).FirstOrDefault();
                Failures.Add(new EvaluationResultViewModel(sName, e));
            }
        }

        void OnViewReportExecuteChanged(bool isReportShowing)
        {
            _isReportBeingViewed = isReportShowing;
            ViewReportCommand.RaiseCanExecuteChanged();
        }

        void BindCommands()
        {
            _printReportCommand = new DelegateCommand<object>(PrintReportCommandExecute, PrintReportCommandCanExecute);
            _viewReportCommand = new DelegateCommand<object>(ViewReportCommandExecute, ViewReportCommandCanExecute);
            _exportReportCommand = new DelegateCommand<object>(ExportReportCommandExecute, ExportReportCommandCanExecute);

        }
        void SubscribeToEvents()
        {
            _eventAggregator.GetEvent<SampleSelected>().Subscribe(SampleSelectedEventHandler);
            _eventAggregator.GetEvent<ViewReportExecuteChanged>().Subscribe(OnViewReportExecuteChanged);
        }

        void UnsubscribeFromEvents()
        {
            _eventAggregator.GetEvent<SampleSelected>().Unsubscribe(SampleSelectedEventHandler);
        }

        void SampleSelectedEventHandler(QcSample sample)
        {
            DisplayedSample = sample;
        }
        void DisplaySampleSpectrumArrayChangedHandler(QcSample sample)
        {
            //get the spectrum and show new counts
        }
        void UpdateDisplayedSampleDescription()
        {
            string newDescription = String.Empty;

            if (DisplayedSample != null)
                newDescription = DisplayedSample.DisplayName;

            if (DisplayedSample != null && DisplayedSample.Isotope != null && DisplayedSample.Isotope.Name != QC.Core.Common.IsotopeType.Empty)
                newDescription += " (" + DisplayedSample.Isotope.Name.ToString() + ")";

            DisplayedSampleDescription = newDescription;
        }
        
        void FormatCalibrationCurve(ICalibrationCurve cc)
        {
            string newCurve = string.Empty;
            newCurve = "Energy = " + cc.Slope + " * channel";
            if (cc.Offset < 0)
                newCurve += " - ";
            else
                newCurve += " + ";

            newCurve += Math.Abs(cc.Offset);

            CurrentCalibrationCurveFriendly = newCurve;
        }

        #endregion

        #region Commands

        public ICommand ExportReportCommand
        {
            get { return _exportReportCommand; }
        }
        void ExportReportCommandExecute(Object state)
        {
            QcReportController reportController = new QcReportController(_customLoggerFacade, _msgBox, _regionManager, _controller.SelectedTest, _eventAggregator, _opticalDriveWrapper, _folderBrowser, _messageManager, _settingsManager);

            reportController.ExportReport();
        }
        bool ExportReportCommandCanExecute(Object state)
        {
            return (Controller.SelectedTest.Status != Domain.Common.TestStatus.Running) &&
                (Controller.SelectedTest.Status != Domain.Common.TestStatus.Pending); ;
        }
        
        public ICommand PrintReportCommand
        {
            get { return _printReportCommand; }
        }
        void PrintReportCommandExecute(Object state)
        {
            //Using TPL to perform view report
            Task printReport =
                Task.Factory.StartNew<Boolean>((stateObj) =>
                {
                    QcTest testToPrint = (QcTest)stateObj;
                    _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Printing QC Report..."));
                    QcReportController reportController = new QcReportController(_customLoggerFacade, _msgBox, _regionManager, _controller.SelectedTest, _eventAggregator, _opticalDriveWrapper, _folderBrowser, _messageManager, _settingsManager);
                    IdealDispatcher.BeginInvoke(() =>
                    {
                        reportController.PrintReport();
                    });
                    return true;
                }, Controller.SelectedTest).ContinueWith(answer =>
                {
                    _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
                }, TaskScheduler.FromCurrentSynchronizationContext());
        }
        bool PrintReportCommandCanExecute(Object state)
        {
            return (Controller.SelectedTest.Status != Domain.Common.TestStatus.Running) &&
                (Controller.SelectedTest.Status != Domain.Common.TestStatus.Pending); ;
        }
       
        public DelegateCommand<object> ViewReportCommand
        {
            get { return _viewReportCommand; }
        }

        void ViewReportCommandExecute(Object state)
        {
            //Using TPL to perform view report
            Task.Factory.StartNew(stateObj =>
            {
                _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Loading Quality Control Test Report..."));
                var reportController = new QcReportController(_customLoggerFacade, _msgBox, _regionManager, _controller.SelectedTest, _eventAggregator, _opticalDriveWrapper, _folderBrowser, _messageManager, _settingsManager);
                IdealDispatcher.BeginInvoke(reportController.ShowReport);
                return true;
            }, Controller.SelectedTest).ContinueWith(answer => _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false)), TaskScheduler.FromCurrentSynchronizationContext());
        }

        bool ViewReportCommandCanExecute(Object state)
        {
            return (Controller.SelectedTest.Status != Domain.Common.TestStatus.Running) && 
                (Controller.SelectedTest.Status != Domain.Common.TestStatus.Pending) && !_isReportBeingViewed;
        }

        
        #endregion

        #region IActiveAware Members

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                if (!value)
                {
                    UnsubscribeFromEvents();
                    if (_controller.SelectedTest.Status != Domain.Common.TestStatus.Pending)
                        _controller.DataService.SaveTest(_controller.SelectedTest);
                }
                else
                {
                    var commands = QCNavigationCommands.NavigateTestResults.RegisteredCommands;
                    foreach (var cmd in commands)
                        ((DelegateCommand<object>)cmd).RaiseCanExecuteChanged();
                }

                if (Controller.SelectedTest != null)
                    FormatCalibrationCurve(Controller.SelectedTest.CalibrationCurve);
            }
        }

        public event EventHandler IsActiveChanged = delegate { };
        private string _errorDetails;

        #endregion

        public void OnQCTestReEvaluateExecutionCommands (object voidPointer)
        {
            _printReportCommand.RaiseCanExecuteChanged();
            _viewReportCommand.RaiseCanExecuteChanged();
            _exportReportCommand.RaiseCanExecuteChanged();
        }
    }

    public class EvaluationResultViewModel
    {
        private readonly QcSampleEvaluationMetadata _eMetadata;
        private readonly string _sampleName;

        public EvaluationResultViewModel(String sampleName, QcSampleEvaluationMetadata eMetadata)
        {
            _eMetadata = eMetadata;
            _sampleName = sampleName;
        }

        public String Message
        {
            get { return _eMetadata.Message; }
        }
        public String SampleName
        {
            get { return _sampleName; }
        }
    }
}
