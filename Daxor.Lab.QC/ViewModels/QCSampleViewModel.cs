﻿using System;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.QC.Core;

namespace Daxor.Lab.QC.ViewModels
{
    /// <summary>
    /// The viewModel for the sample model
    /// </summary>
    public class QCSampleViewModel : ViewModelBase
    {
        readonly QcSample _sample;
        String _dResult = String.Empty;

        public QCSampleViewModel(QcSample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample cannot be null.");

            _sample = sample;
        }

        #region Properties

        public String DetailedResultDescription
        {
            get { return _dResult; }
            set { base.SetValue(ref _dResult, "DetailedResultDescription", value); }
        }
        public Int32 ChangerLocation
        {
            get { return _sample.Position; }
        }

        #endregion
    }
}
