﻿using System;
using System.ComponentModel;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.QC.Common;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Presentation.Commands;

namespace Daxor.Lab.QC.ViewModels
{
    /// <summary>
    /// The viewModel for the setup view
    /// </summary>
    public class QCSetupViewModel : ViewModelBase, IActiveAware 
    {
        #region Private fields

        private readonly IQualityTestController _controller;
        private bool _shouldAnalystReceiveFocus;
        private bool _isActive;

        #endregion

        #region Ctor

        public QCSetupViewModel(IQualityTestController controller, ISettingsManager settingsManager)
        {
            if (controller == null)
                throw new ArgumentNullException("controller");
            if (settingsManager == null)
                throw new ArgumentNullException("settingsManager");
            
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            DisplayName = "QC Test Setup";
            // ReSharper restore DoNotCallOverridableMethodsInConstructor

            _controller = controller;

            SettingsManager = settingsManager;
            IsInjectateVerificationEnabled = SettingsManager.GetSetting<bool>(SettingKeys.SystemInjectateVerificationEnabled);
            
            SettingObserver = new SettingObserver<ISettingsManager>(SettingsManager);
            SettingObserver.RegisterHandler(SettingKeys.SystemInjectateVerificationEnabled,
                                            s => IsInjectateVerificationEnabled = SettingsManager.GetSetting<bool>(SettingKeys.SystemInjectateVerificationEnabled));

            // Geoff testing
            Controller.PropertyChanged += ControllerOnPropertyChanged;
        }

       

        #endregion

        #region Properties

        public IQualityTestController Controller
        {
            get { return _controller; }
        }

        public bool IsInjectateVerificationEnabled { private set; get; }

        public bool StandardAKeyHasFocus { set; get; }

        public bool StandardBKeyHasFocus { set; get; }

        public ISettingsManager SettingsManager { private set; get; }

        public bool ShouldAnalystReceiveFocus
        {
            get { return _shouldAnalystReceiveFocus; }
            set { base.SetValue(ref _shouldAnalystReceiveFocus, "ShouldAnalystReceiveFocus", value); }
        }

        private SettingObserver<ISettingsManager> SettingObserver { get; set; }

        private QcTest CurrentTest { get; set; }

        #endregion

        #region IActiveAware Members
        
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                EvaluateIfAnalystShouldReceiveFocus(Controller.SelectedTest);
                if (_controller.SelectedTest.Status != Domain.Common.TestStatus.Pending)
                    _controller.DataService.SaveTest(_controller.SelectedTest);
            }
        }

        public event EventHandler IsActiveChanged = delegate { };
        

        #endregion

        #region Helpers

        void EvaluateIfAnalystShouldReceiveFocus(ITest test)
        {
            if (test == null)
            {
                ShouldAnalystReceiveFocus = false;
                return;
            }
            var testIsPending = test.Status == Domain.Common.TestStatus.Pending;
            var analystIsEmpty = String.IsNullOrWhiteSpace(test.Analyst);
            ShouldAnalystReceiveFocus = testIsPending && analystIsEmpty;
        }

        private void ControllerOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName != "SelectedTest") 
                return;

            // Unsubscribe from the old test's rule engine
            if (CurrentTest != null)
                CurrentTest.PropertyChanged -= RuleEngineOnRulesEvaluated;

            // Subscribe to the new test's rule engine
            CurrentTest = Controller.SelectedTest;
            CurrentTest.RuleEngine.RulesEvaluated += RuleEngineOnRulesEvaluated;
        }

        private void RuleEngineOnRulesEvaluated(object sender, EventArgs eventArgs)
        {
            // The rule engine has finished evaluating, which affects whether we can navigate to test results
            var commands = QCNavigationCommands.NavigateTestResults.RegisteredCommands;
            foreach (var cmd in commands)
                ((DelegateCommand<object>) cmd).RaiseCanExecuteChanged();

            // ...and if we can start the test
            ((DelegateCommand<object>)Controller.StartSelectedTestExecutionCommand).RaiseCanExecuteChanged();
        }
        #endregion
    }
}
