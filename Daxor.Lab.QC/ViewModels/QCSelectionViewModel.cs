﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Daxor.Lab.Infrastructure.Alerts;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.SystemNotifications;
using Daxor.Lab.QC.Common;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.QC.Reports.ReportControllers;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Regions;
using Telerik.Windows.Data;

namespace Daxor.Lab.QC.ViewModels
{
    /// <summary>
    /// The viewModel for the selection view
    /// </summary>
    public class QCSelectionViewModel : ViewModelBase, IActiveAware
    {
        #region Fields
        
        private readonly IQualityTestController _controller;
        private readonly IEventAggregator _eventAggregator;
        private readonly IMessageBoxDispatcher _msgBox;
        private readonly IRegionManager _regionManager;
        private readonly ILoggerFacade _customLoggerFacade;
        private readonly ISettingsManager _settingsManager;
        private readonly DelegateCommand<QcTestItem> _loadTestCommand;
        private readonly DelegateCommand<object> _printQCTestListCommand;
        private QcTestItem _selectedTestItem;
        private bool _isQCTestNeeded, _isLinearityNeeded, _isContaminationNeeded, _isStandardsNeeded,
               _calibrationFailed, _contaminationFailed, _standardsFailed, _isSourcesOnlyNeeded;
        private bool _canSelectQCTest; 
        private readonly System.Timers.Timer _timer;
        
        #endregion

        #region Ctor
        
        public QCSelectionViewModel(IQualityTestController controller, IEventAggregator eventAggregator, IMessageBoxDispatcher msgBox, 
                                        IRegionManager regionManager, ILoggerFacade customLoggerFacade, ISettingsManager settingsManager)
        {
            _controller = controller;
            _eventAggregator = eventAggregator;
            _msgBox = msgBox;
            _regionManager = regionManager;
            _customLoggerFacade = customLoggerFacade;
            _settingsManager = settingsManager;
            TestItems = new VirtualQueryableCollectionView(_controller.DataService.GetTestItems()) { LoadSize = 16 };

            _controller.TestItemsNeedRefresh += (o, e) => RefreshTestItemsIfActive();

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            DisplayName = "QC Test Selection";
            // ReSharper restore DoNotCallOverridableMethodsInConstructor

            _canSelectQCTest = true;
            
            _loadTestCommand = new DelegateCommand<QcTestItem>(LoadTestCommandExecute, LoadTestCommandCanExecute);
            _printQCTestListCommand = new DelegateCommand<object>(PrintQCListReportCommandExecute, PrintQCListReportCommandCanExecute);
            _eventAggregator.GetEvent<QCTestDueChanged>().Subscribe(OnQCTestDueChanged, Microsoft.Practices.Composite.Presentation.Events.ThreadOption.PublisherThread, false);
            _eventAggregator.GetEvent<ManualRestoreStarted>().Subscribe(OnManualRestoreStarted, Microsoft.Practices.Composite.Presentation.Events.ThreadOption.PublisherThread, false);
            _eventAggregator.GetEvent<ManualRestoreCompleted>().Subscribe(OnManualRestoreCompleted, Microsoft.Practices.Composite.Presentation.Events.ThreadOption.PublisherThread, false);

            _timer = new System.Timers.Timer(30 * 1000);
            _timer.Elapsed += OnTimerElapsed;
            _timer.Start();
        }

        #endregion

        #region Properties

        public QcTestItem SelectedTestItem
        {
            get { return _selectedTestItem; }
            set { base.SetValue(ref _selectedTestItem, "SelectedTestItem", value); }
        }

        public int EndingTestItemIndex{ get; set; }
        public int BeginningTestItemIndex{ get; set; }
        public VirtualQueryableCollectionView TestItems{ get; private set; }
        public IQualityTestController Controller{get { return _controller; }}
        public bool IsRunning{get { return _controller.RunningTest != null; }}

        public bool IsQCTestNeeded
        {
            get { return _isQCTestNeeded; }
            set { base.SetValue(ref _isQCTestNeeded, "IsQCTestNeeded", value); }
        }

        public bool IsLinearityNeeded
        {
            get { return _isLinearityNeeded; }
            set { base.SetValue(ref _isLinearityNeeded, "IsLinearityNeeded", value); }
        }

        public bool IsContaminationNeeded
        {
            get { return _isContaminationNeeded; }
            set { base.SetValue(ref _isContaminationNeeded, "IsContaminationNeeded", value); }
        }

        public bool IsStandardsNeeded
        {
            get { return _isStandardsNeeded; }
            set { base.SetValue(ref _isStandardsNeeded, "IsStandardsNeeded", value); }
        }

        public bool IsSourcesOnlyNeeded
        {
            get { return _isSourcesOnlyNeeded; }
            set { base.SetValue(ref _isSourcesOnlyNeeded, "IsSourcesOnlyNeeded", value); }
        }

        public bool CalibrationFailed
        {
            get { return _calibrationFailed; }
            set { base.SetValue(ref _calibrationFailed, "CalibrationFailed", value); }
        }

        public bool ContaminationFailed
        {
            get { return _contaminationFailed; }
            set { base.SetValue(ref _contaminationFailed, "ContaminationFailed", value); }
        }

        public bool StandardsFailed
        {
            get { return _standardsFailed; }
            set { base.SetValue(ref _standardsFailed, "StandardsFailed", value); }
        }

        public bool SourcesFailed
        {
            get { return _sourcesFailed; }
            set { base.SetValue(ref _sourcesFailed, "SourcesFailed", value); }
        }

        #endregion

        #region Commands

        public ICommand LoadTestCommand { get { return _loadTestCommand; } }

        void LoadTestCommandExecute(QcTestItem item)
        {
            Controller.SelectTestCommand.Execute(item.TestId);
        }

        bool LoadTestCommandCanExecute(QcTestItem item)
        {
            return item != null;
        }

        public ICommand PrintQCListReportCommand { get { return _printQCTestListCommand; } }

        void PrintQCListReportCommandExecute(object obj)
        {
            Task.Factory.StartNew(() =>
             {
                 _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Printing QC Test List Report..."));
                 var reportController = new QCTestListReportController(_settingsManager);

                 var zeroBasedBeginningIndex = BeginningTestItemIndex - 1;
                 var testItemsList = TestItems.Cast<QcTestItem>();
                 reportController.PrintQcTestList(testItemsList.Skip(zeroBasedBeginningIndex).Take(EndingTestItemIndex - zeroBasedBeginningIndex));
             }).ContinueWith(x => _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false)), TaskScheduler.FromCurrentSynchronizationContext());
        }

        bool PrintQCListReportCommandCanExecute(object obj)
        {
            return TestItems.VirtualItemCount > 0;
        }

        #endregion
        
        #region Event Handlers

        void OnManualRestoreStarted(object voidPointer)
        {
            _canSelectQCTest = false;
        }

        void OnManualRestoreCompleted(object voidPointer)
        {
            _canSelectQCTest = true;
        }

        void OnQCTestDueChanged(object voidPointer)
        {
            VerifyAndUpdateSystemQCState();
        }

        private void OnTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            VerifyAndUpdateSystemQCState();
        }

        #endregion

        #region IActiveAware Members

        private bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;

                if (value)
                    VerifyAndUpdateSystemQCState();
                
                RefreshTestItemsIfActive();
            }
        }

        public event EventHandler IsActiveChanged = delegate { };
        private bool _sourcesFailed;

        #endregion

        #region Helper Methods

        void RefreshTestItemsIfActive()
        {
            if (!_isActive) return;
            TestItems.VirtualItemCount = 0;
            TestItems.VirtualItemCount = _controller.DataService.GetTestItems().Count();
            ((DelegateCommand<object>) PrintQCListReportCommand).RaiseCanExecuteChanged();
        }

        void VerifyAndUpdateSystemQCState()
        {
            if (!_canSelectQCTest)
                return;

            var interval = _settingsManager.GetSetting<int>(SettingKeys.QcDailyQcExpirationTime) * -1;

            var qcExpiredDateTime = DateTime.Now.AddSeconds(interval);
            var recentQcTestSample = RecentQCTestSample.GetRecentQCTestSamples(_settingsManager);

            IsQCTestNeeded = false;

            IsLinearityNeeded = false;
            IsContaminationNeeded = false;
            IsStandardsNeeded = false;
            IsSourcesOnlyNeeded = false;

            CalibrationFailed = false;
            ContaminationFailed = false;
            StandardsFailed = false;
            SourcesFailed = false;

            for (var i = 0; i < recentQcTestSample.Length; i++)
            {
                // if array[i] is null, then we know that some QC test is required
                if (recentQcTestSample[i] == null)
                    UpdateNeededTests(i+1);
                // If the current QC expired before the previous test was ran (and the test passed), then we need to updateNeeded tests
                else if (qcExpiredDateTime >= recentQcTestSample[i].TestDate && recentQcTestSample[i].Pass)
                    UpdateNeededTests(recentQcTestSample[i].Position);
                // If the sample failed, then we need to update Failed tests appropriately
                else if (!recentQcTestSample[i].Pass)
                    UpdateFailedTests(recentQcTestSample[i].Position);
            }

            if (((_settingsManager.GetSetting<DateTime>(RuntimeSettingKeys.SystemStartupDateTime) >
                   _settingsManager.GetSetting<DateTime>(SettingKeys.QcLastSuccessfulFullQcOrLinearityDate)) ||
                IsLinearityNeeded || SourcesFailed))
            {
                IsSourcesOnlyNeeded = true;
            }

            //Only Fire an alert if it is due and we aren't running a QC test
            if ((IsContaminationNeeded || IsSourcesOnlyNeeded ||IsStandardsNeeded ||
                CalibrationFailed || ContaminationFailed || StandardsFailed || SourcesFailed)&&(Controller.RunningTest==null))
            {
                _settingsManager.SetSetting(SettingKeys.QcTestDue, true);
                var systemNotification = SystemNotificationArray.Notifications[(int)SystemNotificationArray.NotificationIndex.QcDue];
                
                var payloadRemove = new AlertViolationPayload(systemNotification.SystemNotificationId)
                    {
                        RemoveAlertFromList = true
                    };

                _eventAggregator.GetEvent<AlertViolationChanged>().Publish(payloadRemove);
                
                var payload = (AlertViolationPayload)systemNotification.SystemNotificationData;
                if ((IsContaminationNeeded || ContaminationFailed) && 
                    (IsStandardsNeeded || StandardsFailed) &&
                    (IsSourcesOnlyNeeded || SourcesFailed))
                {
                    payload.Resolution = AlertResolutionPayload.FullQCAlert;
                }
                else if (IsContaminationNeeded || ContaminationFailed)
                    payload.Resolution = AlertResolutionPayload.ContaminationQCAlert;
                else if (IsStandardsNeeded || StandardsFailed)
                    payload.Resolution = AlertResolutionPayload.StandardsQCAlert;
                else if (IsSourcesOnlyNeeded || SourcesFailed)
                    payload.Resolution = AlertResolutionPayload.LinearityQCAlert;
                 
                systemNotification.SystemNotificationDateTime = DateTime.Now;
                systemNotification.RemoveFromQue = false;

                _eventAggregator.GetEvent<SystemNotificationOccurred>().Publish(systemNotification);
            }
            else
            {
                _settingsManager.SetSetting(SettingKeys.QcTestDue, false);
                SystemNotification systemNotification =
                                SystemNotificationArray.Notifications[(int)SystemNotificationArray.NotificationIndex.QcDue];

                var payload = new AlertViolationPayload(systemNotification.SystemNotificationId)
                    {
                        RemoveAlertFromList = true
                    };

                _eventAggregator.GetEvent<AlertViolationChanged>().Publish(payload);
            }
        }

        private void UpdateFailedTests(int samplePosition)
        {
            IsQCTestNeeded = true;
            if (samplePosition == 1)
                CalibrationFailed = true;
            else if (samplePosition == 2 || samplePosition == 21 || samplePosition == 22)
                SourcesFailed = true;
            else if (samplePosition == 11 || samplePosition == 12)
                StandardsFailed = true;
            else
                ContaminationFailed = true;
        }

        private void UpdateNeededTests(int samplePosition)
        {
            IsQCTestNeeded = true;
            if (samplePosition == 1)
                return;
            if (samplePosition == 2 || samplePosition == 21 || samplePosition == 22)
                IsLinearityNeeded = true;
            else if (samplePosition == 11 || samplePosition == 12)
                IsStandardsNeeded = true;
            else
                IsContaminationNeeded = true;
        }

        #endregion
    }
}
