﻿using System;
using System.Linq;
using System.Windows;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.QC.Common;
using Daxor.Lab.QC.Interfaces;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.QC
{
    public class Module : AppModule
    {
        private IQualityTestController _controller;
 
        public Module(IUnityContainer container) : base(AppModuleIds.QualityControl, container)
        {
            ShortDisplayName = "Quality\nControl";
            FullDisplayName = "Quality Control";
            DisplayOrder = 2;
            BuildDate = DateTime.Now.AddDays(-10);
            IsConfigurable = true;
        }

        public override void InitializeAppModule()
        {
            // Initialize local resource dictionary
	        try
	        {
		        AddResourceDictionaryToMergedDictionaries();
	        }
	        catch
	        {
				throw new ModuleLoadException(FullDisplayName, "add the resource dictionary");
	        }

	        RegisterQcModuleNavigator();
            RegisterQcRuleEngine();
            ResolveQcRuleEngine();

            try
            {
                InitializeAndConfigureNavigationCommands();
            }
            catch
            {
                throw new ModuleLoadException(FullDisplayName, "initialize and configure navigation commands");
            }

            InitializeAppModuleNavigator();
        }

        protected virtual void AddResourceDictionaryToMergedDictionaries()
	    {
		    Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary
			    {
				    Source = new Uri(@"pack://application:,,,/Daxor.Lab.QC;component/Common/Resources.xaml")
			    });
	    }

	    /// <summary>
        /// Initializes and configures NavigationCompositeCommands
        /// </summary>
        protected virtual void InitializeAndConfigureNavigationCommands()
        {
	        _controller = UnityContainer.Resolve<IQualityTestController>();
	        //Navigate to test selection
	        QCNavigationCommands.NavigateTestSelection.RegisterCommand(new DelegateCommand<Object>(o =>
	        {
	            VirtualKeyboardManager.CloseActiveKeyboard();
	            if (_controller.SelectedTest != null && _controller.SelectedTest.Status == TestStatus.Pending)
	            {
	                var msgBox = UnityContainer.Resolve<IMessageBoxDispatcher>();
	                var choices = new[] {"Discard Test", "Cancel"};

	                int result = msgBox.ShowMessageBox(MessageBoxCategory.Question,
	                    "All entered information will be lost. Do you wish to discard entered information?",
	                    "Discard Changes",
	                    choices);

	                if (choices[result] == "Cancel")
	                    return;
	            }
	            UnityContainer.Resolve<IAppModuleNavigator>(UniqueAppId)
	                .ActivateView(DestinationViewKeys.QcTestSelection);
	        }, o => true));

	        // Navigate to test setup
	        QCNavigationCommands.NavigateTestSetup.RegisterCommand(new DelegateCommand<Object>(o =>
	        {
	            VirtualKeyboardManager.CloseActiveKeyboard();
	            UnityContainer.Resolve<IAppModuleNavigator>(UniqueAppId).ActivateView(DestinationViewKeys.QcTestSetup);
	        }, o => true));

	        // Navigate to test results
	        QCNavigationCommands.NavigateTestResults.RegisterCommand(new DelegateCommand<Object>(o =>
	        {
	            VirtualKeyboardManager.CloseActiveKeyboard();
	            UnityContainer.Resolve<IAppModuleNavigator>(UniqueAppId)
	                .ActivateView(DestinationViewKeys.QcTestResults);
	        }, o =>
	        {
	            // Don't navigate if the test has broken rules
	            if (_controller.SelectedTest == null) return false;
	            var brokenRules = _controller.SelectedTest.RuleEngine.GetBrokenRulesOnModel(_controller.SelectedTest);
	            return brokenRules.Count(rule => rule.IsEnabled) == 0;
	        }
	            ));
        }

        private void InitializeAppModuleNavigator()
        {
            try
            {
                UnityContainer.Resolve<IAppModuleNavigator>(UniqueAppId).Initialize();
            }
            catch
            {
                throw new ModuleLoadException(FullDisplayName, "initialize the Module Navigator");
            }
        }

        private void ResolveQcRuleEngine()
        {
            try
            {
                var ruleEngine = (QCRuleEngine)UnityContainer.Resolve<IRuleEngine>(UniqueAppId);
                ruleEngine.Initialize();
            }
            catch
            {
                throw new ModuleLoadException(FullDisplayName, "initialize the Rule Engine");
            }
        }

        private void RegisterQcRuleEngine()
        {
            try
            {
                UnityContainer.RegisterType<IRuleEngine, QCRuleEngine>(UniqueAppId, new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(FullDisplayName, "register the Rule Engine");
            }
        }

        private void RegisterQcModuleNavigator()
        {
            try
            {
                UnityContainer.RegisterType<IAppModuleNavigator, QCModuleNavigator>(UniqueAppId,
                    new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(FullDisplayName, "initialize the Module Navigator");
            }
        }
    }
}
