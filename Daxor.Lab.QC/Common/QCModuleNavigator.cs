﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.QC.Views;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.QC.Common
{
    /// <summary>
    /// QC module navigator. 
    /// </summary>
    public class QCModuleNavigator : IAppModuleNavigator
    {
        #region Fields

        readonly string _moduleKey;
        readonly IEventAggregator _eventAggregator;
        readonly IUnityContainer _container;
        readonly IRegionManager _regionManager;
        readonly ILoggerFacade _logger;

        readonly List<string> _validViewKeys = new List<string>();
        Object _innerHostView = null;
        IRegionManager _scopedInnerRegionManager, _scopedOutterRegionManager;
        Tuple<String, Object, IRegion> _activeViewRegion = null;    //ViewName, ViewInstance and Region
        bool _isInitialized = false;

        #endregion

        #region Ctor

        public QCModuleNavigator(IUnityContainer container, IEventAggregator eventAggregator, IRegionManager regionManager, INavigationCoordinator navigationCoordinator, ILoggerFacade logger)
        {
            //Cache all my dependencies
            _container = container;
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _logger = logger;

            //Set my local application key
            _moduleKey = AppModuleIds.QualityControl;
            
            //Register myself with a global application coordinator
            NavigationCoordinator = navigationCoordinator;
            navigationCoordinator.RegisterAppModuleNavigator(_moduleKey, this);

            //Valid view keys
            _validViewKeys.AddRange(new String[] { DestinationViewKeys.QcTestResults, DestinationViewKeys.QcTestSelection, DestinationViewKeys.QcTestSetup });
        }

        #endregion

        #region Properties

        public bool IsActive
        {
            get { return NavigationCoordinator.ActiveAppModuleKey.Equals(_moduleKey); }
        }
        public INavigationCoordinator NavigationCoordinator
        {
            get;
            private set;
        }
        public string AppModuleKey  //Unique application module key
        {
            get { return _moduleKey; }
        }
        public FrameworkElement RootViewHost
        {
            get;
            private set;
        }

        #region Private 

        private IRegion OuterRegion //Region switches TestSelection, TestSetup
        {
            get;
            set;
        }
        private IRegion InnerRegion //Region switches TestSetup, TestResults
        {
            get;
            set;
        }
        
        #endregion

        #endregion

        #region Methods

        public bool CanDeactivate()
        {
            return true;
        }
        public bool CanActivate()
        {
            return true;
        }

        public void Activate(DisplayViewOption displayViewOption, object payload)
        {
            if (IsActive)
                return;

            ThrowsExceptionIfNotInitialized();

            //Fire aggregate event to display MainMenuDropDown part of application
            _eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(ApplicationParts.MainMenuDropDown, true));
            _eventAggregator.GetEvent<ActiveViewTitleChanged>().Publish("Quality Control");

            //Inner region creation
            if (InnerRegion == null)
            {
                _innerHostView = _container.Resolve<InnerHost>();
                _scopedInnerRegionManager = _scopedOutterRegionManager.Regions[ModuleRegions.QCOuterRegion].Add(_innerHostView, null, true);
                InnerRegion = _scopedInnerRegionManager.Regions[ModuleRegions.QCInnerRegion];
                InnerRegion.Add(_container.Resolve<QCSetupView>(), DestinationViewKeys.QcTestSetup);
                InnerRegion.Add(_container.Resolve<QCSampleValuesView>(), DestinationViewKeys.QcTestResults);

            }

            ActivateView(DestinationViewKeys.QcTestSelection, payload);
        }
        public void Deactivate(Action onDeactivateCallback)
        {
            if (IsActive == false) return;

            //Close the keyboard
            VirtualKeyboardManager.CloseActiveKeyboard();

            //Go back
            OuterRegion.Activate(OuterRegion.Views.First());

            //Deactive currently active view
            if (_activeViewRegion != null && _activeViewRegion.Item3 != null && _activeViewRegion.Item2 != null)
            {
                _activeViewRegion.Item3.Deactivate(_activeViewRegion.Item2);
            }

            //Deactive existing views and results, except the selectionView
            DeactiveAndDestoryRegionViews(InnerRegion);
            DeactiveAndDestoryRegionViews(_scopedInnerRegionManager.Regions[ModuleRegions.ResultsRegion]);

            //Deactive innerHostView
            if (InnerRegion != null)
            {
                if (_innerHostView != null && _scopedInnerRegionManager != null)
                {
                    _scopedOutterRegionManager.Regions[ModuleRegions.QCOuterRegion].Deactivate(_innerHostView);
                    _scopedOutterRegionManager.Regions[ModuleRegions.QCOuterRegion].Remove(_innerHostView);
                    _innerHostView = null;

                    _scopedInnerRegionManager.Regions.Remove(ModuleRegions.QCInnerRegion);
                    _scopedInnerRegionManager.Regions.Remove(ModuleRegions.ResultsRegion);
                }
             
                InnerRegion = null;

            }


            //Reset active view region tuple
            _activeViewRegion = null;

            //Callback
            onDeactivateCallback();
        }

        public void ActivateView(string viewKey, object payload)
        {
            //Throws exception if navigatior is not initialized
            ThrowsExceptionIfNotInitialized();

            //If view key is not valid
            if (!_validViewKeys.Contains(viewKey))
                throw new ArgumentException("ActiveView QC. viewKey is not valid. Provide a valid key.");
          
            //Stops navigation if a view is already active.
            if (_activeViewRegion != null && _activeViewRegion.Item1 == viewKey)
                return;

            //Gets result and request region
            IRegion resultRegion = _scopedInnerRegionManager.Regions[ModuleRegions.ResultsRegion];
            IRegion requestedRegion = (viewKey == DestinationViewKeys.QcTestSetup || viewKey == DestinationViewKeys.QcTestResults) ? InnerRegion : OuterRegion;
            Object requestedView = null;
            
            //Need to navigate to outer region
            if (requestedRegion == OuterRegion)
            {
                //1. Deactive all active views in innerRegion
                DeactiveAllRegionViews(InnerRegion);

                //2. Deactive and destroy all views in a region
                DeactiveAndDestoryRegionViews(resultRegion);

                //3. Active view in outterRegion
                requestedView = OuterRegion.Views.First();
                OuterRegion.Activate(requestedView);
            }
            else if (requestedRegion == InnerRegion)
            {
                //1. Ensure result region gets destroyed when navigating away
                if (viewKey == DestinationViewKeys.QcTestSetup)
                {
                    DeactiveAndDestoryRegionViews(_scopedInnerRegionManager.Regions[ModuleRegions.ResultsRegion]);
                }
                else
                {
                    //Add a view to the region if missing and actives as well
                    if (resultRegion.ActiveViews.Count() == 0)
                        resultRegion.Add(_container.Resolve<QCResultsView>());
                }

                //2. Active view based on view key
                requestedView = InnerRegion.GetView(viewKey);
                InnerRegion.Activate(requestedView);

                //3. Lastly active innerHost view, as all other inner region views are its children
                if(!OuterRegion.ActiveViews.Contains(_innerHostView))
                    OuterRegion.Activate(_innerHostView);
            }

            //Guarantees that workspace region is active
            EnsureRootViewRegionExistence();
            _activeViewRegion = new Tuple<string, object, IRegion>(viewKey, requestedView, requestedRegion);
        }
        public void ActivateView(string viewKey)
        {
            ActivateView(viewKey, null);
        }

        public void Initialize()
        {
            if (_isInitialized) return;
            try
            {
                InitViews(true);
                _isInitialized = true;
            }
            catch (Exception ex)
            {
                _isInitialized = false;
                _logger.Log(ex.Message, Microsoft.Practices.Composite.Logging.Category.Exception, Microsoft.Practices.Composite.Logging.Priority.None);
                throw new ApplicationException(ex.InnerException.Message);
            }
        }

        #endregion

        #region Helpers

        static void DeactiveAndDestoryRegionViews(IRegion iRegion)
        {
            //If iRegion is null, simply return
            if (iRegion == null) return;

            //1. Deactive all views in a region
            DeactiveAllRegionViews(iRegion);

            //2. Remove all view in a region
            var viewsToRemove = iRegion.Views.ToList();
            viewsToRemove.ForEach((v) => iRegion.Remove(v));
        }
        static void DeactiveAllRegionViews(IRegion iRegion)
        {
            if(iRegion == null) return;

            iRegion.ActiveViews.ToList().ForEach((v)=>iRegion.Deactivate(v));
        }
        void EnsureRootViewRegionExistence()
        {
            IRegion workspaceRegion = _regionManager.Regions[RegionNames.WorkspaceRegion];

            //Check if the View was added to the region; if not add and activate
            if (!workspaceRegion.Views.Contains(RootViewHost))
            {
                workspaceRegion.Add(RootViewHost);

            }
            workspaceRegion.Activate(RootViewHost);
        }
        void ThrowsExceptionIfNotInitialized()
        {
            if (!_isInitialized)
                throw new ApplicationException("QCModuleNavigator is not initialized.");
        }

        /// <summary>
        /// Initialize only required screen for QC. As of last change, only test selection screen is active.
        /// </summary>
        /// <param name="withDI">
        /// True  : every UI screen will be initialized with Dependency Injection using Unity container
        /// False : every UI screen will be newed(Ui screen = new Ui())
        ///  </param>
        private void InitViews(bool withDI)
        {
            //Get RootViewHost
            RootViewHost = _container.Resolve<ModuleHost>();

            //Create outer scoped region
            _scopedOutterRegionManager = _regionManager.Regions[RegionNames.WorkspaceRegion].Add(RootViewHost, String.Empty, true);
            OuterRegion = _scopedOutterRegionManager.Regions[ModuleRegions.QCOuterRegion];

            //First screen for the UI
            OuterRegion.Add(_container.Resolve<QCSelectionView>(), DestinationViewKeys.QcTestSelection);
        }


        #endregion
    }
}
