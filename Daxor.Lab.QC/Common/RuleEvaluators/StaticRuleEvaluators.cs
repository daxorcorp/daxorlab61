﻿using System;

namespace Daxor.Lab.QC.Common.RuleEvaluators
{
    public static class StaticRuleEvaluators
    {
        public static bool FieldIsNotEmpty(string fieldValue)
        {
            return !(String.IsNullOrWhiteSpace(fieldValue) || String.IsNullOrEmpty(fieldValue));
        }
    }
}
