﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.QCModule.Common
{
    public static class ViewKeys
    {
        public const string QCTestSelection = "selection";
        public const string QCTestSetup = "setup";
        public const string QCTestResults = "results";
    }
}
