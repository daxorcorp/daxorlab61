﻿using System.Windows;
using System.Windows.Input;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Daxor.Lab.QC.Common
{
    /// <summary>
    /// Exposes Command attached property in order to forward MouseLeftButtonDown event on
    /// GridViewRow object, passes GridViewRow.Item to the Command's CanExecute() and Execute() methods
    /// Now our ViewModel can simply bind its ICommand command to this attached property on Telerik DataGrid control.
    /// </summary>
    public class AttachedCommandBinding
    {
        #region RowActivated Command

        public static ICommand GetRowActivatedCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(RowActivatedCommandProperty);
        }

        public static void SetRowActivatedCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(RowActivatedCommandProperty, value);
        }

        // Using a DependencyProperty as the backing store for RowActivatedCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RowActivatedCommandProperty =
            DependencyProperty.RegisterAttached("RowActivatedCommand", typeof(ICommand),
            typeof(AttachedCommandBinding), new FrameworkPropertyMetadata(CommandChanged));

        private static void CommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var frameworkElement = d as FrameworkElement;
            if (frameworkElement != null)
                frameworkElement.AddHandler(UIElement.PreviewMouseLeftButtonDownEvent, new RoutedEventHandler(MouseLeftButtonDownHandler), true);
        }

        private static void MouseLeftButtonDownHandler(object sender, RoutedEventArgs args)
        {
            var dependencyObject = sender as DependencyObject;
            if (dependencyObject == null) return;

            var frameworkElement = args.OriginalSource as FrameworkElement;
            if (frameworkElement == null) return;
            var gridViewRow = frameworkElement.ParentOfType<GridViewRow>();

            if (gridViewRow == null) return;
            args.Handled = false;

            var rowActivatedCommand = GetRowActivatedCommand(dependencyObject);
            if (rowActivatedCommand != null && rowActivatedCommand.CanExecute(gridViewRow.Item))
                rowActivatedCommand.Execute(gridViewRow.Item);
        }

        #endregion
    }
}
