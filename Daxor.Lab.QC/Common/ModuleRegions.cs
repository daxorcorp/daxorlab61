﻿namespace Daxor.Lab.QC.Common
{
    /// <summary>
    /// QC module internal regions.
    /// </summary>
    public static class ModuleRegions
    {
        public const string HostRegion = "HostRegion";
        public const string TestSetupRegion = "TestSetupRegion";
        public const string QCOuterRegion = "QCOuterRegion";
        public const string QCInnerRegion = "QCInnerRegion";
        public const string ResultsRegion = "TestResultsRegion";
    }
}
