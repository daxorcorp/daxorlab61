﻿namespace Daxor.Lab.QC.Common
{
    public static class RuleIDs
    {
        public static string InjectateLotFieldRequired = "QC_RequiredField_InjectateLot";
        public static string AnalystFieldRequired = "QC_RequiredField_Analyst";
        public static string StandardAKeyFieldRequired = "QC_RequiredField_StandardAKey";
        public static string StandardBKeyFieldRequired = "QC_RequiredField_StandardBKey";
    }
}
