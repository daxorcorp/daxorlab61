﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Daxor.Lab.QC.Common.Converters
{
    public class ValueToDashOrValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var resultDouble = value as double?;
            if (resultDouble != null)
            {
                if ((resultDouble > 0 && resultDouble <= 2.0) || resultDouble == -1) return String.Empty;

                return (resultDouble.Value <= 0 || Double.IsNaN(resultDouble.Value) || Double.IsInfinity(resultDouble.Value))
                        ? "-" : resultDouble.Value.ToString(CultureInfo.InvariantCulture);
            }

            var resultInt = value as int?;
            if (resultInt.HasValue)
                return (resultInt.Value <= 0) ? "-" : resultInt.Value.ToString(CultureInfo.InvariantCulture);

            var resultString = value as string;
            if (resultString != null)
                return (String.IsNullOrEmpty(resultString) || String.IsNullOrWhiteSpace(resultString)) ? "" : resultString;

            return String.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
