﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Daxor.Lab.QC.Common.Converters
{
    public class StringToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var valueAsString = value as string;

            return string.IsNullOrEmpty(valueAsString) ? Visibility.Collapsed : Visibility.Visible;
        }
        
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
