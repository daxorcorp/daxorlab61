﻿using System;
using System.Windows.Data;
using Daxor.Lab.QC.Core;

namespace Daxor.Lab.QC.Common.Converters
{
    class QCSampleNameToROIConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is QcSample))
                return "";
            if (((QcSample)value).DisplayName == "QC 1")
                return "584 to 740 keV";
            else
                return "308 to 420 keV";
        }

        

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
