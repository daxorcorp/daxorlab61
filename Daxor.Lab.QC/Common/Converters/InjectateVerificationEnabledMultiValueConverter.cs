﻿using System;
using System.Windows;
using System.Windows.Data;
using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Common.Converters
{
    public class InjectateVerificationEnabledMultiValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // If the test type isn't what we expected, have the field be hidden by default.
            if (!(values[0] is TestType))
                return System.Windows.DependencyProperty.UnsetValue;

            var testType = (TestType) values[0];
            var isInjectateVerificationEnabled = values[1] as Boolean?;

            // Unknown injectate verification state
            if (!isInjectateVerificationEnabled.HasValue)
                return System.Windows.DependencyProperty.UnsetValue;

            if ((testType == TestType.Standards || testType == TestType.Full) && isInjectateVerificationEnabled.Value)
                return Visibility.Visible;

            return Visibility.Collapsed;
        }


        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return new[] { value, value };
        }
    }
}
