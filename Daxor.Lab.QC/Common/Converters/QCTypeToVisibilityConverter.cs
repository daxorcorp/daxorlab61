﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Common.Converters
{
    public class QCTypeToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is TestType)) return Visibility.Visible;

            var testType = (TestType)value;
            if (testType == TestType.Linearity || testType == TestType.Contamination)
                return Visibility.Hidden;

            return Visibility.Visible;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
 