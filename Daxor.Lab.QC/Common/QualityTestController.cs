﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Factories;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.QCModule.ExeContext;
using Daxor.Lab.SettingsManager;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.QCModule.Common
{
    /// <summary>
    /// QualityTest controller
    /// </summary>
    public class QualityTestController : ObservableObject, IQualityTestController
    {
        #region Fields

        readonly IUnityContainer _container;
        readonly IEventAggregator _eventAggregator;
        readonly IAppModuleNavigator _navigator;
        readonly ISchemaProvider _sampleSchemaProvider;
        readonly IQualityTestDataService _dataService;
        readonly IMessageBoxDispatcher _msgBox;
        readonly ILoggerFacade _logger;
        readonly ITestExecutionController _exeController;
        readonly IEnumerable<IConfigurator> _testConfigurators;
       
        readonly SettingObserver<ISettingsManager> _settingObserver;
        readonly DelegateCommand<Object> _startSelectedTestExecutionCommand;
        readonly DelegateCommand<Object> _abortTestExecutionCommand;
        readonly DelegateCommand<Guid> _selectTestCommand;
        readonly DelegateCommand<QCTest> _saveTestCommand;
        readonly DelegateCommand<TestType> _beginNewTestCommand;
        
        String _sTestFriendlyName;
        BackgroundTaskManager<QCTest> _testSelector;
        Int32 _testItemsCounts;

        QCTest _rTest = null;
        QCTest _sTest = null;

        #endregion

        #region Properties

        public QCTest RunningTest
        {
            get { return _rTest; }
            set { base.SetValue(ref _rTest, "RunningTest", value); }
        }
        public QCTest SelectedTest
        {
            get { return _sTest; }
            set {
                if (base.SetValue(ref _sTest, "SelectedTest", value))
                {
                    SelectedTestFriendlyName = FormatSelectedTestFriendly(value);
                }

                _startSelectedTestExecutionCommand.RaiseCanExecuteChanged();
                _abortTestExecutionCommand.RaiseCanExecuteChanged();
            }
        }
        public IQualityTestDataService DataService
        {
            get { return _dataService; }
        }
       
        public Int32 TestItemsCount
        {
            get { return _testItemsCounts; }
            private set { base.SetValue(ref _testItemsCounts, "TestItemsCount", value); }
        }
        private ITestExecutionContext RunningContext { get; set; }

        #endregion

        #region Ctor

        public QualityTestController([Dependency(AppModuleKeys.QC)] IAppModuleNavigator localNavigator, IEventAggregator eventAggregator, IUnityContainer container,
                                     ISchemaProvider sampleSchemaProvider, IQualityTestDataService qcDataService, IEnumerable<IConfigurator> configurators,
                                     ITestExecutionController exeController, IMessageBoxDispatcher msgBox, ILoggerFacade logger)
        {
            _navigator = localNavigator;
            _eventAggregator = eventAggregator;
            _sampleSchemaProvider = sampleSchemaProvider;
            _dataService = qcDataService;
            _testConfigurators = configurators;
            _msgBox = msgBox;
            _exeController = exeController;
            _logger = logger;
            _container = container;
            _settingObserver = new SettingObserver<ISettingsManager>(SystemSettingsManager.Manager);

            _startSelectedTestExecutionCommand = new DelegateCommand<Object>(StartSelectedTestExecutionCommandExecute, StartSelectedTestExecutionCommandCanExecute);
            _abortTestExecutionCommand = new DelegateCommand<Object>(AbortTestExecutionCommandExecute, AbortTestExecutionCommandCanExecute);
            _selectTestCommand = new DelegateCommand<Guid>(SelectTestCommandExecute, SelectTestCommandCanExecute);
            _saveTestCommand = new DelegateCommand<QCTest>(SaveTestCommandExecute, SaveTestCommandCanExecute);
            _beginNewTestCommand = new DelegateCommand<TestType>(BeginNewTestCommandExecute, BeginNewTestCommandCanExecute);

            WireSettingsObservers();
        }

        #endregion

        #region Commands

        public ICommand SaveTestCommand
        {

            get { return _saveTestCommand; }
        }
        public ICommand SelectTestCommand
        {
            get { return _selectTestCommand; }
        }
        public ICommand StartSelectedTestExecutionCommand
        {
            get { return _startSelectedTestExecutionCommand; }
        }
        public ICommand BeginNewTestCommand
        {
            get { return _beginNewTestCommand; }
        }
        public ICommand AbortTestExecutionCommand
        {
            get { return _abortTestExecutionCommand; }
        }

        void SaveTestCommandExecute(Daxor.Lab.QC.Core.QCTest test)
        {
            try
            {
               _dataService.SaveTest(test);
               TestItemsCount = _dataService.GetTestItems().Count();
            }
            catch
            {
                _msgBox.ShowMessageBox(Infrastructure.DomainModels.MessageBoxCategory.Error, "Failed to save this Quality Control Test.", "Error Saving A Test", Infrastructure.DomainModels.MessageBoxChoiceSet.OK);
            }
        }
        bool SaveTestCommandCanExecute(Daxor.Lab.QC.Core.QCTest test)
        {
            return test != null;
        }

        void SelectTestCommandExecute(Guid testID)
        {
            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true) { Message = "Loading Quality Control Test ..." });
            _testSelector = new BackgroundTaskManager<QCTest>(
                () =>
                {
                    Thread.Sleep(1000);
                    QCTest sTest = null;
                    try
                    {
                        if (_exeController.IsExecuting() && RunningContext != null && RunningTest != null && RunningTest.InternalID == testID)
                            sTest = RunningTest;
                        else
                            sTest = _dataService.LoadTest(testID);
                    }
                    catch (Exception ex)
                    {
                        sTest = null;
                        _logger.Log(String.Format("Failed SelectQualityControlTest with ID'{0}'. Exception: {1}", testID != null ? testID.ToString() : "no_test_id", ex.Message), Category.Exception, Priority.High);
                    }

                    return sTest;

                }, (sTest) =>
                {
                    _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));

                    if (sTest == null)
                    {
                        _msgBox.ShowMessageBox(Infrastructure.DomainModels.MessageBoxCategory.Error, "Failed to load a test", "Error Retrieving Test", Infrastructure.DomainModels.MessageBoxChoiceSet.OK);
                    }
                   
                    else
                    {
                        //Dealing with aborted tests
                        Daxor.Lab.Infrastructure.DomainModels.MessageBoxReturnResult result = Daxor.Lab.Infrastructure.DomainModels.MessageBoxReturnResult.None;
                        if (sTest.Status == Domain.Common.TestStatus.Aborted)
                        {
                            result = _msgBox.ShowMessageBox(Infrastructure.DomainModels.MessageBoxCategory.Question, "Would you like to delete this aborted test.", "Aborted QC Test Options", Infrastructure.DomainModels.MessageBoxChoiceSet.YesNo);
                        }
                        else if (!sTest.IsPassed && sTest.Status == Domain.Common.TestStatus.Completed)
                        {
                            result = _msgBox.ShowMessageBox(Infrastructure.DomainModels.MessageBoxCategory.Question, "Would you like to delete this failed test.", "Failed QC Test Options", Infrastructure.DomainModels.MessageBoxChoiceSet.YesNo);
                        }

                        //Deal with the user input
                        if (result == Daxor.Lab.Infrastructure.DomainModels.MessageBoxReturnResult.Yes)
                        {
                            _dataService.DeleteTest(sTest.InternalID);
                            FireTestItemsNeedRefresh();
                            return;
                        }

                        SelectedTest = sTest;
                        _eventAggregator.GetEvent<AppModuleModelChanged>().Publish(new ModelInfo(AppModuleKeys.QC, SelectedTest));
                        this.Navigate(ViewKeys.QCTestResults);
                    }
                });

            _testSelector.RunBackgroundTask();
        }
        bool SelectTestCommandCanExecute(Guid testID)
        {
            return testID != null;
        }

        void StartSelectedTestExecutionCommandExecute(Object nullObject)
        {
            try
            {
                //Get execution context based on selected test
                SelectedTest.ConfigureForExecution(_testConfigurators);
                RunningContext = ExecutionContextsFactory.CreateExeContext(SelectedTest, SaveTestCommand.Execute);
                this.WireExecutionControllerEvents();

                //Save intial test
                this.SaveTestCommandExecute(SelectedTest);
                RunningTest = SelectedTest;
                _exeController.Start(RunningContext);
            }
            catch (Exception ex)
            {
                this.UnWireExecutionControllerEvents();
                RunningTest = null;
                RunningContext = null;
               
                _logger.Log(ex.Message, Category.Exception, Priority.High);
                _msgBox.ShowMessageBox(Infrastructure.DomainModels.MessageBoxCategory.Error, "Could not start this test", "QC Test Cannot Be Started", Infrastructure.DomainModels.MessageBoxChoiceSet.OK); 
            }
            finally
            {
                this.ReEvaluateExecutionCommands();
            }
        }
        bool StartSelectedTestExecutionCommandCanExecute(Object nullObject)
        {
            return SelectedTest != null && SelectedTest.Status == Domain.Common.TestStatus.Pending 
                   && !_exeController.IsExecuting() && RunningContext == null;
        }

        void AbortTestExecutionCommandExecute(Object test)
        {
            try
            {
                Task abortingTest =
                    Task.Factory.StartNew<Boolean>((objectState) =>
                    {
                        ITestExecutionController exeController = (ITestExecutionController)objectState;

                        _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Aborting QC test..."));
                        exeController.Abort();
                        Thread.Sleep(2000);

                        return _exeController.IsExecuting();

                    }, _exeController).ContinueWith(output =>
                    {
                        _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false, "Aborting QC test..."));

                        //Implies we are still running
                        if (output.Result)
                            _msgBox.ShowMessageBox(Infrastructure.DomainModels.MessageBoxCategory.Warning, "Abort taking longer than expected", "Aborting QC Test", Infrastructure.DomainModels.MessageBoxChoiceSet.OK);

                    }, TaskScheduler.FromCurrentSynchronizationContext());
            }
            catch (Exception ex)
            {
                _logger.Log("Aborting QCTest. Exception: " + ex.Message, Category.Exception, Priority.High);
            }
            finally
            {
                _abortTestExecutionCommand.RaiseCanExecuteChanged();
                _startSelectedTestExecutionCommand.RaiseCanExecuteChanged();
            }
        }
        bool AbortTestExecutionCommandCanExecute(Object test)
        {
            bool canExecute;
            try
            {
                canExecute = SelectedTest != null && SelectedTest.Status == Domain.Common.TestStatus.Running && _exeController.IsExecuting();
            }
            catch
            {
                canExecute = false;
            }
            return canExecute;
        }

        void BeginNewTestCommandExecute(TestType qcType)
        {
            try
            {
                var test = QCTestFactory.CreateTest(qcType, _sampleSchemaProvider);
                test.InjectateVerificationEnabled = SystemSettingsManager.Manager.GetSetting<Boolean>(SettingKeyConstants.Setting_InjectateVerification_Enabled);
                test.SystemID = SystemSettingsManager.Manager.GetSetting<String>(SettingKeyConstants.Setting_Unique_System_ID);
                test.CalibrationCurve = _dataService.GetCurrentCalibrationCurve();

                //Builds samples and assign sources
                test.BuildSamples();
                test.AssignSourcesToSamples(_dataService.GetSources());

                SelectedTest = test;

                //Navigate to test setup view
                Navigate(ViewKeys.QCTestSetup);
            }
            catch (ArgumentException ex)
            {
                _logger.Log(ex.Message, Category.Exception, Priority.High);
            }
            catch (Exception ex)
            {
                _logger.Log(ex.Message, Category.Exception, Priority.High);
            }
        }
        bool BeginNewTestCommandCanExecute(TestType qcType)
        {
            return true;
        }
       
        #endregion

        #region Methods

        public void Navigate(string key)
        {
            _navigator.ActivateView(key);
        }
        public String SelectedTestFriendlyName
        {
            get { return _sTestFriendlyName;}
            private set { base.SetValue(ref _sTestFriendlyName, "SelectedTestFriendlyName", value); }
        }

        #endregion

        #region TestExecutionControllerEvent

        void OnErrorOccurred(object sender, TestExecutionEventArgs e)
        {
            this.UnWireExecutionControllerEvents();
            if (RunningTest != null && RunningTest.InternalID == e.ExecutingTestID)
            {
                RunningTest.Status = Domain.Common.TestStatus.Aborted;
               // RunningTest.IsPassed = 
                SaveTestCommandExecute(RunningTest);
            }
            if (_navigator.IsActive)
            {
                this.ReEvaluateExecutionCommands();
                _msgBox.ShowMessageBox(Infrastructure.DomainModels.MessageBoxCategory.Error, "There was an error during test execution. Your test is aborted.\n\nPlease contact Daxor Person", "Test Execution Error", Infrastructure.DomainModels.MessageBoxChoiceSet.OK);
            }

            RunningContext = null;
            FireTestItemsNeedRefresh();
        }
        void OnTestStarted(object sender, TestExecutionEventArgs e)
        {
            this.ReEvaluateExecutionCommands();
            if (RunningTest != null && RunningTest.InternalID == e.ExecutingTestID)
            {
                RunningTest.Status = Domain.Common.TestStatus.Running;
                SaveTestCommandExecute(RunningTest);
            }
            FireTestItemsNeedRefresh();
        }
        void OnTestCompleted(object sender, TestExecutionEventArgs e)
        {
            this.UnWireExecutionControllerEvents();
            if (RunningTest != null && RunningTest.InternalID == e.ExecutingTestID)
            {
                RunningTest.Status = Domain.Common.TestStatus.Completed;
                SaveTestCommandExecute(RunningTest);

               //Updating the calibration curve v.0.0.1 --> need to improve on that.
               if((RunningTest is QCCalibrationTest || RunningTest is QCFullTest) && RunningTest.IsPassed == true)
                   _container.RegisterInstance<CalibrationFunction>(new CalibrationFunction(RunningTest.CalibrationCurve.Slope, RunningTest.CalibrationCurve.Offset));
            }

            this.ReEvaluateExecutionCommands();
            RunningContext = null;
            FireTestItemsNeedRefresh();
        }
        void OnTestAborted(object sender, TestExecutionEventArgs e)
        {
            this.UnWireExecutionControllerEvents();
            this.ReEvaluateExecutionCommands();
            if (RunningTest != null && RunningTest.InternalID == e.ExecutingTestID)
            {
                RunningTest.Status = Domain.Common.TestStatus.Aborted;
                SaveTestCommandExecute(RunningTest);
            }

            RunningContext = null;
            FireTestItemsNeedRefresh();
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Wires to settings manager and listening for setting updates, modifying currently visible test
        /// </summary>
        void WireSettingsObservers()
        {
            _settingObserver.RegisterHandler(SettingKeyConstants.Setting_InjectateVerification_Enabled, (s) =>
            {
                if (SelectedTest != null && SelectedTest.Status == Domain.Common.TestStatus.Pending)
                {
                    SelectedTest.InjectateVerificationEnabled = s.GetSetting<Boolean>(SettingKeyConstants.Setting_InjectateVerification_Enabled);
                }
            });
        }
        void ReEvaluateExecutionCommands()
        {
            _abortTestExecutionCommand.RaiseCanExecuteChanged();
            _startSelectedTestExecutionCommand.RaiseCanExecuteChanged();
        }
        void UnWireExecutionControllerEvents()
        {
            if (_exeController == null)
                return;

            _exeController.TestAborted -= OnTestAborted;
            _exeController.TestCompleted -= OnTestCompleted;
            _exeController.TestStarted -= OnTestStarted;
            _exeController.ErrorOccurred -= OnErrorOccurred;
        }
        void WireExecutionControllerEvents()
        {
            if (_exeController == null)
                return;

            _exeController.TestAborted += OnTestAborted;
            _exeController.TestCompleted += OnTestCompleted;
            _exeController.TestStarted += OnTestStarted;
            _exeController.ErrorOccurred+= OnErrorOccurred;
        }
        string FormatSelectedTestFriendly(QCTest test)
        {
            string friendlyName = "Not Determined";
            if (test != null)
            {
                switch (test.QCType)
                {
                    case TestType.Full:
                        friendlyName = "Full Daily";
                        break;
                    case TestType.Contamination:
                        friendlyName = "Contamination";
                        break;
                    case TestType.Linearity:
                        friendlyName = "Linearity";
                        break;
                    case TestType.Standards:
                        friendlyName = "Standards";
                        break;
                    default:
                        friendlyName = "QC test";
                        break;
                }
            }
            return friendlyName;
        }

        #endregion

        #region Events

        public event EventHandler TestItemsNeedRefresh;
        protected virtual void FireTestItemsNeedRefresh()
        {
            var handler = TestItemsNeedRefresh;
            if (handler != null)
                handler(this, null);
        }

        #endregion
    }
}
