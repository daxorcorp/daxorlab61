﻿using Daxor.Lab.QC.Core;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.QC.Common.Events
{
    /// <summary>
    /// The sample selected event
    /// </summary>
    public class SampleSelected : CompositePresentationEvent<QcSample> {}
}
