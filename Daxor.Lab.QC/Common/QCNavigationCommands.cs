﻿using Microsoft.Practices.Composite.Presentation.Commands;

namespace Daxor.Lab.QC.Common
{
    /// <summary>
    /// QC Navigation Commands
    /// </summary>
    public static class QCNavigationCommands
    {
        static CompositeCommand _navigateTestSelection = new CompositeCommand();
        static CompositeCommand _navigateTestSetup = new CompositeCommand();
        static CompositeCommand _navigateTestResults = new CompositeCommand();


        public static CompositeCommand NavigateTestSelection
        {
            get { return _navigateTestSelection; }
        }
        public static CompositeCommand NavigateTestSetup
        {
            get { return _navigateTestSetup; }
        }
        public static CompositeCommand NavigateTestResults
        {
            get { return _navigateTestResults; }
        }
    }
}
