﻿using System;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.QC.Common
{
    public class RecentQCTestSample
    {
        #region Properties
        public bool Pass { get; set; }

        public DateTime TestDate { get; set; }

        public int Position { get; set; }

        #endregion

        #region Ctor
        protected RecentQCTestSample() { }

        protected RecentQCTestSample(GetRecentQCSamplesResult record)
        {
            Pass = record.PASS == 'T';
            Position = record.SAMPLE_POSITION;
            TestDate = record.TEST_DATE;
        }

        #endregion

        #region Public APIs

        public static RecentQCTestSample[] GetRecentQCTestSamples(ISettingsManager settingsManager)
        {
            RecentQCTestSample[] recentSamples = new RecentQCTestSample[25];

            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(settingsManager.GetSetting<string>(SettingKeys.SystemLinqConnectionString));

            var result = context.GetRecentQCSamples();

            foreach (GetRecentQCSamplesResult record in result)
            {
                recentSamples[record.SAMPLE_POSITION - 1] = new RecentQCTestSample(record);
            }

            return recentSamples;
        }

        #endregion
    }
}
