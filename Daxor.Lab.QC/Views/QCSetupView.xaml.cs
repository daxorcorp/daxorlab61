﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.TouchClick;
using Daxor.Lab.QC.ViewModels;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.QC.Views
{
    /// <summary>
    /// Interaction logic for TestSetupView.xaml
    /// </summary>
    public partial class QCSetupView : UserControl, IActiveAware
    {
        public QCSetupView()
        {
            InitializeComponent();
        }

        public QCSetupView(QCSetupViewModel vm) : this()
        {
            DataContext = vm;
        }

        #region IActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                IActiveAware vmAware = DataContext as IActiveAware;
                if (vmAware != null)
                {
                    vmAware.IsActive = value;

                    //Attempts to set focus on analyst field
                    QCSetupViewModel vm = DataContext as QCSetupViewModel;
                    if (_isActive && vm != null && !uxAnalystField.IsFocused && vm.ShouldAnalystReceiveFocus)
                    {
                        TouchClick.SetIsSoundEnabled(uxAnalystField, false);
                        //Dealing with logical and physical focus issues
                        uxAnalystField.Focus();
                        Keyboard.Focus(uxAnalystField);
                        TouchClick.SetIsSoundEnabled(uxAnalystField, true);
                    }

                }
                //If this view is not active
                if (_isActive == false){
                    Focus();
                    VirtualKeyboardManager.CloseActiveKeyboard();
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
