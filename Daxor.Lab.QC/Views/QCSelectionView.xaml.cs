﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Daxor.Lab.QC.ViewModels;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.QC.Views
{
    /// <summary>
    /// Interaction logic for TestSelectionView.xaml
    /// </summary>
    public partial class QCSelectionView : IActiveAware
    {
        #region Fields

        private ScrollViewer _scrollViewer;
        private bool _isActive;

        #endregion

        #region Ctors

        public QCSelectionView()
        {
            InitializeComponent();
            Loaded += QCTestSelectionView_Loaded;

            //Forces cursor to be represented as arrow when sorting the data
            uxQualityTestsGrid.Sorting += (s, e) => Mouse.SetCursor(Cursors.Arrow);
        }

        public QCSelectionView(QCSelectionViewModel vm): this()
        {
            DataContext = vm;
        }

        #endregion

        #region Private helpers

        void QCTestSelectionView_Loaded(object sender, RoutedEventArgs e)
        {
            _scrollViewer = (ScrollViewer)uxQualityTestsGrid.Template.FindName("PART_ItemsScrollViewer", uxQualityTestsGrid);
            if (_scrollViewer == null) throw new NullReferenceException("ItemsScrollViewer is not present in VisualTree");

            CalculatePageInfo();
            _scrollViewer.ScrollChanged += (s, e2) => CalculatePageInfo();
        }

        void CalculatePageInfo()
        {
            if (_scrollViewer == null) return;

            uxTotalItems.Text = uxQualityTestsGrid.Items.Count.ToString(CultureInfo.InvariantCulture);
            if (uxQualityTestsGrid.Items.Count == 0)
            {
                uxEndingItemIndex.Text = "0";
                uxStartingItemIndex.Text = "0";
                return;
            }

            var itemHeight = _scrollViewer.ExtentHeight / uxQualityTestsGrid.Items.Count;
            var pageHeight = _scrollViewer.ViewportHeight;

            uxStartingItemIndex.Text = ((int) Math.Round(_scrollViewer.VerticalOffset / itemHeight) + 1).ToString(CultureInfo.InvariantCulture);
            var endingIndex = ((int) Math.Round((_scrollViewer.VerticalOffset + pageHeight) / itemHeight));

            // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
            if (endingIndex > uxQualityTestsGrid.Items.Count)
                uxEndingItemIndex.Text = uxQualityTestsGrid.Items.Count.ToString(CultureInfo.InvariantCulture);
            else
                uxEndingItemIndex.Text = ((int) Math.Round((_scrollViewer.VerticalOffset + pageHeight) / itemHeight)).ToString(CultureInfo.InvariantCulture);

        }

        #endregion

        #region Pagination button click handlers

        void uiPageUpButton_Click(object sender, RoutedEventArgs e)
        {
            _scrollViewer.PageUp();
        }

        void uiPageDownButton_Click(object sender, RoutedEventArgs e)
        {
            _scrollViewer.PageDown();
        }

        #endregion

        #region IActiveAware

        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                var vmAware = DataContext as IActiveAware;
                if (vmAware != null)
                    vmAware.IsActive = value;
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
