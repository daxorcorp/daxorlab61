﻿using System.Windows.Controls;

namespace Daxor.Lab.QC.Views
{
    /// <summary>
    /// Interaction logic for InnerHost.xaml
    /// </summary>
    public partial class InnerHost : UserControl
    {
        public InnerHost()
        {
            InitializeComponent();
        }
    }
}
