﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using Daxor.Lab.QC.ViewModels;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.QC.Views
{
    /// <summary>
    /// Interaction logic for TestSampleValuesView.xaml
    /// </summary>
    public partial class QCSampleValuesView : UserControl, IActiveAware
    {
        private readonly DispatcherTimer _dTimer = new DispatcherTimer();

        public QCSampleValuesView()
        {
            InitializeComponent();

            _dTimer = new DispatcherTimer(new TimeSpan(0, 0, 0, 1, 10),
                DispatcherPriority.DataBind, new EventHandler((s, e) => { _dTimer.Stop(); UpdateDataContext(); }),
                Application.Current.Dispatcher);
            DataContext = ViewModel;
        }

        [Dependency]
        public QCSampleValuesViewModel ViewModel
        {
            get;
            set;
        }


        #region IActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                IActiveAware vmAware = ViewModel as IActiveAware;
                if (vmAware != null)
                {
                    vmAware.IsActive = value;
                }
                if (_isActive)
                {
                    busyIndicator.IsBusy = true;
                    _dTimer.Start();
                }
                else
                {
                    busyIndicator.IsBusy = false;
                    DataContext = null;
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion

        private void UpdateDataContext()
        {
            if (IsActive)
            {
                busyIndicator.IsBusy = false;
                DataContext = ViewModel;
            }
        }
    }
}
