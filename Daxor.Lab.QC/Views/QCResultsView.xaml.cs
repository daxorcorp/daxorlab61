﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.QC.SubViews;
using Daxor.Lab.QC.ViewModels;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.QC.Views
{
    /// <summary>
    /// Interaction logic for TestResultsView.xaml
    /// </summary>
    public partial class QCResultsView : UserControl, IActiveAware
    {
        GammaDetectorChart _gChart = null;

        public QCResultsView()
        {
            InitializeComponent();
            Loaded += new RoutedEventHandler(TestResultsView_Loaded);
            Unloaded += new RoutedEventHandler(TestResultsView_Unloaded);
        }
        public QCResultsView(QCResultsViewModel vm)
            : this()
        {
            DataContext = vm;
            AddChart();
        }

        void TestResultsView_Unloaded(object sender, RoutedEventArgs e)
        {
            analysisTabControl.SelectionChanged -= uiSideTabs_SelectionChanged;
            uiTxtNotes.LostFocus -= uiTxtNotes_LostFocus;
        }

        void TestResultsView_Loaded(object sender, RoutedEventArgs e)
        {
            analysisTabControl.SelectionChanged += uiSideTabs_SelectionChanged;
            uiTxtNotes.LostFocus += uiTxtNotes_LostFocus;
        }

        void uiTxtNotes_LostFocus(object sender, RoutedEventArgs e)
        {
            VirtualKeyboardManager.CloseActiveKeyboard();
        }
     

        #region IActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                IActiveAware vmAware = DataContext as IActiveAware;
                if (vmAware != null)
                {
                    vmAware.IsActive = value;
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion

        private void uiSideTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TabControl tabControl = sender as TabControl;
            if (tabControl != null && _gChart == null && tabControl.SelectedIndex == 0)
            {
                //Add chart
                AddChart();
            }
            VirtualKeyboardManager.CloseActiveKeyboard();
        }

        private void AddChart()
        {
            _gChart = new GammaDetectorChart();
            _gChart.Name = "gammaChart";
            Binding b = new Binding();
            b.Source = DataContext;
            BindingOperations.SetBinding(_gChart, UserControl.DataContextProperty, b);
            _gChart.Margin = new Thickness(-10, 0, -5, 0);
            _gChart.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            Grid.SetRow(_gChart, 0);
            Grid.SetColumn(_gChart, 0);
            TextBlock.SetFontWeight(_gChart, FontWeights.Normal);

            spectrumGrid.Children.Add(_gChart);
        }
    }
}
