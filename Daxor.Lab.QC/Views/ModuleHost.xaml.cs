﻿using System;
using System.Windows.Controls;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.QC.Views
{
    /// <summary>
    /// Interaction logic for ModuleHost.xaml
    /// </summary>
    public partial class ModuleHost : UserControl, IActiveAware
    {
        public ModuleHost()
        {
            InitializeComponent();
        }

        #region IActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                IActiveAware vmAware = DataContext as IActiveAware;
                if (vmAware != null)
                {
                    vmAware.IsActive = value;
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
