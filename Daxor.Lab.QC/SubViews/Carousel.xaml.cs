﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Daxor.Lab.QC.SubViews
{
    /// <summary>
    /// Interaction logic for Carousel.xaml
    /// </summary>
    public partial class Carousel : UserControl
    {
        public Carousel()
        {
            InitializeComponent();
        }

        #region DependencyProperties

        #region TestProtocol


        public int TestProtocol
        {
            get { return (int)GetValue(TestProtocolProperty); }
            set { SetValue(TestProtocolProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TestProtocol.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TestProtocolProperty =
            DependencyProperty.Register("TestProtocol", typeof(int), typeof(Carousel), new UIPropertyMetadata(3, OnTestProtocolChanged, CoerceTestProtocol));

        public string TopText
        {
            get { return (string)GetValue(TopTextProperty); }
            set { SetValue(TopTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TopText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TopTextProperty =
            DependencyProperty.Register("TopText", typeof(string), typeof(Carousel), new UIPropertyMetadata(String.Empty));


        public string MiddleText
        {
            get { return (string)GetValue(MiddleTextProperty); }
            set { SetValue(MiddleTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MiddleText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MiddleTextProperty =
            DependencyProperty.Register("MiddleText", typeof(string), typeof(Carousel), new UIPropertyMetadata(String.Empty));


        public string BottomText
        {
            get { return (string)GetValue(BottomTextProperty); }
            set { SetValue(BottomTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BottomText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BottomTextProperty =
            DependencyProperty.Register("BottomText", typeof(string), typeof(Carousel), new UIPropertyMetadata(String.Empty));


        #endregion

        #endregion

        #region DP Handlers
        static void OnTestProtocolChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            
            Carousel THIS = sender as Carousel;

            THIS.uiCarouselBackground.Visibility = Visibility.Collapsed;
            THIS.uiCarouselBackgroundWithoutStandards.Visibility = Visibility.Collapsed;
            THIS.uiCarouselBackgroundReturnStandards.Visibility = Visibility.Collapsed;
         
            //Set appropriate UI to visible
            switch ((Int32)args.NewValue)
            {
                case 3:
                    THIS.uiCarouselBackground.Visibility = Visibility.Visible;
                    break;
                case 4:
                    THIS.uiCarouselBackgroundWithoutStandards.Visibility = Visibility.Visible;
                    break;
                case 6:
                    THIS.uiCarouselBackgroundReturnStandards.Visibility = Visibility.Visible;
                    break;
            }
        }
        static object CoerceTestProtocol(DependencyObject sender, object value)
        {
            int testProtocol = (Int32)value;
            if (testProtocol < 3) testProtocol = 3;
            if (testProtocol > 6) testProtocol = 6;

            return testProtocol;
        }
        #endregion

    }
}
