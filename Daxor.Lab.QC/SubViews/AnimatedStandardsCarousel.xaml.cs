﻿using System.Windows.Controls;

namespace Daxor.Lab.QC.SubViews
{
    /// <summary>
    /// Interaction logic for AnimatedStandardsCarousel.xaml
    /// </summary>
    public partial class AnimatedStandardsCarousel : UserControl
    {
        public AnimatedStandardsCarousel()
        {
            InitializeComponent();
        }
    }
}
