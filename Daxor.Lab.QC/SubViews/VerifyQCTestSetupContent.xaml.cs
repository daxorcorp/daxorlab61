﻿using System.Windows.Controls;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Interfaces;

namespace Daxor.Lab.QC.SubViews
{
    /// <summary>
    /// Interaction logic for VerifyQCTestSetupContent.xaml
    /// </summary>
    public partial class VerifyQCTestSetupContent : UserControl
    {
        public VerifyQCTestSetupContent()
        {
            InitializeComponent();
        }

        public VerifyQCTestSetupContent(QcTest currentTest)
            : this()
        {
            switch (currentTest.QCType)
            {
                case TestType.Contamination:
                case TestType.Linearity:
                    uiCarousel.TestProtocol = 4;
                    uiInjectateLotNumber.Visibility = System.Windows.Visibility.Hidden;
                    InjectateLotLabel.Visibility = System.Windows.Visibility.Hidden;
                    break;
                default:
                    uiCarousel.TestProtocol = 3;
                    uiInjectateLotNumber.Visibility = System.Windows.Visibility.Visible;
                    InjectateLotLabel.Visibility = System.Windows.Visibility.Visible;
                    break;
            }

            if ((currentTest.IsPassed)||(currentTest.Status == TestStatus.Aborted))
            {
                uiCarousel.TestProtocol = 6;
                uiVerifyLoc.Text = "Move standards to normal Standard A and Standard B location.";
            }
            
            uiAnalyst.Text = currentTest.Analyst;
            uiInjectateLotNumber.Text = currentTest.InjectateLot;
            uiNumberSamples.Text = QcTestNameConverter.Convert(currentTest.QCType);
        }
    }
}
