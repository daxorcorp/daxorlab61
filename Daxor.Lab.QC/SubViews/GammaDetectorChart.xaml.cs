﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Daxor.Lab.QC.Core;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.Charts;
using Microsoft.Research.DynamicDataDisplay.DataSources;
using Microsoft.Research.DynamicDataDisplay.Navigation;
using Microsoft.Research.DynamicDataDisplay.ViewportRestrictions;

namespace Daxor.Lab.QC.SubViews
{
    /// <summary>
    /// Interaction logic for GammaDetectorChart.xaml
    /// </summary>
    public partial class GammaDetectorChart
    {
        CompositeDataSource _graphSource;
        EnumerableDataSource<UInt32> _yDataSource;
        uint[] _ySpectrumArray;
        ViewportAxesRangeRestriction _axisRestrictions;
        double _initialYMaxValue = 20;

        public GammaDetectorChart()
        {
            InitializeComponent();
            lineGraph.DataContextChanged += lineGraph_DataContextChanged;

            InternalD3ChartSetup();
        }

        void InternalD3ChartSetup()
        {
            if (plotter == null)
                return;
          
            HorizontalAxis hAxis = plotter.HorizontalAxis as HorizontalAxis;
            if (hAxis != null)
                hAxis.FontSize = 10.366;

            VerticalAxis vAxis = plotter.VerticalAxis as VerticalAxis;
            if (vAxis != null)
                vAxis.FontSize = 10.366;

            //Setting min/max for YAxis
            plotter.Viewport.AutoFitToView = true;
            _axisRestrictions = new ViewportAxesRangeRestriction(new DisplayRange(-30, 1050), new DisplayRange(-3, _initialYMaxValue));
            plotter.Viewport.Restrictions.Add(_axisRestrictions);

            //Removes context menu and legend from the plotter area
            plotter.Children.RemoveAll<IPlotterElement, DefaultContextMenu>();
            plotter.Children.RemoveAll<IPlotterElement, Legend>();
        }
        void lineGraph_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var newItem = e.NewValue;
            var oldItem = e.OldValue;
            QcSample sample = newItem as QcSample;
            QcSample oSample = oldItem as QcSample;

            if(oSample != null && sample != oSample)
                    oSample.PropertyChanged -= SamplePropertyChanged;

            if (sample != null && newItem != oldItem)
            {
                SetMapping(sample.Spectrum.SpectrumArray);
                lineGraph.DataSource = _graphSource;
                RefreshYSourceFrom(sample.Spectrum.SpectrumArray);
                sample.PropertyChanged += SamplePropertyChanged;
            }
        }
        void SamplePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SpectrumArray")
            {
                QcSample sample = sender as QcSample;
                RefreshYSourceFrom(sample != null ? sample.Spectrum.SpectrumArray : null);
            }
        }

        void SetMapping(UInt32[] sArray)
        {
            int size = sArray == null ? 1024 : sArray.Length;
            uint[] spectrumX = new uint[size];

            for (int i = 0; i < size; i++)
                spectrumX[i] = (uint)(i + 1);

            _ySpectrumArray = new UInt32[size];

            EnumerableDataSource<uint> xDataSource = new EnumerableDataSource<uint>(spectrumX);
            xDataSource.SetXMapping(x => x);

            _yDataSource = new EnumerableDataSource<uint>(_ySpectrumArray);
            _yDataSource.SetYMapping(y => y);

            _graphSource = new CompositeDataSource(xDataSource, _yDataSource);
            RefreshYSourceFrom(sArray);
        }
        void RefreshYSourceFrom(UInt32[] newSource)
        {
            try
            {
                uint[] safeArraySource;
                var size = 1024;
                if (newSource == null)
                    safeArraySource = new UInt32[1024];
                else
                {
                    size = newSource.Length;
                    safeArraySource = newSource;
                }

                if (_ySpectrumArray != null)
                    Array.Copy(safeArraySource, _ySpectrumArray, size);

                //Dispatch to ui
                if (_yDataSource != null)
                    Dispatcher.BeginInvoke((Action)delegate
                    {
                        _axisRestrictions.UpdateYMax(_ySpectrumArray.Max() + _initialYMaxValue);
                        _yDataSource.RaiseDataChanged(); 
                    
                    });
            }
            catch { }
        }


        #region Internal

        private class DisplayRange
        {
            public double Start { get; private set; }
            public double End { get; set; }

            public DisplayRange(double start, double end)
            {
                Start = start;
                End = end;
            }

        }
        private class ViewportAxesRangeRestriction : IViewportRestriction
        {
            private readonly DisplayRange _xRange;
            private readonly DisplayRange _yRange;
            
            public ViewportAxesRangeRestriction(DisplayRange initialXRange, DisplayRange initialYRange)
            {
                _xRange = new DisplayRange(initialXRange.Start, initialXRange.End);
                _yRange = new DisplayRange(initialYRange.Start, initialYRange.End);
            }

            public Rect Apply(Rect oldVisible, Rect newVisible, Viewport2D viewport)
            {

                if (_xRange != null)
                {
                    newVisible.X = _xRange.Start;
                    newVisible.Width = _xRange.End - _xRange.Start;
                }

                if (_yRange != null)
                {
                    newVisible.Y = _yRange.Start;
                    newVisible.Height = _yRange.End - _yRange.Start;
                }

                return newVisible;
            }
            public void UpdateYMax(double newMax)
            {
                var currMax = _yRange.End;
                if (Math.Abs(currMax - newMax) > .00000001)
                {
                    _yRange.End = newMax;
                    RaiseViewPortChanged();
                }
            }

            #region Changed

            public event EventHandler Changed;

            private void RaiseViewPortChanged()
            {
                var handler = Changed;
                if (handler != null)
                    handler(this, null);
            }

            #endregion
        }

/*
        private class NullContextMenu : DefaultContextMenu, IPlotterElement
        {
            public NullContextMenu()
            {
               
            }
             
            void IPlotterElement.OnPlotterAttached(Plotter plotter)
            {
              
            }
            void IPlotterElement.OnPlotterDetaching(Plotter plotter)
            {
            }
            Plotter IPlotterElement.Plotter
            {
                get { return null; }
            }
        }
*/
        #endregion

    }
}
