﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.ComponentModel;

namespace Daxor.Lab.QCModule.Interfaces
{
    public interface IQualityTestController : INotifyPropertyChanged
    {
        /// <summary>
        /// Gets RunningTest
        /// </summary>
        Daxor.Lab.QC.Core.QCTest RunningTest { get; }

        /// <summary>
        /// Gets SelectedTest
        /// </summary>
        Daxor.Lab.QC.Core.QCTest SelectedTest { get; }


        /// <summary>
        /// Gets SaveTestCommand.
        /// </summary>
        ICommand SaveTestCommand { get; }

        /// <summary>
        /// Gets SelectCustomerCommand.
        /// </summary>
        ICommand SelectTestCommand { get; }

        /// <summary>
        /// Gets StartTestExecutionCommand
        /// </summary>
        ICommand StartTestExecutionCommand { get; }

        /// <summary>
        /// Gets BeginNewTestCommand
        /// </summary>
        ICommand BeginNewTestCommand { get; }


        String SelectedTestFriendlyName { get; }

        void Navigate(string key);

    }
}
