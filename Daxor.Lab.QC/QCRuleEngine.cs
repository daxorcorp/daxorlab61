﻿using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Evaluation;
using Daxor.Lab.Infrastructure.RuleFramework.Rules;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Common;
using Daxor.Lab.QC.Common.RuleEvaluators;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.QC
{
    public class QCRuleEngine : RuleEngine
    {
        #region Fields
        private readonly ISettingsManager _settingsManager;

        #endregion

        #region Properties

        public bool IsInitialized { get; private set; }

        #endregion

        #region Ctor
        public QCRuleEngine(ISettingsManager settingsManager)
        {
            _settingsManager = settingsManager;
        }

        #endregion
        
        #region Methods

        public void Initialize()
        {
            // Ensure only one initialization during lifespan
            if (IsInitialized)
                return;

            AddRequiredFieldRulesForFullQCTest();
            AddRequiredFieldRulesForContaminationTest();
            AddRequiredFieldRulesForLinearityTest();
            AddRequiredFieldRulesForStandardsOnlyTest();

            SetEnabledStateOfInjectateVerificationRules();

            _settingsManager.SettingsChanged += OnSettingsManagerSettingsChanged;

            IsInitialized = true;
        }

        #endregion

        #region Helpers

        void OnSettingsManagerSettingsChanged(object sender, SettingsChangedEventArgs e)
        {
            var changedSettings = from s in e.ChangedSettingIdentifiers where s == SettingKeys.SystemInjectateVerificationEnabled select s;
            // ReSharper disable PossibleMultipleEnumeration
            if (changedSettings.Any())
                SetEnabledStateOfInjectateVerificationRules();
        }

        void SetEnabledStateOfInjectateVerificationRules()
        {
            var injectateVerificationEnabled = _settingsManager.GetSetting<bool>(SettingKeys.SystemInjectateVerificationEnabled);
            var affectedRuleIds = new List<string> { RuleIDs.StandardAKeyFieldRequired, RuleIDs.StandardBKeyFieldRequired };

            var ruleIDsToEnable = injectateVerificationEnabled ? affectedRuleIds : new List<string>();
            var ruleIDsToDisable = injectateVerificationEnabled ? new List<string>() : affectedRuleIds;

            EnableRules(ruleIDsToEnable);
            DisableRules(ruleIDsToDisable);
        }

        void AddRequiredFieldRulesForFullQCTest()
        {
            AddRule(new SystemRequiredRule<QcFullTest>("InjectateLot", "Injectate Lot is a required field.",
               m => StaticRuleEvaluators.FieldIsNotEmpty(m.InjectateLot), RuleIDs.InjectateLotFieldRequired));
            AddRule(new SystemRequiredRule<QcFullTest>("Analyst", "Analyst is a required field.",
               m => StaticRuleEvaluators.FieldIsNotEmpty(m.Analyst), RuleIDs.AnalystFieldRequired));
            AddRule(new SystemRequiredRule<QcFullTest>("StandardAKey", "Standard A Key is a required field.",
               m => StaticRuleEvaluators.FieldIsNotEmpty(m.StandardAKey), RuleIDs.StandardAKeyFieldRequired));
            AddRule(new SystemRequiredRule<QcFullTest>("StandardBKey", "Standard B Key is a required field.",
               m => StaticRuleEvaluators.FieldIsNotEmpty(m.StandardBKey), RuleIDs.StandardBKeyFieldRequired));
        }

        void AddRequiredFieldRulesForContaminationTest()
        {
            AddRule(new SystemRequiredRule<QcContaminationTest>("Analyst", "Analyst is a required field.",
               m => StaticRuleEvaluators.FieldIsNotEmpty(m.Analyst), RuleIDs.AnalystFieldRequired));
        }

        void AddRequiredFieldRulesForLinearityTest()
        {
            AddRule(new SystemRequiredRule<QcLinearityTest>("Analyst", "Analyst is a required field.",
               m => StaticRuleEvaluators.FieldIsNotEmpty(m.Analyst), RuleIDs.AnalystFieldRequired));
        }

        void AddRequiredFieldRulesForStandardsOnlyTest()
        {
            AddRule(new SystemRequiredRule<QcStandardsTest>("InjectateLot", "Injectate Lot is a required field.",
               m => StaticRuleEvaluators.FieldIsNotEmpty(m.InjectateLot), RuleIDs.InjectateLotFieldRequired));
            AddRule(new SystemRequiredRule<QcStandardsTest>("Analyst", "Analyst is a required field.",
               m => StaticRuleEvaluators.FieldIsNotEmpty(m.Analyst), RuleIDs.AnalystFieldRequired));
            AddRule(new SystemRequiredRule<QcStandardsTest>("StandardAKey", "Standard A Key is a required field.",
               m => StaticRuleEvaluators.FieldIsNotEmpty(m.StandardAKey), RuleIDs.StandardAKeyFieldRequired));
            AddRule(new SystemRequiredRule<QcStandardsTest>("StandardBKey", "Standard B Key is a required field.",
               m => StaticRuleEvaluators.FieldIsNotEmpty(m.StandardBKey), RuleIDs.StandardBKeyFieldRequired));
        }

        #endregion
    }
}
