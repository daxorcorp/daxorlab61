﻿using System;
using System.Linq;
using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core;

namespace Daxor.Lab.QCModule.ExeContext
{
    /// <summary>
    /// The linearity execution context
    /// </summary>
    public class LinearityExecutionContext : QualityControlExecutionContextBase<QCLinearityTest>
    {
         private readonly Action<QCTest> saveTestAction;
        public LinearityExecutionContext(QCLinearityTest test, Action<QCTest> doSaveTest) : base(test)
        {
            saveTestAction = doSaveTest;
            _bkgSample = (from s in _test.Samples where s.TypeOfSample == QC.Core.Common.SampleType.Background select s).FirstOrDefault();

            if (_bkgSample == null)
                _rSampleIndex = 0;
            else
                _rSampleIndex = 1;
            //_test.PropertyChanged += Test_PropertyChanged;
        }

        //void Test_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    if (e.PropertyName == "TestStatus")
        //        if (saveTestAction != null) saveTestAction(_test);
        //}

        public override ISample GetNextSampleToExecute()
        {
            QCSample nextSample = null;

            if (_rSampleIndex >= 0)
            {
                QCSample currSample = _test.Samples.ElementAt(_rSampleIndex);
                if (currSample != null && currSample.IsEvaluationCompleted)
                    _rSampleIndex++;

                //Will return null if running sample index is greater or equals to total number of samples
                nextSample = _rSampleIndex >= _test.Samples.Count() ? null : _test.Samples.ElementAt(_rSampleIndex);

                if (nextSample != null)
                {
                    double presetLiveTime = _test.GetDesiredExecutionTimeInSeconds(nextSample);
                    if (presetLiveTime == Double.MinValue)
                        throw new Exception("someone did not implement GetDesiredExecutionTimeInSeconds accurately");

                    nextSample.PresetLiveTimeInSeconds = (int)presetLiveTime;
                }
            }

            if (saveTestAction != null) saveTestAction(_test);

            return nextSample;
        }
        public override void ProcessExecutingSample(ISample sample)
        {
            QCSample qSample = sample as QCSample;

            _test.ProcessSample(qSample);

            if (qSample.ExecutionStatus == Domain.Common.SampleExecutionStatus.Completed)
            {
                _test.EvaluateSample(qSample);
            }
            else
            {
                if (qSample.PresetCountsLimit > 0 && qSample.PresetCountLimitReached)
                    FireExecutionParametersUpdated(new ExecutionParametersUpdatedEventArgs(null, null, true));
            }
        }
     
    }
}
