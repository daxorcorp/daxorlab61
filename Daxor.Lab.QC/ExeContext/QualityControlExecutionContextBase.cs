﻿using System;
using System.Linq;
using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core;

namespace Daxor.Lab.QCModule.ExeContext
{
    /// <summary>
    /// Abstract Quality Control execution context base
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class QualityControlExecutionContextBase<T> : ITestExecutionContext where T : QCTest
    {
        #region Fields

        protected readonly T _test;
        protected QCSample _bkgSample;
        protected int _rSampleIndex = -1;

        #endregion

        #region Ctor

        public QualityControlExecutionContextBase(T test)
        {
            if (test == null)
                throw new ArgumentNullException("test cannot be null");

            _test = test;
        }

        #endregion

        public abstract ISample GetNextSampleToExecute();
        public abstract void ProcessExecutingSample(ISample sample);

        #region TestExecutionStages

        public virtual void CancelExecution()
        {
            if (_test != null)
            {
                _test.Status = Domain.Common.TestStatus.Aborted;
                var sample = _test.Samples.ElementAt(_rSampleIndex);
                sample.ExecutionStatus = Domain.Common.SampleExecutionStatus.Completed;
                sample.IsPassed = false;

                if (BackgroundSample.ExecutionStatus != Domain.Common.SampleExecutionStatus.Completed)
                {
                    QCSample bkgSample = BackgroundSample as QCSample;
                    bkgSample.ExecutionStatus = Domain.Common.SampleExecutionStatus.Completed;
                    bkgSample.IsPassed = false;
                }
            }

        }
        public virtual void StartExecution()
        {
            if (_test.Status == Domain.Common.TestStatus.Pending)
                _test.Status = Domain.Common.TestStatus.Running;
        }
        public void OverrideBackground(int counts)
        {
            throw new NotSupportedException("OverrideBackground is not supported in Quality Control");
        }

        #endregion

        #region Properties

        public ISample BackgroundSample
        {
            get { return _bkgSample; }
            protected set { _bkgSample = (QCSample)value; }
        }
        public Guid ExecutingTestID
        {
            get
            {
                if (_test != null)
                    return _test.InternalID;

                return Guid.Empty;
            }
        }
        #endregion

        #region Events

        public event EventHandler<ExecutionParametersUpdatedEventArgs> ExecutionParametersUpdated;
        protected virtual void FireExecutionParametersUpdated(ExecutionParametersUpdatedEventArgs args)
        {
            var handler = ExecutionParametersUpdated;
            if (handler != null)
                handler(this, args);
        }

        #endregion
    }
}
