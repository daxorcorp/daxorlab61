﻿using System;
using System.Linq;
using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.QC.Core;

namespace Daxor.Lab.QCModule.ExeContext
{
    /// <summary>
    /// The calibration execution context
    /// </summary>
    internal class CalibrationExecutionContext : QualityControlExecutionContextBase<QCCalibrationTest>
    {
        private readonly Action<QCTest> saveTestAction;
        public CalibrationExecutionContext(QCCalibrationTest test, Action<QCTest> doSaveTest) : base(test)
        {
            saveTestAction = doSaveTest;
            _bkgSample = (from s in _test.Samples where s.TypeOfSample == QC.Core.Common.SampleType.Background select s).FirstOrDefault();

            if (_bkgSample == null)
                _rSampleIndex = 0;
            else
                _rSampleIndex = 1;
            //_test.PropertyChanged += Test_PropertyChanged;
        }

        //void Test_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    if (e.PropertyName == "TestStatus")
        //        if (saveTestAction != null) saveTestAction(_test);
        //}
        public override Domain.Interfaces.ISample GetNextSampleToExecute()
        {
            QCSample nextSample = null;

            if (_rSampleIndex >= 0)
            {
                QCSample currSample = _test.Samples.ElementAt(_rSampleIndex);
                if (currSample != null && currSample.IsEvaluationCompleted)
                    _rSampleIndex++;

                //Will return null if running sample index is greater or equals to total number of samples
                nextSample = _rSampleIndex >= _test.Samples.Count() ? null : _test.Samples.ElementAt(_rSampleIndex);
               
                if (nextSample != null)
                {
                    double presetLiveTime = _test.GetDesiredExecutionTimeInSeconds(nextSample);
                    if (presetLiveTime == Double.MinValue)
                        throw new Exception("someone did not implement GetDesiredExecutionTimeInSeconds accurately");

                    nextSample.PresetLiveTimeInSeconds = (int)presetLiveTime;
                }
            }

            if (saveTestAction != null) saveTestAction(_test);

            return nextSample;
        }
        public override void ProcessExecutingSample(Domain.Interfaces.ISample sample)
        {
            QCSample rSample = sample as QCSample;

            //Hook up to calibration fine gain changed
            double prevFineGain = _test.CurrFineGain;

            _test.ProcessSample(rSample);
            _test.EvaluateSample(rSample);

            //Stop the test execution
            if (rSample.IsPassed == false)
                _rSampleIndex = -1;

            //Verify that fine gain has changed after evaluation
            if (prevFineGain != _test.CurrFineGain)
                FireExecutionParametersUpdated(new ExecutionParametersUpdatedEventArgs(_test.CurrFineGain, null));
        }

        void CalibrationFineGainChangedHandler(double oldFineGain, double newFineGain)
        {
            FireExecutionParametersUpdated(new ExecutionParametersUpdatedEventArgs(newFineGain, null));
        }
    }
}
