﻿using System;
using System.Linq;
using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core;

namespace Daxor.Lab.QCModule.ExeContext
{
    /// <summary>
    /// The full quality execution context
    /// </summary>
    public class FullQualityExecutionContext : QualityControlExecutionContextBase<QCFullTest>
    {
        private readonly Action<QCTest> saveTestAction;
      
        public FullQualityExecutionContext(QCFullTest fTest,  Action<QCTest> doSaveTest) : base(fTest)
        {
            saveTestAction = doSaveTest;
            _bkgSample = (from s in _test.Samples where s.TypeOfSample == QC.Core.Common.SampleType.Background select s).FirstOrDefault();

            if (_bkgSample == null)
                _rSampleIndex = 0;
            else
                _rSampleIndex = 1;

            //_test.PropertyChanged += Test_PropertyChanged;
        }

        //void Test_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    if(e.PropertyName == "TestStatus")
        //        //Saving test
        //        if (saveTestAction != null) saveTestAction(_test);
        //}
      
        public override ISample GetNextSampleToExecute()
        {
            QCSample nextSample = null;

            if (_rSampleIndex >= 0)
            {

                QCSample currSample = _test.Samples.ElementAt(_rSampleIndex);
                if (currSample != null && currSample.IsEvaluationCompleted)
                    _rSampleIndex++;

                //Will return null if running sample index is greater or equals to total number of samples
                nextSample = _rSampleIndex >= _test.Samples.Count() ? null : _test.Samples.ElementAt(_rSampleIndex);

                //Figure out sample acquisition time; if two or more tests share the sample, acquisition time will be the largest
                if (nextSample != null)
                {
                    double presetLiveTime = _test.GetDesiredExecutionTimeInSeconds(nextSample);
                    if (presetLiveTime == Double.MinValue)
                        throw new Exception("someone did not implement GetDesiredExecutionTimeInSeconds accurately");

                    nextSample.PresetLiveTimeInSeconds = (int)presetLiveTime;
                }
            }

            //Saving test
            if (saveTestAction != null) saveTestAction(_test);
            return nextSample;
        }
       
        public override void ProcessExecutingSample(ISample sample)
        {
            QCSample qcSample = sample as QCSample;

            try
            {
                //Process sample
                _test.ProcessSample(qcSample);

                //Can't rely on someone setting evaluation status to true, by mistake
                qcSample.IsEvaluationCompleted = false;

                //Once sample execution status is completed, we need to evaluate sample for results
                if (qcSample.ExecutionStatus == Domain.Common.SampleExecutionStatus.Completed)
                {
                    double prevFineGain = _test.CurrFineGain;

                    var eResult = _test.EvaluateSample(qcSample);

                    //If failed without continue, don't proceed
                    if (eResult.Result == QC.Core.Common.EvalResult.Failed)
                    {
                        _rSampleIndex = Int32.MinValue;
                    }
                    //If failed, we need to repeat but with new hardware settings
                    else if (eResult.Result == QC.Core.Common.EvalResult.FailedButRepeat)
                    {
                        qcSample.IsEvaluationCompleted = false;

                    }
                    //If failed but we need to go on to the next sample
                    else if (eResult.Result == QC.Core.Common.EvalResult.FailedButContinue)
                    {
                        qcSample.IsEvaluationCompleted = true;
                    }
                   
                    //Fine gain has changed, let's do someting about it
                    if (prevFineGain != _test.CurrFineGain)
                        FireExecutionParametersUpdated(new ExecutionParametersUpdatedEventArgs(_test.CurrFineGain, null));
                }
                else
                {
                    //Check if we reached some internal preset count limit
                    if (qcSample.PresetCountsLimit > 0 && qcSample.PresetCountLimitReached)
                        FireExecutionParametersUpdated(new ExecutionParametersUpdatedEventArgs(null, null, true));
                }
            }
            catch(Exception ex)
            {
                _rSampleIndex = Int32.MinValue;
            }
        }

      
    }
}
