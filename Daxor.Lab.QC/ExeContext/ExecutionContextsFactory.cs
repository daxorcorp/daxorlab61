﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core;

namespace Daxor.Lab.QCModule.ExeContext
{
    /// <summary>
    /// The execution context factory
    /// </summary>
    public static class ExecutionContextsFactory
    {
        /// <summary>
        /// Creates execution context
        /// </summary>
        /// <param name="test"></param>
        /// <param name="actionSaveTest"></param>
        /// <returns></returns>
        public static ITestExecutionContext CreateExeContext(QCTest test, Action<QCTest> actionSaveTest)
        {
            if (test == null)
                throw new ArgumentNullException("QCTest");

            ITestExecutionContext context = null;
            switch (test.QCType)
            {
                case TestType.Full:
                    context = new FullQualityExecutionContext(test as QCFullTest, actionSaveTest); 
                    break;
                case TestType.Calibration:
                    context = new CalibrationExecutionContext(test as QCCalibrationTest, actionSaveTest);
                    break;
                case TestType.Contamination:
                    context = new ContaminationExecutionContext(test as QCContaminationTest, actionSaveTest);
                    break;
                case TestType.Linearity:
                    context = new LinearityExecutionContext(test as QCLinearityTest, actionSaveTest);
                    break;
                case TestType.Standards:
                    context = new StandardExecutionContext(test as QCStandardsTest, actionSaveTest);
                    break;
                default:
                    throw new NotSupportedException(String.Format("ExecutionContext for '{0}' test is not supported." + test.QCType.ToString()));
            }

            return context;
        }
    }
}
