﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.QC.Core;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.QCModule.ExeContext
{
    /// <summary>
    /// The contamination test execution context
    /// </summary>
    internal class ContaminationExecutionContext : QualityControlExecutionContextBase<QCContaminationTest>
    {
        private readonly Action<QCTest> saveTestAction;

        public ContaminationExecutionContext(QCContaminationTest test, Action<QCTest> doSaveTest) : base(test)
        {
            saveTestAction = doSaveTest;
            _bkgSample = (from s in _test.Samples where s.TypeOfSample == QC.Core.Common.SampleType.Background select s).FirstOrDefault();

            if (_bkgSample == null)
                _rSampleIndex = 0;
            else
                _rSampleIndex = 1;
            //_test.PropertyChanged += Test_PropertyChanged;
        }

        //void Test_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    if (e.PropertyName == "TestStatus")
        //        if (saveTestAction != null) saveTestAction(_test);
        //}
        public override Domain.Interfaces.ISample GetNextSampleToExecute()
        {
            QCSample currSample = _test.Samples.ElementAt(_rSampleIndex);
            if (currSample != null && currSample.IsEvaluationCompleted)
                _rSampleIndex++;

            //Will return null if running sample index is greater or equals to total number of samples
            QCSample nextSample = _rSampleIndex >= _test.Samples.Count() ? null : _test.Samples.ElementAt(_rSampleIndex);
            if (nextSample != null)
            {
                double presetLiveTime = _test.GetDesiredExecutionTimeInSeconds(nextSample);
                if (presetLiveTime == Double.MinValue)
                    throw new Exception("someone did not implement GetDesiredExecutionTimeInSeconds accurately");

                nextSample.PresetLiveTimeInSeconds = (int)presetLiveTime;
            }

            if (saveTestAction != null) saveTestAction(_test);
            return nextSample;
        }
        public override void ProcessExecutingSample(Domain.Interfaces.ISample sample)
        {
            QCSample qSample = sample as QCSample;

            //Process sample regardless if it's completed or not
            _test.ProcessSample(qSample);

            //Start evaluation if sample is completed
            if (qSample.ExecutionStatus == Domain.Common.SampleExecutionStatus.Completed)
                _test.EvaluateSample(qSample);
        }

    }
}
