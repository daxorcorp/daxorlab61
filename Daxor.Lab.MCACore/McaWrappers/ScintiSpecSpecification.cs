﻿using Daxor.Lab.MCACore.Interfaces;

namespace Daxor.Lab.MCACore.McaWrappers
{
    /// <summary>
    /// Represents a property bag of bounds specifications and information about a scintiSPEC multi-channel analyzer
    /// </summary>
    public class ScintiSpecSpecification : IMultiChannelAnalyzerSpecification
    {
        /// <summary>
        /// Setting key for the noise level
        /// </summary>
        public const string NoiseLevelSettingKey = "noiselevel";

        /// <summary>
        /// Setting key for the lower-level discriminator
        /// </summary>
        public const string LowerLevelDiscriminatorSettingKey = "lld";

        /// <summary>
        /// Setting key for the high voltage
        /// </summary>
        public const string HighVoltageSettingKey = "highvoltage";

        /// <summary>
        /// Setting key for the fine gain
        /// </summary>
        public const string FineGainSettingKey = "finegain";

        /// <summary>
        /// Setting key for the serial number
        /// </summary>
        public const string SerialNumberKey = "serialnumber";

        /// <summary>
        /// Creates a new ScintiSpecSpecification instance based on the given port number
        /// </summary>
        /// <param name="portNumber">Port used for communicating with the MCA</param>
        public ScintiSpecSpecification(int portNumber)
        {
            PortNumber = portNumber;
        }

        /// <summary>
        /// Gets or sets the port number for communicating with the scintiSPEC MCA
        /// </summary>
        public int PortNumber { get; private set; }

        /// <summary>
        /// Gets the minimum allowable fine gain for the scintiSPEC MCA
        /// </summary>
        public double MinFineGain { get { return 0; } }

        /// <summary>
        /// Gets the maximum allowable fine gain for the scintiSPEC MCA
        /// </summary>
        public double MaxFineGain { get { return 2.0; } }
        
        /// <summary>
        /// Gets the minimum allowable high voltage for the scintiSPEC MCA
        /// </summary>
        public int MinHighVoltage { get { return 600; } }

        /// <summary>
        /// Gets the maximum allowable high voltage for the scintiSPEC MCA
        /// </summary>
        public int MaxHighVoltage { get { return 1200; } }

        /// <summary>
        /// Gets the minimum allowable lower-level discriminator for the scintiSPEC MCA
        /// </summary>
        public int MinLowerLevelDiscriminator { get { return 0; } }
        
        /// <summary>
        /// Gets the maximum allowable lower-level discriminator for the scintiSPEC MCA
        /// </summary>
        public int MaxLowerLevelDiscriminator { get { return 1023; } }

        /// <summary>
        /// Gets the minimum allowable noise level for the scintiSPEC MCA
        /// </summary>
        public int MinNoiseLevel { get { return 0; } }

        /// <summary>
        /// Gets the maximum allowable noise level for the scintiSPEC MCA
        /// </summary>
        public int MaxNoiseLevel { get { return 85; } }
    }
}
