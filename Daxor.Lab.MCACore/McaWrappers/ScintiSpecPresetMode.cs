﻿namespace Daxor.Lab.MCACore.McaWrappers
{
    /// <summary>
    /// Represents a preset mode for the scintiSPEC multi-channel analyzer
    /// </summary>
    public enum ScintiSpecPresetMode
    {
        /// <summary>
        /// Unknown mode
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// Real time
        /// </summary>
        RealTime = 1,

        /// <summary>
        /// Live time
        /// </summary>
        LiveTime = 2,

        /// <summary>
        /// Gross count in the region of interest
        /// </summary>
        GrossCount = 3,
    }
}
