﻿using System;
using System.Runtime.InteropServices;
using Daxor.Lab.MCACore.Interfaces;

namespace Daxor.Lab.MCACore.McaWrappers
{
    /// <summary>
    /// Represents a service that communicates with the scintiSPEC DLL by commmunicating
    /// through interop
    /// </summary>
    public class ScintiSpecDllWrapper : IScintiSpecDllWrapper
    {
        /// <summary>
        /// Number of channels in the spectrum
        /// </summary>
        public const int SpectrumLength = 1024;

        /// <summary>
        /// Normalization factor for converting decimal-based fine gain to integer for DLL API call
        /// </summary>
        public const int FineGainNormalizationFactor = 32768;

        /// <summary>
        /// Number of microseconds per second
        /// </summary>
        public const int MicrosecondsPersecond = 1000000;

        /// <summary>
        /// Software interrupt API call on the scintiSPEC native DLL
        /// </summary>
        /// <param name="rbp">Base pointer register value</param>
        /// <param name="rdi">Destination index register value</param>
        /// <param name="rsi">Source index register value</param>
        /// <param name="rds">Data segment register value</param>
        /// <param name="res">Extra segment register value</param>
        /// <param name="rdx">Data register value</param>
        /// <param name="rcx">Counter register value</param>
        /// <param name="rbx">Base register value</param>
        /// <param name="rax">Accumulator register value</param>
        /// <param name="rip">Instruction pointer register value</param>
        /// <param name="rcs">Code segment register value</param>
        /// <param name="flags">Flags value</param>
        [DllImport("tmcadrv32wdm.dll", EntryPoint = "SWI", SetLastError = true, ExactSpelling = true, CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void SWI(ref Int64 rbp, ref Int64 rdi, ref Int64 rsi, ref Int64 rds,
            ref Int64 res, ref Int64 rdx, ref Int64 rcx, ref Int64 rbx, ref Int64 rax,
            ref Int64 rip, ref Int64 rcs, ref byte flags);

        /// <summary>
        /// Invokes a software interrupt on the scintiSPEC native DLL using the given register values/references
        /// </summary>
        /// <param name="basePointer">Base pointer register value</param>
        /// <param name="destinationIndex">Destination index register value</param>
        /// <param name="sourceIndex">Source index register value</param>
        /// <param name="dataSegment">Data segment register value</param>
        /// <param name="extraSegment">Extra segment register value</param>
        /// <param name="dataRegister">Data register value</param>
        /// <param name="counterRegister">Counter register value</param>
        /// <param name="baseRegister">Base register value</param>
        /// <param name="accumulatorRegister">Accumulator register value</param>
        /// <param name="instructionPointer">Instruction pointer register value</param>
        /// <param name="codeSegment">Code segment register value</param>
        /// <param name="flags">Flags value</param>
        public ScintiSpecVoid InvokeReturningVoid(Int64 basePointer, Int64 destinationIndex, Int64 sourceIndex, Int64 dataSegment,
            Int64 extraSegment, Int64 dataRegister, Int64 counterRegister, Int64 baseRegister, Int64 accumulatorRegister,
            Int64 instructionPointer, Int64 codeSegment, byte flags)
        {
            SWI(ref basePointer, ref destinationIndex, ref sourceIndex, ref dataSegment, ref extraSegment, ref dataRegister, ref counterRegister, ref baseRegister, ref accumulatorRegister, ref instructionPointer, ref codeSegment, ref flags);

            return new ScintiSpecVoid {ShouldThrowException = accumulatorRegister < 0};
        }

        /// <summary>
        /// Invokes a software interrupt on the scintiSPEC native DLL using the given register values/references
        /// </summary>
        /// <param name="basePointer">Base pointer register value</param>
        /// <param name="destinationIndex">Destination index register value</param>
        /// <param name="sourceIndex">Source index register value</param>
        /// <param name="dataSegment">Data segment register value</param>
        /// <param name="extraSegment">Extra segment register value</param>
        /// <param name="dataRegister">Data register value</param>
        /// <param name="counterRegister">Counter register value</param>
        /// <param name="baseRegister">Base register value</param>
        /// <param name="accumulatorRegister">Accumulator register value</param>
        /// <param name="instructionPointer">Instruction pointer register value</param>
        /// <param name="codeSegment">Code segment register value</param>
        /// <param name="flags">Flags value</param>
        /// <returns>Integer result of the operation</returns>
        public Int64 InvokeReturningInt(Int64 basePointer, Int64 destinationIndex, Int64 sourceIndex, Int64 dataSegment,
            Int64 extraSegment, Int64 dataRegister, Int64 counterRegister, Int64 baseRegister, Int64 accumulatorRegister,
            Int64 instructionPointer, Int64 codeSegment, byte flags)
        {
            SWI(ref basePointer, ref destinationIndex, ref sourceIndex, ref dataSegment, ref extraSegment, ref dataRegister, ref counterRegister, ref baseRegister, ref accumulatorRegister, ref instructionPointer, ref codeSegment, ref flags);

            return counterRegister;
        }

        /// <summary>
        /// Invokes a software interrupt on the scintiSPEC native DLL using the given register values/references
        /// </summary>
        /// <param name="basePointer">Base pointer register value</param>
        /// <param name="destinationIndex">Destination index register value</param>
        /// <param name="sourceIndex">Source index register value</param>
        /// <param name="dataSegment">Data segment register value</param>
        /// <param name="extraSegment">Extra segment register value</param>
        /// <param name="dataRegister">Data register value</param>
        /// <param name="counterRegister">Counter register value</param>
        /// <param name="baseRegister">Base register value</param>
        /// <param name="accumulatorRegister">Accumulator register value</param>
        /// <param name="instructionPointer">Instruction pointer register value</param>
        /// <param name="codeSegment">Code segment register value</param>
        /// <param name="flags">Flags value</param>
        /// <returns>Boolean result of the operation</returns>
        public ScintiSpecBool InvokeReturningBool(Int64 basePointer, Int64 destinationIndex, Int64 sourceIndex, Int64 dataSegment,
            Int64 extraSegment, Int64 dataRegister, Int64 counterRegister, Int64 baseRegister, Int64 accumulatorRegister,
            Int64 instructionPointer, Int64 codeSegment, byte flags)
        {
            SWI(ref basePointer, ref destinationIndex, ref sourceIndex, ref dataSegment, ref extraSegment, ref dataRegister, ref counterRegister, ref baseRegister, ref accumulatorRegister, ref instructionPointer, ref codeSegment, ref flags);

            return new ScintiSpecBool
            {
                ReturnValue = counterRegister == 1,
                ShouldThrowException = accumulatorRegister < 0
            };
        }

        /// <summary>
        /// Invokes a software interrupt on the scintiSPEC native DLL using the given register values/references
        /// </summary>
        /// <param name="basePointer">Base pointer register value</param>
        /// <param name="destinationIndex">Destination index register value</param>
        /// <param name="sourceIndex">Source index register value</param>
        /// <param name="dataSegment">Data segment register value</param>
        /// <param name="extraSegment">Extra segment register value</param>
        /// <param name="dataRegister">Data register value</param>
        /// <param name="baseRegister">Base register value</param>
        /// <param name="accumulatorRegister">Accumulator register value</param>
        /// <param name="instructionPointer">Instruction pointer register value</param>
        /// <param name="codeSegment">Code segment register value</param>
        /// <param name="flags">Flags value</param>
        /// <returns>Array of unsigned integers</returns>
        public UInt32[] InvokeReturningIntArray(Int64 basePointer, Int64 destinationIndex, Int64 sourceIndex, Int64 dataSegment,
            Int64 extraSegment, Int64 dataRegister, Int64 baseRegister, Int64 accumulatorRegister,
            Int64 instructionPointer, Int64 codeSegment, byte flags)

        {
            IntPtr unmanagedBufferPointer = Marshal.AllocHGlobal(sizeof (UInt32)*SpectrumLength);
            try
            {
                Int64 unmanagedBuffer = unmanagedBufferPointer.ToInt64();

                SWI(ref basePointer, ref destinationIndex, ref sourceIndex, ref dataSegment, ref extraSegment, ref dataRegister, 
                    ref unmanagedBuffer, ref baseRegister, ref accumulatorRegister, ref instructionPointer, ref codeSegment, ref flags);

                var managedBuffer = ConvertToManagedBuffer(unmanagedBufferPointer);

                return managedBuffer;
            }
            catch
            {
                return null;
            }
            finally
            {
                Marshal.FreeHGlobal(unmanagedBufferPointer);
            }
        }

        private static unsafe UInt32[] ConvertToManagedBuffer(IntPtr pointer)
        {
            var managedBuffer = new UInt32[SpectrumLength];
            for (var i = 0; i < SpectrumLength; i++)
                managedBuffer[i] = *((UInt32*)pointer.ToPointer() + i);

            return managedBuffer;
        }
    }
}
