﻿using System;
using Daxor.Lab.MCACore.Interfaces;

namespace Daxor.Lab.MCACore.McaWrappers
{
    /// <summary>
    /// Represents a service that communicates with a DLL wrapper to fulfill multi-channel analyzer
    /// behavior.
    /// </summary>
    public class ScintiSpecDriver : IScintiSpecDriver
    {
        private readonly IScintiSpecDllWrapper _dllWrapper;

        /// <summary>
        /// Creates a new ScintiSpecDriver instance
        /// </summary>
        /// <param name="scintiSpecDllWrapper">Instance of a wrapper capable of communicating
        /// with the native scintiSPEC DLL</param>
        /// <exception cref="ArgumentNullException">If the given wrapper instance is null</exception>
        public ScintiSpecDriver(IScintiSpecDllWrapper scintiSpecDllWrapper)
        {
            if (scintiSpecDllWrapper == null) throw new ArgumentNullException("scintiSpecDllWrapper");
            _dllWrapper = scintiSpecDllWrapper;
        }

        /// <summary>
        /// Starts acquisition using the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>True if acquisition started successfully; false otherwise</returns>
        /// <exception cref="Exception">If acquisition could not be started</exception>
        public bool StartAcquisitionOnPort(Int64 portNumber)
        {
            var result = _dllWrapper.InvokeReturningBool(0, portNumber, 0, 0, 0, 0, 1, 1, 9, 0, 0, default(byte));
            
            if (result.ShouldThrowException) throw new Exception("StartAcquisitionOnPort(" + portNumber + ") has failed.");

            return result.ReturnValue;
        }

        /// <summary>
        /// Stops acquisition using the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <exception cref="Exception">If acquisition could not be stopped</exception>
        public void StopAcquisitionOnPort(Int64 portNumber)
        {
            if (!IsAcquiringOnPort(portNumber)) return;

            var result = _dllWrapper.InvokeReturningVoid(0, portNumber, 0, 0, 0, 0, 1, 0, 9, 0, 0, default(byte));

            if (result.ShouldThrowException) throw new Exception("StopAcquisitionOnPort(" + portNumber + ") has failed.");
        }

        /// <summary>
        /// Determines whether or not the MCA at the given port is currently acquiring
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>True if acquisition is in progress; false otherwise</returns>
        /// <exception cref="Exception">If the acquisition status could not be obtained</exception>
        public bool IsAcquiringOnPort(Int64 portNumber)
        {
            var result = _dllWrapper.InvokeReturningBool(0, portNumber, 1, 0, 0, 0, 0, 1, 10, 0, 0, default(byte));
            
            if (result.ShouldThrowException) throw new Exception("IsAcquiringOnPort(" + portNumber + ") has failed.");

            return result.ReturnValue;
        }

        /// <summary>
        /// Clears the active spectrum and preset times/counts for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <remarks>Does not reset the regions of interest</remarks>
        public void ClearSpectrumAndPresetsOnPort(Int64 portNumber)
        {
            _dllWrapper.InvokeReturningVoid(0, portNumber, 0, 0, 0, 0, 0, 0, 30, 0, 0, default(byte));
        }

        /// <summary>
        /// Sets the fine gain for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="fineGain">Fine gain</param>
        public void SetFineGainOnPort(Int64 portNumber, double fineGain)
        {
            var normalizedFineGain = (Int64) (ScintiSpecDllWrapper.FineGainNormalizationFactor * fineGain);

            _dllWrapper.InvokeReturningVoid(0, portNumber, 0, 0, 0, 0, 0, normalizedFineGain, 15, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the fine gain of the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Fine gain of the desired MCA (rounded to three decimal places)</returns>
        public double GetFineGainOnPort(Int64 portNumber)
        {
            var result = _dllWrapper.InvokeReturningInt(0, portNumber, 0, 0, 0, 0, 0, 0, 16, 0, 0, default(byte));

            return Math.Round(result/(double) ScintiSpecDllWrapper.FineGainNormalizationFactor, 3, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Sets the high voltage of the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="highVoltageInVolts">High voltage (in volts)</param>
        public void SetHighVoltageOnPort(Int64 portNumber, int highVoltageInVolts)
        {
            _dllWrapper.InvokeReturningVoid(0, portNumber, 0, 0, 0, 0, 0, highVoltageInVolts, 269, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the high voltage of the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>High voltage (in volts)</returns>
        /// <remarks>Previous implementation checked to see if the MCA was connected first</remarks>
        public int GetHighVoltageOnPort(Int64 portNumber)
        {
            return (int) _dllWrapper.InvokeReturningInt(0, portNumber, 0, 0, 0, 0, 0, 0, 270, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the analog-to-digital converter (ADC) high voltage of the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Power number</param>
        /// <returns>ADC high voltage (in volts)</returns>
        /// <remarks>Previous implementation checked to see if the MCA was connected first</remarks>
        public int GetAnalogToDigitalConverterHighVoltageOnPort(Int64 portNumber)
        {
            return (int)_dllWrapper.InvokeReturningInt(0, portNumber, 0, 0, 0, 0, 0, 0, 2574, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the spectrum (i.e., counts per channel) for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Array of counts for each channel</returns>
        public UInt32[] GetSpectrumOnPort(Int64 portNumber)
        {
            return _dllWrapper.InvokeReturningIntArray(0, portNumber, 0, 0, 0, 0, 0, 25, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the spectrum length (i.e., number of channels) for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Number of channels</returns>
        public int GetSpectrumLengthOnPort(Int64 portNumber)
        {
            return (int)_dllWrapper.InvokeReturningInt(0, portNumber, 0, 0, 0, 0, 0, 0, 28, 0, 0, default(byte));
        }

        /// <summary>
        /// Sets the noise level for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="noiseLevel">Noise level (channel)</param>
        /// <remarks>No bounds checks are done on the noise level value</remarks>
        public void SetNoiseLevelOnPort(Int64 portNumber, int noiseLevel)
        {
            _dllWrapper.InvokeReturningVoid(0, portNumber, 0, 0, 0, 0, 0, noiseLevel, 870, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the noise level for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Noise level (channel)</returns>
        public int GetNoiseLevelOnPort(Int64 portNumber)
        {
            return (int)_dllWrapper.InvokeReturningInt(0, portNumber, 0, 0, 0, 0, 0, 0, 869, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the lower-level discriminator (LLD) for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>LLD (channel)</returns>
        public int GetLowerLevelDiscriminatorOnPort(Int64 portNumber)
        {
            return (int)_dllWrapper.InvokeReturningInt(0, portNumber, 0, 0, 0, 0, 0, 0, 101, 0, 0, default(byte));
        }

        /// <summary>
        /// Sets the lower-level discriminator (LLD) for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="lowerLevelDiscriminatorChannel">LLD (channel)</param>
        /// <remarks>No bounds checks are done on the LLD value</remarks>
        public void SetLowerLevelDiscriminatorOnPort(Int64 portNumber, int lowerLevelDiscriminatorChannel)
        {
            _dllWrapper.InvokeReturningVoid(0, portNumber, 0, 0, 0, 0, 0, lowerLevelDiscriminatorChannel, 102, 0, 0, default(byte));
        }

        /// <summary>
        /// Presets the real-time limit for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="realTimeInseconds">Real time (seconds)</param>
        public void PresetRealTimeOnPort(Int64 portNumber, long realTimeInseconds)
        {
            _dllWrapper.InvokeReturningVoid(0, portNumber, 0, 0, 0, 0, 1, realTimeInseconds * ScintiSpecDllWrapper.MicrosecondsPersecond, 42, 0, 0, default(byte));
        }

        /// <summary>
        /// Presets the live-time limit for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="liveTimeInSeconds">Live time (seconds)</param>
        public void PresetLiveTimeOnPort(Int64 portNumber, long liveTimeInSeconds)
        {
            _dllWrapper.InvokeReturningVoid(0, portNumber, 0, 0, 0, 0, 1, liveTimeInSeconds * ScintiSpecDllWrapper.MicrosecondsPersecond, 43, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the preset mode (e.g., live time, gross counts in ROI) for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Preset mode</returns>
        public ScintiSpecPresetMode GetPresetModeOnPort(Int64 portNumber)
        {
            return (ScintiSpecPresetMode)_dllWrapper.InvokeReturningInt(0, portNumber, 0, 0, 0, 0, 0, 0, 558, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the real time for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Real time (in microseconds)</returns>
        public Int64 GetRealTimeInMicrosecondsOnPort(Int64 portNumber)
        {
            return _dllWrapper.InvokeReturningInt(0, portNumber, 0, 0, 0, 0, 0, 0, 39, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the live time for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Live time (in microseconds)</returns>
        public Int64 GetLiveTimeInMicrosecondsOnPort(Int64 portNumber)
        {
            return _dllWrapper.InvokeReturningInt(0, portNumber, 0, 0, 0, 0, 0, 0, 40, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the dead time for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Dead time (in microseconds)</returns>
        public Int64 GetDeadTimeInMicrosecondsOnPort(Int64 portNumber)
        {
            return _dllWrapper.InvokeReturningInt(0, portNumber, 0, 0, 0, 0, 0, 0, 41, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the serial number for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Serial number in the format yy12nnnn (yy = year, nnnn = number)</returns>
        public Int64 GetSerialNumberOnPort(Int64 portNumber)
        {
            return _dllWrapper.InvokeReturningInt(0, portNumber, 0, 0, 0, 0, 0, 0, 254, 0, 0, default(byte));
        }

        /// <summary>
        /// Shuts down the communication thread for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <remarks>There is a thread that communicates with the MCA that must be terminated before closing the
        /// application.</remarks>
        public void ShutDownOnPort(Int64 portNumber)
        {
            _dllWrapper.InvokeReturningVoid(0, portNumber, 0, 0, 0, 0, 0, 0, 100, 0, 0, default(byte));
        }

        /// <summary>
        /// Determines if the MCA at the given port is connected
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>True if the MCA is connected; false otherwise</returns>
        public bool IsConnectedOnPort(Int64 portNumber)
        {
            var result= _dllWrapper.InvokeReturningBool(0, portNumber, 0, 0, 0, 0, 0, 0, 511, 0, 0, default(byte));

            return result.ReturnValue;
        }

        /// <summary>
        /// Gets the preset-reached state for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Preset state</returns>
        public ScintiSpecPresetState GetPresetStateOnPort(Int64 portNumber)
        {
            return (ScintiSpecPresetState) _dllWrapper.InvokeReturningInt(0, portNumber, 0, 0, 0, 1, 0, 0, 302, 0, 0, default(byte));
        }

        /// <summary>
        /// Sets the region of interest for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="leftChannel">Left channel marker</param>
        /// <param name="rightChannel">Right channel marker</param>
        /// <remarks>No bounds checking is done on the markers</remarks>
        public void SetRegionOfInterestOnPort(Int64 portNumber, int leftChannel, int rightChannel)
        {
            _dllWrapper.InvokeReturningVoid(0, portNumber, 0, 0, 0, 0, rightChannel, leftChannel, 29, 0, 0, default(byte));
        }

        #region Unused in DaxorLab

        /// <summary>
        /// Starts acquisition on all available MCAs
        /// </summary>
        /// <returns>True if acquisition started successfully; false otherwise</returns>
        /// <exception cref="Exception">If acquisition could not be started</exception>
        public bool StartAcquisitionOnAllMcas()
        {
            var result = _dllWrapper.InvokeReturningBool(0, 1, 1, 0, 0, 0, 1, 1, 256, 0, 0, default(byte));

            if (result.ShouldThrowException) throw new Exception("StartAcquisitionOnAllMcas() has failed.");

            return result.ReturnValue;
        }

        /// <summary>
        /// Stops acquisition on all available MCAs
        /// </summary>
        /// <exception cref="Exception">If acquisition could not be stopped</exception>
        public void StopAcquisitionOnAllMcas()
        {
            var result = _dllWrapper.InvokeReturningVoid(0, 1, 1, 0, 0, 0, 1, 0, 256, 0, 0, default(byte));

            if (result.ShouldThrowException) throw new Exception("StopAcquisitionOnAllMcas() has failed.");
        }

        /// <summary>
        /// Gets the index of the spectrum for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Spectrum index</returns>
        public Int64 GetSpectrumIndexOnPort(Int64 portNumber)
        {
            return _dllWrapper.InvokeReturningInt(0, portNumber, 0, 0, 0, 0, 0, 0, 4, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the actual port index (documentation not clear)
        /// </summary>
        /// <returns>Port index</returns>
        public Int64 GetActualPortIndex()
        {
            return _dllWrapper.InvokeReturningInt(0, 0, 0, 1, 0, 0, 0, 0, 6, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the total number of allocated spectra
        /// </summary>
        /// <returns>Number of allocated spectra</returns>
        /// <remarks>Original implementation returned the base register, not the counter
        /// register; this is assumed to be a mistake</remarks>
        public Int64 GetTotalNumberOfAllocatedSpectra()
        {
            return _dllWrapper.InvokeReturningInt(0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, default(byte));
        }

        /// <summary>
        /// Gets the total number of ports
        /// </summary>
        /// <returns>Number of ports</returns>
        /// <remarks>Original implementation returned the base register, not the counter
        /// register; this is assumed to be a mistake</remarks>
        public Int64 GetTotalNumberOfPorts()
        {
            return _dllWrapper.InvokeReturningInt(0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, default(byte));
        }

        /// <summary>
        /// Presets the gross-counts limit in the region of interest for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="grossCounts">Gross counts</param>
        public void PresetGrossCountsInRegionOfInterestOnPort(Int64 portNumber, long grossCounts)
        {
            _dllWrapper.InvokeReturningVoid(0, portNumber, 0, 0, 0, 0, 0, grossCounts, 555, 0, 0, default(byte));
        }

        /// <summary>
        /// Resets the device driver
        /// </summary>
        public void Reset()
        {
            _dllWrapper.InvokeReturningVoid(0, 0, 0, 0, 0, 0, 0, 0, 606, 0, 0, default(byte));
        }
        
        #endregion
    }
}
