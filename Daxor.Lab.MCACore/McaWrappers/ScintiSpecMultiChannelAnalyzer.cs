﻿using System;
using System.Globalization;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.MCACore.Exceptions;
using Daxor.Lab.MCACore.Interfaces;

namespace Daxor.Lab.MCACore.McaWrappers
{
    /// <summary>
    /// Represents a service that implements the behavior of a Target/ICx scintiSPEC multi-channel analyzer
    /// </summary>
    public class ScintiSpecMultiChannelAnalyzer : IMultiChannelAnalyzer
    {
        private const int OneSecond = 1000;
        
        private readonly IScintiSpecDriver _scintiSpecDriver;
        private readonly IHardwareConfigurationManager _hardwareConfigurationManager;
        private readonly IMultiChannelAnalyzerSpecification _multiChannelAnalyzerSpecification;
        private readonly IWindowsEventLogger _windowsEventLogger;
        private readonly IThreadWrapper _threadWrapper;
        private readonly int _portNumber;
        private bool _isConnected;

        #region Public API

        /// <summary>
        /// Creates a new ScintiSpecMultiChannelAnalyzer instance and sets hardware settings
        /// based on the current configuration of settings
        /// </summary>
        /// <param name="scintiSpecDriver">Driver that facilitates communication with the hardware</param>
        /// <param name="hardwareConfigurationManager">Hardware configuration manager for managing settings</param>
        /// <param name="multiChannelAnalyzerSpecification">Specification for the scintiSPEC MCA</param>
        /// <param name="windowsEventLogger">Event logger instance</param>
        /// <param name="threadWrapper">Service to handle threading issues</param>
        /// <exception cref="ArgumentNullException">If any argument is null</exception>
        public ScintiSpecMultiChannelAnalyzer(IScintiSpecDriver scintiSpecDriver,
            IHardwareConfigurationManager hardwareConfigurationManager,
            IMultiChannelAnalyzerSpecification multiChannelAnalyzerSpecification, 
            IWindowsEventLogger windowsEventLogger,
            IThreadWrapper threadWrapper)
        {
            ThrowIfAnyArgumentIsNull(scintiSpecDriver, hardwareConfigurationManager, multiChannelAnalyzerSpecification,
                windowsEventLogger, threadWrapper);

            _scintiSpecDriver = scintiSpecDriver;
            _hardwareConfigurationManager = hardwareConfigurationManager;
            _multiChannelAnalyzerSpecification = multiChannelAnalyzerSpecification;
            _windowsEventLogger = windowsEventLogger;
            _threadWrapper = threadWrapper;

            _portNumber = _multiChannelAnalyzerSpecification.PortNumber;

            _isConnected = _scintiSpecDriver.IsConnectedOnPort(_portNumber);

            SetHardwareSettingsBasedOnCurrentConfiguration();
        }

        /// <summary>
        /// Occurs when acquisition has started
        /// </summary>
        public event Action<IMultiChannelAnalyzer, DateTime> AcquisitionStarted;

        /// <summary>
        /// Occurs when acquisition has completed
        /// </summary>
        public event Action<IMultiChannelAnalyzer, SpectrumData> AcquisitionCompleted;

        /// <summary>
        /// Occurs when acquisition has stopped (but not completed)
        /// </summary>
        public event Action<IMultiChannelAnalyzer, DateTime> AcquisitionStopped;

        /// <summary>
        /// Gets whether or not the multi-channel analyzer is connected. If the
        /// MCA was not previously connected, its current configuration of settings 
        /// is loaded.
        /// </summary>
        public bool IsConnected
        {
            get
            {
                var isConnectedNow = _scintiSpecDriver.IsConnectedOnPort(_portNumber);
                if (!_isConnected && isConnectedNow)
                    SetHardwareSettingsBasedOnCurrentConfiguration();
                _isConnected = isConnectedNow;

                return _isConnected;
            }
        }

        /// <summary>
        /// Gets or sets the noise level; the noise level is also propagated to the hardware configuration manager
        /// </summary>
        /// <exception cref="OutOfBoundsException">If setting the noise to a value outside the bounds
        /// set forth in the specification</exception>
        public int NoiseLevel 
        {
            get { return _scintiSpecDriver.GetNoiseLevelOnPort(_portNumber); }
            set
            {
                if (value < _multiChannelAnalyzerSpecification.MinNoiseLevel || value > _multiChannelAnalyzerSpecification.MaxNoiseLevel)
                    ThrowNoiseLevelOutOfBounds(value);

                _scintiSpecDriver.SetNoiseLevelOnPort(_portNumber, value);
                _hardwareConfigurationManager.GetSetting(ScintiSpecSpecification.NoiseLevelSettingKey).SetValue(value);
            }
        }

        /// <summary>
        /// Gets or sets the lower-level discriminator; the LLD is also propagated to the hardware configuration manager
        /// </summary>
        /// <exception cref="OutOfBoundsException">If setting the LLD to a value outside the bounds
        /// set forth in the specification</exception>
        public int LowerLevelDiscriminator
        {
            get { return _scintiSpecDriver.GetLowerLevelDiscriminatorOnPort(_portNumber); }
            set
            {
                if (value < _multiChannelAnalyzerSpecification.MinLowerLevelDiscriminator || value > _multiChannelAnalyzerSpecification.MaxLowerLevelDiscriminator)
                    ThrowLowerLevelDiscriminatorOutOfBounds(value);

                _scintiSpecDriver.SetLowerLevelDiscriminatorOnPort(_portNumber, value);
                _hardwareConfigurationManager.GetSetting(ScintiSpecSpecification.LowerLevelDiscriminatorSettingKey).SetValue(value);
            }
        }

        /// <summary>
        /// Gets or sets the fine gain; the fine gain is also propagated to the hardware configuration manager
        /// </summary>
        /// <exception cref="OutOfBoundsException">If setting the fine gain to a value outside the bounds
        /// set forth in the specification</exception>
        public double FineGain
        {
            get { return _scintiSpecDriver.GetFineGainOnPort(_portNumber); }
            set
            {
                if (value < _multiChannelAnalyzerSpecification.MinFineGain || value > _multiChannelAnalyzerSpecification.MaxFineGain)
                    ThrowFineGainOutOfBounds(value);

                _scintiSpecDriver.SetFineGainOnPort(_portNumber, value);
                _hardwareConfigurationManager.GetSetting(ScintiSpecSpecification.FineGainSettingKey).SetValue(value);
            }
        }

        /// <summary>
        /// Gets or sets the high voltage; the high voltage is also propagated to the hardware configuration manager
        /// </summary>
        /// <exception cref="OutOfBoundsException">If setting the high voltage to a value outside the bounds
        /// set forth in the specification</exception>
        public int HighVoltage
        {
            get { return _scintiSpecDriver.GetHighVoltageOnPort(_portNumber); }
            set
            {
                if (value < _multiChannelAnalyzerSpecification.MinHighVoltage || value > _multiChannelAnalyzerSpecification.MaxHighVoltage)
                    ThrowHighVoltageOutOfBounds(value);

                _scintiSpecDriver.SetHighVoltageOnPort(_portNumber, value);
                _hardwareConfigurationManager.GetSetting(ScintiSpecSpecification.HighVoltageSettingKey).SetValue(value);
            }       
        }

        /// <summary>
        /// Gets the serial number
        /// </summary>
        public int SerialNumber
        {
            get { return (int) _scintiSpecDriver.GetSerialNumberOnPort(_portNumber); }
        }

        /// <summary>
        /// Gets the spectrum length (i.e., number of channels)
        /// </summary>
        public int SpectrumLength
        {
            get { return _scintiSpecDriver.GetSpectrumLengthOnPort(_portNumber); }
        }

        /// <summary>
        /// Gets the high voltage for the analog-to-digital converter
        /// </summary>
        public int AnalogToDigitalConverterHighVoltage
        {
            get { return _scintiSpecDriver.GetAnalogToDigitalConverterHighVoltageOnPort(_portNumber); }
        }

        /// <summary>
        /// Gets whether or not acquisition is in progress
        /// </summary>
        public bool IsAcquiring
        {
            get { return _scintiSpecDriver.IsAcquiringOnPort(_portNumber); }
        }

        /// <summary>
        /// Gets whether or not the real-time has been preset
        /// </summary>
        public bool IsRealTimePreset
        {
            get { return _scintiSpecDriver.GetPresetModeOnPort(_portNumber) == ScintiSpecPresetMode.RealTime; }
        }

        /// <summary>
        /// Gets whether or not the live-time has been preset
        /// </summary>
        public bool IsLiveTimePreset
        {
            get { return _scintiSpecDriver.GetPresetModeOnPort(_portNumber) == ScintiSpecPresetMode.LiveTime; }
        }

        /// <summary>
        /// Gets whether or not the gross counts in the region-of-interest have been preset
        /// </summary>
        public bool IsGrossCountPreset
        {
            get { return _scintiSpecDriver.GetPresetModeOnPort(_portNumber) == ScintiSpecPresetMode.GrossCount; }
        }

        /// <summary>
        /// Gets whether or not acquisition has completed
        /// </summary>
        public bool HasAcquisitionCompleted
        {
            get
            {
                // Whenever a preset condition is reached, the preset state will be marked as "reached".
                // Any subsequent calls will return "not reached" until a new preset condition is reached.
                var currentAcquisitionState = _scintiSpecDriver.GetPresetStateOnPort(_portNumber);
                
                if (currentAcquisitionState == ScintiSpecPresetState.Reached && !InnerHasAcquisitionCompleted)
                    InnerHasAcquisitionCompleted = true;

                return InnerHasAcquisitionCompleted;
            }
        }

        /// <summary>
        /// Presets the live time
        /// </summary>
        /// <param name="liveTimeInSeconds">Live time preset (in seconds)</param>
        /// <exception cref="OutOfBoundsException">If the live time is negative</exception>
        public void PresetLiveTime(Int64 liveTimeInSeconds)
        {
            if (liveTimeInSeconds < 0)
                throw new OutOfBoundsException("0", Int64.MaxValue.ToString(), liveTimeInSeconds.ToString(), "Live-time preset value is out of bounds");

            _scintiSpecDriver.PresetLiveTimeOnPort(_portNumber, liveTimeInSeconds);
        }

        /// <summary>
        /// Presets the real time
        /// </summary>
        /// <param name="realTimeInSeconds">Real time preset (in seconds)</param>
        /// <exception cref="OutOfBoundsException">If the real time is negative</exception>
        public void PresetRealTime(Int64 realTimeInSeconds)
        {
            if (realTimeInSeconds < 0)
                throw new OutOfBoundsException("0", Int64.MaxValue.ToString(), realTimeInSeconds.ToString(), "Real-time preset value is out of bounds");

            _scintiSpecDriver.PresetRealTimeOnPort(_portNumber, realTimeInSeconds);
        }

        /// <summary>
        /// Presets the gross counts in the region-of-interest
        /// </summary>
        /// <param name="counts">Count limit</param>
        /// <param name="beginChannel">Beginning (left) channel</param>
        /// <param name="endChannel">Ending (right) channel</param>
        /// <exception cref="OutOfBoundsException">If any argument is negative, or if the beginning channel
        /// exceeds the ending channel</exception>
        public void PresetGrossCountsInRegionOfInterest(Int64 counts, int beginChannel, int endChannel)
        {
            if (counts < 0)
                throw new OutOfBoundsException("0", Int64.MaxValue.ToString(), counts.ToString(), "Preset gross counts value is out of bounds");
            if (beginChannel < 0)
                throw new OutOfBoundsException("0", Int64.MaxValue.ToString(), beginChannel.ToString(), "Region-of-interest beginning channel value is out of bounds");
            if (endChannel < 0)
                throw new OutOfBoundsException("0", Int64.MaxValue.ToString(), endChannel.ToString(), "Region-of-interest ending channel value is out of bounds");

            if (beginChannel > endChannel) { var swap = beginChannel; beginChannel = endChannel; endChannel = swap; }

            _scintiSpecDriver.SetRegionOfInterestOnPort(_portNumber, beginChannel, endChannel);
            _scintiSpecDriver.PresetGrossCountsInRegionOfInterestOnPort(_portNumber, counts);
        }
        
        /// <summary>
        /// Starts acquisition (if not already in progress) and initiates an acquisition monitoring thread
        /// </summary>
        /// <returns>True if acquisition started; false otherwise</returns>
        public bool StartAcquisition()
        {
            try
            {
                if (!IsAcquiring)
                {
                    if (!_scintiSpecDriver.StartAcquisitionOnPort(_portNumber)) 
                        return false;

                    InnerHasAcquisitionCompleted = false;
                    _threadWrapper.Start(MonitorAcquisition);
                    _threadWrapper.Sleep(0);

                    return true;
                }
            }
            catch (Exception ex)
            {
                _windowsEventLogger.Log("Exception while starting acquisition: " + ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Stops acquisition (if not already in progress)
        /// </summary>
        public void StopAcquisition()
        {
            if (!IsAcquiring) return;

            _scintiSpecDriver.StopAcquisitionOnPort(_portNumber);
            InnerHasAcquisitionCompleted = false;
        }

        /// <summary>
        /// Gets the spectrum data
        /// </summary>
        /// <returns>Populated SpectrumData instance; empty if the MCA is not connected
        /// or the spectrum count array could not be obtained</returns>
        public SpectrumData GetSpectrumData()
        {
            if (!IsConnected) return SpectrumData.Empty();

            UInt32[] spectrumArray;
            try
            {
                spectrumArray = _scintiSpecDriver.GetSpectrumOnPort(_portNumber);
            }
            catch
            {
                return SpectrumData.Empty();
            }

            return new SpectrumData(_scintiSpecDriver.GetRealTimeInMicrosecondsOnPort(_portNumber),
                _scintiSpecDriver.GetLiveTimeInMicrosecondsOnPort(_portNumber), 
                _scintiSpecDriver.GetDeadTimeInMicrosecondsOnPort(_portNumber),
                spectrumArray);
        }

        /// <summary>
        /// Clears the spectrum and any preset limits
        /// </summary>
        public void ClearSpectrumAndPresets()
        {
            _scintiSpecDriver.ClearSpectrumAndPresetsOnPort(_portNumber);
        }

        /// <summary>
        /// Saves the multi-channel analyzer configuration using the hardware configuration manager
        /// </summary>
        public void SaveConfiguration()
        {
            _hardwareConfigurationManager.SaveConfiguration();
        }

        /// <summary>
        /// Resets the multi-channel analyzer to a default configuration based
        /// on the hardware configuration manager's defaults
        /// </summary>
        public void ResetToDefaultConfiguration()
        {
            _hardwareConfigurationManager.ResetToDefaultConfiguration();
            SetHardwareSettingsBasedOnCurrentConfiguration();
        }
        
        /// <summary>
        /// Shuts down the multi-channel analyzer
        /// </summary>
        public void ShutDown()
        {
            _scintiSpecDriver.StopAcquisitionOnPort(_portNumber);
            _scintiSpecDriver.ShutDownOnPort(_portNumber);
        }

        #endregion

        #region Protected / internal

        internal IScintiSpecDriver Driver { get { return _scintiSpecDriver; } }

        internal IMultiChannelAnalyzerSpecification Specification { get { return _multiChannelAnalyzerSpecification; } }

        internal IThreadWrapper ThreadWrapper { get { return _threadWrapper; } }

        internal IWindowsEventLogger Logger { get { return _windowsEventLogger; } }

        internal IHardwareConfigurationManager HardwareConfigurationManager { get { return _hardwareConfigurationManager; } }

        protected bool InnerHasAcquisitionCompleted { get; set; }

        #endregion

        #region Protected helpers

        protected void MonitorAcquisition()
        {
            _threadWrapper.Sleep(OneSecond);

            try
            {
                var isStillAcquiring = _scintiSpecDriver.IsAcquiringOnPort(_portNumber);
                if (isStillAcquiring)
                    RaiseAcquisitionStarted();
                while (isStillAcquiring)
                {
                    _threadWrapper.Sleep(OneSecond);
                    isStillAcquiring = _scintiSpecDriver.IsAcquiringOnPort(_portNumber);
                }

                if (!_scintiSpecDriver.IsConnectedOnPort(_portNumber))
                {
                    _windowsEventLogger.Log("MCA disconnected while monitoring acquisition");
                    return;
                }

                if (HasAcquisitionCompleted)
                    RaiseAcquisitionCompleted();
                else
                    RaiseAcquisitionStopped();
            }
            catch (Exception ex)
            {
                _windowsEventLogger.Log("Exception while monitoring acquisition: " + ex.Message);
            }
        }

        #endregion

        #region Private helpers

        // ReSharper disable UnusedParameter.Local
        private static void ThrowIfAnyArgumentIsNull(IScintiSpecDriver scintiSpecDriver,
            IHardwareConfigurationManager hardwareConfigurationManager,
            IMultiChannelAnalyzerSpecification multiChannelAnalyzerSpecification, IWindowsEventLogger windowsEventLogger,
            IThreadWrapper threadWrapper)
        {
            if (scintiSpecDriver == null) throw new ArgumentNullException("scintiSpecDriver");
            if (hardwareConfigurationManager == null) throw new ArgumentNullException("hardwareConfigurationManager");
            if (multiChannelAnalyzerSpecification == null) throw new ArgumentNullException("multiChannelAnalyzerSpecification");
            if (windowsEventLogger == null) throw new ArgumentNullException("windowsEventLogger");
            if (threadWrapper == null) throw new ArgumentNullException("threadWrapper");
        }
        // ReSharper restore UnusedParameter.Local

        private void SetHardwareSettingsBasedOnCurrentConfiguration()
        {
            if (!_scintiSpecDriver.IsConnectedOnPort(_portNumber)) return;

            NoiseLevel = _hardwareConfigurationManager.GetSetting(ScintiSpecSpecification.NoiseLevelSettingKey).GetValue<int>();
            LowerLevelDiscriminator = _hardwareConfigurationManager.GetSetting(ScintiSpecSpecification.LowerLevelDiscriminatorSettingKey).GetValue<int>();
            FineGain = _hardwareConfigurationManager.GetSetting(ScintiSpecSpecification.FineGainSettingKey).GetValue<double>();
            HighVoltage = _hardwareConfigurationManager.GetSetting(ScintiSpecSpecification.HighVoltageSettingKey).GetValue<int>();
        }

        private void ThrowNoiseLevelOutOfBounds(int badValue)
        {
            throw new OutOfBoundsException(_multiChannelAnalyzerSpecification.MinNoiseLevel.ToString(),
                _multiChannelAnalyzerSpecification.MaxNoiseLevel.ToString(), badValue.ToString(),
                "Noise level value is out of bounds");
        }

        private void ThrowLowerLevelDiscriminatorOutOfBounds(int badValue)
        {
            throw new OutOfBoundsException(_multiChannelAnalyzerSpecification.MinLowerLevelDiscriminator.ToString(),
                _multiChannelAnalyzerSpecification.MaxLowerLevelDiscriminator.ToString(), badValue.ToString(),
                "Lower-level discriminator value is out of bounds");
        }

        private void ThrowFineGainOutOfBounds(double badValue)
        {
            throw new OutOfBoundsException(_multiChannelAnalyzerSpecification.MinFineGain.ToString(CultureInfo.InvariantCulture),
                _multiChannelAnalyzerSpecification.MaxFineGain.ToString(CultureInfo.InvariantCulture), badValue.ToString(CultureInfo.InvariantCulture),
                "Fine gain value is out of bounds");
        }

        private void ThrowHighVoltageOutOfBounds(int badValue)
        {
            throw new OutOfBoundsException(_multiChannelAnalyzerSpecification.MinHighVoltage.ToString(),
                _multiChannelAnalyzerSpecification.MaxHighVoltage.ToString(), badValue.ToString(),
                "High voltage value is out of bounds");
        }

        private void RaiseAcquisitionStarted()
        {
            if (AcquisitionStarted != null)
                AcquisitionStarted(this, DateTime.Now);
        }

        private void RaiseAcquisitionCompleted()
        {
            if (AcquisitionCompleted != null)
                AcquisitionCompleted(this, GetSpectrumData());
        }

        private void RaiseAcquisitionStopped()
        {
            if (AcquisitionStopped != null)
                AcquisitionStopped(this, DateTime.Now);
        }

        #endregion
    }
}
