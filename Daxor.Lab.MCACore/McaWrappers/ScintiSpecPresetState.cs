﻿namespace Daxor.Lab.MCACore.McaWrappers
{
    /// <summary>
    /// Represents a preset state for the scintiSPEC multi-channel analyzer
    /// </summary>
    public enum ScintiSpecPresetState
    {
        /// <summary>
        /// Preset has not yet been reached
        /// </summary>
        NotReached = 0,

        /// <summary>
        /// Preset has been reached
        /// </summary>
        Reached = 1,
    }
}
