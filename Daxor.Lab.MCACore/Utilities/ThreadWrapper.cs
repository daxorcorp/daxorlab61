﻿using System;
using System.Threading;
using Daxor.Lab.MCACore.Interfaces;

namespace Daxor.Lab.MCACore.Utilities
{
    /// <summary>
    /// Represents a service that uses System.Threading to start a thread and sleep the current thread
    /// </summary>
    public class ThreadWrapper : IThreadWrapper
    {
        /// <summary>
        /// Starts a new thread that will execute the given action
        /// </summary>
        /// <param name="monitorAction">Action to execute on the thread</param>
        public void Start(Action monitorAction)
        {
            var monitorThread = new Thread(new ThreadStart(monitorAction))
            {
                Priority = ThreadPriority.Highest,
                IsBackground = true,
                Name = "AcquisitionMonitorThread"
            };

            monitorThread.Start();
        }

        /// <summary>
        /// Makes the thread sleep for a given amount of time.
        /// </summary>
        /// <param name="sleepTimeInMilliseconds">Sleep time (in milliseconds)</param>
        public void Sleep(int sleepTimeInMilliseconds)
        {
            Thread.Sleep(sleepTimeInMilliseconds);
        }
    }
}
