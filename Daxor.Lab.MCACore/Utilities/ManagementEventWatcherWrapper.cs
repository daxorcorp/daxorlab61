﻿using System;
using System.Management;
using Daxor.Lab.MCACore.Interfaces;

namespace Daxor.Lab.MCACore.Utilities
{
    /// <summary>
    /// Wraps System.Management.ManagementEventWatcher to facilitate unit testing
    /// </summary>
    public class ManagementEventWatcherWrapper : IManagementEventWatcher
    {
        private readonly ManagementEventWatcher _realEventWatcher;

        /// <summary>
        /// Creates a new ManagementEventWatcherWrapper instance based on the given polling query
        /// </summary>
        /// <param name="query">Query to be used when polling for events</param>
        public ManagementEventWatcherWrapper(EventQuery query)
        {
            _realEventWatcher = new ManagementEventWatcher(query);
            _realEventWatcher.EventArrived += OnEventArrived;

            EventQuery = query;
        }

        /// <summary>
        /// Gets or sets the event query to be used when polling
        /// </summary>
        public EventQuery EventQuery { get; private set; }

        /// <summary>
        /// Raised when an event arrives
        /// </summary>
        public event EventHandler EventArrived;

        /// <summary>
        /// Starts the polling process
        /// </summary>
        public void Start()
        {
            _realEventWatcher.Start();
        }

        /// <summary>
        /// Stops the polling process
        /// </summary>
        public void Stop()
        {
            _realEventWatcher.Stop();
        }

        private void OnEventArrived(object sender, EventArrivedEventArgs eventArgs)
        {
            var handler = EventArrived;
            if (handler != null)
                handler(sender, eventArgs);
        }
    }
}
