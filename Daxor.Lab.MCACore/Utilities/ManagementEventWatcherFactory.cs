﻿using System.Management;
using Daxor.Lab.MCACore.Interfaces;

namespace Daxor.Lab.MCACore.Utilities
{
    /// <summary>
    /// Represents a service that returns a wrapper instance of the ManagementEventWatcher
    /// </summary>
    public class ManagementEventWatcherFactory : IManagementEventWatcherFactory
    {
        /// <summary>
        /// Creates a ManagementEventWatcherWrapper instance based on the given event query
        /// </summary>
        /// <param name="eventQuery">Event query to use when polling</param>
        /// <returns>IManagementEventWatcher instance</returns>
        public IManagementEventWatcher CreateWatcherWithQuery(EventQuery eventQuery)
        {
            return new ManagementEventWatcherWrapper(eventQuery);
        }
    }
}
