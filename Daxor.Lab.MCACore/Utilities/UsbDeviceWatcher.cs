﻿using System;
using System.Management;
using Daxor.Lab.MCACore.Interfaces;

namespace Daxor.Lab.MCACore.Utilities
{
    public class UsbDeviceWatcher : IUsbDeviceWatcher
    {
        /// <summary>
        /// Query string to select USB-controller-related events
        /// </summary>
        public const string EventQueryFormatString = "SELECT * FROM __InstanceOperationEvent WITHIN {0} WHERE TargetInstance ISA 'Win32_USBControllerDevice'";

        private readonly IManagementEventWatcherFactory _managementEventWatcherFactory;
        private IManagementEventWatcher _managementEventWatcher;

        #region Public API

        /// <summary>
        /// Creates a new UsbDeviceWatcher instance
        /// </summary>
        /// <param name="managementEventWatcherFactory">Instance of a factory that can create IManagementEventWatcher instances</param>
        /// <exception cref="ArgumentNullException">If the factory instance is null</exception>
        public UsbDeviceWatcher(IManagementEventWatcherFactory managementEventWatcherFactory)
        {
            if (managementEventWatcherFactory == null) throw new ArgumentNullException("managementEventWatcherFactory");
            _managementEventWatcherFactory = managementEventWatcherFactory;
        }

        /// <summary>
        /// Raised when any USB device is connected
        /// </summary>
        public event Action<string> Connected;

        /// <summary>
        /// Raised when any USB device is disconnected
        /// </summary>
        public event Action Disconnected;

        /// <summary>
        /// Starts the USB event-polling process
        /// </summary>
        /// <param name="pollingIntervalInSeconds">Number of seconds between queries</param>
        public void Start(int pollingIntervalInSeconds)
        {
            var eventQuery = new EventQuery(string.Format(EventQueryFormatString, pollingIntervalInSeconds));

            _managementEventWatcher = _managementEventWatcherFactory.CreateWatcherWithQuery(eventQuery);
            _managementEventWatcher.EventArrived += OnEventArrived;
            _managementEventWatcher.Start();
        }

        /// <summary>
        /// Stops the USB event-polling process
        /// </summary>
        public void Stop()
        {
            if (_managementEventWatcher != null)
                _managementEventWatcher.Stop();
        }

        #endregion

        #region Helpers

        private void RaiseConnected(string manufacturer)
        {
            if (Connected != null)
                Connected(manufacturer);
        }

        private void RaiseDisconnected()
        {
            if (Disconnected != null)
                Disconnected();
        }

        private void OnEventArrived(object sender, EventArgs eventArgs)
        {
            using (var managementObject = GetDependentManagementObject(eventArgs))
            {
                try
                {
                    if (!GetDeviceId(managementObject).Contains(@"USB")) return;

                    var manufacturer = GetManufacturer(managementObject);
                    RaiseConnected(manufacturer);
                }
                catch (ManagementException)
                {
                    RaiseDisconnected();
                }
            }
        }

        protected virtual ManagementObject GetDependentManagementObject(EventArgs eventArgs)
        {
            var eventArrivedArgs = eventArgs as EventArrivedEventArgs;

            // ReSharper disable once PossibleNullReferenceException
            var managementBaseObject = (ManagementBaseObject)eventArrivedArgs.NewEvent["TargetInstance"];

            return new ManagementObject(managementBaseObject["Dependent"].ToString());
        }

        protected virtual string GetDeviceId(ManagementObject managementObject)
        {
            return managementObject.GetPropertyValue("DeviceID").ToString();
        }

        protected virtual string GetManufacturer(ManagementObject managementObject)
        {
            return managementObject.GetPropertyValue("Manufacturer").ToString();
        }

        #endregion
    }
}
