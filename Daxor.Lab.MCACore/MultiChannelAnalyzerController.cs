﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.MCACore.Interfaces;

namespace Daxor.Lab.MCACore
{
    /// <summary>
    /// A service that has access to one or more multi-channel analyzers, and surfaces events 
    /// that happen concerning those MCAs
    /// </summary>
    /// <remarks>
    /// Upon construction, the controller attempts to find connected MCAs. New devices will
    /// not be added after the service is initialized. That is, a new MCA cannot be connected
    /// and added to the pool; only *reconnection* of previously connected MCAs is supported.
    /// </remarks>
    public class MultiChannelAnalyzerController : IMultiChannelAnalyzerController, IDisposable
    {
        private readonly IUsbDeviceWatcher _usbDeviceWatcher;
        private readonly IMultiChannelAnalyzerBuilder _mcaBuilder;
        private readonly IWindowsEventLogger _windowsEventLogger;
        private readonly bool _failIfNoConnectedDevices;
        private ConcurrentDictionary<string, IMultiChannelAnalyzer> _connectedMcas;
        private ConcurrentDictionary<string, IMultiChannelAnalyzer> _disconnectedMcas;
        private ConcurrentDictionary<IMultiChannelAnalyzer, string> _mcaIds;

        #region Public API

        /// <summary>
        /// Constructs a new MultiChannelAnalyzerController instance by initializing connections
        /// with known multi-channel analyzers
        /// </summary>
        /// <param name="usbDeviceWatcher">USB device watcher for monitoring disconnects and reconnects</param>
        /// <param name="mcaBuilder">Builder that can create vendor-specific IMultiChannelAnalyzer instances</param>
        /// <param name="windowsEventLogger">Windows logger for the MCA service</param>
        /// <param name="failIfNoConnectedDevices">Whether or not the constructor should fail if
        /// there are no connected devices (true by default)</param>
        /// <exception cref="ArgumentNullException">If any argument is null</exception>
        /// <exception cref="Exception">If the service could not be initialized, or if there
        /// are no available connected MCAs</exception>
        public MultiChannelAnalyzerController(IUsbDeviceWatcher usbDeviceWatcher, IMultiChannelAnalyzerBuilder mcaBuilder, 
            IWindowsEventLogger windowsEventLogger, bool failIfNoConnectedDevices = true)
        {
            if (usbDeviceWatcher == null) throw new ArgumentNullException("usbDeviceWatcher");
            if (mcaBuilder == null) throw new ArgumentNullException("mcaBuilder");
            if (windowsEventLogger == null) throw new ArgumentNullException("windowsEventLogger");

            _usbDeviceWatcher = usbDeviceWatcher;
            _mcaBuilder = mcaBuilder;
            _windowsEventLogger = windowsEventLogger;
            _failIfNoConnectedDevices = failIfNoConnectedDevices;
            
            Initialize();
        }

        /// <summary>
        /// Finalizes the controller by using IDisposable.Dispose()
        /// </summary>
        ~MultiChannelAnalyzerController()
        {
            Dispose();
        }

        /// <summary>
        /// Occurs when a multi-channel analyzer is connected to the system
        /// </summary>
        public event Action<string> MultiChannelAnalyzerConnected;

        /// <summary>
        /// Occurs when a multi-channel analyzer is disconnected from the system
        /// </summary>
        public event Action<string> MultiChannelAnalyzerDisconnected;

        /// <summary>
        /// Occurs when acquisition has completed on a specific multi-channel analyzer
        /// </summary>
        public event Action<string, SpectrumData> AcquisitionCompleted;

        /// <summary>
        /// Occurs when acquisition has started on a specific multi-channel analyzer
        /// </summary>
        public event Action<string, DateTime> AcquisitionStarted;

        /// <summary>
        /// Occurs when acquisition has stopped on a specific multi-channel analyzer
        /// </summary>
        public event Action<string, DateTime> AcquisitionStopped;

        /// <summary>
        /// Gets the IMultiChannelAnalyzer instance corresponding to the given identifier
        /// </summary>
        /// <param name="id">Identifier of the desired MCA instance</param>
        /// <returns>Instance corresponding to the given identifier</returns>
        /// <exception cref="KeyNotFoundException">If there is no connected or disconnected
        /// MCA with the given identifier</exception>
        public IMultiChannelAnalyzer GetMultiChannelAnalyzerWithId(string id)
        {
            _windowsEventLogger.Log("MCA '" + id + "' was requested");

            if (_connectedMcas.ContainsKey(id)) return _connectedMcas[id];
            if (_disconnectedMcas.ContainsKey(id)) return _disconnectedMcas[id];

            throw new KeyNotFoundException("Unable to find MCA by ID '" + id + "'");
        }

        /// <summary>
        /// Gets the list of identifiers for all known multi-channel analyzers,
        /// connected or disconnected
        /// </summary>
        /// <returns>List of identifiers for all MCAs; empty list if no MCAs</returns>
        public List<string> GetAllMultiChannelAnalyzerIds()
        {
            _windowsEventLogger.Log("List of all MCA IDs was requested");

            var idList = new List<string>();
            idList.AddRange(_connectedMcas.Keys);
            idList.AddRange(_disconnectedMcas.Keys);

            return idList;
        }

        /// <summary>
        /// Invokes the save operation for the configuration files for each 
        /// multi-channel analyzer
        /// </summary>
        public void SaveConfigurationsForAllMultiChannelAnalyzers()
        {
            _windowsEventLogger.Log("Saving configurations for all MCAs");

            foreach (var mca in _connectedMcas.Values) mca.SaveConfiguration();
            foreach (var mca in _disconnectedMcas.Values) mca.SaveConfiguration();
        }

        /// <summary>
        /// Invokes the reset operation on the configuration file for a given
        /// multi-channel analyzer
        /// </summary>
        /// <param name="id">Identifier of the MCA to have its configuration 
        /// file reset</param>
        public void ResetMultiChannelAnalyzerToDefaultConfiguration(string id)
        {
            _windowsEventLogger.Log("Resetting MCA '" + id + "' to its default configuration");

            var mca = GetMultiChannelAnalyzerWithId(id);
            mca.ResetToDefaultConfiguration();
        }

        /// <summary>
        /// Finalizes the controller by shutting down MCAs and the USB device watcher, and
        /// by disconnected from MCA events
        /// </summary>
        public void Dispose()
        {
			if (_connectedMcas != null)
			{
				foreach (var connectedMca in _connectedMcas.Values)
				{
					connectedMca.ShutDown();
					connectedMca.AcquisitionCompleted -= OnMcaAcquisitionCompleted;
					connectedMca.AcquisitionStarted -= OnMcaAcquisitionStarted;
					connectedMca.AcquisitionStopped -= OnMcaAcquisitionStopped;
				}
			}

			if (_disconnectedMcas != null)
			{
				foreach (var disconnectedMca in _disconnectedMcas.Values)
				{
					disconnectedMca.AcquisitionCompleted -= OnMcaAcquisitionCompleted;
					disconnectedMca.AcquisitionStarted -= OnMcaAcquisitionStarted;
					disconnectedMca.AcquisitionStopped -= OnMcaAcquisitionStopped;
				}
			}

			if (_usbDeviceWatcher != null)
			{
				_usbDeviceWatcher.Stop();
				_usbDeviceWatcher.Connected -= OnDeviceWatcherConnected;
				_usbDeviceWatcher.Disconnected -= OnDeviceWatcherDisconnected;
			}

			if (_windowsEventLogger != null) _windowsEventLogger.Log("MCA controller disposed");
        }

        #endregion

        #region Protected helpers

        protected void AddMcaToConnectedCollection(string id, IMultiChannelAnalyzer connectedMca)
        {
            if (!_connectedMcas.TryAdd(id, connectedMca))
                throw new Exception("The MCA '" + id + "' already exists in the connected MCA collection");
        }

        protected IMultiChannelAnalyzer RemoveMcaFromConnectedCollection(string id)
        {
            IMultiChannelAnalyzer removedMca;
            _connectedMcas.TryRemove(id, out removedMca);

            return removedMca;
        }

        protected void AddMcaToDisconnectedCollection(string id, IMultiChannelAnalyzer disconnectedMca)
        {
            if (!_disconnectedMcas.TryAdd(id, disconnectedMca))
                throw new Exception("The MCA '" + id + "' already exists in the disconnected MCA collection");
        }

        protected IMultiChannelAnalyzer RemoveMcaFromDisconnectedCollection(string id)
        {
            IMultiChannelAnalyzer removedMca;
            _disconnectedMcas.TryRemove(id, out removedMca);

            return removedMca;
        }

        protected void RegisterMultiChannelAnalyzerAndId(IMultiChannelAnalyzer multiChannelAnalyzer, string id)
        {
            _mcaIds[multiChannelAnalyzer] = id;
        }

        protected int ConnectedMcaCount() { return _connectedMcas.Count; }

        protected int DisconnectedMcaCount() { return _disconnectedMcas.Count; }

        protected void OnDeviceWatcherConnected(string manufacturer)
        {
            if (!_mcaBuilder.IsRecognizedManufacturer(manufacturer))
                return;

            foreach (var disconnectedMcaId in _disconnectedMcas.Keys)
            {
                if (!_disconnectedMcas[disconnectedMcaId].IsConnected)
                    continue;

                var nowConnectedMca = RemoveMcaFromDisconnectedCollection(disconnectedMcaId);
                AddMcaToConnectedCollection(disconnectedMcaId, nowConnectedMca);
                RaiseConnectedForMultiChannelAnalyzerId(disconnectedMcaId);
                _windowsEventLogger.Log("MCA '" + disconnectedMcaId + "' has been reconnected");
            }
        }

        protected void OnDeviceWatcherDisconnected()
        {
            foreach (var connectedMcaId in _connectedMcas.Keys)
            {
                if (_connectedMcas[connectedMcaId].IsConnected)
                    continue;

                var nowDisconnectedMcaId = RemoveMcaFromConnectedCollection(connectedMcaId);
                AddMcaToDisconnectedCollection(connectedMcaId, nowDisconnectedMcaId);
                RaiseDisconnectedForMultiChannelAnalyzerId(connectedMcaId);
                _windowsEventLogger.Log("MCA '" + connectedMcaId + "' has been disconnected");
            }
        }

        protected void OnMcaAcquisitionCompleted(IMultiChannelAnalyzer senderMca, SpectrumData spectrumData)
        {
            var senderMcaId = _mcaIds[senderMca];
            _windowsEventLogger.Log("MCA '" + senderMcaId + "' completed acquisition");
            RaiseAcquisitionCompleted(senderMcaId, spectrumData);
        }

        protected void OnMcaAcquisitionStarted(IMultiChannelAnalyzer senderMca, DateTime startTime)
        {
            var senderMcaId = _mcaIds[senderMca];
            _windowsEventLogger.Log("MCA '" + senderMcaId + "' started acquisition");
            RaiseAcquisitionStarted(senderMcaId, startTime);
        }

        protected void OnMcaAcquisitionStopped(IMultiChannelAnalyzer senderMca, DateTime startTime)
        {
            var senderMcaId = _mcaIds[senderMca];
            _windowsEventLogger.Log("MCA '" + senderMcaId + "' stopped acquisition");
            RaiseAcquisitionStopped(senderMcaId, startTime);
        }

        protected virtual bool AnyConnectedMultiChannelAnalyzers()
        {
            if (_failIfNoConnectedDevices)
                return !_connectedMcas.IsEmpty;

            return true;
        }
        
        #endregion

        #region Private helpers

        private void Initialize()
        {
            _connectedMcas = new ConcurrentDictionary<string, IMultiChannelAnalyzer>();
            _disconnectedMcas = new ConcurrentDictionary<string, IMultiChannelAnalyzer>();
            _mcaIds = new ConcurrentDictionary<IMultiChannelAnalyzer, string>();
            
            _usbDeviceWatcher.Connected += OnDeviceWatcherConnected;
            _usbDeviceWatcher.Disconnected += OnDeviceWatcherDisconnected;
            _usbDeviceWatcher.Start(1);
            _windowsEventLogger.Log("USB device watcher started");

            try
            {
                AddScintiSpecMultiChannelAnalyzer();
            }
            catch (Exception ex)
            {
                _windowsEventLogger.Log("Unable to initialize the MCA controller: " + ex.Message + "\n" + ex.StackTrace);
                throw;
            }

            if (!AnyConnectedMultiChannelAnalyzers()) 
                throw new Exception("No connected MCAs are available");
        }

        private void AddScintiSpecMultiChannelAnalyzer()
        {
            var scintiSpecMca = _mcaBuilder.BuildScintiSpecMultiChannelAnalyzer();
            var isMcaConnected = scintiSpecMca.IsConnected;
            _windowsEventLogger.Log("scintiSPEC MCA (serial number '" + scintiSpecMca.SerialNumber + "') is connected: " +
                                    isMcaConnected);

            if (!isMcaConnected) return;

            var mcaId = "scintiSPEC" + scintiSpecMca.SerialNumber;
            RegisterMultiChannelAnalyzerAndId(scintiSpecMca, mcaId);
            AddMcaToConnectedCollection(mcaId, scintiSpecMca);
            scintiSpecMca.AcquisitionCompleted += OnMcaAcquisitionCompleted;
            scintiSpecMca.AcquisitionStarted += OnMcaAcquisitionStarted;
            scintiSpecMca.AcquisitionStopped += OnMcaAcquisitionStopped;
        }

        private void RaiseConnectedForMultiChannelAnalyzerId(string mcaId)
        {
            if (MultiChannelAnalyzerConnected != null) MultiChannelAnalyzerConnected(mcaId);
        }

        private void RaiseDisconnectedForMultiChannelAnalyzerId(string mcaId)
        {
            if (MultiChannelAnalyzerDisconnected != null) MultiChannelAnalyzerDisconnected(mcaId);
        }

        private void RaiseAcquisitionCompleted(string mcaId, SpectrumData spectrumData)
        {
            if (AcquisitionCompleted != null) AcquisitionCompleted(mcaId, spectrumData);
        }

        private void RaiseAcquisitionStarted(string mcaId, DateTime startTime)
        {
            if (AcquisitionStarted != null) AcquisitionStarted(mcaId, startTime);
        }

        private void RaiseAcquisitionStopped(string mcaId, DateTime startTime)
        {
            if (AcquisitionStopped != null) AcquisitionStopped(mcaId, startTime);
        }

        #endregion
    }
}
