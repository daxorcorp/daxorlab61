﻿using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Daxor.Lab.MCACore.Utilities;

namespace Daxor.Lab.MCACore
{
    /// <summary>
    /// A service that assembles dependencies and configurations needed for
    /// a specific vendor's multi-channel analyzer
    /// </summary>
    public class MultiChannelAnalyzerBuilder : IMultiChannelAnalyzerBuilder
    {
        private const string ScintiSpecManufacturer = "target";

        /// <summary>
        /// Default port number used for a scintiSPEC multi-channel analyzer
        /// </summary>
        public const int DefaultScintiSpecPortNumber = 1;

        /// <summary>
        /// Folder containing the configuration files
        /// </summary>
        public const string ConfigurationFileFolder = @"c:\programdata\daxor\lab\config\";

        /// <summary>
        /// Default ICx-brand multi-channel analyzer configuration
        /// </summary>
        public const string DefaultIcxMcaConfigurationFilePath = ConfigurationFileFolder + "DefaultIcxMcaConfiguration.xml";

        private readonly IWindowsEventLogger _mcaControllerLogger;
        private readonly IWindowsEventLogger _hardwareConfigurationManagerLogger;

        /// <summary>
        /// Creates a new MultiChannelAnalyzerBuilder instance
        /// </summary>
        /// <param name="mcaControllerLogger">Windows event logger used by the MCA controller</param>
        /// <param name="hardwareConfigurationManagerLogger">Windows event logger used by the hardware configuration manager</param>
        /// <exception cref="ArgumentNullException">If either argument is null</exception>
        public MultiChannelAnalyzerBuilder(IWindowsEventLogger mcaControllerLogger, IWindowsEventLogger hardwareConfigurationManagerLogger)
        {
            if (mcaControllerLogger == null) throw new ArgumentNullException("mcaControllerLogger");
            if (hardwareConfigurationManagerLogger == null) throw new ArgumentNullException("hardwareConfigurationManagerLogger");

            _mcaControllerLogger = mcaControllerLogger;
            _hardwareConfigurationManagerLogger = hardwareConfigurationManagerLogger;
        }

        /// <summary>
        /// Builds a IMultiChannelAnalyzer instance capable of communicating with 
        /// a scintiSPEC multi-channel analyzer
        /// </summary>
        /// <remarks>
        /// This method assembles all of the dependencies and configuration items needed
        /// for the underlying concrete class.
        /// </remarks>
        /// <returns>Instance that can communicate with a scintiSPEC MCA</returns>
        public IMultiChannelAnalyzer BuildScintiSpecMultiChannelAnalyzer()
        {
            // NOTE: If in the future multiple scintiSPEC MCAs are to be used, note that per
            // the vendor documentation, port numbers start at 1 (one), and that the
            // highest serial number gets the lowest port number.

            var mcaSpecification = new ScintiSpecSpecification(DefaultScintiSpecPortNumber);
            var scintiSpecDriver = CreateScintiSpecDriver();
            var serialNumber = scintiSpecDriver.GetSerialNumberOnPort(DefaultScintiSpecPortNumber);
            var mcaIdentifier = "scintiSPEC" + serialNumber;
            var configurationRepository = new HardwareConfigurationRepository(ConfigurationFileFolder + mcaIdentifier + ".xml", DefaultIcxMcaConfigurationFilePath);
            var hardwareConfigurationManager = CreateHardwareConfigurationManager(configurationRepository);
            hardwareConfigurationManager.GetSetting(ScintiSpecSpecification.SerialNumberKey).SetValue((int) serialNumber);

            return new ScintiSpecMultiChannelAnalyzer(scintiSpecDriver, hardwareConfigurationManager, mcaSpecification, _mcaControllerLogger, new ThreadWrapper());
        }

        /// <summary>
        /// Determines if the given manufacturer has a multi-channel analyzer instance
        /// that can be built by this class
        /// </summary>
        /// <param name="manufacturer">Name of the manufacturer</param>
        /// <returns>True if a corresponding MCA can be built; false otherwise</returns>
        /// <remarks>Case does not matter</remarks>
        /// <exception cref="ArgumentNullException">If the manufacturer string is null</exception>
        public bool IsRecognizedManufacturer(string manufacturer)
        {
            if (manufacturer == null) throw new ArgumentNullException("manufacturer");

            return manufacturer.StartsWith(ScintiSpecManufacturer, StringComparison.InvariantCultureIgnoreCase);
        }

        #region Protected virtual

        protected virtual IScintiSpecDriver CreateScintiSpecDriver()
        {
            return new ScintiSpecDriver(new ScintiSpecDllWrapper());
        }

        protected virtual IHardwareConfigurationManager CreateHardwareConfigurationManager(IHardwareConfigurationRepository hardwareConfigurationRepository)
        {
            return new HardwareConfigurationManager.HardwareConfigurationManager(_hardwareConfigurationManagerLogger, hardwareConfigurationRepository);
        }

        #endregion
    }
}
