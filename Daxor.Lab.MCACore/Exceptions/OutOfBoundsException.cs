﻿namespace Daxor.Lab.MCACore.Exceptions
{
    /// <summary>
    /// Exception that is thrown when a configuration value is out of bounds
    /// </summary>
    public class OutOfBoundsException : System.Exception
    {
        /// <summary>
        /// Creates a new OutOfBoundsException instance based on the given values
        /// </summary>
        /// <param name="minValue">Minimum value allowed</param>
        /// <param name="maxValue">Maximum value allowed</param>
        /// <param name="outOfBoundsValue">Value that triggered the exception</param>
        /// <param name="message">Additional information</param>
        public OutOfBoundsException(string minValue, string maxValue, string outOfBoundsValue, string message)
        {
            MinValue = minValue;
            MaxValue = maxValue;
            OutOfBoundsValue = outOfBoundsValue;
            Message = message;
        }

        /// <summary>
        /// Gets or sets the minimum value allowed
        /// </summary>
        public string MinValue { get; set; }

        /// <summary>
        /// Gets or sets the maximum value allowed
        /// </summary>
        public string MaxValue { get; set; }

        /// <summary>
        /// Gets or sets the value that triggered the exception
        /// </summary>
        public string OutOfBoundsValue { get; set; }

        /// <summary>
        /// Gets or sets the additional information message
        /// </summary>
        public new string Message { get; set; }
    }
}
