﻿using System;
using Daxor.Lab.MCACore.McaWrappers;

namespace Daxor.Lab.MCACore.Interfaces
{
    /// <summary>
    /// Defines the behavior of a Target/ICx scintiSPEC multi-channel analyzer
    /// </summary>
    public interface IScintiSpecDriver
    {
        /// <summary>
        /// Starts acquisition using the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>True if acquisition started successfully; false otherwise</returns>
        bool StartAcquisitionOnPort(Int64 portNumber);

        /// <summary>
        /// Stops acquisition using the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        void StopAcquisitionOnPort(Int64 portNumber);

        /// <summary>
        /// Determines whether or not the MCA at the given port is currently acquiring
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>True if acquisition is in progress; false otherwise</returns>
        bool IsAcquiringOnPort(Int64 portNumber);

        /// <summary>
        /// Clears the active spectrum and preset times/counts for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        void ClearSpectrumAndPresetsOnPort(Int64 portNumber);

        /// <summary>
        /// Sets the fine gain for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="fineGain">Fine gain</param>
        void SetFineGainOnPort(Int64 portNumber, double fineGain);

        /// <summary>
        /// Gets the fine gain of the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Fine gain of the desired MCA</returns>
        double GetFineGainOnPort(Int64 portNumber);

        /// <summary>
        /// Sets the high voltage of the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="highVoltageInVolts">High voltage (in volts)</param>
        void SetHighVoltageOnPort(Int64 portNumber, int highVoltageInVolts);

        /// <summary>
        /// Gets the high voltage of the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>High voltage (in volts)</returns>
        int GetHighVoltageOnPort(Int64 portNumber);
        
        /// <summary>
        /// Gets the analog-to-digital converter (ADC) high voltage of the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Power number</param>
        /// <returns>ADC high voltage (in volts)</returns>
        int GetAnalogToDigitalConverterHighVoltageOnPort(Int64 portNumber);

        /// <summary>
        /// Gets the spectrum (i.e., counts per channel) for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Array of counts for each channel</returns>
        UInt32[] GetSpectrumOnPort(Int64 portNumber);

        /// <summary>
        /// Gets the spectrum length (i.e., number of channels) for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Number of channels</returns>
        int GetSpectrumLengthOnPort(Int64 portNumber);

        /// <summary>
        /// Sets the noise level for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="noiseLevel">Noise level (channel)</param>
        void SetNoiseLevelOnPort(Int64 portNumber, int noiseLevel);

        /// <summary>
        /// Gets the noise level for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Noise level (channel)</returns>
        int GetNoiseLevelOnPort(Int64 portNumber);

        /// <summary>
        /// Gets the lower-level discriminator (LLD) for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>LLD (channel)</returns>
        int GetLowerLevelDiscriminatorOnPort(Int64 portNumber);

        /// <summary>
        /// Sets the lower-level discriminator (LLD) for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="lowerLevelDiscriminatorChannel">LLD (channel)</param>
        void SetLowerLevelDiscriminatorOnPort(Int64 portNumber, int lowerLevelDiscriminatorChannel);

        /// <summary>
        /// Presets the real-time limit for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="realTimeInseconds">Real time (seconds)</param>
        void PresetRealTimeOnPort(Int64 portNumber, long realTimeInseconds);

        /// <summary>
        /// Presets the live-time limit for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="liveTimeInSeconds">Live time (seconds)</param>
        void PresetLiveTimeOnPort(Int64 portNumber, long liveTimeInSeconds);

        /// <summary>
        /// Presets the gross-counts limit in the region of interest for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="grossCounts">Gross counts</param>
        void PresetGrossCountsInRegionOfInterestOnPort(Int64 portNumber, long grossCounts);

        /// <summary>
        /// Gets the preset mode (e.g., live time, gross counts in ROI) for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Preset mode</returns>
        ScintiSpecPresetMode GetPresetModeOnPort(Int64 portNumber);

        /// <summary>
        /// Gets the real time for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Real time (in microseconds)</returns>
        Int64 GetRealTimeInMicrosecondsOnPort(Int64 portNumber);

        /// <summary>
        /// Gets the live time for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Live time (in microseconds)</returns>
        Int64 GetLiveTimeInMicrosecondsOnPort(Int64 portNumber);

        /// <summary>
        /// Gets the dead time for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Dead time (in microseconds)</returns>
        Int64 GetDeadTimeInMicrosecondsOnPort(Int64 portNumber);
        
        /// <summary>
        /// Gets the serial number for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Serial number</returns>
        Int64 GetSerialNumberOnPort(Int64 portNumber);

        /// <summary>
        /// Shuts down the communication thread for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        void ShutDownOnPort(Int64 portNumber);

        /// <summary>
        /// Determines if the MCA at the given port is connected
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>True if the MCA is connected; false otherwise</returns>
        bool IsConnectedOnPort(Int64 portNumber);

        /// <summary>
        /// Gets the preset-reached state for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Preset state</returns>
        ScintiSpecPresetState GetPresetStateOnPort(Int64 portNumber);

        /// <summary>
        /// Sets the region of interest for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <param name="leftChannel">Left channel marker</param>
        /// <param name="rightChannel">Right channel marker</param>
        void SetRegionOfInterestOnPort(Int64 portNumber, int leftChannel, int rightChannel);

        #region Unused in DaxorLab

        /// <summary>
        /// Starts acquisition on all available MCAs
        /// </summary>
        /// <returns>True if acquisition started successfully; false otherwise</returns>
        bool StartAcquisitionOnAllMcas();

        /// <summary>
        /// Stops acquisition on all available MCAs
        /// </summary>
        void StopAcquisitionOnAllMcas();

        /// <summary>
        /// Gets the index of the spectrum for the MCA at the given port
        /// </summary>
        /// <param name="portNumber">Port number</param>
        /// <returns>Spectrum index</returns>
        Int64 GetSpectrumIndexOnPort(Int64 portNumber);

        /// <summary>
        /// Gets the actual port index (documentation not clear)
        /// </summary>
        /// <returns>Port index</returns>
        Int64 GetActualPortIndex();

        /// <summary>
        /// Gets the total number of allocated spectra
        /// </summary>
        /// <returns>Number of allocated spectra</returns>
        Int64 GetTotalNumberOfAllocatedSpectra();

        /// <summary>
        /// Gets the total number of ports
        /// </summary>
        /// <returns>Number of ports</returns>
        Int64 GetTotalNumberOfPorts();

        /// <summary>
        /// Resets the device driver
        /// </summary>
        void Reset();

        #endregion
    }
}
