﻿using System;
using System.Collections.Generic;
using Daxor.Lab.MCAContracts;

namespace Daxor.Lab.MCACore.Interfaces
{
    /// <summary>
    /// Represents the behavior of a service that has access to one or more 
    /// multi-channel analyzers, and surfaces events that happen concerning 
    /// those MCAs
    /// </summary>
    public interface IMultiChannelAnalyzerController
    {
        /// <summary>
        /// Occurs when a multi-channel analyzer is connected to the system
        /// </summary>
        event Action<string> MultiChannelAnalyzerConnected;

        /// <summary>
        /// Occurs when a multi-channel analyzer is disconnected from the system
        /// </summary>
        event Action<string> MultiChannelAnalyzerDisconnected;

        /// <summary>
        /// Occurs when acquisition has completed on a specific multi-channel analyzer
        /// </summary>
        event Action<string, SpectrumData> AcquisitionCompleted;

        /// <summary>
        /// Occurs when acquisition has started on a specific multi-channel analyzer
        /// </summary>
        event Action<string, DateTime> AcquisitionStarted;

        /// <summary>
        /// Occurs when acquisition has stopped on a specific multi-channel analyzer
        /// </summary>
        event Action<string, DateTime> AcquisitionStopped;

        /// <summary>
        /// Gets the IMultiChannelAnalyzer instance corresponding to the given identifier
        /// </summary>
        /// <param name="id">Identifier of the desired MCA instance</param>
        /// <returns>Instance corresponding to the given identifier</returns>
        IMultiChannelAnalyzer GetMultiChannelAnalyzerWithId(string id);

        /// <summary>
        /// Gets the list of identifiers for all known multi-channel analyzers,
        /// connected or disconnected
        /// </summary>
        /// <returns>List of identifiers for all MCAs</returns>
        List<string> GetAllMultiChannelAnalyzerIds();

        /// <summary>
        /// Invokes the save operation for the configuration files for each 
        /// multi-channel analyzer
        /// </summary>
        void SaveConfigurationsForAllMultiChannelAnalyzers();

        /// <summary>
        /// Invokes the reset operation on the configuration file for a given
        /// multi-channel analyzer
        /// </summary>
        /// <param name="id">Identifier of the MCA to have its configuration 
        /// file reset</param>
        void ResetMultiChannelAnalyzerToDefaultConfiguration(string id);
    }
}
