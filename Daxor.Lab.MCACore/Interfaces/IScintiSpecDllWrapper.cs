﻿using System;

namespace Daxor.Lab.MCACore.Interfaces
{
    /// <summary>
    /// Boolean-based return type that also indicates whether an exception would have been thrown
    /// </summary>
    public struct ScintiSpecBool
    {
        public bool ReturnValue;
        public bool ShouldThrowException;
    }

    /// <summary>
    /// Void-based return type that indicates whether an exception would have been thrown
    /// </summary>
    public struct ScintiSpecVoid
    {
        public bool ShouldThrowException;
    }

    /// <summary>
    /// Defines the behavior of a service that communicates with the scintiSPEC native DLL
    /// </summary>
    public interface IScintiSpecDllWrapper
    {
        /// <summary>
        /// Invokes a software interrupt on the scintiSPEC native DLL using the given register values/references
        /// </summary>
        /// <param name="basePointer">Base pointer register value</param>
        /// <param name="destinationIndex">Destination index register value</param>
        /// <param name="sourceIndex">Source index register value</param>
        /// <param name="dataSegment">Data segment register value</param>
        /// <param name="extraSegment">Extra segment register value</param>
        /// <param name="dataRegister">Data register value</param>
        /// <param name="counterRegister">Counter register value</param>
        /// <param name="baseRegister">Base register value</param>
        /// <param name="accumulatorRegister">Accumulator register value</param>
        /// <param name="instructionPointer">Instruction pointer register value</param>
        /// <param name="codeSegment">Code segment register value</param>
        /// <param name="flags">Flags value</param>
        ScintiSpecVoid InvokeReturningVoid(Int64 basePointer, Int64 destinationIndex, Int64 sourceIndex, Int64 dataSegment,
            Int64 extraSegment, Int64 dataRegister, Int64 counterRegister, Int64 baseRegister, Int64 accumulatorRegister,
            Int64 instructionPointer, Int64 codeSegment, byte flags);

        /// <summary>
        /// Invokes a software interrupt on the scintiSPEC native DLL using the given register values/references
        /// </summary>
        /// <param name="basePointer">Base pointer register value</param>
        /// <param name="destinationIndex">Destination index register value</param>
        /// <param name="sourceIndex">Source index register value</param>
        /// <param name="dataSegment">Data segment register value</param>
        /// <param name="extraSegment">Extra segment register value</param>
        /// <param name="dataRegister">Data register value</param>
        /// <param name="counterRegister">Counter register value</param>
        /// <param name="baseRegister">Base register value</param>
        /// <param name="accumulatorRegister">Accumulator register value</param>
        /// <param name="instructionPointer">Instruction pointer register value</param>
        /// <param name="codeSegment">Code segment register value</param>
        /// <param name="flags">Flags value</param>
        /// <returns>Integer result of the operation</returns>
        Int64 InvokeReturningInt(Int64 basePointer, Int64 destinationIndex, Int64 sourceIndex, Int64 dataSegment,
            Int64 extraSegment, Int64 dataRegister, Int64 counterRegister, Int64 baseRegister, Int64 accumulatorRegister,
            Int64 instructionPointer, Int64 codeSegment, byte flags);

        /// <summary>
        /// Invokes a software interrupt on the scintiSPEC native DLL using the given register values/references
        /// </summary>
        /// <param name="basePointer">Base pointer register value</param>
        /// <param name="destinationIndex">Destination index register value</param>
        /// <param name="sourceIndex">Source index register value</param>
        /// <param name="dataSegment">Data segment register value</param>
        /// <param name="extraSegment">Extra segment register value</param>
        /// <param name="dataRegister">Data register value</param>
        /// <param name="counterRegister">Counter register value</param>
        /// <param name="baseRegister">Base register value</param>
        /// <param name="accumulatorRegister">Accumulator register value</param>
        /// <param name="instructionPointer">Instruction pointer register value</param>
        /// <param name="codeSegment">Code segment register value</param>
        /// <param name="flags">Flags value</param>
        /// <returns>Boolean result of the operation</returns>
        ScintiSpecBool InvokeReturningBool(Int64 basePointer, Int64 destinationIndex, Int64 sourceIndex, Int64 dataSegment,
            Int64 extraSegment, Int64 dataRegister, Int64 counterRegister, Int64 baseRegister, Int64 accumulatorRegister,
            Int64 instructionPointer, Int64 codeSegment, byte flags);

        /// <summary>
        /// Invokes a software interrupt on the scintiSPEC native DLL using the given register values/references
        /// </summary>
        /// <param name="basePointer">Base pointer register value</param>
        /// <param name="destinationIndex">Destination index register value</param>
        /// <param name="sourceIndex">Source index register value</param>
        /// <param name="dataSegment">Data segment register value</param>
        /// <param name="extraSegment">Extra segment register value</param>
        /// <param name="dataRegister">Data register value</param>
        /// <param name="baseRegister">Base register value</param>
        /// <param name="accumulatorRegister">Accumulator register value</param>
        /// <param name="instructionPointer">Instruction pointer register value</param>
        /// <param name="codeSegment">Code segment register value</param>
        /// <param name="flags">Flags value</param>
        /// <returns>Array of unsigned integers</returns>
        UInt32[] InvokeReturningIntArray(Int64 basePointer, Int64 destinationIndex, Int64 sourceIndex, Int64 dataSegment,
            Int64 extraSegment, Int64 dataRegister, Int64 baseRegister, Int64 accumulatorRegister,
            Int64 instructionPointer, Int64 codeSegment, byte flags);
    }
}
