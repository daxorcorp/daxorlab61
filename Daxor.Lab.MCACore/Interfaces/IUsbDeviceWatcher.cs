﻿using System;

namespace Daxor.Lab.MCACore.Interfaces
{
    /// <summary>
    /// Defines the behavior of a service that raises events when USB devices are connected
    /// or disconnected from the system
    /// </summary>
    public interface IUsbDeviceWatcher
    {
        /// <summary>
        /// Occurs when any USB device is connected
        /// </summary>
        event Action<string> Connected;

        /// <summary>
        /// Occurs when any USB device is disconnected
        /// </summary>
        event Action Disconnected;

        /// <summary>
        /// Starts the USB event-polling process
        /// </summary>
        /// <param name="pollingIntervalInSeconds">Number of seconds between queries</param>
        void Start(int pollingIntervalInSeconds);

        /// <summary>
        /// Stops the event-polling process
        /// </summary>
        void Stop();
    }
}
