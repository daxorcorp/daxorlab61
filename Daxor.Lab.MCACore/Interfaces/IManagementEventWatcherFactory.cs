﻿using System.Management;

namespace Daxor.Lab.MCACore.Interfaces
{
    /// <summary>
    /// Represents the behavior of a service that can create ManagementEventWatcher instances
    /// </summary>
    public interface IManagementEventWatcherFactory
    {
        /// <summary>
        /// Creates an IManagementEventWatcher instance based on the given event query
        /// </summary>
        /// <param name="eventQuery">Event query to use when polling</param>
        /// <returns>IManagementEventWatcher instance</returns>
        IManagementEventWatcher CreateWatcherWithQuery(EventQuery eventQuery);
    }
}
