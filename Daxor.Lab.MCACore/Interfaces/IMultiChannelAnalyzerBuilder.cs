﻿namespace Daxor.Lab.MCACore.Interfaces
{
    /// <summary>
    /// Represents the behavior of a service that can build fully-constructed
    /// instances of IMultiChannelAnalyzer
    /// </summary>
    public interface IMultiChannelAnalyzerBuilder
    {
        /// <summary>
        /// Builds a IMultiChannelAnalyzer instance capable of communicating with 
        /// a scintiSPEC multi-channel analyzer
        /// </summary>
        /// <returns>Instance that can communicate with a scintiSPEC MCA</returns>
        IMultiChannelAnalyzer BuildScintiSpecMultiChannelAnalyzer();

        /// <summary>
        /// Determines if the given manufacturer has a multi-channel analyzer instance
        /// that can be built by this class
        /// </summary>
        /// <param name="manufacturer">Name of the manufacturer</param>
        /// <returns>True if a corresponding MCA can be built; false otherwise</returns>
        /// <remarks>Case does not matter</remarks>
        bool IsRecognizedManufacturer(string manufacturer);
    }
}
