﻿namespace Daxor.Lab.MCACore.Interfaces
{
    /// <summary>
    /// Represents a property bag of bounds specifications and information about a particular multi-channel analyzer
    /// </summary>
    public interface IMultiChannelAnalyzerSpecification
    {
        /// <summary>
        /// Gets the port number for communicating with the MCA
        /// </summary>
        int PortNumber { get; }

        /// <summary>
        /// Gets the minimum allowable fine gain for the MCA
        /// </summary>
        double MinFineGain { get; }

        /// <summary>
        /// Gets the maximum allowable fine gain for the MCA
        /// </summary>
        double MaxFineGain { get; }

        /// <summary>
        /// Gets the minimum allowable high voltage for the MCA
        /// </summary>
        int MinHighVoltage { get; }

        /// <summary>
        /// Gets the maximum allowable high voltage for the MCA
        /// </summary>
        int MaxHighVoltage { get; }

        /// <summary>
        /// Gets the minimum allowable lower-level discriminator for the MCA
        /// </summary>
        int MinLowerLevelDiscriminator { get; }

        /// <summary>
        /// Gets the maximum allowable lower-level discriminator for the MCA
        /// </summary>
        int MaxLowerLevelDiscriminator { get; }

        /// <summary>
        /// Gets the minimum allowable noise level for the MCA
        /// </summary>
        int MinNoiseLevel { get; }

        /// <summary>
        /// Gets the maximum allowable noise level for the MCA
        /// </summary>
        int MaxNoiseLevel { get; }
    }
}
