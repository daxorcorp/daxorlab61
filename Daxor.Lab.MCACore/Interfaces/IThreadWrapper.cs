﻿using System;

namespace Daxor.Lab.MCACore.Interfaces
{
    /// <summary>
    /// Defines the behavior of a basic wrapper around threading
    /// </summary>
    public interface IThreadWrapper
    {
        /// <summary>
        /// Starts a new thread that will execute the given action
        /// </summary>
        /// <param name="monitorAction">Action to execute on the thread</param>
        void Start(Action monitorAction);

        /// <summary>
        /// Makes the thread sleep for a given amount of time.
        /// </summary>
        /// <param name="sleepTimeInMilliseconds">Sleep time (in milliseconds)</param>
        void Sleep(int sleepTimeInMilliseconds);
    }
}
