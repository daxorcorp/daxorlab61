﻿using System;
using Daxor.Lab.MCAContracts;

namespace Daxor.Lab.MCACore.Interfaces
{
    /// <summary>
    /// Represents the behavior of a multi-channel analyzer
    /// </summary>
    public interface IMultiChannelAnalyzer
    {
        /// <summary>
        /// Occurs when acquisition has started
        /// </summary>
        event Action<IMultiChannelAnalyzer, DateTime> AcquisitionStarted;

        /// <summary>
        /// Occurs when acquisition has completed
        /// </summary>
        event Action<IMultiChannelAnalyzer, SpectrumData> AcquisitionCompleted;

        /// <summary>
        /// Occurs when acquisition has stopped (but not completed)
        /// </summary>
        event Action<IMultiChannelAnalyzer, DateTime> AcquisitionStopped;

        /// <summary>
        /// Gets or sets the noise level
        /// </summary>
        int NoiseLevel { get; set; }

        /// <summary>
        /// Gets or sets the lower-level discriminator
        /// </summary>
        int LowerLevelDiscriminator { get; set; }

        /// <summary>
        /// Gets or sets the fine gain
        /// </summary>
        double FineGain { get; set; }
        
        /// <summary>
        /// Gets or sets the high voltage
        /// </summary>
        int HighVoltage { get; set; }

        /// <summary>
        /// Gets whether or not the multi-channel analyzer is connected
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// Gets the serial number
        /// </summary>
        int SerialNumber { get; }

        /// <summary>
        /// Gets the spectrum length (i.e., number of channels)
        /// </summary>
        int SpectrumLength { get; }

        /// <summary>
        /// Gets the high voltage for the analog-to-digital converter
        /// </summary>
        int AnalogToDigitalConverterHighVoltage { get; }

        /// <summary>
        /// Gets whether or not acquisition is in progress
        /// </summary>
        bool IsAcquiring { get; }

        /// <summary>
        /// Gets whether or not the real-time has been preset
        /// </summary>
        bool IsRealTimePreset { get; }

        /// <summary>
        /// Gets whether or not the live-time has been preset
        /// </summary>
        bool IsLiveTimePreset { get; }

        /// <summary>
        /// Gets whether or not the gross counts in the region-of-interest have been preset
        /// </summary>
        bool IsGrossCountPreset { get; }

        /// <summary>
        /// Gets whether or not acquisition has completed
        /// </summary>
        bool HasAcquisitionCompleted { get; }

        /// <summary>
        /// Presets the live time
        /// </summary>
        /// <param name="liveTimeInSeconds">Live time preset (in seconds)</param>
        void PresetLiveTime(Int64 liveTimeInSeconds);

        /// <summary>
        /// Presets the real time
        /// </summary>
        /// <param name="realTimeInSeconds">Real time preset (in seconds)</param>
        void PresetRealTime(Int64 realTimeInSeconds);

        /// <summary>
        /// Presets the gross counts in the region-of-interest
        /// </summary>
        /// <param name="counts">Count limit</param>
        /// <param name="beginChannel">Beginning (left) channel</param>
        /// <param name="endChannel">Ending (right) channel</param>
        void PresetGrossCountsInRegionOfInterest(Int64 counts, int beginChannel, int endChannel);

        /// <summary>
        /// Starts acquisition
        /// </summary>
        /// <returns>True if acquisition started; false otherwise</returns>
        bool StartAcquisition();

        /// <summary>
        /// Stops acquisition
        /// </summary>
        void StopAcquisition();

        /// <summary>
        /// Gets the spectrum data
        /// </summary>
        /// <returns>Populated SpectrumData instance</returns>
        SpectrumData GetSpectrumData();
        
        /// <summary>
        /// Clears the spectrum and any preset limits
        /// </summary>
        void ClearSpectrumAndPresets();

        /// <summary>
        /// Saves the multi-channel analyzer configuration
        /// </summary>
        void SaveConfiguration();

        /// <summary>
        /// Resets the multi-channel analyzer to a default configuration
        /// </summary>
        void ResetToDefaultConfiguration();
        
        /// <summary>
        /// Shuts down the multi-channel analyzer
        /// </summary>
        void ShutDown();
    }
}
