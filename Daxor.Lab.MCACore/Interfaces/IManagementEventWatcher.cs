﻿using System;
using System.Management;

namespace Daxor.Lab.MCACore.Interfaces
{
    /// <summary>
    /// Represents the behavior of a wrapper around System.Management.ManagementEventWatcher
    /// </summary>
    public interface IManagementEventWatcher
    {
        /// <summary>
        /// The query used when polling
        /// </summary>
        EventQuery EventQuery { get; }

        /// <summary>
        /// Raised when an event arrives
        /// </summary>
        event EventHandler EventArrived;

        /// <summary>
        /// Starts the polling process
        /// </summary>
        void Start();

        /// <summary>
        /// Stops the polling process
        /// </summary>
        void Stop();
    }
}
