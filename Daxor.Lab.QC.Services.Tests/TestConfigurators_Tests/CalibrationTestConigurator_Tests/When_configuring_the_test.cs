using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.QC.Services.TestConfigurators;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Services.Tests.TestConfigurators_Tests.CalibrationTestConigurator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_configuring_the_test
    {
        [TestMethod]
        public void Then_the_initial_fine_gain_adjustment_comes_from_the_settings_manager()
        {
            var mockSettingsManger = MockRepository.GenerateMock<ISettingsManager>();
            mockSettingsManger.Expect(m => m.GetSetting<double>(SettingKeys.QcInitialFineGainAdjustment)).Return(0.0);

            var configurator = new CalibrationTestConfigurator(MockRepository.GenerateStub<IDetector>(), null, 
                MockRepository.GenerateStub<IQualityTestDataService>(), mockSettingsManger, MockRepository.GenerateStub<IFineGainAdjustmentService>(), null, null, null);

            configurator.Configure(new QcCalibrationTest(null));

            mockSettingsManger.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
