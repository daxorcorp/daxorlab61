﻿using System;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Services.TestExecutionContext;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.QC.Services.Tests.TestExecutionContext_Tests
{
    class StubQCExecutionContextThatReturnsTheGivenExecutingSample :QualityControlExecutionContext
    {
        public StubQCExecutionContextThatReturnsTheGivenExecutingSample(QcTest test, IDetector detector, ILoggerFacade logger, Action<QcTest> saveTestAction, ISettingsManager settingsManager) 
            : base(test, detector, logger, saveTestAction, settingsManager)
        {
        }

        internal int RunningSampleIndexPassthrough
        {
            get { return RunningSampleIndex; }
            set { RunningSampleIndex = value; }
        }
    }
}
