using System;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Services.TestExecutionContext;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Services.Tests.TestExecutionContext_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_test_is_completed
    {
        [TestMethod]
        public void Then_the_fine_gain_is_changed_if_needed()
        {
            const double initialFineGain = 1.125;
            const double endingFineGain = 1.625;

            var qcTest = new QcCalibrationTest(null);
            var stubDetector = MockRepository.GenerateStub<IDetector>();
            stubDetector.FineGain = initialFineGain;

            var dummyLogger = MockRepository.GenerateStub<ILoggerFacade>();
            var dummySaveTestAction = new Action<QcTest>(test => { });
            var dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var qcContext = new QualityControlExecutionContext(qcTest, stubDetector, dummyLogger, dummySaveTestAction, dummySettingsManager)
            {
                ExecutingTestStatus = TestStatus.Running
            };
            stubDetector.FineGain = endingFineGain;

            qcContext.ExecutingTestStatus = TestStatus.Completed;

            Assert.AreEqual(endingFineGain, stubDetector.FineGain);
        }
    }
    // ReSharper restore InconsistentNaming
}
