using System;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Factories;
using Daxor.Lab.QC.Core.Tests;
using Daxor.Lab.QC.Services.TestExecutionContext;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Services.Tests.TestExecutionContext_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_test_is_aborted
    {
        private QcTest _qcTest;
        private IDetector _stubDetector;
        private ILoggerFacade _dummyLogger;
        private Action<QcTest> _dummySaveTestAction;
        private ISettingsManager _dummySettingsManager;

        [TestInitialize]
        public void TestInitialize()
        {
            _qcTest = QcTestFactory.CreateTest(TestType.Calibration, new QcSchemaProvider(), null);
            _stubDetector = MockRepository.GenerateStub<IDetector>();

            _dummyLogger = MockRepository.GenerateStub<ILoggerFacade>();
            _dummySaveTestAction = test => { };
            _dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();
        }
        
        [TestMethod]
        public void Then_the_fine_gain_is_restored_to_the_initial_value()
        {
            const double initialFineGain = 1.125;
            const double intermediateFineGain = 1.625;

            _stubDetector.FineGain = initialFineGain;

            var qcContext = new QualityControlExecutionContext(_qcTest, _stubDetector, _dummyLogger, _dummySaveTestAction, _dummySettingsManager)
            {
                ExecutingTestStatus = TestStatus.Running
            };

            _stubDetector.FineGain = intermediateFineGain;

            qcContext.ExecutingTestStatus = TestStatus.Aborted;

            Assert.AreEqual(initialFineGain, _stubDetector.FineGain);
        }

        [TestMethod]
        public void And_the_active_sample_is_the_background_sample_then_IsPassed_is_null()
        {
            _qcTest.BuildSamples();

            var qcContext = new StubQCExecutionContextThatReturnsTheGivenExecutingSample(_qcTest, _stubDetector, _dummyLogger, _dummySaveTestAction, _dummySettingsManager)
            {
                ExecutingTestStatus = TestStatus.Running,
                RunningSampleIndexPassthrough = 0
            };

            var background = qcContext.BackgroundSample as QcSample;
            if (background == null) Assert.Fail("Could not find background sample");

            background.IsPassed = false;

            qcContext.ExecutingTestStatus = TestStatus.Aborted;

            Assert.AreEqual(null, (qcContext.BackgroundSample as QcSample).IsPassed);
        }

        [TestMethod]
        public void And_the_active_sample_is_something_other_than_the_background_sample_then_IsPassed_is_null()
        {
            _qcTest.BuildSamples();

            var qcContext = new StubQCExecutionContextThatReturnsTheGivenExecutingSample(_qcTest, _stubDetector, _dummyLogger, _dummySaveTestAction, _dummySettingsManager)
            {
                ExecutingTestStatus = TestStatus.Running,
                RunningSampleIndexPassthrough = 1
            };

            var setupSample = qcContext.GetNextSampleToExecute() as QcSample;
            if (setupSample == null) Assert.Fail("Could not find calibration setup sample");

            setupSample.IsPassed = false;

            qcContext.ExecutingTestStatus = TestStatus.Aborted;

            Assert.AreEqual(null, setupSample.IsPassed);
        }
    }
    // ReSharper restore InconsistentNaming
}
