﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Services.Tests.QCTestController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_saving_the_test
    {
        private const int FiveMinutes = 5;
        private const int TimeTolerance = 500;

        private QcTestController _testController;
        private QcTest _qcTest;

        [TestInitialize]
        public void TestInitialize()
        {
            var dataService = MockRepository.GenerateStub<IQualityTestDataService>();
            dataService.Expect(m => m.GetTestItems()).Return(new List<QcTestItem>().AsQueryable());

            var settingsManager = MockRepository.GenerateStub<ISettingsManager>();

            _testController = new QcTestController(null, null, null, null, null, dataService, null, null, null, null,
                settingsManager, null);

            _qcTest = new QcFullTest(null) { Date = DateTime.Now.Subtract(new TimeSpan(0, 0, FiveMinutes, 0)) };
        }

        [TestMethod]
        public void Then_the_date_of_the_test_is_updated()
        {
            var oldCreationDate = _qcTest.Date;

            _testController.SaveTestCommandExecute(_qcTest);

            Assert.IsTrue(IsDateWithinTolerance(oldCreationDate, _qcTest.Date, FiveMinutes));
        }

        [TestMethod]
        public void Then_the_date_of_the_test_is_set_correctly()
        {
            var oldCreationDate = DateTime.Now;

            _testController.SaveTestCommandExecute(_qcTest);

            Assert.IsTrue(IsDateWithinTolerance(oldCreationDate, _qcTest.Date));
        }

        private static bool IsDateWithinTolerance(DateTime date1, DateTime date2, int minutes = 0)
        {
            var difference = Math.Abs((date1 - date2).TotalMilliseconds) - (minutes*60*1000);
            return difference <= TimeTolerance;
        }
    }
    // ReSharper restore InconsistentNaming
}
