using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.QC.Services.Tests.QCTestController_Tests.Stubs
{
    internal class QcTestControllerThatWiresUpEventHandlers : QcTestController
    {
        public QcTestControllerThatWiresUpEventHandlers(ISettingsManager settingsManager, ITestExecutionController testExecutionController,
            IQualityTestDataService qcDataService, IUnityContainer container, 
            IEventAggregator eventAggregator) : 
                base(null, null, eventAggregator, container, null, qcDataService, null, testExecutionController, null, null, settingsManager, null)
        { 
        }

        public void ListenToExecutionControllerEvents()
        {
            WireUpToExecutionControllerEvents();
        }
    }
}