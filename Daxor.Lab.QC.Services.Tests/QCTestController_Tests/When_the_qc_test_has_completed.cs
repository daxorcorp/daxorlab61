﻿using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Factories;
using Daxor.Lab.QC.Core.Tests;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.QC.Services.Tests.QCTestController_Tests.Stubs;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using TestExecutionEventArgs = Daxor.Lab.Domain.Eventing.TestExecutionEventArgs;

namespace Daxor.Lab.QC.Services.Tests.QCTestController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_QC_test_has_completed
    {
        private ISettingsManager _mockSettingsManager;
        private ITestExecutionController _stubTestExecutionController;
        private IQualityTestDataService _stubQcDataService;
        private IUnityContainer _stubContainer;
        private IEventAggregator _stubEventAggregator;
        
        [TestInitialize]
        public void Initialize()
        {
            _mockSettingsManager = Substitute.For<ISettingsManager>();
            _stubTestExecutionController = Substitute.For<ITestExecutionController>();
            _stubQcDataService = Substitute.For<IQualityTestDataService>();
            _stubContainer = Substitute.For<IUnityContainer>();
            _stubEventAggregator = Substitute.For<IEventAggregator>();

            var dummyTestItems = new List<QcTestItem> {new QcTestItem()};
            _stubQcDataService.GetTestItems().Returns(dummyTestItems.AsQueryable());

            _stubEventAggregator.GetEvent<QCTestDueChanged>().Returns(new QCTestDueChanged());
            _stubEventAggregator.GetEvent<QCTestReEvaluateExecutionCommands>().Returns(new QCTestReEvaluateExecutionCommands());
        }

        [TestMethod]
        public void And_the_standards_has_failed_but_linearity_has_passed_then_linearity_is_not_needed()
        {
            var qcTestController = new QcTestControllerThatWiresUpEventHandlers(_mockSettingsManager, _stubTestExecutionController, _stubQcDataService, _stubContainer, _stubEventAggregator);

            var failingFullQcTest = CreateTestWithFailingStandardA();
            qcTestController.RunningTest = failingFullQcTest;
            qcTestController.ListenToExecutionControllerEvents();

            _stubTestExecutionController.TestCompleted += Raise.EventWith(new object(), new TestExecutionEventArgs(failingFullQcTest, failingFullQcTest.InternalId));

            _mockSettingsManager.Received(1).SetSetting(SettingKeys.QcLastSuccessfulFullQcOrLinearityDate, Arg.Any<DateTime>());
        }

        [TestMethod]
        public void And_only_linearity_has_failed_then_linearity_is_needed()
        {
            var qcTestController = new QcTestControllerThatWiresUpEventHandlers(_mockSettingsManager, _stubTestExecutionController, _stubQcDataService, _stubContainer, _stubEventAggregator);
            
            var failingFullQcTest = CreateTestWithFailingLinearity();
            qcTestController.RunningTest = failingFullQcTest;
            qcTestController.ListenToExecutionControllerEvents();

            _stubTestExecutionController.TestCompleted += Raise.EventWith(new object(), new TestExecutionEventArgs(failingFullQcTest, failingFullQcTest.InternalId));

            _mockSettingsManager.DidNotReceive().SetSetting(SettingKeys.QcLastSuccessfulFullQcOrLinearityDate, Arg.Any<DateTime>());
        }

        private QcTest CreateTestWithFailingLinearity()
        {
            var failingFullQcTest = QcTestFactory.CreateTest(TestType.Full, new QcSchemaProvider(), null);
            failingFullQcTest.BuildSamples();
            failingFullQcTest.CalibrationCurve = new CalibrationCurve(47, 1.0, 2.0);
           
            foreach (var subtest in ((QcFullTest)failingFullQcTest).SubTests)
                foreach (var sample in subtest.Samples)
                    sample.IsPassed = sample.TypeOfSample != SampleType.QC2;
            return failingFullQcTest;
        }

        private QcTest CreateTestWithFailingStandardA()
        {
            var failingFullQcTest = QcTestFactory.CreateTest(TestType.Full, new QcSchemaProvider(), null);
            failingFullQcTest.BuildSamples();
            failingFullQcTest.CalibrationCurve = new CalibrationCurve(47, 1.0, 2.0);
            
            foreach (var subtest in ((QcFullTest) failingFullQcTest).SubTests)
                foreach (var sample in subtest.Samples)
                    sample.IsPassed = sample.TypeOfSample != SampleType.StandardA;
            return failingFullQcTest;
        }
    }

    // ReSharper restore RedundantArgumentName
}