﻿using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.QC.Services.Tests.CalibrationCurveDataService_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_saving_the_calibration_curve
	{
		private ICalibrationCurve _stubCalibrationCurve;
		private ICalibrationCurveDataAccess _mockDataAccess;
		private IEventAggregator _stubEventAggregator;
		private ILoggerFacade _stubLoggerFacade;

		private GetCalibrationCurveRequest _stubEvent;
		private CalibrationCurveRecord _stubRecord;

		// ReSharper disable once NotAccessedField.Local
		private CalibrationCurveDataService _dataService;

		[TestInitialize]
		public void Initialize()
		{
			_mockDataAccess = Substitute.For<ICalibrationCurveDataAccess>();
			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_stubLoggerFacade = Substitute.For<ILoggerFacade>();

			_stubEvent = Substitute.For<GetCalibrationCurveRequest>();

			_stubCalibrationCurve = new CalibrationCurve(1, 2, 3);
			_stubRecord = new CalibrationCurveRecord{CalibrationCurveId = 4, Slope = 5, OffsetValue = 6};

			_stubEventAggregator.GetEvent<GetCalibrationCurveRequest>().Returns(_stubEvent);
			_stubEventAggregator.GetEvent<CalibrationCurveCalculated>().Returns(new CalibrationCurveCalculated());

			_mockDataAccess.SelectCurrent(_stubLoggerFacade).Returns(_stubRecord);
			_mockDataAccess.Insert(Arg.Any<CalibrationCurveRecord>(), _stubLoggerFacade).Returns(_stubRecord);
			_dataService = new CalibrationCurveDataService(_mockDataAccess, _stubEventAggregator, _stubLoggerFacade);
		}

		[TestMethod]
		public void Then_the_calibration_curve_data_access_service_is_used_to_see_if_the_curve_is_already_saved()
		{
			_dataService.SaveCalibrationCurve(_stubCalibrationCurve, _stubLoggerFacade);

			_mockDataAccess.Received(1).SelectCurrent(_stubLoggerFacade);
		}

		[TestMethod]
		public void And_a_record_is_found_then_that_records_curve_ID_is_returned()
		{
			_mockDataAccess.Select(Arg.Any<int>(), _stubLoggerFacade).Returns(new CalibrationCurveRecord { CalibrationCurveId = 7, Slope = 8, OffsetValue = 9 });
			_dataService = new CalibrationCurveDataService(_mockDataAccess, _stubEventAggregator, _stubLoggerFacade);
			
            Assert.AreEqual(7, _dataService.SaveCalibrationCurve(_stubCalibrationCurve, _stubLoggerFacade));
		}

		[TestMethod]
		public void And_a_record_is_not_found_then_a_new_record_is_inserted()
		{
			_dataService.SaveCalibrationCurve(_stubCalibrationCurve, _stubLoggerFacade);

			_mockDataAccess.Received(1).Insert(Arg.Any<CalibrationCurveRecord>(), _stubLoggerFacade);
		}

		[TestMethod]
		public void And_a_record_is_not_found_and_a_new_record_can_not_be_added_then_the_old_curve_ID_is_returned()
		{
			_mockDataAccess.Insert(Arg.Any<CalibrationCurveRecord>(), _stubLoggerFacade).Returns((CalibrationCurveRecord) null);
			_dataService = new CalibrationCurveDataService(_mockDataAccess, _stubEventAggregator, _stubLoggerFacade);

			Assert.AreEqual(1, _dataService.SaveCalibrationCurve(_stubCalibrationCurve, _stubLoggerFacade));
		}

		[TestMethod]
		public void And_a_record_is_not_found_and_a_new_record_is_added_then_the_new_records_curve_ID_is_returned()
		{
			Assert.AreEqual(4, _dataService.SaveCalibrationCurve(_stubCalibrationCurve, _stubLoggerFacade));
		}
	}
	// ReSharper restore InconsistentNaming
}
