﻿using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.QC.Services.Tests.CalibrationCurveDataService_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_constructing_a_calibration_curve_data_service
	{
		[TestMethod]
		public void The_event_aggregator_subscribes_to_the_get_calibration_curve_request_event()
		{
			var stubDataAccess = Substitute.For<ICalibrationCurveDataAccess>();
			stubDataAccess.SelectCurrent(Arg.Any<ILoggerFacade>()).Returns(new CalibrationCurveRecord());
			var mockEventAggregator = Substitute.For<IEventAggregator>();

			var dummyEvent = Substitute.For<GetCalibrationCurveRequest>();

            mockEventAggregator.GetEvent<GetCalibrationCurveRequest>().Returns(dummyEvent);
            mockEventAggregator.GetEvent<CalibrationCurveCalculated>().Returns(new CalibrationCurveCalculated());

		    // ReSharper disable once UnusedVariable
            var dataService = new CalibrationCurveDataService(stubDataAccess, mockEventAggregator, Substitute.For<ILoggerFacade>());

            mockEventAggregator.Received(1).GetEvent<GetCalibrationCurveRequest>();
		}
	}
	// ReSharper restore InconsistentNaming
}
