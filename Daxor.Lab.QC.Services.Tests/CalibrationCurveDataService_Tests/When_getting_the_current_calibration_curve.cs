﻿using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Infrastructure.Events;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.QC.Services.Tests.CalibrationCurveDataService_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_getting_the_current_calibration_curve
	{
		private ICalibrationCurveDataAccess _mockDataAccess;
		private IEventAggregator _stubEventAggregator;
		private ILoggerFacade _stubLoggerFacade;

		private GetCalibrationCurveRequest _stubEvent;

		// ReSharper disable once NotAccessedField.Local
		private CalibrationCurveDataService _dataService;

		[TestInitialize]
		public void Initialize()
		{
			_mockDataAccess = Substitute.For<ICalibrationCurveDataAccess>();
			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_stubLoggerFacade = Substitute.For<ILoggerFacade>();

			_stubEvent = Substitute.For<GetCalibrationCurveRequest>();

			_mockDataAccess.SelectCurrent(_stubLoggerFacade).Returns(new CalibrationCurveRecord {CalibrationCurveId = 1, Slope = 2, OffsetValue = 3});
			_stubEventAggregator.GetEvent<GetCalibrationCurveRequest>().Returns(_stubEvent);
			_stubEventAggregator.GetEvent<CalibrationCurveCalculated>().Returns(new CalibrationCurveCalculated());

			_dataService = new CalibrationCurveDataService(_mockDataAccess, _stubEventAggregator, _stubLoggerFacade);
		}

		[TestMethod]
		public void Then_the_calibration_curve_data_access_service_is_used_to_get_the_data_record()
		{
			_dataService.GetCurrentCalibrationCurve(_stubLoggerFacade);

			//Once at init and once during method call
			_mockDataAccess.Received(2).SelectCurrent(_stubLoggerFacade);
		}

		[TestMethod]
		public void Then_the_curve_that_is_returned_uses_the_data_obtained_from_the_data_access()
		{
			var expected = new CalibrationCurve(1, 2, 3);

			var actual = _dataService.GetCurrentCalibrationCurve(_stubLoggerFacade);

			Assert.AreEqual(expected.CurveId, actual.CurveId);
			Assert.AreEqual(expected.Slope, actual.Slope);
			Assert.AreEqual(expected.Offset, actual.Offset);
		}
	}
	// ReSharper restore InconsistentNaming
}
