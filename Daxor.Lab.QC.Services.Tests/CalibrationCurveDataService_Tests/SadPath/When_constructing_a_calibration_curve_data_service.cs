﻿using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
// ReSharper disable ObjectCreationAsStatement

namespace Daxor.Lab.QC.Services.Tests.CalibrationCurveDataService_Tests.SadPath
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_constructing_a_calibration_curve_data_service
	{
		[TestMethod]
		public void And_the_calibration_curve_data_access_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new CalibrationCurveDataService(null, Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>()), "calibrationCurveDataAccess");
		}

		[TestMethod]
		public void And_the_event_aggregator_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new CalibrationCurveDataService(Substitute.For<ICalibrationCurveDataAccess>(), null, Substitute.For<ILoggerFacade>()), "eventAggregator");
		}

		[TestMethod]
		public void And_the_logger_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new CalibrationCurveDataService(Substitute.For<ICalibrationCurveDataAccess>(), Substitute.For<IEventAggregator>(), null), "logger");
		}
	}
	// ReSharper restore InconsistentNaming
}
