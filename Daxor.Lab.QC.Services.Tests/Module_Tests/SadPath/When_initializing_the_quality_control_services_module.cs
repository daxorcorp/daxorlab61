﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Interfaces;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.QC.Services.Tests.Module_Tests.SadPath
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_the_Quality_Control_Services_module
	{
		private Module _module;
		private IUnityContainer _container;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_container = Substitute.For<IUnityContainer>();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_schema_provider_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<ISchemaProvider, SchemasProvider>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control Services", ex.ModuleName);
				Assert.AreEqual("initialize the Schema Provider", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_calibration_curve_data_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<ICalibrationCurveDataService, CalibrationCurveDataService>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control Services", ex.ModuleName);
				Assert.AreEqual("initialize the Calibration Curve Data Service", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_QC_test_repository_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IRepository<QcTest>, QcTestRepository>()).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control Services", ex.ModuleName);
				Assert.AreEqual("initialize the Quality Control Test Repository", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_quality_test_data_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IQualityTestDataService, QualityTestDataService>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control Services", ex.ModuleName);
				Assert.AreEqual("initialize the Quality Test Data Service", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_isotope_matching_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IIsotopeMatchingService, IsotopeMatchingService>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control Services", ex.ModuleName);
				Assert.AreEqual("initialize the Isotope Matching Service", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_fine_gain_adjustment_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IFineGainAdjustmentService, FineGainAdjustmentService>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control Services", ex.ModuleName);
				Assert.AreEqual("initialize the Fine Gain Adjustment Service", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_calibration_curve_data_access_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<ICalibrationCurveDataAccess, CalibrationCurveDataAccess>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control Services", ex.ModuleName);
				Assert.AreEqual("initialize the Calibration Curve Data Access", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_test_configurator_collection_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IEnumerable<IConfigurator>, TestConfiguratorCollection>()).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control Services", ex.ModuleName);
				Assert.AreEqual("initialize the Test Configurator Collection", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_Canberra_peak_finder_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IPeakFinder, CanberraPeakFinder>()).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control Services", ex.ModuleName);
				Assert.AreEqual("initialize the Canberra Peak Finder", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_quality_control_test_controller_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IQualityTestController, QcTestController>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control Services", ex.ModuleName);
				Assert.AreEqual("initialize the Quality Control Test Controller", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_quality_control_calibration_test_payload_processor_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IQcCalibrationTestPayloadProcessor, QcCalibrationTestPayloadProcessor>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control Services", ex.ModuleName);
				Assert.AreEqual("initialize the Quality Control Calibration Test Payload Processor", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}
	}
	// ReSharper restore InconsistentNaming
}