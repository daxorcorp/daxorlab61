using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaTest_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_dealing_with_physicans_specialty
    {
        private BVATest _bvaTest;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _bvaTest = new BVATest(null);
        }

        [TestMethod]
        public void Then_the_specialty_can_be_set_and_retrieved()
        {
            _bvaTest.PhysiciansSpecialty = "Intensive Care";

            Assert.AreEqual("Intensive Care", _bvaTest.PhysiciansSpecialty);
        }

        [TestMethod]
        public void Then_setting_the_specialty_raises_PropertyChanged()
        {
            var wasRaised = false;
            _bvaTest.PropertyChanged += (sender, args) => { wasRaised = args.PropertyName == "PhysiciansSpecialty"; };

            _bvaTest.PhysiciansSpecialty = "Intensive Care";
            
            Assert.IsTrue(wasRaised);
        }
    }
    // ReSharper restore InconsistentNaming
}
