﻿using System.Linq;
using Daxor.Lab.BVA.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaTest_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_building_patient_samples_based_on_protocol
    {
        [TestMethod]
        public void And_initially_building_the_samples_then_the_samples_display_names_should_be_the_schemas_friendly_name()
        {
            // This sets the protocol, which builds the inner sample collection
            var bvaTest = BvaTestFactory.CreateBvaTestWithRandomFriendlyNames(3, 10);

            var sample1a = (from i in bvaTest.CurrentSampleSchema.SampleLayoutItems where i.Type == SampleType.Sample1 && i.Range == SampleRange.A select i).FirstOrDefault();
            var sample2a = (from i in bvaTest.CurrentSampleSchema.SampleLayoutItems where i.Type == SampleType.Sample2 && i.Range == SampleRange.A select i).FirstOrDefault();
            var sample3a = (from i in bvaTest.CurrentSampleSchema.SampleLayoutItems where i.Type == SampleType.Sample3 && i.Range == SampleRange.A select i).FirstOrDefault();
            var sample1b = (from i in bvaTest.CurrentSampleSchema.SampleLayoutItems where i.Type == SampleType.Sample1 && i.Range == SampleRange.B select i).FirstOrDefault();
            var sample2b = (from i in bvaTest.CurrentSampleSchema.SampleLayoutItems where i.Type == SampleType.Sample2 && i.Range == SampleRange.B select i).FirstOrDefault();
            var sample3b = (from i in bvaTest.CurrentSampleSchema.SampleLayoutItems where i.Type == SampleType.Sample3 && i.Range == SampleRange.B select i).FirstOrDefault();

            var observedSample1a = (from s in bvaTest.Samples where s.Type == SampleType.Sample1 && s.Range == SampleRange.A select s).FirstOrDefault();
            var observedSample2a = (from s in bvaTest.Samples where s.Type == SampleType.Sample2 && s.Range == SampleRange.A select s).FirstOrDefault();
            var observedSample3a = (from s in bvaTest.Samples where s.Type == SampleType.Sample3 && s.Range == SampleRange.A select s).FirstOrDefault();
            var observedSample1b = (from s in bvaTest.Samples where s.Type == SampleType.Sample1 && s.Range == SampleRange.B select s).FirstOrDefault();
            var observedSample2b = (from s in bvaTest.Samples where s.Type == SampleType.Sample2 && s.Range == SampleRange.B select s).FirstOrDefault();
            var observedSample3b = (from s in bvaTest.Samples where s.Type == SampleType.Sample3 && s.Range == SampleRange.B select s).FirstOrDefault();

            // ReSharper disable PossibleNullReferenceException
            Assert.AreEqual(sample1a.FriendlyName, observedSample1a.DisplayName);
            Assert.AreEqual(sample2a.FriendlyName, observedSample2a.DisplayName);
            Assert.AreEqual(sample3a.FriendlyName, observedSample3a.DisplayName);
            Assert.AreEqual(sample1b.FriendlyName, observedSample1b.DisplayName);
            Assert.AreEqual(sample2b.FriendlyName, observedSample2b.DisplayName);
            Assert.AreEqual(sample3b.FriendlyName, observedSample3b.DisplayName);
            // ReSharper restore PossibleNullReferenceException
        }

        [TestMethod]
        public void And_increasing_the_protocol_then_the_samples_display_names_should_be_the_schemas_friendly_name()
        {
            // This sets the protocol, which builds the inner sample collection
            var bvaTest = BvaTestFactory.CreateBvaTestWithRandomFriendlyNames(3, 10);

            var sample4a = (from i in bvaTest.CurrentSampleSchema.SampleLayoutItems where i.Type == SampleType.Sample4 && i.Range == SampleRange.A select i).FirstOrDefault();
            var sample4b = (from i in bvaTest.CurrentSampleSchema.SampleLayoutItems where i.Type == SampleType.Sample4 && i.Range == SampleRange.B select i).FirstOrDefault();

            bvaTest.Protocol = 4;

            var observedSample4a = (from s in bvaTest.Samples where s.Type == SampleType.Sample4 && s.Range == SampleRange.A select s).FirstOrDefault();
            var observedSample4b = (from s in bvaTest.Samples where s.Type == SampleType.Sample4 && s.Range == SampleRange.B select s).FirstOrDefault();

            // ReSharper disable PossibleNullReferenceException
            Assert.AreEqual(sample4a.FriendlyName, observedSample4a.DisplayName);
            Assert.AreEqual(sample4b.FriendlyName, observedSample4b.DisplayName);
            // ReSharper restore PossibleNullReferenceException
        }
    }
    // ReSharper restore InconsistentNaming
}
