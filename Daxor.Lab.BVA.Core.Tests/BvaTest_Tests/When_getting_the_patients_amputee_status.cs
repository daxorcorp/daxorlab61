﻿using Daxor.Lab.Domain.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.BVA.Core.Tests.BvaTest_Tests
{
	[TestClass]
	public class When_getting_the_patients_amputee_status
	{
		private BVATestThatDoesntUpdateRules _test;

		private BVAPatient _patient;

		private IRuleEngine _ruleEngine;

		[TestInitialize]
		public void Init()
		{
			_ruleEngine = Substitute.For<IRuleEngine>();

			_patient = new BVAPatient(_ruleEngine);

			_test = new BVATestThatDoesntUpdateRules {Patient = _patient};
		}

		[TestMethod]
		public void Then_it_returns_the_amputee_status_of_the_patient()
		{
			Assert.IsFalse(_test.PatientIsAmputee);

			_patient.IsAmputee = true;

			Assert.IsTrue(_test.PatientIsAmputee);
		}
	}
}
