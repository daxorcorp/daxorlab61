﻿using System.Linq;
using Daxor.Lab.BVA.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaTest_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_building_baseline_and_standard_samples
    {
        [TestMethod]
        public void Then_the_samples_display_names_should_be_the_schemas_friendly_name()
        {
            var bvaTest = BvaTestFactory.CreateBvaTestWithRandomFriendlyNames(3, 10);
            var standardASample = (from i in bvaTest.CurrentSampleSchema.SampleLayoutItems where i.Type == SampleType.Standard && i.Range == SampleRange.A select i).FirstOrDefault();
            var standardBSample = (from i in bvaTest.CurrentSampleSchema.SampleLayoutItems where i.Type == SampleType.Standard && i.Range == SampleRange.B select i).FirstOrDefault();;
            var baselineASample = (from i in bvaTest.CurrentSampleSchema.SampleLayoutItems where i.Type == SampleType.Baseline && i.Range == SampleRange.A select i).FirstOrDefault();;
            var baselineBSample = (from i in bvaTest.CurrentSampleSchema.SampleLayoutItems where i.Type == SampleType.Baseline && i.Range == SampleRange.B select i).FirstOrDefault(); ;

            bvaTest.BuildBaselineAndStandardSamples();

            var observedStandardASample = (from s in bvaTest.StandardSamples where s.Range == SampleRange.A select s).FirstOrDefault();
            var observedStandardBSample = (from s in bvaTest.StandardSamples where s.Range == SampleRange.B select s).FirstOrDefault();
            var observedBaselineASample = (from s in bvaTest.BaselineSamples where s.Range == SampleRange.A select s).FirstOrDefault();
            var observedBaselineBSample = (from s in bvaTest.BaselineSamples where s.Range == SampleRange.B select s).FirstOrDefault();

            // ReSharper disable PossibleNullReferenceException
            Assert.AreEqual(standardASample.FriendlyName, observedStandardASample.DisplayName);
            Assert.AreEqual(standardBSample.FriendlyName, observedStandardBSample.DisplayName);
            Assert.AreEqual(baselineASample.FriendlyName, observedBaselineASample.DisplayName);
            Assert.AreEqual(baselineBSample.FriendlyName, observedBaselineBSample.DisplayName);
            // ReSharper restore PossibleNullReferenceException
        }
    }
    // ReSharper restore InconsistentNaming
}
