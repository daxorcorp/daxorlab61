﻿using Daxor.Lab.Domain.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.BVA.Core.Tests.BvaTest_Tests
{
	[TestClass]
	public class When_building_the_amputee_ideals_correction_factor
	{
		private BVATest _test;

		private IRuleEngine _ruleEngine;

		[TestInitialize]
		public void Init()
		{
			_ruleEngine = Substitute.For<IRuleEngine>();

			_test = new BVATest(_ruleEngine);
		}

		[TestMethod]
		public void And_the_patient_is_not_an_amputee_then_the_initial_correction_factor_is_0()
		{
			_test.Patient = new BVAPatient(_ruleEngine) { IsAmputee = false };

			Assert.AreEqual(0, _test.AmputeeIdealsCorrectionFactor);
		}

		[TestMethod]
		public void And_the_patient_is_an_amputee_then_the_initial_correction_factor_is_unselected()
		{
			_test.Patient = new BVAPatient(_ruleEngine) { IsAmputee = true };

			Assert.AreEqual(-1, _test.AmputeeIdealsCorrectionFactor);
		}
	}
}
