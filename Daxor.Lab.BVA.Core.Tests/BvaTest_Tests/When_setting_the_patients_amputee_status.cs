﻿using System;
using Daxor.Lab.Domain.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.BVA.Core.Tests.BvaTest_Tests
{
	[TestClass]
	public class When_setting_the_patients_amputee_status
	{
		private BVATestThatDoesntUpdateRules _test;

		private BVAPatient _patient;

		private IRuleEngine _ruleEngine;

		[TestInitialize]
		public void Init()
		{
			_ruleEngine = Substitute.For<IRuleEngine>();

			_patient = new BVAPatient(_ruleEngine);

			_test = new BVATestThatDoesntUpdateRules { Patient = _patient };
		}

		[TestMethod]
		public void Then_the_patients_amputee_status_is_changed()
		{
			Assert.IsFalse(_test.Patient.IsAmputee);
			Assert.IsFalse(_test.PatientIsAmputee);

			_test.Patient.IsAmputee = true;

			Assert.IsTrue(_test.PatientIsAmputee);
		}

		[TestMethod]
		public void Then_property_changed_is_raised_for_the_amputee_ideals_correction_factor()
		{
			var wasEventRaised = false;
			_test.PropertyChanged += (sender, args) =>
			{
				if (args.PropertyName == "AmputeeIdealsCorrectionFactor") wasEventRaised = true;
			};

			_test.PatientIsAmputee = true;

			Assert.IsTrue(wasEventRaised);
		}

		[TestMethod]
		public void And_the_patients_amputee_status_is_being_changed_from_amputee_to_amputee_then_the_correction_factor_stays_the_same()
		{
			//Arrange
			_test.Patient.IsAmputee = true;
			_test.AmputeeIdealsCorrectionFactor = 5;

			//Act
			_test.PatientIsAmputee = true;

			//Assert
			Assert.AreEqual(5, _test.AmputeeIdealsCorrectionFactor);
		}

		[TestMethod]
		public void And_the_PatientIsAmputee_property_is_being_changed_from_amputee_to_non_amputee_then_the_correction_factor_becomes_0()
		{
			//Arrange
			_test.Patient.IsAmputee = true;
			_test.AmputeeIdealsCorrectionFactor = 5;

			//Act
			_test.PatientIsAmputee = false;

			//Assert
			Assert.AreEqual(0, _test.AmputeeIdealsCorrectionFactor);
		}

		[TestMethod]
		public void And_the_patients_amputee_status_is_being_changed_from_amputee_to_non_amputee_then_the_correction_factor_becomes_0()
		{
			//Arrange
			_test.Patient.IsAmputee = true;
			_test.AmputeeIdealsCorrectionFactor = 5;

			//Act
			_patient.IsAmputee = false;

			//Assert
			Assert.AreEqual(0, _test.AmputeeIdealsCorrectionFactor);
		}

		[TestMethod]
		public void And_the_patients_amputee_status_is_being_changed_from_non_amputee_to_amputee_then_the_correction_factor_becomes_unselected()
		{
			//Arrange
			_test.Patient.IsAmputee = false;

			//Act
			_test.PatientIsAmputee = true;

			//Assert
			Assert.AreEqual(-1, _test.AmputeeIdealsCorrectionFactor);
		}

		[TestMethod]
		public void And_the_patients_amputee_status_is_being_changed_from_non_amputee_to_non_amputee_then_the_correction_factor_stays_0()
		{
			//Arrange
			_test.Patient.IsAmputee = false;

			//Act
			_test.PatientIsAmputee = false;

			//Assert
			Assert.AreEqual(0, _test.AmputeeIdealsCorrectionFactor);
		}

		[TestMethod]
		public void And_the_patients_amputee_status_is_being_changed_then_PatientIsAmputee_should_fire_property_changed()
		{
			//Arrange
			var wasEventRaised = false;
			_test.PropertyChanged += (sender, args) =>
			{
				if (args.PropertyName == "PatientIsAmputee") wasEventRaised = true;
			};

			//Act
			_test.PatientIsAmputee = true;

			//Assert
			Assert.IsTrue(wasEventRaised);
		}

		[TestMethod]
		public void And_the_patients_amputee_status_is_being_changed_then_AmputeeIdealsCorrectionFactor_should_fire_property_changed()
		{
			//Arrange
			var wasEventRaised = false;
			_test.Patient.IsAmputee = true;
			_test.AmputeeIdealsCorrectionFactor = 5;
			_test.PropertyChanged += (sender, args) =>
			{
				if (args.PropertyName == "AmputeeIdealsCorrectionFactor") wasEventRaised = true;
			};

			//Act
			_test.Patient.IsAmputee = false;

			//Assert
			Assert.IsTrue(wasEventRaised);
		}

		[TestMethod]
		public void And_the_patients_amputee_status_is_being_changed_to_false_then_the_AmputeeIdealsCorrectionFactor_should_be_reset()
		{
			//Arrange
			_test.Patient.IsAmputee = true;
			_test.AmputeeIdealsCorrectionFactor = 5;

			//Act
			_test.Patient.IsAmputee = false;

			//Assert
			Assert.AreEqual(0, _test.AmputeeIdealsCorrectionFactor);
		}

	    [TestMethod]
	    public void And_the_amputee_status_of_an_old_patient_is_being_changed_then_PatientIsAmputee_does_not_fire_property_changed()
        {
            //Arrange
            var wasEventRaised = false;
            _test.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == "PatientIsAmputee") wasEventRaised = true;
            };

	        var oldPatient = _test.Patient;
            _test.Patient = new BVAPatient(Guid.NewGuid(), null);

            //Act
            oldPatient.IsAmputee = true;

            //Assert
            Assert.IsFalse(wasEventRaised);
	    }
	}
}
