﻿using System.Linq;
using Daxor.Lab.BVA.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaTest_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_assigning_sample_type_and_range_to_sample
    {
        [TestMethod]
        public void Then_the_samples_display_names_should_be_the_schemas_friendly_name()
        {
            var bvaTest = BvaTestFactory.CreateBvaTestWithRandomFriendlyNames(3, 10);
            var sample3b = (from i in bvaTest.CurrentSampleSchema.SampleLayoutItems where i.Type == SampleType.Sample3 && i.Range == SampleRange.B select i).FirstOrDefault();

            // ReSharper disable PossibleNullReferenceException
            var mockSample = new BVASample(sample3b.Position, null);
            bvaTest.AssignSampleTypeAndRangeToSample(mockSample);

            Assert.AreEqual(sample3b.FriendlyName, mockSample.DisplayName);
            // ReSharper restore PossibleNullReferenceException
        }
    }
    // ReSharper restore InconsistentNaming
}
