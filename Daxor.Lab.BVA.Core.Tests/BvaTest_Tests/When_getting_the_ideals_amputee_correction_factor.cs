﻿using Daxor.Lab.BVA.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaTest_Tests
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_getting_the_ideals_amputee_correction_factor
	{
		private BVATest _test;

		[TestInitialize]
		public void Init()
		{
			_test = BvaTestFactory.CreateBvaTest(5, 60);
		}

		[TestMethod]
		public void And_the_patient_is_not_an_amputee_then_the_correction_factor_is_0()
		{
			Assert.IsFalse(_test.PatientIsAmputee);
			Assert.AreEqual(0, _test.AmputeeIdealsCorrectionFactor);
		}

		[TestMethod]
		public void And_the_patient_is_not_an_amputee_then_the_correction_factor_does_not_change()
		{
			_test.AmputeeIdealsCorrectionFactor = 5;

			Assert.AreEqual(0, _test.AmputeeIdealsCorrectionFactor);
		}

		[TestMethod]
		public void And_the_patient_is_an_amputee_then_the_correction_factor_is_unselected()
		{
			_test.PatientIsAmputee = true;

			Assert.AreEqual(-1, _test.AmputeeIdealsCorrectionFactor);
		}

		[TestMethod]
		public void And_the_patient_is_an_amputee_then_the_correction_factor_can_be_changed()
		{
			_test.PatientIsAmputee = true;

			_test.AmputeeIdealsCorrectionFactor = 5;

			Assert.AreEqual(5, _test.AmputeeIdealsCorrectionFactor);
		}
	}
	// ReSharper restore InconsistentNaming
}