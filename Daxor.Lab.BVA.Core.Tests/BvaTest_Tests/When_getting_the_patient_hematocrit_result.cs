﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaTest_Tests
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_getting_the_patient_hematocrit_result
	{
		private BVATest _bvaTest;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_bvaTest = new BVATest(null);
		}

		[TestMethod]
		public void And_the_test_is_a_manual_test_and_the_hematocrit_fill_is_one_per_test_and_no_data_has_been_entered_then_the_patient_hematocrit_is_0()
		{
			//Arrange
			_bvaTest.Mode = TestMode.Manual;
			_bvaTest.HematocritFill = HematocritFillType.OnePerTest;

			//Act
			var observedPatientHematocrit = _bvaTest.PatientHematocritResult;

			//Assert
			Assert.AreEqual(0, observedPatientHematocrit);
		}

        [TestMethod]
        public void And_the_test_is_a_manual_test_and_the_hematocrit_fill_is_two_per_sample_and_no_data_has_been_entered_then_the_patient_hematocrit_is_0()
        {
            //Arrange
            _bvaTest.Mode = TestMode.Manual;
            _bvaTest.HematocritFill = HematocritFillType.TwoPerSample;

            //Act
            var observedPatientHematocrit = _bvaTest.PatientHematocritResult;

            //Assert
            Assert.AreEqual(0, observedPatientHematocrit);
        }

        [TestMethod]
        public void And_the_test_is_a_manual_test_and_the_hematocrit_fill_is_one_per_sample_and_no_data_has_been_entered_then_the_patient_hematocrit_is_0()
        {
            //Arrange
            _bvaTest.Mode = TestMode.Manual;
            _bvaTest.HematocritFill = HematocritFillType.OnePerSample;

            //Act
            var observedPatientHematocrit = _bvaTest.PatientHematocritResult;

            //Assert
            Assert.AreEqual(0, observedPatientHematocrit);
        }

        [TestMethod]
        public void And_the_test_is_a_manual_test_and_the_hematocrit_fill_is_last_two_and_no_data_has_been_entered_then_the_patient_hematocrit_is_0()
        {
            //Arrange
            _bvaTest.Mode = TestMode.Manual;
            _bvaTest.HematocritFill = HematocritFillType.LastTwo;

            //Act
            var observedPatientHematocrit = _bvaTest.PatientHematocritResult;

            //Assert
            Assert.AreEqual(0, observedPatientHematocrit);
        }
	}
	// ReSharper restore InconsistentNaming
}