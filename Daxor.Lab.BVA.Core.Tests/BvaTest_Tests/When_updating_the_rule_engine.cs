﻿using Daxor.Lab.Domain.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Core.Tests.BvaTest_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_updating_the_rule_engine
    {
        private BVATestThatDoesntUpdateRules _bvaTestThatDoesntUpdateRules;

        [TestInitialize]
        public void Initialize()
        {
            _bvaTestThatDoesntUpdateRules = new BVATestThatDoesntUpdateRules();
        }

        [TestMethod]
        public void And_the_test_mode_is_automatic_then_the_user_required_rules_are_enabled()
        {
            _bvaTestThatDoesntUpdateRules.Mode = TestMode.Automatic;

            var newRuleEngine = Substitute.For<IRuleEngine>();

            _bvaTestThatDoesntUpdateRules.UpdateRuleEngine(newRuleEngine);

            Assert.IsTrue(_bvaTestThatDoesntUpdateRules.WasEnabled);
        }

        [TestMethod]
        public void And_the_test_mode_is_manual_then_the_user_required_rules_are_disabled()
        {
            _bvaTestThatDoesntUpdateRules.Mode = TestMode.Manual;

            var newRuleEngine = Substitute.For<IRuleEngine>();

            _bvaTestThatDoesntUpdateRules.UpdateRuleEngine(newRuleEngine);

            Assert.IsTrue(_bvaTestThatDoesntUpdateRules.WasDisabled);
        }

        [TestMethod]
        public void And_the_test_mode_is_VOPS_then_the_user_required_rules_are_disabled()
        {
            _bvaTestThatDoesntUpdateRules.Mode = TestMode.VOPS;

            var newRuleEngine = Substitute.For<IRuleEngine>();

            _bvaTestThatDoesntUpdateRules.UpdateRuleEngine(newRuleEngine);

            Assert.IsTrue(_bvaTestThatDoesntUpdateRules.WasDisabled);
        }
    }
    // ReSharper restore RedundantArgumentName
}