﻿using Daxor.Lab.Domain.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.BVA.Core.Tests.BvaTest_Tests
{
	[TestClass]
	public class When_assigning_the_amputee_ideals_correction_factor
	{
		private BVATest _test;

		private IRuleEngine _ruleEngine;

		[TestInitialize]
		public void Init()
		{
			_ruleEngine = Substitute.For<IRuleEngine>();

			_test = new BVATest(_ruleEngine);
		}

		[TestMethod]
		public void And_the_patient_is_not_an_amputee_then_the_correction_factor_does_not_change()
		{
			//Arrange
			_test.Patient.IsAmputee = false;
			Assert.AreEqual(0, _test.AmputeeIdealsCorrectionFactor);

			//Act
			_test.AmputeeIdealsCorrectionFactor = 5;

			//Assert
			Assert.AreEqual(0, _test.AmputeeIdealsCorrectionFactor);
		}

		[TestMethod]
		public void And_the_patient_is_an_amputee_then_the_correction_factor_does_change()
		{
			_test.Patient.IsAmputee = true;

			_test.AmputeeIdealsCorrectionFactor = 9;
			Assert.AreEqual(9, _test.AmputeeIdealsCorrectionFactor);

			_test.AmputeeIdealsCorrectionFactor = 0;
			Assert.AreEqual(0, _test.AmputeeIdealsCorrectionFactor);
		}

		[TestMethod]
		public void And_the_patient_is_an_amputee_then_the_correction_factor_is_only_applied_to_ideal_blood_volumes()
		{
			//Arrange
			var fakeIdealsVolume = new BVAVolume(_ruleEngine)
			{
				Type = BloodType.Ideal,
				CorrectionFactor = 0
			};

			var fakeMeasuredVolume = new BVAVolume(_ruleEngine)
			{
				Type = BloodType.Measured,
				CorrectionFactor = 0
			};

			_test.Volumes[BloodType.Ideal] = fakeIdealsVolume;
			_test.Volumes[BloodType.Measured] = fakeMeasuredVolume;

			Assert.AreEqual(0, _test.Volumes[BloodType.Ideal].CorrectionFactor);
			Assert.AreEqual(0, _test.Volumes[BloodType.Measured].CorrectionFactor);

			//Act
			_test.Patient.IsAmputee = true;
			_test.AmputeeIdealsCorrectionFactor = 5;

			//Assert
			Assert.AreEqual(5, _test.Volumes[BloodType.Ideal].CorrectionFactor);
			Assert.AreEqual(0, _test.Volumes[BloodType.Measured].CorrectionFactor);
		}
	}
}
