﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaTest_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_changing_the_test_mode
    {
        private BVATestThatDoesntUpdateRules _bvaTestThatDoesntUpdateRules;

        [TestInitialize]
        public void Initialize()
        {
            _bvaTestThatDoesntUpdateRules = new BVATestThatDoesntUpdateRules();
        }

        [TestMethod]
        public void And_the_new_test_mode_is_manual_then_the_user_required_rules_are_disabled()
        {
            _bvaTestThatDoesntUpdateRules.Mode = TestMode.Manual;

            Assert.IsTrue(_bvaTestThatDoesntUpdateRules.WasDisabled);
        }

        [TestMethod]
        public void And_the_test_mode_is_manual_and_is_changing_to_automatic_then_the_user_required_rules_are_enabled()
        {
            _bvaTestThatDoesntUpdateRules.Mode = TestMode.Manual;

            Assert.IsTrue(_bvaTestThatDoesntUpdateRules.WasDisabled);

            _bvaTestThatDoesntUpdateRules.Mode = TestMode.Automatic;

            Assert.IsTrue(_bvaTestThatDoesntUpdateRules.WasEnabled);
        }

        [TestMethod]
        public void And_the_new_test_mode_is_VOPS_then_the_user_required_rules_are_disabled()
        {
            _bvaTestThatDoesntUpdateRules.Mode = TestMode.VOPS;

            Assert.IsTrue(_bvaTestThatDoesntUpdateRules.WasDisabled);
        }

        [TestMethod]
        public void And_the_test_mode_is_VOPS_and_is_changing_to_automatic_then_the_user_required_rules_are_enabled()
        {
            _bvaTestThatDoesntUpdateRules.Mode = TestMode.VOPS;

            Assert.IsTrue(_bvaTestThatDoesntUpdateRules.WasDisabled);

            _bvaTestThatDoesntUpdateRules.Mode = TestMode.Automatic;

            Assert.IsTrue(_bvaTestThatDoesntUpdateRules.WasEnabled);
        }
    }

    // ReSharper restore RedundantArgumentName
}