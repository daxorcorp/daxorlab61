using Daxor.Lab.Domain.Interfaces;
using NSubstitute;

namespace Daxor.Lab.BVA.Core.Tests.BvaTest_Tests
{
    // ReSharper disable once InconsistentNaming
    internal class BVATestThatDoesntUpdateRules : BVATest
    {
        public bool WasDisabled;
        public bool WasEnabled;

        public BVATestThatDoesntUpdateRules() : base(Substitute.For<IRuleEngine>())
        {
            
        }

        public override void DisableOrEnableUserRequiredRules(bool disable)
        {
            WasDisabled = disable;
            WasEnabled = !disable;
        }
    }
}