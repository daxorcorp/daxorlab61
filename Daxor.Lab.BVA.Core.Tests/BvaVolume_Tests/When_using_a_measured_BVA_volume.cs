﻿using Daxor.Lab.Domain.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.BVA.Core.Tests.BvaVolume_Tests
{
	[TestClass]
	public class When_using_a_measured_BVA_volume
	{
		private BVAVolume _volume;

		private IRuleEngine _ruleEngine;

		[TestInitialize]
		public void Init()
		{
			_ruleEngine = Substitute.For<IRuleEngine>();

			_volume = new BVAVolume(_ruleEngine)
			{
				Type = BloodType.Measured,
				RedCellCount = 10,
				PlasmaCount = 10
			};
		}

		[TestMethod]
		public void And_the_correction_factor_is_0_then_the_whole_count_stays_the_same()
		{
			Assert.AreEqual(0, _volume.CorrectionFactor);
			Assert.AreEqual(20, _volume.WholeCount);
		}

		[TestMethod]
		public void And_the_correction_factor_is_not_0_then_the_whole_count_stays_the_same()
		{
			_volume.CorrectionFactor = 9;
			Assert.AreEqual(20, _volume.WholeCount);
		}
	}
}
