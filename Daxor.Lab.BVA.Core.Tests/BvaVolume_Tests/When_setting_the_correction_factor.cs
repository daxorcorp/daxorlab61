﻿using Daxor.Lab.Domain.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.BVA.Core.Tests.BvaVolume_Tests
{
	[TestClass]
	public class When_setting_the_correction_factor
	{
		private BVAVolume _volume;

		private IRuleEngine _ruleEngine;

		[TestInitialize]
		public void Init()
		{
			_ruleEngine = Substitute.For<IRuleEngine>();

			_volume = new BVAVolume(_ruleEngine);
		}

		[TestMethod]
		public void And_the_blood_type_is_ideal_then_the_correction_factor_can_be_changed()
		{
			//Arrange
			_volume.Type = BloodType.Ideal;
			Assert.AreEqual(0, _volume.CorrectionFactor);

			//Act
			_volume.CorrectionFactor = 5;

			//Assert
			Assert.AreEqual(5, _volume.CorrectionFactor);
		}

		[TestMethod]
		public void And_the_blood_type_is_not_ideal_then_the_correction_factor_does_not_change()
		{
			//Arrange
			_volume.Type = BloodType.Measured;
			Assert.AreEqual(0, _volume.CorrectionFactor);

			//Act
			_volume.CorrectionFactor = 5;

			//Assert
			Assert.AreEqual(0, _volume.CorrectionFactor);
		}

		[TestMethod]
		public void And_the_blood_type_is_ideal_but_the_new_value_is_out_of_range_then_the_correction_factor_does_not_change()
		{
			_volume.Type = BloodType.Ideal;
			_volume.CorrectionFactor = 5;
			Assert.AreEqual(5, _volume.CorrectionFactor);

			_volume.CorrectionFactor = -2;
			Assert.AreEqual(5, _volume.CorrectionFactor);

			_volume.CorrectionFactor = 10;
			Assert.AreEqual(5, _volume.CorrectionFactor);
		}

		[TestMethod]
		public void And_the_blood_type_is_ideal_and_trying_to_set_the_correction_factor_to_negative_1_then_it_is_set_to_0()
		{
			_volume.Type = BloodType.Ideal;
			_volume.CorrectionFactor = 5;
			Assert.AreEqual(5, _volume.CorrectionFactor);

			_volume.CorrectionFactor = -1;
			Assert.AreEqual(0, _volume.CorrectionFactor);

			_volume.CorrectionFactor = 10;
			Assert.AreEqual(0, _volume.CorrectionFactor);
		}

		[TestMethod]
		public void And_the_correction_factor_is_set_then_property_changed_is_raised_for_the_correction_factor()
		{
			//Arrange
			var wasEventRaised = false;
			_volume.PropertyChanged += (sender, args) =>
			{
				if (args.PropertyName == "CorrectionFactor") wasEventRaised = true;
			};
			_volume.Type = BloodType.Ideal;

			//Act
			_volume.CorrectionFactor = 5;

			//Assert
			Assert.IsTrue(wasEventRaised);
		}

		[TestMethod]
		public void And_the_correction_factor_is_set_then_property_changed_is_raised_for_the_red_cell_count()
		{
			//Arrange
			var wasEventRaised = false;
			_volume.PropertyChanged += (sender, args) =>
			{
				if (args.PropertyName == "RedCellCount") wasEventRaised = true;
			};
			_volume.Type = BloodType.Ideal;

			//Act
			_volume.CorrectionFactor = 5;

			//Assert
			Assert.IsTrue(wasEventRaised);
		}

		[TestMethod]
		public void And_the_correction_factor_is_set_then_property_changed_is_raised_for_the_plasma_count()
		{
			//Arrange
			var wasEventRaised = false;
			_volume.PropertyChanged += (sender, args) =>
			{
				if (args.PropertyName == "PlasmaCount") wasEventRaised = true;
			};
			_volume.Type = BloodType.Ideal;

			//Act
			_volume.CorrectionFactor = 5;

			//Assert
			Assert.IsTrue(wasEventRaised);
		}

		[TestMethod]
		public void And_the_correction_factor_is_set_then_property_changed_is_raised_for_the_whole_count()
		{
			//Arrange
			var wasEventRaised = false;
			_volume.PropertyChanged += (sender, args) =>
			{
				if (args.PropertyName == "WholeCount") wasEventRaised = true;
			};
			_volume.Type = BloodType.Ideal;

			//Act
			_volume.CorrectionFactor = 5;

			//Assert
			Assert.IsTrue(wasEventRaised);
		}
	}
}
