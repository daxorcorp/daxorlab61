﻿using System;
using Daxor.Lab.Domain.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.BVA.Core.Tests.BvaVolume_Tests
{
	[TestClass]
	public class When_using_an_ideal_BVA_Volume
	{
		private BVAVolume _volume;
		private IRuleEngine _ruleEngine;
	    private const int InitialRedCellVolume = 5040;
        private const int InitialPlasmaVolume = 5040;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_ruleEngine = Substitute.For<IRuleEngine>();

			_volume = new BVAVolume(_ruleEngine)
			{
				Type = BloodType.Ideal,
				RedCellCount = InitialRedCellVolume,
				PlasmaCount = InitialPlasmaVolume,
			};
		}

		[TestMethod]
		public void And_the_correction_factor_is_0_then_the_whole_count_stays_the_same()
		{
			Assert.AreEqual(0, _volume.CorrectionFactor);
			Assert.AreEqual(InitialPlasmaVolume + InitialRedCellVolume, _volume.WholeCount);
		}

		[TestMethod]
		public void And_the_correction_factor_is_0_then_the_plasma_count_does_not_change()
		{
			_volume.CorrectionFactor = 0;
			Assert.AreEqual(InitialPlasmaVolume, _volume.PlasmaCount);
		}

		[TestMethod]
		public void And_the_correction_factor_is_0_then_the_red_cell_count_does_not_change()
		{
			_volume.CorrectionFactor = 0;
			Assert.AreEqual(InitialRedCellVolume, _volume.RedCellCount);
		}

		[TestMethod]
		public void And_the_correction_factor_is_not_0_then_the_whole_count_changes()
		{
			_volume.CorrectionFactor = 9;

		    var expectedWholeCount = (int) (Math.Round(InitialRedCellVolume*0.91, 0) + 
                Math.Round(InitialPlasmaVolume*0.91, 0));

			Assert.AreEqual(expectedWholeCount, _volume.WholeCount);
		}

		[TestMethod]
		public void And_the_correction_factor_is_not_0_then_the_plasma_count_changes()
		{
			_volume.CorrectionFactor = 9;

		    var expectedPlasmaCount = (int) Math.Round(InitialPlasmaVolume*0.91, 0);

			Assert.AreEqual(expectedPlasmaCount, _volume.PlasmaCount);
		}

		[TestMethod]
		public void And_the_correction_factor_is_not_0_then_the_red_cell_count_changes()
		{
			_volume.CorrectionFactor = 9;

            var expectedRedCellCount = (int)Math.Round(InitialRedCellVolume*0.91, 0);

			Assert.AreEqual(expectedRedCellCount, _volume.RedCellCount);
		}

		[TestMethod]
		public void And_the_modified_plasma_count_should_be_rounded_down_then_it_is_rounded_down()
		{
			_volume.CorrectionFactor = 9;

			Assert.AreEqual(4586.4, Math.Round(InitialPlasmaVolume * .91, 1));
			Assert.AreEqual(4586, _volume.PlasmaCount);
		}

		[TestMethod]
		public void And_the_modified_plasma_count_should_be_rounded_up_then_it_is_rounded_up()
		{
			_volume.CorrectionFactor = 1;

			Assert.AreEqual(4989.6, Math.Round(InitialPlasmaVolume * .99, 1));
			Assert.AreEqual(4990, _volume.PlasmaCount);
		}

		[TestMethod]
		public void And_the_modified_red_cell_count_should_be_rounded_down_then_it_is_rounded_down()
		{
			_volume.CorrectionFactor = 9;

			Assert.AreEqual(4586.4, Math.Round(InitialRedCellVolume * .91, 1));
			Assert.AreEqual(4586, _volume.RedCellCount);
		}

		[TestMethod]
		public void And_the_modified_red_cell_count_should_be_rounded_up_then_it_is_rounded_up()
		{
			_volume.CorrectionFactor = 1;

            Assert.AreEqual(4989.6, Math.Round(InitialRedCellVolume * .99, 1));
			Assert.AreEqual(4990, _volume.RedCellCount);
		}

		[TestMethod]
		public void And_the_modified_whole_count_is_the_sum_of_red_cell_and_plasma_volumes()
		{
			_volume.CorrectionFactor = 1;

		    var roundedPlasmaVolume = (int) Math.Round(5040 * 0.99, 0);
            var roundedRedCellVolume = (int) Math.Round(5040 * 0.99, 0);

			Assert.AreEqual(9980, roundedRedCellVolume + roundedPlasmaVolume);
			Assert.AreEqual(9980, _volume.WholeCount);
		}

		[TestMethod]
		public void And_the_modified_whole_count_should_be_rounded_up_then_it_is_rounded_up()
		{
			_volume.CorrectionFactor = 9;

            var roundedPlasmaVolume = (int)Math.Round(5040 * 0.91, 0);
            var roundedRedCellVolume = (int)Math.Round(5040 * 0.91, 0);

            Assert.AreEqual(9172, roundedRedCellVolume + roundedPlasmaVolume);
            Assert.AreEqual(9172, _volume.WholeCount);
        }
	}
}
