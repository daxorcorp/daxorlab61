﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaCompositeSample_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_a_BvaCompositeSample
    {
        [TestMethod]
        public void Then_SampleA_is_set_correctly()
        {
            // Samples given out of order to prove that order does not matter for the ctor
            var samples = new List<BVASample> 
            {
                new BVASample(1, null) { Type = SampleType.Sample1, Range = SampleRange.B },
                new BVASample(0, null) { Type = SampleType.Sample1, Range = SampleRange.A }
            };
            
            var compositeSample = new BVACompositeSample(null, samples, null);

            Assert.AreEqual(samples[1], compositeSample.SampleA, "Sample A wasn't as expected");
        }

        [TestMethod]
        public void Then_SampleB_is_set_correctly()
        {
            // Samples given out of order to prove that order does not matter for the ctor
            var samples = new List<BVASample> 
            {
                new BVASample(1, null) { Type = SampleType.Sample1, Range = SampleRange.B },
                new BVASample(0, null) { Type = SampleType.Sample1, Range = SampleRange.A }
            };

            var compositeSample = new BVACompositeSample(null, samples, null);

            Assert.AreEqual(samples[0], compositeSample.SampleB, "Sample B wasn't as expected");
        }

        [TestMethod]
        public void Then_the_test_instance_is_set_correctly()
        {
            var samples = new List<BVASample> 
            {
                new BVASample(1, null) { Type = SampleType.Sample1, Range = SampleRange.A },
                new BVASample(0, null) { Type = SampleType.Sample1, Range = SampleRange.B }
            };
            var bvaTest = new BVATest(null);

            var compositeSample = new BVACompositeSample(bvaTest, samples, null);

            Assert.AreEqual(bvaTest, compositeSample.Test, "Test instance was not as expected");
        }
    }
    // ReSharper restore InconsistentNaming
}
