﻿using System;
using System.Collections.Generic;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaCompositeSample_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_a_BvaCompositeSample
    {
        [TestMethod]
        public void And_the_collection_of_samples_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var dummy = new BVACompositeSample(null, null, null);
            }, "samples");
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_the_collection_of_samples_is_empty_then_an_exception_is_thrown()
        {
            var emptySamples = new List<BVASample>();
            // ReSharper disable UnusedVariable
            AssertEx.Throws<ArgumentOutOfRangeException>(() => { var dummy = new BVACompositeSample(null, emptySamples, null); });
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_sample_A_type_is_undefined_then_an_exception_is_thrown()
        {
            var onlySampleATypeUndefined = new List<BVASample> 
            {
                new BVASample(0, null) { Type = SampleType.Undefined, Range = SampleRange.A }, 
                new BVASample(1, null) { Type = SampleType.Sample1, Range = SampleRange.B } 
            };
            // ReSharper disable UnusedVariable
            AssertEx.Throws<NotSupportedException>(() =>
            {
                var dummy = new BVACompositeSample(null, onlySampleATypeUndefined, null);
            });
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_sample_B_type_is_undefined_then_an_exception_is_thrown()
        {
            var onlySampleBTypeUndefined = new List<BVASample> 
            {
                new BVASample(0, null) { Type = SampleType.Sample1, Range = SampleRange.A }, 
                new BVASample(1, null) { Type = SampleType.Undefined, Range = SampleRange.B } 
            };
            // ReSharper disable UnusedVariable
            AssertEx.Throws<NotSupportedException>(() =>
            {
                var dummy = new BVACompositeSample(null, onlySampleBTypeUndefined, null);
            });
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_there_is_no_sample_A_then_an_exception_is_thrown()
        {
            var onlySampleBTypeUndefined = new List<BVASample> 
            {
                new BVASample(1, null) { Type = SampleType.Sample1, Range = SampleRange.B } 
            };
            // ReSharper disable UnusedVariable
            AssertEx.Throws<NotSupportedException>(() =>
            {
                var dummy = new BVACompositeSample(null, onlySampleBTypeUndefined, null);
            });
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_there_is_no_sample_B_then_an_exception_is_thrown()
        {
            var onlySampleATypeUndefined = new List<BVASample> 
            {
                new BVASample(1, null) { Type = SampleType.Sample1, Range = SampleRange.A } 
            };
            // ReSharper disable UnusedVariable
            AssertEx.Throws<NotSupportedException>(() =>
            {
                var dummy = new BVACompositeSample(null, onlySampleATypeUndefined, null);
            });
            // ReSharper restore UnusedVariable
        }
    }
    // ReSharper restore InconsistentNaming
}
