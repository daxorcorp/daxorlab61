using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_BVA_constants
    {
        [TestMethod]
        [RequirementValue(308)]
        public void Then_the_lower_energy_bound_for_I131_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), BvaDomainConstants.I131LowerEnergyBound);
        }

        [TestMethod]
        [RequirementValue(420)]
        public void Then_the_upper_energy_bound_for_I131_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), BvaDomainConstants.I131UpperEnergyBound);
        }
    }
    // ReSharper restore InconsistentNaming
}
