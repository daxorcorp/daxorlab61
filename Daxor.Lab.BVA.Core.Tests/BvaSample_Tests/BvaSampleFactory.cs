﻿using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.BVA.Core.Tests.BvaSample_Tests
{
    public static class BvaSampleFactory
    {
        public static BVASample CreateSampleWithArbitraryPosition()
        {
            return new BVASample(3, null);
        }

        public static BVASample CreateSampleWithArbitraryPositionWithStatus(SampleExecutionStatus executionStatus)
        {
            return new BVASample(3, null) { ExecutionStatus = executionStatus };
        }
    }
}
