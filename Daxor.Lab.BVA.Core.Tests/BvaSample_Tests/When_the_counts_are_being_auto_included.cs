﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaSample_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_counts_are_being_auto_included
    {
        [TestMethod]
        public void And_the_counts_are_already_excluded_then_the_exclusion_states_are_correct()
        {
            var bvaSample = BvaSampleFactory.CreateSampleWithArbitraryPosition();
            bvaSample.IsCountAutoExcluded = true;
            bvaSample.IsCountExcluded = true;

            bvaSample.IsCountAutoExcluded = false;

            Assert.IsFalse(bvaSample.IsCountExcluded, "Count should not be excluded");
            Assert.IsFalse(bvaSample.IsCountAutoExcluded, "Count should not be auto-excluded");
        }

        [TestMethod]
        public void And_the_counts_are_not_already_excluded_then_the_exclusion_states_are_correct()
        {
            var bvaSample = BvaSampleFactory.CreateSampleWithArbitraryPosition();
            bvaSample.IsCountAutoExcluded = true;
            bvaSample.IsCountExcluded = false;

            bvaSample.IsCountAutoExcluded = false;

            Assert.IsFalse(bvaSample.IsCountExcluded, "Count should not be excluded");
            Assert.IsFalse(bvaSample.IsCountAutoExcluded, "Count should not be auto-excluded");
        }
    }
    // ReSharper restore InconsistentNaming
}
