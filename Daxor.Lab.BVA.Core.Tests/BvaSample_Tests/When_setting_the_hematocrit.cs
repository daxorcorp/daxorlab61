﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaSample_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_hematocrit
    {
        [TestMethod]
        public void Then_the_getter_works_correctly()
        {
            var bvaSample = BvaSampleFactory.CreateSampleWithArbitraryPosition();
            const double expectedHematocrit = 45.6;

            bvaSample.Hematocrit = expectedHematocrit;

            Assert.AreEqual(expectedHematocrit, bvaSample.Hematocrit);
        }

        [TestMethod]
        public void Then_PropertyChanged_is_fired()
        {
            var bvaSample = BvaSampleFactory.CreateSampleWithArbitraryPosition();
            var propertyChangedWasFired = false;
            bvaSample.PropertyChanged += (sender, args) =>
                {
                    if (args.PropertyName == "Hematocrit")
                        propertyChangedWasFired = true;
                };

            bvaSample.Hematocrit = 45.6;

            Assert.IsTrue(propertyChangedWasFired);
        }
    }
    // ReSharper restore InconsistentNaming
}
