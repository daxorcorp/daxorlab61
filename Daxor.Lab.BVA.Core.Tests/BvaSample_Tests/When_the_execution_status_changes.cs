﻿using Daxor.Lab.Domain.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaSample_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_execution_status_changes
    {
        [TestMethod]
        public void Then_PropertyChanged_is_also_fired_for_counts()
        {
            var sample = BvaSampleFactory.CreateSampleWithArbitraryPositionWithStatus(SampleExecutionStatus.Acquiring);
            var countsPropertyChangedFired = false;
            sample.PropertyChanged += (sender, args) =>
                {
                    if (args.PropertyName == "Counts")
                        countsPropertyChangedFired = true;
                };

            sample.ExecutionStatus = SampleExecutionStatus.Completed;

            Assert.IsTrue(countsPropertyChangedFired,
                          "Counts property should have been 'changed' when ExecutionStatus changed");
        }
    }
    // ReSharper restore InconsistentNaming
}
