﻿using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaSample_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_a_BVASample
    {
        [TestMethod]
        [RequirementValue(-1)]
        public void Then_the_default_hematocrit_is_matches_requirements()
        {
            var bvaSample = BvaSampleFactory.CreateSampleWithArbitraryPosition();
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), bvaSample.Hematocrit);
        }

        [TestMethod]
        public void Then_the_counts_are_not_excluded_by_default()
        {
            var bvaSample = BvaSampleFactory.CreateSampleWithArbitraryPosition();
            Assert.IsFalse(bvaSample.IsCountExcluded);
        }

        [TestMethod]
        public void Then_the_counts_are_not_auto_excluded_by_default()
        {
            var bvaSample = BvaSampleFactory.CreateSampleWithArbitraryPosition();
            Assert.IsFalse(bvaSample.IsCountAutoExcluded);
        }
    }
    // ReSharper restore InconsistentNaming
}
