﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Core.Tests.BvaSample_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_counts_are_being_auto_excluded
    {
        [TestMethod]
        public void And_the_counts_are_already_excluded_then_the_exclusion_states_are_correct()
        {
            var bvaSample = BvaSampleFactory.CreateSampleWithArbitraryPosition();
            bvaSample.IsCountAutoExcluded = false;
            bvaSample.IsCountExcluded = true;

            bvaSample.IsCountAutoExcluded = true;

            Assert.IsTrue(bvaSample.IsCountExcluded, "Count should be excluded");
            Assert.IsTrue(bvaSample.IsCountAutoExcluded, "Count should be auto-excluded");
        }

        [TestMethod]
        public void And_the_counts_are_not_already_excluded_then_the_exclusion_states_are_correct()
        {
            var bvaSample = BvaSampleFactory.CreateSampleWithArbitraryPosition();

            bvaSample.IsCountAutoExcluded = true;

            Assert.IsTrue(bvaSample.IsCountExcluded, "Count should be excluded");
            Assert.IsTrue(bvaSample.IsCountAutoExcluded, "Count should be auto-excluded");
        }
    }
    // ReSharper restore InconsistentNaming
}
