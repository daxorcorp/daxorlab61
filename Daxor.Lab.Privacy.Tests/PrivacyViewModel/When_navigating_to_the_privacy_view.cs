﻿using System.Collections.Generic;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Privacy.Common;
using Daxor.Lab.Privacy.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Privacy.Tests.PrivacyViewModel
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_navigating_to_the_privacy_view
    {
        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        public void Then_the_progress_header_is_set_correctly()
        {
            // Arrange
            var metadata = new TestExecutionContextMetadata("whatever") { DisplayTestName = "Important Test" };

            var stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubTestExecutionController.Expect(c => c.IsExecuting).Return(true);
            stubTestExecutionController.Expect(c => c.ExecutingContextMetadata).Return(metadata);
            stubTestExecutionController.Expect(c => c.ProgressMetadata).Return(new TestExecutionProgressMetadata());

            var progressSubViewModel = new ProgressSubViewModel();

            var privacyViewModel = new PrivacyViewModelThatIgnoresAggregateEvents(null, stubTestExecutionController, progressSubViewModel);
            var vmAsNavAware = (INavigationAware)privacyViewModel;

            // Act
            vmAsNavAware.NavigatingTo();

            // Assert
            Assert.AreEqual("Important Test in Progress", progressSubViewModel.ProgressHeader);
        }

        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        public void And_the_time_units_should_be_pluralized_then_the_time_units_are_pluralized()
        {
            // Arrange
            var metadata = new TestExecutionContextMetadata("whatever") { DisplayTestName = "Important Test" };

            var progressMetadata = new TestExecutionProgressMetadata();

            var stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubTestExecutionController.Expect(c => c.IsExecuting).Return(true);
            stubTestExecutionController.Expect(c => c.ExecutingContextMetadata).Return(metadata);
            stubTestExecutionController.Expect(c => c.ProgressMetadata).Return(progressMetadata);

            var progressSubViewModel = new ProgressSubViewModel();

            var privacyViewModel = new PrivacyViewModelThatIgnoresAggregateEvents(null, stubTestExecutionController, progressSubViewModel);
            var vmAsNavAware = (INavigationAware)privacyViewModel;

            // Act/Assert
            var testTimes = new List<int> {90, 120};
            foreach (var time in testTimes)
            {
                progressMetadata.TotalEstimatedExecutionTimeInSeconds = time;
                vmAsNavAware.NavigatingTo();

                StringAssert.EndsWith(progressSubViewModel.TimeUnits, privacyViewModel.TimeUnitsLabelPlural);
            }
        }

        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        public void And_the_time_units_should_be_singular_then_then_time_units_are_not_pluralized()
        {
            // Arrange
            var metadata = new TestExecutionContextMetadata("whatever") { DisplayTestName = "Important Test" };

            var progressMetadata = new TestExecutionProgressMetadata();

            var stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubTestExecutionController.Expect(c => c.IsExecuting).Return(true);
            stubTestExecutionController.Expect(c => c.ExecutingContextMetadata).Return(metadata);
            stubTestExecutionController.Expect(c => c.ProgressMetadata).Return(progressMetadata);

            var progressSubViewModel = new ProgressSubViewModel();

            var privacyViewModel = new PrivacyViewModelThatIgnoresAggregateEvents(null, stubTestExecutionController, progressSubViewModel);
            var vmAsNavAware = (INavigationAware)privacyViewModel;

            // Act/Assert
            for (int i = 1; i < 90; i++)
            {
                progressMetadata.TotalEstimatedExecutionTimeInSeconds = i;
                vmAsNavAware.NavigatingTo();
                
                StringAssert.EndsWith(progressSubViewModel.TimeUnits, privacyViewModel.TimeUnitsLabelSingular);
            }
        }

        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        public void Then_the_time_label_is_set_correctly()
        {
            // Arrange
            var metadata = new TestExecutionContextMetadata("whatever") { DisplayTestName = "Important Test" };

            var stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubTestExecutionController.Expect(c => c.IsExecuting).Return(true);
            stubTestExecutionController.Expect(c => c.ExecutingContextMetadata).Return(metadata);
            stubTestExecutionController.Expect(c => c.ProgressMetadata).Return(new TestExecutionProgressMetadata { TotalEstimatedExecutionTimeInSeconds = 1 });

            var progressSubViewModel = new ProgressSubViewModel();

            var privacyViewModel = new PrivacyViewModelThatIgnoresAggregateEvents(null, stubTestExecutionController, progressSubViewModel);
            var vmAsNavAware = (INavigationAware)privacyViewModel;

            // Act
            vmAsNavAware.NavigatingTo();

            // Assert
            StringAssert.EndsWith(progressSubViewModel.TimeLabel, privacyViewModel.EstimatedUntilCompleteLabel);
        }

        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        public void And_there_is_less_than_one_minute_remaining_on_the_test_then_the_time_remaining_shows_less_than_one()
        {
            // Arrange
            var metadata = new TestExecutionContextMetadata("whatever") { DisplayTestName = "Important Test" };

            var stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubTestExecutionController.Expect(c => c.IsExecuting).Return(true);
            stubTestExecutionController.Expect(c => c.ExecutingContextMetadata).Return(metadata);
            stubTestExecutionController.Expect(c => c.ProgressMetadata).Return(new TestExecutionProgressMetadata { TotalEstimatedExecutionTimeInSeconds = 45 });

            var progressSubViewModel = new ProgressSubViewModel();

            var privacyViewModel = new PrivacyViewModelThatIgnoresAggregateEvents(null, stubTestExecutionController, progressSubViewModel);
            var vmAsNavAware = (INavigationAware)privacyViewModel;

            // Act
            vmAsNavAware.NavigatingTo();

            // Assert
            Assert.AreEqual("<1", progressSubViewModel.TimeRemaining);
        }


        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        public void And_there_are_30_or_more_secs_remaining_for_the_minute_then_the_time_remaining_is_rounded_up()
        {
            // Arrange
            var metadata = new TestExecutionContextMetadata("whatever") { DisplayTestName = "Important Test" };

            var stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubTestExecutionController.Expect(c => c.IsExecuting).Return(true);
            stubTestExecutionController.Expect(c => c.ExecutingContextMetadata).Return(metadata);
            stubTestExecutionController.Expect(c => c.ProgressMetadata).Return(new TestExecutionProgressMetadata { TotalEstimatedExecutionTimeInSeconds = 90 });

            var progressSubViewModel = new ProgressSubViewModel();

            var privacyViewModel = new PrivacyViewModelThatIgnoresAggregateEvents(null, stubTestExecutionController, progressSubViewModel);
            var vmAsNavAware = (INavigationAware)privacyViewModel;

            // Act
            vmAsNavAware.NavigatingTo();

            // Assert
            Assert.AreEqual("2", progressSubViewModel.TimeRemaining);
        }

        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        public void And_there_are_less_than_30_secs_remaining_for_the_minute_then_the_time_remaining_is_rounded_down()
        {
            // Arrange
            var metadata = new TestExecutionContextMetadata("whatever") { DisplayTestName = "Important Test" };

            var stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubTestExecutionController.Expect(c => c.IsExecuting).Return(true);
            stubTestExecutionController.Expect(c => c.ExecutingContextMetadata).Return(metadata);
            stubTestExecutionController.Expect(c => c.ProgressMetadata).Return(new TestExecutionProgressMetadata { TotalEstimatedExecutionTimeInSeconds = 89 });

            var progressSubViewModel = new ProgressSubViewModel();

            var privacyViewModel = new PrivacyViewModelThatIgnoresAggregateEvents(null, stubTestExecutionController, progressSubViewModel);
            var vmAsNavAware = (INavigationAware)privacyViewModel;

            // Act
            vmAsNavAware.NavigatingTo();

            // Assert
            Assert.AreEqual("1", progressSubViewModel.TimeRemaining);
        }
    }
    // ReSharper restore InconsistentNaming
}
