﻿using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Privacy.Tests.PrivacyViewModel
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_the_view_model
    {
        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        [RequirementValue("Minute")]
        public void Then_the_singular_minute_label_matches_requirements()
        {
            var viewModel = new ViewModels.PrivacyViewModel(null, null, null, null);
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), viewModel.TimeUnitsLabelSingular);
        }

        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        [RequirementValue("Minutes")]
        public void Then_the_plural_minute_label_matches_requirements()
        {
            var viewModel = new ViewModels.PrivacyViewModel(null, null, null, null);
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), viewModel.TimeUnitsLabelPlural);
        }

        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        [RequirementValue("in Progress")]
        public void Then_the_in_progress_label_matches_requirements()
        {
            var viewModel = new ViewModels.PrivacyViewModel(null, null, null, null);
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), viewModel.InProgressLabel);
        }

        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        [RequirementValue("Estimated Until Complete")]
        public void Then_the_estimated_until_complete_label_matches_requirements()
        {
            var viewModel = new ViewModels.PrivacyViewModel(null, null, null, null);
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), viewModel.EstimatedUntilCompleteLabel);
        }
    }
    // ReSharper restore InconsistentNaming
}
