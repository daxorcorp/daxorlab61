﻿using System.Collections.Generic;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Privacy.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Privacy.Tests.PrivacyViewModel
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_progress_sub_view_model_sets_progress_to_visible
    {
        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        public void And_the_time_units_should_be_pluralized_then_the_time_units_are_pluralized()
        {
            // Arrange
            var contextMetadata = new TestExecutionContextMetadata("whatever") { DisplayTestName = "Important Test" };
            
            var progressMetadata = new TestExecutionProgressMetadata();

            var stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubTestExecutionController.Expect(c => c.IsExecuting).Return(true);
            stubTestExecutionController.Expect(c => c.ExecutingContextMetadata).Return(contextMetadata);
            stubTestExecutionController.Expect(c => c.ProgressMetadata).Return(progressMetadata);

            var progressSubViewModel = new ProgressSubViewModel();


            // Act/Assert
            var testTimes = new List<int> {90, 120};
            foreach (var time in testTimes)
            {
                progressMetadata.TotalEstimatedExecutionTimeInSeconds = 60; // Singular
                
                var privacyViewModel = new PrivacyViewModelThatIgnoresAggregateEvents(null, stubTestExecutionController,
                                                                                      progressSubViewModel);

                progressMetadata.TotalEstimatedExecutionTimeInSeconds = time; // Value warranting plural
                progressSubViewModel.IsProgressVisible = true;

                StringAssert.EndsWith(progressSubViewModel.TimeUnits, privacyViewModel.TimeUnitsLabelPlural);
            }
        }

        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        public void And_the_time_units_should_be_singluar_then_the_time_units_are_not_pluralized()
        {
            // Arrange
            var contextMetadata = new TestExecutionContextMetadata("whatever") { DisplayTestName = "Important Test" };

            // (Start with plural -- two minutes)
            var progressMetadata = new TestExecutionProgressMetadata();

            var stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubTestExecutionController.Expect(c => c.IsExecuting).Return(true);
            stubTestExecutionController.Expect(c => c.ExecutingContextMetadata).Return(contextMetadata);
            stubTestExecutionController.Expect(c => c.ProgressMetadata).Return(progressMetadata);

            var progressSubViewModel = new ProgressSubViewModel();


            // Act/Assert
            for (int i = 1; i < 90; i++)
            {
                progressMetadata.TotalEstimatedExecutionTimeInSeconds = 120; // Plural
                var privacyViewModel = new PrivacyViewModelThatIgnoresAggregateEvents(null, stubTestExecutionController, progressSubViewModel);

                progressMetadata.TotalEstimatedExecutionTimeInSeconds = i; //  Value warranting singular
                progressSubViewModel.IsProgressVisible = true;

                StringAssert.EndsWith(progressSubViewModel.TimeUnits, privacyViewModel.TimeUnitsLabelSingular);
            }
        }

        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        public void And_there_is_less_than_one_minute_remaining_on_the_test_then_the_time_remaining_shows_less_than_one()
        {
            // Arrange
            var contextMetadata = new TestExecutionContextMetadata("whatever") { DisplayTestName = "Important Test" };

            // (Start with plural -- two minutes)
            var progressMetadata = new TestExecutionProgressMetadata();

            var stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubTestExecutionController.Expect(c => c.IsExecuting).Return(true);
            stubTestExecutionController.Expect(c => c.ExecutingContextMetadata).Return(contextMetadata);
            stubTestExecutionController.Expect(c => c.ProgressMetadata).Return(progressMetadata);

            var progressSubViewModel = new ProgressSubViewModel();

            progressMetadata.TotalEstimatedExecutionTimeInSeconds = 120; // Initial value
            // ReSharper disable UnusedVariable
            var privacyViewModel = new PrivacyViewModelThatIgnoresAggregateEvents(null, stubTestExecutionController, progressSubViewModel);
            // ReSharper restore UnusedVariable

            // Act
            progressMetadata.TotalEstimatedExecutionTimeInSeconds = 45; // New value
            progressSubViewModel.IsProgressVisible = true;

            // Assert
            Assert.AreEqual("<1", progressSubViewModel.TimeRemaining);
        }

        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        public void And_there_are_30_or_more_secs_remaining_for_the_minute_then_the_time_remaining_is_rounded_up()
        {
            // Arrange
            var contextMetadata = new TestExecutionContextMetadata("whatever") { DisplayTestName = "Important Test" };

            // (Start with plural -- two minutes)
            var progressMetadata = new TestExecutionProgressMetadata();

            var stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubTestExecutionController.Expect(c => c.IsExecuting).Return(true);
            stubTestExecutionController.Expect(c => c.ExecutingContextMetadata).Return(contextMetadata);
            stubTestExecutionController.Expect(c => c.ProgressMetadata).Return(progressMetadata);

            var progressSubViewModel = new ProgressSubViewModel();

            progressMetadata.TotalEstimatedExecutionTimeInSeconds = 120; // Initial value
            // ReSharper disable UnusedVariable
            var privacyViewModel = new PrivacyViewModelThatIgnoresAggregateEvents(null, stubTestExecutionController, progressSubViewModel);
            // ReSharper restore UnusedVariable

            // Act
            progressMetadata.TotalEstimatedExecutionTimeInSeconds = 90; // New value
            progressSubViewModel.IsProgressVisible = true;

            // Assert
            Assert.AreEqual("2", progressSubViewModel.TimeRemaining);
        }

        [TestMethod]
        [TestCategory("Privacy")]
        [TestCategory("ViewModel")]
        public void And_there_are_less_than_30_secs_remaining_for_the_minute_then_the_time_remaining_is_rounded_down()
        {
            // Arrange
            var contextMetadata = new TestExecutionContextMetadata("whatever") { DisplayTestName = "Important Test" };

            // (Start with plural -- two minutes)
            var progressMetadata = new TestExecutionProgressMetadata();

            var stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            stubTestExecutionController.Expect(c => c.IsExecuting).Return(true);
            stubTestExecutionController.Expect(c => c.ExecutingContextMetadata).Return(contextMetadata);
            stubTestExecutionController.Expect(c => c.ProgressMetadata).Return(progressMetadata);

            var progressSubViewModel = new ProgressSubViewModel();

            progressMetadata.TotalEstimatedExecutionTimeInSeconds = 120; // Initial value
            // ReSharper disable UnusedVariable
            var privacyViewModel = new PrivacyViewModelThatIgnoresAggregateEvents(null, stubTestExecutionController, progressSubViewModel);
            // ReSharper restore UnusedVariable

            progressMetadata.TotalEstimatedExecutionTimeInSeconds = 89; // New value
            progressSubViewModel.IsProgressVisible = true;

            // Assert
            Assert.AreEqual("1", progressSubViewModel.TimeRemaining);
        }
    }
    // ReSharper restore InconsistentNaming
}
