using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Privacy.ViewModels;
using Microsoft.Practices.Composite.Events;

namespace Daxor.Lab.Privacy.Tests.PrivacyViewModel
{
    internal class PrivacyViewModelThatIgnoresAggregateEvents : ViewModels.PrivacyViewModel
    {
        internal PrivacyViewModelThatIgnoresAggregateEvents(IEventAggregator eventAggregator, 
                                                            ITestExecutionController testExecutionController,
                                                            ProgressSubViewModel vm) : base(null, eventAggregator, testExecutionController, vm)
        {
        }

        protected override void SubscribeToAggregrateEvents()
        {
            // Do nothing.
        }
    }
}