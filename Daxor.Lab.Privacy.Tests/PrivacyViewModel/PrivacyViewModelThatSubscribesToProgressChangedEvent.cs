﻿using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Privacy.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Privacy.Tests.PrivacyViewModel
{
    public class PrivacyViewModelThatSubscribesToProgressChangedEvent : ViewModels.PrivacyViewModel
    {
        internal PrivacyViewModelThatSubscribesToProgressChangedEvent(IEventAggregator eventAggregator, 
                                                            ITestExecutionController testExecutionController,
                                                            ProgressSubViewModel vm) : base(null, eventAggregator, testExecutionController, vm)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            SubscribeToAggregrateEvents();
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        protected override void SubscribeToAggregrateEvents()
        {
            // Only subscribe to progress changed event
            SubscribeToTestExecutionProgressChangedEvent(ThreadOption.PublisherThread);
        }
    }
}
