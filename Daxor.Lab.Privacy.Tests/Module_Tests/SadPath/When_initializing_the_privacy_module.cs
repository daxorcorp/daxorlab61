﻿using System;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Privacy.Views;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Privacy.Tests.Module_Tests.SadPath
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_the_Privacy_module
	{
		private Module _module;
		private IEventAggregator _eventAggregator;
		private IRegionManager _regionManager;
		private IUnityContainer _container;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_container = Substitute.For<IUnityContainer>();
			_eventAggregator = Substitute.For<IEventAggregator>();
			_regionManager = Substitute.For<IRegionManager>();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_privacy_view_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IActiveAware, PrivacyView>(Arg.Any<string>())).Throw(new Exception());
			_module = new Module(_container, _eventAggregator, _regionManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Privacy", ex.ModuleName);
				Assert.AreEqual("initialize the Privacy view", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_resolving_the_privacy_module_controller_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<PrivacyModuleController>()).Throw(new Exception());
			_module = new Module(_container, _eventAggregator, _regionManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Privacy", ex.ModuleName);
				Assert.AreEqual("initialize the Privacy Module Controller", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}
	}
	// ReSharper restore InconsistentNaming
}