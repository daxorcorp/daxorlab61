﻿using Daxor.Lab.ApplicationServices.Interfaces;
using Daxor.Lab.ApplicationServices.Proxies;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.SCContracts;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.ApplicationServices.Hardware
{
    /// <summary>
    /// Represents a sample changer whose behavior is carried out by a
    /// physical (i.e., hardware) sample changer.
    /// </summary>
    public class SampleChanger : SampleChangerWcfProxy, ISampleChanger, ISampleChangerServiceCallbackEvents
    {
        #region Constants

        // TODO: This should be obtained through a better mechanism, maybe Unity?
        private const string SampleChangerEndpointConfigurationName = "scTCP";

        /// <summary>
        /// Disconnect message for when IDetector.Disconnect() was called explicitly.
        /// </summary>
        public const string ExplicitDisconnectMessage = "Disconnect() was explicitly called on SampleChanger.";

        /// <summary>
        /// Disconnect message for when the WCF proxy cannot be reached.
        /// </summary>
        public const string ProxyFailureMessage = "Unable to communicate with the WCF service.";

        #endregion

        #region Ctor

        /// <summary>
        /// Creates a new SampleChanger instance that communicates with a 
        /// physical (i.e., hardware) sample changer through the proxy
        /// instance that this class extends.
        /// </summary>
        /// <param name="logger">Event logger instance</param>
        public SampleChanger(ILoggerFacade logger) : base(SampleChangerEndpointConfigurationName, logger)
        {
            SampleChangerProxy.ServiceProxyOpened += (o, e) => FireConnectedEvent();
            SampleChangerProxy.ServiceProxyFailed += (o, e) => FireDisconnectedEvent(ProxyFailureMessage);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the sample changer proxy instance.
        /// </summary>
        private ISampleChangerProxy SampleChangerProxy
        {
            get { return this; }
        }

        /// <summary>
        /// Position that the sample changer is moving to
        /// </summary>
        private int GoalPosition {get; set;}

        #endregion

        #region ISampleChanger members

        #region Events

        /// <summary>
        /// Occurs when the sample changer has been connected from the computer.
        /// </summary>
        public event SampleChangerConnectedHandler Connected;

        /// <summary>
        /// Occurs when the sample changer has been disconnected from the computer.
        /// </summary>
        public event SampleChangerDisconnectedHandler Disconnected;

        /// <summary>
        /// Occurs when the sample changer encounters an error.
        /// </summary>
        public event ErrorOccurredHandler ErrorOccurred;

        /// <summary>
        /// Occurs when the sample changer starts changing to another position.
        /// </summary>
        public event PositionChangeBeginHandler PositionChangeBegin;

        /// <summary>
        /// Occurs when the sample changer has finished changing to a position.
        /// </summary>
        public event PositionChangeEndHandler PositionChangeEnd;

        /// <summary>
        /// Occurs when the sample changer starts seeking a goal position.
        /// </summary>
        public event PositionSeekBeginHandler PositionSeekBegin;

        /// <summary>
        /// Occurs when the sample changer has cancelled a seek operation.
        /// </summary>
        public event PositionSeekCancelHandler PositionSeekCancelled;

        /// <summary>
        /// Occurs when the sample changer has finished seeking a goal position.
        /// </summary>
        public event PositionSeekEndHandler PositionSeekEnd;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the sample changer's current position.
        /// </summary>
        public int CurrentPosition
        {
            get { return SampleChangerProxy.GetCurrentPosition(); }
        }

        /// <summary>
        /// Gets whether the sample changer is moving.
        /// </summary>
        public new bool IsMoving
        {
            get { return SampleChangerProxy.IsMoving(); }
        }

        /// <summary>
        /// Gets the number of positions the changer is capable of accessing
        /// </summary>
        public int NumberOfPositions
        {
            get { return 25; }
        }

        /// <summary>
        /// Gets the number of milliseconds required to move from one position to
        /// the adjacent position
        /// </summary>
        public int PositionChangeDelayInMilliseconds
        {
            get { return 10850; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Connects the sample changer by opening and subscribing to the proxy sample changer.
        /// </summary>
        public void Connect()
        {
            SampleChangerProxy.OpenAndSubscribe();
            FireConnectedEvent();
        }

        /// <summary>
        /// Computes the number moves required to get from the given starting position to
        /// the given ending position based on a carousel-style sample changer that only 
        /// moves clockwise
        /// </summary>
        /// <param name="startPosition">Starting position</param>
        /// <param name="endPosition">Ending position</param>
        /// <returns>Number of moves to get from the starting position to the ending position</returns>
        public int GetNumberOfMovesBetween(int startPosition, int endPosition)
        {
            if (startPosition > endPosition)
                return endPosition + (NumberOfPositions - startPosition);

            return endPosition - (startPosition % NumberOfPositions);
        }

        /// <summary>
        /// Disconnects the sample changer by unsubscribing from the proxy sample changer.
        /// </summary>
        public void Disconnect()
        {
            SampleChangerProxy.Unsubscribe();
            FireDisconnectedEvent(ExplicitDisconnectMessage);
        }

        /// <summary>
        /// Moves the sample changer to a given goal position.
        /// </summary>
        /// <param name="goalPosition">Goal position</param>
        public void MoveTo(int goalPosition)
        {
            GoalPosition = goalPosition;
            SampleChangerProxy.MoveToPosition(goalPosition);
        }

        /// <summary>
        /// Stops the sample changer
        /// </summary>
        public void Stop()
        {
            if (IsMoving)
            {
                FirePositionSeekEnd(CurrentPosition);
                FirePositionSeekCancel(CurrentPosition, GoalPosition);
            }
            SampleChangerProxy.StopSampleChanger();
        }

        #endregion

        #endregion

        #region Callback handlers

        /// <summary>
        /// Forwards the ErrorOccurred callback event to the local handler.
        /// </summary>
        /// <param name="errorCode">Error code</param>
        /// <param name="message">Error message</param>
        void ISampleChangerServiceCallbackEvents.OnChangeErrorCallbackEvent(int errorCode, string message)
        {
            FireErrorOccurredEvent(errorCode, message);
        }

        /// <summary>
        /// Forwards the PositionChangeBegin callback event to the local handler.
        /// </summary>
        /// <param name="oldPosition">Old position</param>
        /// <param name="newPosition">New position</param>
        void ISampleChangerServiceCallbackEvents.OnPositionChangeBeginCallbackEvent(int oldPosition, int newPosition)
        {
            FirePositionChangeBegin(oldPosition, newPosition);
        }

        /// <summary>
        /// Forwards the PositionChangeEnd callback event to the local handler.
        /// </summary>
        /// <param name="currPosition">Current position (what the position has changed to)</param>
        void ISampleChangerServiceCallbackEvents.OnPositionChangeEndCallbackEvent(int currPosition)
        {
            FirePositionChangeEnd(currPosition);
        }

        /// <summary>
        /// Forwards the PositionSeekBegin callback event to the local handler.
        /// </summary>
        /// <param name="currPosition">Current position</param>
        /// <param name="goalPosition">Goal position</param>
        void ISampleChangerServiceCallbackEvents.OnPositionSeekBeginCallbackEvent(int currPosition, int goalPosition)
        {
            FirePositionSeekBegin(currPosition, goalPosition);
        }

        /// <summary>
        /// Forwards the PositionSeekCancel callback event to the local handler.
        /// </summary>
        /// <param name="currPosition">Current position</param>
        /// <param name="goalPosition">Goal position</param>
        void ISampleChangerServiceCallbackEvents.OnPositionSeekCancelCallbackEvent(int currPosition, int goalPosition)
        {
            FirePositionSeekCancel(currPosition, goalPosition);
        }

        /// <summary>
        /// Forwards the PositionSeekEnd callback event to the local handler.
        /// </summary>
        /// <param name="currPosition">Current position (what the position has changed to)</param>
        void ISampleChangerServiceCallbackEvents.OnPositionSeekEndCallbackEvent(int currPosition)
        {
            FirePositionSeekEnd(currPosition);
        }

        #endregion

        #region Protected

        // These are virtual so that subclasses can override them if desired.

        /// <summary>
        /// Raises the Connected event.
        /// </summary>
        protected virtual void FireConnectedEvent()
        {
            var handler = Connected;
            if (handler != null)
                handler();

            Logger.Log("SampleChanger.Connected event fired", Category.Info, Priority.None);
        }

        /// <summary>
        /// Raises the Disconnected event.
        /// </summary>
        /// <param name="reason">Reason the sample changer was disconnected</param>
        protected virtual void FireDisconnectedEvent(string reason)
        {
            var handler = Disconnected;
            if (handler != null)
                handler(reason);

            Logger.Log("SampleChanger.Disconnected event fired. Reason: " + reason, Category.Info, Priority.None);
        }

        /// <summary>
        /// Raises the ErrorOccurred event.
        /// </summary>
        /// <param name="errorCode">Error code</param>
        /// <param name="message">Error message</param>
        protected virtual void FireErrorOccurredEvent(int errorCode, string message)
        {
            var handler = ErrorOccurred;
            if (handler != null)
                handler(errorCode, message);

            Logger.Log("SampleChanger.ErrorOccurred event fired with error code '" +
                errorCode + "' and message '" + message + "'", Category.Exception, Priority.High);
        }

        /// <summary>
        /// Raises the PositionChangeBegin event.
        /// </summary>
        /// <param name="currPosition">Old position</param>
        /// <param name="newPosition">New position</param>
        protected virtual void FirePositionChangeBegin(int currPosition, int newPosition)
        {
            var handler = PositionChangeBegin;
            if (handler != null)
                handler(currPosition, newPosition);

            Logger.Log("SampleChanger.PositionChangeBegin event fired with current position '" +
                currPosition + "' and new position '" + newPosition + "'", Category.Info, Priority.None);
        }

        /// <summary>
        /// Raises the PositionChangeEnd event.
        /// </summary>
        /// <param name="currPosition">Current position (what the position has changed to)</param>
        protected virtual void FirePositionChangeEnd(int currPosition)
        {
            var handler = PositionChangeEnd;
            if (handler != null)
                handler(currPosition);

            Logger.Log("SampleChanger.PositionChangeEnd event fired with current position '" +
                currPosition + "'", Category.Info, Priority.None);
        }

        /// <summary>
        /// Raises the PositionSeekBegin event.
        /// </summary>
        /// <param name="currPosition">Current position</param>
        /// <param name="goalPosition">Goal position</param>
        protected virtual void FirePositionSeekBegin(int currPosition, int goalPosition)
        {
            var handler = PositionSeekBegin;
            if (handler != null)
                handler(currPosition, goalPosition);

            Logger.Log("SampleChanger.PositionSeekBegin event fired with current position '" +
                currPosition + "' and goal position '" + goalPosition + "'", Category.Info, Priority.None);
        }

        /// <summary>
        /// Raises the PositionSeekCancel event.
        /// </summary>
        /// <param name="currPosition">Current position</param>
        /// <param name="goalPosition">Goal position</param>
        protected virtual void FirePositionSeekCancel(int currPosition, int goalPosition)
        {
            var handler = PositionSeekCancelled;
            if (handler != null)
                handler(currPosition, goalPosition);

            Logger.Log("SampleChanger.PositionSeekCancel event fired with current position '" +
                currPosition + "' and goal position '" + goalPosition + "'", Category.Info, Priority.None);
        }

        /// <summary>
        /// Raises the PositionSeekEnd event.
        /// </summary>
        /// <param name="currPosition">Current position (what the position has changed to)</param>
        protected virtual void FirePositionSeekEnd(int currPosition)
        {
            var handler = PositionSeekEnd;
            if (handler != null)
                handler(currPosition);
            
            Logger.Log("SampleChanger.PositionSeekEnd event fired with current position '" +
                currPosition + "'", Category.Info, Priority.None);
        }

        #endregion

    }
}

