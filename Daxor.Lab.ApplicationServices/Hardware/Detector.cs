﻿using System;
using System.Linq;
using System.Text;
using Daxor.Lab.ApplicationServices.Interfaces;
using Daxor.Lab.ApplicationServices.Proxies;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.MCAContracts;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.ApplicationServices.Hardware
{
    /// <summary>
    /// Represents a detector whose behavior is carried out by a
    /// physical (i.e., hardware) gamma counter.
    /// </summary>
    public class Detector : DetectorWcfProxy, IDetector, IConfigurable, IMultiChannelAnalyzerServiceCallbackEvents
    {
        #region Constants

        // TODO: This should be obtained through a better mechanism, maybe Unity?
        private const string LocalEndpointConfigurationName = "mcaTCP";

        /// <summary>
        /// Disconnect message for when IDetector.Disconnect() was called explicitly.
        /// </summary>
        private const string ExplicitDisconnectMessage = "Disconnect() was explicitly called on Detector.";

        /// <summary>
        /// Disconnect message for when the WCF proxy cannot be reached.
        /// </summary>
        private const string ProxyFailureMessage = "Unable to communicate with the WCF service.";

        /// <summary>
        /// Disconnect message for when the WCF proxy cannot communicate with the USB gamma counter.
        /// </summary>
        private const string UsbFailureMessage = "The USB device was unplugged.";

        #endregion

        #region Fields
        private bool _isStillConnected;
        private readonly string _detectorName;
     
        #endregion

        #region Ctor

        /// <summary>
        /// Creates a new Detector instance that communicates with a
        /// physical (i.e., hardware) detector through the proxy instance that
        /// this class extends.
        /// </summary>
        /// <param name="logger">Event logger instance</param>
        public Detector(ILoggerFacade logger)
            : base(LocalEndpointConfigurationName, logger)
        {
            // NOTE: This just grabs the first available MCA from the service
            _detectorName = DetectorProxy.GetAllMultiChannelAnalyzerIds().FirstOrDefault();
              
            DetectorProxy.ServiceProxyOpened += (o, e) => FireConnectedEvent(DetectorName);
            DetectorProxy.ServiceProxyFailed += (o, e) => FireDisconnectedEvent(DetectorName, ProxyFailureMessage);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the unique detector name.
        /// </summary>
        public string DetectorName
        {
            get { return _detectorName; }
        }

        /// <summary>
        /// Gets the detector proxy instance.
        /// </summary>
        public IDetectorProxy DetectorProxy
        {
            get { return this; }
        }

        #endregion

        #region IDetector members

        #region Events

        /// <summary>
        /// Occurs when the spectrum acquisition has completed.
        /// </summary>
        public event AcquisitionCompletedHandler AcquisitionCompleted;

        /// <summary>
        /// Occurs when spectrum acquisition has started.
        /// </summary>
        public event AcquisitionStartedHandler AcquisitionStarted;

        /// <summary>
        /// Occurs when spectrum acquisition has stopped.
        /// </summary>
        public event AcquisitionStoppedHandler AcquisitionStopped;

        /// <summary>
        /// Occurs when the detector is connected to the computer.
        /// </summary>
        public event DetectorConnectedHandler Connected;

        /// <summary>
        /// Occurs when the detector has been disconnected from the computer.
        /// </summary>
        public event DetectorDisconnectedHandler Disconnected;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the detector's analog-to-digital (ADC) current voltage.
        /// </summary>
        public double AdcCurrent
        {
            get 
            {
                ThrowExceptionIfNotConnected("adcCurrent can be accessed");
                return DetectorProxy.GetAnalogToDigitalConverterHighVoltage(DetectorName); 
            }
        }

        /// <summary>
        /// Gets or sets the detector's fine gain.
        /// </summary>
        public double FineGain
        {
            get 
            {
                ThrowExceptionIfNotConnected("fine gain can be accessed");
                return DetectorProxy.GetFineGain(DetectorName); 
            }
            set 
            {
                ThrowExceptionIfNotConnected("fine gain can be set");
                DetectorProxy.SetFineGain(DetectorName, value); 
            }
        }

        /// <summary>
        /// Gets whether the spectrum acquisition has completed.
        /// </summary>
        public new bool HasAcquisitionCompleted
        {
            get 
            {
                ThrowExceptionIfNotConnected("acquisition completed");
                return DetectorProxy.HasAcquisitionCompleted(DetectorName); 
            }
        }

        /// <summary>
        /// Gets whether or not the detector is currently acquiring a spectrum.
        /// </summary>
        public new bool IsAcquiring
        {
            get 
            {
                ThrowExceptionIfNotConnected("detector is still acquiring");
                return DetectorProxy.IsAcquiring(DetectorName); 
            }
        }

        /// <summary>
        /// Gets whether or not the detector proxy is open and the detector is connected.
        /// </summary>
        public new bool IsConnected
        {
            get 
            {
                ThrowExceptionIfNotConnected("detector is connected");
                return DetectorProxy.IsOpen && DetectorProxy.IsConnected(DetectorName); 
            }
        }

        /// <summary>
        /// Gets whether or not the gross counts for acquisition have been preset.
        /// </summary>
        public bool IsGrossCountsPreset
        {
            get 
            {
                ThrowExceptionIfNotConnected("gross counts are preset");
                return DetectorProxy.IsGrossCountPreset(DetectorName); 
            }
        }

        /// <summary>
        /// Gets whether or not the live time for acquisition has been preset.
        /// </summary>
        public new bool IsLiveTimePreset
        {
            get 
            {
                ThrowExceptionIfNotConnected("live time has been preset");
                return DetectorProxy.IsLiveTimePreset(DetectorName); 
            }
        }

        /// <summary>
        /// Gets whether or not the real time for acquisition has been preset.
        /// </summary>
        public new bool IsRealTimePreset
        {
            get 
            {
                ThrowExceptionIfNotConnected("real time is preset");
                return DetectorProxy.IsRealTimePreset(DetectorName); 
            }
        }

        /// <summary>
        /// Gets or sets the detector's low level discriminator value.
        /// </summary>
        public int LowLevelDiscriminator
        {
            get 
            {
                ThrowExceptionIfNotConnected("low level discriminator can be accessed");
                return DetectorProxy.GetLowerLevelDiscriminator(DetectorName); 
            }
            set 
            {
                ThrowExceptionIfNotConnected("low level discriminator can be set");
                DetectorProxy.SetLowerLevelDiscriminator(DetectorName, value); 
            }
        }

        /// <summary>
        /// Gets or sets the detector's noise value
        /// </summary>
        public int Noise
        {
            get 
            {
                ThrowExceptionIfNotConnected("noise can be accessed");
                return DetectorProxy.GetNoiseLevel(DetectorName); 
            }
            set 
            {
                ThrowExceptionIfNotConnected("noise can be set");
                DetectorProxy.SetNoiseLevel(DetectorName, value); 
            }
        }

        /// <summary>
        /// Gets the detector's serial number.
        /// </summary>
        public int SerialNumber
        {
            get 
            {
                ThrowExceptionIfNotConnected("serial number can be accessed");
                return DetectorProxy.GetSerialNumber(DetectorName); 
            }
        }

        /// <summary>
        /// Gets the detector's current spectrum.
        /// </summary>
        public Spectrum Spectrum
        {
            get 
            {
                ThrowExceptionIfNotConnected("specturm can be accessed");
                return SpectrumMapper.From(DetectorProxy.GetSpectrumData(DetectorName)); 
            }
        }

        /// <summary>
        /// Gets the length of the detector's current spectrum (in channels).
        /// </summary>
        public int SpectrumLength
        {
            get 
            {
                ThrowExceptionIfNotConnected("spectrum length can be accessed");
                return DetectorProxy.GetSpectrumLength(DetectorName); 
            }
        }

        /// <summary>
        /// Gets or sets the detector's current voltage.
        /// </summary>
        public int Voltage
        {
            get 
            {
                ThrowExceptionIfNotConnected("voltage can be accessed");
                return DetectorProxy.GetHighVoltage(DetectorName); 
            }
            set 
            {
                ThrowExceptionIfNotConnected("voltage can be set");
                DetectorProxy.SetHighVoltage(DetectorName, value); 
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Connects the detector by opening and subscribing to the proxy detector.
        /// </summary>
        public void Connect()
        {
            DetectorProxy.OpenAndSubscribe();
            
            _isStillConnected = true;
            FireConnectedEvent(DetectorName);
        }

        /// <summary>
        /// Disconnects the detector by unsubscribing from the proxy.
        /// </summary>
        public void Disconnect()
        {
            DetectorProxy.StopAcquisition(DetectorName);
            DetectorProxy.Unsubscribe();
            FireDisconnectedEvent(DetectorName, ExplicitDisconnectMessage);
        }

        /// <summary>
        /// Resets the detector's current spectrum and resets the time values.
        /// </summary>
        public void ResetSpectrum()
        {
            DetectorProxy.ClearSpectrumAndPresets(DetectorName);
        }

        /// <summary>
        /// Starts acquiring a spectrum for a given number of seconds.
        /// </summary>
        /// <param name="timeInSeconds">During of acquisition (in seconds)</param>
        /// <param name="isLiveTime">Whether the live time should be timeInSeconds</param>
        public void StartAcquiring(int timeInSeconds, bool isLiveTime = true)
        {
            ResetSpectrum();
            DetectorProxy.PresetTime(DetectorName, isLiveTime, timeInSeconds);
            DetectorProxy.StartAcquisition(DetectorName);
        }

        /// <summary>
        /// Starts acquiring until a given number of gross counts have been obtained
        /// within the specified channel range.
        /// </summary>
        /// <param name="grossCounts">Goal counts to acquire</param>
        /// <param name="startChannel">Starting channel</param>
        /// <param name="endChannel">Ending channel</param>
        public void StartAcquiring(uint grossCounts, int startChannel, int endChannel)
        {
            ResetSpectrum();
            DetectorProxy.PresetGrossCountInRegionOfInterest(DetectorName, grossCounts, startChannel, endChannel);
        }

        /// <summary>
        /// Stops the spectrum acquisition.
        /// </summary>
        public void StopAcquiring()
        {
            DetectorProxy.StopAcquisition(DetectorName);
        }

        #endregion

        #endregion

        #region IConfigurable members

        /// <summary>
        /// Saves the detector configuration.
        /// </summary>
        public void SaveConfiguration()
        {
            DetectorProxy.SaveConfigurationsForAllMultiChannelAnalyzers();
        }

        /// <summary>
        /// Resets the detector configuration.
        /// </summary>
        public void ResetConfiguration()
        {
            DetectorProxy.ResetMultiChannelAnalyzerToDefaultConfiguration(_detectorName);
        }

        #endregion

        #region Callback handlers

        /// <summary>
        /// Forwards the AcquisitionCompleted callback event to the local handler.
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that completed its acquisition</param>
        /// <param name="completedSpectrumData">Spectrum data that was acquired</param>
        void IMultiChannelAnalyzerServiceCallbackEvents.AcquisitionCompleted(string mcaId, SpectrumData completedSpectrumData)
        {
            FireAcquisitionCompletedEvent(mcaId, SpectrumMapper.From(completedSpectrumData));
        }

        /// <summary>
        /// Forwards the AcquisitionStarted callback event to the local handler.
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that started acquiring</param>ed with the event</param>
        /// <param name="startTime">Time at which acquisition started</param>
        void IMultiChannelAnalyzerServiceCallbackEvents.AcquisitionStarted(string mcaId, DateTime startTime)
        {
            FireAcquisitionStartedEvent(mcaId, startTime);
        }

        /// <summary>
        /// Forwards the AcquisitionStopped callback event to the local handler.
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA that stopped acquiring</param>
        /// <param name="stopTime">Time at which acquisition stopped</param>
        void IMultiChannelAnalyzerServiceCallbackEvents.AcquisitionStopped(string mcaId, DateTime stopTime)
        {
            FireAcquisitionStoppedEvent(mcaId, stopTime);
        }

        /// <summary>
        /// Forwards the McaConnected callback event to the local handler.
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that was connected</param>
        void IMultiChannelAnalyzerServiceCallbackEvents.MultiChannelAnalyzerConnected(string mcaId)
        {
            FireConnectedEvent(mcaId);
        }

        /// <summary>
        /// Forwards the McaDisconnected callback event to the local handler.
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that was disconnected</param>
        void IMultiChannelAnalyzerServiceCallbackEvents.MultiChannelAnalyzerDisconnected(string mcaId)
        {
            FireDisconnectedEvent(mcaId, UsbFailureMessage);
        }

        #endregion

        #region Event raisers
        // These are virtual so that subclasses can override them if desired.

        /// <summary>
        /// Raises the AcquisitionCompleted event.
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that completed its acquisition</param>
        /// <param name="completedSpectrum">Spectrum that was acquired</param>
        protected virtual void FireAcquisitionCompletedEvent(string mcaId, Spectrum completedSpectrum)
        {
            var handler = AcquisitionCompleted;
            if (handler != null)
                handler(mcaId, completedSpectrum);

            Logger.Log("Detector.AcquisitionCompleted event fired for detector '" + mcaId + "'", Category.Info, Priority.High);
        }

        /// <summary>
        /// Raises the AcquisitionStarted event.
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that started acquiring</param>
        /// <param name="startTime">Time at which acquisition started</param>
        protected virtual void FireAcquisitionStartedEvent(string mcaId, DateTime startTime)
        {
            var handler = AcquisitionStarted;
            if (handler != null)
                handler(mcaId, startTime);

            Logger.Log("Detector.AcquisitionStarted event fired for detector '" + mcaId + 
                "' at time " + startTime, Category.Info, Priority.High);
        }

        /// <summary>
        /// Raises the AcquisitionStopped event.
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA that stopped acquiring</param>
        /// <param name="stopTime">Time at which acquisition stopped</param>
        protected virtual void FireAcquisitionStoppedEvent(string mcaId, DateTime stopTime)
        {
            var handler = AcquisitionStopped;
            if (handler != null)
                handler(mcaId, stopTime);

            Logger.Log("Detector.AcquisitionStopped event fired for detector '" + mcaId + "' at time " + stopTime, Category.Info, Priority.High);
        }

        /// <summary>
        /// Raises the Connected event.
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA associated with the event</param>
        protected virtual void FireConnectedEvent(string mcaId)
        {
            var handler = Connected;
            if (handler != null)
                handler(mcaId);

            Logger.Log("Detector.Connected event fired for detector '" + mcaId + "'", Category.Info, Priority.High);
        }

        /// <summary>
        /// Raises the Disconnected event.
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that was disconnected</param>
        /// <param name="reason">Reason the detector was disconnected</param>
        protected virtual void FireDisconnectedEvent(string mcaId, string reason)
        {
            _isStillConnected = false;   // Cameron: We need to set this to false before we let the handler go to work

            var handler = Disconnected;

            if (handler != null)
                handler(mcaId, reason);

            Logger.Log("Detector.Disconnected event fired for detector '" + mcaId + "'. Reason: " + reason, Category.Info, Priority.High);
        }

        #endregion

        #region ToString()

        public override string ToString()
        {
            var value = new StringBuilder();

            value.Append("Daxor.Lab.ApplicationServices.Hardware.Detector:\n");
            value.Append("  ADC current: " + AdcCurrent + "\n");
            value.Append("  Fine gain: " + FineGain + "\n");
            value.Append("  Acquisition completed? " + (HasAcquisitionCompleted ? "Yes" : "No") + "\n");
            value.Append("  Acquiring? " + (IsAcquiring ? "Yes" : "No") + "\n");
            value.Append("  Gross counts preset? " + (IsGrossCountsPreset ? "Yes" : "No") + "\n");
            value.Append("  Live time preset? " + (IsLiveTimePreset ? "Yes" : "No") + "\n");
            value.Append("  Real time preset? " + (IsRealTimePreset ? "Yes" : "No") + "\n");
            value.Append("  LLD: " + LowLevelDiscriminator + "\n");
            value.Append("  Noise: " + Noise + "\n");
            value.Append("  Serial number: " + SerialNumber + "\n");
            value.Append("  Spectrum length: " + SpectrumLength + "\n");
            value.Append("  High voltage: " + Voltage + "\n");

            return value.ToString();
        }
        
        #endregion

        #region Helper Methods
        private void ThrowExceptionIfNotConnected(string info)
        {
            if (!_isStillConnected)
                throw new Exception("The detector is no longer connected, can't determine if the " + info + " or not.");
        }
        #endregion
    }
}
