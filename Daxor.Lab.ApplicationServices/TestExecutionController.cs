﻿using System;
using System.ComponentModel;
using System.Threading;
using Daxor.Lab.ApplicationServices.Interfaces;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.ApplicationServices
{
    public class TestExecutionController : ITestExecutionController
    {
        #region Constants

        private const int WorkerThreadDelayInMilliseconds = 1000;

        #endregion

        #region Fields

        private readonly object _objectLock = new object();
        private readonly ISampleChanger _sampleChanger;
        private readonly IEventAggregator _eventAggregator;
        private readonly IBackgroundAcquisitionService _backgroundAcquisitionService;
        private readonly ILoggerFacade _logger;
        private readonly BackgroundWorker _testExecutionThread = new BackgroundWorker();
        private TestExecutionStep _testExecutionStep;
        private bool _didSampleChangerErrorOccur;

        #endregion

        #region Ctor

        public TestExecutionController(ISampleChanger sampleChanger, IEventAggregator eventAggregator,
                                    IBackgroundAcquisitionService backgroundAcquisitionService, 
                                    IExecutionTimeHelper executionTimeHelper,
                                    ILoggerFacade logger)
        {
            _sampleChanger = sampleChanger;
            _eventAggregator = eventAggregator;
            _backgroundAcquisitionService = backgroundAcquisitionService;
            ExecutionTimeHelper = executionTimeHelper;
            _logger = logger;

            // Set up the background worker thread that controlls the actual execution.
            _testExecutionThread.WorkerSupportsCancellation = true;
            _testExecutionThread.DoWork += TestExecutionThread_DoWork;
            _testExecutionThread.Disposed += TestExecutionThread_Disposed;
            _testExecutionThread.RunWorkerCompleted += TestExecutionThread_RunWorkerCompleted;

            _eventAggregator.GetEvent<DetectorDisconnected>().Subscribe(OnDetectorDisconnected);
        }

        #endregion

        #region Properties

        private ITestExecutionContext Context { get; set; }
        private ISample CurrentRunningSample { get; set; }
        private IExecutionTimeHelper ExecutionTimeHelper { get; set; }
        private bool RecountingSameSample { get; set; }
        private bool WasBackgroundAcquisitionServiceRestarted { get; set; }
        private int ElapsedBackgroundAcquisitionTimeInMilliseconds { get; set; }

        public Boolean DidDetectorBecomeDisconnected { get; private set; }

        public TestExecutionControllerStatus ControllerStatus
        {
            get;
            private set;
        }

        public TestExecutionStep ExecutionStep
        {
            get { return _testExecutionStep; }
            private set
            {
                lock (_objectLock)
                {
                    _testExecutionStep = value;
                }
            }
        }
      
        public TestExecutionProgressMetadata ProgressMetadata
        {
            get;
            private set;
        }
        #endregion

        #region Background worker methods

        void TestExecutionThread_DoWork(object sender, DoWorkEventArgs e)
        {
            _didSampleChangerErrorOccur = false;
            DidDetectorBecomeDisconnected = false;
            var context = e.Argument as ITestExecutionContext;
            if (context == null)
                throw new NotSupportedException("ITestExecutionContext cannot be null when executing a test (DoWork method)");
            var timeline = BuildExecutionTimeline(context);

            var expectedBackgroundAcquisitionTimeInMilliseconds = 0;
            var backgroundAcquisitionEvent = timeline.GetEventForTestExecutionStep(TestExecutionStep.CountBackground);
            if (backgroundAcquisitionEvent != null)
            {
                expectedBackgroundAcquisitionTimeInMilliseconds = backgroundAcquisitionEvent.EndTimeInMilliseconds -
                                                                  backgroundAcquisitionEvent.BeginTimeInMilliseconds;
            }

            ProgressMetadata = new TestExecutionProgressMetadata
                                   {
                                       TotalEstimatedExecutionTimeInSeconds = timeline.TotalExecutionTimeInMilliseconds / (int) TimeConstants.MillisecondsPerSecond,
                                       ElapsedExecutionTimeInSeconds = 0
                                   };
            AttachToHardwareEvents();
            _backgroundAcquisitionService.RestartedBecauseOfHighBackground += OnBackgroundAcquisitionServiceRestartedBecauseOfHighBackground;

            ControllerStatus = TestExecutionControllerStatus.TestInProgress;
            ExecutionStep = TestExecutionStep.Unknown;

            try
            {
                //Execute test until its completion
                while (ControllerStatus == TestExecutionControllerStatus.TestInProgress)
                {
                    //Test execution cancellation
                    if (_testExecutionThread.CancellationPending)
                    {
                        e.Cancel = true;
                        ControllerStatus = TestExecutionControllerStatus.TestAborted;
                        ExecutionStep = TestExecutionStep.Unknown;
                        break;
                    }
                    //Determine next step. Returns true if more work remains to be done, false otherwise.
                    if (!DetermineNextSamplesToProcess(context))
                        break;

                    EnsureBackgroundAcquistionStep(context);
                    PerformBackgroundAcquisitionStep(context);
                    PerformMoveToSampleStep();
                    PerformMoveToSampleCompletedStep(context);
                    PerformCountSampleStep(context);

                    // Unknown step should not be present at this stage
                    if (ExecutionStep == TestExecutionStep.Unknown)
                        throw new Exception("TestExecutionService is at 'unknown' execution step.");

                    // Based on what's currently happening with the execution, see if the timeline needs adjusting.
                    var executingEvent = new ExecutionTimelineEvent { SamplePosition = CurrentRunningSample.Position, ExecutionStep = ExecutionStep };
                    if (ExecutionStep == TestExecutionStep.CountSampleCompleted)
                        executingEvent.ExecutionStep = TestExecutionStep.CountSample;

                    if (WasBackgroundAcquisitionServiceRestarted)
                    {
                        WasBackgroundAcquisitionServiceRestarted = false;
                        var unusedTimeInMilliseconds = expectedBackgroundAcquisitionTimeInMilliseconds -
                                ElapsedBackgroundAcquisitionTimeInMilliseconds;
                        var recountTimeInMilliseconds = (int) (_backgroundAcquisitionService.Context.PresetBackgroundAcquisitionTimeInSeconds*
                                TimeConstants.MillisecondsPerSecond);
                        var delayTimeInMilliseconds = recountTimeInMilliseconds - unusedTimeInMilliseconds;

                        timeline.Delay((int)(ProgressMetadata.ElapsedExecutionTimeInSeconds * TimeConstants.MillisecondsPerSecond),
                            delayTimeInMilliseconds);
                        ProgressMetadata.TotalEstimatedExecutionTimeInSeconds =
                            (int)(timeline.TotalExecutionTimeInMilliseconds / TimeConstants.MillisecondsPerSecond);

                        expectedBackgroundAcquisitionTimeInMilliseconds += delayTimeInMilliseconds;
                    }

                    var timelineStatus = timeline.GetStatus((int) (ProgressMetadata.ElapsedExecutionTimeInSeconds * TimeConstants.MillisecondsPerSecond),
                                                            executingEvent);
                    if (timelineStatus == ExecutionTimelineStatus.BehindSchedule)
                    {
                        if (RecountingSameSample)
                        {
                            RecountingSameSample = false;
                            timeline.Delay((int)(ProgressMetadata.ElapsedExecutionTimeInSeconds * TimeConstants.MillisecondsPerSecond), context.ComputeSampleAcquisitionTimeInMilliseconds(CurrentRunningSample));
                        }
                        else
                        {
                            timeline.Delay((int)(ProgressMetadata.ElapsedExecutionTimeInSeconds * TimeConstants.MillisecondsPerSecond));
                        }
                        ProgressMetadata.TotalEstimatedExecutionTimeInSeconds =
                            (int) (timeline.TotalExecutionTimeInMilliseconds/TimeConstants.MillisecondsPerSecond);
                    }
                    else if (timelineStatus == ExecutionTimelineStatus.AheadOfSchedule)
                    {
                        timeline.Advance((int)(ProgressMetadata.ElapsedExecutionTimeInSeconds * TimeConstants.MillisecondsPerSecond));
                        ProgressMetadata.TotalEstimatedExecutionTimeInSeconds =
                            (int)(timeline.TotalExecutionTimeInMilliseconds / TimeConstants.MillisecondsPerSecond);
                    }

                    FireProgressUpdated(new TestExecutionProgressChangedEventArgs(GetExecutingTestType(),
                        CurrentRunningSample.InternalId,
                        ProgressMetadata,
                        context.Metadata as TestExecutionContextMetadata, ExecutionStep));

                    // This event loop processes once each second.
                    ProgressMetadata.ElapsedExecutionTimeInSeconds++;

                    Thread.Sleep(WorkerThreadDelayInMilliseconds);
                }

                e.Result = context;
            }
            catch (Exception ex)
            {
                _logger.Log(String.Format("Test Execution Controller background worker thread encountered an exception: {0} {1}", ex.Message, ex.StackTrace), Category.Exception, Priority.None);
                throw;
            }
        }

        void TestExecutionThread_Disposed(object sender, EventArgs e)
        {
            _logger.Log("Disposing TestExecution worker thread.", Category.Debug, Priority.None);
        }

        void TestExecutionThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Detach from all hardware events
            DetachFromHardwareEvents();

            _backgroundAcquisitionService.RestartedBecauseOfHighBackground -= OnBackgroundAcquisitionServiceRestartedBecauseOfHighBackground;

            if (Context != null && Context.BackgroundSample != null)
            {
                _backgroundAcquisitionService.InvalidatePreviousBackground();
            }

            //We got an exception in the TestExecution worker thread
            if (e.Error != null)
            {
                _logger.Log(String.Format("Test Execution Controller worker thread has completed, but because of an error: {0} {1}", e.Error.Message, e.Error.StackTrace), 
                    Category.Exception, Priority.None);
                if (Context != null && Context.ExecutingTest != null)
                {
                    Context.ExecutingTestStatus = TestStatus.Aborted;
                
                    _backgroundAcquisitionService.StopAsync();
                    if (!_didSampleChangerErrorOccur)
                        _sampleChanger.Stop();

                    FireTestAborted(new TestExecutionEventArgs(Context.ExecutingTest, Context.ExecutingTestId));
                }
            }
            else if (e.Cancelled)
            {
                _logger.Log("Test Execution Controller worker thread has completed, but because it was cancelled.",
                    Category.Info, Priority.None);
                if (Context != null && Context.ExecutingTest != null)
                {
                    Context.ExecutingTestStatus = TestStatus.Aborted;

                    _backgroundAcquisitionService.StopAsync();
                    if (!_didSampleChangerErrorOccur)
                        _sampleChanger.Stop();

                    FireTestAborted(new TestExecutionEventArgs(Context.ExecutingTest, Context.ExecutingTestId));
                }
            }
            else
            {
                //Success
                if (Context != null && Context.ExecutingTest != null)
                {
                    Context.ExecutingTestStatus = TestStatus.Completed;
                   
                    FireTestCompleted(new TestExecutionEventArgs(Context.ExecutingTest, Context.ExecutingTestId));
                }
            }

            Context = null;
            CurrentRunningSample = null;
        }

        #endregion

        #region Execution steps

        void EnsureBackgroundAcquistionStep(ITestExecutionContext context)
        {
            //Determine if background is required for the test
            //If context.BackgroundSample == null then background is not required, otherwise perform background
            if (context.BackgroundSample != null && _testExecutionStep == TestExecutionStep.Unknown)
                _testExecutionStep = TestExecutionStep.MoveToBackground;

            //Stop background if background is not required
            else if (context.BackgroundSample == null && _backgroundAcquisitionService.IsActive)
            {
                _backgroundAcquisitionService.StopAsync();
                _testExecutionStep = TestExecutionStep.CancelBackground;
            }
            else if (_testExecutionStep == TestExecutionStep.Unknown || _testExecutionStep == TestExecutionStep.CancelBackground)
            {
                //Wait until background acquisition service stops completely, then change the state
                if (_testExecutionStep == TestExecutionStep.CancelBackground && !_backgroundAcquisitionService.IsActive)
                    _testExecutionStep = TestExecutionStep.CountSampleCompleted;
            }
        }

        void PerformBackgroundAcquisitionStep(ITestExecutionContext context)
        {
            if (_testExecutionStep == TestExecutionStep.MoveToBackground || _testExecutionStep == TestExecutionStep.CountBackground)
            {
                //We already have a good background previously attained
                if (_backgroundAcquisitionService.WasGoodBackgroundAttained)
                {
                    _backgroundAcquisitionService.StopAsync();

                    //Update running execution context
                    context.BackgroundSample.ExecutionStatus = SampleExecutionStatus.Completed;

                    if (!_backgroundAcquisitionService.IsBackgroundOverridden && 
                        _backgroundAcquisitionService.PreviousIntegral.HasValue && 
                        _backgroundAcquisitionService.PreviousAcquisitionTimeInSeconds.HasValue)
                    {
                        context.ProcessBackground(_backgroundAcquisitionService.PreviousIntegral.Value, _backgroundAcquisitionService.PreviousAcquisitionTimeInSeconds.Value, true, _backgroundAcquisitionService.PreviousSpectrum);
                    }
                    else
                    {
                        if (_backgroundAcquisitionService.PreviousIntegral.HasValue && 
                            _backgroundAcquisitionService.PreviousAcquisitionTimeInSeconds.HasValue)
                        {
                            context.ProcessBackground(_backgroundAcquisitionService.PreviousIntegral.Value, _backgroundAcquisitionService.PreviousAcquisitionTimeInSeconds.Value, true, null, true);
                        }
                    }

                    //Next step will be based on CountSampleCompleted
                    _testExecutionStep = TestExecutionStep.CountSampleCompleted;
                }
                else
                {
                    //Start background acquisition, if it's not running already
                    if (!_backgroundAcquisitionService.IsActive)
                        _backgroundAcquisitionService.StartAsync();

                    //Live spectrum update from background service
                    if (_backgroundAcquisitionService.CurrentSpectrum != null)
                    {
                        ElapsedBackgroundAcquisitionTimeInMilliseconds += WorkerThreadDelayInMilliseconds;
                        context.ProcessBackground(_backgroundAcquisitionService.CurrentIntegral, _backgroundAcquisitionService.CurrentAcqusitionTimeInSeconds, false, _backgroundAcquisitionService.CurrentSpectrum);
                    }

                    //Change internal step
                    if (_testExecutionStep == TestExecutionStep.MoveToBackground)
                        context.BackgroundSample.ExecutionStatus = SampleExecutionStatus.InFlight;

                    else if (_testExecutionStep == TestExecutionStep.CountBackground)
                        context.BackgroundSample.ExecutionStatus = SampleExecutionStatus.Acquiring;
                }

                if (_backgroundAcquisitionService.IsActive)
                {
                    //Update current execution step from the background service
                    if (_backgroundAcquisitionService.Status == BackgroundAcquisitionServiceStatus.SeekingBackgroundPosition)
                        _testExecutionStep = TestExecutionStep.MoveToBackground;

                    else if (_backgroundAcquisitionService.Status == BackgroundAcquisitionServiceStatus.Acquiring)
                        _testExecutionStep = TestExecutionStep.CountBackground;
                }
            }

        }
        void PerformMoveToSampleStep()
        {
            if (ExecutionStep == TestExecutionStep.MoveToSample)
            {
                //Start moving sample changer to the goal position
                if (_sampleChanger.CurrentPosition != CurrentRunningSample.Position && !_sampleChanger.IsMoving)
                {
                    CurrentRunningSample.ExecutionStatus = SampleExecutionStatus.InFlight;
                    _sampleChanger.MoveTo(CurrentRunningSample.Position);
                }
                else
                {
                    if (_sampleChanger.CurrentPosition == CurrentRunningSample.Position)
                    {
                        CurrentRunningSample.ExecutionStatus = SampleExecutionStatus.Idle;
                        ExecutionStep = TestExecutionStep.MoveToSampleCompleted;
                    }
                }
            }
        }
        void PerformMoveToSampleCompletedStep(ITestExecutionContext context)
        {
            if (ExecutionStep == TestExecutionStep.MoveToSampleCompleted)
            {
                CurrentRunningSample.ExecutionStatus = SampleExecutionStatus.Acquiring;
                context.PerformAcquisition(CurrentRunningSample, ContextAcquisitionCompletedCallback);
                ExecutionStep = TestExecutionStep.CountSample;
            }
        }
        void PerformCountSampleStep(ITestExecutionContext context)
        {
            if (ExecutionStep == TestExecutionStep.CountSample)
            {
                context.PerformAcquisition(CurrentRunningSample, ContextAcquisitionCompletedCallback);
            }
        }

        bool DetermineNextSamplesToProcess(ITestExecutionContext context)
        {
            if (context.BackgroundSample != null && _testExecutionStep == TestExecutionStep.Unknown)
            {
                _testExecutionStep = TestExecutionStep.MoveToBackground;
                CurrentRunningSample = Context.BackgroundSample;
            }
            else
            {
                //Determine next step
                if (CurrentRunningSample == null || _testExecutionStep == TestExecutionStep.CountSampleCompleted)
                {
                    var previousRunningSample = CurrentRunningSample;
                    CurrentRunningSample = context.GetNextSampleToExecute();
                    RecountingSameSample = CurrentRunningSample == previousRunningSample;
                    _testExecutionStep = TestExecutionStep.MoveToSample;

                    //All done, we are finished
                    if (CurrentRunningSample == null)
                    {
                        ControllerStatus = TestExecutionControllerStatus.TestCompleted;
                        return false;
                    }
                }
            }

            return true;
        }
        void ContextAcquisitionCompletedCallback(ISample acqSample)
        {
            acqSample.ExecutionStatus = SampleExecutionStatus.Completed;
            ExecutionStep = TestExecutionStep.CountSampleCompleted;

            _eventAggregator.GetEvent<SampleAcquisitionCompleted>().Publish(new SampleAcquisitionPayload(acqSample, Context.ExecutingTestId));
        }
        
        #endregion

        #region ITestExecutionController

        public event EventHandler<TestExecutionEventArgs> ErrorOccurred;
        public event EventHandler<TestExecutionProgressChangedEventArgs> ProgressChanged;
        public event EventHandler<TestExecutionEventArgs> TestAborted;
        public event EventHandler<TestExecutionEventArgs> TestCompleted;
        public event EventHandler<TestExecutionEventArgs> TestStarted;
       
        public void Abort()
        {
            // Change test status and fire aggregate event and unhook from events
            if (_testExecutionThread != null && _testExecutionThread.IsBusy)
                _testExecutionThread.CancelAsync();
        }

        public void Start(ITestExecutionContext context)
        {
            // Can only execute one test at a time
            if (IsExecuting)
                return;

            if (context == null)
                throw new ArgumentNullException("context");
            if (context.ExecutingTest == null)
                throw new NullReferenceException("ExecutingTestType");

            Context = context;
            Context.ExecutingTestStatus = TestStatus.Running;

            try
            {
                FireTestStarted(new TestExecutionEventArgs(Context.ExecutingTest, Context.ExecutingTest.InternalId));
                _testExecutionThread.RunWorkerAsync(Context);
            }
            catch (Exception ex)
            {
                Context.ExecutingTestStatus = TestStatus.Aborted;
                _logger.Log(String.Format("Test Execution Controller failed to start test execution because of an exception: {0} {1}", ex.Message, ex.StackTrace), Category.Exception, Priority.High);
                throw;
            }
        }
        
        public bool IsExecuting
        {
            get
            {
                return _testExecutionThread != null && _testExecutionThread.IsBusy && !_testExecutionThread.CancellationPending;
            }
        }

        public Type GetExecutingTestType()
        {
            return ExecutingTest != null ? ExecutingTest.GetType() : typeof(object);
        }
        public ITest ExecutingTest
        {
            get { return Context != null ? Context.ExecutingTest : null; }
        }
        public object ExecutingContextMetadata
        {
            get { return Context != null ? Context.Metadata : null; }
        }

        #endregion

        #region Helper methods

        void AttachToHardwareEvents()
        {
            _sampleChanger.PositionChangeEnd += _sampleChanger_PositionChangeEnd;
            _sampleChanger.PositionSeekBegin += _sampleChanger_PositionSeekBegin;
            _sampleChanger.PositionSeekEnd += _sampleChanger_PositionSeekEnd;
            _sampleChanger.PositionSeekCancelled += _sampleChanger_PositionSeekCancel;
            _sampleChanger.ErrorOccurred += _sampleChanger_ChangerError;
        }

        void DetachFromHardwareEvents()
        {
            _sampleChanger.PositionChangeEnd -= _sampleChanger_PositionChangeEnd;
            _sampleChanger.PositionSeekBegin -= _sampleChanger_PositionSeekBegin;
            _sampleChanger.PositionSeekEnd -= _sampleChanger_PositionSeekEnd;
            _sampleChanger.PositionSeekCancelled -= _sampleChanger_PositionSeekCancel;
            _sampleChanger.ErrorOccurred -= _sampleChanger_ChangerError;
        }
      
        ExecutionTimeline BuildExecutionTimeline(ITestExecutionContext context)
        {
            return ExecutionTimeHelper.BuildExecutionTimeline(context);
        }

        void OnBackgroundAcquisitionServiceRestartedBecauseOfHighBackground(object sender, EventArgs e)
        {
            WasBackgroundAcquisitionServiceRestarted = true;
        }

        #endregion

        #region Hardware event handlers

        void _sampleChanger_ChangerError(int errorCode, string message)
        {
            if (IsExecuting)
            {
                _didSampleChangerErrorOccur = true;
                Abort();
            }
        }
        void _sampleChanger_PositionSeekBegin(int currPosition, int seekPosition)
        {
            if (!IsExecuting) return;

            if (ControllerStatus == TestExecutionControllerStatus.TestInProgress && ExecutionStep == TestExecutionStep.MoveToSample &&
                CurrentRunningSample != null && CurrentRunningSample.Position != seekPosition)
            {
                System.Diagnostics.Debug.Print("CurrentRunningSample {0} seekPosition {1}", CurrentRunningSample.Position, seekPosition);

                throw new Exception("Suppossed to be going to spot " + CurrentRunningSample.Position + ", but was told to go to " +seekPosition+" instead.");
            }
        }
        void _sampleChanger_PositionSeekEnd(int currPosition)
        {
            if (!IsExecuting) return;

            if (ControllerStatus == TestExecutionControllerStatus.TestInProgress)
            {
                if (ExecutionStep == TestExecutionStep.MoveToSample)
                {

                    if (CurrentRunningSample != null && CurrentRunningSample.Position != currPosition)
                        throw new Exception("Suppossed to have stopped at " + CurrentRunningSample.Position + " but my current position is " + currPosition);
                }
            }
            else
                throw new Exception("TestExecutionService. PositionSeekEnd " + currPosition + ". Why am I here.");

        }
        void _sampleChanger_PositionSeekCancel(int currPosition, int seekPosition)
        {
            if (!IsExecuting) return;

            Abort();
        }
        void _sampleChanger_PositionChangeEnd(int newPosition)
        {
            if (!IsExecuting) return;

            if (ControllerStatus != TestExecutionControllerStatus.TestInProgress)
                throw new Exception("PositionChangedEnd " + newPosition + ". SampleChanger moved no position change");
        }

        public void OnDetectorDisconnected(object o)
        {
            if (IsExecuting)
            {
                DidDetectorBecomeDisconnected = true;
                Abort();
            }
        }

        #endregion

        #region Event raisers

        protected void FireErrorOccurred(TestExecutionEventArgs args)
        {
            var handler = ErrorOccurred;
            if (handler != null)
                handler(this, args);
        }
        protected void FireTestAborted(TestExecutionEventArgs args)
        {
            var handler = TestAborted;
            if (handler != null)
                handler(this, args);
        }
        protected void FireTestCompleted(TestExecutionEventArgs args)
        {
            var handler = TestCompleted;
            if (handler != null)
                handler(this, args);
        }
        protected void FireTestStarted(TestExecutionEventArgs args)
        {
            var handler = TestStarted;
            if (handler != null)
                handler(this, args);
        }
        protected void FireProgressUpdated(TestExecutionProgressChangedEventArgs args)
        {
            var handler = ProgressChanged;
            if (handler != null)
                handler(this, args);
        }

        #endregion
    }
}
