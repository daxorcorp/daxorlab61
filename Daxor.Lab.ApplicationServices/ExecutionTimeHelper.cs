using System;
using Daxor.Lab.ApplicationServices.Interfaces;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.ApplicationServices
{
    /// <summary>
    /// Represents a helper object that can compute the timings of the individual
    /// components of a test's execution.
    /// </summary>
    public class ExecutionTimeHelper : IExecutionTimeHelper
    {
        #region Ctor

        public ExecutionTimeHelper(
            ISampleChanger sampleChanger, 
            IBackgroundAcquisitionService backgroundAcquisitionService)
        {
            SampleChanger = sampleChanger;
            BackgroundAcquisitionService = backgroundAcquisitionService;
        }

        #endregion

        #region Properties

        public IBackgroundAcquisitionService BackgroundAcquisitionService { get; private set; }
        public ISampleChanger SampleChanger { get; private set; }

        #endregion

        #region IExecutionTimeHelper members

        /// <summary>
        /// Builds an execution timeline for the given test execution context
        /// </summary>
        /// <param name="executionContext">Execution context on which the timeline is based</param>
        /// <returns>Execution timeline for the given test execution context</returns>
        public ExecutionTimeline BuildExecutionTimeline(ITestExecutionContext executionContext)
        {
            var timeline = new ExecutionTimeline();
            var lastEventEndTime = 0;

            //
            // Handle moving to background first.
            //
            var timeToMoveToBackground = ComputeTimeToMoveToBackground(executionContext);
            if (timeToMoveToBackground != 0)
            {
                var moveToBackgroundEvent = new ExecutionTimelineEvent
                    {
                        BeginTimeInMilliseconds = 0,
                        EndTimeInMilliseconds = timeToMoveToBackground,
                        SamplePosition = executionContext.BackgroundSample.Position,
                        ExecutionStep = TestExecutionStep.MoveToBackground
                    };
                timeline.Append(moveToBackgroundEvent);
                lastEventEndTime = timeToMoveToBackground;
            }

            //
            // Handle counting background.
            //
            var timeToAcquireBackground = ComputeTimeToAcquireBackground(executionContext);
            if (timeToAcquireBackground != 0)
            {
                var acquireBackgroundEvent = new ExecutionTimelineEvent
                    {
                        BeginTimeInMilliseconds = lastEventEndTime,
                        EndTimeInMilliseconds = lastEventEndTime + timeToAcquireBackground,
                        SamplePosition = executionContext.BackgroundSample.Position,
                        ExecutionStep = TestExecutionStep.CountBackground
                    };
                timeline.Append(acquireBackgroundEvent);
                lastEventEndTime += timeToAcquireBackground;
            }

            //
            // Handle executing the test.
            //
            var orderedSamples = executionContext.OrderedSamplesExcludingBackground;
            if (orderedSamples == null)
                throw new NotSupportedException("Execution context has no collection of ordered sample positions");
            var currentPosition = executionContext.BackgroundSample == null ? SampleChanger.CurrentPosition : executionContext.BackgroundSample.Position;
            

            // Move/Count pairs
            foreach (var sample in orderedSamples)
            {
                // Move to the sample position from where we are now.
                var numMovesToDesiredPosition = SampleChanger.GetNumberOfMovesBetween(currentPosition, sample.Position);
                var timeToMoveToDesiredPosition = numMovesToDesiredPosition*
                                                  SampleChanger.PositionChangeDelayInMilliseconds;
                var moveToDesiredPositionEvent = new ExecutionTimelineEvent
                    {
                        BeginTimeInMilliseconds = lastEventEndTime,
                        EndTimeInMilliseconds = lastEventEndTime + timeToMoveToDesiredPosition,
                        SamplePosition = sample.Position,
                        ExecutionStep = TestExecutionStep.MoveToSample
                    };
                timeline.Append(moveToDesiredPositionEvent);
                lastEventEndTime += timeToMoveToDesiredPosition;

                // Count that sample.
                var timeToCountDesiredSample = executionContext.ComputeSampleAcquisitionTimeInMilliseconds(sample);
                var countDesiredPositionEvent = new ExecutionTimelineEvent
                    {
                        BeginTimeInMilliseconds = lastEventEndTime,
                        EndTimeInMilliseconds = lastEventEndTime + timeToCountDesiredSample,
                        ExecutionStep = TestExecutionStep.CountSample,
                        SamplePosition = sample.Position
                    };
                timeline.Append(countDesiredPositionEvent);
                lastEventEndTime += timeToCountDesiredSample;

                currentPosition = sample.Position;
            }

            return timeline;
        }

        #endregion

        #region Internal helpers

        /// <summary>
        /// Computes the amount of time required to acquire the background sample
        /// </summary>
        /// <param name="executionContext">Execution context that has information about
        /// the background sample</param>
        /// <returns>Time (in milliseconds) to acquire the background sample; zero
        /// if the background is not required or the time cannot be computed</returns>
        internal int ComputeTimeToAcquireBackground(ITestExecutionContext executionContext)
        {
            if (executionContext == null)
                throw new ArgumentNullException("executionContext");

            // No background required
            if (executionContext.BackgroundSample == null) return 0;

            // No way to get times for calculation
            if (BackgroundAcquisitionService == null || !BackgroundAcquisitionService.IsActive || BackgroundAcquisitionService.Context == null) return 0;

            // Already have a good background
            if (BackgroundAcquisitionService.WasGoodBackgroundAttained) return 0;

            var backgroundCountTimeInSeconds =
                BackgroundAcquisitionService.Context.PresetBackgroundAcquisitionTimeInSeconds;
            var elapsedBackgroundCountTimeInSeconds = BackgroundAcquisitionService.RunningBackgroundExecutionTimeInSeconds;
            return (backgroundCountTimeInSeconds - elapsedBackgroundCountTimeInSeconds) *
                   (int)TimeConstants.MillisecondsPerSecond;
        }


        /// <summary>
        /// Computes the amount of time required for the sample changer to move to the background position
        /// </summary>
        /// <param name="executionContext">Execution context that has information about
        /// the background sample</param>
        /// <returns>Time (in milliseconds) to move to the background position; zero if the background
        /// is not required or the time cannot be computed</returns>
        internal int ComputeTimeToMoveToBackground(ITestExecutionContext executionContext)
        {
            if (executionContext == null)
                throw new ArgumentNullException("executionContext");

            if (SampleChanger == null || executionContext.BackgroundSample == null) return 0;

            var numberOfMovesToBackground = SampleChanger.GetNumberOfMovesBetween(SampleChanger.CurrentPosition, executionContext.BackgroundSample.Position);
            return numberOfMovesToBackground * SampleChanger.PositionChangeDelayInMilliseconds;
        }

        #endregion
    }
}
