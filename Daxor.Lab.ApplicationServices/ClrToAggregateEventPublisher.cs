﻿using Daxor.Lab.ApplicationServices.Interfaces;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Microsoft.Practices.Composite.Events;

namespace Daxor.Lab.ApplicationServices
{
    /// <summary>
    /// Represents a service that subscribes to CLR events on IDetector, ISampleChanger and ITestExecutionController
    /// then publishes the corresponding aggregate events when those events are raised.
    /// </summary>
    public class ClrToAggregateEventPublisher : IHardwareAggregateEventPublisher, ITextExecutionControllerEventPublisher
    {
        #region Fields

        private readonly IEventAggregator _eventAggregator;
        private readonly IDetector _detector;
        private readonly ISampleChanger _sampleChanger;
        private readonly ITestExecutionController _testExecutionController;

        #endregion

        #region Ctor

        /// <summary>
        /// Creates a new ClrToAggregateEventPublisher instance based on the given
        /// event aggregator, detector, and sample changer instances.
        /// </summary>
        /// <param name="eventAggregator">Event aggregator used for publishing aggregate events</param>
        /// <param name="detector">Detector whose CLR events are watched</param>
        /// <param name="sampleChanger">Sample changer whose CLR events are watched</param>
        /// <param name="testExecutionController">Object that controls the execution of tests</param>
        public ClrToAggregateEventPublisher(IEventAggregator eventAggregator, 
            IDetector detector, 
            ISampleChanger sampleChanger, 
            ITestExecutionController testExecutionController)
        {
            _eventAggregator = eventAggregator;
            _detector = detector;
            _sampleChanger = sampleChanger;
            _testExecutionController = testExecutionController;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the event aggregator instance.
        /// </summary>
        internal IEventAggregator Aggregator
        {
            get { return _eventAggregator; }
        }

        /// <summary>
        /// Gets the detector instance.
        /// </summary>
        internal IDetector Detector
        {
            get { return _detector; }
        }

        /// <summary>
        /// Gets the sample changer instance.;
        /// </summary>
        internal ISampleChanger SampleChanger
        {
            get { return _sampleChanger; }
        }

        /// <summary>
        /// Get the test execution controller
        /// </summary>
        internal ITestExecutionController TestExecutionController
        {
            get { return _testExecutionController; }
        }

        #endregion

        #region IHardwareAggregateEventPublisher members

        /// <summary>
        /// SubscribeToHardwareEvents to CLR events by hooking up methods that publish the corresponding
        /// aggregate events. This method has no effect if the CLR events have already
        /// been subscribed to.
        /// </summary>
        public void SubscribeToHardwareEvents()
        {
            Detector.Disconnected += OnDetectorDisconnected;

            SampleChanger.Connected += OnSampleChangerConnected;
            SampleChanger.Disconnected += OnSampleChangerDisconnected;
            SampleChanger.ErrorOccurred += OnSampleChangerErrorOccurred;
            SampleChanger.PositionChangeBegin += OnSampleChangerPositionChangeBegin;
            SampleChanger.PositionChangeEnd += OnSampleChangerPositionSeekEnd;
            SampleChanger.PositionSeekBegin += OnSampleChangerPositionSeekBegin;
            SampleChanger.PositionSeekCancelled += OnSampleChangerPositionSeekCancelled;
            SampleChanger.PositionSeekEnd += OnSampleChangerPositionSeekEnd;
        }

        #endregion

        #region ITestExecutionControllerEventPublisher members

        /// <summary>
        /// SubscribeToHardwareEvents to CLR events by hooking up methods that publish the 
        /// corresponding aggregate events. This method has no effect if the CLR events have 
        /// already been subscribed to.
        /// </summary>
        public void SubscribeToTestExecutionControllerEvents()
        {
            TestExecutionController.ErrorOccurred += OnTestExecutionControllerErrorOccurred;
            TestExecutionController.TestAborted += OnTestExecutionControllerTestAborted;
            TestExecutionController.TestCompleted += OnTestExecutionControllerTestCompleted;
            TestExecutionController.TestStarted += OnTestExecutionControllerTestStarted;
            TestExecutionController.ProgressChanged += OnTestExecutionControllerProgressChanged;
        }

        #endregion

        #region Sample changer and detector event handlers

        #region Detector

        /// <summary>
        /// Handles the detector disconnected event by publishing the DetectorDisconnected aggregate event
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that was disconnected</param>
        /// <param name="reason">Reason the detector was disconnected</param>
        protected virtual void OnDetectorDisconnected(string mcaId, string reason)
        {
            _eventAggregator.GetEvent<DetectorDisconnected>().Publish(null);
        }

        #endregion

        #region Sample changer

        /// <summary>
        /// Handles the ISampleChanger.Connected event by ignoring it, as there is
        /// currently no corresponding aggregate event needed for this CLR event.
        /// </summary>
        protected virtual void OnSampleChangerConnected()
        {
        }

        /// <summary>
        /// Handles the ISampleChanger.Disconnected event by ignoring it, as there is
        /// currently no corresponding aggregate event needed for this CLR event.
        /// </summary>
        /// <param name="reason">Reason for being disconnected</param>
        protected virtual void OnSampleChangerDisconnected(string reason)
        {
        }

        /// <summary>
        /// Handles the ISampleChanger.ErrorOccurred event by publishing the
        /// SampleChangerErrorOccurred event.
        /// </summary>
        /// <param name="errorCode">Error code</param>
        /// <param name="message">Message pertaining to the error</param>
        protected virtual void OnSampleChangerErrorOccurred(int errorCode, string message)
        {
            Aggregator.GetEvent<SampleChangerErrorOccurred>().Publish(new SampleChangerErrorOccurredPayload
            {
                ErrorCode = errorCode,
                Message = message
            });
        }

        /// <summary>
        /// Handles the ISampleChanger.PositionChangeBegin event by publishing the
        /// SampleChangerPositionChangeBegin event.
        /// </summary>
        /// <param name="oldPosition">Old sample changer position</param>
        /// <param name="newPosition">New position</param>
        protected virtual void OnSampleChangerPositionChangeBegin(int oldPosition, int newPosition)
        {
            Aggregator.GetEvent<SampleChangerPositionChangeStarted>().Publish(new SampleChangerPositionChangeStartedPayload
            {
                OldPosition = oldPosition,
                NewPosition = newPosition
            });

        }

        /// <summary>
        /// Handles the ISampleChanger.PositionChangeEnd event by publishing the
        /// SampleChangerPositionChangeEnd event.
        /// </summary>
        /// <param name="newPosition">New sample changer position</param>
        protected virtual void OnSampleChangerPositionChangeEnd(int newPosition)
        {
            Aggregator.GetEvent<SampleChangerPositionChangeCompleted>().Publish(new SampleChangerPositionChangeCompletedPayload
            {
                NewPosition = newPosition
            });
        }

        /// <summary>
        /// Handles the ISampleChanger.PositionSeekBegin event by publishing the
        /// SampleChangerSeekBeghin event.
        /// </summary>
        /// <param name="currPosition">Current sample changer position</param>
        /// <param name="goalPosition">Goal position</param>
        protected virtual void OnSampleChangerPositionSeekBegin(int currPosition, int goalPosition)
        {
            Aggregator.GetEvent<SampleChangerPositionSeekStarted>().Publish(new SampleChangerPositionSeekStartedPayload
            {
                CurrentPosition = currPosition,
                GoalPosition = goalPosition
            });
        }

        /// <summary>
        /// Handles the ISampleChanger.PositionSeekEnd event by publishing the 
        /// SampleChangerPositionSeekEnd event.
        /// </summary>
        /// <param name="currPosition">Current sample changer position</param>
        protected virtual void OnSampleChangerPositionSeekEnd(int currPosition)
        {
            Aggregator.GetEvent<SampleChangerPositionSeekCompleted>().Publish(new SampleChangerPositionSeekCompletedPayload
            {
                CurrentPosition = currPosition
            });
        }

        /// <summary>
        /// Handles the ISampleChanger.PositionSeekCancelled event by publishing the 
        /// SampleChangerPositionSeekCancel event.
        /// </summary>
        /// <param name="currPosition">Current sample changer position</param>
        /// <param name="goalPosition">Goal position</param>
        protected virtual void OnSampleChangerPositionSeekCancelled(int currPosition, int goalPosition)
        {
            Aggregator.GetEvent<SampleChangerPositionSeekCancelled>().Publish(new SampleChangerPositionSeekCancelledPayload
            {
                CurrentPosition = currPosition,
                GoalPosition = goalPosition
            });
        }

        #endregion

        #endregion

        #region Test execution controller event handlers

        void OnTestExecutionControllerTestStarted(object sender, Domain.Eventing.TestExecutionEventArgs e)
        {
            _eventAggregator.GetEvent<TestStarted>().Publish(e.ExecutingTest);
        }

        void OnTestExecutionControllerTestCompleted(object sender, Domain.Eventing.TestExecutionEventArgs e)
        {
            _eventAggregator.GetEvent<TestCompleted>().Publish(e.ExecutingTest);
        }

        void OnTestExecutionControllerTestAborted(object sender, Domain.Eventing.TestExecutionEventArgs e)
        {
            _eventAggregator.GetEvent<TestAborted>().Publish(e.ExecutingTest);
        }

        void OnTestExecutionControllerErrorOccurred(object sender, Domain.Eventing.TestExecutionEventArgs e)
        {
            _eventAggregator.GetEvent<TestAborted>().Publish(e.ExecutingTest);
        }

        void OnTestExecutionControllerProgressChanged(object sender, Domain.Eventing.TestExecutionProgressChangedEventArgs e)
        {
            _eventAggregator.GetEvent<TestExecutionProgressChanged>().Publish(e);
        }

        // Need progress updated event here

        #endregion
    }
}
