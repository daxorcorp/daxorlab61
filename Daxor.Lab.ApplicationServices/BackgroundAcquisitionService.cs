﻿using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Events;
using System;
using System.ComponentModel;
using System.Threading;

namespace Daxor.Lab.ApplicationServices
{
    public class BackgroundAcquisitionService : IBackgroundAcquisitionService
    {
        #region Fields

        private readonly IDetector _detector;
        private readonly ISampleChanger _sampleChanger;
        private readonly ISpectroscopyService _spectroscopyService;
        private readonly IEventAggregator _eventAggregator;
        private readonly ILoggerFacade _logger;
        private readonly SettingObserver<ISettingsManager> _settingsObserver;
        private readonly ISettingsManager _settingsManager;

        private BackgroundAcquisitionContext _context;
        private bool _wasGoodBackgroundAttained;
        private int? _previousAcquisitionTimeInSeconds;
        private int? _previousIntegral;
        double _liveTimeWhenHighCpmOccurredInSeconds = Double.NegativeInfinity;

        private readonly object _wasGoodBackgroundAttainedLock = new object();
        private readonly object _previousAcquisitionTimeInSecondsLock = new object();
        private readonly object _previousIntegralLock = new object();

        #endregion

        #region Ctor

        public BackgroundAcquisitionService(IDetector detector, ISampleChanger sampleChanger,
                                    ILoggerFacade logger, ISpectroscopyService spectroscopyService,
                                    IEventAggregator eventAggregator, ISettingsManager settingsManager)
        {
            _detector = detector;
            _sampleChanger = sampleChanger;
            _logger = logger;
            _spectroscopyService = spectroscopyService;
            _eventAggregator = eventAggregator;
            _settingsManager = settingsManager;
            _settingsObserver = new SettingObserver<ISettingsManager>(_settingsManager);

            Status = BackgroundAcquisitionServiceStatus.Initializing;
            SubscribeToAggregateEvents();
            InitializeAcquisitionWorker();
            InitializeSettingsObserver();
        }
        #endregion

        #region Properties

        /// <summary>
        /// Gets and sets the BackgroundWorker used to acquire background counts.
        /// </summary>
        internal BackgroundWorker AcquisitionWorker { get; set; }

        /// <summary>
        /// Gets the detector used for acquiring background counts.
        /// </summary>
        internal IDetector Detector
        {
            get { return _detector; }
        }

        /// <summary>
        /// Gets the event aggregator.
        /// </summary>
        internal IEventAggregator Aggregator
        {
            get { return _eventAggregator; }
        }

        /// <summary>
        /// Gets the logger instance.
        /// </summary>
        internal ILoggerFacade Logger
        {
            get { return _logger; }
        }

        /// <summary>
        /// Gets the sample changer used for acquiring background counts.
        /// </summary>
        internal ISampleChanger SampleChanger
        {
            get { return _sampleChanger; }
        }

        /// <summary>
        /// Gets the spectroscopy service.
        /// </summary>
        internal ISpectroscopyService SpectroscopyService
        {
            get { return _spectroscopyService; }
        }

        #endregion

        #region IBackgroundAcquisitionService members

        /// <summary>
        /// Occurs when a good background has been obtained.
        /// </summary>
        public event EventHandler<BackgroundAttainedEventArgs> GoodBackgroundAttained;

        /// <summary>
        /// Occurs when the background acquisition service restarts counting because
        /// the background was too high for a certain time interval
        /// </summary>
        public event EventHandler RestartedBecauseOfHighBackground;

        /// <summary>
        /// Gets the background acquisition context.
        /// </summary>
        public BackgroundAcquisitionContext Context
        {
            get
            {
                if (_context == null)
                    return null;

                return _context.ShallowCopy();
            }
            private set { _context = value; }
        }

        /// <summary>
        /// Gets whether or not the service is active. Active means there is a valid worker
        /// thread that is busy and not in the process of being cancelled.
        /// </summary>
        public bool IsActive
        {
            get
            {
                return (AcquisitionWorker != null) &&
                    AcquisitionWorker.IsBusy &&
                    !AcquisitionWorker.CancellationPending;
            }
        }

        /// <summary>
        /// Gets or sets whether or not the background count value is overridden with some other value.
        /// </summary>
        public bool IsBackgroundOverridden
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets whether or not the current background level (in counts per minute) is too high.
        /// </summary>
        public bool IsCurrentCpmTooHigh
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the status of the background acquisition service.
        /// </summary>
        public BackgroundAcquisitionServiceStatus Status
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets whether or not a good background has been attained. When setting the
        /// value, the BackgroundAcquisitionAttainedGoodBackground event is published. If being
        /// set to false (i.e., a good background was not attained), the previous acquisition
        /// time, previous integral, and previous spectrum are set to null.
        /// </summary>
        public bool WasGoodBackgroundAttained
        {
            get { return _wasGoodBackgroundAttained; }
            private set
            {
                lock (_wasGoodBackgroundAttainedLock)
                {
                    _wasGoodBackgroundAttained = value;

                    Aggregator.GetEvent<BackgroundAcquisitionGoodBackgroundAttained>().Publish(_wasGoodBackgroundAttained);

                    if (!_wasGoodBackgroundAttained)
                    {
                        PreviousAcquisitionTimeInSeconds = null;
                        PreviousIntegral = null;
                        PreviousSpectrum = null;
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets the number of seconds the background acquisition service has been running.
        /// </summary>
        public int RunningBackgroundExecutionTimeInSeconds
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the current background acquisition time in seconds.
        /// </summary>
        public int CurrentAcqusitionTimeInSeconds
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the current background rate in counts per minute. 
        /// Returns zero if the rate cannot be computed.
        /// </summary>
        public int CurrentCountsPerMinute
        {
            get
            {
                int currentCpm = 0;
                try
                {
                    if ((CurrentAcqusitionTimeInSeconds != 0) && !Double.IsNaN(CurrentAcqusitionTimeInSeconds))
                        currentCpm = (int)Math.Round(CurrentIntegral / (double)CurrentAcqusitionTimeInSeconds * TimeConstants.SecondsPerMinute, 0);
                }
                catch
                {
                    currentCpm = 0;
                }
                return currentCpm;
            }
        }

        /// <summary>
        /// Gets the current background rate in counts per second.
        /// </summary>
        public double CurrentCountsPerSecond
        {
            get
            {
                return CurrentCountsPerMinute / TimeConstants.SecondsPerMinute;
            }
        }

        /// <summary>
        /// Gets or sets the current background integral (i.e., counts in the ROI).
        /// </summary>
        public int CurrentIntegral
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the current spectrum for the background.
        /// </summary>
        public Spectrum CurrentSpectrum
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the previous background acquisition time in seconds.
        /// </summary>
        public int? PreviousAcquisitionTimeInSeconds
        {
            get { return _previousAcquisitionTimeInSeconds; }
            private set
            {
                if (_previousAcquisitionTimeInSeconds == value)
                    return;

                lock (_previousAcquisitionTimeInSecondsLock)
                {
                    _previousAcquisitionTimeInSeconds = value;
                }
            }
        }

        /// <summary>
        /// Gets the previous background rate in counts per minute.
        /// Returns null if the previous rate cannot be determined.
        /// </summary>
        public int? PreviousCountsPerMinute
        {
            get
            {
                int? prevCpm = null;
                if ((PreviousIntegral != null) && (PreviousAcquisitionTimeInSeconds != null))
                {
                    try
                    {
                        prevCpm = (int)Math.Round((int)PreviousIntegral / (double)PreviousAcquisitionTimeInSeconds * TimeConstants.SecondsPerMinute, 0);
                    }
                    catch
                    {
                        prevCpm = null;
                    }
                }

                return prevCpm;
            }
        }

        /// <summary>
        /// Gets the previous background rate in counts per second.
        /// </summary>
        public double? PreviousCountsPerSecond
        {
            get
            {
                if (PreviousCountsPerMinute == null)
                    return null;

                return (double)PreviousCountsPerMinute / TimeConstants.SecondsPerMinute;
            }
        }

        /// <summary>
        /// Gets the previous background integral (i.e., counts in the ROI).
        /// </summary>
        public int? PreviousIntegral
        {
            get { return _previousIntegral; }
            private set
            {
                if (_previousIntegral == value)
                    return;

                lock (_previousIntegralLock)
                {
                    _previousIntegral = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the previous spectrum for the background.
        /// </summary>
        public Spectrum PreviousSpectrum
        {
            get;
            private set;
        }

        /// <summary>
        /// Invalidates the previous background level.
        /// </summary>
        public void InvalidatePreviousBackground()
        {
            // Reset integral and spectrum
            CurrentIntegral = -1;
            PreviousIntegral = CurrentIntegral;
            CurrentSpectrum = null;
            PreviousSpectrum = null;

            WasGoodBackgroundAttained = false;
        }

        /// <summary>
        /// Overrides the current background level (in counts per minute) with another value
        /// and stops any current acquisition.
        /// </summary>
        /// <param name="newCpm">Overriding background level (in counts per minute)</param>
        public void OverrideBackground(int newCpm)
        {
            IsBackgroundOverridden = true;
            WasGoodBackgroundAttained = true;
            PreviousIntegral = newCpm;
            PreviousAcquisitionTimeInSeconds = (int)TimeConstants.SecondsPerMinute;
            Status = BackgroundAcquisitionServiceStatus.Idle;
            StopAsync();

            FireGoodBackgroundAttained((int)PreviousIntegral, (int)PreviousAcquisitionTimeInSeconds, true);
        }

        /// <summary>
        /// Starts the background acquisition service asynchronously by setting up the 
        /// execution context and then starting the service.
        /// </summary>
        /// <returns>True if the service started successfully; false otherwise</returns>
        public bool StartAsync()
        {
            IsBackgroundOverridden = false;

            if (Context == null)
            {
                var context = new BackgroundAcquisitionContext
                {
                    BackgroundPosition = _settingsManager.GetSetting<Int32>(SettingKeys.SystemBackgroundPosition),
                    PresetBackgroundAcquisitionTimeInSeconds = _settingsManager.GetSetting<Int32>(SettingKeys.SystemBackgroundAcquisitionTimeInSeconds),
                    HighCpmBoundary = _settingsManager.GetSetting<Int32>(SettingKeys.SystemHighCpmBoundary),
                    HighCpmRestartTimeIntervalInSeconds = _settingsManager.GetSetting<Int32>(SettingKeys.SystemHighCpmRestartTimeIntervalInSeconds),
                    RoiLowBound = _settingsManager.GetSetting<Int32>(SettingKeys.SystemI131RoiLowBound),
                    RoiHighBound = _settingsManager.GetSetting<Int32>(SettingKeys.SystemI131RoiHighBound),
                    ShouldUseLiveAcquisitionTime = true,
                };

                // Cache a copy of background acquisition context. Background acquisition context will always
                // contain simple properties, no references
                Context = context.ShallowCopy();
            }

            return InnerStart();
        }

        /// <summary>
        /// Stops the background acquisition service asynchronously by cancelling
        /// the acquisition worker (if it's busy).
        /// </summary>
        public void StopAsync()
        {
            if (AcquisitionWorker != null && AcquisitionWorker.IsBusy)
                AcquisitionWorker.CancelAsync();
        }

        #endregion

        #region Private helper methods

        #region BackgroundWorker-related methods

        /// <summary>
        /// Initializes the BackgroundWorker that handles the acquisition of background
        /// counts by wiring up delegates that handle the work, progress updates, and
        /// completion.
        /// </summary>
        void InitializeAcquisitionWorker()
        {
            AcquisitionWorker = new BackgroundWorker {WorkerSupportsCancellation = true };
            AcquisitionWorker.DoWork += OnAcquisitionWorkerDoWork;
            AcquisitionWorker.RunWorkerCompleted += OnAcquisitionWorkerRunWorkerCompleted;
        }

        /// <summary>
        /// Starts the acquisition worker thread after checking various conditions.
        /// </summary>
        /// <returns>True if the acquisition worker was started; false if the service
        /// is already running</returns>
        /// <exception cref="ApplicationException">If the service could not be started
        /// because of an error condition</exception>
        internal bool InnerStart()
        {
            const string errorPrefix = "Unable to start the background acquisition service ";
            string innerError = null;

            // Can't start the service if already active.
            if (IsActive)
                return false;

            if (Context == null)
                innerError = errorPrefix + "without a BackgroundAcquisitionContext instance.";

            else if (Detector == null)
                innerError = errorPrefix + "without an IDetector instance.";

            else if (!Detector.IsConnected)
                innerError = errorPrefix + "when the detector is disconnected.";

            else if (SampleChanger == null)
                innerError = errorPrefix + "without an ISampleChanger instance.";

            else if (Context.RoiLowBound >= Context.RoiHighBound)
                innerError = errorPrefix + "when the ROI left bound is greater than or equal to the ROI right bound.";

            else if (Context.PresetBackgroundAcquisitionTimeInSeconds <= 0)
                innerError = errorPrefix + "when the acquisition time is less than or equal to 0 seconds.";

            else if (Context.BackgroundPosition <= 0)
                innerError = errorPrefix + "when the background position is less than or equal to zero.";

            if (!String.IsNullOrEmpty(innerError))
                throw new ApplicationException(String.Format("{0} {1}", "Cannot start background acquisition.", innerError));

            Status = BackgroundAcquisitionServiceStatus.Initializing;
            HookUpEventHandlers();

            if (!AcquisitionWorker.IsBusy)
                AcquisitionWorker.RunWorkerAsync(_context);

            return true;
        }

        /// <summary>
        /// XXXX
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        void OnAcquisitionWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                // Initialize the acquisition process by stopping the sample changer and detector.
                var context = (BackgroundAcquisitionContext)e.Argument;
                SampleChanger.Stop();
                Detector.StopAcquiring();
                Detector.ResetSpectrum();
                Aggregator.GetEvent<BackgroundAcquisitionStarted>().Publish(null);

                while (true)
                {
                    // Background acquisition has been stopped, so start the cancellation process
                    // and leave this process loop.
                    if (AcquisitionWorker.CancellationPending)
                    {
                        e.Cancel = true;
                        PerformAcquisitionWorkerCancellation();
                        break;
                    }

                    // Initialization is done if we're already at the background position.
                    if (Status == BackgroundAcquisitionServiceStatus.Initializing && SampleChangerIsAtBackgroundPosition(context.BackgroundPosition))
                        Status = BackgroundAcquisitionServiceStatus.ReadyToBeginAcquisition;

                    // If we're not already seeking background (and not at background), move there.
                    if (Status != BackgroundAcquisitionServiceStatus.SeekingBackgroundPosition && !SampleChangerIsAtBackgroundPosition(context.BackgroundPosition))
                    {
                        Status = BackgroundAcquisitionServiceStatus.SeekingBackgroundPosition;
                        SampleChanger.MoveTo(context.BackgroundPosition);
                    }

                    // At the background position, so start counting.
                    if (Status == BackgroundAcquisitionServiceStatus.ReadyToBeginAcquisition)
                        PerformBackgroundAcquisition(context);

                    // Check if we've finished acquiring. If so, reset things and check the background.
                    if ((Status == BackgroundAcquisitionServiceStatus.Acquiring) && Detector.HasAcquisitionCompleted)
                    {
                        CurrentIntegral = Convert.ToInt32(SpectroscopyService.ComputeIntegral(Detector.Spectrum, Context.RoiLowBound, Context.RoiHighBound));
                        CurrentAcqusitionTimeInSeconds = Context.PresetBackgroundAcquisitionTimeInSeconds;
                        PreviousIntegral = CurrentIntegral;
                        PreviousAcquisitionTimeInSeconds = CurrentAcqusitionTimeInSeconds;
                        CurrentSpectrum = Detector.Spectrum;
                        PreviousSpectrum = Detector.Spectrum;

                        if (!CheckForHighBackground())
                        {
                            WasGoodBackgroundAttained = true;
                            FireGoodBackgroundAttained((int)PreviousIntegral, (int)PreviousAcquisitionTimeInSeconds);
                        }
                        else
                            WasGoodBackgroundAttained = false;

                        Status = BackgroundAcquisitionServiceStatus.ReadyToBeginAcquisition;
                    }

                    // Acquire the spectrum, run spectroscopy, update count rates and times
                    if (Status == BackgroundAcquisitionServiceStatus.Acquiring)
                    {
                        try
                        {
                            Spectrum spectrum = Detector.Spectrum;
                            double percentCompleted = 0;
                            if (spectrum != null)
                            {
                                CurrentAcqusitionTimeInSeconds = (int)Math.Round(spectrum.LiveTimeInSeconds, 0);
                                CurrentIntegral = Convert.ToInt32(SpectroscopyService.ComputeIntegral(spectrum, context.RoiLowBound, context.RoiHighBound));
                                CurrentSpectrum = spectrum;

                                percentCompleted = (CurrentAcqusitionTimeInSeconds / (double)context.PresetBackgroundAcquisitionTimeInSeconds) * 100.0;
                                RunningBackgroundExecutionTimeInSeconds = CurrentAcqusitionTimeInSeconds;

                                CheckForHighBackground();
                            }

                            // Report the progress of the background acquisiton
                            Aggregator.GetEvent<BackgroundAcquisitionProgressChanged>().Publish(
                                    new BackgroundAcquisitionProgressPayload((int)Math.Round(percentCompleted, 0), CurrentAcqusitionTimeInSeconds,
                                        context.PresetBackgroundAcquisitionTimeInSeconds, Status, CurrentCountsPerMinute, PreviousCountsPerMinute,
                                        IsActive, IsCurrentCpmTooHigh, WasGoodBackgroundAttained));
                        }
                        catch (Exception ex)
                        {
                            Logger.Log("The background acquisition encountered an exception. Message: " + ex.Message + "\nStack trace: " + ex.StackTrace, Category.Exception, Priority.High);
                        }
                    }

                    // Let the UI changes be dispatched in a timely manner.
                    Thread.Sleep(1000);
                }
            }
            catch (Exception ex)
            {
                Logger.Log("The background acquisition worker method encountered an exception. Message: " + ex.Message + "\nStack trace: " + ex.StackTrace, Category.Exception, Priority.High);
            }
        }

        /// <summary>
        /// Determines if the current background rate is too high and modifies state
        /// accordingly.
        /// </summary>
        /// <returns>True if the background is high; false otherwise</returns>
        private bool CheckForHighBackground()
        {
            if (CurrentCountsPerMinute >= Context.HighCpmBoundary)
            {
                IsCurrentCpmTooHigh = true;

                // ReSharper disable CompareOfFloatsByEqualityOperator
                if (_liveTimeWhenHighCpmOccurredInSeconds == Double.NegativeInfinity)
                // ReSharper restore CompareOfFloatsByEqualityOperator
                {
                    // We went from having a good (or no previous) background to high background.
                    _liveTimeWhenHighCpmOccurredInSeconds = CurrentAcqusitionTimeInSeconds;
                }
                else
                {
                    Logger.Log(String.Format("High background encountered. Waiting to reach high CPM restart interval: {0}/{1} sec", CurrentAcqusitionTimeInSeconds - _liveTimeWhenHighCpmOccurredInSeconds, Context.HighCpmRestartTimeIntervalInSeconds), Category.Debug, Priority.None);

                    // If we reached the restart background interval, restart.
                    if ((CurrentAcqusitionTimeInSeconds - _liveTimeWhenHighCpmOccurredInSeconds) >= Context.HighCpmRestartTimeIntervalInSeconds)
                    {
                        WasGoodBackgroundAttained = false;
                        Status = BackgroundAcquisitionServiceStatus.ReadyToBeginAcquisition;
                        _liveTimeWhenHighCpmOccurredInSeconds = Double.NegativeInfinity;
                    }
                }

                return true; // High background
            }

            // Background used to be too high but is okay now.
            if (IsCurrentCpmTooHigh)
            {
                IsCurrentCpmTooHigh = false;
                FireRestartedBecauseOfHighBackground();
            }

            _liveTimeWhenHighCpmOccurredInSeconds = Double.NegativeInfinity;

            return false;
        }

        /// <summary>
        /// Handles the BackgroundWorker.RunWorkerCompleted event by logging any error that
        /// occurred (i.e., the worker was completed but with an error).
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        void OnAcquisitionWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((!e.Cancelled) && (e.Error != null))
            {
                var message = "The BackgroundWorker thread in BackgroundAcquisitionService " +
                    "completed but with an error: " + e.Error.Message;
                Logger.Log(message, Category.Exception, Priority.High);
            }
        }

        /// <summary>
        /// Cancels/aborts the background acquisition process by unhooking event handlers,
        /// stopping the detector and sample changer, and publishing the BackgroundAcquisitionAborted
        /// event.
        /// </summary>
        void PerformAcquisitionWorkerCancellation()
        {
            UnhookEventHandlers();
            Detector.StopAcquiring();
            SampleChanger.Stop();
            Aggregator.GetEvent<BackgroundAcquisitionAborted>().Publish(null);
        }

        /// <summary>
        /// Begins the acquisition process by resetting the detector and acquiring for an amount
        /// of time specified by the given context.
        /// </summary>
        /// <param name="context">Execution context for the background</param>
        void PerformBackgroundAcquisition(BackgroundAcquisitionContext context)
        {
            Detector.StopAcquiring();
            Detector.ResetSpectrum();
            Detector.StartAcquiring(context.PresetBackgroundAcquisitionTimeInSeconds, context.ShouldUseLiveAcquisitionTime);
            Status = BackgroundAcquisitionServiceStatus.Acquiring;
        }

        #endregion

        #region Settings obsever-related methods

        /// <summary>
        /// Subscribes to changes in certain settings.
        /// </summary>
        void InitializeSettingsObserver()
        {
            _settingsObserver.RegisterHandler(SettingKeys.SystemBackgroundPosition,
                manager => OnBackgroundPositionSettingChanged(manager, SettingKeys.SystemBackgroundPosition));
            _settingsObserver.RegisterHandler(SettingKeys.SystemBackgroundAcquisitionTimeInSeconds,
                manager => OnBackgroundAcquisitionTimeInSecondsSettingChanged(manager, SettingKeys.SystemBackgroundAcquisitionTimeInSeconds));
            _settingsObserver.RegisterHandler(SettingKeys.SystemHighCpmBoundary,
                manager => OnHighCountsPerMinuteBoundarySettingChanged(manager, SettingKeys.SystemHighCpmBoundary));
            _settingsObserver.RegisterHandler(SettingKeys.SystemHighCpmRestartTimeIntervalInSeconds,
                manager => OnHighCountsPerMinuteRestartTimeIntervalInSecondsSettingChanged(manager, SettingKeys.SystemHighCpmRestartTimeIntervalInSeconds));
            _settingsObserver.RegisterHandler(SettingKeys.SystemI131RoiLowBound,
                manager => OnRegionOfInterestLowBoundSettingChanged(manager, SettingKeys.SystemI131RoiLowBound));
            _settingsObserver.RegisterHandler(SettingKeys.SystemI131RoiHighBound,
                manager => OnRegionOfInterestHighBoundSettingChanged(manager, SettingKeys.SystemI131RoiHighBound));

        }

        /// <summary>
        /// Handles a change to the background position setting by updating the background
        /// acquisition context. If this service is active, a previously attained good background
        /// is invalidated and the service state is set to Initializing. If there is no acquisition
        /// context, this method has no effect.
        /// </summary>
        /// <param name="settingsManager">Settings manager</param>
        /// <param name="settingKey">Setting key that has been changed</param>
        void OnBackgroundPositionSettingChanged(ISettingsManager settingsManager, string settingKey)
        {
            if (_context == null)
                return;

            _context.BackgroundPosition = settingsManager.GetSetting<Int32>(settingKey);
            if (IsActive)
            {
                WasGoodBackgroundAttained = false;
                Status = BackgroundAcquisitionServiceStatus.Initializing;
            }
        }

        /// <summary>
        /// Handles a change to the background acquisition time setting by updating the background
        /// acquisition context. If this service is active, a previously attained good background
        /// is invalidated and the service state is set to Initializing. If there is no acquisition
        /// context, this method has no effect.
        /// </summary>
        /// <param name="settingsManager">Settings manager</param>
        /// <param name="settingKey">Setting key that has been changed</param>
        void OnBackgroundAcquisitionTimeInSecondsSettingChanged(ISettingsManager settingsManager, string settingKey)
        {
            if (_context == null)
                return;

            _context.PresetBackgroundAcquisitionTimeInSeconds = settingsManager.GetSetting<Int32>(settingKey);
            if (IsActive)
            {
                WasGoodBackgroundAttained = false;
                Status = BackgroundAcquisitionServiceStatus.Initializing;
            }
        }

        /// <summary>
        /// Handles a change to the high CPM boundary setting by updating the background
        /// acquisition context. If there is no acquisition context, this method has no effect.
        /// </summary>
        /// <param name="settingsManager">Settings manager</param>
        /// <param name="settingKey">Setting key that has been changed</param>
        void OnHighCountsPerMinuteBoundarySettingChanged(ISettingsManager settingsManager, string settingKey)
        {
            if (_context == null)
                return;

            _context.HighCpmBoundary = settingsManager.GetSetting<Int32>(settingKey);
        }

        /// <summary>
        /// Handles a change to the high CPM restart time interval setting by updating the background
        /// acquisition context. If there is no acquisition context, this method has no effect.
        /// </summary>
        /// <param name="settingsManager">Settings manager</param>
        /// <param name="settingKey">Setting key that has been changed</param>
        void OnHighCountsPerMinuteRestartTimeIntervalInSecondsSettingChanged(ISettingsManager settingsManager, string settingKey)
        {
            if (_context == null)
                return;

            _context.HighCpmRestartTimeIntervalInSeconds = settingsManager.GetSetting<Int32>(settingKey);
        }

        /// <summary>
        /// Handles a change to the ROI low bound setting by updating the background
        /// acquisition context. If there is no acquisition context, this method has no effect.
        /// </summary>
        /// <param name="settingsManager">Settings manager</param>
        /// <param name="settingKey">Setting key that has been changed</param>
        void OnRegionOfInterestLowBoundSettingChanged(ISettingsManager settingsManager, string settingKey)
        {
            if (_context == null)
                return;

            _context.RoiLowBound = settingsManager.GetSetting<Int32>(settingKey);
        }

        /// <summary>
        /// Handles a change to the ROI high bound setting by updating the background
        /// acquisition context. If there is no acquisition context, this method has no effect.
        /// </summary>
        /// <param name="settingsManager">Settings manager</param>
        /// <param name="settingKey">Setting key that has been changed</param>
        void OnRegionOfInterestHighBoundSettingChanged(ISettingsManager settingsManager, string settingKey)
        {
            if (_context == null)
                return;

            _context.RoiHighBound = settingsManager.GetSetting<Int32>(settingKey);
        }

        #endregion

        #region Bookkeeping methods

        /// <summary>
        /// Hooks up the detector and sample changer event handlers.
        /// </summary>
        void HookUpEventHandlers()
        {
            Detector.Disconnected += OnDetectorDisconnected;

            SampleChanger.PositionSeekEnd += OnSampleChangerPositionSeekEnd;
            SampleChanger.Disconnected += OnSampleChangerDisconnected;
        }

        /// <summary>
        /// Determines if the sample changer is at the given background position.
        /// </summary>
        /// <param name="backgroundPosition">Position corresponding to background</param>
        /// <returns>True if at the background position; false otherwise</returns>
        internal bool SampleChangerIsAtBackgroundPosition(int backgroundPosition)
        {
            return backgroundPosition == SampleChanger.CurrentPosition;
        }

        /// <summary>
        /// Subscribes to the TestCompleted and TestAborted aggregate events.
        /// </summary>
        private void SubscribeToAggregateEvents()
        {
            Aggregator.GetEvent<TestCompleted>().Subscribe(t => StartAsync(), ThreadOption.PublisherThread, false);
            Aggregator.GetEvent<TestAborted>().Subscribe(t => InvalidatePreviousBackground(), ThreadOption.PublisherThread, false);
        }

        /// <summary>
        /// Unhooks the detector and sample changer event handlers.
        /// </summary>
        void UnhookEventHandlers()
        {
            Detector.Disconnected -= OnDetectorDisconnected;

            SampleChanger.PositionSeekEnd -= OnSampleChangerPositionSeekEnd;
            SampleChanger.Disconnected -= OnSampleChangerDisconnected;
        }

        #endregion

        #region Sample changer and detector event handlers

        #region Detector

        /// <summary>
        /// Handles the IDetector.Disconnected event by cancelling acquisition.
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that disconnected</param>
        /// <param name="reason">Reason for the detector being disconnected</param>
        void OnDetectorDisconnected(string mcaId, string reason)
        {
            if (AcquisitionWorker.IsBusy)
                StopAsync();
            else
                PerformAcquisitionWorkerCancellation();

            Logger.Log("BackgroundAcquisitionService no longer busy because the detector was disconnected (reason = " + reason + ")",
                Category.Warn, Priority.High);
        }

        #endregion

        #region Sample changer

        /// <summary>
        /// Handles the ISampleChanger.Disconnected event by cancelling acquisition.
        /// </summary>
        /// <param name="reason">Reason for the sample changer being disconnected</param>
        void OnSampleChangerDisconnected(string reason)
        {
            if (AcquisitionWorker.IsBusy)
                StopAsync();
            else
                PerformAcquisitionWorkerCancellation();

            Logger.Log("BackgroundAcquisitionService no longer busy because the sample changer was disconnected (reason = " + reason + ")",
                Category.Warn, Priority.High);
        }

        /// <summary>
        /// Handles the ISampleChanger.PositionSeekEnd event by setting the service state
        /// to "ready to begin acquisition" and publishing the SampleChangerPositionSeekEnd 
        /// event. If the service isn't active is not already seeking to the background, 
        /// this method has no effect.
        /// </summary>
        /// <param name="currPosition">Current sample changer position</param>
        void OnSampleChangerPositionSeekEnd(int currPosition)
        {
            if (!IsActive && (Status != BackgroundAcquisitionServiceStatus.SeekingBackgroundPosition)) return;

            Status = BackgroundAcquisitionServiceStatus.ReadyToBeginAcquisition;
        }

        #endregion

        #endregion

        #region Event raisers

        /// <summary>
        /// Raises the GoodBackgroundAttained event
        /// </summary>
        /// <param name="backgroundCount">Background count</param>
        /// <param name="acquisitionTimeInSeconds">Background acquisition time (in seconds)</param>
        /// <param name="isOverridden">Whether or not the background count is overridden</param>
        protected virtual void FireGoodBackgroundAttained(int backgroundCount, int acquisitionTimeInSeconds, bool isOverridden = false)
        {
            var handler = GoodBackgroundAttained;
            if (handler != null)
                handler(this, new BackgroundAttainedEventArgs(backgroundCount, acquisitionTimeInSeconds, true, isOverridden));
        }

        /// <summary>
        /// Raises the RestartedBecauseOfHighBackground event
        /// </summary>
        protected virtual void FireRestartedBecauseOfHighBackground()
        {
            var handler = RestartedBecauseOfHighBackground;
            if (handler != null)
                handler(this, null);
        }

        #endregion

        #endregion
    }
}
