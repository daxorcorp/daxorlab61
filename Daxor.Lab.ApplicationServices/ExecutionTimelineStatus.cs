﻿namespace Daxor.Lab.ApplicationServices
{
    /// <summary>
    /// Specifies constants defining the status of the test execution relative to a timeline
    /// </summary>
    public enum ExecutionTimelineStatus
    {
        /// <summary>
        /// The status is unknown
        /// </summary>
        Unknown, 

        /// <summary>
        /// The test execution is taking longer than the timeline predicts
        /// </summary>
        BehindSchedule,

        /// <summary>
        /// The test execution matches what the timeline predicts
        /// </summary>
        OnSchedule,

        /// <summary>
        /// The test execution is happening faster than the timeline predicts
        /// </summary>
        AheadOfSchedule,
    }
}
