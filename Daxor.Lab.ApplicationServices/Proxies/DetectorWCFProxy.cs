﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Timers;
using Daxor.Lab.ApplicationServices.Interfaces;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.MCAContracts;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.ApplicationServices.Proxies
{
	// NOTE: Keep-alive timer commented out by Geoff per Andrey's request.
	// Another mechanism is being used for handling time-outs. 1/6/2012

	/// <summary>
	/// WCF service proxy for a gamma detector.
	/// </summary>
	[CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, UseSynchronizationContext = false)]
	public class DetectorWcfProxy : IDetectorProxy
	{
		#region Constants

		private const string ProxyName = " |Detector WCF v6.0| ";
		private const int ProxyReconnectIntervalInSeconds = 10;

		#endregion

		#region Private fields

		private readonly InstanceContext _callbackInstance;
		private readonly object _channelLock = new object();
		private IList<string> _detectorCollection;
		private readonly string _endpointConfigurationName;
		private readonly ILoggerFacade _logger;

		#endregion

		#region Ctor

		/// <summary>
		/// Creates a new DetectorWCFProxy instance
		/// </summary>
		/// <param name="endPointConfigurationName">Name of the client endpoint information 
		/// in the application configuration file</param>
		/// <param name="logger">Event logger instance</param>
		public DetectorWcfProxy(string endPointConfigurationName, ILoggerFacade logger)
		{
			// Get the endpoint name and create proxy.
			IsOpen = false;
			_endpointConfigurationName = endPointConfigurationName;
			_callbackInstance = new InstanceContext(this);

			_logger = logger;
		}

		#endregion

		#region InnerDuplexClient (private inner class)

		/// <summary>
		/// Represents a private duplex client used for communciating with the detector service.
		/// </summary>
		private class InnerDuplexClient : DuplexClientBase<IMultiChannelAnalyzerServiceContract>
		{
			/// <summary>
			/// Creates a new InnerDuplexClient based on a specified instance context and
			/// endpoint configuration.
			/// </summary>
			/// <param name="callbackInstance">Object used to create the instance context 
			/// that associates the callback object with the channel to the service</param>
			/// <param name="endpointConfigurationName">Name of the client endpoint information in the 
			/// application configuration file</param>
			public InnerDuplexClient(InstanceContext callbackInstance, string endpointConfigurationName)
				: base(callbackInstance, endpointConfigurationName) { }

			/// <summary>
			/// Gets the duplex client channel for the detector service.
			/// </summary>
			public IMultiChannelAnalyzerServiceContract SafeChannel
			{
				get
				{
					return Channel;
				}
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the object used to create the instance context that associates the 
		/// callback object with the channel to the service.
		/// </summary>
		internal InstanceContext CallbackInstance
		{
			get { return _callbackInstance; }
		}

		/// <summary>
		/// Gets or sets the InnerDuplexClient (WCF service) instance.
		/// </summary>
		private InnerDuplexClient DuplexClient
		{
			get;
			set;
		}

		/// <summary>
		/// Gets the name of the client endpoint information in the application configuration file.
		/// </summary>
		internal string EndpointConfigurationName
		{
			get { return _endpointConfigurationName; }
		}

		/// <summary>
		/// Gets the channel used for communicating with the WCF service.
		/// </summary>
		internal IMultiChannelAnalyzerServiceContract InnerChannel
		{
			get { return DuplexClient.SafeChannel; }
		}

		/// <summary>
		/// Gets or sets the event logger instance.
		/// </summary>
		internal ILoggerFacade Logger
		{
			get { return _logger; }
		}

		/// <summary>
		/// Gets or sets the reconnection timer.
		/// </summary>
		internal Timer ReconnectTimer
		{
			get;
			private set;
		}

		#endregion

		#region Invoke methods

		/// <summary>
		/// Attempts to perform the given action, which typically involves calling a
		/// method exposed on the WCF service contract.
		/// </summary>
		/// <remarks>If the first attempt results in a communication exception,
		/// the action will be tried one additional time.</remarks>
		/// <param name="invokeAction">Action to perform</param>
		/// <exception cref="FaultException">Invoking the action resulted in a communication fault</exception>
		/// <exception cref="Exception">Retrying the invoke action resulted in a communication exception</exception>
		private void Invoke(Action invokeAction)
		{
			OpenAndSubscribe();
			
			try
			{    
				invokeAction();
			}
			catch (CommunicationException commException)
			{
				// If there is a fault, pass the exception on to the caller.
				if (commException is FaultException)
				{
					throw;
				}
				if (commException is EndpointNotFoundException)
				{
					throw new GammaCounterEndPointNotFoundException("Couldn't find the endpoint for the multiple channel analyzer.", commException);
				}
				// Attempt to retry previous call; if not successful, forward the error up the calling stack
				try
				{
					invokeAction();
				}
				catch (CommunicationException secondCommException)
				{
					const string errorMessage = "DetectorWCFProxy failed to establish communication with the service.";
					Logger.Log(errorMessage + " Exception details: " + secondCommException.Message,
						Category.Exception, Priority.High);

					throw new GammaCounterCommunicationException(errorMessage);
				}
			}
		}

		/// <summary>
		/// Attempts to call the given function, which typically involves calling a
		/// method exposed on the WCF service contract.
		/// </summary>
		/// <remarks>If the first attempt results in a communication exception,
		/// the function will be tried one additional time.</remarks>
		/// <typeparam name="TResult">Return type of the given function</typeparam>
		/// <param name="invokeFunc">Function to call</param>
		/// <returns>Result of the function</returns>
		/// <exception cref="FaultException">Invoking the function resulted in a communication fault</exception>
		/// <exception cref="Exception">Retrying the invoke action resulted in a communication exception</exception>
		private TResult Invoke<TResult>(Func<TResult> invokeFunc)
		{
			TResult result;
			OpenAndSubscribe();
			try
			{    
				result = invokeFunc();
			}
			catch (CommunicationException commException)
			{
				// If there is a fault, pass the exception on to the caller.
				if (commException is FaultException)
				{
					throw;
				}
				// Attempt to retry previous call; if not successful, forward the error up the calling stack
				try
				{
					result = invokeFunc();
				}
				catch (CommunicationException secondCommException)
				{
					const string errorMessage = "DetectorWCFProxy failed to establish communication with the service.";
					Logger.Log(errorMessage + " Exception details: " + secondCommException.Message,
						Category.Exception, Priority.High);

					throw new Exception(errorMessage);
				}
			}
			return result;
		}

		#endregion

		#region Connect/disconnect management methods

		#region Faulted/Opened event handlers

		/// <summary>
		/// Handles the channel faulted event by setting up (or re-enabling) the
		/// reconnection timer so that there will be an attempt to reconnect
		/// with the service after the fault.
		/// </summary>
		/// <remarks>
		/// 1. This handler sets IsOpen to false.
		/// 2. This handler fires the ServiceProxyFailed event.
		/// </remarks>
		/// <param name="sender">Initiator of the event</param>
		/// <param name="e">Event arguments</param>
		private void OnChannelFaulted(object sender, EventArgs e)
		{
			IsOpen = false;
			if (ReconnectTimer == null)
			{
				ReconnectTimer = new Timer();
				ReconnectTimer.Elapsed += ReconnectProxyOnTimer;
				ReconnectTimer.AutoReset = false;
				ReconnectTimer.Interval = new TimeSpan(0, 0, 0, ProxyReconnectIntervalInSeconds).TotalMilliseconds;
			}
			if (!ReconnectTimer.Enabled)
			{
				ReconnectTimer.Enabled = true;
			}

			FireServiceProxyFailed("The channel has faulted and a reconnect timer has been enabled.");
		}

		/// <summary>
		/// Handles the channel opened event by subscribing to callbacks and disabling
		/// the reconnect timer.
		/// </summary>
		/// <remarks>
		/// 1. This handler sets IsOpen to true.
		/// 2. This handler fires the ServiceProxyOpened event.
		/// 3. Any exceptions thrown by Subscribe() are consumed.
		/// </remarks>
		/// <param name="sender">Initiator of the event</param>
		/// <param name="args">Event arguments</param>
		private void OnChannelOpened(object sender, EventArgs args)
		{
		    try
		    {
		        IsOpen = true;

		        // Subscribe for the callbacks.
		        ((IServiceSubscriberContract) this).Subscribe();

		        // Turn off the reconnect timer, because we're connected at this point.
		        if (ReconnectTimer != null)
		            ReconnectTimer.Enabled = false;

		        Logger.Log("DetectorWCFProxy - Connected to detector service.", Category.Debug, Priority.None);
		        FireServiceProxyOpened();
		    }
		    catch (Exception e)
		    {
		        Logger.Log("The channel could not be opened: " + e.Message, Category.Exception, Priority.High);
		    }
		}

		#endregion

		/// <summary>
		/// Closes or aborts the connection to the WCF service. If the connection
		/// could not be closed, an attempt is made to abort the connection.
		/// </summary>
		/// <remarks>Any exceptions thrown by Close() or Abort() are consumed.</remarks>
		private void CloseOrAbortChannel()
		{
			DuplexClient.InnerDuplexChannel.Opened -= OnChannelOpened;
			DuplexClient.InnerDuplexChannel.Faulted -= OnChannelFaulted;

			try
			{
				DuplexClient.Close();
			}
			catch
			{
			    try
			    {
                    DuplexClient.Abort();
			    }
                catch (Exception e)
                {
                    Logger.Log("We tried to close the channel and it didn't work, and neither did aborting the client: " + e.Message, Category.Exception, Priority.High);
                }
			}
		}

		/// <summary>
		/// Attempts to reconnect to the WCF channel by closing/aborting the existing
		/// connection, re-establishing a new channel instance, then opening that
		/// channel.
		/// </summary>
		/// <remarks>Any exceptions thrown are consumed.</remarks>
		private void Reconnect()
		{
		    try
		    {
		        CloseOrAbortChannel();
		        DuplexClient = null;

		        DuplexClient = new InnerDuplexClient(CallbackInstance, EndpointConfigurationName);

		        DuplexClient.InnerDuplexChannel.Opened += OnChannelOpened;
		        DuplexClient.InnerDuplexChannel.Faulted += OnChannelFaulted;

		        DuplexClient.InnerDuplexChannel.Open();
		    }
            catch (Exception e)
            {
                Logger.Log("The client could not be reconnected: " + e.Message, Category.Exception, Priority.High);
            }
		}

		/// <summary>
		/// Responds to the reconnect timer by reconnecting to the WCF service.
		/// </summary>
		/// <remarks>
		/// 1. This method is called when the reconnect timer has elapsed (which
		/// occurs after the channel has faulted).
		/// 2. Any exceptions thrown are consumed.
		/// </remarks>
		/// <param name="sender">Initiator of the event</param>
		/// <param name="args">Event arguments</param>
		private void ReconnectProxyOnTimer(object sender, ElapsedEventArgs args)
		{
		    try
		    {
		        ReconnectTimer.Enabled = false;
		        Logger.Log("DetectorWCFProxy recreating detector proxy on timer -- " + DateTime.Now.ToLongTimeString(),
		            Category.Debug, Priority.None);
		        Reconnect();
		    }
            catch (Exception e)
            {
                Logger.Log("The client could not be reconnected on a timer: " + e.Message, Category.Exception, Priority.High);
            }
		}

		#endregion

		#region IMultiChannelAnalyzerService members

		/// <summary>
		/// Gets whether the spectrum acquisition has completed.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		public bool HasAcquisitionCompleted(string detectorName)
		{
			return Invoke(() => InnerChannel.HasAcquisitionCompleted(detectorName));
		}

		/// <summary>
		/// Gets the detector's current voltage.
		/// </summary>
		/// <param name="detectorName"></param>
		/// <returns>The detector's voltage</returns>
		public int GetHighVoltage(string detectorName)
		{
			return Invoke(() => InnerChannel.GetHighVoltage(detectorName));
		}

		/// <summary>
		/// Sets the detector's current voltage.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <param name="voltage">Desired voltage</param>
		public void SetHighVoltage(string detectorName, int voltage)
		{
			Invoke(() => InnerChannel.SetHighVoltage(detectorName, voltage));
		}

		/// <summary>
		/// Gets the detector's current fine gain.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <returns>The detector's fine gain</returns>
		public double GetFineGain(string detectorName)
		{
			return Invoke(() => InnerChannel.GetFineGain(detectorName));
		}

		/// <summary>
		/// Sets the detector's fine gain.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <param name="fineGain">Desired fine gain level</param>
		public void SetFineGain(string detectorName, double fineGain)
		{
			Invoke(() => InnerChannel.SetFineGain(detectorName, fineGain));
		}

		/// <summary>
		/// Gets the detector's current noise level.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <returns>The detector's noise level</returns>
		public int GetNoiseLevel(string detectorName)
		{
			return Invoke(() => InnerChannel.GetNoiseLevel(detectorName));
		}

		/// <summary>
		/// Sets the detector's noise level.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <param name="noise">Desired noise level</param>
		public void SetNoiseLevel(string detectorName, int noise)
		{
			Invoke(() => InnerChannel.SetNoiseLevel(detectorName, noise));
		}

		/// <summary>
		/// Gets the detector's current low level discriminator value.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <returns>The detector's low level discriminator value.</returns>
		public int GetLowerLevelDiscriminator(string detectorName)
		{
			return Invoke(() => InnerChannel.GetLowerLevelDiscriminator(detectorName));
		}

		/// <summary>
		/// Sets the detector's low level discriminator value.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <param name="lld">Desired low level discriminator value</param>
		public void SetLowerLevelDiscriminator(string detectorName, int lld)
		{
			Invoke(() => InnerChannel.SetLowerLevelDiscriminator(detectorName, lld));
		}

		/// <summary>
		/// Gets whether the detector is currently acquiring a spectrum.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <returns>True if the detector is acquiring a spectrum; false otherwise</returns>
		public bool IsAcquiring(string detectorName)
		{
			return Invoke(() => InnerChannel.IsAcquiring(detectorName));
		}

		/// <summary>
		/// Gets the detector's serial number.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <returns>The serial number of the detector</returns>
		public int GetSerialNumber(string detectorName)
		{
			return Invoke(() => InnerChannel.GetSerialNumber(detectorName));
		}

		/// <summary>
		/// Gets the detector's analog-to-digital (ADC) current voltage.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <returns>The detector's analog-to-digital (ADC) current voltage.</returns>
		public int GetAnalogToDigitalConverterHighVoltage(string detectorName)
		{
			return Invoke(() => InnerChannel.GetAnalogToDigitalConverterHighVoltage(detectorName));
		}

		/// <summary>
		/// Gets whether or not the detector is connected to the computer.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <returns>True if the detector is connected; false otherwise</returns>
		public bool IsConnected(string detectorName)
		{
			return Invoke(() => InnerChannel.IsConnected(detectorName));
		}

		/// <summary>
		/// Gets whether or not the real time for acquisition has been preset.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <returns>True if the real time has been preset; false otherwise</returns>
		public bool IsRealTimePreset(string detectorName)
		{
			return Invoke(() => InnerChannel.IsRealTimePreset(detectorName));
		}

		/// <summary>
		/// Gets whether or not the live time for acquisition has been preset.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <returns>True if the live time has been preset; false otherwise</returns>
		public bool IsLiveTimePreset(string detectorName)
		{
			return Invoke(() => InnerChannel.IsLiveTimePreset(detectorName));
		}

		/// <summary>
		/// Gets whether or not the gross counts for acquisition have been preset.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <returns>True if the gross counts have been preset; false otherwise</returns>
		public bool IsGrossCountPreset(string detectorName)
		{
			return Invoke(() => InnerChannel.IsGrossCountPreset(detectorName));
		}

		/// <summary>
		/// Shuts down the detector's monitoring thread.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		public void ShutDown(string detectorName)
		{
			Invoke(() => InnerChannel.ShutDown(detectorName));
		}

		/// <summary>
		/// Gets the detector's current spectrum.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <returns>The detector's spectrum.</returns>
		public SpectrumData GetSpectrumData(string detectorName)
		{
			return Invoke(() => InnerChannel.GetSpectrumData(detectorName));
		}

		/// <summary>
		/// Presets the acquisition time.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <param name="isLiveTime">Whether the specified time is the live time</param>
		/// <param name="presetTimeInSeconds">Duration of acquisition (in seconds)</param>
		public void PresetTime(string detectorName, bool isLiveTime, long presetTimeInSeconds)
		{
			Invoke(() => InnerChannel.PresetTime(detectorName, isLiveTime, presetTimeInSeconds));
		}

		/// <summary>
		/// Presets the acquisition count.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <param name="presetCounts">Goal counts to acquire</param>
		/// <param name="startChannel">Starting channel</param>
		/// <param name="endChannel">Ending channel</param>
		public void PresetGrossCountInRegionOfInterest(string detectorName, long presetCounts, int startChannel, int endChannel)
		{
			Invoke(() => InnerChannel.PresetGrossCountInRegionOfInterest(detectorName, presetCounts, startChannel, endChannel));
		}

		/// <summary>
		/// Clears the detector's current spectrum and acquisition times.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		public void ClearSpectrumAndPresets(string detectorName)
		{
			Invoke(() => InnerChannel.ClearSpectrumAndPresets(detectorName));
		}

		/// <summary>
		/// Starts the spectrum acquisition.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <returns></returns>
		public bool StartAcquisition(string detectorName)
		{
			return Invoke(() => InnerChannel.StartAcquisition(detectorName));
		}

		/// <summary>
		/// Stops the spectrum acquisition.
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		public void StopAcquisition(string detectorName)
		{
			Invoke(() => InnerChannel.StopAcquisition(detectorName));
		}

		/// <summary>
		/// Gets the length of the detector's current spectrum (in channels).
		/// </summary>
		/// <param name="detectorName">Identifier of the detector</param>
		/// <returns>The length of the detector's spectrum (in channels).</returns>
		public int GetSpectrumLength(string detectorName)
		{
			return Invoke(() => InnerChannel.GetSpectrumLength(detectorName));
		}

		/// <summary>
		/// Gets a list of all detectors registered with the service.
		/// </summary>
		/// <returns>List of detectors</returns>
		public IList<string> GetAllMultiChannelAnalyzerIds()
		{
			//TODO: Change caching... no more MultiChannelAnalyzerCollection
			return Invoke(() => InnerChannel.GetAllMultiChannelAnalyzerIds());
		}

		/// <summary>
		/// Saves the detector configurations.
		/// </summary>
		public void SaveConfigurationsForAllMultiChannelAnalyzers()
		{
			Invoke(() => InnerChannel.SaveConfigurationsForAllMultiChannelAnalyzers());
		}

		/// <summary>
		/// Resets the detector configuration.
		/// </summary>
		public void ResetMultiChannelAnalyzerToDefaultConfiguration(string mcaId)
		{
			Invoke(() => InnerChannel.ResetMultiChannelAnalyzerToDefaultConfiguration(mcaId));
		}

		#endregion

		#region IServiceSubscriber members

		// This interface is implemented explicity so that Subscribe() and Unsubscribe() aren't
		// made public on this class.

		/// <summary>
		/// Subscribes to the channel for callbacks.
		/// </summary>
		void IServiceSubscriberContract.Subscribe()
		{
			Invoke(() => InnerChannel.Subscribe());
		}

		/// <summary>
		/// Unsubscribes from the channel.
		/// </summary>
		void IServiceSubscriberContract.Unsubscribe()
		{
			Invoke(() => InnerChannel.Unsubscribe());
		}

		#endregion

		#region IDetectorProxy members

		/// <summary>
		/// Gets the list of detector names registered with the WCF service.
		/// </summary>
		public IList<string> DetectorCollection
		{
			get
			{
				if (_detectorCollection == null)
				{
					try
					{
						_detectorCollection = GetAllMultiChannelAnalyzerIds();
					}
					catch
					{
						_detectorCollection = null;
					}
				}
				return _detectorCollection;
			}
		}

		#endregion

		#region IServiceProxy members

		/// <summary>
		/// Occurs when the WCF proxy fails.
		/// </summary>
		public event ServiceProxyEventHandler ServiceProxyFailed;

		/// <summary>
		/// Occurs when the WCF proxy is opened.
		/// </summary>
		public event ServiceProxyEventHandler ServiceProxyOpened;

		/// <summary>
		/// Gets or sets whether the proxy is open (i.e., connected).
		/// </summary>
		public bool IsOpen
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets the name of the proxy (e.g., local, WCF, demo).
		/// </summary>
		public string Name
		{
			get { return ProxyName; }
		}

		/// <summary>
		/// Opens and subscribes to a WCF client.
		/// </summary>
		/// <remarks>If the proxy is already open, this method has no effect.</remarks>
		/// <exception cref="EndpointNotFoundException">The WCF endpoint could not be found</exception>
		/// <exception cref="CommunicationException">There was a problem communicating with the WCF endpoint</exception>
		public void OpenAndSubscribe()
		{
			// Don't attempt to recreate proxy if a valid proxy already exists.
			if (IsOpen)
				return;

			try
			{
				// We have a client, but it's faulted -- so close it.
				if ((DuplexClient != null) && (DuplexClient.State == CommunicationState.Faulted))
					CloseOrAbortChannel();

				// Create a new client.
				lock (_channelLock)
				{
					DuplexClient = new InnerDuplexClient(CallbackInstance, EndpointConfigurationName);
					DuplexClient.InnerDuplexChannel.Opened += OnChannelOpened;
					DuplexClient.InnerDuplexChannel.Faulted += OnChannelFaulted;
					DuplexClient.InnerDuplexChannel.Open();
				}
			}
			catch (EndpointNotFoundException ex)
			{
				string err = "DetectorWCFProxy failed to find the WCF service residing at the expected location.\n" +
							 "Will auto-reconnect in " + ProxyReconnectIntervalInSeconds + " second(s).";

				FireServiceProxyFailed(err);
				throw new GammaCounterEndPointNotFoundException("The multiple channel analyzer could not find the endpoint.", ex);
			}
			catch (CommunicationException ex)
			{
				string err = "DetectorWCFProxy failed to establish communication with WCF service.\n" +
							 "Will auto-reconnect in " + ProxyReconnectIntervalInSeconds + " second(s).";

				FireServiceProxyFailed(err);
				throw new GammaCounterCommunicationException("The application could not communicate with the multiple channel analyzer.", ex);
			}
		}

		#endregion

		#region IServiceProxy event handlers

		/// <summary>
		/// Raises the ServiceProxyFailed event.
		/// </summary>
		/// <param name="errorMessage">Error message associated with the event</param>
		protected virtual void FireServiceProxyFailed(string errorMessage)
		{
			var handler = ServiceProxyFailed;
			if (handler != null)
				handler(this, null);

			Logger.Log("DetectorWCFProxy.ServiceProxyFailed event fired with message '" + errorMessage + "'",
				Category.Exception, Priority.High);
		}

		/// <summary>
		/// Raises the ServiceProxyOpened event.
		/// </summary>
		protected virtual void FireServiceProxyOpened()
		{
			var handler = ServiceProxyOpened;
			if (handler != null)
				handler(this, null);

			Logger.Log("DetectorWCFProxy.ServiceProxyOpened event fired", Category.Info, Priority.None);
		}

		#endregion
	}
}
