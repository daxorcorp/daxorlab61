﻿using System;
using System.ServiceModel;
using System.Timers;
using Daxor.Lab.ApplicationServices.Interfaces;
using Daxor.Lab.SCContracts;
using Microsoft.Practices.Composite.Logging;
using Daxor.Lab.Infrastructure.Exceptions;

namespace Daxor.Lab.ApplicationServices.Proxies
{
	/// <summary>
	/// WCF service proxy for a sample changer.
	/// </summary>
	[CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, UseSynchronizationContext = false)]
	public class SampleChangerWcfProxy : ISampleChangerProxy
	{
		#region Constants

		private const string ProxyName = "|Sample Changer WCF v3.3.4|";
		private const int ProxyReconnectIntervalInSeconds = 10;
		
		#endregion

		#region Private fields

		private readonly InstanceContext _callbackInstance;
		private readonly object _channelLock = new object();
		private readonly string _endpointConfigurationName;
		private readonly ILoggerFacade _logger;

		#endregion

		#region Ctor

		/// <summary>
		/// Creates a new SampleChangerWCFProxy instance.
		/// </summary>
		/// <param name="endPointConfigurationName">Name of the client endpoint information 
		/// in the application configuration file</param>
		/// <param name="logger">Event logger instance</param>
		public SampleChangerWcfProxy(string endPointConfigurationName, ILoggerFacade logger)
		{
			_endpointConfigurationName = endPointConfigurationName;
			_callbackInstance = new InstanceContext(this);
			_logger = logger;
		}

		#endregion

		#region InnerDuplexClient (private inner class)

		/// <summary>
		/// Represents a private duplex client used for communicating with the sample changer service.
		/// </summary>
		private class InnerDuplexClient : DuplexClientBase<ISampleChangerService>
		{
			/// <summary>
			/// Creates a new InnerDuplexClient based on a specified instance context and
			/// endpoint configuration.
			/// </summary>
			/// <param name="callbackInstance">Object used to create the instance context 
			/// that associates the callback object with the channel to the service</param>
			/// <param name="endpointConfigurationName">Name of the client endpoint information in the 
			/// application configuration file</param>
			public InnerDuplexClient(InstanceContext callbackInstance, string endpointConfigurationName)
				: base(callbackInstance, endpointConfigurationName) { }

			/// <summary>
			/// Gets the duplex client channel for the sample changer service.
			/// </summary>
			public ISampleChangerService SafeChannel
			{
				get
				{
					return Channel;
				}
			}
		}

		#endregion

		#region Properties

		/// <summary>
		/// Gets the object used to create the instance context that associates the 
		/// callback object with the channel to the service.
		/// </summary>
		internal InstanceContext CallbackInstance
		{
			get { return _callbackInstance; }
		}

		/// <summary>
		/// Gets or sets the InnerDuplexClient (WCF service) instance.
		/// </summary>
		private InnerDuplexClient DuplexClient
		{
			get;
			set;
		}

		/// <summary>
		/// Gets the name of the client endpoint information in the application configuration file.
		/// </summary>
		internal string EndpointConfigurationName
		{
			get { return _endpointConfigurationName; }
		}

		/// <summary>
		/// Gets the channel used for communicating with the WCF service.
		/// </summary>
		internal ISampleChangerService InnerChannel
		{
			get { return DuplexClient.SafeChannel; }
		}

		/// <summary>
		/// Gets or sets the event logger instance.
		/// </summary>
		internal ILoggerFacade Logger
		{
			get { return _logger; }
		}

		/// <summary>
		/// Gets or sets the reconnection timer.
		/// </summary>
		internal Timer ReconnectTimer
		{
			get;
			private set;
		}

		#endregion

		#region Invoke

		/// <summary>
		/// Attempts to perform the given action, which typically involves calling a
		/// method exposed on the WCF service contract.
		/// </summary>
		/// <remarks>If the first attempt results in a communication exception,
		/// the action will be tried one additional time.</remarks>
		/// <param name="invokeAction">Action to perform</param>
		/// <exception cref="FaultException">Invoking the action resulted in a communication fault</exception>
		/// <exception cref="Exception">Retrying the invoke action resulted in a communication exception</exception>
		private void Invoke(Action invokeAction)
		{
			OpenAndSubscribe();
			try
			{
				invokeAction();
			}
			catch (CommunicationException commException)
			{
				// If there is a fault, pass the exception on to the caller.
				if (commException is FaultException)
				{
					throw;
				}
				// Attempt to retry previous call; if not successful, forward the error up the calling stack
				try
				{
					invokeAction();
				}
				catch (CommunicationException secondCommException)
				{
					const string errorMessage = "SampleChangerWCFProxy failed to establish communication with the service.";
					Logger.Log(errorMessage + " Exception details: " + secondCommException.Message,
						Category.Exception, Priority.High);

					throw new Exception(errorMessage);
				}
			}
		}

		/// <summary>
		/// Attempts to call the given function, which typically involves calling a
		/// method exposed on the WCF service contract.
		/// </summary>
		/// <remarks>If the first attempt results in a communication exception,
		/// the function will be tried one additional time.</remarks>
		/// <typeparam name="TResult">Return type of the given function</typeparam>
		/// <param name="invokeFunc">Function to call</param>
		/// <returns>Result of the function</returns>
		/// <exception cref="FaultException">Invoking the function resulted in a communication fault</exception>
		/// <exception cref="Exception">Retrying the invoke action resulted in a communication exception</exception>
		private TResult Invoke<TResult>(Func<TResult> invokeFunc)
		{
			TResult result;
			OpenAndSubscribe();
			try
			{
				result = invokeFunc();
			}
			catch (CommunicationException commException)
			{
				// If there is a fault, pass the exception on to the caller.
				if (commException is FaultException)
				{
					throw;
				}
				// Attempt to retry previous call; if not successful, forward the error up the calling stack
				try
				{
					result = invokeFunc();
				}
				catch (CommunicationException secondCommException)
				{
					const string errorMessage = "SampleChangerWCFProxy failed to establish communication with the service.";
					Logger.Log(errorMessage + " Exception details: " + secondCommException.Message,
						Category.Exception, Priority.High);

					throw new Exception(errorMessage);
				}
			}

			return result;
		}

		#endregion

		#region Connect/disconnect management methods

		#region Faulted/Opened event handlers

		/// <summary>
		/// Handles the channel faulted event by setting up (or re-enabling) the
		/// reconnection timer so that there will be an attempt to reconnect
		/// with the service after the fault.
		/// </summary>
		/// <remarks>
		/// 1. This handler sets IsOpen to false.
		/// 2. This handler fires the ServiceProxyFailed event.
		/// </remarks>
		/// <param name="sender">Initiator of the event</param>
		/// <param name="e">Event arguments</param>
		private void OnChannelFaulted(object sender, EventArgs e)
		{
			IsOpen = false;
			if (ReconnectTimer == null)
			{
				ReconnectTimer = new Timer();
				ReconnectTimer.Elapsed += ReconnectProxyOnTimer;
				ReconnectTimer.AutoReset = false;
				ReconnectTimer.Interval = new TimeSpan(0, 0, 0, ProxyReconnectIntervalInSeconds).TotalMilliseconds;
			}
			if (!ReconnectTimer.Enabled)
			{
				ReconnectTimer.Enabled = true;
			}

			FireServiceProxyFailed("The channel has faulted and a reconnect timer has been enabled.");
		}

		/// <summary>
		/// Handles the channel opened event by subscribing to callbacks and disabling
		/// the reconnect timer.
		/// </summary>
		/// <remarks>
		/// 1. This handler sets IsOpen to true.
		/// 2. This handler fires the ServiceProxyOpened event.
		/// 3. Any exceptions thrown by Subscribe() are consumed.
		/// </remarks>
		/// <param name="sender">Initiator of the event</param>
		/// <param name="args">Event arguments</param>
		private void OnChannelOpened(object sender, EventArgs args)
		{
		    try
		    {
		        IsOpen = true;

		        // Subscribe for the callbacks.
		        ((ISubscriber) this).Subscribe();

		        // Turn off the reconnect timer, because we're connected at this point.
		        if (ReconnectTimer != null)
		            ReconnectTimer.Enabled = false;

		        Logger.Log("SampleChangerWCFProxy -- Connected to sample changer service.", Category.Debug, Priority.None);
		        FireServiceProxyOpened();
            }
            catch (Exception e)
            {
                Logger.Log("The channel could not be opened: " + e.Message, Category.Exception, Priority.High);
            }
		}

		#endregion

		/// <summary>
		/// Closes or aborts the connection to the WCF service. If the connection
		/// could not be closed, an attempt is made to abort the connection.
		/// </summary>
		/// <remarks>Any exceptions thrown by Close() or Abort() are consumed.</remarks>
		private void CloseOrAbortChannel()
		{
			DuplexClient.InnerDuplexChannel.Opened -= OnChannelOpened;
			DuplexClient.InnerDuplexChannel.Faulted -= OnChannelFaulted;

			try
			{
				DuplexClient.Close();
			}
			catch
			{
			    try
			    {
			        DuplexClient.Abort();
			    }
                catch (Exception e)
                {
                    Logger.Log("We tried to close the channel and it didn't work, and neither did aborting the client: " + e.Message, Category.Exception, Priority.High);
                }
			}
		}

		/// <summary>
		/// Attempts to reconnect to the WCF channel by closing/aborting the existing
		/// connection, re-establishing a new channel instance, then opening that
		/// channel.
		/// </summary>
		/// <remarks>Any exceptions thrown are consumed.</remarks>
		private void Reconnect()
		{
		    try
		    {
		        CloseOrAbortChannel();
		        DuplexClient = null;

		        DuplexClient = new InnerDuplexClient(CallbackInstance, EndpointConfigurationName);

		        DuplexClient.InnerDuplexChannel.Opened += OnChannelOpened;
		        DuplexClient.InnerDuplexChannel.Faulted += OnChannelFaulted;

		        DuplexClient.InnerDuplexChannel.Open();
            }
            catch (Exception e)
            {
                Logger.Log("The client could not be reconnected: " + e.Message, Category.Exception, Priority.High);
            }
		}

		/// <summary>
		/// Responds to the reconnect timer by reconnecting to the WCF service.
		/// </summary>
		/// <remarks>
		/// 1. This method is called when the reconnect timer has elapsed (which
		/// occurs after the channel has faulted).
		/// 2. Any exceptions thrown are consumed.
		/// </remarks>
		/// <param name="sender">Initiator of the event</param>
		/// <param name="args">Event arguments</param>
		private void ReconnectProxyOnTimer(object sender, ElapsedEventArgs args)
		{
		    try
		    {
		        ReconnectTimer.Enabled = false;
		        Logger.Log("SampleChangerWCFProxy recreating sample changer proxy on timer -- " + DateTime.Now.ToLongTimeString(), Category.Debug, Priority.None);
		        Reconnect();
		    }
            catch (Exception e)
            {
                Logger.Log("The client could not be reconnected on a timer: " + e.Message, Category.Exception, Priority.High);
            }
		}

		#endregion

		#region ISampleChangerService Members

		/// <summary>
		/// Gets the current position of the sample changer.
		/// </summary>
		/// <returns>Current position of the sample changer</returns>
		public int GetCurrentPosition()
		{
			return Invoke(() => InnerChannel.GetCurrentPosition());
		}

		/// <summary>
		/// Gets the length of time between reading positions.
		/// </summary>
		/// <returns>Length of time between reading positions</returns>
		public TimeSpan GetSleepTime()
		{
			return Invoke(() => InnerChannel.GetSleepTime());
		}

		/// <summary>
		/// Gets the delay (in seconds) between testing for position changes.
		/// </summary>
		/// <returns>Delay (in seconds) between testing for position changes</returns>
		public double GetTestDelay()
		{
			return Invoke(() => InnerChannel.GetTestDelay());
		}

		/// <summary>
		/// Gets the time at which the last position was attained.
		/// </summary>
		/// <returns>Time at which the last position was attained</returns>
		public DateTime GetTimeLastPositionAttained()
		{
			return Invoke(() => InnerChannel.GetTimeLastPositionAttained());
		}

		/// <summary>
		/// Gets the sample changer version.
		/// </summary>
		/// <returns>Sample changer version</returns>
		public string GetVersion()
		{
			return Invoke(() => InnerChannel.GetVersion());
		}

		/// <summary>
		/// Gets whether the sample changer is currently moving (i.e, changing positions).
		/// </summary>
		/// <returns>True if the sample changer is moving; false otherwise</returns>
		public bool IsMoving()
		{
			return Invoke(() => InnerChannel.IsMoving());
		}

		/// <summary>
		/// Gets whether or not the sample changer connection is opened.
		/// </summary>
		/// <returns>True if the sample changer connection is opened; false otherwise</returns>
		public bool IsOpened()
		{
			return Invoke(() => InnerChannel.IsOpened());
		}

		/// <summary>
		/// Moves the sample changer to the specified position.
		/// </summary>
		/// <param name="goalPosition">Goal/destination position</param>
		public void MoveToPosition(int goalPosition)
		{
			Invoke(() => InnerChannel.MoveToPosition(goalPosition));
		}

		/// <summary>
		/// "Pulses" the sample changer by moving it one position.
		/// </summary>
		public void PulseSampleChanger()
		{
			Invoke(() => InnerChannel.PulseSampleChanger());
		}

		/// <summary>
		/// Sets the length of time between reading positions to the specified amount.
		/// </summary>
		/// <param name="sleepSpan">Desired sleep time</param>
		public void SetSleepTime(TimeSpan sleepSpan)
		{
			Invoke(() => InnerChannel.SetSleepTime(sleepSpan));
		}

		/// <summary>
		/// Sets the delay (in seconds) between testing for position changes.
		/// </summary>
		/// <param name="testDelayInSeconds">Delay amount (in seconds)</param>
		public void SetTestDelay(double testDelayInSeconds)
		{
			Invoke(() => InnerChannel.SetTestDelay(testDelayInSeconds));
		}

		/// <summary>
		/// Stops the sample changer from moving.
		/// </summary>
		public void StopSampleChanger()
		{
			Invoke(() => InnerChannel.StopSampleChanger());
		}

		public SensorData GetRawSensorData()
		{
			return Invoke(() => InnerChannel.GetRawSensorData());
		}
		/// <summary>
		/// Terminates the connection to the sample changer.
		/// </summary>
		public void Terminate()
		{
			Invoke(() => InnerChannel.Terminate());
		}

		/// <summary>
		/// Checks to see if the sample changer has power ran to it.
		/// </summary>
		/// <returns>True if the sample changer is receiving power, false otherwise</returns>
		public bool IsPoweredUp()
		{
			return Invoke(() => InnerChannel.IsPoweredUp());
		}

		#endregion

		#region ISubscriber members

		// This interface is implemented explicity so that Subscribe() and Unsubscribe() aren't
		// made public on this class.

		/// <summary>
		/// Subscribes to the channel for callbacks.
		/// </summary>
		void ISubscriber.Subscribe()
		{
			Invoke(() => InnerChannel.Subscribe());
		}

		/// <summary>
		/// Unsubscribes from the channel.
		/// </summary>
		void ISubscriber.Unsubscribe()
		{
			Invoke(() => InnerChannel.Unsubscribe());
		}

		#endregion

		#region IServiceProxy members

		/// <summary>
		/// Occurs when the proxy fails.
		/// </summary>
		public event ServiceProxyEventHandler ServiceProxyFailed;

		/// <summary>
		/// Occurs when the proxy is opened.
		/// </summary>
		public event ServiceProxyEventHandler ServiceProxyOpened;

		/// <summary>
		/// Gets or sets whether the proxy is open (i.e., connected).
		/// </summary>
		public bool IsOpen
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets the name of the WCF service proxy.
		/// </summary>
		public string Name
		{
			get { return ProxyName; }
		}

		/// <summary>
		/// Opens and subscribes to a WCF client.
		/// </summary>
		/// <remarks>If the proxy is already open, this method has no effect.</remarks>
		/// <exception cref="EndpointNotFoundException">The WCF endpoint could not be found</exception>
		/// <exception cref="CommunicationException">There was a problem communicating with the WCF endpoint</exception>
		public void OpenAndSubscribe()
		{
			// Don't attempt to recreate proxy if a valid proxy already exists.
			if (IsOpen)
				return;

			try
			{
				// We have a client, but it's faulted -- so close it.
				if ((DuplexClient != null) && (DuplexClient.State == CommunicationState.Faulted))
					CloseOrAbortChannel();

				// Create a new client.
				lock (_channelLock)
				{
					DuplexClient = new InnerDuplexClient(CallbackInstance, EndpointConfigurationName);
					DuplexClient.InnerDuplexChannel.Opened += OnChannelOpened;
					DuplexClient.InnerDuplexChannel.Faulted += OnChannelFaulted;
					DuplexClient.InnerDuplexChannel.Open();
				}
			}
			catch (EndpointNotFoundException ex)
			{
				var err = "SampleChangerWCFProxy failed to find the WCF service residing at the expected location.\n" +
							 "Will auto-reconnect in " + ProxyReconnectIntervalInSeconds + " second(s).";
				FireServiceProxyFailed(err);
				throw new SampleChangerEndPointNotFoundException("Couldn't find the endpoint for the sample changer.", ex);
			}
			catch (CommunicationException ex)
			{
				string err = "SampleChangerWCFProxy failed to establish communication with WCF service.\n" +
							 "Will auto-reconnect in " + ProxyReconnectIntervalInSeconds + " second(s).";
				FireServiceProxyFailed(err);
				throw new SampleChangerCommunicationException("Couldn't establish the connection for the sample changer.", ex);
			}
		}

		#endregion

		#region IServiceProxy event handlers

		/// <summary>
		/// Raises the ServiceProxyFailed event.
		/// </summary>
		/// <param name="errorMessage">Error message associated with the event</param>
		protected virtual void FireServiceProxyFailed(string errorMessage)
		{
			var handler = ServiceProxyFailed;
			if (handler != null)
				handler(this, errorMessage);

			Logger.Log("SampleChangerWCFProxy.ServiceProxyFailed event fired with message '" + errorMessage + "'",
				Category.Exception, Priority.High);
		}

		/// <summary>
		/// Raises the ServiceProxyOpened event.
		/// </summary>
		protected virtual void FireServiceProxyOpened()
		{
			var handler = ServiceProxyOpened;
			if (handler != null)
				handler(this, null);

			Logger.Log("SampleChangerWCFProxy.ServiceProxyOpened event fired", Category.Info, Priority.None);
		}

		#endregion

	}
}
