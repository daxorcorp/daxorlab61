﻿using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using System;
using Daxor.Lab.Domain.Entities.Base;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.ApplicationServices
{
	/// <summary>
	/// Simple user authentication service
	/// </summary>
	public class AuthenticationManager : ObservableObject, IAuthenticationManager
	{
		#region Fields

		private User _loggedInUser;
		private readonly ISettingsManager _settingsManager;

		#endregion

		#region Properties
		public User LoggedInUser
		{
			get { return _loggedInUser; }
			private set { base.SetValue(ref _loggedInUser, "LoggedInUser", value); }
		}
		#endregion

		#region Ctor
		public AuthenticationManager(ISettingsManager settingsManager)
		{
			_settingsManager = settingsManager;
		}
		#endregion

		#region Methods

		public bool Login(AuthorizationLevel desiredLevel, string lPassword)
		{
			try
			{
				var passcode = _settingsManager.GetSetting<String>(AuthorizationLevelToSettingKey(desiredLevel));
				if (passcode == lPassword)
				{
					LoggedInUser = new User(desiredLevel, lPassword);
					_settingsManager.SetSetting(RuntimeSettingKeys.SystemPasswordSecureExport, passcode);
					RaiseLoginEvent();
				}
				else
				{
					RaiseLoginFailed();
				}
			}
			catch
			{
				LoggedInUser = null;
			}
			return LoggedInUser != null;
		}

		public User Login(String password)
		{
			try
			{
				var topBottomAuthLevels = new[] { AuthorizationLevel.CustomerAdmin, AuthorizationLevel.Service, AuthorizationLevel.Internal };
				User loggedInUser = null;

				// ReSharper disable once LoopCanBeConvertedToQuery
				foreach (var t in topBottomAuthLevels)
				{
					var passcode = _settingsManager.GetSetting<String>(AuthorizationLevelToSettingKey(t));
					if (passcode != password) continue;
					loggedInUser = new User(t, password);
					break;
				}

				if (loggedInUser == null)
					RaiseLoginFailed();
				else
					RaiseLoginEvent();

				_loggedInUser = loggedInUser;
			}
			catch
			{
				_loggedInUser = null;
			}
			return _loggedInUser;
		}

		public void Logout()
		{
			LoggedInUser = null;
			FireLogoutCompleted();
		}

		#endregion

		#region Events

		public event EventHandler LoginFailed;
		public event EventHandler LoginSucceeded;
		public event EventHandler LogoutCompleted;

		protected virtual void RaiseLoginFailed()
		{
			EventHandler handler = LoginFailed;
			if (handler != null)
				handler(this, null);
		}
		protected virtual void FireLoginSucceeded()
		{
			EventHandler handler = LoginSucceeded;
			if (handler != null)
				handler(this, null);
		}
		protected virtual void FireLogoutCompleted()
		{
			EventHandler handler = LogoutCompleted;
			if (handler != null)
				handler(this, null);
		}

		private void RaiseLoginEvent()
		{
			if (LoggedInUser != null)
				FireLoginSucceeded();
			else
				RaiseLoginFailed();
		}
		#endregion

		#region Helper Methods
		private static string AuthorizationLevelToSettingKey(AuthorizationLevel level)
		{
			var settingKey = String.Empty;
			switch (level)
			{
				case AuthorizationLevel.CustomerAdmin:
					settingKey = SettingKeys.SystemPasswordCustomerAdmin;
					break;
				case AuthorizationLevel.Service:
					settingKey = SettingKeys.SystemPasswordService;
					break;
				case AuthorizationLevel.Internal:
					settingKey = SettingKeys.SystemPasswordInternal;
					break;
			}

			return settingKey;
		}
		#endregion
	}
}
