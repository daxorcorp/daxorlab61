﻿using Daxor.Lab.Domain.Entities;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.ApplicationServices
{
    /// <summary>
    /// Provides a data mapping between the Spectrum and SpectrumData classes.
    /// </summary>
    public static class SpectrumMapper
    {
        /// <summary>
        /// Creates a Spectrum instance from a given SpectrumData instance.
        /// </summary>
        /// <param name="spectrumData">SpectrumData instance to convert from</param>
        /// <returns>Spectrum instance whose properties match that of the given 
        /// SpectrumData instnace</returns>
        /// <remarks>SpectrumData uses microseconds; Spectrum uses seconds</remarks>
        public static Spectrum From(SpectrumData spectrumData)
        {
            if (spectrumData == null)
                return null;

            return new Spectrum
            {
                LiveTimeInSeconds = spectrumData.LiveTimeInMicroseconds / TimeConstants.MicrosecondsPerSecond,
                RealTimeInSeconds = spectrumData.RealTimeInMicroseconds / TimeConstants.MicrosecondsPerSecond,
                SpectrumArray = spectrumData.SpectrumArray,
            };
        }
    }
}
