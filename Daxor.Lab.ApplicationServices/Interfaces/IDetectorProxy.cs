﻿using System.Collections.Generic;
using Daxor.Lab.MCAContracts;

namespace Daxor.Lab.ApplicationServices.Interfaces
{
    /// <summary>
    /// Represents the behavior of a service proxy for a detector.
    /// </summary>
    public interface IDetectorProxy : IServiceProxy, IMultiChannelAnalyzerServiceContract
    {
        /// <summary>
        /// Gets the list of detector names associated with the service proxy.
        /// </summary>
        IList<string> DetectorCollection { get; }
    }
}
