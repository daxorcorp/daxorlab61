﻿using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.ApplicationServices.Interfaces
{
    /// <summary>
    /// Defines the behavior of helper object that can compute the amount of time
    /// required to execute the components of a test
    /// </summary>
    public interface IExecutionTimeHelper
    {
        /// <summary>
        /// Builds an execution timeline for the given test execution context
        /// </summary>
        /// <param name="executionContext">Execution context on which the timeline is based</param>
        /// <returns>Execution timeline for the given test execution context</returns>
        ExecutionTimeline BuildExecutionTimeline(ITestExecutionContext executionContext);
    }
}
