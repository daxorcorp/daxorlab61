﻿using Daxor.Lab.SCContracts;

namespace Daxor.Lab.ApplicationServices.Interfaces
{
    /// <summary>
    /// Represents the behavior of a service proxy for a sample changer.
    /// </summary>
    /// <remarks>Although this interface only combines two interfaces,
    /// it makes other code more readable to have a single interface. It also
    /// allows for the addition of sample changer proxy-specific items
    /// in the future.</remarks>
    public interface ISampleChangerProxy : IServiceProxy, ISampleChangerService
    {
    }
}
