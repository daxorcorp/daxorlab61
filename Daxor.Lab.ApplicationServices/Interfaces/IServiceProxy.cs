﻿namespace Daxor.Lab.ApplicationServices.Interfaces
{
    /// <summary>
    /// Represents the method that will handler service proxy events.
    /// </summary>
    /// <param name="sender">Originator of the event</param>
    /// <param name="message">Message associated with the event</param>
    public delegate void ServiceProxyEventHandler(object sender, string message);

    /// <summary>
    /// Exposes events, properties, and methods that define the behavior of a
    /// service proxy.
    /// </summary>
    public interface IServiceProxy
    {
        #region Events

        /// <summary>
        /// Occurs when the proxy fails.
        /// </summary>
        event ServiceProxyEventHandler ServiceProxyFailed;

        /// <summary>
        /// Occurs when the proxy is opened.
        /// </summary>
        event ServiceProxyEventHandler ServiceProxyOpened;

        #endregion

        #region Properties

        /// <summary>
        /// Gets whether the proxy is open (i.e., connected).
        /// </summary>
        bool IsOpen { get; }

        /// <summary>
        /// Gets the name of the proxy (e.g., local, WCF, demo).
        /// </summary>
        string Name { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Opens the service proxy and subscribes to it.
        /// </summary>
        /// <remarks>This method must be called before any service calls
        /// can be made on the proxy.</remarks>
        void OpenAndSubscribe();

        #endregion
    }
}
