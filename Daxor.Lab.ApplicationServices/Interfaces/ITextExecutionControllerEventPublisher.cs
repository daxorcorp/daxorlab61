﻿namespace Daxor.Lab.ApplicationServices.Interfaces
{
    /// <summary>
    /// Represents behavior for subscribing to CLR events associated with the
    /// a test execution controller and publishing the corresponding aggregate events.
    /// </summary>
    /// <remarks>There is no way to unsubscribe because modules can only be initialized,
    /// not unloaded.</remarks>
    public interface ITextExecutionControllerEventPublisher
    {
        /// <summary>
        /// SubscribeToHardwareEvents to CLR events by hooking up methods that publish the corresponding
        /// aggregate events.
        /// </summary>
        void SubscribeToTestExecutionControllerEvents();
    }
}
