﻿using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.ApplicationServices
{
    /// <summary>
    /// Represents an event in a test execution timeline
    /// </summary>
    public class ExecutionTimelineEvent
    {
        /// <summary>
        /// Gets or sets the beginning time for the event
        /// </summary>
        public int BeginTimeInMilliseconds { get; set; }

        /// <summary>
        /// Gets or sets the ending time for the event
        /// </summary>
        public int EndTimeInMilliseconds { get; set; }

        /// <summary>
        /// Gets or sets the sample position (if any) associated with the event
        /// </summary>
        public int? SamplePosition { get; set; }

        /// <summary>
        /// Gets or sets the type of event (execution step)
        /// </summary>
        public TestExecutionStep ExecutionStep { get; set; }

        /// <summary>
        /// Returns a string version of the event
        /// </summary>
        /// <returns>String formatted to show begin-to-end time, sample (if applicable),
        /// and execution step</returns>
        public override string ToString()
        {
            return string.Format("{0,20} to {1,20}: {2} with step {3}",
                                 BeginTimeInMilliseconds, EndTimeInMilliseconds,
                                 SamplePosition == null ? "No sample" : "Sample " + SamplePosition, ExecutionStep);
        }
    }
}
