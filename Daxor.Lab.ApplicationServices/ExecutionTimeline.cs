﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.ApplicationServices
{
    public class ExecutionTimeline
    {
        #region Ctor

        /// <summary>
        /// Creates a new ExecutionTimeline instance with an empty event list
        /// </summary>
        public ExecutionTimeline()
        {
            Events = new List<ExecutionTimelineEvent>();
        }

        #endregion

        #region Properites

        /// <summary>
        /// Gets the total execution time (in milliseconds) based on the current event timeline
        /// </summary>
        public int TotalExecutionTimeInMilliseconds
        {
            get
            {
                var eventCount = Events.Count;
                return eventCount == 0 ? 0 : Events[eventCount - 1].EndTimeInMilliseconds;
            }
        }

        /// <summary>
        /// List of events in the timeline
        /// </summary>
        private List<ExecutionTimelineEvent> Events { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Appends the given event to the existing timeline
        /// </summary>
        /// <param name="eventToAppend">Event to append</param>
        public void Append(ExecutionTimelineEvent eventToAppend)
        {
            Events.Add(eventToAppend);
        }

        /// <summary>
        /// Gets the first event in the timeline whose execution step matches the given value
        /// </summary>
        /// <param name="step">Test execution step to match</param>
        /// <returns>ExecutionTimelineEvent instance corresponding to the given step;
        /// null if no event matches the step</returns>
        public ExecutionTimelineEvent GetEventForTestExecutionStep(TestExecutionStep step)
        {
            return (from e in Events where e.ExecutionStep == step select e).FirstOrDefault();
        }

        /// <summary>
        /// Gets the status of the timeline based on the specified amount of time elapsed and
        /// what event is currently taking place
        /// </summary>
        /// <param name="elapsedTimeInMilliseconds">Elapsed time (in milliseconds)</param>
        /// <param name="currentEvent">Event currently taking place</param>
        /// <returns>OnSchedule if the given event is within the range of the expected event;
        /// Ahead if the given event is beyond the expected event; Behind if the given
        /// event is prior to the expected event</returns>
        public ExecutionTimelineStatus GetStatus(int elapsedTimeInMilliseconds, ExecutionTimelineEvent currentEvent)
        {
            // Find the current event in the timeline
            var targetEvent = (from e in Events
                               where
                                   e.SamplePosition == currentEvent.SamplePosition &&
                                   e.ExecutionStep == currentEvent.ExecutionStep
                               select e).FirstOrDefault();
            if (targetEvent == null)
                return ExecutionTimelineStatus.Unknown;

            // Begin <= Elapsed <= End
            if (targetEvent.BeginTimeInMilliseconds <= elapsedTimeInMilliseconds && targetEvent.EndTimeInMilliseconds >= elapsedTimeInMilliseconds)
                return ExecutionTimelineStatus.OnSchedule;

            // We've started this event before we expected
            if (elapsedTimeInMilliseconds < targetEvent.BeginTimeInMilliseconds)
                return ExecutionTimelineStatus.AheadOfSchedule;

            return ExecutionTimelineStatus.BehindSchedule;
        }

        /// <summary>
        /// Delays the current event and all following events on the timeline based on
        /// the amount of time elapsed and when the previous event was expected to end
        /// </summary>
        /// <param name="elapsedTimeInMilliseconds">Elapsed time (in milliseconds)</param>
        public void Delay(int elapsedTimeInMilliseconds)
        {
            var actualCurrentEventIndex = GetEventIndexAt(elapsedTimeInMilliseconds);
            
            // No prior event to modify.
            if (actualCurrentEventIndex <= 0) return;

            // Find out how behind we are.
            var numOfMillisecondsBehind = elapsedTimeInMilliseconds - Events[actualCurrentEventIndex - 1].EndTimeInMilliseconds;

            // Adjust the previous event so that we still haven't finished it yet.
            Events[actualCurrentEventIndex - 1].EndTimeInMilliseconds += numOfMillisecondsBehind;
            
            // Adjust all following events so that they are delayed.
            var numEvents = Events.Count;
            for (var i=actualCurrentEventIndex; i < numEvents; i++)
            {
                Events[i].BeginTimeInMilliseconds += numOfMillisecondsBehind;
                Events[i].EndTimeInMilliseconds += numOfMillisecondsBehind;
            }
        }

        /// <summary>
        /// Delays the current event and all following events on the timeline based on
        /// the specified amount
        /// </summary>
        /// <param name="elapsedTimeInMilliseconds">Elapsed time (in milliseconds)</param>
        /// <param name="numMillisecondsToDelay">Number of milliseconds to delay</param>
        public void Delay(int elapsedTimeInMilliseconds, int numMillisecondsToDelay)
        {
            var actualCurrentEventIndex = GetEventIndexAt(elapsedTimeInMilliseconds);

            // No prior event to modify.
            if (actualCurrentEventIndex < 0) return;

            // Find out how behind we are.
            var numOfMillisecondsBehind = numMillisecondsToDelay;

            // Adjust the previous event (if available) so that we still haven't finished it yet.
            if (actualCurrentEventIndex > 0)
                Events[actualCurrentEventIndex - 1].EndTimeInMilliseconds += numOfMillisecondsBehind;

            // Note: If there is no previous event, we're delaying the first event in the timeline
            // which is already in progress. Adjusting the begin time for the first event is acceptable
            // because that value is never needed for delays/advances/total time computation.

            // Adjust all following events so that they are delayed.
            var numEvents = Events.Count;
            for (var i = actualCurrentEventIndex; i < numEvents; i++)
            {
                Events[i].BeginTimeInMilliseconds += numOfMillisecondsBehind;
                Events[i].EndTimeInMilliseconds += numOfMillisecondsBehind;
            }
        }

        /// <summary>
        /// Advances all following events on the timeline based on the amount of time elapsed 
        /// and when the current event is expected to end
        /// </summary>
        /// <param name="elapsedTimeInMilliseconds">Number of milliseconds to delay</param>
        public void Advance(int elapsedTimeInMilliseconds)
        {
            var actualCurrentEventIndex = GetEventIndexAt(elapsedTimeInMilliseconds);

            // Find out how ahead we are.
            if (actualCurrentEventIndex == -1) return;

            var numOfMillisecondsAhead = Events[actualCurrentEventIndex].EndTimeInMilliseconds - elapsedTimeInMilliseconds;

            // Adjust the current event so that we've finished it.
            Events[actualCurrentEventIndex].EndTimeInMilliseconds -= numOfMillisecondsAhead;

            // Adjust all following events so that they are delayed.
            var numEvents = Events.Count;
            for (var i = actualCurrentEventIndex + 1; i < numEvents; i++)
            {
                Events[i].BeginTimeInMilliseconds -= numOfMillisecondsAhead;
                Events[i].EndTimeInMilliseconds -= numOfMillisecondsAhead;
            }
        }

        #endregion

        #region Helper methods

        /// <summary>
        /// Gets the index of the event whose time range includes the given elapsed time
        /// </summary>
        /// <param name="elapsedTimeInMilliseconds">Elapsed time (in milliseconds)</param>
        /// <returns>Index of the event; -1 if no matching event could be found</returns>
        private int GetEventIndexAt(int elapsedTimeInMilliseconds)
        {
            var numEvents = Events.Count;
            for (var i = 0; i < numEvents; i++)
            {
                if (Events[i].BeginTimeInMilliseconds <= elapsedTimeInMilliseconds && Events[i].EndTimeInMilliseconds >= elapsedTimeInMilliseconds)
                    return i;
            }
            return -1;
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Returns newline-separated string versions of the events in the timeline
        /// </summary>
        /// <returns>Events in the timeline in string form</returns>
        public override string ToString()
        {
            var toReturn = new StringBuilder();
            foreach (var e in Events)
                toReturn.Append(e + "\n");
            return toReturn.ToString();
        }

        #endregion
    }
}
