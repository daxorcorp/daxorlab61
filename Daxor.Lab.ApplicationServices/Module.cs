﻿using System;
using Daxor.Lab.ApplicationServices.Hardware;
using Daxor.Lab.ApplicationServices.Interfaces;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Domain.Services;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.ApplicationServices
{
    public class Module : IModule
    {
        #region Fields

	    private const string ModuleName = "Application Services";

        private readonly IUnityContainer _container;
        private readonly ILoggerFacade _logger;
        private readonly ISettingsManager _settingsManager;
        #endregion

        #region Ctor
        public Module(IUnityContainer container, ILoggerFacade logger, ISettingsManager settingsManager)
        {
            _container = container;
            _logger = logger;
            _settingsManager = settingsManager;
        }
        #endregion

        public void Initialize()
        {
            RegisterDefaultCalibrationCurve();
            RegisterAuthenticationManager();
            RegisterSpectroscopyService();
            RegisterAndConnectToDetector();
            RegisterAndConnectToSampleChanger();
            RegisterBackgroundAcquisitionService();
            RegisterExecutionTimeHelper();
            RegisterTestExecutionController();
            RegisterAndInitializeAggregateEventPublisher();
            StartBackgroundAcquisitionService();
        }

        private void RegisterDefaultCalibrationCurve()
        {
	        try
	        {
		        _container.RegisterInstance<ICalibrationCurve>(new CalibrationCurve(0, 1, 0));
	        }
	        catch
	        {
		        throw new ModuleLoadException(ModuleName, "initialize the Calibration Curve");
	        }
        }

        private void RegisterSpectroscopyService()
        {
            // Register the spectroscopy service
            try
            {
                _container.RegisterType<ISpectroscopyService, SpectroscopyService>(new ContainerControlledLifetimeManager());
            }
            catch (Exception ex)
            {
                var err = String.Format("ApplicationServices module failed to register a spectroscopy service: '{0}'", ex.Message);
				_logger.Log(err, Category.Exception, Priority.High);

				throw new ModuleLoadException(ModuleName, "initialize the Spectroscopy Service");
            }
        }

        private void RegisterExecutionTimeHelper()
        {
            // Register the spectroscopy service
            try
            {
                _container.RegisterType<IExecutionTimeHelper, ExecutionTimeHelper>(new ContainerControlledLifetimeManager());
            }
            catch (Exception ex)
            {
                var err = String.Format("ApplicationServices module failed to register an execution time helper: '{0}'", ex.Message);
				_logger.Log(err, Category.Exception, Priority.High);

				throw new ModuleLoadException(ModuleName, "initialize the Execution Time Helper");
            }
        }

        private void RegisterBackgroundAcquisitionService()
        {
            try
            {
                _container.RegisterType<IBackgroundAcquisitionService, BackgroundAcquisitionService>(new ContainerControlledLifetimeManager());
            }
            catch (Exception ex)
            {
                var err = String.Format("ApplicationServices module failed to register a background acquisition service: '{0}'", ex.Message);
				_logger.Log(err, Category.Exception, Priority.High);

				throw new ModuleLoadException(ModuleName, "initialize the Background Acquisition Service");
            }
        }

        private void StartBackgroundAcquisitionService()
        {
            // Start up the background acquisition service.
            try
            {
                var acqService = _container.Resolve<IBackgroundAcquisitionService>();
                acqService.StartAsync();
            }
            catch (Exception ex)
            {
                var err = String.Format("ApplicationServices module failed to start a background acquisition service: '{0}'", ex.Message);
				_logger.Log(err, Category.Exception, Priority.High);

				throw new ModuleLoadException(ModuleName, "start the Background Acquisition Service");
            }
        }

        private void RegisterAndInitializeAggregateEventPublisher()
        {
            // Now that there's an appropriate IDetector and ISampleChanger instance in Unity, hook up
            // the aggregate event publisher.
            try
            {
                _container.RegisterType<IHardwareAggregateEventPublisher, ClrToAggregateEventPublisher>(new ContainerControlledLifetimeManager());
                _container.RegisterType<ITextExecutionControllerEventPublisher, ClrToAggregateEventPublisher>();
                
                var hardwareEventPublisher = _container.Resolve<IHardwareAggregateEventPublisher>();
                hardwareEventPublisher.SubscribeToHardwareEvents();

                var controllerEventPublisher = _container.Resolve<ITextExecutionControllerEventPublisher>();
                controllerEventPublisher.SubscribeToTestExecutionControllerEvents();
            }
            catch (Exception ex)
            {
                var err = String.Format("ApplicationServices module failed to register and initialize an aggregate event publisher: '{0}'", ex.Message);
				_logger.Log(err, Category.Exception, Priority.High);

	            throw new ModuleLoadException(ModuleName, "initialize the Aggregate Event Publisher");
            }
        }

        private void RegisterAndConnectToSampleChanger()
        {
            try
            {
                var isSampleChangerInDemoMode = _settingsManager.GetSetting<bool>(SettingKeys.SystemSampleChangerDemoModeEnabled);

                // Register the appropriate sample changer.
                if (!isSampleChangerInDemoMode)
                    _container.RegisterType<ISampleChanger, SampleChanger>(new ContainerControlledLifetimeManager(), new InjectionConstructor(_logger));
                else
                    _container.RegisterInstance<ISampleChanger>(new DemoSampleChanger(), new ContainerControlledLifetimeManager());

                // Now that the sample changer is registered, connect to it.
                var sampleChanger = _container.Resolve<ISampleChanger>();
                sampleChanger.Connect();
                if (!sampleChanger.IsPoweredUp())
                    throw new SampleChangerNotPoweredUpException("The sample changer is not receiving power. Please make sure that the sample changer has power.");
            }
            catch (Exception ex)
            {
                var err = String.Format("ApplicationServices module failed to register and connect to a sample changer: '{0}'", ex.Message);
                _logger.Log(err, Category.Exception, Priority.High);
                throw;
            }
        }

        private void RegisterAndConnectToDetector()
        {
            try
            {
                var isDetectorInDemoMode = _settingsManager.GetSetting<bool>(SettingKeys.SystemDetectorDemoModeEnabled);

                // Register the appropriate detector.
                if (!isDetectorInDemoMode)
                    _container.RegisterType<IDetector, Detector>(new ContainerControlledLifetimeManager(), new InjectionConstructor(_logger));
                else
                    _container.RegisterInstance<IDetector>(new DemoDetector(), new ContainerControlledLifetimeManager());

                // Now that the detector is registered, connect to it.
                var detector = _container.Resolve<IDetector>();
                detector.Connect();
            }
            catch (Exception ex)
            {
                var err = String.Format("ApplicationServices module failed to register and connect to a detector: '{0}'", ex.Message);
                _logger.Log(err, Category.Exception, Priority.High);
                throw new GammaCounterNotConnectedException(ex.Message, ex);
            }
        }

        private void RegisterTestExecutionController()
        {
            // Now that there's an appropriate IDetector and ISampleChanger instance in Unity, hook up
            // the aggregate event publisher.
            try
            {
                _container.RegisterType<ITestExecutionController, TestExecutionController>(new ContainerControlledLifetimeManager());
            }
            catch (Exception ex)
            {
                var err = String.Format("ApplicationServices module failed to register a test execution controller: '{0}'", ex.Message);
				_logger.Log(err, Category.Exception, Priority.High);

				throw new ModuleLoadException(ModuleName, "initialize the Test Execution Controller");
            }
        }

        private void RegisterAuthenticationManager()
        {
            try
            {
                _container.RegisterType<IAuthenticationManager, AuthenticationManager>(new ContainerControlledLifetimeManager());
            }
            catch (Exception ex)
            {
                var err = String.Format("ApplicationServices module failed to register an authentication manager: '{0}'", ex.Message);
                _logger.Log(err, Category.Exception, Priority.High);

				throw new ModuleLoadException(ModuleName, "initialize the Authentication Manager");
            }
        }
    }
}
