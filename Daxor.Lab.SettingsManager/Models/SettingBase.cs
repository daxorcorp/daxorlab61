﻿using System;
using System.Collections.Generic;
using System.Security;
using Daxor.Lab.Domain.Entities.Base;
using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.SettingsManager.Models
{
	public abstract class SettingBase : ObservableObject
	{
		bool _isDirty;
		readonly string _key;
		object _value;
		string _group;
		string _appModuleKey;
		readonly string _valueTypeAsString;
		string _header;
		int _orderWithinGroup = -1;
		bool _isValidType = true;
		// ReSharper disable once InconsistentNaming
		protected AuthorizationLevel _modifyPermission;
		protected object CachedOriginalValue;

		protected SettingBase(string key, object value, string typeAsString, AuthorizationLevel modifyPermission)
		{
			_key = key;
			_valueTypeAsString = typeAsString;
			// ReSharper disable once DoNotCallOverridableMethodsInConstructor
			_value = InitializeValue(value);
			CachedOriginalValue = _value;
			_modifyPermission = modifyPermission;

			// ReSharper disable once DoNotCallOverridableMethodsInConstructor
			ValueType = ConvertStringToType(_valueTypeAsString);
		}

		public virtual string Key
		{
			get { return _key; }
		}

		public virtual object Value
		{
			get { return _value; }
			set
			{
				if (SetValue(ref _value, "Value", value))
					UpdateSettingDirtiness();
			}
		}

		public bool IsDirty
		{
			get { return _isDirty; }
			protected set { SetValue(ref _isDirty, "IsDirty", value); }
		}

		public int HeaderId
		{
			get;
			set;
		}

		public string Header
		{
			get { return _header; }
			set { SetValue(ref _header, "Header", value); }
		}

		public int GroupId
		{
			get;
			set;
		}

		public string Group
		{
			get { return _group; }
			set { SetValue(ref _group, "Group", value); }
		}

		public int ModuleId
		{
			get;
			set;
		}

		public int OrderWithinGroup
		{
			get { return _orderWithinGroup; }
			set { SetValue(ref _orderWithinGroup, "OrderWithinGroup", value); }
		}

		public string AppModuleKey
		{
			get { return _appModuleKey; }
			set { SetValue(ref _appModuleKey, "AppModuleKey", value); }
		}

		public string ValueTypeAsString
		{
			get { return _valueTypeAsString; }
		}

		public AuthorizationLevel ModifyPermission
		{
			get { return _modifyPermission; }
		}

		public Type ValueType
		{
			get;
			protected set;
		}

		public bool IsValidType
		{
			get { return _isValidType; }
			protected set { SetValue(ref _isValidType, "IsValidType", value); }
		}

		protected abstract void UpdateSettingDirtiness();

	    protected virtual object InitializeValue(object value)
	    {
	        return value;
	    }

		public abstract object ValueToSave
		{
			get;
		}

		protected virtual Type ConvertStringToType(string valueTypeAsString)
		{
			var type = typeof(string);

			if (valueTypeAsString == "string" || valueTypeAsString == "folderbrowser")
			{
				type = typeof(string);
				IsValidType = true;
			}
			else if (valueTypeAsString == "securestring")
			{
				type = typeof(SecureString);
				IsValidType = true;
			}
			else if (valueTypeAsString == "integer")
			{
				type = typeof(int);
				int convertedValue;
				IsValidType = Value != null && int.TryParse(Value.ToString(), out convertedValue);
			}
			else if (valueTypeAsString == "double")
			{
				type = typeof(double);
				double convertedValue;
                IsValidType = Value != null && double.TryParse(Value.ToString(), out convertedValue);
			}
			else if (valueTypeAsString == "datetime" || valueTypeAsString == "date" || valueTypeAsString == "time")
			{
				type = typeof(DateTime);
				DateTime convertedValue;
                IsValidType = Value != null && DateTime.TryParse(Value.ToString(), out convertedValue);
			}
			else if (valueTypeAsString == "boolean")
			{
				type = typeof(bool);
				bool convertedValue;

				var convValue = "false";
				if (Value != null && (Value.ToString().ToLowerInvariant() == "t" || Value.ToString().ToLowerInvariant() == "true"))
					convValue = "true";

				IsValidType = bool.TryParse(convValue, out convertedValue);
			}
			else if (valueTypeAsString == "collection")
			{
				type = typeof(IList<ObservableValue>);
				IsValidType = true;
			}
			else
				IsValidType = false;

			return type;
		}
	}
}
