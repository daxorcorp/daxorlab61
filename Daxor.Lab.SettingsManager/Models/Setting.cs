﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.SettingsManager.Models
{
	/// <summary>
	/// Model representing a setting
	/// </summary>
	/// <typeparam name="T">Type of setting value</typeparam>
	internal class Setting<T> : ISetting
	{
		readonly SettingSource _source;
		readonly string _key;
		object _value;

		public Setting(string key, SettingSource source, string typeAsString)
		{
			if (string.IsNullOrEmpty(key))
				throw new ArgumentNullException("key");

			_key = key;
			_source = source;
			Type = ConvertStringToType(typeAsString);
		}

		public string Key
		{
			get { return _key; }
		}
		public SettingSource Source
		{
			get { return _source; }
		}
		public T Value
		{
			get { return (T)_value; }
			set
			{
				((ISetting)this).Value = value;
				FirePropertyChanged("Value");
			}
		}
		object ISetting.Value
		{
			get { return _value; }
			set { _value = value; }
		}

		public Type Type { get; protected set; }

		private static Type ConvertStringToType(string typeAsString)
		{
			switch (typeAsString)
			{
				case "string":
				case "folderbrowser":
					return typeof(string);
				case "securestring":
					return typeof(SecureString);
				case "integer":
					return typeof(int);
				case "double":
					return typeof(double);
				case "datetime":
				case "date":
				case "time":
					return typeof(DateTime);
				case "boolean":
					return typeof(bool);
				case "collection":
					return typeof (IList<ObservableValue>);
				default:
					return typeof (object);
			}
		}

		#region INotifyPropertyChanged Members

		public event PropertyChangedEventHandler PropertyChanged;
		protected void FirePropertyChanged(string propertyName)
		{
			var handler = PropertyChanged;
			if (handler != null)
				handler(this, new PropertyChangedEventArgs(propertyName));
		}
		#endregion
	}
}
