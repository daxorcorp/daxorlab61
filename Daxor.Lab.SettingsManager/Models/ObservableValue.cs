﻿using System.ComponentModel;
using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.SettingsManager.Models
{
    public class ObservableValue : ObservableObject, IEditableObject
    {
        string _cachedValue;
        string _cValue;
        
        public string Value
        {
            get { return _cValue; }
            set { SetValue(ref _cValue, "Value", value); }
        }

        void IEditableObject.BeginEdit()
        {
            _cachedValue = Value;
        }

        void IEditableObject.CancelEdit()
        {
            if (_cachedValue != null)
            {
                Value = _cachedValue;
                _cachedValue = null;
            }
        }

        void IEditableObject.EndEdit()
        {
           _cachedValue = null;
        }
    }
}
