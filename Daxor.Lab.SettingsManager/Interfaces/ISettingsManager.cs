﻿using System;
using System.Collections.Generic;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Models;

namespace Daxor.Lab.SettingsManager.Interfaces
{
    /// <summary>
    /// General interface for settings manager
    /// </summary>
    public interface ISettingsManager
    {
        T GetSetting<T>(string settingName);
        void SetSetting<T>(string settingName, T settingValue);
        void SetSettings(List<SettingBase> settingsToUpdate);

        event EventHandler<SettingsChangedEventArgs> SettingsChanged;
    }
}
