﻿using System;
using System.ComponentModel;
using Daxor.Lab.SettingsManager.Common;

namespace Daxor.Lab.SettingsManager.Interfaces
{
	public interface ISetting : INotifyPropertyChanged
	{
		string Key { get; }
		object Value { get; set; }
		SettingSource Source { get; }
		Type Type { get; }
	}
}
