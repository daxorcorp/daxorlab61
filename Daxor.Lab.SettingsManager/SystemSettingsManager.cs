﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.SettingsManager.Models;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.SettingsManager
{
	/// <summary>
	/// Settings Manager used throughout the DaxorLab
	/// </summary>
	public class SystemSettingsManager : ISettingsManager
	{
		#region Fields

		/// <summary>
		/// TODO: Removed hardcoded database password to DomainConstants, see if there is a better solution in the future. PAndreason
		/// </summary>
		private readonly string _databasePassword = DomainConstants.DaxorDatabasePassword;
		private readonly Dictionary<string, ISetting> _settingsCollection = new Dictionary<string, ISetting>();
		private readonly ILoggerFacade _logger;
		private readonly ISettingsManagerDataService _settingsManagerDataService;
		private readonly string _xmlFileName;
		
		#endregion

		#region Ctor

		public SystemSettingsManager(ILoggerFacade logger, ISettingsManagerDataService settingsManagerDataService, string fileName = @".\Settings.xml")
		{
			if (logger == null) throw new ArgumentNullException("logger");
			if (settingsManagerDataService == null) throw new ArgumentNullException("settingsManagerDataService");

			_logger = logger;
			_settingsManagerDataService = settingsManagerDataService;
			_xmlFileName = fileName;

			try
			{
				// ReSharper disable DoNotCallOverridableMethodsInConstructor
				ConfigureSystemSettingsManager();
				ConfigureDatabaseSettingsHelper();
				// ReSharper restore DoNotCallOverridableMethodsInConstructor
			}
			catch (Exception ex)
			{
				logger.Log(ex.Message, Category.Exception, Priority.High);
				throw;
			}

		}

		#endregion

		#region Properties

		public Dictionary<string, ISetting> SettingsCollection
		{
			get { return _settingsCollection; }
		} 

		#endregion

		#region Methods

		public T GetSetting<T>(string settingName)
		{
			try
			{
				var setting = InternalGetSetting(settingName);
			  
					// For ints, doubles, and DateTimes you have to "box" them into objects before you can cast them
				if (typeof(T) == typeof(int))
				{
					object temp = int.Parse(setting.Value.ToString());
					return (T)temp;
				}

				if (typeof(T) == typeof(double))
				{
					object temp = double.Parse(setting.Value.ToString());
					return (T)temp;
				}

				if (typeof(T) == typeof(DateTime))
				{
					DateTime temp;
					DateTime.TryParse(setting.Value.ToString(), out temp);
					object obj = temp;
					return (T)obj;
				}

				return (T)setting.Value;
			}
			catch
			{
				return default(T);
			}
		}

		public void SetSetting<T>(string settingName, T settingValue)
		{
			if (String.IsNullOrWhiteSpace(settingName))
				return;
			try
			{
				var settingToUpdate = InternalGetSetting(settingName);

				if (settingToUpdate.Value.Equals(settingValue))
					return;

				if (typeof(T) == typeof(int))
				{
					if (int.Parse(settingToUpdate.Value.ToString()) == int.Parse(settingValue.ToString()))
						return;

					if (typeof(T) == typeof(double))
					{
						// ReSharper disable once CompareOfFloatsByEqualityOperator
						if (double.Parse(settingToUpdate.Value.ToString()) == double.Parse(settingValue.ToString()))
							return;
					}
				}
				else if (typeof(T) == typeof(DateTime))
				{
					DateTime tempDateOne;
					DateTime.TryParse(settingToUpdate.Value.ToString(), out tempDateOne);

					DateTime tempDateTwo;
					DateTime.TryParse(settingValue.ToString(), out tempDateTwo);

					if (tempDateOne == tempDateTwo)
						return;
				}

				else if (typeof(T) == typeof(bool))
				{
					if (bool.Parse(settingToUpdate.Value.ToString()) == bool.Parse(settingValue.ToString()))
						return;
					if (settingToUpdate.Value.ToString() == settingValue.ToString())
						return;
				}

				settingToUpdate.Value = settingValue;
				if (settingToUpdate.Source == SettingSource.Database)
					InternalSetSetting(settingToUpdate);

				FireSettingsChanged(new List<string>(new[] { settingName }));
			}
			catch (Exception setSettingException)
			{
				_logger.Log(setSettingException.Message, Category.Exception, Priority.High);
			}
		}
		
		public void SetSettings(List<SettingBase> settingsToUpdate)
		{
			if (settingsToUpdate == null || settingsToUpdate.Count <= 0)
				return;

			// Do we want atomic (i.e., set all or set none) behavior here?
			try
			{
				var keys = new List<string>();

				// Maybe use a stored proc that only makes one trip to the DB?
				foreach (var setting in settingsToUpdate)
				{
					var isSettingValid = true;

					var settingToUpdate = InternalGetSetting(setting.Key);
					
					switch (settingToUpdate.Type.Name.ToLowerInvariant())
					{
						case "integer":
						case "int32":
                            int throwAwayInt;
					        isSettingValid = setting.Value != null && int.TryParse(setting.Value.ToString(), out throwAwayInt);
                            break;
						case "double":
					        double throwAwayDouble;
                            isSettingValid = setting.Value != null && double.TryParse(setting.Value.ToString(), out throwAwayDouble);
							break;
						case "boolean":
                            var convValue = "false";

                            if (setting.Value != null && (setting.Value.ToString().ToLowerInvariant() == "t" || setting.Value.ToString().ToLowerInvariant() == "true"))
                                convValue = "true";

					        bool throwAwayBool = false;
                            isSettingValid = setting.Value != null && bool.TryParse(convValue, out throwAwayBool);
					        if (isSettingValid) setting.Value = throwAwayBool;
							break;
					}

					// ReSharper disable once InvertIf
					if (isSettingValid)
					{
						if (setting.Value.GetType() != typeof (ObservableCollection<ObservableValue>))
							settingToUpdate.Value = setting.Value;
						else
						{
							var newSetting = new ObservableCollection<string>();

							foreach (var value in ((ObservableCollection<ObservableValue>) setting.Value))
							{
								newSetting.Add(value.Value);
							}

							settingToUpdate.Value = newSetting;
						}

						keys.Add(setting.Key);

						if (settingToUpdate.Source == SettingSource.Database)
							InternalSetSetting(settingToUpdate);
					}
				}

				FireSettingsChanged(keys);
			}
			catch (Exception ex)
			{
			   _logger.Log("SetSettings. " + ex.Message, Category.Exception, Priority.High);
			}
		}

		#endregion

		#region Helpers

		protected virtual void ConfigureDatabaseSettingsHelper()
		{
			DatabaseSettingsHelper.SCLayoutID = GetSetting<int>(SettingKeys.SystemSampleChangerLayoutId);
			DatabaseSettingsHelper.DefaultMeasurementSystem = GetSetting<string>(SettingKeys.SystemDefaultMeasurementSystem);
			DatabaseSettingsHelper.ID2Label = GetSetting<string>(SettingKeys.BvaTestId2Label);
			DatabaseSettingsHelper.LocationLabel = GetSetting<string>(SettingKeys.BvaLocationLabel);
		}

		protected virtual void ConfigureSystemSettingsManager()
		{
			ReadXmlSettings(_xmlFileName);
			ReadDatabaseSettings();
			SetRunTimeSettings();
		}
	 
		protected virtual void ReadDatabaseSettings()
		{
			_settingsManagerDataService.BringAllDatabasesOnline();

			InitializeDatabaseConnectionString();

			var settingsFromDatabase = _settingsManagerDataService.GetSettingsFromDatabase(GetSetting<string>(SettingKeys.SystemLinqConnectionString));

			foreach (var settingResult in settingsFromDatabase)
			{
				ISetting setting = new Setting<Object>(settingResult.SETTING_ID, SettingSource.Database, settingResult.DATA_TYPE.ToLower());
				switch (settingResult.DATA_TYPE.ToLower())
				{
					case "boolean":
						setting.Value = (settingResult.VALUE.ToLower() == "t" || settingResult.VALUE.ToLower() == "true");
						break;
					case "collection":
						setting.Value = settingResult.VALUE.Split('|').ToList();
						break;
					default:
						setting.Value = settingResult.VALUE;
						break;
				}

				AddSetting(setting);
			}
		}

		/// <summary>
		/// Reads an XML file at the specified location and adds the settings contained in the file to
		/// the settings collection.
		/// </summary>
		/// <param name="fileName">The string representation of the fill path to the XML file.</param>
		/// <exception cref="FileNotFoundException">
		/// If the file is not found at the location that is specified, a file not found exception is thrown.
		/// </exception>
		/// <exception cref="InvalidOperationException">
		/// If at any of the nodes in the xml file are null, then an InvalidOperationException is thrown.
		/// </exception>
		protected virtual void ReadXmlSettings(string fileName)
		{
			if (!File.Exists(fileName)) throw new FileNotFoundException("The file '" + fileName + "' could not be found.");

			var data = from d in XElement.Load(fileName).Elements("Data_Node")
					   select d;

			foreach (var record in data)
			{
				var settingName = record.Element("Name");
				var settingType = record.Element("Type");
				var settingValue = record.Element("Value");

				if ((settingName == null) || (settingType == null) || (settingValue == null))
					throw new InvalidOperationException("The settings XML file found at '" + fileName + "' is improperly formed.");

				if (String.IsNullOrWhiteSpace(settingName.Value) || String.IsNullOrWhiteSpace(settingType.Value) || String.IsNullOrWhiteSpace(settingValue.Value))
					throw new InvalidOperationException("The settings XML file found at '" + fileName + "' is improperly formed.");

				var settingKey = settingName.Value;

				ISetting setting = new Setting<object>(settingKey, SettingSource.XML, settingType.Value.ToLower());

				switch (settingType.Value.ToLower())
				{
					case "string":
						setting.Value = settingValue.Value;
						break;
					case "integer":
						setting.Value = Int32.Parse(settingValue.Value);
						break;
					case "double":
						setting.Value = Double.Parse(settingValue.Value);
						break;
					case "boolean":
						setting.Value = settingValue.Value.ToUpperInvariant() == "T";
						break;
				}

				AddSetting(setting);
			}
		}

		protected virtual void SetRunTimeSettings()
		{
			var systemStartupDateTimeSetting = new Setting<DateTime>(RuntimeSettingKeys.SystemStartupDateTime, SettingSource.RunTime, "datetime")
			{
				Value = DateTime.Now
			};

			var exportPasswordSetting = new Setting<string>(RuntimeSettingKeys.SystemPasswordSecureExport, SettingSource.RunTime, "string")
			{
				Value = ""
			};

			var daxorLabBackupFileNameSetting = new Setting<string>(RuntimeSettingKeys.SystemDaxorLabDatabaseBackupFilename, SettingSource.RunTime, "string")
			{
				Value = "DaxorSuite"
			};

			var qcArchiveBackupFileNameSetting = new Setting<string>(RuntimeSettingKeys.SystemQcArchiveDatabaseBackupFilename, SettingSource.RunTime, "string")
			{
				Value = "QCDatabaseArchive"
			};

			var manualBackupFileNameSetting = new Setting<string>(RuntimeSettingKeys.SystemManualBackupFileName, SettingSource.RunTime, "string")
			{
				Value = "DaxorLabSuite.zip"
			};

			AddSetting(systemStartupDateTimeSetting);
			AddSetting(exportPasswordSetting);
			AddSetting(daxorLabBackupFileNameSetting);
			AddSetting(qcArchiveBackupFileNameSetting);
			AddSetting(manualBackupFileNameSetting);

			SetVersionInformation();
		}

		void InitializeDatabaseConnectionString()
		{
			var databaseConnectionStringSetting = new Setting<string>(SettingKeys.SystemLinqConnectionString, SettingSource.RunTime, "string")
			{
				Value = @"Data Source=" + GetSetting<string>(SettingKeys.SystemDatabaseServer) + ";Initial Catalog=" +
						GetSetting<string>(SettingKeys.SystemDatabaseName) + ";Persist Security Info=True;User ID=" +
						GetSetting<string>(SettingKeys.SystemDatabaseUsername) + ";Password=" + _databasePassword
			};

			DatabaseSettingsHelper.LINQConnectionString = databaseConnectionStringSetting.Value;

			AddSetting(databaseConnectionStringSetting);
			AddSetting(new Setting<string>(SettingKeys.SystemDatabasePassword, SettingSource.RunTime, "string") { Value =_databasePassword });

		}

		protected void AddSetting(ISetting setting)
		{
			if (!_settingsCollection.ContainsKey(setting.Key))
				_settingsCollection.Add(setting.Key, setting);
		}

		private void SetVersionInformation()
		{
			var asm = Assembly.GetExecutingAssembly();
			var fvi = FileVersionInfo.GetVersionInfo(asm.Location);
			SetSetting(SettingKeys.BvaModuleVersion, fvi.FileVersion);
		}

		internal ISetting InternalGetSetting(string key)
		{
			ISetting setting = null;
			try
			{
				if (_settingsCollection.ContainsKey(key))
					setting = _settingsCollection[key];
			}
			catch (Exception ex)
			{
				_logger.Log(ex.Message, Category.Exception, Priority.High);
			}

			return setting;
		}

		/// <summary>
		/// Updates the database whenever a setting has changed
		/// </summary>
		/// <param name="setting">Setting that has changed</param>
		internal void InternalSetSetting(ISetting setting)
		{        
			var context = new DaxorLabDatabaseLinqDataContext(GetSetting<string>(SettingKeys.SystemLinqConnectionString));

			// ReSharper disable once CanBeReplacedWithTryCastAndCheckForNull
			if (setting.Value is ICollection)
			{
				var strBuilder = new StringBuilder();
				((IEnumerable)setting.Value).OfType<object>().ToList().ForEach(o => strBuilder.Append(o.ToString() + "|"));
				context.SetSettingValue(setting.Key, strBuilder.ToString().TrimEnd('|'));
			}
			else
				context.SetSettingValue(setting.Key, setting.Value.ToString());

			if (setting.Key == SettingKeys.BvaTestId2Label)
				DatabaseSettingsHelper.ID2Label = GetSetting<string>(SettingKeys.BvaTestId2Label);

			if (setting.Key == SettingKeys.BvaLocationLabel)
				DatabaseSettingsHelper.LocationLabel = GetSetting<string>(SettingKeys.BvaLocationLabel);
		}
		
		#endregion

		#region Events

		public event EventHandler<SettingsChangedEventArgs> SettingsChanged;

		protected virtual void FireSettingsChanged(List<String> changedSettingIdentifiers)
		{
			var handler = SettingsChanged;
			if (handler != null)
				handler(this, new SettingsChangedEventArgs(changedSettingIdentifiers));
		}

		#endregion
	}
 }
