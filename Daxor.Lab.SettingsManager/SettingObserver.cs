﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.SettingsManager
{
	/// <summary>
	/// SettingObserver works identical to propertyObserver, allows clients to subscribe and execution action
	/// when a given setting changes it's value
	/// Simple version of property observer; need to make it with weak listener.
	/// </summary>
	/// <typeparam name="TSettingManager"></typeparam>
	public class SettingObserver<TSettingManager> where TSettingManager : ISettingsManager
	{
		#region Fields

		readonly Dictionary<string, Action<TSettingManager>> _settingNameToHandlerMap;
		readonly WeakReference _settingsManager;

		#endregion

		#region Ctor

		public SettingObserver(TSettingManager settingManager)
		{
			if (settingManager == null)
				throw new NullReferenceException("settingManager");

			_settingsManager = new WeakReference(settingManager);
			_settingNameToHandlerMap = new Dictionary<string, Action<TSettingManager>>();
		}

		#endregion

		#region Public Methods

		#region RegisterHandler

		/// <summary>
		/// Registers a callback to be invoked when the SettingsChanged event has been raised for the specified setting.
		/// </summary>
		public SettingObserver<TSettingManager> RegisterHandler(
			String settingKey,
			Action<TSettingManager> handler)
		{
			if (settingKey == null)
				throw new ArgumentNullException("settingKey");

			if (handler == null)
				throw new ArgumentNullException("handler");


			var settingManager = GetSettingManager();

			if (settingManager == null) return this;

			if(!_settingNameToHandlerMap.Any())
				settingManager.SettingsChanged += SettingManager_SettingsChanged;

			_settingNameToHandlerMap[settingKey] = handler;

			return this;
		}
		#endregion

		#region UnregisterHandler

		/// <summary>
		/// Removes the callback associated with the specified setting key.
		/// </summary>
		public SettingObserver<TSettingManager> UnregisterHandler(string settingKey)
		{
			if (settingKey == null)
				throw new ArgumentNullException("settingKey");

			var settingManager = GetSettingManager();

			if (settingManager == null) return this;

			if (!_settingNameToHandlerMap.ContainsKey(settingKey)) return this;

			_settingNameToHandlerMap.Remove(settingKey);
			if (!_settingNameToHandlerMap.Any())
				settingManager.SettingsChanged -= SettingManager_SettingsChanged;

			return this;
		}

		#endregion

		#endregion

		#region GetSettingManager

		TSettingManager GetSettingManager()
		{
			try
			{
				return (TSettingManager)_settingsManager.Target;
			}
			catch
			{
				return default(TSettingManager);
			}
		}

		#endregion

		#region IWeakEventListener

		void SettingManager_SettingsChanged(object sender, SettingsChangedEventArgs e)
		{

			var settingSource = (TSettingManager)sender;

			if (e.ChangedSettingIdentifiers == null || !e.ChangedSettingIdentifiers.Any()) return;

			foreach (var settingName in e.ChangedSettingIdentifiers)
			{
				Action<TSettingManager> handler;
				if (_settingNameToHandlerMap.TryGetValue(settingName, out handler))
					handler(settingSource);
			}
		}
		#endregion
	}
}
