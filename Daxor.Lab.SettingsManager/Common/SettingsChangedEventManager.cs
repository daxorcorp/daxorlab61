﻿using System;
using System.Windows;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.SettingsManager.Common
{
	/// <summary>
	/// Weak event Manager; not used by Observer
	/// </summary>
	public class SettingsChangedEventManager : WeakEventManager
	{
		public static void AddListener(ISettingsManager source, IWeakEventListener listener)
		{
			CurrentManager.ProtectedAddListener(source, listener);
		}
		public static void RemoveListener(ISettingsManager source, IWeakEventListener listener)
		{
			CurrentManager.ProtectedRemoveListener(source, listener);
		}

		protected override void StartListening(object source)
		{
			var manager = source as ISettingsManager;
			if (manager == null)
				throw new NullReferenceException("Unable to start listening for ISetttingsManager.SettingsChanged evebt " +
				"because specified source ('" + source.GetType() + "') is not ISetttingsManager.");

			manager.SettingsChanged += manager_SettingsChanged;
		}
		protected override void StopListening(object source)
		{
			var manager = source as ISettingsManager;
			if (manager == null)
				throw new NullReferenceException("Unable to start listening for ISetttingsManager.SettingsChanged evebt " +
				"because specified source ('" + source.GetType() + "') is not ISetttingsManager.");

			manager.SettingsChanged -= manager_SettingsChanged;
		}


		void manager_SettingsChanged(object sender, SettingsChangedEventArgs e)
		{
			DeliverEvent(sender, e);
		}

		/// <summary>
		/// Gets the instance of TestBoxPreviewGotFocusEventManager registered with WeakEventManager.
		/// If no instance has been set, this property creates and instance and sets it as the manager.
		/// </summary>
		private static SettingsChangedEventManager CurrentManager
		{
			get
			{
				var managerType = typeof(SettingsChangedEventManager);
				var manager = (SettingsChangedEventManager)GetCurrentManager(managerType);

				if (manager != null) return manager;

				manager = new SettingsChangedEventManager();
				SetCurrentManager(managerType, manager);
				return manager;
			}
		}
	}
}
