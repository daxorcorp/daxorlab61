﻿namespace Daxor.Lab.SettingsManager.Common
{
    public static class RuntimeSettingKeys
    {
        public static readonly string SystemStartupDateTime = "SYSTEM_STARTUP_TIME";
        public static readonly string SystemPasswordSecureExport = "SYSTEM_PASSWORD_SECURE_EXPORT";
        public static readonly string SystemDaxorLabDatabaseBackupFilename = "SYSTEM_DAXORLAB_DATABASE_BACKUP_FILENAME";
        public static readonly string SystemQcArchiveDatabaseBackupFilename = "SYSTEM_QC_ARCHIVE_DATABASE_BACKUP_FILENAME";
        public static readonly string SystemManualBackupFileName = "SYSTEM_MANUAL_BACKUP_FILENAME";
    }
}
