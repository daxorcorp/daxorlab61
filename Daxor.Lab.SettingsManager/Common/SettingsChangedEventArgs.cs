﻿using System;
using System.Collections.Generic;

namespace Daxor.Lab.SettingsManager.Common
{
    /// <summary>
    /// EventArgs when SettingsChange
    /// </summary>
    public class SettingsChangedEventArgs : EventArgs
    {
        public SettingsChangedEventArgs(IEnumerable<String> changedSettingIdentifiers)
        {
            ChangedSettingIdentifiers = changedSettingIdentifiers;
        }

        public IEnumerable<string> ChangedSettingIdentifiers { get; private set; }
    }
}
