﻿
namespace Daxor.Lab.SettingsManager.Common
{
	public enum SettingSource
	{
		// ReSharper disable once InconsistentNaming
		XML,
		Database,
		RunTime,
	}
}
