﻿
namespace Daxor.Lab.SettingsManager.Common
{
	public static class SettingKeys
	{
		public static readonly string BvaAccessionFieldRequired = "BVA_ACCESSION_FIELD_REQUIRED";
		public static readonly string BvaAnalystFieldRequired = "BVA_ANALYST_FIELD_REQUIRED";
		public static readonly string BvaAutomaticPointExclusionEnabled = "BVA_AUTOMATIC_POINT_EXCLUSION_ENABLED";
		public static readonly string BvaAutomaticPointExclusionMaxNumberOfExclusions = "BVA_AUTOMATIC_POINT_EXCLUSION_MAX_NUM_EXCLUSIONS";
		public static readonly string BvaAutomaticPointExclusionMinProtocol = "BVA_AUTOMATIC_POINT_EXCLUSION_MIN_PROTOCOL";
		public static readonly string BvaAutomaticPointExclusionRseThreshold = "BVA_AUTOMATIC_POINT_EXCLUSION_RSE_THRESHOLD";
		public static readonly string BvaAutomaticPointExclusionStdDevThreshold = "BVA_AUTOMATIC_POINT_EXCLUSION_STDDEV_THRESHOLD";
		public static readonly string BvaCcFieldRequired = "BVA_CC_FIELD_REQUIRED";
		public static readonly string BvaDateOfBirthFieldRequired = "BVA_DATE_OF_BIRTH_FIELD_REQUIRED";
		public static readonly string BvaDefaultHematocritFillType = "BVA_DEFAULT_HEMATOCRIT_FILL_TYPE";
		public static readonly string BvaDefaultProtocol = "BVA_DEFAULT_PROTOCOL";
		public static readonly string BvaDefaultTubeType = "BVA_DEFAULT_TUBE_TYPE";
		public static readonly string BvaFirstNameFieldRequired = "BVA_FIRST_NAME_FIELD_REQUIRED";
		public static readonly string BvaHematocritRangeFemale = "BVA_HEMATOCRIT_RANGE_FEMALE";
		public static readonly string BvaHematocritRangeMale = "BVA_HEMATOCRIT_RANGE_MALE";
		public static readonly string BvaInjectateDescription = "BVA_INJECTATE_DESCRIPTION";
		public static readonly string BvaInjectateDoseFieldRequired = "BVA_INJECTATE_DOSE_FIELD_REQUIRED";
		public static readonly string BvaLastNameFieldRequired = "BVA_LAST_NAME_FIELD_REQUIRED";
		public static readonly string BvaLocationFieldRequired = "BVA_LOCATION_FIELD_REQUIRED";
		public static readonly string BvaLocationLabel = "BVA_LOCATION_LABEL";
		public static readonly string BvaMiddleNameFieldRequired = "BVA_MIDDLE_NAME_FIELD_REQUIRED";
		public static readonly string BvaMinimumStandardCpm = "BVA_MINIMUM_STANDARD_CPM";
		public static readonly string BvaModuleVersion = "BVA_MODULE_VERSION";
		public static readonly string BvaNormalizedHematocritRangeFemale = "BVA_NORMALIZED_HEMATOCRIT_RANGE_FEMALE";
		public static readonly string BvaNormalizedHematocritRangeMale = "BVA_NORMALIZED_HEMATOCRIT_RANGE_MALE";
		public static readonly string BvaPacsEnabled = "BVA_PACS_ENABLED";
		public static readonly string BvaReferenceVolumeInMl = "BVA_REFERENCE_VOLUME_IN_ML";
		public static readonly string BvaReferringPhysicianFieldRequired = "BVA_REFERRING_PHYSICIAN_FIELD_REQUIRED";
        public static readonly string BvaPhysiciansSpecialtyFieldRequired = "BVA_PHYSICIANS_SPECIALTY_FIELD_REQUIRED";
	    public static readonly string BvaPhysiciansSpecialties = "BVA_PHYSICIANS_SPECIALTIES";
		public static readonly string BvaReferringPhysicians = "BVA_REFERRING_PHYSICIANS";
		public static readonly string BvaReportDefaultShowAuditTrail = "BVA_REPORT_DEFAULT_SHOW_AUDIT_TRAIL";
		public static readonly string BvaReportDefaultShowReportFindings = "BVA_REPORT_DEFAULT_SHOW_REPORT_FINDINGS";
		public static readonly string BvaReportLogoEnabled = "BVA_REPORT_LOGO_ENABLED";
		public static readonly string BvaTestId2Label = "BVA_TEST_ID2_LABEL";
		public static readonly string BvaTestListAbortedTestsDisplayed = "BVA_TEST_LIST_ABORTED_TESTS_DISPLAYED";
		public static readonly string BvaTestListVopsTestsDisplayed = "BVA_TEST_LIST_VOPS_TESTS_DISPLAYED";
		public static readonly string InjectateVerificationBitMapping = "INJECTATE_VERIFICATION_BIT_MAPPING";
		public static readonly string InjectateVerificationChecksum = "INJECTATE_VERIFICATION_CHECKSUM";
		public static readonly string InjectateVerificationKeyLength = "INJECTATE_VERIFICATION_KEY_LENGTH";
		public static readonly string InjectateVerificationKeyRepresentation = "INJECTATE_VERIFICATION_KEY_REPRESENTATION";
		public static readonly string InjectateVerificationStartingBatchId = "INJECTATE_VERIFICATION_STARTING_BATCH_ID";
		public static readonly string InjectateVerificationStartingDateOfManufacturing = "INJECTATE_VERIFICATION_STARTING_DOM";
		public static readonly string QcEstimatedExecutionTimeForQc2 = "QC_ESTIMATED_EXECUTION_TIME_QC2";
		public static readonly string QcEstimatedExecutionTimeForQc3 = "QC_ESTIMATED_EXECUTION_TIME_QC3";
		public static readonly string QcEstimatedExecutionTimeForQc4 = "QC_ESTIMATED_EXECUTION_TIME_QC4";
		public static readonly string QcBa133Centroid = "QC_BA133_CENTROID";
		public static readonly string QcCalibrationTestMaxFineGain = "QC_CALIBRATION_TEST_MAX_FINE_GAIN";
		public static readonly string QcCalibrationTestMaxPassingFwhm = "QC_CALIBRATION_TEST_MAX_PASSING_FWHM";
		public static readonly string QcCalibrationTestMinCounts = "QC_CALIBRATION_TEST_MIN_COUNTS";
		public static readonly string QcCalibrationTestMinFineGain = "QC_CALIBRATION_TEST_MIN_FINE_GAIN";
		public static readonly string QcCalibrationTestMinPassingFwhm = "QC_CALIBRATION_TEST_MIN_PASSING_FWHM";
		public static readonly string QcContaminationBackgroundMultiplier = "QC_CONTAMINATION_BACKGROUND_MULTIPLIER";
		public static readonly string QcContaminationCountTimeInSeconds = "QC_CONTAMINATION_COUNT_TIME_IN_SEC";
		public static readonly string QcContaminationMaxCpm = "QC_CONTAMINATION_MAX_CPM";
		public static readonly string QcCs137Centroid = "QC_CS137_CENTROID";
		public static readonly string QcDailyQcExpirationTime = "QC_DAILY_QC_EXPIRATION_TIME";
		public static readonly string QcInitialFineGainAdjustment = "QC_INITIAL_FINE_GAIN_ADJUSTMENT";
		public static readonly string QcLastQcTestPassed = "QC_LAST_QC_TEST_PASSED";
		public static readonly string QcLastSuccessfulFullQcOrLinearityDate = "QC_LAST_SUCCESSFUL_FULL_QC_OR_LINEARITY_DATE";
		public static readonly string QcLinearityCountTimeInSeconds = "QC_LINEARITY_COUNT_TIME_IN_SEC";
		public static readonly string QcLinearityMaxPassingFwhm = "QC_LINEARITY_MAX_PASSING_FWHM";
		public static readonly string QcLinearityMinCounts = "QC_LINEARITY_MIN_COUNTS";
		public static readonly string QcLinearityMinPassingFwhm = "QC_LINEARITY_MIN_PASSING_FWHM";
		public static readonly string QcMaxNumberOfFineGainAdjustments = "QC_MAX_NUMBER_OF_FINE_GAIN_ADJUSTMENTS";
		public static readonly string QcResolutionCentroidTolerance = "QC_RESOLUTION_CENTROID_TOLERANCE";
		public static readonly string QcSetupCentroidPrecisionInkeV = "QC_SETUP_CENTROID_PRECISION_IN_KEV";
		public static readonly string QcSetupSampleCountTimeInSeconds = "QC_SETUP_SAMPLE_COUNT_TIME_IN_SEC";
		public static readonly string QcSetupSampleThresholdCountTimeInSeconds = "QC_SETUP_SAMPLE_THRESHOLD_COUNT_TIME_IN_SEC";
		public static readonly string QcStandardsCountTimeInSeconds = "QC_STANDARDS_COUNT_TIME_IN_SEC";
		public static readonly string QcStandardsMinCpm = "QC_STANDARDS_MIN_CPM";
		public static readonly string QcStandardsStdDevDifferenceMultiplier = "QC_STANDARDS_STD_DEV_DIFFERENCE_MULTIPLIER";
		public static readonly string QcTestDue = "QC_TEST_DUE";
		public static readonly string SystemAutomaticBackupLocation = "SYSTEM_AUTOMATIC_BACKUP_LOCATION";
		public static readonly string SystemBackgroundAcquisitionTimeInSeconds = "SYSTEM_BACKGROUND_ACQUISITION_TIME_IN_SEC";
		public static readonly string SystemBackgroundPosition = "SYSTEM_BACKGROUND_POSITION";
		public static readonly string SystemClickSoundFilePath = "SYSTEM_CLICK_SOUND_FILE_PATH";
		public static readonly string SystemCurrentCalibrationCurveId = "SYSTEM_CURRENT_CALIBRATION_CURVE_ID";
		public static readonly string SystemDatabaseName = "Database";
		public static readonly string SystemDatabasePassword = "DatabasePassword";
		public static readonly string SystemDatabaseServer = "DatabaseName";
		public static readonly string SystemDatabaseUsername = "UserName";
		public static readonly string SystemDefaultAutomaticBackupLocation = "SYSTEM_DEFAULT_AUTOMATIC_BACKUP_LOCATION";
		public static readonly string SystemDefaultLanguageId = "SYSTEM_DEFAULT_LANGUAGE_ID";
		public static readonly string SystemDefaultMeasurementSystem = "SYSTEM_DEFAULT_MEASUREMENT_SYSTEM";
		public static readonly string SystemDefaultReportZoom = "SYSTEM_DEFAULT_REPORT_ZOOM";
		public static readonly string SystemDepartmentAddress = "SYSTEM_DEPARTMENT_ADDRESS";
		public static readonly string SystemDepartmentDirector = "SYSTEM_DEPARTMENT_DIRECTOR";
		public static readonly string SystemDepartmentName = "SYSTEM_DEPARTMENT_NAME";
		public static readonly string SystemDepartmentPhoneNumber = "SYSTEM_DEPARTMENT_PHONE_NUMBER";
		public static readonly string SystemDetectorDemoModeEnabled = "SYSTEM_DETECTOR_DEMO_MODE_ENABLED";
		public static readonly string SystemDetectorFineGain = "SYSTEM_DETECTOR_FINE_GAIN";
		public static readonly string SystemDetectorHighVoltage = "SYSTEM_DETECTOR_HIGH_VOLTAGE";
		public static readonly string SystemDeviceModality = "SYSTEM_DEVICE_MODALITY";
		public static readonly string SystemHighCpmBoundary = "SYSTEM_HIGH_CPM_BOUNDARY";
		public static readonly string SystemHighCpmRestartTimeIntervalInSeconds = "SYSTEM_HIGH_CPM_RESTART_TIME_INTERVAL_IN_SEC";
		public static readonly string SystemHospitalName = "SYSTEM_HOSPITAL_NAME";
		public static readonly string SystemI131RoiHighBound = "SYSTEM_I131_ROI_HIGH_BOUND";
		public static readonly string SystemI131RoiLowBound = "SYSTEM_I131_ROI_LOW_BOUND";
		public static readonly string SystemInjectateVerificationEnabled = "SYSTEM_INJECTATE_VERIFICATION_ENABLED";
		public static readonly string SystemLastAutomatedBackup = "SYSTEM_LAST_AUTOMATED_BACKUP";
		public static readonly string SystemLinqConnectionString = "LINQConnectionString";
		public static readonly string SystemNumberOfBackupsToKeep = "SYSTEM_NUMBER_OF_BACKUPS_TO_KEEP";
		public static readonly string SystemPasswordCustomerAdmin = "SYSTEM_PASSWORD_CUSTOMER_ADMIN";
		public static readonly string SystemPasswordInternal = "SYSTEM_PASSWORD_INTERNAL";
		public static readonly string SystemPasswordService = "SYSTEM_PASSWORD_SERVICE";
		public static readonly string SystemPasswordUnsecureExport = "SYSTEM_PASSWORD_UNSECURE_EXPORT";
		public static readonly string SystemQcDatabaseArchiveDatabaseName = "QCDatabaseName";
		public static readonly string SystemSampleChangerDemoModeEnabled = "SYSTEM_SAMPLE_CHANGER_DEMO_MODE_ENABLED";
		public static readonly string SystemSampleChangerLayoutId = "SYSTEM_SAMPLE_CHANGER_LAYOUT_ID";
		public static readonly string SystemScheduledAutomatedBackupTime = "SYSTEM_SCHEDULED_AUTOMATED_BACKUP_TIME";
		public static readonly string SystemShowQcArchivesButton = "SYSTEM_SHOW_QC_ARCHIVES_BUTTON";
		public static readonly string SystemSoundEnabled = "SYSTEM_SOUND_ENABLED";
		public static readonly string SystemTempFilePath = "SYSTEM_TEMP_FILE_PATH";
		public static readonly string SystemUniqueSystemId = "SYSTEM_UNIQUE_SYSTEM_ID";
		public static readonly string SystemWarningSoundFilePath = "SYSTEM_WARNING_SOUND_FILE_PATH";
		public static readonly string SystemShowOrderingPhysiciansReportButton = "SYSTEM_SHOW_ORDERING_PHYSICIANS_REPORT_BUTTON";
	}
} 