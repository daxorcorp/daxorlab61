using System;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_a_sample
    {
        private QcCalibrationTest _calibrationTest;

        [TestInitialize]
        public void TestInitialize()
        {
            _calibrationTest = new CalibrationTestThatDoesNotInitializeFineGainMetadata(null)
            {
                PayloadProcessor = new QcCalibrationTestPayloadProcessor()
            };
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_sample_status_is_not_completed_then_the_correct_metadata_is_returned()
        {
            var notCompletedSample = new QcSample();
            var expectedMetadata = new QcSampleEvaluationMetadata(notCompletedSample.InternalId, QcSampleEvaluationResult.Undetermined);

            var nonCompleteExecutionStatuses = Enum.GetValues(typeof(SampleExecutionStatus))
                                                   .Cast<SampleExecutionStatus>()
                                                   .Where(executionStatus => executionStatus != SampleExecutionStatus.Completed);
            foreach (var executionStatus in nonCompleteExecutionStatuses)
            {
                notCompletedSample.ExecutionStatus = executionStatus;

                var observedMetadata = _calibrationTest.EvaluateSample(notCompletedSample);

                Assert.AreEqual(expectedMetadata, observedMetadata, "Didn't get expected metadata for execution type: " + executionStatus);
            }
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_sample_type_is_background_then_the_correct_metadata_is_returned()
        {
            var completedBackgroundSample = new QcSample();
            var expectedMetadata = new QcSampleEvaluationMetadata(completedBackgroundSample.InternalId, QcSampleEvaluationResult.Pass);

            completedBackgroundSample.ExecutionStatus = SampleExecutionStatus.Completed;
            completedBackgroundSample.TypeOfSample = SampleType.Background;

            var observedMetadata = _calibrationTest.EvaluateSample(completedBackgroundSample);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_sample_status_is_completed_then_the_fine_gain_metadata_is_initialized()
        {
            const double CurrentFineGain = 1.005;
            const double MinimumFineGain = 0.625;
            const double MaximumFineGain = 1.725;

            var backgroundSample = new QcSample
            {
                ExecutionStatus = SampleExecutionStatus.Completed,
                TypeOfSample = SampleType.Background
            };

            var specialTest = new CalibrationTestThatDoesNotFindTheSetupIndex
            {
                MinFineGainAllowed = MinimumFineGain,
                MaxFineGainAllowed = MaximumFineGain,
                StartFineGain = CurrentFineGain
            };

            var expectedFineGainMetadata = new FineGainMetadata
            {
                CurrentFineGain = CurrentFineGain,
                IsFirstAdjustment = true,
                MinFineGain = MinimumFineGain,
                MaxFineGain = MaximumFineGain
            };

            specialTest.EvaluateSample(backgroundSample);

            Assert.IsTrue(AreEqual(expectedFineGainMetadata, specialTest.FineGainMetaDataWrapper));
        }

        private static bool AreEqual(FineGainMetadata expected, FineGainMetadata observed)
        {
            if (expected.IsFirstAdjustment != observed.IsFirstAdjustment) return false;
            if (Math.Abs(expected.CurrentFineGain - observed.CurrentFineGain) > 1E-10) return false;
            if (Math.Abs(expected.MaxFineGain - observed.MaxFineGain) > 1E-10) return false;
            return Math.Abs(expected.MinFineGain - observed.MinFineGain) < 1E-10;
        }
    }
    // ReSharper restore InconsistentNaming
}
