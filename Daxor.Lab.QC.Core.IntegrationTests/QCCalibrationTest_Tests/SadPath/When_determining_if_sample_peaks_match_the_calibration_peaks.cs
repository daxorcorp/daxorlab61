using System;
using System.Collections.Generic;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Factories;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_sample_peaks_match_the_calibration_peaks
    {
        private QcCalibrationTest _calibrationTest;

        [TestInitialize]
        public void TestInitialize()
        {
            _calibrationTest = new QcCalibrationTest(null) { IsotopeMatchingService = new IsotopeMatchingService() };
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_isotope_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_calibrationTest.SamplePeaksMatchTheSetupIsotopePeaks(null, new List<Peak>()), "setupIsotope");

        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_list_of_peaks_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_calibrationTest.SamplePeaksMatchTheSetupIsotopePeaks(IsotopeFactory.CreateIsotope(IsotopeType.Ba133), null), "peaksSortedByCentroid");
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_isotope_matching_factory_instance_has_not_been_set_then_an_exception_is_thrown()
        {
            _calibrationTest.IsotopeMatchingService = null;
            AssertEx.Throws<InvalidOperationException>(()=>_calibrationTest.SamplePeaksMatchTheSetupIsotopePeaks(IsotopeFactory.CreateIsotope(IsotopeType.Ba133), new List<Peak>()));
        }
    }
    // ReSharper restore InconsistentNaming
}
