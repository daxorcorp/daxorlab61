using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.CalibrationSampleTests.SetupSampleTests.FineGainAdjustingSubcases
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_the_number_of_fine_gain_adjustments_is_at_the_limit : And_the_number_of_fine_gain_adjustments_exceeds_the_limit
    {
        [TestInitialize]
        public new void TestInitialize()
        {
            base.TestInitialize();
            _calibrationTest.NumberOfFineGainAdjustmentsWrapper = MaxAllowableFineGainAdjustments;
            _calibrationTest.FineGainMetadataWrapper = new FineGainMetadata();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void Then_the_sample_is_marked_as_failed()
        {
            base.Then_the_sample_is_marked_as_failed();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void Then_the_evaluation_for_the_sample_is_complete()
        {
            base.Then_the_evaluation_for_the_sample_is_complete();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void Then_the_error_details_for_the_sample_are_correct()
        {
            base.Then_the_error_details_for_the_sample_are_correct();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void Then_the_sample_failed_evaluation_metadata_is_correct()
        {
            base.Then_the_sample_failed_evaluation_metadata_is_correct();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void Then_sample_failed_metadata_is_added()
        {
            base.Then_sample_failed_metadata_is_added();
        }
    }
    // ReSharper restore InconsistentNaming
}
