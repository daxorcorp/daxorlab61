using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.SampleEvaluators;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.CalibrationSampleTests.SetupSampleTests.FineGainAdjustingSubcases
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_the_number_of_fine_gain_adjustments_exceeds_the_limit
    {
        public const int MaxAllowableFineGainAdjustments = 10;
        
        private QcSample _setupSample;
        protected CalibrationTestThatDoesNotInitializeFineGainMetadata _calibrationTest;

        [TestInitialize]
        public void TestInitialize()
        {
            _calibrationTest = new CalibrationTestThatDoesNotInitializeFineGainMetadata(null);
            _calibrationTest.BuildSamples();
            _calibrationTest.IsFineGainDoneAdjusting = false;
            _calibrationTest.MaxAllowableFineGainAdjustments = MaxAllowableFineGainAdjustments;
            _calibrationTest.FineGainMetadataWrapper = new FineGainMetadata();
            _calibrationTest.NumberOfFineGainAdjustmentsWrapper = MaxAllowableFineGainAdjustments + 1;
            _calibrationTest.PayloadProcessor = new QcCalibrationTestPayloadProcessor();
            
            _setupSample = _calibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC1);
            if (_setupSample == null) throw new NotSupportedException("Unable to find setup sample");
            _setupSample.ExecutionStatus = SampleExecutionStatus.Completed;
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_is_marked_as_failed()
        {
            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(false, _setupSample.IsPassed);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_evaluation_for_the_sample_is_complete()
        {
            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(true, _setupSample.IsEvaluationCompleted);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_error_details_for_the_sample_are_correct()
        {
            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(String.Format(SetupSampleEvaluator.MaximumFineGainAdjustmentsExceededFormatString, MaxAllowableFineGainAdjustments), 
                _setupSample.ErrorDetails);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_failed_evaluation_metadata_is_correct()
        {
            var observedMetadata = _calibrationTest.EvaluateSample(_setupSample);

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId,
                QcSampleEvaluationResult.Failed,
                String.Format(SetupSampleEvaluator.MaximumFineGainAdjustmentsExceededFormatString, MaxAllowableFineGainAdjustments));
            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_sample_failed_metadata_is_added()
        {
            _calibrationTest.EvaluateSample(_setupSample);

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId,
                QcSampleEvaluationResult.Failed,
                String.Format(SetupSampleEvaluator.MaximumFineGainAdjustmentsExceededFormatString, MaxAllowableFineGainAdjustments));
            CollectionAssert.AreEqual(new List<QcSampleEvaluationMetadata> { expectedMetadata }, _calibrationTest.EvaluationResults.ToList());
        }
    }
    // ReSharper restore InconsistentNaming
}
