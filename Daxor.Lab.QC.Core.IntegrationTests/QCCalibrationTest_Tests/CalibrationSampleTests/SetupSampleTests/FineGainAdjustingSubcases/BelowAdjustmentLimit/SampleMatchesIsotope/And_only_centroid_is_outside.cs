using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.CalibrationSampleTests.SetupSampleTests.FineGainAdjustingSubcases.BelowAdjustmentLimit.SampleMatchesIsotope
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_only_centroid_is_outside
    {
        public const int MaxAllowableFineGainAdjustments = 10;
        public const double FirstFineGainAdjustment = 0.049;

        protected QcSample _setupSample;
        protected CalibrationTestWhoseSamplePeaksMatchTheSetupIsotopePeaks _calibrationTest;

        [TestInitialize]
        public void TestInitialize()
        {
            _calibrationTest = new CalibrationTestWhoseSamplePeaksMatchTheSetupIsotopePeaks
            {
                ShouldCentroidBeWithinTolerance = false,
                ShouldPeakBeWithinRoi = true,
                PayloadProcessor = new QcCalibrationTestPayloadProcessor(),
                IsotopeMatchingService = MockRepository.GenerateStub<IIsotopeMatchingService>()
            };
            _calibrationTest.IsotopeMatchingService.Expect(x => x.IsMatching(null, null)).IgnoreArguments().Return(true);
            _calibrationTest.BuildSamples();
            _calibrationTest.IsFineGainDoneAdjusting = false;
            _calibrationTest.MaxAllowableFineGainAdjustments = MaxAllowableFineGainAdjustments;
            _calibrationTest.FineGainMetadataWrapper = new FineGainMetadata();
            _calibrationTest.NumberOfFineGainAdjustmentsWrapper = MaxAllowableFineGainAdjustments - 1;
            _calibrationTest.PeakFinder = MockRepository.GenerateStub<IPeakFinder>();
          
            _calibrationTest.PeakFinder.Expect(f => f.FindPeaksSortedByCentroid(null, -1, -1))
                .IgnoreArguments()
                .Return(new List<Peak> { new Peak() });

            _calibrationTest.SpectroscopyService = MockRepository.GenerateStub<ISpectroscopyService>();

            _setupSample = _calibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC1);
            if (_setupSample == null) throw new NotSupportedException("Unable to find setup sample");

            _setupSample.ExecutionStatus = SampleExecutionStatus.Completed;
            _setupSample.Source = new QcSource(new Isotope(IsotopeType.Cs137, 0, UnitOfTime.Minute, new List<double>()), DateTime.Now,
                0, 0, 0, "whatever", 0);

            _calibrationTest.FineGainAdjustmentService = MockRepository.GenerateStub<IFineGainAdjustmentService>();
            _calibrationTest.FineGainAdjustmentService.InitialFineGainAdjustment = FirstFineGainAdjustment;
        }
        
        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_a_new_fine_gain_is_determined()
        {
            var mockFineGainAdjustmentService = MockRepository.GenerateMock<IFineGainAdjustmentService>();
            mockFineGainAdjustmentService.Expect(x => x.DetermineNewFineGainMetadata
                (Arg<double>.Is.Anything, 
                Arg<double>.Is.Anything,
                Arg<FineGainMetadata>.Is.Equal(_calibrationTest.FineGainMetadataWrapper))).Return(new FineGainMetadata());

            _calibrationTest.FineGainAdjustmentService = mockFineGainAdjustmentService;
            
            _calibrationTest.EvaluateSample(_setupSample);

            mockFineGainAdjustmentService.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_number_of_fine_gain_adjustments_is_incremented()
        {
            var stubFineGainAdjustmentService = MockRepository.GenerateStub<IFineGainAdjustmentService>();
            stubFineGainAdjustmentService.Expect(x => x.DetermineNewFineGainMetadata(0, 0, null))
                .IgnoreArguments()
                .Return(new FineGainMetadata());

            _calibrationTest.NumberOfFineGainAdjustmentsWrapper = MaxAllowableFineGainAdjustments - 1;
            _calibrationTest.FineGainAdjustmentService = stubFineGainAdjustmentService;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(MaxAllowableFineGainAdjustments, _calibrationTest.NumberOfFineGainAdjustmentsWrapper);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_a_fine_gain_is_changed_to_the_new_value()
        {
            const double newFineGain = 0.456;

            var stubFineGainAdjustmentService = MockRepository.GenerateStub<IFineGainAdjustmentService>();
            stubFineGainAdjustmentService.Expect(x => x.DetermineNewFineGainMetadata(0, 0, null))
                .IgnoreArguments()
                .Return(new FineGainMetadata { CurrentFineGain = newFineGain });

            _calibrationTest.FineGainAdjustmentService = stubFineGainAdjustmentService;

            _calibrationTest.CurrentFineGain = 0.123;
            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(newFineGain, _calibrationTest.CurrentFineGain);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_correct_metadata_is_returned()
        {
            var stubFineGainAdjustmentService = MockRepository.GenerateStub<IFineGainAdjustmentService>();
            stubFineGainAdjustmentService.Expect(x => x.DetermineNewFineGainMetadata(0, 0, null))
                .IgnoreArguments()
                .Return(new FineGainMetadata {CurrentFineGain = _calibrationTest.CurrentFineGain + 0.03 });

            _calibrationTest.FineGainAdjustmentService = stubFineGainAdjustmentService;

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.FailedButRepeat);
            var observedMetadata = _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_fine_gain_did_not_change_then_the_correct_metadata_is_returned()
        {
            _calibrationTest.CurrentFineGain = 1.013;

            var stubFineGainAdjustmentService = MockRepository.GenerateStub<IFineGainAdjustmentService>();
            stubFineGainAdjustmentService.Expect(x => x.DetermineNewFineGainMetadata(0, 0, null))
                .IgnoreArguments()
                .Return(new FineGainMetadata { CurrentFineGain = _calibrationTest.CurrentFineGain });

            _calibrationTest.FineGainAdjustmentService = stubFineGainAdjustmentService;

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Failed, "Further fine gain adjustments will have no effect (10 adjustment(s) made).");
            var observedMetadata = _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_fine_gain_did_not_change_then_the_sample_is_marked_as_failed()
        {
            _calibrationTest.CurrentFineGain = 1.013;

            var stubFineGainAdjustmentService = MockRepository.GenerateStub<IFineGainAdjustmentService>();
            stubFineGainAdjustmentService.Expect(x => x.DetermineNewFineGainMetadata(0, 0, null))
                .IgnoreArguments()
                .Return(new FineGainMetadata { CurrentFineGain = _calibrationTest.CurrentFineGain });

            _calibrationTest.FineGainAdjustmentService = stubFineGainAdjustmentService;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(false, _setupSample.IsPassed);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_fine_gain_did_not_change_then_the_sample_error_details_are_correct()
        {
            _calibrationTest.CurrentFineGain = 1.013;
            _calibrationTest.NumberOfFineGainAdjustmentsWrapper = 5;

            var stubFineGainAdjustmentService = MockRepository.GenerateStub<IFineGainAdjustmentService>();
            stubFineGainAdjustmentService.Expect(x => x.DetermineNewFineGainMetadata(0, 0, null))
                .IgnoreArguments()
                .Return(new FineGainMetadata { CurrentFineGain = _calibrationTest.CurrentFineGain });

            _calibrationTest.FineGainAdjustmentService = stubFineGainAdjustmentService;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual("Further fine gain adjustments will have no effect (6 adjustment(s) made).", _setupSample.ErrorDetails);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_fine_gain_did_not_change_then_sample_evaluation_has_completed()
        {
            _calibrationTest.CurrentFineGain = 1.013;
            _calibrationTest.NumberOfFineGainAdjustmentsWrapper = 5;

            var stubFineGainAdjustmentService = MockRepository.GenerateStub<IFineGainAdjustmentService>();
            stubFineGainAdjustmentService.Expect(x => x.DetermineNewFineGainMetadata(0, 0, null))
                .IgnoreArguments()
                .Return(new FineGainMetadata { CurrentFineGain = _calibrationTest.CurrentFineGain });

            _calibrationTest.FineGainAdjustmentService = stubFineGainAdjustmentService;

            _calibrationTest.EvaluateSample(_setupSample);
            Assert.IsTrue(_setupSample.IsEvaluationCompleted);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_fine_gain_did_not_change_then_the_list_of_failures_includes_this_sample()
        {
            _calibrationTest.CurrentFineGain = 1.013;
            _calibrationTest.NumberOfFineGainAdjustmentsWrapper = 5;

            var stubFineGainAdjustmentService = MockRepository.GenerateStub<IFineGainAdjustmentService>();
            stubFineGainAdjustmentService.Expect(x => x.DetermineNewFineGainMetadata(0, 0, null))
                .IgnoreArguments()
                .Return(new FineGainMetadata { CurrentFineGain = _calibrationTest.CurrentFineGain });

            _calibrationTest.FineGainAdjustmentService = stubFineGainAdjustmentService;

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Failed,"Further fine gain adjustments will have no effect (6 adjustment(s) made).");

            _calibrationTest.EvaluateSample(_setupSample);

            CollectionAssert.AreEqual(new List<QcSampleEvaluationMetadata> { expectedMetadata }, _calibrationTest.EvaluationResults.ToList());
        }
    }
    // ReSharper restore InconsistentNaming
}
