using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.CalibrationSampleTests.SetupSampleTests.FineGainAdjustingSubcases.BelowAdjustmentLimit.SampleMatchesIsotope
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_ROI_and_centroid_are_outside : And_only_centroid_is_outside
    {
        [TestInitialize]
        public new void TestInitialize()
        {
            _calibrationTest = new CalibrationTestWhoseSamplePeaksMatchTheSetupIsotopePeaks
            {
                ShouldCentroidBeWithinTolerance = false,
                ShouldPeakBeWithinRoi = false,
                PayloadProcessor = new QcCalibrationTestPayloadProcessor(),
                IsotopeMatchingService = MockRepository.GenerateStub<IIsotopeMatchingService>()
            };
            _calibrationTest.IsotopeMatchingService.Expect(x => x.IsMatching(null, null)).IgnoreArguments().Return(true);
            _calibrationTest.BuildSamples();
            _calibrationTest.IsFineGainDoneAdjusting = false;
            _calibrationTest.MaxAllowableFineGainAdjustments = MaxAllowableFineGainAdjustments;
            _calibrationTest.FineGainMetadataWrapper = new FineGainMetadata();
            _calibrationTest.NumberOfFineGainAdjustmentsWrapper = MaxAllowableFineGainAdjustments - 1;
            _calibrationTest.PeakFinder = MockRepository.GenerateStub<IPeakFinder>();

            _calibrationTest.PeakFinder.Expect(f => f.FindPeaksSortedByCentroid(null, -1, -1))
                .IgnoreArguments()
                .Return(new List<Peak> { new Peak() });

            _calibrationTest.SpectroscopyService = MockRepository.GenerateStub<ISpectroscopyService>();

            _setupSample = _calibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC1);
            if (_setupSample == null) throw new NotSupportedException("Unable to find setup sample");
            _setupSample.ExecutionStatus = SampleExecutionStatus.Completed;
            _setupSample.Source = new QcSource(new Isotope(IsotopeType.Ba133, 0, UnitOfTime.Minute, new List<double>()), DateTime.Now,
                0, 0, 0, "whatever", 0);

            _calibrationTest.FineGainAdjustmentService = MockRepository.GenerateStub<IFineGainAdjustmentService>();
            _calibrationTest.FineGainAdjustmentService.InitialFineGainAdjustment = FirstFineGainAdjustment;
        }
    }
    // ReSharper restore InconsistentNaming
}
