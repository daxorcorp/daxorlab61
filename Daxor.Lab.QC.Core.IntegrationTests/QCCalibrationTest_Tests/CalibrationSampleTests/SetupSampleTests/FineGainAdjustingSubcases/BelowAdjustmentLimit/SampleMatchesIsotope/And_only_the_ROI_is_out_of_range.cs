using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.CalibrationSampleTests.SetupSampleTests.FineGainAdjustingSubcases.BelowAdjustmentLimit.SampleMatchesIsotope
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_only_the_ROI_is_out_of_range : And_only_centroid_is_outside
    {
        [TestInitialize]
        public new void TestInitialize()
        {
            _calibrationTest = new CalibrationTestWhoseSamplePeaksMatchTheSetupIsotopePeaks
            {
                ShouldCentroidBeWithinTolerance = true,
                ShouldPeakBeWithinRoi = false,
                PayloadProcessor = new QcCalibrationTestPayloadProcessor(),
                IsotopeMatchingService = MockRepository.GenerateStub<IIsotopeMatchingService>()
            };
            _calibrationTest.IsotopeMatchingService.Expect(x => x.IsMatching(null, null)).IgnoreArguments().Return(true);
            _calibrationTest.BuildSamples();
            _calibrationTest.IsFineGainDoneAdjusting = false;
            _calibrationTest.MaxAllowableFineGainAdjustments = MaxAllowableFineGainAdjustments;
            _calibrationTest.FineGainMetadataWrapper = new FineGainMetadata();
            _calibrationTest.NumberOfFineGainAdjustmentsWrapper = MaxAllowableFineGainAdjustments - 1;
            _calibrationTest.PeakFinder = MockRepository.GenerateStub<IPeakFinder>();

            _calibrationTest.PeakFinder.Expect(f => f.FindPeaksSortedByCentroid(null, -1, -1))
                .IgnoreArguments()
                .Return(new List<Peak> { new Peak() });

            _calibrationTest.SpectroscopyService = MockRepository.GenerateStub<ISpectroscopyService>();

            _setupSample = _calibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC1);
            if (_setupSample == null) throw new NotSupportedException("Unable to find setup sample");
            _setupSample.ExecutionStatus = SampleExecutionStatus.Completed;
            _setupSample.Source = new QcSource(new Isotope(IsotopeType.Ba133, 0, UnitOfTime.Minute, new List<double>()), DateTime.Now,
                0, 0, 0, "whatever", 0);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void Then_a_new_fine_gain_is_determined()
        {
            base.Then_a_fine_gain_is_changed_to_the_new_value();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void Then_the_number_of_fine_gain_adjustments_is_incremented()
        {
            base.Then_the_number_of_fine_gain_adjustments_is_incremented();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void Then_a_fine_gain_is_changed_to_the_new_value()
        {
            base.Then_a_fine_gain_is_changed_to_the_new_value();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void Then_the_correct_metadata_is_returned()
        {
            base.Then_the_correct_metadata_is_returned();
        }
    }

    // ReSharper restore InconsistentNaming
}
