using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.CalibrationSampleTests.SetupSampleTests.FineGainAdjustingSubcases.BelowAdjustmentLimit.SampleMatchesIsotope
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_peak_and_centroid_are_in_tolerance
    {
        public const int MaxAllowableFineGainAdjustments = 10;
        public const int MinimumPassingFwhm = 10;
        public const int MaximumPassingFwhm = 50;
        public const string FwhmFailureDetails = "Setup fails on FWHM.";

        private QcSample _setupSample;
        private CalibrationTestWhoseSamplePeaksMatchTheSetupIsotopePeaks _calibrationTest;

        [TestInitialize]
        public void TestInitialize()
        {
            _calibrationTest = new CalibrationTestWhoseSamplePeaksMatchTheSetupIsotopePeaks
            {
                ShouldCentroidBeWithinTolerance = true,
                ShouldPeakBeWithinRoi = true,
                PayloadProcessor = new QcCalibrationTestPayloadProcessor(),
                IsotopeMatchingService = MockRepository.GenerateStub<IIsotopeMatchingService>()
            };
            _calibrationTest.IsotopeMatchingService.Expect(x => x.IsMatching(null, null)).IgnoreArguments().Return(true);
            _calibrationTest.BuildSamples();
            _calibrationTest.IsFineGainDoneAdjusting = false;
            _calibrationTest.MaxAllowableFineGainAdjustments = MaxAllowableFineGainAdjustments;
            _calibrationTest.MinimumPassingSetupFullWidthHalfMax = MinimumPassingFwhm;
            _calibrationTest.MaximumPassingSetupFullWidthHalfMax = MaximumPassingFwhm;
            _calibrationTest.FineGainMetadataWrapper = new FineGainMetadata();
            _calibrationTest.NumberOfFineGainAdjustmentsWrapper = MaxAllowableFineGainAdjustments - 1;
            _calibrationTest.PeakFinder = MockRepository.GenerateStub<IPeakFinder>();
            _calibrationTest.PeakFinder.Expect(f => f.FindPeaksSortedByCentroid(null, -1, -1))
                .IgnoreArguments()
                .Return(new List<Peak> { new Peak { Centroid = 30 } });

            _calibrationTest.SpectroscopyService = MockRepository.GenerateStub<ISpectroscopyService>();

            _setupSample = _calibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC1);
            if (_setupSample == null) throw new NotSupportedException("Unable to find setup sample");

            _setupSample.ExecutionStatus = SampleExecutionStatus.Completed;
            _setupSample.Source = new QcSource(new Isotope(IsotopeType.Ba133, 0, UnitOfTime.Minute, new List<double>()), DateTime.Now,
                0, 0, 0, "whatever", 0);
            _setupSample.StartChannel = 10;
            _setupSample.EndChannel = 50;
            _setupSample.Centroid = 30;
        }

        [TestMethod]
        public void And_the_sample_has_been_evaluated()
        {
            _calibrationTest.EvaluateSample(_setupSample);
            Assert.IsTrue(_setupSample.IsEvaluationCompleted);
        }

        #region FWHM below minimum

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_below_the_minimum_passing_FWHM_then_the_sample_is_marked_as_failed()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingFwhm - 1;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(false, _setupSample.IsPassed);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_below_the_minimum_passing_FWHM_then_the_correct_metadata_is_returned()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingFwhm - 1;

            var observedResult = _calibrationTest.EvaluateSample(_setupSample);

            var expectedResult = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Failed, FwhmFailureDetails);

            Assert.AreEqual(expectedResult, observedResult);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_below_the_minimum_passing_FWHM_then_the_sample_error_details_are_correct()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingFwhm - 1;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(FwhmFailureDetails, _setupSample.ErrorDetails);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_below_the_minimum_passing_FWHM_then_the_failed_metadata_is_added()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingFwhm - 1;

            _calibrationTest.EvaluateSample(_setupSample);

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Failed, FwhmFailureDetails);

            CollectionAssert.AreEqual(new List<QcSampleEvaluationMetadata> { expectedMetadata }, _calibrationTest.EvaluationResults.ToList());
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_below_the_minimum_passing_FWHM_then_the_fine_gain_has_not_finished_adjusting()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingFwhm - 1;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.IsFalse(_calibrationTest.IsFineGainDoneAdjusting);
        }

        #endregion

        #region FWHM above maximum

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_above_the_maximum_passing_FWHM_then_the_sample_is_marked_as_failed()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingFwhm + 1;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(false, _setupSample.IsPassed);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_above_the_maximum_passing_FWHM_then_the_correct_metadata_is_returned()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingFwhm + 1;

            var observedResult = _calibrationTest.EvaluateSample(_setupSample);

            var expectedResult = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Failed, FwhmFailureDetails);

            Assert.AreEqual(expectedResult, observedResult);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_above_the_maximum_passing_FWHM_then_the_sample_error_details_are_correct()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingFwhm + 1;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(FwhmFailureDetails, _setupSample.ErrorDetails);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_above_the_maximum_passing_FWHM_then_the_failed_metadata_is_added()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingFwhm + 1;

            _calibrationTest.EvaluateSample(_setupSample);

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Failed, FwhmFailureDetails);

            CollectionAssert.AreEqual(new List<QcSampleEvaluationMetadata> { expectedMetadata }, _calibrationTest.EvaluationResults.ToList());
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_above_the_maximum_passing_FWHM_then_the_fine_gain_has_not_finished_adjusting()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingFwhm + 1;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.IsFalse(_calibrationTest.IsFineGainDoneAdjusting);
        }

        #endregion

        #region FWHM at minimum

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_minimum_then_we_are_finished_adjusting_the_fine_gain()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingFwhm;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.IsTrue(_calibrationTest.IsFineGainDoneAdjusting);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_minimum_then_the_sample_preset_counts_limit_is_set_correctly()
        {
            const int setupSourceCountThreshold = 100;
            _setupSample.FullWidthHalfMax = MinimumPassingFwhm;
            _calibrationTest.SetupSourceCountThreshold = setupSourceCountThreshold;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(setupSourceCountThreshold, _setupSample.PresetCountsLimit);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_minimum_then_the_prior_sample_duration_in_seconds_is_updated_correctly()
        {
            const int sampleDurationInSeconds = 500;
            _calibrationTest.SampleDurationInSeconds = sampleDurationInSeconds;
            _setupSample.FullWidthHalfMax = MinimumPassingFwhm;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(sampleDurationInSeconds, _calibrationTest.PriorSampleDuration);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_minimum_then_the_sample_duration_in_seconds_is_updated_correctly()
        {
            const int setupSourceCountThresholdAcquisitionTimeInSeconds = 500;
            _calibrationTest.SetupSourceCountThresholdAcquisitionTimeInSeconds = setupSourceCountThresholdAcquisitionTimeInSeconds;
            _setupSample.FullWidthHalfMax = MinimumPassingFwhm;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(setupSourceCountThresholdAcquisitionTimeInSeconds, _calibrationTest.SampleDurationInSeconds);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_minimum_then_the_correct_metadata_is_returned()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingFwhm;

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId,
                QcSampleEvaluationResult.FailedButRepeat);

            var observedMetadata = _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        #endregion

        #region FWHM between min and max

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_between_the_minimum_and_maximum_then_we_are_finished_adjusting_the_fine_gain()
        {
            _setupSample.FullWidthHalfMax = (MinimumPassingFwhm + MaximumPassingFwhm) / 2;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.IsTrue(_calibrationTest.IsFineGainDoneAdjusting);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_between_the_minimum_and_maximum_then_the_sample_preset_counts_limit_is_set_correctly()
        {
            const int setupSourceCountThreshold = 100;
            _setupSample.FullWidthHalfMax = (MinimumPassingFwhm + MaximumPassingFwhm) / 2;
            _calibrationTest.SetupSourceCountThreshold = setupSourceCountThreshold;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(setupSourceCountThreshold, _setupSample.PresetCountsLimit);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_between_the_minimum_and_maximum_then_the_prior_sample_duration_in_seconds_is_updated_correctly()
        {
            const int sampleDurationInSeconds = 500;
            _calibrationTest.SampleDurationInSeconds = sampleDurationInSeconds;
            _setupSample.FullWidthHalfMax = (MinimumPassingFwhm + MaximumPassingFwhm) / 2;
            
            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(sampleDurationInSeconds, _calibrationTest.PriorSampleDuration);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_between_the_minimum_and_maximum_then_the_sample_duration_in_seconds_is_updated_correctly()
        {
            const int setupSourceCountThresholdAcquisitionTimeInSeconds = 500;
            _calibrationTest.SetupSourceCountThresholdAcquisitionTimeInSeconds = setupSourceCountThresholdAcquisitionTimeInSeconds;
            _setupSample.FullWidthHalfMax = (MinimumPassingFwhm + MaximumPassingFwhm) / 2;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(setupSourceCountThresholdAcquisitionTimeInSeconds, _calibrationTest.SampleDurationInSeconds);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_between_the_minimum_and_maximum_then_the_correct_metadata_is_returned()
        {
            _setupSample.FullWidthHalfMax = (MinimumPassingFwhm + MaximumPassingFwhm) / 2;

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId,
                QcSampleEvaluationResult.FailedButRepeat);

            var observedMetadata = _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        #endregion

        #region FWHM at maximum

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_maximum_then_we_are_finished_adjusting_the_fine_gain()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingFwhm;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.IsTrue(_calibrationTest.IsFineGainDoneAdjusting);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_maximum_then_the_sample_preset_counts_limit_is_set_correctly()
        {
            const int setupSourceCountThreshold = 100;
            _setupSample.FullWidthHalfMax = MaximumPassingFwhm;
            _calibrationTest.SetupSourceCountThreshold = setupSourceCountThreshold;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(setupSourceCountThreshold, _setupSample.PresetCountsLimit);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_maximum_then_the_prior_sample_duration_in_seconds_is_updated_correctly()
        {
            const int sampleDurationInSeconds = 500;
            _calibrationTest.SampleDurationInSeconds = sampleDurationInSeconds;
            _setupSample.FullWidthHalfMax = MaximumPassingFwhm;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(sampleDurationInSeconds, _calibrationTest.PriorSampleDuration);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_maximum_then_the_sample_duration_in_seconds_is_updated_correctly()
        {
            const int setupSourceCountThresholdAcquisitionTimeInSeconds = 500;
            _calibrationTest.SetupSourceCountThresholdAcquisitionTimeInSeconds = setupSourceCountThresholdAcquisitionTimeInSeconds;
            _setupSample.FullWidthHalfMax = MaximumPassingFwhm;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(setupSourceCountThresholdAcquisitionTimeInSeconds, _calibrationTest.SampleDurationInSeconds);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_maximum_then_the_correct_metadata_is_returned()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingFwhm;

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId,
                QcSampleEvaluationResult.FailedButRepeat);

            var observedMetadata = _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        #endregion
    }
    // ReSharper restore InconsistentNaming
}
