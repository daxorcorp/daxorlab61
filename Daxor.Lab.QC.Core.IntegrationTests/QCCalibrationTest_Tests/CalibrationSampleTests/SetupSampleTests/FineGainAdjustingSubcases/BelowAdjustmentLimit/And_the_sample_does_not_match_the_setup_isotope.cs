using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.SampleEvaluators;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.CalibrationSampleTests.SetupSampleTests.FineGainAdjustingSubcases.BelowAdjustmentLimit
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_the_sample_does_not_match_the_setup_isotope
    {
        public const int MaxAllowableFineGainAdjustments = 10;

        private QcSample _setupSample;
        private CalibrationTestWhoseSamplePeaksDoNotMatchTheSetupIsotopePeaks _calibrationTest;

        [TestInitialize]
        public void TestInitialize()
        {
            _calibrationTest = new CalibrationTestWhoseSamplePeaksDoNotMatchTheSetupIsotopePeaks(null);
            _calibrationTest.BuildSamples();
            _calibrationTest.IsFineGainDoneAdjusting = false;
            _calibrationTest.MaxAllowableFineGainAdjustments = MaxAllowableFineGainAdjustments;
            _calibrationTest.FineGainMetadataWrapper = new FineGainMetadata();
            _calibrationTest.NumberOfFineGainAdjustmentsWrapper = MaxAllowableFineGainAdjustments - 1;
            _calibrationTest.PeakFinder = MockRepository.GenerateStub<IPeakFinder>();
            _calibrationTest.PayloadProcessor = new QcCalibrationTestPayloadProcessor();
            _calibrationTest.IsotopeMatchingService = MockRepository.GenerateStub<IIsotopeMatchingService>();
            _calibrationTest.IsotopeMatchingService.Expect(x => x.IsMatching(null, null))
                .IgnoreArguments()
                .Return(false);
            _calibrationTest.PeakFinder.Expect(f => f.FindPeaksSortedByCentroid(null, -1, -1))
                .IgnoreArguments()
                .Return(new List<Peak>());

            _setupSample = _calibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC1);
            if (_setupSample == null) throw new NotSupportedException("Unable to find setup sample");
            _setupSample.ExecutionStatus = SampleExecutionStatus.Completed;
            _setupSample.Source = new QcSource(new Isotope(IsotopeType.Ba133, 0, UnitOfTime.Minute, new List<double>()), DateTime.Now,
                0, 0, 0, "whatever", 0);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_number_of_fine_gain_adjustments_is_incremented()
        {
            _calibrationTest.EvaluateSample(_setupSample);
            Assert.AreEqual(MaxAllowableFineGainAdjustments, _calibrationTest.NumberOfFineGainAdjustmentsWrapper);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_is_marked_as_failed()
        {
            _calibrationTest.EvaluateSample(_setupSample);
            Assert.AreEqual(false, _setupSample.IsPassed);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_evaluation_for_the_sample_is_complete()
        {
            _calibrationTest.EvaluateSample(_setupSample);
            Assert.IsTrue(_setupSample.IsEvaluationCompleted);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_correct_metadata_is_returned()
        {
            var observedMetadata = _calibrationTest.EvaluateSample(_setupSample);

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId,
                QcSampleEvaluationResult.Failed, SetupSampleEvaluator.SetupIsotopeNotDetectedMessage);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_error_details_are_correct()
        {
            _calibrationTest.EvaluateSample(_setupSample);
            Assert.AreEqual(SetupSampleEvaluator.SetupIsotopeNotDetectedMessage, _setupSample.ErrorDetails);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_metadata_is_added()
        {
            _calibrationTest.EvaluateSample(_setupSample);

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId,
                            QcSampleEvaluationResult.Failed, SetupSampleEvaluator.SetupIsotopeNotDetectedMessage);
            CollectionAssert.AreEqual(new List<QcSampleEvaluationMetadata> { expectedMetadata }, _calibrationTest.EvaluationResults.ToList());
        }

    }
    // ReSharper restore InconsistentNaming
}
