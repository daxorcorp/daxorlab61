using System;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.CalibrationSampleTests.SetupSampleTests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_fine_gain_has_finished_adjusting
    {
        private const int SampleDuration = 12;
        private CalibrationTestThatDoesNotInitializeFineGainMetadata _calibrationTest;
        private QcSample _setupSample;

        [TestInitialize]
        public void TestInitialize()
        {
            _calibrationTest = new CalibrationTestThatDoesNotInitializeFineGainMetadata(null);
            _calibrationTest.BuildSamples();
            _calibrationTest.PayloadProcessor = new QcCalibrationTestPayloadProcessor();

            _setupSample = _calibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC1);
            if (_setupSample == null) throw new NotSupportedException("Unable to find setup sample");
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_duration_for_the_test_is_correct()
        {
            _setupSample.ExecutionStatus = SampleExecutionStatus.Completed;

            _calibrationTest.IsFineGainDoneAdjusting = true;
            _calibrationTest.PriorSampleDuration = SampleDuration;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(SampleDuration, _calibrationTest.SampleDurationInSeconds);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_has_been_evaluated()
        {
            _setupSample.ExecutionStatus = SampleExecutionStatus.Completed;

            _calibrationTest.IsFineGainDoneAdjusting = true;
            _calibrationTest.PriorSampleDuration = SampleDuration;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.IsTrue(_setupSample.IsEvaluationCompleted);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_fine_gain_finished_flag_has_been_reset()
        {
            _setupSample.ExecutionStatus = SampleExecutionStatus.Completed;

            _calibrationTest.IsFineGainDoneAdjusting = true;
            _calibrationTest.PriorSampleDuration = SampleDuration;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.IsFalse(_calibrationTest.IsFineGainDoneAdjusting);
        }
    }
    // ReSharper restore InconsistentNaming
}
