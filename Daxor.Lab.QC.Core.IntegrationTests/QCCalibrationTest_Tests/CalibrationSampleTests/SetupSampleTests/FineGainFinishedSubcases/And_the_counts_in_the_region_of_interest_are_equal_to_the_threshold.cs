using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.CalibrationSampleTests.SetupSampleTests.FineGainFinishedSubcases
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_the_counts_in_the_region_of_interest_are_equal_to_the_threshold : And_the_counts_in_the_region_of_interest_exceed_the_threshold
    {
        [TestInitialize]
        public new void TestInitialize()
        {
            base.TestInitialize();
        }

        #region Sample State Tests

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_between_the_min_FWHM_and_the_max_FWHM_then_the_sample_is_marked_as_passed()
        {
            base.And_the_FWHM_is_between_the_min_FWHM_and_the_max_FWHM_then_the_sample_is_marked_as_passed();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_less_than_the_min_FWHM_then_the_sample_is_marked_as_failed()
        {
            base.And_the_FWHM_is_less_than_the_min_FWHM_then_the_sample_is_marked_as_failed();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_equal_to_the_min_FWHM_then_the_sample_is_marked_as_passed()
        {
            base.And_the_FWHM_is_equal_to_the_min_FWHM_then_the_sample_is_marked_as_passed();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_equal_to_the_max_FWHM_then_the_sample_is_marked_as_passed()
        {
            base.And_the_FWHM_is_equal_to_the_max_FWHM_then_the_sample_is_marked_as_passed();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_greater_than_the_max_FWHM_then_the_sample_is_marked_as_failed()
        {
            base.And_the_FWHM_is_greater_than_the_max_FWHM_then_the_sample_is_marked_as_failed();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_less_than_the_min_FWHM_then_the_sample_error_message_is_correct()
        {
            base.And_the_FWHM_is_less_than_the_min_FWHM_then_the_sample_error_message_is_correct();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_greater_than_the_max_FWHM_then_the_sample_error_message_is_correct()
        {
            base.And_the_FWHM_is_greater_than_the_max_FWHM_then_the_sample_error_message_is_correct();
        }

        #endregion

        #region Metadata Tests
        
        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_less_than_the_min_FWHM_then_the_metadata_is_added()
        {
            base.And_the_FWHM_is_less_than_the_min_FWHM_then_the_metadata_is_added();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_less_than_the_min_FWHM_then_the_correct_failure_metadata_is_returned()
        {
            base.And_the_FWHM_is_less_than_the_min_FWHM_then_the_correct_failure_metadata_is_returned();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_equal_to_the_min_FWHM_then_the_correct_passed_metadata_is_returned()
        {
            base.And_the_FWHM_is_equal_to_the_min_FWHM_then_the_correct_passed_metadata_is_returned();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_greater_than_the_max_FWHM_then_the_correct_failure_metadata_is_returned()
        {
            base.And_the_FWHM_is_greater_than_the_max_FWHM_then_the_correct_failure_metadata_is_returned();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_greater_than_the_max_FWHM_then_the_metadata_is_added()
        {
            base.And_the_FWHM_is_greater_than_the_max_FWHM_then_the_metadata_is_added();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_between_the_min_FWHM_and_the_max_FWHM_then_the_metadata_is_not_added()
        {
            base.And_the_FWHM_is_between_the_min_FWHM_and_the_max_FWHM_then_the_metadata_is_not_added();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_equal_to_the_min_FWHM_then_the_metadata_is_not_added()
        {
            base.And_the_FWHM_is_equal_to_the_min_FWHM_then_the_metadata_is_not_added();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_equal_to_the_max_FWHM_then_the_metadata_is_not_added()
        {
            base.And_the_FWHM_is_equal_to_the_max_FWHM_then_the_metadata_is_not_added();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_equal_to_the_max_FWHM_then_the_correct_passed_metadata_is_returned()
        {
            base.And_the_FWHM_is_equal_to_the_max_FWHM_then_the_correct_passed_metadata_is_returned();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public new void And_the_FWHM_is_between_the_min_FWHM_and_the_max_FWHM_then_the_correct_passed_metadata_is_returned()
        {
            base.And_the_FWHM_is_between_the_min_FWHM_and_the_max_FWHM_then_the_correct_passed_metadata_is_returned();
        }

        #endregion

        protected override void SetupCalibrationTestWithCountsExceedingThreshold()
        {
            base.SetupCalibrationTestWithCountsExceedingThreshold();
            _calibrationTest.SetupSourceCountThreshold = 0;
        }
    }
    // ReSharper restore InconsistentNaming
}
