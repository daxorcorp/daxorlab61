using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.CalibrationSampleTests.SetupSampleTests.FineGainFinishedSubcases
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_the_counts_in_the_region_of_interest_do_not_exceed_the_threshold
    {
        private const int SampleDuration = 12;
        private const double MinimumPassingSetupFullWidthHalfMax = 1;
        private const double MaximumPassingSetupFullWidthHalfMax = 3;
        private const int SetupSourceCountThreshold = 200;
        const string CountThresholdFormatString = "Setup source count threshold ({0}) was not reached.";

        private QcSample _setupSample;
        private CalibrationTestThatDoesNotInitializeFineGainMetadata _calibrationTest;

        [TestInitialize]
        public void TestInitialize()
        {
            _calibrationTest = new CalibrationTestThatDoesNotInitializeFineGainMetadata(null);
            _calibrationTest.BuildSamples();
            _calibrationTest.PayloadProcessor = new QcCalibrationTestPayloadProcessor();

            SetupTestWithHighSetupSourceCountThreshold();
            SetupCompletedSetupSample();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_is_marked_as_failed()
        {
            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(false, _setupSample.IsPassed);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_evaluation_for_the_sample_is_complete()
        {
            _calibrationTest.EvaluateSample(_setupSample);

            Assert.IsTrue(_setupSample.IsEvaluationCompleted);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_error_details_message_is_correct()
        {
            var errorMessage = String.Format(CountThresholdFormatString, SetupSourceCountThreshold);
            var metadata = _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(errorMessage, metadata.Message);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_sample_failed_metadata_is_added()
        {
            var errorMessage = String.Format(CountThresholdFormatString, SetupSourceCountThreshold);
            var metadata = _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(errorMessage, metadata.Message);
            CollectionAssert.AreEqual(new List<QcSampleEvaluationMetadata> { metadata }, _calibrationTest.EvaluationResults.ToList());
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_failed_metadata_is_returned()
        {
            var errorMessage = String.Format(CountThresholdFormatString, SetupSourceCountThreshold);
            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Failed, errorMessage);

            var observedMetadata = _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        private void SetupTestWithHighSetupSourceCountThreshold()
        {
            _calibrationTest.IsFineGainDoneAdjusting = true;
            _calibrationTest.PriorSampleDuration = SampleDuration;
            _calibrationTest.SetupSourceCountThreshold = SetupSourceCountThreshold;
            _calibrationTest.MinimumPassingSetupFullWidthHalfMax = MinimumPassingSetupFullWidthHalfMax;
            _calibrationTest.MaximumPassingSetupFullWidthHalfMax = MaximumPassingSetupFullWidthHalfMax;
        }

        private void SetupCompletedSetupSample()
        {
            _setupSample = _calibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC1);
            if (_setupSample == null) throw new NotSupportedException("Unable to find setup sample");

            _setupSample.ExecutionStatus = SampleExecutionStatus.Completed;
        }
    }
    // ReSharper restore InconsistentNaming
}
