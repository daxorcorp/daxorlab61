using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.SampleEvaluators;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.CalibrationSampleTests.SetupSampleTests.FineGainFinishedSubcases
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_the_counts_in_the_region_of_interest_exceed_the_threshold
    {
        private const int SampleDuration = 12;
        private const double MinimumPassingSetupFullWidthHalfMax = 1;
        private const double MaximumPassingSetupFullWidthHalfMax = 3;
        protected CalibrationTestThatDoesNotInitializeFineGainMetadata _calibrationTest;
        protected QcSample _setupSample;

        [TestInitialize]
        public void TestInitialize()
        {
            _calibrationTest = new CalibrationTestThatDoesNotInitializeFineGainMetadata(null);
            _calibrationTest.BuildSamples();
            _calibrationTest.PayloadProcessor = new QcCalibrationTestPayloadProcessor();

            SetupCalibrationTestWithCountsExceedingThreshold();
            SetupCompletedSetupSample();
        }

        #region Sample State Tests

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_between_the_min_FWHM_and_the_max_FWHM_then_the_sample_is_marked_as_passed()
        {
            _setupSample.FullWidthHalfMax = (MinimumPassingSetupFullWidthHalfMax + MaximumPassingSetupFullWidthHalfMax) / 2;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(true, _setupSample.IsPassed);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_less_than_the_min_FWHM_then_the_sample_is_marked_as_failed()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingSetupFullWidthHalfMax - 1;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(false, _setupSample.IsPassed);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_less_than_the_min_FWHM_then_the_evaluation_for_the_sample_is_complete()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingSetupFullWidthHalfMax - 1;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.IsTrue(_setupSample.IsEvaluationCompleted);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_min_FWHM_then_the_sample_is_marked_as_passed()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingSetupFullWidthHalfMax;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(true, _setupSample.IsPassed);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_max_FWHM_then_the_sample_is_marked_as_passed()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingSetupFullWidthHalfMax;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(true, _setupSample.IsPassed);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_greater_than_the_max_FWHM_then_the_sample_is_marked_as_failed()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingSetupFullWidthHalfMax + 1;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(false, _setupSample.IsPassed);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_greater_than_the_max_FWHM_then_the_evaluation_for_the_sample_is_complete()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingSetupFullWidthHalfMax + 1;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.IsTrue(_setupSample.IsEvaluationCompleted);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_less_than_the_min_FWHM_then_the_sample_error_message_is_correct()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingSetupFullWidthHalfMax - 1;

            var errorMessage = String.Format(SetupSampleEvaluator.SetupFwhmIsNotWithinToleranceFormatString, _setupSample.FullWidthHalfMax, MinimumPassingSetupFullWidthHalfMax, 3);

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(errorMessage, _setupSample.ErrorDetails);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_greater_than_the_max_FWHM_then_the_sample_error_message_is_correct()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingSetupFullWidthHalfMax + 1;

            var errorMessage = String.Format(SetupSampleEvaluator.SetupFwhmIsNotWithinToleranceFormatString, _setupSample.FullWidthHalfMax, MinimumPassingSetupFullWidthHalfMax, 3);

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(errorMessage, _setupSample.ErrorDetails);
        }
        
        #endregion

        #region Metadata Tests
        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_less_than_the_min_FWHM_then_the_metadata_is_added()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingSetupFullWidthHalfMax - 1;

            var errorMessage = String.Format(SetupSampleEvaluator.SetupFwhmIsNotWithinToleranceFormatString, _setupSample.FullWidthHalfMax, MinimumPassingSetupFullWidthHalfMax, 3);
            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Failed, errorMessage);
            _calibrationTest.EvaluateSample(_setupSample);

            CollectionAssert.AreEqual(new List<QcSampleEvaluationMetadata> { expectedMetadata }, _calibrationTest.EvaluationResults.ToList());
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_less_than_the_min_FWHM_then_the_correct_failure_metadata_is_returned()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingSetupFullWidthHalfMax - 1;

            var errorMessage = String.Format(SetupSampleEvaluator.SetupFwhmIsNotWithinToleranceFormatString, _setupSample.FullWidthHalfMax, MinimumPassingSetupFullWidthHalfMax, 3);
            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Failed, errorMessage);

            var observedMetadata = _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_min_FWHM_then_the_correct_passed_metadata_is_returned()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingSetupFullWidthHalfMax;

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Pass);

            var observedMetadata = _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_greater_than_the_max_FWHM_then_the_correct_failure_metadata_is_returned()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingSetupFullWidthHalfMax + 1;

            var errorMessage = String.Format(SetupSampleEvaluator.SetupFwhmIsNotWithinToleranceFormatString, _setupSample.FullWidthHalfMax, MinimumPassingSetupFullWidthHalfMax, 3);
            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Failed, errorMessage);

            var observedMetadata = _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_greater_than_the_max_FWHM_then_the_metadata_is_added()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingSetupFullWidthHalfMax + 1;

            var errorMessage = String.Format(SetupSampleEvaluator.SetupFwhmIsNotWithinToleranceFormatString, _setupSample.FullWidthHalfMax, MinimumPassingSetupFullWidthHalfMax, 3);
            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Failed, errorMessage);
            _calibrationTest.EvaluateSample(_setupSample);

            CollectionAssert.AreEqual(new List<QcSampleEvaluationMetadata> { expectedMetadata }, _calibrationTest.EvaluationResults.ToList());
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_between_the_min_FWHM_and_the_max_FWHM_then_the_metadata_is_not_added()
        {
            _setupSample.FullWidthHalfMax = (MinimumPassingSetupFullWidthHalfMax + MaximumPassingSetupFullWidthHalfMax) / 2;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(0, _calibrationTest.EvaluationResultCount);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_min_FWHM_then_the_metadata_is_not_added()
        {
            _setupSample.FullWidthHalfMax = MinimumPassingSetupFullWidthHalfMax;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(0, _calibrationTest.EvaluationResultCount);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_max_FWHM_then_the_metadata_is_not_added()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingSetupFullWidthHalfMax;

            _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(0, _calibrationTest.EvaluationResultCount);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_equal_to_the_max_FWHM_then_the_correct_passed_metadata_is_returned()
        {
            _setupSample.FullWidthHalfMax = MaximumPassingSetupFullWidthHalfMax;

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Pass);

            var observedMetadata = _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_FWHM_is_between_the_min_FWHM_and_the_max_FWHM_then_the_correct_passed_metadata_is_returned()
        {
            _setupSample.FullWidthHalfMax = (MinimumPassingSetupFullWidthHalfMax + MaximumPassingSetupFullWidthHalfMax) / 2;

            var expectedMetadata = new QcSampleEvaluationMetadata(_setupSample.InternalId, QcSampleEvaluationResult.Pass);

            var observedMetadata = _calibrationTest.EvaluateSample(_setupSample);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        #endregion

        #region Private Helpers
        protected virtual void SetupCalibrationTestWithCountsExceedingThreshold()
        {
            _calibrationTest.IsFineGainDoneAdjusting = true;
            _calibrationTest.PriorSampleDuration = SampleDuration;
            _calibrationTest.SetupSourceCountThreshold = -1;
            _calibrationTest.MinimumPassingSetupFullWidthHalfMax = MinimumPassingSetupFullWidthHalfMax;
            _calibrationTest.MaximumPassingSetupFullWidthHalfMax = MaximumPassingSetupFullWidthHalfMax;
        }

        private void SetupCompletedSetupSample()
        {
            _setupSample = _calibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC1);
            if (_setupSample == null) throw new NotSupportedException("Unable to find setup sample");

            _setupSample.ExecutionStatus = SampleExecutionStatus.Completed;
        }

        #endregion
    }
    // ReSharper restore InconsistentNaming
}
