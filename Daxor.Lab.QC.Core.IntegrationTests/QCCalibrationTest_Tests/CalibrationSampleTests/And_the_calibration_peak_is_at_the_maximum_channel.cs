using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.CalibrationSampleTests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_the_calibration_peak_is_at_the_maximum_channel
    {
        private CalibrationTestWhoseSamplePeaksMatchTheSetupIsotopePeaks _calibrationTest;
        private QcSample _calibrationSample;

        private const int _startChannel = 20;
        private const int _endChannel = 50;

        [TestInitialize]
        public void TestInitialize()
        {
            _calibrationTest = new CalibrationTestWhoseSamplePeaksMatchTheSetupIsotopePeaks
            {
                ShouldCentroidBeWithinTolerance = false,
                ShouldPeakBeWithinRoi = false,
                PeakFinder = GeneratePeakFinder(),
                PayloadProcessor = new QcCalibrationTestPayloadProcessor()
            };
            _calibrationTest.BuildSamples();

            _calibrationTest.SpectroscopyService = MockRepository.GenerateStub<ISpectroscopyService>();

            _calibrationSample = _calibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC2);
            if (_calibrationSample == null) throw new NotSupportedException("Unable to find QC2 in the test");
            _calibrationSample.ExecutionStatus = SampleExecutionStatus.Completed;
            _calibrationSample.StartChannel = _startChannel;
            _calibrationSample.EndChannel = _endChannel;
            _calibrationSample.Source = new QcSource(new Isotope(IsotopeType.Ba133, 0, UnitOfTime.Minute, new List<double> { 3.0 }), DateTime.Now,
                0, 0, 0, "whatever", 0);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_has_been_evaluated()
        {
            _calibrationTest.EvaluateSample(_calibrationSample);

            Assert.IsTrue(_calibrationSample.IsEvaluationCompleted);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_is_marked_as_passed()
        {
            _calibrationTest.EvaluateSample(_calibrationSample);

            Assert.AreEqual(true, _calibrationSample.IsPassed);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_metadata_is_cleared()
        {
            _calibrationTest.AddMetadata(new QcSampleEvaluationMetadata(Guid.Empty, QcSampleEvaluationResult.Pass));
            _calibrationTest.EvaluateSample(_calibrationSample);

            Assert.AreEqual(0, _calibrationTest.EvaluationResultCount);
            Assert.AreEqual(0, _calibrationTest.EvaluationResults.Count());
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_correct_calibration_curve_is_set()
        {
            var setupSample = _calibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC1);
            if (setupSample == null)
                Assert.Fail("Could not find the setup sample");

            var mockSpectroscopyService = MockRepository.GenerateMock<ISpectroscopyService>();
            mockSpectroscopyService.Expect(x => x.ComputeCentroid(_calibrationSample.Spectrum.SpectrumArray, _calibrationSample.StartChannel, _calibrationSample.EndChannel))
                        .Return(333.5);
            mockSpectroscopyService.Expect(x => x.ComputeCentroid(setupSample.Spectrum.SpectrumArray, setupSample.StartChannel, setupSample.EndChannel))
                        .Return(662.0);

            _calibrationTest.SetupPeakEnergy = 662.8732;
            _calibrationTest.CalibrationPeakEnergy = 320.5;
            _calibrationTest.SetupCentroid = 662.0;
            _calibrationTest.SpectroscopyService = mockSpectroscopyService;

            _calibrationTest.EvaluateSample(_calibrationSample);

            var observedCalibrationCurve = _calibrationTest.CalibrationCurve;
            var expectedCalibrationCurve = new CalibrationCurve(-1, new[] { 662.0, 333.5 },
                new[] { 662.8732, 320.5 });

            Assert.IsTrue(AreCalibrationCurvesTheSame(expectedCalibrationCurve, observedCalibrationCurve));
            mockSpectroscopyService.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_correct_metadata_is_returned()
        {
            var observedMetadata = _calibrationTest.EvaluateSample(_calibrationSample);
            var expectedMetadata = new QcSampleEvaluationMetadata(_calibrationSample.InternalId, QcSampleEvaluationResult.Pass);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        #region Helper Methods

        private static IPeakFinder GeneratePeakFinder()
        {
            var stubPeakFinder = MockRepository.GenerateStub<IPeakFinder>();
            stubPeakFinder.Expect(f => f.FindPeaks(new uint[] { }, 0))
                 .IgnoreArguments()
                 .Return(new List<Peak> { new Peak { Centroid = _endChannel} });
            return stubPeakFinder;
        }

        private static bool AreCalibrationCurvesTheSame(CalibrationCurve expectedCalibrationCurve, ICalibrationCurve observedCalibrationCurve)
        {
            if (expectedCalibrationCurve.CurveId != observedCalibrationCurve.CurveId)
                return false;
            if (Math.Abs(expectedCalibrationCurve.Offset - observedCalibrationCurve.Offset) > 1E-300)
                return false;

            return !(Math.Abs(expectedCalibrationCurve.Slope - observedCalibrationCurve.Slope) > 1E-300);
        }

        #endregion
    }
    // ReSharper restore InconsistentNaming
}
