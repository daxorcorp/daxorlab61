using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Core.IntegrationTests.QCCalibrationTest_Tests.CalibrationSampleTests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class And_the_calibration_peak_is_below_the_minimum_channel
    {
        private const int _startChannel = 20;
        private const int _endChannel = 50;

        private CalibrationTestWhoseSamplePeaksMatchTheSetupIsotopePeaks _calibrationTest;
        private QcSample _calibrationSample;

        [TestInitialize]
        public void TestInitialize()
        {
            _calibrationTest = new CalibrationTestWhoseSamplePeaksMatchTheSetupIsotopePeaks
            {
                ShouldCentroidBeWithinTolerance = false,
                ShouldPeakBeWithinRoi = false,
                SpectroscopyService = MockRepository.GenerateStub<ISpectroscopyService>(),
                PeakFinder = GeneratePeakFinderWithCentroidBelowMinimum(),
                PayloadProcessor = new QcCalibrationTestPayloadProcessor()
            };

            _calibrationTest.BuildSamples();

            _calibrationSample = _calibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC2);
            if (_calibrationSample == null) throw new NotSupportedException("Unable to find QC2 in the test");
            _calibrationSample.ExecutionStatus = SampleExecutionStatus.Completed;
            _calibrationSample.StartChannel = _startChannel;
            _calibrationSample.EndChannel = _endChannel;
            _calibrationSample.Source = new QcSource(new Isotope(IsotopeType.Ba133, 0, UnitOfTime.Minute, new List<double> { 3.0 }), DateTime.Now,
                0, 0, 0, "whatever", 0);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_has_been_evaluated()
        {
            _calibrationTest.EvaluateSample(_calibrationSample);

            Assert.IsTrue(_calibrationSample.IsEvaluationCompleted);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_correct_metadata_is_returned()
        {
            var expectedMetadata = new QcSampleEvaluationMetadata(_calibrationSample.InternalId, QcSampleEvaluationResult.FailedButContinue, "Found peak is outside of expected range in channels (Low: " + _calibrationSample.StartChannel + ").");
            var observedMetdata = _calibrationTest.EvaluateSample(_calibrationSample);

            Assert.AreEqual(expectedMetadata, observedMetdata);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_error_details_are_correct()
        {
            _calibrationTest.EvaluateSample(_calibrationSample);

            Assert.AreEqual("Found peak is outside of expected range in channels (Low: " + _calibrationSample.StartChannel + ").", _calibrationSample.ErrorDetails);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_is_marked_as_failed()
        {
            _calibrationTest.EvaluateSample(_calibrationSample);

            Assert.IsTrue(_calibrationSample.IsPassed == false);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_metadata_is_added()
        {
            var expectedMetadata = new QcSampleEvaluationMetadata(_calibrationSample.InternalId, QcSampleEvaluationResult.FailedButContinue, "Found peak is outside of expected range in channels (Low: " + _calibrationSample.StartChannel + ").");
            _calibrationTest.EvaluateSample(_calibrationSample);

            CollectionAssert.AreEqual(new List<QcSampleEvaluationMetadata> { expectedMetadata }, _calibrationTest.EvaluationResults.ToList());
        }

        private static IPeakFinder GeneratePeakFinderWithCentroidBelowMinimum()
        {
            var stubPeakFinder = MockRepository.GenerateStub<IPeakFinder>();
            stubPeakFinder.Expect(f => f.FindPeaks(new uint[] { }, 0))
                 .IgnoreArguments()
                 .Return(new List<Peak> { new Peak { Centroid = _startChannel - 1 } });
            return stubPeakFinder;
        }
    }
    // ReSharper restore InconsistentNaming
}
