﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Interfaces.Tests.BVATestItem_Tests
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_using_basic_properties
	{
		private BVATestItem _bvaTestItem;

		private BVATest _test;

		[TestInitialize]
		public void Init()
		{
			_test = BvaTestFactory.CreateBvaTest(5, 60);

			_bvaTestItem = new BVATestItem(_test);
		}

		[TestMethod]
		public void Then_the_amputee_status_can_be_set()
		{
			Assert.IsFalse(_bvaTestItem.AmputeeStatus);

			_bvaTestItem.AmputeeStatus = true;

			Assert.IsTrue(_bvaTestItem.AmputeeStatus);
		}

		[TestMethod]
		public void Then_setting_the_amputee_status_fires_PropertyChanged()
		{
			var propertyChangedWasFired = false;
			_bvaTestItem.PropertyChanged += (sender, args) => { propertyChangedWasFired = args.PropertyName == "AmputeeStatus"; };
			_bvaTestItem.AmputeeStatus = true;
			Assert.IsTrue(propertyChangedWasFired, "Setting the amputee status did not fire the PropertyChanged event");
		}
	}
	// ReSharper restore InconsistentNaming
}