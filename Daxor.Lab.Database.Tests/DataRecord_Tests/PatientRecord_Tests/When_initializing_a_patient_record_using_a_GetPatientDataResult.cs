﻿using System;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.LINQ;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Database.Tests.DataRecord_Tests.PatientRecord_Tests
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_a_patient_record_using_a_GetPatientDataResult
	{
		private GetPatientDataResult _getPatientDataResult;

		[TestInitialize]
		public void Init()
		{
			_getPatientDataResult = new GetPatientDataResult();
		}

		[TestMethod]
		public void And_the_patient_is_an_amputee_then_the_record_says_the_patient_is_an_amputee()
		{
			//Arrange
			_getPatientDataResult.IS_AMPUTEE = true;

			//Act
			var record = new PatientRecord(_getPatientDataResult);

			//Assert
			Assert.IsTrue(record.IsAmputee);
		}

		[TestMethod]
		public void And_the_patient_is_not_an_amputee_then_the_record_says_the_patient_is_not_an_amputee()
		{
			//Arrange
			_getPatientDataResult.IS_AMPUTEE = false;

			//Act
			var record = new PatientRecord(_getPatientDataResult);

			//Assert
			Assert.IsFalse(record.IsAmputee);
		}
	}
	// ReSharper restore InconsistentNaming
}