﻿using System;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Database.Tests.DataRecord_Tests.AuditTrailRecord_Tests.SadPath
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class When_converting_values_to_the_current_context
    {
        private StubAuditTrailRecordThatExposesConvertToCurrentContext _auditTrailRecord;
        private const string InvalidMetricValueToRound = "a82.4722";
        private const string PatientWeightFieldIdentifier = "PATIENT_WEIGHT";
        private const string PatientHeightFieldIdentifier = "PATIENT_HEIGHT";
        private const string MetricMeasurementSystemIdentifier = "METRIC";
        private const string UsMeasurementSystemIdentifier = "US";

        [TestInitialize]
        public void TestInitialize()
        {
            _auditTrailRecord = new StubAuditTrailRecordThatExposesConvertToCurrentContext
            {
                ChangedBy = "Somebody",
                ChangeTime = DateTime.Now.ToString(CultureInfo.InvariantCulture)
            };
        }

        [TestMethod]
        public void And_the_measurement_system_is_metric_and_the_value_for_weight_is_invalid_then_the_empty_string_is_returned()
        {
            _auditTrailRecord.BvaTestMeasurementSystem = MetricMeasurementSystemIdentifier;

            Assert.AreEqual(String.Empty, _auditTrailRecord.ConvertValueToCurrentContextPassthrough(InvalidMetricValueToRound, Guid.NewGuid(), PatientWeightFieldIdentifier));
        }

        [TestMethod]
        public void And_the_measurement_system_is_metric_and_the_value_for_height_is_invalid_then_the_empty_string_is_returned()
        {
            _auditTrailRecord.BvaTestMeasurementSystem = MetricMeasurementSystemIdentifier;

            Assert.AreEqual(String.Empty, _auditTrailRecord.ConvertValueToCurrentContextPassthrough(InvalidMetricValueToRound, Guid.NewGuid(), PatientHeightFieldIdentifier));
        }

        [TestMethod]
        public void And_the_measurement_system_is_US_and_the_value_for_weight_is_invalid_then_the_empty_string_is_returned()
        {
            _auditTrailRecord.BvaTestMeasurementSystem = UsMeasurementSystemIdentifier;

            Assert.AreEqual(String.Empty, _auditTrailRecord.ConvertValueToCurrentContextPassthrough(InvalidMetricValueToRound, Guid.NewGuid(), PatientWeightFieldIdentifier));
        }

        [TestMethod]
        public void And_the_measurement_system_is_US_and_the_value_for_height_is_invalid_then_the_empty_string_is_returned()
        {
            _auditTrailRecord.BvaTestMeasurementSystem = UsMeasurementSystemIdentifier;

            Assert.AreEqual(String.Empty, _auditTrailRecord.ConvertValueToCurrentContextPassthrough(InvalidMetricValueToRound, Guid.NewGuid(), PatientHeightFieldIdentifier));
        }
    }
}
