﻿using System;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.Tests.DataRecord_Tests.AuditTrailRecord_Tests
{
    public class StubAuditTrailRecordThatExposesConvertToCurrentContext: AuditTrailRecord
    {
        public string ConvertValueToCurrentContextPassthrough(string value, Guid testId, string fieldName)
        {
            return ConvertValueToCurrentContext(value,testId,fieldName);
        }
    }
}
