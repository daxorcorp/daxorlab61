﻿using System;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Database.Tests.DataRecord_Tests.AuditTrailRecord_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_values_to_the_current_context
    {
		private StubAuditTrailRecordThatExposesConvertToCurrentContext _auditTrailRecord;
	    private const string AmputeeStatusIdentifier = "IS_AMPUTEE";
	    private const string AmputeeIdealsCorrectionFactorIdentifier = "AMPUTEE_IDEALS_CORRECTION_FACTOR";
		private const string MetricMeasurementSystemIdentifier = "METRIC";
        private const string MetricValueToRound = "82.4722";
        private const string PatientWeightFieldIdentifier = "PATIENT_WEIGHT";
        private const string PatientHeightFieldIdentifier = "PATIENT_HEIGHT";
        private const string UsMeasurementSystemIdentifier = "US";

        [TestInitialize]
        public void TestInitialize()
        {
            _auditTrailRecord = new StubAuditTrailRecordThatExposesConvertToCurrentContext
            {
                ChangedBy = "Somebody",
                ChangeTime = DateTime.Now.ToString(CultureInfo.InvariantCulture)
            };
        }

        [TestMethod]
        public void And_the_measurement_system_is_metric_then_the_weight_in_the_audit_trail_is_rounded_correctly()
        {
            _auditTrailRecord.BvaTestMeasurementSystem = MetricMeasurementSystemIdentifier;

            Assert.AreEqual("82.47 kg", _auditTrailRecord.ConvertValueToCurrentContextPassthrough(MetricValueToRound, Guid.NewGuid(),PatientWeightFieldIdentifier));
        }

        [TestMethod]
        public void And_the_measurement_system_is_metric_then_the_height_in_the_audit_trail_is_rounded_correctly()
        {
            _auditTrailRecord.BvaTestMeasurementSystem = MetricMeasurementSystemIdentifier;

            Assert.AreEqual("82.47 cm", _auditTrailRecord.ConvertValueToCurrentContextPassthrough(MetricValueToRound, Guid.NewGuid(), PatientHeightFieldIdentifier));
        }

        [TestMethod]
        public void And_the_measurement_system_is_US_then_the_weight_in_the_audit_trail_is_rounded_correctly()
        {
            _auditTrailRecord.BvaTestMeasurementSystem = UsMeasurementSystemIdentifier;

            Assert.AreEqual("181.82 lb", _auditTrailRecord.ConvertValueToCurrentContextPassthrough(MetricValueToRound, Guid.NewGuid(), PatientWeightFieldIdentifier));
        }

        [TestMethod]
        public void And_the_measurement_system_is_US_then_the_height_in_the_audit_trail_is_rounded_correctly()
        {
            _auditTrailRecord.BvaTestMeasurementSystem = UsMeasurementSystemIdentifier;

            Assert.AreEqual("32.47 in", _auditTrailRecord.ConvertValueToCurrentContextPassthrough(MetricValueToRound, Guid.NewGuid(), PatientHeightFieldIdentifier));
        }

		[TestMethod]
		public void And_the_amputee_status_is_true_then_amputee_is_returned()
		{
			Assert.AreEqual("Amputee", _auditTrailRecord.ConvertValueToCurrentContextPassthrough("1", Guid.NewGuid(), AmputeeStatusIdentifier));
		}

		[TestMethod]
		public void And_the_amputee_status_is_false_then_non_amputee_is_returned()
		{
			Assert.AreEqual("Non-amputee", _auditTrailRecord.ConvertValueToCurrentContextPassthrough("0", Guid.NewGuid(), AmputeeStatusIdentifier));
		}

		[TestMethod]
		public void And_the_amputee_ideals_correction_factor_is_0_then_0_percent_is_returned()
		{
			Assert.AreEqual("0%", _auditTrailRecord.ConvertValueToCurrentContextPassthrough("0", Guid.NewGuid(), AmputeeIdealsCorrectionFactorIdentifier));
		}

		[TestMethod]
		public void And_the_amputee_ideals_correction_factor_is_5_then_negative_5_percent_is_returned()
		{
			Assert.AreEqual("-5%", _auditTrailRecord.ConvertValueToCurrentContextPassthrough("5", Guid.NewGuid(), AmputeeIdealsCorrectionFactorIdentifier));
		}
    }
    // ReSharper restore InconsistentNaming
}
