﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Ionic.Zip;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.SqlServer.Management.Smo;

namespace Daxor.Lab.Shell.ViewModels
{
    /// <summary>
    /// Takes care of creating the automatic backup and purging the old backups
    /// </summary>
    public class AutomaticBackupViewModel : INotifyPropertyChanged
    {
        #region Constants
        private const string MessageBoxCaption = "Automatic Backup";
        private const string CancelAutomaticBackupMessage = "If Automatic Backup is canceled, a new automatic backup will be created at the next regularly scheduled time.\n\nDo you want to cancel this backup?";

        #endregion

        #region Fields
        private delegate void CloseApplication();
        private readonly ILoggerFacade _logger;
        private readonly AutoResetEvent _displayMessageEvent = new AutoResetEvent(false);
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly ISettingsManager _settingsManager;
        private readonly IBackupServiceFactory _serviceFactory;
        private readonly IMessageManager _messageManager;
        private readonly IZipFileFactory _zipFileFactory;

        private readonly string _customDirectory;
        private readonly string _defaultDirectory;
        private string _directoryUsed;
        private bool _backgroundIsBusy;
        private bool _canceledClicked;
        private bool _useIdealDispatcher = true;

        #endregion

        #region Ctor
        public AutomaticBackupViewModel(ILoggerFacade customLoggerFacade, 
                                        IMessageBoxDispatcher messageBoxDispatcher, 
                                        ISettingsManager settingsManager, 
                                        IBackupServiceFactory serviceFactory, 
                                        IMessageManager messageManager,
                                        IZipFileFactory zipFileFactory)
        {
             //InitializeBackupWorker();
            
            _cancelCommand = new DelegateCommand<object>(CancelCommandExecute, CancelCommandCanExecute);
            _logger = customLoggerFacade;
            _settingsManager = settingsManager;
            _serviceFactory = serviceFactory;
            _messageManager = messageManager;
            _zipFileFactory = zipFileFactory;
            PropertyChanged += OnPropertyChanged;
            
            _messageBoxDispatcher = messageBoxDispatcher;
            _canceledClicked = false;
            _customDirectory = settingsManager.GetSetting<string>(SettingKeys.SystemAutomaticBackupLocation);
            _defaultDirectory = settingsManager.GetSetting<string>(SettingKeys.SystemDefaultAutomaticBackupLocation);
        }

        #endregion

        #region Properties

        private int _operationProgress;
        public int OperationProgress
        {
            get
            {
                return _operationProgress;
            }
            set
            {
                _operationProgress = value;
                FirePropertyChanged("OperationProgress");
            }
        }

        private readonly DelegateCommand<object> _cancelCommand;
        public ICommand CancelCommand
        {
            get { return _cancelCommand; }
        }

        private string _description;
        public String Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                FirePropertyChanged("Description");
            }
        }

        private TaskScheduler _taskScheduler;
        public TaskScheduler TaskScheduler
        {
            get { return _taskScheduler ?? (_taskScheduler = TaskScheduler.Default); }
            set
            {
                _taskScheduler = value;
                _useIdealDispatcher = false;
            }
        }

        #endregion

        #region Command Methods

        public void CancelCommandExecute(object o)
        {
            if (!_backgroundIsBusy)
            {
                CleanUpTempFiles();
                Close();
                return;
            }
            var choices = new[] {"Cancel This Backup", "Continue Backup"};
            var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Question, CancelAutomaticBackupMessage,
                "Automatic Backup", choices);

            if (choices[result] == "Continue Backup")
                return;

            _canceledClicked = true;
            ((DelegateCommand<object>) CancelCommand).RaiseCanExecuteChanged();
            Description = "Cleaning up temp files";
            OperationProgress = 0;
            CleanUpTempFiles();
        }

        private bool CancelCommandCanExecute(object param)
        {
            return _backgroundIsBusy && !_canceledClicked;
        }

        #endregion

        #region Public API
        /// <summary>
        /// Starts the backup process
        /// </summary>
        public void BeginBackup()
        {
            PerformBackup();
        }

        /// <summary>
        /// Removes all backups that are older than a set date
        /// </summary>
        protected virtual void PurgeOldBackups()
        {
            var oldDate = _settingsManager.GetSetting<int>(SettingKeys.SystemNumberOfBackupsToKeep);
            var files = SafeFileOperations.FileManager.GetFilesInDirectory(_directoryUsed, "*.zip");
            foreach (var file in files.Where(file => file.CreationTime <= DateTime.Now.AddDays(-oldDate)))
                SafeFileOperations.Delete(file.FullName);
        }

        private void PerformBackup()
        {
            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            var exportTask = new Task(() => BackupTask(tokenSource), token);

            // ReSharper disable once PossibleNullReferenceException
            exportTask.ContinueWith(result => OnBackupTaskFaulted(result.Exception.InnerException), token, TaskContinuationOptions.NotOnRanToCompletion, TaskScheduler);
            exportTask.ContinueWith(result => OnBackupTaskCompleted(), token,
                TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler);

            exportTask.Start(TaskScheduler);
        }

        private void Close()
        {
            _backgroundIsBusy = false;
            CloseApplication handler = CloseBackupWindow;
            if (_useIdealDispatcher)
                Application.Current.Dispatcher.Invoke(handler, DispatcherPriority.Normal, null);
        }

        private void OnBackupTaskCompleted()
        {
            _settingsManager.SetSetting(SettingKeys.SystemLastAutomatedBackup, DateTime.Now);
            CleanUpTempFiles();
            PurgeOldBackups();
            Close();
        }

        private void OnBackupTaskFaulted(Exception exception)
        {
            _logger.Log(exception.Message, Category.Exception, Priority.High);
            _backgroundIsBusy = false;

            CleanUpTempFiles();

            if (_useIdealDispatcher)
                IdealDispatcher.BeginInvoke(DisplayFaultedMessage);
            else
                DisplayFaultedMessage();
            
            _settingsManager.SetSetting(SettingKeys.SystemLastAutomatedBackup, DateTime.Now);
            Close();
        }

        protected virtual void BackupTask(CancellationTokenSource tokenSource)
        {
            _backgroundIsBusy = true;
            Description = "Backing up databases";

            var didDisplayMessage = false;
            _directoryUsed = _customDirectory;
            if (!SafeFileOperations.FileManager.DirectoryExists(_customDirectory))
            {
                didDisplayMessage = true;
                _directoryUsed = _settingsManager.GetSetting<string>(SettingKeys.SystemDefaultAutomaticBackupLocation);
                if (!SafeFileOperations.FileManager.DirectoryExists(_defaultDirectory))
                {
                    DisplayDefaultLocationNotfoundMessage();
                    _displayMessageEvent.WaitOne();
                    Close();
                    tokenSource.Cancel();
                    return;
                }
                DisplayBackupLocationNotFoundMessage();
            }
            if (didDisplayMessage)
                _displayMessageEvent.WaitOne();

            var backup = _serviceFactory.CreateDaxorLabBackupService();
            backup.PercentCompleteHandler = delegate(object sender, PercentCompleteEventArgs args) { OperationProgress = args.Percent; };
            backup.Backup(_directoryUsed + "DaxorSuite.bak");
            
            CreateZipFile();
        }

        #endregion
        
        private void CreateZipFile()
        {
            if (_canceledClicked)
                return;

            OperationProgress = 0;
            Description = "Creating zip file";
            
            try
            {
                var zipFile = _zipFileFactory.CreateZipFile();
                var now = DateTime.Now;
                var zipFilePath = _directoryUsed + now.Month + "_" + now.Day + "_" + now.Year + "_DaxorSuite.zip";

                zipFile.AttachProgressHandlerToZip(zip_SaveProgress);
                zipFile.AddFileToDirectory(_directoryUsed + "DaxorSuite.bak", "Backups");
                zipFile.Save(zipFilePath);
            }
            catch (Exception ex)
            {
                var errorMessage = "An error occurred while creating the zip file.\nError Message: " + ex.Message;
                _logger.Log(errorMessage, Category.Exception, Priority.High);
                throw;
            }
        }

        #region ZipFile Methods
        private void zip_SaveProgress(object sender, SaveProgressEventArgs e)
        {
            if (_canceledClicked)
            {
                e.Cancel = true;
                return;
            }
            if (e.TotalBytesToTransfer == 0)
                OperationProgress = 0;

            else
            {
                var bytesTransferred = e.BytesTransferred;
                var totalToTransfer = e.TotalBytesToTransfer;
                OperationProgress = (int)((bytesTransferred / totalToTransfer) * 100);
                if (OperationProgress == 100)
                    Description = "Automatic Backup Completed";
            }
        }

        #endregion

        #region Helper Methods

        private void DisplayDefaultLocationNotfoundMessage()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysDefaultBackupLocationNotFound).FormattedMessage;
            message = String.Format(message, _customDirectory);

            if (_useIdealDispatcher)
                IdealDispatcher.BeginInvoke(() => ShowMessage(MessageBoxCategory.Error, message));
            else
                ShowMessage(MessageBoxCategory.Error, message);
        }

        /// <summary>
        /// This does not need to work with Ideal Dispatcher because this occurs on the UI thread
        /// </summary>
        private void DisplayFaultedMessage()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysAutomaticBackupFailure).FormattedMessage;
            _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, message, MessageBoxCaption, MessageBoxChoiceSet.Ok);
            _logger.Log(message, Category.Info, Priority.High);
            _displayMessageEvent.Set();
        }

        private void DisplayBackupLocationNotFoundMessage()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysAutomaticBackupLocationNotFound).FormattedMessage;
            message = String.Format(message, _customDirectory);
            if (_useIdealDispatcher)
                IdealDispatcher.BeginInvoke(() => ShowMessage(MessageBoxCategory.Warning, message));
            else
                ShowMessage(MessageBoxCategory.Warning, message);
        }

        private void ShowMessage(MessageBoxCategory category, string message)
        {
            _messageBoxDispatcher.ShowMessageBox(category, message, MessageBoxCaption, MessageBoxChoiceSet.Ok);
            
            var loggingCategory = category == MessageBoxCategory.Warning ? Category.Warn : Category.Exception;
            _logger.Log(message, loggingCategory, Priority.High);
            
            _displayMessageEvent.Set();
        }

        protected virtual void CleanUpTempFiles()
        {
            try
            {
                SafeFileOperations.Delete(_directoryUsed, "*.bak");
                SafeFileOperations.Delete(_directoryUsed, "*.tmp");
            }
            catch (Exception ex)
            {
                var errMessage = "Hit an exception while deleting the temporary files.\nError Message:" + ex.Message;
                _logger.Log(errMessage, Category.Exception, Priority.High);
            }
        }
        #endregion

        #region Delegate Methods
        
        private void CloseBackupWindow()
        {
            var autoBackupWindow = Application.Current.Windows.OfType<Window>().SingleOrDefault(w => w.Title == "Automatic Backup Prompt");
            if (autoBackupWindow != null)
                autoBackupWindow.Close();
        }

        #endregion

        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void FirePropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
        }
        #endregion
    }
}
