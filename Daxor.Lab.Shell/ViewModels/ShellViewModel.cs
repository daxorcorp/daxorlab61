﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.RuleFramework;
using Daxor.Lab.Infrastructure.Services;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Shell.Common;
using Daxor.Lab.Shell.Views;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;

namespace Daxor.Lab.Shell.ViewModels
{
    /// <summary>
    /// The viewModel for the shell
    /// </summary>
    public class ShellViewModel : ViewModelBase
    {
        #region Fields

        private BusyPayload _bContent;
        private readonly ILoggerFacade _customLoggerFacade;
        private readonly IMessageBoxDispatcher _msgDispatcher;
        private readonly ISettingsManager _settingsManager;
        private readonly IMessageManager _messageManager;
        private bool _isBusy;
        private bool _isBackingUpNow;
        private bool _haveShownMovementMessage;
        private readonly System.Timers.Timer _timer;
        public delegate void DelFunction();
        private readonly IBackupServiceFactory _serviceFactory;
        private readonly IZipFileFactory _zipFileFactory;
        
        #endregion

        #region Ctor

        public ShellViewModel(IEventAggregator eventAggregator, 
                              ILoggerFacade customLoggerFacade, 
                              IMessageBoxDispatcher msgDispatcher, 
                              ISettingsManager settingsManager, 
                              IBackupServiceFactory serviceFactory, 
                              IMessageManager messageManager,
                              IZipFileFactory zipFileFactory)
        {
            _customLoggerFacade = customLoggerFacade;
            _msgDispatcher = msgDispatcher;
            _settingsManager = settingsManager;
            _serviceFactory = serviceFactory;
            _messageManager = messageManager;
            _zipFileFactory = zipFileFactory;
            ShowCurrentAlertCommand = new DelegateCommand<Object>(ExecuteShowCurrentAlert);
            _customLoggerFacade.Log(String.Format("{0} {1:g}", "***************ShellViewModel created***************", DateTime.Now), Category.Info, Priority.None);

            eventAggregator.GetEvent<BusyStateChanged>().Subscribe(OnBusyStateChanged, false);
            eventAggregator.GetEvent<GlobalShutdownInitiated>().Subscribe(o=> _timer.Stop());
            eventAggregator.GetEvent<DetectorDisconnected>().Subscribe(OnDetectorDisconnected);
            eventAggregator.GetEvent<SampleChangerErrorOccurred>().Subscribe(OnSampleChangerErrorOccurred);

            _timer = new System.Timers.Timer(30 * 1000); // fire every 30 seconds
            _timer.Elapsed += OnTimerElapsed;
            _timer.Start();
            _isBackingUpNow = false;
            _haveShownMovementMessage = false;
        }
        
        #endregion

        #region Automatic Backup Methods

        public void ShowAutomaticBackup()
        {
#if !DEBUG
            var backupViewModel = new AutomaticBackupViewModel(_customLoggerFacade, _msgDispatcher, _settingsManager, _serviceFactory, _messageManager, _zipFileFactory);
            var autoBackup = new AutomaticBackup(backupViewModel);
            backupViewModel.BeginBackup();
            autoBackup.ShowDialog();
#endif
        }

        private void OnTimerElapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //To make sure that we can showing, the error, every 30 seconds, we'll check this flag
            if (_haveShownMovementMessage)
                _haveShownMovementMessage = false;
            
            // If we're already backing up, no need to stay in here
            if (_isBackingUpNow)
                return;

            // If we we have already ran one today, no need to run this again
            if (!(_settingsManager.GetSetting<DateTime>(SettingKeys.SystemLastAutomatedBackup).Date < DateTime.Now.Date))
                return;

            if (IsItTimeToRunAutoBackup(_settingsManager.GetSetting<DateTime>(SettingKeys.SystemScheduledAutomatedBackupTime)))
            {
                _isBackingUpNow = true;
                DelFunction handler = ShowAutomaticBackup;
                Application.Current.Dispatcher.Invoke(handler, DispatcherPriority.Normal, null);
                _isBackingUpNow = false;
            }
        }
        #endregion

        #region Properties

        public BusyPayload BusyContent
        {
            get { return _bContent; }
            set { SetValue(ref _bContent, "BusyContent", value); }
        }
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetValue(ref _isBusy, "IsBusy", value); }
        }

        #endregion

        #region Commands
        public ICommand ShowCurrentAlertCommand
        {
            get;
            private set;
        }

        private void ExecuteShowCurrentAlert(Object element)
        {
            var dObject = element as DependencyObject;
            if (dObject != null && ValidationHelper.GetHasAlertViolation(dObject))
            {
                var metadata = ValidationHelper.GetBrokenRuleMetadata(dObject);

                var err = new RuleViolationMetadata
                    {
                        VisualBrush = new System.Windows.Media.VisualBrush(((FrameworkElement)dObject)),
                        VisualHeight = ((FrameworkElement)dObject).ActualHeight,
                        VisualWidth = ((FrameworkElement)dObject).ActualWidth,
                    };
                var validationError = Validation.GetErrors(dObject).FirstOrDefault();
                if(validationError == null || validationError.ErrorContent == null)
                    return;

                err.Symptoms = validationError.ErrorContent.ToString();

                if (metadata != null)
                {
                    err.Resolution = metadata.Resolution + " [Alert: " + _messageManager.GetMessage(metadata.AlertId).MessageId + "]";
                    err.HelpType = metadata.ResolutionTypeAsEnum;
                }
                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Information, err, "Alert", MessageBoxChoiceSet.Close);
                _customLoggerFacade.Log(err.Symptoms + err.Resolution, Category.Info, Priority.Low);
            }
        }
        #endregion

        private void OnBusyStateChanged(BusyPayload bPayload)
        {
            //Ignore if busy payload was not provided
            if (bPayload == null)
                return;

            IsBusy = bPayload.IsBusy;
            if (IsBusy)
            {
                BusyContent = bPayload;
            }
        }

        #region Error Handling
        
        /// <summary>
        /// Called when the detector has been disconnected
        /// </summary>
        /// <param name="ignoredArgument">Argument is ignored</param>
        public void OnDetectorDisconnected(object ignoredArgument)
        {
            var messageCaption = GetMessageBoxInformation(MessageConstants.GammaCounterModule);
            var messageContent = _messageManager.GetMessage(MessageKeys.SysGammaCounterDisconnected).FormattedMessage;
            _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error, messageContent, messageCaption, new [] {MessageConstants.RebootChoice});

            _customLoggerFacade.Log(messageContent, Category.Info, Priority.Low);
            ShutdownAndReboot.RebootWindows();
        }

        /// <summary>
        /// Fired when an error occurred with the Sample Changer
        /// </summary>
        /// <param name="errorPayload"></param>
        public void OnSampleChangerErrorOccurred(object errorPayload)
        {
            var errorInfo = errorPayload as SampleChangerErrorOccurredPayload;
            if (errorInfo == null) return;

            if (errorInfo.Message.Contains("power"))
            {
                var messageCaption = GetMessageBoxInformation(MessageConstants.SampleChangerModule);
                var messageContent = _messageManager.GetMessage(MessageKeys.SysSampleChangerDisconnected).FormattedMessage;
                
                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error, messageContent, messageCaption, new[] { MessageConstants.RebootChoice });
                _customLoggerFacade.Log(messageContent, Category.Info, Priority.Low);
                ShutdownAndReboot.RebootWindows();
            }

            if (errorInfo.Message.Contains("Unexpected position change") && !_haveShownMovementMessage) 
            {
                const string msgTitle = "Unexpected Sample Changer Position Change";
                var msgDescription = _messageManager.GetMessage(MessageKeys.SysSampleChangerUnexpectedMovement).FormattedMessage;

                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error, msgDescription, msgTitle, MessageBoxChoiceSet.Close);
                _customLoggerFacade.Log(msgDescription, Category.Info, Priority.Low);
                _haveShownMovementMessage = true;
            }

            if (errorInfo.Message.Contains("Unable to start motor"))
            {
                const string msgTitle = "Stalled Sample Changer Motor";
                var msgDescription = _messageManager.GetMessage(MessageKeys.SysMotorStalled).FormattedMessage;
                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error, msgDescription, msgTitle, MessageBoxChoiceSet.Close);
                _customLoggerFacade.Log(msgDescription, Category.Info, Priority.Low);
            }
        }

        #endregion

        #region Helper Methods
        private static string GetMessageBoxInformation(string moduleName)
        {
            return String.Format("Loss of Connection With {0}", moduleName);
        }

        private bool IsItTimeToRunAutoBackup(DateTime autoBackupTime)
        {
            var now = DateTime.Now;

            if (now.TimeOfDay.Hours > autoBackupTime.TimeOfDay.Hours)
                return true;

            if (now.TimeOfDay.Hours < autoBackupTime.TimeOfDay.Hours)
                return false;

            if (now.TimeOfDay.Minutes >= autoBackupTime.TimeOfDay.Minutes)
                return true;

            return false;
        } 
        #endregion
    }
}
