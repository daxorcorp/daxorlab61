﻿using System;
using System.Threading;
using System.Windows;

namespace Daxor.Lab.Shell
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly Bootstrapper _bootStrapper;
        private readonly Mutex _synchronizationToken;
        private readonly bool _isApplicationRunning;

        public App()
        {
            _synchronizationToken = new Mutex(true, "{27423e3b-7f64-46f6-b716-e143a7aa6fff}");

            _isApplicationRunning = !_synchronizationToken.WaitOne(TimeSpan.Zero, true);

            if (_isApplicationRunning)
            {
                System.Windows.MessageBox.Show("The software is getting itself ready; please wait a moment for the main screen to appear.",
                    "DAXOR Blood Volume Analyzer", MessageBoxButton.OK, MessageBoxImage.Information);
                Current.Shutdown();
                return;
            }

            _bootStrapper = new Bootstrapper();
            InitializeComponent();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            if (_isApplicationRunning) return;

            base.OnStartup(e);

            ShutdownMode = ShutdownMode.OnMainWindowClose;
          
            Thread.Sleep(1000);
            _bootStrapper.Run();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            if (_isApplicationRunning) return;

            base.OnExit(e);
            
            if (_bootStrapper != null)
            {
                _bootStrapper.CleanUp(e);
            }

            _synchronizationToken.ReleaseMutex();
        }
    }
}
