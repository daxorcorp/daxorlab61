﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;
using System.Windows.Media.Animation;

namespace Daxor.Lab.Shell.Views
{
    /// <summary>
    /// Interaction logic for SplashView.xaml
    /// </summary>
    public partial class SplashView : Window
    {
        static SplashView spViewSingleton = null;
        static DispatcherTimer _timer = new DispatcherTimer();
        bool unloadStoryboardComplete;

        public SplashView()
        {
            InitializeComponent();
            Closing += SplashView_Closing;
        }

        void SplashView_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Storyboard closing = (Storyboard)TryFindResource("UnLoadSplash");
            if (!unloadStoryboardComplete && closing != null)
            {
                closing.Completed += new System.EventHandler(closing_Completed);
                closing.Begin();
                e.Cancel = true;
            }
        }
        void closing_Completed(object sender, System.EventArgs e)
        {
            unloadStoryboardComplete = true;
            Close();
        }

        public static void ShowAsync()
        {
            Thread thread = new Thread(() =>
            {
                spViewSingleton = new SplashView();
                spViewSingleton.Show();

                spViewSingleton.Closed += (sender, e) =>
                {
                    spViewSingleton.Dispatcher.InvokeShutdown();
                };

                System.Windows.Threading.Dispatcher.Run();
            });

            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }
        public static void CloseAsync(int timeout = 2)
        {
            CloseAsync(null, timeout);
        }
        public static void CloseAsync(Action callBack, int timeout = 2)
        {
            if (spViewSingleton == null)
                return;

            spViewSingleton.Dispatcher.Invoke((Action)delegate
            {
                //Provides extra 3 sec to close
                if (callBack != null)
                    spViewSingleton.Dispatcher.ShutdownFinished += (o, e) => callBack();

                if (_timer.IsEnabled)
                    _timer.Stop();
                
                _timer.Interval = new TimeSpan(0, 0, timeout);
                _timer.Tick += new EventHandler(_timer_Tick);
                _timer.Start();
            });
        }

        static void _timer_Tick(object sender, EventArgs e)
        {
            _timer.Tick -= _timer_Tick;
            _timer.Stop();

            if (spViewSingleton != null)
                spViewSingleton.Dispatcher.BeginInvoke((Action)delegate { spViewSingleton.Close(); });
        }
    }
}
