﻿using System.Windows;
using Daxor.Lab.Shell.ViewModels;

namespace Daxor.Lab.Shell.Views
{
    /// <summary>
    /// Interaction logic for AutomaticBackup.xaml
    /// </summary>
    public partial class AutomaticBackup : Window
    {
        public AutomaticBackup()
        {
            InitializeComponent();
        }
        public AutomaticBackup(AutomaticBackupViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }
    }
}
