﻿using System.Windows;
using Daxor.Lab.Infrastructure.Commands;
using Daxor.Lab.Shell.ViewModels;
using Daxor.Lab.Infrastructure.TouchClick;
using System.Windows.Media.Animation;
using System.Windows.Input;
using System;
using Daxor.Lab.Infrastructure.RuleFramework;
using System.Windows.Controls;
using System.Linq;

namespace Daxor.Lab.Shell.Views
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : Window
    {
        public ShellView()
        {
            InitializeComponent();
            Loaded += new RoutedEventHandler(ShellView_Loaded);
#if DEBUG
            //Remove once in release
            MouseRightButtonDown += new MouseButtonEventHandler(OnMouseDown);
            MouseMove += new MouseEventHandler(OnMouseMove);
#endif

        }

        //Window default location
        void ShellView_Loaded(object sender, RoutedEventArgs e)
        {
            WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
            Left = (SystemParameters.PrimaryScreenWidth / 2) - (Width / 2);
            Top = (SystemParameters.PrimaryScreenHeight / 2) - (Height / 2);

        }
        public ShellView(ShellViewModel vm) : this()
        {
            DataContext = vm;

        }

        private void OnWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (GlobalCommands.ShutdownCommand.CanExecute(e))
                GlobalCommands.ShutdownCommand.Execute(e);
        }

#if DEBUG

        //Remove once in release
        public Point LastPoint { get; set; }
        void OnMouseDown(object sender, EventArgs e)
        {
            MouseEventArgs args = (MouseEventArgs)e;
            if (args.RightButton == MouseButtonState.Pressed)
            {
                LastPoint = args.GetPosition(this);
            }
        }
        void OnMouseMove(object sender, EventArgs e)
        {
            MouseEventArgs args = (MouseEventArgs)e;
            if (args.RightButton == MouseButtonState.Pressed)
            {
                if (LastPoint.X >= 0 && LastPoint.Y >= 0)
                {

                    Top += (args.GetPosition(this).Y - LastPoint.Y);
                    Left += (args.GetPosition(this).X - LastPoint.X);
                }
            }
        }
#endif

    }
}
