﻿
namespace Daxor.Lab.Shell.Common
{
    public class MessageConstants
    {
        public const string SystemErrorCaption = "System Error";
        public const string GeneralErrorMessageContent = "A general error has occurred and the application will close.";
        public const string CloseApplicationChoice = "Close Application";
        public const string SqlExceptionMessageContent = "A connection to the database could not be established.";
        public const string ContactInformation = "\n\nIf the problem persists, please call DAXOR Customer Support at 1-888-774-3268.";
        public const string SqlExceptionError = " [Error: SYS 3]";
        public const string LossOfConnectionFormatString = "Loss of Connection with {0}";
        public const string GammaCounterModule = "Gamma Counter";
        public const string SampleChangerModule = "Sample Changer";
        public const string RebootChoice = "Reboot";
        public const string GeneralSystemError = " [Error: SYS 2]";
    }
}
