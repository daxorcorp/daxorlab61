﻿using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.DatabaseServices.Factories;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.BarCodeScanner;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Controls;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Factories;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.RegionAdapters;
using Daxor.Lab.Infrastructure.Services;
using Daxor.Lab.Infrastructure.SystemNotifications;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.Infrastructure.TouchClick;
using Daxor.Lab.Infrastructure.UserControls;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageBox;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Shell.Common;
using Daxor.Lab.Shell.Views;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Presentation.Regions;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Composite.UnityExtensions;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;

namespace Daxor.Lab.Shell
{
    /// <summary>
    /// Bootstrapper for Daxor.Labs
    /// 
    /// *** Bootstrapper steps ***
    /// 1. Create a LoggerFacade
    /// 2. Create and configure a module catalog
    /// 3. Create and configure the container
    /// 4. Configure default region adapter mappings
    /// 5. Configure default region behaviors
    /// 6. Register framework exception types
    /// 7. Create the shell
    /// 8. Initialize the shell
    /// 9. Initialize the modules
    /// </summary>
    internal class Bootstrapper : UnityBootstrapper
    {
        #region Events

        public event EventHandler LoadingCompleted;
        public event EventHandler LoadingFailed;
        
        #endregion

        //Step 1
        protected override void ConfigureContainer()
        {
            //Base registration will not override custom shared services
            base.ConfigureContainer();

            var assemblyLocation = Path.GetDirectoryName(Assembly.GetCallingAssembly().Location) ?? string.Empty;
            var configFilePath = Path.Combine(assemblyLocation, "DaxorLab.exe");

            var configuration = ConfigurationManager.OpenExeConfiguration(configFilePath);
            var section = (UnityConfigurationSection)configuration.GetSection("unity");

            section.Containers.Default.Configure(Container);

            //Registers core application services

            //Hook up to the unhandled exceptions
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
            
            RegisterCoreServices();

            var logger = Container.Resolve<ILoggerFacade>();
            var settingsManager = Container.Resolve<ISettingsManager>();

            logger.Log("***** Software version: " + settingsManager.GetSetting<string>(SettingKeys.BvaModuleVersion) + " *****",
                Category.Info, Priority.Low);

            //Instantiating ScannerListener as singleton. Does not expose any properties/methods; simply sends aggregate events
            //based on windows messages received from Motorola driver through Snapi.dll
            Container.RegisterInstance(typeof(ScannerListener), new ContainerControlledLifetimeManager());
        }
        //Step 2
        protected override RegionAdapterMappings ConfigureRegionAdapterMappings()
        {
            // Create default region adapters for ContentControl, Selector, ItemsControl
            RegionAdapterMappings mappings = base.ConfigureRegionAdapterMappings();

            mappings.RegisterMapping(typeof(ModuleSwitcher), Container.Resolve<ModuleSwitcherRegionAdapter>());
            mappings.RegisterMapping(typeof(ViewSwitcher), Container.Resolve<ViewSwitcherRegionAdapter>());
            mappings.RegisterMapping(typeof(Grid), Container.Resolve<GridRegionAdapter>());

            return mappings;
        }
        //Step 3
        protected override DependencyObject CreateShell()
        {
            //Register shared System notification bus; must be created before the shell is resolved
            Container.RegisterType<ISystemNotificationWatcher, SystemNotificationWatcher>(new ContainerControlledLifetimeManager());
            Container.Resolve<ISystemNotificationWatcher>();

            var shell = Container.Resolve<ShellView>();

            //Initializes custom dispatcher to use throughout the application
            IdealDispatcher.Initialize(shell.Dispatcher);

            shell.Show();
            Container.Resolve<ScannerListener>();

            return shell;
        }
        //Step 4
        protected override IModuleCatalog GetModuleCatalog()
        {
            return new ConfigurationModuleCatalog();
        }
        //Step 5
        protected override void InitializeModules()
        {
            try
            {
                base.InitializeModules();

                //Creating VirtualKeyboardManager(VKM). Earlier instantiation of VKM would fail as it has
                //a dependency on IRegionManager, and IRegionManager needs to set the base window first.

                var keyboardRegion = Container.Resolve<IRegionManager>().Regions[RegionNames.KeyboardRegion];
                var eventAgg = Container.Resolve<IEventAggregator>();

                VirtualKeyboardManager.Initialize(eventAgg, keyboardRegion);
                VirtualKeyboardManager.RegisterKeyboards(Container.Resolve<DefaultVirtualKeyboardCatalog>().GetVirtualKeyboards());

                FireLoadingCompleted();
            }
            catch (Exception ex)
            {
                var messageManager = Container.Resolve<IMessageManager>();
                Message errorAlert = messageManager.GetMessage(MessageKeys.SysModuleLoadFailed);
                string userFriendlyMessage = errorAlert.Description + "\n\n";

                userFriendlyMessage += GetGeneralModuleErrorMessage(ex);
                userFriendlyMessage += errorAlert.Resolution + " [Error: " +
                    errorAlert.MessageId + "]";

                //Log, hide loading screen, display to the user a message
                Container.Resolve<ILoggerFacade>().Log(ex.Message, Category.Exception, Priority.High);
                FireLoadingFailed();

                var msgBox = Container.Resolve<IMessageBoxDispatcher>();
                if (msgBox != null)
                {
                    msgBox.ShowMessageBox(MessageBoxCategory.Error, userFriendlyMessage, "Module Initialization Failure", new[] { "Close Application" });
                    var logger = Container.Resolve<ILoggerFacade>();
                    logger.Log(userFriendlyMessage, Category.Info, Priority.Low);
                }

                //Exits application, possible restart required
                Application.Current.Shutdown();
            }
        }

        private string GetGeneralModuleErrorMessage(Exception ex)
        {
            if (ex is ModuleNotFoundException)
                return "Could not find a module:\nMessage: "+ex.Message;
            if (ex is ModuleTypeLoaderNotFoundException)
                return "One or more modules could not be found.\nMessage: "+ex.Message;
            if (ex is ModuleTypeLoadingException)
                return "Could not load one or more modules\nMessage: " + ex.Message;
            if (ex is DuplicateModuleException)
				return "Tried to load duplicate modules\nMessage: " + ex.Message;
            if (ex is ModuleInitializeException)
                return GetDetailedModuleErrorMessage(ex);
            if (ex is ModularityException)
                return "There was a general problem with loading one or more modules\nMessage: " + ex.Message;

            return "";
        }

        private static string GetDetailedModuleErrorMessage(Exception ex)
        {
            if (ex.InnerException is SampleChangerEndPointNotFoundException)
                return "Unable to communicate with the sample changer.\n\n";
            if (ex.InnerException is SampleChangerCommunicationException)
                return "Unable to communicate with the sample changer.\n\n";
            if (ex.InnerException is GammaCounterEndPointNotFoundException)
                return "Unable to communicate with the gamma counter..\n\n";
            if (ex.InnerException is GammaCounterCommunicationException)
                return "Unable to communicate with the gamma counter.\n\n";
            if (ex.InnerException is GammaCounterNotConnectedException)
                return "Unable to communicate with the gamma counter.\n\n";
            if (ex.InnerException is SampleChangerNotPoweredUpException)
				return "The sample changer is not receiving power. Please make sure that the sample changer is plugged in and turned on.\n\n";
			// ReSharper disable once CanBeReplacedWithTryCastAndCheckForNull
			if (ex.InnerException is ModuleLoadException)
				return GetDetailedModuleLoadErrorMessage((ModuleLoadException) (ex.InnerException));
			
            return "";
        }

		public static string GetDetailedModuleLoadErrorMessage(ModuleLoadException exception)
		{
			return "The " + exception.ModuleName + " module was unable to " + exception.FailingAction + ".\n\n";
		}

	    //Step n
        public void CleanUp(EventArgs exitEventArgs)
        {
            //Can't do anything about it, can't find our hardware service.
            if (Container == null)
                return;

            var logger = Container.Resolve<ILoggerFacade>();

            //Trying to stop sample changer and disconnect from it
            try
            {
                var sampleChanger = Container.Resolve<ISampleChanger>();
                if (sampleChanger != null)
                {
                    var eventInfo = "Empty";
                    if (exitEventArgs is ExitEventArgs)
                        eventInfo =
                            (exitEventArgs as ExitEventArgs).ApplicationExitCode.ToString(CultureInfo.InvariantCulture);

                    logger.Log("Cleaning up in bootstrapper, exit event info is: " + eventInfo, Category.Info,
                        Priority.None);
                    sampleChanger.Stop();
                    sampleChanger.Disconnect();
                }
            }
            catch (Exception ex)
            {
                logger.Log("Bootstrapper.CleanUp() was unable to stop and disconnect from the sample changer: " + ex.Message, Category.Exception, Priority.High);
            }

            //Trying to stop mca and disconnect from it
            try
            {
                var detector = Container.Resolve<IDetector>();
                if (detector != null && detector.IsConnected)
                {
                    detector.StopAcquiring();
                    detector.Disconnect();
                }
            }
            catch (Exception ex)
            {
                logger.Log("Bootstrapper.CleanUp() was unable to stop and disconnect from the detector: " + ex.Message, Category.Exception, Priority.High);
            }
        }

        #region Protected

        protected virtual void FireLoadingCompleted()
        {
            var handler = LoadingCompleted;
            if (handler != null)
                handler(this, null);
        }
        protected virtual void FireLoadingFailed()
        {
            var handler = LoadingFailed;
            if (handler != null)
                handler(this, null);
        }

        #endregion

        #region Helpers

        private void RegisterCoreServices()
        {
            // Register logger
            Container.RegisterType<ILoggerFacade, CustomFileLogger>(new ContainerControlledLifetimeManager(),
                new InjectionConstructor(ConfigurationManager.AppSettings["LogPath"]));
            
            Container.RegisterType<IMessageBoxUiRegistrar, MessageBoxUIRegistrar>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IMessageBoxDispatcher, XamlMessageBoxDispatcher>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IStarBurnWrapper, StarBurnWrapper>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IFolderBrowser, FolderBrowser>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IDriveAccessorService, DriveAccessorService>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IFolderChooser, FolderChooser>(new ContainerControlledLifetimeManager());

            Container.RegisterType<ISettingsManagerDataService, SettingsManagerDataService>(new ContainerControlledLifetimeManager());
            Container.RegisterInstance(typeof (ISettingsManager), new SystemSettingsManager(Container.Resolve<ILoggerFacade>(),
                    Container.Resolve<ISettingsManagerDataService>()), new ContainerControlledLifetimeManager());

            Container.RegisterType<IBackupServiceFactory, BackupServiceFactory>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IRestoreServiceFactory, RestoreServiceFactory>(new ContainerControlledLifetimeManager());

            Container.RegisterType<IMessageRepository, DatabaseMessageRepository>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IMessageManager, SystemMessageManager>(new ContainerControlledLifetimeManager());

            Container.RegisterInstance(new Infrastructure.CalibrationFunction(1, 0));
            Container.RegisterType<IZipFileFactory, ZipFileFactory>(new ContainerControlledLifetimeManager());

            Container.RegisterType<IAppModuleInfoCatalog, AppModuleInfoCatalog>(new ContainerControlledLifetimeManager());

            Container.RegisterType<INavigationCoordinator, AppModuleNavigationCoordinator>(new ContainerControlledLifetimeManager());

            //Initialize sounds
            var settingsManager = Container.Resolve<ISettingsManager>();
            var touchWav = settingsManager.GetSetting<String>(SettingKeys.SystemClickSoundFilePath);
            var warningWav = settingsManager.GetSetting<String>(SettingKeys.SystemWarningSoundFilePath);

            TouchPlayer.InitializeSound(touchWav, Sound.Focus);
            TouchPlayer.InitializeSound(touchWav, Sound.Touch);
            TouchPlayer.InitializeSound(warningWav, Sound.Warning);

            TouchPlayer.IsEnabled = settingsManager.GetSetting<Boolean>(SettingKeys.SystemSoundEnabled);
            settingsManager.SettingsChanged += (o, e) =>
            {
                if (e.ChangedSettingIdentifiers.Contains(SettingKeys.SystemSoundEnabled))
                    TouchPlayer.IsEnabled = settingsManager.GetSetting<Boolean>(SettingKeys.SystemSoundEnabled);
            };
        }

        private void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var unhandledException = e.ExceptionObject as Exception;

            var messageCaption = MessageConstants.SystemErrorCaption;
            var messageContent = MessageConstants.GeneralErrorMessageContent;
            var choiceTexts = new[] { MessageConstants.CloseApplicationChoice };
            BuildMessageContent(e, ref messageContent, ref messageCaption, ref choiceTexts);

            var logger = Container.Resolve<ILoggerFacade>();

            RecursivelyLogExceptions(unhandledException, logger);
            logger.Log(messageContent, Category.Info, Priority.Low);

            InformUserOfUnhandledException(messageContent, messageCaption, choiceTexts, logger);
        }

        private static void RecursivelyLogExceptions(Exception unhandledException, ILoggerFacade logger)
        {
            if (unhandledException == null) return;

            logger.Log(
                "DaxorLab Unhandled Exception: " + unhandledException.Message + "Stack trace for exception: " +
                unhandledException.StackTrace, Category.Exception, Priority.High);
            
            while (unhandledException.InnerException != null)
            {
                logger.Log(
                    "DaxorLab Unhandled InnerException: " + unhandledException.InnerException.Message +
                    "Stack trace for inner exception: " + unhandledException.InnerException.StackTrace, Category.Exception,
                    Priority.High);
                unhandledException = unhandledException.InnerException;
            }
        }

        private void BuildMessageContent(UnhandledExceptionEventArgs e, ref string messageContent, 
                                           ref string messageCaption, ref string[] choiceTexts)
        {
            if (IsExceptionDatabaseRelated(e.ExceptionObject))
            {
                messageContent = MessageConstants.SqlExceptionMessageContent;
                messageContent = String.Concat(messageContent, MessageConstants.ContactInformation,
                                               MessageConstants.SqlExceptionError);
            }
            else if (e.ExceptionObject is GammaCounterNotConnectedException)
            {
                var messageManager = Container.Resolve<IMessageManager>();
                messageCaption = String.Format(MessageConstants.LossOfConnectionFormatString,
                                               MessageConstants.GammaCounterModule);
                messageContent = messageManager.GetMessage(MessageKeys.SysGammaCounterDisconnected).FormattedMessage;
                choiceTexts = new[] {MessageConstants.RebootChoice};
            }
            else
            {
                messageContent = String.Concat(messageContent, MessageConstants.ContactInformation,
                                               MessageConstants.GeneralSystemError);
            }
        }

        private void InformUserOfUnhandledException(string messageContent, string messageCaption, string[] choiceTexts, ILoggerFacade logger)
        {
            try
            {
                var messageBoxDispatcher = Container.Resolve<IMessageBoxDispatcher>();

                // If there is no message service then throw an ArgumentNullException to get into the catch;
                // This logs the issue and uses the system messageBox.
                if (messageBoxDispatcher == null) throw new NullReferenceException("A message box dispatcher is not available to display error messages; ");

                var choice = messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, messageContent, messageCaption,
                                                                 choiceTexts);

                if (choiceTexts[choice] == MessageConstants.RebootChoice)
                    ShutdownAndReboot.RebootWindows();
                else
                    Application.Current.Shutdown();
            }
            catch (Exception ex)
            {
                logger.Log("Unable to display messageBox; Using system messageBox instead; Exception: " +
                           ex.Message, Category.Info, Priority.Low);

                System.Windows.MessageBox.Show(messageContent, messageCaption, MessageBoxButton.OK,
                                               MessageBoxImage.Error);

                Application.Current.Shutdown();
            }
        }

        private bool IsExceptionDatabaseRelated(object exceptionObject)
        {
            if (!(exceptionObject is Exception)) return false;

            var unhandledException = exceptionObject as Exception;

            while (unhandledException != null)
            {
                if (unhandledException is System.Data.SqlClient.SqlException) return true;
                unhandledException = unhandledException.InnerException;
            }

            return false;
        }

        #endregion
    }
}
