﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Daxor.Lab.Footer.Common
{
    /// <summary>
    /// Interaction logic for DaxorMediaPlayer.xaml
    /// </summary>
    public partial class DaxorMediaPlayer : UserControl
    {
        public DaxorMediaPlayer()
        {
            InitializeComponent();
        }

        public MediaElement Player
        {
            get { return (MediaElement)GetValue(PlayerProperty); }
            set { SetValue(PlayerProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Player.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PlayerProperty =
            DependencyProperty.Register("Player", typeof(MediaElement), typeof(DaxorMediaPlayer), new PropertyMetadata(null, new PropertyChangedCallback(OnMediaPlayerChanged)));

        static void OnMediaPlayerChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            if (args.OldValue == null && args.NewValue != null)
            {
                DaxorMediaPlayer p = o as DaxorMediaPlayer;
                p.InitializePropertyValues();
            }
        }

        // Play the media.
        void OnMouseDownPlayMedia(object sender, MouseButtonEventArgs args)
        {
            if (!PlayerExists()) return;
            // The Play method will begin the media if it is not currently active or 
            // resume media if it is paused. This has no effect if the media is
            // already running.
            Player.Play();

            // Initialize the MediaElement property values.
            InitializePropertyValues();

        }

        // Pause the media.
        void OnMouseDownPauseMedia(object sender, MouseButtonEventArgs args)
        {
            if (!PlayerExists()) return;
            // The Pause method pauses the media if it is currently running.
            // The Play method can be used to resume.
            Player.Pause();

        }

        // Stop the media.
        void OnMouseDownStopMedia(object sender, MouseButtonEventArgs args)
        {
            if (!PlayerExists()) return;
            // The Stop method stops and resets the media to be played from
            // the beginning.
            Player.Stop();

        }

        // Change the volume of the media.
        private void ChangeMediaVolume(object sender, RoutedPropertyChangedEventArgs<double> args)
        {
            if (!PlayerExists()) return;
            Player.Volume = (double)volumeSlider.Value;
        }

      
        // When the media playback is finished. Stop() the media to seek to media start.
        private void Element_MediaEnded(object sender, EventArgs e)
        {
            if (!PlayerExists()) return;
            Player.Stop();
        }

      
        protected void InitializePropertyValues()
        {
            if (!PlayerExists()) return;

            // Set the media's starting Volume and SpeedRatio to the current value of the
            // their respective slider controls.
            Player.Volume = (double)volumeSlider.Value;
        }

        private void PART_MedialElement_Unloaded(object sender, RoutedEventArgs e)
        {
            if (!PlayerExists()) return;
            Player.Stop();
        }

        bool PlayerExists()
        {
            return !(Player == null);
        }

        private void OnMouseDownStopMedia_Click(object sender, RoutedEventArgs e)
        {
            if (!PlayerExists()) return;
            // The Stop method stops and resets the media to be played from
            // the beginning.
            Player.Stop();

        }
        private void OnMouseDownPlayMedia_Click(object sender, RoutedEventArgs e)
        {
            if (!PlayerExists()) return;
            // The Stop method stops and resets the media to be played from
            // the beginning.
            Player.Play();

        }
        private void OnMouseDownPauseMedia_Click(object sender, RoutedEventArgs e)
        {
            if (!PlayerExists()) return;
            // The Stop method stops and resets the media to be played from
            // the beginning.
            Player.Pause();

        }
    }
}
