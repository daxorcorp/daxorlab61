﻿using System;
using System.Diagnostics;
using System.Windows;
using Daxor.Lab.Footer.Views;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Exceptions;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.Footer
{
    public class Module : IModule
    {
	    private const string ModuleName = "Footer";

        private readonly IRegionManager _regionManager;
        private readonly IUnityContainer _container;

        public Module(IRegionManager regionManager, IUnityContainer container)
        {
            _regionManager = regionManager;
            _container = container;
        }

        public void Initialize()
        {
            Debug.Print("Initializing FooterModule...");
     
            //Initialize local resource dictionary
            try
            {
                AddResourceDictionaryToMergedDictionaries();
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "add the resource dictionary");
            }

            var footerView = ResolveFooterView();
            
	        _regionManager.Regions[RegionNames.FooterRegion].Add(footerView);
            _regionManager.Regions[RegionNames.FooterRegion].Activate(footerView);
        }

        protected virtual void AddResourceDictionaryToMergedDictionaries()
	    {
	        Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary
	            { Source = new Uri(@"pack://application:,,,/Daxor.Lab.Footer;component/Common/AlertDataTemplates.xaml") });
	    }

        private FooterView ResolveFooterView()
        {
            FooterView view;
            try
            {
                view = _container.Resolve<FooterView>();
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the Footer view");
            }
            return view;
        }
    }
}
