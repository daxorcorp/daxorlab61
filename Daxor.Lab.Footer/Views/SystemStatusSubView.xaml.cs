﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Daxor.Lab.Footer.ViewModels;
using System.Diagnostics;
using System.ComponentModel;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Utilities;

namespace Daxor.Lab.Footer.Views
{
    /// <summary>
    /// Interaction logic for SystemStatusView.xaml
    /// </summary>
    public partial class SystemStatusSubView : UserControl
    {
        private PropertyObserver<SystemStatusSubViewModel> sysObserver;

        public SystemStatusSubView()
        {
            DataContextChanged += new DependencyPropertyChangedEventHandler(SystemStatusSubView_DataContextChanged);

            InitializeComponent();
            VisualStateManager.GoToState(this, "MovingToBkg", false);
        }

        void SystemStatusSubView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            SystemStatusSubViewModel vm = null;

            if (e.NewValue != null)
                vm = e.NewValue as SystemStatusSubViewModel;

            if (vm != null)
            {
                sysObserver = new PropertyObserver<SystemStatusSubViewModel>(vm);
                sysObserver.RegisterHandler((m) => m.CurrentSystemMode, SyncUIWithSystemMode);

                SyncUIWithSystemMode(vm);
            }
            else
            {
                if (sysObserver != null)
                {
                    sysObserver.UnregisterHandler((m) => m.CurrentSystemMode);
                    sysObserver = null;
                }
            }
        }

        void SyncUIWithSystemMode(SystemStatusSubViewModel vm)
        {
            switch (vm.CurrentSystemMode)
            {
                case SystemStatusSubViewModel.SysMode.SeekingBackground:
                    VisualStateManager.GoToState(this, "MovingToBkg", true);
                    break;

                case SystemStatusSubViewModel.SysMode.CountingBkgWithoutPreviousBkg:
                    VisualStateManager.GoToState(this, "BkgAcquisitionWithoutPrevBkg", true);
                    break;

                case SystemStatusSubViewModel.SysMode.CountingBkgWithPreviousBkg:
                    VisualStateManager.GoToState(this, "BkgAcquisitionWithPrevBkg", true);
                    break;

                case SystemStatusSubViewModel.SysMode.PerformingTestMovingToSample:
                case SystemStatusSubViewModel.SysMode.PerformingTestCountingSample:
                    VisualStateManager.GoToState(this, "PerformingTestCountingSample", true);
                    break;

                case SystemStatusSubViewModel.SysMode.PerformingTestMovingWithNavigation:
                case SystemStatusSubViewModel.SysMode.PerformingTestCountingWithNavigation:
                    VisualStateManager.GoToState(this, "PerformingTestWithNavigation", true);
                    break;

                case SystemStatusSubViewModel.SysMode.RestartBackgroundManual:
                    VisualStateManager.GoToState(this, "RestartBackground", true);
                    break;

                default:
                    VisualStateManager.GoToState(this, "MovingToBkg", true);
                    break;
            }
        }
    }
}
