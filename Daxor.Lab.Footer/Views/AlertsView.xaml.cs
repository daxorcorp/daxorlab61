﻿using System.Windows.Controls;
using Daxor.Lab.Footer.ViewModels;

namespace Daxor.Lab.Footer.Views
{
    /// <summary>
    /// Interaction logic for AlertsView.xaml
    /// </summary>
    public partial class AlertsView : UserControl
    {
        public AlertsView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handles the situation where the user selects the alert (even if the selected index 
        /// is the same) and passes the necessary information to the command on the view model.
        /// </summary>
        /// <remarks>
        /// (1) This is necessary because using an interaction trigger in the XAML (for the
        /// PreviewMouseButtonDown event) only works the first time because the selection
        /// index is forced to zero. When the event is recalled and then reinserted, the
        /// selected index is not zero, meaning that CanExecute is passed a null reference.
        /// (2) The selected index is set to -1 after this method completes so that multiple
        /// clicks are allowed. (Recall that SelectionChanged only fires when there is a 
        /// change, not when the user clicks on a list member.)</remarks>
        /// <param name="sender">ListBox whose selection has changed</param>
        /// <param name="e">Arguments for the event</param>
        private void AppAlerts_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var alertsViewModel = DataContext as AlertsViewModel;
            var senderAsListBox = sender as ListBox;
            if (alertsViewModel == null || senderAsListBox == null) return;
            if (!alertsViewModel.NavigateToQcModuleCommand.CanExecute(senderAsListBox.SelectedItem)) return;

            alertsViewModel.NavigateToQcModuleCommand.Execute(senderAsListBox.SelectedItem);
            senderAsListBox.SelectedIndex = -1;
        }
    }
}
