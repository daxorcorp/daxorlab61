﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Daxor.Lab.Footer.ViewModels;

namespace Daxor.Lab.Footer.Views
{
    /// <summary>
    /// Interaction logic for FooterView.xaml
    /// </summary>
    public partial class FooterView : UserControl
    {
        private static DispatcherTimer _timer;

        public FooterView()
        {
            InitializeComponent();
            SyncTimes();
            InitTimer();
        }
        public FooterView(FooterViewModel vm)
            : this()
        {
            DataContext = vm;
        }

      
        #region TimeSync 
        /// <summary>
        /// No unit testing necessary, simple time keeping.
        /// </summary>
        /// 
        private void InitTimer()
        {
            if (_timer != null) return;

            _timer = new DispatcherTimer(new TimeSpan(0, 0, 1),
                DispatcherPriority.Background,
                new EventHandler((s, e)=>SyncTimes()),
                Application.Current.Dispatcher);
        }

        private void SyncTimes()
        {
            DateTime time = DateTime.Now;
            txtTime.Text = String.Format("{0:t}", time).TrimEnd(toRemove);
            txtMeridiem.Text = String.Format("{0:tt}", time);
            txtDate.Text = String.Format("{0:M/d/yy}", time);
        }

        private char[] toRemove = new char[] {' ', 'A', 'P', 'M'};
        #endregion
    }
}
