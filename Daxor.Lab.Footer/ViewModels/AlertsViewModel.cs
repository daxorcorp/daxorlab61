﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Input;
using Daxor.Lab.Footer.Models;
using Daxor.Lab.Infrastructure.Alerts;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.SystemNotifications;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;

namespace Daxor.Lab.Footer.ViewModels
{
    /// <summary>
    /// Alerts view model
    /// </summary>
    public class AlertsViewModel : ViewModelBase
    {
        #region Constants

        private const string SettingsChangeFormatString = "The demo mode setting{0} for the {1} changed. The application will need to be closed and restarted for the change{0} to take effect.\n\nDo you want to close the application now?\n\n(Note: If you choose not to restart, no future prompts will occur for changes in {2} until the application is restarted.)";

        #endregion

        #region Fields

        private readonly IEventAggregator _eventAggregator;
        private readonly ThreadSafeObservableCollection<Alert> _alerts = new ThreadSafeObservableCollection<Alert>();
        private readonly ISystemNotificationWatcher _systemNotificationWatcher;
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly DelegateCommand<object> _navigateToQcModuleDelegateCommand;
        readonly INavigationCoordinator _navCoordinator;
        private readonly SettingObserver<ISettingsManager> _sObserver;
        private readonly ILoggerFacade _logger;
        private readonly ISettingsManager _settingsManager;
        private bool _hasBeenAlertedAboutDetectorDemoModeChange;
        private bool _hasBeenAlertedAboutSampleChangerDemoModeChange;
        private bool _isDetectorInDemoMode;
        private bool _isSampleChangerInDemoMode;
        private bool _isAlertListExpanded;

        protected bool HasBeenAlertedAboutAlertsOnStartup;
        protected bool HasBeenAlertedAboutFullQCAlerts;
        protected bool HasBeenAlertedAboutContaminationAlerts;
        protected bool HasBeenAlertedAboutStandardsAlerts;
        protected bool HasBeenAlertedAboutLinearityAlerts;
        #endregion

        #region Properties

        public bool IsAlertCollectionExpanded 
        {
            get
            {
                return _isAlertListExpanded;
            }
            set
            {
                _isAlertListExpanded = value;
                FirePropertyChanged("IsAlertCollectionExpanded");
            }
        }

        public ThreadSafeObservableCollection<Alert> Alerts
        {
            get { return _alerts; }
        }

        public bool IsAlertCollectionEmpty
        {
            get
            {
                return Alerts.Count == 0;
            }
        }

        protected Guid QCDueAlertId { get; private set; }

        #endregion

        #region Ctor

        public AlertsViewModel(IEventAggregator eventAggregator, INavigationCoordinator navCoordinator, 
                               ILoggerFacade logger, ISettingsManager settingsManager, IMessageBoxDispatcher messageBoxDispatcher,
                               ISystemNotificationWatcher systemNotificationWatcher)
        {
            _eventAggregator = eventAggregator;
            _navCoordinator = navCoordinator;
            _logger = logger;
            _settingsManager = settingsManager;
            _sObserver = new SettingObserver<ISettingsManager>(_settingsManager);

            _navigateToQcModuleDelegateCommand = new DelegateCommand<object>(NavigateToQcModuleExecute, NavigateToQcModuleCanExecute);

            _isDetectorInDemoMode = _settingsManager.GetSetting<bool>(SettingKeys.SystemDetectorDemoModeEnabled);
            _isSampleChangerInDemoMode = _settingsManager.GetSetting<bool>(SettingKeys.SystemSampleChangerDemoModeEnabled);
            Alerts.CollectionChanged += (s, arg) => FirePropertyChanged("IsAlertCollectionEmpty");

            _eventAggregator.GetEvent<AlertViolationChanged>().Subscribe(ProcessAlertEvent, false);

            _messageBoxDispatcher = messageBoxDispatcher;
            _systemNotificationWatcher = systemNotificationWatcher;
            _systemNotificationWatcher.SystemNotificationChanged += OnSystemNotificationChanged;

            QCDueAlertId = Guid.NewGuid();

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            DealWithExistingAlerts();
            InitializeSettingObserver();
            // ReSharper restore DoNotCallOverridableMethodsInConstructor

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            FirePropertyChanged("Alerts", "IsAlertCollectionEmpty");
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Navigate-to-QC command

        public ICommand NavigateToQcModuleCommand
        {
            get { return _navigateToQcModuleDelegateCommand; }
        }

        void NavigateToQcModuleExecute(object arg)
        {
            _navCoordinator.ActivateAppModule(AppModuleIds.QualityControl, null, _navCoordinator.ActiveAppModuleKey);
        }

        bool NavigateToQcModuleCanExecute(object alertObject)
        {
            var triggeringAlert = (Alert) alertObject;
            if (triggeringAlert == null) return false;

            return !IsAlertCollectionEmpty && (triggeringAlert.AlertMessageBody == AlertResolutionPayload.FullQCAlert ||
                triggeringAlert.AlertMessageBody == AlertResolutionPayload.ContaminationQCAlert ||
                triggeringAlert.AlertMessageBody == AlertResolutionPayload.LinearityQCAlert ||
                triggeringAlert.AlertMessageBody == AlertResolutionPayload.StandardsQCAlert);
        }

        #endregion

        #region Aggregate Event handlers

        private void ProcessAlertEvent(AlertViolationPayload newAlert)
        {
            // Adding new alerts
            var newAlertCopy = newAlert;
            if (!newAlert.RemoveAlertFromList && !Alerts.AsParallel().Any(a => a.AlertId == newAlertCopy.AlertId))
            {
                Alerts.Add(Alert.CreateAlert(newAlert));
                FirePropertyChanged("IsAlertCollectionEmpty");

                if (!HasBeenAlertedAboutAlertsOnStartup)
                {
                    IsAlertCollectionExpanded = true;
                    HasBeenAlertedAboutFullQCAlerts = true;
                    HasBeenAlertedAboutLinearityAlerts = true;
                    HasBeenAlertedAboutAlertsOnStartup = true;
                }
                else if (!HasBeenAlertedAboutFullQCAlerts && newAlert.Resolution==AlertResolutionPayload.FullQCAlert)
                {
                    IsAlertCollectionExpanded = true;
                    HasBeenAlertedAboutFullQCAlerts = true;
                }
                else if (!HasBeenAlertedAboutContaminationAlerts && newAlert.Resolution == AlertResolutionPayload.ContaminationQCAlert)
                {
                    IsAlertCollectionExpanded = true;
                    HasBeenAlertedAboutContaminationAlerts = true;
                }
                else if (!HasBeenAlertedAboutLinearityAlerts && newAlert.Resolution == AlertResolutionPayload.LinearityQCAlert)
                {
                    IsAlertCollectionExpanded = true;
                    HasBeenAlertedAboutLinearityAlerts = true;
                }
                else if (!HasBeenAlertedAboutStandardsAlerts && newAlert.Resolution==AlertResolutionPayload.StandardsQCAlert)
                {
                    IsAlertCollectionExpanded = true;
                    HasBeenAlertedAboutStandardsAlerts = true;
                }

                _logger.Log(String.Format("AlertsViewModel: Alert added : {0}", newAlert.Resolution), Category.Debug, Priority.Low);
            }
            // Removing alerts
            else if (newAlert.RemoveAlertFromList)
            {
                var alerts = (from a in Alerts.AsParallel() where a.AlertId == newAlertCopy.AlertId select a).ToList();
                if (alerts.Count > 0)
                {
                    alerts.ForEach(a =>
                    {
                        _logger.Log(String.Format("AlertsViewModel: Alert removed : {0}", newAlert.Resolution), Category.Debug, Priority.Low);
                        Alerts.Remove(a);
                    });
                }
            }

            if (_settingsManager.GetSetting<bool>(SettingKeys.QcTestDue)) return;
            
            HasBeenAlertedAboutFullQCAlerts = false;
            HasBeenAlertedAboutContaminationAlerts = false;
            HasBeenAlertedAboutStandardsAlerts = false;
            HasBeenAlertedAboutLinearityAlerts = false;
        }

        #endregion

        #region Helpers

        private void InformUserOfDetectorDemoSettingChanged()
        {
            var choices = new[] { "Close Application", "Cancel" };
            var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning,
                String.Format(SettingsChangeFormatString, String.Empty, "detector has", "this setting"),
                "Application Restart Required", choices);

            if (choices[result] == "Close Application")
            {
                var stackTrace = new StackTrace();
                _logger.Log("***InformUserOfDetectorDemoSettingChanged() called by someone: " + stackTrace, Category.Info, Priority.High);
                System.Windows.Application.Current.MainWindow.Close();
            }
            _hasBeenAlertedAboutDetectorDemoModeChange = true;
        }

        private void InformUserOfSampleChangerDemoSettingChanged()
        {
            var choices = new[] { "Close Application", "Cancel" };
            var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning,
                        String.Format(SettingsChangeFormatString, String.Empty, "sample changer has", "this setting"),
                        "Application Restart Required", choices);

            if (choices[result] == "Close Application")
            {
                var stackTrace = new StackTrace();
                _logger.Log("***InformUserOfSampleChangerDemoSettingChanged() called by someone: " + stackTrace, Category.Info, Priority.High);
                System.Windows.Application.Current.MainWindow.Close();
            }

            _hasBeenAlertedAboutSampleChangerDemoModeChange = true;
        }

        private void InformUserOfDetectorAndSampleChangerDemoSettingsChanged()
        {
            var choices = new[] { "Close Application", "Cancel" };
            var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning,
                        String.Format(SettingsChangeFormatString, "s", "detector and sample changer have", "these settings"),
                        "Application Restart Required", choices);

            if (choices[result] == "Close Application")
            {
                var stackTrace = new StackTrace();
                _logger.Log("***InformUserOfDetectorAndSampleChangerDemoSettingChanged() called by someone: " + stackTrace, Category.Info, Priority.High);
                System.Windows.Application.Current.MainWindow.Close();
            }

            _hasBeenAlertedAboutDetectorDemoModeChange = true;
            _hasBeenAlertedAboutSampleChangerDemoModeChange = true;
        }

        protected virtual void DealWithExistingAlerts()
        {
            var alerts = from x in _systemNotificationWatcher.SystemNotifications
                         orderby x.SystemNotificationDateTime
                         select x;

            foreach (var alert in alerts)
            {
                if (alert.SystemNotificationData == null) continue;

                if (alert.SystemNotificationData is AlertViolationPayload)
                    _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alert.SystemNotificationData as AlertViolationPayload);

                if (!(alert is MessageBoxContent)) continue;

                var alertMessageBoxContent = alert as MessageBoxContent;
                IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information, alertMessageBoxContent.Message, 
                                                                                       alertMessageBoxContent.Caption, MessageBoxChoiceSet.Close));
            }
        }

        void OnSystemNotificationChanged(object sender, SystemNotificationEventArgs e)
        {
            var payload = e.ChangedSystemNotification.SystemNotificationData;

            if (e.ChangedSystemNotification is MessageBoxContent)
            {
                var alertMessageBoxContent = e.ChangedSystemNotification as MessageBoxContent;
                if (alertMessageBoxContent.DisplayMessageBox)
                {
                    if (payload is AlertViolationPayload)
                    {
                        var alertViolationPayload = payload as AlertViolationPayload;
                        IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information, alertViolationPayload.Resolution, alertViolationPayload.ErrorHeader, MessageBoxChoiceSet.Close));
                    }
                    else
                    {
                        IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information, alertMessageBoxContent.Message, alertMessageBoxContent.Caption, MessageBoxChoiceSet.Close));
                    }
                }
            }

            if (payload is AlertViolationPayload)
            {
                _eventAggregator.GetEvent<AlertViolationChanged>().Publish(payload as AlertViolationPayload);
            }
        }

        /// <summary>
        /// Registers which settings that the settings observer should listen for.
        /// </summary>
        protected virtual void InitializeSettingObserver()
        {
            _sObserver.RegisterHandler(SettingKeys.SystemDetectorDemoModeEnabled, s =>
            {
                var currentSampleChangerDemoMode = _settingsManager.GetSetting<bool>(SettingKeys.SystemSampleChangerDemoModeEnabled);
                var currentDetectorDemoMode = _settingsManager.GetSetting<bool>(SettingKeys.SystemDetectorDemoModeEnabled);
                if (_hasBeenAlertedAboutDetectorDemoModeChange) return;

                if (_isSampleChangerInDemoMode != currentSampleChangerDemoMode)
                    InformUserOfDetectorAndSampleChangerDemoSettingsChanged();
                else
                    InformUserOfDetectorDemoSettingChanged();
                _isSampleChangerInDemoMode = currentSampleChangerDemoMode;
                _isDetectorInDemoMode = currentDetectorDemoMode;
            });

            _sObserver.RegisterHandler(SettingKeys.SystemSampleChangerDemoModeEnabled, s =>
            {
                var currentSampleChangerDemoMode = _settingsManager.GetSetting<bool>(SettingKeys.SystemSampleChangerDemoModeEnabled);
                var currentDetectorDemoMode = _settingsManager.GetSetting<bool>(SettingKeys.SystemDetectorDemoModeEnabled);
                if (_hasBeenAlertedAboutSampleChangerDemoModeChange) return;

                if (_isDetectorInDemoMode != currentDetectorDemoMode)
                    InformUserOfDetectorAndSampleChangerDemoSettingsChanged();
                else
                    InformUserOfSampleChangerDemoSettingChanged();
                _isSampleChangerInDemoMode = currentSampleChangerDemoMode;
                _isDetectorInDemoMode = currentDetectorDemoMode;
            });
        }
        #endregion
    }
}
