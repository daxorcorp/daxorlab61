﻿using System;
using System.Globalization;
using System.Windows.Input;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Footer.ViewModels
{
    public class SystemStatusSubViewModel : ViewModelBase
    {

        #region Enum
       
        public enum SysMode
        {
            SeekingBackground,
            CountingBkgWithPreviousBkg,
            CountingBkgWithoutPreviousBkg,
            PerformingTestCountingSample,
            PerformingTestMovingToSample,
            PerformingTestCountingWithNavigation,
            PerformingTestMovingWithNavigation,
            RestartBackgroundManual,
            Idle,
        }
        
        #endregion

        #region Fields
        
        private readonly IEventAggregator _eventAggregator;
        private readonly IBackgroundAcquisitionService _bkgService;
        private readonly ILoggerFacade _customLoggerFacade;
        private readonly DelegateCommand<Object> _navigateToRunningTestCommand;
        private readonly DelegateCommand<Object> _restartBackgroundCommand;

        private ITest _rTest;

        private string _taskTotalTime, _taskRemainingTime, _taskRemainingTotalTime, _systemStateDescription;
        private string _sampleName, _sysModeDescription, _leftInfoHeader, _rightInfoValue, _rightInfoHeader, _leftInfoValue;
        private bool _isHighBkgDetected;
        private int _prevBkgCountsPerMinute, _currBkgCountsPerMinute, _percentageCompleted;
        private SysMode _systemMode = SysMode.Idle;

        #endregion

        #region Ctor

        public SystemStatusSubViewModel(IBackgroundAcquisitionService bkgService, IEventAggregator eventAggregator, ILoggerFacade customLoggerFacade)
        {
            _bkgService = bkgService;
            _eventAggregator = eventAggregator;
            _customLoggerFacade = customLoggerFacade;

            //Instantiating commands
            _navigateToRunningTestCommand = new DelegateCommand<object>(NavigateToRunningTestCommandExecute, NavigateToRunningTestCommandCanExecute);
            _restartBackgroundCommand = new DelegateCommand<object>(RestartBackgroundCommandExecute, RestartBackgroundCommandCanExecute);
             
            //Setting current system mode to seeking background at instantiation
            CurrentSystemMode = SysMode.SeekingBackground;
            SubscribeToAggregateEvents();
        }

        #endregion

        #region Properties
        
        public SysMode CurrentSystemMode
        {
            get { return _systemMode; }
            private set
            {

                if (!base.SetValue(ref _systemMode, "CurrentSystemMode", value))
                    return;

                //Update leftInfo/Right INfo
                if (_systemMode == SysMode.CountingBkgWithPreviousBkg || _systemMode == SysMode.CountingBkgWithoutPreviousBkg)
                {
                    LeftInfoHeader = "CURRENT CPM";
                    RightInfoHeader = "PREVIOUS CPM";
                }
                else if (_systemMode == SysMode.PerformingTestCountingSample)
                    LeftInfoHeader = "CURRENT COUNTS";

                //Update system mode description
                if (_systemMode == SysMode.CountingBkgWithoutPreviousBkg || _systemMode == SysMode.CountingBkgWithPreviousBkg ||
                    _systemMode == SysMode.PerformingTestCountingSample || _systemMode == SysMode.PerformingTestCountingWithNavigation)

                    SystemModeDescription = "Measuring...";

                else if (_systemMode == SysMode.RestartBackgroundManual)
                {
                    SystemModeDescription = String.Empty;
                    LeftInfoHeader = String.Empty;
                    LeftInfoValue = String.Empty;
                    RightInfoHeader = String.Empty;
                    RightInfoValue = String.Empty;
                    SampleName = String.Empty;
                }
                else
                {
                    SystemModeDescription = "Moving to";
                    LeftInfoHeader = String.Empty;
                    LeftInfoValue = String.Empty;
                    RightInfoHeader = String.Empty;
                    RightInfoValue = String.Empty;
                }

                //Update system state Description
                if (_systemMode == SysMode.CountingBkgWithoutPreviousBkg || _systemMode == SysMode.CountingBkgWithPreviousBkg ||
                    _systemMode == SysMode.SeekingBackground)
                {
                    SystemStateDescription = "Background Acquisition in Progress";
                    SampleName = "Background";
                }
                else if (_systemMode == SysMode.SeekingBackground)
                {
                    SystemModeDescription = "Background Acquisition";
                }
                else if (_systemMode == SysMode.PerformingTestCountingSample || _systemMode == SysMode.PerformingTestCountingWithNavigation ||
                        _systemMode == SysMode.PerformingTestMovingToSample || _systemMode == SysMode.PerformingTestMovingWithNavigation)
                {
                }
                else if (_systemMode == SysMode.RestartBackgroundManual)
                    SystemStateDescription = "A Test Has Been Aborted";

                else
                    SystemStateDescription = "System Idle";
            }
        }

        public bool IsHighBackgroundDetected
        {
            get { return _isHighBkgDetected; }
            set { base.SetValue(ref _isHighBkgDetected, "IsHighBackgroundDetected", value); }
        }
        public int PreviousBackgroundCountsPerMinute
        {
            get { return _prevBkgCountsPerMinute; }
            set { base.SetValue(ref _prevBkgCountsPerMinute, "PreviousBackgroundCountsPerMinute", value); }
        }
        public int CurrentBackgroundCountsPerMinute
        {
            get { return _currBkgCountsPerMinute;}
            set { base.SetValue(ref _currBkgCountsPerMinute, "CurrentBackgroundCountsPerMinute", value); }
        }
        public int PercentageCompleted
        {
            get { return _percentageCompleted; }
            set { base.SetValue(ref _percentageCompleted, "PercentageCompleted", value); }
        }
        
        public string TaskTotalTime
        {
            get { return _taskTotalTime; }
            set { base.SetValue(ref _taskTotalTime, "TaskTotalTime", value); }
        }
        public string TaskRemainingTime
        {
            get { return _taskRemainingTime; }
            set { base.SetValue(ref _taskRemainingTime, "TaskRemainingTime", value); }
        }
        public string TaskRemainingTotalTime
        {
            get { return _taskRemainingTotalTime; }
            set { base.SetValue(ref _taskRemainingTotalTime, "TaskRemainingTotalTime", value); }
        }
        public string SystemStateDescription
        {
            get { return _systemStateDescription; }
            set { base.SetValue(ref _systemStateDescription, "SystemStateDescription", value); }
        }
        public string SampleName
        {
            get { return _sampleName; }
            set { base.SetValue(ref _sampleName, "SampleName", value); }
        }

        /// <summary>
        /// Such as Either Counting or Moving
        /// </summary>
        public string SystemModeDescription
        {
            get { return _sysModeDescription; }
            set { base.SetValue(ref _sysModeDescription, "SystemModeDescription", value); }
        }

        #region Left/Right Header/Value Infos

        public string LeftInfoHeader
        {
            get { return _leftInfoHeader; }
            set { base.SetValue(ref _leftInfoHeader, "LeftInfoHeader", value); }
        }
        public string LeftInfoValue
        {
            get { return _leftInfoValue; }
            set { base.SetValue(ref _leftInfoValue, "LeftInfoValue", value); }
        }
        public string RightInfoHeader
        {
            get { return _rightInfoHeader; }
            set { base.SetValue(ref _rightInfoHeader, "RightInfoHeader", value); }
        }
        public string RightInfoValue
        {
            get { return _rightInfoValue; }
            set { base.SetValue(ref _rightInfoValue, "RightInfoValue", value); }
        }
        #endregion

        public ITest RunningTest
        {
            get { return _rTest; }
            private set
            {
                if (!base.SetValue(ref _rTest, "RunningTest", value))
                    return;

                _navigateToRunningTestCommand.RaiseCanExecuteChanged();
                _restartBackgroundCommand.RaiseCanExecuteChanged();
            }
        }

        #endregion

        #region Commands

        public ICommand NavigateToRunningTestCommand
        {
            get { return _navigateToRunningTestCommand; }
        }
        private bool NavigateToRunningTestCommandCanExecute(object parm)
        {
            return RunningTest != null;
        }
        private void NavigateToRunningTestCommandExecute(object parm)
        {

        }

        public ICommand RestartBackgroundCommand
        {
            get { return _restartBackgroundCommand; }
        }
        private bool RestartBackgroundCommandCanExecute(object parm)
        {
            return true;
        }
        private void RestartBackgroundCommandExecute(object parm)
        {
            if (RunningTest == null)
            {
                // The Restart Background Button is not visible when there is a running test, but this event can
                // fire anyway.  We only want to restart the background if there isn't a running test
                _customLoggerFacade.Log("RESTART BACKGROUND BUTTON CLICKED", Category.Info, Priority.None);
                _bkgService.StartAsync();
            }
        }

        #endregion

        #region AggregateEvents

        private void SubscribeToAggregateEvents()
        {
            _eventAggregator.GetEvent<BackgroundAcquisitionProgressChanged>().Subscribe(BackgroundAcquisitionChanged, ThreadOption.UIThread, false, r => (RunningTest != null && RunningTest.Status != TestStatus.Running) || RunningTest == null);
            _eventAggregator.GetEvent<BackgroundAcquisitionStarted>().Subscribe(BackgroundAcquisitionStarted, ThreadOption.UIThread, false, r => (RunningTest != null && RunningTest.Status != TestStatus.Running) || RunningTest == null);
            
            _eventAggregator.GetEvent<TestStarted>().Subscribe(TestStarted, ThreadOption.UIThread, false);
            _eventAggregator.GetEvent<TestCompleted>().Subscribe(TestFinishedAborted, ThreadOption.UIThread, false);
            _eventAggregator.GetEvent<TestAborted>().Subscribe(TestFinishedAborted, ThreadOption.UIThread, false);
            _eventAggregator.GetEvent<TestExecutionProgressChanged>().Subscribe(TestExecutionProgressChanged, ThreadOption.UIThread, false);
        }

        private void BackgroundAcquisitionChanged(BackgroundAcquisitionProgressPayload payload)
        {
            if (payload == null)
                throw new ArgumentNullException("payload");
            
            // Some test is running
            if (CurrentSystemMode == SysMode.PerformingTestCountingSample || CurrentSystemMode == SysMode.PerformingTestCountingWithNavigation ||
                CurrentSystemMode == SysMode.PerformingTestMovingToSample || CurrentSystemMode == SysMode.PerformingTestMovingWithNavigation)
            {

                if (payload.IsActive && payload.Status == BackgroundAcquisitionServiceStatus.Acquiring)
                {
                    LeftInfoHeader = "CURRENT CPM";
                    LeftInfoValue = payload.CurrentCpm.ToString(CultureInfo.InvariantCulture);
                }
            }
            else
            {
                switch (payload.Status)
                {
                    case BackgroundAcquisitionServiceStatus.SeekingBackgroundPosition:
                        CurrentSystemMode = SysMode.SeekingBackground;
                        break;

                    case BackgroundAcquisitionServiceStatus.Acquiring:

                        LeftInfoValue = payload.CurrentCpm.ToString(CultureInfo.InvariantCulture);
                        IsHighBackgroundDetected = payload.IsCurrentCpmTooHigh;
                        
                        SystemStateDescription = IsHighBackgroundDetected ? "High Background" : "Background Acquisition in Progress";

                        PercentageCompleted = payload.PercentageCompleted;
                        TaskTotalTime = FormattedTaskTime(payload.TotalTaskTimeInSeconds);
                        TaskRemainingTime = FormattedTaskTime((payload.TimeElapsedInSeconds));
                        TaskRemainingTotalTime = FormattedTaskTime(payload.TotalTaskTimeInSeconds - payload.TimeElapsedInSeconds);
                        if (payload.WasGoodBackgroundAttained)
                        {
                            RightInfoValue = payload.PreviousCpm != null
                                                 ? ((int) payload.PreviousCpm).ToString(CultureInfo.InvariantCulture)
                                                 : string.Empty;
                            CurrentSystemMode = SysMode.CountingBkgWithPreviousBkg;
                        }
                        else
                            CurrentSystemMode = SysMode.CountingBkgWithoutPreviousBkg;
                        
                        break;
                }
            }
        }
        private void BackgroundAcquisitionStarted(object payload)
        {
            if (CurrentSystemMode != SysMode.PerformingTestCountingSample && CurrentSystemMode != SysMode.PerformingTestCountingWithNavigation &&
                CurrentSystemMode != SysMode.PerformingTestMovingToSample && CurrentSystemMode != SysMode.PerformingTestMovingWithNavigation)
                CurrentSystemMode = SysMode.SeekingBackground;
        }
        private void TestExecutionProgressChanged(TestExecutionProgressChangedEventArgs progressChangedPayload)
        {
            PercentageCompleted = progressChangedPayload.ProgressMetadata.PercentCompleted;
            TaskTotalTime = FormattedTaskTime(progressChangedPayload.ProgressMetadata.TotalEstimatedExecutionTimeInSeconds);
           
            TaskRemainingTime = FormattedTaskTime((int)Math.Round((double)progressChangedPayload.ProgressMetadata.RemainingExecutionTimeInSeconds, 0));
            TaskRemainingTotalTime = FormattedTaskTime(progressChangedPayload.ProgressMetadata.RemainingExecutionTimeInSeconds);

            SampleName = progressChangedPayload.ContextMetadata.FriendlyExecutingSampleName;

            //Update the system status description
            SystemStateDescription = String.Format("{0} in Progress", progressChangedPayload.ContextMetadata.DisplayTestName);

            if (progressChangedPayload.CurrentExecutionStep == TestExecutionStep.CountSample ||
                progressChangedPayload.CurrentExecutionStep == TestExecutionStep.CountBackground)
            {
                if (CurrentSystemMode != SysMode.PerformingTestCountingWithNavigation)
                    CurrentSystemMode = SysMode.PerformingTestCountingSample;

                LeftInfoValue = progressChangedPayload.ContextMetadata.UpdatedSampleCounts.ToString(CultureInfo.InvariantCulture);
            }
            else if (progressChangedPayload.CurrentExecutionStep == TestExecutionStep.MoveToSample ||
                     progressChangedPayload.CurrentExecutionStep == TestExecutionStep.MoveToBackground)
            {
                if (CurrentSystemMode != SysMode.PerformingTestMovingWithNavigation)
                    CurrentSystemMode = SysMode.PerformingTestMovingToSample;
            }
        }
        private void TestStarted(ITest runningTest)
        {
            if (RunningTest != null)
                throw new Exception("HandleAggregateTestStarted. System is in undefined state - CurrentRunningTest was not null");

            //Reset high background detected and setup starting test
            IsHighBackgroundDetected = false;
            RunningTest = runningTest;
            CurrentSystemMode = SysMode.PerformingTestMovingToSample;
        }
        private void TestFinishedAborted(ITest test)
        {
            if (RunningTest == null)
                throw new Exception("Test has finished but the currentTest is null.");

            if (!RunningTest.InternalId.Equals(test.InternalId))
                throw new Exception("Test has finished but testIDs do not match.");

            CurrentSystemMode = test.Status == TestStatus.Aborted ? SysMode.RestartBackgroundManual : SysMode.SeekingBackground;
            
            RunningTest = null;
        }

        #endregion

        #region Helpers

        string FormattedTaskTime(int timeInSec)
        {   
            return String.Format("{0:h\\:mm\\:ss}", new TimeSpan(0, 0, timeInSec));
        }

        #endregion
    }
}
