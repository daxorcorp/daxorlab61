﻿using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Microsoft.Practices.Composite.Events;
using Daxor.Lab.Infrastructure.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using Daxor.Lab.Infrastructure.Constants;

namespace Daxor.Lab.Footer.ViewModels
{
    public class FooterViewModel : ViewModelBase
    {
        private readonly AlertsViewModel _alertsVm;
        private readonly SystemStatusSubViewModel _sysVm;
        private readonly IEventAggregator _eventAggregator;

        private bool _isFooterVisible = true;

        public FooterViewModel(AlertsViewModel alertsVm, SystemStatusSubViewModel sysVm, IEventAggregator eventAggregator)
        {
            _alertsVm = alertsVm;
            _sysVm = sysVm;
            _eventAggregator = eventAggregator;

            SubscribeToAggregateEvents();
        }

        public AlertsViewModel AlertsViewModel
        {
            get { return _alertsVm; }
        }
        public SystemStatusSubViewModel SystemViewModel
        {
            get { return _sysVm; }
        }

        public bool IsFooterVisible
        {
            get { return _isFooterVisible; }
            set
            {
                if (_isFooterVisible == value) return;

                _isFooterVisible = value;
                FirePropertyChanged("IsFooterVisible");
            }
        }

        private void SubscribeToAggregateEvents()
        {
            _eventAggregator.GetEvent<AppPartActivationChanged>().Subscribe(
                OnPartActivationChanged,
                ThreadOption.UIThread,
                false,
                (p) => { return p.AppPartName == ApplicationParts.Footer; });
        }
        private void OnPartActivationChanged(ActivationChangedPayload payload)
        {
            IsFooterVisible = payload.IsActivated;
        }

    }
}
