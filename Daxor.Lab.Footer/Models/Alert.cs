﻿using System.Collections.Generic;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.RuleFramework;
using Daxor.Lab.Infrastructure.Alerts;

namespace Daxor.Lab.Footer.Models
{
    public enum AlertTypes
    {
        BVAlert,
        SystemAlert
    }

    public class Alert : AlertBase
    {
        public static Alert CreateAlert(string alertType, string alertOwner, int alertIdByOwner, string alertMsgType, string alertHeader, string alertMsgBody)
        {
            return new Alert()
            {
                AlertType = alertType,
                AlertOwner = alertOwner,
                AlertIdByOwner = alertIdByOwner,
                AlertMessageType = alertMsgType,
                AlertHeader = alertHeader,
                AlertMessageBody = alertMsgBody
            };
        }

        public static Alert CreateAlert(AlertViolationPayload newAlert)
        {
            return new Alert()
            {
                AlertType = AlertTypes.BVAlert.ToString(),
                AlertMessageBody = newAlert.Resolution,
                AlertHeader = newAlert.ErrorHeader,
                AlertOwner = newAlert.ErrorOwner,
                AlertMessageType = "Warning",
                AlertId = newAlert.AlertId,
                UniqueId = newAlert.UniqueModelPropertyId

            };
        }
    }
}
