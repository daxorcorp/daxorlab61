﻿namespace Daxor.Lab.ReportViewer
{
    public enum CellStyle
    {
        Header,
        UsDateTime,
        Bold,
        ColumnHeading,
        SubHeader,
        UsDate,
        None
    };
}