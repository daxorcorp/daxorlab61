using System;
using System.Collections;
using CarlosAg.ExcelXmlWriter;

namespace Daxor.Lab.ReportViewer
{
    public interface IExcelFileBuilder
    {
        Workbook ExcelWorkbook { get; set; }
        ArrayList GetColumnsArray { get; }
        Worksheet GetFirstWorksheet();

        /// <summary>
        /// Adds a single worksheet with the given name
        /// </summary>
        /// <param name="worksheetName">Name for the given worksheet</param>
        /// <exception cref="ArgumentNullException">Thrown if worksheet name is null or empty</exception>
        void AddWorksheet(string worksheetName);

        /// <summary>
        /// Inserts content formatted as a number into the given worksheet[cellRow][cellColumn]
        /// </summary>
        void InsertNumberCell(Worksheet worksheet, int cellRow, int cellColumn, string content, CellStyle styleChoice);

        /// <summary>
        /// Inserts content into the given worksheet[cellRow][cellColumn]
        /// </summary>
        void InsertStringCell(Worksheet worksheet, int cellRow, int cellColumn, string content, CellStyle styleChoice);

        /// <summary>
        /// Inserts content formatted as a date and time into the given worksheet[cellRow][cellColumn]
        /// </summary>
        void InsertDateTimeCell(Worksheet worksheet, int cellRow, int cellColumn, string content, CellStyle styleChoice);

        /// <summary>
        /// Saves workbook to absolute path specified
        /// </summary>
        /// <param name="absolutePath"></param>
        /// <exception cref="ArgumentNullException">Thrown if absolutePath is null or empty</exception>
        void SaveExcelWorkbook(string absolutePath);

        int TotalConfigColumns();
        int TotalColumns();
        void UpdateColumnHeader(string argOrgHeader, string argConfigHeader);
        void LoadColumnsFromXML(string xmlFilePath);
        int Name2Column(string argColumnName);

        /// <summary>
        /// Returns Header Name
        /// </summary>
        /// <param name="argColNumber">Column Number starting from 1 and up</param>
        /// <returns></returns>
        string Column2Name(int argColNumber);

        IColumn GetIColumn(int argColNumber);
        void Dispose();
    }
}