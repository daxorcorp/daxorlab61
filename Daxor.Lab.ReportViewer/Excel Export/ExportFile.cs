﻿using System;
using System.Collections;
using System.Xml;
using System.Text;

namespace Daxor.Lab.ReportViewer
{
    
    public interface IColumn
    {
        string Format { get; set; }
        string HeaderName { get; set; }
        int ColumnNumber { get; }
        bool IsVisible { get; }
    }


    public class ReportColumn : IColumn
    {
        #region PrivateMemebers
        private const string DEFAULTFORMAT = "@";
        private string mHeaderName;
        private string mColumnFormat;
        private int mColNumber;
        private bool mVisible;
        #endregion

        #region Properties
        public string Format
        {
            set
            {
                mColumnFormat = value;
                if (mColumnFormat == string.Empty)
                    mColumnFormat = DEFAULTFORMAT;
            }

            get { return mColumnFormat; }
        }
        public string HeaderName
        {
            set { mHeaderName = value; }

            get { return mHeaderName; }
        }
        public int ColumnNumber
        {
            get{ return mColNumber; }
        }
        public bool IsVisible
        {
            get { return mVisible; }
        }

        #endregion

        #region Constructor
        public ReportColumn(string argName, string argFormat, int argColNumber, bool argVisible)
        {
            mHeaderName = argName;
            mColumnFormat = argFormat;
            mColNumber = argColNumber;
            mVisible = argVisible;
        }
        #endregion

    }



    public abstract class ExportFile : IDisposable    
    {

        private Hashtable mAllColumns;          // All Columns: predifined and configurable
        private Hashtable mConfigColumns;       // Configurable colums

        private ArrayList mIndexColumns;
        protected ExportFile()
        {
            mAllColumns = new Hashtable();
            mConfigColumns = new Hashtable();
            mIndexColumns = new ArrayList();
        }
        private void AddColumn(string argColName, string argFormat, int argNumber, bool argIsVisible)
        {
            ReportColumn rColumn = new ReportColumn(argColName, argFormat, argNumber, argIsVisible);
            mAllColumns.Add(argColName, rColumn);
            mIndexColumns.Add(rColumn);
        }  
        private void RemoveColumn(string argColName)
        {
            if (mAllColumns.ContainsKey(argColName))
            {
                mAllColumns.Remove(argColName);
            }
        }

        public ArrayList GetColumnsArray
        {
            get { return mIndexColumns; }
        }
        public int TotalConfigColumns()
        {
            return mConfigColumns.Count;
        }
        public int TotalColumns()
        {
            return mAllColumns.Count;
        }
        public void UpdateColumnHeader(string argOrgHeader, string argConfigHeader)
        {
            // check if original header is allowed to be changed
            if (mConfigColumns.ContainsKey(argOrgHeader))
                ((IColumn)(mAllColumns[argOrgHeader])).HeaderName = argConfigHeader;
        }
     
        /************************************************
         * Loads Fields ( including names, format from an XML file )
         * 
         * ***********************************************/
        public void LoadColumnsFromXML(string xmlFilePath)
        {
            XmlDocument doc = new XmlDocument();
            string colHeader, colFormat, type;
            int colNumber = 1;
            bool colVisible;
            try
            {
                doc.Load(xmlFilePath);
                XmlNodeList columnList = doc.GetElementsByTagName("COLUMN");
                
                // Go through ecah column
                foreach (XmlNode node in columnList)
                {
                    colVisible = true;
                    XmlElement colElement = (XmlElement) node;
                    colHeader = colElement.GetElementsByTagName("HEADER")[0].InnerText;
                    colFormat = colElement.GetElementsByTagName("FORMAT")[0].InnerText;
                    colVisible = colElement.GetElementsByTagName("VISIBLE")[0].InnerText.ToUpper().Equals("TRUE");
                    type = "";

                    // Continue if column is not visible
                    if (!colVisible) continue;

                    // Configurable attribute
                    if (colElement.HasAttributes)
                    {
                        type = colElement.Attributes["type"].InnerText;
                        if (type.Equals("configurable"))
                            mConfigColumns.Add(colHeader, colFormat);
                    }

                    AddColumn(colHeader, colFormat, colNumber, colVisible);
                    colNumber++;
                }
            }
            catch
            {
                throw new ApplicationException("Error reading " + xmlFilePath);
            }

        }


        public int Name2Column(string argColumnName)
        {
            return mAllColumns.ContainsKey(argColumnName) ? ((IColumn)mAllColumns[argColumnName]).ColumnNumber : -1;
        }
        /// <summary>
        /// Returns Header Name
        /// </summary>
        /// <param name="argColNumber">Column Number starting from 1 and up</param>
        /// <returns></returns>
        public string Column2Name(int argColNumber)
        {
            return ((IColumn)mIndexColumns[argColNumber - 1]).HeaderName;
        }
        
        public IColumn GetIColumn(int argColNumber)
        {
            return (argColNumber >= 1 && argColNumber <= mIndexColumns.Count) ? (IColumn)mIndexColumns[argColNumber - 1] : null;
        }



        public void Dispose()
        {
            mAllColumns.Clear();
            mAllColumns = null;
            mConfigColumns.Clear();
            mConfigColumns = null;
        }

    }
}
