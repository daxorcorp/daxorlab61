﻿using System;
using System.Collections.Generic;
using System.Globalization;
using CarlosAg.ExcelXmlWriter;
using Daxor.Lab.Infrastructure.Extensions;

namespace Daxor.Lab.ReportViewer.Excel_Export
{
    public class ExcelFileBuilder : ExportFile, IExcelFileBuilder
    {
        #region Constants

        public const int MaxWorksheetsSupported = 2;
        public const string DefaultFontFamily = "Calibri";

        #endregion

        #region Field

        private Dictionary<CellStyle, string> _styles = new Dictionary<CellStyle, string>();
        
        #endregion

        #region Properties

        public Workbook ExcelWorkbook { get; set; }

        #endregion

        #region Ctor

        public ExcelFileBuilder()
        {
            ExcelWorkbook = new Workbook();
            BuildStyles();
            SetWorkBookProperties();
            CreateDefaultStyles();
        }
        
        #endregion

        #region Public API

        public Worksheet GetFirstWorksheet()
        {
            return ExcelWorkbook.Worksheets[0];
        }

        /// <summary>
        /// Adds a single worksheet with the given name
        /// </summary>
        /// <param name="worksheetName">Name for the given worksheet</param>
        /// <exception cref="ArgumentNullException">Thrown if worksheet name is null or empty</exception>
        public void AddWorksheet(string worksheetName)
        {
            if (String.IsNullOrEmpty(worksheetName))
                throw new ArgumentNullException("worksheetName");

            if (ExcelWorkbook.Worksheets.Count > 0)
                ExcelWorkbook.Worksheets.Clear();
            
            ExcelWorkbook.Worksheets.Add(worksheetName);
        }

        /// <summary>
        /// Inserts content formatted as a number into the given worksheet[cellRow][cellColumn]
        /// </summary>
        public void InsertNumberCell(Worksheet worksheet, int cellRow, int cellColumn, string content, CellStyle styleChoice)
        {
            var formattedContent = FormatContentAsNumber(content);
            InsertCell(worksheet, cellRow, cellColumn, formattedContent, DataType.Number, styleChoice);
        }

        /// <summary>
        /// Inserts content into the given worksheet[cellRow][cellColumn]
        /// </summary>
        public void InsertStringCell(Worksheet worksheet, int cellRow, int cellColumn, string content, CellStyle styleChoice)
        {
            InsertCell(worksheet, cellRow, cellColumn, content, DataType.String, styleChoice);
        }

        /// <summary>
        /// Inserts content formatted as a date and time into the given worksheet[cellRow][cellColumn]
        /// </summary>
        public void InsertDateTimeCell(Worksheet worksheet, int cellRow, int cellColumn, string content, CellStyle styleChoice)
        {
            var formattedContent = FormatContentAsDateTime(content);
            InsertCell(worksheet, cellRow, cellColumn, formattedContent, DataType.DateTime, styleChoice);
        }

        /// <summary>
        /// Saves workbook to absolute path specified
        /// </summary>
        /// <param name="absolutePath"></param>
        /// <exception cref="ArgumentNullException">Thrown if absolutePath is null or empty</exception>
        public void SaveExcelWorkbook(string absolutePath)
        {
            if (String.IsNullOrEmpty(absolutePath))
                throw new ArgumentNullException("absolutePath");

            ExcelWorkbook.Save(absolutePath);

            CleanObjects();
        }

        #endregion

        #region Helper Methods

        private void InsertCell(Worksheet worksheet, int cellRow, int cellColumn, string content, DataType type, CellStyle styleChoice)
        {
            var cell = GetCell(worksheet, cellRow, cellColumn);
            if (cell == null)
                return;

            string styleId;
            if (_styles.TryGetValue(styleChoice, out styleId))
                if (!String.IsNullOrEmpty(styleId))
                    cell.StyleID = styleId;

            cell.Data.Type = type;
            if (!String.IsNullOrEmpty(content))
                cell.Data.Text = content;
        }

        private WorksheetCell GetCell(Worksheet worksheet, int cellRow, int cellColumn)
        {
            if (worksheet == null)
                throw new ArgumentNullException("worksheet");

            if (cellRow < 1)
                throw new ArgumentOutOfRangeException("cellRow","cellRow is less than 1");
            if (cellColumn < 1)
                throw new ArgumentOutOfRangeException("cellColumn", "cellColumn is less than 1");

            WorksheetCell worksheetCell = null;
            WorksheetRow worksheetRow = null;

            //Check number of rows in a given worksheet, create more if necessary
            while (worksheet.Table.Rows.Count < cellRow)
            {
                worksheetRow = new WorksheetRow();
                worksheet.Table.Rows.Add(worksheetRow);
                worksheetRow.Index = worksheet.Table.Rows.Count;
            }
            //Row already exists
            if (worksheetRow == null)
                worksheetRow = worksheet.Table.Rows[cellRow - 1];


            //Check number of cols in a given worksheet, in a given column
            while (worksheetRow.Cells.Count < cellColumn)
            {
                worksheetCell = new WorksheetCell();
                worksheetRow.Cells.Add(worksheetCell);
                worksheetCell.Index = worksheetRow.Cells.Count;
            }

            if (worksheetCell == null)
                worksheetCell = worksheetRow.Cells[cellColumn - 1];

            return worksheetCell;
        }

        private void CreateDefaultStyles()
        {
            if (StyleDoesNotExist(CellStyle.Header))
                AddHeaderStyle();

            if (StyleDoesNotExist(CellStyle.SubHeader))
                AddSubHeaderStyle();

            if (StyleDoesNotExist(CellStyle.ColumnHeading))
                AddColumnHeaderStyle();

            if (StyleDoesNotExist(CellStyle.Bold))
                AddBoldStyle();

            if (StyleDoesNotExist(CellStyle.UsDateTime))
                AddUsDateTimeStyle();

            if (StyleDoesNotExist(CellStyle.UsDate))
                AddUsDateStyle();
        }

        private void AddHeaderStyle()
        {
            string headerStyleId;
            if (!_styles.TryGetValue(CellStyle.Header, out headerStyleId))
                return;

            var headerStyle = ExcelWorkbook.Styles.Add(headerStyleId);
            headerStyle.Font.FontName = DefaultFontFamily;
            headerStyle.Font.Size = 12;
            headerStyle.Font.Bold = true;
        }

        private void AddSubHeaderStyle()
        {
            string subHeaderStyleId;
            if (!_styles.TryGetValue(CellStyle.SubHeader, out subHeaderStyleId))
                return;

            var subHeaderStyle = ExcelWorkbook.Styles.Add(subHeaderStyleId);
            subHeaderStyle.Font.FontName = DefaultFontFamily;
            subHeaderStyle.Font.Size = 12;
        }

        private void AddColumnHeaderStyle()
        {
            string columnHeadingStyleId;
            if (!_styles.TryGetValue(CellStyle.ColumnHeading, out columnHeadingStyleId))
                return;

            var columnHeaderStyle = ExcelWorkbook.Styles.Add(columnHeadingStyleId);
            columnHeaderStyle.Font.FontName = DefaultFontFamily;
            columnHeaderStyle.Font.Bold = true;
            columnHeaderStyle.Font.Size = 11;
        }

        private void AddBoldStyle()
        {
            string boldStyleId;
            if (!_styles.TryGetValue(CellStyle.Bold, out boldStyleId))
                return;

            var boldStyle = ExcelWorkbook.Styles.Add(boldStyleId);
            boldStyle.Font.FontName = DefaultFontFamily;
            boldStyle.Font.Bold = true;
            boldStyle.Font.Size = 12;
        }

        private void AddUsDateTimeStyle()
        {
            string usDateTimeStyleId;
            if (!_styles.TryGetValue(CellStyle.UsDateTime, out usDateTimeStyleId))
                return;

            var usDateTimeStyle = ExcelWorkbook.Styles.Add(usDateTimeStyleId);
            usDateTimeStyle.Font.FontName = DefaultFontFamily;
            usDateTimeStyle.NumberFormat = "mm/dd/yyyy\\ h:mm\\ AM/PM";
            usDateTimeStyle.Font.Size = 11;
        }

        private void AddUsDateStyle()
        {
            string usDateStyleId;
            if (!_styles.TryGetValue(CellStyle.UsDate, out usDateStyleId))
                return;

            var usDateStyle = ExcelWorkbook.Styles.Add(usDateStyleId);
            usDateStyle.Font.FontName = DefaultFontFamily;
            usDateStyle.NumberFormat = "mm/dd/yyyy";
            usDateStyle.Font.Size = 11;
        }

        private bool StyleDoesNotExist(CellStyle choice)
        {
            string styleName;

            if (!_styles.TryGetValue(choice, out styleName))
                throw new ArgumentException("choice is not a valid option for a style");

            if (ExcelWorkbook == null)
                return false;

            return ExcelWorkbook.Styles.IndexOf(styleName) == -1;
        }

        private void SetWorkBookProperties()
        {
            ExcelWorkbook.Properties.Author = "DAXOR Corporation";
            ExcelWorkbook.Properties.Company = ExcelWorkbook.Properties.Author;
            ExcelWorkbook.Properties.Created = DateTime.Now;
        }

        private void CleanObjects()
        {
            if (ExcelWorkbook == null)
                return;

            GetFirstWorksheet().Table.Columns.Clear();
            GetFirstWorksheet().Table.Rows.Clear();
            ExcelWorkbook.Worksheets.Clear();
        }

        private void BuildStyles()
        {
            _styles = new Dictionary<CellStyle, string>();
            var keys = Enum.GetValues(typeof (CellStyle));

            foreach (var key in keys)
                _styles.Add((CellStyle)key, key.ToString());

            _styles[CellStyle.None] = null;
        }

        private string FormatContentAsNumber(string content)
        {
            if (String.IsNullOrEmpty(content))
                throw new ArgumentNullException("content");

            double doubleResult;
            int intResult;

            if (content.Trim().EndsWith("%"))
                content = content.Replace("%", string.Empty).Trim();

            if (content.Contains("e", StringComparison.OrdinalIgnoreCase))
                throw new ArgumentException("content cannot be converted to a number because it contains e or E.");

            if (Double.TryParse(content, out doubleResult))
                return doubleResult.ToString(CultureInfo.InvariantCulture);
            
            if (Int32.TryParse(content, out intResult))
                return intResult.ToString(CultureInfo.InvariantCulture);

            throw new ArgumentException("content cannot be converted to a number because it is not a number.");
        }
        
        private String FormatContentAsDateTime(string content)
        {
            DateTime dateTimeResult;

            if (DateTime.TryParse(content, out dateTimeResult))
                return dateTimeResult.ToString("s");

            throw new ArgumentException("content cannot be converted to a DateTime because it's not a valid DateTime.");
        }

        #endregion
    }
}
