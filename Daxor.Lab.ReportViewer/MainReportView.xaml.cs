﻿using System;
using System.Security.Permissions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using Telerik.Reporting;

namespace Daxor.Lab.ReportViewer
{
    /// <summary>
    /// Interaction logic for MainReportWindow.xaml
    /// </summary>
    public partial class MainReportView
    {
        #region Fields

        private readonly ReportControllerBase _rptController;

        #endregion

        #region Properties
        public IRegion Region
        {
            get;
            set;
        }

        #endregion

        #region Ctor
        public MainReportView(ReportControllerBase controller, ISettingsManager settingsManager, IEventAggregator eventAggregator)
        {
            InitializeComponent();
            _rptController = controller;
            ReportViewer.ZoomPercent = settingsManager.GetSetting<Int32>(SettingKeys.SystemDefaultReportZoom);

            CommandBindings.Add(new CommandBinding(ReportCommands.CloseReportCommand,
                                  (o, e) =>
                                  {
                                      if (Region.ActiveViews.Contains(this))
                                      {
                                          Region.Deactivate(this);
                                          Region.Remove(this);
                                          eventAggregator.GetEvent<ViewReportExecuteChanged>().Publish(false);
                                      }
                                  }, (o, e) => { e.CanExecute = true; }));

            CommandBindings.Add(new CommandBinding(ReportCommands.ScrollReportUpCommand,
                                  (o, e) =>
                                  {
                                      var vwr = VisualHelperEx.FindVisualChildByName<ScrollViewer>(ReportViewer, "PageScrollViewer");
                                      if (vwr != null)
                                          vwr.ScrollToTop();
                                  },
                                  (o, e) => { e.CanExecute = true; }));

            CommandBindings.Add(new CommandBinding(ReportCommands.ScrollReportDownCommand,
                                  (o, e) =>
                                  {
                                      var vwr = VisualHelperEx.FindVisualChildByName<ScrollViewer>(ReportViewer, "PageScrollViewer");
                                      if (vwr != null)
                                          vwr.ScrollToBottom();
                                  }, (o, e) => { e.CanExecute = true; }));

            CommandBindings.Add(new CommandBinding(ReportCommands.PrintQuickReportCommand,
                                  (o, e) =>
                                  {
                                      DoEvents();
                                      _rptController.PrintReport();
                                      DoEvents();
                                  }, (o, e) => { e.CanExecute = true; }));
        }

        #endregion
        [SecurityPermissionAttribute(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public void DoEvents()
        {
            var frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }    
        
        void MainReportView_Loaded(object sender, RoutedEventArgs e)
        {
            var reportSourceInstance = new InstanceReportSource {ReportDocument = _rptController.ReportToView};
            ReportViewer.ReportSource = reportSourceInstance;
        }

        private void ReportViewer_OnLoaded(object sender, RoutedEventArgs e)
        {
            var layoutRoot = (FrameworkElement)VisualTreeHelper.GetChild(ReportViewer, 0);
            DataContext = layoutRoot.DataContext;

            var scrollViewer = VisualHelperEx.FindVisualChildByName<ScrollViewer>(ReportViewer, "PageScrollViewer");

            scrollViewer.Focus();
        }
    }
}
