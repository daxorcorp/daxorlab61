﻿using System;
using System.ComponentModel;
using System.Linq;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Telerik.Reporting;


namespace Daxor.Lab.ReportViewer
{
    public abstract class ReportControllerBase : INotifyPropertyChanged
    {
        #region Events
 
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties

        protected IRegionManager RegionManager { get; set; }
        protected MainReportView View { get; set; }
        protected IEventAggregator EventAggregator { get; set; }
        protected ILoggerFacade Logger { get; set; }
        protected ISettingsManager SettingsManager { get; set; }
        public abstract IReportDocument ReportToView { get; }

        #endregion

        #region Abstract Methods

        public abstract void ExportReport();
        public abstract void PrintReport();
        public abstract void LoadReportData();

        #endregion

        #region Concrete Methods
    
        /// <summary>
        /// Loads a report based on the test associated with the report controller and activates the MainReportView with
        /// the report that was generated.
        /// </summary>
        public virtual void ShowReport()
        {
            EventAggregator.GetEvent<ViewReportExecuteChanged>().Publish(true);

            LoadReportData();

            try
            {
                var reportRegion = RegionManager.Regions[RegionNames.ReportRegion];
                View = new MainReportView(this, SettingsManager, EventAggregator) {Region = reportRegion};

                if (!reportRegion.ActiveViews.Any())
                    reportRegion.Add(View, RegionNames.ReportRegion);

                reportRegion.Activate(View);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message, Category.Exception, Priority.High);
            }
        }   

        #endregion

        protected virtual string BuildFileName(string id)
        {
            var fileName = id + "-" + DateTime.Now.ToString("MM-dd-yyyy hh-mm-ss tt");
            return ReplaceInvalidCharacters(fileName);
        }

        protected string ReplaceInvalidCharacters(string fileName)
        {
            fileName = fileName.Replace("\\", "-");
            fileName = fileName.Replace("/", "-");
            fileName = fileName.Replace(":", "-");
            fileName = fileName.Replace("*", "-");
            fileName = fileName.Replace("?", "-");
            fileName = fileName.Replace("\"", "-");
            fileName = fileName.Replace(">", "-");
            fileName = fileName.Replace("<", "-");
            fileName = fileName.Replace("|", "-");

            return fileName;
        }

        #region Event Handlers

        protected virtual void FirePropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
