﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Daxor.Lab.ReportViewer.SubViewModels;

namespace Daxor.Lab.ReportViewer.SubViews
{
    /// <summary>
    /// Interaction logic for PrintReportDialogInterface.xaml
    /// </summary>
    public partial class PrintReportDialogInterface : UserControl
    {
        public PrintReportDialogInterface(PrintReportViewModel vm)
        {
            InitializeComponent();

            DataContext = vm;
        }

        private void ReportStyle_Click(object sender, RoutedEventArgs e)
        {
            switch (((RadioButton)sender).Name)
            {
                case "ReportStyle_Full":
                    ((PrintReportViewModel)DataContext).SelectedReportStyle = Daxor.Lab.ReportViewer.SubViewModels.PrintReportViewModel.ReportStyle.Full;
                    break;
                case "ReportStyle_Physician":
                    ((PrintReportViewModel)DataContext).SelectedReportStyle = Daxor.Lab.ReportViewer.SubViewModels.PrintReportViewModel.ReportStyle.Physicians;
                    break;
                case "ReportStyle_Analyst":
                    ((PrintReportViewModel)DataContext).SelectedReportStyle = Daxor.Lab.ReportViewer.SubViewModels.PrintReportViewModel.ReportStyle.Analysts;
                    break;
				case "ReportStyle_Summary":
					((PrintReportViewModel)DataContext).SelectedReportStyle = Daxor.Lab.ReportViewer.SubViewModels.PrintReportViewModel.ReportStyle.Summary;
					break;
				// v6.05  2019-12-18  Lee Muller added this for the new [Summary] report button.  It still NEEDS a PrintReportViewModel.ReportStyle.Summary
            }
        }

		private void ReportStyle_Full_Checked(object sender, RoutedEventArgs e)
		{

		}

		private void ReportStyle_Analyst_Checked(object sender, RoutedEventArgs e)
		{

		}

		private void ReportStyle_Physician_Checked(object sender, RoutedEventArgs e)
		{

		}

		private void ReportStyle_Summary_Checked(object sender, RoutedEventArgs e)
		{
			// v6.05  2019-12-18  Lee Muller added this and CASE statement above, for the new [Summary] report button.
		}
    }
}
