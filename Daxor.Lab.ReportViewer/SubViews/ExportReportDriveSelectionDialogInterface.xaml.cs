﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Daxor.Lab.ReportViewer.SubViewModels;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.ReportViewer.SubViews
{
    /// <summary>
    /// Interaction logic for ExportReportDriveSelectionDialogInterface.xaml
    /// </summary>
    public partial class ExportReportDriveSelectionDialogInterface : UserControl
    {
        private object dummyNode = new Object();
        private ILoggerFacade _logger;

        public ExportReportDriveSelectionDialogInterface(ExportReportDriveSelectionViewModel vm, ILoggerFacade logger)
        {
            InitializeComponent();
            DataContext = vm;
            _logger = logger;
            textBlock_DirectoryLabel.Text = "Selected Folder:";
            Thread thread = new Thread(SetListCaption);
            thread.Start();
        }

        private void folder_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;

            if (item.Items.Count == 1 && item.Items[0] == dummyNode)
            {
                item.Items.Clear();
                try
                {
                    foreach (string s in Directory.GetDirectories(item.Tag.ToString()))
                    {
                        TreeViewItem subitem = new TreeViewItem();
                        subitem.Header = s.Substring(s.LastIndexOf("\\") + 1);
                        subitem.Tag = s;
                        subitem.FontWeight = FontWeights.Normal;
                        subitem.Style = treeView_DirectoryPath.ItemContainerStyle;
                        subitem.Items.Add(dummyNode);
                        subitem.Expanded += new RoutedEventHandler(folder_Expanded);
                        item.Items.Add(subitem);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Log(ex.Message, Category.Exception, Priority.High);
                }
            }
        }

        private void treeView_DirectoryPath_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            try
            {
                ((ExportReportDriveSelectionViewModel)DataContext).DirectoryPath = ((TreeViewItem)((TreeView)sender).SelectedItem).Tag.ToString();

                string s = ((ExportReportDriveSelectionViewModel)DataContext).DirectoryPath;

                if (s.Last<char>() != '\\')
                    ((ExportReportDriveSelectionViewModel)DataContext).DirectoryPath += @"\";
            }
            catch (Exception ex)
            {
                _logger.Log(ex.Message, Category.Exception, Priority.High);
            }
        }

        private void DriveList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            treeView_DirectoryPath.Items.Clear();
            ((ExportReportDriveSelectionViewModel)DataContext).DirectoryPath = "Select Folder";
            if (DriveList.SelectedItem == null)
                return;

            DriveListClass dlc = (DriveListClass)DriveList.SelectedItem;
            TreeViewItem item = new TreeViewItem();
            item.Header = ((DriveInfo)dlc.Tag).RootDirectory.ToString();
            item.Tag = ((DriveInfo)dlc.Tag).RootDirectory.ToString();
            item.FontWeight = FontWeights.Bold;
            item.Items.Add(dummyNode);
            item.Expanded += new RoutedEventHandler(folder_Expanded);
            treeView_DirectoryPath.Items.Add(item);

        }

        private void SetListCaption()
        {
            TextBlock textBlock = null;
            while (textBlock == null)
            {
                textBlock = (TextBlock)DriveList.Template.FindName("PART_TopListCaption", DriveList);

                if (textBlock != null)
                {
                    Dispatcher.BeginInvoke(new Action(delegate()
                    {
                        textBlock.Text = "Available Drives";
                    }
                                        ));
                }
                else
                    Thread.Sleep(100);
            }
        }


    }
}
