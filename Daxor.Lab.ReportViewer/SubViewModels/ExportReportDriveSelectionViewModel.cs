﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.Base;
using Microsoft.Practices.Composite.Presentation.Commands;
using System.Management;
using System.Collections.ObjectModel;
using System.IO;
using Daxor.Lab.Infrastructure.DomainModels;
using System.Diagnostics;
using Daxor.Lab.Infrastructure.Threading;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.ReportViewer.SubViewModels
{
    public class DriveListClass
    {
        public string Content { get; set; }

        public Object Tag { get; set; }

        public bool IsUSB { get; set; }

        public override string ToString()
        {
            return Content;
        }
    }

    public class ExportReportDriveSelectionViewModel : ViewModelBase, IDisposable 
    {
        private string _directoryPath = string.Empty;
        private static ManagementEventWatcher USBDriveInsertedWatcher = null;
        private static ManagementEventWatcher USBDriveRemovedWatcher = null;
        
        public static DelegateCommand<object> RefreshDrives { get; set; }

        public ThreadSafeObservableCollection<DriveListClass> DriveList { get; set; }
        
        private object dummyNode = new Object();

        private readonly ILoggerFacade _logger;

        public ExportReportDriveSelectionViewModel(ILoggerFacade logger)
        {
            DirectoryPath = "Select Folder";
            AddRemoveUSBHandler();
            AddInsertUSBHandler();
            DriveList = new ThreadSafeObservableCollection<DriveListClass>();
            _logger = logger;
            ExportReportDriveSelectionViewModel.RefreshDrives = new DelegateCommand<object>(delegate(object o) { IdealDispatcher.BeginInvoke((Action)delegate() { LoadDrives(); } ); }, o => { return true; });
            ExportReportDriveSelectionViewModel.RefreshDrives.Execute(null);        
        }

        public string DirectoryPath
        {
            get
            {
                return _directoryPath;
            }
            set
            {
                _directoryPath = value;
                FirePropertyChanged("DirectoryPath");
            }
        }
               
        private void LoadDrives()
        {
            try
            {

                DriveList.Clear();

                DriveInfo[] drives = DriveInfo.GetDrives();

                foreach (DriveInfo di in drives)
                {
                    if (di.DriveType == DriveType.Network || di.DriveType == DriveType.Removable)
                    {
                        string imagePath = string.Empty;

                        DriveListClass dlc = new DriveListClass()
                        {
                            Content = di.Name,
                            Tag = di,
                            IsUSB = di.DriveType == DriveType.Removable,
                        };

                        DriveList.Add(dlc);
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Log(ex.Message, Category.Exception, Priority.High);
            }
        }

        private void AddRemoveUSBHandler()
        {
            WqlEventQuery q;
            ManagementScope scope = new ManagementScope("root\\CIMV2");
            scope.Options.EnablePrivileges = true;
            try
            {
                q = new WqlEventQuery();
                q.EventClassName = "__InstanceDeletionEvent";
                q.WithinInterval = new TimeSpan(0, 0, 3);
                q.Condition = "TargetInstance ISA 'Win32_USBControllerdevice'";
                USBDriveRemovedWatcher = new ManagementEventWatcher(scope, q);
                USBDriveRemovedWatcher.EventArrived += USBDriveEvent;
                USBDriveRemovedWatcher.Start();
            }
            catch (Exception e)
            {
                _logger.Log(e.Message, Category.Exception, Priority.High);
                if (USBDriveRemovedWatcher != null)
                    USBDriveRemovedWatcher.Stop();
            }
        }

        private void AddInsertUSBHandler()
        {
            WqlEventQuery q;
            ManagementScope scope = new ManagementScope("root\\CIMV2");
            scope.Options.EnablePrivileges = true;
            try
            {
                q = new WqlEventQuery();
                q.EventClassName = "__InstanceCreationEvent";
                q.WithinInterval = new TimeSpan(0, 0, 3);
                q.Condition = "TargetInstance ISA 'Win32_USBControllerdevice'";
                USBDriveInsertedWatcher = new ManagementEventWatcher(scope, q);
                USBDriveInsertedWatcher.EventArrived += USBDriveEvent;
                USBDriveInsertedWatcher.Start();
            }
            catch (Exception e)
            {
                _logger.Log(e.Message, Category.Exception, Priority.High);
                if (USBDriveInsertedWatcher != null)
                    USBDriveInsertedWatcher.Stop();
            }
        }

        private static void USBDriveEvent(object sender, EventArrivedEventArgs e)
        {
            RefreshDrives.Execute(null);
        }
        protected override void OnDispose()
        {
            base.OnDispose();
            if (USBDriveInsertedWatcher != null)
            {
                USBDriveInsertedWatcher.Stop();
                USBDriveInsertedWatcher.Dispose();
                USBDriveInsertedWatcher = null;
            }

            if (USBDriveRemovedWatcher != null)
            {
                USBDriveRemovedWatcher.Stop();
                USBDriveRemovedWatcher.Dispose();
                USBDriveRemovedWatcher = null;
            }
        }
    }
}
