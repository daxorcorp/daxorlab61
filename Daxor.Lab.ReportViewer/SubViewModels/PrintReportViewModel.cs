﻿using System;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.ReportViewer.SubViewModels
{
    public class PrintReportViewModel : ViewModelBase
    {
        public enum ReportStyle
        {
            Undefined,
            Full,
            Physicians,
            Analysts,
			Summary
        }

        private readonly ISettingsManager _settingsManager;
        private ReportStyle _reportStyle;
        private bool _includeFindings;
        private bool _includeAuditTrail;

        public PrintReportViewModel(ISettingsManager settingsManager)
        {
            if (settingsManager == null) throw new ArgumentNullException("settingsManager");
            _settingsManager = settingsManager;

            SelectedReportStyle = ReportStyle.Full;

            var settingObserver = new SettingObserver<ISettingsManager>(_settingsManager);
            settingObserver.RegisterHandler(SettingKeys.BvaReportDefaultShowReportFindings, 
                s => IncludeFindings = s.GetSetting<bool>(SettingKeys.BvaReportDefaultShowReportFindings));
            settingObserver.RegisterHandler(SettingKeys.BvaReportDefaultShowAuditTrail, 
                s => IncludeAuditTrail = s.GetSetting<bool>(SettingKeys.BvaReportDefaultShowAuditTrail));
        }

        public ReportStyle SelectedReportStyle 
        {
            get { return _reportStyle; }

            set
            {
				// I Feel like base is getting a its prop _reportStyle set here so i feel like this insinuates that base is the bvaReport and we need to somehow change this to if
				// summary is chosen then we need to change the base to use the BvaQuickReportController.
                if (!base.SetValue(ref _reportStyle, "SelectedReportStyle", value))
                    return;

                switch(_reportStyle)
                {
                    case ReportStyle.Full:
                        IncludeAuditTrail = _settingsManager.GetSetting<bool>(SettingKeys.BvaReportDefaultShowAuditTrail);
                        IncludeFindings = _settingsManager.GetSetting<bool>(SettingKeys.BvaReportDefaultShowReportFindings);
                        break;

                    case ReportStyle.Physicians:
                        IncludeFindings = _settingsManager.GetSetting<bool>(SettingKeys.BvaReportDefaultShowReportFindings);
                        IncludeAuditTrail = false;
                        break;

                    case ReportStyle.Analysts:
                        IncludeAuditTrail = false;
                        IncludeFindings = false;
                        break;

					case ReportStyle.Summary:				
					// v6.05  2019-12-19   L Muller    Added     Still NEEDS the actual Summary Report
						IncludeAuditTrail = false;
						IncludeFindings = false;
						break;
                }
            }
        }

        public bool IncludeFindings 
        {
            get { return _includeFindings; }
            set { base.SetValue(ref _includeFindings, "IncludeFindings", value); } 
        }

        public bool IncludeAuditTrail 
        {
            get { return _includeAuditTrail; }
            set { base.SetValue(ref _includeAuditTrail, "IncludeAuditTrail", value); }
        }
    }
}
