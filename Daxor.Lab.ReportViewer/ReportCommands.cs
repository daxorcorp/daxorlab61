﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace Daxor.Lab.ReportViewer
{
    public static class ReportCommands
    {
        public static RoutedCommand CloseReportCommand = new RoutedCommand();
        
        public static RoutedCommand ScrollReportUpCommand = new RoutedCommand();

        public static RoutedCommand ScrollReportDownCommand = new RoutedCommand();

        public static RoutedCommand PrintReportCommand = new RoutedCommand();

		public static RoutedCommand PrintQuickReportCommand = new RoutedCommand();
    }
}
