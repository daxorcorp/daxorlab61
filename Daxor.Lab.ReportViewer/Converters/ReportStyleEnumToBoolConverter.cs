﻿using System;
using System.Windows.Data;

namespace Daxor.Lab.ReportViewer.Converters
{
    public class ReportStyleEnumToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var parameterString = parameter as string;

            var style = (SubViewModels.PrintReportViewModel.ReportStyle)value;

            if (style == SubViewModels.PrintReportViewModel.ReportStyle.Physicians)
                return parameterString != "AuditTrail";
            
            return style != SubViewModels.PrintReportViewModel.ReportStyle.Analysts;
        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
