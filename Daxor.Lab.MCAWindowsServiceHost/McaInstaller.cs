﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Daxor.Lab.MCAWindowsServiceHost
{
    [RunInstaller(true)]
    public class McaInstaller : Installer
    {
        private ServiceProcessInstaller processInstaller;
        private ServiceInstaller serviceInstaller;

        public McaInstaller()
        {
            processInstaller = new ServiceProcessInstaller();
            processInstaller.Account = ServiceAccount.LocalSystem;
            
            serviceInstaller = new ServiceInstaller();
            serviceInstaller.ServiceName = "McaServiceHost";
            serviceInstaller.DisplayName = "Daxor Multichannel Analyzer Service";
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.Description = "WCF service host for the multichannel analyzer service.";

            Installers.Add(processInstaller);
            Installers.Add(serviceInstaller);

            serviceInstaller.AfterInstall += new InstallEventHandler(serviceInstaller_AfterInstall);
        }

        void serviceInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            ServiceController sc = new ServiceController("McaServiceHost");
            sc.Start();
        }
    }
}
