﻿using System.ServiceModel;
using System.ServiceProcess;
using Daxor.Lab.MCAService;
using System;
using System.Diagnostics;

namespace Daxor.Lab.MCAWindowsServiceHost
{
    public partial class McaHost : ServiceBase
    {
        #region Ctor

        public McaHost()
        {
            InitializeComponent();
            this.ServiceName = "McaServiceHost";
        }

        #endregion

        public static void Main()
        {
            ServiceBase.Run(new McaHost());
        }

        #region Properties

        public ServiceHost McaServiceHost { get; protected set; }
        public MultiChannelAnalyzerService McaService { get; protected set; }

        #endregion

        #region Service event handlers

        protected override void OnStart(string[] args)
        {
            if (McaServiceHost != null)
                TerminateMcaService();

            try
            {
                McaService = new MultiChannelAnalyzerService();
            }
            catch (Exception ex)
            {
                this.EventLog.WriteEntry("Exception on start: " + ex.Message, EventLogEntryType.Error);
            }
            McaServiceHost = new ServiceHost(McaService);
            McaServiceHost.Open();
            McaServiceHost.Faulted += new EventHandler(OnServiceFaulted);

            string baseAddresses = "";
            foreach (Uri address in McaServiceHost.BaseAddresses)
                baseAddresses += " " + address.AbsoluteUri;
            string s = String.Format("{0} listening at {1}", this.ServiceName, baseAddresses);
            this.EventLog.WriteEntry(s, EventLogEntryType.Information);
        }
        protected override void OnStop()
        {
            TerminateMcaService();
        }

        #endregion

        #region Helpers

        private void TerminateMcaService()
        {
            if (McaService != null)
            {
                foreach (string mcaName in McaService.GetAllMultiChannelAnalyzerIds())
                    McaService.ShutDown(mcaName);
            }
            if (McaServiceHost != null)
            {
                McaServiceHost.Close();
                McaServiceHost = null;
            }
        }

        protected void OnServiceFaulted(object sender, EventArgs e)
        {
            string s = string.Format("{0} has faulted", this.ServiceName);
            this.EventLog.WriteEntry(s, EventLogEntryType.Error);
        }

        #endregion
    }
}
