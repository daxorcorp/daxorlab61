using System.Collections.Generic;
using Daxor.Lab.ReportViewer.SubViewModels;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.ReportViewer.Tests.SubViewModels_Tests.PrintReportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_include_audit_trail_property
    {
        [TestMethod]
        public void Then_the_getter_returns_the_correct_state()
        {
            var dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            
            var viewModel = new PrintReportViewModel(dummySettingsManager) {IncludeAuditTrail = false};
            Assert.IsFalse(viewModel.IncludeAuditTrail);

            viewModel.IncludeAuditTrail = true;
            Assert.IsTrue(viewModel.IncludeAuditTrail);
        }

        [TestMethod]
        public void And_changing_the_state_then_PropertyChanged_is_raised()
        {
            var dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();

            var viewModel = new PrintReportViewModel(dummySettingsManager) { IncludeAuditTrail = false };

            var propertiesChanged = new List<string>();
            viewModel.PropertyChanged += (sender, args) => propertiesChanged.Add(args.PropertyName);

            viewModel.IncludeAuditTrail = true;   // First change
            viewModel.IncludeAuditTrail = false;  // Second change
            
            CollectionAssert.AreEqual(new List<string>{"IncludeAuditTrail", "IncludeAuditTrail"}, propertiesChanged);
        }

        [TestMethod]
        public void And_not_changing_the_state_then_PropertyChanged_is_not_raised()
        {
            var dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();

            var viewModel = new PrintReportViewModel(dummySettingsManager) { IncludeAuditTrail = false };

            var propertiesChanged = new List<string>();
            viewModel.PropertyChanged += (sender, args) => propertiesChanged.Add(args.PropertyName);

            viewModel.IncludeAuditTrail = false;

            Assert.AreEqual(0, propertiesChanged.Count);
        }
    }
    // ReSharper restore InconsistentNaming
}
