using System.Collections.Generic;
using Daxor.Lab.ReportViewer.SubViewModels;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.ReportViewer.Tests.SubViewModels_Tests.PrintReportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_include_findings_property
    {
        [TestMethod]
        public void Then_the_getter_returns_the_correct_state()
        {
            var dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();

            var viewModel = new PrintReportViewModel(dummySettingsManager) { IncludeFindings = false };
            Assert.IsFalse(viewModel.IncludeFindings);

            viewModel.IncludeFindings = true;
            Assert.IsTrue(viewModel.IncludeFindings);
        }

        [TestMethod]
        public void And_changing_the_state_then_PropertyChanged_is_raised()
        {
            var dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();

            var viewModel = new PrintReportViewModel(dummySettingsManager) { IncludeFindings = false };

            var propertiesChanged = new List<string>();
            viewModel.PropertyChanged += (sender, args) => propertiesChanged.Add(args.PropertyName);

            viewModel.IncludeFindings = true;   // First change
            viewModel.IncludeFindings = false;  // Second change

            CollectionAssert.AreEqual(new List<string> { "IncludeFindings", "IncludeFindings" }, propertiesChanged);
        }

        [TestMethod]
        public void And_not_changing_the_state_then_PropertyChanged_is_not_raised()
        {
            var dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();

            var viewModel = new PrintReportViewModel(dummySettingsManager) { IncludeFindings = false };

            var propertiesChanged = new List<string>();
            viewModel.PropertyChanged += (sender, args) => propertiesChanged.Add(args.PropertyName);

            viewModel.IncludeFindings = false;

            Assert.AreEqual(0, propertiesChanged.Count);
        }
    }
    // ReSharper restore InconsistentNaming
}
