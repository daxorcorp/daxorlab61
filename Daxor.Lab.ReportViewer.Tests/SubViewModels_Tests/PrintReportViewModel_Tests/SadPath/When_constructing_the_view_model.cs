using Daxor.Lab.ReportViewer.SubViewModels;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.ReportViewer.Tests.SubViewModels_Tests.PrintReportViewModel_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_the_view_model
    {
        [TestMethod]
        public void And_the_settings_manager_instance_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once UnusedVariable
            AssertEx.ThrowsArgumentNullException(() => { var unused = new PrintReportViewModel(null); },
                "settingsManager");
        }
    }
    // ReSharper restore InconsistentNaming
}
