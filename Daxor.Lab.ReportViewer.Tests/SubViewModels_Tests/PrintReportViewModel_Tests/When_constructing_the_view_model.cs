using Daxor.Lab.ReportViewer.SubViewModels;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.ReportViewer.Tests.SubViewModels_Tests.PrintReportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_the_view_model
    {
        [TestMethod]
        public void Then_the_default_report_style_is_full()
        {
            var dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var viewModel = new PrintReportViewModel(dummySettingsManager);

            Assert.AreEqual(viewModel.SelectedReportStyle, PrintReportViewModel.ReportStyle.Full);
        }
    }
    // ReSharper restore InconsistentNaming
}
