using System.Collections.Generic;
using Daxor.Lab.ReportViewer.SubViewModels;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.ReportViewer.Tests.SubViewModels_Tests.PrintReportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_selected_report_style_property
    {
        [TestMethod]
        public void Then_the_getter_returns_the_correct_state()
        {
            var dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var viewModel = new PrintReportViewModel(dummySettingsManager);
            const PrintReportViewModel.ReportStyle expectedReportStyle = PrintReportViewModel.ReportStyle.Physicians;

            viewModel.SelectedReportStyle = expectedReportStyle;

            Assert.AreEqual(viewModel.SelectedReportStyle, expectedReportStyle);
        }

        [TestMethod]
        public void And_setting_the_report_style_to_full_then_both_the_report_findings_and_audit_trail_are_based_on_settings()
        {
            var mockSettingsManager = MockRepository.GenerateMock<ISettingsManager>();
            mockSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaReportDefaultShowReportFindings))
                               .Return(true);
            mockSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaReportDefaultShowAuditTrail))
                               .Return(true);

            var viewModel = new PrintReportViewModel(mockSettingsManager); // Full is the default style

            Assert.IsTrue(viewModel.IncludeFindings);
            Assert.IsTrue(viewModel.IncludeAuditTrail);

            mockSettingsManager.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_setting_the_report_style_to_physician_then_report_findings_are_based_on_settings_but_the_audit_trail_is_disabled()
        {
            var mockSettingsManager = MockRepository.GenerateMock<ISettingsManager>();
            mockSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaReportDefaultShowReportFindings))
                               .Return(true);

            var viewModel = new PrintReportViewModel(mockSettingsManager)
            {
                SelectedReportStyle = PrintReportViewModel.ReportStyle.Physicians
            };

            Assert.IsTrue(viewModel.IncludeFindings);
            Assert.IsFalse(viewModel.IncludeAuditTrail);

            mockSettingsManager.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_setting_the_report_style_to_analyst_then_neither_the_report_findings_nor_the_audit_trail_are_included()
        {
            var dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var viewModel = new PrintReportViewModel(dummySettingsManager)
            {
                SelectedReportStyle = PrintReportViewModel.ReportStyle.Analysts
            };

            Assert.IsFalse(viewModel.IncludeFindings);
            Assert.IsFalse(viewModel.IncludeAuditTrail);
        }

        [TestMethod]
        public void And_changing_the_report_style_then_PropertyChanged_is_raised()
        {
            var dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var viewModel = new PrintReportViewModel(dummySettingsManager);

            var propertiesChanged = new List<string>();
            viewModel.PropertyChanged += (sender, args) => propertiesChanged.Add(args.PropertyName);

            viewModel.SelectedReportStyle = PrintReportViewModel.ReportStyle.Analysts;

            CollectionAssert.Contains(propertiesChanged, "SelectedReportStyle");
        }

        [TestMethod]
        public void And_not_changing_the_report_style_then_PropertyChanged_is_not_raised()
        {
            var dummySettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var viewModel = new PrintReportViewModel(dummySettingsManager);

            var propertiesChanged = new List<string>();
            viewModel.PropertyChanged += (sender, args) => propertiesChanged.Add(args.PropertyName);

            // Full is the default report style
            viewModel.SelectedReportStyle = PrintReportViewModel.ReportStyle.Full;

            Assert.AreEqual(0, propertiesChanged.Count);
        }
    }
    // ReSharper restore InconsistentNaming
}
