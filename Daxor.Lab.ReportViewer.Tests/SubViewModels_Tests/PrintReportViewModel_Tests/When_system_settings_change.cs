using Daxor.Lab.ReportViewer.SubViewModels;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.ReportViewer.Tests.SubViewModels_Tests.PrintReportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_system_settings_change
    {
        [TestMethod]
        public void And_the_show_report_findings_setting_is_disabled_then_findings_should_not_be_included()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaReportDefaultShowReportFindings))
                               .Return(true)
                               .Repeat
                               .Once();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaReportDefaultShowReportFindings))
                               .Return(false)
                               .Repeat
                               .Once();
            var viewModel = new PrintReportViewModel(stubSettingsManager);

            stubSettingsManager.Raise(m => m.SettingsChanged += null, stubSettingsManager,
                new SettingsChangedEventArgs(new [] { SettingKeys.BvaReportDefaultShowReportFindings }));

            Assert.IsFalse(viewModel.IncludeFindings);
        }

        [TestMethod]
        public void And_the_show_report_findings_setting_is_enabled_then_findings_should_be_included()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaReportDefaultShowReportFindings))
                               .Return(false)
                               .Repeat
                               .Once();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaReportDefaultShowReportFindings))
                               .Return(true)
                               .Repeat
                               .Once();
            var viewModel = new PrintReportViewModel(stubSettingsManager);

            stubSettingsManager.Raise(m => m.SettingsChanged += null, stubSettingsManager,
                new SettingsChangedEventArgs(new[] { SettingKeys.BvaReportDefaultShowReportFindings }));

            Assert.IsTrue(viewModel.IncludeFindings);
        }

        [TestMethod]
        public void And_the_show_audit_trail_setting_is_disabled_then_the_audit_trail_should_not_be_included()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaReportDefaultShowAuditTrail))
                               .Return(true)
                               .Repeat
                               .Once();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaReportDefaultShowAuditTrail))
                               .Return(false)
                               .Repeat
                               .Once();
            var viewModel = new PrintReportViewModel(stubSettingsManager);

            stubSettingsManager.Raise(m => m.SettingsChanged += null, stubSettingsManager,
                new SettingsChangedEventArgs(new[] { SettingKeys.BvaReportDefaultShowAuditTrail }));

            Assert.IsFalse(viewModel.IncludeAuditTrail);
        }

        [TestMethod]
        public void And_the_show_audit_trail_setting_is_enabled_then_the_audit_trail_should_be_included()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaReportDefaultShowAuditTrail))
                               .Return(false)
                               .Repeat
                               .Once();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaReportDefaultShowAuditTrail))
                               .Return(true)
                               .Repeat
                               .Once();
            var viewModel = new PrintReportViewModel(stubSettingsManager);

            stubSettingsManager.Raise(m => m.SettingsChanged += null, stubSettingsManager,
                new SettingsChangedEventArgs(new[] { SettingKeys.BvaReportDefaultShowAuditTrail }));

            Assert.IsTrue(viewModel.IncludeAuditTrail);
        }
    }
    // ReSharper restore InconsistentNaming
}
