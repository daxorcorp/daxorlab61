using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.WindowsEventLogger_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_logging_an_entry
    {
        private static WindowsEventLogger _logger;

        [ClassInitialize]
        public static void BeforeTests(TestContext context)
        {
            _logger = new WindowsEventLogger("DaxorLabTestLog", "DaxorLab Integration Test Log");
        }

        [ClassCleanup]
        public static void AfterTests()
        {
            Helpers.ClearEventLog();
        }

        [TestMethod]
        public void Then_that_message_is_logged()
        {
            _logger.Log("Message here");

            Assert.AreEqual("Message here", Helpers.GetLastLogEntry().Message);
        }
    }
    // ReSharper restore InconsistentNaming
}
