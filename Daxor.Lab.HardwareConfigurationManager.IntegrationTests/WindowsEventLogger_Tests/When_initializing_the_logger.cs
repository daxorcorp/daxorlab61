﻿using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.WindowsEventLogger_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_logger
    {
        [ClassCleanup]
        public static void AfterTests()
        {
            Helpers.ClearEventLog();
        }

        [TestInitialize]
        public void BeforeEachTest()
        {
            RemoveEventSourceAndWriteAnEntryToCreateEventLog();
        }

        [TestMethod]
        public void And_the_event_source_does_not_exist_then_it_is_created()
        {
            Assert.IsTrue(EventLog.SourceExists(Helpers.EventSourceName));
        }

        [TestMethod]
        public void Then_the_maximum_log_size_is_1_GB()
        {
            Assert.AreEqual(1024 * 1024, Helpers.GetEventLog().MaximumKilobytes);
        }

        [TestMethod]
        public void Then_the_overflow_action_is_to_overwrite_as_needed()
        {
            Assert.AreEqual(OverflowAction.OverwriteAsNeeded, Helpers.GetEventLog().OverflowAction);
        }

        [TestMethod]
        public void Then_there_is_no_minimum_number_of_days_to_keep_entries()
        {
            Assert.AreEqual(0, Helpers.GetEventLog().MinimumRetentionDays);
        }

        private static void RemoveEventSourceAndWriteAnEntryToCreateEventLog()
        {
            if (EventLog.SourceExists(Helpers.EventSourceName))
                EventLog.DeleteEventSource(Helpers.EventSourceName);
            Assert.IsFalse(EventLog.SourceExists(Helpers.EventSourceName));

            var logger = new WindowsEventLogger(Helpers.EventSourceName, Helpers.EventLogFriendlyName);
            logger.Log("Message here");
        }
    }
    // ReSharper restore InconsistentNaming
}
