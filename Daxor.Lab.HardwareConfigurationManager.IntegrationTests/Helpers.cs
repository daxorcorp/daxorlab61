﻿using System.Diagnostics;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests
{
    public static class Helpers
    {
        public const string EventSourceName = "DaxorLabTestLog";

        public const string EventLogFriendlyName = "DaxorLab Integration Test Log";

        public static EventLog GetEventLog()
        {
            var logName = EventLog.LogNameFromSourceName(EventSourceName, ".");
            return new EventLog(logName);
        }

        public static void ClearEventLog()
        {
            GetEventLog().Clear();
        }

        public static EventLogEntry GetLastLogEntry()
        {
            var eventLog = GetEventLog();
            return eventLog.Entries[eventLog.Entries.Count - 1];
        }

        public static EventLogEntry[] GetEventLogEntries()
        {
            var logEntries = new EventLogEntry[GetEventLog().Entries.Count];
            GetEventLog().Entries.CopyTo(logEntries, 0);

            return logEntries;
        }
    }
}
