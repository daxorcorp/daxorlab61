using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_reading_double_valued_settings
    {
        private Dictionary<string, HardwareSetting> _configurations;

        [ClassInitialize]
        public static void BeforeAllTests(TestContext context)
        {
            Directory.CreateDirectory(@"C:\temp\");
        }

        [TestInitialize]
        public void BeforeEachTest()
        {
            var repository = new HardwareConfigurationRepository(HardwareConfigurationRepositoryTestHelpers.DummyFile, "default");
            HardwareConfigurationRepositoryTestHelpers.WriteOneOfEachSettingType();
            _configurations = repository.ReadAll();
        }

        [TestCleanup]
        public void AfterEachTest()
        {
            File.Delete(HardwareConfigurationRepositoryTestHelpers.DummyFile);
        }

        [TestMethod]
        public void Then_the_value_is_set()
        {
            Assert.AreEqual(1.234, _configurations["Double Setting"].GetValue<double>());
        }
    }
    // ReSharper restore InconsistentNaming
}
