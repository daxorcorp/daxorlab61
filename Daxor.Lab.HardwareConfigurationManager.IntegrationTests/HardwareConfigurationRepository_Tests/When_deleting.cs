using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_deleting
    {
        private const string SourceFileName = @"c:\temp\source.txt";

        [ClassInitialize]
        public static void BeforeAllTests(TestContext context)
        {
            Directory.CreateDirectory(@"C:\temp\");
        }

        [TestMethod]
        public void And_the_current_filename_exists_then_the_file_is_deleted()
        {
            File.WriteAllText(SourceFileName, "Source content");
            var repository = new HardwareConfigurationRepository(SourceFileName, "default");

            repository.TryAndDelete();

            Assert.IsFalse(File.Exists(SourceFileName));
        }

        [TestMethod]
        public void And_the_current_filename_exists_then_the_operation_is_considered_successful()
        {
            File.WriteAllText(SourceFileName, "Source content");
            var repository = new HardwareConfigurationRepository(SourceFileName, "default");

            Assert.IsTrue(repository.TryAndDelete());
        }

        [TestMethod]
        public void And_the_current_filename_does_not_exist_then_the_operation_is_considered_unsuccessful()
        {
            var repository = new HardwareConfigurationRepository(SourceFileName, "default");
            Assert.IsFalse(File.Exists(SourceFileName));

            Assert.IsFalse(repository.TryAndDelete());
        }
    }
    // ReSharper restore InconsistentNaming
}
