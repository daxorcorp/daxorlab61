using System.IO;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_reading_double_valued_settings
    {
        private IHardwareConfigurationRepository _repository;

        [ClassInitialize]
        public static void BeforeAllTests(TestContext context)
        {
            Directory.CreateDirectory(@"C:\temp\");
        }

        [TestInitialize]
        public void BeforeEachTest()
        {
            _repository = new HardwareConfigurationRepository(HardwareConfigurationRepositoryTestHelpers.DummyFile, "default");
        }

        [TestCleanup]
        public void AfterEachTest()
        {
            File.Delete(HardwareConfigurationRepositoryTestHelpers.DummyFile);
        }

        [TestMethod]
        public void And_the_value_is_empty_then_an_exception_is_thrown()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteDoubleSettingWithEmptyValue();

            AssertEx.Throws(() => _repository.ReadAll());
        }

        [TestMethod]
        public void And_the_value_attribute_is_missing_then_an_exception_is_thrown()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteDoubleSettingWithNoValueElement();

            AssertEx.Throws(() => _repository.ReadAll());
        }

        [TestMethod]
        public void And_the_value_is_not_a_valid_double_then_an_exception_is_thrown()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteDoubleSettingWithInvalidValue();

            AssertEx.Throws(() => _repository.ReadAll());
        }
    }
    // ReSharper restore InconsistentNaming
}
