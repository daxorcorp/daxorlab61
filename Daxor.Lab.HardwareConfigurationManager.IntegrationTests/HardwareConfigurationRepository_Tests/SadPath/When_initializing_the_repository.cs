using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_repository
    {
        [TestMethod]
        public void And_the_filename_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentException>(() => new HardwareConfigurationRepository(null, "default"));
        }

        [TestMethod]
        public void And_the_default_configuration_filename_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentException>(() => new HardwareConfigurationRepository("file", null));
        }
    }
    // ReSharper restore InconsistentNaming
}
