using System.IO;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_reading
    {
        private IHardwareConfigurationRepository _repository;

        [ClassInitialize]
        public static void BeforeAllTests(TestContext context)
        {
            Directory.CreateDirectory(@"C:\temp\");
        }

        [TestInitialize]
        public void BeforeEachTest()
        {
            _repository = new HardwareConfigurationRepository(HardwareConfigurationRepositoryTestHelpers.DummyFile, "default");
        }

        [TestCleanup]
        public void AfterEachTest()
        {
            File.Delete(HardwareConfigurationRepositoryTestHelpers.DummyFile);
        }

        [TestMethod]
        public void And_the_file_cannot_be_read_then_an_exception_is_thrown()
        {
            Assert.IsFalse(File.Exists("whatever"));

            var repository = new HardwareConfigurationRepository("whatever", "default");

            AssertEx.Throws(() => repository.ReadAll());
        }

        [TestMethod]
        public void And_the_name_attribute_is_missing_then_an_exception_is_thrown()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteSettingWithMissingName();

            AssertEx.Throws(() => _repository.ReadAll());
        }

        [TestMethod]
        public void And_the_type_attribute_is_missing_then_an_exception_is_thrown()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteSettingWithMissingType();

            AssertEx.Throws(() => _repository.ReadAll());
        }

        [TestMethod]
        public void And_the_type_value_is_not_recognized_then_an_exception_is_thrown()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteSettingWithUnknownType();

            AssertEx.Throws(() => _repository.ReadAll());
        }

        [TestMethod]
        public void And_the_description_attribute_is_missing_then_an_exception_is_thrown()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteSettingWithMissingDescription();

            AssertEx.Throws(() => _repository.ReadAll());
        }

        [TestMethod]
        public void And_loading_a_setting_with_the_same_name_as_an_existing_setting_then_an_exception_is_thrown()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteDuplicateSettings();

            AssertEx.Throws(() => _repository.ReadAll());
        }
    }
    // ReSharper restore InconsistentNaming
}
