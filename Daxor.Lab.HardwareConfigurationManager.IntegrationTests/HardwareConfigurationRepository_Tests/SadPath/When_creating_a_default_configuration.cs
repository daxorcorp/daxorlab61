using System.IO;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_a_default_configuration
    {
        [TestMethod]
        public void And_the_default_file_does_not_exist_then_an_exception_is_thrown()
        {
            const string testFile = @"c:\temp\test.txt";

            File.Delete(testFile);
            Assert.IsFalse(File.Exists(testFile));

            var repository = new HardwareConfigurationRepository("whatever", testFile);
            
            AssertEx.Throws(() => repository.CreateDefaultConfiguration());
        }
    }
    // ReSharper restore InconsistentNaming
}
