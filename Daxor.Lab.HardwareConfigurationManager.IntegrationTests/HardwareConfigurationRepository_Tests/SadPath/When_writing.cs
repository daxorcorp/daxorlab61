using System;
using System.Collections.Generic;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_writing
    {
        [TestMethod]
        public void And_there_is_a_problem_saving_then_an_exception_is_thrown()
        {
            var repository = new HardwareConfigurationRepository("/", "default");

            AssertEx.Throws(() => repository.WriteAll(new Dictionary<string, HardwareSetting>()));
        }

        [TestMethod]
        public void And_given_a_null_configuration_then_an_exception_is_thrown()
        {
            var repository = new HardwareConfigurationRepository("test", "default");

            AssertEx.Throws<ArgumentNullException>(() => repository.WriteAll(null));
        }

        [TestMethod]
        public void And_a_setting_instance_is_null_then_an_exception_is_thrown()
        {
            var repository = new HardwareConfigurationRepository("test", "default");
            var configuration = new Dictionary<string, HardwareSetting>
            {
                {"test", null}
            };

            AssertEx.Throws(() => repository.WriteAll(configuration));
        }
    }
    // ReSharper restore InconsistentNaming
}
