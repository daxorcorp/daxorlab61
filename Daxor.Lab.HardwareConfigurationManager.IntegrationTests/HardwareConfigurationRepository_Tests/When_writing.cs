using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_writing
    {
        private const string SourceFileName = @"c:\temp\source.xml";
        private IHardwareConfigurationRepository _repository;
        
        [ClassInitialize]
        public static void BeforeAllTests(TestContext context)
        {
            Directory.CreateDirectory(@"C:\temp\");
        }

        [TestInitialize]
        public void BeforeEachTest()
        {
            _repository = new HardwareConfigurationRepository(SourceFileName, "default");
        }

        [TestCleanup]
        public void AfterEachTest()
        {
            File.Delete(SourceFileName);
        }

        [TestMethod]
        public void And_the_description_on_the_setting_instance_is_null_then_it_is_written_as_empty()
        {
            var configuration = CreateConfigurationDictionaryWithNullDescription();
            Assert.IsNull(configuration["bad"].Description);

            _repository.WriteAll(configuration);

            var documentRoot = XElement.Load(SourceFileName);
            var settingDescriptions = from x in documentRoot.Elements("setting").Attributes("description") select x;
            Assert.IsTrue(settingDescriptions.Any(s => s.Value == string.Empty));
        }

        [TestMethod]
        public void And_the_value_on_the_setting_instance_is_null_then_it_is_written_as_empty()
        {
            var configuration = CreateConfigurationDictionaryWithNullValue();
            Assert.IsNull(configuration["bad"].GetValue<string>());

            _repository.WriteAll(configuration);

            var documentRoot = XElement.Load(SourceFileName);
            var settingValues = from x in documentRoot.Elements("setting").Attributes("value") select x;
            Assert.IsTrue(settingValues.Any(s => s.Value == string.Empty));
        }

        [TestMethod]
        public void Then_the_root_node_for_the_XML_is_HardwareConfiguration()
        {
            var configurations = CreateConfigurationDictionaryWithMultipleValues();

            _repository.WriteAll(configurations);

            var documentRoot = XElement.Load(SourceFileName);
            Assert.AreEqual("HardwareConfiguration", documentRoot.Name);
        }

        [TestMethod]
        public void And_there_are_no_settings_then_the_root_node_has_no_children()
        {
            _repository.WriteAll(new Dictionary<string, HardwareSetting>());

            var documentRoot = XElement.Load(SourceFileName);
            Assert.IsFalse(documentRoot.HasElements);
        }

        [TestMethod]
        public void Then_all_settings_are_written()
        {
            var configurations = CreateConfigurationDictionaryWithMultipleValues();

            _repository.WriteAll(configurations);

            var documentRoot = XElement.Load(SourceFileName);
            Assert.AreEqual(4, documentRoot.Elements("setting").Count());
        }

        [TestMethod]
        public void Then_each_setting_has_exactly_the_expected_child_elements()
        {
            var configurations = CreateConfigurationDictionaryWithMultipleValues();

            _repository.WriteAll(configurations);

            var documentRoot = XElement.Load(SourceFileName);
            var settingElements = from x in documentRoot.Elements("setting") select x;

            foreach (var settingElement in settingElements)
            {
                Assert.AreEqual(4, settingElement.Attributes().Count());
                Assert.AreEqual(1, settingElement.Attributes("value").Count());
                Assert.AreEqual(1, settingElement.Attributes("name").Count());
                Assert.AreEqual(1, settingElement.Attributes("type").Count());
                Assert.AreEqual(1, settingElement.Attributes("description").Count());
            }
        }

        private static Dictionary<string, HardwareSetting> CreateConfigurationDictionaryWithNullDescription()
        {
            var badSetting = new HardwareSetting("bad", HardwareSetting.StringType);
            badSetting.SetValue("Value");

            var configuration = new Dictionary<string, HardwareSetting>
            {
                {"bad", badSetting}
            };
            return configuration;
        }

        private static Dictionary<string, HardwareSetting> CreateConfigurationDictionaryWithNullValue()
        {
            var badSetting = new HardwareSetting("bad", HardwareSetting.StringType)
            {
                Description = "Desc",
            };

            var configuration = new Dictionary<string, HardwareSetting>
            {
                {"bad", badSetting}
            };
            return configuration;
        }

        private static Dictionary<string, HardwareSetting> CreateConfigurationDictionaryWithMultipleValues()
        {
            var configurations = new Dictionary<string, HardwareSetting>();

            var stringSetting = new HardwareSetting("string setting", HardwareSetting.StringType) { Description = "string description" };
            stringSetting.SetValue("value");
            configurations[stringSetting.Name] = stringSetting;

            var integerSetting = new HardwareSetting("int setting", HardwareSetting.IntegerType) { Description = "integer description" };
            integerSetting.SetValue(-34);
            configurations[integerSetting.Name] = integerSetting;

            var doubleSetting = new HardwareSetting("double setting", HardwareSetting.DoubleType) { Description = "double description" };
            doubleSetting.SetValue(1.234);
            configurations[doubleSetting.Name] = doubleSetting;

            var boolSetting = new HardwareSetting("boolean setting", HardwareSetting.BooleanType) { Description = "boolean description" };
            boolSetting.SetValue(true);
            configurations[boolSetting.Name] = boolSetting;
            
            return configurations;
        }
    }
    // ReSharper restore InconsistentNaming
}
