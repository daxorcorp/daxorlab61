using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_a_repository_exists
    {
        [TestMethod]
        public void And_the_file_is_present_then_the_repository_exists()
        {
            Directory.CreateDirectory(@"C:\temp\");
            HardwareConfigurationRepositoryTestHelpers.WriteOneOfEachSettingType();
            Assert.IsTrue(File.Exists(HardwareConfigurationRepositoryTestHelpers.DummyFile));

            var repository = new HardwareConfigurationRepository(HardwareConfigurationRepositoryTestHelpers.DummyFile, "default");

            Assert.IsTrue(repository.Exists());
            
            File.Delete(HardwareConfigurationRepositoryTestHelpers.DummyFile);
        }

        [TestMethod]
        public void And_the_file_is_absent_then_the_repository_does_not_exist()
        {
            Assert.IsFalse(File.Exists(@"c:\temp\whatever.txt"));

            var repository = new HardwareConfigurationRepository(@"c:\temp\whatever.txt", "default");

            Assert.IsFalse(repository.Exists());
        }
    }
    // ReSharper restore InconsistentNaming
}
