﻿using System.Xml.Linq;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests
{
    public class HardwareConfigurationRepositoryTestHelpers
    {
        public static string DummyFile = @"c:\temp\test.xml";

        public static void WriteSettingWithMixedCase()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Double Setting"));
            setting.Add(new XAttribute("description", "Double Description"));
            setting.Add(new XAttribute("value", "1.234"));
            setting.Add(new XAttribute("type", "dOUblE"));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteOneOfEachSettingType()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "String Setting"));
            setting.Add(new XAttribute("description", "String Description"));
            setting.Add(new XAttribute("value", "String Value"));
            setting.Add(new XAttribute("type", HardwareSetting.StringType));
            dummyRoot.Add(setting);

            setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Integer Setting"));
            setting.Add(new XAttribute("description", "Integer Description"));
            setting.Add(new XAttribute("value", "-34"));
            setting.Add(new XAttribute("type", HardwareSetting.IntegerType));
            dummyRoot.Add(setting);

            setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Double Setting"));
            setting.Add(new XAttribute("description", "Double Description"));
            setting.Add(new XAttribute("value", "1.234"));
            setting.Add(new XAttribute("type", HardwareSetting.DoubleType));
            dummyRoot.Add(setting);

            setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Boolean Setting"));
            setting.Add(new XAttribute("description", "Boolean Description"));
            setting.Add(new XAttribute("value", "T"));
            setting.Add(new XAttribute("type", HardwareSetting.BooleanType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteSettingWithEmptyDescription()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Double Setting"));
            setting.Add(new XAttribute("description", ""));
            setting.Add(new XAttribute("value", "1.234"));
            setting.Add(new XAttribute("type", HardwareSetting.DoubleType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteSettingWithMissingName()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("description", "Double Description"));
            setting.Add(new XAttribute("value", "1.234"));
            setting.Add(new XAttribute("type", HardwareSetting.DoubleType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteSettingWithMissingType()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Double Setting"));
            setting.Add(new XAttribute("description", "Double Description"));
            setting.Add(new XAttribute("value", "1.234"));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteSettingWithUnknownType()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Double Setting"));
            setting.Add(new XAttribute("description", "Double Description"));
            setting.Add(new XAttribute("value", "1.234"));
            setting.Add(new XAttribute("type", "unknown"));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteSettingWithMissingDescription()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Double Setting"));
            setting.Add(new XAttribute("value", "1.234"));
            setting.Add(new XAttribute("type", HardwareSetting.DoubleType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteDuplicateSettings()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Double Setting"));
            setting.Add(new XAttribute("description", "Double Description"));
            setting.Add(new XAttribute("value", "1.234"));
            setting.Add(new XAttribute("type", HardwareSetting.DoubleType));
            dummyRoot.Add(setting);

            setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Double Setting"));
            setting.Add(new XAttribute("description", "Double Description"));
            setting.Add(new XAttribute("value", "1.234"));
            setting.Add(new XAttribute("type", HardwareSetting.DoubleType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteEmptySettingsFile()
        {
            var dummyRoot = new XElement("HardwareConfiguration");
            dummyRoot.Save(DummyFile);
        }

        public static void WriteMultipleValidBooleanSettings()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "True"));
            setting.Add(new XAttribute("description", "Boolean Description"));
            setting.Add(new XAttribute("value", "T"));
            setting.Add(new XAttribute("type", HardwareSetting.BooleanType));
            dummyRoot.Add(setting);

            setting = new XElement("setting");
            setting.Add(new XAttribute("name", "t"));
            setting.Add(new XAttribute("description", "Boolean Description"));
            setting.Add(new XAttribute("value", "t"));
            setting.Add(new XAttribute("type", HardwareSetting.BooleanType));
            dummyRoot.Add(setting);

            setting = new XElement("setting");
            setting.Add(new XAttribute("name", "False"));
            setting.Add(new XAttribute("description", "Boolean Description"));
            setting.Add(new XAttribute("value", "F"));
            setting.Add(new XAttribute("type", HardwareSetting.BooleanType));
            dummyRoot.Add(setting);

            setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Whatever"));
            setting.Add(new XAttribute("description", "Boolean Description"));
            setting.Add(new XAttribute("value", "W"));
            setting.Add(new XAttribute("type", HardwareSetting.BooleanType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteBooleanSettingWithEmptyValue()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Boolean Setting"));
            setting.Add(new XAttribute("description", "Boolean Description"));
            setting.Add(new XAttribute("value", ""));
            setting.Add(new XAttribute("type", HardwareSetting.BooleanType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteBooleanSettingWithNoValueElement()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Boolean Setting"));
            setting.Add(new XAttribute("description", "Boolean Description"));
            setting.Add(new XAttribute("type", HardwareSetting.BooleanType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteDoubleSettingWithNoValueElement()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Double Setting"));
            setting.Add(new XAttribute("description", "Double Description"));
            setting.Add(new XAttribute("type", HardwareSetting.DoubleType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteDoubleSettingWithEmptyValue()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Double Setting"));
            setting.Add(new XAttribute("description", "Double Description"));
            setting.Add(new XAttribute("value", ""));
            setting.Add(new XAttribute("type", HardwareSetting.DoubleType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteDoubleSettingWithInvalidValue()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Double Setting"));
            setting.Add(new XAttribute("description", "Double Description"));
            setting.Add(new XAttribute("value", "abc"));
            setting.Add(new XAttribute("type", HardwareSetting.DoubleType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteIntegerSettingWithEmptyValue()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Integer Setting"));
            setting.Add(new XAttribute("description", "Integer Description"));
            setting.Add(new XAttribute("value", ""));
            setting.Add(new XAttribute("type", HardwareSetting.IntegerType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteIntegerSettingWithNoValueElement()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Integer Setting"));
            setting.Add(new XAttribute("description", "Integer Description"));
            setting.Add(new XAttribute("type", HardwareSetting.IntegerType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteIntegerSettingWithInvalidValue()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Integer Setting"));
            setting.Add(new XAttribute("description", "Integer Description"));
            setting.Add(new XAttribute("value", "abc"));
            setting.Add(new XAttribute("type", HardwareSetting.IntegerType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }

        public static void WriteThreeStringSettings()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "String Setting"));
            setting.Add(new XAttribute("description", "String Description"));
            setting.Add(new XAttribute("value", "String Value"));
            setting.Add(new XAttribute("type", HardwareSetting.StringType));
            dummyRoot.Add(setting);

            setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Empty String Setting"));
            setting.Add(new XAttribute("description", "String Description"));
            setting.Add(new XAttribute("value", ""));
            setting.Add(new XAttribute("type", HardwareSetting.StringType));
            dummyRoot.Add(setting);

            setting = new XElement("setting");
            setting.Add(new XAttribute("name", "Empty Default String Setting"));
            setting.Add(new XAttribute("description", "String Description"));
            setting.Add(new XAttribute("value", "String Value"));
            setting.Add(new XAttribute("type", HardwareSetting.StringType));
            dummyRoot.Add(setting);


            dummyRoot.Save(DummyFile);
        }

        public static void WriteStringSettingWithNoValueElement()
        {
            var dummyRoot = new XElement("HardwareConfiguration");

            var setting = new XElement("setting");
            setting.Add(new XAttribute("name", "String Setting"));
            setting.Add(new XAttribute("description", "String Description"));
            setting.Add(new XAttribute("type", HardwareSetting.StringType));
            dummyRoot.Add(setting);

            dummyRoot.Save(DummyFile);
        }
    }
}