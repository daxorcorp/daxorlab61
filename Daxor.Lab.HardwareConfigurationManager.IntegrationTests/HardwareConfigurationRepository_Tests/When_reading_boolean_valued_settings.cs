using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_reading_boolean_valued_settings
    {
        private Dictionary<string, HardwareSetting> _configurations;
        
        [ClassInitialize]
        public static void BeforeAllTests(TestContext context)
        {
            Directory.CreateDirectory(@"C:\temp\");
        }

        [TestInitialize]
        public void BeforeEachTest()
        {
            var repository = new HardwareConfigurationRepository(HardwareConfigurationRepositoryTestHelpers.DummyFile, "default");
            HardwareConfigurationRepositoryTestHelpers.WriteMultipleValidBooleanSettings();
            _configurations = repository.ReadAll();
        }

        [TestCleanup]
        public void AfterEachTest()
        {
            File.Delete(HardwareConfigurationRepositoryTestHelpers.DummyFile);
        }

        [TestMethod]
        public void And_the_value_is_T_then_it_is_considered_to_be_true()
        {
            Assert.IsTrue(_configurations["True"].GetValue<bool>());
        }

        [TestMethod]
        public void Then_case_does_not_affect_values_considered_to_be_true()
        {
            Assert.IsTrue(_configurations["t"].GetValue<bool>());
        }

        [TestMethod]
        public void And_the_value_doesnt_start_with_T_then_it_is_considered_to_be_false()
        {
            Assert.IsFalse(_configurations["False"].GetValue<bool>());
            Assert.IsFalse(_configurations["Whatever"].GetValue<bool>());
        }
    }
    // ReSharper restore InconsistentNaming
}
