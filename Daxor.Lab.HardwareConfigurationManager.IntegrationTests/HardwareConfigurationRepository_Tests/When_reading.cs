using System.IO;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_reading
    {
        private IHardwareConfigurationRepository _repository;

        [ClassInitialize]
        public static void BeforeAllTests(TestContext context)
        {
            Directory.CreateDirectory(@"C:\temp\");
        }

        [TestInitialize]
        public void BeforeEachTest()
        {
            _repository = new HardwareConfigurationRepository(HardwareConfigurationRepositoryTestHelpers.DummyFile, "default");
        }

        [TestCleanup]
        public void AfterEachTest()
        {
            File.Delete(HardwareConfigurationRepositoryTestHelpers.DummyFile);
        }

        [TestMethod]
        public void Then_the_case_of_the_type_attribute_value_does_not_matter()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteSettingWithMixedCase();

            var configurations = _repository.ReadAll();

            Assert.AreEqual(1.234, configurations["Double Setting"].GetValue<double>());
        }

        [TestMethod]
        public void Then_the_setting_name_is_used()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteOneOfEachSettingType();

            var configurations = _repository.ReadAll();

            Assert.IsTrue(configurations.ContainsKey("String Setting"));
            Assert.IsTrue(configurations.ContainsKey("Integer Setting"));
            Assert.IsTrue(configurations.ContainsKey("Double Setting"));
            Assert.IsTrue(configurations.ContainsKey("Boolean Setting"));
        }

        [TestMethod]
        public void Then_the_setting_type_is_used()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteOneOfEachSettingType();

            var configurations = _repository.ReadAll();

            Assert.AreEqual(HardwareSetting.StringType, configurations["String Setting"].Type);
            Assert.AreEqual(HardwareSetting.IntegerType, configurations["Integer Setting"].Type);
            Assert.AreEqual(HardwareSetting.DoubleType, configurations["Double Setting"].Type);
            Assert.AreEqual(HardwareSetting.BooleanType, configurations["Boolean Setting"].Type);
        }

        [TestMethod]
        public void Then_the_setting_description_is_used()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteOneOfEachSettingType();

            var configurations = _repository.ReadAll();

            Assert.AreEqual("String Description",  configurations["String Setting"].Description);
            Assert.AreEqual("Integer Description", configurations["Integer Setting"].Description);
            Assert.AreEqual("Double Description",  configurations["Double Setting"].Description);
            Assert.AreEqual("Boolean Description", configurations["Boolean Setting"].Description);
        }

        [TestMethod]
        public void Then_the_setting_description_can_be_empty()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteSettingWithEmptyDescription();

            var configurations = _repository.ReadAll();

            Assert.AreEqual(string.Empty, configurations["Double Setting"].Description);
        }

        [TestMethod]
        public void And_there_are_no_setting_elements_in_the_file_then_the_collection_is_empty()
        {
            HardwareConfigurationRepositoryTestHelpers.WriteEmptySettingsFile();

            AssertEx.IsEmpty(_repository.ReadAll());
        }
    }
    // ReSharper restore InconsistentNaming
}
