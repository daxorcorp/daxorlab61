using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_reading_string_valued_settings
    {
        private Dictionary<string, HardwareSetting> _configurations;

        [ClassInitialize]
        public static void BeforeAllTests(TestContext context)
        {
            Directory.CreateDirectory(@"C:\temp\");
        }

        [TestInitialize]
        public void BeforeEachTest()
        {
            var repository = new HardwareConfigurationRepository(HardwareConfigurationRepositoryTestHelpers.DummyFile, "default");
            HardwareConfigurationRepositoryTestHelpers.WriteThreeStringSettings();
            _configurations = repository.ReadAll();
        }

        [TestCleanup]
        public void AfterEachTest()
        {
            File.Delete(HardwareConfigurationRepositoryTestHelpers.DummyFile);
        }

        [TestMethod]
        public void Then_the_value_is_set()
        {
            Assert.AreEqual("String Value", _configurations["String Setting"].GetValue<string>());
        }

        [TestMethod]
        public void Then_the_value_can_be_empty()
        {
            Assert.AreEqual(string.Empty, _configurations["Empty String Setting"].GetValue<string>());
        }
    }
    // ReSharper restore InconsistentNaming
}
