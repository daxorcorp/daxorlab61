using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_repository
    {
        private HardwareConfigurationRepository _repository;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _repository = new HardwareConfigurationRepository("whatever", "default");
        }

        [TestMethod]
        public void Then_the_filename_is_set()
        {
            Assert.AreEqual("whatever", _repository.Filename);
        }

        [TestMethod]
        public void Then_the_default_configuration_filename_is_set()
        {
            Assert.AreEqual("default", _repository.DefaultFilename);
        }
    }
    // ReSharper restore InconsistentNaming
}
