using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.HardwareConfigurationManager.IntegrationTests.HardwareConfigurationRepository_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_a_default_configuration
    {
        private const string SourceFileName = @"c:\temp\source.txt";
        private const string TargetFileName = @"c:\temp\target.txt";

        [TestInitialize]
        public void BeforeAllTests()
        {
            Directory.CreateDirectory(@"C:\temp\");
            File.WriteAllText(SourceFileName, "Source content");
        }

        [TestCleanup]
        public void AfterAllTests()
        {
            File.Delete(SourceFileName);
            File.Delete(TargetFileName);
        }

        [TestMethod]
        public void And_the_configuration_file_does_not_exist_then_the_default_file_is_used_to_create_it()
        {
            File.Delete(TargetFileName);
            Assert.IsFalse(File.Exists(TargetFileName));

            var repository = new HardwareConfigurationRepository(TargetFileName, SourceFileName);
            repository.CreateDefaultConfiguration();

            Assert.IsTrue(File.Exists(TargetFileName));
        }

        [TestMethod]
        public void And_the_configuration_file_exists_then_the_default_file_replaces_it()
        {
            Directory.CreateDirectory(@"C:\temp\");
            File.WriteAllText(TargetFileName, "Content to replace");

            Assert.IsTrue(File.Exists(TargetFileName));

            var repository = new HardwareConfigurationRepository(TargetFileName, SourceFileName);
            repository.CreateDefaultConfiguration();

            string targetContents;
            using (var reader = new StreamReader(TargetFileName))
            {
                targetContents = reader.ReadToEnd();
            }

            Assert.AreEqual("Source content", targetContents);
        }
    }
    // ReSharper restore InconsistentNaming
}
