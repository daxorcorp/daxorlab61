using System.Linq;
using Daxor.Lab.QC.Reports.Common;
using Daxor.Lab.QC.Reports.ReportModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Reports.Tests.ReportModels_Tests.LinearitySubReportModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_given_a_test_aborted_immediately_after_being_started
    {
        private LinearitySubReportModel _linearitySubReportModel;
        private readonly int[] linearitySourcePositions = { 2, 21, 22 };

        // Code that should be executed before running each test in this class.
        [TestInitialize]
        public void TestInitialize()
        {
            var test = QcTestFactory.CreateLinearityWithCalibrationThatIsAborted();

            _linearitySubReportModel = new LinearitySubReportModel(test);
        }

        [TestMethod]
        public void Then_the_cumulative_pass_fail_label_on_linearity_is_test_incomplete()
        {
            var actualCumulativePassFailLabel = _linearitySubReportModel.GenerateLinearityTestPassFailLabel();

            Assert.AreEqual(QcReportConstants.TestIncompleteLabel, actualCumulativePassFailLabel);
        }

        [TestMethod]
        public void Then_the_calibration_sample_for_the_linearity_test_is_marked_as_failed()
        {
            var calibrationSample = _linearitySubReportModel.LinearitySamples.FirstOrDefault(s => s.Position == 1);
            if (calibrationSample == null) Assert.Fail("Cannot find the calibration sample!!");

            var actualSamplePassFailLabel = calibrationSample.Status;

            Assert.AreEqual(QcReportConstants.FailLabel, actualSamplePassFailLabel);
        }

        [TestMethod]
        public void Then_the_linearity_sources_sample_state_for_the_linearity_test_are_marked_as_not_applicable()
        {
            var linearitySamples = _linearitySubReportModel.LinearitySamples.Where(s => linearitySourcePositions.Contains(s.Position)).ToArray();
            if (linearitySamples == null || linearitySamples.Count() != linearitySourcePositions.Count())
                Assert.Fail("Cannot find the linearity samples");

            var passStates = from s in linearitySamples select s.Status;

            Assert.IsTrue(passStates.All(s => s == QcReportConstants.NotApplicableLabel));
        }
    }
    // ReSharper restore InconsistentNaming
}
