using System.Linq;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Reports.ReportModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Reports.Tests.ReportModels_Tests.LinearitySubReportModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_test_is_aborted
    {
        [TestMethod]
        public void And_one_of_the_sources_counted_successfully_then_its_values_are_not_NA()
        {
            var qcTest = QcTestFactory.CreateLinearityWithCalibrationThatIsAborted();

            var setupSample = qcTest.Samples.FirstOrDefault(x => x.TypeOfSample == SampleType.QC1);
            if (setupSample == null) Assert.Fail("Could not find the setup isotope.");

            setupSample.IsPassed = true;
            setupSample.CountsInRegionOfInterest = 1000;
            setupSample.Spectrum.LiveTimeInSeconds = 60;
            
            var calibrationSample = qcTest.Samples.FirstOrDefault(x => x.TypeOfSample == SampleType.QC2);
            if (calibrationSample == null) Assert.Fail("Could not find the calibration isotope");

            calibrationSample.IsPassed = null;

            var linearityModel = new LinearitySubReportModel(qcTest);

            var expectedSetupSample = linearityModel.LinearitySamples.FirstOrDefault(x => x.Position == setupSample.Position);
            if(expectedSetupSample == null) Assert.Fail("Could not find the expected setup sample.");

            Assert.AreNotEqual("N/A", expectedSetupSample.MeasuredRate);
            Assert.AreNotEqual("N/A", expectedSetupSample.MeasuredRatioOfRates);
            Assert.AreNotEqual("N/A", expectedSetupSample.NominalActivity);
            Assert.AreNotEqual("N/A", expectedSetupSample.To);
        }
    }
    // ReSharper restore InconsistentNaming
}
