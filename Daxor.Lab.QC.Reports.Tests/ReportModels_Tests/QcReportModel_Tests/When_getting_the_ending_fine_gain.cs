using System.Globalization;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.QC.Reports.ReportModels;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Reports.Tests.ReportModels_Tests.QcReportModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_ending_fine_gain
    {
        // 6 types of QC tests and aborted/non-aborted => 6 x 2 = 12
        // Check rounding
        private static ISettingsManager _settingsManager;

        [ClassInitialize]
        public static void TestsClassInitialize(TestContext context)
        {
            _settingsManager = MockRepository.GenerateStub<ISettingsManager>();
        }

        [TestMethod]
        public void And_using_a_full_QC_test_that_has_not_been_aborted_then_the_ending_fine_gain_of_the_test_is_used()
        {
            const double expectedFineGain = 1.234;
            var qcTest = QcTestFactory.CreateFullQcTest();
            qcTest.EndFineGain = expectedFineGain;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual(expectedFineGain.ToString(CultureInfo.InvariantCulture), testReportModel.GenerateEndFineGain());
        }

        [TestMethod]
        public void And_using_a_full_QC_test_that_has_been_aborted_then_NA_is_used()
        {
            const string expectedFineGain = "N/A";
            var qcTest = QcTestFactory.CreateFullQcTest();
            qcTest.EndFineGain = 1.234; 
            qcTest.Status = TestStatus.Aborted;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual(expectedFineGain, testReportModel.GenerateEndFineGain());
        }

        [TestMethod]
        public void And_using_a_calibration_QC_test_that_has_not_been_aborted_then_the_ending_fine_gain_of_the_test_is_used()
        {
            const double expectedFineGain = 1.234;
            var qcTest = QcTestFactory.CreateQcCalibrationTest();
            qcTest.EndFineGain = expectedFineGain;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual(expectedFineGain.ToString(CultureInfo.InvariantCulture), testReportModel.GenerateEndFineGain());
        }

        [TestMethod]
        public void And_using_a_calibration_QC_test_that_has_been_aborted_then_NA_is_used()
        {
            const string expectedFineGain = "N/A";
            var qcTest = QcTestFactory.CreateQcCalibrationTest();
            qcTest.EndFineGain = 1.234;
            qcTest.Status = TestStatus.Aborted;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual(expectedFineGain, testReportModel.GenerateEndFineGain());
        }

        [TestMethod]
        public void And_using_a_linearity_QC_test_that_has_not_been_aborted_then_the_ending_fine_gain_of_the_test_is_used()
        {
            const double expectedFineGain = 1.234;
            var qcTest = QcTestFactory.CreateLinearityWithCalibration();
            qcTest.EndFineGain = expectedFineGain;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual(expectedFineGain.ToString(CultureInfo.InvariantCulture), testReportModel.GenerateEndFineGain());
        }

        [TestMethod]
        public void And_using_a_linearity_QC_test_that_has_been_aborted_then_NA_is_used()
        {
            const string expectedFineGain = "N/A";
            var qcTest = QcTestFactory.CreateLinearityWithCalibration();
            qcTest.EndFineGain = 1.234;
            qcTest.Status = TestStatus.Aborted;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual(expectedFineGain, testReportModel.GenerateEndFineGain());
        }

        [TestMethod]
        public void And_using_a_contamination_QC_test_that_has_not_been_aborted_then_the_starting_fine_gain_of_the_test_is_used()
        {
            const double expectedFineGain = 5.678;
            var qcTest = QcTestFactory.CreateContaminationTest();
            qcTest.EndFineGain = 1.234;
            qcTest.StartFineGain = expectedFineGain;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual(expectedFineGain.ToString(CultureInfo.InvariantCulture), testReportModel.GenerateEndFineGain());
        }

        [TestMethod]
        public void And_using_a_contamination_QC_test_that_has_been_aborted_then_NA_is_used()
        {
            const string expectedFineGain = "N/A";
            var qcTest = QcTestFactory.CreateContaminationTest();
            qcTest.EndFineGain = 1.234;
            qcTest.Status = TestStatus.Aborted;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual(expectedFineGain, testReportModel.GenerateEndFineGain());
        }

        [TestMethod]
        public void And_using_a_standards_QC_test_that_has_not_been_aborted_then_the_starting_fine_gain_of_the_test_is_used()
        {
            const double expectedFineGain = 5.678;
            var qcTest = QcTestFactory.CreateStandardsTest();
            qcTest.EndFineGain = 1.234;
            qcTest.StartFineGain = expectedFineGain;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual(expectedFineGain.ToString(CultureInfo.InvariantCulture), testReportModel.GenerateEndFineGain());
        }

        [TestMethod]
        public void And_using_a_standards_QC_test_that_has_been_aborted_then_NA_is_used()
        {
            const string expectedFineGain = "N/A";
            var qcTest = QcTestFactory.CreateStandardsTest();
            qcTest.EndFineGain = 1.234;
            qcTest.Status = TestStatus.Aborted;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual(expectedFineGain, testReportModel.GenerateEndFineGain());
        }

        [TestMethod]
        public void And_using_a_linearity_without_calibration_QC_test_that_has_not_been_aborted_then_the_starting_fine_gain_of_the_test_is_used()
        {
            const double expectedFineGain = 5.678;
            var qcTest = QcTestFactory.CreateLinearityWithoutCalibrationWithFailingQc2();
            qcTest.EndFineGain = 1.234;
            qcTest.StartFineGain = expectedFineGain;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual(expectedFineGain.ToString(CultureInfo.InvariantCulture), testReportModel.GenerateEndFineGain());
        }

        [TestMethod]
        public void And_using_a_linearity_without_calibration_QC_test_that_has_been_aborted_then_NA_is_used()
        {
            const string expectedFineGain = "N/A";
            var qcTest = QcTestFactory.CreateLinearityWithoutCalibrationWithFailingQc2();
            qcTest.EndFineGain = 1.234;
            qcTest.Status = TestStatus.Aborted;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual(expectedFineGain, testReportModel.GenerateEndFineGain());
        }

        [TestMethod]
        public void Then_the_fine_gain_is_rounded_correctly()
        {
            var qcTest = QcTestFactory.CreateLinearityWithCalibration();
            qcTest.EndFineGain = 1.2345;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual("1.235", testReportModel.GenerateEndFineGain());
        }
    }
    // ReSharper restore InconsistentNaming
}
