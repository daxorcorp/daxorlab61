using System.Globalization;
using Daxor.Lab.QC.Reports.ReportModels;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Reports.Tests.ReportModels_Tests.QcReportModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_starting_fine_gain
    {
        private static ISettingsManager _settingsManager;

        [ClassInitialize]
        public static void TestsClassInitialize(TestContext context)
        {
            _settingsManager = MockRepository.GenerateStub<ISettingsManager>();
        }

        [TestMethod]
        public void Then_the_fine_gain_is_based_on_the_QC_test()
        {
            const double expectedFineGain = 1.234;
            var qcTest = QcTestFactory.CreateLinearityWithCalibration();
            qcTest.StartFineGain = expectedFineGain;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);
            
            Assert.AreEqual(expectedFineGain.ToString(CultureInfo.InvariantCulture), testReportModel.GenerateStartFineGain());
        }

        [TestMethod]
        public void Then_the_fine_gain_is_rounded_correctly()
        {
            var qcTest =  QcTestFactory.CreateLinearityWithCalibration();
            qcTest.StartFineGain = 1.2345;

            var testReportModel = new QcReportModel(qcTest, _settingsManager);

            Assert.AreEqual("1.235", testReportModel.GenerateStartFineGain());
        }
    }
    // ReSharper restore InconsistentNaming
}
