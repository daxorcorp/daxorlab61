using System;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Reports.ReportModels;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Reports.Tests.ReportModels_Tests.StandardsSubReportModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_test_is_aborted_with_one_standard_finished
    {
        public const int QcStandardsCountTimeInSeconds = 180;

        private QcTest _qcTest;
        private QcSample _sampleA;
        private QcSample _sampleB;
        private ISettingsManager _settingsManager;

        [TestInitialize]
        public void TestInitialize()
        {
            _qcTest = QcTestFactory.CreateStandardsTest();

            _sampleA = _qcTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.StandardA);
            if (_sampleA == null) throw new NotSupportedException("Unable to locate Standard A");
            _sampleA.CountsInRegionOfInterest = 1000;
            _sampleA.Spectrum.LiveTimeInSeconds = QcStandardsCountTimeInSeconds;
            _sampleA.IsPassed = null;

            _sampleB = _qcTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.StandardB);
            if (_sampleB == null) throw new NotSupportedException("Unable to locate Standard B");
            _sampleB.CountsInRegionOfInterest = 100;
            _sampleB.Spectrum.LiveTimeInSeconds = 60;
            _sampleB.IsPassed = null;

            _qcTest.Status = TestStatus.Aborted;

            _settingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _settingsManager.Expect(m => m.GetSetting<int>(SettingKeys.QcStandardsCountTimeInSeconds))
                .Return(QcStandardsCountTimeInSeconds);
        }

        [TestMethod]
        public void Then_the_measured_rate_of_the_finished_standard_is_not_NA()
        {
            var reportModel = new StandardsSubReportModel(_qcTest, _settingsManager);

            var standardASample = reportModel.StandardSamples.FirstOrDefault(s => s.Position == _sampleA.Position);
            if (standardASample == null) Assert.Fail("Cannot find standard A sample in report model");

            Assert.AreNotEqual("N/A", standardASample.MeasuredRate);
        }

        [TestMethod]
        public void Then_the_actual_count_of_the_finished_standard_is_not_NA()
        {
            var reportModel = new StandardsSubReportModel(_qcTest, _settingsManager);

            var standardASample = reportModel.StandardSamples.FirstOrDefault(s => s.Position == _sampleA.Position);
            if (standardASample == null) Assert.Fail("Cannot find standard A sample in report model");

            Assert.AreNotEqual("N/A", standardASample.ActualCounts);
        }

        [TestMethod]
        public void Then_the_measured_rate_of_the_aborted_standard_is_NA()
        {
            var reportModel = new StandardsSubReportModel(_qcTest, _settingsManager);

            var standardBSample = reportModel.StandardSamples.FirstOrDefault(s => s.Position == _sampleB.Position);
            if (standardBSample == null) Assert.Fail("Cannot find standard B sample in report model");

            Assert.AreEqual("N/A", standardBSample.MeasuredRate);
        }

        [TestMethod]
        public void Then_the_actual_count_of_the_aborted_standard_is_NA()
        {
            var reportModel = new StandardsSubReportModel(_qcTest, _settingsManager);

            var standardBSample = reportModel.StandardSamples.FirstOrDefault(s => s.Position == _sampleB.Position);
            if (standardBSample == null) Assert.Fail("Cannot find standard B sample in report model");

            Assert.AreEqual("N/A", standardBSample.ActualCounts);
        }
    }
    // ReSharper restore InconsistentNaming
}
