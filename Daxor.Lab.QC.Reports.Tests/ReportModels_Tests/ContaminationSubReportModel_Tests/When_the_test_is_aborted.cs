using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.QC.Reports.ReportModels;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Reports.Tests.ReportModels_Tests.ContaminationSubReportModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_test_is_aborted
    {
        [TestMethod]
        public void Then_samples_with_a_null_IsPassed_state_show_counts_of_NA()
        {
            var qcContaminationTest = QcTestFactory.CreateContaminationTest();
            qcContaminationTest.Status = TestStatus.Aborted;

            var samples = qcContaminationTest.Samples.ToArray();

            foreach (var sample in samples)
            {
                sample.CountsInRegionOfInterest = 1000;
                sample.Spectrum.LiveTimeInSeconds = 60;
                sample.IsPassed = true;
            }

            var notPassedSample = samples.FirstOrDefault(x => x.Position == 15);
            if (notPassedSample==null) Assert.Fail("Could not find sample 15");

            notPassedSample.IsPassed = null;

            var contaminationReportModel = new ContaminationSubReportModel(qcContaminationTest,
                MockRepository.GenerateStub<ISettingsManager>());

            var contaminationSampleNumber15 = contaminationReportModel.SecondHalfOfContaminationSamples.FirstOrDefault(x => x.Position == 15);
            if (contaminationSampleNumber15 == null) Assert.Fail("Could not find sample 15");

            Assert.AreEqual("N/A", contaminationSampleNumber15.MeasuredRate);
            Assert.AreEqual("N/A", contaminationSampleNumber15.Status);
        }
    }
    // ReSharper restore InconsistentNaming
}
