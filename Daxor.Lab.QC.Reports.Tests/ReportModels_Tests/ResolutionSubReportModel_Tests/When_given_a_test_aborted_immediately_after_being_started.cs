﻿using System.Linq;
using Daxor.Lab.QC.Reports.Common;
using Daxor.Lab.QC.Reports.ReportModels;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Reports.Tests.ReportModels_Tests.ResolutionSubReportModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_given_a_test_aborted_immediately_after_being_started
    {
        private ResolutionSubReportModel _resolutionReportModel;
        private readonly int[] linearitySourcePositions = { 2, 21, 22 };
        private readonly int[] sourcePositions = { 1, 2, 21, 22 };

        // Code that should be executed before running each test in this class.
        [TestInitialize]
        public void TestInitialize()
        {
            var test = QcTestFactory.CreateLinearityWithCalibrationThatIsAborted();
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();

            _resolutionReportModel = new ResolutionSubReportModel(test, stubSettingsManager);
        }

        [TestMethod]
        public void Then_the_cumulative_pass_fail_label_on_resolution_is_test_Incomplete()
        {
            var actualCumulativePassFailLabel = _resolutionReportModel.GenerateResolutionPassFailLabel();

            Assert.AreEqual(QcReportConstants.TestIncompleteLabel, actualCumulativePassFailLabel);
        }

        [TestMethod]
        public void Then_the_calibration_sample_for_the_resolution_test_is_marked_as_failed()
        {
            var calibrationSample = _resolutionReportModel.ResolutionSamples.FirstOrDefault(s => s.Position == 1);
            if (calibrationSample == null) Assert.Fail("Cannot find the calibration sample!!");

            var actualSamplePassFailLabel = calibrationSample.Status;

            Assert.AreEqual(QcReportConstants.FailLabel, actualSamplePassFailLabel);
        }

        [TestMethod]
        public void Then_the_linearity_sources_sample_state_for_the_resolution_test_are_marked_as_not_applicable()
        {
            var resolutionSamples = _resolutionReportModel.ResolutionSamples.Where(s => linearitySourcePositions.Contains(s.Position)).ToArray();
            if (resolutionSamples == null || resolutionSamples.Count() != linearitySourcePositions.Count())
                Assert.Fail("Cannot find the resolution samples");

            var passStates = from s in resolutionSamples select s.Status;

            Assert.IsTrue(passStates.All(s => s == QcReportConstants.NotApplicableLabel));
        }

        [TestMethod]
        public void Then_the_centroids_for_the_resolution_samples_are_marked_as_not_applicable()
        {
            var resolutionSamples = _resolutionReportModel.ResolutionSamples.Where(s => sourcePositions.Contains(s.Position)).ToArray();
            if (resolutionSamples == null || resolutionSamples.Count() != sourcePositions.Count())
                Assert.Fail("Cannot find the source samples");

            var passStates = from s in resolutionSamples select s.Centroid;

            Assert.IsTrue(passStates.All(s => s == QcReportConstants.NotApplicableLabel));
        }

        [TestMethod]
        public void Then_the_FWHM_for_the_resolution_samples_are_marked_as_not_applicable()
        {
            var resolutionSamples = _resolutionReportModel.ResolutionSamples.Where(s => sourcePositions.Contains(s.Position)).ToArray();
            if (resolutionSamples == null || resolutionSamples.Count() != sourcePositions.Count())
                Assert.Fail("Cannot find the source samples");

            var passStates = from s in resolutionSamples select s.FWHM;

            Assert.IsTrue(passStates.All(s => s == QcReportConstants.NotApplicableLabel));
        }

        [TestMethod]
        public void Then_the_measured_counts_for_the_resolution_samples_are_marked_as_not_applicable()
        {
            var resolutionSamples = _resolutionReportModel.ResolutionSamples.Where(s => sourcePositions.Contains(s.Position)).ToArray();
            if (resolutionSamples == null || resolutionSamples.Count() != sourcePositions.Count())
                Assert.Fail("Cannot find the source samples");

            var passStates = from s in resolutionSamples select s.ActualCounts;

            Assert.IsTrue(passStates.All(s => s == QcReportConstants.NotApplicableLabel));
        }
    }
    // ReSharper restore InconsistentNaming
}
