using Daxor.Lab.Domain.Common;
using Daxor.Lab.QC.Reports.ReportModels;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Reports.Tests.ReportModels_Tests.ResolutionSubReportModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_generating_resolution_failure_details
    {
        [TestMethod]
        public void And_the_test_is_aborted_then_there_are_no_failure_details()
        {
            // Aborting the QC test sets the sample counts, FWHM, and centroid labels to "N/A"
            var test = QcTestFactory.CreateLinearityWithoutCalibrationWithFailingQc2();
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            test.Status = TestStatus.Aborted;

            var reportModel = new ResolutionSubReportModel(test, stubSettingsManager);

            var failureDetails = reportModel.GenerateResolutionFailureDetails();
            Assert.IsTrue(failureDetails.Count == 0);
        }
    }
    // ReSharper restore InconsistentNaming
}
