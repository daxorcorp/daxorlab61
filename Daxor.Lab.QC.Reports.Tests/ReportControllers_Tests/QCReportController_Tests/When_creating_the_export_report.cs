﻿using Daxor.Lab.QC.Reports.Tests.ReportControllers_Tests.QCReportController_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Daxor.Lab.QC.Reports.Tests.ReportControllers_Tests.QCReportController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_building_the_export_report
    {
        [TestMethod]
        public void Then_the_file_name_contains_the_system_id_and_the_properly_formatted_time()
        {
            var qcTest = QcTestFactory.CreateLinearityWithCalibration();
            qcTest.SystemId = "DORO 107";

            var reportController = new StubQcReportWithTestOnlyCtor(qcTest);
            reportController.ExportReport();

            var expectedResult = qcTest.SystemId + "-" + DateTime.Now.ToString("MM-dd-yyyy hh-mm-ss tt");
            var actualResult = reportController.FileName;

            Assert.AreEqual(expectedResult, actualResult);
        }
    }
    // ReSharper restore InconsistentNaming
}
