using System;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.QC.Reports.Tests.ReportControllers_Tests.QCReportController_Tests.Stubs;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Reports.Tests.ReportControllers_Tests.QCReportController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_exporting_a_report
    {
        private IFolderBrowser _stubFolderBrowser;

        [TestInitialize]
        public void TestInitialize()
        {
            _stubFolderBrowser = MockRepository.GenerateStub<IFolderBrowser>();
            _stubFolderBrowser.Stub(fb => fb.IsPathValid()).Return(true);
            _stubFolderBrowser.IsOpticalDrive = true;
        }

        [TestMethod]
        public void And_the_export_fails_then_the_busy_indicator_is_disabled()
        {
            var mockEventAggregator = MockRepository.GenerateMock<IEventAggregator>();
            mockEventAggregator.Expect(e => e.GetEvent<BusyStateChanged>().Publish(Arg<BusyPayload>.Matches(x => !x.IsBusy)));

            var failingQcReportController = new StubQCReportControllerThatFailsToExport(mockEventAggregator,
                _stubFolderBrowser);

            failingQcReportController.ExportReport();

            mockEventAggregator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_export_fails_then_the_correct_message_is_displayed()
        {
            var errorMessage = new Message("key", "description", "fix it", "id", MessageType.Alert);
            var stubMessageManager = MockRepository.GenerateStub<IMessageManager>();
            stubMessageManager.Expect(x => x.GetMessage(MessageKeys.BvaExportUnsuccessful)).Return(errorMessage);

            var mockMessageBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMessageBoxDispatcher.Expect(d => d.ShowMessageBox(
                Arg<MessageBoxCategory>.Is.Equal(MessageBoxCategory.Error),
                Arg<string>.Is.Equal(errorMessage.FormattedMessage),
                Arg<string>.Is.Equal("Export Quality Control Report"),
                Arg<MessageBoxChoiceSet>.Is.Equal(MessageBoxChoiceSet.Close)));

            var failingExportController = new StubQCReportControllerThatFailsToExport(_stubFolderBrowser, mockMessageBoxDispatcher,
                stubMessageManager);
 
            failingExportController.ExportReport();

            mockMessageBoxDispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_export_fails_then_the_exception_message_is_logged()
        {
            var errorMessage = new Message("key", "description", "fix it", "id", MessageType.Alert);
            var stubMessageManager = MockRepository.GenerateStub<IMessageManager>();
            stubMessageManager.Expect(x => x.GetMessage(MessageKeys.BvaExportUnsuccessful)).Return(errorMessage);

            var logMessage = String.Format("{0}; inner exception: {1}", errorMessage.FormattedMessage, 
                 StubQCReportControllerThatFailsToExport.ExceptionMessage);

            var mockLoggerFacade = MockRepository.GenerateMock<ILoggerFacade>();
            mockLoggerFacade.Expect(x => x.Log(logMessage, Category.Exception, Priority.High));

            var failingExportController = new StubQCReportControllerThatFailsToExport(_stubFolderBrowser,
                stubMessageManager, mockLoggerFacade);

            failingExportController.ExportReport();

            mockLoggerFacade.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_optical_drive_fails_then_the_export_fails()
        {
            var stubStarBurn = MockRepository.GenerateStub<IStarBurnWrapper>();
            stubStarBurn.Expect(w => w.CreateDataBurner()).Throw(new Exception(StubQcReportControllerWithCustomizedExportFaulted.FailureMessage));

            var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            stubEventAggregator.Expect(x => x.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());

            var failingExportController = new StubQcReportControllerWithCustomizedExportFaulted(stubEventAggregator, _stubFolderBrowser, stubStarBurn);

            failingExportController.ExportReport();

            Assert.IsTrue(failingExportController.FailedWithCorrectMessage);
        }

        [TestMethod]
        public void And_the_export_fails_then_a_single_message_box_is_shown_to_the_user()
        {
            var stubStarBurn = MockRepository.GenerateStub<IStarBurnWrapper>();
            stubStarBurn.Expect(w => w.CreateDataBurner()).Throw(new Exception(StubQcReportControllerWithCustomizedExportFaulted.FailureMessage));

            var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            stubEventAggregator.Expect(x => x.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());

            var mockMessageBoxDispathcer = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMessageBoxDispathcer.Expect(x => x.ShowMessageBox(MessageBoxCategory.Error, Guid.Empty, null, null))
                .IgnoreArguments()
                .Repeat.Once().Return(0);

            var failingExportController = new StubQcReportControllerWithCustomizedExportFaulted(stubEventAggregator, _stubFolderBrowser, stubStarBurn, mockMessageBoxDispathcer);

            failingExportController.ExportReport();

            mockMessageBoxDispathcer.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
