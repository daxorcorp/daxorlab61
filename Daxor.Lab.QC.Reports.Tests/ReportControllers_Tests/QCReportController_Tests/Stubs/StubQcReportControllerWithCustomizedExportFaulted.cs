﻿using System;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.QC.Reports.ReportControllers;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Rhino.Mocks;
using Telerik.Reporting.Processing;

namespace Daxor.Lab.QC.Reports.Tests.ReportControllers_Tests.QCReportController_Tests.Stubs
{
    public class StubQcReportControllerWithCustomizedExportFaulted : QcReportController
    {
        public const string FailureMessage = "cannot export";

        public StubQcReportControllerWithCustomizedExportFaulted(IEventAggregator eventAggregator, IFolderBrowser folderBrowser, IStarBurnWrapper starBurnWrapper) :
            base(MockRepository.GenerateStub<ILoggerFacade>(),
                MockRepository.GenerateStub<IMessageBoxDispatcher>(),
                MockRepository.GenerateStub<IRegionManager>(),
                QcTestFactory.CreateLinearityWithCalibration(), 
                eventAggregator,
                starBurnWrapper,
                folderBrowser,
                MockRepository.GenerateStub<IMessageManager>(), MockRepository.GenerateStub<ISettingsManager>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public StubQcReportControllerWithCustomizedExportFaulted(IEventAggregator eventAggregator,
            IFolderBrowser folderBrowser, IStarBurnWrapper starBurnWrapper, IMessageBoxDispatcher messageBoxDispatcher)
            :
                base(MockRepository.GenerateStub<ILoggerFacade>(),
                    messageBoxDispatcher,
                    MockRepository.GenerateStub<IRegionManager>(),
                    QcTestFactory.CreateLinearityWithCalibration(),
                    eventAggregator,
                    starBurnWrapper,
                    folderBrowser,
                    MockRepository.GenerateStub<IMessageManager>(), MockRepository.GenerateStub<ISettingsManager>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public bool FailedWithCorrectMessage { get; protected set; }

        protected override void OnExportTaskFaulted(Exception faultingException)
        {
            FailedWithCorrectMessage = faultingException.InnerException.Message == FailureMessage;
        }

        protected override void SavePdfRenderingToFile(ReportProcessor reportProcessor, string tempPath, string fileName)
        {
        }

        protected override void CleanupAfterExport(string tempPath, string fileName)
        {
        }
    }
}
