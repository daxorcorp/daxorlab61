﻿using System;
using System.Threading;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.QC.Reports.ReportControllers;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Rhino.Mocks;
using Telerik.Reporting.Processing;

namespace Daxor.Lab.QC.Reports.Tests.ReportControllers_Tests.QCReportController_Tests.Stubs
{
    public class StubQCReportControllerThatFailsToExport : QcReportController
    {
        public const string ExceptionMessage = "An exception was thrown";

        public StubQCReportControllerThatFailsToExport(IEventAggregator eventAggregator, IFolderBrowser folderBrowser) :
            base(MockRepository.GenerateStub<ILoggerFacade>(),
                MockRepository.GenerateStub<IMessageBoxDispatcher>(),
                MockRepository.GenerateStub<IRegionManager>(),
                QcTestFactory.CreateLinearityWithCalibration(), 
                eventAggregator,
                MockRepository.GenerateStub<IStarBurnWrapper>(),
                folderBrowser,
                MockRepository.GenerateStub<IMessageManager>(), MockRepository.GenerateStub<ISettingsManager>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public StubQCReportControllerThatFailsToExport(IFolderBrowser folderBrowser, IMessageBoxDispatcher msgBoxDispatcher, IMessageManager messageManager) :
            base(MockRepository.GenerateStub<ILoggerFacade>(),
                msgBoxDispatcher,
                MockRepository.GenerateStub<IRegionManager>(),
                QcTestFactory.CreateLinearityWithCalibration(),
                MockRepository.GenerateStub<IEventAggregator>(),
                MockRepository.GenerateStub<IStarBurnWrapper>(),
                folderBrowser,
                messageManager, 
                MockRepository.GenerateStub<ISettingsManager>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public StubQCReportControllerThatFailsToExport(IFolderBrowser folderBrowser, IMessageManager messageManager, ILoggerFacade loggerFacade) :
            base(loggerFacade,
                MockRepository.GenerateStub<IMessageBoxDispatcher>(),
                MockRepository.GenerateStub<IRegionManager>(),
                QcTestFactory.CreateLinearityWithCalibration(),
                MockRepository.GenerateStub<IEventAggregator>(),
                MockRepository.GenerateStub<IStarBurnWrapper>(),
                folderBrowser,
                messageManager,
                MockRepository.GenerateStub<ISettingsManager>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        protected override void ExportTask(ReportProcessor reportProcessor, string tempPath, CancellationTokenSource tokenSource)
        {
            throw new ArgumentException(ExceptionMessage);
        }
    }
}
