﻿using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Reports.ReportControllers;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Reports.Tests.ReportControllers_Tests.QCReportController_Tests.Stubs
{
    public class StubQcReportWithTestOnlyCtor : QcReportController
    {
        private readonly QcTest _test;
        public string FileName { get; private set; }

        public StubQcReportWithTestOnlyCtor(QcTest test)
            : base(MockRepository.GenerateStub<ILoggerFacade>(), MockRepository.GenerateStub<IMessageBoxDispatcher>(), MockRepository.GenerateStub<IRegionManager>(),
                test, MockRepository.GenerateStub<IEventAggregator>(), MockRepository.GenerateStub<IStarBurnWrapper>(), MockRepository.GenerateStub<IFolderBrowser>(), MockRepository.GenerateStub<IMessageManager>(),
                MockRepository.GenerateStub<ISettingsManager>())
        {
            _test = test;
        }

        public override void ExportReport()
        {
            FileName = BuildFileName(_test.SystemId);
        }
    }
}
