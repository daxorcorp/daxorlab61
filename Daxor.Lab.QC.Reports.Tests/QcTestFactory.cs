﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Tests;

namespace Daxor.Lab.QC.Reports.Tests
{
    public class QcTestFactory
    {
        private static readonly int[] BariumPositions = {2, 21, 22};

        public static QcTest CreateLinearityWithoutCalibrationWithFailingQc2()
        {
            var linearityTest = CreateBasicTest(TestType.LinearityWithoutCalibration);
            AddLinearitySamples(linearityTest);

            var qc2Sample = (from s in linearityTest.Samples where s.Position == 2 select s).FirstOrDefault();
            if (qc2Sample == null) throw new NotSupportedException("Cannot find QC 2");
            qc2Sample.IsPassed = false;

            return linearityTest;
        }

        public static QcTest CreateLinearityWithCalibration()
        {
            var linearityTest = CreateBasicTest(TestType.Linearity);
            AddCalibrationSample(linearityTest);
            AddLinearitySamples(linearityTest);

            return linearityTest;
        }

        public static QcTest CreateQcCalibrationTest()
        {
            var qcCalibrationTest = CreateBasicTest(TestType.Calibration);
            AddCalibrationSample(qcCalibrationTest);

            return qcCalibrationTest;
        }

        public static QcTest CreateFullQcTest()
        {
            var fullQcTest = CreateBasicTest(TestType.Full);
            AddCalibrationSample(fullQcTest);
            AddLinearitySamples(fullQcTest);

            return fullQcTest;
        }

        public static QcTest CreateContaminationTest()
        {
            return CreateBasicTest(TestType.Contamination);
        }

        public static QcTest CreateStandardsTest()
        {
            return CreateBasicTest(TestType.Standards);
        }

        public static QcTest CreateLinearityWithCalibrationThatIsAborted()
        {
            var linearityTest = CreateBasicTest(TestType.Linearity);

            AddCalibrationSample(linearityTest);
            var cesiumSample = (from s in linearityTest.Samples where s.Position == 1 select s).FirstOrDefault();
            if (cesiumSample == null) throw new NotSupportedException("Cannot find QC 1");
            cesiumSample.IsPassed = false;

            AddLinearitySamples(linearityTest);
            linearityTest.Status = TestStatus.Aborted;

            return linearityTest;
        }

        private static QcTest CreateBasicTest(TestType type)
        {
            var test = Core.Factories.QcTestFactory.CreateTest(type, new QcSchemaProvider(), null);
            test.BuildSamples();
            foreach (var sample in test.Samples) sample.Spectrum.LiveTimeInSeconds = 1;

            return test;
        }

        private static void AddCalibrationSample(Test<QcSample> test)
        {
            var cesiumIsotope = new Isotope(IsotopeType.Cs137, 0.0, 0.0, new List<double>());
            var cesiumSource = new QcSource(cesiumIsotope, DateTime.Now, 2, 0.0, 0.0, "4567", 5000);
            var cesiumSample = (from s in test.Samples where s.Position == 1 select s).FirstOrDefault();
            if (cesiumSample == null) throw new NotSupportedException("Cannot find QC 1 sample");
            cesiumSample.Source = cesiumSource;
        }

        private static void AddLinearitySamples(Test<QcSample> test)
        {
            var bariumIsotope = new Isotope(IsotopeType.Ba133, 0.0, 0.0, new List<double>());
            var bariumSource = new QcSource(bariumIsotope, DateTime.Now, 2, 0.0, 0.0, "1234", 4000);
            foreach (var position in BariumPositions)
            {
                var sampleAtPosition = (from s in test.Samples where s.Position == position select s).FirstOrDefault();
                if (sampleAtPosition == null)
                    throw new NotSupportedException("Cannot find sample at position number " + position);
                sampleAtPosition.Source = bariumSource;
            }
        }
    }
}
