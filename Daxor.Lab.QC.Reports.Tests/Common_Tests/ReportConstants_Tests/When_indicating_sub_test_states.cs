using Daxor.Lab.QC.Reports.Common;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Reports.Tests.Common_Tests.ReportConstants_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_indicating_sub_test_states
    {
        [TestMethod]
        [RequirementValue("Test Incomplete")]
        public void Then_the_test_incomplete_label_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), QcReportConstants.TestIncompleteLabel);
        }

        [TestMethod]
        [RequirementValue("PASS")]
        public void Then_the_cumulative_pass_label_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), QcReportConstants.CumulativePassLabel);
        }

        [TestMethod]
        [RequirementValue("FAIL")]
        public void Then_the_cumulative_fail_label_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), QcReportConstants.CumulativeFailLabel);
        }
    }
    // ReSharper restore InconsistentNaming
}
