using Daxor.Lab.QC.Reports.Common;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Reports.Tests.Common_Tests.ReportConstants_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_indicating_sample_states
    {
        [TestMethod]
        [RequirementValue("Pass")]
        public void Then_the_pass_label_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), QcReportConstants.PassLabel);
        }

        [TestMethod]
        [RequirementValue("Fail")]
        public void Then_the_failure_label_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), QcReportConstants.FailLabel);
        }

        [TestMethod]
        [RequirementValue("N/A")]
        public void Then_the_not_applicable_label_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), QcReportConstants.NotApplicableLabel);
        }
    }
    // ReSharper restore InconsistentNaming
}
