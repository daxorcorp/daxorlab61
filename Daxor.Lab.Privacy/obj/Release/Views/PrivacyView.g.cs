﻿#pragma checksum "..\..\..\Views\PrivacyView.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "CA571B64E25B1DBCC1812E9008BACFDB91CC711C"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Daxor.Lab.Infrastructure.Commands;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Controls;
using Daxor.Lab.Infrastructure.Converters;
using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.RuleFramework;
using Daxor.Lab.Infrastructure.TouchClick;
using Daxor.Lab.Infrastructure.UserControls;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.Privacy.SubViews;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Daxor.Lab.Privacy.Views {
    
    
    /// <summary>
    /// PrivacyView
    /// </summary>
    public partial class PrivacyView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\Views\PrivacyView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Daxor.Lab.Privacy.Views.PrivacyView uiPrivacyView;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\Views\PrivacyView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button uiClose;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Daxor.Lab.Privacy;component/views/privacyview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\PrivacyView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.uiPrivacyView = ((Daxor.Lab.Privacy.Views.PrivacyView)(target));
            return;
            case 2:
            this.uiClose = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\..\Views\PrivacyView.xaml"
            this.uiClose.Click += new System.Windows.RoutedEventHandler(this.ClosePrivacy);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

