﻿using System.Windows;
using System.Windows.Input;

namespace Daxor.Lab.Privacy.SubViews
{
    /// <summary>
    /// Interaction logic for ProgressSubView.xaml
    /// </summary>
    public partial class ProgressSubView
    {
        public ProgressSubView()
        {
            InitializeComponent();
            
        }

        #region NavigationDP

        public ICommand NavigateToMainMenu
        {
            get { return (ICommand)GetValue(NavigateToMainMenuProperty); }
            set { SetValue(NavigateToMainMenuProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NavigateToMainMenu.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NavigateToMainMenuProperty =
            DependencyProperty.Register("NavigateToMainMenu", typeof(ICommand), typeof(ProgressSubView), new UIPropertyMetadata(null));



        public ICommand NavigateToRunningTest
        {
            get { return (ICommand)GetValue(NavigateToRunningTestProperty); }
            set { SetValue(NavigateToRunningTestProperty, value); }
        }

        // Using a DependencyProperty as the backing store for NavigateToRunningTest.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty NavigateToRunningTestProperty =
            DependencyProperty.Register("NavigateToRunningTest", typeof(ICommand), typeof(ProgressSubView), new UIPropertyMetadata(null));

        #endregion



        public bool TestHasFinished
        {
            get { return (bool)GetValue(TestHasFinishedProperty); }
            set { SetValue(TestHasFinishedProperty, value); }
        }   

        // Using a DependencyProperty as the backing store for TestHasFinished.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TestHasFinishedProperty =
            DependencyProperty.Register("TestHasFinished", typeof(bool), typeof(ProgressSubView), new UIPropertyMetadata(false, OnTestHasFinishedChanged));


        private static void OnTestHasFinishedChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            var newValue = (bool)args.NewValue;
            if (newValue)
                VisualStateManager.GoToState((FrameworkElement)o, "TestCompleted", true);
        }
    }
}
