﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.Privacy.Common
{

     /// <summary>
    /// Unique view keys used to identify each UserControl.
    /// </summary>
    internal class PrivacyModuleViewKeys
    {
        public static readonly Guid MainView = Guid.NewGuid();
    }
}
