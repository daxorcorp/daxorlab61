﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.Privacy.Common
{
    /// <summary>
    /// Implementers get notified of navigation action taken by the user
    /// </summary>
    /// TODO: Refactor for the whole system; goes to Infrastructure.
    public interface INavigationAware
    {
        void NavigatingTo();
        void NavigatingFrom(Action callback);
    }

}
