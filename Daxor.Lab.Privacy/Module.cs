﻿using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Privacy.Common;
using Daxor.Lab.Privacy.Views;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.Privacy
{
    public class Module : IModule
    {
	    private const string ModuleName = "Privacy";

        private readonly IUnityContainer _container;
        private readonly IEventAggregator _eventAggregator;
        private readonly IRegionManager _regionManager;

        public Module(IUnityContainer container, IEventAggregator eventAggregator, IRegionManager regionManager)
        {
            _container = container;
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
        }

        public void Initialize()
        {
            RegisterPrivacyView();
            RegisterPrivacyModuleController();
        }

        private void RegisterPrivacyModuleController()
        {
            // Instantiates and registers PrivacyModuleController which governs the life of Privacy Views/ViewModels
            // of privacy module
            try
            {
                _container.RegisterInstance(_container.Resolve<PrivacyModuleController>(), new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the Privacy Module Controller");
            }
        }

        private void RegisterPrivacyView()
        {
            try
            {
                _container.RegisterType<IActiveAware, PrivacyView>(PrivacyModuleViewKeys.MainView.ToString());
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the Privacy view");
            }
        }
    }
}
