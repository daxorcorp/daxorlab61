﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using Daxor.Lab.Privacy.Common;
using Microsoft.Practices.Composite;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.NavigationFramework;

namespace Daxor.Lab.Privacy
{
    /// <summary>
    /// Responsible for Adding/Removed and Activating/Deactivating privacy Views/ViewModels.
    /// Cleaning them when necessary etc.
    /// </summary>
    public class PrivacyModuleController
    {
        #region Fields

        readonly IEventAggregator _eventAggregator;
        readonly IRegionManager _regionManager;
        readonly IUnityContainer _diContainer;

        #endregion

        #region Ctor

        public PrivacyModuleController(IEventAggregator eventAggregator, 
                                       IRegionManager regionManager, 
                                       IUnityContainer diContainer, 
                                       INavigationCoordinator naviCoordinator)
        {
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _diContainer = diContainer;

            SubscribeToAggregateEvents();
        }

        #endregion

        #region Helpers

        private void SubscribeToAggregateEvents()
        {
            //Hook up to AppPartActivation requests
            _eventAggregator.GetEvent<AppPartActivationChanged>().Subscribe(
                (r) => ProcessViewActivation(r), ThreadOption.UIThread,
                true, (s) => { return s.AppPartName == ApplicationParts.Privacy; });
        }
        private void ProcessViewActivation(ActivationChangedPayload activationRequest)
        {
            //Closes active keyboard if present
            VirtualKeyboardManager.CloseActiveKeyboard();

            //Get overlay region
            IRegion viewRegion = (from r in _regionManager.Regions
                                  where r.Name == RegionNames.OverlayRegion
                                  select r).FirstOrDefault();
            if (viewRegion == null)
                return;

            var view = viewRegion.GetView(PrivacyModuleViewKeys.MainView.ToString());
            if(view == null)
            {
                // Instantiate new view through DI container and add to the region
                view =  _diContainer.Resolve<IActiveAware>(PrivacyModuleViewKeys.MainView.ToString());

                //Adds and activates the view
                viewRegion.Add(view, PrivacyModuleViewKeys.MainView.ToString());
            }

            //To active a view
            if (activationRequest.IsActivated)
            {

                if (!viewRegion.ActiveViews.Contains(view))
                {
                    //Notify current active view of future deactivation;
                    INavigationAware currentActiveView = viewRegion.ActiveViews.FirstOrDefault() as INavigationAware;
                    if (currentActiveView != null)
                    {
                        currentActiveView.NavigatingFrom(() =>
                        {
                            viewRegion.Deactivate(currentActiveView);
                            viewRegion.Remove(currentActiveView);
                            currentActiveView = null;

                          
                            //Adds view to the region and activates it(default behavior of singleActive region)
                            viewRegion.Activate(view);

                            //Notify newView of preparing to navigate to
                            INavigationAware newView = view as INavigationAware;
                            if (newView != null)
                                newView.NavigatingTo();
                        });
                    }
                  
                }
                else
                {
                    //Notify newView of preparing to navigate to
                    INavigationAware newView = view as INavigationAware;
                    if (newView != null)
                        newView.NavigatingTo();
                }
            }
            //To deactivate a view
            else
            {
                if (viewRegion.ActiveViews.Contains(view))
                {
                    //Notify current active view of future deactivation;
                    INavigationAware currentActiveView = viewRegion.ActiveViews.FirstOrDefault() as INavigationAware;
                    if (currentActiveView != null)
                    {
                        currentActiveView.NavigatingFrom(() =>
                        {
                            if (currentActiveView != null)
                            {
                                viewRegion.Deactivate(currentActiveView);
                                viewRegion.Remove(currentActiveView);
                                currentActiveView = null;
                            }
                        });
                    }
                }
            }
        }

        #endregion
    }
}
