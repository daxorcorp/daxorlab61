﻿using System;
using System.Globalization;
using System.Windows.Input;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.Privacy.Common;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Privacy.ViewModels
{
    public class PrivacyViewModel : ViewModelBase, IActiveAware, INavigationAware
    {
        #region Constants

        public readonly string TimeUnitsLabelSingular = "Minute";
        public readonly string TimeUnitsLabelPlural = "Minutes";
        public readonly string InProgressLabel = "in Progress";
        public readonly string EstimatedUntilCompleteLabel = "Estimated Until Complete";

        #endregion

        #region Fields

        readonly INavigationCoordinator _navigationService;
        readonly IEventAggregator _eventAggregator;
        readonly ITestExecutionController _testExecutionController;
        readonly DelegateCommand<Object> _navigateToMainMenuCommand;
        readonly DelegateCommand<Object> _navigateToRunningTestCommand;

        PropertyObserver<ProgressSubViewModel> _progressObserver;
        ProgressSubViewModel _progressViewModel;

        #endregion

        #region Properties

        public ProgressSubViewModel ProgressViewModel
        {
            get { return _progressViewModel; }
            set
            {
                if (value == null)
                    _progressObserver = null;
                else
                {
                    _progressObserver = new PropertyObserver<ProgressSubViewModel>(value);
                    _progressObserver.RegisterHandler(p => p.IsProgressVisible, vm => UpdateProgressViewModel());
                }

                base.SetValue(ref _progressViewModel, "ProgressViewModel", value);
            }       
        }
     
        #endregion

        #region Ctor

        public PrivacyViewModel(INavigationCoordinator navigationService, 
                                IEventAggregator eventAggregator, 
                                ITestExecutionController testExecutionController,
                                ProgressSubViewModel vm)
        {   
            _navigationService = navigationService;
            _eventAggregator = eventAggregator;
            _testExecutionController = testExecutionController;
            ProgressViewModel = vm;

            //Create delegate commands navigation commands
            _navigateToMainMenuCommand = new DelegateCommand<object>(NavigateToMainMenu);
            _navigateToRunningTestCommand = new DelegateCommand<object>(NavigateToRunningTest, CanNavigateToRunningTest);
        }

        #endregion

        #region Commands

        public ICommand NavigateToMainMenuCommand
        {
            get { return _navigateToMainMenuCommand; }
        }
        void NavigateToMainMenu(Object param)
        {
            _eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(ApplicationParts.Privacy, false));

            if (_navigationService.ActiveAppModuleKey != AppModuleIds.MainMenu)
                _navigationService.ActivateAppModule(AppModuleIds.MainMenu, null, DisplayViewOption.Default, _navigationService.ActiveAppModuleKey);
        }

        public ICommand NavigateToRunningTestCommand
        {
            get { return _navigateToRunningTestCommand; }
        }
        void NavigateToRunningTest(Object param)
        {
            if (_testExecutionController.IsExecuting)
            {
                var metadata = _testExecutionController.ExecutingContextMetadata as TestExecutionContextMetadata;
                if (metadata == null)
                    return;
                
                _eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(ApplicationParts.Privacy, false));
                _navigationService.ActivateAppModule(metadata.AppModuleKey, _testExecutionController.ExecutingTest, DisplayViewOption.Result, _navigationService.ActiveAppModuleKey);

            }
        }
        bool CanNavigateToRunningTest(Object param)
        {
            return _testExecutionController.IsExecuting ;
        }

        #endregion

        #region Helpers

        void OnTestStatusChanged(ITest test)
        {
            if (test.Status == TestStatus.Completed)
                  ProgressViewModel.IsTestCompleted = true;
            else
            {
                ProgressViewModel.IsProgressVisible = test.Status == TestStatus.Running;
                ProgressViewModel.IsTestCompleted = false;
            }

 
        }
        void OnExecutionProgressChanged(TestExecutionProgressChangedEventArgs progressChangedPayload)
        {
            UpdateProgressViewModel();
        }

        void UpdateProgressViewModel()
        {
            if (!_testExecutionController.IsExecuting)
                return;

            if (_testExecutionController.ProgressMetadata.RemainingExecutionTimeInSeconds <= 0)
            {
                ProgressViewModel.TimeRemaining = "Test Complete";
                ProgressViewModel.PercentageCompleted = 100;
                ProgressViewModel.TimeLabel = "";
                ProgressViewModel.TimeUnits = "";
            }
            else
            {
                ProgressViewModel.PercentageCompleted = _testExecutionController.ProgressMetadata.PercentCompleted;

                // At or beyond 30-second point rounds up; below rounds down
                var timeRemaining = (int)Math.Round(_testExecutionController.ProgressMetadata.RemainingExecutionTimeInSeconds / TimeConstants.SecondsPerMinute, 0);
                
                // To prevent showing "0", simply display "<1" for 0-59 seconds
                ProgressViewModel.TimeRemaining = _testExecutionController.ProgressMetadata.RemainingExecutionTimeInSeconds > 59 ? 
                    timeRemaining.ToString(CultureInfo.InvariantCulture) : "<1";

                // 0:00-0:59 == "<1 Minute"; 1:00-1:29 == "1 Minute"
                ProgressViewModel.TimeUnits = _testExecutionController.ProgressMetadata.RemainingExecutionTimeInSeconds < 90 ? 
                    TimeUnitsLabelSingular : TimeUnitsLabelPlural;
            }
        }
        
        #endregion

        #region IActiveAware Members

        public bool IsActive
        {
            get;
            set;
        }
        public event EventHandler IsActiveChanged = delegate { };

        #endregion

        #region INavigationAware Members

        protected virtual void SubscribeToAggregrateEvents()
        {
            SubscribeToTestExecutionProgressChangedEvent();
            SubscribeToTestStateChangeEvents();
        }

        protected virtual void SubscribeToTestExecutionProgressChangedEvent(ThreadOption threadOption = ThreadOption.UIThread)
        {
            _eventAggregator.GetEvent<TestExecutionProgressChanged>().Subscribe(OnExecutionProgressChanged, threadOption, false);
        }

        protected virtual void SubscribeToTestStateChangeEvents()
        {
            _eventAggregator.GetEvent<TestStarted>().Subscribe(OnTestStatusChanged, ThreadOption.UIThread, false);
            _eventAggregator.GetEvent<TestAborted>().Subscribe(OnTestStatusChanged, ThreadOption.UIThread, false);
            _eventAggregator.GetEvent<TestCompleted>().Subscribe(OnTestStatusChanged, ThreadOption.UIThread, false); 
        }

        void INavigationAware.NavigatingTo()
        {
            SubscribeToAggregrateEvents();
          
            ProgressViewModel.IsProgressVisible = _testExecutionController.IsExecuting;
            if (ProgressViewModel.IsProgressVisible)
            {
                  var metadata = _testExecutionController.ExecutingContextMetadata as TestExecutionContextMetadata;
                  if (metadata != null)
                  {
                      ProgressViewModel.ProgressHeader = String.Format("{0} {1}", metadata.DisplayTestName, InProgressLabel);
                      ProgressViewModel.TimeLabel = String.Format(" {0}", EstimatedUntilCompleteLabel);
                  }
            }

            UpdateProgressViewModel();
        }
        void INavigationAware.NavigatingFrom(Action callback)
        {
            _eventAggregator.GetEvent<TestExecutionProgressChanged>().Unsubscribe(OnExecutionProgressChanged);
            _eventAggregator.GetEvent<TestStarted>().Unsubscribe(OnTestStatusChanged);
            _eventAggregator.GetEvent<TestAborted>().Unsubscribe(OnTestStatusChanged);
            _eventAggregator.GetEvent<TestCompleted>().Unsubscribe(OnTestStatusChanged);

            if (callback != null)
                callback();
        }

        #endregion
    }
}
