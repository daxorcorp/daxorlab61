﻿using Daxor.Lab.Infrastructure.Base;

namespace Daxor.Lab.Privacy.ViewModels
{
    public class ProgressSubViewModel : ViewModelBase
    {
        string _timeRemaining;
        string _progressHeader;
        int _precentageCompleted;
        string _timeUnits;
        string _timeLabel;
        bool _isProgressViewVisible;
        bool _isTestCompleted;

        public string TimeRemaining
        {
            get { return _timeRemaining; }
            set { base.SetValue(ref _timeRemaining, "TimeRemaining", value); }
        }
        public string ProgressHeader
        {
            get { return _progressHeader; }
            set { base.SetValue(ref _progressHeader, "ProgressHeader", value); }
        }
        public int PercentageCompleted
        {
            get { return _precentageCompleted; }
            set { base.SetValue(ref _precentageCompleted, "PercentageCompleted", value); }
        }
        public string TimeUnits
        {
            get { return _timeUnits; }
            set { base.SetValue(ref _timeUnits, "TimeUnits", value); }
        }
        public string TimeLabel
        {
            get { return _timeLabel; }
            set { base.SetValue(ref _timeLabel, "TimeLabel", value); }
        }
        public bool IsProgressVisible
        {
            get { return _isProgressViewVisible; }
            set { base.SetValue(ref _isProgressViewVisible, "IsProgressVisible", value); }
        }
        public bool IsTestCompleted
        {
            get { return _isTestCompleted; }
            set { base.SetValue(ref _isTestCompleted, "IsTestCompleted", value); }
        }
    }
}
