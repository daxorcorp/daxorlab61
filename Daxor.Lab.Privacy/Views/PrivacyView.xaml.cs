﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Privacy.ViewModels;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Composite.Regions;
using Daxor.Lab.Privacy.Common;
using Daxor.Lab.Infrastructure.Utilities;
using System.Windows.Media.Animation;
using System.Windows.Media;

namespace Daxor.Lab.Privacy.Views
{
    /// <summary>
    /// Interaction logic for PrivacyView.xaml
    /// </summary>
    public partial class PrivacyView : UserControl, IActiveAware, INavigationAware
    {
        #region Fields

        private IEventAggregator _eventAggregator;
        private Action _callbackNavigationFrom;
        private Storyboard _activationStoryboard;
        private Storyboard _deactivationStoryboard;
        private TransitionState _currTransitionState;
        #endregion

        #region Ctor

        public PrivacyView()
        {
            InitializeComponent();

            if (DesignerPropertiesExtension.IsInDesignModeStatic)
            {
                var tGroup = new TransformGroup();
                tGroup.Children.Add(new TranslateTransform(0, 0));
                RenderTransform = tGroup;
            }

            _currTransitionState = TransitionState.Closed;

            //Two storyboards controlling activation and deactivation
            _activationStoryboard = (Storyboard)TryFindResource("showViewStoryboard");
            _deactivationStoryboard = (Storyboard)TryFindResource("hideViewStoryboard");
        }
        public PrivacyView(PrivacyViewModel vm, IEventAggregator eventAggregator)
            : this()
        {
            DataContext = vm;
            _eventAggregator = eventAggregator;
        }

        #endregion

        #region IActiveAware Members

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                IActiveAware vmAware = DataContext as IActiveAware;
                if (vmAware != null)
                    vmAware.IsActive = value;
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion

        #region INavigationAware Members

        //Navigating to
        void INavigationAware.NavigatingTo()
        {
            //Already in the process of opening
            if (_currTransitionState == TransitionState.Opening || _currTransitionState == TransitionState.Opened)
                return;

            _currTransitionState = TransitionState.Opening;

            PreviewMouseLeftButtonDown += PrivacyView_PreviewMouseLeftButtonDown;
            _activationStoryboard.Completed += new StoryboardEventHandlerWrapper<UIElement>(this, _activationStoryboard,
                (ui) =>
                {
                    if (ui != null)
                        ui.PreviewMouseLeftButtonDown -= PrivacyView_PreviewMouseLeftButtonDown;

                    _currTransitionState = TransitionState.Opened;

                    INavigationAware vmNavigationAware = DataContext as INavigationAware;
                    if (vmNavigationAware != null)
                        vmNavigationAware.NavigatingTo();

                }).CustomHandler;

            //Starts animation
            _activationStoryboard.Begin();
        }

        //Navigating from
        void INavigationAware.NavigatingFrom(Action callback)
        {
            //Already in the process of closing/closed
            if (_currTransitionState == TransitionState.Closing || _currTransitionState == TransitionState.Closed)
                return;

            _currTransitionState = TransitionState.Closing;

            _callbackNavigationFrom = (Action)callback;
            _deactivationStoryboard.Completed += new StoryboardEventHandlerWrapper<Object>(null, _deactivationStoryboard,
                (o) =>
                {
                    if (_callbackNavigationFrom != null)
                    {
                        _callbackNavigationFrom();
                    }
                    _currTransitionState = TransitionState.Closed;

                    INavigationAware vmNavigationAware = DataContext as INavigationAware;
                    if (vmNavigationAware != null)
                        vmNavigationAware.NavigatingFrom(null);

                }).CustomHandler;

            //Starts animation
            _deactivationStoryboard.Begin();
        }

        #endregion

        #region Helpers

        void ClosePrivacy(object sender, RoutedEventArgs e)
        {
            _eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(ApplicationParts.Privacy, false));
        }
        //Handles all interaction while navigation is in process
        void PrivacyView_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;
        }
       
        #endregion

      
    }

    /// <summary>
    /// Signifies the transition state of the view
    /// </summary>
    internal enum TransitionState
    {
        Closed,
        Opened,
        Opening,
        Closing
    }
}
