﻿using System;
using Daxor.Lab.Header.Views;
using Daxor.Lab.Infrastructure.Exceptions;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Header.Tests.Module_Tests.SadPath
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_the_Header_module
	{
		private Module _module;

		private IRegionManager _regionManager;
		private IUnityContainer _container;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_container = Substitute.For<IUnityContainer>();
			_regionManager = Substitute.For<IRegionManager>();
		}

		[TestMethod]
		public void And_an_error_occurs_while_resolving_the_header_view_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<HeaderView>()).Throw(new Exception());
			_module = new Module(_regionManager, _container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Header", ex.ModuleName);
				Assert.AreEqual("resolve the Header view", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}
	}
	// ReSharper restore InconsistentNaming
}