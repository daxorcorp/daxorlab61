﻿using Daxor.Lab.DatabaseServices.Common;
using Daxor.Lab.DatabaseServices.IntegrationTests.TestHelpers;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System.IO;

namespace Daxor.Lab.DatabaseServices.IntegrationTests.BackupService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_backing_up_daxor_database
    {
        private static IBackupServiceFactory _factory;
        private bool _wasCalled;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _factory = TestUtilites.CreateBackupServiceFactory(Constants.Version);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            File.Delete(Constants.BackupFilePath);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_backup_complete_handler_is_hooked_up_then_its_used()
        {
            var backup = _factory.CreateDaxorLabBackupService();
            _wasCalled = false;
            backup.BackupCompletedHandler = (sender, args) => { _wasCalled = true; };

            backup.Backup(Constants.BackupFilePath);
            Assert.IsTrue(_wasCalled);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_backup_percent_handler_is_hooked_up_then_its_used()
        {
            var backup = _factory.CreateDaxorLabBackupService();
            _wasCalled = false;
            backup.PercentCompleteHandler = (sender, args) => { _wasCalled = true; };

            backup.Backup(Constants.BackupFilePath);
            Assert.IsTrue(_wasCalled);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_backup_is_created()
        {
            var backup = _factory.CreateDaxorLabBackupService();
            backup.Backup(Constants.BackupFilePath);

            Assert.IsTrue(File.Exists(Constants.BackupFilePath));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_backup_already_exists_then_the_backup_is_erased_first()
        {
            var mockFileManager = MockRepository.GenerateMock<IFileManager>();
            mockFileManager.Expect(x => x.FileExists(null)).IgnoreArguments().Return(true);
            mockFileManager.Expect(x => x.DeleteFile(Constants.BackupFilePath));

            SafeFileOperations.FileManager = mockFileManager;

            var backup = _factory.CreateDaxorLabBackupService();
            backup.Backup(Constants.BackupFilePath);

            mockFileManager.VerifyAllExpectations();

            SafeFileOperations.FileManager = null;
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_version_is_added_to_the_backup()
        {
            var backup = _factory.CreateDaxorLabBackupService();
            backup.Backup(Constants.BackupFilePath);

            var backupVersion = VersionExtractor.GetBackupVersion(Constants.BackupFilePath);
            Assert.IsTrue(backupVersion == Constants.Version);
        }
    }
    // ReSharper restore InconsistentNaming
}