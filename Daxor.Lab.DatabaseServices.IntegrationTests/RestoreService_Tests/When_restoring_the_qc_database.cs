﻿using Daxor.Lab.DatabaseServices.Factories;
using Daxor.Lab.DatabaseServices.IntegrationTests.TestHelpers;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace Daxor.Lab.DatabaseServices.IntegrationTests.RestoreService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_restoring_the_qc_database
    {
        private const string InvalidVersion = "6.0.0.1";
        private const string ValidVersion = "6.1.2.3";
        private const string FilePath = @"C:\temp\create.bak";

        private static ISettingsManager _settingsManager;
        private static RestoreServiceFactory _restoreFactory;
        private static IBackupServiceFactory _backupFactory;

        private bool _wasCalled;

        [TestInitialize]
        public void TestInitialize()
        {
            _settingsManager = TestUtilites.CreateSettingsManager(ValidVersion);
            _restoreFactory = new RestoreServiceFactory(_settingsManager);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            File.Delete(FilePath);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_version_of_the_backup_is_different_from_the_running_version_then_the_backup_is_restored()
        {
            CreateBackup(InvalidVersion);
            var restoreService = _restoreFactory.CreateQcArchiveRestoreService();

            restoreService.Restore(FilePath);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_version_of_the_backup_is_the_same_as_running_version_then_the_backup_is_restored()
        {
            CreateBackup(ValidVersion);
            var restoreService = _restoreFactory.CreateQcArchiveRestoreService();

            restoreService.Restore(FilePath);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_percent_complete_handler_is_hooked_up_then_its_used()
        {
            CreateBackup(ValidVersion);

            var restoreService = _restoreFactory.CreateQcArchiveRestoreService();
            restoreService.PercentCompleteHandler = (sender, args) => { _wasCalled = true; };

            _wasCalled = false;            
            restoreService.Restore(FilePath);

            Assert.IsTrue(_wasCalled);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_on_restore_complete_handler_is_hooked_up_then_its_used()
        {
            CreateBackup(ValidVersion);

            var restoreService = _restoreFactory.CreateQcArchiveRestoreService();
            restoreService.RestoreCompletedHandler = (sender, args) => { _wasCalled = true; };

            _wasCalled = false;
            restoreService.Restore(FilePath);

            Assert.IsTrue(_wasCalled);
        }

        private void CreateBackup(string version)
        {
            _backupFactory = TestUtilites.CreateBackupServiceFactory(version);
            var service = _backupFactory.CreateQcArchiveBackupService();
            service.Backup(FilePath);
        }
    }
    // ReSharper restore InconsistentNaming
}
