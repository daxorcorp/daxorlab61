﻿using Daxor.Lab.DatabaseServices.Factories;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Rhino.Mocks;

namespace Daxor.Lab.DatabaseServices.IntegrationTests.TestHelpers
{
    public static class TestUtilites
    {
        public static IBackupServiceFactory CreateBackupServiceFactory(string version)
        {
            var stubSettingsManager = CreateSettingsManager(version);
            return new BackupServiceFactory(stubSettingsManager);
        }

        public static ISettingsManager CreateSettingsManager(string version)
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemQcDatabaseArchiveDatabaseName)).Return("QCDatabaseArchive");
            stubSettingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemDatabaseUsername)).Return("DaxorDBO");
            stubSettingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemDatabasePassword)).Return("daxor");
            stubSettingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemDatabaseServer)).Return("(local)");
            stubSettingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemDatabaseName)).Return("DaxorLab");
            stubSettingsManager.Expect(x => x.GetSetting<string>(SettingKeys.BvaModuleVersion)).Return(version);
            
            return stubSettingsManager;
        }
    }
}
