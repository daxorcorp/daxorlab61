﻿namespace Daxor.Lab.DatabaseServices.IntegrationTests
{
    public static class Constants
    {
        public const string BackupFilePath = @"C:\temp\create.bak";
        public const string Version = "6.0.1.4432";
    }
}
