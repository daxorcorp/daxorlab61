﻿using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Infrastructure.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Extensions_Tests.LinqExtensions_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_partitioning_an_IEnumerable
    {
        [TestMethod]
        public void And_there_are_fewer_items_in_the_list_than_the_list_size_then_a_collection_with_a_collection_containing_all_the_items_is_returned()
        {
            var listOfEightObjects = new List<object>();

            for(var i=0; i < 8; i++)
                listOfEightObjects.Add(new object());

            var observedResult = listOfEightObjects.Partition(listSize: 10);

            // ReSharper disable once AssignNullToNotNullAttribute
            CollectionAssert.AreEqual(listOfEightObjects, observedResult.FirstOrDefault().ToList());
        }

        [TestMethod]
        public void And_there_are_more_items_in_the_list_than_the_list_size_then_a_collection_with_n_collections_is_returned()
        {
            var listOfEightObjects = new List<object>();

            for (var i = 0; i < 8; i++)
                listOfEightObjects.Add(new object());

            var observedResult1 = listOfEightObjects.Partition(listSize: 5);

            Assert.AreEqual(2, observedResult1.Count());

            var observedResult2 = listOfEightObjects.Partition(listSize: 2);

            Assert.AreEqual(4, observedResult2.Count());
        }

        [TestMethod]
        public void And_there_are_no_items_in_the_list_then_an_IEnumerable_with_no_elements_is_returned()
        {
            var listWithNoObjects = new List<object>();

            var observedResult = listWithNoObjects.Partition(listSize: 2);

            Assert.AreEqual(0, observedResult.Count());
        }
    }
    // ReSharper restore RedundantArgumentName
}