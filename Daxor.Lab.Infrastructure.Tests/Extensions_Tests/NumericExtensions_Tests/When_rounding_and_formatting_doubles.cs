using Daxor.Lab.Infrastructure.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Extensions_Tests.NumericExtensions_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_rounding_and_formatting_doubles
    {
        [TestMethod]
        public void And_the_number__is_between_one_and_zero_then_the_string_starts_with_zero()
        {
            const string expectedResult = "0.4";
            var observedResult = .4.RoundAndFormat(1);
            Assert.AreEqual(expectedResult, observedResult);
        }

        [TestMethod]
        public void And_the_number_is_whole_then_no_rounding_takes_place()
        {
            const string expectedResult = "2";
            var observedResult = 2.0.RoundAndFormat(0);
            Assert.AreEqual(expectedResult, observedResult);
        }

        [TestMethod]
        public void And_the_number_has_only_one_decimal_place_then_rounding_to_one_decimal_place_does_not_change_the_value()
        {
            const string expectedResult = "2.3";
            var observedResult = 2.3.RoundAndFormat(1);
            Assert.AreEqual(expectedResult, observedResult);
        }

        [TestMethod]
        public void And_the_number_has_two_decimal_places_then_rounding_to_one_decimal_rounds_up_from_midpoint_as_expected()
        {
            const string expectedResult = "2.5";
            var observedResult = 2.45.RoundAndFormat(1);
            Assert.AreEqual(expectedResult, observedResult);
        }

        [TestMethod]
        public void And_the_number_has_two_decimal_places_then_rounding_to_one_decimal_place_rounds_down()
        {
            const string expectedResult = "2.4";
            var observedResult = 2.41.RoundAndFormat(1);
            Assert.AreEqual(expectedResult, observedResult);
        }

        [TestMethod]
        public void And_the_number_has_four_decimal_places_then_rounding_to_three_decimal_places_works_as_expected()
        {
            const string expectedResult = "3.458";
            var observedResult = 3.4578.RoundAndFormat(3);
            Assert.AreEqual(expectedResult, observedResult);
        }
    }
    // ReSharper restore InconsistentNaming
}
