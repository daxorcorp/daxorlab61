﻿using System.Windows.Data;
using Daxor.Lab.Infrastructure.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_an_enum_to_a_boolean
    {
        public enum EnumTest
        {
            ThingOne,
            ThingTwo
        }

        private static EnumToBooleanConverter _converter;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _converter = new EnumToBooleanConverter();
        }

        [TestMethod]
        public void And_the_value_matches_the_parameter_then_true_is_returned()
        {
            Assert.IsTrue((bool) _converter.Convert(EnumTest.ThingOne, null, EnumTest.ThingOne, null));
        }

        [TestMethod]
        public void And_the_value_and_the_parameter_do_not_match_then_false_is_returned()
        {
            Assert.IsFalse((bool)_converter.Convert(EnumTest.ThingOne, null, EnumTest.ThingTwo, null));
        }

        [TestMethod]
        public void And_the_value_is_true_then_the_parameter_is_returned()
        {
            const EnumTest expectedEnum = EnumTest.ThingOne;
            Assert.AreEqual(expectedEnum, _converter.ConvertBack(true, null, expectedEnum, null));
        }

        [TestMethod]
        public void And_the_value_is_false_then_BindingDoNothing_is_returned()
        {
            const EnumTest expectedEnum = EnumTest.ThingOne;
            Assert.AreEqual(Binding.DoNothing, _converter.ConvertBack(false, null, expectedEnum, null));
        }
    }
    // ReSharper restore InconsistentNaming
}
