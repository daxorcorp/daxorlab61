﻿using System;
using System.Collections.Generic;
using System.Windows;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters
{
    [TestClass]
    // ReSharper disable InconsistentNaming
    public class When_converting_a_running_TestStatus_to_visibility
    {
        private static TestStatusRunningToVisibilityConverter _converter;
     
        [ClassInitialize]
        public static void TestsClassInitialize(TestContext context)
        {
            _converter = new TestStatusRunningToVisibilityConverter();
        }

        [TestMethod]
        public void And_the_test_status_is_anything_but_running_then_collapsed_is_returned()
        {
            var numStatuses = Enum.GetNames(typeof(TestStatus)).Length;
            var testStatusesThatFailed = new List<TestStatus>();
            for (var i = 0; i < numStatuses; i++)
            {
                var status = (TestStatus)i;
                if (status == TestStatus.Running)
                    continue;

                var actual = (Visibility)_converter.Convert(status, null, null, null);
                if (actual != Visibility.Collapsed)
                    testStatusesThatFailed.Add(status);

            }
            Assert.IsTrue(testStatusesThatFailed.Count == 0, TestStatusVisibilityHelper.CreateAssertMessage(testStatusesThatFailed));
        }

        [TestMethod]
        public void And_the_test_status_is_running_then_visible_is_returned()
        {
            var observedConversion = _converter.Convert(TestStatus.Running, null, null, null);
            Assert.AreEqual(Visibility.Visible, observedConversion);
        }
    }
    // ReSharper restore InconsistentNaming
}
