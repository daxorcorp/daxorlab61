﻿using Daxor.Lab.Infrastructure.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_regular_expression_to_a_boolean
    {
        private static RegexToBooleanConverter _converter;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _converter = new RegexToBooleanConverter();
        }

        [TestMethod]
        public void And_the_value_matches_the_regex_then_true_is_returned()
        {
            Assert.IsTrue((bool) _converter.Convert("abc123", null, @"\d", null));
        }

        [TestMethod]
        public void And_the_value_does_not_match_the_regex_then_false_is_returned()
        {
            Assert.IsFalse((bool)_converter.Convert("abcdef", null, @"\d", null));
        }

        [TestMethod]
        public void And_the_regex_is_invalid_then_false_is_returned()
        {
            Assert.IsFalse((bool)_converter.Convert("abcdef", null, @"\6", null));
        }
        
        [TestMethod]
        public void And_the_input_string_is_null_then_false_is_returned()
        {
            Assert.IsFalse((bool)_converter.Convert(null, null, @"\d", null));
        }

        [TestMethod]
        public void And_the_regex_is_null_then_false_is_returned()
        {
            Assert.IsFalse((bool)_converter.Convert("123", null, null, null));
        }
    }
    // ReSharper restore InconsistentNaming
}
