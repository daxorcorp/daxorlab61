﻿using Daxor.Lab.Infrastructure.Converters;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_an_enum_to_a_boolean
    {
        [TestMethod]
        public void And_the_value_is_null_then_an_exception_is_thrown()
        {
            var converter = new EnumToBooleanConverter();
            AssertEx.ThrowsArgumentNullException(()=>converter.Convert(null, null, null, null), "value");
        }

        [TestMethod]
        public void And_converting_back_and_the_value_is_null_then_an_exception_is_thrown()
        {
            var converter = new EnumToBooleanConverter();
            AssertEx.ThrowsArgumentNullException(()=>converter.ConvertBack(null, null, null, null), "value");
        }
    }
    // ReSharper restore InconsistentNaming
}
