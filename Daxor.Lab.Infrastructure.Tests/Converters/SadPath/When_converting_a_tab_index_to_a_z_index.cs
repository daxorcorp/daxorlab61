﻿using System.Windows.Controls;
using System.Windows.Data;
using Daxor.Lab.Infrastructure.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters.SadPath
{
    [TestClass]
    // ReSharper disable InconsistentNaming
    public class When_converting_a_tab_index_to_a_z_index
    {
        private static TabIndexToZIndexConverter _converter;

        [ClassInitialize]
        public static void TestsClassInitialize(TestContext context)
        {
            _converter = new TabIndexToZIndexConverter();
        }
        
        [TestMethod]
        public void And_the_values_array_is_null_then_Binding_DoNothing_is_returned()
        {
            var expectedResult = Binding.DoNothing;

            var observedResult = _converter.Convert(null, null, null, null);

            Assert.AreEqual(expectedResult, observedResult);
        }

        [TestMethod]
        public void And_the_values_array_does_not_contain_three_items_then_Binding_DoNothing_is_returned()
        {
            var expectedResult = Binding.DoNothing;
            var values = new object[] {new TabItem(), new TabControl()};
            var observedResult = _converter.Convert(values, null, null, null);

            Assert.AreEqual(expectedResult, observedResult);
        }

        [TestMethod]
        public void And_the_TabItem_is_null_then_Binding_DoNothing_is_returned()
        {
            var expectedResult = Binding.DoNothing;
            var values = new object[] {null, new TabControl(), 3};
            var observedResult = _converter.Convert(values, null, null, null);

            Assert.AreEqual(expectedResult,observedResult);
        }

        [TestMethod]
        public void And_the_TabControl_is_null_then_Binding_DoNothing_is_returned()
        {
            var expectedResult = Binding.DoNothing;
            var values = new object[] { new TabItem(), null, 3 };
            var observedResult = _converter.Convert(values, null, null, null);

            Assert.AreEqual(expectedResult, observedResult);
        }

        [TestMethod]
        public void And_the_TabCount_is_null_then_Binding_DoNothing_is_returned()
        {
            var expectedResult = Binding.DoNothing;
            var values = new object[] { new TabItem(), new TabControl(), null};
            var observedResult = _converter.Convert(values, null, null, null);

            Assert.AreEqual(expectedResult, observedResult);
        }

        [TestMethod]
        public void And_the_TabCount_is_not_a_valid_integer_then_Binding_DoNothing_is_returned()
        {
            var expectedResult = Binding.DoNothing;
            var values = new object[] { new TabItem(), new TabControl(), "abc" };
            var observedResult = _converter.Convert(values, null, null, null);

            Assert.AreEqual(expectedResult, observedResult);
        }

    }
    // ReSharper restore InconsistentNaming
}
