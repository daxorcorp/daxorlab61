﻿using System;
using Daxor.Lab.Infrastructure.Converters;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_gender_to_a_boolean
    {
        private static GenderToBooleanConverter _converter;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _converter = new GenderToBooleanConverter();
        }

        [TestMethod]
        public void And_the_value_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_converter.Convert(null, null, null, null), "value");
        }

        [TestMethod]
        public void And_the_value_is_not_a_gender_then_an_exception_is_thrown()
        {
            AssertEx.Throws<NotSupportedException>(()=>_converter.Convert(DateTime.Now, null, null, null));
        }

        [TestMethod]
        public void And_converting_back_and_the_value_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_converter.ConvertBack(null, null, null, null), "value");
        }

        [TestMethod]
        public void And_the_value_is_not_a_boolean_then_an_exception_is_thrown()
        {
            AssertEx.Throws<NotSupportedException>(()=>_converter.ConvertBack(DateTime.Now, null, null, null));
        }
    }
    // ReSharper restore InconsistentNaming
}
