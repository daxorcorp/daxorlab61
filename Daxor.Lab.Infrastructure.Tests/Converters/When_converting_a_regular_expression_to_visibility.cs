﻿using System.Linq;
using System.Windows;
using Daxor.Lab.Infrastructure.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters
{
    [TestClass]
    // ReSharper disable InconsistentNaming
    public class When_converting_a_regular_expression_to_visibility
    {
        private static RegExpToVisibilityConverter _converter;
        private const string InputString = "This is the value";

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _converter = new RegExpToVisibilityConverter(); 
        }
        
        [TestMethod]
        public void And_an_invalid_regular_expression_is_given_then_the_value_is_returned_unchanged()
        {
            const string invalidRegEx = (string) null;
            const Visibility inputValue = Visibility.Visible;

            var observedConversion = _converter.Convert(inputValue, null, invalidRegEx, null);

            Assert.AreEqual(inputValue, observedConversion);
        }

        [TestMethod]
        public void And_a_valid_regular_expression_is_given_and_there_is_a_match_then_visible_is_returned()
        {
            var regEx = InputString.Substring(5);  // Ensured to match
            const Visibility expectedConversion = Visibility.Visible;

            var observedConversion = _converter.Convert(InputString, null, regEx, null);

            Assert.AreEqual(expectedConversion, observedConversion);
        }

        [TestMethod]
        public void And_a_valid_RegEx_is_passed_and_there_is_not_a_match_then_convert_returns_collapsed()
        {
            var regEx = InputString.Substring(5).Reverse();  // Ensured not to match
            const Visibility expectedConversion = Visibility.Collapsed;

            var observedConversion = _converter.Convert(InputString, null, regEx, null);

            Assert.AreEqual(expectedConversion, observedConversion);
        }
    }
    // ReSharper restore InconsistentNaming
}
