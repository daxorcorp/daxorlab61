﻿using Daxor.Lab.Infrastructure.Converters;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_ComboBox_item_values_to_a_default_value
    {
        private static ComboBoxItemValueToDefaultValueConverter _converter;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _converter = new ComboBoxItemValueToDefaultValueConverter();
        }

        [TestMethod]
        [RequirementValue("SELECT")]
        public void Then_the_default_value_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), ComboBoxItemValueToDefaultValueConverter.DefaultValue);
        }

        [TestMethod]
        public void And_the_item_value_to_convert_is_an_empty_string_then_the_default_value_is_returned()
        {
            var actualValue = _converter.Convert(string.Empty, null, null, null);

            Assert.AreEqual(ComboBoxItemValueToDefaultValueConverter.DefaultValue, actualValue);
        }

        [TestMethod]
        public void And_the_item_value_is_not_the_empty_string_then_the_given_item_value_is_returned()
        {
            var actualValue = _converter.Convert(string.Empty, null, null, null);

            Assert.AreEqual(ComboBoxItemValueToDefaultValueConverter.DefaultValue, actualValue);
        }

        [TestMethod]
        public void And_the_item_value_is_null_then_the_given_null_is_returned()
        {
            var actualValue = _converter.Convert(null, null, null, null);

            Assert.AreEqual(null, actualValue);
        }
    }
    // ReSharper restore InconsistentNaming
}
