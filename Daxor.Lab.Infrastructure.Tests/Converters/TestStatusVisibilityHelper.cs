﻿using System.Collections.Generic;
using System.Text;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.Infrastructure.Tests.Converters
{
    public class TestStatusVisibilityHelper
    {
        public static string CreateAssertMessage(IEnumerable<TestStatus> testStatusesThatFailed)
        {
            var builder = new StringBuilder();
            foreach (var status in testStatusesThatFailed)
                builder.Append(status.ToString() + "\n");

            return builder.ToString();
        }
    }
}