﻿using System;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_DateTime_to_null
    {
        private static DateTimeToNullConverter _converter;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _converter = new DateTimeToNullConverter();
        }

        [TestMethod]
        public void And_the_given_DateTime_is_null_then_null_is_returned()
        {
            Assert.IsNull(_converter.Convert(null, null, null, null));
        }

        [TestMethod]
        public void And_the_given_DateTime_is_the_null_date_constant_then_null_is_returned()
        {
            Assert.IsNull(_converter.Convert(DomainConstants.NullDateTime, null, null, null));
        }

        [TestMethod]
        public void And_the_given_DateTime_is_invalid_then_null_is_returned()
        {
            Assert.IsNull(_converter.Convert("02/30/2013 12:00:00", null, null, null));
        }

        [TestMethod]
        public void And_the_given_DateTime_is_valid_and_not_the_null_date_constant_then_the_given_DateTime_is_returned()
        {
            var testingDateTime = new DateTime(2013, 2, 15);
            Assert.AreEqual(testingDateTime, _converter.Convert(testingDateTime, null, null, null));
        }
    }
    // ReSharper restore InconsistentNaming
}
