﻿using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_gender_to_a_boolean
    {
        private static GenderToBooleanConverter _converter;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _converter = new GenderToBooleanConverter();
        }

        [TestMethod]
        public void And_the_value_is_male_then_true_is_returned()
        {
            Assert.IsTrue((bool) _converter.Convert(GenderType.Male, null, null, null));
        }

        [TestMethod]
        public void And_the_value_is_male_and_the_value_should_be_inverted_then_false_is_returned()
        {
            Assert.IsFalse((bool)_converter.Convert(GenderType.Male, null, GenderToBooleanConverter.InvertGender, null));
        }

        [TestMethod]
        public void And_the_value_is_female_then_false_is_returned()
        {
            Assert.IsFalse((bool)_converter.Convert(GenderType.Female, null, null, null));
        }

        [TestMethod]
        public void And_the_value_is_female_and_the_value_should_be_inverted_then_true_is_returned()
        {
            Assert.IsTrue((bool)_converter.Convert(GenderType.Female, null, GenderToBooleanConverter.InvertGender, null));
        }

        [TestMethod]
        public void And_the_value_is_unknown_gender_then_false_is_returned()
        {
            Assert.IsFalse((bool)_converter.Convert(GenderType.Unknown, null, null, null));
        }

        [TestMethod]
        public void And_the_value_is_unknown_and_the_value_should_be_inverted_then_false_is_returned()
        {
            Assert.IsFalse((bool)_converter.Convert(GenderType.Unknown, null, GenderToBooleanConverter.InvertGender, null));
        }

        [TestMethod]
        public void And_the_parameter_is_something_unexpected_then_the_parameter_is_ignored()
        {
            Assert.IsTrue((bool)_converter.Convert(GenderType.Male, null, GenderType.Unknown, null));
        }

        [TestMethod]
        public void And_the_value_is_true_then_the_gender_is_male()
        {
            Assert.AreEqual(GenderType.Male, _converter.ConvertBack(true, null, null, null));
        }

        [TestMethod]
        public void And_the_value_is_true_and_the_parameter_should_be_inverted_then_the_gender_is_female()
        {
            Assert.AreEqual(GenderType.Female, _converter.ConvertBack(true, null, GenderToBooleanConverter.InvertGender, null));
        }

        [TestMethod]
        public void And_the_value_is_false_then_the_gender_is_female()
        {
            Assert.AreEqual(GenderType.Female, _converter.ConvertBack(false, null, null, null));
        }

        [TestMethod]
        public void And_the_value_is_false_and_the_parameter_should_be_inverted_then_the_gender_is_male()
        {
            Assert.AreEqual(GenderType.Male, _converter.ConvertBack(false, null, GenderToBooleanConverter.InvertGender, null));
        }

        [TestMethod]
        public void And_converting_back_and_the_parameter_is_something_unexpected_then_the_parameter_is_ignored()
        {
            Assert.AreEqual(GenderType.Male, _converter.ConvertBack(true, null, GenderType.Unknown, null));
        }
    }
    // ReSharper restore InconsistentNaming
}
