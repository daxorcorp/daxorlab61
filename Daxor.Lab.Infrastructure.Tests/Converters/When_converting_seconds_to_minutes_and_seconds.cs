﻿using System;
using Daxor.Lab.Infrastructure.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters
{
    [TestClass]
    // ReSharper disable InconsistentNaming
    public class When_converting_seconds_to_minutes_and_seconds
    {
        private const int Seconds = 803;
        private const string SecondsAsMinutesAndSeconds = "13:23";
        
        private static SecondsToMinutesAndSecondsConverter _converter;

        #region Convert()

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _converter = new SecondsToMinutesAndSecondsConverter();
        }

        [TestMethod]
        public void And_the_value_to_convert_is_null_then_the_empty_string_is_returned()
        {
            var observedConversion = _converter.Convert(null, null, null, null);
            Assert.AreEqual(String.Empty, observedConversion);
        }

        [TestMethod]
        public void And_the_value_is_not_a_valid_number_then_zero_minutes_zero_seconds_is_returned()
        {
            var observedConversion = _converter.Convert("something funky", null, null, null);
            Assert.AreEqual("0:00", observedConversion);
        }
        
        [TestMethod]
        public void And_the_value_is_undefined_seconds_then_the_empty_string_is_returned()
        {
            var observedConversion = _converter.Convert(SecondsToMinutesAndSecondsConverter.UndefinedSeconds, null, null, null);
            Assert.AreEqual(String.Empty, observedConversion);
        }

        [TestMethod]
        public void And_the_seconds_value_is_valid_then_convert_returns_the_string_representing_minutes_and_seconds()
        {
            var observedConversion = _converter.Convert(Seconds, null, null, null);
            Assert.AreEqual(SecondsAsMinutesAndSeconds, observedConversion);
        }

        #endregion

        #region ConvertBack()

        [TestMethod]
        public void And_the_minutes_with_seconds_value_is_valid_then_the_integer_representing_seconds_is_returned()
        {
            var observedConversion = _converter.ConvertBack(SecondsAsMinutesAndSeconds, null, null, null);
            Assert.AreEqual(Seconds, observedConversion);
        }

        [TestMethod]
        public void And_the_value_to_convert_back_is_null_then_the_empty_string_is_returned()
        {
            var observedConversion = _converter.ConvertBack(null, null, null, null);
            Assert.AreEqual(String.Empty, observedConversion);
        }

        [TestMethod]
        public void And_the_value_to_convert_is_just_the_separator_then_zero_is_returned()
        {
            var separatorOnlyString = SecondsToMinutesAndSecondsConverter.TimeSeparatorChar;
            var observedConversion = _converter.ConvertBack(separatorOnlyString, null, null, null);
            Assert.AreEqual(0, observedConversion);
        }

        [TestMethod]
        public void And_the_value_to_convert_does_not_contain_seconds_then_the_integer_representing_seconds_is_returned()
        {
            var threeMinutesZeroSeconds = "3" + SecondsToMinutesAndSecondsConverter.TimeSeparatorChar;
            const int seconds = 180;

            var observedConversion = _converter.ConvertBack(threeMinutesZeroSeconds, null, null, null);
            Assert.AreEqual(seconds, observedConversion);
        }

        [TestMethod]
        public void And_the_value_to_convert_does_not_contain_minutes_then_the_integer_representing_seconds_is_returned()
        {
            var threeMinutesZeroSeconds = SecondsToMinutesAndSecondsConverter.TimeSeparatorChar + "3";
            const int seconds = 3;

            var observedConversion = _converter.ConvertBack(threeMinutesZeroSeconds, null, null, null);
            Assert.AreEqual(seconds, observedConversion);
        }

        [TestMethod]
        public void And_the_value_to_convert_has_two_time_separators_then_undefined_seconds_is_returned()
        {
            var threeMinutesZeroSeconds = SecondsToMinutesAndSecondsConverter.TimeSeparatorChar +
                                          SecondsToMinutesAndSecondsConverter.TimeSeparatorChar;
            
            var observedConversion = _converter.ConvertBack(threeMinutesZeroSeconds, null, null, null);
            Assert.AreEqual(SecondsToMinutesAndSecondsConverter.UndefinedSeconds, observedConversion);
        }

        #endregion
    }
    // ReSharper restore InconsistentNaming
}