﻿using System;
using System.Windows;
using Daxor.Lab.Infrastructure.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_boolean_to_inverted_visibility
    {
        private static BooleanToInvertedVisibilityConverter _converter;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _converter = new BooleanToInvertedVisibilityConverter();
        }

        [TestMethod]
        public void And_the_value_to_convert_is_true_then_visibility_is_collapsed()
        {
            var actualVisibility = _converter.Convert(true, null, null, null);

            Assert.AreEqual(Visibility.Collapsed, actualVisibility);
        }

        [TestMethod]
        public void And_the_value_to_convert_is_false_then_visibility_is_visible()
        {
            var actualVisibility = _converter.Convert(false, null, null, null);

            Assert.AreEqual(Visibility.Visible, actualVisibility);
        }

        [TestMethod]
        public void And_the_value_to_convert_is_null_then_visibility_is_collapsed()
        {
            var actualVisibility = _converter.Convert(null, null, null, null);

            Assert.AreEqual(Visibility.Collapsed, actualVisibility);
        }

        [TestMethod]
        public void And_the_value_to_convert_is_not_a_boolean_then_visibility_is_collapsed()
        {
            var actualVisibility = _converter.Convert(DateTime.Now, null, null, null);

            Assert.AreEqual(Visibility.Collapsed, actualVisibility);
        }
    }
    // ReSharper restore InconsistentNaming
}
