﻿using Daxor.Lab.Infrastructure.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_boolean_to_an_inverted_boolean
    {
        private static BooleanToInvertedBooleanConverter _converter;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _converter = new BooleanToInvertedBooleanConverter();
        }

        [TestMethod]
        public void And_the_value_is_true_then_false_is_returned()
        {
            Assert.IsFalse((bool) _converter.Convert(true, null, null, null));
        }

        [TestMethod]
        public void And_the_value_is_false_then_true_is_returned()
        {
            Assert.IsTrue((bool)_converter.Convert(false, null, null, null));
        }

        [TestMethod]
        public void And_converting_back_and_the_value_is_true_then_false_is_returned()
        {
            Assert.IsFalse((bool)_converter.ConvertBack(true, null, null, null));
        }

        [TestMethod]
        public void And_converting_back_and_the_value_is_false_then_true_is_returned()
        {
            Assert.IsTrue((bool)_converter.ConvertBack(false, null, null, null));
        }
    }
    // ReSharper restore InconsistentNaming
}
