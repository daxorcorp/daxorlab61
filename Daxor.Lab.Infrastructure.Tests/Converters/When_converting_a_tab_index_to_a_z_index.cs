﻿using System.Collections.Generic;
using System.Windows.Controls;
using Daxor.Lab.Infrastructure.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters
{
    [TestClass]
    // ReSharper disable InconsistentNaming
    public class When_converting_a_tab_index_to_a_z_index
    {
        private const int TabCount = 3;
        private static TabIndexToZIndexConverter _converter;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _converter = new TabIndexToZIndexConverter();
        }

        [TestMethod]
        public void And_the_first_of_three_tabs_is_selected_then_the_z_indices_are_correct()
        {
            // ReSharper disable PossibleNullReferenceException
            var values = CreateConverterValuesWithASpecificTabSelected(0);
            var tabItems = (values[1] as TabControl).Items;
            // ReSharper restore PossibleNullReferenceException

            var zIndices = new List<int>();
            foreach (var tabItem in tabItems)
            {
                values[0] = tabItem;
                zIndices.Add((int) _converter.Convert(values, null, null, null));
            }

            CollectionAssert.AreEqual(new List<int> { 3, 1, 0 }, zIndices);
        }

        [TestMethod]
        public void And_the_second_of_three_tabs_is_selected_then_the_z_indices_are_correct()
        {
            // ReSharper disable PossibleNullReferenceException
            var values = CreateConverterValuesWithASpecificTabSelected(1);
            var tabItems = (values[1] as TabControl).Items;
            // ReSharper restore PossibleNullReferenceException

            var zIndices = new List<int>();
            foreach (var tabItem in tabItems)
            {
                values[0] = tabItem;
                zIndices.Add((int)_converter.Convert(values, null, null, null));
            }

            CollectionAssert.AreEqual(new List<int> { 2, 3, 0 }, zIndices);
        }

        [TestMethod]
        public void And_the_third_of_three_tabs_is_selected_then_the_z_indices_are_correct()
        {
            // ReSharper disable PossibleNullReferenceException
            var values = CreateConverterValuesWithASpecificTabSelected(2);
            var tabItems = (values[1] as TabControl).Items;
            // ReSharper restore PossibleNullReferenceException

            var zIndices = new List<int>();
            foreach (var tabItem in tabItems)
            {
                values[0] = tabItem;
                zIndices.Add((int)_converter.Convert(values, null, null, null));
            }

            CollectionAssert.AreEqual(new List<int> { 2, 1, 3 }, zIndices);
        }

        private object[] CreateConverterValuesWithASpecificTabSelected(int selectedIndex)
        {
            var tabItems = new List<TabItem>();
            for (var i = 0; i < TabCount; i++)
                tabItems.Add(new TabItem { IsSelected = selectedIndex == i });
            var tabControl = new TabControl {ItemsSource = tabItems};
            return new object[] { null, tabControl, TabCount };
        }
    }
    // ReSharper restore InconsistentNaming
}
