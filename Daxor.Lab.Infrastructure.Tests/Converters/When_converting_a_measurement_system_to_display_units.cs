﻿using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Converters
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_measurement_system_to_display_units
    {
        private static MeasurementSystemToUnitsDisplayConverter _converter;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _converter = new MeasurementSystemToUnitsDisplayConverter();
        }

        [TestMethod]
        public void And_the_value_is_null_then_convert_returns_null()
        {
            Assert.AreEqual(null, _converter.Convert(null, null, null, null));
        }

        [TestMethod]
        public void And_the_value_is_not_a_valid_enum_type_then_the_string_equivalent_of_the_value_is_returned()
        {
            Assert.AreEqual("cm", _converter.Convert(UnitOfMeasurementConstants.MetricUnitOfLength, null, null, null));
        }

        [TestMethod]
        public void And_the_parameter_is_null_then_the_string_equivalent_of_the_value_is_returned()
        {
            Assert.AreEqual("English", _converter.Convert(MeasurementSystem.English, null, null, null));
        }

        [TestMethod]
        public void And_the_value_is_english_and_the_parameter_is_height_then_inches_is_returned()
        {
            Assert.AreEqual(UnitOfMeasurementConstants.USUnitOfLength, 
                _converter.Convert(MeasurementSystem.English, null, MeasurementSystemToUnitsDisplayConverter.HeightParameterName, null));
        }

        [TestMethod]
        public void And_the_value_is_english_and_the_parameter_is_weight_then_pounds_is_returned()
        {
            Assert.AreEqual(UnitOfMeasurementConstants.USUnitOfMass,
                _converter.Convert(MeasurementSystem.English, null, MeasurementSystemToUnitsDisplayConverter.WeightParameterName, null));
        }

        [TestMethod]
        public void And_the_value_is_metric_and_the_parameter_is_height_then_centimeters_is_returned()
        {
            Assert.AreEqual(UnitOfMeasurementConstants.MetricUnitOfLength,
                _converter.Convert(MeasurementSystem.Metric, null, MeasurementSystemToUnitsDisplayConverter.HeightParameterName, null));
        }

        [TestMethod]
        public void And_the_value_is_metric_and_the_parameter_is_weight_then_kilograms_is_returned()
        {
            Assert.AreEqual(UnitOfMeasurementConstants.MetricUnitOfMass,
                _converter.Convert(MeasurementSystem.Metric, null, MeasurementSystemToUnitsDisplayConverter.WeightParameterName, null));
        }

        [TestMethod]
        public void And_the_value_is_null_then_english_is_returned()
        {
            Assert.AreEqual(MeasurementSystem.English, _converter.ConvertBack(null, null, null, null));
        }

        [TestMethod]
        public void And_the_value_is_not_a_valid_display_unit_then_english_is_returned()
        {
            Assert.AreEqual(MeasurementSystem.English, _converter.ConvertBack("whatever", null, null, null));
        }

        [TestMethod]
        public void And_the_value_is_centimeters_then_metric_is_returned()
        {
            Assert.AreEqual(MeasurementSystem.Metric, _converter.ConvertBack(UnitOfMeasurementConstants.MetricUnitOfLength, null, null, null));
        }

        [TestMethod]
        public void And_the_value_is_kilograms_then_metric_is_returned()
        {
            Assert.AreEqual(MeasurementSystem.Metric, _converter.ConvertBack(UnitOfMeasurementConstants.MetricUnitOfMass, null, null, null));
        }

        [TestMethod]
        public void And_the_value_is_inches_then_english_is_returned()
        {
            Assert.AreEqual(MeasurementSystem.English, _converter.ConvertBack(UnitOfMeasurementConstants.USUnitOfLength, null, null, null));
        }

        [TestMethod]
        public void And_the_value_is_pounds_then_english_is_returned()
        {
            Assert.AreEqual(MeasurementSystem.English, _converter.ConvertBack(UnitOfMeasurementConstants.USUnitOfMass, null, null, null));
        }
        // ReSharper restore InconsistentNaming
    }
}
