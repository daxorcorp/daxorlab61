﻿using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Infrastructure.Tests.Utilities.SafeFileOperations_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_IFileManager_instance
    {
        [TestMethod]
        public void And_the_instance_is_null_then_SystemFileManager_becomes_the_IFileManager_instance()
        {
            SafeFileOperations.FileManager = null;

            // ReSharper disable PossibleNullReferenceException
            Assert.AreEqual(SafeFileOperations.FileManager.GetType(), typeof(SystemFileManager));
            // ReSharper restore PossibleNullReferenceException
        }

        [TestMethod]
        public void And_the_instance_is_not_null_then_the_given_instance_becomes_the_IFileManager_instance()
        {
            var dummyFileManager = MockRepository.GenerateStub<IFileManager>();
            
            SafeFileOperations.FileManager = dummyFileManager;
            // ReSharper disable PossibleNullReferenceException
            Assert.AreNotEqual(SafeFileOperations.FileManager.GetType(), typeof(SystemFileManager));
            // ReSharper restore PossibleNullReferenceException
        }
    }
    // ReSharper restore InconsistentNaming
}
