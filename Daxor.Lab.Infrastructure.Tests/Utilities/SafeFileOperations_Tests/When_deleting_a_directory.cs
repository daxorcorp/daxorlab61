﻿using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Infrastructure.Tests.Utilities.SafeFileOperations_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_deleting_a_directory
    {
        private const string DirectoryToDelete = @"C:\directory\";

        [TestMethod]
        public void And_the_directory_is_empty_then_the_directory_is_deleted()
        {
            // Arrange
            var mockFileManager = MockRepository.GenerateMock<IFileManager>();
            mockFileManager.Stub(x => x.DirectoryExists(DirectoryToDelete)).Return(true);
            mockFileManager.Expect(x => x.DeleteDirectory(DirectoryToDelete)).Repeat.Once();
            SafeFileOperations.FileManager = mockFileManager;

            // Act
            SafeFileOperations.DeleteDirectory(DirectoryToDelete);

            // Assert
            mockFileManager.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_directory_does_not_exist_then_nothing_happens()
        {
            // Arrange
            var mockFileManager = MockRepository.GenerateMock<IFileManager>();
            mockFileManager.Stub(x => x.DirectoryExists(DirectoryToDelete)).Return(false);
            mockFileManager.Expect(x => x.DeleteDirectory(DirectoryToDelete)).Repeat.Never();
            SafeFileOperations.FileManager = mockFileManager;

            // Act
            SafeFileOperations.DeleteDirectory(DirectoryToDelete);

            // Assert
            mockFileManager.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
