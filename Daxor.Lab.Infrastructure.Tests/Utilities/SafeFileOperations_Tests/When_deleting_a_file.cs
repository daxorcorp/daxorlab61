﻿using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Infrastructure.Tests.Utilities.SafeFileOperations_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_deleting_a_file
    {
        [TestMethod]
        public void And_the_file_exists_then_the_files_readonly_property_is_cleared_and_the_file_is_deleted()
        {
            const string fileName = "file.txt";

            // Arrange
            var mockRepository = new MockRepository();
            var mockFileManager = mockRepository.DynamicMock<IFileManager>();
            mockFileManager.Stub(x => x.FileExists(fileName)).Return(true);
            using (mockRepository.Ordered())
            {
                mockFileManager.Expect(m => m.RemoveReadOnlyProperty(fileName));
                mockFileManager.Expect(m => m.DeleteFile(fileName));
            }
            mockRepository.ReplayAll();

            SafeFileOperations.FileManager = mockFileManager;

            // Act
            SafeFileOperations.Delete(fileName);

            // Assert
            mockFileManager.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
