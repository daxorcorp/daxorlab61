﻿using System;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Infrastructure.Tests.Utilities.SafeFileOperations_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_deleting_files_using_a_wildcard
    {
        [TestMethod]
        public void And_the_wildcard_is_null_then_an_exception_is_thrown()
        {
            const string directory = "c:\\";

            var stubFileManager = MockRepository.GenerateStub<IFileManager>();
            stubFileManager.Stub(m => m.DirectoryExists(directory)).Return(true);
            stubFileManager.Stub(m => m.GetFilesInDirectory(directory, null)).Throw(new ArgumentNullException());
            SafeFileOperations.FileManager = stubFileManager;

            AssertEx.ThrowsArgumentNullException(()=>SafeFileOperations.Delete(directory, null), "wildCardString");
        }
    }
    // ReSharper restore InconsistentNaming
}
