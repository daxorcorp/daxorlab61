﻿using System.IO;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Infrastructure.Tests.Utilities.SafeFileOperations_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_deleting_a_file
    {
        [TestMethod]
        public void And_the_file_does_not_exist_then_an_exception_is_thrown()
        {
            var stubFileManager = MockRepository.GenerateStub<IFileManager>();
            stubFileManager.Expect(m => m.RemoveReadOnlyProperty(null))
                           .IgnoreArguments()
                           .Throw(new FileNotFoundException());
            SafeFileOperations.FileManager = stubFileManager;

            AssertEx.Throws<FileNotFoundException>(()=>SafeFileOperations.Delete("whatever"));
        }
    }
}
