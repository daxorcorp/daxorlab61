﻿using System.Collections.Generic;
using System.IO;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Infrastructure.Tests.Utilities.SafeFileOperations_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_deleting_files_using_a_wildcard
    {
        private const string Directory = @"C:\temp\";
        private const string WildcardString = "*.tmp";

        [TestMethod]
        public void And_the_directory_exists_and_files_match_the_wildcard_then_those_files_are_deleted()
        {
            // Arrange
            var mockFileManager = MockRepository.GenerateStrictMock<IFileManager>();
            var fileInfo = new List<FileInfo>
                {
                    new FileInfo(Directory + "stuff.tmp"),
                    new FileInfo(Directory + "other.tmp")
                };
            mockFileManager.Stub(x => x.DirectoryExists(Directory)).Return(true);
            mockFileManager.Stub(x => x.GetFilesInDirectory(Directory, WildcardString)).Return(fileInfo);
            mockFileManager.Expect(x => x.DeleteFile(Directory + "stuff.tmp")).Repeat.Once();
            mockFileManager.Expect(x => x.DeleteFile(Directory + "other.tmp")).Repeat.Once();
            SafeFileOperations.FileManager = mockFileManager;

            // Act
            SafeFileOperations.Delete(Directory, WildcardString);

            // Assert
            mockFileManager.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_directory_does_not_exist_then_no_files_are_deleted()
        {
            // Arrange
            var mockFileManager = MockRepository.GenerateMock<IFileManager>();
            mockFileManager.Stub(x => x.DirectoryExists(Directory)).Return(false);
            mockFileManager.Expect(x => x.DeleteFile(null)).IgnoreArguments().Repeat.Never();
            SafeFileOperations.FileManager = mockFileManager;

            // Act
            SafeFileOperations.Delete(Directory, WildcardString);

            // Assert
            mockFileManager.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_no_files_match_the_wildcard_then_no_files_are_deleted()
        {
            // Arrange
            var mockFileManager = MockRepository.GenerateMock<IFileManager>();
            mockFileManager.Stub(x => x.DirectoryExists(Directory)).Return(true);
            mockFileManager.Stub(x => x.GetFilesInDirectory(Directory, WildcardString)).Return(new List<FileInfo>());
            mockFileManager.Expect(x => x.DeleteFile(null)).IgnoreArguments().Repeat.Never();
            SafeFileOperations.FileManager = mockFileManager;

            // Act
            SafeFileOperations.Delete(Directory, WildcardString);

            // Assert
            mockFileManager.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
