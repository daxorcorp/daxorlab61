﻿using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Infrastructure.Tests.Utilities.SafeFileOperations_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_copying_a_file
    {
        private const string SourceFilename = "from.txt";
        private const string DestinationFilename = "to.txt";

        [TestMethod]
        public void And_the_source_file_does_not_exist_and_the_destination_file_does_not_exist_then_nothing_happens()
        {
            // Arrange
            var mockFileManager = MockRepository.GenerateMock<IFileManager>();
            mockFileManager.Stub(x => x.FileExists(SourceFilename)).Return(false);
            mockFileManager.Stub(x => x.FileExists(DestinationFilename)).Return(false);
            mockFileManager.Expect(x => x.CopyFile(SourceFilename, DestinationFilename)).Repeat.Never();

            SafeFileOperations.FileManager = mockFileManager;

            // Act
            SafeFileOperations.Copy(SourceFilename, DestinationFilename);

            // Assert
            mockFileManager.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_source_file_does_not_exist_and_the_destination_file_does_exists_then_nothing_happens()
        {
            // Arrange
            var mockFileManager = MockRepository.GenerateMock<IFileManager>();
            mockFileManager.Stub(x => x.FileExists(SourceFilename)).Return(false);
            mockFileManager.Stub(x => x.FileExists(DestinationFilename)).Return(true);
            mockFileManager.Expect(x => x.CopyFile(SourceFilename, DestinationFilename)).Repeat.Never();

            SafeFileOperations.FileManager = mockFileManager;

            // Act
            SafeFileOperations.Copy(SourceFilename, DestinationFilename);

            // Assert
            mockFileManager.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_source_file_exists_and_the_destination_file_does_not_then_the_file_is_copied()
        {
            // Arrange
            var mockFileManager = MockRepository.GenerateMock<IFileManager>();
            mockFileManager.Stub(x => x.FileExists(SourceFilename)).Return(true);
            mockFileManager.Stub(x => x.FileExists(DestinationFilename)).Return(false);
            mockFileManager.Expect(x => x.CopyFile(SourceFilename, DestinationFilename)).Repeat.Once();

            SafeFileOperations.FileManager = mockFileManager;

            // Act
            SafeFileOperations.Copy(SourceFilename, DestinationFilename);

            // Assert
            mockFileManager.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_source_file_exists_and_the_destination_file_does_exist_then_the_destination_file_is_made_nonreadonly_and_is_copied()
        {
            // Arrange
            var mockFileManager = MockRepository.GenerateMock<IFileManager>();
            mockFileManager.Stub(x => x.FileExists(SourceFilename)).Return(true);
            mockFileManager.Stub(x => x.FileExists(DestinationFilename)).Return(true);
            mockFileManager.Expect(x => x.RemoveReadOnlyProperty(DestinationFilename)).Repeat.Once();
            mockFileManager.Expect(x => x.CopyFile(SourceFilename, DestinationFilename)).Repeat.Once();

            SafeFileOperations.FileManager = mockFileManager;

            // Act
            SafeFileOperations.Copy(SourceFilename, DestinationFilename);

            // Assert
            mockFileManager.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}