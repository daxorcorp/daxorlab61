﻿using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Utilities.IonicZipWrapper_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_with_a_password
    {
        [TestMethod]
        public void And_the_password_isnt_null_or_whitespace_then_the_password_is_set()
        {
            var wrapper = new IonicZipWrapper("password");
            Assert.AreEqual("password", wrapper.Password);
        }
    }
    // ReSharper restore InconsistentNaming
}
