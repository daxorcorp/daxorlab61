﻿using Daxor.Lab.Infrastructure.Utilities;

namespace Daxor.Lab.Infrastructure.Tests.Utilities.IonicZipWrapper_Tests
{
    public class StubWrapperThatDoesntActuallySaveToDisk : IonicZipWrapper
    {
        public override void Save(string filename)
        {
            HasZipFileBeenSaved = true;
        }
    }
}
