﻿using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Utilities.IonicZipWrapper_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_adding_files
    {
        const string FileName = "somefile.txt";
        const string FilePath = @"C:\temp\" + FileName;
        const string Directory = "Backups";
        IonicZipWrapper _wrapper;
        
        [TestInitialize]
        public void TestInitialize() 
        {
            _wrapper = new IonicZipWrapper();
        }

        [TestMethod]
        public void And_a_specific_directory_is_not_specified_then_the_files_are_in_the_root_directory()
        {
            _wrapper.AddFileToRoot(FilePath);
            var result = _wrapper.Files.Find(x => x == FileName);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void And_a_specific_directory_is_specified_then_the_files_are_saved_in_that_directory()
        {
            _wrapper.AddFileToDirectory(FilePath, Directory);
            var result = _wrapper.Files.Find(x => x == Directory + "/" + FileName);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void And_files_have_been_added_then_the_added_files_are_in_the_collection_of_added_files()
        {
            var expectedFileList = new List<string> { FileName };
            _wrapper.AddFileToRoot(FilePath);
            var actualFileList = _wrapper.Files;
            
            CollectionAssert.AreEqual(expectedFileList, actualFileList);
        }

        
    }
    // ReSharper restore InconsistentNaming
}
