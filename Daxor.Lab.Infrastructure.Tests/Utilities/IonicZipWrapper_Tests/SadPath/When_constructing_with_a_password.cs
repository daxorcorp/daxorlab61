﻿using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Daxor.Lab.Infrastructure.Tests.Utilities.IonicZipWrapper_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_with_a_password
    {
        [TestMethod]
        public void And_the_password_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(() => { var wrapper = new IonicZipWrapper(null); }, "password");
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_the_password_is_whitespace_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.Throws<ArgumentException>(() => { var wrapper = new IonicZipWrapper("    "); });
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_the_password_is_empty_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.Throws<ArgumentException>(() => { var wrapper = new IonicZipWrapper(String.Empty); });
            // ReSharper restore UnusedVariable
        }
    }
    // ReSharper restore InconsistentNaming

}
