﻿using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Daxor.Lab.Infrastructure.Tests.Utilities.IonicZipWrapper_Tests.SadPath
{
    // ReSharper disable InconsistentNaming   
    [TestClass]
    public class When_adding_files
    {
        private IonicZipWrapper _wrapper;
        private const string FileName = "somefile.txt";
        const string FilePath = @"C:\temp\" + FileName;
        const string Directory = "Backups";

        [TestInitialize]
        public void TestInitialize()
        {
            _wrapper = new IonicZipWrapper();
        }

        [TestMethod]
        public void And_the_file_path_to_add_to_the_root_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_wrapper.AddFileToRoot(null), "filename");
        }

        [TestMethod]
        public void And_the_directory_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_wrapper.AddFileToDirectory(FileName, null), "directory");
        }

        [TestMethod]
        public void And_the_directory_is_whitespace_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentException>(()=>_wrapper.AddFileToDirectory(FileName, "    "));
        }

        [TestMethod]
        public void And_the_file_path_to_add_to_the_root_consists_of_whitespace_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentException>(()=>_wrapper.AddFileToRoot("     "));
        }

        [TestMethod]
        public void And_the_file_path_is_the_empty_string_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentException>(()=>_wrapper.AddFileToRoot(""));
        }

        [TestMethod]
        public void And_the_file_has_already_been_saved_then_no_new_files_can_be_added_to_the_root()
        {
            var stubZipWrapper = new StubWrapperThatDoesntActuallySaveToDisk();
            stubZipWrapper.Save("whatever");

            AssertEx.Throws<ObjectDisposedException>(()=>stubZipWrapper.AddFileToRoot(FilePath));
        }

        [TestMethod]
        public void And_the_file_has_already_been_saved_then_no_new_files_can_be_added()
        {
            var stubZipWrapper = new StubWrapperThatDoesntActuallySaveToDisk();
            stubZipWrapper.Save("whatever");

            AssertEx.Throws<ObjectDisposedException>(()=>stubZipWrapper.AddFileToDirectory(FileName, Directory));
        }

        [TestMethod]
        public void And_the_file_has_already_been_saved_then_the_previous_file_list_cannot_be_accessed()
        {
            var stubZipWrapper = new StubWrapperThatDoesntActuallySaveToDisk();
            stubZipWrapper.Save("whatever");

            // ReSharper disable once UnusedVariable
            AssertEx.Throws<ObjectDisposedException>(() => { var throwAway = stubZipWrapper.Files; });
        }
    }
    // ReSharper restore InconsistentNaming
}
