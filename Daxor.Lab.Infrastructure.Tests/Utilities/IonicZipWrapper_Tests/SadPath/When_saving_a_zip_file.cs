﻿using System;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Utilities.IonicZipWrapper_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_saving_a_zip_file
    {
        #region Constants

        private const string FileName = "somefile.txt";
        private const string Directory = @"C:\temp\";
        private const string ZipName = "content.zip";

        #endregion

        #region Fields

        private readonly IonicZipWrapper _wrapper = new IonicZipWrapper();

        #endregion

        [TestMethod]
        public void And_the_file_name_to_add_to_the_root_is_null_then_an_exception_is_thrown()
        {
            _wrapper.AddFileToRoot(FileName);
            AssertEx.ThrowsArgumentNullException(()=>_wrapper.Save(null), "filename");
        }

        [TestMethod]
        public void And_no_files_have_been_added_then_an_exception_is_thrown()
        {
            AssertEx.Throws<InvalidOperationException>(()=>_wrapper.Save(Directory + ZipName));
        }

        [TestMethod]
        public void And_the_file_name_to_add_to_the_root_consists_of_whitespace_then_an_exception_is_thrown()
        {
            _wrapper.AddFileToRoot(FileName);
            AssertEx.Throws<ArgumentException>(()=>_wrapper.Save("    "));
        }

        [TestMethod]
        public void And_the_file_name_to_add_to_the_root_is_empty_then_an_exception_is_thrown()
        {
            _wrapper.AddFileToRoot(FileName);
            AssertEx.Throws<ArgumentException>(()=>_wrapper.Save(String.Empty));
        }
    }
    // ReSharper restore InconsistentNaming

}
