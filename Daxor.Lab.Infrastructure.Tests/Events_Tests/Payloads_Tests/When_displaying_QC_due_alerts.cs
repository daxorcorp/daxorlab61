using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.Tests.Events_Tests.Payloads_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_displaying_QC_due_alerts
    {
        private const string AlertSuffix = " QC Due";

        [TestMethod]
        [RequirementValue("Full" + AlertSuffix)]
        public void And_daily_QC_is_due_then_the_alert_text_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), AlertResolutionPayload.FullQCAlert);
        }

        [TestMethod]
        [RequirementValue("Contamination" + AlertSuffix)]
        public void And_contamination_QC_is_due_then_the_alert_text_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), AlertResolutionPayload.ContaminationQCAlert);
        }

        [TestMethod]
        [RequirementValue("Standards" + AlertSuffix)]
        public void And_standards_QC_is_due_then_the_alert_text_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), AlertResolutionPayload.StandardsQCAlert);
        }

        [TestMethod]
        [RequirementValue("Linearity" + AlertSuffix)]
        public void And_linearity_QC_is_due_then_the_alert_text_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), AlertResolutionPayload.LinearityQCAlert);
        }
    }
    // ReSharper restore InconsistentNaming
}
