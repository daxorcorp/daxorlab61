﻿using System;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.Footer.Tests.Module_Tests
{
	internal class FooterModuleThatDoesNotAddResourceDictionaries : Module
	{
		public FooterModuleThatDoesNotAddResourceDictionaries(IRegionManager regionManager, IUnityContainer container) : base(regionManager, container)
		{
			ThrowExceptionOnAddToMergedDictionaries = false;
		}

		public bool ThrowExceptionOnAddToMergedDictionaries { get; set; }

		protected override void AddResourceDictionaryToMergedDictionaries()
		{
			if (ThrowExceptionOnAddToMergedDictionaries)
				throw new Exception();
		}
	}
}
