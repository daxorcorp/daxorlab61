﻿using System;
using Daxor.Lab.Footer.Views;
using Daxor.Lab.Infrastructure.Exceptions;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Footer.Tests.Module_Tests.SadPath
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_the_Footer_module
	{
		private FooterModuleThatDoesNotAddResourceDictionaries _module;

		private IRegionManager _regionManager;
		private IUnityContainer _container;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_container = Substitute.For<IUnityContainer>();
			_regionManager = Substitute.For<IRegionManager>();
		}

		[TestMethod]
		public void And_an_error_occurs_while_adding_the_resource_dictionary_then_the_correct_module_load_exception_is_thrown()
		{
			_module = new FooterModuleThatDoesNotAddResourceDictionaries(_regionManager, _container)
			{
				ThrowExceptionOnAddToMergedDictionaries = true
			};

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Footer", ex.ModuleName);
				Assert.AreEqual("add the resource dictionary", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_resolving_the_footer_view_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<FooterView>()).Throw(new Exception());
			_module = new FooterModuleThatDoesNotAddResourceDictionaries(_regionManager, _container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Footer", ex.ModuleName);
				Assert.AreEqual("initialize the Footer view", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}
	}
	// ReSharper restore InconsistentNaming
}