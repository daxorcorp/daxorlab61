﻿using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.DatabaseServices.Services
{
    public class DaxorRestoreService : RestoreService
    {
        public DaxorRestoreService(ISettingsManager settingsManager, string databaseName) : base(settingsManager, databaseName)
        {
        }

        public override bool AreBackupsFromTheSameVersion(string backupVersion, string runningVersion)
        {
            return backupVersion == runningVersion;
        }
    }
}
