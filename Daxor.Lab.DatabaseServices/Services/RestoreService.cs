﻿using Daxor.Lab.DatabaseServices.Common;
using Daxor.Lab.DatabaseServices.Factories;
using Daxor.Lab.DatabaseServices.Factories.DatabaseFactories;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System;

namespace Daxor.Lab.DatabaseServices.Services
{
    public class RestoreService : IRestoreService
    {
        #region Fields

        private readonly ISettingsManager _settingsManager;
        private readonly string _databaseName;

        #endregion

        #region Ctor

        public RestoreService(ISettingsManager settingsManager, string databaseName)
        {
            if (settingsManager == null)
                throw new ArgumentNullException("settingsManager");
            if (databaseName == null)
                throw new ArgumentNullException("databaseName");
            if (string.IsNullOrWhiteSpace(databaseName))
                throw new InvalidArgumentException("databaseName must not consist of whitespace");

            _settingsManager = settingsManager;
            _databaseName = databaseName;
        }

        #endregion

        #region Properties

        public ServerMessageEventHandler RestoreCompletedHandler { get; set; }
        public PercentCompleteEventHandler PercentCompleteHandler { get; set; }

        #endregion

        public void Restore(string fileName)
        {
            if (fileName == null)
                throw new ArgumentNullException("fileName");
            if (String.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("fileName must not consist of whitespace");

            var backupVersion = VersionExtractor.GetBackupVersion(fileName);
            var runningVersion = _settingsManager.GetSetting<string>(SettingKeys.BvaModuleVersion);

            if (!AreBackupsFromTheSameVersion(backupVersion, runningVersion))
                throw new DatabaseVersionMismatchException(backupVersion);

            var server = ServerFactory.ConfigureRestoreServer(_settingsManager);
            var restore = RestoreFactory.CreateRestore(_databaseName, fileName, this);

            try
            {
                TakeDatabaseOffline(server);
                restore.SqlRestore(server);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Could not restore the database: " + ex.Message, ex);
            }
        }

        public virtual bool AreBackupsFromTheSameVersion(string backupVersion, string runningVersion)
        {
            return true;
        }

        private void TakeDatabaseOffline(Server server)
        {
            var database = server.Databases[_databaseName];
            if (database == null) return;

            server.KillAllProcesses(_databaseName);
            database.SetOffline();
        }
    }
}
