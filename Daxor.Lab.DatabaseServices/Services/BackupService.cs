﻿using System;
using System.IO;
using Daxor.Lab.DatabaseServices.Common;
using Daxor.Lab.DatabaseServices.Factories.DatabaseFactories;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace Daxor.Lab.DatabaseServices.Services
{
    public class BackupService : IBackupService
    {
        #region Fields

        private readonly ISettingsManager _settingsManager;
        private readonly string _databaseName;

        #endregion

        #region Ctor

        public BackupService(ISettingsManager settingsManager, string databaseName)
        {
            if (settingsManager == null)
                throw new ArgumentNullException("settingsManager");
            if (databaseName == null)
                throw new ArgumentNullException("databaseName");
            if (string.IsNullOrWhiteSpace(databaseName))
                throw new InvalidArgumentException("databaseName must not consist of whitespace");

            _settingsManager = settingsManager;
            _databaseName = databaseName;
        }

        #endregion

        #region Properties

        public ServerMessageEventHandler BackupCompletedHandler { get; set; }
        public PercentCompleteEventHandler PercentCompleteHandler { get; set; }

        #endregion

        #region Public API

        public void Backup(string filePath)
        {
            if (filePath == null)
                throw new ArgumentNullException("filePath");
            if (String.IsNullOrWhiteSpace(filePath))
                throw new ArgumentException("filePath must be a valid path");

            var server = ServerFactory.ConfigureBackupServer(_settingsManager);

            var backup = BackupFactory.CreateBackup(_databaseName, filePath, this);

            try
            {
                if (SafeFileOperations.FileManager.FileExists(filePath))
                    SafeFileOperations.Delete(filePath);

                backup.SqlBackup(server);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Unable to backup database", ex);
            }

            AddVersionInformation(filePath, _settingsManager.GetSetting<string>(SettingKeys.BvaModuleVersion));
        }

        #endregion

        #region Private Helpers

        private static void AddVersionInformation(string fileName, string version)
        {
            using (var writer = new StreamWriter(fileName, append: true))
            {
                writer.WriteLine(Constants.DatabaseVersionPrefix + version);
                writer.Close();
            }
        }

        #endregion
    }
}
