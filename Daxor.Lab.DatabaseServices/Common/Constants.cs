﻿namespace Daxor.Lab.DatabaseServices.Common
{
    public static class Constants
    {
        public const string DatabaseVersionPrefix = "DaxorLab v";
    }
}
