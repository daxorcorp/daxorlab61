﻿using System;
using System.IO;
using Daxor.Lab.Infrastructure.Utilities;

namespace Daxor.Lab.DatabaseServices.Common
{
    public static class VersionExtractor
    {
        public static string GetBackupVersion(string sourceFile)
        {
            if (!SafeFileOperations.FileManager.FileExists(sourceFile))
                return String.Empty;

            using (var reader = new StreamReader(sourceFile))
            {
                while (reader.EndOfStream == false)
                {
                    var line = reader.ReadLine();
                    if (line == null)
                        continue;

                    if (line.Contains(Constants.DatabaseVersionPrefix))
                        return VersionWithoutPrefix(line);
                }
            }

            return String.Empty;
        }

        private static string VersionWithoutPrefix(string line)
        {
            var startingIndex = line.IndexOf(Constants.DatabaseVersionPrefix, StringComparison.Ordinal);
            startingIndex += Constants.DatabaseVersionPrefix.Length;

            return line.Substring(startingIndex);
        }
    }
}
