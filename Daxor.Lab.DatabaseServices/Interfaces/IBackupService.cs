﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace Daxor.Lab.DatabaseServices.Interfaces
{
    public interface IBackupService
    {
        void Backup(string filePath);
        ServerMessageEventHandler BackupCompletedHandler { get; set; }
        PercentCompleteEventHandler PercentCompleteHandler { get; set; }
    }
}
