﻿using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace Daxor.Lab.DatabaseServices.Interfaces
{
    public interface IRestoreService
    {
        void Restore(string destinationFileName);
        ServerMessageEventHandler RestoreCompletedHandler { get; set; }
        PercentCompleteEventHandler PercentCompleteHandler { get; set; }
    }
}
