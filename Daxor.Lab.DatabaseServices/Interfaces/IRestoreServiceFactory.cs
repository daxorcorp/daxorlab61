﻿namespace Daxor.Lab.DatabaseServices.Interfaces
{
    public interface IRestoreServiceFactory
    {
        IRestoreService CreateDaxorDatabaseRestoreService();
        IRestoreService CreateQcArchiveRestoreService();
    }
}
