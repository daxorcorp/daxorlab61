﻿namespace Daxor.Lab.DatabaseServices.Interfaces
{
    public interface IBackupServiceFactory
    {
        IBackupService CreateDaxorLabBackupService();
        IBackupService CreateQcArchiveBackupService();
    }
}
