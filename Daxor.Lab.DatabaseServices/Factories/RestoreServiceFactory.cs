﻿using System;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.DatabaseServices.Services;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.DatabaseServices.Factories
{
    public class RestoreServiceFactory : IRestoreServiceFactory
    {
        #region Fields

        private readonly ISettingsManager _settingsManager;

        #endregion

        #region Ctor

        public RestoreServiceFactory(ISettingsManager settingsManager)
        {
            if (settingsManager == null)
                throw new ArgumentNullException("settingsManager");

            _settingsManager = settingsManager;
        }

        #endregion

        #region Public API

        public IRestoreService CreateDaxorDatabaseRestoreService()
        {
            var databaseName = _settingsManager.GetSetting<string>(SettingKeys.SystemDatabaseName);
            return new DaxorRestoreService(_settingsManager, databaseName);
        }

        public IRestoreService CreateQcArchiveRestoreService()
        {
            var databaseName = _settingsManager.GetSetting<string>(SettingKeys.SystemQcDatabaseArchiveDatabaseName);
            return new RestoreService(_settingsManager, databaseName);
        }

        #endregion
    }
}
