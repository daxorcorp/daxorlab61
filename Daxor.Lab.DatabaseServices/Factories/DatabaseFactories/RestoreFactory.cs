﻿using Daxor.Lab.DatabaseServices.Interfaces;
using Microsoft.SqlServer.Management.Smo;

namespace Daxor.Lab.DatabaseServices.Factories.DatabaseFactories
{
    public static class RestoreFactory
    {
        public static Restore CreateRestore(string databaseName, string fileName, IRestoreService service)
        {
            var restore = new Restore
            {
                Database = databaseName,
                Action = RestoreActionType.Database,
                Restart = false,
                ReplaceDatabase = true,
                NoRecovery = false
            };

            restore.Devices.AddDevice(fileName, DeviceType.File);

            if (service.RestoreCompletedHandler != null)
                restore.Complete += service.RestoreCompletedHandler;
            if (service.PercentCompleteHandler != null)
                restore.PercentComplete += service.PercentCompleteHandler;

            return restore;
        }
    }
}
