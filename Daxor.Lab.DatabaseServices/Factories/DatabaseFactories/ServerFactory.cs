﻿using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace Daxor.Lab.DatabaseServices.Factories.DatabaseFactories
{
    public static class ServerFactory
    {
        public static Server ConfigureRestoreServer(ISettingsManager settingsManager)
        {
            var connection = ConfigureConnection(settingsManager);
            connection.DatabaseName = "master";

            return new Server(connection) {LoginMode = ServerLoginMode.Mixed};
        }

        public static Server ConfigureBackupServer(ISettingsManager settingsManager)
        {
            return new Server(ConfigureConnection(settingsManager));
        }

        private static ServerConnection ConfigureConnection(ISettingsManager settingsManager)
        {
            var connection = new ServerConnection
            {
                ServerInstance = settingsManager.GetSetting<string>(SettingKeys.SystemDatabaseServer),
                LoginSecure = false,
                Login = settingsManager.GetSetting<string>(SettingKeys.SystemDatabaseUsername),
                Password = settingsManager.GetSetting<string>(SettingKeys.SystemDatabasePassword)
            };
            return connection;
        }
    }
}
