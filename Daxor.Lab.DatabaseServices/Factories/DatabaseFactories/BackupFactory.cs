﻿using Daxor.Lab.DatabaseServices.Interfaces;
using Microsoft.SqlServer.Management.Smo;

namespace Daxor.Lab.DatabaseServices.Factories.DatabaseFactories
{
    public static class BackupFactory
    {
        public static Backup CreateBackup(string databaseName, string fileName, IBackupService service)
        {
            var backup = new Backup
            {
                Database = databaseName,
                Action = BackupActionType.Database,
                Incremental = false
            };

            backup.Devices.AddDevice(fileName, DeviceType.File);
            backup.BackupSetName = "Daxor Diagnostic Suite Backup";
            backup.BackupSetDescription = "Daxor Diagnostic Suite Database - Full Backup";
            backup.Initialize = true;

            if (service.BackupCompletedHandler != null)
                backup.Complete += service.BackupCompletedHandler;
            if (service.PercentCompleteHandler != null)
                backup.PercentComplete += service.PercentCompleteHandler;

            return backup;
        }
    }
}
