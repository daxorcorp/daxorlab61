﻿using System;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.DatabaseServices.Services;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.SqlServer.Management.Common;

namespace Daxor.Lab.DatabaseServices.Factories
{
    public class BackupServiceFactory : IBackupServiceFactory
    {
        private readonly ISettingsManager _settingsManager;

        public BackupServiceFactory(ISettingsManager settingsManager)
        {
            if (settingsManager == null)
                throw new ArgumentNullException("settingsManager");

            _settingsManager = settingsManager;
        }

        public IBackupService CreateDaxorLabBackupService()
        {
            var databaseName = _settingsManager.GetSetting<string>(SettingKeys.SystemDatabaseName);
            ThrowExceptionIfNotValid(databaseName);
            return new BackupService(_settingsManager, databaseName);
        }

        public IBackupService CreateQcArchiveBackupService()
        {
            var databaseName = _settingsManager.GetSetting<string>(SettingKeys.SystemQcDatabaseArchiveDatabaseName);
            ThrowExceptionIfNotValid(databaseName);
            return new BackupService(_settingsManager, databaseName);
        }

        // ReSharper disable once UnusedParameter.Local
        private static void ThrowExceptionIfNotValid(string databaseName)
        {
            if (databaseName == null)
                throw new ArgumentNullException("databaseName");
            
            if (String.IsNullOrWhiteSpace(databaseName))
                throw new InvalidArgumentException("databaseName cannot consist of whitespace");
        }
    }
}