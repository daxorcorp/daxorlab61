using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Services;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.OrderingPhysiciansReportViewModel_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_list_of_physicians_records_changes
    {
        [TestMethod]
        public void And_the_new_list_is_null_then_an_empty_list_is_used_instead()
        {
            var stubMessageManager = Substitute.For<IMessageManager>();
            stubMessageManager.GetMessage(Arg.Any<string>()).Returns(new Message("notnull", "", "", "", MessageType.Undefined));
            
            var viewModel = new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(),
                Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>(),
                Substitute.For<IReportControllerFactory>(), Substitute.For<IReportProcessor>(),
                Substitute.For<IUtilityDataService>(), stubMessageManager,
                Substitute.For<IMessageBoxDispatcher>(), Substitute.For<IFolderBrowser>(),
                Substitute.For<IStarBurnWrapper>(), Substitute.For<IExcelFileBuilder>()) { OrderingPhysicianDataRecords = null };

            Assert.AreEqual(0, viewModel.OrderingPhysicianDataRecords.Count);
        }
    }
    // ReSharper restore InconsistentNaming
}
