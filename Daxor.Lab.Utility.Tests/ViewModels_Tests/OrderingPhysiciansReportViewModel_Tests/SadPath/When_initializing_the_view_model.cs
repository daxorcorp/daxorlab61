using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Services;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.OrderingPhysiciansReportViewModel_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_view_model
    {
        [TestMethod]
        public void And_the_region_manager_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                // ReSharper disable once ObjectCreationAsStatement
                new OrderingPhysiciansReportViewModel(null,
                    Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                    Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                    Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(),
                    Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                    Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(),
                    Substitute.For<IExcelFileBuilder>());
            }, "regionManager");
        }

        [TestMethod]
        public void And_the_event_aggregator_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                // ReSharper disable once ObjectCreationAsStatement
                new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                    null, Substitute.For<ILoggerFacade>(),
                    Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                    Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(), 
                    Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                    Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(),
                    Substitute.For<IExcelFileBuilder>());
            }, "eventAggregator");
        }

        [TestMethod]
        public void And_the_logger_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                // ReSharper disable once ObjectCreationAsStatement
                new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                    Substitute.For<IEventAggregator>(), null,
                    Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                    Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(), 
                    Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                    Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(),
                    Substitute.For<IExcelFileBuilder>());
            }, "logger");
        }

        [TestMethod]
        public void And_the_settings_manager_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                // ReSharper disable once ObjectCreationAsStatement
                new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                    Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                    null, Substitute.For<IReportControllerFactory>(),
                    Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(), 
                    Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                    Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(),
                    Substitute.For<IExcelFileBuilder>());
            }, "settingsManager");
        }

        [TestMethod]
        public void And_the_report_controller_factory_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                // ReSharper disable once ObjectCreationAsStatement
                new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                    Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                    Substitute.For<ISettingsManager>(), null,
                    Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(), 
                    Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                    Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(),
                    Substitute.For<IExcelFileBuilder>());
            }, "reportControllerFactory");
        }

        [TestMethod]
        public void And_the_report_processor_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                // ReSharper disable once ObjectCreationAsStatement
                new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                    Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                    Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                    null, Substitute.For<IUtilityDataService>(), Substitute.For<IMessageManager>(),
                    Substitute.For<IMessageBoxDispatcher>(), Substitute.For<IFolderBrowser>(),
                    Substitute.For<IStarBurnWrapper>(), Substitute.For<IExcelFileBuilder>());
            }, "reportProcessor");
        }

        [TestMethod]
        public void And_the_utility_data_service_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                // ReSharper disable once ObjectCreationAsStatement
                new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                    Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                    Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                    Substitute.For<IReportProcessor>(), null, Substitute.For<IMessageManager>(),
                    Substitute.For<IMessageBoxDispatcher>(), Substitute.For<IFolderBrowser>(),
                    Substitute.For<IStarBurnWrapper>(), Substitute.For<IExcelFileBuilder>());
            }, "utilityDataService");
        }

        [TestMethod]
        public void And_the_message_manager_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                // ReSharper disable once ObjectCreationAsStatement
                new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                    Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                    Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                    Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(), null,
                    Substitute.For<IMessageBoxDispatcher>(), Substitute.For<IFolderBrowser>(),
                    Substitute.For<IStarBurnWrapper>(), Substitute.For<IExcelFileBuilder>());
            }, "messageManager");
        }
        
        [TestMethod]
        public void And_the_message_box_dispatcher_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                // ReSharper disable once ObjectCreationAsStatement
                new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                    Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                    Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                    Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(), Substitute.For<IMessageManager>(),
                    null, Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(),
                    Substitute.For<IExcelFileBuilder>());
            }, "messageBoxDispatcher");
        }

        [TestMethod]
        public void And_the_folder_browser_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                // ReSharper disable once ObjectCreationAsStatement
                new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                    Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                    Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                    Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(), Substitute.For<IMessageManager>(),
                    Substitute.For<IMessageBoxDispatcher>(), null, Substitute.For<IStarBurnWrapper>(),
                    Substitute.For<IExcelFileBuilder>());
            }, "folderBrowser");
        }

        [TestMethod]
        public void And_the_starburn_wrapper_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                // ReSharper disable once ObjectCreationAsStatement
                new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                    Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                    Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                    Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(), Substitute.For<IMessageManager>(),
                    Substitute.For<IMessageBoxDispatcher>(), Substitute.For<IFolderBrowser>(), null, Substitute.For<IExcelFileBuilder>());
            }, "starBurnWrapper");
        }

        [TestMethod]
        public void And_the_excel_file_builder_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                // ReSharper disable once ObjectCreationAsStatement
                new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                    Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                    Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                    Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(), Substitute.For<IMessageManager>(),
                    Substitute.For<IMessageBoxDispatcher>(), Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(), null);
            }, "excelFileBuilder");
        }
    }
    // ReSharper restore InconsistentNaming
}
