using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Services;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.OrderingPhysiciansReportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_list_of_physician_records_changes
    {
        private OrderingPhysiciansReportViewModel _viewModel;
        private Message _message;
        private IMessageManager _fakeMessageManager;
        private IMessageBoxDispatcher _mockDispatcher;


        [TestInitialize]
        public void Before_each_test()
        {
            var stubUtilityDataService = Substitute.For<IUtilityDataService>();
            stubUtilityDataService.GetOrderingPhysiciansBetween(DateTime.Now, DateTime.Now)
                .ReturnsForAnyArgs(new List<OrderingPhysicianDataRecord>());

            _message = new Message(MessageKeys.UtilityOrderingPhysiciansLookupFailed, "Foo", "FIXIT!", "UTIL 12", MessageType.Error);

            _fakeMessageManager = Substitute.For<IMessageManager>();
            _fakeMessageManager.GetMessage(MessageKeys.UtilityOrderingPhysiciansLookupFailed)
                .Returns(_message);

            _mockDispatcher = Substitute.For<IMessageBoxDispatcher>();

            _viewModel = new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(),
                Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>(),
                Substitute.For<IReportControllerFactory>(), Substitute.For<IReportProcessor>(),
                stubUtilityDataService, _fakeMessageManager,
                _mockDispatcher, Substitute.For<IFolderBrowser>(),
                Substitute.For<IStarBurnWrapper>(), Substitute.For<IExcelFileBuilder>());
        }

        [TestMethod]
        public void Then_PropertyChanged_is_fired_on_AreThereTestsInRange()
        {
            // Arrange
            var wasRaised = false;
            _viewModel.IsActive = true;

            _viewModel.PropertyChanged += (sender, args) => 
                wasRaised = args.PropertyName == "AreThereTestsInTheRange" || wasRaised;

            var stubListWithRecords = new List<OrderingPhysicianDataRecord> {new OrderingPhysicianDataRecord()};

            // Act
            _viewModel.OrderingPhysicianDataRecords = stubListWithRecords;

            // Assert
            Assert.IsTrue(wasRaised);
        }

        [TestMethod]
        public void Then_PropertyChanged_is_fired_on_NumberOfTestsInRangeText()
        {
            // Arrange
            var wasRaised = false;
            _viewModel.IsActive = true;

            _viewModel.PropertyChanged += (sender, args) => 
                wasRaised = args.PropertyName == "NumberOfTestsInRangeText" || wasRaised;

            var stubListWithRecords = new List<OrderingPhysicianDataRecord> { new OrderingPhysicianDataRecord() };

            // Act
            _viewModel.OrderingPhysicianDataRecords = stubListWithRecords;

            // Assert
            Assert.IsTrue(wasRaised);
        }

        [TestMethod]
        public void Then_we_evaluate_whether_to_enable_or_disable_the_view_report_button()
        {
            // Arrange
            _viewModel.IsActive = true;

            Assert.IsFalse(_viewModel.ViewReportCommand.CanExecute(null)); // Should be disabled with an empty list

            var stubListWithRecords = new List<OrderingPhysicianDataRecord> {new OrderingPhysicianDataRecord()};

            // Act
            _viewModel.OrderingPhysicianDataRecords = stubListWithRecords;

            // Assert
            Assert.IsTrue(_viewModel.ViewReportCommand.CanExecute(null));
        }

        [TestMethod]
        public void Then_we_evaluate_whether_to_enable_or_disable_the_print_report_button()
        {
            //Arrange
            _viewModel.IsActive = true;
            
            Assert.IsFalse(_viewModel.PrintReportCommand.CanExecute(null)); //Disabled w/ empty list

            var stubListWithRecords = new List<OrderingPhysicianDataRecord> { new OrderingPhysicianDataRecord() };

            //Act
            _viewModel.OrderingPhysicianDataRecords = stubListWithRecords;

            //Assert
            Assert.IsTrue(_viewModel.PrintReportCommand.CanExecute(null));
        }

        [TestMethod]
        public void Then_we_evaluate_whether_to_enable_or_disable_the_export_report_button()
        {
            //Arrange
            _viewModel.IsActive = true;

            Assert.IsFalse(_viewModel.ExportReportCommand.CanExecute(null)); //Disabled w/ empty list

            var stubListWithRecords = new List<OrderingPhysicianDataRecord> { new OrderingPhysicianDataRecord() };

            //Act
            _viewModel.OrderingPhysicianDataRecords = stubListWithRecords;

            //Assert
            Assert.IsTrue(_viewModel.ExportReportCommand.CanExecute(null));
        }

        [TestMethod]
        public void And_the_value_the_list_is_being_set_to_is_null_then_a_message_is_displayed()
        {
            _viewModel.OrderingPhysicianDataRecords = null;

            _fakeMessageManager.Received().GetMessage(MessageKeys.UtilityOrderingPhysiciansLookupFailed);

            _mockDispatcher.Received()
                .ShowMessageBox(MessageBoxCategory.Error, _message.FormattedMessage,
                    OrderingPhysiciansReportViewModel.PhysiciansListNullErrorCaption, MessageBoxChoiceSet.Close);
        }
    }
    // ReSharper restore InconsistentNaming
}
