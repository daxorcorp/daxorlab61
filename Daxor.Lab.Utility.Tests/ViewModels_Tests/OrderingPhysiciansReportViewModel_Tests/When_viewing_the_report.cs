using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Services;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Regions;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.OrderingPhysiciansReportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_viewing_the_report
    {
        [TestMethod]
        public void And_a_report_is_already_being_shown_then_the_view_report_button_is_disabled()
        {
            var viewModel = new OrderingPhysiciansReportViewModel(CreateNonEmptyRegionManager(),
                Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(),
                Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(), Substitute.For<IExcelFileBuilder>());

            Assert.IsFalse(viewModel.ViewReportCommand.CanExecute(null));
        }

        [TestMethod]
        public void And_no_report_is_being_shown_and_there_are_tests_in_the_date_range_then_the_view_report_button_is_enabled()
        {
            var stubUtilityDataService = Substitute.For<IUtilityDataService>();
            stubUtilityDataService.GetOrderingPhysiciansBetween(Arg.Any<DateTime>(), Arg.Any<DateTime>())
                .Returns(new List<OrderingPhysicianDataRecord>{new OrderingPhysicianDataRecord()});
            var viewModel = new OrderingPhysiciansReportViewModel(CreateEmptyRegionManager(),
                Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                Substitute.For<IReportProcessor>(), stubUtilityDataService, 
                Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(), Substitute.For<IExcelFileBuilder>()) { IsActive = true };


            Assert.IsTrue(viewModel.ViewReportCommand.CanExecute(null));
        }

        [TestMethod]
        public void Then_the_busy_indicator_is_shown_and_hidden_correctly()
        {
            // Arrange
            var mockEvent = Substitute.For<BusyStateChanged>();
            var stubEventAggregator = Substitute.For<IEventAggregator>();
            stubEventAggregator.GetEvent<BusyStateChanged>().Returns(mockEvent);

            var viewModel = new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                stubEventAggregator, Substitute.For<ILoggerFacade>(),
                Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(), 
                Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(), Substitute.For<IExcelFileBuilder>()) { TaskScheduler = new CurrentThreadTaskScheduler() };

            // Act
            viewModel.ViewReportCommand.Execute(null);

            // Assert
            Received.InOrder(() =>
            {
                mockEvent.Publish(Arg.Is<BusyPayload>(payload => payload.IsBusy && payload.Message == "Loading Report..."));
                mockEvent.Publish(Arg.Is<BusyPayload>(payload => !payload.IsBusy));
            });
        }

        [TestMethod]
        public void Then_a_log_entry_is_written()
        {
            // Arrange
            var stubEventAggregator = Substitute.For<IEventAggregator>();
            stubEventAggregator.GetEvent<BusyStateChanged>().Returns(Substitute.For<BusyStateChanged>());
            var mockLogger = Substitute.For<ILoggerFacade>();

            var viewModel = new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                stubEventAggregator, mockLogger,
                Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(), 
                Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(), Substitute.For<IExcelFileBuilder>()) { TaskScheduler = new CurrentThreadTaskScheduler() };

            // Act
            viewModel.ViewReportCommand.Execute(null);

            // Assert
            mockLogger.Received().Log("VIEW ORDERING PHYSICIANS REPORT BUTTON CLICKED", Category.Info, Priority.None);
        }

        [TestMethod]
        public void Then_the_factory_is_used_to_get_the_correct_report_controller()
        {
            // Arrange
            var stubRegionManager = Substitute.For<IRegionManager>();
            var stubLogger = Substitute.For<ILoggerFacade>();
            var stubSettingsManager = Substitute.For<ISettingsManager>();
            var stubReportProcessor = Substitute.For<IReportProcessor>();
            var fromDateTime = new DateTime(2014, 1, 1);
            var toDateTime = new DateTime(2014, 11, 11);
            var dataRecords = new List<OrderingPhysicianDataRecord> {new OrderingPhysicianDataRecord()};
            var stubUtilityDataService =  Substitute.For<IUtilityDataService>();
            stubUtilityDataService.GetOrderingPhysiciansBetween(Arg.Any<DateTime>(), Arg.Any<DateTime>())
                .Returns(dataRecords);
            
            var stubEventAggregator = Substitute.For<IEventAggregator>();
            stubEventAggregator.GetEvent<BusyStateChanged>().Returns(Substitute.For<BusyStateChanged>());

            var stubMessageBoxDispatcher = Substitute.For<IMessageBoxDispatcher>();
            var stubFolderBrowser = Substitute.For<IFolderBrowser>();
            var stubMessageManager = Substitute.For<IMessageManager>();
            var stubStarburnWrapper = Substitute.For<IStarBurnWrapper>();
            var stubExcelFileBuilder = Substitute.For<IExcelFileBuilder>();

            var mockFactory = Substitute.For<IReportControllerFactory>();
            mockFactory.CreateOrderingPhysiciansReportController(null, null, null, null, fromDateTime, toDateTime, new List<OrderingPhysicianDataRecord>(), 
                null, null, null, null, null, null).ReturnsForAnyArgs(Substitute.For<ReportControllerBase>());

            var viewModel = new OrderingPhysiciansReportViewModel(stubRegionManager,
                stubEventAggregator, stubLogger,
                stubSettingsManager, mockFactory,
                stubReportProcessor, stubUtilityDataService,
                stubMessageManager, stubMessageBoxDispatcher,
                stubFolderBrowser, stubStarburnWrapper,
                stubExcelFileBuilder)
            {
                TaskScheduler = new CurrentThreadTaskScheduler(),
                IsActive = true,
                FromDateTime = fromDateTime, 
                ToDateTime = toDateTime,
            };

            // Act
            viewModel.ViewReportCommand.Execute(null);

            // Assert
            mockFactory.Received().CreateOrderingPhysiciansReportController(stubLogger, stubRegionManager,
                stubEventAggregator, stubSettingsManager, fromDateTime, toDateTime, dataRecords, 
                stubReportProcessor, stubMessageBoxDispatcher, stubFolderBrowser, stubMessageManager, stubStarburnWrapper, stubExcelFileBuilder);
        }

        [TestMethod]
        public void Then_the_report_is_shown()
        {
            // Arrange
            var stubRegionManager = Substitute.For<IRegionManager>();
            var stubLogger = Substitute.For<ILoggerFacade>();
            var stubSettingsManager = Substitute.For<ISettingsManager>();
            var stubReportProcessor = Substitute.For<IReportProcessor>();
            var fromDateTime = new DateTime(2014, 1, 1);
            var toDateTime = new DateTime(2014, 11, 11);
            var stubUtilityDataService = Substitute.For<IUtilityDataService>();
            stubUtilityDataService.GetOrderingPhysiciansBetween(Arg.Any<DateTime>(), Arg.Any<DateTime>())
                .Returns(new List<OrderingPhysicianDataRecord>());

            var stubEventAggregator = Substitute.For<IEventAggregator>();
            stubEventAggregator.GetEvent<BusyStateChanged>().Returns(Substitute.For<BusyStateChanged>());

            var mockReportController = Substitute.For<ReportControllerBase>();

            var stubFactory = Substitute.For<IReportControllerFactory>();
            stubFactory.CreateOrderingPhysiciansReportController(null, null, null, null, DateTime.Now, DateTime.Now, new List<OrderingPhysicianDataRecord>(), 
                null, null, null, null, null, null).ReturnsForAnyArgs(mockReportController);

            var viewModel = new OrderingPhysiciansReportViewModel(stubRegionManager,
                stubEventAggregator, stubLogger,
                stubSettingsManager, stubFactory,
                stubReportProcessor, stubUtilityDataService, 
                Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(),
                Substitute.For<IExcelFileBuilder>())
            {
                TaskScheduler = new CurrentThreadTaskScheduler(),
                FromDateTime = fromDateTime,
                ToDateTime = toDateTime
            };

            // Act
            viewModel.ViewReportCommand.Execute(null);

            // Assert
            mockReportController.Received().ShowReport();
        }

        private static IRegionManager CreateEmptyRegionManager()
        {
            var regionManager = new RegionManager();
            var region = Substitute.For<IRegion>();
            region.Name.Returns(RegionNames.ReportRegion);

            // ReSharper disable once CollectionNeverUpdated.Local
            var views = new List<object>();
            var viewCollection = Substitute.For<IViewsCollection>();
            viewCollection.GetEnumerator().Returns(views.GetEnumerator());

            region.ActiveViews.Returns(viewCollection);

            regionManager.Regions.Add(region);

            return regionManager;
        }

        private static IRegionManager CreateNonEmptyRegionManager()
        {
            var regionManager = new RegionManager();
            var region = Substitute.For<IRegion>();
            region.Name.Returns(RegionNames.ReportRegion);

            var views = new List<object> { new object() };
            var viewCollection = Substitute.For<IViewsCollection>();
            viewCollection.GetEnumerator().Returns(views.GetEnumerator());

            region.ActiveViews.Returns(viewCollection);
            
            regionManager.Regions.Add(region);

            return regionManager;
        }
    }
    // ReSharper restore InconsistentNaming
}
