﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Services;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.OrderingPhysiciansReportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_the_number_of_tests_in_range_text
    {
        private OrderingPhysiciansReportViewModel _viewModel;
        private Message _message;
        private IMessageManager _fakeMessageManager;
        private IMessageBoxDispatcher _mockDispatcher;

        [TestInitialize]
        public void Before_each_test()
        {
            var stubUtilityDataService = Substitute.For<IUtilityDataService>();
            stubUtilityDataService.GetOrderingPhysiciansBetween(DateTime.Now, DateTime.Now)
                .ReturnsForAnyArgs(new List<OrderingPhysicianDataRecord>());

            _message = new Message(MessageKeys.UtilityOrderingPhysiciansLookupFailed, "Foo", "FIXIT!", "UTIL 12", MessageType.Error);

            _fakeMessageManager = Substitute.For<IMessageManager>();
            _fakeMessageManager.GetMessage(MessageKeys.UtilityOrderingPhysiciansLookupFailed)
                .Returns(_message);

            _mockDispatcher = Substitute.For<IMessageBoxDispatcher>();

            _viewModel = new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(),
                Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>(),
                Substitute.For<IReportControllerFactory>(), Substitute.For<IReportProcessor>(),
                stubUtilityDataService, _fakeMessageManager,
                _mockDispatcher, Substitute.For<IFolderBrowser>(),
                Substitute.For<IStarBurnWrapper>(), Substitute.For<IExcelFileBuilder>());
        }

        [TestMethod]
        [RequirementValue("There were no tests ordered")]
        public void And_there_are_no_physicians_records_then_the_text_matches_requirements()
        {
            _viewModel.IsActive = true;
            var stubListWithRecords = new List<OrderingPhysicianDataRecord>();

            // Act
            _viewModel.OrderingPhysicianDataRecords = stubListWithRecords;

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), _viewModel.NumberOfTestsInRangeText);
        }

        [TestMethod]
        [RequirementValue("There was 1 test ordered")]
        public void And_there_is_one_physicians_record_then_the_text_matches_requirements()
        {
            _viewModel.IsActive = true;

            var stubListWithRecords = new List<OrderingPhysicianDataRecord> { new OrderingPhysicianDataRecord() };

            // Act
            _viewModel.OrderingPhysicianDataRecords = stubListWithRecords;

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), _viewModel.NumberOfTestsInRangeText);
        }

        [TestMethod]
        [RequirementValue("There were 2 tests ordered")]
        public void And_there_are_2_physicians_records_then_the_text_matches_requirements()
        {
            _viewModel.IsActive = true;
            var stubListWithRecords = new List<OrderingPhysicianDataRecord> { new OrderingPhysicianDataRecord(), new OrderingPhysicianDataRecord() };

            // Act
            _viewModel.OrderingPhysicianDataRecords = stubListWithRecords;

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), _viewModel.NumberOfTestsInRangeText);
        }

        [TestMethod]
        [RequirementValue("There were 10 tests ordered")]
        public void And_there_are_10_physicians_records_then_the_text_matches_requirements()
        {
            _viewModel.IsActive = true;
            var stubListWithRecords = new List<OrderingPhysicianDataRecord> ();
            for(var i = 0; i < 10; i++)
                stubListWithRecords.Add(new OrderingPhysicianDataRecord());

            // Act
            _viewModel.OrderingPhysicianDataRecords = stubListWithRecords;

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), _viewModel.NumberOfTestsInRangeText);
        }

        [TestMethod]
        [RequirementValue("There were 200 tests ordered")]
        public void And_there_are_200_physicians_records_then_the_text_matches_requirements()
        {
            _viewModel.IsActive = true;
            var stubListWithRecords = new List<OrderingPhysicianDataRecord>();
            for (var i = 0; i < 200; i++)
                stubListWithRecords.Add(new OrderingPhysicianDataRecord());

            // Act
            _viewModel.OrderingPhysicianDataRecords = stubListWithRecords;

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), _viewModel.NumberOfTestsInRangeText);
        }

        [TestMethod]
        [RequirementValue("There were 21 tests ordered")]
        public void And_there_are_multiple_tests_in_the_physicians_records_then_the_text_matches_requirements()
        {
            _viewModel.IsActive = true;
            var stubListWithRecords = new List<OrderingPhysicianDataRecord>();
            for (var i = 0; i < 5; i++)
            {
                // Each test record represents a physician, so a physician can have more than one test.
                var recordToAdd = new OrderingPhysicianDataRecord
                {
                    TestRecordsList = new List<OrderingPhysicianTestRecord>
                    {
                        new OrderingPhysicianTestRecord(), 
                        new OrderingPhysicianTestRecord(), 
                        new OrderingPhysicianTestRecord(), 
                        new OrderingPhysicianTestRecord()
                    }
                };
                stubListWithRecords.Add(recordToAdd);
            }
            stubListWithRecords.Add(new OrderingPhysicianDataRecord());

            // Act
            _viewModel.OrderingPhysicianDataRecords = stubListWithRecords;

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), _viewModel.NumberOfTestsInRangeText);
        }
    }
    // ReSharper restore RedundantArgumentName
}