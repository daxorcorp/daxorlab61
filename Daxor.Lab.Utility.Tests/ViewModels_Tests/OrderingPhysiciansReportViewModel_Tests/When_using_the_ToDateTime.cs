﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Services;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.OrderingPhysiciansReportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_ToDateTime
    {
        private const int OneDay = 1;
        private DateTime DateThatIsNotTodaysDate = new DateTime(1981, 4, 26);

        private OrderingPhysiciansReportViewModel _viewModel;
        private IUtilityDataService _fakeDataService;

        [TestInitialize]
        public void Initialize()
        {
            _fakeDataService = Substitute.For<IUtilityDataService>();
            _fakeDataService.GetOrderingPhysiciansBetween(Arg.Any<DateTime>(), Arg.Any<DateTime>())
                .Returns(new List<OrderingPhysicianDataRecord>());

            _viewModel = new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                Substitute.For<IReportProcessor>(), _fakeDataService, 
                Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(),
                Substitute.For<IExcelFileBuilder>());
        }

        [TestMethod]
        public void And_the_view_is_made_active_then_the_ToDateTime_is_set_to_todays_date()
        {
            _viewModel.ToDateTime = DateThatIsNotTodaysDate;

            _viewModel.IsActive = true;

            Assert.AreEqual(DateTime.Today.Date, _viewModel.ToDateTime.Date);
        }

        [TestMethod]
        public void And_the_FromDateTime_is_set_to_the_same_date_as_the_ToDateTime_then_the_ToDateTime_is_set_to_one_day_after_the_FromDateTime()
        {
            _viewModel.ToDateTime = DateThatIsNotTodaysDate;
            _viewModel.FromDateTime = DateThatIsNotTodaysDate;

            Assert.AreEqual(DateThatIsNotTodaysDate.AddDays(OneDay).Date, _viewModel.ToDateTime.Date);
        }

        [TestMethod]
        public void And_the_FromDateTime_is_set_to_a_later_date_than_the_ToDateTime_and_the_ToDateTime_is_not_todays_date_then_the_ToDateTime_is_set_to_todays_date()
        {
            _viewModel.ToDateTime = DateThatIsNotTodaysDate.AddDays(-16*OneDay);
            _viewModel.FromDateTime = DateThatIsNotTodaysDate;

            Assert.AreEqual(DateThatIsNotTodaysDate.AddDays(OneDay).Date, _viewModel.ToDateTime.Date);
        }

        [TestMethod]
        public void And_the_FromDateTime_is_set_to_a_later_date_than_the_ToDateTime_and_the_ToDateTime_is_todays_date_then_the_ToDateTime_is_set_to_todays_date()
        {
            _viewModel.ToDateTime = DateTime.Today;
            _viewModel.FromDateTime = DateTime.Today;

            Assert.AreEqual(DateTime.Today.Date, _viewModel.ToDateTime.Date);
        }

        [TestMethod]
        public void And_the_FromDateTime_is_set_to_todays_date_then_the_ToDateTime_is_set_to_todays_date_and_the_FromDateTime_is_set_to_yesterdays_date()
        {
            _viewModel.ToDateTime = DateThatIsNotTodaysDate;
            _viewModel.FromDateTime = DateTime.Today;

            Assert.AreEqual(DateTime.Today.Date, _viewModel.ToDateTime.Date);
            Assert.AreEqual(DateTime.Today.AddDays(-OneDay).Date, _viewModel.FromDateTime.Date);
        }

        [TestMethod]
        public void And_the_value_is_being_set_then_PropertyChanged_is_raised_for_both_the_to_and_from_properties()
        {
            var propertiesChanged = new List<string>();
            _viewModel.PropertyChanged += (sender, args) => propertiesChanged.Add(args.PropertyName);

            _viewModel.ToDateTime = DateThatIsNotTodaysDate;

            CollectionAssert.Contains(propertiesChanged, "FromDateTime");
            CollectionAssert.Contains(propertiesChanged, "ToDateTime");
        }

        [TestMethod]
        public void And_the_value_is_changed_then_the_utility_data_service_is_queried_using_the_current_date_range_plus_one_day()
        {
            _viewModel.IsActive = true;

            _viewModel.ToDateTime = DateThatIsNotTodaysDate;

            _fakeDataService.Received().GetOrderingPhysiciansBetween(Arg.Any<DateTime>(), DateThatIsNotTodaysDate.AddDays(1));
        }
    }
    // ReSharper restore RedundantArgumentName
}