using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Services;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.OrderingPhysiciansReportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_view_model
    {
        private OrderingPhysiciansReportViewModel _viewModel;

        [TestInitialize]
        public void Before_each_test()
        {
            _viewModel = new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(), 
                Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                Substitute.For<IReportProcessor>(), Substitute.For<IUtilityDataService>(), 
                Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(),
                Substitute.For<IExcelFileBuilder>());
        }

        [TestMethod]
        public void Then_the_view_report_command_is_initialized()
        {
            Assert.IsNotNull(_viewModel.ViewReportCommand);
        }

        [TestMethod]
        public void Then_the_print_report_command_is_initialized()
        {
            Assert.IsNotNull(_viewModel.PrintReportCommand);
        }

        [TestMethod]
        public void Then_the_export_report_command_is_initialized()
        {
            Assert.IsNotNull(_viewModel.ExportReportCommand);
        }

        [TestMethod]
        public void Then_the_collection_of_ordering_physicians_data_records_is_empty()
        {
            Assert.AreEqual(0, _viewModel.OrderingPhysicianDataRecords.Count);
        }
    }
    // ReSharper restore InconsistentNaming
}
