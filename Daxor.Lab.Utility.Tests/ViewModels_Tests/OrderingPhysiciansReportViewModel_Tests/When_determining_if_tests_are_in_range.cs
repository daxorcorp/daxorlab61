﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Services;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.OrderingPhysiciansReportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_tests_are_in_range
    {
        [TestMethod]
        public void And_the_list_of_tests_is_empty_then_there_are_no_tests_in_range()
        {
            var stubUtilityDataService = Substitute.For<IUtilityDataService>();

            var stubEmptyList = new List<OrderingPhysicianDataRecord>();

            stubUtilityDataService.GetOrderingPhysiciansBetween(DateTime.Now, DateTime.Now)
                .ReturnsForAnyArgs(stubEmptyList);

            var orderingPhysiciansReportViewModel = new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),Substitute.For<IEventAggregator>(), 
                                                                                          Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>(),
                                                                                          Substitute.For<IReportControllerFactory>(), Substitute.For<IReportProcessor>(),
                                                                                          stubUtilityDataService, Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                                                                                          Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(),
                                                                                          Substitute.For<IExcelFileBuilder>())
            {
                IsActive = true
            };

            Assert.IsFalse(orderingPhysiciansReportViewModel.AreThereTestsInTheRange);
        }

        [TestMethod]
        public void And_the_list_of_tests_isnt_empty_then_there_are_tests_in_range()
        {
            var stubUtilityDataService = Substitute.For<IUtilityDataService>();

            var stubListWithStuffInIt = new List<OrderingPhysicianDataRecord> {new OrderingPhysicianDataRecord()};

            stubUtilityDataService.GetOrderingPhysiciansBetween(DateTime.Now, DateTime.Now)
                .ReturnsForAnyArgs(stubListWithStuffInIt);

            var orderingPhysiciansReportViewModel = new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(), Substitute.For<IEventAggregator>(),
                                                                                          Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>(),
                                                                                          Substitute.For<IReportControllerFactory>(), Substitute.For<IReportProcessor>(),
                                                                                          stubUtilityDataService, Substitute.For<IMessageManager>(), Substitute.For<IMessageBoxDispatcher>(),
                                                                                          Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(),
                                                                                          Substitute.For<IExcelFileBuilder>())
            {
                IsActive = true
            };

            Assert.IsTrue(orderingPhysiciansReportViewModel.AreThereTestsInTheRange);
        }
    }
    // ReSharper restore RedundantArgumentName
}