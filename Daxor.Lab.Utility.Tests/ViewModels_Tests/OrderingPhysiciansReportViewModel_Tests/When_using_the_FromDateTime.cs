﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Services;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.OrderingPhysiciansReportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_FromDateTime
    {
        private const int OneDay = 1;
        private DateTime DateThatIsNotTodaysDate = new DateTime(1981, 4, 26);

        private OrderingPhysiciansReportViewModel _viewModel;

        private IUtilityDataService _fakeDataService;

        [TestInitialize]
        public void Initialize()
        {
            _fakeDataService = Substitute.For<IUtilityDataService>();
            _fakeDataService.GetOrderingPhysiciansBetween(Arg.Any<DateTime>(), Arg.Any<DateTime>())
                .Returns(new List<OrderingPhysicianDataRecord>());

            _viewModel = new OrderingPhysiciansReportViewModel(Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(), Substitute.For<ILoggerFacade>(),
                Substitute.For<ISettingsManager>(), Substitute.For<IReportControllerFactory>(),
                Substitute.For<IReportProcessor>(), _fakeDataService, Substitute.For<IMessageManager>(),
                Substitute.For<IMessageBoxDispatcher>(), Substitute.For<IFolderBrowser>(), Substitute.For<IStarBurnWrapper>(),
                Substitute.For<IExcelFileBuilder>());
        }

        [TestMethod]
        public void And_the_view_is_made_active_then_the_FromDateTime_is_set_to_30_days_prior_to_todays_date()
        {
            _viewModel.FromDateTime = DateThatIsNotTodaysDate;

            _viewModel.IsActive = true;

            Assert.AreEqual(DateTime.Today.AddDays(-30).Date, _viewModel.FromDateTime.Date);
        }

        [TestMethod]
        public void And_the_ToDateTime_is_set_to_the_same_date_as_the_FromDateTime_then_the_FromDateTime_is_set_to_one_day_before_the_ToDateTime()
        {
            _viewModel.FromDateTime = DateThatIsNotTodaysDate;
            _viewModel.ToDateTime = DateThatIsNotTodaysDate;

            Assert.AreEqual(DateThatIsNotTodaysDate.AddDays(-OneDay).Date, _viewModel.FromDateTime.Date);
        }

        [TestMethod]
        public void And_the_ToDateTime_is_set_to_an_earlier_date_than_the_FromDateTime_then_the_FromDateTime_is_set_to_one_day_before_the_ToDateTime()
        {
            _viewModel.FromDateTime = DateThatIsNotTodaysDate;
            _viewModel.ToDateTime = DateThatIsNotTodaysDate.AddDays(-16*OneDay);

            Assert.AreEqual(DateThatIsNotTodaysDate.AddDays(-17*OneDay).Date, _viewModel.FromDateTime.Date);
        }

        [TestMethod]
        public void And_the_value_is_being_set_then_PropertyChanged_is_raised()
        {
            var propertiesChanged = new List<string>();
            _viewModel.PropertyChanged += (sender, args) => propertiesChanged.Add(args.PropertyName);

            _viewModel.FromDateTime = DateThatIsNotTodaysDate;

            CollectionAssert.Contains(propertiesChanged, "FromDateTime");
        }

        [TestMethod]
        public void And_the_value_is_changed_then_the_utility_data_service_is_queried_using_the_current_date_range()
        {
            _viewModel.IsActive = true;

            _viewModel.FromDateTime = DateThatIsNotTodaysDate;

            _fakeDataService.Received().GetOrderingPhysiciansBetween(DateThatIsNotTodaysDate, Arg.Any<DateTime>());
        }
    }
    // ReSharper restore RedundantArgumentName
}