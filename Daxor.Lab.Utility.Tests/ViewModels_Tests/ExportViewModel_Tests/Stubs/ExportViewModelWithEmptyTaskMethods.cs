﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.MessageBox;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests.Stubs
{
    public class ExportViewModelWithEmptyTaskMethods : ExportViewModel
    {
        public ExportViewModelWithEmptyTaskMethods(IFolderChooser folderChooser, IEventAggregator eventAggregator)
            : base(null, null, eventAggregator, null, null, folderChooser, null, null, MockRepository.GenerateStub<ISettingsManager>(), null, null, null)
        {
            SelectedItems = new Collection<object>();
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

	    public ExportViewModelWithEmptyTaskMethods(IFolderChooser folderChooser, IEventAggregator eventAggregator,
		    IMessageManager messageManager)
		    : base(null, null, eventAggregator, null, null, folderChooser, null, null, MockRepository.GenerateStub<ISettingsManager>(), messageManager, null, null)
		{
			SelectedItems = new Collection<object>();
			TaskScheduler = new CurrentThreadTaskScheduler();
	    }

        public ExportViewModelWithEmptyTaskMethods(IFolderChooser folderChooser)
            : base(null, null, MockRepository.GenerateStub<IEventAggregator>(), null, null, folderChooser, null, null, MockRepository.GenerateStub<ISettingsManager>(), null, null, null)
        {
            SelectedItems = new Collection<object>();
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        protected override void ExportTask(List<BVATestItem> selectedBvaTestItems, string tempPath, string fileName, bool isOpticalDrive, string zipFilePath,
            CancellationTokenSource tokenSource)
        {
        }

        protected override void OnExportTaskSuccessful()
        {
            // Nope.
        }

        protected override void OnExportTaskFaulted(Exception exception)
        {
            // Nope.
        }
    }
}
