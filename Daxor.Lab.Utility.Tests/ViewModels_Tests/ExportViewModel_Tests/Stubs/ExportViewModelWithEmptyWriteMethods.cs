﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.MessageBox;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests.Stubs
{
    public class ExportViewModelWithEmptyWriteMethods : ExportViewModel
    {
        public ExportViewModelWithEmptyWriteMethods(IFolderChooser folderChooser, IEventAggregator eventAggregator)
            : base(
                null, null, eventAggregator, null, null, folderChooser, null, null,
                MockRepository.GenerateStub<ISettingsManager>(), null, null, null)
        {
            SelectedItems = new Collection<object>();
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public bool WasBurnedToOpticalMedia { get; protected set; }
        public bool WasWrittenToNonOpticalMedia { get; protected set; }

        protected override void OnExportTaskSuccessful()
        {
            // Nope.
        }

        protected override void OnExportTaskFaulted(Exception exception)
        {
            // Nope.
        }

        protected override void BurnToOpticalMedia(string tempPath, string fileName, CancellationTokenSource tokenSource)
        {
            WasBurnedToOpticalMedia = true;
        }

        protected override void SaveToNonOpticalMedia(string tempPath, string fileName, string zipFilePath)
        {
            WasWrittenToNonOpticalMedia = true;
        }

        protected override void BuildAndSaveExcelReport(List<BVATestItem> selectedBvaTestItems, string tempPath,
            string fileName)
        {
            // Nope.
        }

        protected override void CreateZipFile(string tempPath, string fileName)
        {
            // Nope.
        }

        protected override void CleanupTempFiles(string filePath)
        {
            // Nope.
        }
    }
}
