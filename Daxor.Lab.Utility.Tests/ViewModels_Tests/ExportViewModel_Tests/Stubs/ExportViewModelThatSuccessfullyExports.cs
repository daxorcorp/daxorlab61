﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageBox;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests.Stubs
{
    public class ExportViewModelThatSuccessfullyExports : ExportViewModel
    {
        public ExportViewModelThatSuccessfullyExports(IFolderChooser folderChooser, IEventAggregator eventAggregator, 
                                                      IMessageBoxDispatcher messageBoxDispatcher)
            : base(null, messageBoxDispatcher, eventAggregator, MockRepository.GenerateStub<ILoggerFacade>(), null, 
                   folderChooser, null, null, MockRepository.GenerateStub<ISettingsManager>(), null, null, null)
        {
            SelectedItems = new Collection<object>();
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        protected override void ExportTask(List<BVATestItem> selectedBvaTestItems, string tempPath, string fileName, bool isOpticalDrive, string zipFilePath,
            CancellationTokenSource tokenSource)
        {
            // Nothing to do
        }
    }
}
