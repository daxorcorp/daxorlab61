﻿using System;
using RocketDivision.StarBurnX;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests.Stubs
{
    public class DataBurnerThatFailsToBurn : DataBurner
    {
        public const string ExceptionMesasage = "oh snap";
        #region IDataBurner Members

        public DataFolder AddDirectory(string DirName)
        {
            throw new NotImplementedException();
        }

        public DataFile AddFile(string FileName)
        {
            return null;
        }

        public BootImage BootImage
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void Burn(bool TestBurn, string VolumeName, string PublisherName, string ApplicationName)
        {
            throw new Exception(ExceptionMesasage);
        }

        public void Cancel()
        {
            throw new NotImplementedException();
        }

        public DataFolder CreateFolder(string Name)
        {
            throw new NotImplementedException();
        }

        public Drive Drive
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public DataFiles Files
        {
            get { throw new NotImplementedException(); }
        }

        public DataFile GetFileObject(string objectPath)
        {
            throw new NotImplementedException();
        }

        public void ImportTrack(int TrackNumber)
        {
            throw new NotImplementedException();
        }

        public void IntDetach(int pVal)
        {
            throw new NotImplementedException();
        }

        public bool IntIsTreeBuilt(int pKey)
        {
            throw new NotImplementedException();
        }

        public void IntRemoveFromTree(int pVal)
        {
            throw new NotImplementedException();
        }

        public string LastError
        {
            get { throw new NotImplementedException(); }
        }

        public STARBURN_RESULT_CODES LastErrorCode
        {
            get { throw new NotImplementedException(); }
        }

        public bool LockTray
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public STARBURN_WRITE_MODE Mode
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Name
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool NextSessionAllowed
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void OnBeforeAddDirectory(int pVal, string pDirName)
        {
            throw new NotImplementedException();
        }

        public void OnEditLockedTree(int pKey)
        {
            throw new NotImplementedException();
        }

        public string Path
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void SaveImageFile(string FileName, string VolumeName, string PublisherName, string ApplicationName)
        {
            throw new NotImplementedException();
        }

        public int Size
        {
            get { throw new NotImplementedException(); }
        }

        public void Verify()
        {
            throw new NotImplementedException();
        }

        public int WriteSpeed
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region IDataBurnerEvents_Event Members

        public event IDataBurnerEvents_OnBufferEventHandler OnBuffer
        {
            add { }
            remove { }
        }

        public event IDataBurnerEvents_OnNameCollisionEventHandler OnNameCollision
        {
            add { }
            remove { }
        }

        public event IDataBurnerEvents_OnProgressEventHandler OnProgress
        {
            add { }
            remove { }
        }

        public event IDataBurnerEvents_OnTestWriteDisabledEventHandler OnTestWriteDisabled
        {
            add { }
            remove { }
        }

        public event IDataBurnerEvents_OnVerifyEventHandler OnVerify
        {
            add { }
            remove { }
        }

        #endregion
    }
}
