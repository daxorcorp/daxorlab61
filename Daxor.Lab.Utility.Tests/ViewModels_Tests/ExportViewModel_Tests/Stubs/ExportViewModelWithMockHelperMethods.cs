﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageBox;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests.Stubs
{
    public class ExportViewModelWithMockHelperMethods : ExportViewModel
    {
        public ExportViewModelWithMockHelperMethods(IFolderChooser folderChooser, 
                                                    IEventAggregator eventAggregator)
            : base(null, 
                   MockRepository.GenerateStub<IMessageBoxDispatcher>(), 
                   eventAggregator, 
                   MockRepository.GenerateStub<ILoggerFacade>(), null, 
                   folderChooser, 
                   null, 
                   null, 
                   MockRepository.GenerateStub<ISettingsManager>(), 
                   null, 
                   null,
                   null)
        {
            SelectedItems = new Collection<object>();
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public ExportViewModelWithMockHelperMethods(IFolderChooser folderChooser,
                                            IEventAggregator eventAggregator, IStarBurnWrapper starburnWrapper, ISettingsManager settingsManager)
            : base(null,
                   MockRepository.GenerateStub<IMessageBoxDispatcher>(),
                   eventAggregator,
                   MockRepository.GenerateStub<ILoggerFacade>(),
                   starburnWrapper,
                   folderChooser,
                   null,
                   null,
                   settingsManager,
                   null,
                   null,
                   null)
        {
            SelectedItems = new Collection<object>();
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public ExportViewModelWithMockHelperMethods(IFolderChooser folderChooser,
                                    IEventAggregator eventAggregator, IStarBurnWrapper starburnWrapper, IMessageBoxDispatcher messageBoxDispatcher, ISettingsManager settingsManager)
            : base(null,
                   messageBoxDispatcher,
                   eventAggregator,
                   MockRepository.GenerateStub<ILoggerFacade>(),
                   starburnWrapper,
                   folderChooser,
                   null,
                   null,
                   settingsManager,
                   null,
                   null,
                   null)
        {
            SelectedItems = new Collection<object>();
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public ExportViewModelWithMockHelperMethods(IFolderChooser folderChooser,
                            IEventAggregator eventAggregator, IStarBurnWrapper starburnWrapper, ILoggerFacade logger, ISettingsManager settingsManager)
            : base(null,
                   MockRepository.GenerateStub<IMessageBoxDispatcher>(),
                   eventAggregator,
                   logger,
                   starburnWrapper,
                   folderChooser,
                   null,
                   null,
                   settingsManager,
                   null,
                   null,
                   null)
        {
            SelectedItems = new Collection<object>();
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public ExportViewModelWithMockHelperMethods(IFolderChooser folderChooser,
                    IEventAggregator eventAggregator, IStarBurnWrapper starburnWrapper,
                    ILoggerFacade logger, IMessageBoxDispatcher messageBoxDispatcher, ISettingsManager settingsManager)
            : base(null,
                   messageBoxDispatcher,
                   eventAggregator,
                   logger,
                   starburnWrapper,
                   folderChooser,
                   null,
                   null,
                   settingsManager,
                   null,
                   null,
                   null)
        {
            SelectedItems = new Collection<object>();
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public bool DidBuildAndSave { get; protected set; }
        public bool DidCreateZipFile { get; protected set; }
        public bool DidSaveToNonOpticalMedia { get; protected set; }
        public bool DidCleanUpTempFiles { get; protected set; }
        public bool DidTaskFault { get; protected set; }

        protected override void BuildAndSaveExcelReport(List<BVATestItem> selectedBvaTestItems, string tempPath, string fileName)
        {
            DidBuildAndSave = true;
        }

        protected override void CleanupTempFiles(string filePath)
        {
            DidCleanUpTempFiles = true;
        }

        protected override void CreateZipFile(string tempPath, string fileName)
        {
            DidCreateZipFile = true;
        }

        protected override void SaveToNonOpticalMedia(string tempPath, string fileName, string zipFilePath)
        {
            DidSaveToNonOpticalMedia = true;
        }

        protected override void OnExportTaskFaulted(Exception exception)
        {
            DidTaskFault = true;
        }

        protected override void OnExportTaskSuccessful()
        {
            //nope
        }
    }
}
