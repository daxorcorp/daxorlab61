﻿using System.Collections.Generic;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Services;
using Daxor.Lab.Utility.ViewModels;
using Rhino.Mocks;
using Daxor.Lab.BVA.Interfaces;
using Microsoft.Practices.Composite.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager;
using Microsoft.Practices.Composite.Logging;
using Daxor.Lab.MessageBox;
using Daxor.Lab.MessageManager.Interfaces;
using System.Collections.ObjectModel;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests.Stubs
{
    public class ExportViewModelThatSuccessfullyExportsToZipFile : ExportViewModel
    {
        public const string FilePath = @"C:\temp\";
        public const string FileName = "somefile";
        public const string FullFileName = FilePath + FileName;

        public ExportViewModelThatSuccessfullyExportsToZipFile(IFolderChooser folderChooser, IEventAggregator eventAggregator, IZipFileFactory zipFileFactory) :
            base(MockRepository.GenerateStub<IBVADataService>(), MockRepository.GenerateStub<IMessageBoxDispatcher>(),
           eventAggregator, MockRepository.GenerateStub<ILoggerFacade>(),
            MockRepository.GenerateStub<IStarBurnWrapper>(), folderChooser,
            MockRepository.GenerateStub<IMeasuredCalcEngineService>(), MockRepository.GenerateStub<IIdealsCalcEngineService>(),
            MockRepository.GenerateStub<ISettingsManager>(), MockRepository.GenerateStub<IMessageManager>(),
            MockRepository.GenerateStub<IExcelReportGenerator>(), zipFileFactory)
        {
            SelectedItems = new Collection<object>();
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        protected override void ExportTask(List<BVATestItem> selectedBvaTestItems, string tempPath, string fileName, bool isOpticalDrive, string zipFilePath, System.Threading.CancellationTokenSource tokenSource)
        {
            CreateZipFile(FilePath, FileName);
        }
    }
}
