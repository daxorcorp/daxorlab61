using System.Collections.ObjectModel;
using Daxor.Lab.MessageBox;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_export_button_is_clicked
    {
        [TestMethod]
        public void And_there_are_no_valid_items_then_the_folder_chooser_isnt_displayed()
        {
            var mockFolderChooser = MockRepository.GenerateStrictMock<IFolderChooser>();
            var exportViewModel = new ExportViewModel(null, null, null, null, null, mockFolderChooser, null, null, null, null, null, null)
            {
                SelectedItems = null
            };

            exportViewModel.ExportCommand.Execute(null);

            mockFolderChooser.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_user_does_not_select_a_location_then_no_further_export_action_occurs()
        {
            var stubFolderChooser = MockRepository.GenerateStub<IFolderChooser>();
            stubFolderChooser.Expect(x => x.GetFolderChoice()).Return(null);

            var mockEventAggregator = MockRepository.GenerateStrictMock<IEventAggregator>();
            var mockMessageManager = MockRepository.GenerateStrictMock<IMessageManager>();

            var dummySelectedItems = new Collection<object>();
            var exportViewModel = new ExportViewModel(null, null, mockEventAggregator, null, null, stubFolderChooser,
                null, null, null, mockMessageManager, null, null)
            {
                SelectedItems = dummySelectedItems
            };

            exportViewModel.ExportCommand.Execute(dummySelectedItems);

            // These dependencies should never be used by the method under test
            mockEventAggregator.VerifyAllExpectations();
            mockMessageManager.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
