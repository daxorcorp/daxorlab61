using System.Collections.ObjectModel;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_there_are_no_selected_tests
    {
        [TestMethod]
        public void Then_the_export_button_is_disabled()
        {
            var exportViewModel = new ExportViewModel(null, null, null, null, null, null, null, null, null, null, null, null)
            {
                SelectedItems = new Collection<object>()
            };

            Assert.IsFalse(exportViewModel.ExportCommand.CanExecute(null)); 
        }
    }
    // ReSharper restore InconsistentNaming
}
