using System;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageBox;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests.Stubs;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_performing_the_export_task_and_exporting_to_optical_media
    {
        public static IFolderChooser _stubFolderChooser;
        public static IEventAggregator _stubEventAggregator;
        public static IMessageBoxDispatcher _messageBoxDispatcher;
        public static ISettingsManager _stubSettingsManager;

        [ClassInitialize]
        public static void TestsClassInitialize(TestContext context)
        {
            _stubFolderChooser = MockRepository.GenerateStub<IFolderChooser>();
            _stubFolderChooser.Expect(x => x.GetFolderChoice()).Return(new FolderChoice { Path = @"C:\temp\", IsOpticalDrive = true});

            _stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            _stubEventAggregator.Expect(a => a.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());

            _messageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            _messageBoxDispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Error,
                DataBurnerThatFailsToBurn.ExceptionMesasage, ExportViewModel.ExportDiscErrorCaption, MessageBoxChoiceSet.Close)).Return(0);

            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _stubSettingsManager.Expect(x => x.GetSetting<string>(SettingKeys.SystemTempFilePath)).Return(@"C:\temp\");
        }

        [TestMethod]
        public void And_the_drive_fails_then_the_export_fails()
        {
            var stubStarburnWrapper = MockRepository.GenerateStub<IStarBurnWrapper>();
            stubStarburnWrapper.Expect(x => x.CreateDataBurner()).Throw(new Exception());

            var exportViewModel = new ExportViewModelWithMockHelperMethods(_stubFolderChooser, _stubEventAggregator, 
                stubStarburnWrapper, _messageBoxDispatcher, _stubSettingsManager);

            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            Assert.IsTrue(exportViewModel.DidTaskFault);
        }

        [TestMethod]
        public void And_the_burn_fails_then_a_message_is_displayed()
        {
            var stubStarburnWrapper = MockRepository.GenerateStub<IStarBurnWrapper>();
            stubStarburnWrapper.Expect(x => x.CreateDataBurner()).Return(new DataBurnerThatFailsToBurn());

            var mockMessageDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMessageDispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Error,
                DataBurnerThatFailsToBurn.ExceptionMesasage, ExportViewModel.ExportDiscErrorCaption, MessageBoxChoiceSet.Close)).Return(0);

            var exportViewModel = new ExportViewModelWithMockHelperMethods(_stubFolderChooser, _stubEventAggregator,
                stubStarburnWrapper, mockMessageDispatcher, _stubSettingsManager);

            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            mockMessageDispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_burn_fails_then_the_error_is_logged()
        {
            var stubStarburnWrapper = MockRepository.GenerateStub<IStarBurnWrapper>();
            stubStarburnWrapper.Expect(x => x.CreateDataBurner()).Return(new DataBurnerThatFailsToBurn());

            var mockLogger = MockRepository.GenerateMock<ILoggerFacade>();
            mockLogger.Expect(x => x.Log(DataBurnerThatFailsToBurn.ExceptionMesasage, Category.Exception, Priority.High));

            var exportViewModel = new ExportViewModelWithMockHelperMethods(_stubFolderChooser, _stubEventAggregator,
                stubStarburnWrapper, mockLogger, _messageBoxDispatcher, _stubSettingsManager);

            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            mockLogger.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_burn_fails_then_the_busy_indicator_is_collapsed()
        {
            var stubStarburnWrapper = MockRepository.GenerateStub<IStarBurnWrapper>();
            stubStarburnWrapper.Expect(x => x.CreateDataBurner()).Return(new DataBurnerThatFailsToBurn());

            var mockEventAggregator = MockRepository.GenerateMock<IEventAggregator>();
            mockEventAggregator.Expect(
                e => e.GetEvent<BusyStateChanged>().Publish(Arg<BusyPayload>.Matches(x => !x.IsBusy)));

            var exportViewModel = new ExportViewModelWithMockHelperMethods(_stubFolderChooser, mockEventAggregator,
                stubStarburnWrapper, _stubSettingsManager);

            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            mockEventAggregator.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
