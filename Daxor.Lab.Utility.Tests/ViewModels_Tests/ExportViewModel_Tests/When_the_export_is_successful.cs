using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageBox;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests.Stubs;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_export_is_successful
    {
        public static IFolderChooser _stubFolderChooser;
        public static Message _message;

        // Code that should be executed before running the first test in this class.
        [ClassInitialize]
        public static void TestsClassInitialize(TestContext context)
        {
            _stubFolderChooser = MockRepository.GenerateStub<IFolderChooser>();
            _stubFolderChooser.Expect(x => x.GetFolderChoice()).Return(new FolderChoice { Path = @"C:\temp\" });

            _message = new Message("key", "description", "resolution", "messageId", MessageType.Error);
        }

        [TestMethod]
        public void Then_a_message_is_displayed()
        {
            var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            stubEventAggregator.Expect(a => a.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());
            
            var mockMessageBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMessageBoxDispatcher.Expect(
                x => x.ShowMessageBox(MessageBoxCategory.Information, ExportViewModel.SuccessfulExportMessage,
                    ExportViewModel.ExportCaption, MessageBoxChoiceSet.Close)).Return(0);

            var exportViewModel = new ExportViewModelThatSuccessfullyExports(_stubFolderChooser, stubEventAggregator,
                mockMessageBoxDispatcher);
            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            mockMessageBoxDispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_the_busy_indicator_is_disabled()
        {
            var stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();

            var mockEventAggregator = MockRepository.GenerateMock<IEventAggregator>();
            mockEventAggregator.Expect(
                e => e.GetEvent<BusyStateChanged>().Publish(Arg<BusyPayload>.Matches(x => !x.IsBusy)));

            var exportViewModel = new ExportViewModelThatSuccessfullyExports(_stubFolderChooser, mockEventAggregator,
                stubMessageBoxDispatcher);

            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            mockEventAggregator.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_the_flag_for_exporting_all_tests_is_cleared()
        {
            var stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            stubEventAggregator.Expect(a => a.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());

            var exportViewModel = new ExportViewModelThatSuccessfullyExports(_stubFolderChooser, stubEventAggregator,
                 stubMessageBoxDispatcher);

            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            Assert.IsFalse(exportViewModel.ExportAllTests);
        }

        [TestMethod]
        public void Then_the_zip_folder_is_created_with_the_xml_file_in_the_root_directory()
        {
            const string xmlFileName = ExportViewModelThatSuccessfullyExportsToZipFile.FullFileName + ".xml";

            var stubFolderChooser = MockRepository.GenerateStub<IFolderChooser>();
            stubFolderChooser.Expect(x => x.GetFolderChoice()).Return(new FolderChoice{IsOpticalDrive = false, Path = @"C:\temp\"});

            var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            stubEventAggregator.Expect(x => x.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());

            var mockZipFileWrapper = MockRepository.GenerateMock<IIonicZipWrapper>();
            mockZipFileWrapper.Expect(x => x.AddFileToRoot(xmlFileName));

            var stubZipFileFactory = MockRepository.GenerateStub<IZipFileFactory>();
            stubZipFileFactory.Expect(x => x.CreateZipFileWithPassword(null))
                .IgnoreArguments()
                .Return(mockZipFileWrapper);

            var exportViewModel = new ExportViewModelThatSuccessfullyExportsToZipFile(stubFolderChooser,
                stubEventAggregator, stubZipFileFactory);

            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            mockZipFileWrapper.VerifyAllExpectations();
        }

    }
    // ReSharper restore InconsistentNaming
}
