using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.MessageBox;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests.Stubs;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_user_selects_a_valid_export_path
    {
        private IFolderChooser _stubFolderChooser;
        private Message _message;

        // Code that should be executed before running the first test in this class.
        [TestInitialize]
        public void BeforeEachTest()
        {
			_stubFolderChooser = Substitute.For<IFolderChooser>();
			_stubFolderChooser.GetFolderChoice().Returns(new FolderChoice { Path = "c:\\temp\\" });

            _message = new Message("key", "description", "resolution", "messageId", MessageType.Error);
        }

        [TestMethod]
        public void Then_the_busy_indicator_is_displayed()
        {
            var mockEventAggregator = MockRepository.GenerateMock<IEventAggregator>();
            mockEventAggregator.Expect(e => e.GetEvent<BusyStateChanged>().Publish(Arg<BusyPayload>.Matches(x => x.IsBusy)));

			var exportViewModel = new ExportViewModelWithEmptyTaskMethods(_stubFolderChooser, mockEventAggregator);
            
            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            mockEventAggregator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_burning_to_an_optical_drive_then_the_export_task_is_invoked_correctly()
        {
            var folderChoice = new FolderChoice {IsOpticalDrive = true, Path = @"C:\temp\"};
            
            _stubFolderChooser = MockRepository.GenerateStub<IFolderChooser>();
            _stubFolderChooser.Expect(x => x.GetFolderChoice()).Return(folderChoice);

            var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            stubEventAggregator.Expect(a => a.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());

            var exportViewModel = new ExportViewModelWithEmptyWriteMethods(_stubFolderChooser, stubEventAggregator);

            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            Assert.IsTrue(exportViewModel.WasBurnedToOpticalMedia);
        }

        [TestMethod]
        public void And_writing_to_a_non_optical_drive_then_the_export_task_is_invoked_correctly()
        {
            var folderChoice = new FolderChoice { IsOpticalDrive = false, Path = @"C:\temp\" };

            _stubFolderChooser = MockRepository.GenerateStub<IFolderChooser>();
            _stubFolderChooser.Expect(x => x.GetFolderChoice()).Return(folderChoice);

            var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            stubEventAggregator.Expect(a => a.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());

            var exportViewModel = new ExportViewModelWithEmptyWriteMethods(_stubFolderChooser, stubEventAggregator);

            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            Assert.IsTrue(exportViewModel.WasWrittenToNonOpticalMedia);
        }
    }
    // ReSharper restore InconsistentNaming
}
