using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.MessageBox;
using Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests.Stubs;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_performing_the_export_task
    {
        public static IFolderChooser _stubFolderChooser;
        public static IEventAggregator _stubEventAggregator;
        public static ExportViewModelWithMockHelperMethods _exportViewModel;

        [ClassInitialize]
        public static void TestsClassInitialize(TestContext context)
        {
            _stubFolderChooser = MockRepository.GenerateStub<IFolderChooser>();
            _stubFolderChooser.Expect(x => x.GetFolderChoice()).Return(new FolderChoice { Path = @"C:\temp\" });

            _stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            _stubEventAggregator.Expect(a => a.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _exportViewModel = new ExportViewModelWithMockHelperMethods(_stubFolderChooser, _stubEventAggregator);
        }

        [TestMethod]
        public void Then_the_excel_report_is_built_and_saved()
        {
            _exportViewModel.ExportCommand.Execute(_exportViewModel.SelectedItems);

            Assert.IsTrue(_exportViewModel.DidBuildAndSave);
        }

        [TestMethod]
        public void Then_the_zip_file_is_created()
        {
            _exportViewModel.ExportCommand.Execute(_exportViewModel.SelectedItems);

            Assert.IsTrue(_exportViewModel.DidCreateZipFile);
        }

        [TestMethod]
        public void And_exporting_to_non_optical_media_then_the_content_is_saved()
        {
            _exportViewModel.ExportCommand.Execute(_exportViewModel.SelectedItems);

            Assert.IsTrue(_exportViewModel.DidSaveToNonOpticalMedia);
        }

        [TestMethod]
        public void And_the_files_have_been_written_then_temp_files_are_cleaned_up()
        {
            _exportViewModel.ExportCommand.Execute(_exportViewModel.SelectedItems);

            Assert.IsTrue(_exportViewModel.DidCleanUpTempFiles);
        }
    }
    // ReSharper restore InconsistentNaming
}
