using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageBox;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests.Stubs;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_export_task_fails
    {
        public static IFolderChooser _stubFolderChooser;
        public static Message _message;

        // Code that should be executed before running the first test in this class.
        [ClassInitialize]
        public static void TestsClassInitialize(TestContext context)
        {
            _stubFolderChooser = MockRepository.GenerateStub<IFolderChooser>();
            _stubFolderChooser.Expect(x => x.GetFolderChoice()).Return(new FolderChoice { Path = @"C:\temp\" });

            _message = new Message("key", "description", "resolution", "messageId", MessageType.Error);
        }
        
        [TestMethod]
        public void Then_a_message_is_displayed()
        {
            var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            stubEventAggregator.Expect(a => a.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());
            var stubLogger = MockRepository.GenerateStub<ILoggerFacade>();

            var mockMessageBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMessageBoxDispatcher.Expect(
                x => x.ShowMessageBox(MessageBoxCategory.Error, ExportViewModelThatFailsToExport.ExportExceptionMessage, 
                    ExportViewModel.ExportCaption, MessageBoxChoiceSet.Close))
                .Return(0);

            var exportViewModel = new ExportViewModelThatFailsToExport(_stubFolderChooser, stubEventAggregator,
                stubLogger, mockMessageBoxDispatcher);

            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            mockMessageBoxDispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_an_error_is_logged()
        {
            var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            stubEventAggregator.Expect(a => a.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());
            
            var stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();

            var mockLogger = MockRepository.GenerateMock<ILoggerFacade>();
            mockLogger.Expect(x => x.Log(ExportViewModelThatFailsToExport.ExportExceptionMessage, Category.Exception, Priority.High));

            var exportViewModel = new ExportViewModelThatFailsToExport(_stubFolderChooser, stubEventAggregator,
                mockLogger, stubMessageBoxDispatcher);

            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            mockLogger.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_the_busy_indicator_is_disabled()
        {
            var stubLogger = MockRepository.GenerateStub<ILoggerFacade>();

            var stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();

            var mockEventAggregator = MockRepository.GenerateMock<IEventAggregator>();
            mockEventAggregator.Expect(e => e.GetEvent<BusyStateChanged>().Publish(Arg<BusyPayload>.Matches(x => !x.IsBusy)));

            var exportViewModel = new ExportViewModelThatFailsToExport(_stubFolderChooser, mockEventAggregator,
                stubLogger, stubMessageBoxDispatcher);

            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            mockEventAggregator.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_the_flag_for_exporting_all_tests_is_cleared()
        {
            var stubLogger = MockRepository.GenerateStub<ILoggerFacade>();
            var stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            stubEventAggregator.Expect(a => a.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());

            var exportViewModel = new ExportViewModelThatFailsToExport(_stubFolderChooser, stubEventAggregator,
                stubLogger, stubMessageBoxDispatcher);

            exportViewModel.ExportCommand.Execute(exportViewModel.SelectedItems);

            Assert.IsFalse(exportViewModel.ExportAllTests);
        }
    }
    // ReSharper restore InconsistentNaming
}
