using System;
using System.Collections.ObjectModel;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageBox;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ExportViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_user_selects_an_invalid_path
    {
        public static IFolderChooser _stubFolderChooser;
        public static Message _message;

        // Code that should be executed before running the first test in this class.
        [ClassInitialize]
        public static void TestsClassInitialize(TestContext context)
        {
            _stubFolderChooser = MockRepository.GenerateStub<IFolderChooser>();
            _stubFolderChooser.Expect(x => x.GetFolderChoice()).Return(new FolderChoice { Path = String.Empty });

            _message = new Message("key", "description", "resolution", "messageId", MessageType.Error);
        }
        
        [TestMethod]
        public void Then_a_message_is_displayed()
        {
            var mockMessageManager = MockRepository.GenerateMock<IMessageManager>();
            mockMessageManager.Expect(x => x.GetMessage(MessageKeys.UtilityExportPathInvalid))
                .Return(_message);

            var mockMessageBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMessageBoxDispatcher.Expect(
                x =>
                    x.ShowMessageBox(MessageBoxCategory.Error, _message.FormattedMessage, "Export BVA Tests", MessageBoxChoiceSet.Close)).Return(0);

            var stubLogger = MockRepository.GenerateStub<ILoggerFacade>();

            var dummySelectedItems = new Collection<object>();
            var exportViewModel = new ExportViewModel(null, mockMessageBoxDispatcher, null, stubLogger, null, _stubFolderChooser,
                null, null, null, mockMessageManager, null, null)
            {
                SelectedItems = dummySelectedItems
            };

            exportViewModel.ExportCommand.Execute(dummySelectedItems);

            // These dependencies should never be used by the method under test
            mockMessageBoxDispatcher.VerifyAllExpectations();
            mockMessageManager.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_an_error_is_logged()
        {
            var stubMessageManager = MockRepository.GenerateStub<IMessageManager>();
            stubMessageManager.Expect(x => x.GetMessage(MessageKeys.UtilityExportPathInvalid))
                .Return(_message);

            var stubMessageBoxDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();

            var mockLogger = MockRepository.GenerateMock<ILoggerFacade>();
            mockLogger.Expect(x => x.Log(_message.FormattedMessage, Category.Info, Priority.Low));

            var dummySelectedItems = new Collection<object>();
            var exportViewModel = new ExportViewModel(null, stubMessageBoxDispatcher, null, mockLogger, null, _stubFolderChooser,
                null, null, null, stubMessageManager, null, null)
            {
                SelectedItems = dummySelectedItems
            };

            exportViewModel.ExportCommand.Execute(dummySelectedItems);

            mockLogger.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
