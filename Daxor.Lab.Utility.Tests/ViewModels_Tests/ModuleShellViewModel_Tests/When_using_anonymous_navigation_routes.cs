﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Rhino.Mocks.Constraints;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ModuleShellViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_anonymous_navigation_routes
    {
        private IEnumerable<NavigationRoute> _anonymousRoutes;

        [TestInitialize]
        public void Initialize()
        {
           
            var viewModel = new ModuleShellViewModelThatExposesAnonymousRoutes();

            _anonymousRoutes = viewModel.GetAnonymousRoutes();
        }

        [TestMethod]
        public void Then_home_is_the_first_route()
        {
            var firstRoute = _anonymousRoutes.First();

            Assert.AreEqual(ModuleShellViewModel.HomeDescription, firstRoute.Description);
            Assert.AreEqual(ModuleShellViewModel.HomeViewId, firstRoute.NavigateTo);
        }

        [TestMethod]
        public void Then_calibrate_touch_screen_is_the_second_route()
        {
            var secondRoute = _anonymousRoutes.Skip(1).First();

            Assert.AreEqual(ModuleShellViewModel.CalibrateScreenDescription,secondRoute.Description);
            Assert.AreEqual(ModuleShellViewModel.CalibrateViewId, secondRoute.NavigateTo);
        }

        [TestMethod]
        public void Then_reset_printer_is_the_third_route()
        {
            var thirdRoute = _anonymousRoutes.Skip(2).First();

            Assert.AreEqual(ModuleShellViewModel.ResetPrinterDescription, thirdRoute.Description);
            Assert.AreEqual(ModuleShellViewModel.ResetPrinterViewId, thirdRoute.NavigateTo);
        }

        [TestMethod]
        public void Then_backup_is_the_fourth_route()
        {
            var fourthRoute = _anonymousRoutes.Skip(3).First();

            Assert.AreEqual(ModuleShellViewModel.ManualBackupDescription, fourthRoute.Description);
            Assert.AreEqual(ModuleShellViewModel.ManualBackupViewId, fourthRoute.NavigateTo);
        }

        [TestMethod]
        public void And_qc_archives_is_enabled_then_qc_archives_is_the_sixth_route()
        {
            var stubSettingsmanager = Substitute.For<ISettingsManager>();
            stubSettingsmanager.GetSetting<bool>(SettingKeys.SystemShowQcArchivesButton).Returns(true);

            var viewModel = new ModuleShellViewModelThatExposesAnonymousRoutes(stubSettingsmanager);

            _anonymousRoutes = viewModel.GetAnonymousRoutes();
            var sixthRoute = _anonymousRoutes.LastOrDefault();

            Assert.IsNotNull(sixthRoute);
            Assert.AreEqual(ModuleShellViewModel.QcArchivesDescription, sixthRoute.Description);
            Assert.AreEqual(ModuleShellViewModel.QcArchiveViewId, sixthRoute.NavigateTo);
        }

        [TestMethod]
        public void And_qc_archives_is_disabled_then_qc_archives_is_not_in_the_route_list()
        {
            var stubSettingsmanager = Substitute.For<ISettingsManager>();
            stubSettingsmanager.GetSetting<bool>(SettingKeys.SystemShowQcArchivesButton).Returns(false);

            var viewModel = new ModuleShellViewModelThatExposesAnonymousRoutes(stubSettingsmanager);

            _anonymousRoutes = viewModel.GetAnonymousRoutes();
            
            Assert.IsFalse(_anonymousRoutes.Any(r=>r.Description == ModuleShellViewModel.QcArchivesDescription));
        }

        [TestMethod]
        public void And_ordering_physicians_report_is_enabled_then_ordering_physicians_report_is_the_fifth_route()
        {
            var stubSettingsmanager = Substitute.For<ISettingsManager>();
            stubSettingsmanager.GetSetting<bool>(SettingKeys.SystemShowOrderingPhysiciansReportButton).Returns(true);

            var viewModel = new ModuleShellViewModelThatExposesAnonymousRoutes(stubSettingsmanager);

            _anonymousRoutes = viewModel.GetAnonymousRoutes();
            var fifthRoute = _anonymousRoutes.Skip(4).First();

            Assert.AreEqual(ModuleShellViewModel.OrderingPhysiciansReportsDescription, fifthRoute.Description);
            Assert.AreEqual(ModuleShellViewModel.OrderingPhysiciansReportsViewId, fifthRoute.NavigateTo);
        }

        [TestMethod]
        public void And_ordering_physicians_report_is_disabled_then_ordering_physicians_report_is_not_in_the_route_list()
        {
            var stubSettingsmanager = Substitute.For<ISettingsManager>();
            stubSettingsmanager.GetSetting<bool>(SettingKeys.SystemShowOrderingPhysiciansReportButton).Returns(false);

            var viewModel = new ModuleShellViewModelThatExposesAnonymousRoutes(stubSettingsmanager);

            _anonymousRoutes = viewModel.GetAnonymousRoutes();

            Assert.IsFalse(_anonymousRoutes.Any(r => r.Description == ModuleShellViewModel.OrderingPhysiciansReportsDescription));
        }
    }

    // ReSharper restore InconsistentNaming

}
