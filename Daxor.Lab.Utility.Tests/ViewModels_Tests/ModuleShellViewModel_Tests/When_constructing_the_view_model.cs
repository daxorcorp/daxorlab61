﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ModuleShellViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_the_view_model
    {
        [TestMethod]
        public void Then_the_anonymous_navigation_routes_are_the_default()
        {
            var viewModel = new ModuleShellViewModelThatExposesAnonymousRoutes();

            var expectedRoutes = viewModel.GetAnonymousRoutes().ToArray();
            var actualRoutes = viewModel.NavigationRoutes.ToArray();

            for (var index = 0; index < expectedRoutes.Count(); index++)
            {
                Assert.AreEqual(expectedRoutes[index].Description, actualRoutes[index].Description);
                Assert.AreEqual(expectedRoutes[index].NavigateTo, actualRoutes[index].NavigateTo);
            }
        }
    }
    // ReSharper restore InconsistentNaming
}
