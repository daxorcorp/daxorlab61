using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.ViewModels;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ModuleShellViewModel_Tests
{
    internal class ModuleShellViewModelThatExposesAnonymousRoutes : ModuleShellViewModel
    {

        internal ModuleShellViewModelThatExposesAnonymousRoutes() :base (null, Substitute.For<IAuthenticationManager>(),null, null, null, Substitute.For<ISettingsManager>())
        {
            
        }

        internal ModuleShellViewModelThatExposesAnonymousRoutes(ISettingsManager settingsManager)
            : base(
                null, Substitute.For<IAuthenticationManager>(), null, null, null, settingsManager)
        {
            
        }

        internal IEnumerable<NavigationRoute> GetAnonymousRoutes()
        {
            return GetAnonymousNavigationRoutes();
        }
    }
}