using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ModuleShellViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_route_names
    {
        [TestMethod]
        [RequirementValue("Calibrate Screen")]
        public void Then_the_calibration_screen_route_description_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), ModuleShellViewModel.CalibrateScreenDescription);
        }

        [TestMethod]
        [RequirementValue("Reset Printer")]
        public void Then_the_reset_printer_route_description_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), ModuleShellViewModel.ResetPrinterDescription);
        }

        [TestMethod]
        [RequirementValue("Manual Backup")]
        public void Then_the_manual_backup_route_description_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), ModuleShellViewModel.ManualBackupDescription);
        }

        [TestMethod]
        [RequirementValue("QC Archives")]
        public void Then_the_QC_archives_route_description_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), ModuleShellViewModel.QcArchivesDescription);
        }

        [TestMethod]
        [RequirementValue("Ordering Physicians Report")]
        public void Then_the_ordering_physicians_report_route_description_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), ModuleShellViewModel.OrderingPhysiciansReportsDescription);
        }

        [TestMethod]
        [RequirementValue("Export Results\nto Excel")]
        public void Then_the_Excel_export_route_description_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), ModuleShellViewModel.ExportResultsToExcelDescription);
        }

        [TestMethod]
        [RequirementValue("Home")]
        public void Then_the_home_route_description_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), ModuleShellViewModel.HomeDescription);
        }

        [TestMethod]
        [RequirementValue("Settings")]
        public void Then_the_settings_route_description_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), ModuleShellViewModel.SettingsDescription);
        }

        [TestMethod]
        [RequirementValue("Date & Time")]
        public void Then_the_date_time_route_description_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), ModuleShellViewModel.DateAndTimeDescription);
        }

        [TestMethod]
        [RequirementValue("Database")]
        public void Then_the_database_route_description_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), ModuleShellViewModel.DatabaseDescription);
        }

        [TestMethod]
        [RequirementValue("Services")]
        public void Then_the_services_route_description_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), ModuleShellViewModel.ServicesDescription);
        }
    }
    // ReSharper restore InconsistentNaming
}
