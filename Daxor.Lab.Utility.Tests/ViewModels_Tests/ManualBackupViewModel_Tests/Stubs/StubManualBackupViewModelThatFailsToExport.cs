﻿using System;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Logging;
using NSubstitute;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ManualBackupViewModel_Tests.Stubs
{
    public class StubManualBackupViewModelThatFailsToExport : ManualBackupViewModel
    {
        public const string BackupExceptionMessage = "doesn't work";
        
        public StubManualBackupViewModelThatFailsToExport(IMessageManager messageManager) :
            base(MockRepository.GenerateStub<IMessageBoxDispatcher>(),
                 MockRepository.GenerateStub<ILoggerFacade>(),
                 MockRepository.GenerateStub<ISettingsManager>(),
                 MockRepository.GenerateStub<IStarBurnWrapper>(),
                 messageManager, MockRepository.GenerateStub<IBackupServiceFactory>(), Substitute.For<IZipFileFactory>())
        {
            IsOpticalDrive = true;
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public StubManualBackupViewModelThatFailsToExport(IMessageManager messageManager, ILoggerFacade logger) :
            base(MockRepository.GenerateStub<IMessageBoxDispatcher>(), 
                 logger,
                 MockRepository.GenerateStub<ISettingsManager>(), 
                 MockRepository.GenerateStub<IStarBurnWrapper>(),
                 messageManager, MockRepository.GenerateStub<IBackupServiceFactory>(), Substitute.For<IZipFileFactory>())
        {
            IsOpticalDrive = true;
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public StubManualBackupViewModelThatFailsToExport(IMessageManager messageManager, IMessageBoxDispatcher msgBoxDispatcher) :
            base(msgBoxDispatcher,
                 MockRepository.GenerateStub<ILoggerFacade>(),
                 MockRepository.GenerateStub<ISettingsManager>(),
                 MockRepository.GenerateStub<IStarBurnWrapper>(),
                 messageManager, MockRepository.GenerateStub<IBackupServiceFactory>(), Substitute.For<IZipFileFactory>())
        {
            IsOpticalDrive = true;
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public bool DidDeleteTempFiles { get; protected set; }

        protected override void BackupDatabasesTask()
        {
            throw new Exception(BackupExceptionMessage);
        }

        protected override void CreateZipFile(string tempDaxorPath, string tempQcArchivePath, string tempDaxorManualBackupFilePath)
        {
        }

        protected override void DeleteTempFiles()
        {
            DidDeleteTempFiles = true;
        }
    }
}
