﻿using System;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Logging;
using NSubstitute;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ManualBackupViewModel_Tests.Stubs
{
    public class StubManualBackupViewModel : ManualBackupViewModel
    {
        public const string FailureMessage = "8-ton elephants";

        public StubManualBackupViewModel(IStarBurnWrapper starBurnWrapper) :
            base(MockRepository.GenerateStub<IMessageBoxDispatcher>(),
                 MockRepository.GenerateStub<ILoggerFacade>(),
                 MockRepository.GenerateStub<ISettingsManager>(),
                 starBurnWrapper,
                 MockRepository.GenerateStub<IMessageManager>(), MockRepository.GenerateStub<IBackupServiceFactory>(), Substitute.For<IZipFileFactory>())
        {
            IsOpticalDrive = true;
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public bool FailedWithCorrectMessage { get; protected set; }

        protected override void BackupDatabasesTask()
        {
            // Completes successfully by doing nothing
        }

        protected override void CreateZipFile(string tempDaxorPath, string tempQcArchivePath, string tempDaxorManualBackupFilePath)
        {
        }

        protected override void DeleteTempFiles()
        {
        }

        protected override void OnBackupDatabaseTaskFaulted(Exception faultingException)
        {
            FailedWithCorrectMessage = faultingException.InnerException.Message == FailureMessage;
        }
    }

}
