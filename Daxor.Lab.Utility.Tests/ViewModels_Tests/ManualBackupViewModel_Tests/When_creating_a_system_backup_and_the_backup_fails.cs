using System.Windows;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.Utility.Tests.ViewModels_Tests.ManualBackupViewModel_Tests.Stubs;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ManualBackupViewModel_Tests
{
    // ReSharper disable once InconsistentNaming
    [TestClass]
    public class When_creating_a_system_backup_and_the_backup_fails
    {
        [TestMethod]
        public void Then_the_failure_reason_is_logged()
        {
            var stubMessageManager = MockRepository.GenerateStub<IMessageManager>();
            stubMessageManager.Expect(m => m.GetMessage("whatever"))
                .IgnoreArguments()
                .Return(new Message("key", "description", "resolution", "id", MessageType.Error));

            var mockLogger = MockRepository.GenerateMock<ILoggerFacade>();
            mockLogger.Expect(l =>
                    l.Log(StubManualBackupViewModelThatFailsToExport.BackupExceptionMessage, Category.Exception, Priority.High));

            var viewModel = new StubManualBackupViewModelThatFailsToExport(stubMessageManager, mockLogger);

            viewModel.BackupDatabaseCommand.Execute(null);

            mockLogger.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_the_user_is_shown_a_corresponding_message()
        {
            var stubMessage = new Message("key", "description", "resolution", "id", MessageType.Error);
            var stubMessageManager = MockRepository.GenerateStub<IMessageManager>();
            stubMessageManager.Expect(m => m.GetMessage("whatever"))
                .IgnoreArguments()
                .Return(stubMessage);

            var mockMsgBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMsgBoxDispatcher.Expect(d => d.ShowMessageBox(
                Arg<MessageBoxCategory>.Is.Equal(MessageBoxCategory.Error),
                Arg<string>.Is.Equal(stubMessage.FormattedMessage),
                Arg<string>.Is.Equal("Database Backup"),
                Arg<MessageBoxChoiceSet>.Is.Equal(MessageBoxChoiceSet.Close)));

            var viewModel = new StubManualBackupViewModelThatFailsToExport(stubMessageManager, mockMsgBoxDispatcher);

            viewModel.BackupDatabaseCommand.Execute(null);

            mockMsgBoxDispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_the_busy_indicator_is_disabled_and_the_visibility_mask_is_disabled()
        {
            var stubMessageManager = MockRepository.GenerateStub<IMessageManager>();
            stubMessageManager.Expect(m => m.GetMessage("whatever"))
                .IgnoreArguments()
                .Return(new Message("key", "description", "resolution", "id", MessageType.Error));

            var viewModel = new StubManualBackupViewModelThatFailsToExport(stubMessageManager);

            viewModel.BackupDatabaseCommand.Execute(null);

            Assert.AreEqual(Visibility.Collapsed, viewModel.MaskVisibility);
        }

        [TestMethod]
        public void Then_temp_files_are_cleaned_up()
        {
            var stubMessageManager = MockRepository.GenerateStub<IMessageManager>();
            stubMessageManager.Expect(m => m.GetMessage("whatever"))
                .IgnoreArguments()
                .Return(new Message("key", "description", "resolution", "id", MessageType.Error));

            var viewModel = new StubManualBackupViewModelThatFailsToExport(stubMessageManager);

            viewModel.BackupDatabaseCommand.Execute(null);

            Assert.IsTrue(viewModel.DidDeleteTempFiles);
        }
    }
}