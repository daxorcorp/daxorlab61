using System;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Utility.Tests.ViewModels_Tests.ManualBackupViewModel_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ManualBackupViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_saving_a_system_backup_to_an_optical_drive
    {
        // Note: There are other tests that ensure task failure workflows are correct 
        // (i.e., what happens when the task fails)
        [TestMethod]
        public void And_there_is_no_valid_drive_then_the_backup_task_fails()
        {
            var faultyStarBurnWrapper = MockRepository.GenerateStub<IStarBurnWrapper>();
            faultyStarBurnWrapper.Expect(w => w.CreateDataBurner()).Throw(new Exception(StubManualBackupViewModel.FailureMessage));

            var viewModel = new StubManualBackupViewModel(faultyStarBurnWrapper);

            viewModel.BackupDatabaseCommand.Execute(null);
        }
    }
    // ReSharper restore InconsistentNaming
}
