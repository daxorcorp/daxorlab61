using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.ViewModels_Tests.ManualBackupViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_a_system_backup
    {
        private const string SystemId = "unit_test_id";
        private const string ManualBackupFileName = "testing.bak";

        [TestMethod]
        public void Then_the_backup_filename_contains_the_system_ID_followed_by_dash_followed_by_a_common_filename()
        {
            // Arrange
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<string>(SettingKeys.SystemUniqueSystemId)).Return(SystemId);
            stubSettingsManager.Expect(m => m.GetSetting<string>(RuntimeSettingKeys.SystemManualBackupFileName)).Return(ManualBackupFileName);
            
            var backupDatabaseSubViewModel = new ManualBackupViewModel(null, null, stubSettingsManager, null, null, null, Substitute.For<IZipFileFactory>());
            
            // Act
            var observedBackupFileName = backupDatabaseSubViewModel.CreateBackupFileName();

            // Assert
            const string expectedBackupFileName = SystemId + "-" + ManualBackupFileName;
            Assert.AreEqual(expectedBackupFileName, observedBackupFileName);
        }
    }
    // ReSharper restore InconsistentNaming
}
