﻿using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Utility.Tests.Common_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_Excel_column_headers
    {
        [TestMethod]
        [RequirementValue("Analyst")]
        public void And_using_Analyst_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.Analyst.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Analyzed On")]
        public void And_using_AnalzyedOn_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.AnalyzedOn.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Last Name")]
        public void And_using_LastName_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.LastName.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("First Name")]
        public void And_using_FirstName_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.FirstName.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Middle Name")]
        public void And_using_MiddleName_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.MiddleName.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Date Of Birth")]
        public void And_using_DateOfBirth_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.DateOfBirth.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        /// <remarks>
        /// Since the location label can be customized, this is just testing that we can access the string attribute
        /// </remarks>
        [TestMethod]
        [RequirementValue("LocationLabel")]
        public void And_using_LocationLabel_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.LocationLabel.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Hospital ID")]
        public void And_using_HospitalId_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.HospitalId.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        /// <remarks>
        /// Since the TestID2 can be customzied, this is just testing that we can access the string attribute
        /// </remarks>
        [TestMethod]
        [RequirementValue("TestID2Label")]
        public void And_using_TestID2Label_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.TestId2Label.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Patient Sex")]
        public void And_using_PatientSex_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.PatientSex.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Patient Height (cm)")]
        public void And_using_PatientHeightInCm_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.PatientHeightInCm.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Patient Weight (kg)")]
        public void And_using_PatientWeightinKg_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.PatientWeightInKg.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Ideal Weight Deviation (%)")]
        public void And_using_IdealWeightDeviation_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.IdealWeightDeviation.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Blood Volume (mL)")]
        public void And_using_BloodVolume_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.BloodVolume.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Ideal Blood Volume (mL)")]
        public void And_using_IdealBloodVolume_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.IdealBloodVolume.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("BV Deviation (mL)")]
        public void And_using_BloodVolumeDeviation_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.BloodVolumeDeviation.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("BV Excess/Deficit (%)")]
        public void And_using_BloodVolumeExcessAndDeficit_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.BloodVolumeExcessAndDeficit.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Plasma Volume (mL)")]
        public void And_using_PlasmaVolume_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.PlasmaVolume.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Ideal Plasma Volume (mL)")]
        public void And_using_IdealPlasmaVolume_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.IdealPlasmaVolume.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("PV Deviation (mL)")]
        public void And_using_PlasmaVolumeDeviation_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.PlasmaVolumeDeviation.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("PV Excess/Deficit (%)")]
        public void And_using_PlasmaVolumeExcessAndDeficit_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.PlasmaVolumeExcessAndDeficit.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("RBC Volume (mL)")]
        public void And_using_RedBloodCellVolume_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.RedBloodCellVolume.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Ideal RBC Volume (mL)")]
        public void And_using_IdealRedBloodCellVolume_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.IdealRedBloodCellVolume.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("RBC Deviation (mL)")]
        public void And_using_RedBloodCellDeviation_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.RedBloodCellDeviation.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("RBC Excess/Deficit (%)")]
        public void And_using_RedBloodCellExcessAndDeficit_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.RedBloodCellExcessAndDeficit.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Slope (%/min)")]
        public void And_using_Slope_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.Slope.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("StdDev (%)")]
        public void And_using_StandardDeviation_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.StandardDeviation.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Normal HCT (%)")]
        public void And_using_NormalHct_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.NormalHct.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Peripheral HCT (%)")]
        public void And_using_PeripheralHct_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.PeripheralHct.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Referring MD")]
        public void And_using_ReferringMd_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.ReferringMd.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("CC")]
        public void And_using_CC_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.CC.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Injectate Lot")]
        public void And_using_InjectateLot_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.InjectateLot.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }

        [TestMethod]
        [RequirementValue("Comments")]
        public void And_using_Comments_then_the_string_attribute_matches_requirements()
        {
            var actualHeaderName = BvaExcelColumnHeader.Comments.GetStringValue();

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), actualHeaderName);
        }
    }
    // ReSharper restore InconsistentNaming
}
