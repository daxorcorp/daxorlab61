﻿using System;
using Daxor.Lab.Utility.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Utility.Tests.Common_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_DateTimeConverter
    {
        private DateTimeConverter _dateTimeConverter;

        [TestInitialize]
        public void Initialize()
        {
            _dateTimeConverter = new DateTimeConverter();
        }

        [TestMethod]
        public void Then_a_date_with_time_is_converted_to_the_corresponding_DateTime_representation()
        {
            var validDate = new DateTime(2014,11,24,5,12,32);

            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            var observedDate = _dateTimeConverter.Convert(validDate.ToString(), typeof (DateTime), null, null);

            Assert.AreEqual(validDate, observedDate);
        }

        [TestMethod]
        public void Then_a_DateTime_is_converted_to_the_corresponding_string_representation_of_the_date_with_time()
        {
            var validDate = new DateTime(2014, 11, 24, 5, 12, 32);

            var observedDate = _dateTimeConverter.ConvertBack(validDate, typeof(DateTime), null,null);

            // ReSharper disable once SpecifyACultureInStringConversionExplicitly
            Assert.AreEqual(validDate.ToString(), observedDate);
        }
    }
    // ReSharper restore InconsistentNaming

}
