﻿using System;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Common;
using Ionic.Zip;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Common_Tests.DaxorLabRestore_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_temporary_file_restore_path_for_the_zip_file
    {
        private const string DaxorBackupFileName = "DaxorSuite";
        private const string MssqlBackupExtension = ".bak";
        private const string ZipFileExtension = ".zip";
        private const string BackupsLocationInZip = @"Backups\";
        private const string NotTheDaxorSuiteBackup = "QCDatabaseArchive";
        private const string TemporaryFileLocation = @"C:\temp";

        private IMessageBoxDispatcher _stubMessageBoxDispatcher;
        private ILoggerFacade _stubLoggerFacade;
        private ISettingsManager _stubSettingsManager;
        private IMessageManager _stubMessageManager;
        private IZipFileFactory _stubZipFileFactory;
        private IIonicZipWrapper _stubIonicZipWrapper;
        private DaxorLabRestore _daxorLabRestore;

        [TestInitialize]
        public void Initialize()
        {
            _stubMessageBoxDispatcher = Substitute.For<IMessageBoxDispatcher>();
            _stubMessageManager = Substitute.For<IMessageManager>();
            _stubLoggerFacade = Substitute.For<ILoggerFacade>();
            _stubSettingsManager = Substitute.For<ISettingsManager>();
            _stubZipFileFactory = Substitute.For<IZipFileFactory>();
            _stubIonicZipWrapper = Substitute.For<IIonicZipWrapper>();
            _stubZipFileFactory.CreateZipFileWithPassword(Arg.Any<string>()).Returns(_stubIonicZipWrapper);

            _stubSettingsManager.GetSetting<string>(RuntimeSettingKeys.SystemDaxorLabDatabaseBackupFilename)
                .Returns(DaxorBackupFileName);

            _stubSettingsManager.GetSetting<String>(SettingKeys.SystemTempFilePath).Returns(TemporaryFileLocation);
        }

        [TestMethod]
        public void And_the_zip_file_path_ends_with_bak_and_contains_the_database_backup_files_name_then_the_zip_file_path_is_returned()
        {
            _daxorLabRestore = new DaxorLabRestore(_stubMessageBoxDispatcher, _stubLoggerFacade, _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

            const string expectedResult = DaxorBackupFileName + MssqlBackupExtension;

            var observedResult = _daxorLabRestore.GetTemporaryFileRestorePathForZipFiles(expectedResult);

            Assert.AreEqual(expectedResult, observedResult);
        }

        [TestMethod]
        public void And_the_zip_file_path_does_not_end_with_bak_or_zip_then_null_is_returned()
        {
            _daxorLabRestore = new DaxorLabRestore(_stubMessageBoxDispatcher, _stubLoggerFacade, _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

            var observedResult = _daxorLabRestore.GetTemporaryFileRestorePathForZipFiles(DaxorBackupFileName);

            Assert.IsNull(observedResult);
        }

        [TestMethod]
        public void And_the_zip_file_does_not_contain_the_DaxorLab_backup_then_an_exception_is_thrown()
        {
            var zipToReturn = new ZipFile(DaxorBackupFileName + ZipFileExtension);
            zipToReturn.AddFile(BackupsLocationInZip + NotTheDaxorSuiteBackup);
            _stubIonicZipWrapper.Read(Arg.Any<string>()).Returns(zipToReturn);

            _daxorLabRestore = new DaxorLabRestore(_stubMessageBoxDispatcher, _stubLoggerFacade, _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

            AssertEx.Throws<DatabaseRestoreBackupFileNotFoundException>(() =>
            {
                // ReSharper disable once UnusedVariable
                var observedResult = _daxorLabRestore.GetTemporaryFileRestorePathForZipFiles(DaxorBackupFileName + ZipFileExtension);
            });
        }

        [TestMethod]
        public void And_the_zip_file_contains_the_DaxorLab_backup_then_the_root_directory_is_determined()
        {
            const string backupFileFullPath = BackupsLocationInZip + DaxorBackupFileName + MssqlBackupExtension;

            var zipToReturn = new ZipFile(DaxorBackupFileName + ZipFileExtension);
            zipToReturn.AddFile(backupFileFullPath);
            _stubIonicZipWrapper.Read(Arg.Any<string>()).Returns(zipToReturn);

            _daxorLabRestore = Substitute.For<DaxorLabRestore>(_stubMessageBoxDispatcher, _stubLoggerFacade,
                _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

            _daxorLabRestore.GetTemporaryFileRestorePathForZipFiles(DaxorBackupFileName + ZipFileExtension);

            _daxorLabRestore.Received().DetermineRootDirectory(zipToReturn[backupFileFullPath]);
        }

        [TestMethod]
        public void And_the_zip_file_contains_the_DaxorLab_backup_then_the_settings_manager_gets_the_temp_location()
        {
            const string backupFileFullPath = BackupsLocationInZip + DaxorBackupFileName + MssqlBackupExtension;

            var zipToReturn = new ZipFile(DaxorBackupFileName + ZipFileExtension);
            zipToReturn.AddFile(backupFileFullPath);
            _stubIonicZipWrapper.Read(Arg.Any<string>()).Returns(zipToReturn);

            _daxorLabRestore = Substitute.For<DaxorLabRestore>(_stubMessageBoxDispatcher, _stubLoggerFacade,
                _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

            _daxorLabRestore.GetTemporaryFileRestorePathForZipFiles(DaxorBackupFileName + ZipFileExtension);

            _stubSettingsManager.Received()
                .GetSetting<string>(SettingKeys.SystemTempFilePath);
        }

        [TestMethod]
        public void And_the_zip_file_contains_the_DaxorLab_backup_then_the_backup_file_name_is_determined()
        {
            const string backupFileFullPath = BackupsLocationInZip + DaxorBackupFileName + MssqlBackupExtension;

            var zipToReturn = new ZipFile(DaxorBackupFileName + ZipFileExtension);
            zipToReturn.AddFile(backupFileFullPath);
            _stubIonicZipWrapper.Read(Arg.Any<string>()).Returns(zipToReturn);

            _daxorLabRestore = Substitute.For<DaxorLabRestore>(_stubMessageBoxDispatcher, _stubLoggerFacade,
                _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

            _daxorLabRestore.GetTemporaryFileRestorePathForZipFiles(DaxorBackupFileName + ZipFileExtension);

            _daxorLabRestore.Received().DetermineBackupFileName(zipToReturn[backupFileFullPath]);
        }

        [TestMethod]
        public void And_the_zip_file_contains_the_DaxorLab_backup_then_the_backup_file_is_extracted()
        {
            const string backupFileFullPath = BackupsLocationInZip + DaxorBackupFileName + MssqlBackupExtension;

            var zipToReturn = new ZipFile(DaxorBackupFileName + ZipFileExtension);
            zipToReturn.AddFile(backupFileFullPath);
            _stubIonicZipWrapper.Read(Arg.Any<string>()).Returns(zipToReturn);

            _daxorLabRestore = Substitute.For<DaxorLabRestore>(_stubMessageBoxDispatcher, _stubLoggerFacade,
                _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

            _daxorLabRestore.GetTemporaryFileRestorePathForZipFiles(DaxorBackupFileName + ZipFileExtension);

            _daxorLabRestore.Received().ExtractBackupToCorrectLocation(zipToReturn[backupFileFullPath]);
        }

        public void And_the_zip_file_contains_the_DaxorLab_backup_then_the_backup_directory_is_deleted()
        {
            const string backupFileFullPath = BackupsLocationInZip + DaxorBackupFileName + MssqlBackupExtension;

            var zipToReturn = new ZipFile(DaxorBackupFileName + ZipFileExtension);
            zipToReturn.AddFile(backupFileFullPath);
            _stubIonicZipWrapper.Read(Arg.Any<string>()).Returns(zipToReturn);

            _daxorLabRestore = Substitute.For<DaxorLabRestore>(_stubMessageBoxDispatcher, _stubLoggerFacade,
                _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);


            _daxorLabRestore.GetTemporaryFileRestorePathForZipFiles(DaxorBackupFileName + ZipFileExtension);


            _daxorLabRestore.Received().DeleteBackupDirectory(TemporaryFileLocation, DaxorBackupFileName + MssqlBackupExtension);
        }
    }
    // ReSharper restore RedundantArgumentName
}