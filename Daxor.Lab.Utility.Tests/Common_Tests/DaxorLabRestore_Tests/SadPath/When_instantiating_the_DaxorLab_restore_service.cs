﻿using System;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Common;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Common_Tests.DaxorLabRestore_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_instantiating_the_DaxorLab_restore_service
    {
        private IMessageBoxDispatcher _stubMessageBoxDispatcher;
        private ILoggerFacade _stubLoggerFacade;
        private ISettingsManager _stubSettingsManager;
        private IMessageManager _stubMessageManager;
        private IZipFileFactory _stubZipFileFactory;

        [TestInitialize]
        public void Initialize()
        {
            _stubMessageBoxDispatcher = Substitute.For<IMessageBoxDispatcher>();
            _stubMessageManager = Substitute.For<IMessageManager>();
            _stubLoggerFacade = Substitute.For<ILoggerFacade>();
            _stubSettingsManager = Substitute.For<ISettingsManager>();
            _stubZipFileFactory = Substitute.For<IZipFileFactory>();
        }
        // ReSharper disable UnusedVariable
        [TestMethod]
        public void And_the_message_box_dispatcher_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var restore = new DaxorLabRestore(null, _stubLoggerFacade, _stubSettingsManager, _stubMessageManager,
                    _stubZipFileFactory);
            }, "messageBoxDispatcher");
        }

        [TestMethod]
        public void And_the_logger_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var restore = new DaxorLabRestore(_stubMessageBoxDispatcher, null, _stubSettingsManager, _stubMessageManager,
                    _stubZipFileFactory);
            }, "logger");
        }

        [TestMethod]
        public void And_the_settings_manager_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var restore = new DaxorLabRestore(_stubMessageBoxDispatcher, _stubLoggerFacade, null, _stubMessageManager,
                    _stubZipFileFactory);
            }, "settingsManager");
        }

        [TestMethod]
        public void And_the_message_manager_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var restore = new DaxorLabRestore(_stubMessageBoxDispatcher, _stubLoggerFacade, _stubSettingsManager, null,
                    _stubZipFileFactory);
            }, "messageManager");
        }

        [TestMethod]
        public void And_the_Ionic_zip_file_factory_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var restore = new DaxorLabRestore(_stubMessageBoxDispatcher, _stubLoggerFacade, _stubSettingsManager, _stubMessageManager,
                    null);
            }, "ionicZipFileFactory");
        }
        // ReSharper restore UnusedVariable
    }
    // ReSharper restore RedundantArgumentName
}