﻿using System;
using Daxor.Lab.Utility.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Utility.Tests.Common_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_DateTimeConverter
    {
        private DateTimeConverter _dateTimeConverter;

        [TestInitialize]
        public void Initialize()
        {
            _dateTimeConverter = new DateTimeConverter();
        }

        [TestMethod]
        public void And_the_string_to_convert_is_null_then_it_is_converted_to_null()
        {
            Assert.IsNull(_dateTimeConverter.Convert(null,typeof(DateTime?),null, null));
        }

        [TestMethod]
        public void And_the_datetime_string_to_convert_is_not_valid_it_is_converted_to_null()
        {
            Assert.IsNull(_dateTimeConverter.Convert("Phillip", typeof(DateTime?), null, null));
        }

        [TestMethod]
        public void And_the_DateTime_value_is_null_then_it_is_converted_to_the_empty_string()
        {
            Assert.AreEqual(String.Empty, _dateTimeConverter.ConvertBack(null, typeof(DateTime?), null, null));
        }
    }
    // ReSharper restore InconsistentNaming
}
