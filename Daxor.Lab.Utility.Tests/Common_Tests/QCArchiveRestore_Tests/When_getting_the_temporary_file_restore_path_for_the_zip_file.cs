﻿using System;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Common;
using Ionic.Zip;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Common_Tests.QCArchiveRestore_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_temporary_file_restore_path_for_the_zip_file
    {
        private const string QCArchiveBackupFileName = "QCDatabaseArchive";
        private const string MssqlBackupExtension = ".bak";
        private const string ZipFileExtension = ".zip";
        private const string BackupsLocationInZip = @"Backups\";
        private const string NotTheQCArchiveBackup = "DaxorSuite";
        private const string TemporaryFileLocation = @"C:\temp";

        private IMessageBoxDispatcher _stubMessageBoxDispatcher;
        private ILoggerFacade _stubLoggerFacade;
        private ISettingsManager _stubSettingsManager;
        private IMessageManager _stubMessageManager;
        private IZipFileFactory _stubZipFileFactory;
        private IIonicZipWrapper _stubIonicZipWrapper;
        private QCArchiveRestore _qcArchiveRestore;

        [TestInitialize]
        public void Initialize()
        {
            _stubMessageBoxDispatcher = Substitute.For<IMessageBoxDispatcher>();
            _stubMessageManager = Substitute.For<IMessageManager>();
            _stubLoggerFacade = Substitute.For<ILoggerFacade>();
            _stubSettingsManager = Substitute.For<ISettingsManager>();
            _stubZipFileFactory = Substitute.For<IZipFileFactory>();
            _stubIonicZipWrapper = Substitute.For<IIonicZipWrapper>();
            _stubZipFileFactory.CreateZipFileWithPassword(Arg.Any<string>()).Returns(_stubIonicZipWrapper);

            _stubSettingsManager.GetSetting<string>(RuntimeSettingKeys.SystemDaxorLabDatabaseBackupFilename)
                .Returns(QCArchiveBackupFileName);

            _stubSettingsManager.GetSetting<String>(SettingKeys.SystemTempFilePath).Returns(TemporaryFileLocation);
        }

        [TestMethod]
        public void And_the_zip_file_path_ends_with_bak_and_contains_the_database_backup_files_name_then_the_zip_file_path_is_returned()
        {
            _qcArchiveRestore = new QCArchiveRestore(_stubMessageBoxDispatcher, _stubLoggerFacade, _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

            const string expectedResult = QCArchiveBackupFileName + MssqlBackupExtension;

            var observedResult = _qcArchiveRestore.GetTemporaryFileRestorePathForZipFiles(expectedResult);

            Assert.AreEqual(expectedResult, observedResult);
        }

        [TestMethod]
        public void And_the_zip_file_path_does_not_end_with_bak_or_zip_then_null_is_returned()
        {
            _qcArchiveRestore = new QCArchiveRestore(_stubMessageBoxDispatcher, _stubLoggerFacade, _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

            var observedResult = _qcArchiveRestore.GetTemporaryFileRestorePathForZipFiles(QCArchiveBackupFileName);

            Assert.IsNull(observedResult);
        }

        [TestMethod]
        public void And_the_zip_file_does_not_contain_the_QcDatabaseArchive_backup_then_an_exception_is_thrown()
        {
            var zipToReturn = new ZipFile(QCArchiveBackupFileName + ZipFileExtension);
            zipToReturn.AddFile(BackupsLocationInZip + NotTheQCArchiveBackup);
            _stubIonicZipWrapper.Read(Arg.Any<string>()).Returns(zipToReturn);

            _qcArchiveRestore = new QCArchiveRestore(_stubMessageBoxDispatcher, _stubLoggerFacade, _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

            AssertEx.Throws<DatabaseRestoreBackupFileNotFoundException>(() =>
            {
                // ReSharper disable once UnusedVariable
                var observedResult = _qcArchiveRestore.GetTemporaryFileRestorePathForZipFiles(QCArchiveBackupFileName + ZipFileExtension);
            });
        }

        [TestMethod]
        public void And_the_zip_file_contains_the_QcDatabaseArchive_backup_then_the_root_directory_is_determined()
        {
                const string backupFileFullPath = BackupsLocationInZip + QCArchiveBackupFileName + MssqlBackupExtension;
                
                var zipToReturn = new ZipFile(QCArchiveBackupFileName + ZipFileExtension);
                zipToReturn.AddFile(backupFileFullPath);
                _stubIonicZipWrapper.Read(Arg.Any<string>()).Returns(zipToReturn);

                _qcArchiveRestore = Substitute.For<QCArchiveRestore>(_stubMessageBoxDispatcher, _stubLoggerFacade,
                    _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

                _qcArchiveRestore.GetTemporaryFileRestorePathForZipFiles(QCArchiveBackupFileName + ZipFileExtension);

                _qcArchiveRestore.Received().DetermineRootDirectory(zipToReturn[backupFileFullPath]);
        }

        [TestMethod]
        public void And_the_zip_file_contains_the_QcDatabaseArchive_backup_then_the_settings_manager_gets_the_temp_location()
        {
                const string backupFileFullPath = BackupsLocationInZip + QCArchiveBackupFileName + MssqlBackupExtension;

                var zipToReturn = new ZipFile(QCArchiveBackupFileName + ZipFileExtension);
                zipToReturn.AddFile(backupFileFullPath);
                _stubIonicZipWrapper.Read(Arg.Any<string>()).Returns(zipToReturn);

                _qcArchiveRestore = Substitute.For<QCArchiveRestore>(_stubMessageBoxDispatcher, _stubLoggerFacade,
                    _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

                _qcArchiveRestore.GetTemporaryFileRestorePathForZipFiles(QCArchiveBackupFileName + ZipFileExtension);

                _stubSettingsManager.Received()
                    .GetSetting<string>(SettingKeys.SystemTempFilePath);
        }

        [TestMethod]
        public void And_the_zip_file_contains_the_QcDatabaseArchive_backup_then_the_backup_file_name_is_determined()
        {
                const string backupFileFullPath = BackupsLocationInZip + QCArchiveBackupFileName + MssqlBackupExtension;

                var zipToReturn = new ZipFile(QCArchiveBackupFileName + ZipFileExtension);
                zipToReturn.AddFile(backupFileFullPath);
                _stubIonicZipWrapper.Read(Arg.Any<string>()).Returns(zipToReturn);

                _qcArchiveRestore = Substitute.For<QCArchiveRestore>(_stubMessageBoxDispatcher, _stubLoggerFacade,
                    _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

                _qcArchiveRestore.GetTemporaryFileRestorePathForZipFiles(QCArchiveBackupFileName + ZipFileExtension);

                _qcArchiveRestore.Received().DetermineBackupFileName(zipToReturn[backupFileFullPath]);
        }

        [TestMethod]
        public void And_the_zip_file_contains_the_QcDatabaseArchive_backup_then_the_backup_file_is_extracted()
        {
                const string backupFileFullPath = BackupsLocationInZip + QCArchiveBackupFileName + MssqlBackupExtension;

                var zipToReturn = new ZipFile(QCArchiveBackupFileName + ZipFileExtension);
                zipToReturn.AddFile(backupFileFullPath);
                _stubIonicZipWrapper.Read(Arg.Any<string>()).Returns(zipToReturn);

                _qcArchiveRestore = Substitute.For<QCArchiveRestore>(_stubMessageBoxDispatcher, _stubLoggerFacade,
                    _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);

                _qcArchiveRestore.GetTemporaryFileRestorePathForZipFiles(QCArchiveBackupFileName + ZipFileExtension);

                _qcArchiveRestore.Received().ExtractBackupToCorrectLocation(zipToReturn[backupFileFullPath]);
        }

        public void And_the_zip_file_contains_the_QcDatabaseArchive_backup_then_the_backup_directory_is_deleted()
        {
                const string backupFileFullPath = BackupsLocationInZip + QCArchiveBackupFileName + MssqlBackupExtension;

                var zipToReturn = new ZipFile(QCArchiveBackupFileName + ZipFileExtension);
                zipToReturn.AddFile(backupFileFullPath);
                _stubIonicZipWrapper.Read(Arg.Any<string>()).Returns(zipToReturn);

                _qcArchiveRestore = Substitute.For<QCArchiveRestore>(_stubMessageBoxDispatcher, _stubLoggerFacade,
                    _stubSettingsManager, _stubMessageManager, _stubZipFileFactory);


                _qcArchiveRestore.GetTemporaryFileRestorePathForZipFiles(QCArchiveBackupFileName + ZipFileExtension);


                _qcArchiveRestore.Received().DeleteBackupDirectory(TemporaryFileLocation, QCArchiveBackupFileName + MssqlBackupExtension);
        }
    }
    // ReSharper restore RedundantArgumentName
}