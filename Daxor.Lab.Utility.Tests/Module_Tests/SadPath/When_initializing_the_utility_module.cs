﻿using System;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.ReportViewer.Excel_Export;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Services;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Module_Tests.SadPath
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_the_Utility_module
	{
		private ModuleThatDoesNotInitializeSettingsDataAccessLayer _module;

		private IUnityContainer _container;

		[TestInitialize]
		public void Before_every_test()
		{
			_container = Substitute.For<IUnityContainer>();
		}

		[TestMethod]
		public void And_an_error_occurs_when_resolving_the_settings_manager_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<ISettingsManager>()).Throw(new Exception());
			_module = new ModuleThatDoesNotInitializeSettingsDataAccessLayer(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Utilities", ex.ModuleName);
				Assert.AreEqual("initialize the Settings Manager", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_initializing_the_settings_data_access_layer_then_the_correct_module_load_exception_is_thrown()
		{
			_module = new ModuleThatDoesNotInitializeSettingsDataAccessLayer(_container)
			{
				ThrowExceptionOnInitializeSettingsDataAccessLayer = true
			};

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Utilities", ex.ModuleName);
				Assert.AreEqual("initialize the Settings Data Access Layer", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_utility_module_navigator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IAppModuleNavigator, UtilityModuleNavigator>(Arg.Any<string>(), Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new ModuleThatDoesNotInitializeSettingsDataAccessLayer(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Utilities", ex.ModuleName);
				Assert.AreEqual("register the Utility Module Navigator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_resolve_the_utility_module_navigator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<IAppModuleNavigator>(Arg.Any<string>())).Throw(new Exception());
			_module = new ModuleThatDoesNotInitializeSettingsDataAccessLayer(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Utilities", ex.ModuleName);
				Assert.AreEqual("initialize the Utility Module Navigator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_excel_report_generator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IExcelReportGenerator, ExcelReportGenerator>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new ModuleThatDoesNotInitializeSettingsDataAccessLayer(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Utilities", ex.ModuleName);
				Assert.AreEqual("initialize the Excel Report Generator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_report_controller_factory_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IReportControllerFactory, ReportControllerFactory>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new ModuleThatDoesNotInitializeSettingsDataAccessLayer(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Utilities", ex.ModuleName);
				Assert.AreEqual("initialize the Report Controller Factory", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_utility_data_mapper_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IUtilityDataMapper, UtilityDataMapper>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new ModuleThatDoesNotInitializeSettingsDataAccessLayer(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Utilities", ex.ModuleName);
				Assert.AreEqual("initialize the Utility Data Mapper", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_utility_data_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IUtilityDataService, UtilityDataService>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new ModuleThatDoesNotInitializeSettingsDataAccessLayer(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Utilities", ex.ModuleName);
				Assert.AreEqual("initialize the Utility Data Service", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_report_processor_wrapper_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IReportProcessor, ReportProcessorWrapper>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new ModuleThatDoesNotInitializeSettingsDataAccessLayer(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Utilities", ex.ModuleName);
				Assert.AreEqual("initialize the Report Processor Wrapper", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_registering_the_excel_file_builder_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IExcelFileBuilder, ExcelFileBuilder>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new ModuleThatDoesNotInitializeSettingsDataAccessLayer(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Utilities", ex.ModuleName);
				Assert.AreEqual("initialize the Excel File Builder", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}
	}
	// ReSharper restore InconsistentNaming
}