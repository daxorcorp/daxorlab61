﻿using System;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.Utility.Tests.Module_Tests
{
	internal class ModuleThatDoesNotInitializeSettingsDataAccessLayer : Module
	{
		public ModuleThatDoesNotInitializeSettingsDataAccessLayer(IUnityContainer container) : base(container)
		{
			ThrowExceptionOnInitializeSettingsDataAccessLayer = false;
		}

		public bool ThrowExceptionOnInitializeSettingsDataAccessLayer { get; set; }

		protected override void InitializeSettingsDataAccessLayer(ISettingsManager settingsManager)
		{
			if (ThrowExceptionOnInitializeSettingsDataAccessLayer)
				throw new Exception();
		}
	}
}
