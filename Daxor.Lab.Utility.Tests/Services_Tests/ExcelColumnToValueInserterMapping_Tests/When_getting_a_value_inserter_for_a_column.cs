﻿using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.Utility.Tests.Services_Tests.ExcelColumnToValueInserterMapping_Tests
{
	[TestClass]
	public class When_getting_a_value_inserter_for_a_column
	{
		private ExcelColumnToValueInserterMapping _excelColumnToValueInserterMapping;

		[TestInitialize]
		public void Init()
		{
			_excelColumnToValueInserterMapping = new ExcelColumnToValueInserterMapping();
		}

		[TestMethod]
		public void Then_there_is_an_inserter_for_the_amputee_status_column()
		{
			Assert.AreEqual(ValueInserters.InsertAmputeeStatus, _excelColumnToValueInserterMapping.GetValueInserterForColumn(BvaExcelColumnHeader.AmputeeStatus));
		}

		[TestMethod]
		public void Then_there_is_an_inserter_for_the_amputee_ideals_correction_factor_column()
		{
			Assert.AreEqual(ValueInserters.InsertAmputeeIdealsCorrectionFactor, _excelColumnToValueInserterMapping.GetValueInserterForColumn(BvaExcelColumnHeader.AmputeeIdealsCorrectionFactor));
		}
	}
}
