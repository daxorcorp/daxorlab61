using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ExcelRowConfiguration_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_checking_to_see_if_ideals_are_available
    {
        [TestMethod]
        public void And_the_given_test_is_null_then_an_exception_is_thrown()
        {
            var stubIdealsCalEngineService = MockRepository.GenerateStub<IIdealsCalcEngineService>();
            
            var rowConfiguration = new ExcelRowConfiguration(stubIdealsCalEngineService) {Test = null};
            AssertEx.Throws<InvalidOperationException>(()=>rowConfiguration.AreIdealsResultsAvailable());
        }

        [TestMethod]
        public void And_the_ideals_calc_engine_is_null_then_an_exception_is_thrown()
        {
            var rowConfiguration = new ExcelRowConfiguration(null) { Test = new BVATest(null) };
            AssertEx.Throws<InvalidOperationException>(() => rowConfiguration.AreIdealsResultsAvailable());
        }
    }
    // ReSharper restore InconsistentNaming
}
