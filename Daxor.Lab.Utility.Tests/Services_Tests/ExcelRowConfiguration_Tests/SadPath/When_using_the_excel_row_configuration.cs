﻿using System;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Daxor.Lab.BVA.Tests.ViewModels;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ExcelRowConfiguration_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_Excel_row_configuration
    {
        [TestMethod]
        public void And_the_test_is_null_then_an_exception_is_thrown()
        {
            var rowConfiguration = new ExcelRowConfiguration(null);

            // ReSharper disable once UnusedVariable
            AssertEx.Throws<InvalidOperationException>(() => { var ignored = rowConfiguration.Test; });
        }

        [TestMethod]
        public void And_the_measured_calc_engine_results_are_null_then_an_exception_is_thrown()
        {
            var rowConfiguration = new ExcelRowConfiguration(null);

            // ReSharper disable once UnusedVariable
            AssertEx.Throws<InvalidOperationException>(() => { var ignored = rowConfiguration.MeasuredCalcEngineResults; });
        }

        [TestMethod]
        public void And_the_test_patient_is_null_then_an_exception_is_thrown()
        {
            var rowConfiguration = new ExcelRowConfiguration(null)
            {
                Test = TestFactory.CreateBvaTest(3)
            };

            rowConfiguration.Test.Patient = null;
            
            // ReSharper disable once UnusedVariable
            AssertEx.Throws<InvalidOperationException>(() => { var ignored = rowConfiguration.Test; });
        }
    }
    // ReSharper restore InconsistentNaming

}
