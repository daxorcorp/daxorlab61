﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ExcelRowConfiguration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_checking_to_see_if_ideals_are_available
    {
        private const double _maximumHeightInCm = 20.0;
        private const double _minimumHeightInCm = 10.0;
        private const double _inRangeHeight = (_maximumHeightInCm + _minimumHeightInCm) / 2;
        private readonly double _inRangeWeightDeviation = (BvaDomainConstants.MaximumWeightDeviation + BvaDomainConstants.MinimumWeightDeviation) / 2.0;

        private BVATest _stubBvaTest;
        private IIdealsCalcEngineService _stubIdealsCalcEngine;

        [TestInitialize]
        public void TestInitialize()
        {
            _stubBvaTest = TestFactory.CreateBvaTest(3);
            
            _stubIdealsCalcEngine = MockRepository.GenerateStub<IIdealsCalcEngineService>();
            _stubIdealsCalcEngine.Expect(i => i.MinimumHeightInCm).Return(_minimumHeightInCm);
            _stubIdealsCalcEngine.Expect(i => i.MaximumHeightInCm).Return(_maximumHeightInCm);
        }

        [TestMethod]
        public void And_the_weight_deviation_is_greater_than_the_maximum_then_ideals_results_are_not_available()
        {
            _stubBvaTest.Patient.HeightInCm = _inRangeHeight;

            _stubIdealsCalcEngine.Expect(i => 
                                         i.GetIdeals(_stubBvaTest.Patient.WeightInKg, _stubBvaTest.Patient.HeightInCm, (int)_stubBvaTest.Patient.Gender))
                                         .Return(new IdealsCalcEngineResults(0, 0, 0, BvaDomainConstants.MaximumWeightDeviation + 1));

            var rowConfiguration = new ExcelRowConfiguration(_stubIdealsCalcEngine) {Test = _stubBvaTest};

            Assert.IsFalse(rowConfiguration.AreIdealsResultsAvailable());
        }

        [TestMethod]
        public void And_the_weight_deviation_is_less_than_the_minimum_then_ideals_results_are_not_available()
        {
            _stubBvaTest.Patient.HeightInCm = _inRangeHeight;

            _stubIdealsCalcEngine.Expect(i =>
                                         i.GetIdeals(_stubBvaTest.Patient.WeightInKg, _stubBvaTest.Patient.HeightInCm, (int)_stubBvaTest.Patient.Gender))
                                         .Return(new IdealsCalcEngineResults(0, 0, 0, BvaDomainConstants.MinimumWeightDeviation - 1));

            var rowConfiguration = new ExcelRowConfiguration(_stubIdealsCalcEngine) { Test = _stubBvaTest };

            Assert.IsFalse(rowConfiguration.AreIdealsResultsAvailable());
        }

        [TestMethod]
        public void And_the_patients_height_is_greater_than_the_maximum_then_ideals_results_are_not_available()
        {
            _stubBvaTest.Patient.HeightInCm = _maximumHeightInCm + 1;

            _stubIdealsCalcEngine.Expect(i =>
                                         i.GetIdeals(_stubBvaTest.Patient.WeightInKg, _stubBvaTest.Patient.HeightInCm, (int)_stubBvaTest.Patient.Gender))
                                         .Return(new IdealsCalcEngineResults(0, 0, 0, _inRangeWeightDeviation));

            var rowConfiguration = new ExcelRowConfiguration(_stubIdealsCalcEngine) { Test = _stubBvaTest };

            Assert.IsFalse(rowConfiguration.AreIdealsResultsAvailable());
        }

        [TestMethod]
        public void And_the_patients_height_is_less_than_the_minimum_then_ideals_results_are_not_available()
        {
            _stubBvaTest.Patient.HeightInCm = _minimumHeightInCm - 1;

            _stubIdealsCalcEngine.Expect(i =>
                                         i.GetIdeals(_stubBvaTest.Patient.WeightInKg, _stubBvaTest.Patient.HeightInCm, (int)_stubBvaTest.Patient.Gender))
                                         .Return(new IdealsCalcEngineResults(0, 0, 0, _inRangeWeightDeviation));

            var rowConfiguration = new ExcelRowConfiguration(_stubIdealsCalcEngine) { Test = _stubBvaTest };

            Assert.IsFalse(rowConfiguration.AreIdealsResultsAvailable());
        }
        
        [TestMethod]
        public void And_the_patients_height_is_in_range_and_weight_deviation_is_in_range_then_ideals_results_are_available()
        {
            _stubBvaTest.Patient.HeightInCm = _inRangeHeight;

            _stubIdealsCalcEngine.Expect(i =>
                                         i.GetIdeals(_stubBvaTest.Patient.WeightInKg, _stubBvaTest.Patient.HeightInCm, (int)_stubBvaTest.Patient.Gender))
                                         .Return(new IdealsCalcEngineResults(0, 0, 0, _inRangeWeightDeviation));

            var rowConfiguration = new ExcelRowConfiguration(_stubIdealsCalcEngine) { Test = _stubBvaTest };

            Assert.IsTrue(rowConfiguration.AreIdealsResultsAvailable());
        }
    }
    // ReSharper restore InconsistentNaming
}
