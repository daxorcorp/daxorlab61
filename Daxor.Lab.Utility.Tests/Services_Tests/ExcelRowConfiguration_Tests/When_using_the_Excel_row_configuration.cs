using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ExcelRowConfiguration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_Excel_row_configuration
    {
        [TestMethod]
        public void Then_setting_the_test_instance_to_null_is_allowed()
        {
            // ReSharper disable once UnusedVariable
            var rowConfiguration = new ExcelRowConfiguration(MockRepository.GenerateStub<IIdealsCalcEngineService>())
            {
                Test = null
            };
        }

        [TestMethod]
        public void Then_setting_the_measured_calc_engine_results_instance_to_null_is_allowed()
        {
            // ReSharper disable once UnusedVariable
            var rowConfiguration = new ExcelRowConfiguration(MockRepository.GenerateStub<IIdealsCalcEngineService>())
            {
                MeasuredCalcEngineResults = null
            };
        }
    }
    // ReSharper restore InconsistentNaming
}
