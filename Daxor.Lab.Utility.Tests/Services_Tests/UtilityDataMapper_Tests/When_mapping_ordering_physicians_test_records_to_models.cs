using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Utility.Tests.Services_Tests.UtilityDataMapper_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_mapping_ordering_physicians_test_records_to_models
    {
        private OrderingPhysicianDataRecord _firstPhysicanDataRecord;
        private OrderingPhysicianDataRecord _secondPhysicanDataRecord;
        private OrderingPhysicianDataRecord _thirdPhysicanDataRecord;
        private OrderingPhysicianDataRecord _fourthPhysicanDataRecord;
        private List<GetOrderingPhysiciansBetweenResult> _databaseRecords;
        private IUtilityDataMapper _dataMapper;

        [TestInitialize]
        public void Initialize()
        {
            // NOTE: Records are being created out of order to ensure the tests work as designed.

            _firstPhysicanDataRecord = new OrderingPhysicianDataRecord
            {
                ReferringPhysician = "Geoff Mazeroff",
                PhysicianSpecialty = "Memes",
                TestRecordsList = new List<OrderingPhysicianTestRecord>
                {
                    new OrderingPhysicianTestRecord
                    {
                        Analyst = "PTA",
                        CcReportTo = "Stacy",
                        Location = "Doro",
                        TestDate = DateTime.Today.ToString("M/d/yy"),
                    },

                    new OrderingPhysicianTestRecord
                    {
                        Analyst = "PTA",
                        CcReportTo = "Stacy",
                        Location = "Doro",
                        TestDate = DateTime.Today.AddDays(-2).ToString("M/d/yy"),
                    }
                }
            };

            _secondPhysicanDataRecord = new OrderingPhysicianDataRecord
            {
                ReferringPhysician = "Stacy Mullinax",
                PhysicianSpecialty = UtilityDataMapper.UnknownSpecialityLabel,
                TestRecordsList = new List<OrderingPhysicianTestRecord>
                {
                    new OrderingPhysicianTestRecord
                    {
                        Analyst = "PTA",
                        CcReportTo = "Stacy",
                        Location = "Doro",
                        TestDate = DateTime.Today.AddDays(-1).ToString("M/d/yy"),
                    }
                }
            };

            _thirdPhysicanDataRecord = new OrderingPhysicianDataRecord
            {
                ReferringPhysician = UtilityDataMapper.UnknownPhysicianLabel,
                PhysicianSpecialty = UtilityDataMapper.UnknownSpecialityLabel,
                TestRecordsList = new List<OrderingPhysicianTestRecord>
                {
                    new OrderingPhysicianTestRecord
                    {
                        Analyst = "PTA",
                        CcReportTo = "Stacy",
                        Location = "Doro",
                        TestDate = DateTime.Today.AddDays(-3).ToString("M/d/yy"),
                    }
                }
            };

            _fourthPhysicanDataRecord = new OrderingPhysicianDataRecord
            {
                ReferringPhysician = UtilityDataMapper.UnknownPhysicianLabel,
                PhysicianSpecialty = UtilityDataMapper.UnknownSpecialityLabel,
                TestRecordsList = new List<OrderingPhysicianTestRecord>
                {
                    new OrderingPhysicianTestRecord
                    {
                        Analyst = "PTA",
                        CcReportTo = "Stacy",
                        Location = "Doro",
                        TestDate = DateTime.Today.AddDays(-4).ToString("M/d/yy"),
                    }
                }
            };

            _databaseRecords = new List<GetOrderingPhysiciansBetweenResult>
            {
                // Oldest | Geoff
                new GetOrderingPhysiciansBetweenResult
                {
                    ANALYST = _firstPhysicanDataRecord.TestRecordsList.ToArray()[1].Analyst,
                    CC_REPORT_TO = _firstPhysicanDataRecord.TestRecordsList.ToArray()[1].CcReportTo,
                    LOCATION = _firstPhysicanDataRecord.TestRecordsList.ToArray()[1].Location,
                    TEST_DATE = DateTime.Parse(_firstPhysicanDataRecord.TestRecordsList.ToArray()[1].TestDate),
                    PHYSICIANS_SPECIALTY = _firstPhysicanDataRecord.PhysicianSpecialty,
                    REFERRING_PHYSICIAN = _firstPhysicanDataRecord.ReferringPhysician
                },
                // Middle | Stacy
                new GetOrderingPhysiciansBetweenResult
                {
                    ANALYST = _secondPhysicanDataRecord.TestRecordsList.First().Analyst,
                    CC_REPORT_TO = _secondPhysicanDataRecord.TestRecordsList.First().CcReportTo,
                    LOCATION = _secondPhysicanDataRecord.TestRecordsList.First().Location,
                    TEST_DATE = DateTime.Parse(_secondPhysicanDataRecord.TestRecordsList.First().TestDate),
                    PHYSICIANS_SPECIALTY = string.Empty,
                    REFERRING_PHYSICIAN = _secondPhysicanDataRecord.ReferringPhysician
                },
                // Newest | Geoff
                new GetOrderingPhysiciansBetweenResult
                {
                    ANALYST = _firstPhysicanDataRecord.TestRecordsList.First().Analyst,
                    CC_REPORT_TO = _firstPhysicanDataRecord.TestRecordsList.First().CcReportTo,
                    LOCATION = _firstPhysicanDataRecord.TestRecordsList.First().Location,
                    TEST_DATE = DateTime.Now,
                    PHYSICIANS_SPECIALTY = _firstPhysicanDataRecord.PhysicianSpecialty,
                    REFERRING_PHYSICIAN = _firstPhysicanDataRecord.ReferringPhysician
                },
                //Empty Physician name
                new GetOrderingPhysiciansBetweenResult
                {
                    ANALYST = _thirdPhysicanDataRecord.TestRecordsList.First().Analyst,
                    CC_REPORT_TO = _thirdPhysicanDataRecord.TestRecordsList.First().CcReportTo,
                    LOCATION = _thirdPhysicanDataRecord.TestRecordsList.First().Location,
                    TEST_DATE = DateTime.Parse(_thirdPhysicanDataRecord.TestRecordsList.First().TestDate),
                    PHYSICIANS_SPECIALTY = string.Empty,
                    REFERRING_PHYSICIAN = string.Empty
                },

                new GetOrderingPhysiciansBetweenResult
                {
                    ANALYST = _fourthPhysicanDataRecord.TestRecordsList.First().Analyst,
                    CC_REPORT_TO = _fourthPhysicanDataRecord.TestRecordsList.First().CcReportTo,
                    LOCATION = _fourthPhysicanDataRecord.TestRecordsList.First().Location,
                    TEST_DATE = DateTime.Parse(_fourthPhysicanDataRecord.TestRecordsList.First().TestDate),
                    PHYSICIANS_SPECIALTY = "     ",
                    REFERRING_PHYSICIAN = "     "
                },
            };

            _dataMapper = new UtilityDataMapper();
        }

        [TestMethod]
        public void Then_the_physician_who_ordered_a_test_most_recently_is_first_in_the_collection()
        {
            var recordsInOrder = new List<OrderingPhysicianDataRecord>
            {
                _firstPhysicanDataRecord,
                _secondPhysicanDataRecord,
                _thirdPhysicanDataRecord,
                _fourthPhysicanDataRecord,
            };

            var observedRecords = _dataMapper.MapOrderingPhysicianDataRecords(_databaseRecords);

            CollectionAssert.AreEqual(recordsInOrder, observedRecords);
        }

        [TestMethod]
        public void Then_there_is_only_one_entry_for_each_physician_in_the_collection()
        {
            var observedRecords = _dataMapper.MapOrderingPhysicianDataRecords(_databaseRecords);

            Assert.IsTrue(observedRecords.FindAll(x=> x.ReferringPhysician == "Geoff Mazeroff").Count == 1);
        }

        [TestMethod]
        public void Then_the_test_entries_for_each_physician_are_ordered_so_that_most_recent_tests_come_first()
        {
            var observedRecords = _dataMapper.MapOrderingPhysicianDataRecords(_databaseRecords);

            // The list of database records to map starts with the oldest test. So if they
            // come out sorted correctly, the records have been grouped and ordered
            // correctly.
            Assert.AreEqual(_firstPhysicanDataRecord, observedRecords.FirstOrDefault());
        }

        [TestMethod]
        public void And_a_physician_has_multiple_specialties_then_the_most_recent_one_is_used()
        {
            // Oldest and newset both start out w/ the same speciality
            GetGeoffsOldestRecord().PHYSICIANS_SPECIALTY = "Nit Picking";

            var observedRecords = _dataMapper.MapOrderingPhysicianDataRecords(_databaseRecords);

            // ReSharper disable once PossibleNullReferenceException
            Assert.AreEqual("Memes", observedRecords.FirstOrDefault().PhysicianSpecialty);
        }

        [TestMethod]
        public void And_a_physicians_most_recent_specialty_is_empty_then_the_most_recent_non_empty_one_is_used()
        {
            GetGeoffsNewestRecord().PHYSICIANS_SPECIALTY = string.Empty;
            GetGeoffsOldestRecord().PHYSICIANS_SPECIALTY = "Nit Picking";

            var observedRecords = _dataMapper.MapOrderingPhysicianDataRecords(_databaseRecords);

            // ReSharper disable once PossibleNullReferenceException
            Assert.AreEqual("Nit Picking", observedRecords.FirstOrDefault().PhysicianSpecialty);
        }

        [TestMethod]
        public void And_all_of_a_physicians_specialties_are_empty_then_the_speciality_is_listed_as_unknown()
        {
            var observedRecords = _dataMapper.MapOrderingPhysicianDataRecords(_databaseRecords);

            Assert.AreEqual(UtilityDataMapper.UnknownSpecialityLabel, observedRecords[1].PhysicianSpecialty);
        }

        [TestMethod]
        public void And_the_physicians_specialty_is_white_space_then_the_specialty_is_listed_as_unknown()
        {
            var observedRecords = _dataMapper.MapOrderingPhysicianDataRecords(_databaseRecords);

            Assert.AreEqual(UtilityDataMapper.UnknownSpecialityLabel, observedRecords[3].PhysicianSpecialty);
        }

        [TestMethod]
        public void And_the_referring_physicians_name_is_not_known_then_the_physicians_name_is_listed_as_unknown()
        {
            var observedRecords = _dataMapper.MapOrderingPhysicianDataRecords(_databaseRecords);

            Assert.AreEqual(UtilityDataMapper.UnknownPhysicianLabel, observedRecords[2].ReferringPhysician);
        }

        [TestMethod]
        public void And_the_referring_physicians_name_is_white_space_then_the_physicians_name_is_listed_as_unknown()
        {
            var observedRecords = _dataMapper.MapOrderingPhysicianDataRecords(_databaseRecords);

            Assert.AreEqual(UtilityDataMapper.UnknownPhysicianLabel, observedRecords[3].ReferringPhysician);
        }

        [TestMethod]
        public void Then_the_date_is_displayed_with_no_time_component()
        {
            var observedRecords = _dataMapper.MapOrderingPhysicianDataRecords(_databaseRecords);

            Assert.AreEqual(DateTime.Now.ToString("M/d/yy"), observedRecords.First().TestRecordsList.First().TestDate);
        }

        [TestMethod]
        [RequirementValue("Unknown Physician")]
        public void Then_the_unknown_referring_physician_label_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), UtilityDataMapper.UnknownPhysicianLabel);
        }

        [TestMethod]
        [RequirementValue("Unknown Specialty")]
        public void Then_the_unknown_physician_specialty_label_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), UtilityDataMapper.UnknownSpecialityLabel);
        }

        private GetOrderingPhysiciansBetweenResult GetGeoffsOldestRecord()
        {
            return (from r in _databaseRecords where r.REFERRING_PHYSICIAN == "Geoff Mazeroff" orderby r.TEST_DATE select r).FirstOrDefault();
        }

        private GetOrderingPhysiciansBetweenResult GetGeoffsNewestRecord()
        {
            return (from r in _databaseRecords where r.REFERRING_PHYSICIAN == "Geoff Mazeroff"  orderby r.TEST_DATE descending select r).FirstOrDefault();
        }
    }
    // ReSharper restore InconsistentNaming
}
