using CarlosAg.ExcelXmlWriter;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ExcelReportGenerator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_saving_the_report
    {
        private ExcelReportGenerator _reportGenerator;
        private IExcelFileBuilder _mockExcelFileBuilder;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockExcelFileBuilder = Substitute.For<IExcelFileBuilder>();
            var workbook = new Workbook();
            workbook.Worksheets.Add("testworksheet");
            _mockExcelFileBuilder.GetFirstWorksheet().Returns(workbook.Worksheets[0]);
            
            _reportGenerator = new ExcelReportGenerator(Substitute.For<ISettingsManager>(), Substitute.For<IIdealsCalcEngineService>(),
                _mockExcelFileBuilder);
        }

        [TestMethod]
        public void Then_the_Excel_file_builder_is_used()
        {
            _reportGenerator.SaveReport("somepath");

            _mockExcelFileBuilder.Received(1).SaveExcelWorkbook("somepath");
        }

        [TestMethod]
        public void Then_the_initial_worksheet_is_readded()
        {
            _reportGenerator.SaveReport("somepath");

            _mockExcelFileBuilder.Received(2).AddWorksheet(ExcelReportGenerator.WorksheetName);
        }
    }
    // ReSharper restore InconsistentNaming
}
