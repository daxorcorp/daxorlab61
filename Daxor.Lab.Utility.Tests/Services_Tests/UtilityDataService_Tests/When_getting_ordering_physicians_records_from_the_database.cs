using System;
using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Utility.Services;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Services_Tests.UtilityDataService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_ordering_physicians_records_from_the_database
    {
        [TestMethod]
        public void Then_the_LINQ_database_context_is_used()
        {
            var dataUtiltyService = new UtilityDataServiceThatDoesntHitTheDataBase(Substitute.For<ILoggerFacade>(), Substitute.For<IUtilityDataMapper>());

            dataUtiltyService.GetOrderingPhysiciansBetween(DateTime.Now, DateTime.Now);

            Assert.IsTrue(dataUtiltyService.WasGetOrderingPhysiciansFromDatabaseCalled);
        }

        [TestMethod]
        public void And_the_LINQ_database_context_is_used_then_the_data_mapper_is_used()
        {
            var mockDataMapper = Substitute.For<IUtilityDataMapper>();
            // ReSharper disable once CollectionNeverUpdated.Local
            var records = new List<GetOrderingPhysiciansBetweenResult>();

            var dataUtiltyService = new UtilityDataServiceThatDoesntHitTheDataBase(Substitute.For<ILoggerFacade>(), mockDataMapper, records);

            dataUtiltyService.GetOrderingPhysiciansBetween(DateTime.Now, DateTime.Now);

            mockDataMapper.Received().MapOrderingPhysicianDataRecords(records);
        }
    }

    // ReSharper restore InconsistentNaming
}
