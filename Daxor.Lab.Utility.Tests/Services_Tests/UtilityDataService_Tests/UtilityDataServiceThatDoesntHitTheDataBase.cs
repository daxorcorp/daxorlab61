using System;
using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Utility.Services;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Utility.Tests.Services_Tests.UtilityDataService_Tests
{
    internal class UtilityDataServiceThatDoesntHitTheDataBase : UtilityDataService
    {
        private readonly IEnumerable<GetOrderingPhysiciansBetweenResult> _records; 

        internal UtilityDataServiceThatDoesntHitTheDataBase(ILoggerFacade logger, IUtilityDataMapper dataMapper) : base(logger,dataMapper)
        {
            _records = new List<GetOrderingPhysiciansBetweenResult>();
        }

        internal UtilityDataServiceThatDoesntHitTheDataBase(ILoggerFacade logger, IUtilityDataMapper dataMapper, IEnumerable<GetOrderingPhysiciansBetweenResult> records )
            : base(logger, dataMapper)
        {
            _records = records;
        }

        internal bool WasGetOrderingPhysiciansFromDatabaseCalled { get; set; }

        protected override IEnumerable<GetOrderingPhysiciansBetweenResult> GetOrderingPhysiciansFromDatabase(DateTime dateFromDateTime, DateTime dateToDateTime)
        {
            WasGetOrderingPhysiciansFromDatabaseCalled = true;

            return _records;
        }
    }
}