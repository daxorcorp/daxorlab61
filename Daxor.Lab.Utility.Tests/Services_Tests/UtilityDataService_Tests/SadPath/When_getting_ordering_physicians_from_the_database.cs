using System;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Services;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Services_Tests.UtilityDataService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_ordering_physicians_from_the_database
    {
        [TestMethod]
        public void And_an_exception_is_thrown_when_connecting_to_the_database_then_an_exception_is_thrown()
        {
            var dataUtilityService = new UtilityDataServiceThatThrowsException(Substitute.For<ILoggerFacade>(), Substitute.For<IUtilityDataMapper>());

            AssertEx.Throws(() => dataUtilityService.GetOrderingPhysiciansBetween(DateTime.Now, DateTime.Now));
        }

        [TestMethod]
        public void And_an_exception_is_thrown_when_connecting_to_the_database_then_the_exception_is_logged()
        {
            var mockLogger = Substitute.For<ILoggerFacade>();
            var dataUtilityService = new UtilityDataServiceThatThrowsException(mockLogger, Substitute.For<IUtilityDataMapper>());

            try
            {
                dataUtilityService.GetOrderingPhysiciansBetween(DateTime.Now, DateTime.Now);
            }
            catch
            {
                mockLogger.Received().Log("Exception: Test Exception", Category.Exception, Priority.High);
                return;
            }

            Assert.Fail("No exception thrown");
        }
    }
    // ReSharper restore InconsistentNaming
}
