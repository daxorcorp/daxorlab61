using System;
using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Utility.Services;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Utility.Tests.Services_Tests.UtilityDataService_Tests
{
    internal class UtilityDataServiceThatThrowsException : UtilityDataService
    {
        internal UtilityDataServiceThatThrowsException(ILoggerFacade logger, IUtilityDataMapper dataMapper) : base(logger,dataMapper)
        {
        }

        protected override IEnumerable<GetOrderingPhysiciansBetweenResult> GetOrderingPhysiciansFromDatabase(DateTime dateFromDateTime, DateTime dateToDateTime)
        {
            throw new Exception("Test Exception");
        }
    }
}