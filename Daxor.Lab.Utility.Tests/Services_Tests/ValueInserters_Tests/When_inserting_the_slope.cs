﻿using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_inserting_the_slope
    {
        [TestMethod]
        public void Then_the_slope_rounded_to_two_decimal_places_is_inserted()
        {
            const int column = 86;
            const double slope = 0.123;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, slope, 0, 0)
            };

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet,
                rowConfigurationWithNoIdeals.Row, column, (slope * 100).RoundAndFormat(2), CellStyle.None));

            // Act
            ValueInserters.InsertSlope(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
