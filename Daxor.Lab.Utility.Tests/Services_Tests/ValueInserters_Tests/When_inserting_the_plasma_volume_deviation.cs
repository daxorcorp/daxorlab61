using System.Globalization;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_inserting_the_plasma_volume_deviation
    {
        [TestMethod]
        public void And_ideals_are_not_available_then_NA_is_inserted()
        {
            const int column = 86;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(false);
            mockExcelFileBuilder.Expect(x => x.InsertStringCell(ValueInserters.Worksheet, rowConfigurationWithNoIdeals.Row, column, "N/A", CellStyle.None));

            // Act
            ValueInserters.InsertPlasmaVolumeDeviation(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_ideals_are_available_then_plasma_volume_deviation_is_inserted()
        {
            const int column = 86;
            const int ideal = 20;
            const int measured = 25;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3)
            };

            rowConfigurationWithNoIdeals.Test.Volumes[BloodType.Measured].PlasmaCount = measured;
            rowConfigurationWithNoIdeals.Test.Volumes[BloodType.Ideal].PlasmaCount = ideal;

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet, rowConfigurationWithNoIdeals.Row, column, (measured - ideal).ToString(CultureInfo.InvariantCulture), CellStyle.None));

            // Act
            ValueInserters.InsertPlasmaVolumeDeviation(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
