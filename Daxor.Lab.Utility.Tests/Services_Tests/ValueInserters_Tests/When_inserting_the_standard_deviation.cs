﻿using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_inserting_the_standard_deviation
    {
        [TestMethod]
        public void And_the_standard_deviation_is_NaN_then_0_is_inserted()
        {
            const int column = 86;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, double.NaN, 0)
            };

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet, 
                rowConfigurationWithNoIdeals.Row, column, "0", CellStyle.None));

            // Act
            ValueInserters.InsertStandardDeviation(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_standard_deviation_is_valid_then_the_standard_deviation_rounded_to_three_decimal_places_is_inserted()
        {
            const int column = 86;
            const double stdDeviation = 0.123;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, stdDeviation, 0)
            };

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet, 
                rowConfigurationWithNoIdeals.Row, column, (stdDeviation * 100).RoundAndFormat(3), CellStyle.None));

            // Act
            ValueInserters.InsertStandardDeviation(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
