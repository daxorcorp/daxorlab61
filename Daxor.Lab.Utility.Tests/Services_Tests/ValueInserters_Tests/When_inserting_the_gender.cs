﻿using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_inserting_the_gender
    {
        [TestMethod]
        public void And_the_patient_is_female_then_the_gender_is_inserted()
        {
            const int column = 86;
            const GenderType femaleGender = GenderType.Female;

            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0)
            };

            rowConfigurationWithNoIdeals.Test.Patient.Gender = femaleGender;

            mockExcelFileBuilder.Expect(x => x.InsertStringCell(ValueInserters.Worksheet,
                rowConfigurationWithNoIdeals.Row, column, femaleGender.ToString(), CellStyle.None));

            // Act
            ValueInserters.InsertPatientGender(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_patient_is_male_then_the_gender_is_inserted()
        {
            const int column = 86;
            const GenderType maleGender = GenderType.Male;

            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0)
            };

            rowConfigurationWithNoIdeals.Test.Patient.Gender = maleGender;

            mockExcelFileBuilder.Expect(x => x.InsertStringCell(ValueInserters.Worksheet,
                rowConfigurationWithNoIdeals.Row, column, maleGender.ToString(), CellStyle.None));

            // Act
            ValueInserters.InsertPatientGender(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_patient_is_unknown_then_the_gender_is_inserted()
        {
            const int column = 86;
            const GenderType unknownGender = GenderType.Unknown;

            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0)
            };

            rowConfigurationWithNoIdeals.Test.Patient.Gender = unknownGender;

            mockExcelFileBuilder.Expect(x => x.InsertStringCell(ValueInserters.Worksheet,
                rowConfigurationWithNoIdeals.Row, column, unknownGender.ToString(), CellStyle.None));

            // Act
            ValueInserters.InsertPatientGender(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
