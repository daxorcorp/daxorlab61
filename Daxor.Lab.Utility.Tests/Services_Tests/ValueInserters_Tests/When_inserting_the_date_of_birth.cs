﻿using System;
using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_inserting_the_date_of_birth
    {
        [TestMethod]
        public void And_the_date_of_birth_is_null_then_the_empty_string_is_inserted()
        {
            const int column = 86;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0)
            };

            rowConfigurationWithNoIdeals.Test.Patient.DateOfBirth = null;

            mockExcelFileBuilder.Expect(x => x.InsertStringCell(ValueInserters.Worksheet,
                rowConfigurationWithNoIdeals.Row, column, String.Empty, CellStyle.None));

            // Act
            ValueInserters.InsertDateOfBirth(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_date_of_birth_is_not_null_then_the_date_of_birth_is_inserted()
        {
            const int column = 86;
            var dateOfBirth = new DateTime(2012,4,26);

            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0)
            };

            rowConfigurationWithNoIdeals.Test.Patient.DateOfBirth = dateOfBirth;

            mockExcelFileBuilder.Expect(x => x.InsertDateTimeCell(ValueInserters.Worksheet,
                rowConfigurationWithNoIdeals.Row, column, dateOfBirth.ToShortDateString(), CellStyle.UsDate));

            // Act
            ValueInserters.InsertDateOfBirth(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
