﻿using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
	[TestClass]
	public class When_inserting_the_amputee_ideals_correction_factor
	{
		private IExcelFileBuilder _mockExcelFileBuilder;

		private ExcelRowConfigurationWithCustomAreIdealsResultsAvailable _rowConfigurationWithNoIdeals;

		[TestInitialize]
		public void Init()
		{
			_mockExcelFileBuilder = Substitute.For<IExcelFileBuilder>();

			ValueInsertersHelpers.InitializeValueInserters(_mockExcelFileBuilder);

			_rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
			{
				Test = TestFactory.CreateBvaTest(3),
				MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0)
			};
		}

		[TestMethod]
		public void And_the_patient_is_not_an_amputee_then_0_percent_is_inserted()
		{
			//Arrange
			_rowConfigurationWithNoIdeals.Test.PatientIsAmputee = false;

			//Act
			ValueInserters.InsertAmputeeIdealsCorrectionFactor(_rowConfigurationWithNoIdeals, 3);

			//Assert
			_mockExcelFileBuilder.Received(1).InsertStringCell(ValueInserters.Worksheet, _rowConfigurationWithNoIdeals.Row, 3, "0%", CellStyle.None);
		}

		[TestMethod]
		public void And_the_patient_is_an_amputee_and_the_correction_factor_has_not_been_set_then_unselected_is_inserted()
		{
			//Arrange
			_rowConfigurationWithNoIdeals.Test.PatientIsAmputee = true;

			//Act
			ValueInserters.InsertAmputeeIdealsCorrectionFactor(_rowConfigurationWithNoIdeals, 3);

			//Assert
			_mockExcelFileBuilder.Received(1).InsertStringCell(ValueInserters.Worksheet, _rowConfigurationWithNoIdeals.Row, 3, "Unselected", CellStyle.None);
		}

		[TestMethod]
		public void And_the_patient_is_an_amputee_and_the_correction_factor_has_been_set_to_0_then_0_percent_is_inserted()
		{
			//Arrange
			_rowConfigurationWithNoIdeals.Test.PatientIsAmputee = true;
			_rowConfigurationWithNoIdeals.Test.AmputeeIdealsCorrectionFactor = 0;

			//Act
			ValueInserters.InsertAmputeeIdealsCorrectionFactor(_rowConfigurationWithNoIdeals, 3);

			//Assert
			_mockExcelFileBuilder.Received(1).InsertStringCell(ValueInserters.Worksheet, _rowConfigurationWithNoIdeals.Row, 3, "0%", CellStyle.None);
		}

		[TestMethod]
		public void
			And_the_patient_is_an_amputee_and_the_correction_factor_has_been_set_to_a_positive_number_then_that_number_as_a_negative_percentage_is_inserted()
		{
			//Arrange
			_rowConfigurationWithNoIdeals.Test.PatientIsAmputee = true;
			_rowConfigurationWithNoIdeals.Test.AmputeeIdealsCorrectionFactor = 5;

			//Act
			ValueInserters.InsertAmputeeIdealsCorrectionFactor(_rowConfigurationWithNoIdeals, 3);

			//Assert
			_mockExcelFileBuilder.Received(1).InsertStringCell(ValueInserters.Worksheet, _rowConfigurationWithNoIdeals.Row, 3, "-5%", CellStyle.None);
		}
	}
}
