﻿using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Utility.Services;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    public class ExcelRowConfigurationWithCustomAreIdealsResultsAvailable : ExcelRowConfiguration
    {
        private readonly bool _areIdealResultsAvailable;

        public ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(bool areIdealResultsAvailable) : base(MockRepository.GenerateStub<IIdealsCalcEngineService>())
        {
            _areIdealResultsAvailable = areIdealResultsAvailable;
        }

        public override bool AreIdealsResultsAvailable()
        {
            return _areIdealResultsAvailable;
        }
    }
}
