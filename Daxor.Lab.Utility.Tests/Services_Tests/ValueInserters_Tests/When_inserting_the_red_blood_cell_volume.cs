﻿using System.Globalization;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_inserting_the_red_blood_cell_volume
{
        [TestMethod]
        public void And_ideals_are_available_then_the_ideal_red_blood_cell_volume_is_inserted()
        {
            const int column = 86;
            const int measured = 20;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3)
            };
            rowConfigurationWithNoIdeals.Test.Volumes[BloodType.Measured].RedCellCount = measured;

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet, rowConfigurationWithNoIdeals.Row, column, measured.ToString(CultureInfo.InvariantCulture), CellStyle.None));

            // Act
            ValueInserters.InsertRedBloodCellVolumeInMl(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming

}
