﻿using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_inserting_the_ideal_weight_deviation
    {
        [TestMethod]
        public void Then_the_weight_deviation_rounded_to_one_decimal_place_is_inserted()
        {
            const int column = 86;
            const double weightDeviation = 123.456;

            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0)
            };

            rowConfigurationWithNoIdeals.Test.Patient.DeviationFromIdealWeight = weightDeviation;

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet,
                rowConfigurationWithNoIdeals.Row, column, weightDeviation.RoundAndFormat(1), CellStyle.None));

            // Act
            ValueInserters.InsertIdealWeightDeviation(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
