﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_inserting_the_red_blood_cell_excess_or_deficit
    {
        [TestMethod]
        public void And_the_red_cell_excess_or_deficit_is_0_then_NA_is_inserted()
        {
            const int column = 86;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(false)
            {
                Test = TestFactory.CreateBvaTest(3)
            };

            rowConfigurationWithNoIdeals.Test.Volumes[BloodType.Ideal].RedCellCount = 0;

            mockExcelFileBuilder.Expect(x => x.InsertStringCell(ValueInserters.Worksheet, 
                rowConfigurationWithNoIdeals.Row, column, "N/A", CellStyle.None));

            // Act
            ValueInserters.InsertRedBloodCellExcessOrDeficit(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_red_cell_excess_or_deficit_is_not_0_then_the_excess_or_deficit_rounded_to_one_decimal_place_is_inserted()
        {
            const int column = 86;
            const int ideal = 20;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3)
            };

            rowConfigurationWithNoIdeals.Test.Volumes[BloodType.Ideal].RedCellCount = ideal;
            rowConfigurationWithNoIdeals.Test.Volumes[BloodType.Measured].RedCellCount = ideal + 5;

            var expectedDeviation = ((((ideal + 5) - ideal)/(double)ideal) * 100).RoundAndFormat(1);

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet, 
                rowConfigurationWithNoIdeals.Row, column, expectedDeviation, CellStyle.None));

            // Act
            ValueInserters.InsertRedBloodCellExcessOrDeficit(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
