﻿using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_inserting_the_middle_name
    {
        [TestMethod]
        public void Then_the_patients_middle_name_is_inserted()
        {
            const int column = 86;
            const string patientsMiddleName = "AMiddleName";

            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0)
            };

            rowConfigurationWithNoIdeals.Test.Patient.MiddleName = patientsMiddleName;

            mockExcelFileBuilder.Expect(x => x.InsertStringCell(ValueInserters.Worksheet,
                rowConfigurationWithNoIdeals.Row, column, patientsMiddleName, CellStyle.None));

            // Act
            ValueInserters.InsertMiddleName(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
