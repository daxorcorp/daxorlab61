using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_inserting_the_normalized_hematocrit
    {
        [TestMethod]
        public void And_ideals_are_not_available_then_NA_is_inserted()
        {
            const int column = 86;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(false)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0)
            };

            mockExcelFileBuilder.Expect(x => x.InsertStringCell(ValueInserters.Worksheet, rowConfigurationWithNoIdeals.Row, column, "N/A", CellStyle.None));

            // Act
            ValueInserters.InsertNormalizedHematocrit(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_normalized_hct_is_nan_then_NA_is_inserted()
        {
            const int column = 86;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, double.NaN)
            };

            mockExcelFileBuilder.Expect(x => x.InsertStringCell(ValueInserters.Worksheet, rowConfigurationWithNoIdeals.Row, column, "N/A", CellStyle.None));

            // Act
            ValueInserters.InsertNormalizedHematocrit(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_ideals_are_available_and_the_normalized_hct_is_a_whole_number_then_the_correct_normalized_hematocrit_is_inserted()
        {
            const int column = 86;
            const double normalizedHct = 25;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, normalizedHct)
            };

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet, 
                rowConfigurationWithNoIdeals.Row, column, normalizedHct.RoundAndFormat(1), CellStyle.None));

            // Act
            ValueInserters.InsertNormalizedHematocrit(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_ideals_are_available_and_the_normalized_hct_that_should_be_rounded_then_the_correct_normalized_hematocrit_is_inserted()
        {
            const int column = 86;
            const double normalizedHct = 25.45;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, normalizedHct)
            };

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet,
                rowConfigurationWithNoIdeals.Row, column, normalizedHct.RoundAndFormat(1), CellStyle.None));

            // Act
            ValueInserters.InsertNormalizedHematocrit(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_ideals_are_available_and_the_normalized_hct_that_should_not_be_rounded_then_the_correct_normalized_hematocrit_is_inserted()
        {
            const int column = 86;
            const double normalizedHct = 25.2;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, normalizedHct)
            };

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet,
                rowConfigurationWithNoIdeals.Row, column, normalizedHct.RoundAndFormat(1), CellStyle.None));

            // Act
            ValueInserters.InsertNormalizedHematocrit(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_ideals_are_available_and_the_normalized_hct_that_should_be_rounded_down_then_the_correct_normalized_hematocrit_is_inserted()
        {
            const int column = 86;
            const double normalizedHct = 25.31;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, normalizedHct)
            };

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet,
                rowConfigurationWithNoIdeals.Row, column, normalizedHct.RoundAndFormat(1), CellStyle.None));

            // Act
            ValueInserters.InsertNormalizedHematocrit(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
