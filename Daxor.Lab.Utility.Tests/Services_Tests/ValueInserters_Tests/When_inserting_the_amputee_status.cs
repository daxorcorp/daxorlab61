﻿using System;
using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
	[TestClass]
	public class When_inserting_the_amputee_status
	{
		private IExcelFileBuilder _mockExcelFileBuilder;

		private ExcelRowConfigurationWithCustomAreIdealsResultsAvailable _rowConfigurationWithNoIdeals;

		[TestInitialize]
		public void Init()
		{
			_mockExcelFileBuilder = Substitute.For<IExcelFileBuilder>();

			ValueInsertersHelpers.InitializeValueInserters(_mockExcelFileBuilder);

			_rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
			{
				Test = TestFactory.CreateBvaTest(3),
				MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0)
			};
		}

		[TestMethod]
		public void And_the_patient_is_not_an_amputee_then_non_amputee_is_inserted()
		{
			//Arrange
			_rowConfigurationWithNoIdeals.Test.Patient.IsAmputee = false;

			//Act
			ValueInserters.InsertAmputeeStatus(_rowConfigurationWithNoIdeals, 3);

			//Assert
			_mockExcelFileBuilder.Received(1).InsertStringCell(ValueInserters.Worksheet, _rowConfigurationWithNoIdeals.Row, 3, "False", CellStyle.None);
		}

		[TestMethod]
		public void And_the_patient_is_an_amputee_then_amputee_is_inserted()
		{
			//Arrange
			_rowConfigurationWithNoIdeals.Test.Patient.IsAmputee = true;

			//Act
			ValueInserters.InsertAmputeeStatus(_rowConfigurationWithNoIdeals, 3);

			//Assert
			_mockExcelFileBuilder.Received(1).InsertStringCell(ValueInserters.Worksheet, _rowConfigurationWithNoIdeals.Row, 3, "True", CellStyle.None);
		}
	}
}
