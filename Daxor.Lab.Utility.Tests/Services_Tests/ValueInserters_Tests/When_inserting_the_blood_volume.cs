﻿using System.Globalization;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_inserting_the_blood_volume
    {
        [TestMethod]
        public void Then_the_blood_volume_is_inserted()
        {
            const int column = 86;
            const int plasmaVolume = 123456;

            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0)
            };

            rowConfigurationWithNoIdeals.Test.Volumes[BloodType.Measured].PlasmaCount = plasmaVolume;
            rowConfigurationWithNoIdeals.Test.Volumes[BloodType.Measured].RedCellCount = 0;

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet,
                rowConfigurationWithNoIdeals.Row, column, plasmaVolume.ToString(CultureInfo.InvariantCulture), CellStyle.None));

            // Act
            ValueInserters.InsertBloodVolumeInMl(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
