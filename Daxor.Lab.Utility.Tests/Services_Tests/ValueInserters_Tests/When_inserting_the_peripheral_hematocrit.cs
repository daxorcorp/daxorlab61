﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Tests.ViewModels;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_inserting_the_peripheral_hematocrit
    {
        [TestMethod]
        public void And_peripheral_hematocrit_is_NaN_then_0_is_inserted()
        {
            const int column = 86;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(false)
            {
                Test = TestFactory.CreateBvaTest(3)
            };

            rowConfigurationWithNoIdeals.Test.HematocritFill = HematocritFillType.OnePerTest;
            rowConfigurationWithNoIdeals.Test.PatientFirstCompositeSample.SampleA.Hematocrit = double.NaN;

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet, 
                rowConfigurationWithNoIdeals.Row, column, "0", CellStyle.None));

            // Act
            ValueInserters.InsertPeripheralHematocrit(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_peripheral_hematocrit_is_not_0_then_the_peripheral_hematocrit_rounded_to_one_decimal_place_is_inserted()
        {
            const int column = 86;
            const double hematocrit = 1.123;
            var mockExcelFileBuilder = MockRepository.GenerateMock<IExcelFileBuilder>();

            ValueInsertersHelpers.InitializeValueInserters(mockExcelFileBuilder);

            var rowConfigurationWithNoIdeals = new ExcelRowConfigurationWithCustomAreIdealsResultsAvailable(true)
            {
                Test = TestFactory.CreateBvaTest(3)
            };

            rowConfigurationWithNoIdeals.Test.HematocritFill = HematocritFillType.OnePerTest;
            rowConfigurationWithNoIdeals.Test.PatientFirstCompositeSample.SampleA.Hematocrit = hematocrit;

            var expectedDeviation = hematocrit.RoundAndFormat(1);

            mockExcelFileBuilder.Expect(x => x.InsertNumberCell(ValueInserters.Worksheet, 
                rowConfigurationWithNoIdeals.Row, column, expectedDeviation, CellStyle.None));

            // Act
            ValueInserters.InsertPeripheralHematocrit(rowConfigurationWithNoIdeals, column);

            mockExcelFileBuilder.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
