using CarlosAg.ExcelXmlWriter;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests
{
    public class ValueInsertersHelpers
    {
        public static void InitializeValueInserters()
        {
            InitializeValueInserters(MockRepository.GenerateStub<IExcelFileBuilder>());
        }

        public static void InitializeValueInserters(IExcelFileBuilder excelFileBuilder)
        {
            var worksheet = new Workbook().Worksheets.Add("something");

            try
            {
                ValueInserters.Initialize(excelFileBuilder, worksheet);
            }
            catch { Assert.Fail("Did not initialize Value Inserters."); }
        }

        public static void InitalizeBadValueInserters()
        {
            try
            {
                ValueInserters.Initialize(null, null);
            }
            catch
            {
                return;
            }
            Assert.Fail("Did not initialize bad value inserters.");
        }
    }
}