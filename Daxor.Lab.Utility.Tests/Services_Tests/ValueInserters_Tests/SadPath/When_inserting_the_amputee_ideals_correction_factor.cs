﻿using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
// ReSharper disable InconsistentNaming

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests.SadPath
{
	[TestClass]
	public class When_inserting_the_amputee_ideals_correction_factor
	{
		private ExcelRowConfiguration _rowConfiguration;

		[TestMethod]
		public void And_the_row_configuration_is_null_then_an_exception_is_thrown()
		{
			_rowConfiguration = null;

			AssertEx.ThrowsArgumentNullException(() => ValueInserters.InsertAmputeeIdealsCorrectionFactor(_rowConfiguration, 0), "rowConfiguration");
		}

		[TestMethod]
		public void And_the_row_configuration_is_not_initialized_then_an_exception_is_thrown()
		{
			ValueInsertersHelpers.InitalizeBadValueInserters();
			_rowConfiguration = new ExcelRowConfiguration(Substitute.For<IIdealsCalcEngineService>())
			{
				Row = 1,
				Test = new BVATest(null),
				MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0)
			};

			AssertEx.Throws<InvalidOperationException>(() => ValueInserters.InsertAmputeeIdealsCorrectionFactor(_rowConfiguration, 86));
		}
	}
}
