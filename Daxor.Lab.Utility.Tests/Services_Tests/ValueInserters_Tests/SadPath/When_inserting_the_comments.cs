﻿using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_inserting_the_comments
    {
        private ExcelRowConfiguration _rowConfiguration;

        [TestMethod]
        public void And_the_value_inserters_have_not_been_initialized_then_an_exception_is_thrown()
        {
            ValueInsertersHelpers.InitalizeBadValueInserters();
            _rowConfiguration = new ExcelRowConfiguration(MockRepository.GenerateStub<IIdealsCalcEngineService>())
            {
                Row = 1,
                Test = new BVATest(null),
                MeasuredCalcEngineResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0)
            };

            AssertEx.Throws<InvalidOperationException>(()=>ValueInserters.InsertComments(_rowConfiguration, 86));
        }

        [TestMethod]
        public void And_the_row_configuration_is_null_then_an_exception_is_thrown()
        {
            ValueInsertersHelpers.InitializeValueInserters();
            _rowConfiguration = null;

            AssertEx.ThrowsArgumentNullException(() => ValueInserters.InsertComments(_rowConfiguration, 86), "rowConfiguration");
        }
    }
    // ReSharper restore InconsistentNaming
}
