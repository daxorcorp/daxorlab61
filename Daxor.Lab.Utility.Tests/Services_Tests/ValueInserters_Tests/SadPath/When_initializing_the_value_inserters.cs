using System;
using CarlosAg.ExcelXmlWriter;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Utility.Tests.Services_Tests.ValueInserters_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_value_inserters
    {
        private Worksheet _worksheet;

        [TestInitialize]
        public void TestInitialize()
        {
            var workbook = new Workbook();
            workbook.Worksheets.Add("something");
            _worksheet = workbook.Worksheets[0];
        }

        [TestMethod]
        public void And_the_Excel_file_builder_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>ValueInserters.Initialize(null, _worksheet), "excelFileBuilder");
        }

        [TestMethod]
        public void And_the_worksheet_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>ValueInserters.Initialize(MockRepository.GenerateStub<IExcelFileBuilder>(), null), "worksheet");
        }

        [TestMethod]
        public void Then_a_bad_initialization_marks_the_inserters_as_not_initialized()
        {
            ValueInserters.Initialize(MockRepository.GenerateStub<IExcelFileBuilder>(), _worksheet);
            Assert.IsTrue(ValueInserters.IsInitialized);

            try
            {
                // Example of bad initialization
                ValueInserters.Initialize(null, _worksheet);
            }
                // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
                Assert.IsFalse(ValueInserters.IsInitialized);
                return;
            }

            Assert.Fail("No exception was thrown");
        }
    }
    // ReSharper restore InconsistentNaming
}
