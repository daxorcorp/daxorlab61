using System;
using System.Collections.Generic;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_constructing_the_model
    {
        private OrderingPhysiciansReportModel _model;
        private readonly DateTime FromTime = new DateTime(2014, 1, 2);
        private readonly DateTime ToTime = new DateTime(2014, 11, 22);
        private List<OrderingPhysicianDataRecord> _dataRecords;
        
        [TestInitialize]
        public void Before_each_test()
        {
            _dataRecords = new List<OrderingPhysicianDataRecord> { new OrderingPhysicianDataRecord() };
            _model = new OrderingPhysiciansReportModel(FromTime, ToTime, _dataRecords, Substitute.For<IExcelFileBuilder>());
        }

        [TestMethod]
        public void Then_the_from_date_is_set()
        {
            Assert.AreEqual(FromTime, _model.FromDateTime);
        }

        [TestMethod]
        public void Then_the_to_date_is_set()
        {
            Assert.AreEqual(ToTime, _model.ToDateTime);
        }

        [TestMethod]
        public void Then_the_ordering_physician_data_records_list_is_set()
        {
            Assert.AreEqual(_dataRecords, _model.OrderingPhysicianDataRecords);
        }

        [TestMethod]
        public void Then_the_printed_on_date_is_the_current_date_and_time_with_no_seconds()
        {
            var printedOnDateAsDateTime = DateTime.Parse(_model.GeneratePrintedOnDate().Replace("Printed: ", ""));
            var diff = DateTime.Now - printedOnDateAsDateTime;

            Assert.IsTrue(Math.Abs(diff.Minutes) <= 1);
        }

        [TestMethod]
        public void Then_the_printed_on_date_prefix_is_correct()
        {
            var dateLabelAndValue = _model.GeneratePrintedOnDate();
            Assert.IsTrue(dateLabelAndValue.StartsWith("Printed: "));
        }
    }
    // ReSharper restore InconsistentNaming
}
