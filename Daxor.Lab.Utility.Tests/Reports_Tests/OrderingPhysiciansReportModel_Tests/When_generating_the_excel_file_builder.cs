﻿using System;
using System.Collections.Generic;
using System.Reflection;
using CarlosAg.ExcelXmlWriter;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_generating_the_Excel_file_builder
    {
        private IExcelFileBuilder _excelFileBuilder;
        private OrderingPhysiciansReportModel _reportModel;
        private List<OrderingPhysicianDataRecord> _physicianDataRecords;
        private OrderingPhysicianTestRecord _testRecord1;
        private OrderingPhysicianTestRecord _testRecord2;
        private OrderingPhysicianDataRecord _dataRecord1;
        private OrderingPhysicianDataRecord _dataRecord2;
        private DateTime _fromDateTime;
        private DateTime _toDateTime;
        private const string HospitalName = "St. Doro";

        [TestInitialize]
        public void Initialize()
        {
            _testRecord1 = new OrderingPhysicianTestRecord
            {
                Analyst = "GAM",
                CcReportTo = "Stacy",
                TestDate = "4/26/81",
                Location = "DORO"
            };

            _testRecord2 = new OrderingPhysicianTestRecord
            {
                Analyst = "PTA",
                CcReportTo = "Geoff",
                TestDate = "4/24/81",
                Location = "Methodist"
            };

            _dataRecord1 = new OrderingPhysicianDataRecord
            {
                ReferringPhysician = "Phillip",
                PhysicianSpecialty = "Green Stuff",
                TestRecordsList = new List<OrderingPhysicianTestRecord> { _testRecord1, _testRecord2 }
            };

            _dataRecord2 = new OrderingPhysicianDataRecord
            {
                ReferringPhysician = "Randy",
                PhysicianSpecialty = "Red Stuff",
                TestRecordsList = new List<OrderingPhysicianTestRecord> { _testRecord1, _testRecord2 }
            };

            _physicianDataRecords = new List<OrderingPhysicianDataRecord> { _dataRecord1, _dataRecord2 };

            _fromDateTime = new DateTime(1981, 4, 20);
            _toDateTime = new DateTime(1981, 4, 28);

            _excelFileBuilder = Substitute.For<IExcelFileBuilder>();

            _reportModel = new OrderingPhysiciansReportModel(_fromDateTime, _toDateTime, _physicianDataRecords, _excelFileBuilder);
        }

        [TestMethod]
        [RequirementValue("Ordering Physicians")]
        public void Then_the_worksheet_name_matches_requirements()
        {
            _reportModel.GenerateExcelFileBuilder(HospitalName);

            _excelFileBuilder.Received().AddWorksheet(RequirementHelper.GetRequirementValueAsString());
        }

        [TestMethod]
        public void Then_the_data_is_added_to_the_first_worksheet()
        {
            _reportModel.GenerateExcelFileBuilder(HospitalName);

            _excelFileBuilder.Received().GetFirstWorksheet();
        }

        [TestMethod]
        public void Then_the_header_row_matches_requirements()
        {
            _reportModel.GenerateExcelFileBuilder(HospitalName);

            Received.InOrder(() =>
            {
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 1, 1, "BVA Results for: ", CellStyle.Bold);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 1, 2, HospitalName, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 1, 3, "From: " + _fromDateTime.ToString(Constants.DateRangeFormat), CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 1, 4, "To: " + _toDateTime.ToString(Constants.DateRangeFormat), CellStyle.None);
            });
        }

        [TestMethod]
        public void Then_the_exported_on_row_matches_requirements()
        {
            _reportModel.GenerateExcelFileBuilder(HospitalName);

            Received.InOrder(() =>
            {
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 2, 1, "Exported On: ", CellStyle.Bold);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 2, 2, DateTime.Now.ToString("g"), CellStyle.None);
            });
        }

        [TestMethod]
        public void Then_the_version_row_matches_requirements()
        {
            _reportModel.GenerateExcelFileBuilder(HospitalName);

            Received.InOrder(() =>
            {
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 3, 1, "Version: ", CellStyle.Bold);

                var version = Assembly.GetExecutingAssembly().GetName().Version;
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 3, 2, version.ToString(), CellStyle.None);
            });
        }

        [TestMethod]
        public void Then_the_column_headers_match_requirements()
        {
            _reportModel.GenerateExcelFileBuilder(HospitalName);

            Received.InOrder(() =>
            {
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 5, 1, OrderingPhysiciansColumnHeader.PhysiciansName.GetStringValue(), CellStyle.ColumnHeading);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 5, 2, OrderingPhysiciansColumnHeader.PhysiciansSpecialty.GetStringValue(), CellStyle.ColumnHeading);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 5, 3, OrderingPhysiciansColumnHeader.TestDate.GetStringValue(), CellStyle.ColumnHeading);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 5, 4, OrderingPhysiciansColumnHeader.Analyst.GetStringValue(), CellStyle.ColumnHeading);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 5, 5, OrderingPhysiciansColumnHeader.CcReportTo.GetStringValue(), CellStyle.ColumnHeading);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 5, 6, OrderingPhysiciansColumnHeader.Location.GetStringValue(), CellStyle.ColumnHeading);
            });
        }

        [TestMethod]
        public void Then_the_physician_data_is_added_properly()
        {
            _reportModel.GenerateExcelFileBuilder(HospitalName);

            Received.InOrder(() =>
            {
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 6, 1, _dataRecord1.ReferringPhysician, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 6, 2, _dataRecord1.PhysicianSpecialty, CellStyle.None);
                _excelFileBuilder.InsertDateTimeCell(Arg.Any<Worksheet>(), 6, 3, _testRecord1.TestDate, CellStyle.UsDate);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 6, 4, _testRecord1.Analyst, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 6, 5, _testRecord1.CcReportTo, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 6, 6, _testRecord1.Location, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 7, 1, _dataRecord1.ReferringPhysician, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 7, 2, _dataRecord1.PhysicianSpecialty, CellStyle.None);
                _excelFileBuilder.InsertDateTimeCell(Arg.Any<Worksheet>(), 7, 3, _testRecord2.TestDate, CellStyle.UsDate);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 7, 4, _testRecord2.Analyst, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 7, 5, _testRecord2.CcReportTo, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 7, 6, _testRecord2.Location, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 8, 1, _dataRecord2.ReferringPhysician, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 8, 2, _dataRecord2.PhysicianSpecialty, CellStyle.None);
                _excelFileBuilder.InsertDateTimeCell(Arg.Any<Worksheet>(), 8, 3, _testRecord1.TestDate, CellStyle.UsDate);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 8, 4, _testRecord1.Analyst, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 8, 5, _testRecord1.CcReportTo, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 8, 6, _testRecord1.Location, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 9, 1, _dataRecord2.ReferringPhysician, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 9, 2, _dataRecord2.PhysicianSpecialty, CellStyle.None);
                _excelFileBuilder.InsertDateTimeCell(Arg.Any<Worksheet>(), 9, 3, _testRecord2.TestDate, CellStyle.UsDate);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 9, 4, _testRecord2.Analyst, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 9, 5, _testRecord2.CcReportTo, CellStyle.None);
                _excelFileBuilder.InsertStringCell(Arg.Any<Worksheet>(), 9, 6, _testRecord2.Location, CellStyle.None);
            });
        }
    }
    // ReSharper restore RedundantArgumentName
}