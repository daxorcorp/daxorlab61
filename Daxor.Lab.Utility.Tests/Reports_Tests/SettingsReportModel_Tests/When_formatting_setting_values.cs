﻿using System;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Utility.Models;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.SettingsReportModel_Tests
{
    [TestClass]
    // ReSharper disable once InconsistentNaming
    public class When_formatting_setting_values
    {
        [TestMethod]
        public void And_the_value_type_is_date_then_the_short_date_is_returned()
        {
            var dateSetting = new SingleValueSetting("key", new DateTime(2013, 1, 12), "date", AuthorizationLevel.Internal);
            
            var formattedSettingValue = SettingsReportModel.FormatSettingValue(dateSetting);

            Assert.AreEqual("1/12/2013", formattedSettingValue);
        }

        [TestMethod]
        public void And_the_value_type_is_time_then_the_short_time_is_returned()
        {
            var timeSetting = new SingleValueSetting("key", new DateTime(2013, 12, 1, 12, 1, 0), "time", AuthorizationLevel.Internal);
            
            var formattedSettingValue = SettingsReportModel.FormatSettingValue(timeSetting);

            Assert.AreEqual("12:01 PM", formattedSettingValue);
        }

        [TestMethod]
        public void And_the_setting_is_a_choice_setting_then_the_setting_value_is_returned()
        {
            var choiceSetting = new ChoiceSetting("key", 3, "collection", AuthorizationLevel.Internal, "1:one|2:two|3:three");

            var formattedSettingValue = SettingsReportModel.FormatSettingValue(choiceSetting);

            Assert.AreEqual("three", formattedSettingValue);
        }

        [TestMethod]
        public void And_the_value_type_is_boolean_then_the_uppercase_truth_value_is_returned()
        {
            var trueSetting = new SingleValueSetting("key", true, "boolean", AuthorizationLevel.Internal);
            var falseSetting = new SingleValueSetting("key", false, "boolean", AuthorizationLevel.Internal);

            var formattedTrueSettingValue = SettingsReportModel.FormatSettingValue(trueSetting);
            var formattedFalseSettingValue = SettingsReportModel.FormatSettingValue(falseSetting);

            Assert.AreEqual("True", formattedTrueSettingValue);
            Assert.AreEqual("False", formattedFalseSettingValue);
        }

        [TestMethod]
        public void And_the_value_type_is_integer_then_the_value_is_returned()
        {
            var integerSetting = new SingleValueSetting("key", 1234, "integer", AuthorizationLevel.Internal);

            var formattedSettingValue = SettingsReportModel.FormatSettingValue(integerSetting);

            Assert.AreEqual("1234", formattedSettingValue);
        }

        [TestMethod]
        public void And_the_value_type_is_double_then_the_value_is_returned()
        {
            var doubleSetting = new SingleValueSetting("key", 1234.5678, "double", AuthorizationLevel.Internal);

            var formattedSettingValue = SettingsReportModel.FormatSettingValue(doubleSetting);

            Assert.AreEqual("1234.5678", formattedSettingValue);
        }

        [TestMethod]
        public void And_the_value_type_is_string_then_the_value_is_returned()
        {
            const string settingValue = "one two three!";
            var stringSetting = new SingleValueSetting("key", settingValue, "string", AuthorizationLevel.Internal);

            var formattedSettingValue = SettingsReportModel.FormatSettingValue(stringSetting);

            Assert.AreEqual(settingValue, formattedSettingValue);
        }

        [TestMethod]
        public void And_the_value_type_is_datetime_then_the_value_is_returned()
        {
            var settingValue = new DateTime(2013, 12, 1, 12, 1, 0);
            var stringSetting = new SingleValueSetting("key", settingValue, "datetime", AuthorizationLevel.Internal);

            var formattedSettingValue = SettingsReportModel.FormatSettingValue(stringSetting);

            Assert.AreEqual(settingValue.ToString(), formattedSettingValue);
        }
    }
}
