using System;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.SettingsReportModel_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_formatting_setting_values
    {
        [TestMethod]
        public void And_the_setting_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>SettingsReportModel.FormatSettingValue(null), "setting");
        }
    }
    // ReSharper restore InconsistentNaming
}
