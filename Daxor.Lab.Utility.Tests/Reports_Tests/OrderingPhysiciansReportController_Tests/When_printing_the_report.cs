using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_printing_the_report
    {
        private ILoggerFacade _dummyLogger;
        private IRegionManager _dummyRegionManager;
        private IEventAggregator _dummyEventAggregator;
        private OrderingPhysiciansReportModel _dummyReportModel;
        private IReportProcessor _dumReportProcessor;
        private IMessageBoxDispatcher _dummyMessageBoxDispatcher;
        private IFolderBrowser _dummyFolderBrowser;
        private IMessageManager _dummyMessageManager;
        private IStarBurnWrapper _dummyStarBurnWrapper;

        [TestInitialize]
        public void Initialize()
        {
            _dummyLogger = Substitute.For<ILoggerFacade>();
            _dummyRegionManager = Substitute.For<IRegionManager>();
            _dummyEventAggregator = Substitute.For<IEventAggregator>();
            _dummyReportModel = new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, new List<OrderingPhysicianDataRecord>(), Substitute.For<IExcelFileBuilder>());
            _dumReportProcessor = Substitute.For<IReportProcessor>();
            _dummyStarBurnWrapper = Substitute.For<IStarBurnWrapper>();
            _dummyMessageManager = Substitute.For<IMessageManager>();
            _dummyMessageBoxDispatcher = Substitute.For<IMessageBoxDispatcher>();
            _dummyFolderBrowser = Substitute.For<IFolderBrowser>();
        }

        [TestMethod]
        public void Then_the_report_data_is_loaded_first()
        {
            var stubSettingsManager = Substitute.For<ISettingsManager>();
            stubSettingsManager.GetSetting<string>(SettingKeys.SystemHospitalName).Returns("test");

            var reportController = new OrderingPhysiciansReportController(_dummyLogger,
                _dummyRegionManager,
                _dummyEventAggregator,
                stubSettingsManager,
                _dummyReportModel,
                _dumReportProcessor,
                _dummyMessageBoxDispatcher,
                _dummyFolderBrowser,
                _dummyMessageManager,
                _dummyStarBurnWrapper);
            var report = reportController.ReportToView as OrderingPhysiciansReport;

            reportController.PrintReport();

            // ReSharper disable once PossibleNullReferenceException
            Assert.AreEqual("test", report.textBoxHospitalName.Value);
        }

        [TestMethod]
        public void Then_the_report_processor_handles_the_printing()
        {
            var mockReportProcessor = Substitute.For<IReportProcessor>();
            
            var reportController = new OrderingPhysiciansReportController(_dummyLogger,
                _dummyRegionManager,
                _dummyEventAggregator,
                Substitute.For<ISettingsManager>(),
                _dummyReportModel,
                mockReportProcessor,
                _dummyMessageBoxDispatcher,
                _dummyFolderBrowser,
                _dummyMessageManager,
                _dummyStarBurnWrapper);
            var report = reportController.ReportToView as OrderingPhysiciansReport;

            reportController.PrintReport();

            mockReportProcessor.Received().PrintReport(report);
        }
    }
    // ReSharper restore InconsistentNaming
}
