﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Telerik.Reporting;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_generating_the_physicians_sub_report_data
    {
        private ISettingsManager _stubSettingsManager;
        private OrderingPhysicianDataRecord _orderingPhysicianDataRecordWithMoreThanTheMaximumTests;
        private OrderingPhysicianDataRecord _orderingPhysicianDataRecordWithJustAFewTests;
        private List<OrderingPhysicianDataRecord> _stubListWithOneSmallItemAndOneToBigItem;
        private OrderingPhysiciansReportModel _orderingPhysiciansReportModel;
        private OrderingPhysiciansReportControllerThatExposesGenerateOrderingPhysiciansSubReports _orderingPhysiciansReportController;

        [TestInitialize]
        public void Initialize()
        {
            _stubSettingsManager = Substitute.For<ISettingsManager>();
            _orderingPhysicianDataRecordWithMoreThanTheMaximumTests = new OrderingPhysicianDataRecord
            {
                TestRecordsList = new List<OrderingPhysicianTestRecord>()
            };

            for (var i = 0; i < OrderingPhysiciansReportController.MaximumNumberOfRecordsThatFitOnAPage + 5; i++)
                _orderingPhysicianDataRecordWithMoreThanTheMaximumTests.TestRecordsList.Add(new OrderingPhysicianTestRecord());

            _orderingPhysicianDataRecordWithJustAFewTests = new OrderingPhysicianDataRecord
            {
                TestRecordsList = new List<OrderingPhysicianTestRecord>()
            };

            for (var i = 0; i < 5; i++)
                _orderingPhysicianDataRecordWithJustAFewTests.TestRecordsList.Add(new OrderingPhysicianTestRecord());

            _stubListWithOneSmallItemAndOneToBigItem = new List<OrderingPhysicianDataRecord> { _orderingPhysicianDataRecordWithJustAFewTests, _orderingPhysicianDataRecordWithMoreThanTheMaximumTests };

            _orderingPhysiciansReportModel =
                new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, _stubListWithOneSmallItemAndOneToBigItem, Substitute.For<IExcelFileBuilder>());

            _orderingPhysiciansReportController = new OrderingPhysiciansReportControllerThatExposesGenerateOrderingPhysiciansSubReports(_stubSettingsManager, _orderingPhysiciansReportModel);
        }

        [TestMethod]
        public void And_there_is_a_record_with_more_than_the_maximum_tests_added_to_the_page_after_a_record_that_fits_on_the_page_by_itself_then_the_subreports_are_split_properly()
        {
            var observedSubReports = _orderingPhysiciansReportController.GetSubReports().ToArray();

            var expectedNumberOfElementsInFirstSubReportTestList = _orderingPhysicianDataRecordWithJustAFewTests.TestRecordsList.Count;

            var expectedNumberOfElementsInSecondSubReportTestList = OrderingPhysiciansReportController.MaximumNumberOfRecordsThatFitOnAPage -
                                                                    expectedNumberOfElementsInFirstSubReportTestList -
                                                                    (2*OrderingPhysiciansReportController.NumberOfRecordsThatSubReportHeadersOccupy);

            var expectedNumberOfElementsInThirdSubReportTestList = _orderingPhysicianDataRecordWithMoreThanTheMaximumTests.TestRecordsList.Count -
                                                                   expectedNumberOfElementsInSecondSubReportTestList;

            Assert.AreEqual(expectedNumberOfElementsInFirstSubReportTestList, ((List<OrderingPhysicianTestRecord>)((SubReport_OrderingPhysician)((InstanceReportSource)(observedSubReports[0].ReportSource)).ReportDocument).tableTestRecord.DataSource).Count);
            Assert.AreEqual(expectedNumberOfElementsInSecondSubReportTestList, ((List<OrderingPhysicianTestRecord>)((SubReport_OrderingPhysician)((InstanceReportSource)(observedSubReports[1].ReportSource)).ReportDocument).tableTestRecord.DataSource).Count);
            Assert.AreEqual(expectedNumberOfElementsInThirdSubReportTestList, ((List<OrderingPhysicianTestRecord>)((SubReport_OrderingPhysician)((InstanceReportSource)(observedSubReports[2].ReportSource)).ReportDocument).tableTestRecord.DataSource).Count);
        }

        [TestMethod]
        public void And_there_is_a_record_with_more_than_the_maximum_tests_added_to_the_page_after_a_record_that_fits_on_the_page_by_itself_then_the_subreports_contain_the_data_after_being_split()
        {
            var observedSubReports = _orderingPhysiciansReportController.GetSubReports().ToArray();

            var expectedNumberOfElementsInFirstSubReportTestList = _orderingPhysicianDataRecordWithJustAFewTests.TestRecordsList.Count;

            var expectedNumberOfElementsInSecondSubReportTestList = OrderingPhysiciansReportController.MaximumNumberOfRecordsThatFitOnAPage -
                                                                    expectedNumberOfElementsInFirstSubReportTestList -
                                                                    (2 * OrderingPhysiciansReportController.NumberOfRecordsThatSubReportHeadersOccupy);

            CollectionAssert.AreEqual(_orderingPhysicianDataRecordWithMoreThanTheMaximumTests.TestRecordsList.Take(expectedNumberOfElementsInSecondSubReportTestList).ToList(), ((List<OrderingPhysicianTestRecord>)((SubReport_OrderingPhysician)((InstanceReportSource)(observedSubReports[1].ReportSource)).ReportDocument).tableTestRecord.DataSource));
            CollectionAssert.AreEqual(_orderingPhysicianDataRecordWithMoreThanTheMaximumTests.TestRecordsList.Skip(expectedNumberOfElementsInSecondSubReportTestList).ToList(), ((List<OrderingPhysicianTestRecord>)((SubReport_OrderingPhysician)((InstanceReportSource)(observedSubReports[2].ReportSource)).ReportDocument).tableTestRecord.DataSource));
        }
    }

    // ReSharper restore RedundantArgumentName
}