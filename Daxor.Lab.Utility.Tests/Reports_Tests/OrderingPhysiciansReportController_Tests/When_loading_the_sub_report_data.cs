﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_loading_the_sub_report_data
    {
        private ISettingsManager _stubSettingsManager;

        [TestInitialize]
        public void Initialize()
        {
            _stubSettingsManager = Substitute.For<ISettingsManager>();
        }

        [TestMethod]
        public void And_there_are_no_records_in_the_collection_then_only_the_summary_is_added_to_the_detail_section()
        {
            var stubReportModelWithAnEmptyOrderingPhysiciansDataRecordsList =
                new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, new List<OrderingPhysicianDataRecord>(), Substitute.For<IExcelFileBuilder>());
            var _orderingPhysiciansReportController = new OrderingPhysiciansReportController(
                Substitute.For<ILoggerFacade>(), Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(), _stubSettingsManager,
                stubReportModelWithAnEmptyOrderingPhysiciansDataRecordsList,
                Substitute.For<IReportProcessor>(),
                Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(),
                Substitute.For<IMessageManager>(),
                Substitute.For<IStarBurnWrapper>());

            _orderingPhysiciansReportController.LoadReportData();

            Assert.IsTrue(((OrderingPhysiciansReport)_orderingPhysiciansReportController.ReportToView).detail.Items.Count == 1);
        }

        [TestMethod]
        public void And_there_are_records_in_the_collection_then_the_summary_and_one_subreport_for_each_item_are_added_to_the_detail_section()
        {
            var stubOrderingPhysicianDataRecord = new OrderingPhysicianDataRecord
            {
                TestRecordsList = new List<OrderingPhysicianTestRecord> { new OrderingPhysicianTestRecord() }
            };

            var stubListWithOneItem = new List<OrderingPhysicianDataRecord> { stubOrderingPhysicianDataRecord };
            var reportModelWithOneOrderingPhysiciansDataRecord =
                new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, stubListWithOneItem, Substitute.For<IExcelFileBuilder>());
            var _orderingPhysiciansReportController = new OrderingPhysiciansReportController(
                Substitute.For<ILoggerFacade>(), Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(), _stubSettingsManager,
                reportModelWithOneOrderingPhysiciansDataRecord,
                Substitute.For<IReportProcessor>(),
                Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(),
                Substitute.For<IMessageManager>(),
                Substitute.For<IStarBurnWrapper>());

            _orderingPhysiciansReportController.LoadReportData();

            Assert.IsTrue(((OrderingPhysiciansReport)_orderingPhysiciansReportController.ReportToView).detail.Items.Count == 2);
        }

        [TestMethod]
        public void And_there_is_a_record_with_more_than_the_maximum_tests_at_the_beginning_of_the_collection_then_the_summary_and_2_sub_report_items_are_added_to_the_detail_section_for_that_physician()
        {
            var orderingPhysicianDataRecordWithMoreThanTheMaximumTests = new OrderingPhysicianDataRecord
            {
                TestRecordsList = new List<OrderingPhysicianTestRecord>()
            };

            for (var i = 0; i < OrderingPhysiciansReportController.MaximumNumberOfRecordsThatFitOnAPage + 5; i++)
                orderingPhysicianDataRecordWithMoreThanTheMaximumTests.TestRecordsList.Add(new OrderingPhysicianTestRecord());

            var stubListWithOneItem = new List<OrderingPhysicianDataRecord> { orderingPhysicianDataRecordWithMoreThanTheMaximumTests };

            var reportModelWithOneOrderingPhysiciansDataRecord =
                new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, stubListWithOneItem, Substitute.For<IExcelFileBuilder>());

            var _orderingPhysiciansReportController = new OrderingPhysiciansReportController(
                Substitute.For<ILoggerFacade>(), Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(), _stubSettingsManager,
                reportModelWithOneOrderingPhysiciansDataRecord,
                Substitute.For<IReportProcessor>(),
                Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(),
                Substitute.For<IMessageManager>(),
                Substitute.For<IStarBurnWrapper>());

            _orderingPhysiciansReportController.LoadReportData();

            Assert.IsTrue(((OrderingPhysiciansReport)_orderingPhysiciansReportController.ReportToView).detail.Items.Count == 3);
        }
    }
    // ReSharper restore RedundantArgumentName
}