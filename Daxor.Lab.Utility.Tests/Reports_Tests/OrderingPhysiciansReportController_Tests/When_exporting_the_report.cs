using System;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests.Stubs;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_exporting_the_report
    {
        private IFolderBrowser _stubFolderBrowser;

        [TestInitialize]
        public void Initialize()
        {
            _stubFolderBrowser = Substitute.For<IFolderBrowser>();
            _stubFolderBrowser.IsPathValid().Returns(true);
            _stubFolderBrowser.IsOpticalDrive = true;
        }

        #region And the export fails ...

        [TestMethod]
        public void And_the_export_fails_then_the_busy_indicator_is_disabled()
        {
            var mockEventAggregator = Substitute.For<IEventAggregator>();
            var stubEvent = Substitute.For<BusyStateChanged>();
            mockEventAggregator.GetEvent<BusyStateChanged>()
                .Returns(stubEvent);

            var controller = new OrderingPhysiciansReportControllerThatFailsToExport(mockEventAggregator, _stubFolderBrowser);

            controller.ExportReport();

            mockEventAggregator.Received().GetEvent<BusyStateChanged>();
            stubEvent.Received().Publish(Arg.Is<BusyPayload>(p=>!p.IsBusy));
        }

        [TestMethod]
        public void And_the_export_fails_then_the_correct_message_is_displayed()
        {
            var errorMessage = new Message("key", "description", "fix it", "id", MessageType.Alert);

            var stubMessageManager = Substitute.For<IMessageManager>();
            stubMessageManager.GetMessage(MessageKeys.BvaExportUnsuccessful).Returns(errorMessage);

            var mockMessageBoxDispatcher = Substitute.For<IMessageBoxDispatcher>();

            var failingController = new OrderingPhysiciansReportControllerThatFailsToExport(mockMessageBoxDispatcher,
                _stubFolderBrowser, stubMessageManager);

            failingController.ExportReport();

            stubMessageManager.Received().GetMessage(MessageKeys.BvaExportUnsuccessful);
            mockMessageBoxDispatcher.Received().ShowMessageBox(Arg.Is<MessageBoxCategory>(x=> x == MessageBoxCategory.Error),
                                                               Arg.Is<string>(x=>x==errorMessage.FormattedMessage),
                                                               Arg.Is<string>(x=>x==OrderingPhysiciansReportController.ExportOrderingPhysiciansReportMessageBoxCaption),
                                                               Arg.Is<MessageBoxChoiceSet>(x => x== MessageBoxChoiceSet.Close));
        }

        [TestMethod]
        public void And_the_export_fails_then_the_exception_is_logged()
        {
            var errorMessage = new Message("key", "description", "fix it", "id", MessageType.Alert);

            var stubMessageManager = Substitute.For<IMessageManager>();
            stubMessageManager.GetMessage(MessageKeys.BvaExportUnsuccessful).Returns(errorMessage);

            var logMessage = String.Format("{0}; inner exception: {1}", errorMessage.FormattedMessage,
                OrderingPhysiciansReportControllerThatFailsToExport.ExceptionMessage);

            var mockLogger = Substitute.For<ILoggerFacade>();

            var failingReportController = new OrderingPhysiciansReportControllerThatFailsToExport(mockLogger,
                _stubFolderBrowser, stubMessageManager);

            failingReportController.ExportReport();

            mockLogger.Received().Log(logMessage, Category.Exception, Priority.High);
        }

        [TestMethod]
        public void And_the_optical_drive_fails_then_the_export_fails()
        {
            var stubDriveWrapper = Substitute.For<IStarBurnWrapper>();
            stubDriveWrapper.When(w => w.CreateDataBurner())
                .Do(x => {throw new Exception(OrderingPhysiciansReportControllerWithCustomizedExportFaultedMessage.FailureMessage);});

            var stubEventAgregattor = Substitute.For<IEventAggregator>();
            stubEventAgregattor.GetEvent<BusyStateChanged>().Returns(new BusyStateChanged());

            var failingController =
                new OrderingPhysiciansReportControllerWithCustomizedExportFaultedMessage(stubDriveWrapper,
                    stubEventAgregattor, _stubFolderBrowser);

            failingController.ExportReport();

            Assert.IsTrue(failingController.FailedWithCorrectMessage);

        }

        #endregion

        #region And the export is successful ...

        [TestMethod]
        public void Then_the_busy_state_is_set_to_true_before_the_export()
        {
            var stubEventAggregator = Substitute.For<IEventAggregator>();
            var stubEvent = Substitute.For<BusyStateChanged>();
            stubEventAggregator.GetEvent<BusyStateChanged>()
                .Returns(stubEvent);

            var stubSettingsManager = Substitute.For<ISettingsManager>();

            var controller = new OrderingPhysiciansReportControllerThatEportsWithStubbedMethods(stubEventAggregator, stubSettingsManager, _stubFolderBrowser);

            controller.ExportReport();

            stubEventAggregator.Received().GetEvent<BusyStateChanged>();
            stubEvent.Received().Publish(Arg.Is<BusyPayload>(p => p.IsBusy && p.Message == "Exporting Report..."));
        }

        [TestMethod]
        public void Then_the_temp_file_path_is_retrieved_from_the_settings_manager()
        {
            var stubEventAggregator = Substitute.For<IEventAggregator>();
            var stubEvent = Substitute.For<BusyStateChanged>();
            stubEventAggregator.GetEvent<BusyStateChanged>()
                .Returns(stubEvent);
            var stubSettingsManager = Substitute.For<ISettingsManager>();

            var controller = new OrderingPhysiciansReportControllerThatEportsWithStubbedMethods(stubEventAggregator, stubSettingsManager, _stubFolderBrowser);

            controller.ExportReport();

            stubSettingsManager.Received().GetSetting<string>(SettingKeys.SystemTempFilePath);
        }

        [TestMethod]
        public void Then_the_busy_state_is_disabled_when_the_export_completes()
        {
            var stubEventAggregator = Substitute.For<IEventAggregator>();
            var stubEvent = Substitute.For<BusyStateChanged>();
            stubEventAggregator.GetEvent<BusyStateChanged>()
                .Returns(stubEvent);

            var stubSettingsManager = Substitute.For<ISettingsManager>();

            var controller = new OrderingPhysiciansReportControllerThatEportsWithStubbedMethods(stubEventAggregator, stubSettingsManager, _stubFolderBrowser);

            controller.ExportReport();

            Received.InOrder(() =>
            {
                stubEventAggregator.GetEvent<BusyStateChanged>();
                stubEvent.Publish(Arg.Is<BusyPayload>(p => p.IsBusy && p.Message == "Exporting Report..."));
                stubEventAggregator.GetEvent<BusyStateChanged>();
                stubEvent.Publish(Arg.Is<BusyPayload>(p => !p.IsBusy));
            });
        }

        [TestMethod]
        public void Then_the_pdf_report_is_rendered_to_the_file()
        {
            var stubEventAggregator = Substitute.For<IEventAggregator>();
            var stubEvent = Substitute.For<BusyStateChanged>();
            stubEventAggregator.GetEvent<BusyStateChanged>()
                .Returns(stubEvent);
            var stubSettingsManager = Substitute.For<ISettingsManager>();

            var controller = new OrderingPhysiciansReportControllerThatEportsWithStubbedMethods(stubEventAggregator,
                stubSettingsManager, _stubFolderBrowser);

            controller.ExportReport();

            Assert.IsTrue(controller.PdfRenderedToFile);
        }

        [TestMethod]
        public void Then_the_excel_report_is_saved_to_the_file()
        {
            var stubEventAggregator = Substitute.For<IEventAggregator>();
            var stubEvent = Substitute.For<BusyStateChanged>();
            stubEventAggregator.GetEvent<BusyStateChanged>()
                .Returns(stubEvent);
            var stubSettingsManager = Substitute.For<ISettingsManager>();

            var controller = new OrderingPhysiciansReportControllerThatEportsWithStubbedMethods(stubEventAggregator,
                stubSettingsManager, _stubFolderBrowser);

            controller.ExportReport();

            Assert.IsTrue(controller.ExcelReportSavedToFile);
        }

        [TestMethod]
        public void And_the_drive_is_optical_then_the_files_are_burned_to_optical_media()
        {
            var stubEventAggregator = Substitute.For<IEventAggregator>();
            var stubEvent = Substitute.For<BusyStateChanged>();
            stubEventAggregator.GetEvent<BusyStateChanged>()
                .Returns(stubEvent);
            var stubSettingsManager = Substitute.For<ISettingsManager>();

            var controller = new OrderingPhysiciansReportControllerThatEportsWithStubbedMethods(stubEventAggregator,
                stubSettingsManager, _stubFolderBrowser);

            controller.ExportReport();

            Assert.IsTrue(controller.BurnedFilesToOpticalMedia);
            Assert.IsFalse(controller.CopiedFiledToNonOpticalMedia);
        }

        [TestMethod]
        public void And_the_drive_is_not_optical_then_the_files_are_copied_to_non_optical_media()
        {
            var stubEventAggregator = Substitute.For<IEventAggregator>();
            var stubEvent = Substitute.For<BusyStateChanged>();
            stubEventAggregator.GetEvent<BusyStateChanged>()
                .Returns(stubEvent);
            var stubSettingsManager = Substitute.For<ISettingsManager>();

            _stubFolderBrowser.IsOpticalDrive = false;

            var controller = new OrderingPhysiciansReportControllerThatEportsWithStubbedMethods(stubEventAggregator,
                stubSettingsManager, _stubFolderBrowser);

            controller.ExportReport();

            Assert.IsTrue(controller.CopiedFiledToNonOpticalMedia);
            Assert.IsFalse(controller.BurnedFilesToOpticalMedia);
        }

        [TestMethod]
        public void Then_the_temp_files_are_cleaned_up()
        {
            var stubEventAggregator = Substitute.For<IEventAggregator>();
            var stubEvent = Substitute.For<BusyStateChanged>();
            stubEventAggregator.GetEvent<BusyStateChanged>()
                .Returns(stubEvent);
            var stubSettingsManager = Substitute.For<ISettingsManager>();

            var controller = new OrderingPhysiciansReportControllerThatEportsWithStubbedMethods(stubEventAggregator,
                stubSettingsManager, _stubFolderBrowser);

            controller.ExportReport();

            Assert.IsTrue(controller.CleanedUpTempFilesAfterExport);
        }

        [TestMethod]
        public void Then_the_correct_message_is_displayed_when_the_export_completes_successfully()
        {
            var stubEventAggregator = Substitute.For<IEventAggregator>();
            var stubEvent = Substitute.For<BusyStateChanged>();
            stubEventAggregator.GetEvent<BusyStateChanged>()
                .Returns(stubEvent);
            var stubSettingsManager = Substitute.For<ISettingsManager>();

            var stubMessageBoxDispatcher = Substitute.For<IMessageBoxDispatcher>();

            var controller = new OrderingPhysiciansReportControllerThatEportsWithStubbedMethods(stubEventAggregator,
                stubSettingsManager, stubMessageBoxDispatcher, _stubFolderBrowser);

            controller.ExportReport();

            stubMessageBoxDispatcher.Received()
                .ShowMessageBox(MessageBoxCategory.Information,
                    OrderingPhysiciansReportController.ExportCompletedSuccessfullyMessage,
                    OrderingPhysiciansReportController.ExportOrderingPhysiciansReportMessageBoxCaption,
                    MessageBoxChoiceSet.Close);
        }

        [TestMethod]
        [RequirementValue("PlaceHolder")]
        public void Then_the_file_name_matches_requirements()
        {
            const string hospitalName = "St. DORO";
            var fromDate = new DateTime(1981, 4, 26);
            var toDate = new DateTime(1982, 4, 26);
            var stubSettingsManager = Substitute.For<ISettingsManager>();

            stubSettingsManager.GetSetting<string>(SettingKeys.SystemHospitalName).Returns(hospitalName);

            var mockController =
                new OrderingPhysiciansReportControllerWhichOnlyBuildsTheFileNameOnExport(stubSettingsManager, fromDate,
                    toDate);

            mockController.ExportReport();

            var expectedFileName = hospitalName + "_" + fromDate.ToString(Constants.DateRangeFormat) + " to " + toDate.ToString(Constants.DateRangeFormat);
            expectedFileName = expectedFileName.Replace("/", "-");

            Assert.AreEqual(expectedFileName, mockController.FileName);
        }

        #endregion
    }

    // ReSharper restore InconsistentNaming
}
