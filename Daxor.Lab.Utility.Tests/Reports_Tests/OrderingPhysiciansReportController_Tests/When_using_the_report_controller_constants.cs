﻿using System;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_report_controller_constants
    {
        [TestMethod]
        [RequirementValue("The export has completed successfully.")]
        public void Then_the_export_completed_successfully_message_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), OrderingPhysiciansReportController.ExportCompletedSuccessfullyMessage);
        }

        [TestMethod]
        [RequirementValue(45)]
        public void Then_the_maximum_number_of_records_that_fit_on_the_page_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), OrderingPhysiciansReportController.MaximumNumberOfRecordsThatFitOnAPage);
        }

        [TestMethod]
        [RequirementValue(3)]
        public void Then_the_number_of_records_that_sub_report_headers_occupy_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), OrderingPhysiciansReportController.NumberOfRecordsThatSubReportHeadersOccupy);
        }
        
        [TestMethod]
        [RequirementValue(4)]
        public void Then_the_number_of_records_that_summary_sub_report_headers_occupy_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), OrderingPhysiciansReportController.NumberOfRecordsThatSummarySubReportHeadersOccupy);
        }

        [TestMethod]
        [RequirementValue("Continued")]
        public void Then_the_sub_report_continued_label_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), OrderingPhysiciansReportController.SubReportContinuedLabel);
        }

        [TestMethod]
        [RequirementValue(" - ")]
        public void Then_the_report_range_seperator_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), OrderingPhysiciansReportController.ReportRangeSeperator);
        }

        [TestMethod]
        [RequirementValue("Export Ordering Physicians Report")]
        public void Then_the_caption_for_the_export_ordering_physicians_report_message_box_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), OrderingPhysiciansReportController.ExportOrderingPhysiciansReportMessageBoxCaption);
        }

        [TestMethod]
        [RequirementValue("Report Date Range: ")]
        public void Then_the_report_date_range_label_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), OrderingPhysiciansReportController.ReportDateRangeLabel);
        }
    }
    // ReSharper restore RedundantArgumentName
}