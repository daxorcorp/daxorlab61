using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_controller
    {
        private ILoggerFacade _dummyLogger;
        private IRegionManager _dummyRegionManager;
        private IEventAggregator _dummyEventAggregator;
        private ISettingsManager _dummySettingsManager;
        private OrderingPhysiciansReportModel _dummyReportModel;
        private IReportProcessor _dummyReportProcessor;
        private IMessageBoxDispatcher _dummyMessageBoxDispatcher;
        private IFolderBrowser _dummyFolderBrowser;
        private IMessageManager _dummyMessageManager;
        private IStarBurnWrapper _dummyStarBurnWrapper;

        [TestInitialize]
        public void Before_each_test()
        {
            _dummyLogger = Substitute.For<ILoggerFacade>();
            _dummyRegionManager = Substitute.For<IRegionManager>();
            _dummyEventAggregator = Substitute.For<IEventAggregator>();
            _dummySettingsManager = Substitute.For<ISettingsManager>();
            _dummyReportModel = new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, new List<OrderingPhysicianDataRecord>(), Substitute.For<IExcelFileBuilder>());
            _dummyReportProcessor = Substitute.For<IReportProcessor>();
            _dummyMessageBoxDispatcher = Substitute.For<IMessageBoxDispatcher>();
            _dummyFolderBrowser = Substitute.For<IFolderBrowser>();
            _dummyMessageManager = Substitute.For<IMessageManager>();
            _dummyStarBurnWrapper = Substitute.For<IStarBurnWrapper>();
        }

        [TestMethod]
        public void And_the_logger_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() =>
            {
                new OrderingPhysiciansReportController(null, _dummyRegionManager, _dummyEventAggregator,
                    _dummySettingsManager, _dummyReportModel, _dummyReportProcessor, _dummyMessageBoxDispatcher, _dummyFolderBrowser,
                    _dummyMessageManager, _dummyStarBurnWrapper);
            }, "logger");
        }

        [TestMethod]
        public void And_the_region_manager_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() =>
            {
                new OrderingPhysiciansReportController(_dummyLogger, null, _dummyEventAggregator,
                    _dummySettingsManager, _dummyReportModel, _dummyReportProcessor, _dummyMessageBoxDispatcher, _dummyFolderBrowser,
                    _dummyMessageManager, _dummyStarBurnWrapper);
            }, "regionManager");
        }

        [TestMethod]
        public void And_the_event_aggregator_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() =>
            {
                new OrderingPhysiciansReportController(_dummyLogger, _dummyRegionManager, null,
                    _dummySettingsManager, _dummyReportModel, _dummyReportProcessor, _dummyMessageBoxDispatcher, _dummyFolderBrowser,
                    _dummyMessageManager, _dummyStarBurnWrapper);
            }, "eventAggregator");
        }

        [TestMethod]
        public void And_the_settings_manager_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() =>
            {
                new OrderingPhysiciansReportController(_dummyLogger, _dummyRegionManager, _dummyEventAggregator,
                    null, _dummyReportModel, _dummyReportProcessor, _dummyMessageBoxDispatcher, _dummyFolderBrowser,
                    _dummyMessageManager, _dummyStarBurnWrapper);
            }, "settingsManager");
        }

        [TestMethod]
        public void And_the_report_model_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() =>
            {
                new OrderingPhysiciansReportController(_dummyLogger, _dummyRegionManager, _dummyEventAggregator,
                    _dummySettingsManager, null, _dummyReportProcessor, _dummyMessageBoxDispatcher, _dummyFolderBrowser,
                    _dummyMessageManager, _dummyStarBurnWrapper);
            }, "orderingPhysiciansReportModel");
        }

        [TestMethod]
        public void And_the_report_processor_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() =>
            {
                new OrderingPhysiciansReportController(_dummyLogger, _dummyRegionManager, _dummyEventAggregator,
                    _dummySettingsManager, _dummyReportModel, null, _dummyMessageBoxDispatcher, _dummyFolderBrowser,
                    _dummyMessageManager, _dummyStarBurnWrapper);
            }, "reportProcessor");
        }

        [TestMethod]
        public void And_the_message_box_dispatcher_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() =>
            {
                new OrderingPhysiciansReportController(_dummyLogger, _dummyRegionManager, _dummyEventAggregator,
                    _dummySettingsManager, _dummyReportModel, _dummyReportProcessor, null, _dummyFolderBrowser,
                    _dummyMessageManager, _dummyStarBurnWrapper);
            }, "messageBoxDispatcher");
        }

        [TestMethod]
        public void And_the_folder_browser_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() =>
            {
                new OrderingPhysiciansReportController(_dummyLogger, _dummyRegionManager, _dummyEventAggregator,
                    _dummySettingsManager, _dummyReportModel, _dummyReportProcessor, _dummyMessageBoxDispatcher, null,
                    _dummyMessageManager, _dummyStarBurnWrapper);
            }, "folderBrowser");
        }

        [TestMethod]
        public void And_the_message_manager_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() =>
            {
                new OrderingPhysiciansReportController(_dummyLogger, _dummyRegionManager, _dummyEventAggregator,
                    _dummySettingsManager, _dummyReportModel, _dummyReportProcessor, _dummyMessageBoxDispatcher, _dummyFolderBrowser,
                    null, _dummyStarBurnWrapper);
            }, "messageManager");
        }

        [TestMethod]
        public void And_the_starburn_wrapper_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() =>
            {
                new OrderingPhysiciansReportController(_dummyLogger, _dummyRegionManager, _dummyEventAggregator,
                    _dummySettingsManager, _dummyReportModel, _dummyReportProcessor, _dummyMessageBoxDispatcher, _dummyFolderBrowser,
                    _dummyMessageManager, null);
            }, "opticalDriveWrapper");
        }
    }
    // ReSharper restore InconsistentNaming
}
