using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using NSubstitute;
using Telerik.Reporting;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests.Stubs
{
    public class OrderingPhysiciansReportControllerThatExposesGenerateOrderingPhysiciansSubReports : OrderingPhysiciansReportController
    {
        public OrderingPhysiciansReportControllerThatExposesGenerateOrderingPhysiciansSubReports(ISettingsManager settingsManager,
            OrderingPhysiciansReportModel reportModel)
            : base(Substitute.For<ILoggerFacade>(), Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(), settingsManager,
                reportModel,
                Substitute.For<IReportProcessor>(),
                Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(),
                Substitute.For<IMessageManager>(),
                Substitute.For<IStarBurnWrapper>())
        {
            
        }

        public IEnumerable<SubReport> GetSubReports()
        {
            return GenerateOrderingPhysiciansSubReports(0);
        }
    }
}