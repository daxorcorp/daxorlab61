using System;
using System.Collections.Generic;
using System.Threading;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests.Stubs
{
    internal class OrderingPhysiciansReportControllerThatFailsToExport : OrderingPhysiciansReportController
    {
        public const string ExceptionMessage = "An exception was thrown";

        public  OrderingPhysiciansReportControllerThatFailsToExport(IEventAggregator eventAggregator, IFolderBrowser folderBrowser)
            : base (Substitute.For<ILoggerFacade>(),
                Substitute.For<IRegionManager>(),
                eventAggregator,
                Substitute.For<ISettingsManager>(),
                new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, new List<OrderingPhysicianDataRecord>(), Substitute.For<IExcelFileBuilder>()),
                Substitute.For<IReportProcessor>(),
                Substitute.For<IMessageBoxDispatcher>(),
                folderBrowser,
                Substitute.For<IMessageManager>(),
                Substitute.For<IStarBurnWrapper>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public OrderingPhysiciansReportControllerThatFailsToExport(IMessageBoxDispatcher messageBoxDispatcher, IFolderBrowser folderBrowser,
            IMessageManager messageManager)
            : base(Substitute.For<ILoggerFacade>(),
                Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(),
                Substitute.For<ISettingsManager>(),
                new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, new List<OrderingPhysicianDataRecord>(), Substitute.For<IExcelFileBuilder>()),
                Substitute.For<IReportProcessor>(),
                messageBoxDispatcher,
                folderBrowser,
                messageManager,
                Substitute.For<IStarBurnWrapper>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public OrderingPhysiciansReportControllerThatFailsToExport(ILoggerFacade loggerFacade, IFolderBrowser folderBrowser,
            IMessageManager messageManager)
            : base(loggerFacade,
                Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(),
                Substitute.For<ISettingsManager>(),
                new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, new List<OrderingPhysicianDataRecord>(), Substitute.For<IExcelFileBuilder>()),
                Substitute.For<IReportProcessor>(),
                Substitute.For<IMessageBoxDispatcher>(),
                folderBrowser,
                messageManager,
                Substitute.For<IStarBurnWrapper>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        protected override void ExportTask(string fileName, IFolderBrowser folderBrowser, string zipFilePath,
            CancellationTokenSource cancellationTokenSource)
        {
            throw new ArgumentException(ExceptionMessage);
        }
    }
}