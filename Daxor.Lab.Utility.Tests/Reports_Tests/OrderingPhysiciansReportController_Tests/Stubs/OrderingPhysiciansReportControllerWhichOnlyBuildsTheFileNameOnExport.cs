using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests
{
    internal class OrderingPhysiciansReportControllerWhichOnlyBuildsTheFileNameOnExport :
        OrderingPhysiciansReportController
    {
        public string FileName { get; set; }
        public OrderingPhysiciansReportControllerWhichOnlyBuildsTheFileNameOnExport(ISettingsManager settingsManager, DateTime fromDateTime, DateTime toDateTime)
            : base(Substitute.For<ILoggerFacade>(),
                Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(),
                settingsManager,
                new OrderingPhysiciansReportModel(fromDateTime, toDateTime, new List<OrderingPhysicianDataRecord>(),
                    Substitute.For<IExcelFileBuilder>()),
                Substitute.For<IReportProcessor>(),
                Substitute.For<IMessageBoxDispatcher>(), 
                Substitute.For<IFolderBrowser>(),
                Substitute.For<IMessageManager>(),
                Substitute.For<IStarBurnWrapper>())
        {
            
        }

        public override void ExportReport()
        {
            FileName =
                BuildFileNameForOrderingPhysiciansReport(
                    SettingsManager.GetSetting<string>(SettingKeys.SystemHospitalName),
                    OrderingPhysiciansReportModel.FromDateTime, OrderingPhysiciansReportModel.ToDateTime);
        }
    }
}