using System;
using System.Collections.Generic;
using System.Threading;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests.Stubs
{
    internal class OrderingPhysiciansReportControllerThatEportsWithStubbedMethods : OrderingPhysiciansReportController
    {
        public OrderingPhysiciansReportControllerThatEportsWithStubbedMethods(IEventAggregator eventAggregator, ISettingsManager settingsManager, IFolderBrowser folderBrowser)
            : base (Substitute.For<ILoggerFacade>(),
                Substitute.For<IRegionManager>(),
                eventAggregator,
                settingsManager,
                new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, new List<OrderingPhysicianDataRecord>(), Substitute.For<IExcelFileBuilder>()),
                Substitute.For<IReportProcessor>(),
                Substitute.For<IMessageBoxDispatcher>(),
                folderBrowser,
                Substitute.For<IMessageManager>(),
                Substitute.For<IStarBurnWrapper>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public OrderingPhysiciansReportControllerThatEportsWithStubbedMethods(IEventAggregator eventAggregator, ISettingsManager settingsManager, IMessageBoxDispatcher messageBoxDispatcher, IFolderBrowser folderBrowser)
            : base(Substitute.For<ILoggerFacade>(),
                Substitute.For<IRegionManager>(),
                eventAggregator,
                settingsManager,
                new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, new List<OrderingPhysicianDataRecord>(), Substitute.For<IExcelFileBuilder>()),
                Substitute.For<IReportProcessor>(),
                messageBoxDispatcher,
                folderBrowser,
                Substitute.For<IMessageManager>(),
                Substitute.For<IStarBurnWrapper>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public bool PdfRenderedToFile { get; set; }
        public bool ExcelReportSavedToFile { get; set; }
        public bool CleanedUpTempFilesAfterExport { get; set; }
        public bool BurnedFilesToOpticalMedia { get; set; }
        public bool CopiedFiledToNonOpticalMedia { get; set; }

        protected override void OnExportTaskFaulted(Exception faultingException)
        {
            throw new AssertFailedException("ExportTaskFailed");
        }

        protected override void SavePdfRenderingToFile(string fileName, string tempPath)
        {
            PdfRenderedToFile = true;
        }

        protected override void SaveExcelReportToFile(string fileName, string tempPath)
        {
            ExcelReportSavedToFile = true;
        }

        protected override void CleanUpAfterExport(string fileName, string tempPath)
        {
            CleanedUpTempFilesAfterExport = true;
        }

        protected override void BurnToOpticalMedia(string fileName, CancellationTokenSource cancellationTokenSource)
        {
            BurnedFilesToOpticalMedia = true;
        }

        protected override void CopyToNonOpticalMedia(string fileName, string zipFilePath, string tempPath)
        {
            CopiedFiledToNonOpticalMedia = true;
        }
    }
}