using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using NSubstitute;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests.Stubs
{
    internal class OrderingPhysiciansReportControllerWithCustomizedExportFaultedMessage :
        OrderingPhysiciansReportController
    {
        public const string FailureMessage = "Turtles";

        public OrderingPhysiciansReportControllerWithCustomizedExportFaultedMessage(IStarBurnWrapper starBurnWrapper, IEventAggregator eventAggregator,
            IFolderBrowser folderBrowser)
            : base(Substitute.For<ILoggerFacade>(),
                Substitute.For<IRegionManager>(),
                eventAggregator,
                Substitute.For<ISettingsManager>(),
                new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, new List<OrderingPhysicianDataRecord>(), Substitute.For<IExcelFileBuilder>()),
                Substitute.For<IReportProcessor>(),
                Substitute.For<IMessageBoxDispatcher>(),
                folderBrowser,
                Substitute.For<IMessageManager>(),
                starBurnWrapper)
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public bool FailedWithCorrectMessage { get; set; }

        protected override void OnExportTaskFaulted(Exception faultingException)
        {
            FailedWithCorrectMessage = faultingException.InnerException.Message == FailureMessage;
        }

        protected override void OnExportTaskCompleted()
        {
            FailedWithCorrectMessage = false;
        }

        protected override void SavePdfRenderingToFile(string fileName, string tempPath)
        {
        }

        protected override void SaveExcelReportToFile(string fileName, string tempPath)
        {
        }

        protected override void CleanUpAfterExport(string fileName, string tempPath)
        {
        }
    }
}