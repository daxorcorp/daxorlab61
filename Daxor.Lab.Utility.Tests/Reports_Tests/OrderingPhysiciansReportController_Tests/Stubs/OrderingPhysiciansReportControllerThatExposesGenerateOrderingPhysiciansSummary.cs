﻿using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using NSubstitute;
using Telerik.Reporting;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests.Stubs
{
    internal class OrderingPhysiciansReportControllerThatExposesGenerateOrderingPhysiciansSummary :
        OrderingPhysiciansReportController
    {
        public OrderingPhysiciansReportControllerThatExposesGenerateOrderingPhysiciansSummary(
            OrderingPhysiciansReportModel reportModel) :
                base(Substitute.For<ILoggerFacade>(), Substitute.For<IRegionManager>(),
                    Substitute.For<IEventAggregator>(), Substitute.For<ISettingsManager>(),
                    reportModel,
                    Substitute.For<IReportProcessor>(),
                    Substitute.For<IMessageBoxDispatcher>(),
                    Substitute.For<IFolderBrowser>(),
                    Substitute.For<IMessageManager>(),
                    Substitute.For<IStarBurnWrapper>())
        {
              
        }

        public SubReport GetSummaryReport()
        {
            return GenerateOrderingPhysiciansSummary();
        }
    }
}