using System;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_date_range_format
    {
        [TestMethod]
        public void Then_the_month_is_not_padded_with_zeroes()
        {
            var date = new DateTime(2014, 6, 16);
            var observedDate = date.ToString(Constants.DateRangeFormat);

            Assert.AreEqual("6/16/14", observedDate);
        }

        [TestMethod]
        public void Then_the_day_is_not_padded_with_zeroes()
        {
            var date = new DateTime(2014, 12, 6);
            var observedDate = date.ToString(Constants.DateRangeFormat);

            Assert.AreEqual("12/6/14", observedDate);
        }

        [TestMethod]
        public void Then_the_year_is_two_digits()
        {
            var date = new DateTime(2014, 12, 6);
            var observedDate = date.ToString(Constants.DateRangeFormat);

            Assert.AreEqual("12/6/14", observedDate);
        }
    }
    // ReSharper restore InconsistentNaming
}
