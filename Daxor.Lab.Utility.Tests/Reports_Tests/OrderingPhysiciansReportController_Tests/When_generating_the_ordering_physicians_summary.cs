﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Telerik.Reporting;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_generating_the_ordering_physicians_summary
    {
        private OrderingPhysicianDataRecord _orderingPhysicianDataRecordOne;
        private OrderingPhysicianDataRecord _orderingPhysicianDataRecordTwo;
        private OrderingPhysicianDataRecord _orderingPhysicianDataRecordThree;
        private List<OrderingPhysicianDataRecord> _stubOrderingPhysiciansList;
        private OrderingPhysiciansReportModel _orderingPhysiciansReportModel;

        private OrderingPhysiciansReportControllerThatExposesGenerateOrderingPhysiciansSummary _orderingPhysiciansReportController;

        [TestInitialize]
        public void Initialize()
        {
            _orderingPhysicianDataRecordOne = new OrderingPhysicianDataRecord
            {
                TestRecordsList = new List<OrderingPhysicianTestRecord>(),
                PhysicianSpecialty = "Cardiology",
                ReferringPhysician = "Stacy Mullinax"
            };

            for (var i = 0; i < OrderingPhysiciansReportController.MaximumNumberOfRecordsThatFitOnAPage + 5; i++)
                _orderingPhysicianDataRecordOne.TestRecordsList.Add(new OrderingPhysicianTestRecord());

            _orderingPhysicianDataRecordTwo = new OrderingPhysicianDataRecord
            {
                TestRecordsList = new List<OrderingPhysicianTestRecord>(),
                PhysicianSpecialty = "Cardiology",
                ReferringPhysician = "Phillip Andreason"
            };

            for (var i = 0; i < 5; i++)
                _orderingPhysicianDataRecordTwo.TestRecordsList.Add(new OrderingPhysicianTestRecord());

            _orderingPhysicianDataRecordThree = new OrderingPhysicianDataRecord
            {
                TestRecordsList = new List<OrderingPhysicianTestRecord>(),
                PhysicianSpecialty = "Nit Picking",
                ReferringPhysician = "Geoff Mazeroff"
            };

            for (var i = 0; i < OrderingPhysiciansReportController.MaximumNumberOfRecordsThatFitOnAPage * 5; i++)
                _orderingPhysicianDataRecordThree.TestRecordsList.Add(new OrderingPhysicianTestRecord());

            _stubOrderingPhysiciansList = new List<OrderingPhysicianDataRecord> { _orderingPhysicianDataRecordTwo, _orderingPhysicianDataRecordOne, _orderingPhysicianDataRecordThree };

            _orderingPhysiciansReportModel =
                new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, _stubOrderingPhysiciansList, Substitute.For<IExcelFileBuilder>());

            _orderingPhysiciansReportController = new OrderingPhysiciansReportControllerThatExposesGenerateOrderingPhysiciansSummary(_orderingPhysiciansReportModel);
        }

        [TestMethod]
        public void Then_the_physicians_test_total_is_set_correctly()
        {
            var observedSummary = _orderingPhysiciansReportController.GetSummaryReport();

            var expectedTotal = _orderingPhysicianDataRecordOne.TestRecordsList.Count +
                                _orderingPhysicianDataRecordTwo.TestRecordsList.Count +
                                _orderingPhysicianDataRecordThree.TestRecordsList.Count;

            Assert.AreEqual(expectedTotal.ToString(CultureInfo.InvariantCulture), ((SubReport_Summary)((InstanceReportSource)(observedSummary.ReportSource)).ReportDocument).textBoxPhysicianTestTotal.Value);
        }

        [TestMethod]
        public void Then_the_specialties_test_total_is_set_correctly()
        {
            var observedSummary = _orderingPhysiciansReportController.GetSummaryReport();

            var expectedTotal = _orderingPhysicianDataRecordOne.TestRecordsList.Count +
                                _orderingPhysicianDataRecordTwo.TestRecordsList.Count +
                                _orderingPhysicianDataRecordThree.TestRecordsList.Count;

            Assert.AreEqual(expectedTotal.ToString(CultureInfo.InvariantCulture), ((SubReport_Summary)((InstanceReportSource)(observedSummary.ReportSource)).ReportDocument).textBoxSpecialtiesTestTotal.Value);
        }

        [TestMethod]
        public void Then_the_physicians_summary_list_is_set_correctly()
        {
            var observedSummary = _orderingPhysiciansReportController.GetSummaryReport();

            var expectedPhysiciansSummaryList = new List<OrderingPhysicianDataRecord> { _orderingPhysicianDataRecordThree, _orderingPhysicianDataRecordOne, _orderingPhysicianDataRecordTwo };

            CollectionAssert.AreEqual(expectedPhysiciansSummaryList, (List<OrderingPhysicianDataRecord>)(((SubReport_Summary)((InstanceReportSource)(observedSummary.ReportSource)).ReportDocument).tablePhysiciansSummaryRecord.DataSource));
        }

        [TestMethod]
        public void Then_the_specialty_summary_list_is_set_correctly()
        {
            var observedSummary = _orderingPhysiciansReportController.GetSummaryReport();

            var expectedSpecialtySummaryList = new List<SpecialtySummaryItem>
            {
                new SpecialtySummaryItem
                {
                    Specialty = "Nit Picking (1)",
                    Sum = _orderingPhysicianDataRecordThree.TestRecordsList.Count
                },
                new SpecialtySummaryItem
                {
                    Specialty = "Cardiology (2)",
                    Sum =
                        _orderingPhysicianDataRecordOne.TestRecordsList.Count +
                        _orderingPhysicianDataRecordTwo.TestRecordsList.Count
                }
            };

            CollectionAssert.AreEqual(expectedSpecialtySummaryList, (List<SpecialtySummaryItem>)(((SubReport_Summary)((InstanceReportSource)(observedSummary.ReportSource)).ReportDocument).tableSpecialtySummary.DataSource));

        }
    }

    // ReSharper restore RedundantArgumentName
}