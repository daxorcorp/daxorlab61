﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Telerik.Reporting.Drawing;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_loading_the_report_data
    {
        private OrderingPhysiciansReportController _reportController;
        private ISettingsManager _mockSettingsManager;
        private readonly DateTime FromDateTime = DateTime.Now;
        private readonly DateTime ToDateTime = DateTime.Now;

        [TestInitialize]
        public void Initialize()
        {
            _mockSettingsManager = Substitute.For<ISettingsManager>();

            _reportController = new OrderingPhysiciansReportController(Substitute.For<ILoggerFacade>(),
                Substitute.For<IRegionManager>(), 
                Substitute.For<IEventAggregator>(), 
                _mockSettingsManager,
                new OrderingPhysiciansReportModel(FromDateTime, ToDateTime, new List<OrderingPhysicianDataRecord>(), Substitute.For<IExcelFileBuilder>()),
                Substitute.For<IReportProcessor>(), Substitute.For<IMessageBoxDispatcher>(), Substitute.For<IFolderBrowser>(),
                Substitute.For<IMessageManager>(), Substitute.For<IStarBurnWrapper>());
        }

        [TestMethod]
        public void Then_the_controller_gets_the_hospital_name_from_the_settings_manager()
        {
            _reportController.LoadReportData();

            _mockSettingsManager.Received(1).GetSetting<string>(SettingKeys.SystemHospitalName);
        }
        
        [TestMethod]
        public void Then_the_date_range_string_is_properly_formatted()
        {
            var expectedString = OrderingPhysiciansReportController.ReportDateRangeLabel + FromDateTime.ToString(Constants.DateRangeFormat) +
                                 OrderingPhysiciansReportController.ReportRangeSeperator + 
                                 ToDateTime.ToString(Constants.DateRangeFormat);

            _reportController.LoadReportData();

            Assert.AreEqual(expectedString, ((OrderingPhysiciansReport)_reportController.ReportToView).textBoxReportDateRange.Value);
        }

        [TestMethod]
        public void Then_the_printed_on_date_is_correct()
        {
            var stubReportModel = Substitute.For<OrderingPhysiciansReportModel>();
            stubReportModel.GeneratePrintedOnDate().Returns("Hello!");

            var reportController = new OrderingPhysiciansReportController(Substitute.For<ILoggerFacade>(),
                Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(),
                Substitute.For<ISettingsManager>(),
                stubReportModel,
                Substitute.For<IReportProcessor>(),
                Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(),
                Substitute.For<IMessageManager>(), 
                Substitute.For<IStarBurnWrapper>());

            reportController.LoadReportData();

            var report = (OrderingPhysiciansReport) reportController.ReportToView;
            Assert.AreEqual("Hello!", report.textBoxPrintedOn.Value);
        }

        [TestMethod]
        public void Then_the_x_and_y_units_are_zero_after_the_report_has_been_loaded()
        {
            var stubOrderingPhysicianDataRecord = new OrderingPhysicianDataRecord
            {
                TestRecordsList = new List<OrderingPhysicianTestRecord>()
            };

            var stubListWithOneItem = new List<OrderingPhysicianDataRecord> { stubOrderingPhysicianDataRecord };
            var reportModelWithOneOrderingPhysiciansDataRecord =
                new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, stubListWithOneItem, Substitute.For<IExcelFileBuilder>());
            var _orderingPhysiciansReportController = new OrderingPhysiciansReportController(
                Substitute.For<ILoggerFacade>(), Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(), Substitute.For<ISettingsManager>(),
                reportModelWithOneOrderingPhysiciansDataRecord,
                Substitute.For<IReportProcessor>(),
                Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(),
                Substitute.For<IMessageManager>(), 
                Substitute.For<IStarBurnWrapper>());

            _orderingPhysiciansReportController.LoadReportData();

            Assert.AreEqual(Unit.Inch(0), _orderingPhysiciansReportController.UnitX);
            Assert.AreEqual(Unit.Inch(0), _orderingPhysiciansReportController.UnitY);
        }
    }
    // ReSharper restore RedundantArgumentName
}