using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Telerik.Reporting.Drawing;

namespace Daxor.Lab.Utility.Tests.Reports_Tests.OrderingPhysiciansReportController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_controller
    {
        private OrderingPhysiciansReportController _controller;

        [TestInitialize]
        public void Before_each_test()
        {
            _controller = new OrderingPhysiciansReportController(Substitute.For<ILoggerFacade>(),
                Substitute.For<IRegionManager>(),
                Substitute.For<IEventAggregator>(),
                Substitute.For<ISettingsManager>(),
                new OrderingPhysiciansReportModel(DateTime.Now, DateTime.Now, new List<OrderingPhysicianDataRecord>(), Substitute.For<IExcelFileBuilder>()),
                Substitute.For<IReportProcessor>(), 
                Substitute.For<IMessageBoxDispatcher>(),
                Substitute.For<IFolderBrowser>(),
                Substitute.For<IMessageManager>(),
                Substitute.For<IStarBurnWrapper>());
        }

        [TestMethod]
        public void Then_the_report_document_is_an_ordering_physicians_report()
        {
            Assert.IsInstanceOfType(_controller.ReportToView, typeof(OrderingPhysiciansReport));
        }

        [TestMethod]
        public void Then_the_x_and_y_units_are_zero()
        {
            Assert.AreEqual(Unit.Inch(0), _controller.UnitX);
            Assert.AreEqual(Unit.Inch(0), _controller.UnitY);
        }
    }
    // ReSharper restore InconsistentNaming
}
