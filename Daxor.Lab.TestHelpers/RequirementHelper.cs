﻿using System.Diagnostics;

namespace Daxor.Lab.TestHelpers
{
    public static class RequirementHelper
    {
        public static string GetRequirementValueAsString()
        {
            var stackTrace = new StackTrace();
            var caller = stackTrace.GetFrame(1).GetMethod();

            var attribute = (RequirementValueAttribute) caller.GetCustomAttributes(typeof(RequirementValueAttribute), true)[0];

            return attribute.GetRequirementValueAsString();
        }

        public static int GetRequirementValueAsInt()
        {
            var stackTrace = new StackTrace();
            var caller = stackTrace.GetFrame(1).GetMethod();

            var attribute = (RequirementValueAttribute)caller.GetCustomAttributes(typeof(RequirementValueAttribute), true)[0];

            return attribute.GetRequirementValueAsInt();
        }

        public static double GetRequirementValueAsDouble()
        {
            var stackTrace = new StackTrace();
            var caller = stackTrace.GetFrame(1).GetMethod();

            var attribute = (RequirementValueAttribute)caller.GetCustomAttributes(typeof(RequirementValueAttribute), true)[0];

            return attribute.GetRequirementValueAsDouble();
        }

        public static char GetRequirementValueAsChar()
        {
            var stackTrace = new StackTrace();
            var caller = stackTrace.GetFrame(1).GetMethod();

            var attribute = (RequirementValueAttribute)caller.GetCustomAttributes(typeof(RequirementValueAttribute), true)[0];

            return attribute.GetRequirementValueAsChar();
        }
    }
}