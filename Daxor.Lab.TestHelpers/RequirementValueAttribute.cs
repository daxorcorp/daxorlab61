﻿using System;

namespace Daxor.Lab.TestHelpers
{
    [AttributeUsage(AttributeTargets.Method)]
    public class RequirementValueAttribute : Attribute
    {
        private readonly object _requirementValue;

        public RequirementValueAttribute(object requirementValue)
        {
            _requirementValue = requirementValue;
        }

        public string GetRequirementValueAsString()
        {
            return _requirementValue.ToString();
        }

        public int GetRequirementValueAsInt()
        {
            return (int)_requirementValue;
        }

        public double GetRequirementValueAsDouble()
        {
            return (double) _requirementValue;
        }

        public char GetRequirementValueAsChar()
        {
            return (char) _requirementValue;
        }
    }
}
