﻿using System;
using Daxor.Lab.Infrastructure.SC;
using Daxor.Lab.Infrastructure.ServiceContracts.MCA;
using Daxor.Lab.Services.MultiChannelAnalyzer;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Services;
using Daxor.Lab.SettingsManager;
using Microsoft.Practices.Composite.Logging;
using Daxor.Lab.Services.MultiChannelAnalyzer.Common;
using Daxor.Lab.SettingsManager.Common;

namespace Daxor.Lab.Services
{
    /// <summary>
    /// Services module; defines essential services for DaxoLab
    /// </summary>
    public class Module : IModule
    {
        #region Fields

        private readonly IUnityContainer _container;
        private readonly IEventAggregator _eventAggregator;
        private readonly IMessageBoxDispatcher _msgDispatcher;
        private readonly ILoggerFacade _logger;

        #endregion

        #region Ctor

        public Module(IUnityContainer container, IEventAggregator eventAggregator, 
                              IMessageBoxDispatcher msgDispatcher, ILoggerFacade logger)
        {
            _container = container;
            _eventAggregator = eventAggregator;
            _msgDispatcher = msgDispatcher;
            _logger = logger;
         }

        #endregion

        #region IModule

        void IModule.Initialize()
        {
            _container.RegisterType<IAuthenticationManager, AuthenticationManager>(new ContainerControlledLifetimeManager());
            _container.RegisterType<ISpectroscopyService, SpectrocopyService>(new ContainerControlledLifetimeManager());
            _container.RegisterType<IBackgroundAcquisitionService, BackgroundAcquisitionService>(new ContainerControlledLifetimeManager());

            //Registering an appropriate multi-channel analyzer service based on system/user settings
            try
            {
                //Check if we should register default mca as Demo or REAL
                if (SystemSettingsManager.Manager.GetSetting<Boolean>(SettingKeys.DetectorIsDemo))
                    _container.RegisterType<IMultiChannelAnalyzerProxy, DemoMultiChannelAnalyzerService>(new ContainerControlledLifetimeManager(),
                        new InjectionConstructor(new XmlSpectrumLoader(@"Modules/Shared/Services/SpectrumFiles/BackgroundSpectrum.xml").Load()));
                else
                   _container.RegisterType<IMultiChannelAnalyzerProxy, MultiChannelAnalyzerWCFProxy>(new ContainerControlledLifetimeManager(), new InjectionConstructor("mcaTCP", _container.Resolve<IEventAggregator>()));
            }
            catch (Exception ex)
            {
                _logger.Log("Failed to register multi channel analyzer service  with container. Error: " + ex.Message, Category.Exception, Priority.High);
            }

            //Registering an appropriate sample-changer service based on system/user settings
            try

            {
                if (SystemSettingsManager.Manager.GetSetting<Boolean>(SettingKeys.SampleChangerIsDemo))
                    _container.RegisterType<ISampleChangerProxy, SampleChangerDemoProxy>(new ContainerControlledLifetimeManager());
                else
                    _container.RegisterType<ISampleChangerProxy, SampleChangerWCFProxy>(new ContainerControlledLifetimeManager(), new InjectionConstructor("scTCP", _container.Resolve<IEventAggregator>()));
            }
            catch (Exception ex)
            {
                _logger.Log("Failed to register sample changer service with container. Error: " + ex.Message, Category.Exception, Priority.High);
            }

            //Registering TestExecution service used for all types of test
            _container.RegisterType<ITestExecutionService, TestExecutionService>(new ContainerControlledLifetimeManager());

            string hardwareServiceErrors;
            if (!this.TryToConfigureAllServices(out hardwareServiceErrors))
            {
                //_msgDispatcher.ShowMessageBox(Infrastructure.DomainModels.MessageBoxCategory.Warning, hardwareServiceErrors, "Important Services Failed To Start Up", Infrastructure.DomainModels.MessageBoxChoiceSet.Close);
                //_eventAggregator.GetEvent<NotificationChangedEvent
               // throw new ModuleInitializeException();   
            }

            try
            {
                //Starting Background acquisition service; used throughout the system
                //_container.Resolve<IBackgroundAcquisitionService>().StartAsync();
            }
            catch (Exception ex)
            {
                _logger.Log("Failed to start background service. Error: " + ex.Message, Category.Exception, Priority.High);
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// Function attemps to connect to all available services:
        /// 1. MultiChannelAnalyzer service
        /// 2. SampleChanger service
        /// </summary>
        /// <param name="configError">Fills this parameter if any errors have occured.</param>
        /// <returns>True in case of successfull configuration attempt; false otherwise</returns>
        bool TryToConfigureAllServices(out string configError)
        {
            bool success = true;
            configError = String.Empty;
            try
            {
                var mcaService = _container.Resolve<IMultiChannelAnalyzerProxy>();
                mcaService.OpenProxyAndSubscribe();
            }
            catch(Exception ex)
            {
                success = false;
                configError = "Failed to connect to the multichannel analyzer service.";
                _logger.Log(String.Format("Error: '{0}'; Detail: '{1}'", configError, ex.Message), Category.Exception, Priority.High);
            }

            //Attempt at connection to sample changer service
            try
            {
                var scService = _container.Resolve<ISampleChangerProxy>();
                scService.OpenProxyAndSubscribe();
            }
            catch(Exception ex)
            {
                success = false;
                if (String.IsNullOrEmpty(configError))
                    configError = "Failed to connect to the sample changer service.";
                else
                    configError = "\nFailed to connect to the sample changer service.";

                _logger.Log(String.Format("Error: '{0}'; Detail: '{1}'", configError, ex.Message), Category.Exception, Priority.High);
            }

            return success;
        }

        #endregion
    }
}
