﻿using System;
using System.ServiceModel;
using Daxor.Lab.Infrastructure.SC;
using Daxor.Lab.SCContracts;
using Microsoft.Practices.Composite.Events;
using Daxor.Lab.Infrastructure.SystemNotifications;
using Daxor.Lab.Infrastructure.Alerts;

namespace Daxor.Lab.Services
{
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, UseSynchronizationContext = false)]
    public class SampleChangerWCFProxy : ISampleChangerProxy, ISampleChangerServiceCallbackEvents
    {
        #region Fields
        private readonly string _endPointName;
        private readonly IEventAggregator _eventAggregator;
        private object _channelLock = new object();
        private string _proxyName = "WCF";   // Commented out as it is never referenced (GMazeroff, 8/12/2010 1:49pm)
                                             // Will be used in the future in Help section (AKhomenko, 8/16/2010 1:47pm)
        private bool _isProxyConnected;
        #endregion

        #region InnerDuplexClient
        private InnerDuplexClient _duplexClient;
        private InstanceContext _iContext;
        private class InnerDuplexClient : DuplexClientBase<ISampleChangerService>
        {
            public InnerDuplexClient(InstanceContext iContext, string endPointConfig)
                : base(iContext, endPointConfig) { }

            public ISampleChangerService SafeChannel
            {
                get
                {
                    return this.Channel;
                }

            }
        }

        private ISampleChangerService InnerChannel
        {
            get { return _duplexClient.SafeChannel; }
        }
        #endregion

        #region Ctor

        public SampleChangerWCFProxy(string endPointName, IEventAggregator eventAggregator)
        {
            _endPointName = endPointName;
            _eventAggregator = eventAggregator;

            // NOTE: This is done to remove warnings during compile time. Until it is used
            // in future builds.
            // GMazeroff 9/2/2010 3:04pm
            if (_proxyName == "WCF") _proxyName = "WCF";
        }
        
        #endregion

        #region ISampleChangerProxy Members

        public event PositionChangeBeginHandler PositionChangeBegin;
        public event PositionChangeEndHandler PositionChangeEnd;
        public event PositionSeekBeginHandler PositionSeekBegin;
        public event PositionSeekEndHandler PositionSeekEnd;
        public event PositionSeekCancelHandler PositionSeekCancel;

        public event ChangerErrorHandler ChangerError;
        public event ProxyFailedHandler ProxyFailed;
        public event ProxyOpenedHandler ProxyOpened;

        public void OpenProxyAndSubscribe()
        {
            //Don't attempt to recreate proxy if a valid proxy already exists
            if (IsProxyConnected) return;

            try
            {
                if (_duplexClient != null && _duplexClient.State == CommunicationState.Faulted)
                {
                    CloseAbortChannel();

                }
                lock (_channelLock)
                {
                    System.Diagnostics.Debug.WriteLine("Connecting to SampleChanger.");

                    _iContext = new InstanceContext(this);
                    _duplexClient = new InnerDuplexClient(_iContext, _endPointName);
                    _duplexClient.InnerDuplexChannel.Opened += new EventHandler(Channel_Opened);
                    _duplexClient.InnerDuplexChannel.Faulted += new EventHandler(Channel_Faulted);
                    _duplexClient.InnerDuplexChannel.Open();

                    System.Diagnostics.Debug.WriteLine("Connected to SampleChanger!");
                }
            }
            catch (EndpointNotFoundException)
            {
                string err = "Failed to find Daxor MultiChannleAnalyzer service residing at the expected location.";

                OnProxyFailed(err);
                //Log here
            }
            catch (CommunicationException)
            {
                string err = "Failed to establish communication with Daxor MultiChannelAnalyzer service.";
                OnProxyFailed(err);
                //Log here
            }
        }

        public bool IsProxyConnected
        {
            get { return _isProxyConnected; }
            set { _isProxyConnected = value; }
        }
        public string Name
        {
            get { return "WCF VERSION 3.3.4"; }
        }

        protected virtual void OnPositionChangeBegin(int oldPosition, int newPosition)
        {
            if (PositionChangeBegin != null)
            {
                PositionChangeBegin(oldPosition, newPosition);
            }
        }
        protected virtual void OnPositionChangeEnd(int newPosition)
        {
            if (PositionChangeEnd != null)
            {
                PositionChangeEnd(newPosition);
            }
        }

        protected virtual void OnPositionSeekBegin(int currPosition, int seekPosition)
        {
            if (PositionSeekBegin != null)
            {
                PositionSeekBegin(currPosition, seekPosition);
            }
        }
        protected virtual void OnPositionSeekEnd(int currPosition)
        {
            if (PositionChangeBegin != null)
            {
                PositionSeekEnd(currPosition);
            }
        }

        protected virtual void OnPositionSeekCancel(int currPosition, int seekPosition)
        {
            if (PositionSeekCancel != null)
            {
                PositionSeekCancel(currPosition, seekPosition);
            }
        }
        protected virtual void OnChangerError(int errorCode, string message)
        {
            if (ChangerError != null)
            {
                ChangerError(errorCode, message);
            }
        }

        protected virtual void OnProxyFailed(string errorMessage)
        {
            if (ProxyFailed != null)
            {
                ProxyFailed(errorMessage);
            }
        }
        protected virtual void OnProxyOpened()
        {
            if (ProxyOpened != null)
            {
                ProxyOpened();
            }
        }

        #endregion

        #region ISampleChangerService Members

        public void PulseSampleChanger()
        {
            this.Invoke(() => this.InnerChannel.PulseSampleChanger());
        }
        public void MoveToPosition(int GoalPosition)
        {
            this.Invoke(() => this.InnerChannel.MoveToPosition(GoalPosition));
        }
        public bool IsMoving()
        {
            return this.Invoke<bool>(() => this.InnerChannel.IsMoving());
        }
        public bool IsOpened()
        {
            return this.Invoke<bool>(() => this.InnerChannel.IsOpened());
        }
        public void StopSampleChanger()
        {
            this.Invoke(() => this.InnerChannel.StopSampleChanger());
        }
        public void Terminate()
        {
            this.Invoke(() => this.InnerChannel.Terminate());
        }

        public int GetCurrentPosition()
        {
            return this.Invoke<int>(() => this.InnerChannel.GetCurrentPosition());
        }
        public TimeSpan GetSleepTime()
        {
            return this.Invoke<TimeSpan>(() => this.InnerChannel.GetSleepTime());
        }
        public void SetSleepTime(TimeSpan tSpan)
        {
            this.Invoke(() => this.InnerChannel.SetSleepTime(tSpan));
        }

        public double GetTestDelay()
        {
            return this.Invoke<double>(() => this.InnerChannel.GetTestDelay());
        }
        public void SetTestDelay(double tDelay)
        {
            this.Invoke(() => this.InnerChannel.SetTestDelay(tDelay));
        }

        public DateTime GetTimeLastPositionAttained()
        {
            return this.Invoke<DateTime>(() => this.InnerChannel.GetTimeLastPositionAttained());
        }

        public string GetVersion()
        {
            return this.Invoke<string>(() => this.InnerChannel.GetVersion());
        }

        #endregion

        #region ISubscriber Members

        public void Subscribe()
        {
            this.Invoke(() => this.InnerChannel.Subscribe());
        }
        public void Unsubscribe()
        {
            this.Invoke(() => this.InnerChannel.Unsubscribe());
        }

        #endregion

        #region ISampleChangerServiceCallbackEvents Members

        void ISampleChangerServiceCallbackEvents.OnPositionChangeBeginCallbackEvent(int OldPosition, int NewPosition)
        {
            OnPositionChangeBegin(OldPosition, NewPosition);
        }
        void ISampleChangerServiceCallbackEvents.OnPositionChangeEndCallbackEvent(int NewPosition)
        {
            OnPositionChangeEnd(NewPosition);
        }
        void ISampleChangerServiceCallbackEvents.OnPositionSeekBeginCallbackEvent(int CurrentPostion, int SeekPosition)
        {
            OnPositionSeekBegin(CurrentPostion, SeekPosition);
        }
        void ISampleChangerServiceCallbackEvents.OnPositionSeekEndCallbackEvent(int NewPosition)
        {
            OnPositionSeekEnd(NewPosition);
        }
        void ISampleChangerServiceCallbackEvents.OnPositionSeekCancelCallbackEvent(int CurrentPostion, int SeekPosition)
        {
            OnPositionSeekCancel(CurrentPostion, SeekPosition);
        }
        void ISampleChangerServiceCallbackEvents.OnErrorOccurredCallbackEvent(int ErrorCode, string Message)
        {
            OnChangerError(ErrorCode, Message);
             FireSampleChangerErrorEvent(ErrorCode, Message);
        }

        private void FireSampleChangerErrorEvent(int errorCode, string message)
        {
            MessageBoxContent systemNotification =
                (MessageBoxContent)SystemNotificationArray.Notifications[(int)Daxor.Lab.Infrastructure.SystemNotifications.SystemNotificationArray.NotificationIndex.SC_Unavailable];
            systemNotification.SystemNotificationDateTime = DateTime.Now;
            switch(errorCode)
            {
                case 6001: // Unexpected Position Move
                    systemNotification.RemoveFromQue = true;
                    break;
            }

            systemNotification.Message = message;

            _eventAggregator.GetEvent<SystemNotificationEvent>().Publish(systemNotification);
        }

        #endregion

        #region Invoke
        private void Invoke(Action invokeAction)
        {
            this.OpenProxyAndSubscribe();
            try
            {
                invokeAction();
            }
            catch (CommunicationException commExc)
            {
                //See if it's faulted Exception, if so throw othewise
                //retry
                FaultException faltExc = commExc as FaultException;
                if (faltExc != null)
                {
                    throw;
                }
                else
                {
                    //Attempt to retry previous call; if not successful, forward the error up the calling stack
                    try
                    {
                        invokeAction();
                    }
                    catch (CommunicationException)
                    {
                        //Should write the error to the log 
                        //throw;
                        string err = "Failed to establish communication with Daxor MultiChannelAnalyzer service.";
                        throw new Exception(err);

                    }
                }
            }

        }
        private TResult Invoke<TResult>(Func<TResult> invokeFunc)
        {
            this.OpenProxyAndSubscribe();
            TResult result = default(TResult);
            try
            {
                result = invokeFunc();
            }
            catch (CommunicationException commExc)
            {
                //See if it's faulted Exception, if so throw othewise
                //retry
                FaultException faltExc = commExc as FaultException;
                if (faltExc != null)
                {
                    throw;
                }
                else
                {
                    //Attempt to retry previous call; if not successful, forward the error up the calling stack
                    try
                    {
                        result = invokeFunc();
                    }
                    catch (CommunicationException)
                    {
                        //throw;
                        //Should write the error to the log 
                        string err = "Failed to establish communication with Daxor MultiChannelAnalyzer service.";
                        throw new Exception(err);
                    }
                }
            }

            return result;
        }
        #endregion

        #region Helpers

        private void Channel_Opened(object sender, EventArgs e)
        {
            try
            {
                this.IsProxyConnected = true;

                //Subscribe for the callbacks
                this.Subscribe();
                OnProxyOpened();
            }
            catch { }
        }
        private void Channel_Faulted(object sender, EventArgs e)
        {
            this._isProxyConnected = false;
        }
        private void CloseAbortChannel()
        {
            _duplexClient.InnerDuplexChannel.Opened -= new EventHandler(Channel_Opened);
            _duplexClient.InnerDuplexChannel.Faulted -= new EventHandler(Channel_Faulted);
            try
            {
                _duplexClient.Close();
            }
            catch
            {
                try { _duplexClient.Abort(); }
                catch { }
            }
        }

        #endregion
    }
}
