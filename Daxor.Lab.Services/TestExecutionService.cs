﻿#define DEBUG_TIME_REMAINING2
using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.SC;
using Daxor.Lab.Infrastructure.Services;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MCAContracts;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Daxor.Lab.Infrastructure.Entities;

namespace Daxor.Lab.Services
{
    /// <summary>
    /// STRATEGY PATTERN!!!!!!!!!!
    /// Events: TestStarting, TestStarted, TestAborted, TestCompleted,
    /// SampleAcquisitionStarted, SampleAcquisitionCompleted, SampleAcquisitionSpectrumUpdated 
    /// </summary>
    public class TestExecutionService : ITestExecutionService
    {
        #region TEMP CONSTANT

        //NEED TO HAVE CONFIGURATION STUFF
        private const string MCA_TO_USE = "mca1";
        private const int TIME_SEC_BETWEEN_POSITIONS = 10;

        #endregion

        #region Fields
       
        static readonly object _locker = new object();

        private readonly ISampleChangerProxy _sampleChanger;
        private readonly IEventAggregator _eventAggregator;
        private readonly IBackgroundAcquisitionService _bkgService;
        private readonly ILoggerFacade _logger;
        private readonly BackgroundWorker _tExecutionThread = new BackgroundWorker();
        private readonly object timeRemainingLock = new object();

        private TestExecutionStep _exeStep;
        private ITestExecutionContext _context;

        #endregion

        #region Properties

        public ITestExecutionContext Context
        {
            get { return _context; }
            private set
            {
                if (_context != null && _context != value)
                    this.DetachFromEvents(_context);

                _context = value;
                if (_context != null)
                    this.AttachToEvents(_context);
            }
        }
        public TestExecutionServiceState ServiceState
        {
            get;
            private set;
        }
        public TestExecutionStep ExecutionStep
        {
            get { return _exeStep; }
            private set
            {
                lock (_locker)
                {
                    _exeStep = value;
                }
            }
        }
        public bool IsRunning
        {
            get
            {
                return _tExecutionThread != null && _tExecutionThread.IsBusy && !_tExecutionThread.CancellationPending;
            }
        }
        public ExecutionProgressMetadata ProgressMetadata
        {
            get;
            private set;
        }
        private ISample CurrentRunningSample { get; set; }
      
        #endregion

        #region Ctor

        public TestExecutionService(ISampleChangerProxy sampleChanger, IEventAggregator eventAggregator,
                                    IBackgroundAcquisitionService bkgService, ILoggerFacade logger)
        {
            _sampleChanger = sampleChanger;
            _eventAggregator = eventAggregator;
            _bkgService = bkgService;
            _logger = logger;

            this.ConfiureExecutionWorker();
        }

        #endregion

        #region Methods

        public bool StartAsync(ITestExecutionContext context)
        {
            //Determine if test is running, return false; could not start the test
            if (this.IsRunning)
                return false;

            ContractHelper.ArgumentNotNull(context, "TestExecutionContext");
            ContractHelper.ArgumentNotNull(context.Test, "TestExecutionContext.Test");
            ContractHelper.ArgumentNotNull(context.OrderedSamplesToExecute, "TestExecutionContext.OrderedSamplesToExecute");
            ContractHelper.ArgumentNotNull(context.MultiChannelAnalyzer, "TestExecutionContext.MCAService");

            this.Context = context;
            this.Context.Test.TestStatus = TestStatus.Running;

            try
            {
                _eventAggregator.GetEvent<TestStarted>().Publish(Context.Test);
                _tExecutionThread.RunWorkerAsync(Context);
            }
            catch (Exception ex)
            {
                _logger.Log(String.Format("Failed to StartAsync() test execution for {0}", Context.Test.TestType.ToString()), Category.Exception, Priority.High);
                throw ex;
            }

            return true;
        }
        public void StopAsync()
        {
            if (_tExecutionThread != null && _tExecutionThread.IsBusy)
            {
                _tExecutionThread.CancelAsync();
            }
        }

        #endregion

        #region BackgroundWorker

        void TestExecutionThread_DoWork(object sender, DoWorkEventArgs e)
        {
            ITestExecutionContext context = e.Argument as ITestExecutionContext;
            int nextRunningSampleIndex = 0;

            this.ProgressMetadata = new ExecutionProgressMetadata();
            this.ProgressMetadata.TotalEstimatedExecutionTimeSec = this.GetTotalExecutionTime(context);
            this.ProgressMetadata.ElapsedExecutionTimeSec = 0;

            //this.TotalExecutionTimeSec = this.GetTotalExecutionTime(context);
            this.ServiceState = TestExecutionServiceState.TestInProgress;
            this.ExecutionStep = TestExecutionStep.Unknown;
            this.CurrentRunningSample = context.GetNextSample(null);

            try
            {
                //Execute test until its completion
                while (this.ServiceState == TestExecutionServiceState.TestInProgress)
                {
                    //Test execution has been aborted
                    if (_tExecutionThread.CancellationPending)
                    {
                        e.Cancel = true;
                        this.ServiceState = TestExecutionServiceState.TestAborted;
                        this.ExecutionStep = TestExecutionStep.Unknown;
                        break;
                    }

                    this.EnsureBackgroundAcquistionStep(context, nextRunningSampleIndex);
                    this.PerformBackgroundAcquisitionStep(context);
                    this.PerformMoveToSampleStep();
                    this.PerformMoveToSampleCompletedStep(context);
                    this.PerformCountSampleStep(context);
                    this.PerformCountSampleCompletedStep(context, ref nextRunningSampleIndex);

                    //Unknown step should not be present at this stage
                    if (this.ExecutionStep == TestExecutionStep.Unknown)
                    {
                        throw new Exception("TestExecutionService is at 'unknown' execution step.");
                    }

                    //Fire aggregate event Progress update
                    _eventAggregator.GetEvent<TestExecutionProgressChanged>().Publish(new TestExecutionProgressPayload(ServiceState, ExecutionStep)
                    {
                        ExecutingTestID = context.Test.TestID,
                        ExecutingSampleID = GetExecutingSampleIdOrDefault(),
                        ExecutingTestType = context.Test.TestType,
                        ContextMetadata = context.Metadata,
                        ProgressMetadata = this.ProgressMetadata,
                    });

                    Thread.Sleep(1000);
                }

                e.Result = context;
            }
            catch (Exception ex)
            {
                _logger.Log(ex.Message, Category.Exception, Priority.None);
                this.WrapUpTestExecution(context);

                throw;
            }
        }
        void TestExecutionThread_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Progress Changed TestExecutingService...");
        }
        void TestExecutionThread_Disposed(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Disposing TestExecution worker thread.");
        }
        void TestExecutionThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.WrapUpTestExecution(this.Context);
        }

        #endregion

        #region TestExecutionSteps

        void EnsureBackgroundAcquistionStep(ITestExecutionContext context, int nextRunningSampleIndex)
        {
            //Determine if background is required for the test
            if (context.IsBackgroundRequired && ExecutionStep == TestExecutionStep.Unknown)
            {
                context.Test.BackgroundTimeSec = _bkgService.Context.PresetBkgAcquisitionTimeSeconds;
                this.ProgressMetadata.TotalEstimatedExecutionTimeSec += _bkgService.Context.PresetBkgAcquisitionTimeSeconds;
                this.ExecutionStep = TestExecutionStep.MoveToBackground;
            }
            //Stop background if background is not required
            else if (!context.IsBackgroundRequired && _bkgService.IsActive)
            {
                _bkgService.StopAsync();
                ExecutionStep = TestExecutionStep.CancelBackground;
            }
            else if (ExecutionStep == TestExecutionStep.Unknown || ExecutionStep == TestExecutionStep.CancelBackground)
            {
                //Wait until background acquisition service stops completely, then change the state
                if (ExecutionStep == TestExecutionStep.CancelBackground && !_bkgService.IsActive)
                {
                    this.ExecutionStep = TestExecutionStep.MoveToSample;
                    this.CurrentRunningSample = context.OrderedSamplesToExecute.ElementAtOrDefault(nextRunningSampleIndex);
                }
            }
        }
        void PerformBackgroundAcquisitionStep(ITestExecutionContext context)
        {
            if (this.ExecutionStep == TestExecutionStep.MoveToBackground || this.ExecutionStep == TestExecutionStep.CountBackground)
            {
                //We already have a good background previously attained
                if (_bkgService.WasGoodBackgroundAttained)
                {
                    _bkgService.StopAsync();
                    double prevLiveTimeFromLastRead = 0;

                    prevLiveTimeFromLastRead = CurrentRunningSample.LiveTime;

                    context.Test.BackgroundTimeSec = _bkgService.PreviousAcqTimeSec.HasValue ? _bkgService.PreviousAcqTimeSec.Value : 1;
                    context.Test.BackgroundCount = _bkgService.PreviousIntegral.HasValue ? _bkgService.PreviousIntegral.Value : 0;
                   
                    if (!_bkgService.IsBackgroundOverridden)
                    {
                        CurrentRunningSample.SpectrumArray = _bkgService.PreviousSpectrum.SpectrumArray;
                        CurrentRunningSample.LiveTime = _bkgService.PreviousSpectrum.LiveTimeInMicroseconds;
                        CurrentRunningSample.RealTime = _bkgService.PreviousSpectrum.RealTimeInMicroseconds;
                    }

                    //Update running execution time
                    this.ProgressMetadata.ElapsedExecutionTimeSec += (int)Math.Abs(((prevLiveTimeFromLastRead - CurrentRunningSample.LiveTime) / 1000000f));

                    //Update running execution context
                    context.ProcessSample(CurrentRunningSample, true);

                    this.ExecutionStep = TestExecutionStep.MoveToSample;

                    //Get Next Sample to execute
                    this.CurrentRunningSample = context.GetNextSample(CurrentRunningSample); //Not very clear Rasko!?!?!?
                    System.Diagnostics.Debug.WriteLine("TestExecutionService::PerformBackgroundAcquisitionStep w/ good background after context.GetNextSample()");
                }
                else
                {
                    //Start background acquisition
                    if (!_bkgService.IsActive) _bkgService.StartAsync();

                    //Spectrum spectrum = context.MCAService.GetSpectrum(MCA_TO_USE);
                    //ISample background = context.OrderedSamplesToExecute.ElementAtOrDefault(0);
                    if (_bkgService.CurrentSpectrum != null)
                    {
                        CurrentRunningSample.SpectrumArray = _bkgService.CurrentSpectrum.SpectrumArray;
                        CurrentRunningSample.LiveTime = _bkgService.CurrentSpectrum.LiveTimeInMicroseconds;
                        CurrentRunningSample.RealTime = _bkgService.CurrentSpectrum.RealTimeInMicroseconds;
                    }

                    context.ProcessSample(CurrentRunningSample);
                }

                if (_bkgService.IsActive)
                {
                    if (this.ExecutionStep == TestExecutionStep.CountBackground)
                    {
                        this.ProgressMetadata.ElapsedExecutionTimeSec = _bkgService.RunningBkgExecutionTimeInSecs;
                        context.Test.BackgroundTimeSec = _bkgService.CurrentAcqTimeSec;
                        context.Test.BackgroundCount = _bkgService.CurrentIntegral;
                    }

                    //Update current execution step from the background service
                    if (_bkgService.Status == BackgroundStatus.SeekingBackgoundPosition)
                        this.ExecutionStep = TestExecutionStep.MoveToBackground;

                    else if (_bkgService.Status == BackgroundStatus.Acquiring)
                        this.ExecutionStep = TestExecutionStep.CountBackground;
                }
            }
        }
        void PerformMoveToSampleStep()
        {
            if (this.ExecutionStep == TestExecutionStep.MoveToSample)
            {
                //Start moving sample changer to the goal position
                if (_sampleChanger.GetCurrentPosition() != CurrentRunningSample.Position && !_sampleChanger.IsMoving())
                {
                    _sampleChanger.MoveToPosition(CurrentRunningSample.Position);
                }
                else
                {
                    if (_sampleChanger.GetCurrentPosition() == CurrentRunningSample.Position)
                        this.ExecutionStep = TestExecutionStep.MoveToSampleCompleted;
                }
            }
        }
        void PerformMoveToSampleCompletedStep(ITestExecutionContext context)
        {
            if (this.ExecutionStep == TestExecutionStep.MoveToSampleCompleted)
            {
                //Restart counting with context's multi channel analyzer
                context.MultiChannelAnalyzer.StopAcquiring(MCA_TO_USE);
                context.MultiChannelAnalyzer.ClearSpectrumResetTimes(MCA_TO_USE);
                if (CurrentRunningSample.PresetLiveTime <= 0)
                {
                    context.MultiChannelAnalyzer.PresetTime(MCA_TO_USE, true, context.Test.SampleDurationInSeconds);
                }
                else
                {
                    context.MultiChannelAnalyzer.PresetTime(MCA_TO_USE, true, CurrentRunningSample.PresetLiveTime);
                }
                context.MultiChannelAnalyzer.StartAcquiring(MCA_TO_USE);
                this.ExecutionStep = TestExecutionStep.CountSample;
            }
        }
        void PerformCountSampleStep(ITestExecutionContext context)
        {
            if (this.ExecutionStep == TestExecutionStep.CountSample)
            {
                double prevLiveTimeFromLastRead = 0;
                CurrentRunningSample.IsCounting = true;

                //Gets spectrum from MCA service; update RunningSample
                SpectrumData spectrum = context.MultiChannelAnalyzer.GetSpectrum(MCA_TO_USE);
                CurrentRunningSample.SpectrumArray = spectrum.SpectrumArray;
                prevLiveTimeFromLastRead = CurrentRunningSample.LiveTime;
                CurrentRunningSample.LiveTime = spectrum.LiveTimeInMicroseconds;
                CurrentRunningSample.RealTime = spectrum.RealTimeInMicroseconds;

                //Update running execution time
                //this.ExecutionTimeInSec += (int)Math.Abs(((prevLiveTimeFromLastRead - CurrentRunningSample.LiveTime) / 1000000f));
                this.ProgressMetadata.ElapsedExecutionTimeSec += (int)Math.Abs(((prevLiveTimeFromLastRead - CurrentRunningSample.LiveTime) / 1000000f));
                
                //Update underlying execution context
                context.ProcessSample(CurrentRunningSample);
                
               
                //Final spectrum get
                if (context.MultiChannelAnalyzer.HasAcquisitionCompleted(MCA_TO_USE))
                {
                    prevLiveTimeFromLastRead = CurrentRunningSample.LiveTime;
                    SpectrumData sp = context.MultiChannelAnalyzer.GetSpectrum(MCA_TO_USE);
                    CurrentRunningSample.SpectrumArray = sp.SpectrumArray;
                    CurrentRunningSample.LiveTime = sp.LiveTimeInMicroseconds;
                    CurrentRunningSample.RealTime = sp.RealTimeInMicroseconds;

                    //Update running execution time
                    //ExecutionTimeInSec += (int)Math.Abs(((prevLiveTimeFromLastRead - CurrentRunningSample.LiveTime) / 1000000f));
                    this.ProgressMetadata.ElapsedExecutionTimeSec += (int)Math.Abs(((prevLiveTimeFromLastRead - CurrentRunningSample.LiveTime) / 1000000f));


                    context.ProcessSample(CurrentRunningSample, true);
                    _eventAggregator.GetEvent<SampleAcquisitionCompleted>().Publish(new SampleInfo(CurrentRunningSample, context.Test.TestType, context.Test.TestID));

                    CurrentRunningSample.IsCounting = false;
                    this.ExecutionStep = TestExecutionStep.CountSampleCompleted;
                }
            }
        }
        void PerformCountSampleCompletedStep(ITestExecutionContext context, ref int currentRunningSampleIndex)
        {
            if (this.ExecutionStep == TestExecutionStep.CountSampleCompleted)
            {
                
                //We know we have finished when there are no more samples left
                ISample nextSample = context.GetNextSample(CurrentRunningSample);
                CurrentRunningSample = nextSample;

                if (CurrentRunningSample == null)
                {
                    this.ServiceState = TestExecutionServiceState.TestCompleted;
                    
                }
                else
                {
                    currentRunningSampleIndex = CurrentRunningSample.Position;
                    this.ExecutionStep = TestExecutionStep.MoveToSample;
                }
            }
        }

        #endregion

        #region Helpers

        void AttachToEvents(ITestExecutionContext context)
        {
            if (context != null)
            {
                context.MultiChannelAnalyzer.AcquisitionStarted += MCAService_AcquisitionStarted;
                context.MultiChannelAnalyzer.AcquisitionStopped += MCAService_AcquisitionStopped;
                context.MultiChannelAnalyzer.AcquisitionCompleted += MCAService_AcquisitionCompleted;
                context.MultiChannelAnalyzer.McaPlugged += MCAService_McaPlugged;
                context.MultiChannelAnalyzer.McaUnplugged += MCAService_McaUnplugged;
            }

            _sampleChanger.PositionChangeBegin += _sampleChanger_PositionChangeBegin;
            _sampleChanger.PositionChangeEnd += _sampleChanger_PositionChangeEnd;
            _sampleChanger.PositionSeekBegin += _sampleChanger_PositionSeekBegin;
            _sampleChanger.PositionSeekEnd += _sampleChanger_PositionSeekEnd;
            _sampleChanger.PositionSeekCancel += _sampleChanger_PositionSeekCancel;
            _sampleChanger.ChangerError += _sampleChanger_ChangerError;
        }
        void DetachFromEvents(ITestExecutionContext context)
        {
            if (context != null)
            {
                context.MultiChannelAnalyzer.AcquisitionStarted -= MCAService_AcquisitionStarted;
                context.MultiChannelAnalyzer.AcquisitionStopped -= MCAService_AcquisitionStopped;
                context.MultiChannelAnalyzer.AcquisitionCompleted -= MCAService_AcquisitionCompleted;
                context.MultiChannelAnalyzer.McaPlugged -= MCAService_McaPlugged;
                context.MultiChannelAnalyzer.McaUnplugged -= MCAService_McaUnplugged;
            }

            _sampleChanger.PositionChangeBegin -= _sampleChanger_PositionChangeBegin;
            _sampleChanger.PositionChangeEnd -= _sampleChanger_PositionChangeEnd;
            _sampleChanger.PositionSeekBegin -= _sampleChanger_PositionSeekBegin;
            _sampleChanger.PositionSeekEnd -= _sampleChanger_PositionSeekEnd;
            _sampleChanger.PositionSeekCancel -= _sampleChanger_PositionSeekCancel;
            _sampleChanger.ChangerError -= _sampleChanger_ChangerError;
        }
        void WrapUpTestExecution(ITestExecutionContext context)
        {
            //Detach from all events
            this.DetachFromEvents(context);

            //Invalidate background if it was prior required
            if (context != null && context.IsBackgroundRequired)
                _bkgService.InvalidatePreviousBackground();


            //Test has been completed
            if (this.ServiceState == TestExecutionServiceState.TestCompleted)
            {
                context.Test.TestStatus = TestStatus.Completed;
                context.Test.TestDate = DateTime.Now;
            }
            //In any other case, let's treat a test as aborted
            else
            {
                context.Test.TestStatus = TestStatus.Aborted;
                context.Test.TestDate = DateTime.Now;

                _bkgService.StopAsync();
                _sampleChanger.StopSampleChanger();
                context.MultiChannelAnalyzer.StopAcquiring(MCA_TO_USE);
            }

            //Process test last final time
            try
            {
                context.ProcessTest();
            }
            catch (Exception ex)
            {
                _logger.Log(ex.Message, Category.Exception, Priority.High);
            }

            //Final conclusions
            if (context.Test.TestStatus == TestStatus.Completed)
                _eventAggregator.GetEvent<TestFinished>().Publish(this.Context.Test);

            else
                _eventAggregator.GetEvent<TestAborted>().Publish(this.Context.Test);

            this.Context = null;
            this.CurrentRunningSample = null;
           
        }

        void ConfiureExecutionWorker()
        {
            _tExecutionThread.WorkerSupportsCancellation = true;
            _tExecutionThread.DoWork += TestExecutionThread_DoWork;
            _tExecutionThread.Disposed += TestExecutionThread_Disposed;
            _tExecutionThread.RunWorkerCompleted += TestExecutionThread_RunWorkerCompleted;
        }
        int GetTotalExecutionTime(ITestExecutionContext context)
        {
            int totalPositionToTravel = 23;
            return totalPositionToTravel * TIME_SEC_BETWEEN_POSITIONS + context.OrderedSamplesToExecute.Count() * context.Test.SampleDurationInSeconds;
        }
        Guid GetExecutingSampleIdOrDefault()
        {
            if (ServiceState == TestExecutionServiceState.TestCompleted || ServiceState == TestExecutionServiceState.TestAborted || CurrentRunningSample == null)
                return Guid.Empty;
            else
                return CurrentRunningSample.SampleID;

        }
        
        #endregion

        #region MultiChannelAnalyzer Handlers

        void MCAService_McaUnplugged(string mcaLogicalName, string serialNumber)
        {
        }
        void MCAService_McaPlugged(string mcaLogicalName, string serialNumber)
        {
        }

        void MCAService_AcquisitionStarted(string mcaLogicName, string serialNumber)
        {

        }
        void MCAService_AcquisitionCompleted(string mcaLogicalName, string serialNumber, MCAContracts.SpectrumData spect)
        {
        }
        void MCAService_AcquisitionStopped(string mcaLogicName, string serialNumber)
        {
        }

        #endregion

        #region SampleChanger Handlers

        void _sampleChanger_ChangerError(int errorCode, string message)
        {
            if (!this.IsRunning) return;

            System.Diagnostics.Debug.WriteLine("TestExecution is running and I've got ChangerError. I've got to do something.");
        }
        void _sampleChanger_PositionSeekBegin(int currPosition, int seekPosition)
        {
            if (!this.IsRunning) return;

            if (this.ServiceState == TestExecutionServiceState.TestInProgress && this.ExecutionStep == TestExecutionStep.MoveToSample &&
                CurrentRunningSample != null && CurrentRunningSample.Position != seekPosition)
            {
                System.Diagnostics.Debug.Print("CurrentRunningSample {0} seekPosition {1}", CurrentRunningSample.Position, seekPosition);

                throw new Exception("Who's controlling my Sample Changer.... HA????? I NEED TO KNOW");
            }

            //TODO: remove this aggregate event, wrong place to raise it (AK)
            _eventAggregator.GetEvent<SampleChangerPositionSeekBegin>().Publish(new SeekBeginPayload()
            {
                CurrentPosition = currPosition,
                SeekPosition = seekPosition
            });
        }
        void _sampleChanger_PositionSeekEnd(int currPosition)
        {
            if (!this.IsRunning) return;

            if (this.ServiceState == TestExecutionServiceState.TestInProgress)
            {
                if (this.ExecutionStep == TestExecutionStep.MoveToSample)
                {

                    if (this.CurrentRunningSample != null && this.CurrentRunningSample.Position != currPosition)
                        throw new Exception("Someone moved my sample changer my sample changed...");

                }
            }
            else
                throw new Exception("TestExecutionService. PositionSeekEnd " + currPosition + ". Why am I here.");

            //TODO: remove this aggregate event, wrong place to raise it (AK)
            _eventAggregator.GetEvent<SampleChangerPositionSeekEnd>().Publish(new SeekEndPayload()
            {
                CurrentPosition = currPosition,
            });

        }
        void _sampleChanger_PositionSeekCancel(int currPosition, int seekPosition)
        {
            if (!this.IsRunning) return;

            //TODO: remove this aggregate event, wrong place to raise it (AK)
            _eventAggregator.GetEvent<SampleChangerPositionSeekCancel>().Publish(new SeekCancelPayload()
            {
                CurrentPosition = currPosition,
                SeekPosition = seekPosition
            });
        }
        void _sampleChanger_PositionChangeBegin(int oldPosition, int newPosition)
        {
            if (!this.IsRunning) return;

            //TODO: remove this aggregate event, wrong place to raise it (AK)
            _eventAggregator.GetEvent<SampleChangerPositionChangeBegin>().Publish(new ChangeBeginPayload()
            {
                OldPosition = oldPosition,
                NewPosition = newPosition
            });
        }
        void _sampleChanger_PositionChangeEnd(int newPosition)
        {
            if (!this.IsRunning) return;

            if (this.ServiceState == TestExecutionServiceState.TestInProgress)
            {
                if (this.ExecutionStep == TestExecutionStep.MoveToSample)
                {
                    this.ProgressMetadata.ElapsedExecutionTimeSec += 1 * TIME_SEC_BETWEEN_POSITIONS;
                    //ExecutionTimeInSec += 1 * TIME_SEC_BETWEEN_POSITIONS;
                }
            }
            else
                throw new Exception("PositionChangedEnd " + newPosition + ". SampleChanger moved no position change");

            //TODO: remove this aggregate event, wrong place to raise it (AK)
            _eventAggregator.GetEvent<SampleChangerPositionChangeEnd>().Publish(new ChangeEndPayload()
            {
                NewPosition = newPosition
            });
        }

        #endregion
    }
}
