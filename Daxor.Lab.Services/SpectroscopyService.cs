﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.Infrastructure.Interfaces;
using System.Diagnostics.Contracts;

namespace Daxor.Lab.Services
{
    /// <summary>
    /// This class is responsible for providing spectroscopy 
    /// behaviour to its clients.
    /// </summary>
    internal class SpectrocopyService : ISpectroscopyService
    {
        #region ISpectroscopyService

        public ulong Integral(UInt32[] spectArray, int leftBound, int rightBound)
        {
            ulong integral = 0;

            // If specturm is null or empty return 0
            if (!IsValidSpectrum(spectArray, leftBound - 1, rightBound - 1))
                return integral;

            // Get the actual integral under ROI
            try
            {
                for (int i = leftBound - 1; i < rightBound && i >= 0; i++)
                {
                    integral += spectArray[i];
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw;
            }
         
            return integral;
        }
        public ulong Integral(SpectrumData spect, int leftBound, int rightBound)
        {
            if (spect == null)
                return 0;

            return Integral(spect.SpectrumArray, leftBound, rightBound);
        }

        public double Centroid(UInt32[] specArray, int leftBound, int rightBound)
        {
            //Using first moment centroid algorithm as default algorithm utilized by canbera software
            return CentroidFirstMoment(specArray, leftBound, rightBound);
        }

        /// <summary>
        /// Determination of Peak Width Using Analytical Interpolation J.L. Parker p. 117
        /// Canbera lib 3.1 is using identical method.
        /// </summary>
        /// <param name="spect">Spectrum Object</param>
        /// <param name="kMax">Factional height of interest</param>
        /// <param name="leftBound">Left bound of the ROI</param>
        /// <param name="rightBound">Right bound of the ROI</param>
        /// <returns>Full width at K Max (K - being half, 1/10, etc.)</returns>
        /// 
        public double FullWidthKMax(UInt32[] spectArray, float kmaxFraction, int leftBound, int rightBound, out double background)
        {
            background = 0;
            double fwkm = -1;
            int xPeak = int.MinValue;
            double yPeak = double.MinValue;

            double kMax = 0;
            double[,] xyPairs = new double[4, 2];
            double[] spectrumArray;

            if (kmaxFraction <= 0 || kmaxFraction > 1) return 0;

            //Validate the spectrum, return 0 if spectrum is not valid
            if (!IsValidSpectrum(spectArray, leftBound - 1, rightBound - 1))
            {
                return fwkm;
            }

            background = this.RemoveContinuumByStraighLine(spectArray, leftBound, rightBound, out spectrumArray);

            //3point average Peak finding
            // double[] tempSpectrum = AverageSmoothSpectrum2(spectrumArray, 3);
            //spectrumArray = AverageSmoothSpectrum(new Spectrum { SpectrumArray = spectrumArray }, 3);

            //Find Max x,y pair
            for (int i = leftBound - 1; i < rightBound; i++)
            {
                if (spectrumArray[i] > yPeak)
                {
                    yPeak = spectrumArray[i];
                    xPeak = i;
                }
            }
            //Determine if we failed to find our new peak
            if (double.IsInfinity(yPeak) || double.IsNaN(yPeak) || yPeak == double.MinValue || yPeak == 0) return fwkm;

            //Calculate kMax value used for FWKM calculations
            kMax = yPeak * kmaxFraction;
            //System.Diagnostics.Debug.WriteLine("Kmax=" + kMax);

            //Proceed on the left side of the peak, find pair (x1,y1) and (x2,y2)
            bool leftPairDetected = false;
            for (int ch = xPeak - 1; ch >= leftBound - 1; ch--)
            {
                //Found left pair of (x1,y1) and (x2, y2)
                if (spectrumArray[ch] < kMax)
                //if (Math.Round(spectrumArray[ch], 3) < kMax)
                {
                    //Pair below kMax line to the left of the peak
                    xyPairs[0, 0] = ch;
                    xyPairs[0, 1] = spectrumArray[ch];

                    //Pair above kMax Line
                    xyPairs[1, 0] = ch + 1;
                    xyPairs[1, 1] = spectrumArray[ch + 1];
                    leftPairDetected = true;
                    break;
                }
            }
            //System.Diagnostics.Debug.WriteLine("x1=" + xyPairs[0, 0] + " y1=" + xyPairs[0, 1]);
            //System.Diagnostics.Debug.WriteLine("x2=" + xyPairs[1, 0] + " y2=" + xyPairs[1, 1]);

            //Proceed on the right side of the peak, find pair (x1,y1) and (x2,y2)
            bool rightPairDetected = false;
            for (int ch = xPeak + 1; ch < rightBound; ch++)
            {
                if (Math.Round(spectrumArray[ch], 3) < kMax)
                {
                    //Pair below kMax line to the right of the peak
                    xyPairs[3, 0] = ch;
                    xyPairs[3, 1] = spectrumArray[ch];

                    xyPairs[2, 0] = ch - 1;
                    xyPairs[2, 1] = spectrumArray[ch - 1];
                    rightPairDetected = true;
                    break;
                }
            }
            //System.Diagnostics.Debug.WriteLine("x3=" + xyPairs[2, 0] + "y3=" + xyPairs[2, 1]);
            //System.Diagnostics.Debug.WriteLine("x4=" + xyPairs[3, 0] + "y4=" + xyPairs[3, 1]);

            //Final calculations 
            try
            {
                //x coordinate (channel) of intersection of line L with low-energy side of a peak
                double xIntersectionLow;
                if (!leftPairDetected)
                {
                    xIntersectionLow = leftBound - 1;
                }
                else
                {
                    xIntersectionLow = (kmaxFraction * yPeak - xyPairs[0, 1]) / (xyPairs[1, 1] - xyPairs[0, 1]);
                    xIntersectionLow += xyPairs[0, 0];
                }


                //x coordinate (channel) of intersection of line L with high-energy side of a peak
                double xIntersectionHigh;
                if (!rightPairDetected)
                {
                    xIntersectionHigh = rightBound - 1;
                }
                else
                {
                    xIntersectionHigh = (xyPairs[2, 1] - kmaxFraction * yPeak) / (xyPairs[2, 1] - xyPairs[3, 1]);
                    xIntersectionHigh += xyPairs[2, 0];
                }


                fwkm = xIntersectionHigh - xIntersectionLow;
            }
            catch (Exception)
            {
                fwkm = -1;
            }

            return fwkm;
        }
        public double FullWidthKMax(SpectrumData spect, float kmaxFraction, int leftBound, int rightBound, out double background)
        {
            background = -1;
            if (spect == null)
                return 0;

            return FullWidthKMax(spect.SpectrumArray, kmaxFraction, leftBound, rightBound, out background);
        }
        public double FullWidthKMax(SpectrumData spect, float kmaxFraction, int leftBound, int rightBound)
        {
            double temp;
            return FullWidthKMax(spect, kmaxFraction, leftBound, rightBound, out temp);
        }
        public double FullWidthHalfMax(UInt32[] spectArray, int leftBound, int rightBound)
        {
            double temp;
            return FullWidthKMax(spectArray, 0.5f, leftBound, rightBound, out temp);
        }
        public double FullWidthTenthMax(UInt32[] spectArray, int leftBound, int rightBound)
        {
            double temp;
            return FullWidthKMax(spectArray, 0.1f, leftBound, rightBound, out temp);
        }

        /// <summary>
        /// Unweighted sliding-average smooth using width points
        /// </summary>
        /// <param name="spect">Spectrum object</param>
        /// <param name="width">desired width(3, 5, etc)</param>
        /// <returns>New smoothed array</returns>
        public UInt32[] AverageSmoothSpectrum(UInt32[] specArray, int width)
        {
            //Smooth array
            UInt32[] smoothArray = new UInt32[specArray.Length];
            //Trim off channels for which smoothing there was not enough data
            //In general, for an m-width smooth, there will be (width-1)/2 points at
            //the begining of the spectrum and (width-1)/2 the end of the spectrum
            //for which a complete width smooth can not be calculated.
            Array.Clear(smoothArray, 0, specArray.Length);
            for (int i = width / 2; i < specArray.Length - width / 2; i++)
            {
                smoothArray[i] = specArray[i - 1] + specArray[i] + specArray[i + 1];
                smoothArray[i] = (uint)Math.Round(smoothArray[i] / (double)width, 0);
            }

            return smoothArray;
        }

        #endregion

        #region Miscellanious

        #region Centroid Implementations

        double CentroidFirstMoment(SpectrumData spect, int leftBound, int rightBound)
        {
            if (spect == null)
                return 0;

            return CentroidFirstMoment(spect.SpectrumArray, leftBound, rightBound);
        }
        double CentroidFirstMoment(UInt32[] specArray, int leftBound, int rightBound)
        {
            //Determine the Peak Centroid using First-Moment Method 
            //described by  J.L. Parker in "General Topics in Passive Gamma-Ray Assay" ch. 5, p.105
            //PeakCentroid = SUM(Xi * Yi)/SUM(Yi)

            double peakCentroid = 0;
            // If specturm is valid
            if (!IsValidSpectrum(specArray, leftBound - 1, rightBound - 1))
                return peakCentroid;

            try
            {
                ulong sumOfChannelsTimesCounts = 0;
                ulong sumOfCounts = 0;
                for (int channel = leftBound - 1; channel < rightBound; channel++)
                {
                    sumOfChannelsTimesCounts += ((ulong)channel + 1) * (ulong)specArray[channel];
                    sumOfCounts += specArray[channel];
                }

                //Calculate peak Centroid
                if (sumOfCounts == 0)
                    peakCentroid = 0;
                else
                    peakCentroid = (double)sumOfChannelsTimesCounts / sumOfCounts;
            }

            //Spectrum array did not have appropriate channel between leftBound and rightBound
            catch (ArgumentOutOfRangeException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                peakCentroid = 0;
                throw;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                peakCentroid = 0;
                throw;
            }
            return peakCentroid;
        }

        double CentroidFiveChannel(SpectrumData spect, int leftBound, int rightBound)
        {
            if (spect == null)
                return 0;

            return CentroidFiveChannel(spect.SpectrumArray, leftBound, rightBound);
        }
        double CentroidFiveChannel(UInt32[] specArray, int leftBound, int rightBound)
        {
            //Determine the Peak Centroid using Five-Channel Method 
            //described by  J.L. Parker in "General Topics in Passive Gamma-Ray Assay" ch. 5, p.105
            //PeakCentroid = SUM(Xi * Yi)/SUM(Yi)

            double peakCentroid = 0;
            // If specturm is valid
            if (!IsValidSpectrum(specArray, leftBound, rightBound))
            {
                return peakCentroid;
            }
            try
            {
                uint maxCountsInChannel = 0;
                int maxChannel = int.MaxValue;
                for (int channel = leftBound - 1; channel <= rightBound; channel++)
                {
                    //find max
                    if (maxCountsInChannel < specArray[channel])
                    {
                        maxCountsInChannel = specArray[channel];
                        maxChannel = channel;
                    }
                }

                //Find 2 to the right and 2 to the lef
                uint numerator = (specArray[maxChannel + 1] * (maxCountsInChannel - specArray[maxChannel - 2])) -
                                 (specArray[maxChannel - 1] * (specArray[maxChannel] - specArray[maxChannel + 2]));

                uint denominator = (specArray[maxChannel + 1] * (maxCountsInChannel - specArray[maxChannel - 2])) +
                                 (specArray[maxChannel - 1] * (specArray[maxChannel] - specArray[maxChannel + 2]));

                peakCentroid = maxChannel + ((double)numerator / denominator);
            }
            //Spectrum array did not have appropriate channel between leftBound and rightBound
            catch (ArgumentOutOfRangeException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                peakCentroid = 0;
                throw;
            }
            catch (DivideByZeroException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                peakCentroid = 0;
                throw;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                peakCentroid = 0;
                throw;
            }
            return Math.Round(peakCentroid, 3);
        }
        #endregion

        #region Helpers

        // Returns true if spectrum is valid.
        private bool IsValidSpectrum(SpectrumData spect, int leftBound, int rightBound)
        {
            //Specturm is null or spectrum array is empty or range is less than or equals to 0
            if (spect == null || !IsValidSpectrum(spect.SpectrumArray, leftBound, rightBound))
                return false;

            return true;
        }
        private bool IsValidSpectrum(UInt32[] spectrumArray, int leftBound, int rightBound)
        {
            //Specturm is null or spectrum array is empty or range is less than or equals to 0
            if (spectrumArray == null || spectrumArray.Length == 0 || (rightBound - leftBound) <= 0)
                return false;

            return true;
        }
        private void InternalSmooth(UInt32[] oSpectrum, int smoothSize, out float[] rSpectrum)
        {
            Contract.Requires(oSpectrum != null, "Original spectrum cannot be null.");
            Contract.Requires(smoothSize < 3 || smoothSize > 511, "Smooth size must be greater than 1 and less than 512.");
            Contract.Requires(smoothSize % 2 == 0, "Smooth size cannot be an even number.");

            rSpectrum = new float[oSpectrum.Length];
            Array.Copy(oSpectrum, rSpectrum, rSpectrum.Length);
            for (int i = smoothSize / 2; i < oSpectrum.Length - smoothSize / 2; i++)
                rSpectrum[i] = oSpectrum.TakeWhile((v, j) => j >= j - (smoothSize / 2) && j <= j + (smoothSize / 2)).Average((v) => (float)v);
        }

      


        /// <summary>
        /// Removes Compton continuum from the passed spectrum array which is part of Spectrum
        /// object (only between selected ROIs). Algorithm is used by Canbera 3.1 package and
        /// is described in details by J.L Parker p.122
        /// </summary>
        /// <param name="spect">Spectrum object</param>
        /// <param name="leftBound">Left ROI marker</param>
        /// <param name="rightBound">Right ROI marker</param>
        /// <returns>Returns continuum subtracted array</returns>
        public double RemoveContinuumByStraighLine(SpectrumData spect, int leftBound, int rightBound, out double[] continumFreeSpect)
        {
            //uint[] continuumFreeSpect = new uint[spect.SpectrumArray.Length];
            continumFreeSpect = new double[spect.SpectrumArray.Length];

            //Use four channels below leftBound and rightBound
            //to establish 
            double xL = 0;
            double yL = 0;
            double xH = 0;
            double yH = 0;

            double m = double.NaN;  //slope of the straigh line used in continuum substraction
            double b = double.NaN;  //intercept of the straigh line used in continuum substraction
            double background = 0;

            //Find low x and y
            for (int i = leftBound - 2; i > leftBound - 6 && i >= 0; i--)
            {
                xL += i;
                yL += spect.SpectrumArray[i];
            }
            xL /= 4.0;    //find channel averages in the left (4 channel) ROI
            yL /= 4.0;    //find average counts in the left(4 channel) ROI

            //Find high x and y
            for (int j = rightBound; j < rightBound + 4 && j < spect.SpectrumArray.Length; j++)
            {
                xH += j;
                yH += spect.SpectrumArray[j];
            }
            xH /= 4.0;    //find channel averages in the left (4 channel) ROI
            yH /= 4.0;    //find average counts in the left(4 channel) ROI

            //Find slope (m) of the function and intercept (b)
            m = (yH - yL) / (xH - xL);
            b = (xH * yL - xL * yH) / (xH - xL);



            //Remove the background from each channel based on the line we have calculated
            for (int ch = leftBound - 1; ch < rightBound; ch++)
            {
                if ((double)spect.SpectrumArray[ch] < (m * ch + b))
                {
                    continumFreeSpect[ch] = 0;
                    background += continumFreeSpect[ch];
                }
                else
                {
                    //continuumFreeSpect[ch] = spect.SpectrumArray[ch] - (uint)(m * ch + b);
                    //continuumFreeSpect[ch] = Math.Round((double)spect.SpectrumArray[ch] - Math.Round((m * ch + b), 3), 3);
                    continumFreeSpect[ch] = (double)spect.SpectrumArray[ch] - (m * ch + b);
                    //continuumFreeSpect[ch] = (double)spect.SpectrumArray[ch];
                    background += (double)spect.SpectrumArray[ch] - continumFreeSpect[ch];
                }
            }

            //System.Diagnostics.Debug.WriteLine("m =" + m + " b= " + b);
            //System.Diagnostics.Debug.WriteLine("background = " + Math.Round(background,15));

            return background;
        }
        public double RemoveContinuumByStraighLine(UInt32[] spectArray, int leftBound, int rightBound, out double[] continumFreeSpect)
        {
            //uint[] continuumFreeSpect = new uint[spect.SpectrumArray.Length];
            continumFreeSpect = new double[spectArray.Length];

            //Use four channels below leftBound and rightBound
            //to establish 
            double xL = 0;
            double yL = 0;
            double xH = 0;
            double yH = 0;

            double m = double.NaN;  //slope of the straigh line used in continuum substraction
            double b = double.NaN;  //intercept of the straigh line used in continuum substraction
            double background = 0;

            //Find low x and y
            for (int i = leftBound - 2; i > leftBound - 6 && i >= 0; i--)
            {
                xL += i;
                yL += spectArray[i];
            }
            xL /= 4.0;    //find channel averages in the left (4 channel) ROI
            yL /= 4.0;    //find average counts in the left(4 channel) ROI

            //Find high x and y
            for (int j = rightBound; j < rightBound + 4 && j < spectArray.Length; j++)
            {
                xH += j;
                yH += spectArray[j];
            }
            xH /= 4.0;    //find channel averages in the left (4 channel) ROI
            yH /= 4.0;    //find average counts in the left(4 channel) ROI

            //Find slope (m) of the function and intercept (b)
            m = (yH - yL) / (xH - xL);
            b = (xH * yL - xL * yH) / (xH - xL);



            //Remove the background from each channel based on the line we have calculated
            for (int ch = leftBound - 1; ch < rightBound; ch++)
            {
                if ((double)spectArray[ch] < (m * ch + b))
                {
                    continumFreeSpect[ch] = 0;
                    background += continumFreeSpect[ch];
                }
                else
                {
                    //continuumFreeSpect[ch] = spect.SpectrumArray[ch] - (uint)(m * ch + b);
                    //continuumFreeSpect[ch] = Math.Round((double)spect.SpectrumArray[ch] - Math.Round((m * ch + b), 3), 3);
                    continumFreeSpect[ch] = (double)spectArray[ch] - (m * ch + b);
                    //continuumFreeSpect[ch] = (double)spect.SpectrumArray[ch];
                    background += (double)spectArray[ch] - continumFreeSpect[ch];
                }
            }

            //System.Diagnostics.Debug.WriteLine("m =" + m + " b= " + b);
            //System.Diagnostics.Debug.WriteLine("background = " + Math.Round(background,15));

            return background;
        }
        public float RemoveContinuumWithStraightLine(float[] oSpectrum, int lBound, int rBound, out float[] fSpectrum)
        {
            fSpectrum = new float[oSpectrum.Length];

            //Use four channels below leftBound and rightBound
            //to establish 
            float xL = 0;
            float yL = 0;
            float xH = 0;
            float yH = 0;

            float m = float.NaN;  //slope of the straigh line used in continuum substraction
            float b = float.NaN;  //intercept of the straigh line used in continuum substraction
            float background = 0;

            //Find low x and y
            for (int i = lBound - 2; i > lBound - 6 && i >= 0; i--)
            {
                xL += i;
                yL += oSpectrum[i];
            }

            xL /= 4.0f;    //find channel averages in the left (4 channel) ROI
            yL /= 4.0f;    //find average counts in the left(4 channel) ROI

            //Find high x and y
            for (int j = rBound; j < rBound + 4 && j < oSpectrum.Length; j++)
            {
                xH += j;
                yH += oSpectrum[j];
            }
            xH /= 4.0f;    //find channel averages in the left (4 channel) ROI
            yH /= 4.0f;    //find average counts in the left(4 channel) ROI

            //Find slope (m) of the function and intercept (b)
            m = (yH - yL) / (xH - xL);
            b = (xH * yL - xL * yH) / (xH - xL);



            //Remove the background from each channel based on the line we have calculated
            for (int ch = lBound - 1; ch < rBound; ch++)
            {
                if (oSpectrum[ch] < (m * ch + b))
                {
                    fSpectrum[ch] = 0;
                }
                else
                {
                    fSpectrum[ch] = oSpectrum[ch] - (m * ch + b);
                    background += oSpectrum[ch] - fSpectrum[ch];
                }
            }

            return background;
        }

        #endregion

        #region Obsolete
        //public double FirstMomentFullWidthHalfMax(Spectrum spect, double fMomentCentroid, int leftBound, int rightBound)
        //{
        //    double fwhm = 0;
        //    if (!IsValidSpectrum(spect, leftBound - 1, rightBound - 1))
        //    {
        //        return fwhm;
        //    }
        //    double variance = 0;
        //    long sumOfCounts = 0;
        //    double sumOfCentroidOffsetAndCounts = 0;
        //    try
        //    {
        //        for (int channel = leftBound - 1; channel < rightBound; channel++)
        //        {
        //            sumOfCentroidOffsetAndCounts += Math.Pow(((channel + 1) - fMomentCentroid), 2) * spect.SpectrumArray[channel];
        //            sumOfCounts += spect.SpectrumArray[channel];
        //        }
        //        //Find variance, which is squared standard deviation
        //        variance = sumOfCentroidOffsetAndCounts / sumOfCounts;
        //        //Calculate Full Width Half Max
        //        fwhm = 2 * Math.Sqrt(2 * Math.Log(2, Math.E)) * Math.Sqrt(variance);
        //    }
        //    catch (ArgumentOutOfRangeException ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //        fwhm = 0;
        //        throw;
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Diagnostics.Debug.WriteLine(ex.Message);
        //        fwhm = 0;
        //        throw;
        //    }

        //    return fwhm;
        //}
        #endregion

        #endregion
    }

#if OBSOLETE_SPECTROSCOPY
    /// <summary>
    /// This class is responsible for providing spectroscopy 
    /// behaviour to its clients.
    /// WE DONT NEED THIS CLASS, IT IS REPLACED WITH SpectroscopyService class bellow  (RP)
    /// </summary>
    internal class DefaultSpectrocopyService : ISpectroscopyService
    {
    #region METHODS

        public ulong Integral(UInt32[] spectArray, int leftBound, int rightBound)
        {
            return Integral(new Spectrum() { SpectrumArray = spectArray }, leftBound, rightBound);
        }
        public ulong Integral(Spectrum spect, int leftBound, int rightBound)
        {
            ulong integral = 0;

            // If specturm is null or empty return 0
            if (!IsValidSpectrum(spect, leftBound - 1, rightBound - 1))
            {
                return integral;
            }

            // Get the actual integral under ROI
            try
            {
                for (int i = leftBound - 1; i < rightBound && i >= 0; i++)
                {
                    integral += spect.SpectrumArray[i];
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw;
            }
            return integral;
        }

        public double CentroidFirstMoment(Spectrum spect, int leftBound, int rightBound)
        {
            //Determine the Peak Centroid using First-Moment Method 
            //described by  J.L. Parker in "General Topics in Passive Gamma-Ray Assay" ch. 5, p.105
            //PeakCentroid = SUM(Xi * Yi)/SUM(Yi)

            double peakCentroid = 0;
            // If specturm is valid
            if (!IsValidSpectrum(spect, leftBound - 1, rightBound - 1))
            {
                return peakCentroid;
            }

            try
            {
                ulong sumOfChannelsTimesCounts = 0;
                ulong sumOfCounts = 0;
                for (int channel = leftBound - 1; channel < rightBound; channel++)
                {
                    sumOfChannelsTimesCounts += ((ulong)channel + 1) * (ulong)spect.SpectrumArray[channel];
                    sumOfCounts += spect.SpectrumArray[channel];
                }

                //Calculate peak Centroid
                if (sumOfCounts == 0)
                {
                    peakCentroid = 0;
                }
                else
                {
                    peakCentroid = (double)sumOfChannelsTimesCounts / sumOfCounts;
                }
            }

            //Spectrum array did not have appropriate channel between leftBound and rightBound
            catch (ArgumentOutOfRangeException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                peakCentroid = 0;
                throw;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                peakCentroid = 0;
                throw;
            }
            return peakCentroid;
        }
        public double CentroidFirstMoment(UInt32[] specArray, int leftBound, int rightBound)
        {
            return CentroidFirstMoment(new Spectrum() { SpectrumArray = specArray }, leftBound, rightBound);
        }

        public double CentroidFiveChannel(Spectrum spect, int leftBound, int rightBound)
        {
            //Determine the Peak Centroid using Five-Channel Method 
            //described by  J.L. Parker in "General Topics in Passive Gamma-Ray Assay" ch. 5, p.105
            //PeakCentroid = SUM(Xi * Yi)/SUM(Yi)

            double peakCentroid = 0;
            // If specturm is valid
            if (!IsValidSpectrum(spect, leftBound, rightBound))
            {
                return peakCentroid;
            }
            try
            {
                uint maxCountsInChannel = 0;
                int maxChannel = int.MaxValue;
                for (int channel = leftBound - 1; channel <= rightBound; channel++)
                {
                    //find max
                    if (maxCountsInChannel < spect.SpectrumArray[channel])
                    {
                        maxCountsInChannel = spect.SpectrumArray[channel];
                        maxChannel = channel;
                    }
                }

                //Find 2 to the right and 2 to the lef
                uint numerator = (spect.SpectrumArray[maxChannel + 1] * (maxCountsInChannel - spect.SpectrumArray[maxChannel - 2])) -
                                 (spect.SpectrumArray[maxChannel - 1] * (spect.SpectrumArray[maxChannel] - spect.SpectrumArray[maxChannel + 2]));

                uint denominator = (spect.SpectrumArray[maxChannel + 1] * (maxCountsInChannel - spect.SpectrumArray[maxChannel - 2])) +
                                 (spect.SpectrumArray[maxChannel - 1] * (spect.SpectrumArray[maxChannel] - spect.SpectrumArray[maxChannel + 2]));

                peakCentroid = maxChannel + ((double)numerator / denominator);
            }
            //Spectrum array did not have appropriate channel between leftBound and rightBound
            catch (ArgumentOutOfRangeException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                peakCentroid = 0;
                throw;
            }
            catch (DivideByZeroException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                peakCentroid = 0;
                throw;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                peakCentroid = 0;
                throw;
            }
            return Math.Round(peakCentroid, 3);
        }
        public double CentroidFiveChannel(UInt32[] specArray, int leftBound, int rightBound)
        {
            return CentroidFiveChannel(new Spectrum() { SpectrumArray = specArray }, leftBound, rightBound);
        }

        /// <summary>
        /// Determination of Peak Width Using Analytical Interpolation J.L. Parker p. 117
        /// Canbera lib 3.1 is using identical method.
        /// </summary>
        /// <param name="spect">Spectrum Object</param>
        /// <param name="kMax">Factional height of interest</param>
        /// <param name="leftBound">Left bound of the ROI</param>
        /// <param name="rightBound">Right bound of the ROI</param>
        /// <returns>Full width at K Max (K - being half, 1/10, etc.)</returns>
        public double FullWidthkMax(Spectrum spect, float kmaxFraction, int leftBound, int rightBound, out double background)
        {
            double fwkm = 0d;
            int xPeak = int.MinValue;
            double yPeak = double.MinValue;

            double kMax = 0;

            double[,] xyPairs = new double[4, 2];
            double[] spectrumArray;
            background = 0;

            if (kmaxFraction <= 0 || kmaxFraction > 1) return 0;

            //Validate the spectrum, return 0 if spectrum is not valid
            if (!IsValidSpectrum(spect, leftBound - 1, rightBound - 1))
            {
                return fwkm;
            }

            background = RemoveContinuumByStraighLine(spect, leftBound, rightBound, out spectrumArray);

            //3point average Peak finding
            // double[] tempSpectrum = AverageSmoothSpectrum2(spectrumArray, 3);
            //spectrumArray = AverageSmoothSpectrum(new Spectrum { SpectrumArray = spectrumArray }, 3);

            //Find Max x,y pair
            for (int i = leftBound - 1; i < rightBound; i++)
            {
                if (spectrumArray[i] > yPeak)
                //if (Math.Round(spectrumArray[i], 3) > yPeak)
                {
                    yPeak = spectrumArray[i];
                    xPeak = i;
                }
            }
            //Determine if we failed to find our new peak
            if (double.IsInfinity(yPeak) || double.IsNaN(yPeak) || yPeak == double.MinValue || yPeak == 0) return fwkm;

            //Calculate kMax value used for FWKM calculations
            kMax = yPeak * kmaxFraction;
            //System.Diagnostics.Debug.WriteLine("Kmax=" + kMax);

            //Proceed on the left side of the peak, find pair (x1,y1) and (x2,y2)
            bool leftPairDetected = false;
            for (int ch = xPeak - 1; ch >= leftBound - 1; ch--)
            {
                //Found left pair of (x1,y1) and (x2, y2)
                if (spectrumArray[ch] < kMax)
                //if (Math.Round(spectrumArray[ch], 3) < kMax)
                {
                    //Pair below kMax line to the left of the peak
                    xyPairs[0, 0] = ch;
                    xyPairs[0, 1] = spectrumArray[ch];

                    //Pair above kMax Line
                    xyPairs[1, 0] = ch + 1;
                    xyPairs[1, 1] = spectrumArray[ch + 1];
                    leftPairDetected = true;
                    break;
                }
            }
            //System.Diagnostics.Debug.WriteLine("x1=" + xyPairs[0, 0] + " y1=" + xyPairs[0, 1]);
            //System.Diagnostics.Debug.WriteLine("x2=" + xyPairs[1, 0] + " y2=" + xyPairs[1, 1]);

            //Proceed on the right side of the peak, find pair (x1,y1) and (x2,y2)
            bool rightPairDetected = false;
            for (int ch = xPeak + 1; ch < rightBound; ch++)
            {
                if (Math.Round(spectrumArray[ch], 3) < kMax)
                {
                    //Pair below kMax line to the right of the peak
                    xyPairs[3, 0] = ch;
                    xyPairs[3, 1] = spectrumArray[ch];

                    xyPairs[2, 0] = ch - 1;
                    xyPairs[2, 1] = spectrumArray[ch - 1];
                    rightPairDetected = true;
                    break;
                }
            }
            //System.Diagnostics.Debug.WriteLine("x3=" + xyPairs[2, 0] + "y3=" + xyPairs[2, 1]);
            //System.Diagnostics.Debug.WriteLine("x4=" + xyPairs[3, 0] + "y4=" + xyPairs[3, 1]);

            //Final calculations 
            try
            {
                //x coordinate (channel) of intersection of line L with low-energy side of a peak
                double xIntersectionLow;
                if (!leftPairDetected)
                {
                    xIntersectionLow = leftBound - 1;
                }
                else
                {
                    xIntersectionLow = (kmaxFraction * yPeak - xyPairs[0, 1]) / (xyPairs[1, 1] - xyPairs[0, 1]);
                    xIntersectionLow += xyPairs[0, 0];
                }


                //x coordinate (channel) of intersection of line L with high-energy side of a peak
                double xIntersectionHigh;
                if (!rightPairDetected)
                {
                    xIntersectionHigh = rightBound - 1;
                }
                else
                {
                    xIntersectionHigh = (xyPairs[2, 1] - kmaxFraction * yPeak) / (xyPairs[2, 1] - xyPairs[3, 1]);
                    xIntersectionHigh += xyPairs[2, 0];
                }


                fwkm = xIntersectionHigh - xIntersectionLow;
            }
            catch (Exception)
            {
                fwkm = 0;
            }

            if (double.IsNaN(fwkm) || double.IsInfinity(fwkm))
            {
                fwkm = -1;
            }
            return fwkm;
        }
        public double FullWidthkMax(Spectrum spect, float kmaxFraction, int leftBound, int rightBound)
        {
            double tempFWKMbackground;

            return FullWidthkMax(spect, kmaxFraction, leftBound, rightBound, out tempFWKMbackground);
        }
        public double FirstMomentFullWidthHalfMax(Spectrum spect, double fMomentCentroid, int leftBound, int rightBound)
        {
            double fwhm = 0;
            if (!IsValidSpectrum(spect, leftBound - 1, rightBound - 1))
            {
                return fwhm;
            }
            double variance = 0;
            long sumOfCounts = 0;
            double sumOfCentroidOffsetAndCounts = 0;
            try
            {
                for (int channel = leftBound - 1; channel < rightBound; channel++)
                {
                    sumOfCentroidOffsetAndCounts += Math.Pow(((channel + 1) - fMomentCentroid), 2) * spect.SpectrumArray[channel];
                    sumOfCounts += spect.SpectrumArray[channel];
                }
                //Find variance, which is squared standard deviation
                variance = sumOfCentroidOffsetAndCounts / sumOfCounts;
                //Calculate Full Width Half Max
                fwhm = 2 * Math.Sqrt(2 * Math.Log(2, Math.E)) * Math.Sqrt(variance);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                fwhm = 0;
                throw;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                fwhm = 0;
                throw;
            }

            return fwhm;
        }

        #endregion

    #region COMPLEX FUNCTIONS OBSOLETE
        //public double CentroidLinearizedGaussian(Spectrum spect, int leftBound, int rightBound)
        //{
        //    double peakCentroid = 0;
        //    // If specturm is valid
        //    if (!IsValidSpectrum(spect, leftBound, rightBound))
        //    {
        //        return peakCentroid;
        //    }

        //    double[] testArray = new double[] { 6, 5, 7, 10 };
        //    RegressionLine line = new RegressionLine(testArray, leftBound);

        //    System.Diagnostics.Debug.WriteLine("Slope:" + line.Slope + ",  Intercept: " + line.Intercept);

        //    return peakCentroid;

        //}

        //public double CentroidLinearizedGaussian( )
        //{
        //    double peakCentroid = 0;
        //    // If specturm is valid

        //    double[] testArray = new double[] { 6, 5, 7, 10 };
        //    RegressionLine line = new RegressionLine(testArray, 0);

        //    System.Diagnostics.Debug.WriteLine("Slope:" + line.Slope + ",  Intercept: " + line.Intercept);

        //    return peakCentroid;

        //}

        //public double PrintOutCentroidAndFWHM(Spectrum spect, int leftBound, int rightBound)
        //{
        //    double peakCentroid = 0;
        //    // If specturm is valid
        //    if (!IsValidSpectrum(spect, leftBound, rightBound))
        //    {
        //        return peakCentroid;
        //    }

        //    double[] tranformedGaussian = GaussianTransformation(spect, leftBound, rightBound);

        //    RegressionLine line = new RegressionLine(tranformedGaussian, spect, leftBound, rightBound);
        //    GaussianProperties prop = new GaussianProperties(line);
        //    peakCentroid = prop.Centroid;
        //    System.Diagnostics.Debug.WriteLine("Centroid: " + prop.Centroid + ", FWHM: " + prop.FWHM);

        //    return peakCentroid;
        //}

        //public double StraightLineBackgroundSubstraction(Spectrum spect, int leftBound, int rightBound, out double xL, out double xH, out double yL, out double yH, out long bH, out long bL, out int delta)
        //{
        //    double backgroundCounts = 0;

        //    // define 2 ROIs for Low and High Energies values (immidiatly of the Peak ROI)
        //    delta = 20; // 10 channels for low and high eneries ROIs (from Peak ROI)
        //    double avgLow = 0;
        //    double avgHigh = 0;
        //    bH = bL = 0;

        //    // number of channels in ROI
        //    double Np = rightBound - leftBound + 1;
        //    double m = 0, b = 0;

        //    for (int i = leftBound - delta; i < leftBound; i++)
        //    {
        //        avgLow += (double)spect.SpectrumArray[i];
        //    }
        //    bL = (long)avgLow;
        //    avgLow /= (1.0 *delta);

        //    yL = avgLow;
        //    xL = leftBound - delta / 2.0;

        //    for (int i = rightBound + 1; i <= rightBound + delta; i++)
        //    {
        //        avgHigh += (double)spect.SpectrumArray[i];
        //    }
        //    bH = (long)avgHigh;
        //    avgHigh /= (1.0 * delta);
        //    yH = avgHigh;
        //    xH = rightBound + delta / 2.0;

        //    m = (yH - yL) / (xH - xL);
        //    b = (xH * yL - xL * yH) / (xH - xL);

        //    backgroundCounts = ((m * leftBound + b) + (m * rightBound + b)) * (Np / 2);

        //    return backgroundCounts;
        //}

        //public double average(Spectrum spect, int leftBound, int rightBound)
        //{
        //    double avg = 0;

        //    for (int i = leftBound; i <= rightBound; i++)
        //    {
        //        avg += (double)spect.SpectrumArray[i];
        //    }

        //    return (avg / (rightBound - leftBound + 1));

        //}


        //// we use ln ((y(x-1))/(y(x+1))) as transformation that linearize gaussian function
        //private double[] GaussianTransformation(Spectrum spect, int leftBount, int rightBound)
        //{
        //    // n channels in ROI gives us n-2 values in linearized function
        //    double[] linearGaussian = new double[rightBound - leftBount - 1];


        //    int pos = 0;
        //    for (int i = leftBount + 1; i <= rightBound - 1; i++, pos++)
        //    {
        //        //linearGaussian[pos] = (Math.Log((double)spect.SpectrumArray[i], Math.E));
        //        // linearGaussian[pos] = (Math.Log((double)spect.SpectrumArray[i - 1] / spect.SpectrumArray[i], Math.E));
        //        linearGaussian[pos] = (Math.Log((double)spect.SpectrumArray[i - 1] / spect.SpectrumArray[i + 1], Math.E));
        //    }

        //    return linearGaussian;

        //}
        #endregion

    #region HELPERS

        // Returns true if spectrum is valid.
        private bool IsValidSpectrum(Spectrum spect, int leftBound, int rightBound)
        {
            //Specturm is null or spectrum array is empty or range is less than or equals to 0
            if (spect == null || spect.SpectrumArray == null ||
               spect.SpectrumArray.Count() == 0 ||
               (rightBound - leftBound) <= 0)
            {
                return false;
            }


            return true;
        }

        /// <summary>
        /// Unweighted sliding-average smooth using width points
        /// </summary>
        /// <param name="spect">Spectrum object</param>
        /// <param name="width">desired width(3, 5, etc)</param>
        /// <returns>New smoothed array</returns>
        public uint[] AverageSmoothSpectrum(Spectrum spect, int width)
        {
            //Smooth array
            uint[] smoothArray = new uint[spect.SpectrumArray.Length];
            //Trim off channels for which smoothing there was not enough data
            //In general, for an m-width smooth, there will be (width-1)/2 points at
            //the begining of the spectrum and (width-1)/2 the end of the spectrum
            //for which a complete wifth smooth can not be calculated.
            Array.Clear(smoothArray, 0, spect.SpectrumArray.Length);
            for (int i = width / 2; i < spect.SpectrumArray.Length - width / 2; i++)
            {
                smoothArray[i] = spect.SpectrumArray[i - 1] + spect.SpectrumArray[i] + spect.SpectrumArray[i + 1];
                smoothArray[i] = (uint)Math.Round(smoothArray[i] / (double)width, 0);
            }

            return smoothArray;
        }

        /// <summary>
        /// Removes Compton continuum from the passed spectrum array which is part of Spectrum
        /// object (only between selected ROIs). Algorithm is used by Canbera 3.1 package and
        /// is described in details by J.L Parker p.122
        /// </summary>
        /// <param name="spect">Spectrum object</param>
        /// <param name="leftBound">Left ROI marker</param>
        /// <param name="rightBound">Right ROI marker</param>
        /// <returns>Returns continuum subtracted array</returns>
        public double RemoveContinuumByStraighLine(Spectrum spect, int leftBound, int rightBound, out double[] continumFreeSpect)
        {
            //uint[] continuumFreeSpect = new uint[spect.SpectrumArray.Length];
            continumFreeSpect = new double[spect.SpectrumArray.Length];

            //Use four channels below leftBound and rightBound
            //to establish 
            double xL = 0;
            double yL = 0;
            double xH = 0;
            double yH = 0;

            double m = double.NaN;  //slope of the straigh line used in continuum substraction
            double b = double.NaN;  //intercept of the straigh line used in continuum substraction
            double background = 0;

            //Find low x and y
            for (int i = leftBound - 2; i > leftBound - 6 && i >= 0; i--)
            {
                xL += i;
                yL += spect.SpectrumArray[i];
            }
            xL /= 4.0;    //find channel averages in the left (4 channel) ROI
            yL /= 4.0;    //find average counts in the left(4 channel) ROI

            //Find high x and y
            for (int j = rightBound; j < rightBound + 4 && j < spect.SpectrumArray.Length; j++)
            {
                xH += j;
                yH += spect.SpectrumArray[j];
            }
            xH /= 4.0;    //find channel averages in the left (4 channel) ROI
            yH /= 4.0;    //find average counts in the left(4 channel) ROI

            //Find slope (m) of the function and intercept (b)
            m = (yH - yL) / (xH - xL);
            b = (xH * yL - xL * yH) / (xH - xL);



            //Remove the background from each channel based on the line we have calculated
            for (int ch = leftBound - 1; ch < rightBound; ch++)
            {
                if ((double)spect.SpectrumArray[ch] < (m * ch + b))
                {
                    continumFreeSpect[ch] = 0;
                    background += continumFreeSpect[ch];
                }
                else
                {
                    //continuumFreeSpect[ch] = spect.SpectrumArray[ch] - (uint)(m * ch + b);
                    //continuumFreeSpect[ch] = Math.Round((double)spect.SpectrumArray[ch] - Math.Round((m * ch + b), 3), 3);
                    continumFreeSpect[ch] = (double)spect.SpectrumArray[ch] - (m * ch + b);
                    //continuumFreeSpect[ch] = (double)spect.SpectrumArray[ch];
                    background += (double)spect.SpectrumArray[ch] - continumFreeSpect[ch];
                }
            }

            //System.Diagnostics.Debug.WriteLine("m =" + m + " b= " + b);
            //System.Diagnostics.Debug.WriteLine("background = " + Math.Round(background,15));

            return background;
        }

        #endregion


        public double FullWidthkMax(uint[] spectArray, float kmaxFraction, int leftBound, int rightBound, out double background)
        {
            throw new NotImplementedException();
        }

        public double FullWidthWithKMax(Spectrum spect, float kmaxFraction, int leftBound, int rightBound)
        {
            throw new NotImplementedException();
        }
    }
#endif
}
