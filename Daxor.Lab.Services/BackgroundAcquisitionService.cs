﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using Daxor.Lab.Infrastructure.Alerts;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.SC;
using Daxor.Lab.Infrastructure.ServiceContracts.MCA;
using Daxor.Lab.Infrastructure.SystemNotifications;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.SettingsManager;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.Services
{
    /// <summary>
    /// This class is responsible to acquire background.
    /// </summary>
    internal class BackgroundAcquisitionService : IBackgroundAcquisitionService
    {
        #region Const
        
        const int SECONDS_IN_MINUTE = 60;
        const int MAX_SHUTDOWN_WAIT_MILLISECONDS = 10000;

        #endregion

        #region Fields

        readonly IMultiChannelAnalyzerProxy _mca;
        readonly ISampleChangerProxy _sampleChanger;
        readonly ISpectroscopyService _spectroscopy;
        readonly IEventAggregator _eventAggregator;
        readonly ILoggerFacade _logger;
        readonly SettingObserver<ISettingsManager> _sObserver;
        
        BackgroundWorker _acqThreadWorker;
        BackgroundAquisitionContext _context;
        double _liveTimeWhenHighCPMOccuredSec = double.NegativeInfinity;
        bool _wasGoodBkgAttained;
        int? _prevIntegral;
        int? _previousAcqTimeSec;
        object _prevIntegralLock = new object();
        object _prevAcqTimeSecLock = new object();

        //Synchronization events and locks
        ManualResetEvent _syncEvent = new ManualResetEvent(true);
        AutoResetEvent _cancelEvent = new AutoResetEvent(false);
        
        object wasBkgAttainedLock = new object();

        #endregion

        #region Ctor

        public BackgroundAcquisitionService(IMultiChannelAnalyzerProxy mca, ISampleChangerProxy sampleChanger, 
                                            ILoggerFacade logger,ISpectroscopyService spectroscopy, 
                                            IEventAggregator eventAggregator)
        {
            _mca = mca;
            _sampleChanger = sampleChanger;
            _logger = logger;
            _spectroscopy = spectroscopy;
            _eventAggregator = eventAggregator;

            this.Status = BackgroundStatus.Initializing;
            this.SubscribeToAggregateEvents();
            this.InitializeAcquisitionWorker();

            _sObserver = new SettingObserver<ISettingsManager>(SettingsManager.SystemSettingsManager.Manager);
            _sObserver.RegisterHandler("BACKGROUND_POSITION", (manager) => BackgroundPositionSettingChanged(manager, "BACKGROUND_POSITION"));
            _sObserver.RegisterHandler("BACKGROUND_ACQUISITION_TIME_SEC", (manager) => BackgroundAcquisitionTimeSettingChanged(manager, "BACKGROUND_ACQUISITION_TIME_SEC"));
            _sObserver.RegisterHandler("HIGH_CPM_RESTART_TIME_INTERVAL_SEC", (manager) => HighCpmRestartTimeIntervalSettingChanged(manager, "HIGH_CPM_RESTART_TIME_INTERVAL_SEC"));
            _sObserver.RegisterHandler("ROI_LOW_BOUND", (manager) => RoiLowBoundSettingChanged(manager, "ROI_LOW_BOUND"));
            _sObserver.RegisterHandler("ROI_HIGH_BOUND", (manager) => RoiHighBoundSettingChanged(manager, "ROI_HIGH_BOUND"));
            _sObserver.RegisterHandler("HIGH_CPM_BOUNDARY", (manager) => HighCpmBoundarySettingChanged(manager, "HIGH_CPM_BOUNDARY"));
        
        }
        #endregion

        #region SettingsChangedHandlers

        void BackgroundPositionSettingChanged(ISettingsManager sManager, string sKey)
        {
            if (_context != null)
            {
                _context.BackgroundPosition = sManager.GetSetting<Int32>(sKey);
                if (IsActive)
                {
                    WasGoodBackgroundAttained = false;
                    this.Status = BackgroundStatus.Initializing;
                }
            }
        }
        void BackgroundAcquisitionTimeSettingChanged(ISettingsManager sManager, string sKey)
        {
            if (_context == null)
                return;

            _context.PresetBkgAcquisitionTimeSeconds = sManager.GetSetting<Int32>(sKey);
            if (IsActive)
            {
                WasGoodBackgroundAttained = false;
                this.Status = BackgroundStatus.Initializing;
            }
        }
        void HighCpmRestartTimeIntervalSettingChanged(ISettingsManager sManager, string sKey)
        {
            if (_context == null)
                return;
            _context.HighCPMRestartTimeIntervalSec = sManager.GetSetting<Int32>(sKey);
        }
        void RoiLowBoundSettingChanged(ISettingsManager sManager, string sKey)
        {
            if (_context == null)
                return;
            _context.ROILowBound = sManager.GetSetting<Int32>(sKey);

        }
        void RoiHighBoundSettingChanged(ISettingsManager sManager, string sKey)
        {
            if (_context == null)
                return;
            _context.ROIHighBound = sManager.GetSetting<Int32>(sKey);
        }
        void HighCpmBoundarySettingChanged(ISettingsManager sManager, string sKey)
        {
            if (_context == null)
                return;
            _context.HighCPMBoundary = sManager.GetSetting<Int32>(sKey);
        }

        #endregion

        #region Properties

        public bool IsActive
        {
            get
            {
                return _acqThreadWorker != null && _acqThreadWorker.IsBusy && !_acqThreadWorker.CancellationPending;
            }
        }
        public bool WasGoodBackgroundAttained
        {
            get { return _wasGoodBkgAttained; }
            private set
            {
                lock (wasBkgAttainedLock)
                {
                    _wasGoodBkgAttained = value;

                    _eventAggregator.GetEvent<BkgGoodObtained>().Publish(value);

                    if (!value)
                    {
                        this.PreviousAcqTimeSec = null;
                        this.PreviousIntegral = null;
                        this.PreviousSpectrum = null;
                    }
                }
            }
        }
        public bool IsCurrentCpmTooHigh
        {
            get;
            private set;
        }

        public bool IsBackgroundOverridden
        {
            get;
            private set;
        }

        public int CurrentIntegral
        {
            get;
            private set;
        }
        public int CurrentAcqTimeSec
        {
            get;
            private set;
        }
        public int? PreviousIntegral
        {
            get { return _prevIntegral; }
            private set
            {
                if (_prevIntegral == value)
                    return;

                lock (_prevIntegralLock)
                {
                    _prevIntegral = value;
                }
            }
        }
        public int? PreviousAcqTimeSec
        {
            get { return _previousAcqTimeSec; }
            private set
            {
                if (_previousAcqTimeSec == value)
                    return;

                lock (_prevAcqTimeSecLock)
                {
                    _previousAcqTimeSec = value;
                }
            }
        }
        /// <summary>
        /// Should be the actual spectrum DOMAIN VALUE OBJECT not DTO object
        /// </summary>
        public SpectrumData PreviousSpectrum
        {
            get;
            private set;
        }
        public SpectrumData CurrentSpectrum
        {
            get;
            private set;
        }

        public int CurrentCpm
        {
            get
            {
                int currentCpm = 0;
                try
                {
                    //System.Diagnostics.Debug.WriteLine("CurrentAcqTimeSec: {0}, CurrentIntegral: {1}", this.CurrentAcqTimeSec, this.CurrentIntegral);
                    if (this.CurrentAcqTimeSec != 0 && !Double.IsNaN(this.CurrentAcqTimeSec))
                    currentCpm = (int)Math.Round(this.CurrentIntegral / (double)this.CurrentAcqTimeSec * SECONDS_IN_MINUTE, 0);
                }
                catch
                {
                    currentCpm = 0;
                }
                return currentCpm;
            }
        }
        public double CurrentCps
        {
            get
            {
                return (double)this.CurrentCpm / SECONDS_IN_MINUTE;
            }
        }

        public int? PreviousCpm
        {
            get
            {
                int? prevCpm = null;
                if (this.PreviousIntegral != null && this.PreviousAcqTimeSec != null)
                {
                    try
                    {
                        prevCpm = (int)Math.Round((int)this.PreviousIntegral / (double)this.PreviousAcqTimeSec * SECONDS_IN_MINUTE, 0);
                    }
                    catch
                    {
                        prevCpm = null;
                    }
                }

                return prevCpm;
            }
        }
        public double? PreviousCps
        {
            get
            {
                if (this.PreviousCpm == null)
                    return null;

                return (double)PreviousCpm / SECONDS_IN_MINUTE;
            }
        }
        
        public BackgroundStatus Status
        {
            get;
            private set;
        }
        public BackgroundAquisitionContext Context
        {
            get
            {
                if (_context == null)
                    return null;

                return _context.ShallowCopy();
            }
        }
        public int RunningBkgExecutionTimeInSecs
        {
            get;
            private set;
        }

        #endregion

        #region Methods

        public bool StartAsync()
        {
            this.IsBackgroundOverridden = false;

            if (_context == null)
            {
                BackgroundAquisitionContext context = new BackgroundAquisitionContext()
                {
                    MultiChannelAnalyzerID = "mca1",
                    BackgroundPosition = SystemSettingsManager.Manager.GetSetting<Int32>("BACKGROUND_POSITION"),
                    PresetBkgAcquisitionTimeSeconds = SystemSettingsManager.Manager.GetSetting<Int32>("BACKGROUND_ACQUISITION_TIME_SEC"),
                    HighCPMBoundary = SystemSettingsManager.Manager.GetSetting<Int32>("HIGH_CPM_BOUNDARY"),
                    HighCPMRestartTimeIntervalSec = SystemSettingsManager.Manager.GetSetting<Int32>("HIGH_CPM_RESTART_TIME_INTERVAL_SEC"),

                    ROILowBound = SystemSettingsManager.Manager.GetSetting<Int32>("ROI_LOW_BOUND"),
                    ROIHighBound = SystemSettingsManager.Manager.GetSetting<Int32>("ROI_HIGH_BOUND"),
                    ShouldUseLiveAcquisitionTime = true,
                };

                //Cache a copy of background acquisition context. Background acquisition context will always
                //contain simple properties, no references
                _context = context.ShallowCopy();
            }

            return this.InnerStart();
        }
        public void StopAsync()
        {
            if (_acqThreadWorker != null && _acqThreadWorker.IsBusy)
            {
                _acqThreadWorker.CancelAsync();
            }
        }
        public void OverrideBackground(int newCPM)
        {
            //Background overridden
            this.IsBackgroundOverridden = true;
            this.WasGoodBackgroundAttained = true;
            this.PreviousIntegral = newCPM;
            this.PreviousAcqTimeSec = SECONDS_IN_MINUTE;
            this.Status = BackgroundStatus.Idle;
            this.StopAsync();

            FireBackgroundObtained((int)this.PreviousIntegral, (int)this.PreviousAcqTimeSec, true, true);
        }
        public void InvalidatePreviousBackground()
        {
            this.WasGoodBackgroundAttained = false;
        }

        #endregion

        #region BackgroundWorker

        bool InnerStart()
        {
            string innerError = null;

            if (this.IsActive) return false;

            if (_context == null)
                innerError = "BackgroundAcquisitionContext was never provided.";

            if (_mca == null)
                innerError = "MultiChannelAnalyzer service cannot be found.";

            if (_sampleChanger == null)
                innerError = "SampleChanger service cannot be found.";

            if (!_mca.IsProxyOpened)
                innerError = "McaProxy is not opened.";

            if (String.IsNullOrEmpty(_context.MultiChannelAnalyzerID))
                innerError = "Not valid mcaName.";

            if (!_mca.IsConnected(_context.MultiChannelAnalyzerID))
                innerError = "Mca: " + _context.MultiChannelAnalyzerID + " is not connected.";

            //Need to check ROIs
            if (_context.ROILowBound >= _context.ROIHighBound)
                innerError = "RoiLeftBound cannot be equal or greater RoiRightBound";

            if (_context.PresetBkgAcquisitionTimeSeconds <= 0)
                innerError = "Preset acqisition time cannot be less than or equal to 0";

            if (_context.BackgroundPosition <= 0)
                innerError = "Unsupported background position";

            if (!String.IsNullOrEmpty(innerError))
                throw new ApplicationException(String.Format("{0} {1}", "Cannot start background acquisition.", innerError));

            this.Status = BackgroundStatus.Initializing;
            this.HookEventHandlers();

            //Start background acquisition
            if(!_acqThreadWorker.IsBusy)
                _acqThreadWorker.RunWorkerAsync(_context);

            return true;
        }
        void AcqThreadWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                //Gets background acquisition context; stops any existing mca acquisition and clears spectrum and times
                BackgroundAquisitionContext context = (BackgroundAquisitionContext)e.Argument;
                _sampleChanger.StopSampleChanger();
                _mca.StopAcquiring(context.MultiChannelAnalyzerID);
                _mca.ClearSpectrumResetTimes(context.MultiChannelAnalyzerID);
                _eventAggregator.GetEvent<BkgAcquisitionStarted>().Publish(null);

                while (true)
                {
                    //Background acquisition has been stopped
                    if (_acqThreadWorker.CancellationPending)
                    {
                        e.Cancel = true;
                        this.PerformThreadCancellation(context);
                        break;
                    }

                    //Determine if sample changer is at the background position. 
                    //If sample change is not at Bkg position, move it there
                    if (this.Status == BackgroundStatus.Initializing && _sampleChanger.GetCurrentPosition() == context.BackgroundPosition)
                    {
                        //We are already at the goal position
                        this.Status = BackgroundStatus.BeginAcquire;
                    }
                    if (this.Status != BackgroundStatus.SeekingBackgoundPosition && _sampleChanger.GetCurrentPosition() != context.BackgroundPosition)
                    {
                        this.Status = BackgroundStatus.SeekingBackgoundPosition;
                        _sampleChanger.MoveToPosition(context.BackgroundPosition);
                    }

                    //Found background position, start counting mca
                    if (Status == BackgroundStatus.BeginAcquire)
                        this.PerformBeginAcquire(context);

                    //Waits for acquisition complete event to get previous event
                    _syncEvent.WaitOne(2000);

                    //Acquire spectrum, run spectroscopy, update count rates and times
                    if (this.Status == BackgroundStatus.Acquiring)
                    {
                        try
                        {
                            SpectrumData spectrum = _mca.GetSpectrum(context.MultiChannelAnalyzerID);
                            double percentCompleted = 0;
                            if (spectrum != null)
                            {
                                this.CurrentAcqTimeSec = (int)Math.Round(spectrum.LiveTimeInMicroseconds / 1000000f, 0);
                                this.CurrentIntegral = Convert.ToInt32(_spectroscopy.Integral(spectrum, context.ROILowBound, context.ROIHighBound));
                                this.CurrentSpectrum = spectrum;

                                percentCompleted = (this.CurrentAcqTimeSec / (double)context.PresetBkgAcquisitionTimeSeconds) * 100.0;
                                RunningBkgExecutionTimeInSecs = this.CurrentAcqTimeSec;

                                //Deal with high background
                                if (this.CurrentCpm >= context.HighCPMBoundary)
                                {
                                    this.IsCurrentCpmTooHigh = true;
                                    FireHighBackgroundDetected(true);
                                           
                                    if (_liveTimeWhenHighCPMOccuredSec == Double.NegativeInfinity)
                                        _liveTimeWhenHighCPMOccuredSec = this.CurrentAcqTimeSec;
                                    else
                                    {
                                        Debug.WriteLine(String.Format("High background encountered waiting to reach high cpm restart interval: {0}/{1} sec", this.CurrentAcqTimeSec - _liveTimeWhenHighCPMOccuredSec, _context.HighCPMRestartTimeIntervalSec));
                                        //If we reached restart background interval, restart bkg
                                        if ((this.CurrentAcqTimeSec - _liveTimeWhenHighCPMOccuredSec) >= _context.HighCPMRestartTimeIntervalSec)
                                        {
                                            WasGoodBackgroundAttained = false;
                                            this.Status = BackgroundStatus.BeginAcquire;
                                            _liveTimeWhenHighCPMOccuredSec = Double.NegativeInfinity;
                                        }
                                    }
                                }
                                else
                                {
                                    if (IsCurrentCpmTooHigh)
                                    {
                                        IsCurrentCpmTooHigh = false;
                                        FireHighBackgroundDetected(false);
                                    }

                                    _liveTimeWhenHighCPMOccuredSec = Double.NegativeInfinity;
                                }
                            }

                            //Report the progress of background acquisiton
                            _acqThreadWorker.ReportProgress((int)Math.Round(percentCompleted, 0), (object)context.PresetBkgAcquisitionTimeSeconds);

                        }
                        //Inside catch
                        catch(Exception ex)
                        {
                            _logger.Log("Background acquisition encountered exception. Message " + ex.Message, Category.Exception, Priority.High);
                        }

                    }

                    //Let UI changes be dispatched in a timely manner
                    Thread.Sleep(1000);
                }
            }
            //Outside catch
            catch
            {
            }
        }

        private void FireHighBackgroundDetected(bool p)
        {
            SystemNotification systemNotification =
                SystemNotificationArray.Notifications[(int)Daxor.Lab.Infrastructure.SystemNotifications.SystemNotificationArray.NotificationIndex.High_Background];
            systemNotification.SystemNotificationDateTime = DateTime.Now;
            systemNotification.RemoveFromQue = !p;
            ((AlertViolationPayload)systemNotification.SystemNotificationData).RemoveAlertFromList = !p;
 
            _eventAggregator.GetEvent<SystemNotificationEvent>().Publish(systemNotification);
        }
        void AcqThreadWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            _eventAggregator.GetEvent<BkgAcquisitionProgressChanged>().Publish(
                new WorkProgressPayload()
                {
                    PercentageCompleted = e.ProgressPercentage,
                    TotalTaskTimeInSeconds = (int)e.UserState
                });
        }
        void AcqThreadWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Background worker has been cancelled
            if (e.Cancelled)
            {

            }
            else
            {
                if (e.Error != null)
                    System.Diagnostics.Debug.WriteLine("Error has occured in background acquisition service");
            }
        }
        
        #endregion
        
        #region SC_Handlers

        void sampleChangerProxy_PositionSeekCancel(int currPosition, int seekPosition)
        {
            if (!this.IsActive) return;

            //Fire SampleChangerPositionSeekCancel aggregate event
            _eventAggregator.GetEvent<SampleChangerPositionSeekCancel>().Publish(new SeekCancelPayload()
            {
                CurrentPosition = currPosition,
                SeekPosition = seekPosition
            });
        }
        void sampleChangerProxy_PositionSeekEnd(int currPosition)
        {
            if (!this.IsActive && this.Status != BackgroundStatus.SeekingBackgoundPosition) return;

            this.Status = BackgroundStatus.BeginAcquire;
        }

        void sampleChangerProxy_PositionSeekBegin(int currPosition, int seekPosition)
        {
            if (!this.IsActive) return;

            //Fire SampleChangerPositionSeekBegin aggregate event
            _eventAggregator.GetEvent<SampleChangerPositionSeekBegin>().Publish(new SeekBeginPayload()
            {
                CurrentPosition = currPosition,
                SeekPosition = seekPosition
            });
        }
        void sampleChangerProxy_PositionChangeEnd(int newPosition)
        {
            if (!this.IsActive) return;

            //Fire SampleChangerPositionChangeEnd aggregate event
            _eventAggregator.GetEvent<SampleChangerPositionChangeEnd>().Publish(new ChangeEndPayload()
            {
                NewPosition = newPosition
            });
        }
        void sampleChangerProxy_PositionChangeBegin(int oldPosition, int newPosition)
        {
            if (!this.IsActive) return;

            //Fire SampleChangerPositionChangeBegin aggregate event
            _eventAggregator.GetEvent<SampleChangerPositionChangeBegin>().Publish(new ChangeBeginPayload()
            {
                OldPosition = oldPosition,
                NewPosition = newPosition
            });

        }
        void sampleChangerProxy_ChangerError(int errorCode, string message)
        {
            if (!this.IsActive) return;

            //Fire SampleChangerError aggregate event
            _eventAggregator.GetEvent<SampleChangerError>().Publish(new ChangerErrorPayload()
            {
                ErrorCode = errorCode,
                Message = message
            });
        }

        #endregion

        #region MCA_Handlers

        void mcaProxy_AcquisitionCompleted(string mcaLogicalName, string serialNumber, SpectrumData spect)
        {
            if (!this.IsActive) return;

            //Determine if we were acquiring
            if (this.Status == BackgroundStatus.Acquiring)
            {
                //Send signal to block background acquisition thread
                _syncEvent.Reset();

                this.CurrentIntegral = Convert.ToInt32(_spectroscopy.Integral(spect, _context.ROILowBound, _context.ROIHighBound));
                this.CurrentAcqTimeSec = Context.PresetBkgAcquisitionTimeSeconds;
                this.PreviousIntegral = this.CurrentIntegral;
                this.PreviousAcqTimeSec = this.CurrentAcqTimeSec;
                this.CurrentSpectrum = spect;
                this.PreviousSpectrum = spect;
                this.WasGoodBackgroundAttained = true;
                
                this.FireBackgroundObtained((int)this.PreviousIntegral, (int)this.PreviousAcqTimeSec, true);
                this.Status = BackgroundStatus.BeginAcquire;

                //Signal to WaitOne() to proceed
                _syncEvent.Set();
            }
        }   
        void mcaProxy_AcquisitionStopped(string mcaLogicName, string serialNumber)
        {
            if (!this.IsActive) return;
        }
        void mcaProxy_AcquisitionStarted(string mcaLogicName, string serialNumber)
        {
            //if (!this.IsActive) return;
            //_eventAggregator.GetEvent<SampleAcquisitionStarted>().Publish(new SampleInfo(_bkgSample, TestType.UNDEFINED));
        }
        void mcaProxy_ProxyFailed(string errorMessage)
        {
            throw new NotImplementedException();
        }
    
        #endregion

        #region Helpers

        void InitializeAcquisitionWorker()
        {
            _acqThreadWorker = new BackgroundWorker();
            _acqThreadWorker.WorkerReportsProgress = true;
            _acqThreadWorker.WorkerSupportsCancellation = true;
            _acqThreadWorker.DoWork += AcqThreadWorker_DoWork;
            _acqThreadWorker.ProgressChanged += AcqThreadWorker_ProgressChanged;
            _acqThreadWorker.RunWorkerCompleted += AcqThreadWorker_RunWorkerCompleted;
        }
        void SubscribeToAggregateEvents()
        {
            _eventAggregator.GetEvent<TestFinished>().Subscribe((t) => this.StartAsync(), ThreadOption.PublisherThread, false);
            _eventAggregator.GetEvent<TestAborted>().Subscribe((t) =>  this.InvalidatePreviousBackground(), ThreadOption.PublisherThread, false);
        }
       
        void HookEventHandlers()
        {
            _sampleChanger.PositionChangeBegin += sampleChangerProxy_PositionChangeBegin;
            _sampleChanger.PositionChangeEnd += sampleChangerProxy_PositionChangeEnd;
            _sampleChanger.PositionSeekBegin += sampleChangerProxy_PositionSeekBegin;
            _sampleChanger.PositionSeekEnd += sampleChangerProxy_PositionSeekEnd;
            _sampleChanger.PositionSeekCancel += sampleChangerProxy_PositionSeekCancel;
            _sampleChanger.ChangerError += sampleChangerProxy_ChangerError;

            _mca.AcquisitionStarted += mcaProxy_AcquisitionStarted;
            _mca.AcquisitionStopped += mcaProxy_AcquisitionStopped;
            _mca.AcquisitionCompleted += mcaProxy_AcquisitionCompleted;
            _mca.ProxyFailed += mcaProxy_ProxyFailed;

        }
        void UnHookEventHandlers()
        {
            _sampleChanger.PositionChangeBegin -= sampleChangerProxy_PositionChangeBegin;
            _sampleChanger.PositionChangeEnd -= sampleChangerProxy_PositionChangeEnd;
            _sampleChanger.PositionSeekBegin -= sampleChangerProxy_PositionSeekBegin;
            _sampleChanger.PositionSeekEnd -= sampleChangerProxy_PositionSeekEnd;
            _sampleChanger.PositionSeekCancel -= sampleChangerProxy_PositionSeekCancel;
            _sampleChanger.ChangerError -= sampleChangerProxy_ChangerError;

            _mca.AcquisitionStarted -= mcaProxy_AcquisitionStarted;
            _mca.AcquisitionStopped -= mcaProxy_AcquisitionStopped;
            _mca.AcquisitionCompleted -= mcaProxy_AcquisitionCompleted;
            _mca.ProxyFailed -= mcaProxy_ProxyFailed;
        }

        void PerformThreadCancellation(BackgroundAquisitionContext context)
        {
            UnHookEventHandlers();
            _mca.StopAcquiring(context.MultiChannelAnalyzerID);
            _sampleChanger.StopSampleChanger();

            _eventAggregator.GetEvent<BkgAcquisitionAborted>().Publish(null);
        }
        void PerformBeginAcquire(BackgroundAquisitionContext context)
        {
            _mca.StopAcquiring(context.MultiChannelAnalyzerID);
            _mca.ClearSpectrumResetTimes(context.MultiChannelAnalyzerID);
            _mca.PresetTime(context.MultiChannelAnalyzerID, context.ShouldUseLiveAcquisitionTime, context.PresetBkgAcquisitionTimeSeconds);
            _mca.StartAcquiring(context.MultiChannelAnalyzerID);
            this.Status = BackgroundStatus.Acquiring;
        }

       

        #endregion

        #region Events

        public event EventHandler<BackgroundObtainedEventArgs> GoodBackgroundObtained;
        protected virtual void FireBackgroundObtained(int backgroundCount, int acquisitionTimeInSec, bool goodBackground, bool overridden = false)
        {
            var handler = GoodBackgroundObtained;
            if (handler != null)
                handler(this, new BackgroundObtainedEventArgs(backgroundCount, acquisitionTimeInSec, goodBackground, overridden));
        }

        #endregion
    }
}
