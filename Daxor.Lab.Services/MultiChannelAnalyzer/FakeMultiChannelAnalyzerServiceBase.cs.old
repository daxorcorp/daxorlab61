﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.ServiceContracts.MCA;
using Daxor.Lab.MCAContracts;
using System.Timers;

namespace Daxor.Lab.Services.MultiChannelAnalyzer
{
    /// <summary>
    /// Abstract multi channel analyzer demo service
    /// </summary>
    public abstract class FakeMultiChannelAnalyzerServiceBase : IMultiChannelAnalyzerProxy
    {
        #region Constants

        protected const int NUM_SPECTRUM_CHANNELS = 1024;
        protected const int MICROSECONDS_IN_SECOND = 1000000;
        protected const int MICROSECONDS_IN_MILLISECOND = 1000;
        protected const int MICROSECONDS_IN_MINUTE = 60000000;
        protected const int MILISECONDS_IN_SECOND = 1000;

        #endregion

        #region Ctor

        protected FakeMultiChannelAnalyzerServiceBase()
        {
            _acquistionTimer = new Timer();
        }

        #endregion

        #region Events

        public event ProxyAcqCompletedHandler AcquisitionCompleted;
        public event ProxyAcqStartedHandler AcquisitionStarted;
        public event ProxyAcqStoppedHandler AcquisitionStopped;
        public event ProxyMcaPluggedHandler McaPlugged;
        public event ProxyMcaUnpluggedHandler McaUnplugged;
        public event ProxyFailedHandler ProxyFailed;
        public event ProxyOpenedHandler ProxyOpened;

        protected virtual void FireAcquisitionCompleted(string mcaLogicalName, string mcaSerialNumber, SpectrumData spec)
        {
            _isAcquisitionFinished = true;
            ProxyAcqCompletedHandler handler = this.AcquisitionCompleted;
            if (handler != null)
                handler(mcaLogicalName, mcaSerialNumber, spec);
        }
        protected virtual void FireAcquisitionStarted(string mcaLogicalName, string mcaSerialNumber)
        {
            _isAcquisitionFinished = false;
            ProxyAcqStartedHandler handler = this.AcquisitionStarted;
            if (handler != null)
                handler(mcaLogicalName, mcaSerialNumber);
        }
        protected virtual void FireAcquisitionStopped(string mcaLogicalName, string mcaSerialNumber)
        {
            _isAcquisitionFinished = false;
            ProxyAcqStoppedHandler handler = this.AcquisitionStopped;
            if (handler != null)
                handler(mcaLogicalName, mcaSerialNumber);
        }
        protected virtual void FireMcaPlugged(string mcaLogicalName, string mcaSerialNumber)
        {
            ProxyMcaPluggedHandler hander = this.McaPlugged;
            if (hander != null)
                hander(mcaLogicalName, mcaSerialNumber);
        }
        protected virtual void FireMcaUnplugged(string mcaLogicalName, string mcaSerialNumber)
        {
            ProxyMcaUnpluggedHandler handler = this.McaUnplugged;
            if (handler != null)
                handler(mcaLogicalName, mcaSerialNumber);
        }
        protected virtual void FireProxyFailed(string errorMessage)
        {
            ProxyFailedHandler handler = this.ProxyFailed;
            if (handler != null)
                handler(errorMessage);
        }
        protected virtual void FireProxyOpened()
        {
            ProxyOpenedHandler handler = this.ProxyOpened;
            if (handler != null)
                handler();
        }

        #endregion

        #region Properties

        public IList<string> MultiChannelAnalyzerCollection
        {
            get { return _mcaCollection; }
        }
        public bool IsProxyOpened
        {
            get { return _isProxyOpened; }
        }
        public string Name
        {
            get { return _mcaName; }
        }

        #endregion

        #region Methods

        public IList<string> GetAllRegisteredMcaNames()
        {
            return new List<String>() { "mca1" };
        }
       
        public int GetVoltage(string mcaName)
        {
            return _voltage;
        }
        public void SetVoltage(string mcaName, int voltage)
        {
            _voltage = voltage;
        }
        
        public double GetFineGain(string mcaName)
        {
            return _fineGain;
        }
        public void SetFineGain(string mcaName, double fg)
        {
            _fineGain = fg;
        }
        
        public int GetNoise(string mcaName)
        {
            return _noise;
        }
        public void SetNoise(string mcaName, int noise)
        {
            _noise = noise;
        }
       
        public int GetLLD(string mcaName)
        {
            return _lld;
        }
        public void SetLLD(string mcaName, int lld)
        {
            _lld = lld;
        }
   
        public bool IsAcquiring(string mcaName)
        {
            return _isAcquiring;
        }
        public bool IsConnected(string mcaName)
        {
            return _isConnected;
        }
        public bool IsRealTimePreset(string mcaName)
        {
            return _acquireToLiveTime.HasValue && !_acquireToLiveTime.Value;
        }
        public bool IsLiveTimePreset(string mcaName)
        {
            return _acquireToLiveTime.HasValue && _acquireToLiveTime.Value;
        }
        public bool IsGrossCountsPreset(string mcaName)
        {
            return false;
        }
        public bool HasAcquisitionCompleted(string mcaName)
        {
            return _isAcquisitionFinished;
        }

        public string SerialNumber(string mcaName)
        {
            return Guid.NewGuid().ToString();
        }
        public int SpectrumLength(string mcaName)
        {
            return NUM_SPECTRUM_CHANNELS;
        }
        public double GetAdcCurrent(string mcaName)
        {
            return _voltage;
        }

        public void SaveMCAsSettings()
        {

        }

        public void ResetHardwareSettings()
        {

        }

        public void ShutDownMCAThread(string mcaName)
        {
            return;
        }
        public void PresetTime(string mcaName, bool isLiveTime, long presetTimeInSeconds)
        {
            if (!_isAcquiring)
            {
                _acquireToLiveTime = isLiveTime;
                _mcaPresetAcqTimeInMicroSeconds = presetTimeInSeconds * MICROSECONDS_IN_SECOND;
            }
        }
        public void PresetGrossCounts(string mcaName, long presetCounts, int leftMarker, int rightMarker)
        {
            throw new NotSupportedException("PresetGrossCounts are not supported by SciniSPEC mca");
        }
        public void Subscribe()
        {
            System.Diagnostics.Debug.WriteLine("MCA ID: " + _serialNumber + " subscribed.");
        }
        public void Unsubscribe()
        {
            System.Diagnostics.Debug.WriteLine("MCA ID: " + _serialNumber + " unsubscribed.");
        }
        
        #endregion

        #region Abstract

        public virtual void OpenProxyAndSubscribe()
        {
            _isProxyOpened = true;
        }
        public abstract SpectrumData GetSpectrum(string mcaName);
        public abstract void ClearSpectrumResetTimes(string mcaName);
        public abstract bool StartAcquiring(string mcaName);
        public abstract void StopAcquiring(string mcaName);

        #endregion

        #region Fields

        protected Timer _acquistionTimer;
        protected IList<String> _mcaCollection;
        protected bool _isProxyOpened;
        protected bool _isAcquiring;
        protected bool _isConnected;
        protected bool _isAcquisitionFinished;
        protected bool? _acquireToLiveTime;

        protected string _mcaName;
        protected string _serialNumber = "DAXOR-2011-MCA-DEMO";

        protected long _mcaPresetAcqTimeInMicroSeconds = -1;

        private int _voltage = 1000;
        private int _noise;
        private int _lld;
        private double _fineGain;



        #endregion
    }
}
