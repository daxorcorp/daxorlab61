﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.Services.MultiChannelAnalyzer.Common
{
    public interface ISpectrumLoader
    {
        uint[] Load();
    }
}
