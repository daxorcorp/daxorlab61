﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Daxor.Lab.Services.MultiChannelAnalyzer.Common
{
    public class XmlSpectrumLoader : ISpectrumLoader
    {
        private readonly string _xmlUri;
        private readonly int _spectID;

        public XmlSpectrumLoader(string xmlUri, int spectID=1)
        {
            _xmlUri = xmlUri;
            _spectID = spectID;
        }

        public uint[] Load()
        {
            uint[] spectrum = new uint[1024];
            try
            {
                XDocument doc = XDocument.Load(_xmlUri);
                var q = (from s in doc.Descendants("Spectrum")
                        where (int)s.Attribute("ID") == _spectID
                        select (String)s.Element("SpectrumArray")).FirstOrDefault();

                int channel = 0;
                foreach (var counts in q.TrimEnd(' ').Split(' '))
                {
                    if (counts != "")
                    {
                        spectrum[channel] = UInt32.Parse(counts);
                        channel++;
                    }
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("XmlSpectrumLoader failed. Message: " + ex.Message);
                spectrum = new uint[1024];
            }

            return spectrum;
        }
    }
}
