﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.SettingsManager;

namespace Daxor.Lab.Services
{
    /// <summary>
    /// Simple user authentication service
    /// </summary>
    public class AuthenticationManager : ObservableObject, IAuthenticationManager
    {
        #region Fields

        User _loggedInUser;

        #endregion

        #region Methods

        public bool Login(AuthorizationLevel desiredLevel, string lPassword)
        {
            try
            {
                string passcode = SystemSettingsManager.Manager.GetSetting<String>(User.AuthorizationLevelToSettingKey(desiredLevel));
                if (passcode == lPassword)
                {
                    LoggedInUser = new User(desiredLevel, lPassword);
                    this.RaiseLoginEvent();
                }
                else
                {
                    this.RaiseLoginFailed();
                }
            }
            catch
            {
                LoggedInUser = null;
            }
            return LoggedInUser != null;
        }
        public User Login(String password)
        {
            try
            {
                AuthorizationLevel[] topBottomAuthLevels = new AuthorizationLevel[] { AuthorizationLevel.CustomerAnonymous, AuthorizationLevel.CustomerAdmin, AuthorizationLevel.Service, AuthorizationLevel.Internal };
                User loggedInUser = null;

                for (int lIndex = 0; lIndex < topBottomAuthLevels.Length; lIndex++)
                {
                    string passcode = SystemSettingsManager.Manager.GetSetting<String>(User.AuthorizationLevelToSettingKey(topBottomAuthLevels[lIndex]));
                    if (passcode == password)
                    {
                        loggedInUser = new User(topBottomAuthLevels[lIndex], password);
                        break;
                    }
                }
                if (loggedInUser == null)
                    RaiseLoginFailed();
                else
                    RaiseLoginEvent();

                _loggedInUser = loggedInUser;
            }
            catch (Exception ex)
            {
                _loggedInUser = null;
            }
            return _loggedInUser;
        }

        public void Logout()
        {
            LoggedInUser = null;
            FireLogoutCompleted();
        }
        public User LoggedInUser
        {
            get { return _loggedInUser; }
            private set { base.SetValue(ref _loggedInUser, "LoggedInUser", value); }
        }

        #endregion

        #region Events

        public event EventHandler LoginFailed;
        public event EventHandler LoginSucceeded;
        public event EventHandler LogoutCompleted;

        protected virtual void RaiseLoginFailed()
        {
            EventHandler handler = this.LoginFailed;
            if (handler != null)
                handler(this, null);
        }
        protected virtual void FireLoginSucceeded()
        {
            EventHandler handler = this.LoginSucceeded;
            if (handler != null)
                handler(this, null);
        }
        protected virtual void FireLogoutCompleted()
        {
            EventHandler handler = this.LogoutCompleted;
            if (handler != null)
                handler(this, null);
        }

        //Fire event if login was successfull or not
        void RaiseLoginEvent()
        {
            if (LoggedInUser != null)
                FireLoginSucceeded();
            else
                RaiseLoginFailed();
        }
        #endregion
    }
}
