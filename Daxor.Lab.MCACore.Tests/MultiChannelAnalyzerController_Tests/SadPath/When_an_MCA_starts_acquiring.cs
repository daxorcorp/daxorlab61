using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAContracts;using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_an_MCA_starts_acquiring
    {
        private IMultiChannelAnalyzer _stubConnectedMca;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _stubConnectedMca = Substitute.For<IMultiChannelAnalyzer>();
            _stubConnectedMca.IsConnected.Returns(true);
            _stubConnectedMca.SerialNumber.Returns(12345);

            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(_stubConnectedMca);

            _mockLogger = Substitute.For<IWindowsEventLogger>();

            // ReSharper disable once UnusedVariable
            var ignoredController = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(), stubMcaBuilder, _mockLogger);
        }

        [TestMethod]
        public void And_the_MCA_is_unknown_to_the_controller_than_an_exception_is_thrown()
        {
            var unknownMca = Substitute.For<IMultiChannelAnalyzer>();

            AssertEx.Throws(() => _stubConnectedMca.AcquisitionStarted +=
                Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(unknownMca, DateTime.Now));
        }
    }
    // ReSharper restore InconsistentNaming
}
