using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_a_USB_device_is_connected
    {
        private IMultiChannelAnalyzer _mockDisconnectedMca;
        private IUsbDeviceWatcher _stubUsbDeviceWatcher;

        [TestInitialize]
        public void BeforeEachTest()
        {
            var mockConnectedMca = Substitute.For<IMultiChannelAnalyzer>();
            _mockDisconnectedMca = Substitute.For<IMultiChannelAnalyzer>();

            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(mockConnectedMca);
            stubMcaBuilder.IsRecognizedManufacturer("whatever").ReturnsForAnyArgs(true);

            _stubUsbDeviceWatcher = Substitute.For<IUsbDeviceWatcher>();

            var controller = new McaControllerWithExposedMethods(_stubUsbDeviceWatcher, stubMcaBuilder, Substitute.For<IWindowsEventLogger>());

            controller.AddConnectedMca("conn1", mockConnectedMca);
            controller.AddDisconnectedMca("conn1", _mockDisconnectedMca);

            mockConnectedMca.ClearReceivedCalls();
            _mockDisconnectedMca.ClearReceivedCalls();
        }

        [TestMethod]
        public void And_a_device_by_the_same_ID_is_already_connected_then_an_exception_is_thrown()
        {
            _mockDisconnectedMca.IsConnected.Returns(true);

            AssertEx.Throws(() => _stubUsbDeviceWatcher.Connected += Raise.Event<Action<string>>("unknown"));
        }
    }
    // ReSharper restore InconsistentNaming
}
