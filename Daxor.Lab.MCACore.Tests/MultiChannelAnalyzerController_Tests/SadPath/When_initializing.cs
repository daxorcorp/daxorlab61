using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        [TestMethod]
        public void And_the_USB_device_watcher_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentNullException>(() => new MultiChannelAnalyzerController(null,
                Substitute.For<IMultiChannelAnalyzerBuilder>(), Substitute.For<IWindowsEventLogger>()));
        }

        [TestMethod]
        public void And_the_MCA_builder_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentNullException>(() => new MultiChannelAnalyzerController(Substitute.For<IUsbDeviceWatcher>(),
                null, Substitute.For<IWindowsEventLogger>()));
        }

        [TestMethod]
        public void And_the_event_logger_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentNullException>(() => new MultiChannelAnalyzerController(Substitute.For<IUsbDeviceWatcher>(),
                Substitute.For<IMultiChannelAnalyzerBuilder>(), null));
        }
        
        [TestMethod]
        public void And_the_MCA_builder_throws_an_exception_then_the_exception_is_logged()
        {
            var mcaBuilderThatThrowsException = Substitute.For<IMultiChannelAnalyzerBuilder>();
            mcaBuilderThatThrowsException.BuildScintiSpecMultiChannelAnalyzer().Returns(x => { throw new Exception("Problem"); });

            var mockLogger = Substitute.For<IWindowsEventLogger>();

            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws(() => new MultiChannelAnalyzerController(Substitute.For<IUsbDeviceWatcher>(),
                mcaBuilderThatThrowsException, mockLogger));

            mockLogger.Received().Log(Arg.Is<string>(m => m.StartsWith("Unable to initialize the MCA controller: Problem")));
        }

        [TestMethod]
        public void And_there_are_no_MCAs_connected_then_an_exception_is_thrown()
        {
            var wasExceptionThrown = false;

            try
            {
                // ReSharper disable once UnusedVariable
                var controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(),
                    Substitute.For<IMultiChannelAnalyzerBuilder>(), Substitute.For<IWindowsEventLogger>(), true);
            }
            catch (Exception ex)
            {
                wasExceptionThrown = true;
                Assert.AreEqual("No connected MCAs are available", ex.Message);
            }

            Assert.IsTrue(wasExceptionThrown);
        }
    }
    // ReSharper restore InconsistentNaming
}
