using System.Collections.Generic;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_resetting_an_MCA_to_its_default_configuration
    {
        [TestMethod]
        public void And_the_given_ID_does_not_exist_then_an_exception_is_thrown()
        {
            var mockMca = Substitute.For<IMultiChannelAnalyzer>();
            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(mockMca);
            var controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(), stubMcaBuilder, Substitute.For<IWindowsEventLogger>());
            controller.AddConnectedMca("conn1", mockMca);

            AssertEx.Throws<KeyNotFoundException>(() => controller.ResetMultiChannelAnalyzerToDefaultConfiguration("notfound"));

            mockMca.DidNotReceive().ResetToDefaultConfiguration();
        }
    }
    // ReSharper restore InconsistentNaming
}
