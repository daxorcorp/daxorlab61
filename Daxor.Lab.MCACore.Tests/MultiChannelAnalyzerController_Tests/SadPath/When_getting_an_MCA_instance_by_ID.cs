using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_an_MCA_instance_by_ID
    {
        [TestMethod]
        public void And_there_is_no_MCA_by_that_ID_then_an_exception_is_thrown()
        {
            var stubMca = Substitute.For<IMultiChannelAnalyzer>();
            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(stubMca);
            var controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(), 
                stubMcaBuilder, Substitute.For<IWindowsEventLogger>());

            // Make sure both lists have something
            controller.AddConnectedMca("abc", stubMca);
            controller.AddDisconnectedMca("def", stubMca);

            var wasExceptionThrown = true;
            try
            {
                // ReSharper disable once UnusedVariable
                var mca = controller.GetMultiChannelAnalyzerWithId("notfound");
                wasExceptionThrown = false;
            }
            catch (Exception ex)
            {
                Assert.AreEqual("Unable to find MCA by ID 'notfound'", ex.Message);
            }

            if (!wasExceptionThrown) Assert.Fail("Attempting to find non-existing MCA did not fail");
        }
    }
    // ReSharper restore InconsistentNaming
}
