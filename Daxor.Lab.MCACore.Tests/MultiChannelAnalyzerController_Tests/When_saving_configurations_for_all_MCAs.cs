using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_saving_configurations_for_all_MCAs
    {
        private IMultiChannelAnalyzer _stubConnectedMca;
        private IMultiChannelAnalyzer _stubDisconnectedMca;
        private McaControllerWithExposedMethods _controller;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _stubConnectedMca = Substitute.For<IMultiChannelAnalyzer>();
            _stubDisconnectedMca = Substitute.For<IMultiChannelAnalyzer>();

            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(_stubConnectedMca);

            _mockLogger = Substitute.For<IWindowsEventLogger>();

            _controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(), stubMcaBuilder, _mockLogger);

            _controller.AddConnectedMca("conn1", _stubConnectedMca);
            _controller.AddDisconnectedMca("disconn1", _stubDisconnectedMca);
        }

        [TestMethod]
        public void And_an_MCA_is_connected_then_its_configuration_is_saved()
        {
            _controller.SaveConfigurationsForAllMultiChannelAnalyzers();

            _stubConnectedMca.Received().SaveConfiguration();
        }

        [TestMethod]
        public void And_an_MCA_is_disconnected_then_its_configuration_is_saved()
        {
            _controller.SaveConfigurationsForAllMultiChannelAnalyzers();

            _stubDisconnectedMca.Received().SaveConfiguration();
        }

        [TestMethod]
        public void Then_the_action_is_logged()
        {
            _controller.SaveConfigurationsForAllMultiChannelAnalyzers();

            _mockLogger.Received().Log("Saving configurations for all MCAs");
        }
    }
    // ReSharper restore InconsistentNaming
}
