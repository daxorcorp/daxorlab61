using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_an_MCA_completes_its_acquisition
    {
        private const string ExpectedMcaId = "scintiSPEC12345";

        private IMultiChannelAnalyzer _stubConnectedMca;
        private McaControllerWithExposedMethods _controller;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _stubConnectedMca = Substitute.For<IMultiChannelAnalyzer>();
            _stubConnectedMca.IsConnected.Returns(true);
            _stubConnectedMca.SerialNumber.Returns(12345);

            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(_stubConnectedMca);

            _mockLogger = Substitute.For<IWindowsEventLogger>();

            _controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(), stubMcaBuilder, _mockLogger);
        }

        [TestMethod]
        public void Then_the_controller_raises_the_AcquisitionCompleted_event_with_the_correct_parameters()
        {
            var observedMcaId = string.Empty;
            SpectrumData observedSpectrumData = null;

            var expectedSpectrumData = new SpectrumData(3, 4, 5, new uint[4]);

            _controller.AcquisitionCompleted += (s, data) =>
            {
                observedMcaId = s;
                observedSpectrumData = data;
            };
            
            _stubConnectedMca.AcquisitionCompleted += Raise.Event<Action<object, SpectrumData>>(_stubConnectedMca, expectedSpectrumData);

            Assert.AreEqual(ExpectedMcaId, observedMcaId);
            Assert.AreEqual(expectedSpectrumData, observedSpectrumData);
        }

        [TestMethod]
        public void Then_the_action_is_logged()
        {
            _stubConnectedMca.AcquisitionCompleted += Raise.Event<Action<object, SpectrumData>>(_stubConnectedMca, new SpectrumData(3, 4, 5, new uint[4]));

            _mockLogger.Received().Log("MCA '" + ExpectedMcaId + "' completed acquisition");
        }
    }
    // ReSharper restore InconsistentNaming
}
