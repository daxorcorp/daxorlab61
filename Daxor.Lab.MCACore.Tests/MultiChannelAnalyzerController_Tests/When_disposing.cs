using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_disposing
    {
        [TestMethod]
        public void Then_each_connected_MCA_is_shut_down()
        {
            var firstMockConnectedMca = Substitute.For<IMultiChannelAnalyzer>();
            firstMockConnectedMca.SerialNumber.Returns(12345);
            firstMockConnectedMca.IsConnected.Returns(true);
            var secondMockConnectedMca = Substitute.For<IMultiChannelAnalyzer>();
            var controller = CreateControllerWithTwoConnectedMcasAndOneDisconnectedMca(firstMockConnectedMca,
                secondMockConnectedMca, Substitute.For<IMultiChannelAnalyzer>());

            // Act
            controller.Dispose();

            firstMockConnectedMca.Received().ShutDown();
            secondMockConnectedMca.Received().ShutDown();
        }

        [TestMethod]
        public void Then_a_disconnected_MCA_is_not_shut_down()
        {
            var mockDisconnectedMca = Substitute.For<IMultiChannelAnalyzer>();
            var controller = CreateControllerWithTwoConnectedMcasAndOneDisconnectedMca(Substitute.For<IMultiChannelAnalyzer>(),
                Substitute.For<IMultiChannelAnalyzer>(), mockDisconnectedMca);

            // Act
            controller.Dispose();

            mockDisconnectedMca.DidNotReceive().ShutDown();
        }

        [TestMethod]
        public void Then_the_controller_unsubscribes_from_AcquisitionCompleted_events_on_all_MCAs()
        {
            // Set up 2 connected, and 2 disconnected MCAs; each is connected to event handlers
            IMultiChannelAnalyzer connectedMca1;
            IMultiChannelAnalyzer connectedMca2;
            IMultiChannelAnalyzer disconnectedMca1;
            IMultiChannelAnalyzer disconnectedMca2;
            var controller = CreateControllerWithConnectedAndDisconnectedMcasHookedUpToEventHandlers(out connectedMca1, out connectedMca2, 
                out disconnectedMca1, out disconnectedMca2);

            // Ensure all four MCAs are seen via events
            int[] acquisitionCompletedEventCount = {0};
            controller.AcquisitionCompleted += (s, data) => { acquisitionCompletedEventCount[0]++; };
            connectedMca1.AcquisitionCompleted += Raise.Event<Action<IMultiChannelAnalyzer, SpectrumData>>(connectedMca1, null);
            connectedMca2.AcquisitionCompleted += Raise.Event<Action<IMultiChannelAnalyzer, SpectrumData>>(connectedMca2, null);
            disconnectedMca1.AcquisitionCompleted += Raise.Event<Action<IMultiChannelAnalyzer, SpectrumData>>(disconnectedMca1, null);
            disconnectedMca2.AcquisitionCompleted += Raise.Event<Action<IMultiChannelAnalyzer, SpectrumData>>(disconnectedMca2, null);
            Assert.AreEqual(4, acquisitionCompletedEventCount[0]);
            acquisitionCompletedEventCount[0] = 0;

            // Act
            controller.Dispose();

            // Assert
            connectedMca1.AcquisitionCompleted += Raise.Event<Action<IMultiChannelAnalyzer, SpectrumData>>(connectedMca1, null);
            connectedMca2.AcquisitionCompleted += Raise.Event<Action<IMultiChannelAnalyzer, SpectrumData>>(connectedMca2, null);
            disconnectedMca1.AcquisitionCompleted += Raise.Event<Action<IMultiChannelAnalyzer, SpectrumData>>(disconnectedMca1, null);
            disconnectedMca2.AcquisitionCompleted += Raise.Event<Action<IMultiChannelAnalyzer, SpectrumData>>(disconnectedMca2, null);
            Assert.AreEqual(0, acquisitionCompletedEventCount[0]);
        }

        [TestMethod]
        public void Then_the_controller_unsubscribes_from_AcquisitionStarted_events_on_all_MCAs()
        {
            // Set up 2 connected, and 2 disconnected MCAs; each is connected to event handlers
            IMultiChannelAnalyzer connectedMca1;
            IMultiChannelAnalyzer connectedMca2;
            IMultiChannelAnalyzer disconnectedMca1;
            IMultiChannelAnalyzer disconnectedMca2;
            var controller = CreateControllerWithConnectedAndDisconnectedMcasHookedUpToEventHandlers(out connectedMca1, out connectedMca2,
                out disconnectedMca1, out disconnectedMca2);

            // Ensure all four MCAs are seen via events
            int[] acquisitionStartedEventCount = { 0 };
            controller.AcquisitionStarted += (s, time) => { acquisitionStartedEventCount[0]++; };
            connectedMca1.AcquisitionStarted += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(connectedMca1, DateTime.Now);
            connectedMca2.AcquisitionStarted += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(connectedMca2, DateTime.Now);
            disconnectedMca1.AcquisitionStarted += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(disconnectedMca1, DateTime.Now);
            disconnectedMca2.AcquisitionStarted += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(disconnectedMca2, DateTime.Now);
            Assert.AreEqual(4, acquisitionStartedEventCount[0]);
            acquisitionStartedEventCount[0] = 0;

            // Act
            controller.Dispose();

            // Assert
            connectedMca1.AcquisitionStarted += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(connectedMca1, DateTime.Now);
            connectedMca2.AcquisitionStarted += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(connectedMca2, DateTime.Now);
            disconnectedMca1.AcquisitionStarted += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(disconnectedMca1, DateTime.Now);
            disconnectedMca2.AcquisitionStarted += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(disconnectedMca2, DateTime.Now);
            Assert.AreEqual(0, acquisitionStartedEventCount[0]);
        }

        [TestMethod]
        public void Then_the_controller_unsubscribes_from_AcquisitionStopped_events_on_all_MCAs()
        {
            // Set up 2 connected, and 2 disconnected MCAs; each is connected to event handlers
            IMultiChannelAnalyzer connectedMca1;
            IMultiChannelAnalyzer connectedMca2;
            IMultiChannelAnalyzer disconnectedMca1;
            IMultiChannelAnalyzer disconnectedMca2;
            var controller = CreateControllerWithConnectedAndDisconnectedMcasHookedUpToEventHandlers(out connectedMca1, out connectedMca2,
                out disconnectedMca1, out disconnectedMca2);

            // Ensure all four MCAs are seen via events
            int[] acquisitionStoppedEventCount = { 0 };
            controller.AcquisitionStopped += (s, time) => { acquisitionStoppedEventCount[0]++; };
            connectedMca1.AcquisitionStopped += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(connectedMca1, DateTime.Now);
            connectedMca2.AcquisitionStopped += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(connectedMca2, DateTime.Now);
            disconnectedMca1.AcquisitionStopped += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(disconnectedMca1, DateTime.Now);
            disconnectedMca2.AcquisitionStopped += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(disconnectedMca2, DateTime.Now);
            Assert.AreEqual(4, acquisitionStoppedEventCount[0]);
            acquisitionStoppedEventCount[0] = 0;

            // Act
            controller.Dispose();

            // Assert
            connectedMca1.AcquisitionStopped += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(connectedMca1, DateTime.Now);
            connectedMca2.AcquisitionStopped += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(connectedMca2, DateTime.Now);
            disconnectedMca1.AcquisitionStopped += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(disconnectedMca1, DateTime.Now);
            disconnectedMca2.AcquisitionStopped += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(disconnectedMca2, DateTime.Now);
            Assert.AreEqual(0, acquisitionStoppedEventCount[0]);
        }

        [TestMethod]
        public void Then_the_USB_device_watcher_is_stopped()
        {
            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(Substitute.For<IMultiChannelAnalyzer>());
            var mockDeviceWatcher = Substitute.For<IUsbDeviceWatcher>();
            var controller = new McaControllerWithExposedMethods(mockDeviceWatcher, stubMcaBuilder,
                Substitute.For<IWindowsEventLogger>());

            controller.Dispose();

            mockDeviceWatcher.Received().Stop();
        }

        [TestMethod]
        public void Then_the_controller_unsubscribes_from_the_USB_device_watcher_Connected_event()
        {
            var mockMca = Substitute.For<IMultiChannelAnalyzer>();
            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(mockMca);
            stubMcaBuilder.IsRecognizedManufacturer("whatever").Returns(true);
            var stubDeviceWatcher = Substitute.For<IUsbDeviceWatcher>();
            var controller = new McaControllerWithExposedMethods(stubDeviceWatcher, stubMcaBuilder, Substitute.For<IWindowsEventLogger>());
            controller.AddDisconnectedMca("123", mockMca);

            mockMca.ClearReceivedCalls();
            stubDeviceWatcher.Connected += Raise.Event<Action<string>>("whatever");
            // ReSharper disable once NotAccessedVariable
            var unused = mockMca.Received().IsConnected;
            mockMca.ClearReceivedCalls();

            // Act
            controller.Dispose();
            
            // Assert
            stubDeviceWatcher.Connected += Raise.Event<Action<string>>("whatever");
            // ReSharper disable once RedundantAssignment
            unused = mockMca.DidNotReceive().IsConnected;
        }

        [TestMethod]
        public void Then_the_controller_unsubscribes_from_the_USB_device_watcher_Disonnected_event()
        {
            var mockMca = Substitute.For<IMultiChannelAnalyzer>();
            mockMca.IsConnected.Returns(true);
            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(mockMca);
            stubMcaBuilder.IsRecognizedManufacturer("whatever").Returns(true);
            var stubDeviceWatcher = Substitute.For<IUsbDeviceWatcher>();
            var controller = new McaControllerWithExposedMethods(stubDeviceWatcher, stubMcaBuilder, Substitute.For<IWindowsEventLogger>());
            controller.AddConnectedMca("123", mockMca);

            mockMca.ClearReceivedCalls();
            stubDeviceWatcher.Disconnected += Raise.Event<Action>();
            // ReSharper disable once NotAccessedVariable
            var unused = mockMca.Received().IsConnected;
            mockMca.ClearReceivedCalls();

            // Act
            controller.Dispose();

            // Assert
            stubDeviceWatcher.Disconnected += Raise.Event<Action>();
            // ReSharper disable once RedundantAssignment
            unused = mockMca.DidNotReceive().IsConnected;
        }

        [TestMethod]
        public void Then_the_action_is_logged()
        {
            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(Substitute.For<IMultiChannelAnalyzer>());
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(), stubMcaBuilder,
                mockLogger);

            controller.Dispose();

            mockLogger.Received().Log("MCA controller disposed");
        }

        private static McaControllerWithExposedMethods CreateControllerWithConnectedAndDisconnectedMcasHookedUpToEventHandlers(
            out IMultiChannelAnalyzer connectedMca1, out IMultiChannelAnalyzer connectedMca2, 
            out IMultiChannelAnalyzer disconnectedMca1, out IMultiChannelAnalyzer disconnectedMca2)
        {
            connectedMca1 = Substitute.For<IMultiChannelAnalyzer>();
            connectedMca2 = Substitute.For<IMultiChannelAnalyzer>();
            disconnectedMca1 = Substitute.For<IMultiChannelAnalyzer>();
            disconnectedMca2 = Substitute.For<IMultiChannelAnalyzer>();

            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(connectedMca1);

            var controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(), stubMcaBuilder,
                Substitute.For<IWindowsEventLogger>());

            controller.AddConnectedMca("conn1", connectedMca1);
            controller.AddConnectedMca("conn2", connectedMca2);
            controller.AddDisconnectedMca("disconn1", disconnectedMca1);
            controller.AddDisconnectedMca("disconn2", disconnectedMca2);

            controller.ConnectMcaEventHandlersToMca(connectedMca1);
            controller.ConnectMcaEventHandlersToMca(connectedMca2);
            controller.ConnectMcaEventHandlersToMca(disconnectedMca1);
            controller.ConnectMcaEventHandlersToMca(disconnectedMca2);

            return controller;
        }

        private static IDisposable CreateControllerWithTwoConnectedMcasAndOneDisconnectedMca(
            IMultiChannelAnalyzer connectedMca1, IMultiChannelAnalyzer connectedMca2,
            IMultiChannelAnalyzer disconnectedMca)
        {
            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(connectedMca1);

            var controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(), stubMcaBuilder, Substitute.For<IWindowsEventLogger>());

            controller.AddConnectedMca("conn2", connectedMca2);
            controller.AddDisconnectedMca("disconn1", disconnectedMca);

            return controller;
        }
    }
    // ReSharper restore InconsistentNaming
}
