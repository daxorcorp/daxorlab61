using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_a_USB_device_is_connected
    {
        private const string KnownMcaManufacturer = "daxor";

        private IMultiChannelAnalyzer _mockConnectedMca;
        private IMultiChannelAnalyzer _mockDisconnectedMca;
        private IUsbDeviceWatcher _stubUsbDeviceWatcher;
        private IMultiChannelAnalyzerBuilder _fakeMcaBuilder;
        private McaControllerWithExposedMethods _controller;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockConnectedMca = Substitute.For<IMultiChannelAnalyzer>();
            _mockDisconnectedMca = Substitute.For<IMultiChannelAnalyzer>();

            _fakeMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            _fakeMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(_mockConnectedMca);
            _fakeMcaBuilder.IsRecognizedManufacturer(KnownMcaManufacturer).Returns(true);

            _mockLogger = Substitute.For<IWindowsEventLogger>();

            _stubUsbDeviceWatcher = Substitute.For<IUsbDeviceWatcher>();

            _controller = new McaControllerWithExposedMethods(_stubUsbDeviceWatcher, _fakeMcaBuilder, _mockLogger);

            _controller.AddConnectedMca("conn1", _mockConnectedMca);
            _controller.AddDisconnectedMca("disconn1", _mockDisconnectedMca);

            _mockConnectedMca.ClearReceivedCalls();
            _mockDisconnectedMca.ClearReceivedCalls();
        }

        [TestMethod]
        public void And_the_manufacturer_is_not_recognized_then_the_status_of_disconnected_MCAs_are_not_checked()
        {
            _stubUsbDeviceWatcher.Connected += Raise.Event<Action<string>>("unknown");

            _fakeMcaBuilder.Received().IsRecognizedManufacturer("unknown");

            // ReSharper disable UnusedVariable
            var x = _mockConnectedMca.DidNotReceive().IsConnected;
            var y = _mockDisconnectedMca.DidNotReceive().IsConnected;
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void Then_only_disconnected_devices_have_their_connection_status_checked()
        {
            _stubUsbDeviceWatcher.Connected += Raise.Event<Action<string>>(KnownMcaManufacturer);

            // ReSharper disable UnusedVariable
            var x = _mockConnectedMca.DidNotReceive().IsConnected;     
            var y = _mockDisconnectedMca.Received(1).IsConnected;
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_a_recognized_device_is_still_disconnected_then_it_remains_disconnected()
        {
            var wasConnectedEventRaised = false;
            _controller.MultiChannelAnalyzerConnected += s => wasConnectedEventRaised = true;
            
            Assert.AreEqual(1, _controller.NumConnectedMcas());
            Assert.AreEqual(1, _controller.NumDisconnectedMcas());

            _stubUsbDeviceWatcher.Connected += Raise.Event<Action<string>>(KnownMcaManufacturer);

            Assert.AreEqual(1, _controller.NumConnectedMcas());
            Assert.AreEqual(1, _controller.NumDisconnectedMcas());

            Assert.IsFalse(wasConnectedEventRaised);
        }

        [TestMethod]
        public void And_a_recognized_device_is_now_connected_then_it_is_considered_connected_to_the_controller()
        {
            Assert.AreEqual(1, _controller.NumConnectedMcas());
            Assert.AreEqual(1, _controller.NumDisconnectedMcas());
            _mockDisconnectedMca.IsConnected.Returns(true);

            _stubUsbDeviceWatcher.Connected += Raise.Event<Action<string>>(KnownMcaManufacturer);

            Assert.AreEqual(2, _controller.NumConnectedMcas());
            Assert.AreEqual(0, _controller.NumDisconnectedMcas());
        }

        [TestMethod]
        public void And_a_recognized_device_is_now_connected_then_the_Connected_event_is_raised_on_the_controller()
        {
            _mockDisconnectedMca.IsConnected.Returns(true);
            var wasConnectedEventRaised = false;
            _controller.MultiChannelAnalyzerConnected += s => wasConnectedEventRaised = true;

            _stubUsbDeviceWatcher.Connected += Raise.Event<Action<string>>(KnownMcaManufacturer);

            Assert.IsTrue(wasConnectedEventRaised);
        }

        [TestMethod]
        public void And_a_recognized_device_is_now_connected_then_the_action_is_logged()
        {
            _mockDisconnectedMca.IsConnected.Returns(true);

            _stubUsbDeviceWatcher.Connected += Raise.Event<Action<string>>(KnownMcaManufacturer);

            _mockLogger.Received().Log("MCA 'disconn1' has been reconnected");
        }
    }
    // ReSharper restore InconsistentNaming
}
