using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        [TestMethod]
        public void Then_the_USB_watcher_is_started()
        {
            var mockUsbWatcher = Substitute.For<IUsbDeviceWatcher>();
            
            // ReSharper disable once UnusedVariable
            var controller = new McaControllerWithExposedMethods(mockUsbWatcher, 
                Substitute.For<IMultiChannelAnalyzerBuilder>(), Substitute.For<IWindowsEventLogger>());

            mockUsbWatcher.Received().Start(1);
        }

        [TestMethod]
        public void Then_a_log_entry_is_made_about_the_USB_watcher()
        {
            var mockLogger = Substitute.For<IWindowsEventLogger>();

            // ReSharper disable once UnusedVariable
            var controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(),
                Substitute.For<IMultiChannelAnalyzerBuilder>(), mockLogger);

            mockLogger.Received().Log("USB device watcher started");
        }

        [TestMethod]
        public void Then_the_MCA_builder_is_used_to_get_a_scintiSPEC_MCA()
        {
            var mockMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();

            // ReSharper disable once UnusedVariable
            var controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(), 
                mockMcaBuilder, Substitute.For<IWindowsEventLogger>());

            mockMcaBuilder.Received().BuildScintiSpecMultiChannelAnalyzer();
        }

        [TestMethod]
        public void And_the_scintiSPEC_MCA_is_initially_connected_then_its_identifier_is_correct()
        {
            var connectedMca = Substitute.For<IMultiChannelAnalyzer>();
            connectedMca.IsConnected.Returns(true);
            connectedMca.SerialNumber.Returns(13579);

            var mockMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            mockMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(connectedMca);

            var controller = new MultiChannelAnalyzerController(Substitute.For<IUsbDeviceWatcher>(),
                mockMcaBuilder, Substitute.For<IWindowsEventLogger>());

            var observedMca = controller.GetMultiChannelAnalyzerWithId("scintiSPEC13579");

            Assert.AreEqual(connectedMca, observedMca);
        }

        [TestMethod]
        public void Then_the_scintiSPEC_MCA_connection_state_is_logged()
        {
            var connectedMca = Substitute.For<IMultiChannelAnalyzer>();
            connectedMca.IsConnected.Returns(true);
            connectedMca.SerialNumber.Returns(13579);

            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(connectedMca);

            var mockLogger = Substitute.For<IWindowsEventLogger>();

            // ReSharper disable once UnusedVariable
            var controller = new MultiChannelAnalyzerController(Substitute.For<IUsbDeviceWatcher>(),
                stubMcaBuilder, mockLogger);

            mockLogger.Received().Log("scintiSPEC MCA (serial number '13579') is connected: True");
        }
    }
    // ReSharper restore InconsistentNaming
}
