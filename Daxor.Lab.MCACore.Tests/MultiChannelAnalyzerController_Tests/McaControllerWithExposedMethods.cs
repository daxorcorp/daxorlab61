﻿using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests
{
    internal class McaControllerWithExposedMethods : MultiChannelAnalyzerController
    {
        internal McaControllerWithExposedMethods(IUsbDeviceWatcher usbDeviceWatcher,
            IMultiChannelAnalyzerBuilder mcaBuilder, IWindowsEventLogger windowsEventLogger, bool useBaseConnectedMcaCheck = false)
            : base(usbDeviceWatcher, mcaBuilder, windowsEventLogger, useBaseConnectedMcaCheck)
        {
        }

        internal void AddConnectedMca(string id, IMultiChannelAnalyzer mca)
        {
            try
            {
                RegisterMultiChannelAnalyzerAndId(mca, id);
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch { }
            
            AddMcaToConnectedCollection(id, mca);
        }

        internal IMultiChannelAnalyzer RemoveConnectedMca(string id)
        {
            return RemoveMcaFromConnectedCollection(id);
        }
        
        internal void AddDisconnectedMca(string id, IMultiChannelAnalyzer mca)
        {
            try
            {
                RegisterMultiChannelAnalyzerAndId(mca, id);
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch { }

            AddMcaToDisconnectedCollection(id, mca);
        }

        internal IMultiChannelAnalyzer RemoveDisconnectedMca(string id)
        {
            return RemoveMcaFromDisconnectedCollection(id);
        }

        internal int NumConnectedMcas() { return ConnectedMcaCount(); }

        internal int NumDisconnectedMcas() { return DisconnectedMcaCount(); }

        internal void ConnectMcaEventHandlersToMca(IMultiChannelAnalyzer mca)
        {
            mca.AcquisitionStarted += OnMcaAcquisitionStarted;
            mca.AcquisitionCompleted += OnMcaAcquisitionCompleted;
            mca.AcquisitionStopped += OnMcaAcquisitionStopped;
        }
    }
}
