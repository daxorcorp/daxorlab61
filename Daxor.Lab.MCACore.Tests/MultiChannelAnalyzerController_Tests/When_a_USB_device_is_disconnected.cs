using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_a_USB_device_is_disconnected
    {
        private IMultiChannelAnalyzer _mockConnectedMca;
        private IMultiChannelAnalyzer _mockDisconnectedMca;
        private IUsbDeviceWatcher _stubUsbDeviceWatcher;
        private McaControllerWithExposedMethods _controller;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockConnectedMca = Substitute.For<IMultiChannelAnalyzer>();
            _mockDisconnectedMca = Substitute.For<IMultiChannelAnalyzer>();

            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(_mockConnectedMca);

            _mockLogger = Substitute.For<IWindowsEventLogger>();

            _stubUsbDeviceWatcher = Substitute.For<IUsbDeviceWatcher>();

            _controller = new McaControllerWithExposedMethods(_stubUsbDeviceWatcher, stubMcaBuilder, _mockLogger);

            _controller.AddConnectedMca("conn1", _mockConnectedMca);
            _controller.AddDisconnectedMca("disconn1", _mockDisconnectedMca);

            _mockConnectedMca.ClearReceivedCalls();
            _mockDisconnectedMca.ClearReceivedCalls();
        }

        [TestMethod]
        public void Then_only_connected_devices_have_their_connection_status_checked()
        {
            _stubUsbDeviceWatcher.Disconnected += Raise.Event<Action>();

            // ReSharper disable UnusedVariable
            var x = _mockConnectedMca.Received(1).IsConnected;
            var y = _mockDisconnectedMca.DidNotReceive().IsConnected;
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_a_recognized_device_is_still_connected_then_it_remains_connected()
        {
            var wasDisconnectedEventRaised = false;
            _controller.MultiChannelAnalyzerDisconnected += s => wasDisconnectedEventRaised = true;

            Assert.AreEqual(1, _controller.NumConnectedMcas());
            Assert.AreEqual(1, _controller.NumDisconnectedMcas());

            _mockConnectedMca.IsConnected.Returns(true);

            _stubUsbDeviceWatcher.Disconnected += Raise.Event<Action>();

            Assert.AreEqual(1, _controller.NumConnectedMcas());
            Assert.AreEqual(1, _controller.NumDisconnectedMcas());

            Assert.IsFalse(wasDisconnectedEventRaised);
        }

        [TestMethod]
        public void And_a_recognized_device_is_now_disconnected_then_is_considered_disconnected_to_the_controller()
        {
            Assert.AreEqual(1, _controller.NumConnectedMcas());
            Assert.AreEqual(1, _controller.NumDisconnectedMcas());
            _mockDisconnectedMca.IsConnected.Returns(false);

            _stubUsbDeviceWatcher.Disconnected += Raise.Event<Action>();

            Assert.AreEqual(0, _controller.NumConnectedMcas());
            Assert.AreEqual(2, _controller.NumDisconnectedMcas());
        }

        [TestMethod]
        public void And_a_recognized_device_is_now_disconnected_then_the_Disconnected_event_is_raised_on_the_controller()
        {
            _mockDisconnectedMca.IsConnected.Returns(false);
            var wasDisconnectedEventRaised = false;
            _controller.MultiChannelAnalyzerDisconnected += s => wasDisconnectedEventRaised = true;

            _stubUsbDeviceWatcher.Disconnected += Raise.Event<Action>();

            Assert.IsTrue(wasDisconnectedEventRaised);
        }

        [TestMethod]
        public void And_a_recognized_device_is_now_disconnected_then_the_action_is_logged()
        {
            _mockDisconnectedMca.IsConnected.Returns(false);

            _stubUsbDeviceWatcher.Disconnected += Raise.Event<Action>();

            _mockLogger.Received().Log("MCA 'conn1' has been disconnected");
        }
    }
    // ReSharper restore InconsistentNaming
}
