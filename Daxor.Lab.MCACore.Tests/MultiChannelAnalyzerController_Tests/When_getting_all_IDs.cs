using System;
using System.Linq;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_all_IDs
    {
        private IMultiChannelAnalyzer _stubMca;
        private McaControllerWithExposedMethods _controller;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _stubMca = Substitute.For<IMultiChannelAnalyzer>();

            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(_stubMca);

            _mockLogger = Substitute.For<IWindowsEventLogger>();

            _controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(), stubMcaBuilder, _mockLogger);
        }

        [TestMethod]
        public void And_there_are_some_connected_MCAs_and_disconnected_MCAs_then_all_IDs_are_returned()
        {
            _controller.AddConnectedMca("conn1", _stubMca);
            _controller.AddConnectedMca("conn2", _stubMca);
            _controller.AddDisconnectedMca("disconn1", _stubMca);
            _controller.AddDisconnectedMca("disconn2", _stubMca);

            var observedIds = _controller.GetAllMultiChannelAnalyzerIds();

            CollectionAssert.AreEquivalent(new [] { "conn1", "conn2", "disconn1", "disconn2"}, observedIds.ToArray());
        }

        [TestMethod]
        public void And_there_are_no_MCAs_then_an_empty_list_is_returned()
        {
            var observedIds = _controller.GetAllMultiChannelAnalyzerIds();

            Assert.AreEqual(0, observedIds.Count);
        }

        [TestMethod]
        public void Then_the_action_is_logged()
        {
            // ReSharper disable once UnusedVariable
            var observedIds = _controller.GetAllMultiChannelAnalyzerIds();

            _mockLogger.Received().Log("List of all MCA IDs was requested");
        }
    }
    // ReSharper restore InconsistentNaming
}
