using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_resetting_an_MCA_to_its_default_configuration
    {
        private IMultiChannelAnalyzer _mockConnectedMca;
        private IMultiChannelAnalyzer _mockDisconnectedMca;
        private McaControllerWithExposedMethods _controller;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockConnectedMca = Substitute.For<IMultiChannelAnalyzer>();
            _mockDisconnectedMca = Substitute.For<IMultiChannelAnalyzer>();

            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(_mockConnectedMca);

            _mockLogger = Substitute.For<IWindowsEventLogger>();

            _controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(), stubMcaBuilder, _mockLogger);

            _controller.AddConnectedMca("conn1", _mockConnectedMca);
            _controller.AddDisconnectedMca("disconn1", _mockDisconnectedMca);
        }

        [TestMethod]
        public void And_the_MCA_is_connected_then_its_configuration_is_reset()
        {
            _controller.ResetMultiChannelAnalyzerToDefaultConfiguration("conn1");

            _mockConnectedMca.Received().ResetToDefaultConfiguration();
            _mockDisconnectedMca.DidNotReceive().ResetToDefaultConfiguration();
        }

        [TestMethod]
        public void And_the_MCA_is_disconnected_then_its_configuration_is_reset()
        {
            _controller.ResetMultiChannelAnalyzerToDefaultConfiguration("disconn1");

            _mockDisconnectedMca.Received().ResetToDefaultConfiguration();
            _mockConnectedMca.DidNotReceive().ResetToDefaultConfiguration();
        }

        [TestMethod]
        public void Then_the_action_is_logged()
        {
            _controller.ResetMultiChannelAnalyzerToDefaultConfiguration("conn1");

            _mockLogger.Received().Log("Resetting MCA 'conn1' to its default configuration");
        }
    }
    // ReSharper restore InconsistentNaming
}
