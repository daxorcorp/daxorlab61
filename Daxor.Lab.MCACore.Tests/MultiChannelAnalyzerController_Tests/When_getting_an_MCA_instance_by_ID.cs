using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_an_MCA_instance_by_ID
    {
        internal const string DefaultScintiSpecId = "scintiSPEC0";

        private IMultiChannelAnalyzer _stubMca;
        private McaControllerWithExposedMethods _controller;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _stubMca = Substitute.For<IMultiChannelAnalyzer>();

            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(_stubMca);

            _mockLogger = Substitute.For<IWindowsEventLogger>();

            _controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(), stubMcaBuilder, _mockLogger);
        }

        [TestMethod]
        public void And_the_MCA_is_disconnected_then_the_instance_is_returned()
        {
            _stubMca.IsConnected.Returns(false);
            _controller.AddDisconnectedMca(DefaultScintiSpecId, _stubMca);

            var observedMca = _controller.GetMultiChannelAnalyzerWithId(DefaultScintiSpecId);

            Assert.AreEqual(_stubMca, observedMca);
        }
        
        [TestMethod]
        public void And_the_MCA_is_connected_then_the_instance_is_returned()
        {
            _stubMca.IsConnected.Returns(true);
            _controller.AddConnectedMca(DefaultScintiSpecId, _stubMca);
            
            var observedMca = _controller.GetMultiChannelAnalyzerWithId(DefaultScintiSpecId);

            Assert.AreEqual(_stubMca, observedMca);
        }

        [TestMethod]
        public void Then_the_action_is_logged()
        {
            _controller.AddConnectedMca(DefaultScintiSpecId, _stubMca); // Just to keep exception from happening

            // ReSharper disable once UnusedVariable
            var ignored = _controller.GetMultiChannelAnalyzerWithId(DefaultScintiSpecId);

            _mockLogger.Received().Log("MCA '" + DefaultScintiSpecId + "' was requested");
        }
    }
    // ReSharper restore InconsistentNaming
}
