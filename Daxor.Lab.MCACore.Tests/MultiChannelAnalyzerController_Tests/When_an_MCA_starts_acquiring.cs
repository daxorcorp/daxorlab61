using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_an_MCA_starts_acquiring
    {
        private const string ExpectedMcaId = "scintiSPEC12345";

        private IMultiChannelAnalyzer _stubConnectedMca;
        private McaControllerWithExposedMethods _controller;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _stubConnectedMca = Substitute.For<IMultiChannelAnalyzer>();
            _stubConnectedMca.IsConnected.Returns(true);
            _stubConnectedMca.SerialNumber.Returns(12345);

            var stubMcaBuilder = Substitute.For<IMultiChannelAnalyzerBuilder>();
            stubMcaBuilder.BuildScintiSpecMultiChannelAnalyzer().Returns(_stubConnectedMca);

            _mockLogger = Substitute.For<IWindowsEventLogger>();

            _controller = new McaControllerWithExposedMethods(Substitute.For<IUsbDeviceWatcher>(), stubMcaBuilder, _mockLogger);
        }

        [TestMethod]
        public void Then_the_controller_raises_the_AcquisitionStarted_event_with_the_correct_parameters()
        {
            var observedMcaId = string.Empty;
            var observedStartTime = DateTime.MinValue;
            _controller.AcquisitionStarted += (s, time) =>
            {
                observedMcaId = s;
                observedStartTime = time;
            };
            var expectedStartTime = DateTime.Now;
            
            _stubConnectedMca.AcquisitionStarted += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(_stubConnectedMca, expectedStartTime);

            Assert.AreEqual(ExpectedMcaId, observedMcaId);
            Assert.AreEqual(expectedStartTime, observedStartTime);
        }

        [TestMethod]
        public void Then_the_action_is_logged()
        {
            _stubConnectedMca.AcquisitionStarted += Raise.Event<Action<IMultiChannelAnalyzer, DateTime>>(_stubConnectedMca, DateTime.Now);

            _mockLogger.Received().Log("MCA '" + ExpectedMcaId + "' started acquisition");
        }
    }
    // ReSharper restore InconsistentNaming
}
