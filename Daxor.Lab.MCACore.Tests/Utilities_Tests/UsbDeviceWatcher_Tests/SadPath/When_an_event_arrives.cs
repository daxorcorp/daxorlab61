using System;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.Utilities_Tests.UsbDeviceWatcher_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_an_event_arrives
    {
        private UsbDeviceWatcherWithSpecialHandler _deviceWatcher;
        private IManagementEventWatcher _stubManagementEventWatcher;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _stubManagementEventWatcher = Substitute.For<IManagementEventWatcher>();

            var stubEventWatcherFactory = Substitute.For<IManagementEventWatcherFactory>();
            stubEventWatcherFactory.CreateWatcherWithQuery(null).ReturnsForAnyArgs(_stubManagementEventWatcher);

            _deviceWatcher = new UsbDeviceWatcherWithSpecialHandler(stubEventWatcherFactory);

            _deviceWatcher.Start(450);
        }

        [TestMethod]
        public void And_obtaining_the_manufacturer_results_in_a_generic_exception_then_the_exception_is_not_caught()
        {
            _deviceWatcher.DeviceIdToReturn = "USB";
            _deviceWatcher.ManufacturerToReturn = "DAXOR";
            _deviceWatcher.GetManufacturerShouldThrowGenericException = true;

            AssertEx.Throws<Exception>(() => _stubManagementEventWatcher.EventArrived += Raise.EventWith(new object(), new EventArgs()));
        }
    }
    // ReSharper restore InconsistentNaming
}
