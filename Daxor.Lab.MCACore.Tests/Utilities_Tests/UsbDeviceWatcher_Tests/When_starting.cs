using System.Management;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.Utilities_Tests.UsbDeviceWatcher_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_starting
    {
        private IManagementEventWatcher _mockEventWatcher;
        private IManagementEventWatcherFactory _mockFactory;
        private UsbDeviceWatcher _usbDeviceWatcher;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockEventWatcher = Substitute.For<IManagementEventWatcher>();
            _mockFactory = Substitute.For<IManagementEventWatcherFactory>();
            _mockFactory.CreateWatcherWithQuery(null).ReturnsForAnyArgs(_mockEventWatcher);

            _usbDeviceWatcher = new UsbDeviceWatcher(_mockFactory);
            _usbDeviceWatcher.Start(50);
        }

        [TestMethod]
        public void Then_the_management_event_watcher_factory_is_used_to_get_a_watcher()
        {
            _mockFactory.Received().CreateWatcherWithQuery(Arg.Any<EventQuery>());
        }

        [TestMethod]
        public void Then_the_watcher_query_is_correct()
        {
            _mockFactory.Received().CreateWatcherWithQuery(Arg.Is<EventQuery>(q => q.QueryString == "SELECT * FROM __InstanceOperationEvent WITHIN 50 WHERE TargetInstance ISA 'Win32_USBControllerDevice'"));
        }

        [TestMethod]
        public void Then_the_watcher_is_started()
        {
            _mockEventWatcher.Received().Start();
        }
    }
    // ReSharper restore InconsistentNaming
}
