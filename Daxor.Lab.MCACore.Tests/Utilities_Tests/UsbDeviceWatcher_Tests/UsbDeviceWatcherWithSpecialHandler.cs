﻿using System;
using System.Management;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.Utilities;

namespace Daxor.Lab.MCACore.Tests.Utilities_Tests.UsbDeviceWatcher_Tests
{
    internal class UsbDeviceWatcherWithSpecialHandler : UsbDeviceWatcher
    {
        internal UsbDeviceWatcherWithSpecialHandler(IManagementEventWatcherFactory managementEventWatcherFactory) : base(managementEventWatcherFactory)
        {
            ManufacturerToReturn = "Default Manufacturer";
        }

        internal string DeviceIdToReturn { get; set; }

        internal string ManufacturerToReturn { get; set; }

        internal bool GetManufacturerShouldThrowGenericException { get; set; }

        internal bool GetManufacturerShouldThrowManagementException { get; set; }

        protected override ManagementObject GetDependentManagementObject(EventArgs eventArgs)
        {
            return new ManagementObject();
        }

        protected override string GetManufacturer(ManagementObject managementObject)
        {
            if (GetManufacturerShouldThrowGenericException) throw new Exception();
            if (GetManufacturerShouldThrowManagementException) throw new ManagementException();

            return ManufacturerToReturn;
        }

        protected override string GetDeviceId(ManagementObject managementObject)
        {
            return DeviceIdToReturn;
        }
    }
}
