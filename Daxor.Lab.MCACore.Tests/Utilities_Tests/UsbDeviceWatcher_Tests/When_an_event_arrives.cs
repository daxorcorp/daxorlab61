using System;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.Utilities_Tests.UsbDeviceWatcher_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_an_event_arrives
    {
        private UsbDeviceWatcherWithSpecialHandler _deviceWatcher;
        private IManagementEventWatcher _stubManagementEventWatcher;
        private int _numConnectedEventsFired;
        private int _numDisconnectedEventsFired;
        private string _manufacturer;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _numConnectedEventsFired = 0;
            _numDisconnectedEventsFired = 0;

            _stubManagementEventWatcher = Substitute.For<IManagementEventWatcher>();
            
            var stubEventWatcherFactory = Substitute.For<IManagementEventWatcherFactory>();
            stubEventWatcherFactory.CreateWatcherWithQuery(null).ReturnsForAnyArgs(_stubManagementEventWatcher);
            
            _deviceWatcher = new UsbDeviceWatcherWithSpecialHandler(stubEventWatcherFactory);
            _deviceWatcher.Connected += manufacturer => { _numConnectedEventsFired++; _manufacturer = manufacturer; };
            _deviceWatcher.Disconnected += () => _numDisconnectedEventsFired++;

            _deviceWatcher.Start(450);
        }

        [TestMethod]
        public void And_the_device_ID_does_not_indicate_a_USB_device_then_no_event_is_raised()
        {
            _deviceWatcher.DeviceIdToReturn = "whatever";
            
            _stubManagementEventWatcher.EventArrived += Raise.EventWith(new object(), new EventArgs());

            Assert.AreEqual(0, _numConnectedEventsFired);
            Assert.AreEqual(0, _numDisconnectedEventsFired);
        }

        [TestMethod]
        public void And_the_device_ID_indicates_a_USB_device_then_the_connected_event_is_raised()
        {
            _deviceWatcher.DeviceIdToReturn = "USB 4560";
            _stubManagementEventWatcher.EventArrived += Raise.EventWith(new object(), new EventArgs());

            _deviceWatcher.DeviceIdToReturn = "4560USB4952";
            _stubManagementEventWatcher.EventArrived += Raise.EventWith(new object(), new EventArgs());

            Assert.AreEqual(2, _numConnectedEventsFired);
            Assert.AreEqual(0, _numDisconnectedEventsFired);
        }

        [TestMethod]
        public void And_the_device_ID_indicates_a_USB_device_then_the_manufacturer_is_part_of_the_event_payload()
        {
            _deviceWatcher.DeviceIdToReturn = "USB";
            _deviceWatcher.ManufacturerToReturn = "DAXOR";

            _stubManagementEventWatcher.EventArrived += Raise.EventWith(new object(), new EventArgs());

            Assert.AreEqual(1, _numConnectedEventsFired);
            Assert.AreEqual("DAXOR", _manufacturer);
        }

        [TestMethod]
        public void And_getting_the_manufacturer_yields_a_ManagementException_then_the_disconnected_event_is_raised()
        {
            _deviceWatcher.DeviceIdToReturn = "USB";
            _deviceWatcher.GetManufacturerShouldThrowManagementException = true;

            _stubManagementEventWatcher.EventArrived += Raise.EventWith(new object(), new EventArgs());

            Assert.AreEqual(0, _numConnectedEventsFired);
            Assert.AreEqual(1, _numDisconnectedEventsFired);
        }
    }
    // ReSharper restore InconsistentNaming
}
