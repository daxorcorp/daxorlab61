using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.Utilities_Tests.UsbDeviceWatcher_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_stopping
    {
        [TestMethod]
        public void And_the_watcher_had_been_started_before_then_the_watcher_is_stopped()
        {
            var mockEventWatcher = Substitute.For<IManagementEventWatcher>();
            var stubFactory = Substitute.For<IManagementEventWatcherFactory>();
            stubFactory.CreateWatcherWithQuery(null).ReturnsForAnyArgs(mockEventWatcher);

            var usbDeviceWatcher = new UsbDeviceWatcher(stubFactory);
            usbDeviceWatcher.Start(50);
            usbDeviceWatcher.Stop();

            mockEventWatcher.Received().Stop();
        }

        [TestMethod]
        public void And_the_watcher_had_not_been_started_before_then_nothing_happens()
        {
            var mockEventWatcher = Substitute.For<IManagementEventWatcher>();
            var stubFactory = Substitute.For<IManagementEventWatcherFactory>();
            stubFactory.CreateWatcherWithQuery(null).ReturnsForAnyArgs(mockEventWatcher);

            var usbDeviceWatcher = new UsbDeviceWatcher(stubFactory);
            usbDeviceWatcher.Stop();

            mockEventWatcher.DidNotReceive().Stop();
        }

    }
    // ReSharper restore InconsistentNaming
}
