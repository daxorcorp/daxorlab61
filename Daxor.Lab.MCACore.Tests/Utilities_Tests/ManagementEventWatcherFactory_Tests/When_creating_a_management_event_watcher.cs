﻿using System.Management;
using Daxor.Lab.MCACore.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MCACore.Tests.Utilities_Tests.ManagementEventWatcherFactory_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_a_management_event_watcher
    {
        [TestMethod]
        public void Then_a_wrapper_instance_is_returned()
        {
            var factory = new ManagementEventWatcherFactory();
            var watcherInstance = factory.CreateWatcherWithQuery(new EventQuery("SELECT * FROM __InstanceOperationEvent WITHIN 1 WHERE TargetInstance ISA 'Win32_USBControllerDevice'"));

            Assert.IsInstanceOfType(watcherInstance, typeof(ManagementEventWatcherWrapper));
        }
    }
    // ReSharper restore InconsistentNaming

}
