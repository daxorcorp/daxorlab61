﻿using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerBuilder_Tests
{
    internal class McaBuilderWithOverrides : MultiChannelAnalyzerBuilder
    {
        private readonly IHardwareConfigurationManager _hardwareConfigurationManager;
        private readonly IScintiSpecDriver _scintiSpecDriver;

        internal McaBuilderWithOverrides(IWindowsEventLogger mcaControllerLogger, IWindowsEventLogger hardwareConfigurationManagerLogger,
            IHardwareConfigurationManager hardwareConfigurationManager, IScintiSpecDriver scintiSpecDriver) : base(mcaControllerLogger, hardwareConfigurationManagerLogger)
        {
            _hardwareConfigurationManager = hardwareConfigurationManager;
            _scintiSpecDriver = scintiSpecDriver;
        }

        internal IHardwareConfigurationRepository HardwareConfigurationRepository { get; set; }

        protected override IHardwareConfigurationManager CreateHardwareConfigurationManager(IHardwareConfigurationRepository hardwareConfigurationRepository)
        {
            HardwareConfigurationRepository = hardwareConfigurationRepository;
            return _hardwareConfigurationManager;
        }

        protected override IScintiSpecDriver CreateScintiSpecDriver()
        {
            return _scintiSpecDriver;
        }
    }
}
