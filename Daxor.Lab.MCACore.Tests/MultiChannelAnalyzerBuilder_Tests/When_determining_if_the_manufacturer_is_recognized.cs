using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerBuilder_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_the_manufacturer_is_recognized
    {
        private IMultiChannelAnalyzerBuilder _builder;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _builder = new MultiChannelAnalyzerBuilder(Substitute.For<IWindowsEventLogger>(), Substitute.For<IWindowsEventLogger>());
        }

        [TestMethod]
        public void And_the_manufacturer_is_Target_then_it_is_recognized()
        {
            Assert.IsTrue(_builder.IsRecognizedManufacturer("target"));
        }

        [TestMethod]
        public void And_the_manufacturer_is_Target_in_mixed_case_then_it_is_recognized()
        {
            Assert.IsTrue(_builder.IsRecognizedManufacturer("tArGEt"));
        }

        [TestMethod]
        public void And_the_manufacturer_is_Target_at_the_beginning_of_the_string_then_it_is_recognized()
        {
            Assert.IsTrue(_builder.IsRecognizedManufacturer("target manufacturing gmbh"));
        }

        [TestMethod]
        public void And_the_manufacturer_is_not_Target_then_it_is_not_recognized()
        {
            Assert.IsFalse(_builder.IsRecognizedManufacturer("logitech"));
        }
    }
    // ReSharper restore InconsistentNaming
}
