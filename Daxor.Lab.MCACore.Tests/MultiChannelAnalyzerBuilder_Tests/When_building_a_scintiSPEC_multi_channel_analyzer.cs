using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Daxor.Lab.MCACore.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerBuilder_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_building_a_scintiSPEC_multi_channel_analyzer
    {
        private IHardwareConfigurationManager _fakeHardwareConfigurationManager;
        private IScintiSpecDriver _fakeScintiSpecDriver;
        private IWindowsEventLogger _fakeMcaServiceLogger;
        private IWindowsEventLogger _fakeHardwareConfigurationManagerLogger;
        private McaBuilderWithOverrides _builder;
        private HardwareSetting _serialNumberSetting;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _fakeHardwareConfigurationManager = Substitute.For<IHardwareConfigurationManager>();
            _fakeScintiSpecDriver = Substitute.For<IScintiSpecDriver>();
            _fakeMcaServiceLogger = Substitute.For<IWindowsEventLogger>();
            _fakeHardwareConfigurationManagerLogger = Substitute.For<IWindowsEventLogger>();

            _serialNumberSetting = new HardwareSetting(ScintiSpecSpecification.SerialNumberKey, HardwareSetting.IntegerType);
            _serialNumberSetting.SetValue(12345);
            _fakeHardwareConfigurationManager.GetSetting(ScintiSpecSpecification.SerialNumberKey).Returns(_serialNumberSetting);

            _builder = new McaBuilderWithOverrides(_fakeMcaServiceLogger, _fakeHardwareConfigurationManagerLogger, _fakeHardwareConfigurationManager, _fakeScintiSpecDriver);
        }

        [TestMethod]
        public void Then_the_correct_driver_is_used()
        {
            var mca = _builder.BuildScintiSpecMultiChannelAnalyzer() as ScintiSpecMultiChannelAnalyzer;
            if (mca == null) Assert.Fail();

            Assert.AreEqual(_fakeScintiSpecDriver, mca.Driver);
        }

        [TestMethod]
        public void Then_the_correct_specification_is_used()
        {
            var mca = _builder.BuildScintiSpecMultiChannelAnalyzer() as ScintiSpecMultiChannelAnalyzer;
            if (mca == null) Assert.Fail();

            Assert.IsInstanceOfType(mca.Specification, typeof(ScintiSpecSpecification));
        }

        [TestMethod]
        public void Then_the_specification_uses_the_correct_port_number()
        {
            var mca = _builder.BuildScintiSpecMultiChannelAnalyzer() as ScintiSpecMultiChannelAnalyzer;
            if (mca == null) Assert.Fail();

            Assert.AreEqual(MultiChannelAnalyzerBuilder.DefaultScintiSpecPortNumber, mca.Specification.PortNumber);
        }

        [TestMethod]
        public void Then_the_correct_threading_service_is_used()
        {
            var mca = _builder.BuildScintiSpecMultiChannelAnalyzer() as ScintiSpecMultiChannelAnalyzer;
            if (mca == null) Assert.Fail();

            Assert.IsInstanceOfType(mca.ThreadWrapper, typeof(ThreadWrapper));
        }

        [TestMethod]
        public void Then_the_correct_logger_is_used()
        {
            var mca = _builder.BuildScintiSpecMultiChannelAnalyzer() as ScintiSpecMultiChannelAnalyzer;
            if (mca == null) Assert.Fail();

            Assert.AreEqual(_fakeMcaServiceLogger, mca.Logger);            
        }

        [TestMethod]
        public void Then_the_correct_hardware_configuration_manager_is_used()
        {
            var mca = _builder.BuildScintiSpecMultiChannelAnalyzer() as ScintiSpecMultiChannelAnalyzer;
            if (mca == null) Assert.Fail();
            
            Assert.AreEqual(_fakeHardwareConfigurationManager, mca.HardwareConfigurationManager);
        }

        [TestMethod]
        public void Then_the_correct_hardware_configuration_file_name_is_used()
        {
            _fakeScintiSpecDriver.GetSerialNumberOnPort(MultiChannelAnalyzerBuilder.DefaultScintiSpecPortNumber).Returns(12345);
            var mca = _builder.BuildScintiSpecMultiChannelAnalyzer() as ScintiSpecMultiChannelAnalyzer;
            if (mca == null) Assert.Fail();

            const string expectedFileName = MultiChannelAnalyzerBuilder.ConfigurationFileFolder + "scintiSPEC12345.xml";
            Assert.AreEqual(expectedFileName, _builder.HardwareConfigurationRepository.Filename);
        }

        [TestMethod]
        public void Then_the_correct_hardware_configuration_default_file_name_is_used()
        {
            var mca = _builder.BuildScintiSpecMultiChannelAnalyzer() as ScintiSpecMultiChannelAnalyzer;
            if (mca == null) Assert.Fail();

            Assert.AreEqual(MultiChannelAnalyzerBuilder.DefaultIcxMcaConfigurationFilePath, _builder.HardwareConfigurationRepository.DefaultFilename);
        }

        [TestMethod]
        public void Then_the_serial_number_setting_in_the_configuration_is_updated()
        {
            _fakeScintiSpecDriver.GetSerialNumberOnPort(1).Returns(56789);

            var mca = _builder.BuildScintiSpecMultiChannelAnalyzer() as ScintiSpecMultiChannelAnalyzer;
            if (mca == null) Assert.Fail();

            Assert.AreEqual(56789, _serialNumberSetting.GetValue<int>());
        }
    }
    // ReSharper restore InconsistentNaming
}
