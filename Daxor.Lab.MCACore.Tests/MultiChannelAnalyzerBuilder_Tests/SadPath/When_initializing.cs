using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerBuilder_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        [TestMethod]
        public void And_the_MCA_controller_logger_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentNullException>(() => new MultiChannelAnalyzerBuilder(null, Substitute.For<IWindowsEventLogger>()));
        }

        [TestMethod]
        public void And_the_hardware_configuration_manager_logger_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentNullException>(() => new MultiChannelAnalyzerBuilder(Substitute.For<IWindowsEventLogger>(), null));
        }
    }
    // ReSharper restore InconsistentNaming
}
