using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.MultiChannelAnalyzerBuilder_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_the_manufacturer_is_recognized
    {
        [TestMethod]
        public void And_the_manufacturer_is_null_then_an_exception_is_thrown()
        {
            var builder = new MultiChannelAnalyzerBuilder(Substitute.For<IWindowsEventLogger>(), Substitute.For<IWindowsEventLogger>());
            AssertEx.Throws<ArgumentNullException>(() => builder.IsRecognizedManufacturer(null));
        }
    }
    // ReSharper restore InconsistentNaming
}
