using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecSpecification_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        [TestMethod]
        public void Then_the_port_number_is_set()
        {
            const int expectedPortNumber = 56;

            var specification = new ScintiSpecSpecification(expectedPortNumber);

            Assert.AreEqual(expectedPortNumber, specification.PortNumber);
        }
    }
    // ReSharper restore InconsistentNaming
}
