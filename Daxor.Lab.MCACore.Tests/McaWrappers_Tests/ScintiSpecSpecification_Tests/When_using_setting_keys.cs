using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecSpecification_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_setting_keys
    {
        [TestMethod]
        public void Then_the_noise_level_key_matches_requirements()
        {
            Assert.AreEqual("noiselevel", ScintiSpecSpecification.NoiseLevelSettingKey);
        }

        [TestMethod]
        public void Then_the_lower_level_discriminator_key_matches_requirements()
        {
            Assert.AreEqual("lld", ScintiSpecSpecification.LowerLevelDiscriminatorSettingKey);
        }

        [TestMethod]
        public void Then_the_fine_gain_key_matches_requirements()
        {
            Assert.AreEqual("finegain", ScintiSpecSpecification.FineGainSettingKey);
        }

        [TestMethod]
        public void Then_the_high_voltage_key_matches_requirements()
        {
            Assert.AreEqual("highvoltage", ScintiSpecSpecification.HighVoltageSettingKey);
        }
    }
    // ReSharper restore InconsistentNaming
}
