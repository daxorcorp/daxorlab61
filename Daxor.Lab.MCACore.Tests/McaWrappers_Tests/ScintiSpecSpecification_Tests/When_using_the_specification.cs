using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecSpecification_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_specification
    {
        private static ScintiSpecSpecification _specification;

        [ClassInitialize]
        public static void BeforeAllTests(TestContext context)
        {
            _specification = new ScintiSpecSpecification(5);
        }

        [TestMethod]
        public void Then_the_minimum_fine_gain_is_0()
        {
            Assert.AreEqual(0, _specification.MinFineGain);
        }

        [TestMethod]
        public void Then_the_maximum_fine_gain_is_2()
        {
            Assert.AreEqual(2, _specification.MaxFineGain);
        }

        [TestMethod]
        public void Then_the_minimum_high_voltage_is_600()
        {
            Assert.AreEqual(600, _specification.MinHighVoltage);
        }

        [TestMethod]
        public void Then_the_maximum_high_voltage_is_1200()
        {
            Assert.AreEqual(1200, _specification.MaxHighVoltage);
        }

        [TestMethod]
        public void Then_the_minimum_lower_level_discriminator_is_0()
        {
            Assert.AreEqual(0, _specification.MinLowerLevelDiscriminator);
        }

        [TestMethod]
        public void Then_the_maximum_lower_level_discriminator_is_1023()
        {
            Assert.AreEqual(1023, _specification.MaxLowerLevelDiscriminator);
        }

        [TestMethod]
        public void Then_the_minimum_noise_level_is_0()
        {
            Assert.AreEqual(0, _specification.MinNoiseLevel);
        }

        [TestMethod]
        public void Then_the_maximum_noise_level_is_85()
        {
            Assert.AreEqual(85, _specification.MaxNoiseLevel);
        }
    }
    // ReSharper restore InconsistentNaming
}
