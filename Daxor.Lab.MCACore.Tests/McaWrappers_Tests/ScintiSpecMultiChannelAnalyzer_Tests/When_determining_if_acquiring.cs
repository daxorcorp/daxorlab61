using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_acquiring
    {
        [TestMethod]
        public void Then_the_answer_is_obtained_from_the_driver()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);

            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver);

            Assert.IsTrue(mca.IsAcquiring);
        }
    }
    // ReSharper restore InconsistentNaming
}
