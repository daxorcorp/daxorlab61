using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_high_voltage
    {
        [TestMethod]
        public void Then_the_high_voltage_is_obtained_from_the_driver()
        {
            const int expectedHighVoltage = 1035;

            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetHighVoltageOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(expectedHighVoltage);

            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver);

            Assert.AreEqual(expectedHighVoltage, mca.HighVoltage);
        }
    }
    // ReSharper restore InconsistentNaming
}
