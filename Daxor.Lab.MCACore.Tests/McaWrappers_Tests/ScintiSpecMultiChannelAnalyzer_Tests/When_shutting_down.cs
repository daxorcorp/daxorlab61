using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_shutting_down
    {
        [TestMethod]
        public void Then_acquisition_is_stopped_and_the_driver_is_used_to_shut_down()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.ShutDown();

            Received.InOrder(() =>
            {
                mockDriver.StopAcquisitionOnPort(ScintiSpecMcaTestHelpers.PortNumber);
                mockDriver.ShutDownOnPort(ScintiSpecMcaTestHelpers.PortNumber);
            });
        }
    }
    // ReSharper restore InconsistentNaming
}
