using System;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_presetting_gross_counts
    {
        [TestMethod]
        public void Then_the_region_of_interest_is_set_via_the_driver()
        {
            const int expectedBeginChannel = 30;
            const int expectedEndChannel = 600;
            const Int64 expectedCounts = 3450985;

            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.PresetGrossCountsInRegionOfInterest(expectedCounts, expectedBeginChannel, expectedEndChannel);

            mockDriver.Received().SetRegionOfInterestOnPort(ScintiSpecMcaTestHelpers.PortNumber, expectedBeginChannel, expectedEndChannel);
        }

        [TestMethod]
        public void And_the_begin_and_end_marker_are_reversed_but_still_in_range_then_the_order_is_corrected()
        {
            const int expectedBeginChannel = 600;
            const int expectedEndChannel = 30;
            const Int64 expectedCounts = 3450985;

            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.PresetGrossCountsInRegionOfInterest(expectedCounts, expectedBeginChannel, expectedEndChannel);

            mockDriver.Received().SetRegionOfInterestOnPort(ScintiSpecMcaTestHelpers.PortNumber, expectedEndChannel, expectedBeginChannel);
        }

        [TestMethod]
        public void Then_the_counts_are_preset_via_the_driver()
        {
            const Int64 expectedCounts = 3450985;

            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.PresetGrossCountsInRegionOfInterest(expectedCounts, 30, 600);

            mockDriver.Received().PresetGrossCountsInRegionOfInterestOnPort(ScintiSpecMcaTestHelpers.PortNumber, expectedCounts);
        }
    }
    // ReSharper restore InconsistentNaming
}
