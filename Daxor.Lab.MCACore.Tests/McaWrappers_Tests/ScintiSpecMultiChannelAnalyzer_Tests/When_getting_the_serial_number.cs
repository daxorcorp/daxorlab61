using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_serial_number
    {
        [TestMethod]
        public void Then_the_serial_number_is_obtained_from_the_driver()
        {
            const int expectedSerialNumber = 456663;

            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetSerialNumberOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(expectedSerialNumber);

            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver);

            Assert.AreEqual(expectedSerialNumber, mca.SerialNumber);
        }
    }
    // ReSharper restore InconsistentNaming
}
