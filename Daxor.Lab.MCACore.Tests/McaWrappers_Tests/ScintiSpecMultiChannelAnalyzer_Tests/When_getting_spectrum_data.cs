using System;
using System.ComponentModel;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_spectrum_data
    {
        [TestMethod]
        public void And_not_connected_then_empty_spectrum_data_is_returned()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver);

            var spectrumData = mca.GetSpectrumData();

            Assert.IsNull(spectrumData.SpectrumArray);
            Assert.AreEqual(0, spectrumData.DeadTimeInMicroseconds);
            Assert.AreEqual(0, spectrumData.LiveTimeInMicroseconds);
            Assert.AreEqual(0, spectrumData.RealTimeInMicroseconds);
        }

        [TestMethod]
        public void And_connected_then_the_driver_is_used_to_get_the_spectrum_counts()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            var expectedArray = new UInt32[1024];
            mockDriver.GetSpectrumOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(expectedArray);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            var spectrumData = mca.GetSpectrumData();

            Assert.AreEqual(expectedArray, spectrumData.SpectrumArray);
        }

        [TestMethod]
        public void And_connected_then_the_driver_is_used_to_get_the_real_time()
        {
            const int expectedRealTime = 34985;
            
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            mockDriver.GetRealTimeInMicrosecondsOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(expectedRealTime);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            var spectrumData = mca.GetSpectrumData();

            Assert.AreEqual(expectedRealTime, spectrumData.RealTimeInMicroseconds);
        }

        [TestMethod]
        public void And_connected_then_the_driver_is_used_to_get_the_live_time()
        {
            const int expectedLiveTime = 349845;

            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            mockDriver.GetLiveTimeInMicrosecondsOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(expectedLiveTime);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            var spectrumData = mca.GetSpectrumData();

            Assert.AreEqual(expectedLiveTime, spectrumData.LiveTimeInMicroseconds);
        }

        [TestMethod]
        public void And_connected_then_the_driver_is_used_to_get_the_dead_time()
        {
            const int expectedDeadTime = 34984665;

            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            mockDriver.GetDeadTimeInMicrosecondsOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(expectedDeadTime);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            var spectrumData = mca.GetSpectrumData();

            Assert.AreEqual(expectedDeadTime, spectrumData.DeadTimeInMicroseconds);
        }
    }
    // ReSharper restore InconsistentNaming
}
