using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_spectrum_length
    {
        [TestMethod]
        public void Then_the_spectrum_length_is_obtained_from_the_driver()
        {
            const int expectedSpectrumLength = 1068;

            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetSpectrumLengthOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(expectedSpectrumLength);

            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver);

            Assert.AreEqual(expectedSpectrumLength, mca.SpectrumLength);
        }
    }
    // ReSharper restore InconsistentNaming
}
