using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_noise_level
    {
        private const int NoiseLevlToSet = 15;

        [TestMethod]
        public void Then_the_noise_level_is_set_via_the_driver()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.NoiseLevel = NoiseLevlToSet;

            mockDriver.Received().SetNoiseLevelOnPort(ScintiSpecMcaTestHelpers.PortNumber, NoiseLevlToSet);
        }

        [TestMethod]
        public void Then_the_hardware_configuration_manager_is_updated_with_the_new_noise_level()
        {
            var stubHardwareConfigurationManager = Substitute.For<IHardwareConfigurationManager>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithHardwareConfigurationManager(stubHardwareConfigurationManager);

            mca.NoiseLevel = NoiseLevlToSet;

            Assert.AreEqual(NoiseLevlToSet, stubHardwareConfigurationManager.GetSetting(ScintiSpecSpecification.NoiseLevelSettingKey).GetValue<int>());
        }

        [TestMethod]
        public void Then_the_minimum_bound_is_inclusive()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.NoiseLevel = ScintiSpecMcaTestHelpers.MinNoiseLevel;

            mockDriver.Received().SetNoiseLevelOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.MinNoiseLevel);
        }

        [TestMethod]
        public void Then_the_maximum_bound_is_inclusive()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.NoiseLevel = ScintiSpecMcaTestHelpers.MaxNoiseLevel;

            mockDriver.Received().SetNoiseLevelOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.MaxNoiseLevel);
        }
    }
    // ReSharper restore InconsistentNaming
}
