using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_lower_level_discriminator
    {
        [TestMethod]
        public void Then_the_lower_level_discriminator_is_obtained_from_the_driver()
        {
            const int expectedLld = 16;

            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetLowerLevelDiscriminatorOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(expectedLld);

            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver);

            Assert.AreEqual(expectedLld, mca.LowerLevelDiscriminator);
        }
    }
    // ReSharper restore InconsistentNaming
}
