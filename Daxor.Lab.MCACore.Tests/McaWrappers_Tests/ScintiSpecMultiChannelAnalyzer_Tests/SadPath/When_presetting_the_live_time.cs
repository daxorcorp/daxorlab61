using System;
using Daxor.Lab.MCACore.Exceptions;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_presetting_the_live_time
    {
        [TestMethod]
        public void And_the_value_is_negative_then_an_exception_is_thrown()
        {
            var mca = ScintiSpecMcaTestHelpers.CreateBasicMca();
            var exception = GetExceptionFromOutOfBoundsPresetLiveTime(mca, -1);

            Assert.IsNotNull(exception);
            Assert.AreEqual("0", exception.MinValue);
            Assert.AreEqual(Int64.MaxValue.ToString(), exception.MaxValue);
            Assert.AreEqual("Live-time preset value is out of bounds", exception.Message);
        }

        private static OutOfBoundsException GetExceptionFromOutOfBoundsPresetLiveTime(IMultiChannelAnalyzer mca, Int64 liveTime)
        {
            OutOfBoundsException exceptionToReturn = null;

            try
            {
                mca.PresetLiveTime(liveTime);
            }
            catch (OutOfBoundsException ex)
            {
                exceptionToReturn = ex;
            }

            return exceptionToReturn;
        }
    }
    // ReSharper restore InconsistentNaming
}
