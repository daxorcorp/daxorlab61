using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Exceptions;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_noise_level
    {
        [TestMethod]
        public void And_the_value_is_below_the_minimum_then_an_exception_is_thrown()
        {
            var mca = ScintiSpecMcaTestHelpers.CreateBasicMca();
            var exception = GetExceptionFromOutOfBoundsNoiseLevel(mca, ScintiSpecMcaTestHelpers.MinNoiseLevel - 1);

            Assert.IsNotNull(exception);
            Assert.AreEqual(ScintiSpecMcaTestHelpers.MinNoiseLevel.ToString(), exception.MinValue);
            Assert.AreEqual(ScintiSpecMcaTestHelpers.MaxNoiseLevel.ToString(), exception.MaxValue);
            Assert.AreEqual("Noise level value is out of bounds", exception.Message);
        }

        [TestMethod]
        public void And_the_value_is_above_the_maximum_then_an_exception_is_thrown()
        {
            var mca = ScintiSpecMcaTestHelpers.CreateBasicMca();
            var exception = GetExceptionFromOutOfBoundsNoiseLevel(mca, ScintiSpecMcaTestHelpers.MaxNoiseLevel + 1);

            Assert.IsNotNull(exception);
            Assert.AreEqual(ScintiSpecMcaTestHelpers.MinNoiseLevel.ToString(), exception.MinValue);
            Assert.AreEqual(ScintiSpecMcaTestHelpers.MaxNoiseLevel.ToString(), exception.MaxValue);
            Assert.AreEqual("Noise level value is out of bounds", exception.Message);
        }

        [TestMethod]
        public void And_the_value_is_out_of_range_then_the_value_is_not_sent_to_the_driver()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            // ReSharper disable once UnusedVariable
            var exception = GetExceptionFromOutOfBoundsNoiseLevel(mca, ScintiSpecMcaTestHelpers.MaxNoiseLevel + 1);

            mockDriver.DidNotReceive().SetNoiseLevelOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.MaxNoiseLevel + 1);
        }

        [TestMethod]
        public void And_the_value_is_out_of_range_then_the_value_is_not_sent_to_the_hardware_configuration_manager()
        {
            var mockHardwareConfigurationManager = Substitute.For<IHardwareConfigurationManager>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithHardwareConfigurationManager(mockHardwareConfigurationManager);

            // ReSharper disable once UnusedVariable
            var exception = GetExceptionFromOutOfBoundsNoiseLevel(mca, ScintiSpecMcaTestHelpers.MaxNoiseLevel + 1);

            Assert.AreNotEqual(ScintiSpecMcaTestHelpers.MaxNoiseLevel + 1, mockHardwareConfigurationManager.GetSetting(ScintiSpecSpecification.NoiseLevelSettingKey).GetValue<int>());
        }

        private static OutOfBoundsException GetExceptionFromOutOfBoundsNoiseLevel(IMultiChannelAnalyzer mca, int noiseLevel)
        {
            OutOfBoundsException exceptionToReturn = null;

            try
            {
                mca.NoiseLevel = noiseLevel;
            }
            catch (OutOfBoundsException ex)
            {
                exceptionToReturn = ex;
            }

            return exceptionToReturn;
        }
    }
    // ReSharper restore InconsistentNaming
}
