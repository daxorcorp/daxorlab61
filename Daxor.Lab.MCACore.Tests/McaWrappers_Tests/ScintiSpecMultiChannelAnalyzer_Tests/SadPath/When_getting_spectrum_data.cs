using System;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_spectrum_data
    {
        [TestMethod]
        public void And_an_exception_is_thrown_then_empty_spectrum_data_is_returned()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetSpectrumOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(x => { throw new Exception(); });
            stubDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver);

            var spectrumData = mca.GetSpectrumData();

            Assert.IsNull(spectrumData.SpectrumArray);
            Assert.AreEqual(0, spectrumData.DeadTimeInMicroseconds);
            Assert.AreEqual(0, spectrumData.LiveTimeInMicroseconds);
            Assert.AreEqual(0, spectrumData.RealTimeInMicroseconds);
        }
    }
    // ReSharper restore InconsistentNaming
}
