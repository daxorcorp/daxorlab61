using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_monitoring_acquisition
    {
        [TestMethod]
        public void And_acquisition_stopped_because_the_MCA_is_disconnected_then_no_events_are_raised()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true, false);
            stubDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false);
            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver, true);

            var raisedEventNames = string.Empty;
            mca.AcquisitionStopped += (sender, time) => { raisedEventNames += "AcquisitionStopped\n"; };
            mca.AcquisitionCompleted += (sender, data) => { raisedEventNames += "AcquisitionCompleted\n"; };
            
            mca.InvokeMonitorAcquisition();

            Assert.AreEqual(string.Empty, raisedEventNames);
        }

        [TestMethod]
        public void And_acquisition_stopped_because_the_MCA_is_disconnected_then_a_message_is_logged()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true, false);
            stubDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false);
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriverAndLogger(stubDriver, mockLogger, true);

            mca.InvokeMonitorAcquisition();

            mockLogger.Received().Log("MCA disconnected while monitoring acquisition");
        }

        [TestMethod]
        public void And_an_exception_is_thrown_from_the_driver_then_no_events_are_raised()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(x => { throw new Exception(); });
            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver, true);

            var raisedEventNames = string.Empty;
            mca.AcquisitionStopped += (sender, time) => { raisedEventNames += "AcquisitionStopped\n"; };
            mca.AcquisitionCompleted += (sender, data) => { raisedEventNames += "AcquisitionCompleted\n"; };

            mca.InvokeMonitorAcquisition();

            Assert.AreEqual(string.Empty, raisedEventNames);
        }

        [TestMethod]
        public void And_an_exception_is_thrown_from_the_driver_then_a_message_is_logged()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(x => { throw new Exception("message"); });
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriverAndLogger(stubDriver, mockLogger, true);

            mca.InvokeMonitorAcquisition();

            mockLogger.Received().Log("Exception while monitoring acquisition: message");
        }
    }
    // ReSharper restore InconsistentNaming
}
