using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_starting_acquisition
    {
        [TestMethod]
        public void And_an_exception_is_thrown_then_the_message_is_logged()
        {
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriverAndLogger(CreateDriverThatThrowsExceptionWhenStartingAcquisition(), mockLogger);

            mca.StartAcquisition();

            mockLogger.Received().Log("Exception while starting acquisition: message");
        }

        [TestMethod]
        public void And_an_exception_is_thrown_then_acquisition_is_not_in_progress()
        {
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(CreateDriverThatThrowsExceptionWhenStartingAcquisition());

            Assert.IsFalse(mca.StartAcquisition());
        }

        [TestMethod]
        public void And_the_driver_indicates_acquisition_did_not_start_then_acquisition_is_not_in_progress()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false);
            stubDriver.StartAcquisitionOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false);
            var mockThreadWrapper = Substitute.For<IThreadWrapper>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriverAndThreadWrapper(stubDriver, mockThreadWrapper);

            var isAcquiring = mca.StartAcquisition();

            mockThreadWrapper.DidNotReceive().Start(Arg.Any<Action>());
            mockThreadWrapper.DidNotReceive().Sleep(Arg.Any<int>());
            Assert.IsFalse(isAcquiring);
        }

        private IScintiSpecDriver CreateDriverThatThrowsExceptionWhenStartingAcquisition()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false);
            stubDriver.StartAcquisitionOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(x => { throw new Exception("message"); });

            return stubDriver;
        }
    }
    // ReSharper restore InconsistentNaming
}
