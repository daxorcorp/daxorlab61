using System;
using Daxor.Lab.MCACore.Exceptions;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_presetting_gross_counts
    {
        [TestMethod]
        public void And_the_counts_value_is_negative_then_an_exception_is_thrown()
        {
            var mca = ScintiSpecMcaTestHelpers.CreateBasicMca();
            var exception = GetExceptionFromOutOfBoundsPresetCounts(mca, -1);

            Assert.IsNotNull(exception);
            Assert.AreEqual("0", exception.MinValue);
            Assert.AreEqual(Int64.MaxValue.ToString(), exception.MaxValue);
            Assert.AreEqual("Preset gross counts value is out of bounds", exception.Message);
        }

        [TestMethod]
        public void And_the_beginning_channel_value_is_negative_then_an_exception_is_thrown()
        {
            var mca = ScintiSpecMcaTestHelpers.CreateBasicMca();
            var exception = GetExceptionFromOutOfBoundsRoiMarkers(mca, -1, 400);

            Assert.IsNotNull(exception);
            Assert.AreEqual("0", exception.MinValue);
            Assert.AreEqual(Int64.MaxValue.ToString(), exception.MaxValue);
            Assert.AreEqual("Region-of-interest beginning channel value is out of bounds", exception.Message);
        }

        [TestMethod]
        public void And_the_ending_channel_value_is_negative_then_an_exception_is_thrown()
        {
            var mca = ScintiSpecMcaTestHelpers.CreateBasicMca();
            var exception = GetExceptionFromOutOfBoundsRoiMarkers(mca, 0, -1);

            Assert.IsNotNull(exception);
            Assert.AreEqual("0", exception.MinValue);
            Assert.AreEqual(Int64.MaxValue.ToString(), exception.MaxValue);
            Assert.AreEqual("Region-of-interest ending channel value is out of bounds", exception.Message);
        }

        private static OutOfBoundsException GetExceptionFromOutOfBoundsPresetCounts(IMultiChannelAnalyzer mca, Int64 counts)
        {
            OutOfBoundsException exceptionToReturn = null;

            try
            {
                mca.PresetGrossCountsInRegionOfInterest(counts, 30, 500);
            }
            catch (OutOfBoundsException ex)
            {
                exceptionToReturn = ex;
            }

            return exceptionToReturn;
        }

        private static OutOfBoundsException GetExceptionFromOutOfBoundsRoiMarkers(IMultiChannelAnalyzer mca, int beginChannel, int endChannel)
        {
            OutOfBoundsException exceptionToReturn = null;

            try
            {
                mca.PresetGrossCountsInRegionOfInterest(100, beginChannel, endChannel);
            }
            catch (OutOfBoundsException ex)
            {
                exceptionToReturn = ex;
            }

            return exceptionToReturn;
        }
    }
    // ReSharper restore InconsistentNaming
}
