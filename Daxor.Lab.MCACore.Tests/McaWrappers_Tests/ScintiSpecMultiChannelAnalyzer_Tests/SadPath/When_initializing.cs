using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        private readonly IScintiSpecDriver _driver = Substitute.For<IScintiSpecDriver>();
        private readonly IHardwareConfigurationManager _hardwareConfigurationManager = Substitute.For<IHardwareConfigurationManager>();
        private readonly IMultiChannelAnalyzerSpecification _mcaSpec = Substitute.For<IMultiChannelAnalyzerSpecification>();
        private readonly IWindowsEventLogger _logger = Substitute.For<IWindowsEventLogger>();
        private readonly IThreadWrapper _threadWrapper = Substitute.For<IThreadWrapper>();

        [TestMethod]
        public void And_the_scintiSPEC_driver_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() => new ScintiSpecMultiChannelAnalyzer(null, _hardwareConfigurationManager,
                _mcaSpec, _logger, _threadWrapper), "scintiSpecDriver");
        }

        [TestMethod]
        public void And_the_hardware_configuration_manager_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() => new ScintiSpecMultiChannelAnalyzer(_driver, null,
                _mcaSpec, _logger, _threadWrapper), "hardwareConfigurationManager");
        }

        [TestMethod]
        public void And_the_MCA_specification_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() => new ScintiSpecMultiChannelAnalyzer(_driver, _hardwareConfigurationManager,
                null, _logger, _threadWrapper), "multiChannelAnalyzerSpecification");
        }

        [TestMethod]
        public void And_the_logger_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() => new ScintiSpecMultiChannelAnalyzer(_driver, _hardwareConfigurationManager,
                _mcaSpec, null, _threadWrapper), "windowsEventLogger");
        }

        [TestMethod]
        public void And_the_thread_wrapper_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(() => new ScintiSpecMultiChannelAnalyzer(_driver, _hardwareConfigurationManager,
                _mcaSpec, _logger, null), "threadWrapper");
        }
    }
    // ReSharper restore InconsistentNaming
}
