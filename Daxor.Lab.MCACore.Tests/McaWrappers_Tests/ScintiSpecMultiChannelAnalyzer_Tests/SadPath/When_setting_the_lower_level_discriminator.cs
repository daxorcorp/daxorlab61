using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Exceptions;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_lower_level_discriminator
    {
        [TestMethod]
        public void And_the_value_is_below_the_minimum_then_an_exception_is_thrown()
        {
            var mca = ScintiSpecMcaTestHelpers.CreateBasicMca();
            var exception = GetExceptionFromOutOfBoundsLowerLevelDiscriminator(mca, ScintiSpecMcaTestHelpers.MinLowerLevelDiscriminator - 1);

            Assert.IsNotNull(exception);
            Assert.AreEqual(ScintiSpecMcaTestHelpers.MinLowerLevelDiscriminator.ToString(), exception.MinValue);
            Assert.AreEqual(ScintiSpecMcaTestHelpers.MaxLowerLevelDiscriminator.ToString(), exception.MaxValue);
            Assert.AreEqual("Lower-level discriminator value is out of bounds", exception.Message);
        }

        [TestMethod]
        public void And_the_value_is_above_the_maximum_then_an_exception_is_thrown()
        {
            var mca = ScintiSpecMcaTestHelpers.CreateBasicMca();
            var exception = GetExceptionFromOutOfBoundsLowerLevelDiscriminator(mca, ScintiSpecMcaTestHelpers.MaxLowerLevelDiscriminator + 1);

            Assert.IsNotNull(exception);
            Assert.AreEqual(ScintiSpecMcaTestHelpers.MinLowerLevelDiscriminator.ToString(), exception.MinValue);
            Assert.AreEqual(ScintiSpecMcaTestHelpers.MaxLowerLevelDiscriminator.ToString(), exception.MaxValue);
            Assert.AreEqual("Lower-level discriminator value is out of bounds", exception.Message);
        }

        [TestMethod]
        public void And_the_value_is_out_of_range_then_the_value_is_not_sent_to_the_driver()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            // ReSharper disable once UnusedVariable
            var exception = GetExceptionFromOutOfBoundsLowerLevelDiscriminator(mca, ScintiSpecMcaTestHelpers.MaxLowerLevelDiscriminator + 1);

            mockDriver.DidNotReceive().SetLowerLevelDiscriminatorOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.MaxLowerLevelDiscriminator + 1);
        }

        [TestMethod]
        public void And_the_value_is_out_of_range_then_the_value_is_not_sent_to_the_hardware_configuration_manager()
        {
            var mockHardwareConfigurationManager = Substitute.For<IHardwareConfigurationManager>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithHardwareConfigurationManager(mockHardwareConfigurationManager);

            // ReSharper disable once UnusedVariable
            var exception = GetExceptionFromOutOfBoundsLowerLevelDiscriminator(mca, ScintiSpecMcaTestHelpers.MaxLowerLevelDiscriminator + 1);

            Assert.AreNotEqual(ScintiSpecMcaTestHelpers.MaxLowerLevelDiscriminator + 1, mockHardwareConfigurationManager.GetSetting(ScintiSpecSpecification.LowerLevelDiscriminatorSettingKey).GetValue<int>());
        }

        private static OutOfBoundsException GetExceptionFromOutOfBoundsLowerLevelDiscriminator(IMultiChannelAnalyzer mca, int lld)
        {
            OutOfBoundsException exceptionToReturn = null;

            try
            {
                mca.LowerLevelDiscriminator = lld;
            }
            catch (OutOfBoundsException ex)
            {
                exceptionToReturn = ex;
            }

            return exceptionToReturn;
        }
    }
    // ReSharper restore InconsistentNaming
}
