using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_noise_channel
    {
        [TestMethod]
        public void Then_the_noise_channel_is_obtained_from_the_driver()
        {
            const int expectedNoiseChannel = 16;

            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetNoiseLevelOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(expectedNoiseChannel);

            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver);

            Assert.AreEqual(expectedNoiseChannel, mca.NoiseLevel);
        }
    }
    // ReSharper restore InconsistentNaming
}
