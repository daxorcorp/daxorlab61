using System;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_starting_acquisition
    {
        [TestMethod]
        public void And_acquisition_is_already_in_progress_then_acquisition_remains_in_progress()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            var acquisitionResult = mca.StartAcquisition();

            mockDriver.DidNotReceive().StartAcquisitionOnPort(ScintiSpecMcaTestHelpers.PortNumber);
            Assert.IsTrue(acquisitionResult);
        }

        [TestMethod]
        public void And_acquisition_is_not_already_in_progress_then_the_driver_is_used_to_start_acquisition()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.StartAcquisition();

            mockDriver.Received().StartAcquisitionOnPort(ScintiSpecMcaTestHelpers.PortNumber);
        }

        [TestMethod]
        public void And_acquisition_is_not_already_in_progress_then_the_acquisition_monitor_thread_is_started()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false);
            stubDriver.StartAcquisitionOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            var mockThreadWrapper = Substitute.For<IThreadWrapper>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriverAndThreadWrapper(stubDriver, mockThreadWrapper);

            mca.StartAcquisition();

            Received.InOrder(() =>
            {
                mockThreadWrapper.Start(Arg.Is<Action>(x => x != null));
                mockThreadWrapper.Sleep(0);
            });
        }
    }
    // ReSharper restore InconsistentNaming
}
