using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_high_voltage
    {
        private const int HighVoltageToSet = 1035;
        
        [TestMethod]
        public void Then_the_high_voltage_is_set_via_the_driver()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.HighVoltage = HighVoltageToSet;

            mockDriver.Received().SetHighVoltageOnPort(ScintiSpecMcaTestHelpers.PortNumber, HighVoltageToSet);
        }

        [TestMethod]
        public void Then_the_hardware_configuration_manager_is_updated_with_the_new_high_voltage()
        {
            var stubHardwareConfigurationManager = Substitute.For<IHardwareConfigurationManager>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithHardwareConfigurationManager(stubHardwareConfigurationManager);

            mca.HighVoltage = HighVoltageToSet;

            Assert.AreEqual(HighVoltageToSet, stubHardwareConfigurationManager.GetSetting(ScintiSpecSpecification.HighVoltageSettingKey).GetValue<int>());
        }

        [TestMethod]
        public void Then_the_minimum_bound_is_inclusive()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.HighVoltage = ScintiSpecMcaTestHelpers.MinHighVoltage;

            mockDriver.Received().SetHighVoltageOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.MinHighVoltage);
        }

        [TestMethod]
        public void Then_the_maximum_bound_is_inclusive()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.HighVoltage = ScintiSpecMcaTestHelpers.MaxHighVoltage;

            mockDriver.Received().SetHighVoltageOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.MaxHighVoltage);
        }
    }
    // ReSharper restore InconsistentNaming
}
