using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_resetting_to_the_default_configuration
    {
        private IScintiSpecDriver _mockDriver;
        private IMultiChannelAnalyzer _mca;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockDriver = Substitute.For<IScintiSpecDriver>();
            _mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(_mockDriver);
        }

        [TestMethod]
        public void Then_the_hardware_configuration_manager_is_asked_to_reset_to_defaults()
        {
            var mockConfigurationManager = Substitute.For<IHardwareConfigurationManager>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithHardwareConfigurationManager(mockConfigurationManager);

            mca.ResetToDefaultConfiguration();

            mockConfigurationManager.Received().ResetToDefaultConfiguration();
        }

        [TestMethod]
        public void And_the_MCA_is_connected_then_the_noise_channel_is_reset()
        {
            _mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            _mca.ResetToDefaultConfiguration();

            _mockDriver.Received().SetNoiseLevelOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialNoiseLevel);
        }

        [TestMethod]
        public void And_the_MCA_is_connected_then_the_lower_level_discriminator_is_reset()
        {
            _mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            _mca.ResetToDefaultConfiguration();

            _mockDriver.Received().SetLowerLevelDiscriminatorOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialLowerLevelDiscriminator);
        }

        [TestMethod]
        public void And_the_MCA_is_connected_then_the_fine_gain_is_reset()
        {
            _mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            _mca.ResetToDefaultConfiguration();

            _mockDriver.Received().SetFineGainOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialFineGain);
        }

        [TestMethod]
        public void And_the_MCA_is_connected_then_the_high_voltage_is_reset()
        {
            _mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            _mca.ResetToDefaultConfiguration();

            _mockDriver.Received().SetHighVoltageOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialHighVoltage);
        }

        [TestMethod]
        public void And_the_MCA_is_not_connected_then_no_settings_are_affected()
        {
            _mca.ResetToDefaultConfiguration();

            _mockDriver.DidNotReceive().SetNoiseLevelOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialNoiseLevel);
            _mockDriver.DidNotReceive().SetLowerLevelDiscriminatorOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialLowerLevelDiscriminator);
            _mockDriver.DidNotReceive().SetFineGainOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialFineGain);
            _mockDriver.DidNotReceive().SetHighVoltageOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialHighVoltage);
        }
    }
    // ReSharper restore InconsistentNaming
}
