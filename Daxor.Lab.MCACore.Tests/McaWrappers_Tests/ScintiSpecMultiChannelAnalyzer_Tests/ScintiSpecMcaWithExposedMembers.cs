﻿using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    internal class ScintiSpecMcaWithExposedMembers : ScintiSpecMultiChannelAnalyzer
    {
        public ScintiSpecMcaWithExposedMembers(IScintiSpecDriver scintiSpecDriver, IHardwareConfigurationManager hardwareConfigurationManager, IMultiChannelAnalyzerSpecification multiChannelAnalyzerSpecification, IWindowsEventLogger windowsEventLogger, IThreadWrapper threadWrapper) : base(scintiSpecDriver, hardwareConfigurationManager, multiChannelAnalyzerSpecification, windowsEventLogger, threadWrapper)
        {
        }

        internal bool ExposedHasAcquisitionCompleted 
        {
            get { return InnerHasAcquisitionCompleted; }
            set { InnerHasAcquisitionCompleted = value; }
        }

        internal void InvokeMonitorAcquisition()
        {
            MonitorAcquisition();
        }
    }
}
