using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_clearing_the_spectrum_and_presets
    {
        [TestMethod]
        public void Then_the_spectrum_and_presets_are_cleared_via_the_driver()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.ClearSpectrumAndPresets();

            mockDriver.Received().ClearSpectrumAndPresetsOnPort(ScintiSpecMcaTestHelpers.PortNumber);
        }
    }
    // ReSharper restore InconsistentNaming
}
