using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        private IScintiSpecDriver _mockDriver;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockDriver = Substitute.For<IScintiSpecDriver>();
            _mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);

            // ReSharper disable once UnusedVariable
            var ignored = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(_mockDriver);
        }

        [TestMethod]
        public void And_the_MCA_is_connected_then_the_noise_is_set_based_on_the_hardware_configuration()
        {
            _mockDriver.Received().SetNoiseLevelOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialNoiseLevel);
        }

        [TestMethod]
        public void And_the_MCA_is_connected_then_the_lower_level_discriminator_is_set_based_on_the_hardware_configuration()
        {
            _mockDriver.Received().SetLowerLevelDiscriminatorOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialLowerLevelDiscriminator);
        }

        [TestMethod]
        public void And_the_MCA_is_connected_then_the_fine_gain_is_set_based_on_the_hardware_configuration()
        {
            _mockDriver.Received().SetFineGainOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialFineGain);
        }

        [TestMethod]
        public void And_the_MCA_is_connected_then_the_high_voltage_is_set_based_on_the_hardware_configuration()
        {
            _mockDriver.Received().SetHighVoltageOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialHighVoltage);
        }

        [TestMethod]
        public void And_the_MCA_is_not_connected_then_no_settings_are_set()
        {
            var mockDriverForDisconnectedMca = Substitute.For<IScintiSpecDriver>();
            
            // ReSharper disable once UnusedVariable
            var ignored = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriverForDisconnectedMca);

            mockDriverForDisconnectedMca.DidNotReceive().SetNoiseLevelOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialNoiseLevel);
            mockDriverForDisconnectedMca.DidNotReceive().SetLowerLevelDiscriminatorOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialLowerLevelDiscriminator);
            mockDriverForDisconnectedMca.DidNotReceive().SetFineGainOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialFineGain);
            mockDriverForDisconnectedMca.DidNotReceive().SetHighVoltageOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialHighVoltage);
        }

        [TestMethod]
        public void Then_the_connection_status_is_checked()
        {
            _mockDriver.Received().IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber);
        }
    }
    // ReSharper restore InconsistentNaming
}
