using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_fine_gain
    {
        [TestMethod]
        public void Then_the_fine_gain_is_obtained_from_the_driver()
        {
            const double expectedFineGain = 1.569;

            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetFineGainOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(expectedFineGain);

            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver);

            Assert.AreEqual(expectedFineGain, mca.FineGain);
        }
    }
    // ReSharper restore InconsistentNaming
}
