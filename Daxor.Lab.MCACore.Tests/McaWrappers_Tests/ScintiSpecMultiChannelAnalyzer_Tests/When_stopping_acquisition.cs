using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_stopping_acquisition
    {
        [TestMethod]
        public void And_acquisition_is_in_progress_then_the_driver_is_used_to_stop_acquisition()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.StopAcquisition();

            mockDriver.Received().StopAcquisitionOnPort(ScintiSpecMcaTestHelpers.PortNumber);
        }

        [TestMethod]
        public void And_acquisition_is_not_in_progress_then_the_driver_is_not_used_to_stop_acquisition()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.StopAcquisition();

            mockDriver.DidNotReceive().StopAcquisitionOnPort(ScintiSpecMcaTestHelpers.PortNumber);
        }
    }
    // ReSharper restore InconsistentNaming
}
