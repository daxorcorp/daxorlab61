﻿using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    internal static class ScintiSpecMcaTestHelpers
    {
        internal const int PortNumber = 5;
        
        internal const int InitialNoiseLevel = 35;
        internal const int InitialLowerLevelDiscriminator = 18;
        internal const double InitialFineGain = 1.234;
        internal const int InitialHighVoltage = 1335;

        internal const int MinHighVoltage = 0;
        internal const int MaxHighVoltage = 1800;
        internal const int MinNoiseLevel = 0;
        internal const int MaxNoiseLevel = 1023;
        internal const int MinLowerLevelDiscriminator = 0;
        internal const int MaxLowerLevelDiscriminator = 1023;
        internal const double MinFineGain = 0.675;
        internal const double MaxFineGain = 1.456;

        internal static ScintiSpecMultiChannelAnalyzer CreateMcaWithDriver(IScintiSpecDriver driver, bool useExtendedClass = false)
        {
            var stubConfigurationManager = Substitute.For<IHardwareConfigurationManager>();
            AddSettingsToHardwareConfigurationManagerStub(stubConfigurationManager);

            if (useExtendedClass)
                return new ScintiSpecMcaWithExposedMembers(driver, stubConfigurationManager,
                    CreateMcaSpecification(), Substitute.For<IWindowsEventLogger>(),
                    Substitute.For<IThreadWrapper>());

            return new ScintiSpecMultiChannelAnalyzer(driver, stubConfigurationManager,
              CreateMcaSpecification(), Substitute.For<IWindowsEventLogger>(),
              Substitute.For<IThreadWrapper>());
        }

        internal static ScintiSpecMultiChannelAnalyzer CreateMcaWithDriverAndLogger(IScintiSpecDriver driver, IWindowsEventLogger logger, bool useExtendedClass = false)
        {
            var stubConfigurationManager = Substitute.For<IHardwareConfigurationManager>();
            AddSettingsToHardwareConfigurationManagerStub(stubConfigurationManager);

            if (useExtendedClass)
                return new ScintiSpecMcaWithExposedMembers(driver, stubConfigurationManager,
                    CreateMcaSpecification(), logger, Substitute.For<IThreadWrapper>());

            return new ScintiSpecMultiChannelAnalyzer(driver, stubConfigurationManager,
              CreateMcaSpecification(), logger, Substitute.For<IThreadWrapper>());
        }

        internal static ScintiSpecMultiChannelAnalyzer CreateMcaWithDriverAndThreadWrapper(IScintiSpecDriver driver, IThreadWrapper threadWrapper, bool useExtendedClass = false)
        {
            var stubConfigurationManager = Substitute.For<IHardwareConfigurationManager>();
            AddSettingsToHardwareConfigurationManagerStub(stubConfigurationManager);

            if (useExtendedClass)
                return new ScintiSpecMcaWithExposedMembers(driver, stubConfigurationManager, CreateMcaSpecification(), Substitute.For<IWindowsEventLogger>(),
                    threadWrapper);

            return new ScintiSpecMultiChannelAnalyzer(driver, stubConfigurationManager,
              CreateMcaSpecification(), Substitute.For<IWindowsEventLogger>(), threadWrapper);
        }

        internal static ScintiSpecMultiChannelAnalyzer CreateMcaWithHardwareConfigurationManager(IHardwareConfigurationManager manager)
        {
            AddSettingsToHardwareConfigurationManagerStub(manager);

            return new ScintiSpecMultiChannelAnalyzer(Substitute.For<IScintiSpecDriver>(), manager,
              CreateMcaSpecification(), Substitute.For<IWindowsEventLogger>(),
              Substitute.For<IThreadWrapper>());
        }

        internal static ScintiSpecMultiChannelAnalyzer CreateBasicMca()
        {
            return CreateMcaWithDriver(Substitute.For<IScintiSpecDriver>());
        }

        private static IMultiChannelAnalyzerSpecification CreateMcaSpecification()
        {
            var mcaSpecification = Substitute.For<IMultiChannelAnalyzerSpecification>();
            mcaSpecification.PortNumber.Returns(PortNumber);
            mcaSpecification.MinHighVoltage.Returns(MinHighVoltage);
            mcaSpecification.MaxHighVoltage.Returns(MaxHighVoltage);
            mcaSpecification.MinNoiseLevel.Returns(MinNoiseLevel);
            mcaSpecification.MaxNoiseLevel.Returns(MaxNoiseLevel);
            mcaSpecification.MinLowerLevelDiscriminator.Returns(MinLowerLevelDiscriminator);
            mcaSpecification.MaxLowerLevelDiscriminator.Returns(MaxLowerLevelDiscriminator);
            mcaSpecification.MinFineGain.Returns(MinFineGain);
            mcaSpecification.MaxFineGain.Returns(MaxFineGain);
            
            return mcaSpecification;
        }

        private static void AddSettingsToHardwareConfigurationManagerStub(IHardwareConfigurationManager manager)
        {
            var noiseSetting = new HardwareSetting(ScintiSpecSpecification.NoiseLevelSettingKey, "integer");
            noiseSetting.SetValue(InitialNoiseLevel);
            manager.GetSetting(ScintiSpecSpecification.NoiseLevelSettingKey).Returns(noiseSetting);

            var lldSetting = new HardwareSetting(ScintiSpecSpecification.LowerLevelDiscriminatorSettingKey, "integer");
            lldSetting.SetValue(InitialLowerLevelDiscriminator);
            manager.GetSetting(ScintiSpecSpecification.LowerLevelDiscriminatorSettingKey).Returns(lldSetting);

            var fineGainSetting = new HardwareSetting(ScintiSpecSpecification.FineGainSettingKey, "double");
            fineGainSetting.SetValue(InitialFineGain);
            manager.GetSetting(ScintiSpecSpecification.FineGainSettingKey).Returns(fineGainSetting);

            var highVoltageSetting = new HardwareSetting(ScintiSpecSpecification.HighVoltageSettingKey, "integer");
            highVoltageSetting.SetValue(InitialHighVoltage);
            manager.GetSetting(ScintiSpecSpecification.HighVoltageSettingKey).Returns(highVoltageSetting);
        }
    }
}
