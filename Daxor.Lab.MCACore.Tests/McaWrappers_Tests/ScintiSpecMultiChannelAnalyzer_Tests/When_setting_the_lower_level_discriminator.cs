using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_lower_level_discriminator
    {
        private const int LowerLevelDiscriminatorToSet = 15;

        [TestMethod]
        public void Then_the_lower_level_discriminator_is_set_via_the_driver()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.LowerLevelDiscriminator = LowerLevelDiscriminatorToSet;

            mockDriver.Received().SetLowerLevelDiscriminatorOnPort(ScintiSpecMcaTestHelpers.PortNumber, LowerLevelDiscriminatorToSet);
        }

        [TestMethod]
        public void Then_the_hardware_configuration_manager_is_updated_with_the_new_lower_level_discriminator()
        {
            var stubHardwareConfigurationManager = Substitute.For<IHardwareConfigurationManager>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithHardwareConfigurationManager(stubHardwareConfigurationManager);

            mca.LowerLevelDiscriminator = LowerLevelDiscriminatorToSet;

            Assert.AreEqual(LowerLevelDiscriminatorToSet, stubHardwareConfigurationManager.GetSetting(ScintiSpecSpecification.LowerLevelDiscriminatorSettingKey).GetValue<int>());
        }

        [TestMethod]
        public void Then_the_minimum_bound_is_inclusive()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.LowerLevelDiscriminator= ScintiSpecMcaTestHelpers.MinLowerLevelDiscriminator;

            mockDriver.Received().SetLowerLevelDiscriminatorOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.MinLowerLevelDiscriminator);
        }

        [TestMethod]
        public void Then_the_maximum_bound_is_inclusive()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.LowerLevelDiscriminator = ScintiSpecMcaTestHelpers.MaxLowerLevelDiscriminator;

            mockDriver.Received().SetLowerLevelDiscriminatorOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.MaxLowerLevelDiscriminator);
        }
    }
    // ReSharper restore InconsistentNaming
}
