using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_gross_count_is_preset
    {
        [TestMethod]
        public void And_the_driver_indicates_that_gross_count_is_preset_then_gross_count_is_preset()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetPresetModeOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(ScintiSpecPresetMode.GrossCount);

            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver);

            Assert.IsTrue(mca.IsGrossCountPreset);
        }

        [TestMethod]
        public void And_the_driver_indicates_that_gross_count_is_not_preset_then_gross_count_is_not_preset()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetPresetModeOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(ScintiSpecPresetMode.LiveTime);

            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver);

            Assert.IsFalse(mca.IsGrossCountPreset);
        }
    }
    // ReSharper restore InconsistentNaming
}
