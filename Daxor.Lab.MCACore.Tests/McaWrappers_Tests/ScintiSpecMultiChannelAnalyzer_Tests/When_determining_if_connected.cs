using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_connected
    {
        [TestMethod]
        public void Then_the_answer_is_obtained_from_the_driver()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);

            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver);

            Assert.IsTrue(mca.IsConnected);
        }

        [TestMethod]
        public void And_the_MCA_was_previously_disconnected_then_the_noise_channel_is_updated_based_on_the_hardware_configuration_manager()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false, true);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);
            
            // ReSharper disable once UnusedVariable
            var ignored = mca.IsConnected;

            mockDriver.Received(2).SetNoiseLevelOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialNoiseLevel);
        }

        [TestMethod]
        public void And_the_MCA_was_previously_disconnected_then_the_lower_level_discriminator_is_updated_based_on_the_hardware_configuration_manager()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false, true);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            // ReSharper disable once UnusedVariable
            var ignored = mca.IsConnected;

            mockDriver.Received(2).SetLowerLevelDiscriminatorOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialLowerLevelDiscriminator);
        }

        [TestMethod]
        public void And_the_MCA_was_previously_disconnected_then_the_high_voltage_is_updated_based_on_the_hardware_configuration_manager()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false, true);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            // ReSharper disable once UnusedVariable
            var ignored = mca.IsConnected;

            mockDriver.Received(2).SetHighVoltageOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialHighVoltage);
        }

        [TestMethod]
        public void And_the_MCA_was_previously_disconnected_then_the_fine_gain_is_updated_based_on_the_hardware_configuration_manager()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false, true);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            // ReSharper disable once UnusedVariable
            var ignored = mca.IsConnected;

            mockDriver.Received(2).SetFineGainOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialFineGain);
        }

        [TestMethod]
        public void And_the_the_MCA_is_still_disconnected_then_settings_are_not_updated()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false);
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            // ReSharper disable once UnusedVariable
            var ignored = mca.IsConnected;

            mockDriver.DidNotReceive().SetNoiseLevelOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialNoiseLevel);
            mockDriver.DidNotReceive().SetLowerLevelDiscriminatorOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialLowerLevelDiscriminator);
            mockDriver.DidNotReceive().SetFineGainOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialFineGain);
            mockDriver.DidNotReceive().SetHighVoltageOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.InitialHighVoltage);
        }
    }
    // ReSharper restore InconsistentNaming
}
