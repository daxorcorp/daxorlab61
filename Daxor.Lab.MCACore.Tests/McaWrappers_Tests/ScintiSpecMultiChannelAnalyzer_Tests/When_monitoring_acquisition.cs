using System;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_monitoring_acquisition
    {
        [TestMethod]
        public void Then_a_1_second_delay_occurs_before_asking_the_driver_if_acquisition_has_completed()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mockThreadWrapper = Substitute.For<IThreadWrapper>();
            var mca = (ScintiSpecMcaWithExposedMembers) ScintiSpecMcaTestHelpers.CreateMcaWithDriverAndThreadWrapper(mockDriver, mockThreadWrapper, true);

            mca.InvokeMonitorAcquisition();

            Received.InOrder(() =>
            {
                mockThreadWrapper.Sleep(1000);
                mockDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber);
            });
        }

        [TestMethod]
        public void And_acquisition_was_started_initially_then_the_AcquisitionStarted_event_is_raised_with_a_correct_start_time()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true, false);
            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver, true);

            var eventTime = DateTime.Now;
            var wasEventRaised = false;
            mca.AcquisitionStarted += (sender, time) =>
            {
                wasEventRaised = true;
                eventTime = time;
            };
            var startTime = DateTime.Now;

            mca.InvokeMonitorAcquisition();

            Assert.IsTrue(wasEventRaised);
            Assert.IsTrue(Math.Abs((startTime - eventTime).TotalMilliseconds) < 500);
        }

        [TestMethod]
        public void And_acquisition_was_started_initially_then_the_AcquisitionStarted_event_is_only_raised_once()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true, true, false);
            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver, true);

            var eventCount = 0;
            mca.AcquisitionStarted += (sender, time) => { eventCount++; };

            mca.InvokeMonitorAcquisition();

            Assert.AreEqual(1, eventCount);
        }

        [TestMethod]
        public void And_acquisition_was_not_started_then_no_AcquisitionStarted_event_is_raised()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(false);
            var mca = (ScintiSpecMcaWithExposedMembers) ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver, true);

            var wasEventRaised = false;
            mca.AcquisitionStarted += (sender, time) => { wasEventRaised = true; };

            mca.InvokeMonitorAcquisition();

            Assert.IsFalse(wasEventRaised);
        }

        [TestMethod]
        public void And_acquisition_is_in_progress_then_1_second_elapses_between_each_acquisition_status_check()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            mockDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true, true, false);
            var mockThreadWrapper = Substitute.For<IThreadWrapper>();
            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriverAndThreadWrapper(mockDriver, mockThreadWrapper, true);

            mca.InvokeMonitorAcquisition();

            Received.InOrder(() =>
            {
                mockThreadWrapper.Sleep(1000);
                mockDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber);
                mockThreadWrapper.Sleep(1000);
                mockDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber);
                mockThreadWrapper.Sleep(1000);
                mockDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber);
            });

            mockThreadWrapper.Received(3).Sleep(1000);
            mockDriver.Received(3).IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber);
        }

        [TestMethod]
        public void And_acquisition_completed_and_is_no_longer_active_then_the_AcquisitionCompleted_event_is_raised()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true, false);
            stubDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            stubDriver.GetPresetStateOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(ScintiSpecPresetState.Reached);
            stubDriver.GetSpectrumOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(new uint[3]);
            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver, true);

            var wasEventRaised = false;
            mca.AcquisitionCompleted += (sender, spectrumData) => { wasEventRaised = true; };

            mca.InvokeMonitorAcquisition();

            Assert.IsTrue(wasEventRaised);
        }

        [TestMethod]
        public void And_acquisition_completed_and_is_no_longer_active_then_the_AcquisitionCompleted_event_has_the_correct_spectrum_data()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true, false);
            stubDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            stubDriver.GetPresetStateOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(ScintiSpecPresetState.Reached);

            var expectedSpectrumArray = new uint[3];
            stubDriver.GetSpectrumOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(expectedSpectrumArray);
            stubDriver.GetRealTimeInMicrosecondsOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(3000);
            stubDriver.GetLiveTimeInMicrosecondsOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(2500);
            stubDriver.GetDeadTimeInMicrosecondsOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(500);

            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver, true);

            SpectrumData observedSpectrumData = null;
            mca.AcquisitionCompleted += (sender, spectrumData) => { observedSpectrumData = spectrumData; };

            mca.InvokeMonitorAcquisition();

            Assert.AreEqual(expectedSpectrumArray, observedSpectrumData.SpectrumArray);
            Assert.AreEqual(3000, observedSpectrumData.RealTimeInMicroseconds);
            Assert.AreEqual(2500, observedSpectrumData.LiveTimeInMicroseconds);
            Assert.AreEqual(500, observedSpectrumData.DeadTimeInMicroseconds);
        }

        [TestMethod]
        public void And_acquisition_did_not_complete_and_is_no_longer_active_then_the_AcquisitionStopped_event_is_raised()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.IsAcquiringOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true, false);
            stubDriver.IsConnectedOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);
            stubDriver.GetPresetStateOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(ScintiSpecPresetState.NotReached);
            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver, true);

            var eventTime = DateTime.Now;
            var wasEventRaised = false;
            mca.AcquisitionStopped += (sender, time) =>
            {
                wasEventRaised = true;
                eventTime = time;
            };
            var stopTime = DateTime.Now;

            mca.InvokeMonitorAcquisition();

            Assert.IsTrue(wasEventRaised);
            Assert.IsTrue(Math.Abs((stopTime - eventTime).TotalMilliseconds) < 500);
        }
    }
    // ReSharper restore InconsistentNaming
}
