using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_acquisition_has_completed
    {
        [TestMethod]
        public void And_acquisition_was_in_progress_and_the_driver_says_acquisition_is_complete_then_acquisition_has_completed()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetPresetStateOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(ScintiSpecPresetState.Reached);

            var mca = (ScintiSpecMcaWithExposedMembers) ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver, true);
            mca.ExposedHasAcquisitionCompleted = false;

            Assert.IsTrue(mca.HasAcquisitionCompleted);
        }

        [TestMethod]
        public void And_acquisition_was_not_in_progress_and_the_driver_says_acquisition_is_complete_then_acquisition_has_completed()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetPresetStateOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(ScintiSpecPresetState.Reached);

            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver, true);
            mca.ExposedHasAcquisitionCompleted = true;

            Assert.IsTrue(mca.HasAcquisitionCompleted);
        }

        [TestMethod]
        public void And_acquisition_was_in_progress_and_the_driver_says_acquisition_is_not_complete_then_acquisition_has_not_completed()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetPresetStateOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(ScintiSpecPresetState.NotReached);

            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver, true);
            mca.ExposedHasAcquisitionCompleted = false;

            Assert.IsFalse(mca.HasAcquisitionCompleted);
        }

        [TestMethod]
        public void And_acquisition_was_not_in_progress_and_the_driver_says_acquisition_is_not_complete_then_acquisition_is_still_completed()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetPresetStateOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(ScintiSpecPresetState.NotReached);

            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver, true);
            mca.ExposedHasAcquisitionCompleted = true;

            Assert.IsTrue(mca.HasAcquisitionCompleted);
        }

        [TestMethod]
        public void And_a_second_acquisition_is_in_progress_after_a_first_acquisition_did_complete_then_acquisition_has_not_completed()
        {
            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetPresetStateOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(ScintiSpecPresetState.Reached);
            stubDriver.StartAcquisitionOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(true);

            var mca = (ScintiSpecMcaWithExposedMembers)ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver, true);
            mca.StartAcquisition();
            Assert.IsTrue(mca.HasAcquisitionCompleted);
            Assert.IsTrue(mca.ExposedHasAcquisitionCompleted);

            mca.StartAcquisition();
            Assert.IsFalse(mca.ExposedHasAcquisitionCompleted);
        }
    }
    // ReSharper restore InconsistentNaming
}
