using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_ADC_high_voltage
    {
        [TestMethod]
        public void Then_the_analog_to_digital_high_voltage_is_obtained_from_the_driver()
        {
            const int expectedAdcHighVoltage = 1037;

            var stubDriver = Substitute.For<IScintiSpecDriver>();
            stubDriver.GetAnalogToDigitalConverterHighVoltageOnPort(ScintiSpecMcaTestHelpers.PortNumber).Returns(expectedAdcHighVoltage);

            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(stubDriver);

            Assert.AreEqual(expectedAdcHighVoltage, mca.AnalogToDigitalConverterHighVoltage);
        }
    }
    // ReSharper restore InconsistentNaming
}
