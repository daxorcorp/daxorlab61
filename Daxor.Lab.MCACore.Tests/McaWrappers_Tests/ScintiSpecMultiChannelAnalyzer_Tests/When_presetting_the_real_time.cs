using System;
using Daxor.Lab.MCACore.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_presetting_the_real_time
    {
        [TestMethod]
        public void Then_the_real_time_is_preset_via_the_driver()
        {
            const Int64 realTimeInSeconds = 49356;

            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.PresetRealTime(realTimeInSeconds);

            mockDriver.Received().PresetRealTimeOnPort(ScintiSpecMcaTestHelpers.PortNumber, realTimeInSeconds);
        }
    }
    // ReSharper restore InconsistentNaming
}
