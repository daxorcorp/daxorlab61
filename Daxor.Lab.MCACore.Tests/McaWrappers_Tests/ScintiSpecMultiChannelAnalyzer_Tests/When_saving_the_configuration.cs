using Daxor.Lab.HardwareConfigurationManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_saving_the_configuration
    {
        [TestMethod]
        public void Then_the_hardware_configuration_manager_is_asked_to_reset_to_defaults()
        {
            var mockConfigurationManager = Substitute.For<IHardwareConfigurationManager>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithHardwareConfigurationManager(mockConfigurationManager);

            mca.SaveConfiguration();

            mockConfigurationManager.Received().SaveConfiguration();
        }
    }
    // ReSharper restore InconsistentNaming
}
