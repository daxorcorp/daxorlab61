using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecMultiChannelAnalyzer_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_fine_gain
    {
        private const double FineGainToSet = 1.098;

        [TestMethod]
        public void Then_the_fine_gain_is_set_via_the_driver()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.FineGain = FineGainToSet;

            mockDriver.Received().SetFineGainOnPort(ScintiSpecMcaTestHelpers.PortNumber, FineGainToSet);
        }

        [TestMethod]
        public void Then_the_hardware_configuration_manager_is_updated_with_the_new_fine_gain()
        {
            var stubHardwareConfigurationManager = Substitute.For<IHardwareConfigurationManager>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithHardwareConfigurationManager(stubHardwareConfigurationManager);

            mca.FineGain = FineGainToSet;

            Assert.AreEqual(FineGainToSet, stubHardwareConfigurationManager.GetSetting(ScintiSpecSpecification.FineGainSettingKey).GetValue<double>());
        }

        [TestMethod]
        public void Then_the_minimum_bound_is_inclusive()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.FineGain = ScintiSpecMcaTestHelpers.MinFineGain;

            mockDriver.Received().SetFineGainOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.MinFineGain);
        }

        [TestMethod]
        public void Then_the_maximum_bound_is_inclusive()
        {
            var mockDriver = Substitute.For<IScintiSpecDriver>();
            var mca = ScintiSpecMcaTestHelpers.CreateMcaWithDriver(mockDriver);

            mca.FineGain = ScintiSpecMcaTestHelpers.MaxFineGain;

            mockDriver.Received().SetFineGainOnPort(ScintiSpecMcaTestHelpers.PortNumber, ScintiSpecMcaTestHelpers.MaxFineGain);
        }
    }
    // ReSharper restore InconsistentNaming
}
