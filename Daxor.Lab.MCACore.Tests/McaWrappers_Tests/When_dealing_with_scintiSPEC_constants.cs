using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_dealing_with_scintiSPEC_constants
    {
        [TestMethod]
        public void Then_the_spectrum_length_is_1024_channels()
        {
            Assert.AreEqual(1024, ScintiSpecDllWrapper.SpectrumLength);
        }

        [TestMethod]
        public void Then_the_fine_gain_normalization_factor_is_32768()
        {
            Assert.AreEqual(32768, ScintiSpecDllWrapper.FineGainNormalizationFactor);
        }

        [TestMethod]
        public void Then_the_number_of_microseconds_in_one_second_is_1_million()
        {
            Assert.AreEqual(1000000, ScintiSpecDllWrapper.MicrosecondsPersecond);
        }
    }
    // ReSharper restore InconsistentNaming
}
