using System;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_starting_acquisition
    {
        private ScintiSpecDriver _scintiSpecDriver;

        [TestInitialize]
        public void BeforeEachTest()
        {
            var stubDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            stubDllWrapper.InvokeReturningBool(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecBool { ReturnValue = false, ShouldThrowException = true });
            _scintiSpecDriver = new ScintiSpecDriver(stubDllWrapper);
        }

        [TestMethod]
        public void And_using_a_single_MCA_and_a_driver_error_occurred_then_an_exception_is_thrown()
        {
            AssertEx.Throws<Exception>(() => _scintiSpecDriver.StartAcquisitionOnPort(4));
        }

        [TestMethod]
        public void And_using_all_MCAs_and_a_driver_error_occurred_then_an_exception_is_thrown()
        {
            AssertEx.Throws<Exception>(() => _scintiSpecDriver.StartAcquisitionOnAllMcas());
        }
    }
    // ReSharper restore InconsistentNaming
}
