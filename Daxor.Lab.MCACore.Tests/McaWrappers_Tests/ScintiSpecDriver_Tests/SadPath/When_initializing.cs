using System;
using Daxor.Lab.MCACore.McaWrappers;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        [TestMethod]
        public void And_the_scintiSPEC_DLL_wrapper_instance_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentNullException>(() => new ScintiSpecDriver(null));
        }
    }
    // ReSharper restore InconsistentNaming
}
