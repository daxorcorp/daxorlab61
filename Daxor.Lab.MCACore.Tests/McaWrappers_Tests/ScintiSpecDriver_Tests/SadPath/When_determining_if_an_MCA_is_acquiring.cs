using System;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_an_MCA_is_acquiring
    {
        [TestMethod]
        public void And_a_driver_error_occurred_then_an_exception_is_thrown()
        {
            var stubDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            stubDllWrapper.InvokeReturningBool(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecBool { ReturnValue = false, ShouldThrowException = true });
            var scintiSpecDriver = new ScintiSpecDriver(stubDllWrapper);

            AssertEx.Throws<Exception>(() => scintiSpecDriver.IsAcquiringOnPort(4));
        }
    }
    // ReSharper restore InconsistentNaming
}
