using System;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_stopping_acquisition
    {
        private ScintiSpecDriver _driver;

        [TestInitialize]
        public void BeforeEachTest()
        {
            var stubDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            stubDllWrapper.InvokeReturningBool(0, 4, 1, 0, 0, 0, 0, 1, 10, 0, 0, default(byte)).Returns(new ScintiSpecBool { ReturnValue = true });
            _driver = new ScintiSpecDriver(stubDllWrapper);
            stubDllWrapper.InvokeReturningVoid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecVoid { ShouldThrowException = true });   
        }

        [TestMethod]
        public void And_using_single_MCA_and_a_driver_error_occurred_then_an_exception_is_thrown()
        {
            AssertEx.Throws<Exception>(() => _driver.StopAcquisitionOnPort(4));
        }

        [TestMethod]
        public void And_using_all_MCAS_and_a_driver_error_occurred_then_an_exception_is_thrown()
        {
            AssertEx.Throws<Exception>(() => _driver.StopAcquisitionOnAllMcas());
        }
    }
    // ReSharper restore InconsistentNaming
}
