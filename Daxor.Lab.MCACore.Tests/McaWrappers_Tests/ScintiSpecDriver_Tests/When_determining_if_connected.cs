using System;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_connected
    {
        private const int PortNumber = 4;

        private IScintiSpecDllWrapper _mockDllWrapper;
        private ScintiSpecDriver _scintiSpecDriver;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            _scintiSpecDriver = new ScintiSpecDriver(_mockDllWrapper);
        }

        [TestMethod]
        public void Then_the_DLL_is_invoked_correctly()
        {
            _scintiSpecDriver.IsConnectedOnPort(PortNumber);

            _mockDllWrapper.Received().InvokeReturningBool(0, PortNumber, 0, 0, 0, 0, 0, 0, 511, 0, 0, default(byte));
        }

        [TestMethod]
        public void And_the_MCA_is_connected_then_true_is_returned()
        {
            _mockDllWrapper.InvokeReturningBool(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecBool { ReturnValue = true, ShouldThrowException = false });

            var result = _scintiSpecDriver.IsConnectedOnPort(PortNumber);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void And_the_MCA_is_disconnected_then_false_is_returned()
        {
            _mockDllWrapper.InvokeReturningBool(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecBool { ReturnValue = false, ShouldThrowException = false });

            var result = _scintiSpecDriver.IsConnectedOnPort(PortNumber);

            Assert.IsFalse(result);
        }

        [TestMethod]
        public void And_a_driver_error_occurred_then_nothing_happens()
        {
            var stubDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            stubDllWrapper.InvokeReturningBool(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecBool { ReturnValue = false, ShouldThrowException = true });
            var scintiSpecDriver = new ScintiSpecDriver(stubDllWrapper);

            AssertEx.DoesNotThrow(() => scintiSpecDriver.IsConnectedOnPort(PortNumber));
        }
    }
    // ReSharper restore InconsistentNaming
}
