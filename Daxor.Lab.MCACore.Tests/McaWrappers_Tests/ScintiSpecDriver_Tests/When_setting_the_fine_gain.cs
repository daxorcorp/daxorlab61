using System;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_fine_gain
    {
        private const int PortNumber = 4;

        private IScintiSpecDllWrapper _mockDllWrapper;
        private ScintiSpecDriver _scintiSpecDriver;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            _scintiSpecDriver = new ScintiSpecDriver(_mockDllWrapper);
        }

        [TestMethod]
        public void Then_the_DLL_is_invoked_correctly()
        {
            _scintiSpecDriver.SetFineGainOnPort(PortNumber, 1.000);

            _mockDllWrapper.Received().InvokeReturningVoid(0, PortNumber, 0, 0, 0, 0, 0, ScintiSpecDllWrapper.FineGainNormalizationFactor, 15, 0, 0, default(byte));
        }

        [TestMethod]
        public void Then_the_fine_gain_is_normalized_correctly()
        {
            _mockDllWrapper.InvokeReturningVoid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecVoid { ShouldThrowException = false });

            _scintiSpecDriver.SetFineGainOnPort(PortNumber, 0);
            _scintiSpecDriver.SetFineGainOnPort(PortNumber, 0.754);
            _scintiSpecDriver.SetFineGainOnPort(PortNumber, 1.056);
            
            _mockDllWrapper.Received().InvokeReturningVoid(0, PortNumber, 0, 0, 0, 0, 0, 0, 15, 0, 0, default(byte));
            _mockDllWrapper.Received().InvokeReturningVoid(0, PortNumber, 0, 0, 0, 0, 0, (Int64) (ScintiSpecDllWrapper.FineGainNormalizationFactor * 0.754), 15, 0, 0, default(byte));
            _mockDllWrapper.Received().InvokeReturningVoid(0, PortNumber, 0, 0, 0, 0, 0, (Int64) (ScintiSpecDllWrapper.FineGainNormalizationFactor * 1.056), 15, 0, 0, default(byte));
        }

        [TestMethod]
        public void And_a_driver_error_occurred_then_no_exception_is_thrown()
        {
            _mockDllWrapper.InvokeReturningVoid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecVoid { ShouldThrowException = true });

            AssertEx.DoesNotThrow(() => _scintiSpecDriver.SetFineGainOnPort(PortNumber, 4.567));
        } 
    }
    // ReSharper restore InconsistentNaming
}
