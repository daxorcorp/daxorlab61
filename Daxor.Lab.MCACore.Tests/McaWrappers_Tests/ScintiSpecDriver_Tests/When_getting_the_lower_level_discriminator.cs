using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_lower_level_discriminator
    {
        private const int PortNumber = 4;

        private IScintiSpecDllWrapper _mockDllWrapper;
        private ScintiSpecDriver _scintiSpecDriver;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            _scintiSpecDriver = new ScintiSpecDriver(_mockDllWrapper);
        }

        [TestMethod]
        public void Then_the_DLL_is_invoked_correctly()
        {
            _scintiSpecDriver.GetLowerLevelDiscriminatorOnPort(PortNumber);

            _mockDllWrapper.Received().InvokeReturningInt(0, PortNumber, 0, 0, 0, 0, 0, 0, 101, 0, 0, default(byte));
        }

        [TestMethod]
        public void Then_the_correct_lower_level_discriminator_is_returned()
        {
            _mockDllWrapper.InvokeReturningInt(0, PortNumber, 0, 0, 0, 0, 0, 0, 101, 0, 0, default(byte)).Returns(27);

            Assert.AreEqual(27, _scintiSpecDriver.GetLowerLevelDiscriminatorOnPort(PortNumber));
        }
    }
    // ReSharper restore InconsistentNaming
}
