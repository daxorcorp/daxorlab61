using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_an_MCA_is_acquiring
    {
        private const int PortNumber = 4;

        private IScintiSpecDllWrapper _mockDllWrapper;
        private ScintiSpecDriver _scintiSpecDriver;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            _scintiSpecDriver = new ScintiSpecDriver(_mockDllWrapper);
        }

        [TestMethod]
        public void Then_the_DLL_is_invoked_correctly()
        {
            _scintiSpecDriver.IsAcquiringOnPort(PortNumber);

            _mockDllWrapper.Received().InvokeReturningBool(0, PortNumber, 1, 0, 0, 0, 0, 1, 10, 0, 0, default(byte));
        }

        [TestMethod]
        public void And_the_MCA_is_acquiring_then_true_is_returned()
        {
            _mockDllWrapper.InvokeReturningBool(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecBool { ReturnValue = true, ShouldThrowException = false });

            var result = _scintiSpecDriver.IsAcquiringOnPort(PortNumber);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void And_the_MCA_is_not_acquiring_then_false_is_returned()
        {
            _mockDllWrapper.InvokeReturningBool(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecBool { ReturnValue = false, ShouldThrowException = false });

            var result = _scintiSpecDriver.IsAcquiringOnPort(PortNumber);

            Assert.IsFalse(result);
        }
    }
    // ReSharper restore InconsistentNaming
}
