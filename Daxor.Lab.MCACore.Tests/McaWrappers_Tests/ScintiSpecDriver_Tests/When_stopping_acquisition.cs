using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_stopping_acquisition
    {
        private const int PortNumber = 4;

        private IScintiSpecDllWrapper _mockDllWrapper;
        private ScintiSpecDriver _scintiSpecDriver;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            _scintiSpecDriver = new ScintiSpecDriver(_mockDllWrapper);
        }

        [TestMethod]
        public void And_using_a_single_MCA_that_is_currently_acquiring_then_the_DLL_is_invoked_correctly()
        {
            _mockDllWrapper.InvokeReturningBool(0, PortNumber, 1, 0, 0, 0, 0, 1, 10, 0, 0, default(byte)).Returns(new ScintiSpecBool { ReturnValue = true});
            
            _scintiSpecDriver.StopAcquisitionOnPort(PortNumber);

            _mockDllWrapper.Received().InvokeReturningVoid(0, PortNumber, 0, 0, 0, 0, 1, 0, 9, 0, 0, default(byte));
        }

        [TestMethod]
        public void And_using_a_single_MCA_that_is_not_currently_acquiring_then_the_DLL_is_not_invoked()
        {
            _mockDllWrapper.InvokeReturningBool(0, PortNumber, 1, 0, 0, 0, 0, 1, 10, 0, 0, default(byte)).Returns(new ScintiSpecBool { ReturnValue = false });

            _scintiSpecDriver.StopAcquisitionOnPort(PortNumber);

            _mockDllWrapper.DidNotReceive().InvokeReturningVoid(0, PortNumber, 0, 0, 0, 0, 1, 0, 9, 0, 0, default(byte));
        }

        [TestMethod]
        public void And_using_all_MCAs_then_the_DLL_is_invoked_correctly()
        {
            _scintiSpecDriver.StopAcquisitionOnAllMcas();

            _mockDllWrapper.Received().InvokeReturningVoid(0, 1, 1, 0, 0, 0, 1, 0, 256, 0, 0, default(byte));
        }
    }
    // ReSharper restore InconsistentNaming
}
