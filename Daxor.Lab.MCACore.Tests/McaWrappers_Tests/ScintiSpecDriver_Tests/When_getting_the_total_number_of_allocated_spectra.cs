using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_total_number_of_allocated_spectra
    {
        private IScintiSpecDllWrapper _mockDllWrapper;
        private ScintiSpecDriver _scintiSpecDriver;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            _scintiSpecDriver = new ScintiSpecDriver(_mockDllWrapper);
        }

        [TestMethod]
        public void Then_the_DLL_is_invoked_correctly()
        {
            _scintiSpecDriver.GetTotalNumberOfAllocatedSpectra();

            _mockDllWrapper.Received().InvokeReturningInt(0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, default(byte));
        }

        [TestMethod]
        public void Then_the_total_number_of_allocated_spectra_is_returned()
        {
            _mockDllWrapper.InvokeReturningInt(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(7);

            var observedNumber = _scintiSpecDriver.GetTotalNumberOfAllocatedSpectra();

            Assert.AreEqual(7, observedNumber);
        }
    }
    // ReSharper restore InconsistentNaming
}
