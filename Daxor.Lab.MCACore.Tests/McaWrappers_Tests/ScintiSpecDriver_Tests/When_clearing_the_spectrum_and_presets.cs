using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_clearing_the_spectrum_and_presets
    {
        private const int PortNumber = 4;

        private IScintiSpecDllWrapper _mockDllWrapper;
        private ScintiSpecDriver _scintiSpecDriver;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            _scintiSpecDriver = new ScintiSpecDriver(_mockDllWrapper);
        }

        [TestMethod]
        public void Then_the_DLL_is_invoked_correctly()
        {
            _scintiSpecDriver.ClearSpectrumAndPresetsOnPort(4);

            _mockDllWrapper.Received().InvokeReturningVoid(0, PortNumber, 0, 0, 0, 0, 0, 0, 30, 0, 0, default(byte));
        }

        [TestMethod]
        public void And_a_driver_error_occurred_then_no_exception_is_thrown()
        {
            _mockDllWrapper.InvokeReturningVoid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecVoid { ShouldThrowException = true });

            AssertEx.DoesNotThrow(() => _scintiSpecDriver.ClearSpectrumAndPresetsOnPort(4));
        }
    }
    // ReSharper restore InconsistentNaming
}
