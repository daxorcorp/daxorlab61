using System;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_spectrum
    {
        private const int PortNumber = 4;

        private IScintiSpecDllWrapper _mockDllWrapper;
        private ScintiSpecDriver _scintiSpecDriver;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            _scintiSpecDriver = new ScintiSpecDriver(_mockDllWrapper);
        }

        [TestMethod]
        public void Then_the_DLL_is_invoked_correctly()
        {
            _scintiSpecDriver.GetSpectrumOnPort(PortNumber);

            _mockDllWrapper.Received().InvokeReturningIntArray(0, PortNumber, 0, 0, 0, 0, 0, 25, 0, 0, default(byte));
        }

        [TestMethod]
        public void Then_the_spectrum_array_is_returned()
        {
            var expectedArray = new uint[10];
            _mockDllWrapper.InvokeReturningIntArray(0, PortNumber, 0, 0, 0, 0, 0, 25, 0, 0, default(byte)).Returns(expectedArray);
            var observedArray = _scintiSpecDriver.GetSpectrumOnPort(PortNumber);

            Assert.AreEqual(expectedArray, observedArray);
        }

        [TestMethod]
        public void And_the_spectrum_array_from_the_DLL_is_null_then_null_is_returned()
        {
            _mockDllWrapper.InvokeReturningIntArray(0, PortNumber, 0, 0, 0, 0, 0, 25, 0, 0, default(byte)).Returns((UInt32[]) null);
            var observedArray = _scintiSpecDriver.GetSpectrumOnPort(PortNumber);

            Assert.IsNull(observedArray);
        }
    }
    // ReSharper restore InconsistentNaming
}
