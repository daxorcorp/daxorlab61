using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_fine_gain
    {
        private const int PortNumber = 4;

        private IScintiSpecDllWrapper _mockDllWrapper;
        private ScintiSpecDriver _scintiSpecDriver;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            _scintiSpecDriver = new ScintiSpecDriver(_mockDllWrapper);
        }

        [TestMethod]
        public void Then_the_DLL_is_invoked_correctly()
        {
            _scintiSpecDriver.GetFineGainOnPort(PortNumber);

            _mockDllWrapper.Received().InvokeReturningInt(0, PortNumber, 0, 0, 0, 0, 0, 0, 16, 0, 0, default(byte));
        }

        [TestMethod]
        public void Then_the_correct_fine_gain_is_returned()
        {
            _mockDllWrapper.InvokeReturningInt(0, PortNumber, 0, 0, 0, 0, 0, 0, 16, 0, 0, default(byte)).Returns(32768, 24707, 34603, 34583);

            Assert.AreEqual(1.000, _scintiSpecDriver.GetFineGainOnPort(PortNumber));
            Assert.AreEqual(0.754, _scintiSpecDriver.GetFineGainOnPort(PortNumber));
            Assert.AreEqual(1.056, _scintiSpecDriver.GetFineGainOnPort(PortNumber));
            Assert.AreEqual(1.055, _scintiSpecDriver.GetFineGainOnPort(PortNumber));
        }
    }
    // ReSharper restore InconsistentNaming
}
