using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_starting_acquisition
    {
        private const int PortNumber = 4;

        private IScintiSpecDllWrapper _mockDllWrapper;
        private ScintiSpecDriver _scintiSpecDriver;
        
        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            _scintiSpecDriver = new ScintiSpecDriver(_mockDllWrapper);
        }

        [TestMethod]
        public void And_using_a_single_MCA_then_the_DLL_is_invoked_correctly()
        {
            _scintiSpecDriver.StartAcquisitionOnPort(PortNumber);

            _mockDllWrapper.Received().InvokeReturningBool(0, PortNumber, 0, 0, 0, 0, 1, 1, 9, 0, 0, default(byte));
        }

        [TestMethod]
        public void And_using_a_single_MCA_and_the_acquisition_started_successfully_then_true_is_returned()
        {
            _mockDllWrapper.InvokeReturningBool(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecBool { ReturnValue = true, ShouldThrowException = false });

            var started = _scintiSpecDriver.StartAcquisitionOnPort(4);

            Assert.IsTrue(started);
        }

        [TestMethod]
        public void And_using_a_single_MCA_and_the_acquisition_did_not_start_successfully_then_false_is_returned()
        {
            _mockDllWrapper.InvokeReturningBool(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecBool { ReturnValue = false, ShouldThrowException = false });

            var started = _scintiSpecDriver.StartAcquisitionOnPort(4);

            Assert.IsFalse(started);
        }

        [TestMethod]
        public void And_using_all_MCAs_then_the_DLL_is_invoked_correctly()
        {
            _scintiSpecDriver.StartAcquisitionOnAllMcas();

            _mockDllWrapper.Received().InvokeReturningBool(0, 1, 1, 0, 0, 0, 1, 1, 256, 0, 0, default(byte));
        }

        [TestMethod]
        public void And_using_all_MCAs_and_the_acquisition_started_successfully_then_true_is_returned()
        {
            _mockDllWrapper.InvokeReturningBool(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecBool { ReturnValue = true, ShouldThrowException = false });

            var started = _scintiSpecDriver.StartAcquisitionOnAllMcas();

            Assert.IsTrue(started);
        }

        [TestMethod]
        public void And_using_all_MCAs_and_the_acquisition_did_not_start_successfully_then_false_is_returned()
        {
            _mockDllWrapper.InvokeReturningBool(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0).ReturnsForAnyArgs(
                new ScintiSpecBool { ReturnValue = false, ShouldThrowException = false });

            var started = _scintiSpecDriver.StartAcquisitionOnAllMcas();

            Assert.IsFalse(started);
        }
    }
    // ReSharper restore InconsistentNaming
}
