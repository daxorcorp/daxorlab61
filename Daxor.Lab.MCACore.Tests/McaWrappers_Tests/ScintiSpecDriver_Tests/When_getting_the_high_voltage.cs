using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCACore.McaWrappers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCACore.Tests.McaWrappers_Tests.ScintiSpecDriver_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_high_voltage
    {
        private const int PortNumber = 4;

        private IScintiSpecDllWrapper _mockDllWrapper;
        private ScintiSpecDriver _scintiSpecDriver;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockDllWrapper = Substitute.For<IScintiSpecDllWrapper>();
            _scintiSpecDriver = new ScintiSpecDriver(_mockDllWrapper);
        }

        [TestMethod]
        public void Then_the_DLL_is_invoked_correctly()
        {
            _scintiSpecDriver.GetHighVoltageOnPort(PortNumber);

            _mockDllWrapper.Received().InvokeReturningInt(0, PortNumber, 0, 0, 0, 0, 0, 0, 270, 0, 0, default(byte));
        }

        [TestMethod]
        public void Then_the_correct_high_voltage_is_returned()
        {
            _mockDllWrapper.InvokeReturningInt(0, PortNumber, 0, 0, 0, 0, 0, 0, 270, 0, 0, default(byte)).Returns(850);

            Assert.AreEqual(850, _scintiSpecDriver.GetHighVoltageOnPort(PortNumber));
        }
    }
    // ReSharper restore InconsistentNaming
}
