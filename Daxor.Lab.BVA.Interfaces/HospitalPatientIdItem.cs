﻿using System;

namespace Daxor.Lab.BVA.Interfaces
{
    /// <summary>
    /// Represents an association of a patient's internal ID and the hosptial's patient ID.
    /// </summary>
    public class HospitalPatientIdItem
    {
        /// <summary>
        /// Creates a new HospitalPatientIdItem for the given patient ID and hospital patient ID.
        /// </summary>
        /// <param name="patientId">Patient ID (used internally)</param>
        /// <param name="hospitalPatientId">Hospital patient ID (ID for the patient assigned by the hospital)</param>
        public HospitalPatientIdItem(Guid patientId, string hospitalPatientId)
        {
            PatientId = patientId;
            HospitalPatientId = hospitalPatientId;
        }

        /// <summary>
        /// Gets or sets the patient ID.
        /// </summary>
        public Guid PatientId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the hospital patient ID.
        /// </summary>
        public string HospitalPatientId
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates a string version of this item
        /// </summary>
        /// <remarks>Overriding ToString() for binding purposes</remarks>
        /// <returns>Hospital ID for this item</returns>
        public override string ToString()
        {
            return HospitalPatientId ?? String.Empty;
        }
    }
}
