﻿using System;
using Daxor.Lab.Domain.Entities.Base;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.Constants;

namespace Daxor.Lab.BVA.Interfaces
{
    /// <summary>
    /// Test item display 
    /// </summary>
    public class BVATestItem : ObservableObject
    {
        #region Fields

        private BVATest _test;
        private Guid _testId;
        private DateTime _testDate;
        private DateTime? _patientDateOfBirth;
        private string _testId2 = String.Empty;
        private string _patientHospitalId = String.Empty;
        private Guid _patientPid;
        private string _patientFullName = String.Empty;
        private TestStatus _testStatus = TestStatus.Pending;
        private string _testModeDescription;
	    private bool _amputeeStatus;
        private bool _hasSufficientData;
        private bool _wasSaved;
        private string _testStatusDescription;
        private string _testDateAsString;
        private string _patientDateOfBirthAsString;

        #endregion
   
        #region Ctors
        
        public BVATestItem() { }
     
        public BVATestItem(BVATest test)
        {
            _test = test;
            CreatedDate = _test.Date;
            Status = _test.Status;
	        AmputeeStatus = _test.Patient.IsAmputee;
            TestModeDescription = _test.Mode.ToString();
        }

        #endregion

        #region Properties

        public bool WasSaved
        {
            get { return _wasSaved; }
            set { base.SetValue(ref _wasSaved, "WasSaved", value); }
        }

        public Guid TestID
        {
            get { return _testId; }
            set { base.SetValue(ref _testId, "TestID", value); }
        }

        public Guid PatientPID
        {
            get { return _patientPid; }
            set { base.SetValue(ref _patientPid, "PatientPID", value); }
        }

        public DateTime CreatedDate
        {
            get { return _testDate; }
            set
            {
                CreatedDateAsString = value.ToString("MM/dd/yyyy h:mmt");
                base.SetValue(ref _testDate, "CreatedDate", value);
            }
        }

	    public bool AmputeeStatus
	    {
			get { return _amputeeStatus; }
		    set { SetValue(ref _amputeeStatus, "AmputeeStatus", value); }
	    }

        public DateTime? PatientDOB
        {
            get { return _patientDateOfBirth; }
            set
            {
                DateTime newDateOfBirth;
                if (value != null && DateTime.TryParse(value.ToString(), out newDateOfBirth) &&
                    !newDateOfBirth.ToShortDateString().Equals(DomainConstants.NullDateTime))
                    PatientDOBAsString = value.Value.ToString("MM/dd/yyyy");
                else
                    PatientDOBAsString = string.Empty;
                base.SetValue(ref _patientDateOfBirth, "PatientDOB", value);
            }
        }

        public String CreatedDateAsString
        {
            get { return _testDateAsString; }
            set { base.SetValue(ref _testDateAsString, "CreatedDateAsString", value); }
        }

        public String PatientDOBAsString
        {
            get { return _patientDateOfBirthAsString; }
            set { base.SetValue(ref _patientDateOfBirthAsString, "PatientDOBAsString", value); }
        }

        public string PatientFullName
        {
            get { return _patientFullName; }
            set { base.SetValue(ref _patientFullName, "PatientFullName", value); }
        }

        public string TestID2
        {
            get { return _testId2; }
            set { base.SetValue(ref _testId2, "TestID2", value); }
        }

        public string PatientHospitalID
        {
            get { return _patientHospitalId; }
            set { base.SetValue(ref _patientHospitalId, "PatientHospitalID", value); }
        }

        public TestStatus Status
        {
            get { return _testStatus; }
            set
            {
                if (base.SetValue(ref _testStatus, "Status", value))
                    TestStatusDescription = FormatStatusDescription(value, TestModeDescription, HasSufficientData);
            }
        }

        public string TestModeDescription
        {
            get { return _testModeDescription; }
            set
            {
                if (base.SetValue(ref _testModeDescription, "TestModeDescription", value))
                    TestStatusDescription = FormatStatusDescription(Status, value, HasSufficientData);
            }
        }

        public string TestStatusDescription
        {
            get { return _testStatusDescription; }
            set { base.SetValue(ref _testStatusDescription, "TestStatusDescription", value); }
        }

        public bool HasSufficientData
        {
            get { return _hasSufficientData; }
            set
            {
                if (base.SetValue(ref _hasSufficientData, "HasSufficientData", value))
                    TestStatusDescription = FormatStatusDescription(Status, TestModeDescription, value);
            }
        }

        #endregion

        #region PropertyChanged observers for patient/test

        /// <summary>
        /// Begins observing a given test.
        /// </summary>
        /// <param name="test"></param>
        public void BeginObserving(BVATest test)
        {
            if (test == null) return;

            StopObserving();

            TestID = test.InternalId;
            PatientPID = test.Patient.InternalId;

            if (_test == null || test.InternalId != _test.InternalId)
                _test = test;

            _test.PropertyChanged += OnTestPropertyChanged;

            if (_test.Patient != null)
                _test.Patient.PropertyChanged += OnPatientPropertyChanged;
        }

        /// <summary>
        /// Stops observing a given test
        /// </summary>
        public void StopObserving()
        {
            if (_test == null) return;
            
            _test.PropertyChanged -= OnTestPropertyChanged;
            if (_test.Patient != null)
                _test.Patient.PropertyChanged -= OnPatientPropertyChanged;
            _test = null;
        }

        void OnPatientPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var patient = sender as BVAPatient;
            if (patient == null)
                return;

            switch (e.PropertyName)
            {
                case "FullName": PatientFullName = patient.FullName; break;
                case "HospitalPatientId": 
                    PatientHospitalID = patient.HospitalPatientId; 
                    break;
                case "DateOfBirth": PatientDOB = patient.DateOfBirth; break;
            }
        }

        void OnTestPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var test = sender as BVATest;
            if(test == null) return;

            switch (e.PropertyName)
            {
                case "TestID2": TestID2 = test.TestID2; break;
                case "Date": CreatedDate = test.Date; break;
                case "Patient": PatientPID = test.Patient.InternalId; break;
                case "HasSufficientData":
                case "Mode":
                case "Status":
                    TestStatusDescription = FormatStatusDescription(test.Status, test.Mode.ToString(), test.HasSufficientData);
                    break;
            }
        }
       
        #endregion

        #region Static helpers

        public static string FormatStatusDescription(TestStatus status, string testModeDescription, bool hasSufficientData)
        {
            if (status == TestStatus.Running) return "In Progress";
            if (status == TestStatus.Completed)
                switch (testModeDescription)
                {
                    case "Automatic":
                    case "Training":
                        return hasSufficientData ? "Completed" : "Needs Data";
                    default:
                        return testModeDescription;
                }
            if (status == TestStatus.Pending)
                switch (testModeDescription)
                {
                    case "Automatic":
                        return status.ToString();
                    case "Training":
                        return "Pending Training";
                    case "Manual":
                        return "Pending Manual";
                    case "VOPS":
                        return "Pending VOPS";
                    default:
                        return status.ToString();
                }

            return status.ToString();
        }

        public static string FormatFullName(string lastName, string firstName, string middleName)
        {
            if (lastName != String.Empty && firstName != String.Empty)
                return (lastName + ", " + firstName + " " + middleName).Trim();

            if (lastName != String.Empty || firstName != String.Empty)
                return lastName == String.Empty ? firstName : lastName;
            
            return String.Empty;
        }

        #endregion
    }
}
