﻿using System.Collections.Generic;

namespace Daxor.Lab.BVA.Interfaces
{
    /// <summary>
    /// Defines the behavior of a service that calculates the residual standard error
    /// for a set of observed x- and y-values that correspond to a linear regression.
    /// </summary>
    public interface IResidualStandardErrorCalculator
    {
        /// <summary>
        /// Calculates the residual standard error for the given set of independent (x) values, 
        /// observed values, slope, and y-intercept.
        /// </summary>
        /// <param name="independentValues">x-values for the data points</param>
        /// <param name="observedValues">y-values for the data points</param>
        /// <param name="slope">Slope of the regression line</param>
        /// <param name="yIntercept">y-intercept of the regression line</param>
        /// <returns>Calculated residual standard error</returns>
        double CalculateResidualStandardError(IEnumerable<double> independentValues, IEnumerable<double> observedValues, double slope, double yIntercept);
    }
}
