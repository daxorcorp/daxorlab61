﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Core;

namespace Daxor.Lab.BVA.Interfaces
{
    /// <summary>
    /// BVA related data service
    /// </summary>
    public interface IBVADataService
    {
        /// <summary>
        /// Finds analysts whose name start with specified string
        /// </summary>
        /// <param name="startWithName">name prefix</param>
        /// <returns>Collection of analysts</returns>
        IQueryable<String> FindAnalysts(string startWithName);

        /// <summary>
        /// Selects patienthospital items
        /// </summary>
        /// <returns></returns>
        IQueryable<HospitalPatientIdItem> FindHospitalPatientIdItems(string startsWithHospitalId);

        /// <summary>
        /// Selects patient based on hospital ID, which is unique
        /// </summary>
        /// <param name="hospitalId">hospital id of the patient</param>
        /// <param name="concurrencyManager">Concurrency manager</param>
        /// <returns>BVA Patient</returns>
        BVAPatient SelectPatient(string hospitalId, object concurrencyManager);

        /// <summary>
        /// Select default tube
        /// </summary>
        /// <returns>Default tube</returns>
        Tube SelectDefaultTube();

        /// <summary>
        /// Selects all possible tube types
        /// </summary>
        /// <returns>Collection of tubes</returns>
        IEnumerable<Tube> SelectTubes();

        /// <summary>
        /// Selects BVATestItem(s) - shallow copy of BVATest
        /// </summary>
        /// <returns>Collection of BVATestItem</returns>
        IEnumerable<BVATestItem> SelectBVATestItems();

        BVATestItem SelectBVATestItem(Guid testId);
        /// <summary>
        /// Selects specific sample layout schema based on id
        /// </summary>
        /// <param name="schemaId">Schema ID</param>
        /// <returns>Sample layout schema</returns>
        ISampleLayoutSchema SelectLayoutSchema(Int32 schemaId);

        /// <summary>
        /// Selects default layout schema based on value in setting
        /// </summary>
        /// <returns></returns>
        ISampleLayoutSchema SelectLayoutSchema();

        /// <summary>
        /// Selects test from the database
        /// </summary>
        /// <param name="testId">Id of the test to load</param>
        /// <returns>Selected test</returns>
        BVATest SelectTest(Guid testId);

        /// <summary>
        /// Deletes test from the database
        /// </summary>
        /// <param name="testId">TestID to delete</param>
        /// <returns>True if success, false otherwise</returns>
        void DeleteTest(Guid testId);

        /// <summary>
        /// Saves test to the database
        /// </summary>
        /// <param name="test">Test to save</param>
        /// <param name="includeInjectates">True if the injectate information 
        /// should be saved (false by default)</param>
        void SaveTest(BVATest test, bool includeInjectates = false);

        /// <summary>
        /// Checks it tables exist
        /// </summary>
        /// <returns>The number of tables found in the database schema that match
        /// tblTEST_HEADER, tblBVA_TEST, tblPATIENT, and tlkpSTATUS. If not == 4 something is missing.</returns>
        bool AreBvaTestTablesAreOnline();

        /// <summary>
        /// Returns the most recent physician's specialty entered for tests existing in the database.
        /// </summary>
        /// <param name="physiciansName">The name of the physician to look for a specialty</param>
        /// <returns>the name of the physician's specialty</returns>
        string GetMostRecentSpecialty(string physiciansName);

        /// <summary>
        /// Checks the database to see if patient id has tests associated with it
        /// </summary>
        /// <param name="internalId">the patients internal id</param>
        /// <returns></returns>
        bool AreThereAnyTestsThatRanOnPatient(Guid internalId);

        /// <summary>
        /// Deleted a patient record
        /// </summary>
        /// <param name="internalId">patients internal id</param>
        void DeletePatient(Guid internalId);
    }
}
