﻿using System.Collections.Generic;

namespace Daxor.Lab.BVA.Interfaces
{
    /// <summary>
    /// Defines the behavior of a service that calculates the sum of squares of differences
    /// for two lists of values.
    /// </summary>
    public interface ISumOfSquaresOfDifferencesCalculator
    {
        /// <summary>
        /// Calculates the sum of the squares of differences between the given lists' values.
        /// That is, Sum_i((list1_i - list2_i)^2).
        /// </summary>
        /// <param name="list1">First list of values</param>
        /// <param name="list2">Second list of values</param>
        /// <returns>Sum of the squares of differences for the given list values</returns>
        double CalculateSumOfSquaresOfDifferences(IEnumerable<double> list1, IEnumerable<double> list2);
    }
}
