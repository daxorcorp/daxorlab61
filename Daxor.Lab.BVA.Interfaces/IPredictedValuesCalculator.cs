﻿using System.Collections.Generic;

namespace Daxor.Lab.BVA.Interfaces
{
    /// <summary>
    /// Defines the behavior of a serivce that calculates the predicted (i.e., y-values) based 
    /// on parameters for a linear equation.
    /// </summary>
    public interface IPredictedValuesCalculator
    {
        /// <summary>
        /// Calculates the predicted (i.e., y-values) for the given independent values (i.e., x-values),
        /// slope, and y-intercept.
        /// </summary>
        /// <param name="independentValues">Values (i.e., x-values) to be used in the linear equation</param>
        /// <param name="slope">Slope of the line</param>
        /// <param name="yIntercept">y-intercept of the line</param>
        /// <returns>List of predicted y-values that correspond to the given independent values</returns>
        IEnumerable<double> CalculatePredictedValues(IEnumerable<double> independentValues, double slope, double yIntercept);
    }
}
