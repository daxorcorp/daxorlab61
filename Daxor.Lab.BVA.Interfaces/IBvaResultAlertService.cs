﻿using Daxor.Lab.Domain.Evaluation;
using Daxor.Lab.Infrastructure.RuleFramework;

namespace Daxor.Lab.BVA.Services
{
    /// <summary>
    /// Represents a service capable of displaying a message box with information about
    /// resolving alerts associated with BVA test results (i.e., total blood volume,
    /// standard deviation, transudation rate).
    /// </summary>
    /// <remarks>
    /// Alerts associated with input (e.g., weight, post-injection times) are handled
    /// via another mechanism. Those alerts are currently displayed via
    /// Daxor.Lab.Shell.ViewModels.ShellViewModel.ExecuteShowCurrentAlert().
    /// </remarks>
    public interface IBvaResultAlertService
    {
        /// <summary>
        /// Show a message box with information about an alert for the given entity.
        /// </summary>
        /// <param name="entity">Validatable entity with a validation error</param>
        /// <param name="propertyName">Property name that is causing the alert to exist</param>
        /// <param name="causeMessage">String to display in the "cause" section of the message
        /// (e.g., what value caused the alert to occur)</param>
        void ShowAlertMessageBox(EvaluatableObject entity, string propertyName, string causeMessage);
    }
}
