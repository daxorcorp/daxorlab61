﻿using Daxor.Lab.BVA.Core;

namespace Daxor.Lab.BVA.Interfaces
{
    /// <summary>
    /// Defines the behavior of a service that automatically excludes points 
    /// on a blood volume analysis test based on certain criteria.
    /// </summary>
    public interface IAutomaticPointExclusionService
    {
        /// <summary>
        /// Gets whether the service is enabled or not.
        /// </summary>
        bool IsEnabled { get; }

        /// <summary>
        /// Excludes points (if applicable) in a given blood volume analysis test
        /// based on certain criteria.
        /// </summary>
        /// <param name="test">Test whose points will be evaluated for exclusion</param>
        /// <returns>Metadata about the exclusion process</returns>
        AutomaticPointExclusionMetadata ExcludePoints(BVATest test);
    }
}
