﻿using Daxor.Lab.BVA.Core;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.BVA.Interfaces.Events
{
    /// <summary>
    /// The test selected event
    /// </summary>
    public class BvaTestSelected : CompositePresentationEvent<BVATest>
    {
    }
}
