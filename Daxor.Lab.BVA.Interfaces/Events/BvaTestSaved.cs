﻿using System;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.BVA.Interfaces.Events
{
    /// <summary>
    /// The test saved event
    /// </summary>
    public class BvaTestSaved : CompositePresentationEvent<Guid>
    {
    }
}
