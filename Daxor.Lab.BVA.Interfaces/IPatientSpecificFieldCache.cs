﻿using Daxor.Lab.BVA.Core;

namespace Daxor.Lab.BVA.Interfaces
{
    /// <summary>
    /// Defines the behavior of a cache for patient-specific fields.
    /// </summary>
    public interface IPatientSpecificFieldCache
    {
        /// <summary>
        /// Gets or sets whether the cached patient information differs from the actual patient information
        /// </summary>
        bool IsDirty { get; set; }

        /// <summary>
        /// Gets or sets whether the cached patient information is for a new patient.
        /// </summary>
        bool IsNewPatient { get; set; }

        /// <summary>
        /// Initializes the cache with the given patient. 
        /// </summary>
        /// <param name="patient">Patient to initialize the cache </param>
        /// <param name="forceInitialization">If set to true, this will initialize the cache with the information from patient</param>
        /// <returns>Return true if the cache was initialized, false otherwise</returns>
        /// <remarks>If the forceInitialization flag is not set, the cache will only be initialized if the cache was not previously intialized.</remarks>
        bool InitializePatientSpecificFieldCache(BVAPatient patient, bool forceInitialization = false);

        /// <summary>
        /// Overrides the cache with the given patient.
        /// </summary>
        /// <param name="patient"></param>
        void OverridePatientSpecificFieldCache(BVAPatient patient);

        /// <summary>
        /// Pushes the fields in the cache back to the given patient.
        /// </summary>
        /// <param name="patient"></param>
        void RestorePatientFieldsToMatchCache(BVAPatient patient);

        /// <summary>
        /// Marks the cache as uninitialized.
        /// </summary>
        void UninitializePatientSpecificFieldCache();

        /// <summary>
        /// Queries the user via messagebox to either restore the cache (i.e. undo the changes) or override the cache with new information
        /// </summary>
        /// <param name="existingPatient">Patient that exists in the database</param>
        /// <param name="enteredPatient">Patient that the user entered</param>
        void RestoreOrOverrideBasedOnUserChoice(BVAPatient existingPatient, BVAPatient enteredPatient);
    }
}
