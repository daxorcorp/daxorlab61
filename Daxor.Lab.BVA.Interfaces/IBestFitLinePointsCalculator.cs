﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Core;

namespace Daxor.Lab.BVA.Interfaces
{
    /// <summary>
    /// Defines the behavior of a service that computes the endpoints for a best fit
    /// line for blood volume analyses.
    /// </summary>
    public interface IBestFitLinePointsCalculator
    {
        /// <summary>
        /// Calculates the endpoints for a best fit line based on the given samples, 
        /// transudation rate, and total blood volume.
        /// </summary>
        /// <param name="compositeSamples">Collection of composite blood volume 
        /// samples used in the regression</param>
        /// <param name="transudationRate">Transudation rate (i.e., slope) of the
        /// corresponding test</param>
        /// <param name="totalBloodVolume">Total (i.e., time-zero) blood volume
        /// of the corresponding test</param>
        /// <returns>Collection of points that lie on the line of best fit</returns>
        IEnumerable<BestFitLinePoint> CalculateBestFitLinePoints(IEnumerable<BVACompositeSample> compositeSamples,
                                                                 double transudationRate, 
                                                                 double totalBloodVolume);
    }
}
