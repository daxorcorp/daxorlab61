﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.BVA.Core;
using System.Windows.Input;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Events;

namespace Daxor.Lab.BVA.Interfaces
{
    /// <summary>
    /// The Blood Volume Controller
    /// </summary>
    public interface IBloodVolumeTestController
    {
        /// <summary>
        /// Gets data service
        /// </summary>
        IBVADataService DataService { get; }

        /// <summary>
        /// Get logger
        /// </summary>
        ILoggerFacade Logger { get; }

        /// <summary>
        /// Get event aggregator
        /// </summary>
        IEventAggregator EventAggregator { get; }

        /// <summary>
        /// Gets running BVATest
        /// </summary>
        BVATest RunningTest { get; }

        /// <summary>
        /// Gets selected BVATest
        /// </summary>
        BVATest SelectedTest { get; }

        /// <summary>
        /// TestItem being Observed
        /// </summary>
        BVATestItem ObservingTestItem { get; }

        /// <summary>
        /// Selects BVATest
        /// </summary>
        ICommand SelectTestCommand { get; }

        /// <summary>
        /// Gets CreateTestCommand test
        /// </summary>
        ICommand CreateTestCommand { get; }

        /// <summary>
        /// Starts selected test
        /// </summary>
        void StartSelectedTest();
    }
}
