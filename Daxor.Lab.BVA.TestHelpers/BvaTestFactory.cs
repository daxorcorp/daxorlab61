﻿using System;
using System.Collections.Generic;
using Daxor.Lab.BVA.Core;

namespace Daxor.Lab.BVA.TestHelpers
{
    public static class BvaTestFactory
    {
        // Refactor this to do caching if used in other tests. (Geoff; 7/10/2012)
        public static BVATest CreateBvaTest(int numberOfPatientSamples, int sampleDurationInSeconds)
        {
            var schema = CreateNormalSampleLayoutSchema();
            return new BVATest(schema, null) { Protocol = numberOfPatientSamples, Tube = new Tube(), SampleDurationInSeconds = sampleDurationInSeconds };
        }

        public static BVATest CreateBvaTestWithRandomFriendlyNames(int numberOfPatientSamples, int sampleDurationInSeconds)
        {
            var schema = CreateRandomFriendlyNameSampleLayoutSchema();
            return new BVATest(schema, null) { Protocol = numberOfPatientSamples, Tube = new Tube(), SampleDurationInSeconds = sampleDurationInSeconds };
        }

        private static SampleLayoutSchema CreateNormalSampleLayoutSchema()
        {
            var sampleLayoutItems = new List<SampleLayoutItem>
                                        {
                                            new SampleLayoutItem
                                                {
                                                    FriendlyName = "Standard A",
                                                    Position = 3,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Standard
                                                },
                                            new SampleLayoutItem
                                                {
                                                    FriendlyName = "Baseline A",
                                                    Position = 4,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Baseline
                                                },
                                            new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 1A",
                                                    Position = 5,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Sample1
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 2A",
                                                    Position = 6,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Sample2
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 3A",
                                                    Position = 7,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Sample3
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 4A",
                                                    Position = 8,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Sample4
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 5A",
                                                    Position = 9,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Sample5
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 6A",
                                                    Position = 10,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Sample6
                                                },
                                            new SampleLayoutItem
                                                {
                                                    FriendlyName = "Standard B",
                                                    Position = 20,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Standard
                                                },
                                            new SampleLayoutItem
                                                {
                                                    FriendlyName = "Baseline B",
                                                    Position = 19,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Baseline
                                                },
                                            new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 1B",
                                                    Position = 18,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Sample1
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 2B",
                                                    Position = 17,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Sample2
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 3B",
                                                    Position = 16,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Sample3
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 4B",
                                                    Position = 15,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Sample4
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 5B",
                                                    Position = 14,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Sample5
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 6B",
                                                    Position = 13,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Sample6
                                                },
                                          new SampleLayoutItem
                                                { 
                                                    FriendlyName = "Background",
                                                    Position = 24, 
                                                    Range = SampleRange.Undefined, 
                                                    Type = SampleType.Background
                                                },
                                        };
            var schema = new SampleLayoutSchema(0);
            sampleLayoutItems.ForEach(r => schema.TryAddLayoutItem(r));
            return schema;
        }

        private static SampleLayoutSchema CreateRandomFriendlyNameSampleLayoutSchema()
        {
            var schema = CreateNormalSampleLayoutSchema();
            foreach (var sampleLayoutItem in schema.SampleLayoutItems)
                sampleLayoutItem.FriendlyName = Guid.NewGuid().ToString();
            return schema;
        }
    }
}
