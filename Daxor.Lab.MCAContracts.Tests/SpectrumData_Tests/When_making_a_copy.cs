using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MCAContracts.Tests.SpectrumData_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_making_a_copy
    {
        private const int ExpectedRealTime = 123456;
        private const int ExpectedLiveTime = 234567;
        private const int ExpectedDeadTime = 345678;
        private readonly UInt32[] _expectedSpectrumArray = new uint[32];
        private SpectrumData _copySpectrumData;

        [TestInitialize]
        public void BeforeEachTest()
        {
            var originalSpectrumData = new SpectrumData(ExpectedRealTime, ExpectedLiveTime, ExpectedDeadTime, _expectedSpectrumArray);
            _copySpectrumData = originalSpectrumData.GetShallowCopy();
        }

        [TestMethod]
        public void Then_the_real_time_is_copied()
        {
            Assert.AreEqual(ExpectedRealTime, _copySpectrumData.RealTimeInMicroseconds);
        }

        [TestMethod]
        public void Then_the_live_time_is_copied()
        {
            Assert.AreEqual(ExpectedLiveTime, _copySpectrumData.LiveTimeInMicroseconds);
        }

        [TestMethod]
        public void Then_the_dead_time_is_copied()
        {
            Assert.AreEqual(ExpectedDeadTime, _copySpectrumData.DeadTimeInMicroseconds);
        }

        [TestMethod]
        public void Then_the_spectrum_array_pointer_is_copied()
        {
            Assert.AreEqual(_expectedSpectrumArray, _copySpectrumData.SpectrumArray);
        }
    }
    // ReSharper restore InconsistentNaming
}
