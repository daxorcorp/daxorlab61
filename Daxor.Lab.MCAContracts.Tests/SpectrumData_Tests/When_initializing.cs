﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MCAContracts.Tests.SpectrumData_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing
    {
        private const int ExpectedRealTime = 123456;
        private const int ExpectedLiveTime = 234567;
        private const int ExpectedDeadTime = 345678;
        private readonly UInt32[] _expectedSpectrumArray = new uint[32];
        private SpectrumData _spectrumData;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _spectrumData = new SpectrumData(ExpectedRealTime, ExpectedLiveTime, ExpectedDeadTime, _expectedSpectrumArray);
        }

        [TestMethod]
        public void Then_the_real_time_is_set()
        {
            Assert.AreEqual(ExpectedRealTime, _spectrumData.RealTimeInMicroseconds);
        }

        [TestMethod]
        public void Then_the_live_time_is_set()
        {
            Assert.AreEqual(ExpectedLiveTime, _spectrumData.LiveTimeInMicroseconds);
        }

        [TestMethod]
        public void Then_the_dead_time_is_set()
        {
            Assert.AreEqual(ExpectedDeadTime, _spectrumData.DeadTimeInMicroseconds);
        }

        [TestMethod]
        public void Then_the_spectrum_array_is_set()
        {
            Assert.AreEqual(_expectedSpectrumArray, _spectrumData.SpectrumArray);
        }

        [TestMethod]
        public void Then_the_spectrum_array_can_be_null()
        {
            var spectrumData = new SpectrumData(ExpectedRealTime, ExpectedLiveTime, ExpectedDeadTime, null);
            Assert.IsNull(spectrumData.SpectrumArray);
        }
    }
    // ReSharper restore InconsistentNaming
}
