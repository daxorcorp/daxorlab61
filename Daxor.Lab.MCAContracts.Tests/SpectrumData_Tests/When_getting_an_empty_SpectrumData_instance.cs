using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MCAContracts.Tests.SpectrumData_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_an_empty_SpectrumData_instance
    {
        [TestMethod]
        public void Then_the_real_time_is_zero()
        {
            Assert.AreEqual(0, SpectrumData.Empty().RealTimeInMicroseconds);
        }

        [TestMethod]
        public void Then_the_live_time_is_zero()
        {
            Assert.AreEqual(0, SpectrumData.Empty().LiveTimeInMicroseconds);
        }

        [TestMethod]
        public void Then_the_dead_time_is_zero()
        {
            Assert.AreEqual(0, SpectrumData.Empty().DeadTimeInMicroseconds);
        }

        [TestMethod]
        public void Then_the_spectrum_array_is_null()
        {
            Assert.IsNull(SpectrumData.Empty().SpectrumArray);
        }
    }
    // ReSharper restore InconsistentNaming
}
