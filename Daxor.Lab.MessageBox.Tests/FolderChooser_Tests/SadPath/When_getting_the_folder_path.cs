﻿using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System.Collections.Generic;

namespace Daxor.Lab.MessageBox.Tests.FolderChooser_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_folder_path
    {
        [TestMethod]
        public void And_the_user_chooses_to_cancel_then_the_returned_folder_choice_is_null()
        {
            var exportingFolderBrowser = MockRepository.GenerateStub<IFolderBrowser>();
            exportingFolderBrowser.Expect(b => b.IsPathValid()).Return(false);

            var messageBoxChoices = new List<string> { FolderChooser.ExportText, FolderChooser.CancelText };
            var stubDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            stubDispatcher.Expect(d => d.ShowMessageBox(MessageBoxCategory.Undefined, null, null, null))
                .IgnoreArguments()
                .Return(messageBoxChoices.IndexOf(FolderChooser.ExportText));

            var chooser = new FolderChooser(stubDispatcher, new EmptyLogger(),
                MockRepository.GenerateStub<IStarBurnWrapper>(), MockRepository.GenerateStub<IDriveAccessorService>(),
                exportingFolderBrowser);
            var observedFolderChoice = chooser.GetFolderChoice();

            Assert.AreEqual(null, observedFolderChoice);
        }
    }
    // ReSharper restore InconsistentNaming
}
