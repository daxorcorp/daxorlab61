﻿using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.MessageBox.Tests.FolderChooser_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_instantiating_a_FolderChooser
    {
        [TestMethod]
        public void And_the_message_box_dispatcher_is_null_then_an_exception_is_thrown()
        {
            var stubFolderBrowser = MockRepository.GenerateStub<IFolderBrowser>();

            // ReSharper disable ObjectCreationAsStatement
            AssertEx.ThrowsArgumentNullException(()=>new FolderChooser(null, new EmptyLogger(), MockRepository.GenerateStub<IStarBurnWrapper>(), MockRepository.GenerateStub<IDriveAccessorService>(), stubFolderBrowser), "messageBoxDispatcher");
            // ReSharper restore ObjectCreationAsStatement
        }
    }
    // ReSharper restore InconsistentNaming
}
