﻿using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using System.Collections.Generic;

namespace Daxor.Lab.MessageBox.Tests.FolderChooser_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_folder_path
    {
        private static IFolderBrowser _stubFolderBrowser;
        private static List<string> _messageBoxChoices;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _stubFolderBrowser = MockRepository.GenerateStub<IFolderBrowser>();
            _messageBoxChoices = new List<string> { FolderChooser.ExportText, FolderChooser.CancelText };
        }

        [TestMethod]
        public void Then_the_correct_message_box_is_shown()
        {
            var mockDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockDispatcher.Expect(d => d.ShowMessageBox(MessageBoxCategory.Undefined, _stubFolderBrowser,
                                                   FolderChooser.MessageBoxCaption, _messageBoxChoices))
                          .Return(0);

            var chooser = new FolderChooser(mockDispatcher, new EmptyLogger(), MockRepository.GenerateStub<IStarBurnWrapper>(), MockRepository.GenerateStub<IDriveAccessorService>(), _stubFolderBrowser);
            chooser.GetFolderChoice();

            mockDispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_user_chooses_to_cancel_then_the_returned_folder_choice_is_null()
        {
            var stubDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            stubDispatcher.Expect(d => d.ShowMessageBox(MessageBoxCategory.Undefined, null, null, null))
                .IgnoreArguments()
                .Return(_messageBoxChoices.IndexOf(FolderChooser.CancelText));

            var chooser = new FolderChooser(stubDispatcher, new EmptyLogger(), MockRepository.GenerateStub<IStarBurnWrapper>(), MockRepository.GenerateStub<IDriveAccessorService>(), _stubFolderBrowser);
            var observedFolderChoice = chooser.GetFolderChoice();

            Assert.AreEqual(null, observedFolderChoice);
        }

        [TestMethod]
        public void And_the_user_chooses_to_export_to_an_optical_drive_then_the_returned_folder_choice_reflects_this()
        {
            var opticalFolderBrowser = MockRepository.GenerateStub<IFolderBrowser>();
            opticalFolderBrowser.Expect(b => b.IsPathValid()).Return(true);
            opticalFolderBrowser.IsOpticalDrive = true;
            opticalFolderBrowser.CurrentPath = "";
            
            var stubDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            stubDispatcher.Expect(d => d.ShowMessageBox(MessageBoxCategory.Undefined, null, null, null))
                .IgnoreArguments()
                .Return(_messageBoxChoices.IndexOf(FolderChooser.ExportText));

            var chooser = new FolderChooser(stubDispatcher, new EmptyLogger(), MockRepository.GenerateStub<IStarBurnWrapper>(), MockRepository.GenerateStub<IDriveAccessorService>(), opticalFolderBrowser);
            var observedFolderChoice = chooser.GetFolderChoice();

            Assert.IsTrue(observedFolderChoice.IsOpticalDrive);
        }

        [TestMethod]
        public void And_the_user_chooses_to_export_to_a_non_optical_drive_then_the_returned_folder_choice_reflects_this()
        {
            var opticalFolderBrowser = MockRepository.GenerateStub<IFolderBrowser>();
            opticalFolderBrowser.Expect(b => b.IsPathValid()).Return(true);
            opticalFolderBrowser.IsOpticalDrive = false;
            opticalFolderBrowser.CurrentPath = "";

            var stubDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            stubDispatcher.Expect(d => d.ShowMessageBox(MessageBoxCategory.Undefined, null, null, null))
                .IgnoreArguments()
                .Return(_messageBoxChoices.IndexOf(FolderChooser.ExportText));

            var chooser = new FolderChooser(stubDispatcher, new EmptyLogger(), MockRepository.GenerateStub<IStarBurnWrapper>(), MockRepository.GenerateStub<IDriveAccessorService>(), opticalFolderBrowser);
            var observedFolderChoice = chooser.GetFolderChoice();

            Assert.IsFalse(observedFolderChoice.IsOpticalDrive);
        }

        [TestMethod]
        public void And_the_user_chooses_to_export_and_the_path_ends_in_a_folder_separator_then_the_returned_folder_choice_doesnt_add_another()
        {
            var expectedTestPath = "testpath" + FolderChooser.FolderSeparator;

            var opticalFolderBrowser = MockRepository.GenerateStub<IFolderBrowser>();
            opticalFolderBrowser.Expect(b => b.IsPathValid()).Return(true);
            opticalFolderBrowser.IsOpticalDrive = false;
            opticalFolderBrowser.CurrentPath = expectedTestPath;

            var stubDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            stubDispatcher.Expect(d => d.ShowMessageBox(MessageBoxCategory.Undefined, null, null, null))
                .IgnoreArguments()
                .Return(_messageBoxChoices.IndexOf(FolderChooser.ExportText));

            var chooser = new FolderChooser(stubDispatcher, new EmptyLogger(), MockRepository.GenerateStub<IStarBurnWrapper>(), MockRepository.GenerateStub<IDriveAccessorService>(), opticalFolderBrowser);
            var observedFolderChoice = chooser.GetFolderChoice();

            Assert.AreEqual(expectedTestPath, observedFolderChoice.Path);
        }

        [TestMethod]
        public void And_the_user_chooses_to_export_and_the_path_doesnt_end_in_a_folder_separator_then_the_returned_folder_choice_adds_it()
        {
            const string testPathPrefix = "testpath";

            var opticalFolderBrowser = MockRepository.GenerateStub<IFolderBrowser>();
            opticalFolderBrowser.Expect(b => b.IsPathValid()).Return(true);
            opticalFolderBrowser.IsOpticalDrive = false;
            opticalFolderBrowser.CurrentPath = testPathPrefix;

            var stubDispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            stubDispatcher.Expect(d => d.ShowMessageBox(MessageBoxCategory.Undefined, null, null, null))
                .IgnoreArguments()
                .Return(_messageBoxChoices.IndexOf(FolderChooser.ExportText));

            var chooser = new FolderChooser(stubDispatcher, new EmptyLogger(), MockRepository.GenerateStub<IStarBurnWrapper>(), MockRepository.GenerateStub<IDriveAccessorService>(), opticalFolderBrowser);
            var observedFolderChoice = chooser.GetFolderChoice();

            var expectedPath = testPathPrefix + FolderChooser.FolderSeparator;
            Assert.AreEqual(expectedPath, observedFolderChoice.Path);
        }
    }
    // ReSharper restore InconsistentNaming
}
