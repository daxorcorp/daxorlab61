﻿using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.SumOfSquaresOfDifferences_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_arguments_are_null
    {
        private static SumOfSquaresOfDifferencesCalculator _calculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _calculator = new SumOfSquaresOfDifferencesCalculator();
        }

        [TestMethod]
        public void And_the_first_list_argument_is_null_then_an_exception_is_thrown()
        {
            var list = new[] { 1.0, 2.0 };

            AssertEx.ThrowsArgumentNullException(()=>_calculator.CalculateSumOfSquaresOfDifferences(null, list), "source");
        }

        [TestMethod]
        public void And_the_second_list_argument_is_null_then_an_exception_is_thrown()
        {
            var list = new[] { 1.0, 2.0 };

            AssertEx.ThrowsArgumentNullException(()=>_calculator.CalculateSumOfSquaresOfDifferences(list, null), "source");
        }
    }
    // ReSharper restore InconsistentNaming
}
