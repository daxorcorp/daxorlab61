﻿using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.SumOfSquaresOfDifferences_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_list_sizes_differ
    {
        [TestMethod]
        public void Then_an_exception_is_thrown()
        {
            var calc = new SumOfSquaresOfDifferencesCalculator();
            var list1 = new[] {1.0};
            var list2 = new[] {1.0, 2.0};

            AssertEx.Throws<ArgumentException>(()=>calc.CalculateSumOfSquaresOfDifferences(list1, list2));
        }
    }
    // ReSharper restore InconsistentNaming
}
