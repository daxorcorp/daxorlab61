﻿using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.SumOfSquaresOfDifferences_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_second_list_has_invalid_values
    {
        private static SumOfSquaresOfDifferencesCalculator _calculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _calculator = new SumOfSquaresOfDifferencesCalculator();
        }

        [TestMethod]
        public void Because_one_value_is_positive_infinity_then_an_exception_is_thrown()
        {
            var list1 = new[] { 1.0, 2.0 };
            var list2 = new[] { 1.0, Double.PositiveInfinity };

            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculateSumOfSquaresOfDifferences(list1, list2));
        }

        [TestMethod]
        public void Because_one_value_is_negative_infinity_then_an_exception_is_thrown()
        {
            var list1 = new[] { 1.0, 2.0 };
            var list2 = new[] { 1.0, Double.PositiveInfinity };

            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculateSumOfSquaresOfDifferences(list1, list2));
        }

        [TestMethod]
        public void Because_mulitple_values_are_infinity_then_an_exception_is_thrown()
        {
            // Already have tests for +/- infinity, so we'll just test positive infinities here.

            var list1 = new[] { 1.0, 2.0, 3.0, 4.0 };
            var list2 = new[] { 1.0, Double.PositiveInfinity, 3.0, Double.PositiveInfinity };

            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculateSumOfSquaresOfDifferences(list1, list2));
        }

        [TestMethod]
        public void Because_one_value_is_NaN_then_an_exception_is_thrown()
        {
            var list1 = new[] { 1.0, 2.0 };
            var list2 = new[] { 1.0, Double.NaN };

            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculateSumOfSquaresOfDifferences(list1, list2));
        }

        [TestMethod]
        public void Because_mulitple_values_are_NaN_then_an_exception_is_thrown()
        {
            var list1 = new[] { 1.0, 2.0, 3.0, 4.0 };
            var list2 = new[] { 1.0, Double.NaN, 3.0, Double.NaN };

            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculateSumOfSquaresOfDifferences(list1, list2));
        }
    }
    // ReSharper restore InconsistentNaming
}
