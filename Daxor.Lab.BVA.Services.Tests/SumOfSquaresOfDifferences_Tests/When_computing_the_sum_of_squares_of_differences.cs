﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.SumOfSquaresOfDifferences_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_the_sum_of_squares_of_differences
    {
        private static SumOfSquaresOfDifferencesCalculator _calculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _calculator = new SumOfSquaresOfDifferencesCalculator();
        }

        [TestMethod]
        public void And_the_two_lists_are_empty_then_the_result_is_zero()
        {
            var list1 = new List<double>();
            var list2 = new List<double>();

            Assert.AreEqual(0, _calculator.CalculateSumOfSquaresOfDifferences(list1, list2));
        }

        [TestMethod]
        public void Then_the_sum_is_correct()
        {
            var listOfList1s = new List<List<double>>
                {
                    new List<double> {-33.119, -61.756, -34.744},
                    new List<double> {-8.543, 74.643, -93.263},
                    new List<double> {14.822, 11.486, -10.736},
                };
            var listOfList2s = new List<List<double>>
                {
                    new List<double> {95.524, -92.368, -92.956},
                    new List<double> {-4.595, -71.498, -0.966},
                    new List<double> {58.458, 14.143, 0.91},
                };
            var listOfExpectedResults = new List<double> { 20874.753, 29891.515, 2046.789 };

            var actualResults = listOfList1s.Select((t, i) => Math.Round(_calculator.CalculateSumOfSquaresOfDifferences(t, listOfList2s[i]), 3)).ToList();

            var message = new StringBuilder();
            var foundMismatch = false;
            for (var i = 0; i < listOfExpectedResults.Count; i++)
            {
                if (!(Math.Abs(listOfExpectedResults[i] - actualResults[i]) > 1E-100)) continue;

                foundMismatch = true;
                message.Append("Expected value " + listOfExpectedResults[i] + " but got " +
                               actualResults[i] + "; ");
            }

            if (foundMismatch)
                Assert.Fail(message.ToString());
        }
    }
    // ReSharper restore InconsistentNaming
}
