﻿using System;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Services.RuleEvaluators;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Exceptions;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Services.Tests.Module_Tests.SadPath
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_the_BVA_Services_module
	{
		private Module _module;
		private IUnityContainer _container;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_container = Substitute.For<IUnityContainer>();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_patient_repository_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType(typeof(IRepository<>), typeof(PatientRepository))).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA Services", ex.ModuleName);
				Assert.AreEqual("initialize the Patient Repository", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_BVA_data_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IBVADataService, BvaDataService>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA Services", ex.ModuleName);
				Assert.AreEqual("initialize the BVA Data Service", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_blood_volume_test_controller_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IBloodVolumeTestController, BloodVolumeTestController>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA Services", ex.ModuleName);
				Assert.AreEqual("initialize the Blood Volume Test Controller", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_best_fit_line_points_calculator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IBestFitLinePointsCalculator, BestFitLinePointsCalculator>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA Services", ex.ModuleName);
				Assert.AreEqual("initialize the Best Fit Line Points Calculator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_sum_of_squares_of_differences_calculator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<ISumOfSquaresOfDifferencesCalculator, SumOfSquaresOfDifferencesCalculator>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA Services", ex.ModuleName);
				Assert.AreEqual("initialize the Sum of Squares of Differences Calculator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_predicted_values_calculator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IPredictedValuesCalculator, PredictedValuesCalculator>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA Services", ex.ModuleName);
				Assert.AreEqual("initialize the Predicted Values Calculator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_residual_standard_error_calculator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IResidualStandardErrorCalculator, ResidualStandardErrorCalculator>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA Services", ex.ModuleName);
				Assert.AreEqual("initialize the Residual Standard Error Calculator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_automatic_point_exclusion_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IAutomaticPointExclusionService, AutomaticPointExclusionService>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA Services", ex.ModuleName);
				Assert.AreEqual("initialize the Automatic Point Exclusion Service", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_patient_specific_field_cache_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IPatientSpecificFieldCache, PatientSpecificFieldCache>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA Services", ex.ModuleName);
				Assert.AreEqual("initialize the Patient-specific Field Cache", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_BVA_rule_engine_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IRuleEngine, BvaRuleEngine>(Arg.Any<string>(), Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA Services", ex.ModuleName);
				Assert.AreEqual("register the BVA Rule Engine", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_resolve_and_initialize_the_BVA_rule_engine_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<IRuleEngine>(Arg.Any<string>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("BVA Services", ex.ModuleName);
				Assert.AreEqual("initialize the BVA Rule Engine", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}
	}
	// ReSharper restore InconsistentNaming
}