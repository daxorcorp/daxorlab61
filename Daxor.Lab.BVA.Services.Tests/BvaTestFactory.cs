﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.BVA.Services.Tests
{
    internal static class TestFactory
    {
        internal static BVATest CreateBvaTest(int numberOfPatientSamples)
        {
            var sampleLayoutItems = new List<SampleLayoutItem>
                                        {
                                            new SampleLayoutItem
                                                {
                                                    FriendlyName = "Standard A",
                                                    Position = 3,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Standard
                                                },
                                            new SampleLayoutItem
                                                {
                                                    FriendlyName = "Baseline A",
                                                    Position = 4,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Baseline
                                                },
                                            new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 1A",
                                                    Position = 5,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Sample1
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 2A",
                                                    Position = 6,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Sample2
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 3A",
                                                    Position = 7,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Sample3
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 4A",
                                                    Position = 8,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Sample4
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 5A",
                                                    Position = 9,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Sample5
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 6A",
                                                    Position = 10,
                                                    Range = SampleRange.A,
                                                    Type = SampleType.Sample6
                                                },
                                            new SampleLayoutItem
                                                {
                                                    FriendlyName = "Standard B",
                                                    Position = 20,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Standard
                                                },
                                            new SampleLayoutItem
                                                {
                                                    FriendlyName = "Baseline B",
                                                    Position = 19,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Baseline
                                                },
                                            new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 1B",
                                                    Position = 18,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Sample1
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 2B",
                                                    Position = 17,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Sample2
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 3B",
                                                    Position = 16,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Sample3
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 4B",
                                                    Position = 15,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Sample4
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 5B",
                                                    Position = 14,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Sample5
                                                },
                                          new SampleLayoutItem
                                                {
                                                    FriendlyName = "Sample 6B",
                                                    Position = 13,
                                                    Range = SampleRange.B,
                                                    Type = SampleType.Sample6
                                                },
                                        };
            var schema = new SampleLayoutSchema(0);
            sampleLayoutItems.ForEach(r => schema.TryAddLayoutItem(r));
            var bvaTest = new BVATest(schema, null)
                {
                    Protocol = numberOfPatientSamples, 
                    Tube = new Tube(), 
                    HasSufficientData = true, 
                    StandardDevResult = 1,
                    BackgroundCount = 10,
                    Status = TestStatus.Completed
                };

            foreach (var compositeSample in bvaTest.CompositeSamples)
            {
                compositeSample.SampleA.PostInjectionTimeInSeconds = 1;
                compositeSample.SampleB.PostInjectionTimeInSeconds = 1;

                compositeSample.SampleA.Hematocrit = 0.1;
                compositeSample.SampleB.Hematocrit = 0.1;

                compositeSample.SampleA.Counts = 1;
                compositeSample.SampleB.Counts = 1;
            }

            return bvaTest;
        }
    }
}
