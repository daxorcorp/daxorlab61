﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.BvaTestExecutionContext_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_processing_the_spectrum
    {
        [TestMethod]
        public void And_acquisition_has_been_completed_then_the_samples_execution_status_is_set_to_completed()
        {
            // --- Arrange ---
            
            // Just need a non-null test to pass to the execution context
            var threeSampleTest = TestHelpers.BvaTestFactory.CreateBvaTest(3, 10);  
            
            // Detector should be done acquiring and have a non-null Spectrum instance to prevent exceptions.
            var stubDetector = MockRepository.GenerateStub<IDetector>();
            stubDetector.Expect(d => d.HasAcquisitionCompleted).Return(true);
            stubDetector.Expect(d => d.Spectrum).Return(new Spectrum());

            // Just need a sample instance to be "processed"
            var sample = new BVASample(13, null) { PresetLiveTimeInSeconds = 10 }; 

            var context = new TestBvaTestExecutionContext(threeSampleTest)
                { ExposedHasAcquisitionStarted = true, ExposedDetector = stubDetector, ExposedAcquisitionCompletedCallBack = s => { } };

            
            // --- Act ---
            context.PerformAcquisition(sample, s => { });


            // --- Assert ---
            Assert.AreEqual(SampleExecutionStatus.Completed, sample.ExecutionStatus, "Sample's execution status should be completed");
        }
    }
    // ReSharper restore InconsistentNaming
}
