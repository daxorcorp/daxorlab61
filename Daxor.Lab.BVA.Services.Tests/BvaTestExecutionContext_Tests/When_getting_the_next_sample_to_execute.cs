﻿using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.BvaTestExecutionContext_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_next_sample_to_execute
    {
        [TestMethod]
        [TestCategory("BVA")]
        public void And_the_test_has_3_samples_then_GetNextSampleToExecute_returns_a_sample_sequence_based_on_position_number()
        {
            // Arrange
            var threeSampleTest = TestHelpers.BvaTestFactory.CreateBvaTest(3, 10);
            var context = new TestBvaTestExecutionContext(threeSampleTest) { ExposedExecutingSample = null };

            // Act: make a list of samples returned by GetNextSampleToExecute()
            var listOfSamplesToExecute = new List<ISample>();
            var nextSample = context.GetNextSampleToExecute();
            listOfSamplesToExecute.Add(nextSample);

            context.ExposedExecutingSample = nextSample;
            do
            {
                nextSample = context.GetNextSampleToExecute();
                listOfSamplesToExecute.Add(nextSample);
                context.ExposedExecutingSample = nextSample;
            } while (nextSample != null);

            // Assert: compare this list to the correct ordering
            var expectedOrdering = (from s in threeSampleTest.Samples orderby s.Position select s).ToList();
            expectedOrdering.Add(null);
            CollectionAssert.AreEqual(expectedOrdering, listOfSamplesToExecute);
        }

        [TestMethod]
        [TestCategory("BVA")]
        public void And_the_test_has_4_samples_then_GetNextSampleToExecute_returns_a_sample_sequence_based_on_position_number()
        {
            // Arrange
            var threeSampleTest = TestHelpers.BvaTestFactory.CreateBvaTest(4, 10);
            var context = new TestBvaTestExecutionContext(threeSampleTest) { ExposedExecutingSample = null };

            // Act: make a list of samples returned by GetNextSampleToExecute()
            var listOfSamplesToExecute = new List<ISample>();
            var nextSample = context.GetNextSampleToExecute();
            listOfSamplesToExecute.Add(nextSample);

            context.ExposedExecutingSample = nextSample;
            do
            {
                nextSample = context.GetNextSampleToExecute();
                listOfSamplesToExecute.Add(nextSample);
                context.ExposedExecutingSample = nextSample;
            } while (nextSample != null);

            // Assert: compare this list to the correct ordering
            var expectedOrdering = (from s in threeSampleTest.Samples orderby s.Position select s).ToList();
            expectedOrdering.Add(null);
            CollectionAssert.AreEqual(expectedOrdering, listOfSamplesToExecute);
        }

        [TestMethod]
        [TestCategory("BVA")]
        public void And_the_test_has_5_samples_then_GetNextSampleToExecute_returns_a_sample_sequence_based_on_position_number()
        {
            // Arrange
            var threeSampleTest = TestHelpers.BvaTestFactory.CreateBvaTest(5, 10);
            var context = new TestBvaTestExecutionContext(threeSampleTest) { ExposedExecutingSample = null };

            // Act: make a list of samples returned by GetNextSampleToExecute()
            var listOfSamplesToExecute = new List<ISample>();
            var nextSample = context.GetNextSampleToExecute();
            listOfSamplesToExecute.Add(nextSample);

            context.ExposedExecutingSample = nextSample;
            do
            {
                nextSample = context.GetNextSampleToExecute();
                listOfSamplesToExecute.Add(nextSample);
                context.ExposedExecutingSample = nextSample;
            } while (nextSample != null);

            // Assert: compare this list to the correct ordering
            var expectedOrdering = (from s in threeSampleTest.Samples orderby s.Position select s).ToList();
            expectedOrdering.Add(null);
            CollectionAssert.AreEqual(expectedOrdering, listOfSamplesToExecute);
        }

        [TestMethod]
        [TestCategory("BVA")]
        public void And_the_test_has_6_samples_then_GetNextSampleToExecute_returns_a_sample_sequence_based_on_position_number()
        {
            // Arrange
            var threeSampleTest = TestHelpers.BvaTestFactory.CreateBvaTest(6, 10);
            var context = new TestBvaTestExecutionContext(threeSampleTest) { ExposedExecutingSample = null };

            // Act: make a list of samples returned by GetNextSampleToExecute()
            var listOfSamplesToExecute = new List<ISample>();
            var nextSample = context.GetNextSampleToExecute();
            listOfSamplesToExecute.Add(nextSample);

            context.ExposedExecutingSample = nextSample;
            do
            {
                nextSample = context.GetNextSampleToExecute();
                listOfSamplesToExecute.Add(nextSample);
                context.ExposedExecutingSample = nextSample;
            } while (nextSample != null);

            // Assert: compare this list to the correct ordering
            var expectedOrdering = (from s in threeSampleTest.Samples orderby s.Position select s).ToList();
            expectedOrdering.Add(null);
            CollectionAssert.AreEqual(expectedOrdering, listOfSamplesToExecute);
        }
    }
    // ReSharper restore InconsistentNaming
}
