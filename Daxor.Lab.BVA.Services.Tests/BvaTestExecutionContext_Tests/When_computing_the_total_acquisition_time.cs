﻿using System;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.Domain.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.BvaTestExecutionContext_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_the_total_acquisition_time
    {
        [TestMethod]
        [TestCategory("BVA")]
        [TestCategory("ExecutionContext")]
        public void Then_the_computed_time_is_correct()
        {
            var durationGenerator = new Random();
            var expectedSampleCountTimeInSeconds = durationGenerator.Next(100, 300);
            const int numberOfPairedSamples = 3;
            const int numberOfBaselineSamples = 2;
            const int numberOfStandards = 2;
            var bvaTest = BvaTestFactory.CreateBvaTest(numberOfPairedSamples, expectedSampleCountTimeInSeconds);
            var executionContext = new TestBvaTestExecutionContext(bvaTest);
            const int totalNumberOfSamples = (numberOfPairedSamples * 2) + numberOfBaselineSamples + numberOfStandards;

            var acquisitionTimeInSeconds = 0;
            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var bvaSample in executionContext.OrderedSamplesExcludingBackground)
                acquisitionTimeInSeconds += executionContext.ComputeSampleAcquisitionTimeInMilliseconds(bvaSample);
            // ReSharper restore LoopCanBeConvertedToQuery

            Assert.AreEqual(expectedSampleCountTimeInSeconds * totalNumberOfSamples * TimeConstants.MillisecondsPerSecond, acquisitionTimeInSeconds);
        }
    }
    // ReSharper restore InconsistentNaming
}
