using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Services.TestExecutionContext;
using Daxor.Lab.Domain.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.BvaTestExecutionContext_Tests
{
    internal class TestBvaTestExecutionContext : BvaTestExecutionContext
    {
        internal TestBvaTestExecutionContext(BVATest test)
            : base(test, 
                MockRepository.GenerateStub<IDetector>(), 
                MockRepository.GenerateStub<ICalibrationCurve>(), 
                new EmptyLogger(),
                MockRepository.GenerateStub<ISpectroscopyService>(), 
                MockRepository.GenerateStub<IBVADataService>(), 
                MockRepository.GenerateStub<IRuleEngine>())
        {
        }

        internal IDetector ExposedDetector
        {
            get { return Detector; } 
            set { Detector = value; }
        }

        internal ISample ExposedExecutingSample
        {
            set { ExecutingSample = value; }
        }

        internal bool ExposedHasAcquisitionStarted
        {
            set { HasAcquisitionStarted = value; }
        }

        internal Action<ISample> ExposedAcquisitionCompletedCallBack
        {
            set { AcquisitionCompletedCallback = value; }
        }
    }
}