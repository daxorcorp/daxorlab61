﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_settings_change
    {
        private static IMeasuredCalcEngineService _stubMeasuredCalcEngineService;
        private static IResidualStandardErrorCalculator _stubResidualStandardErrorCalculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _stubMeasuredCalcEngineService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            _stubResidualStandardErrorCalculator = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
        }

        [TestMethod]
        public void And_the_setting_is_for_enabling_exclusion_then_the_service_becomes_enabled()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled))
                               .Return(false).Repeat.Once();
            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);
            
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled))
                            .Return(true).Repeat.Once();  // Second call should be true
            stubSettingsManager.Raise(m => m.SettingsChanged += null, this,
                                      new SettingsChangedEventArgs(new List<string> { SettingKeys.BvaAutomaticPointExclusionEnabled }));

            Assert.IsTrue(service.IsEnabled);
        }

        [TestMethod]
        public void And_the_setting_is_for_disabling_exclusion_then_the_service_becomes_disabled()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled))
                               .Return(true).Repeat.Once();
            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled))
                            .Return(false).Repeat.Once();  // Second call should be false
            stubSettingsManager.Raise(m => m.SettingsChanged += null, this,
                                      new SettingsChangedEventArgs(new List<string> { SettingKeys.BvaAutomaticPointExclusionEnabled }));

            Assert.IsFalse(service.IsEnabled);
        }

        [TestMethod]
        public void And_the_setting_is_for_the_stddev_threshold_then_the_service_updates_accordingly()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold))
                               .Return(12.34).Repeat.Once();
            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            const double expectedStdDevThreshold = 56.78;
            stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold))
                            .Return(expectedStdDevThreshold).Repeat.Once();  // Second call should be a different value
            stubSettingsManager.Raise(m => m.SettingsChanged += null, this,
                                      new SettingsChangedEventArgs(new List<string> { SettingKeys.BvaAutomaticPointExclusionStdDevThreshold }));

            Assert.AreEqual(expectedStdDevThreshold, service.StandardDeviationThreshold);
        }

        [TestMethod]
        public void And_the_setting_is_for_the_minimum_patient_protocol_then_the_service_updates_accordingly()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMinProtocol))
                               .Return(5).Repeat.Once();
            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            const int expectedMinimumPatientProtocol = 4;
            stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMinProtocol))
                            .Return(expectedMinimumPatientProtocol).Repeat.Once();  // Second call should be a different value
            stubSettingsManager.Raise(m => m.SettingsChanged += null, this,
                                      new SettingsChangedEventArgs(new List<string> { SettingKeys.BvaAutomaticPointExclusionMinProtocol }));

            Assert.AreEqual(expectedMinimumPatientProtocol, service.MinimumPatientProtocol);
        }

        [TestMethod]
        public void And_the_setting_is_for_the_maximum_number_of_exclusions_then_the_service_updates_accordingly()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions))
                               .Return(5).Repeat.Once();
            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            const int expectedMaximumNumberOfExclusions = 4;
            stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions))
                            .Return(expectedMaximumNumberOfExclusions).Repeat.Once();  // Second call should be a different value
            stubSettingsManager.Raise(m => m.SettingsChanged += null, this,
                                      new SettingsChangedEventArgs(new List<string> { SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions }));

            Assert.AreEqual(expectedMaximumNumberOfExclusions, service.MaximumNumberOfExclusions);
        }

        [TestMethod]
        public void And_the_setting_is_for_the_residual_standard_error_then_the_service_updates_accordingly()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionRseThreshold))
                               .Return(0.029).Repeat.Once();
            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            const double expectedRseThreshold = 0.019;
            stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionRseThreshold))
                            .Return(expectedRseThreshold).Repeat.Once();  // Second call should be a different value
            stubSettingsManager.Raise(m => m.SettingsChanged += null, this,
                                      new SettingsChangedEventArgs(new List<string> { SettingKeys.BvaAutomaticPointExclusionRseThreshold }));

            Assert.AreEqual(expectedRseThreshold, service.ResidualStandardErrorThreshold);
        }
    }
    // ReSharper restore InconsistentNaming
}
