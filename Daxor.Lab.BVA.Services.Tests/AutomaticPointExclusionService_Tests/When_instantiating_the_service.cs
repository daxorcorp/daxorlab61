﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_instantiating_the_service
    {
        private static IMeasuredCalcEngineService _stubMeasuredCalcEngineService;
        private static IResidualStandardErrorCalculator _stubResidualStandardErrorCalculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _stubMeasuredCalcEngineService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            _stubResidualStandardErrorCalculator = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
        }

        [TestMethod]
        public void And_the_service_is_disabled_via_settings_then_the_service_is_disabled()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled))
                            .Return(false);

            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            Assert.IsFalse(service.IsEnabled);
        }

        [TestMethod]
        public void And_the_service_is_enabled_via_settings_then_the_service_is_enabled()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled))
                            .Return(true);

            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            Assert.IsTrue(service.IsEnabled);
        }

        [TestMethod]
        public void Then_the_stddev_threshold_is_based_on_system_settings()
        {
            const double expectedStdDevThreshold = 12.34;
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold))
                            .Return(expectedStdDevThreshold);

            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            Assert.AreEqual(expectedStdDevThreshold, service.StandardDeviationThreshold);
        }

        [TestMethod]
        public void Then_the_minimum_patient_protocol_is_based_on_system_settings()
        {
            const int expectedMinimumPatientProtocol = 5;
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMinProtocol))
                            .Return(expectedMinimumPatientProtocol);

            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            Assert.AreEqual(expectedMinimumPatientProtocol, service.MinimumPatientProtocol);
        }

        [TestMethod]
        public void Then_the_maximum_number_of_exclusions_is_based_on_system_settings()
        {
            const int expectedMaximumNumberOfExclusions = 5;
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions))
                            .Return(expectedMaximumNumberOfExclusions);

            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            Assert.AreEqual(expectedMaximumNumberOfExclusions, service.MaximumNumberOfExclusions);
        }

        [TestMethod]
        public void Then_the_residual_standard_error_threshold_is_based_on_system_settings()
        {
            const double expectedRseThreshold = 0.029;
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionRseThreshold))
                            .Return(expectedRseThreshold);

            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            Assert.AreEqual(expectedRseThreshold, service.ResidualStandardErrorThreshold);
        }
    }
    // ReSharper restore InconsistentNaming
}
