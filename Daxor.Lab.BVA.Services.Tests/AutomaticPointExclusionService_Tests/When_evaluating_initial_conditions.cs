﻿using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_initial_conditions
    {
        private static IMeasuredCalcEngineService _stubMeasuredCalcEngineService;
        private static IResidualStandardErrorCalculator _stubResidualStandardErrorCalculator;
        private static ISettingsManager _stubSettingsManager; 
        const int MinProtocol = 5;
        const double StdDevThreshold = 12.34;

        [ClassInitialize]
        public static void ClassInitialze(TestContext FooBar)
        {
            _stubMeasuredCalcEngineService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            _stubResidualStandardErrorCalculator = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled)).Return(true);
        }

        [TestMethod]
        public void And_the_standard_deviation_is_above_the_threshold_then_work_continues()
        {
            // Arrange
            _stubSettingsManager.Expect(m => m.GetSetting<double>
                (SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(StdDevThreshold);

            var mockService = GenerateMockPointExclusionService();

            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.StandardDevResult = StdDevThreshold + 1; // need to be above the threshold

            // Act
            mockService.ExcludePoints(bvaTest);

            // Assert
            var expectedOrder = new List<string> { "CaptureBvaTestState", "InternalExcludePoints" };
            CollectionAssert.AreEqual(expectedOrder, mockService.Invocations);
        }

        [TestMethod]
        public void And_the_protocol_is_at_the_minimum_then_work_continues()
        {
            // Arrange
            _stubSettingsManager.Expect(m => m.GetSetting<int>
                (SettingKeys.BvaAutomaticPointExclusionMinProtocol)).Return(MinProtocol);

            var mockService = GenerateMockPointExclusionService();

            var bvaTest = TestFactory.CreateBvaTest(MinProtocol);

            // Act
            mockService.ExcludePoints(bvaTest);

            // Assert
            var expectedOrder = new List<string> { "CaptureBvaTestState", "InternalExcludePoints" };
            CollectionAssert.AreEqual(expectedOrder, mockService.Invocations);
        }

        [TestMethod]
        public void And_the_protocol_is_above_the_minimum_then_work_continues()
        {
            // Arrange
            _stubSettingsManager.Expect(m => m.GetSetting<int>
                (SettingKeys.BvaAutomaticPointExclusionMinProtocol)).Return(MinProtocol);

            var mockService = GenerateMockPointExclusionService();

            var bvaTest = TestFactory.CreateBvaTest(MinProtocol + 1);

            // Act
            mockService.ExcludePoints(bvaTest);

            // Assert
            var expectedOrder = new List<string> { "CaptureBvaTestState", "InternalExcludePoints" };
            CollectionAssert.AreEqual(expectedOrder, mockService.Invocations);
        }

        [TestMethod]
        public void And_the_test_has_no_issues_then_work_continues()
        {
            // Arrange
            var mockService = GenerateMockPointExclusionService();

            var bvaTest = TestFactory.CreateBvaTest(4);

            // Act
            mockService.ExcludePoints(bvaTest);

            // Assert
            var expectedOrder = new List<string> { "CaptureBvaTestState", "InternalExcludePoints" };
            CollectionAssert.AreEqual(expectedOrder, mockService.Invocations);
        }

        [TestMethod]
        public void And_the_A_side_hct_is_excluded_then_work_continues()
        {
            // Arrange
            var mockService = GenerateMockPointExclusionService();

            var bvaTest = TestFactory.CreateBvaTest(4);
            var sample1a = (from s in bvaTest.Samples
                            where s.Range == SampleRange.A && s.Type == SampleType.Sample1
                            select s).FirstOrDefault();
            if (sample1a == null) Assert.Fail("Can't deal with null samples from the test factory");
            sample1a.IsHematocritExcluded = true;

            // Act
            mockService.ExcludePoints(bvaTest);

            // Assert
            var expectedOrder = new List<string> { "CaptureBvaTestState", "InternalExcludePoints" };
            CollectionAssert.AreEqual(expectedOrder, mockService.Invocations);
        }

        [TestMethod]
        public void And_the_B_side_hct_is_excluded_then_work_continues()
        {
            // Arrange
            var mockService = GenerateMockPointExclusionService();

            var bvaTest = TestFactory.CreateBvaTest(4);
            var sample1b = (from s in bvaTest.Samples
                            where s.Range == SampleRange.B && s.Type == SampleType.Sample1
                            select s).FirstOrDefault();
            if (sample1b == null) Assert.Fail("Can't deal with null samples from the test factory");
            sample1b.IsHematocritExcluded = true;

            // Act
            mockService.ExcludePoints(bvaTest);

            // Assert
            var expectedOrder = new List<string> { "CaptureBvaTestState", "InternalExcludePoints" };
            CollectionAssert.AreEqual(expectedOrder, mockService.Invocations);
        }

        [TestMethod]
        public void And_counts_for_standard_A_are_excluded_then_work_continues()
        {
            // Arrange
            var mockService = GenerateMockPointExclusionService();

            var bvaTest = TestFactory.CreateBvaTest(4);
            var standardA = (from s in bvaTest.Samples
                             where s.Range == SampleRange.A && s.Type == SampleType.Standard
                             select s).FirstOrDefault();
            if (standardA == null) Assert.Fail("Can't deal with null samples from the test factory");
            standardA.IsCountExcluded = true;

            // Act
            mockService.ExcludePoints(bvaTest);

            // Assert
            var expectedOrder = new List<string> { "CaptureBvaTestState", "InternalExcludePoints" };
            CollectionAssert.AreEqual(expectedOrder, mockService.Invocations);
        }

        [TestMethod]
        public void And_counts_for_standard_B_are_excluded_then_work_continues()
        {
            // Arrange
            var mockService = GenerateMockPointExclusionService();

            var bvaTest = TestFactory.CreateBvaTest(4);
            var standardB = (from s in bvaTest.Samples
                             where s.Range == SampleRange.B && s.Type == SampleType.Standard
                             select s).FirstOrDefault();
            if (standardB == null) Assert.Fail("Can't deal with null samples from the test factory");
            standardB.IsCountExcluded = true;

            // Act
            mockService.ExcludePoints(bvaTest);

            // Assert
            var expectedOrder = new List<string> { "CaptureBvaTestState", "InternalExcludePoints" };
            CollectionAssert.AreEqual(expectedOrder, mockService.Invocations);
        }

        private static MockPointExclusionService GenerateMockPointExclusionService()
        {
            return new MockPointExclusionService(_stubMeasuredCalcEngineService,
                                                 _stubResidualStandardErrorCalculator, _stubSettingsManager)
            {
                UseBaseInternalExcludePoints = false
            };
        }

    }
    // ReSharper restore InconsistentNaming
}
