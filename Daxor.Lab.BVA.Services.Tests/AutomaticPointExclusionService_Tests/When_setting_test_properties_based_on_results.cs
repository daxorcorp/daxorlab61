﻿using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.DomainModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_test_properties_based_on_results
    {
        private const double ExpectedPlasmaVolume = 1234.56;
        private const double ExpectedRedCellVolume = 2345.67;
        private const double ExpectedSlope = 0.123;
        private const double ExpectedStandardDeviation = 4.567;

        private static MeasuredCalcEngineResults _results;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _results = new MeasuredCalcEngineResults(null, 0, ExpectedPlasmaVolume, ExpectedRedCellVolume, ExpectedSlope, ExpectedStandardDeviation, 0);
        }

        [TestMethod]
        public void Then_the_standard_deviation_is_set_correctly()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            AutomaticPointExclusionService.SetTestPropertiesBasedOnCalculationResults(bvaTest, _results);

            Assert.AreEqual(ExpectedStandardDeviation, bvaTest.StandardDevResult);
        }

        [TestMethod]
        public void Then_the_transudation_rate_is_set_correctly()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            AutomaticPointExclusionService.SetTestPropertiesBasedOnCalculationResults(bvaTest, _results);

            Assert.AreEqual(ExpectedSlope, bvaTest.TransudationRateResult);
        }

        [TestMethod]
        public void Then_the_measured_plasma_volume_is_set_correctly()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            AutomaticPointExclusionService.SetTestPropertiesBasedOnCalculationResults(bvaTest, _results);

            Assert.AreEqual(Math.Round(ExpectedPlasmaVolume, 0), bvaTest.Volumes[BloodType.Measured].PlasmaCount);
        }

        [TestMethod]
        public void Then_the_measured_red_cell_volume_is_set_correctly()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            AutomaticPointExclusionService.SetTestPropertiesBasedOnCalculationResults(bvaTest, _results);

            Assert.AreEqual(Math.Round(ExpectedRedCellVolume, 0), bvaTest.Volumes[BloodType.Measured].RedCellCount);
        }
    }
    // ReSharper restore InconsistentNaming
}
