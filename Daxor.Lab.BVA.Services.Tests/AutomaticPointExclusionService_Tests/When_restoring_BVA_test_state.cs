﻿using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_restoring_BVA_test_state
    {
        private static AutomaticPointExclusionService _pointExclusionService;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _pointExclusionService = new AutomaticPointExclusionService(MockRepository.GenerateStub<IMeasuredCalcEngineService>(),
                MockRepository.GenerateStub<IResidualStandardErrorCalculator>(),
                MockRepository.GenerateStub<ISettingsManager>());
        }

        [TestMethod]
        public void Then_the_correct_sample_states_are_being_restored()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            var samplesAsList = bvaTest.Samples.ToList();
            samplesAsList[0].IsCountAutoExcluded = true;  // IsCountExcluded = T, IsCountAutoExcluded = T
            samplesAsList[1].IsCountExcluded = true;      // IsCountExcluded = T, IsCountAutoExcluded = F
            samplesAsList[2].IsCountAutoExcluded = true;
            samplesAsList[2].IsCountExcluded = false;     // IsCountExcluded = F, IsCountAutoExcluded = T

            var bvaTestState = _pointExclusionService.CaptureBvaTestState(bvaTest);

            // Make a change to the fourth sample, which should have no exclusions
            samplesAsList[3].IsCountAutoExcluded = true;

            _pointExclusionService.RestoreBvaTestState(bvaTestState, bvaTest);

            foreach (var state in bvaTestState.SampleExclusionStates)
            {
                var sampleState = state;
                var matchingSample = (from s in samplesAsList where s.InternalId == sampleState.SampleId select s).FirstOrDefault();
                Assert.IsNotNull(matchingSample, "No BVA test sample matched this state: " + state);
                Assert.AreEqual(matchingSample.IsCountExcluded, state.IsCountExcluded, "IsCountExcluded didn't match for state " + state);
                Assert.AreEqual(matchingSample.IsCountAutoExcluded, state.IsCountAutoExcluded, "IsCountAutoExcluded didn't match for state " + state);
            }
        }

        [TestMethod]
        public void Then_the_plasma_volume_is_restored()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            const int expectedPlasmaVolume = 1234;
            bvaTest.Volumes[BloodType.Measured].PlasmaCount = expectedPlasmaVolume;
            var bvaTestState = _pointExclusionService.CaptureBvaTestState(bvaTest);

            // Make a change to see if restoring undoes it
            bvaTest.Volumes[BloodType.Measured].PlasmaCount = 5678;
            _pointExclusionService.RestoreBvaTestState(bvaTestState, bvaTest);

            Assert.AreEqual(expectedPlasmaVolume, bvaTest.Volumes[BloodType.Measured].PlasmaCount);
        }

        [TestMethod]
        public void Then_the_red_cell_volume_is_restored()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            const int expectedRedCellVolume = 1234;
            bvaTest.Volumes[BloodType.Measured].RedCellCount = expectedRedCellVolume;
            var bvaTestState = _pointExclusionService.CaptureBvaTestState(bvaTest);

            // Make a change to see if restoring undoes it
            bvaTest.Volumes[BloodType.Measured].RedCellCount = 5678;
            _pointExclusionService.RestoreBvaTestState(bvaTestState, bvaTest);

            Assert.AreEqual(expectedRedCellVolume, bvaTest.Volumes[BloodType.Measured].RedCellCount);
        }

        [TestMethod]
        public void Then_the_standard_deviation_is_restored()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            const double expectedStandardDeviation = 0.123;
            bvaTest.StandardDevResult = expectedStandardDeviation;
            var bvaTestState = _pointExclusionService.CaptureBvaTestState(bvaTest);

            // Make a change to see if restoring undoes it
            bvaTest.StandardDevResult = 0.5678;
            _pointExclusionService.RestoreBvaTestState(bvaTestState, bvaTest);

            Assert.AreEqual(expectedStandardDeviation, bvaTest.StandardDevResult);
        }

        [TestMethod]
        public void Then_the_transudation_rate_is_restored()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            const double expectedTransudationRate = 0.123;
            bvaTest.TransudationRateResult = expectedTransudationRate;
            var bvaTestState = _pointExclusionService.CaptureBvaTestState(bvaTest);

            // Make a change to see if restoring undoes it
            bvaTest.TransudationRateResult = 0.5678;
            _pointExclusionService.RestoreBvaTestState(bvaTestState, bvaTest);

            Assert.AreEqual(expectedTransudationRate, bvaTest.TransudationRateResult);
        }
    }
    // ReSharper restore InconsistentNaming
}
