﻿using Daxor.Lab.BVA.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_the_average_sample_count
    {
        [TestMethod]
        public void And_both_are_not_excluded_and_have_acceptable_counts_then_the_average_count_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = 15 };
            var sample2 = new BVASample(1, null) { Counts = 45 };

            Assert.AreEqual(30, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_included_with_acceptable_counts_and_sample2_is_included_with_negative_counts_then_sample1s_count_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = 15 };
            var sample2 = new BVASample(1, null) { Counts = -1 };

            Assert.AreEqual(15, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_included_with_acceptable_counts_and_sample2_is_excluded_with_acceptable_counts_then_sample1s_count_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = 15 };
            var sample2 = new BVASample(1, null) { Counts = 20, IsCountExcluded = true};

            Assert.AreEqual(15, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_included_with_acceptable_counts_and_sample2_is_excluded_with_negative_counts_then_sample1s_count_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = 15 };
            var sample2 = new BVASample(1, null) { Counts = -1, IsCountExcluded = true};

            Assert.AreEqual(15, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_included_with_negative_counts_and_sample2_is_included_with_acceptable_counts_then_sample2s_count_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = -1 };
            var sample2 = new BVASample(1, null) { Counts = 15 };

            Assert.AreEqual(15, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_included_with_negative_counts_and_sample2_is_included_with_negative_counts_then_0_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = -1 };
            var sample2 = new BVASample(1, null) { Counts = -1 };

            Assert.AreEqual(0, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_included_with_negative_counts_and_sample2_is_excluded_with_acceptable_counts_then_0_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = -1 };
            var sample2 = new BVASample(1, null) { Counts = 15, IsCountExcluded = true};

            Assert.AreEqual(0, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_included_with_negative_counts_and_sample2_is_excluded_with_negative_counts_then_0_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = -1 };
            var sample2 = new BVASample(1, null) { Counts = -1, IsCountExcluded = true };

            Assert.AreEqual(0, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_excluded_with_acceptable_counts_and_sample2_is_included_with_acceptable_counts_then_sample2s_count_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = 20, IsCountExcluded = true};
            var sample2 = new BVASample(1, null) { Counts = 15 };

            Assert.AreEqual(15, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_excluded_with_acceptable_counts_and_sample2_is_included_with_negative_counts_then_0_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = 20, IsCountExcluded = true };
            var sample2 = new BVASample(1, null) { Counts = -1 };

            Assert.AreEqual(0, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_excluded_with_acceptable_counts_and_sample2_is_excluded_with_acceptable_counts_then_0_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = 20, IsCountExcluded = true };
            var sample2 = new BVASample(1, null) { Counts = 15, IsCountExcluded = true};

            Assert.AreEqual(0, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_excluded_with_acceptable_counts_and_sample2_is_excluded_with_negative_counts_then_0_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = 20, IsCountExcluded = true };
            var sample2 = new BVASample(1, null) { Counts = -1, IsCountExcluded = true };

            Assert.AreEqual(0, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_excluded_with_negative_counts_and_sample2_is_included_with_acceptable_counts_then_sample2s_count_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = -1, IsCountExcluded = true };
            var sample2 = new BVASample(1, null) { Counts = 15 };

            Assert.AreEqual(15, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_excluded_with_negative_counts_and_sample2_is_included_with_negative_counts_then_0_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = -1, IsCountExcluded = true };
            var sample2 = new BVASample(1, null) { Counts = -1 };

            Assert.AreEqual(0, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_excluded_with_negative_counts_and_sample2_is_excluded_with_acceptable_counts_then_0_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = -1, IsCountExcluded = true };
            var sample2 = new BVASample(1, null) { Counts = 15, IsCountExcluded = true };

            Assert.AreEqual(0, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1_is_excluded_with_negative_counts_and_sample2_is_excluded_with_negative_counts_then_0_is_returned()
        {
            var sample1 = new BVASample(0, null) { Counts = -1, IsCountExcluded = true };
            var sample2 = new BVASample(1, null) { Counts = -1, IsCountExcluded = true };

            Assert.AreEqual(0, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }

        [TestMethod]
        public void And_both_samples_are_used_then_no_rounding_occurs()
        {
            var sample1 = new BVASample(0, null) {Counts = 15};
            var sample2 = new BVASample(1, null) {Counts = 16};

            Assert.AreEqual(15.5, AutomaticPointExclusionService.ComputeAverageSampleCount(sample1, sample2));
        }
    }
    // ReSharper restore InconsistentNaming
}
