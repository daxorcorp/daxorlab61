﻿using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    internal class MockMeasuredCalcEngineService : IMeasuredCalcEngineService
    {
        internal List<MeasuredCalcEngineParams> MeasuredCalcEngineParams { get; private set; }

        public MockMeasuredCalcEngineService()
        {
            MeasuredCalcEngineParams = new List<MeasuredCalcEngineParams>();
        }

        public MeasuredCalcEngineResults GetAllResults(MeasuredCalcEngineParams calcEngineParams)
        {
            MeasuredCalcEngineParams.Add(calcEngineParams);
            var unadjustedResults = (from sample in calcEngineParams.Points select new UnadjustedBloodVolumeResult(sample.UniqueId, 123)).ToList();
            return new MeasuredCalcEngineResults(unadjustedResults, 1234, 2345, 3456, 0.123, 0.234, 0.345, 0.456);
        }

        public UnadjustedBloodVolumeResult GetOnlyUbvResult(int refVolume, double antiCouglant, double standardCounts, int background, double controlCounts, BloodVolumePoint p)
        {
            return null; // never used
        }
    }
}