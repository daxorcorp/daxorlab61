﻿using System;
using System.Linq;
using Daxor.Lab.Infrastructure.DomainModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_generating_a_collection_of_unadjusted_BVs
    {
        [TestMethod]
        public void And_subsamples_are_being_used_then_the_collection_is_correct()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            var random = new Random();
            var expectedDictionary = bvaTest.Samples.ToDictionary(sample => sample, sample => random.NextDouble() * 1000);
            var dummyUnadjustedBVResults = expectedDictionary.Keys.Select(sample => new UnadjustedBloodVolumeResult(sample.InternalId, expectedDictionary[sample])).ToList();

            var dummyResults = new MeasuredCalcEngineResults(dummyUnadjustedBVResults, 0, 0, 0, 0, 0, 0);
            var actualDictionary = AutomaticPointExclusionService.GenerateCollectionOfUnadjustedBloodVolumes(dummyResults, bvaTest);

            CollectionAssert.AreEqual(expectedDictionary, actualDictionary);
        }

        [TestMethod]
        public void And_composite_samples_are_being_used_then_the_collection_is_correct()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            var random = new Random();
            var expectedDictionary = bvaTest.CompositeSamples.ToDictionary(sample => sample.SampleA, sample => random.NextDouble() * 1000);
            var dummyUnadjustedBVResults = expectedDictionary.Keys.Select(sample => new UnadjustedBloodVolumeResult(sample.Type, expectedDictionary[sample])).ToList();

            var dummyResults = new MeasuredCalcEngineResults(dummyUnadjustedBVResults, 0, 0, 0, 0, 0, 0);
            var actualDictionary = AutomaticPointExclusionService.GenerateCollectionOfUnadjustedBloodVolumes(dummyResults, bvaTest, true);

            CollectionAssert.AreEqual(expectedDictionary, actualDictionary);
        }

        [TestMethod]
        public void And_a_subsample_has_a_negative_unadjusted_blood_volume_then_that_volume_remains_negative()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            var random = new Random();
            var expectedDictionary = bvaTest.Samples.ToDictionary(sample => sample, sample => random.NextDouble() * 1000);
            expectedDictionary[bvaTest.Samples.First()] *= -1;
            var dummyUnadjustedBVResults = expectedDictionary.Keys.Select(sample => new UnadjustedBloodVolumeResult(sample.InternalId, expectedDictionary[sample])).ToList();

            var dummyResults = new MeasuredCalcEngineResults(dummyUnadjustedBVResults, 0, 0, 0, 0, 0, 0);
            var actualDictionary = AutomaticPointExclusionService.GenerateCollectionOfUnadjustedBloodVolumes(dummyResults, bvaTest);

            CollectionAssert.AreEqual(expectedDictionary, actualDictionary);
        }
    }
    // ReSharper restore InconsistentNaming
}
