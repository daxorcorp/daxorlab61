﻿using Daxor.Lab.BVA.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_looking_for_patient_samples
    {
        [TestMethod]
        public void And_the_sample_is_a_patient_sample_then_IsPatientSample_returns_true()
        {
            for (var sampleType = SampleType.Sample1; sampleType <= SampleType.Sample6; sampleType++)
            {
                var patientSample = new BVASample(1, null) {Type = sampleType};
                Assert.IsTrue(AutomaticPointExclusionService.IsPatientSample(patientSample));
            }
        }

        [TestMethod]
        public void And_the_sample_is_a_standard_then_IsPatientSample_returns_false()
        {
            var standard = new BVASample(1, null) { Type = SampleType.Standard };
            Assert.IsFalse(AutomaticPointExclusionService.IsPatientSample(standard));
        }

        [TestMethod]
        public void And_the_sample_is_a_baseline_then_IsPatientSample_returns_false()
        {
            var baseline = new BVASample(1, null) { Type = SampleType.Baseline };
            Assert.IsFalse(AutomaticPointExclusionService.IsPatientSample(baseline));
        }

        [TestMethod]
        public void And_the_sample_is_the_background_then_IsPatientSample_returns_false()
        {
            var background = new BVASample(1, null) { Type = SampleType.Background };
            Assert.IsFalse(AutomaticPointExclusionService.IsPatientSample(background));
        }

        [TestMethod]
        public void And_the_sample_is_of_undefined_type_then_IsPatientSample_returns_false()
        {
            var undefined = new BVASample(1, null) { Type = SampleType.Undefined };
            Assert.IsFalse(AutomaticPointExclusionService.IsPatientSample(undefined));
        }
    }
    // ReSharper restore InconsistentNaming
}
