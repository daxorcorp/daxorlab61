﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    internal class MockPointExclusionService : AutomaticPointExclusionService
    {
        internal MockPointExclusionService(IMeasuredCalcEngineService measuredCalcEngineService,
                                           IResidualStandardErrorCalculator residualStandardErrorCalculator,
                                           ISettingsManager settingsManager)
            : base(measuredCalcEngineService, residualStandardErrorCalculator, settingsManager)
        {
            Invocations = new List<string>();
            UseBaseTestMeetsInitialConditions = true;
            UseBaseInternalExcludePoints = true;
            UseBaseFindSampleToExclude = false;

            BvaSampleReturnedByFindSampleToExclude = new BVASample(0, null); // Default value
            MetadataReturnedByFindSampleToExclude = new List<string>(); // Default value
        }

        internal List<string> Invocations { get; set; }

        public bool UseBaseTestMeetsInitialConditions { get; set; }

        public bool UseBaseInternalExcludePoints { get; set; }

        public bool UseBaseFindSampleToExclude { get; set; }

        public List<string> MetadataReturnedByFindSampleToExclude { get; set; }

        public BVASample BvaSampleReturnedByFindSampleToExclude { get; set; }

        public override BvaTestState CaptureBvaTestState(BVATest bvaTest)
        {
            Invocations.Add("CaptureBvaTestState");
            return null;
        }

        internal override AutomaticPointExclusionMetadata InternalExcludePoints(BVATest test)
        {
            Invocations.Add("InternalExcludePoints");
            return UseBaseInternalExcludePoints ? base.InternalExcludePoints(test) : null;
        }

        public override void RestoreBvaTestState(BvaTestState bvaTestState, BVATest bvaTest)
        {
            Invocations.Add("RestoreBvaTestState");
        }

        protected override bool TestMeetsInitialConditions(BVATest bvaTest, out AutomaticPointExclusionMetadata failureMetadata)
        {
            if (UseBaseTestMeetsInitialConditions)
                return base.TestMeetsInitialConditions(bvaTest, out failureMetadata);
            
            failureMetadata = null;
            return true;
        }

        internal override List<string> FindSampleToExclude(BVATest bvaTest, out BVASample sampleToExclude)
        {
            Invocations.Add("FindSampleToExclude");
            if (UseBaseFindSampleToExclude)
                return base.FindSampleToExclude(bvaTest, out sampleToExclude);

            sampleToExclude = BvaSampleReturnedByFindSampleToExclude;
            return MetadataReturnedByFindSampleToExclude;
        }
    }
}