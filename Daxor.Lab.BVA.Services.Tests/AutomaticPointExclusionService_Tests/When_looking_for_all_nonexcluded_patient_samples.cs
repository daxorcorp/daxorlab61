﻿using System.Linq;
using Daxor.Lab.BVA.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_looking_for_all_nonexcluded_patient_samples
    {
        [TestMethod]
        public void And_all_samples_have_had_counts_excluded_then_an_empty_collection_is_returned()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            foreach (var sample in bvaTest.Samples)
                sample.IsCountAutoExcluded = true;
            
            Assert.AreEqual(0, AutomaticPointExclusionService.FindAllNonExcludedPatientSamples(bvaTest).Count());
        }

        [TestMethod]
        public void And_one_sample_has_had_counts_excluded_then_the_correct_samples_are_returned()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            var sample1b = (from s in bvaTest.Samples
                           where s.Type == SampleType.Sample1 && s.Range == SampleRange.B
                           select s).FirstOrDefault();
            if (sample1b == null) Assert.Fail("Could not find sample 1B");
            sample1b.IsCountAutoExcluded = true;

            var allButSample1b = from s in bvaTest.Samples 
                                 where s != sample1b && s.Type != SampleType.Background && s.Type != SampleType.Baseline && s.Type != SampleType.Standard 
                                 select s;
            
            CollectionAssert.AreEquivalent(allButSample1b.ToArray(), AutomaticPointExclusionService.FindAllNonExcludedPatientSamples(bvaTest).ToArray());
        }

        [TestMethod]
        public void And_one_sample_has_counts_that_are_autoexcluded_but_not_count_excluded_then_the_correct_samples_are_returned()
        {
            // This tests whether the method under test is looking at BVASample.IsCountAutoExcluded (incorrect)
            // or BVASample.IsCountExcluded (correct).

            var bvaTest = TestFactory.CreateBvaTest(3);
            var sample1b = (from s in bvaTest.Samples
                            where s.Type == SampleType.Sample1 && s.Range == SampleRange.B
                            select s).FirstOrDefault();
            if (sample1b == null) Assert.Fail("Could not find sample 1B");
            sample1b.IsCountAutoExcluded = true;
            sample1b.IsCountExcluded = false;

            var allButSample1b = from s in bvaTest.Samples
                                 where s.Type != SampleType.Background && s.Type != SampleType.Baseline && s.Type != SampleType.Standard
                                 select s;

            CollectionAssert.AreEquivalent(allButSample1b.ToArray(), AutomaticPointExclusionService.FindAllNonExcludedPatientSamples(bvaTest).ToArray());
        }

        [TestMethod]
        public void And_one_sample_has_been_wholly_excluded_then_the_correct_samples_are_returned()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            var sample1b = (from s in bvaTest.Samples
                            where s.Type == SampleType.Sample1 && s.Range == SampleRange.B
                            select s).FirstOrDefault();
            if (sample1b == null) Assert.Fail("Could not find sample 1B");
            sample1b.IsExcluded = true;

            var allButSample1b = from s in bvaTest.Samples
                                 where s != sample1b && s.Type != SampleType.Background && s.Type != SampleType.Baseline && s.Type != SampleType.Standard
                                 select s;

            CollectionAssert.AreEquivalent(allButSample1b.ToArray(), AutomaticPointExclusionService.FindAllNonExcludedPatientSamples(bvaTest).ToArray());
        }
    }
    // ReSharper restore InconsistentNaming
}
