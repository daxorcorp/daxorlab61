﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Rhino.Mocks.Constraints;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_finding_samples_to_exclude
    {
        private const double RseFraction = 0.1234;
        private const double TimeZeroBVFraction = 0.2345;
        private const double StandardDeviationFraction = 0.3456;

        [TestMethod]
        public void Then_subsamples_drive_the_choice_rather_than_composite_samples()
        {
            // -- Arrange --
            var bvaTest = CreateThreeSampleTest();
            var stubMeasuredCalcEngine = new MockMeasuredCalcEngineService();
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var stubRseCalc = CreateStubRseCalcForTestingThatSubsamplesDriveComputationForAThreeSampleTest();
            var service = new AutomaticPointExclusionService(stubMeasuredCalcEngine, stubRseCalc, stubSettingsManager);
            
            // -- Act --
            BVASample outSample;
            service.FindSampleToExclude(bvaTest, out outSample);

            // -- Assert --
            var sample1b = (from s in bvaTest.Samples
                            where s.Type == SampleType.Sample1 && s.Range == SampleRange.B
                            select s).FirstOrDefault();
            Assert.AreEqual(sample1b, outSample);
        }

        [TestMethod]
        public void Then_the_sample_with_the_lowest_RSE_is_returned()
        {
            var bvaTest = CreateThreeSampleTest();
            var stubMeasuredCalcEngine = new MockMeasuredCalcEngineService();
            var stubRseCalc = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseCalc.Expect(c => c.CalculateResidualStandardError(null, null, 0, 0)).IgnoreArguments().Return(0.76).Repeat.Times(3);
            stubRseCalc.Expect(c => c.CalculateResidualStandardError(null, null, 0, 0)).IgnoreArguments().Return(0.06).Repeat.Once();
            stubRseCalc.Expect(c => c.CalculateResidualStandardError(null, null, 0, 0)).IgnoreArguments().Return(0.76).Repeat.Times(2);
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();

            var service = new AutomaticPointExclusionService(stubMeasuredCalcEngine, stubRseCalc, stubSettingsManager);

            var nonExcludedPatientSamples = AutomaticPointExclusionService.FindAllNonExcludedPatientSamples(bvaTest).ToList();
            var fourthSample = nonExcludedPatientSamples[3];

            BVASample outSample;
            service.FindSampleToExclude(bvaTest, out outSample);

            Assert.AreEqual(fourthSample, outSample);
        }

        [TestMethod]
        public void And_two_samples_are_tied_for_lowest_RSE_then_the_first_sample_is_returned()
        {
            var bvaTest = CreateThreeSampleTest();
            var stubMeasuredCalcEngine = new MockMeasuredCalcEngineService();
            var stubRseCalc = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseCalc.Expect(c => c.CalculateResidualStandardError(null, null, 0, 0)).IgnoreArguments().Return(0.76).Repeat.Times(3);
            stubRseCalc.Expect(c => c.CalculateResidualStandardError(null, null, 0, 0)).IgnoreArguments().Return(0.06).Repeat.Twice();
            stubRseCalc.Expect(c => c.CalculateResidualStandardError(null, null, 0, 0)).IgnoreArguments().Return(0.76).Repeat.Once();
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();

            var service = new AutomaticPointExclusionService(stubMeasuredCalcEngine, stubRseCalc, stubSettingsManager);

            var nonExcludedPatientSamples = AutomaticPointExclusionService.FindAllNonExcludedPatientSamples(bvaTest).ToList();
            var fourthSample = nonExcludedPatientSamples[3];

            BVASample outSample;
            service.FindSampleToExclude(bvaTest, out outSample);

            Assert.AreEqual(fourthSample, outSample);
        }

        [TestMethod]
        public void Then_the_correct_metadata_is_returned()
        {
            // -- Arrange --
            var bvaTest = CreateThreeSampleTest();
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var stubRseCalc = CreateStubRseCalcForTestingMetadata();
            var stubMeasuredCalcEngine = CreateMeasuredCalcEngineStubForTestingMetadata();
            var service = new AutomaticPointExclusionService(stubMeasuredCalcEngine, stubRseCalc, stubSettingsManager);
            var expectedMetadata = CreateExpectedMetadata();
            
            // -- Act --
            BVASample ignoredSample;
            var observedMetadata = service.FindSampleToExclude(bvaTest, out ignoredSample);

            // -- Assert -- 
            CollectionAssert.AreEqual(expectedMetadata, observedMetadata);
        }

        private List<string> CreateExpectedMetadata()
        {
            var metadata = new List<string>();
            var sampleTypeLookup = new List<string>
                {"Sample1", "Sample2", "Sample3"};
            var sampleRangeLookup = new List<string>
                {"A", "B"};

            for (var i = 0; i < 6; i++)
            {
                metadata.Add(
                    String.Format(AutomaticPointExclusionService.FindSampleToExcludeMetadataFormatString,
                                  sampleTypeLookup[i/2],
                                  sampleRangeLookup[i%2],
                                  i + RseFraction,
                                  i*2 + 1 + StandardDeviationFraction,
                                  i*2 + 1 + TimeZeroBVFraction));
            }

            return metadata;
        }

        [TestMethod]
        public void Then_no_samples_remain_excluded()
        {
            var bvaTest = CreateThreeSampleTest();
            var mockMeasuredCalcEngine = new MockMeasuredCalcEngineService();
            var stubRseCalc = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();

            var service = new AutomaticPointExclusionService(mockMeasuredCalcEngine, stubRseCalc, stubSettingsManager);

            BVASample outSample;
            service.FindSampleToExclude(bvaTest, out outSample);

            var autoExcludedSamples = from s in bvaTest.Samples where s.IsCountAutoExcluded select s;
            Assert.AreEqual(0, autoExcludedSamples.Count());
        }

        private static BVATest CreateThreeSampleTest()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            foreach (var sample in bvaTest.Samples)
            {
                sample.PostInjectionTimeInSeconds = 1;
                sample.Counts = 1;
            }
            return bvaTest;
        }

        private IResidualStandardErrorCalculator CreateStubRseCalcForTestingThatSubsamplesDriveComputationForAThreeSampleTest()
        {
            const double lowestRse = 0.1234;
            const double nonLowestRse = 0.2345;
            var stubRseCalc = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();

            // 6 subsamples minus one subsample excluded = 5 to compute RSE
            // Order returned:
            //   1A: 0.2345
            //   1B: 0.1234 <-- picked b/c it has the lowest RSE
            //   2A: 0.2345
            //   2B: 0.2345
            //   3A: 0.2345
            //   3B: 0.2345
            stubRseCalc.Stub(c => c.CalculateResidualStandardError(null, null, 0, 0))
                       .IgnoreArguments()
                       .Constraints(List.Count(Is.Equal(5)), Is.Anything(), Is.Anything(), Is.Anything())
                       .Repeat.Once().Return(nonLowestRse);
            stubRseCalc.Stub(c => c.CalculateResidualStandardError(null, null, 0, 0))
                       .IgnoreArguments()
                       .Constraints(List.Count(Is.Equal(5)), Is.Anything(), Is.Anything(), Is.Anything())
                       .Repeat.Once().Return(lowestRse);
            stubRseCalc.Stub(c => c.CalculateResidualStandardError(null, null, 0, 0))
                       .IgnoreArguments()
                       .Constraints(List.Count(Is.Equal(5)), Is.Anything(), Is.Anything(), Is.Anything())
                       .Repeat.Times(4).Return(nonLowestRse);

            return stubRseCalc;
        }

        private IMeasuredCalcEngineService CreateMeasuredCalcEngineStubForTestingMetadata()
        {
            var stubMeasuredCalcEngineService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            for (var i = 0; i < 12; i++)
            {
                stubMeasuredCalcEngineService.Stub(c => c.GetAllResults(null))
                                             .IgnoreArguments()
                                             .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(),
                                                                                   i + TimeZeroBVFraction, 0, 0, 0,
                                                                                   i + StandardDeviationFraction, 0))
                                             .Repeat.Once();
            }

            return stubMeasuredCalcEngineService;
        }

        private IResidualStandardErrorCalculator CreateStubRseCalcForTestingMetadata()
        {
            var stubRseCalc = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            for (var i = 0; i < 12; i++)
            {
                stubRseCalc.Stub(c => c.CalculateResidualStandardError(null, null, 0, 0))
                           .IgnoreArguments().Return(i + RseFraction).Repeat.Once();
            }

            return stubRseCalc;
        }
    }
    // ReSharper restore InconsistentNaming
}
