﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_running_the_exclusion_algorithm
    {
        public const double RseThreshold = 2.5;
        public const double StandardDeviationThreshold = 3.9;

        private static ISettingsManager _settingsManager;
        
        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _settingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _settingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled)).Return(true);
            _settingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionRseThreshold)).Return(RseThreshold);
            _settingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(StandardDeviationThreshold);
            _settingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions)).Return(4);
        }

        [TestMethod]
        public void And_the_initial_test_has_a_stddev_below_the_threshold_and_an_RSE_above_the_threshold_then_the_service_halts()
        {
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold - 1, 0));
            var stubRseService = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold + 1);
            var mockPointExclusionService = new MockPointExclusionService(stubCalcService, stubRseService, _settingsManager)
            {
                UseBaseTestMeetsInitialConditions = false
            };
            var bvaTest = TestFactory.CreateBvaTest(3);

            mockPointExclusionService.ExcludePoints(bvaTest);

            var expectedInvocations = new List<string> { "CaptureBvaTestState", "InternalExcludePoints" };
            CollectionAssert.AreEqual(expectedInvocations, mockPointExclusionService.Invocations);
        }

        [TestMethod]
        public void And_the_initial_test_has_a_stddev_below_the_threshold_and_an_RSE_above_the_threshold_then_correct_metadata_is_returned()
        {
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold - 1, 0));
            var stubRseService = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold + 1);
            var mockPointExclusionService = new MockPointExclusionService(stubCalcService, stubRseService, _settingsManager)
            {
                UseBaseTestMeetsInitialConditions = false
            };
            var bvaTest = TestFactory.CreateBvaTest(3);

            var observedMetadata = mockPointExclusionService.ExcludePoints(bvaTest);

            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                IntermediateMetadata = "-No first individual sample evaluation done because the standard deviation is below the threshold-;",
                NumberOfPointsExcluded = 0,
                ReasonServiceNotRunToCompletion = string.Empty,
                WasServiceRunToCompletion = true,
                LastComputedResidualStandardError = 3.5,
                LastComputedStandardDeviation = 2.9,
                LastComputedTransudationRate = 0.0
            };
            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_the_exclusion_limit_prevents_excluding_a_composite_sample_then_the_service_halts()
        {
            // How this works: Algorithm tries to find a subsample, but that subsample doesn't lower stddev
            // enough to stop looking for more. That subsample is put back in, and the algorithm tries to find
            // a composite sample (i.e., 2 subsamples together). However, the way we have this test set up is 
            // that only one subsample can be excluded (i.e., not high enough for a composite sample).

            // Arrange
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled)).Return(true);
            stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionRseThreshold)).Return(RseThreshold);
            stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(StandardDeviationThreshold);
            stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions)).Return(1);

            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold + 1, 0));
            
            var stubRseService = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold + 1);

            var mockPointExclusionService = new MockPointExclusionService(stubCalcService, stubRseService, stubSettingsManager)
            {
                UseBaseTestMeetsInitialConditions = false
            };
            var bvaTest = TestFactory.CreateBvaTest(3);

            // Act
            mockPointExclusionService.ExcludePoints(bvaTest);

            // Assert
            var expectedInvocations = new List<string> { "CaptureBvaTestState", "InternalExcludePoints", "FindSampleToExclude" };
            CollectionAssert.AreEqual(expectedInvocations, mockPointExclusionService.Invocations);
        }

        [TestMethod]
        public void And_the_exclusion_limit_prevents_excluding_a_composite_sample_then_the_correct_metadata_is_returned()
        {
            // Arrange
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled)).Return(true);
            stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionRseThreshold)).Return(RseThreshold);
            stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(StandardDeviationThreshold);
            stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions)).Return(1);

            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold + 1, 0));

            var stubRseService = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold + 1);

            var mockPointExclusionService = new MockPointExclusionService(stubCalcService, stubRseService, stubSettingsManager)
            {
                UseBaseTestMeetsInitialConditions = false
            };

            var bvaTest = TestFactory.CreateBvaTest(3);

            // Act
            var observedMetadata = mockPointExclusionService.ExcludePoints(bvaTest);

            // Assert
            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                IntermediateMetadata = "-End of finding first individual sample-;-Resetting previously excluded individual sample (UndefinedUndefined) to try finding a composite sample instead-;-No first composite sample evaluation done because doing so would exceed the maximum number of exclusions-;",
                NumberOfPointsExcluded = 0,
                ReasonServiceNotRunToCompletion = string.Empty,
                WasServiceRunToCompletion = true,
                LastComputedResidualStandardError = 3.5,
                LastComputedStandardDeviation = 4.9,
                LastComputedTransudationRate = 0.0
            };
            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_excluding_the_point_lowers_both_stddev_and_RSE_below_thresholds_then_the_service_halts()
        {
            // Arrange
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold + 1, 0))
                           .Repeat.Twice(); // High the first time so that a point is excluded
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold - 1, 0))
                           .Repeat.Twice(); // Low the second time so that the service halts
            var stubRseService = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold + 1)
                          .Repeat.Once(); // High the first time so that a point is excluded
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold - 1)
                          .Repeat.Once(); // Low the second time so that the service halts
            var mockPointExclusionService = new MockPointExclusionService(stubCalcService, stubRseService, _settingsManager)
            {
                UseBaseTestMeetsInitialConditions = false
            };
            var bvaTest = TestFactory.CreateBvaTest(3);

            // Act
            mockPointExclusionService.ExcludePoints(bvaTest);

            // Assert
            var expectedInvocations = new List<string> { "CaptureBvaTestState", "InternalExcludePoints", "FindSampleToExclude" };
            CollectionAssert.AreEqual(expectedInvocations, mockPointExclusionService.Invocations);
        }

        [TestMethod]
        public void And_excluding_the_point_lowers_both_stddev_and_RSE_below_thresholds_then_the_correct_metadata_is_returned()
        {
            // Arrange
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold + 1, 0))
                           .Repeat.Twice(); // High the first time so that a point is excluded
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold - 1, 0))
                           .Repeat.Twice(); // Low the second time so that the service halts
            var stubRseService = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold + 1)
                          .Repeat.Once(); // High the first time so that a point is excluded
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold - 1)
                          .Repeat.Once(); // Low the second time so that the service halts
            var mockPointExclusionService = new MockPointExclusionService(stubCalcService, stubRseService, _settingsManager)
            {
                UseBaseTestMeetsInitialConditions = false
            };
            var bvaTest = TestFactory.CreateBvaTest(3);

            // Act
            var observedMetadata = mockPointExclusionService.ExcludePoints(bvaTest);

            // Assert
            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                IntermediateMetadata = "-End of finding first individual sample-;",
                NumberOfPointsExcluded = 1,
                ReasonServiceNotRunToCompletion = string.Empty,
                WasServiceRunToCompletion = true,
                LastComputedResidualStandardError = 1.5,
                LastComputedStandardDeviation = 2.9,
                LastComputedTransudationRate = 0.0
            };
            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_excluding_the_point_lowers_stddev_below_threshold_and_RSE_to_exact_threshold_then_the_service_halts()
        {
            // The stub services return values *twice* because the point exclusion service has to
            // get statistics on (1) composite samples, and (2) individual samples.

            // Arrange
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold + 1, 0))
                           .Repeat.Twice(); // High the first time so that a point is excluded
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold - 1, 0))
                           .Repeat.Twice(); // Low the second time so that the service halts
            var stubRseService = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold + 1)
                          .Repeat.Once(); // High the first time so that a point is excluded
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold)
                          .Repeat.Once();
            var mockPointExclusionService = new MockPointExclusionService(stubCalcService, stubRseService, _settingsManager)
            {
                UseBaseTestMeetsInitialConditions = false
            };
            var bvaTest = TestFactory.CreateBvaTest(3);

            // Act
            mockPointExclusionService.ExcludePoints(bvaTest);

            // Assert
            var expectedInvocations = new List<string> { "CaptureBvaTestState", "InternalExcludePoints", "FindSampleToExclude" };
            CollectionAssert.AreEqual(expectedInvocations, mockPointExclusionService.Invocations);
        }

        [TestMethod]
        public void And_excluding_the_point_lowers_stddev_below_threshold_and_RSE_to_exact_threshold_then_the_correct_metadata_is_returned()
        {
            // The stub services return values *twice* because the point exclusion service has to
            // get statistics on (1) composite samples, and (2) individual samples.

            // Arrange
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold + 1, 0))
                           .Repeat.Twice(); // High the first time so that a point is excluded
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold - 1, 0))
                           .Repeat.Twice(); // Low the second time so that the service halts
            var stubRseService = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold + 1)
                          .Repeat.Once(); // High the first time so that a point is excluded
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold)
                          .Repeat.Once();
            var mockPointExclusionService = new MockPointExclusionService(stubCalcService, stubRseService, _settingsManager)
            {
                UseBaseTestMeetsInitialConditions = false
            };
            var bvaTest = TestFactory.CreateBvaTest(3);

            // Act
            var observedMetadata = mockPointExclusionService.ExcludePoints(bvaTest);

            // Assert
            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                IntermediateMetadata = "-End of finding first individual sample-;",
                NumberOfPointsExcluded = 1,
                ReasonServiceNotRunToCompletion = string.Empty,
                WasServiceRunToCompletion = true,
                LastComputedResidualStandardError = 2.5,
                LastComputedStandardDeviation = 2.9,
                LastComputedTransudationRate = 0.0
            };
            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_excluding_the_point_lowers_stddev_below_threshold_and_RSE_is_above_threshold_then_the_service_halts()
        {
            // Arrange
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold + 1, 0))
                           .Repeat.Twice(); // High the first time so that a point is excluded
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold - 1, 0))
                           .Repeat.Twice(); // Low the second time so that the service halts
            var stubRseService = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold + 1);
            var mockPointExclusionService = new MockPointExclusionService(stubCalcService, stubRseService, _settingsManager)
            {
                UseBaseTestMeetsInitialConditions = false
            };
            var bvaTest = TestFactory.CreateBvaTest(3);

            // Act
            mockPointExclusionService.ExcludePoints(bvaTest);

            // Assert
            var expectedInvocations = new List<string> { "CaptureBvaTestState", "InternalExcludePoints", "FindSampleToExclude" };
            CollectionAssert.AreEqual(expectedInvocations, mockPointExclusionService.Invocations);
        }

        [TestMethod]
        public void And_excluding_the_point_lowers_stddev_below_threshold_and_RSE_is_above_threshold_then_the_correct_metadata_is_returned()
        {
            // Arrange
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold + 1, 0))
                           .Repeat.Twice(); // High the first time so that a point is excluded
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold - 1, 0))
                           .Repeat.Twice(); // Low the second time so that the service halts
            var stubRseService = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold + 1);
            var mockPointExclusionService = new MockPointExclusionService(stubCalcService, stubRseService, _settingsManager)
            {
                UseBaseTestMeetsInitialConditions = false
            };
            var bvaTest = TestFactory.CreateBvaTest(3);

            // Act
            var observedMetadata = mockPointExclusionService.ExcludePoints(bvaTest);

            // Assert
            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                IntermediateMetadata = "-End of finding first individual sample-;",
                NumberOfPointsExcluded = 1,
                ReasonServiceNotRunToCompletion = string.Empty,
                WasServiceRunToCompletion = true,
                LastComputedResidualStandardError = 3.5,
                LastComputedStandardDeviation = 2.9,
                LastComputedTransudationRate = 0.0
            };
            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_a_point_is_excluded_then_the_intermediate_metadata_is_correct()
        {
            // Arrange
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold + 1, 0))
                           .Repeat.Twice(); // High the first time so that a point is excluded
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold - 1, 0))
                           .Repeat.Twice(); // Low the second time so that the service halts
            var stubRseService = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold + 1)
                          .Repeat.Once(); // High the first time so that a point is excluded
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold - 1)
                          .Repeat.Once(); // Low the second time so that the service halts
            var mockPointExclusionService = new MockPointExclusionService(stubCalcService, stubRseService, _settingsManager)
            {
                UseBaseTestMeetsInitialConditions = false,
                MetadataReturnedByFindSampleToExclude = new List<string> { "Thing 1", "Thing 2" }
            };
            var bvaTest = TestFactory.CreateBvaTest(3);

            // Act
            var observedMetadata = mockPointExclusionService.ExcludePoints(bvaTest);

            // Assert
            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                IntermediateMetadata = "Thing 1;Thing 2;-End of finding first individual sample-;",
                NumberOfPointsExcluded = 1,
                ReasonServiceNotRunToCompletion = string.Empty,
                WasServiceRunToCompletion = true,
                LastComputedResidualStandardError = 1.5,
                LastComputedStandardDeviation = 2.9,
                LastComputedTransudationRate = 0.0
            };
            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_a_point_is_excluded_then_the_point_is_marked_as_autoexcluded()
        {
            // Arrange
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold + 1, 0))
                           .Repeat.Twice(); // High the first time so that a point is excluded
            stubCalcService.Expect(s => s.GetAllResults(null))
                           .IgnoreArguments()
                           .Return(new MeasuredCalcEngineResults(new List<UnadjustedBloodVolumeResult>(), 0, 0, 0, 0, StandardDeviationThreshold - 1, 0))
                           .Repeat.Twice(); // Low the second time so that the service halts
            var stubRseService = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold + 1)
                          .Repeat.Once(); // High the first time so that a point is excluded
            stubRseService.Expect(s => s.CalculateResidualStandardError(null, null, 0, 0))
                          .IgnoreArguments()
                          .Return(RseThreshold - 1)
                          .Repeat.Once(); // Low the second time so that the service halts
            var mockPointExclusionService = new MockPointExclusionService(stubCalcService, stubRseService, _settingsManager)
            {
                UseBaseTestMeetsInitialConditions = false,
            };
            var bvaTest = TestFactory.CreateBvaTest(3);

            // Act
            mockPointExclusionService.ExcludePoints(bvaTest);

            // Assert
            Assert.IsTrue(mockPointExclusionService.BvaSampleReturnedByFindSampleToExclude.IsCountAutoExcluded);
        }
    }
    // ReSharper restore InconsistentNaming
}
