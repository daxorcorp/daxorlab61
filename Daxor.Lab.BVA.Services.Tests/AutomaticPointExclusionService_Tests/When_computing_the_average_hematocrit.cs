﻿using Daxor.Lab.BVA.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_the_average_hematocrit
    {
        [TestMethod]
        public void And_both_hcts_are_not_excluded_and_have_acceptable_values_then_the_average_hct_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = 0.015 };
            var sample2 = new BVASample(1, null) { Hematocrit = 0.045 };

            Assert.AreEqual(0.03, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1s_hct_is_included_with_an_acceptable_value_and_sample2s_hct_is_included_with_a_negative_value_then_sample1s_hct_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = 0.015 };
            var sample2 = new BVASample(1, null) { Hematocrit = -1 };

            Assert.AreEqual(0.015, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1s_hct_is_included_with_an_acceptable_value_and_sample2s_hct_is_excluded_with_an_acceptable_value_then_sample1s_hct_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = 0.015 };
            var sample2 = new BVASample(1, null) { Hematocrit = 0.015, IsHematocritExcluded = true };

            Assert.AreEqual(0.015, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1s_hct_is_included_with_an_acceptable_value_and_sample2s_hct_is_excluded_with_a_negative_value_then_sample1s_hct_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = 0.015 };
            var sample2 = new BVASample(1, null) { Hematocrit = -1, IsHematocritExcluded = true };

            Assert.AreEqual(0.015, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod] 
        public void And_sample1s_hct_is_included_with_a_negative_value_and_sample2s_hct_is_included_with_an_acceptable_value_then_sample2s_hct_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = -1 };
            var sample2 = new BVASample(1, null) { Hematocrit = 0.015 };

            Assert.AreEqual(0.015, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod] 
        public void And_sample1s_hct_is_included_with_a_negative_value_and_sample2s_hct_is_included_with_a_negative_value_then_neg1_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = -1 };
            var sample2 = new BVASample(1, null) { Hematocrit = -1 };

            Assert.AreEqual(-1, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod] 
        public void And_sample1s_hct_is_included_with_a_negative_value_and_sample2s_hct_is_excluded_with_an_acceptable_value_then_neg1_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = -1 };
            var sample2 = new BVASample(1, null) { Hematocrit = 0.015, IsHematocritExcluded = true };

            Assert.AreEqual(-1, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1s_hct_is_included_with_a_negative_value_and_sample2s_hct_is_excluded_with_a_negative_value_then_neg1_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = -1 };
            var sample2 = new BVASample(1, null) { Hematocrit = -1, IsHematocritExcluded = true };

            Assert.AreEqual(-1, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1s_hct_is_excluded_with_an_acceptable_value_and_sample2s_hct_is_included_with_an_acceptable_value_then_sample2s_hct_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = 0.020, IsHematocritExcluded = true };
            var sample2 = new BVASample(1, null) { Hematocrit = 0.015 };

            Assert.AreEqual(0.015, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1s_hct_is_excluded_with_an_acceptable_value_and_sample2s_hct_is_included_with_a_negative_value_then_neg1_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = 0.020, IsHematocritExcluded = true };
            var sample2 = new BVASample(1, null) { Hematocrit = -1 };

            Assert.AreEqual(-1, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1s_hct_is_excluded_with_an_acceptable_value_and_sample2s_hct_is_excluded_with_an_acceptable_value_then_neg1_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = 0.020, IsHematocritExcluded = true };
            var sample2 = new BVASample(1, null) { Hematocrit = 0.015, IsHematocritExcluded = true };

            Assert.AreEqual(-1, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1s_hct_is_excluded_with_an_acceptable_value_and_sample2s_hct_is_excluded_with_a_negative_value_then_neg1_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = 0.020, IsHematocritExcluded = true };
            var sample2 = new BVASample(1, null) { Hematocrit = -1, IsHematocritExcluded = true };

            Assert.AreEqual(-1, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1s_hct_is_excluded_with_a_negative_value_and_sample2s_hct_is_included_with_an_acceptable_value_then_sample2s_hct_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = -1, IsHematocritExcluded = true };
            var sample2 = new BVASample(1, null) { Hematocrit = 0.015 };

            Assert.AreEqual(0.015, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1s_hct_is_excluded_with_a_negative_value_and_sample2s_hct_is_included_with_a_negative_value_then_neg1_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = -1, IsHematocritExcluded = true };
            var sample2 = new BVASample(1, null) { Hematocrit = -1 };

            Assert.AreEqual(-1, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1s_hct_is_excluded_with_a_negative_value_and_sample2s_hct_is_excluded_with_an_acceptable_value_then_neg1_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = -1, IsHematocritExcluded = true };
            var sample2 = new BVASample(1, null) { Hematocrit = 0.015, IsHematocritExcluded = true };

            Assert.AreEqual(-1, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

        [TestMethod]
        public void And_sample1s_hct_is_excluded_with_a_negative_value_and_sample2s_hct_is_excluded_with_a_negative_value_then_neg1_is_returned()
        {
            var sample1 = new BVASample(0, null) { Hematocrit = -1, IsHematocritExcluded = true };
            var sample2 = new BVASample(1, null) { Hematocrit = -1, IsHematocritExcluded = true };

            Assert.AreEqual(-1, AutomaticPointExclusionService.ComputeAverageSampleHematocrit(sample1, sample2));
        }

    }
    // ReSharper restore InconsistentNaming
}
