﻿using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_generating_calc_engine_parameters
    {
        // Not covering if BVATest.Samples is null because BVATest's constructor calls helper methods
        // that build the baseline and standard samples, so other tests (which should pass) would 
        // prove that BVATest.Samples cannot become null. (Also BVATest.Samples has no public setter.)
        //
        // Similarly, there are no tests for whether the standards or baseline samples are missing.

        [TestMethod]
        public void And_the_test_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>AutomaticPointExclusionService.GenerateCalcEngineParameters(null), "bvaTest");
        }

        [TestMethod]
        public void And_the_tube_for_the_test_is_null_then_an_exception_is_thrown()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.Tube = null;

            AssertEx.Throws<NotSupportedException>(()=>AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest));
        }
    }
    // ReSharper restore InconsistentNaming
}
