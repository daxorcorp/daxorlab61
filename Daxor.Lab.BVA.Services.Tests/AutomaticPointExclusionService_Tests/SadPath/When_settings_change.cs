﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_settings_change
    {
        private static IMeasuredCalcEngineService _stubMeasuredCalcEngineService;
        private static IResidualStandardErrorCalculator _stubResidualStandardErrorCalculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _stubMeasuredCalcEngineService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            _stubResidualStandardErrorCalculator = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
        }

        [TestMethod]
        public void And_the_minimum_protocol_is_an_invalid_value_then_the_prior_value_is_unchanged()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            const int originalMinimumPatientProtocol = 5;
            stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMinProtocol))
                               .Return(originalMinimumPatientProtocol).Repeat.Once();
            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            const int invalidMinimumPatientProtocol = -1;
            stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMinProtocol))
                            .Return(invalidMinimumPatientProtocol).Repeat.Once();  // Second call should be a different value

            var wasExceptionThrown = false;
            try
            {
                stubSettingsManager.Raise(m => m.SettingsChanged += null, this,
                                          new SettingsChangedEventArgs(new List<string> { SettingKeys.BvaAutomaticPointExclusionMinProtocol }));
            }
            catch
            {
                wasExceptionThrown = true;
            }
            Assert.AreEqual(originalMinimumPatientProtocol, service.MinimumPatientProtocol);
            Assert.IsTrue(wasExceptionThrown);
        }

        [TestMethod]
        public void And_the_maximum_number_of_exclusions_is_an_invalid_value_then_the_prior_value_is_unchanged()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            const int originalMaximumNumberOfExclusions = 5;
            stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions))
                               .Return(originalMaximumNumberOfExclusions).Repeat.Once();
            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            const int invalidMaximumNumberOfExclusions = -1;
            stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions))
                            .Return(invalidMaximumNumberOfExclusions).Repeat.Once();  // Second call should be a different value

            var wasExceptionThrown = false;
            try
            {
                stubSettingsManager.Raise(m => m.SettingsChanged += null, this,
                                          new SettingsChangedEventArgs(new List<string> { SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions }));
            }
            catch
            {
                wasExceptionThrown = true;
            }
            Assert.AreEqual(originalMaximumNumberOfExclusions, service.MaximumNumberOfExclusions);
            Assert.IsTrue(wasExceptionThrown);
        }

        [TestMethod]
        public void And_the_residual_standard_error_threshold_is_an_invalid_value_then_the_prior_value_is_unchanged()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            const double originalRseThreshold = 0.029;
            stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionRseThreshold))
                               .Return(originalRseThreshold).Repeat.Once();
            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            const double invalidRseThreshold = -0.1;
            stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionRseThreshold))
                            .Return(invalidRseThreshold).Repeat.Once();  // Second call should be a different value

            var wasExceptionThrown = false;
            try
            {
                stubSettingsManager.Raise(m => m.SettingsChanged += null, this,
                                          new SettingsChangedEventArgs(new List<string> { SettingKeys.BvaAutomaticPointExclusionRseThreshold }));
            }
            catch
            {
                wasExceptionThrown = true;
            }
            Assert.AreEqual(originalRseThreshold, service.ResidualStandardErrorThreshold);
            Assert.IsTrue(wasExceptionThrown);
        }
    }
    // ReSharper restore InconsistentNaming
}
