﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_generating_a_collection_of_unadjusted_BVs
    {
        public BVATest _dummyBvaTest;
        public Sample _sample1a;
        public Sample _sample2a;
        public Sample _sample3a;

        [TestInitialize]
        public void TestInitialize()
        {
            _dummyBvaTest = TestFactory.CreateBvaTest(3);
            _sample1a = (from s in _dummyBvaTest.Samples where s.Type == SampleType.Sample1 && s.Range == SampleRange.A select s).FirstOrDefault();
            _sample2a = (from s in _dummyBvaTest.Samples where s.Type == SampleType.Sample2 && s.Range == SampleRange.A select s).FirstOrDefault();
            _sample3a = (from s in _dummyBvaTest.Samples where s.Type == SampleType.Sample3 && s.Range == SampleRange.A select s).FirstOrDefault();

            if (_sample1a == null || _sample2a == null || _sample3a == null)
                Assert.Fail("Cannot find appropriate samples in the dummy test");
        }
        
        [TestMethod]
        public void And_the_calc_engine_results_are_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>AutomaticPointExclusionService.GenerateCollectionOfUnadjustedBloodVolumes(null, TestFactory.CreateBvaTest(3)), "calcEngineResults");
        }

        [TestMethod]
        public void And_the_test_is_null_then_an_exception_is_thrown()
        {
            var dummyResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0);
            AssertEx.ThrowsArgumentNullException(()=>AutomaticPointExclusionService.GenerateCollectionOfUnadjustedBloodVolumes(dummyResults, null), "bvaTest");
        }

        [TestMethod]
        public void And_the_unadjusted_blood_volumes_collection_is_null_then_an_exception_is_thrown()
        {
            var dummyResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0);
            AssertEx.Throws<NotSupportedException>(()=>AutomaticPointExclusionService.GenerateCollectionOfUnadjustedBloodVolumes(dummyResults, TestFactory.CreateBvaTest(3)));
        }

        [TestMethod]
        public void And_the_unadjusted_blood_volumes_contains_a_null_value_then_an_exception_is_thrown()
        {
            var dummyUnadjustedBVResults = new List<UnadjustedBloodVolumeResult>
                {
                    new UnadjustedBloodVolumeResult(_sample1a.InternalId, 1234.56),
                    null,
                    new UnadjustedBloodVolumeResult(_sample3a.InternalId, 4567.89)
                };
            var dummyResults = new MeasuredCalcEngineResults(dummyUnadjustedBVResults, 0, 0, 0, 0, 0, 0);
            AssertEx.Throws<NullReferenceException>(()=>AutomaticPointExclusionService.GenerateCollectionOfUnadjustedBloodVolumes(dummyResults, _dummyBvaTest));
        }

        [TestMethod]
        public void And_the_unadjusted_blood_volume_has_a_sample_ID_not_found_in_the_test_then_an_exception_is_thrown()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            var random = new Random();
            var expectedDictionary = bvaTest.Samples.ToDictionary(sample => sample, sample => random.NextDouble() * 1000);
            var dummyUnadjustedBVResults = expectedDictionary.Keys.Select(sample => new UnadjustedBloodVolumeResult(Guid.NewGuid(), expectedDictionary[sample])).ToList();
            var dummyResults = new MeasuredCalcEngineResults(dummyUnadjustedBVResults, 0, 0, 0, 0, 0, 0);
            
            AssertEx.Throws<NotSupportedException>(()=>AutomaticPointExclusionService.GenerateCollectionOfUnadjustedBloodVolumes(dummyResults, bvaTest));
        }
    }
    // ReSharper restore InconsistentNaming
}
