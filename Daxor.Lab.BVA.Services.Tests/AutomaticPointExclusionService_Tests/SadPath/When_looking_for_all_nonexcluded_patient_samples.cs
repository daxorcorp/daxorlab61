﻿using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_looking_for_all_nonexcluded_patient_samples
    {
        [TestMethod]
        public void And_the_given_test_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>AutomaticPointExclusionService.FindAllNonExcludedPatientSamples(null), "bvaTest");
        }
    }
    // ReSharper restore InconsistentNaming
}
