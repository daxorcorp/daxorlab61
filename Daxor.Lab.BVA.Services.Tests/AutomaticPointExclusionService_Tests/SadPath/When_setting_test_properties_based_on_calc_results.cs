﻿using System;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_test_properties_based_on_calc_results
    {
        [TestMethod]
        public void And_the_given_test_is_null_than_an_exception_is_thrown()
        {
            var dummyResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, 0, 0);
            AssertEx.ThrowsArgumentNullException(()=>AutomaticPointExclusionService.SetTestPropertiesBasedOnCalculationResults(null, dummyResults), "bvaTest");
        }

        [TestMethod]
        public void And_the_given_calc_results_instance_is_null_than_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>AutomaticPointExclusionService.SetTestPropertiesBasedOnCalculationResults(TestFactory.CreateBvaTest(3), null), "calcEngineResults");
        }

        [TestMethod]
        public void And_the_plasma_volume_result_is_NaN_than_an_exception_is_thrown()
        {
            var stubResults = new MeasuredCalcEngineResults(null, 0, double.NaN, 0, 0, 0, 0);
            AssertEx.Throws<NotSupportedException>(()=>AutomaticPointExclusionService.SetTestPropertiesBasedOnCalculationResults(TestFactory.CreateBvaTest(3), stubResults));
        }

        [TestMethod]
        public void And_the_red_cell_volume_result_is_NaN_than_an_exception_is_thrown()
        {
            var stubResults = new MeasuredCalcEngineResults(null, 0, 0, double.NaN, 0, 0, 0);
            AssertEx.Throws<NotSupportedException>(()=>AutomaticPointExclusionService.SetTestPropertiesBasedOnCalculationResults(TestFactory.CreateBvaTest(3), stubResults));
        }

        [TestMethod]
        public void And_the_standard_deviation_is_NaN_than_an_exception_is_thrown()
        {
            var stubResults = new MeasuredCalcEngineResults(null, 0, 0, 0, 0, double.NaN, 0);
            AssertEx.Throws<NotSupportedException>(()=>AutomaticPointExclusionService.SetTestPropertiesBasedOnCalculationResults(TestFactory.CreateBvaTest(3), stubResults));
        }
    }
    // ReSharper restore InconsistentNaming
}
