﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_the_average_sample_count
    {
        [TestMethod]
        public void And_the_first_sample_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>AutomaticPointExclusionService.ComputeAverageSampleCount(null, new BVASample(0, null)), "sampleOne");
        }

        [TestMethod]
        public void And_the_second_sample_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>AutomaticPointExclusionService.ComputeAverageSampleCount(new BVASample(0, null), null), "sampleTwo");
        }
    }
    // ReSharper restore InconsistentNaming
}
