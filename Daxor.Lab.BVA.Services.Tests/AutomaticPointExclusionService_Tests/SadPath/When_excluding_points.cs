﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath.Helpers;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_excluding_points
    {
        private static IMeasuredCalcEngineService _stubMeasuredCalcEngineService;
        private static IResidualStandardErrorCalculator _stubResidualStandardErrorCalculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _stubMeasuredCalcEngineService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            _stubResidualStandardErrorCalculator = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
        }

        [TestMethod]
        public void And_the_service_is_disabled_then_the_correct_metadata_is_returned()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled))
                               .Return(false);
            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator, stubSettingsManager);

            var expectedMetadata = new AutomaticPointExclusionMetadata
                {
                    NumberOfPointsExcluded = 0,
                    WasServiceRunToCompletion = false,
                    ReasonServiceNotRunToCompletion = "Service is not enabled"
                };

            var observedMetadata = service.ExcludePoints(BvaTestFactory.CreateBvaTest(4, 30));

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_the_test_instance_is_null_then_an_exception_is_thrown()
        {
            var service = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator,
                                                             CreateSettingsManagerWithPointExclusionEnabled());

            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var unused = service.ExcludePoints(null);
            }, "test");
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_capturing_exclusion_state_fails_then_the_exception_is_not_caught_and_work_halts()
        {
            var service = new PointExclusionServiceWithBadCapture(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator,
                                                             CreateSettingsManagerWithPointExclusionEnabled());
            var exceptionThrown = false;
            try
            {
                service.ExcludePoints(new BVATest(null));
            }
            catch
            {
                exceptionThrown = true;
                Assert.IsFalse(service.WasInternalExcludePointsInvoked);
                Assert.IsFalse(service.WasInternalExcludePointsInvoked);
            }

            if (!exceptionThrown)
                Assert.Fail("Capturing exclusion state throwing an exception was not handled properly");
        }

        [TestMethod]
        public void And_internal_exclusion_fails_then_original_states_are_restored()
        {
            var service = new PointExclusionServiceWithBadInteralExcludePoints(_stubMeasuredCalcEngineService,
                                                 _stubResidualStandardErrorCalculator,
                                                 CreateSettingsManagerWithPointExclusionEnabled());
            service.ExcludePoints(new BVATest(null));
            Assert.IsTrue(service.WasRestoreInvoked);
        }

        [TestMethod]
        public void And_restoring_exclusion_state_fails_then_the_exception_is_not_caught()
        {
            var service = new PointExclusionServiceWithBadRestore(_stubMeasuredCalcEngineService,
                                                             _stubResidualStandardErrorCalculator,
                                                             CreateSettingsManagerWithPointExclusionEnabled());
            AssertEx.Throws(()=>service.ExcludePoints(new BVATest(null)));
        }

        private static ISettingsManager CreateSettingsManagerWithPointExclusionEnabled()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled))
                               .Return(true);
            return stubSettingsManager;
        }
    }
    // ReSharper restore InconsistentNaming
}
