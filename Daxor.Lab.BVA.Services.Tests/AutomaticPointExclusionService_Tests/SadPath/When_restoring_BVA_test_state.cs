﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath.Helpers;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_restoring_BVA_test_state
    {
        private static AutomaticPointExclusionService _pointExclusionService;
        private static BVATest _bvaTest;
        private static BvaTestState _bvaTestState;
            
        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _pointExclusionService = new AutomaticPointExclusionService(MockRepository.GenerateStub<IMeasuredCalcEngineService>(),
                MockRepository.GenerateStub<IResidualStandardErrorCalculator>(),
                MockRepository.GenerateStub<ISettingsManager>());

            _bvaTest = TestFactory.CreateBvaTest(3);
            var samplesAsList = _bvaTest.Samples.ToList();
            samplesAsList[0].IsCountAutoExcluded = true;  // IsCountExcluded = T, IsCountAutoExcluded = T
            samplesAsList[1].IsCountExcluded = true;      // IsCountExcluded = T, IsCountAutoExcluded = F
            samplesAsList[2].IsCountAutoExcluded = true;
            samplesAsList[2].IsCountExcluded = false;     // IsCountExcluded = F, IsCountAutoExcluded = T

            _bvaTestState = _pointExclusionService.CaptureBvaTestState(_bvaTest);
        }

        [TestMethod]
        public void And_the_test_state_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_pointExclusionService.RestoreBvaTestState(null, _bvaTest), "bvaTestState");
        }

        [TestMethod]
        public void And_the_collection_of_sample_states_has_a_null_member_then_an_exception_is_thrown()
        {
            var sampleExclusionStates = _bvaTestState.SampleExclusionStates.ToList();
            sampleExclusionStates[1] = null;
            AssertEx.Throws<NullReferenceException>(()=>_pointExclusionService.RestoreBvaTestState(new BvaTestState {SampleExclusionStates = sampleExclusionStates}, _bvaTest));
        }

        [TestMethod]
        public void And_the_given_test_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_pointExclusionService.RestoreBvaTestState(_bvaTestState, null), "bvaTest");
        }

        [TestMethod]
        public void And_the_given_test_has_a_null_sample_list_then_an_exception_is_thrown()
        {
            var bvaTest = new BvaTestWithSampleSetter();

            AssertEx.ThrowsArgumentNullException(()=>_pointExclusionService.RestoreBvaTestState(_bvaTestState, bvaTest), "source");
        }

        [TestMethod]
        public void And_the_collection_of_sample_states_differs_in_size_than_the_test_samples_collection_then_an_exception_is_thrown()
        {
            var brokenSampleExclusionStateList = new List<SampleExclusionState> {_bvaTestState.SampleExclusionStates.First()};
            AssertEx.Throws<NotSupportedException>(()=>_pointExclusionService.RestoreBvaTestState(new BvaTestState {SampleExclusionStates = brokenSampleExclusionStateList}, _bvaTest));
        }

        [TestMethod]
        public void And_the_collection_of_sample_states_has_an_id_not_in_the_test_samples_collection_then_an_exception_is_thrown()
        {
            var firstState = _bvaTestState.SampleExclusionStates.First();
            var firstGuid = firstState.SampleId;
            firstState.SampleId = Guid.NewGuid();

            AssertEx.Throws<NotSupportedException>(()=>_pointExclusionService.RestoreBvaTestState(_bvaTestState, _bvaTest));

            firstState.SampleId = firstGuid; // restore the change
       }

       [TestMethod]
       public void And_the_given_test_has_a_null_measured_volumes_collection_then_an_exception_is_thrown()
       {
           var bvaTest = TestFactory.CreateBvaTest(3);
           var bvaTestState = _pointExclusionService.CaptureBvaTestState(bvaTest);
           bvaTest.Volumes[BloodType.Measured] = null;

           AssertEx.Throws<NotSupportedException>(()=>_pointExclusionService.RestoreBvaTestState(bvaTestState, bvaTest));
       }

       [TestMethod]
       public void And_the_given_test_state_has_a_null_sample_state_collection_then_an_exception_is_thrown()
       {
           var bvaTest = TestFactory.CreateBvaTest(3);
           var bvaTestState = _pointExclusionService.CaptureBvaTestState(bvaTest);
           bvaTestState.SampleExclusionStates = null;

           AssertEx.Throws<NotSupportedException>(()=>_pointExclusionService.RestoreBvaTestState(bvaTestState, bvaTest));
       }
    }
    // ReSharper restore InconsistentNaming
}
