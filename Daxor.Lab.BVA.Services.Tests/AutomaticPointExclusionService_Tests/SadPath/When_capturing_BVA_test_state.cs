﻿using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath.Helpers;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_capturing_BVA_test_state
    {
        private static AutomaticPointExclusionService _pointExclusionService;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _pointExclusionService = new AutomaticPointExclusionService(MockRepository.GenerateStub<IMeasuredCalcEngineService>(),
                MockRepository.GenerateStub<IResidualStandardErrorCalculator>(),
                MockRepository.GenerateStub<ISettingsManager>());
        }

        [TestMethod]
        public void And_the_given_test_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_pointExclusionService.CaptureBvaTestState(null), "bvaTest");
        }

        [TestMethod]
        public void And_the_given_test_has_a_null_sample_list_then_an_exception_is_thrown()
        {
            var bvaTest = new BvaTestWithSampleSetter();

           AssertEx.ThrowsArgumentNullException(()=> _pointExclusionService.CaptureBvaTestState(bvaTest), "source");
        }

        [TestMethod]
        public void And_the_given_test_has_a_sample_that_is_null_then_an_exception_is_thrown()
        {
            var bvaTest = new BvaTestWithSampleSetter();
            bvaTest.SetSamples(new [] {new BVASample(1, null), null, new BVASample(3, null)});

            AssertEx.Throws<NullReferenceException>(()=>_pointExclusionService.CaptureBvaTestState(bvaTest));
        }

        [TestMethod]
        public void And_the_given_test_has_a_null_measured_volumes_collection_then_an_exception_is_thrown()
        {
            var bvaTest = new BvaTestWithSampleSetter();
            bvaTest.Volumes[BloodType.Measured] = null;

            AssertEx.Throws<NotSupportedException>(()=>_pointExclusionService.CaptureBvaTestState(bvaTest));
        }
    }
    // ReSharper restore InconsistentNaming
}
