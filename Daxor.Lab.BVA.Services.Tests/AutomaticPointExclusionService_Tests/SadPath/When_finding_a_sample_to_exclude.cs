﻿using System;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_finding_a_sample_to_exclude
    {
        [TestMethod]
        public void And_finding_all_nonexcluded_patient_samples_fails_then_the_exception_is_not_caught()
        {
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            var stubRseCalc = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var service = new AutomaticPointExclusionService(stubCalcService, stubRseCalc, stubSettingsManager);

            BVASample outSample;
            AssertEx.Throws(()=>service.FindSampleToExclude(null, out outSample));
        }

        [TestMethod]
        public void And_the_measured_calc_engine_throws_an_exception_then_the_exception_is_not_caught()
        {
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(c => c.GetAllResults(null)).IgnoreArguments().Throw(new Exception());
            var stubRseCalc = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var service = new AutomaticPointExclusionService(stubCalcService, stubRseCalc, stubSettingsManager);

            BVASample outSample;
            AssertEx.Throws(()=>service.FindSampleToExclude(TestFactory.CreateBvaTest(3), out outSample));
        }

        [TestMethod]
        public void And_setting_test_properties_based_on_calc_results_fails_then_the_exception_is_not_caught()
        {
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            stubCalcService.Expect(c => c.GetAllResults(null)).IgnoreArguments().Return(null);
            var stubRseCalc = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var service = new AutomaticPointExclusionService(stubCalcService, stubRseCalc, stubSettingsManager);

            BVASample outSample;
            AssertEx.Throws(()=>service.FindSampleToExclude(TestFactory.CreateBvaTest(3), out outSample));
        }

        [TestMethod]
        public void And_generating_the_collection_of_unadjusted_volumes_fails_then_the_exception_is_not_caught()
        {
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            var calcResults = new MeasuredCalcEngineResults(null, 1234, 2345, 3456, 0.123, 0.234, 0.345, 0.456);
            stubCalcService.Expect(c => c.GetAllResults(null)).IgnoreArguments().Return(calcResults);

            var stubRseCalc = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var service = new AutomaticPointExclusionService(stubCalcService, stubRseCalc, stubSettingsManager);

            BVASample outSample;
            AssertEx.Throws(()=>service.FindSampleToExclude(TestFactory.CreateBvaTest(3), out outSample));
        }

        [TestMethod]
        public void And_the_RSE_calc_service_throws_an_exception_then_the_exception_is_not_caught()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            var unadjustedResults = (from sample in bvaTest.Samples where sample.Type != SampleType.Background && sample.Type != SampleType.Baseline && sample.Type != SampleType.Standard select new UnadjustedBloodVolumeResult(sample.InternalId, 123)).ToList();
            var stubCalcService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            var calcResults = new MeasuredCalcEngineResults(unadjustedResults, 1234, 2345, 3456, 0.123, 0.234, 0.345, 0.456);
            stubCalcService.Expect(c => c.GetAllResults(null)).IgnoreArguments().Return(calcResults);

            var stubRseCalc = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseCalc.Expect(c => c.CalculateResidualStandardError(null, null, 0, 0))
                       .IgnoreArguments()
                       .Throw(new Exception());

            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();

            var service = new AutomaticPointExclusionService(stubCalcService, stubRseCalc, stubSettingsManager);

            BVASample outSample;
            AssertEx.Throws(()=>service.FindSampleToExclude(bvaTest, out outSample));
        }
    }
    // ReSharper restore InconsistentNaming
}
