﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Core;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath.Helpers
{
    internal class BvaTestWithSampleSetter : BVATest
    {
        private IEnumerable<BVASample> _samples;

        internal BvaTestWithSampleSetter() : base(null)
        {
        }

        public override IEnumerable<BVASample> Samples
        {
            get { return _samples; }
        }

        internal void SetSamples(IEnumerable<BVASample> newSamples)
        {
            _samples = newSamples;
        }
    }
}