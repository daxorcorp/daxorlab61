﻿using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath.Helpers
{
    internal class PointExclusionServiceWithBadRestore : AutomaticPointExclusionService
    {
        internal PointExclusionServiceWithBadRestore(IMeasuredCalcEngineService measuredCalcEngineService,
                                                                  IResidualStandardErrorCalculator residualStandardErrorCalculator,
                                                                  ISettingsManager settingsManager)
            : base(measuredCalcEngineService, residualStandardErrorCalculator, settingsManager)
        {
        }

        public override BvaTestState CaptureBvaTestState(BVATest bvaTest)
        {
            return null;
        }

        internal override AutomaticPointExclusionMetadata InternalExcludePoints(BVATest test)
        {
            throw new Exception();
        }

        public override void RestoreBvaTestState(BvaTestState bvaTestState, BVATest bvaTest)
        {
            throw new Exception();
        }

        protected override bool TestMeetsInitialConditions(BVATest bvaTest, out AutomaticPointExclusionMetadata failureMetadata)
        {
            failureMetadata = null;
            return true;
        }
    }
}
