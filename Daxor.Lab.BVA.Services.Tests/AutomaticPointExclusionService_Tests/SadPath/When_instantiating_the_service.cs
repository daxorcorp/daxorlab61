﻿using System;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_instantiating_the_service
    {
        private static ISettingsManager _stubSettingsManager;
        private static IMeasuredCalcEngineService _stubMeasuredCalcEngineService;
        private static IResidualStandardErrorCalculator _stubResidualStandardErrorCalculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _stubMeasuredCalcEngineService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            _stubResidualStandardErrorCalculator = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
        }

        [TestMethod]
        public void And_the_measured_calc_engine_instance_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var unused = new AutomaticPointExclusionService(null, _stubResidualStandardErrorCalculator,
                    _stubSettingsManager);
            }, "measuredCalcEngineService");
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_the_RSE_calc_instance_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var unused = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService, null,
                    _stubSettingsManager);
            }, "residualStandardErrorCalculator");
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_the_settings_manager_instance_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var unused = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                    _stubResidualStandardErrorCalculator, null);
            }, "settingsManager");
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_the_minimum_protocol_is_negative_then_an_exception_is_thrown()
        {
            _stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMinProtocol)).Return(-1);

            // ReSharper disable UnusedVariable
            AssertEx.Throws<NotSupportedException>(
                () =>
                {
                    var unused = new AutomaticPointExclusionService(_stubMeasuredCalcEngineService,
                        _stubResidualStandardErrorCalculator, _stubSettingsManager);
                });
            // ReSharper restore UnusedVariable
        }
    }
    // ReSharper restore InconsistentNaming
}
