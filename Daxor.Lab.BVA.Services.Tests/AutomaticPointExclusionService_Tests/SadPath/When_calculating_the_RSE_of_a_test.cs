﻿using System;
using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_calculating_the_RSE_of_a_test
    {
        private static IMeasuredCalcEngineService _stubMeasuredCalcEngineService;
        private static IResidualStandardErrorCalculator _stubResidualStandardErrorCalculator;
        private static ISettingsManager _stubSettingsManager;
        private static MockPointExclusionService _pointExclusionService;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _stubMeasuredCalcEngineService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            _stubResidualStandardErrorCalculator = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _pointExclusionService = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);
        }

        [TestMethod]
        public void And_the_test_is_null_then_an_exception_is_thrown()
        {
            var stubVolumes = new Dictionary<BVASample, double>();
            AssertEx.ThrowsArgumentNullException(()=>_pointExclusionService.CalculateResidualStandardErrorOfTest(null, stubVolumes), "bvaTest");
        }

        [TestMethod]
        public void And_the_collection_of_volumes_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_pointExclusionService.CalculateResidualStandardErrorOfTest(TestFactory.CreateBvaTest(3), null), "individualVolumes");
        }

        [TestMethod]
        public void And_the_RSE_calculator_throws_an_exception_then_the_exception_is_not_caught()
        {
            var stubRseCalc = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            stubRseCalc.Expect(c => c.CalculateResidualStandardError(null, null, 0, 0))
                       .IgnoreArguments()
                       .Throw(new NotSupportedException());
            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, stubRseCalc, _stubSettingsManager);

            var stubVolumes = new Dictionary<BVASample, double>();
            AssertEx.Throws<NotSupportedException>(()=>service.CalculateResidualStandardErrorOfTest(TestFactory.CreateBvaTest(3), stubVolumes));
        }
    }
    // ReSharper restore InconsistentNaming
}
