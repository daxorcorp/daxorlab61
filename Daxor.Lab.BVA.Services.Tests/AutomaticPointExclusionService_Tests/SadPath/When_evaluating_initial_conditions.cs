﻿using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_initial_conditions
    {
        private static ISettingsManager _stubSettingsManager;
        private static IMeasuredCalcEngineService _stubMeasuredCalcEngineService;
        private static IResidualStandardErrorCalculator _stubResidualStandardErrorCalculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled)).Return(true);
            _stubMeasuredCalcEngineService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            _stubResidualStandardErrorCalculator = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
        }

        [TestMethod]
        public void And_the_standard_deviation_is_positive_infinity_then_work_halts()
        {
            _stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(12.34).Repeat.Once();

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService,
                                                        _stubResidualStandardErrorCalculator, _stubSettingsManager);
            
            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.StandardDevResult = double.PositiveInfinity;

            service.ExcludePoints(bvaTest);

            Assert.AreEqual(0, service.Invocations.Count);
        }

        [TestMethod]
        public void And_the_standard_deviation_is_positive_infinity_then_the_correct_metadata_is_returned()
        {
            _stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(12.34).Repeat.Once();

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.StandardDevResult = double.PositiveInfinity;

            var expectedMetadata = new AutomaticPointExclusionMetadata
                {
                    NumberOfPointsExcluded = 0,
                    WasServiceRunToCompletion = false,
                    ReasonServiceNotRunToCompletion = "Standard deviation of the test was infinity"
                };

            var observedMetadata = service.ExcludePoints(bvaTest);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_the_standard_deviation_is_negative_infinity_then_work_halts()
        {
            _stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(12.34).Repeat.Once();

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.StandardDevResult = double.NegativeInfinity;

            service.ExcludePoints(bvaTest);

            Assert.AreEqual(0, service.Invocations.Count);
        }

        [TestMethod]
        public void And_the_standard_deviation_is_negative_infinity_then_the_correct_metadata_is_returned()
        {
            _stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(12.34).Repeat.Once();

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.StandardDevResult = double.NegativeInfinity;

            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                NumberOfPointsExcluded = 0,
                WasServiceRunToCompletion = false,
                ReasonServiceNotRunToCompletion = "Standard deviation of the test was infinity"
            };

            var observedMetadata = service.ExcludePoints(bvaTest);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_the_standard_deviation_is_NaN_then_work_halts()
        {
            _stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(12.34).Repeat.Once();

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.StandardDevResult = double.NaN;

            service.ExcludePoints(bvaTest);

            Assert.AreEqual(0, service.Invocations.Count);
        }

        [TestMethod]
        public void And_the_standard_deviation_is_NaN_then_the_correct_metadata_is_returned()
        {
            _stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(12.34).Repeat.Once();

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.StandardDevResult = double.NaN;

            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                NumberOfPointsExcluded = 0,
                WasServiceRunToCompletion = false,
                ReasonServiceNotRunToCompletion = "Standard deviation of the test was NaN"
            };

            var observedMetadata = service.ExcludePoints(bvaTest);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_the_standard_deviation_is_less_the_threshold_then_work_halts()
        {
            const double stdDevThreshold = 12.34;
            _stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(stdDevThreshold).Repeat.Once();

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.StandardDevResult = stdDevThreshold - 0.001;

            service.ExcludePoints(bvaTest);

            Assert.AreEqual(0, service.Invocations.Count);
        }

        [TestMethod]
        public void And_the_standard_deviation_is_less_the_threshold_then_the_correct_metadata_is_returned()
        {
            const double stdDevThreshold = 12.34;
            _stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(stdDevThreshold).Repeat.Once();

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.StandardDevResult = stdDevThreshold - 0.001;

            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                NumberOfPointsExcluded = 0,
                WasServiceRunToCompletion = false,
                ReasonServiceNotRunToCompletion = "Standard deviation is at or below the threshold (" + stdDevThreshold + ")"
            };

            var observedMetadata = service.ExcludePoints(bvaTest);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_the_standard_deviation_is_at_the_threshold_then_work_halts()
        {
            const double stdDevThreshold = 12.34;
            _stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(stdDevThreshold).Repeat.Once();

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.StandardDevResult = stdDevThreshold;

            service.ExcludePoints(bvaTest);

            Assert.AreEqual(0, service.Invocations.Count);
        }

        [TestMethod]
        public void And_the_standard_deviation_is_at_the_threshold_then_the_correct_metadata_is_returned()
        {
            const double stdDevThreshold = 12.34;
            _stubSettingsManager.Expect(m => m.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold)).Return(stdDevThreshold).Repeat.Once();

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.StandardDevResult = stdDevThreshold;

            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                NumberOfPointsExcluded = 0,
                WasServiceRunToCompletion = false,
                ReasonServiceNotRunToCompletion = "Standard deviation is at or below the threshold (" + stdDevThreshold + ")"
            };

            var observedMetadata = service.ExcludePoints(bvaTest);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_the_protocol_is_less_than_the_minimum_protocol_then_the_correct_metadata_is_returned()
        {
            const int minPatientProtocol = 5;
            _stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMinProtocol)).Return(minPatientProtocol).Repeat.Once();

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(minPatientProtocol - 1);

            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                NumberOfPointsExcluded = 0,
                WasServiceRunToCompletion = false,
                ReasonServiceNotRunToCompletion = "Patient protocol (number of composite samples) is below the threshold (" + minPatientProtocol + ")"
            };

            var observedMetadata = service.ExcludePoints(bvaTest);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_the_protocol_is_below_the_minimum_then_work_halts()
        {
            const int minPatientProtocol = 5;
            _stubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMinProtocol)).Return(minPatientProtocol).Repeat.Once();

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager); 

            var bvaTest = TestFactory.CreateBvaTest(minPatientProtocol - 1);
            
            service.ExcludePoints(bvaTest);

            Assert.AreEqual(0, service.Invocations.Count);
        }

        [TestMethod]
        public void And_the_test_has_insufficient_data_then_the_correct_metadata_is_returned()
        {
            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(4);
            bvaTest.HasSufficientData = false;

            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                NumberOfPointsExcluded = 0,
                WasServiceRunToCompletion = false,
                ReasonServiceNotRunToCompletion = "Test does not have sufficient data to compute a total blood volume"
            };

            var observedMetadata = service.ExcludePoints(bvaTest);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_the_test_has_insufficient_data_then_work_halts()
        {
            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(4);
            bvaTest.HasSufficientData = false;

            service.ExcludePoints(bvaTest);

            Assert.AreEqual(0, service.Invocations.Count);
        }

        [TestMethod]
        public void And_the_test_has_any_samples_whose_counts_have_been_excluded_then_the_correct_metadata_is_returned()
        {
            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(4);
            var sample1a = (from s in bvaTest.Samples
                            where s.Range == SampleRange.A && s.Type == SampleType.Sample1
                            select s).FirstOrDefault();
            var sample4b = (from s in bvaTest.Samples
                            where s.Range == SampleRange.B && s.Type == SampleType.Sample4
                            select s).FirstOrDefault();
            if (sample1a == null || sample4b == null) Assert.Fail("Can't deal with null samples from the test factory");
            sample1a.IsCountExcluded = true;
            sample4b.IsCountExcluded = true;

            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                NumberOfPointsExcluded = 0,
                WasServiceRunToCompletion = false,
                ReasonServiceNotRunToCompletion = "Test has at least one sample whose user-excluded and auto-excluded states do not match"
            };

            var observedMetadata = service.ExcludePoints(bvaTest);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_the_test_has_any_samples_whose_counts_have_been_excluded_then_work_halts()
        {
            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(4);
            var sample1a = (from s in bvaTest.Samples
                            where s.Range == SampleRange.A && s.Type == SampleType.Sample1
                            select s).FirstOrDefault();
            var sample4b = (from s in bvaTest.Samples
                            where s.Range == SampleRange.B && s.Type == SampleType.Sample4
                            select s).FirstOrDefault();
            if (sample1a == null || sample4b == null) Assert.Fail("Can't deal with null samples from the test factory");
            sample1a.IsCountExcluded = true;
            sample4b.IsCountExcluded = true;

            service.ExcludePoints(bvaTest);

            Assert.AreEqual(0, service.Invocations.Count);
        }

        [TestMethod]
        public void And_the_test_has_any_samples_whose_counts_have_been_autoexcluded_then_the_correct_metadata_is_returned()
        {
            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(4);
            var sample1a = (from s in bvaTest.Samples
                            where s.Range == SampleRange.A && s.Type == SampleType.Sample1
                            select s).FirstOrDefault();
            var sample4b = (from s in bvaTest.Samples
                            where s.Range == SampleRange.B && s.Type == SampleType.Sample4
                            select s).FirstOrDefault();
            if (sample1a == null || sample4b == null) Assert.Fail("Can't deal with null samples from the test factory");
            sample1a.IsCountAutoExcluded = true;
            sample1a.IsCountExcluded = false; // This is because auto-excluded marks excluded as well
            sample4b.IsCountAutoExcluded = true;
            sample4b.IsCountExcluded = false;

            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                NumberOfPointsExcluded = 0,
                WasServiceRunToCompletion = false,
                ReasonServiceNotRunToCompletion = "Test has at least one sample whose user-excluded and auto-excluded states do not match"
            };

            var observedMetadata = service.ExcludePoints(bvaTest);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_the_test_has_any_samples_whose_counts_have_been_autoexcluded_then_work_halts()
        {
            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(4);
            var sample1a = (from s in bvaTest.Samples
                            where s.Range == SampleRange.A && s.Type == SampleType.Sample1
                            select s).FirstOrDefault();
            var sample4b = (from s in bvaTest.Samples
                            where s.Range == SampleRange.B && s.Type == SampleType.Sample4
                            select s).FirstOrDefault();
            if (sample1a == null || sample4b == null) Assert.Fail("Can't deal with null samples from the test factory");
            sample1a.IsCountAutoExcluded = true;
            sample1a.IsCountExcluded = false; // This is because auto-excluded marks excluded as well
            sample4b.IsCountAutoExcluded = true;
            sample4b.IsCountExcluded = false;

            service.ExcludePoints(bvaTest);

            Assert.AreEqual(0, service.Invocations.Count);
        }

        [TestMethod]
        public void And_the_test_has_a_sample_with_both_hcts_excluded_then_the_correct_metadata_is_returned()
        {
            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(4);
            var sample1a = (from s in bvaTest.Samples
                            where s.Range == SampleRange.A && s.Type == SampleType.Sample1
                            select s).FirstOrDefault();
            var sample1b = (from s in bvaTest.Samples
                            where s.Range == SampleRange.B && s.Type == SampleType.Sample1
                            select s).FirstOrDefault();
            if (sample1a == null || sample1b == null) Assert.Fail("Can't deal with null samples from the test factory");
            sample1a.IsHematocritExcluded = true;
            sample1b.IsHematocritExcluded = true;
            
            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                NumberOfPointsExcluded = 0,
                WasServiceRunToCompletion = false,
                ReasonServiceNotRunToCompletion = "Test has at least one sample where both hematocrits are excluded"
            };

            var observedMetadata = service.ExcludePoints(bvaTest);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_the_test_has_a_sample_with_both_hcts_excluded_then_work_halts()
        {
            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, _stubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(4);
            var sample1a = (from s in bvaTest.Samples
                            where s.Range == SampleRange.A && s.Type == SampleType.Sample1
                            select s).FirstOrDefault();
            var sample1b = (from s in bvaTest.Samples
                            where s.Range == SampleRange.B && s.Type == SampleType.Sample1
                            select s).FirstOrDefault();
            if (sample1a == null || sample1b == null) Assert.Fail("Can't deal with null samples from the test factory");
            sample1a.IsHematocritExcluded = true;
            sample1b.IsHematocritExcluded = true;

            service.ExcludePoints(bvaTest);

            Assert.AreEqual(0, service.Invocations.Count);
        }

        [TestMethod]
        public void And_the_test_has_too_few_samples_then_the_correct_metadata_is_returned()
        {
            const int maximumNumberOfExclusions = 6;
            var localStubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            localStubSettingsManager.Stub(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions)).Return(maximumNumberOfExclusions);
            localStubSettingsManager.Stub(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled)).Return(true);
            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, localStubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(4);
            var expectedMetadata = new AutomaticPointExclusionMetadata
            {
                NumberOfPointsExcluded = 0,
                WasServiceRunToCompletion = false,
                ReasonServiceNotRunToCompletion = "Test could potentially have fewer than " + 
                    AutomaticPointExclusionService.MinimumSampleCountAfterExclusion +
                    " individual sample(s) remaining after possible exclusion of " + maximumNumberOfExclusions + " sample(s)"
            };

            var observedMetadata = service.ExcludePoints(bvaTest);
            
            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_the_test_has_too_few_samples_then_work_halts()
        {
            const int maximumNumberOfExclusions = 6;
            var localStubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            localStubSettingsManager.Expect(m => m.GetSetting<int>(SettingKeys.BvaAutomaticPointExclusionMaxNumberOfExclusions)).Return(maximumNumberOfExclusions);
            localStubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled)).Return(true);
            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, _stubResidualStandardErrorCalculator, localStubSettingsManager);

            var bvaTest = TestFactory.CreateBvaTest(4);

            service.ExcludePoints(bvaTest);

            Assert.AreEqual(0, service.Invocations.Count);
        }
    }
    // ReSharper restore InconsistentNaming
}
