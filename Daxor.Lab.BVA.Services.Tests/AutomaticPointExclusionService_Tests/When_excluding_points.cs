﻿using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_excluding_points
    {
        [TestMethod]
        public void Then_the_exclusion_states_are_being_stored_before_performing_internal_exclusion()
        {
            var stubMeasuredCalcEngineService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            var stubResidualStandardErrorCalculator = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled))
                               .Return(true);
            var mockService = new MockPointExclusionService(stubMeasuredCalcEngineService,
                                                            stubResidualStandardErrorCalculator, stubSettingsManager)
                {
                    UseBaseTestMeetsInitialConditions = false,
                    UseBaseInternalExcludePoints = false
                };

            mockService.ExcludePoints(new BVATest(null));

            var expectedOrder = new List<string> { "CaptureBvaTestState", "InternalExcludePoints" };
            CollectionAssert.AreEqual(expectedOrder, mockService.Invocations);
        }

        [TestMethod]
        public void Then_the_exclusion_states_arent_restored_if_internal_exclusion_was_successful()
        {
            var stubMeasuredCalcEngineService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            var stubResidualStandardErrorCalculator = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(SettingKeys.BvaAutomaticPointExclusionEnabled))
                               .Return(true);

            var mockService = new MockPointExclusionService(stubMeasuredCalcEngineService,
                                                            stubResidualStandardErrorCalculator, stubSettingsManager);

            CollectionAssert.DoesNotContain(mockService.Invocations, "RestoreSampleExclusionStates");
        }
    }
    // ReSharper restore InconsistentNaming
}
