﻿using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.DomainModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_generating_calc_engine_parameters
    {
        [TestMethod]
        public void Then_the_reference_volume_is_correct()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.RefVolume = 1000;
            
            var calcParams = AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest);

            Assert.AreEqual(bvaTest.RefVolume, calcParams.ReferenceVolume);
        }

        [TestMethod]
        public void Then_the_anticoagulant_factor_is_correct()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.Tube.AnticoagulantFactor = 0.99;

            var calcParams = AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest);

            Assert.AreEqual(bvaTest.Tube.AnticoagulantFactor, calcParams.AnticoagulantFactor);
        }

        [TestMethod]
        public void Then_the_background_counts_are_correct()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            bvaTest.BackgroundCount = 15;
            bvaTest.BackgroundTimeInSeconds = 30;
            bvaTest.SampleDurationInSeconds = 60;
            bvaTest.ComputeTestAdjustedBackgroundCounts();
            const int expectedCpm = 30;

            var calcParams = AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest);

            Assert.AreEqual(expectedCpm, calcParams.Background);
        }

        [TestMethod]
        public void Then_the_standard_counts_are_correct()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            var standardA = (from s in bvaTest.Samples where s.Type == SampleType.Standard && s.Range == SampleRange.A select s).FirstOrDefault();
            var standardB = (from s in bvaTest.Samples where s.Type == SampleType.Standard && s.Range == SampleRange.B select s).FirstOrDefault();
            if (standardA == null || standardB == null)
                Assert.Fail("Could not find Standard A or B in the test samples collection");
            standardA.Counts = 10000;
            standardB.Counts = 10001;
            const double expectedAverageStandardCounts = 10000.5;

            var calcParams = AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest);

            Assert.AreEqual(expectedAverageStandardCounts, calcParams.StandardCounts);
        }

        [TestMethod]
        public void Then_the_baseline_counts_are_correct()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            var baselineA = (from s in bvaTest.Samples where s.Type == SampleType.Baseline && s.Range == SampleRange.A select s).FirstOrDefault();
            var baselineB = (from s in bvaTest.Samples where s.Type == SampleType.Baseline && s.Range == SampleRange.B select s).FirstOrDefault();
            if (baselineA == null || baselineB == null)
                Assert.Fail("Could not find Baseline A or B in the test samples collection");
            baselineA.Counts = 30;
            baselineB.Counts = 33;
            const double expectedBaselineCounts = 31.5;

            var calcParams = AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest);

            Assert.AreEqual(expectedBaselineCounts, calcParams.BaselineCounts);
        }

        [TestMethod]
        public void Then_the_baseline_hematocrit_is_correct()
        {
            var bvaTest = TestFactory.CreateBvaTest(3);
            var baselineA = (from s in bvaTest.Samples where s.Type == SampleType.Baseline && s.Range == SampleRange.A select s).FirstOrDefault();
            var baselineB = (from s in bvaTest.Samples where s.Type == SampleType.Baseline && s.Range == SampleRange.B select s).FirstOrDefault();
            if (baselineA == null || baselineB == null)
                Assert.Fail("Could not find Baseline A or B in the test samples collection");
            baselineA.Hematocrit = 0.030;
            baselineB.Hematocrit = 0.033;
            const double expectedBaselineHematocrit = 0.0315;

            var calcParams = AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest);

            Assert.AreEqual(expectedBaselineHematocrit, calcParams.BaselineHematocrit);
        }

        [TestMethod]
        public void Then_the_points_collection_does_not_contain_samples_with_negative_counts()
        {
            var bvaTest = CreateBvaTestWithPositiveCountsAndPostInjectionTimes(3);
            var sample1a = (from s in bvaTest.Samples where s.Type == SampleType.Sample1 && s.Range == SampleRange.A select s).FirstOrDefault();
            if (sample1a == null)
                Assert.Fail("Could not find Sample 1A in the test samples collection");
            sample1a.Counts = -1;

            var calcParams = AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest);

            Assert.IsTrue(calcParams.Points.All(p => p.Counts >= 0));
        }

        [TestMethod]
        public void Then_the_points_collection_contains_only_patient_samples()
        {
            const int protocol = 6;
            var bvaTest = CreateBvaTestWithPositiveCountsAndPostInjectionTimes(protocol);
            
            var calcParams = AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest);

            Assert.AreEqual(protocol * 2, calcParams.Points.Count());
        }

        [TestMethod]
        public void Then_the_points_collection_contains_only_nonzero_post_injection_times()
        {
            var bvaTest = CreateBvaTestWithPositiveCountsAndPostInjectionTimes(3);
            var sample1a = (from s in bvaTest.Samples where s.Type == SampleType.Sample1 && s.Range == SampleRange.A select s).FirstOrDefault();
            if (sample1a == null)
                Assert.Fail("Could not find Sample 1A in the test samples collection");
            sample1a.PostInjectionTimeInSeconds = 0;

            var calcParams = AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest);

            Assert.IsTrue(calcParams.Points.All(p => p.TimePostInjectionInSeconds > 0));
        }

        [TestMethod]
        public void Then_the_points_collection_contains_no_negative_post_injection_times()
        {
            var bvaTest = CreateBvaTestWithPositiveCountsAndPostInjectionTimes(3);
            var sample1a = (from s in bvaTest.Samples where s.Type == SampleType.Sample1 && s.Range == SampleRange.A select s).FirstOrDefault();
            if (sample1a == null)
                Assert.Fail("Could not find Sample 1A in the test samples collection");
            sample1a.PostInjectionTimeInSeconds = -1;

            var calcParams = AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest);

            Assert.IsTrue(calcParams.Points.All(p => p.TimePostInjectionInSeconds > 0));
        }

        [TestMethod]
        public void Then_the_points_collection_contains_no_points_with_counts_excluded()
        {
            var bvaTest = CreateBvaTestWithPositiveCountsAndPostInjectionTimes(3);
            var sample1a = (from s in bvaTest.Samples where s.Type == SampleType.Sample1 && s.Range == SampleRange.A select s).FirstOrDefault();
            if (sample1a == null)
                Assert.Fail("Could not find Sample 1A in the test samples collection");
            sample1a.IsCountAutoExcluded = true;

            var calcParams = AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest);

            Assert.IsTrue(calcParams.Points.All(p => !p.AreBothCountsExcluded));
        }

        [TestMethod]
        public void Then_the_points_collection_contains_no_points_that_are_entirely_excluded()
        {
            var bvaTest = CreateBvaTestWithPositiveCountsAndPostInjectionTimes(3);
            var sample1a = (from s in bvaTest.Samples where s.Type == SampleType.Sample1 && s.Range == SampleRange.A select s).FirstOrDefault();
            if (sample1a == null)
                Assert.Fail("Could not find Sample 1A in the test samples collection");
            sample1a.IsExcluded = true;

            var calcParams = AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest);

            Assert.IsTrue(calcParams.Points.All(p => !p.IsPointRejected));
        }

        [TestMethod]
        public void Then_the_points_collection_is_correct()
        {
            var bvaTest = CreateBvaTestWithPositiveCountsAndPostInjectionTimes(3);
            var sample1a = (from s in bvaTest.Samples where s.Type == SampleType.Sample1 && s.Range == SampleRange.A select s).FirstOrDefault();
            var sample1b = (from s in bvaTest.Samples where s.Type == SampleType.Sample1 && s.Range == SampleRange.B select s).FirstOrDefault();
            var sample2a = (from s in bvaTest.Samples where s.Type == SampleType.Sample2 && s.Range == SampleRange.A select s).FirstOrDefault();
            var sample2b = (from s in bvaTest.Samples where s.Type == SampleType.Sample2 && s.Range == SampleRange.B select s).FirstOrDefault();
            var sample3a = (from s in bvaTest.Samples where s.Type == SampleType.Sample3 && s.Range == SampleRange.A select s).FirstOrDefault();
            var sample3b = (from s in bvaTest.Samples where s.Type == SampleType.Sample3 && s.Range == SampleRange.B select s).FirstOrDefault();

            SetSampleProperties(sample1a, sample1b, sample2a, sample2b, sample3a, sample3b);
            var expectedBvPoints = GenerateBvPointList(sample1a, sample1b, sample2a, sample2b, sample3a, sample3b);

            var calcParams = AutomaticPointExclusionService.GenerateCalcEngineParameters(bvaTest);

            CollectionAssert.AreEquivalent(expectedBvPoints, calcParams.Points);
        }

        private static BVATest CreateBvaTestWithPositiveCountsAndPostInjectionTimes(int protocol)
        {
            var bvaTest = TestFactory.CreateBvaTest(protocol);
            foreach (var sample in bvaTest.Samples)
            {
                sample.Counts = 40;
                sample.PostInjectionTimeInSeconds = 1;
            }
            return bvaTest;
        }

        private static void SetSampleProperties(BVASample sample1a, BVASample sample1b, BVASample sample2a, BVASample sample2b,
                                                BVASample sample3a, BVASample sample3b)
        {
            sample1a.PostInjectionTimeInSeconds = 10;
            sample1a.Counts = 10;
            sample1a.Hematocrit = 0.10;

            sample1b.IsHematocritExcluded = true;
            sample1b.PostInjectionTimeInSeconds = 20;
            sample1b.Counts = 20;
            sample1b.Hematocrit = 0.20;

            sample2a.PostInjectionTimeInSeconds = 30;
            sample2a.Counts = 30;
            sample2a.Hematocrit = 0.30;

            sample2b.PostInjectionTimeInSeconds = 40;
            sample2b.Counts = 40;
            sample2b.Hematocrit = 0.40;

            sample3a.PostInjectionTimeInSeconds = 50;
            sample3a.Counts = 50;
            sample3a.Hematocrit = 0.50;

            sample3b.PostInjectionTimeInSeconds = 60;
            sample3b.Counts = 60;
            sample3b.Hematocrit = 0.60;
        }


        private static List<BloodVolumePoint> GenerateBvPointList(BVASample sample1a, BVASample sample1b, BVASample sample2a, BVASample sample2b,
                                                BVASample sample3a, BVASample sample3b)
        {
            return new List<BloodVolumePoint>
                {
                    new BloodVolumePoint(sample1a.InternalId, false, false, false, 10, 10, 0.1, 0),
                    new BloodVolumePoint(sample1b.InternalId, false, true, false, 20, 20, 0.2, 0),
                    new BloodVolumePoint(sample2a.InternalId, false, false, false, 30, 30, 0.3, 0),
                    new BloodVolumePoint(sample2b.InternalId, false, false, false, 40, 40, 0.4, 0),
                    new BloodVolumePoint(sample3a.InternalId, false, false, false, 50, 50, 0.5, 0),
                    new BloodVolumePoint(sample3b.InternalId, false, false, false, 60, 60, 0.6, 0),
                };
        }
    }
    // ReSharper restore InconsistentNaming
}
