﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.AutomaticPointExclusionService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_calculating_the_RSE_of_a_test
    {
        private static IMeasuredCalcEngineService _stubMeasuredCalcEngineService;
        private static ISettingsManager _stubSettingsManager;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _stubMeasuredCalcEngineService = MockRepository.GenerateStub<IMeasuredCalcEngineService>();
            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
        }

        [TestMethod]
        public void Then_the_value_returned_comes_from_the_given_RSE_calculator()
        {
            const double expectedRse = 0.1234;
            var stubRseCalc = MockRepository.GenerateStub<IResidualStandardErrorCalculator>();
            var stubVolumes = new Dictionary<BVASample, double>();
            stubRseCalc.Expect(c => c.CalculateResidualStandardError(null, null, 0, 0)).IgnoreArguments().Return(expectedRse);

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, stubRseCalc, _stubSettingsManager);
            Assert.AreEqual(expectedRse, service.CalculateResidualStandardErrorOfTest(TestFactory.CreateBvaTest(3), stubVolumes));
        }

        [TestMethod]
        public void And_the_individual_volumes_are_greater_than_zero_then_the_inputs_to_the_RSE_calculator_are_correct()
        {
            const double expectedSlope = 0.1234;

            var bvaTest = CreateBvaTestWithPostInjectionTimes();
            var positiveVolumeLookup = CreatePositiveVolumeLookup(bvaTest);
            var expectedPostInjectionTimesInMinutes = ComputeExpectedPostInjectionTimesInMinutes(bvaTest);
            var expectedSampleVolumes = ComputeExpectedVolumes(positiveVolumeLookup);
            var expectedYIntercept = Math.Log(bvaTest.Volumes[BloodType.Measured].WholeCount);

            var mockRseCalc = MockRepository.GenerateMock<IResidualStandardErrorCalculator>();
            mockRseCalc.Expect(
                c => c.CalculateResidualStandardError(expectedPostInjectionTimesInMinutes, expectedSampleVolumes,
                        expectedSlope, expectedYIntercept)).Return(0);

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, mockRseCalc, _stubSettingsManager);

            service.CalculateResidualStandardErrorOfTest(bvaTest, positiveVolumeLookup);

            mockRseCalc.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_an_individual_volume_is_zero_then_the_inputs_to_the_RSE_calculator_are_correct()
        {
            const double expectedSlope = 0.1234;

            var bvaTest = CreateBvaTestWithPostInjectionTimes();
            var positiveVolumeLookup = CreateVolumeLookupWithOneZeroVolume(bvaTest);
            var expectedPostInjectionTimesInMinutes = ComputeExpectedPostInjectionTimesInMinutes(bvaTest);
            var expectedSampleVolumes = ComputeExpectedVolumes(positiveVolumeLookup);
            var expectedYIntercept = Math.Log(bvaTest.Volumes[BloodType.Measured].WholeCount);

            var mockRseCalc = MockRepository.GenerateMock<IResidualStandardErrorCalculator>();
            mockRseCalc.Expect(
                c => c.CalculateResidualStandardError(expectedPostInjectionTimesInMinutes, expectedSampleVolumes,
                        expectedSlope, expectedYIntercept)).Return(0);

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, mockRseCalc, _stubSettingsManager);

            service.CalculateResidualStandardErrorOfTest(bvaTest, positiveVolumeLookup);

            mockRseCalc.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_an_individual_volume_is_negative_then_the_inputs_to_the_RSE_calculator_are_correct()
        {
            const double expectedSlope = 0.1234;

            var bvaTest = CreateBvaTestWithPostInjectionTimes();
            var positiveVolumeLookup = CreateVolumeLookupWithOneNegativeVolume(bvaTest);
            var expectedPostInjectionTimesInMinutes = ComputeExpectedPostInjectionTimesInMinutes(bvaTest);
            var expectedSampleVolumes = ComputeExpectedVolumes(positiveVolumeLookup);
            var expectedYIntercept = Math.Log(bvaTest.Volumes[BloodType.Measured].WholeCount);

            var mockRseCalc = MockRepository.GenerateMock<IResidualStandardErrorCalculator>();
            mockRseCalc.Expect(
                c => c.CalculateResidualStandardError(expectedPostInjectionTimesInMinutes, expectedSampleVolumes,
                        expectedSlope, expectedYIntercept)).Return(0);

            var service = new MockPointExclusionService(_stubMeasuredCalcEngineService, mockRseCalc, _stubSettingsManager);

            service.CalculateResidualStandardErrorOfTest(bvaTest, positiveVolumeLookup);

            mockRseCalc.VerifyAllExpectations();
        }

        //If negative individual volume, treats as zero when passed to RSECalc
        //If zero individual volume, treats as zero when passed to RSECalc
        
        private static BVATest CreateBvaTestWithPostInjectionTimes()
        {
            const double slope = 0.1234;
            const int sample1PostInjectionTimeInSeconds = 720;
            const int sample2PostInjectionTimeInSeconds = 1080;
            const int sample3PostInjectionTimeInSeconds = 1440;

            var bvaTest = TestFactory.CreateBvaTest(3);
            var sample1 = from s in bvaTest.Samples where s.Type == SampleType.Sample1 select s;
            foreach (var sample in sample1)
                sample.PostInjectionTimeInSeconds = sample1PostInjectionTimeInSeconds;

            var sample2 = from s in bvaTest.Samples where s.Type == SampleType.Sample2 select s;
            foreach (var sample in sample2)
                sample.PostInjectionTimeInSeconds = sample2PostInjectionTimeInSeconds;
            
            var sample3 = from s in bvaTest.Samples where s.Type == SampleType.Sample3 select s;
            foreach (var sample in sample3)
                sample.PostInjectionTimeInSeconds = sample3PostInjectionTimeInSeconds;

            bvaTest.TransudationRateResult = slope;
            bvaTest.Volumes[BloodType.Measured].PlasmaCount = 500;
            bvaTest.Volumes[BloodType.Measured].RedCellCount = 67;
            
            return bvaTest;
        }

        private static Dictionary<BVASample, double> CreatePositiveVolumeLookup(Test<BVASample> bvaTest)
        {
            var individualVolumes = new Dictionary<BVASample, double>();

            var sample1 = from s in bvaTest.Samples where s.Type == SampleType.Sample1 select s;
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var sample in sample1)
                individualVolumes.Add(sample, sample.Range == SampleRange.A ? 1000 : 1001);
            
            var sample2 = from s in bvaTest.Samples where s.Type == SampleType.Sample2 select s;
            foreach (var sample in sample2)
                individualVolumes.Add(sample, sample.Range == SampleRange.A ? 2000 : 2001);
            
            var sample3 = from s in bvaTest.Samples where s.Type == SampleType.Sample3 select s;
            foreach (var sample in sample3)
                individualVolumes.Add(sample, sample.Range == SampleRange.A ? 3000 : 3001);

            return individualVolumes;
        }

        private static Dictionary<BVASample, double> CreateVolumeLookupWithOneZeroVolume(Test<BVASample> bvaTest)
        {
            var individualVolumes = new Dictionary<BVASample, double>();

            var sample1 = from s in bvaTest.Samples where s.Type == SampleType.Sample1 select s;
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var sample in sample1)
                individualVolumes.Add(sample, sample.Range == SampleRange.A ? 1000 : 1001);

            var sample2 = from s in bvaTest.Samples where s.Type == SampleType.Sample2 select s;
            foreach (var sample in sample2)
                individualVolumes.Add(sample, sample.Range == SampleRange.A ? 2000 : 2001);

            var sample3 = from s in bvaTest.Samples where s.Type == SampleType.Sample3 select s;
            foreach (var sample in sample3)
                individualVolumes.Add(sample, sample.Range == SampleRange.A ? 0 : 3001);

            return individualVolumes;
        }

        private static Dictionary<BVASample, double> CreateVolumeLookupWithOneNegativeVolume(Test<BVASample> bvaTest)
        {
            var individualVolumes = new Dictionary<BVASample, double>();

            var sample1 = from s in bvaTest.Samples where s.Type == SampleType.Sample1 select s;
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var sample in sample1)
                individualVolumes.Add(sample, sample.Range == SampleRange.A ? 1000 : 1001);

            var sample2 = from s in bvaTest.Samples where s.Type == SampleType.Sample2 select s;
            foreach (var sample in sample2)
                individualVolumes.Add(sample, sample.Range == SampleRange.A ? 2000 : 2001);

            var sample3 = from s in bvaTest.Samples where s.Type == SampleType.Sample3 select s;
            foreach (var sample in sample3)
                individualVolumes.Add(sample, sample.Range == SampleRange.A ? -1 : 3001);

            return individualVolumes;
        }

        private static IEnumerable<double> ComputeExpectedPostInjectionTimesInMinutes(Test<BVASample> bvaTest)
        {
            var sample1 = from s in bvaTest.Samples where s.Type == SampleType.Sample1 select s;
            var expectedPostInjectionTimesInMinutes = sample1.Select(sample => sample.PostInjectionTimeInSeconds/60.0).ToList();

            var sample2 = from s in bvaTest.Samples where s.Type == SampleType.Sample2 select s;
            expectedPostInjectionTimesInMinutes.AddRange(sample2.Select(sample => sample.PostInjectionTimeInSeconds/60.0));

            var sample3 = from s in bvaTest.Samples where s.Type == SampleType.Sample3 select s;
            expectedPostInjectionTimesInMinutes.AddRange(sample3.Select(sample => sample.PostInjectionTimeInSeconds/60.0));

            return expectedPostInjectionTimesInMinutes;
        }

        private static IEnumerable<double> ComputeExpectedVolumes(Dictionary<BVASample, double> volumes)
        {
            return volumes.Keys.Select(sample => volumes[sample] > 0 ? Math.Log(volumes[sample]) : 0).ToList();
        }

    }
    // ReSharper restore InconsistentNaming
}
