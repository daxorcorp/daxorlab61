﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.BVA.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.BestFitLinePointsCalculator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_best_fit_line_points
    {
        private static BestFitLinePointsCalculator _calculator;
        private static IEnumerable<BVACompositeSample> _samples;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _calculator = new BestFitLinePointsCalculator();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _samples = CreateSamples();
        }

        [TestMethod]
        public void Then_only_two_points_are_returned()
        {
            var points = _calculator.CalculateBestFitLinePoints(_samples, 1, 1);

            Assert.AreEqual(2, points.Count(), "The best fit line should only have two points");
        }

        [TestMethod]
        public void Then_the_first_points_independent_value_is_zero()
        {
            var points = _calculator.CalculateBestFitLinePoints(_samples, 1, 1);

            Assert.AreEqual(0, points.First().IndependentValue, "Independent (x) value of the first point should be zero");
        }

        [TestMethod]
        public void Then_the_first_points_dependent_value_is_the_total_blood_volume()
        {
            const int totalBloodVolume = 3456;
            var points = _calculator.CalculateBestFitLinePoints(_samples, 1, totalBloodVolume);

            Assert.AreEqual(totalBloodVolume, points.First().DependentValue, "Dependent (y) value of the first point should be the total blood volume");
        }

        [TestMethod]
        public void Then_the_second_points_independent_value_is_the_highest_post_injection_time()
        {
            var samples = CreateSamples(10 * 60, 30 * 60, 20 * 60);  // Purposely out of order to show that order doesn't matter to the algorithm
            var points = _calculator.CalculateBestFitLinePoints(samples, 1, 1);

            Assert.AreEqual(30, points.Last().IndependentValue, "Independent (x) value of the first point should be the max post-injection time");
        }

        [TestMethod]
        public void Then_the_second_points_dependent_value_is_correct()
        {
            var samples = CreateSamples(720, 1440, 2160);
            var transudationRates = new List<double> {0.01, 0.01, 0.015, 0.02};  // These are in percent
            var totalBloodVolumes = new List<double> {3456, 4567, 3456, 4567};
            var expectedDependentValues = new List<double> {4954, 6546, 5931, 9383};

            var actualDependentValues = transudationRates.Select((t, i) => Math.Round(_calculator.CalculateBestFitLinePoints(samples, t, totalBloodVolumes[i]).Last().DependentValue, 0)).ToList();

            var message = new StringBuilder();
            var foundMismatch = false;
            for (var i = 0; i < expectedDependentValues.Count; i++)
            {
                if (!(Math.Abs(expectedDependentValues[i] - actualDependentValues[i]) > 1E-100)) continue;
                
                foundMismatch = true;
                message.Append("Expected value " + expectedDependentValues[i] + " but got " +
                               actualDependentValues[i] + "; ");
            }

            if (foundMismatch)
                Assert.Fail(message.ToString());
        }

        private static IEnumerable<BVACompositeSample> CreateSamples(int firstPostInjectionTime = 0, int secondPostInjectionTime = 0, int thirdPostInjectionTime = 0)
        {
            return new List<BVACompositeSample>
                {
                    new BVACompositeSample(null,
                                           new BVASample(1, null) {Range = SampleRange.A, Type = SampleType.Sample1, PostInjectionTimeInSeconds = firstPostInjectionTime },
                                           new BVASample(2, null) {Range = SampleRange.B, Type = SampleType.Sample1, PostInjectionTimeInSeconds = firstPostInjectionTime },
                                           null),
                    new BVACompositeSample(null,
                                           new BVASample(3, null) {Range = SampleRange.A, Type = SampleType.Sample2, PostInjectionTimeInSeconds = secondPostInjectionTime },
                                           new BVASample(4, null) {Range = SampleRange.B, Type = SampleType.Sample2, PostInjectionTimeInSeconds = secondPostInjectionTime },
                                           null),
                    new BVACompositeSample(null,
                                           new BVASample(5, null) {Range = SampleRange.A, Type = SampleType.Sample3, PostInjectionTimeInSeconds = thirdPostInjectionTime },
                                           new BVASample(6, null) {Range = SampleRange.B, Type = SampleType.Sample3, PostInjectionTimeInSeconds = thirdPostInjectionTime },
                                           null)
                };
        }
    }
    // ReSharper restore InconsistentNaming
}
