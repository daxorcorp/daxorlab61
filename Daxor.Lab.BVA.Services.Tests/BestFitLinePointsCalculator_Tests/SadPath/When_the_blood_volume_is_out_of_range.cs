﻿using System;
using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.BestFitLinePointsCalculator_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_blood_volume_is_out_of_range
    {
        private static BestFitLinePointsCalculator _calculator;
        private static List<BVACompositeSample> _samples;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _calculator = new BestFitLinePointsCalculator();

            _samples = new List<BVACompositeSample>
                {
                    new BVACompositeSample(null,
                                           new BVASample(1, null) {Range = SampleRange.A, Type = SampleType.Sample1},
                                           new BVASample(2, null) {Range = SampleRange.B, Type = SampleType.Sample1},
                                           null),
                    new BVACompositeSample(null,
                                           new BVASample(3, null) {Range = SampleRange.A, Type = SampleType.Sample2},
                                           new BVASample(4, null) {Range = SampleRange.B, Type = SampleType.Sample2},
                                           null)
                };
        }

        [TestMethod]
        public void Because_its_positive_infinity_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculateBestFitLinePoints(_samples, 1, Double.PositiveInfinity));
        }

        [TestMethod]
        public void Because_its_negative_infinity_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculateBestFitLinePoints(_samples, 1, Double.NegativeInfinity));
        }

        [TestMethod]
        public void Because_its_NaN_infinity_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculateBestFitLinePoints(_samples, 1, Double.NaN));
        }
    }
    // ReSharper restore InconsistentNaming
}
