﻿using System;
using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.BestFitLinePointsCalculator_Tests.SadPath
{
    [TestClass]
    // ReSharper disable InconsistentNaming
    public class When_there_are_problems_with_the_samples_list
    {
        private static BestFitLinePointsCalculator _calculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _calculator = new BestFitLinePointsCalculator();
        }

        [TestMethod]
        public void Because_the_list_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_calculator.CalculateBestFitLinePoints(null, 1, 1), "source");
        }

        [TestMethod]
        public void Because_the_list_of_samples_is_empty_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentException>(()=>_calculator.CalculateBestFitLinePoints(new List<BVACompositeSample>(), 1, 1));
        }

        [TestMethod]
        public void Because_the_list_of_samples_has_only_member_then_an_exception_is_thrown()
        {
            var singleSample = new BVACompositeSample(null, 
                new BVASample(1, null) { Range = SampleRange.A, Type = SampleType.Sample1 },
                new BVASample(2, null) { Range = SampleRange.B, Type = SampleType.Sample1 }, null);
            AssertEx.Throws<ArgumentException>(()=>_calculator.CalculateBestFitLinePoints(new List<BVACompositeSample> { singleSample }, 1, 1));
        }

        [TestMethod]
        public void Because_one_of_the_samples_is_null_then_an_exception_is_thrown()
        {
            var singleSample = new BVACompositeSample(null,
                new BVASample(1, null) { Range = SampleRange.A, Type = SampleType.Sample1 },
                new BVASample(2, null) { Range = SampleRange.B, Type = SampleType.Sample1 }, null);
            AssertEx.ThrowsArgumentNullException(()=>_calculator.CalculateBestFitLinePoints(new List<BVACompositeSample> { singleSample, null }, 1, 1), "compositeSamples");
        }

        [TestMethod]
        public void Because_multiple_samples_are_null_then_an_exception_is_thrown()
        {
            var singleSample = new BVACompositeSample(null,
                new BVASample(1, null) { Range = SampleRange.A, Type = SampleType.Sample1 },
                new BVASample(2, null) { Range = SampleRange.B, Type = SampleType.Sample1 }, null);
            AssertEx.ThrowsArgumentNullException(()=>_calculator.CalculateBestFitLinePoints(new List<BVACompositeSample> { null, singleSample, null }, 1, 1), "compositeSamples");
        }
    }
    // ReSharper restore InconsistentNaming
}
