﻿using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.PredictedValuesCalculator_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_theres_a_problem_with_the_list
    {
        private static PredictedValuesCalculator _calculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _calculator = new PredictedValuesCalculator();
        }

        [TestMethod]
        public void Because_the_list_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_calculator.CalculatePredictedValues(null, 1, 1), "source");
        }

        [TestMethod]
        public void Because_one_value_is_positive_infinity_then_an_exception_is_thrown()
        {
            var list = new[] { 1.0, Double.PositiveInfinity };

            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculatePredictedValues(list, 1, 1));
        }

        [TestMethod]
        public void Because_one_value_is_negative_infinity_then_an_exception_is_thrown()
        {
            var list = new[] { 1.0, Double.PositiveInfinity };

            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculatePredictedValues(list, 1, 1));
        }

        [TestMethod]
        public void Because_mulitple_values_are_infinity_then_an_exception_is_thrown()
        {
            // Already have tests for +/- infinity, so we'll just test positive infinities here.

            var list = new[] { 1.0, Double.PositiveInfinity, 3.0, Double.PositiveInfinity };

            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculatePredictedValues(list, 1, 1));
        }

        [TestMethod]
        public void Because_one_value_is_NaN_then_an_exception_is_thrown()
        {
            var list = new[] { 1.0, Double.NaN };

            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculatePredictedValues(list, 1, 1));
        }

        [TestMethod]
        public void Because_mulitple_values_are_NaN_then_an_exception_is_thrown()
        {
            var list = new[] { 1.0, Double.NaN, 3.0, Double.NaN };

            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculatePredictedValues(list, 1, 1));
        }
    }
    // ReSharper restore InconsistentNaming
}
