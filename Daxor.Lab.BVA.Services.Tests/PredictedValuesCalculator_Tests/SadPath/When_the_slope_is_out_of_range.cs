﻿using System;
using System.Collections.Generic;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.PredictedValuesCalculator_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_slope_is_out_of_range
    {
        private static PredictedValuesCalculator _calculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _calculator = new PredictedValuesCalculator();
        }

        [TestMethod]
        public void Because_it_is_positive_infinity_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculatePredictedValues(new List<double>(), double.PositiveInfinity, 1));
        }

        [TestMethod]
        public void Because_it_is_negative_infinity_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculatePredictedValues(new List<double>(), double.NegativeInfinity, 1));
        }

        [TestMethod]
        public void Because_it_is_NaN_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentOutOfRangeException>(()=>_calculator.CalculatePredictedValues(new List<double>(), double.NaN, 1));
        }
    }
    // ReSharper restore InconsistentNaming
}
