﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.PredictedValuesCalculator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_predicted_values
    {
        private static PredictedValuesCalculator _calculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _calculator = new PredictedValuesCalculator();
        }

        [TestMethod]
        public void And_the_list_of_independent_values_is_empty_then_an_empty_list_is_returned()
        {
            var predictedValues = _calculator.CalculatePredictedValues(new List<double>(), 1, 1);
            Assert.AreEqual(0, predictedValues.Count());
        }

        [TestMethod]
        public void Then_the_number_of_returned_values_matches_the_number_of_given_independent_values()
        {
            var independentValues = new List<double> {1.0, 2.0, 3.0};
            var predictedValues = _calculator.CalculatePredictedValues(independentValues, 1, 1);
            Assert.AreEqual(independentValues.Count, predictedValues.Count());
        }

        [TestMethod]
        public void Then_the_predicted_values_are_correct()
        {
            var independentValues = new List<double> { 1.12, 2.39, -5.76, 43.1 };
            var listOfSlopes = new List<double> {3.41, -0.02, 11.87};
            var listOfYIntercepts = new List<double> {54.2, 0, -2.87};

            var predictedValuesLists = new List<List<double>>
                {
                    new List<double> { 58.019, 62.350, 34.558, 201.171},
                    new List<double> { -0.022, -0.048, 0.115, -0.862},
                    new List<double> { 10.424, 25.499, -71.241, 508.727},
                };

            var message = new StringBuilder();
            var foundMismatch = false;
            var numTrials = predictedValuesLists.Count;
            for (var i = 0; i < numTrials; i++)
            {
                var roundedResults = _calculator.CalculatePredictedValues(independentValues, listOfSlopes[i], listOfYIntercepts[i])
                                        .Select(result => Math.Round(result, 3)).ToList();

                if (AreListsEqual(roundedResults, predictedValuesLists[i])) continue;

                foundMismatch = true;
                message.Append("Expected values " + ListToString(predictedValuesLists[i]) + " but got " +
                               ListToString(roundedResults) + "; ");
            }

            if (foundMismatch)
                Assert.Fail(message.ToString());
        }

        private static bool AreListsEqual(IEnumerable<double> list1, IEnumerable<double> list2)
        {
            var list1AsArray = list1 as double[] ?? list1.ToArray();
            var list1Size = list1AsArray.Count();
            var list2AsArray = list2 as double[] ?? list2.ToArray();
            if (list1Size != list2AsArray.Count()) return false;

            for (var i = 0; i < list1Size; i++)
                if (Math.Abs(list1AsArray[i] - list2AsArray[i]) > 1E-30) return false;

            return true;
        }

        private static string ListToString(IEnumerable<double> list)
        {
            var toReturn = new StringBuilder();
            toReturn.Append("{ ");
            foreach (var item in list) toReturn.Append(item + " ");
            toReturn.Append("}");
            return toReturn.ToString();
        }
    }
    // ReSharper restore InconsistentNaming
}
