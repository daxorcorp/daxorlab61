using System;
using Daxor.Lab.Database;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Domain.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.DataMapper_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_the_patient_record_to_a_model
    {
        [TestMethod]
        public void And_the_given_record_is_null_then_an_empty_model_is_returned()
        {
            var observedModel = DataMapper.RecordToModel((PatientRecord) null);

            Assert.AreEqual(Guid.Empty, observedModel.InternalId);
        }

        [TestMethod]
        public void Then_the_internal_ID_matches_the_patient_ID()
        {
            var guid = Guid.NewGuid();
            var patientRecord = new PatientRecord { PID = guid };

            var observedResult = DataMapper.RecordToModel(patientRecord);

            Assert.AreEqual(guid, observedResult.InternalId);
        }

        [TestMethod]
        public void Then_the_first_name_matches_the_patient_first_name()
        {
            const string name = "name";
            var patientRecord = new PatientRecord {FirstName = name };

            var observedResult = DataMapper.RecordToModel(patientRecord);

            Assert.AreEqual(name, observedResult.FirstName);
        }
        
        [TestMethod]
        public void Then_the_last_name_matches_the_patient_last_name()
        {
            const string name = "name";
            var patientRecord = new PatientRecord { LastName = name };

            var observedResult = DataMapper.RecordToModel(patientRecord);

            Assert.AreEqual(name, observedResult.LastName);
        }

        [TestMethod]
        public void Then_the_middle_name_matches_the_patient_middle_name()
        {
            const string name = "J.";
            var patientRecord = new PatientRecord { MiddleName = name };

            var observedResult = DataMapper.RecordToModel(patientRecord);

            Assert.AreEqual(name, observedResult.MiddleName);
        }

        [TestMethod]
        public void And_the_gender_is_male_then_the_patient_gender_is_male()
        {
            var patientRecord = new PatientRecord { Gender = Sex.Male };

            var observedResult = DataMapper.RecordToModel(patientRecord);

            Assert.AreEqual(GenderType.Male, observedResult.Gender);
        }

        [TestMethod]
        public void And_the_gender_is_female_then_the_patient_gender_is_female()
        {
            var patientRecord = new PatientRecord { Gender = Sex.Female };

            var observedResult = DataMapper.RecordToModel(patientRecord);

            Assert.AreEqual(GenderType.Female, observedResult.Gender);
        }

        [TestMethod]
        public void And_the_gender_is_unknown_then_the_patient_gender_is_unknown()
        {
            var patientRecord = new PatientRecord { Gender = Sex.Unknown};

            var observedResult = DataMapper.RecordToModel(patientRecord);

            Assert.AreEqual(GenderType.Unknown, observedResult.Gender);
        }

        [TestMethod]
        public void And_the_gender_is_not_valid_then_the_patient_gender_is_unknown()
        {
            var patientRecord = new PatientRecord { Gender = (Sex)69 };

            var observedResult = DataMapper.RecordToModel(patientRecord);

            Assert.AreEqual(GenderType.Unknown, observedResult.Gender);
        }

		[TestMethod]
		public void Then_the_amputee_status_is_true_then_the_patient_amputee_status_is_true()
		{
			var patientRecord = new PatientRecord { IsAmputee = true };

			var observedResult = DataMapper.RecordToModel(patientRecord);

			Assert.IsTrue(observedResult.IsAmputee);
		}

		[TestMethod]
		public void Then_the_amputee_status_is_false_then_the_patient_amputee_status_is_false()
		{
			var patientRecord = new PatientRecord { IsAmputee = false };

			var observedResult = DataMapper.RecordToModel(patientRecord);

			Assert.IsFalse(observedResult.IsAmputee);
		}

        [TestMethod]
        public void Then_the_hospital_patient_ID_matches_the_patient_hospital_ID()
        {
            const string hospitalId = "whatever";
            var patientRecord = new PatientRecord { PatientHospitalId = hospitalId };

            var observedResult = DataMapper.RecordToModel(patientRecord);

            Assert.AreEqual(hospitalId, observedResult.HospitalPatientId);
        }

        [TestMethod]
        public void And_the_date_of_birth_is_the_earliest_possible_then_the_patient_date_of_birth_is_null()
        {
            var earlyDate = new DateTime(1753, 1, 1);

            var patientRecord = new PatientRecord {DOB = earlyDate};

            var observedResult = DataMapper.RecordToModel(patientRecord);

            Assert.AreEqual(null, observedResult.DateOfBirth);
        }

        [TestMethod]
        public void And_the_date_of_birth_is_normal_then_the_patient_date_of_birth_is_returned()
        {
            var normalDate = new DateTime(1981,4,26);

            var patientRecord = new PatientRecord { DOB = normalDate };

            var observedResult = DataMapper.RecordToModel(patientRecord);

            Assert.AreEqual(normalDate, observedResult.DateOfBirth);
        }
    }
    // ReSharper restore InconsistentNaming
}
