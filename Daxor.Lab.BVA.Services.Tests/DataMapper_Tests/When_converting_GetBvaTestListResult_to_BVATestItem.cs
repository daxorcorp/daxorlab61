using System;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Domain.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.DataMapper_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_GetBvaTestListResult_to_BVATestItem
    {
        [TestMethod]
        public void Then_the_test_ID_matches_the_given_test_ID()
        {
            var expectedId = Guid.NewGuid();
            var listResult = new GetBvaTestListResult {TEST_ID = expectedId};

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual(expectedId, observedTestItem.TestID);
        }

        [TestMethod]
        public void Then_WasSaved_is_true()
        {
            var listResult = new GetBvaTestListResult();

            var observedTestItem = DataMapper.RecordToModel(listResult);

           Assert.IsTrue(observedTestItem.WasSaved);
        }

        [TestMethod]
        public void Then_the_created_date_matches_the_given_created_date()
        {
            var createdDate = DateTime.Now;
            var listResult = new GetBvaTestListResult { TEST_DATE = createdDate };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual(createdDate, observedTestItem.CreatedDate);
        }

        [TestMethod]
        public void And_the_date_of_birth_is_the_earliest_possible_date_then_the_date_of_birth_is_set_to_null()
        {
            var dateOfBirth = new DateTime(1753, 1, 1);
            var listResult = new GetBvaTestListResult { DOB = dateOfBirth };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual(null, observedTestItem.PatientDOB);
        }

        [TestMethod]
        public void And_the_date_of_birth_is_valid_then_the_date_of_birth_is_set()
        {
            var dateOfBirth = DateTime.Now;
            var listResult = new GetBvaTestListResult { DOB = dateOfBirth };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual(dateOfBirth, observedTestItem.PatientDOB);
        }

        [TestMethod]
        public void Then_the_full_name_matches_the_formatted_name()
        {
            const string firstName = "firstName";
            const string middleName = "middleName";
            const string lastName = "lastName";

            var listResult = new GetBvaTestListResult
            {
                FIRST_NAME = firstName,
                MIDDLE_NAME = middleName,
                LAST_NAME = lastName
            };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual(lastName + ", " + firstName + " " + middleName, observedTestItem.PatientFullName);
        }

        [TestMethod]
        public void Then_the_patient_hospital_ID_matches_the_hospital_ID()
        {
            const string hospitalId = "ID";
            var listResult = new GetBvaTestListResult { PATIENT_HOSPITAL_ID = hospitalId };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual(hospitalId, observedTestItem.PatientHospitalID);
        }

        [TestMethod]
        public void Then_the_patient_ID_matches_the_patient_ID()
        {
            var patientId = Guid.NewGuid();
            var listResult = new GetBvaTestListResult { PID = patientId };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual(patientId, observedTestItem.PatientPID);
        }

        [TestMethod]
        public void Then_patient_ID2_matches_the_ID2()
        {
            const string id2 = "whatever";
            var listResult = new GetBvaTestListResult { ID2 = id2 };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual(id2, observedTestItem.TestID2);
        }

        [TestMethod]
        public void And_the_test_status_is_undefined_then_test_status_matches()
        {
            const int undefinedTestStatusId = -1;

            var listResult = new GetBvaTestListResult { STATUS_ID = undefinedTestStatusId };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual((TestStatus)undefinedTestStatusId, observedTestItem.Status);
        }

        [TestMethod]
        public void And_the_test_status_is_not_valid_then_test_status_matches_the_undefined_test_status()
        {
            const int notDefinedTestStatusId = 12;

            var listResult = new GetBvaTestListResult { STATUS_ID = notDefinedTestStatusId };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual(TestStatus.Undefined, observedTestItem.Status);
        }

        [TestMethod]
        public void And_the_test_status_is_pending_then_test_status_matches()
        {
            const int pendingTestStatusId = 0;

            var listResult = new GetBvaTestListResult { STATUS_ID = pendingTestStatusId };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual((TestStatus)pendingTestStatusId, observedTestItem.Status);
        }

        [TestMethod]
        public void And_the_test_status_is_running_then_test_status_matches()
        {
            const int runningTestStatusId = 1;

            var listResult = new GetBvaTestListResult { STATUS_ID = runningTestStatusId };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual((TestStatus)runningTestStatusId, observedTestItem.Status);
        }

        [TestMethod]
        public void And_the_test_status_is_aborted_then_test_status_matches()
        {
            const int abortedTestStatusId = 2;

            var listResult = new GetBvaTestListResult { STATUS_ID = abortedTestStatusId };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual((TestStatus)abortedTestStatusId, observedTestItem.Status);
        }

        [TestMethod]
        public void And_the_test_status_is_deleted_then_test_status_matches()
        {
            const int deletedTestStatusId = 3;

            var listResult = new GetBvaTestListResult { STATUS_ID = deletedTestStatusId };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual((TestStatus)deletedTestStatusId, observedTestItem.Status);
        }

        [TestMethod]
        public void And_the_test_status_is_completed_then_test_status_matches()
        {
            const int completedTestStatusId = 4;

            var listResult = new GetBvaTestListResult { STATUS_ID = completedTestStatusId };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual((TestStatus)completedTestStatusId, observedTestItem.Status);
        }

        [TestMethod]
        public void And_the_record_has_sufficient_data_then_has_sufficient_data_is_set_to_true()
        {
            var listResult = new GetBvaTestListResult { HAS_SUFFICIENT_DATA = 'T'};

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.IsTrue(observedTestItem.HasSufficientData);
        }

        [TestMethod]
        public void And_the_record_does_not_have_sufficient_data_then_has_sufficient_data_is_set_to_false()
        {
            var listResult = new GetBvaTestListResult { HAS_SUFFICIENT_DATA = 'F' };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.IsFalse(observedTestItem.HasSufficientData);
        }

        [TestMethod]
        public void Then_the_test_mode_description_is_set()
        {
            const string description = "description";
            var listResult = new GetBvaTestListResult { TEST_MODE_DESCRIPTION = description };

            var observedTestItem = DataMapper.RecordToModel(listResult);

            Assert.AreEqual(description, observedTestItem.TestModeDescription);
        }
    }
    // ReSharper restore InconsistentNaming
}
