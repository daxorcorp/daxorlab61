using System;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Services.Tests.DataMapper_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_test_model_to_a_record
    {
        [TestMethod]
        public void Then_the_ordering_physician_specialty_is_transferred()
        {
            var bvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            bvaTest.PhysiciansSpecialty = "ICU";
            
            var bvaTestRecord = DataMapper.ModelToRecord(bvaTest, Substitute.For<ISettingsManager>());

            Assert.AreEqual("ICU", bvaTestRecord.PhysiciansSpecialty);
        }

        [TestMethod]
        public void And_the_physician_specialty_is_null_then_empty_string_is_transferred()
        {
            var bvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            bvaTest.PhysiciansSpecialty = null;

            var bvaTestRecord = DataMapper.ModelToRecord(bvaTest, Substitute.For<ISettingsManager>());

            Assert.AreEqual(String.Empty, bvaTestRecord.PhysiciansSpecialty);
        }

	    [TestMethod]
	    public void And_the_patient_is_an_amputee_then_the_patient_amputee_status_is_transferred()
	    {
		    var bvaTest = BvaTestFactory.CreateBvaTest(5, 60);
		    bvaTest.Patient.IsAmputee = true;

		    var bvaTestRecord = DataMapper.ModelToRecord(bvaTest, Substitute.For<ISettingsManager>());

			Assert.IsTrue(bvaTestRecord.Patient.IsAmputee);
	    }

	    [TestMethod]
		public void And_the_patient_is_not_an_amputee_then_the_patient_amputee_status_is_transferred()
		{
			var bvaTest = BvaTestFactory.CreateBvaTest(5, 60);

			var bvaTestRecord = DataMapper.ModelToRecord(bvaTest, Substitute.For<ISettingsManager>());

			Assert.IsFalse(bvaTestRecord.Patient.IsAmputee);
	    }

		[TestMethod]
		public void And_the_patient_is_not_an_amputee_then_the_default_correction_factor_is_transferred()
		{
			var bvaTest = BvaTestFactory.CreateBvaTest(5, 60);

			bvaTest.AmputeeIdealsCorrectionFactor = 5;
			bvaTest.Patient.IsAmputee = false;

			var bvaTestRecord = DataMapper.ModelToRecord(bvaTest, Substitute.For<ISettingsManager>());

			Assert.AreEqual(0, bvaTestRecord.AmputeeIdealsCorrectionFactor);
		}

		[TestMethod]
		public void And_the_patient_is_an_amputee_then_the_correction_factor_is_transferred()
		{
			var bvaTest = BvaTestFactory.CreateBvaTest(5, 60);

			bvaTest.AmputeeIdealsCorrectionFactor = 5;
			bvaTest.Patient.IsAmputee = true;

			var bvaTestRecord = DataMapper.ModelToRecord(bvaTest, Substitute.For<ISettingsManager>());

			Assert.AreEqual(5, bvaTestRecord.AmputeeIdealsCorrectionFactor);
		}
    }
    // ReSharper restore InconsistentNaming
}
