using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Services.Tests.DataMapper_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_the_test_record_to_a_model
    {
        private BvaTestRecord _bvaTestRecord;
        private BVATest _bvaTest;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _bvaTest = BvaTestFactory.CreateBvaTest(5, 60);
            _bvaTestRecord = DataMapper.ModelToRecord(_bvaTest, Substitute.For<ISettingsManager>());
            _bvaTestRecord.TubeTypeRecord = new TubeTypeRecord();
        }

        [TestMethod]
        public void Then_the_ordering_physician_specialty_is_transferred()
        {
            _bvaTestRecord.PhysiciansSpecialty = "ICU";

            var testModel = DataMapper.RecordToModel(_bvaTestRecord, _bvaTest.CurrentSampleSchema);

            Assert.AreEqual("ICU", testModel.PhysiciansSpecialty);
        }

		[TestMethod]
		public void And_the_patient_is_an_amputee_then_the_patient_amputee_status_is_transferred()
		{
			_bvaTestRecord.Patient.IsAmputee = true;

			var testModel = DataMapper.RecordToModel(_bvaTestRecord, _bvaTest.CurrentSampleSchema);

			Assert.IsTrue(testModel.Patient.IsAmputee);
		}

		[TestMethod]
		public void And_the_patient_is_not_an_amputee_then_the_patient_amputee_status_is_transferred()
		{
			_bvaTestRecord.Patient.IsAmputee = false;

			var testModel = DataMapper.RecordToModel(_bvaTestRecord, _bvaTest.CurrentSampleSchema);

			Assert.IsFalse(testModel.Patient.IsAmputee);
		}

		[TestMethod]
		public void And_the_patient_is_an_amputee_then_the_amputee_ideals_correction_factor_is_transferred()
		{
			_bvaTestRecord.AmputeeIdealsCorrectionFactor = 5;
			_bvaTestRecord.Patient.IsAmputee = true;

			var testModel = DataMapper.RecordToModel(_bvaTestRecord, _bvaTest.CurrentSampleSchema);

			Assert.AreEqual(5, testModel.AmputeeIdealsCorrectionFactor);
		}

		[TestMethod]
		public void And_the_patient_is_not_an_amputee_then_the_default_amputee_ideals_correction_factor_is_transferred()
		{
			_bvaTestRecord.AmputeeIdealsCorrectionFactor = 5;
			_bvaTestRecord.Patient.IsAmputee = false;

			var testModel = DataMapper.RecordToModel(_bvaTestRecord, _bvaTest.CurrentSampleSchema);

			Assert.AreEqual(0, testModel.AmputeeIdealsCorrectionFactor);
		}
    }
    // ReSharper restore InconsistentNaming
}
