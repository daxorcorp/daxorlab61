﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Services.Tests.PatientSpecificFieldCache_Tests
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_the_patient_changes
	{
		private PatientSpecificFieldCache _patientSpecificFieldCache;

		private BVAPatient _patient;

		[TestInitialize]
		public void Init()
		{
			_patientSpecificFieldCache = new PatientSpecificFieldCache(Substitute.For<IMessageBoxDispatcher>(), Substitute.For<ILoggerFacade>(), Substitute.For<IMessageManager>());
		}

		[TestMethod]
		public void And_the_amputee_status_changes_from_amputee_to_non_amputee_then_the_cache_is_considered_to_be_dirty()
		{
			//Arrange
			_patient = new BVAPatient(null) { IsAmputee = true };

			_patientSpecificFieldCache.InitializePatientSpecificFieldCache(_patient);

			//Act
			_patient.IsAmputee = false;

			//Assert
			Assert.IsTrue(_patientSpecificFieldCache.IsDirty);
		}

		[TestMethod]
		public void And_the_amputee_status_changes_from_non_amputee_to_amputee_then_the_cache_is_considered_to_be_dirty()
		{
			//Arrange
			_patient = new BVAPatient(null) { IsAmputee = false };

			_patientSpecificFieldCache.InitializePatientSpecificFieldCache(_patient);

			//Act
			_patient.IsAmputee = true;

			//Assert
			Assert.IsTrue(_patientSpecificFieldCache.IsDirty);
		}
	}
	// ReSharper restore InconsistentNaming
}