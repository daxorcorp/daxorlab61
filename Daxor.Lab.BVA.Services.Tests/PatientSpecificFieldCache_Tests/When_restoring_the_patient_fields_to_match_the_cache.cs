﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Services.Tests.PatientSpecificFieldCache_Tests
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_restoring_the_patient_fields_to_match_the_cache
	{
		private PatientSpecificFieldCache _patientSpecificFieldCache;

		[TestInitialize]
		public void Init()
		{
			_patientSpecificFieldCache = new PatientSpecificFieldCache(Substitute.For<IMessageBoxDispatcher>(), Substitute.For<ILoggerFacade>(), Substitute.For<IMessageManager>());
		}

		[TestMethod]
		public void Then_the_amputee_status_is_restored()
		{
			//Arrange
			var amputeePatient = new BVAPatient(null) {IsAmputee = true};
			var nonAmputeePatient = new BVAPatient(null) {IsAmputee = false};

			_patientSpecificFieldCache.InitializePatientSpecificFieldCache(amputeePatient);

			//Act
			_patientSpecificFieldCache.RestorePatientFieldsToMatchCache(nonAmputeePatient);

			//Assert
			Assert.IsTrue(nonAmputeePatient.IsAmputee);
		}
	}
	// ReSharper restore InconsistentNaming
}