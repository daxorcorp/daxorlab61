﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.BVA.Services.Tests.BloodVolumeTestController_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_constructing_a_blood_volume_test_controller
	{
		[TestMethod]
		public void Then_the_event_aggregator_subscribes_to_the_calibration_curve_calculated_event()
		{
			var mockEvent = Substitute.For<CalibrationCurveCalculated>();

			var mockEventAggregator = Substitute.For<IEventAggregator>();
			mockEventAggregator.GetEvent<CalibrationCurveCalculated>().Returns(mockEvent);

			// ReSharper disable once UnusedVariable
			var controller = new BloodVolumeTestController(Substitute.For<ITestExecutionController>(), Substitute.For<IBVADataService>(), Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>(), mockEventAggregator, Substitute.For<IRuleEngine>(), Substitute.For<IDetector>(), Substitute.For<ISpectroscopyService>());

			mockEventAggregator.Received(1).GetEvent<CalibrationCurveCalculated>();
			mockEvent.ReceivedWithAnyArgs(1).Subscribe(null);
		}
	}
	// ReSharper restore InconsistentNaming
}
