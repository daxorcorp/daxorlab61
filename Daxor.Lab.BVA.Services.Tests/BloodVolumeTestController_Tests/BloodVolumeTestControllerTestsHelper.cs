﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using NSubstitute;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.BloodVolumeTestController_Tests
{
	internal static class BloodVolumeTestControllerTestsHelper
	{
		internal static BloodVolumeTestController CreateBloodVolumeTestController()
		{
			var stubCurveCalculatedEvent = new CalibrationCurveCalculated();
			var stubTestSavedEvent = new BvaTestSaved();
			var stubTestSelectedEvent = new BvaTestSelected();

			var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
			stubEventAggregator.Stub(e => e.GetEvent<BvaTestSaved>()).Return(stubTestSavedEvent);
			stubEventAggregator.Stub(e => e.GetEvent<BvaTestSelected>()).Return(stubTestSelectedEvent);
			stubEventAggregator.Stub(e => e.GetEvent<CalibrationCurveCalculated>()).Return(stubCurveCalculatedEvent);

			var stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();

			return new BloodVolumeTestController(stubTestExecutionController, Substitute.For<IBVADataService>(),Substitute.For<ILoggerFacade>(),Substitute.For<ISettingsManager>(), 
				stubEventAggregator,Substitute.For<IRuleEngine>(),Substitute.For<IDetector>(), Substitute.For<ISpectroscopyService>());
		}
	}
}
