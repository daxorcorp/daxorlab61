﻿using System;
using Daxor.Lab.BVA.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.BloodVolumeTestController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_selected_test_is_the_running_test
    {
        [TestMethod]
        [TestCategory("BVA")]
        public void Then_SelectedTest_returns_the_running_test_instance()
        {
            //
            // Arrange
            //
            var testController = BloodVolumeTestControllerTestsHelper.CreateBloodVolumeTestController();
            var runningTest = new BVATest(null) { InternalId = Guid.NewGuid() };
            typeof(BloodVolumeTestController).GetProperty("RunningTest").SetValue(testController, runningTest, null);


            //
            // Act
            //
            var testWithSameIdAsRunningTest = new BVATest(null) { InternalId = runningTest.InternalId };
            typeof(BloodVolumeTestController).GetProperty("SelectedTest").SetValue(testController, testWithSameIdAsRunningTest, null);


            //
            // Assert
            //
            var observedTest = testController.SelectedTest;
            Assert.AreEqual(runningTest.GetHashCode(), observedTest.GetHashCode(),
                "The instance of the running test was not returned when a selected test with the same ID was requested.");
        }
    }
    // ReSharper restore InconsistentNaming
}
