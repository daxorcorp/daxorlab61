﻿using System;
using Daxor.Lab.BVA.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.BVA.Services.Tests.BloodVolumeTestController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_there_is_no_running_test
    {
        [TestMethod]
        [TestCategory("BVA")]
        public void Then_SelectedTest_returns_the_selected_test_instance()
        {
            //
            // Arrange
            //
            BloodVolumeTestController testController = BloodVolumeTestControllerTestsHelper.CreateBloodVolumeTestController();
            typeof(BloodVolumeTestController).GetProperty("RunningTest").SetValue(testController, null, null);


            //
            // Act
            //
            var someOtherTest = new BVATest(null) { InternalId = Guid.NewGuid() };
            typeof(BloodVolumeTestController).GetProperty("SelectedTest").SetValue(testController, someOtherTest, null);


            //
            // Assert
            //
            var observedTest = testController.SelectedTest;
            Assert.AreEqual(someOtherTest.GetHashCode(), observedTest.GetHashCode(),
                "The instance of the selected test was not returned when the running test is null.");
        }
    }
    // ReSharper restore InconsistentNaming
}
