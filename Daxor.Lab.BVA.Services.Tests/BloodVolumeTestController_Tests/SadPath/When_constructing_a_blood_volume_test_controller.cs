﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

// ReSharper disable ObjectCreationAsStatement

namespace Daxor.Lab.BVA.Services.Tests.BloodVolumeTestController_Tests.SadPath
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_constructing_a_blood_volume_test_controller
	{
		[TestMethod]
		public void And_the_execution_controller_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new BloodVolumeTestController(null, Substitute.For<IBVADataService>(), Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>(), Substitute.For<IEventAggregator>(), Substitute.For<IRuleEngine>(), Substitute.For<IDetector>(), Substitute.For<ISpectroscopyService>()), "executionController");
		}

		[TestMethod]
		public void And_the_data_service_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new BloodVolumeTestController(Substitute.For<ITestExecutionController>(), null, Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>(), Substitute.For<IEventAggregator>(), Substitute.For<IRuleEngine>(), Substitute.For<IDetector>(), Substitute.For<ISpectroscopyService>()), "dataService");
		}

		[TestMethod]
		public void And_the_logger_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new BloodVolumeTestController(Substitute.For<ITestExecutionController>(), Substitute.For<IBVADataService>(), null, Substitute.For<ISettingsManager>(), Substitute.For<IEventAggregator>(), Substitute.For<IRuleEngine>(), Substitute.For<IDetector>(), Substitute.For<ISpectroscopyService>()), "logger");
		}

		[TestMethod]
		public void And_the_settings_manager_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new BloodVolumeTestController(Substitute.For<ITestExecutionController>(), Substitute.For<IBVADataService>(), Substitute.For<ILoggerFacade>(), null, Substitute.For<IEventAggregator>(), Substitute.For<IRuleEngine>(), Substitute.For<IDetector>(), Substitute.For<ISpectroscopyService>()), "settingsManager");
		}

		[TestMethod]
		public void And_the_event_aggregator_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new BloodVolumeTestController(Substitute.For<ITestExecutionController>(), Substitute.For<IBVADataService>(), Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>(), null, Substitute.For<IRuleEngine>(), Substitute.For<IDetector>(), Substitute.For<ISpectroscopyService>()), "eventAggregator");
		}

		[TestMethod]
		public void And_the_rule_engine_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new BloodVolumeTestController(Substitute.For<ITestExecutionController>(), Substitute.For<IBVADataService>(), Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>(), Substitute.For<IEventAggregator>(), null, Substitute.For<IDetector>(), Substitute.For<ISpectroscopyService>()), "ruleEngine");
		}

		[TestMethod]
		public void And_the_detector_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new BloodVolumeTestController(Substitute.For<ITestExecutionController>(), Substitute.For<IBVADataService>(), Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>(), Substitute.For<IEventAggregator>(), Substitute.For<IRuleEngine>(), null, Substitute.For<ISpectroscopyService>()), "detector");
		}

		[TestMethod]
		public void And_the_spectrosopy_service_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new BloodVolumeTestController(Substitute.For<ITestExecutionController>(), Substitute.For<IBVADataService>(), Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>(), Substitute.For<IEventAggregator>(), Substitute.For<IRuleEngine>(), Substitute.For<IDetector>(), null), "spectroscopyService");
		}
	}
	// ReSharper restore InconsistentNaming
}
