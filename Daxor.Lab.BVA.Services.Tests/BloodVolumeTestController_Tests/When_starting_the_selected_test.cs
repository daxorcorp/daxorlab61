﻿using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Services.TestExecutionContext;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.BloodVolumeTestController_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_starting_the_selected_test
	{
		private IBVADataService _stubDataService;
		private IDetector _stubDetector;
		private IEventAggregator _mockEventAggregator;
		private ILoggerFacade _stubLogger;
		private IRuleEngine _stubRuleEngine;
		private ISettingsManager _stubSettingsManager;
		private ISpectroscopyService _stubSpectroscopyService;
		private ITestExecutionController _mockExecutionController;
	    private BVATest _bvaTestToRun;
		private BloodVolumeTestController _testController;

		[TestInitialize]
		public void Initialize()
		{
			_stubDataService = Substitute.For<IBVADataService>();
			_stubDetector = Substitute.For<IDetector>();
			_mockEventAggregator = Substitute.For<IEventAggregator>();
			_stubLogger = Substitute.For<ILoggerFacade>();
			_stubRuleEngine = Substitute.For<IRuleEngine>();
			_stubSettingsManager = Substitute.For<ISettingsManager>();
			_stubSpectroscopyService = Substitute.For<ISpectroscopyService>();
			_mockExecutionController = Substitute.For<ITestExecutionController>();

		    _bvaTestToRun = BvaTestFactory.CreateBvaTest(6, 60);
            _stubDataService.SelectTest(Arg<Guid>.Is.Anything).Returns(_bvaTestToRun);
			_mockExecutionController.IsExecuting.Returns(false);
			_mockEventAggregator.GetEvent<BvaTestSelected>().Returns(new BvaTestSelected());
            _mockEventAggregator.GetEvent<CalibrationCurveCalculated>().Returns(new CalibrationCurveCalculated());
            _mockEventAggregator.GetEvent<GetCalibrationCurveRequest>().Returns(new GetCalibrationCurveRequest());
            _testController = new BloodVolumeTestController(_mockExecutionController, _stubDataService, _stubLogger, _stubSettingsManager, _mockEventAggregator, _stubRuleEngine, _stubDetector, _stubSpectroscopyService);
			_testController.SelectTestCommand.Execute(Substitute.For<BVATestItem>());
		}

		[TestMethod]
		public void And_a_test_is_executing_then_nothing_happens()
		{
			// Arrange
			_mockExecutionController.IsExecuting.Returns(true);
			_mockEventAggregator.GetEvent<CalibrationCurveCalculated>().Returns(new CalibrationCurveCalculated());
			_testController = new BloodVolumeTestController(_mockExecutionController, _stubDataService, _stubLogger, _stubSettingsManager, _mockEventAggregator, _stubRuleEngine, _stubDetector, _stubSpectroscopyService);

			// Act
			_testController.StartSelectedTest();

			// Assert
			_mockEventAggregator.DidNotReceive().GetEvent<GetCalibrationCurveRequest>();
            _mockExecutionController.DidNotReceive().Start(Arg<ITestExecutionContext>.Is.Anything);
		}

        [TestMethod]
        public void And_no_test_is_running_then_the_running_test_instance_is_updated_based_on_the_selected_test()
        {
            _mockEventAggregator.GetEvent<CalibrationCurveCalculated>().Returns(new CalibrationCurveCalculated());
            _testController = new BloodVolumeTestController(_mockExecutionController, _stubDataService, _stubLogger, _stubSettingsManager, _mockEventAggregator, _stubRuleEngine, _stubDetector, _stubSpectroscopyService);

            Assert.IsNull(_testController.RunningTest);

            _testController.SelectTestCommand.Execute(new BVATestItem());
            _testController.StartSelectedTest();

            Assert.AreEqual(_bvaTestToRun, _testController.RunningTest);
        }

	    [TestMethod]
	    public void And_no_test_is_running_then_an_event_is_raised_to_get_the_current_calibration_curve()
	    {
            _mockEventAggregator.GetEvent<CalibrationCurveCalculated>().Returns(new CalibrationCurveCalculated());
            _testController = new BloodVolumeTestController(_mockExecutionController, _stubDataService, _stubLogger, _stubSettingsManager, _mockEventAggregator, _stubRuleEngine, _stubDetector, _stubSpectroscopyService);
            
            _testController.SelectTestCommand.Execute(new BVATestItem());
            _testController.StartSelectedTest();

	        _mockEventAggregator.Received(1).GetEvent<GetCalibrationCurveRequest>();
	    }

        [TestMethod]
        public void And_no_test_is_running_then_the_test_is_started_with_the_correct_execution_context()
        {
            BvaTestExecutionContext testExecutionContext = null;
            var curveEvent = new CalibrationCurveCalculated();
            _mockEventAggregator.GetEvent<CalibrationCurveCalculated>().Returns(curveEvent);
            _mockExecutionController.WhenForAnyArgs(c => c.Start(null)).Do(args => testExecutionContext = (BvaTestExecutionContext)args[0]);
            _testController = new BloodVolumeTestController(_mockExecutionController, _stubDataService, _stubLogger, _stubSettingsManager, _mockEventAggregator, _stubRuleEngine, _stubDetector, _stubSpectroscopyService);

            var expectedCalibrationCurve = new CalibrationCurve(5, 6, 7);
            curveEvent.Publish(expectedCalibrationCurve);
            
            _testController.SelectTestCommand.Execute(new BVATestItem());
            _testController.StartSelectedTest();

            Assert.IsNotNull(testExecutionContext);
            Assert.AreEqual(_bvaTestToRun, testExecutionContext.ExecutingTest);
            Assert.AreEqual(_stubDetector, testExecutionContext.Detector);
            Assert.AreEqual(expectedCalibrationCurve, testExecutionContext.CalibrationCurve);
            Assert.AreEqual(_stubLogger, testExecutionContext.Logger);
            Assert.AreEqual(_stubSpectroscopyService, testExecutionContext.SpectroscopyService);
            Assert.AreEqual(_stubDataService, testExecutionContext.BvaDataService);
            Assert.AreEqual(_stubRuleEngine, testExecutionContext.RuleEngine);
        }
	}
	// ReSharper restore InconsistentNaming
}
