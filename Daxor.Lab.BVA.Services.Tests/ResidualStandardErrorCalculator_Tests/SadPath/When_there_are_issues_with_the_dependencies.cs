﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.ResidualStandardErrorCalculator_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_there_are_issues_with_the_dependencies
    {
        [TestMethod]
        public void Because_the_predicted_values_calculator_is_null_then_an_exception_is_thrown()
        {
            var stubSsdCalc = MockRepository.GenerateStub<ISumOfSquaresOfDifferencesCalculator>();
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(
                () =>
                {
                    var unused = new ResidualStandardErrorCalculator(null, stubSsdCalc);
                }, "predictedValuesCalculator");
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void Because_the_ssd_calculator_is_null_then_an_exception_is_thrown()
        {
            var stubPredictedValsCalc = MockRepository.GenerateStub<IPredictedValuesCalculator>();
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(
                () =>
                {
                    var unused = new ResidualStandardErrorCalculator(stubPredictedValsCalc, null);
                }, "sumOfSquaresOfDifferencesCalculator");
            // ReSharper restore UnusedVariable
        }
    }
    // ReSharper restore InconsistentNaming
}
