﻿using System;
using System.Collections.Generic;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.ResidualStandardErrorCalculator_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_there_are_issues_with_the_lists_of_values
    {
        private static ResidualStandardErrorCalculator _rseCalculator;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            var stubPredictedValsCalc = MockRepository.GenerateStub<IPredictedValuesCalculator>();
            var stubSsdCalc = MockRepository.GenerateStub<ISumOfSquaresOfDifferencesCalculator>();
            _rseCalculator = new ResidualStandardErrorCalculator(stubPredictedValsCalc, stubSsdCalc);
        }

        [TestMethod]
        public void Because_the_first_list_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_rseCalculator.CalculateResidualStandardError(null, new List<double>(), 1, 1), "source");
        }

        [TestMethod]
        public void Because_the_second_list_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_rseCalculator.CalculateResidualStandardError(new List<double>(), null, 1, 1), "source");
        }
        
        [TestMethod]
        public void Because_both_lists_are_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_rseCalculator.CalculateResidualStandardError(null, null, 1, 1), "source");
        }

        [TestMethod]
        public void Because_the_lists_are_of_differing_sizes_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentException>(()=>_rseCalculator.CalculateResidualStandardError(new List<double> { 1.0, 2.0, 3.0 }, 
                new List<double> { 1.0, 2.0, 3.0, 4.0 }, 1, 1));
        }

        [TestMethod]
        public void Because_there_are_less_than_three_members_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentException>(()=>_rseCalculator.CalculateResidualStandardError(new List<double> { 1.0, 2.0 },
                new List<double> { 1.0, 2.0 }, 1, 1));
        }
    }
    // ReSharper restore InconsistentNaming
}
