﻿using System;
using System.Collections.Generic;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.ResidualStandardErrorCalculator_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_dependencies_throw_exceptions
    {
        [TestMethod]
        public void And_the_predicted_values_calc_throws_an_exception_then_the_exception_is_not_caught()
        {
            var stubPredictedValsCalc = MockRepository.GenerateStub<IPredictedValuesCalculator>();
            stubPredictedValsCalc.Expect(c => c.CalculatePredictedValues(null, 0, 0))
                                 .IgnoreArguments()
                                 .Throw(new NotSupportedException());
            var stubSsdCalc = MockRepository.GenerateStub<ISumOfSquaresOfDifferencesCalculator>();
            var rseCalc = new ResidualStandardErrorCalculator(stubPredictedValsCalc, stubSsdCalc);

            var values = new List<double> {1.0, 2.0, 3.0};
            AssertEx.Throws<NotSupportedException>(()=>rseCalc.CalculateResidualStandardError(values, values, 1, 1));
        }

        [TestMethod]
        public void And_the_ssd_calc_throws_an_exception_then_the_exception_is_not_caught()
        {
            var stubPredictedValsCalc = MockRepository.GenerateStub<IPredictedValuesCalculator>();
            var stubSsdCalc = MockRepository.GenerateStub<ISumOfSquaresOfDifferencesCalculator>();
            stubSsdCalc.Expect(c => c.CalculateSumOfSquaresOfDifferences(null, null))
                                 .IgnoreArguments()
                                 .Throw(new NotSupportedException());
            var rseCalc = new ResidualStandardErrorCalculator(stubPredictedValsCalc, stubSsdCalc);

            var values = new List<double> { 1.0, 2.0, 3.0 };
            AssertEx.Throws<NotSupportedException>(()=>rseCalc.CalculateResidualStandardError(values, values, 1, 1));
        }
    }
    // ReSharper restore InconsistentNaming
}
