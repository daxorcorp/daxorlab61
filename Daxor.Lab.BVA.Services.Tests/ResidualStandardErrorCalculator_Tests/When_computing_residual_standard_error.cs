﻿using System;
using System.Text;
using System.Collections.Generic;
using Daxor.Lab.BVA.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Services.Tests.ResidualStandardErrorCalculator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_computing_residual_standard_error
    {
        [TestMethod]
        public void Then_the_predicted_values_calc_is_being_used_correctly()
        {
            var stubSsdCalc = MockRepository.GenerateStub<ISumOfSquaresOfDifferencesCalculator>();

            var expectedValues = new List<double> {1.0, 2.0, 3.0};
            const double expectedSlope = 12.34;
            const double expectedYIntercept = 5.67;
            var mockPredValsCalc = MockRepository.GenerateMock<IPredictedValuesCalculator>();
            mockPredValsCalc.Expect(c => c.CalculatePredictedValues(expectedValues, expectedSlope, expectedYIntercept));

            var rseCalc = new ResidualStandardErrorCalculator(mockPredValsCalc, stubSsdCalc);

            rseCalc.CalculateResidualStandardError(expectedValues, expectedValues, expectedSlope, expectedYIntercept);

            mockPredValsCalc.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_the_ssd_calc_is_being_used_correctly()
        {
            var dummyInputValues = new List<double> {1.0, 2.0, 3.0};
            var expectedPredictedValues = new List<double> {4.0, 5.0, 6.0};
            
            var stubPredValsCalc = MockRepository.GenerateStub<IPredictedValuesCalculator>();
            stubPredValsCalc.Expect(c => c.CalculatePredictedValues(null, 0, 0))
                            .IgnoreArguments()
                            .Return(expectedPredictedValues);

            var mockSsdCalc = MockRepository.GenerateMock<ISumOfSquaresOfDifferencesCalculator>();
            mockSsdCalc.Expect(c => c.CalculateSumOfSquaresOfDifferences(expectedPredictedValues, dummyInputValues))
                       .Return(0);

            var rseCalc = new ResidualStandardErrorCalculator(stubPredValsCalc, mockSsdCalc);

            rseCalc.CalculateResidualStandardError(dummyInputValues, dummyInputValues, 1, 1);

            mockSsdCalc.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_the_calculated_value_is_correct()
        {
            var stubPredValsCalc = MockRepository.GenerateStub<IPredictedValuesCalculator>();
            var stubSsdCalc = MockRepository.GenerateStub<ISumOfSquaresOfDifferencesCalculator>();
            var ssdValues = new List<double> { 12.34, 56.78, 90.12 };
            foreach(var ssdValue in ssdValues)
                stubSsdCalc.Expect(c => c.CalculateSumOfSquaresOfDifferences(null, null))
                       .IgnoreArguments()
                       .Return(ssdValue).Repeat.Once();
            
            var rseCalc = new ResidualStandardErrorCalculator(stubPredValsCalc, stubSsdCalc);

            var listSizes = new List<int> {5, 12, 17};
            var expectedRseValues = new List<double> {2.028, 2.383, 2.451};

            var message = new StringBuilder();
            var foundMismatch = false;
            for (var i = 0; i < ssdValues.Count; i++)
            {
                var dummyList = new List<double>();
                for(var j=0; j < listSizes[i]; j++) dummyList.Add(12.34);

                stubSsdCalc.Expect(c => c.CalculateSumOfSquaresOfDifferences(null, null))
                           .IgnoreArguments()
                           .Return(ssdValues[i]);

                var observedRseValue = Math.Round(rseCalc.CalculateResidualStandardError(dummyList, dummyList, 0, 0), 3);
                if (!(Math.Abs(observedRseValue - expectedRseValues[i]) > 1E-30)) continue;
                
                foundMismatch = true;
                message.Append("Calculated RSE=" + observedRseValue + "; expected RSE=" + expectedRseValues[i]);
            }

            if (foundMismatch)
                Assert.Fail(message.ToString());
        }
    }
    // ReSharper restore InconsistentNaming
}
