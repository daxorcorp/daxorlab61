﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Daxor.Lab.SCConsoleClient
{
    /// <summary>
    /// Console app for sample changer
    /// </summary>
    class MainUI
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("Connecting to SMC service...");
                DSMChanger.SampleChangerServiceClient changer;

                try
                {
                    SampleChangerCallback smcCallback = new SampleChangerCallback();
                    InstanceContext ctx = new InstanceContext(smcCallback);
                    changer = new DSMChanger.SampleChangerServiceClient(ctx, "dsmcNetTcp");
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(" Success.");
                    Console.ResetColor();
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(" Failed.");
                    Console.ResetColor();
                    throw;
                }

                Console.Write("Subscribing to the SMC service...");
                changer.Subscribe();
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(" Success.");
                Console.ResetColor();

                MainUI.PrintHelpMenu();
                for (; ; )
                {
                    Console.Write("Select your command: ");
                    System.ConsoleKeyInfo kInfo = Console.ReadKey(false);


                    if (kInfo.Modifiers == ConsoleModifiers.Shift && kInfo.Key == ConsoleKey.G)
                    {
                        int position = -1;
                        do
                        {
                            Console.Write(Environment.NewLine);
                            Console.Write("position [1 - 25]: ");

                        } while (!Int32.TryParse(Console.ReadLine(), out position));

                        if (position < 1 || position > 25)
                            Console.WriteLine("Invalid position!!!");
                        else
                            changer.MoveToPosition(position);
                    }
                    else if (kInfo.Modifiers == ConsoleModifiers.Shift && kInfo.Key == ConsoleKey.S)
                    {
                        Console.WriteLine("\nStopping sample changer...");
                        changer.StopSampleChanger();
                        Console.WriteLine("Stopping sample changer. Success.");
                    }
                    else if (kInfo.Modifiers == ConsoleModifiers.Shift && kInfo.Key == ConsoleKey.B)
                    {
                        DSMChanger.SensorData sData = changer.GetRawSensorData();
                        Console.WriteLine("\nCurrent Position Reading:");
                        Console.WriteLine("Binary: " +  sData.PositionInBinaryFormat);
                        Console.WriteLine("Decimal: " + sData.PositionInDecimal);
                    }
                    //Question mark
                    else if (kInfo.Modifiers == ConsoleModifiers.Shift && (int)kInfo.KeyChar == 63)
                    {
                        Console.WriteLine("\nSample changer isMoving: " + changer.IsMoving());
                    }
                    else if (kInfo.Modifiers == ConsoleModifiers.Shift && kInfo.Key == ConsoleKey.P)
                    {
                        Console.WriteLine("\nCurrent Position: " + changer.GetCurrentPosition());
                    }
                    else if (kInfo.Key == ConsoleKey.F1)
                    {
                        PrintHelpMenu();
                    }
                    else if (kInfo.Key == ConsoleKey.Escape)
                    {
                        changer.StopSampleChanger();
                        changer.Unsubscribe();
                        break;
                    }
                    else if (kInfo.Key == ConsoleKey.Delete)
                    {
                        Console.Clear();
                        PrintHelpMenu();
                    }
                    else
                    {
                        Console.WriteLine("\nInvalid command was entered.");
                    }

                    Console.WriteLine();
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\nError connection to SMC service. Exception: " + ex.Message);
                Console.ReadLine();
            }
        }
        static void PrintHelpMenu()
        {
            Console.WriteLine("\n-------------------- commands ------------------- ");
            Console.WriteLine("<SHIFT> + G   : go to position.");
            Console.WriteLine("<SHIFT> + S   : stops changer.");
            Console.WriteLine("<SHIFT> + P   : current position.");
            Console.WriteLine("<SHIFT> + B   : binary position reading.");
            Console.WriteLine("<SHIFT> + ?   : true if changer is moving, false otherwise.");
            Console.WriteLine("<DEL>         : clear the screen.");
            Console.WriteLine("<F1>          : help.");
            Console.WriteLine("<ESC>         : exit.\n");

        }
    }
}
