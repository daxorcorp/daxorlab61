﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Daxor.Lab.SCConsoleClient
{
    /// <summary>
    /// Callback recieved from the sample changer service
    /// </summary>
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, UseSynchronizationContext = false)]
    public class SampleChangerCallback : DSMChanger.ISampleChangerServiceCallback
    {
        #region Callbacks

        void DSMChanger.ISampleChangerServiceCallback.OnPositionChangeBeginCallbackEvent(int OldPosition, int NewPosition)
        {
            FirePositionChangeBegin(OldPosition, NewPosition);
        }
        void DSMChanger.ISampleChangerServiceCallback.OnPositionChangeEndCallbackEvent(int NewPosition)
        {
            FirePositionChangeEnd(NewPosition);
        }
        void DSMChanger.ISampleChangerServiceCallback.OnPositionSeekBeginCallbackEvent(int CurrentPostion, int SeekPosition)
        {
            FirePositionSeekBegin(CurrentPostion, SeekPosition);
        }
        void DSMChanger.ISampleChangerServiceCallback.OnPositionSeekEndCallbackEvent(int NewPosition)
        {
            FirePositionSeekEnd(NewPosition);
        }
        void DSMChanger.ISampleChangerServiceCallback.OnPositionSeekCancelCallbackEvent(int CurrentPostion, int SeekPosition)
        {
            FirePositionSeekCancel(CurrentPostion, SeekPosition);
        }
        void DSMChanger.ISampleChangerServiceCallback.OnChangeErrorCallbackEvent(int ErrorCode, string Message)
        {
            FireChangerError(ErrorCode, Message);
        }

        #endregion

        #region Events

        public event PositionChangeBeginHandler PositionChangeBegin;
        public event PositionChangeEndHandler PositionChangeEnd;
        public event PositionSeekBeginHandler PositionSeekBegin;
        public event PositionSeekEndHandler PositionSeekEnd;
        public event PositionSeekCancelHandler PositionSeekCancel;
        public event ChangerErrorHandler ChangerError;

        #endregion

        #region Protected

        protected virtual void FirePositionChangeBegin(int oldPosition, int newPosition)
        {
            var handler = PositionChangeBegin;
            if (handler != null)
                handler(oldPosition, newPosition);
        }
        protected virtual void FirePositionChangeEnd(int newPosition)
        {
            var handler = PositionChangeEnd;
            if (handler != null)
                handler(newPosition);
        }
        protected virtual void FirePositionSeekBegin(int currPosition, int seekPosition)
        {
            var handler = PositionSeekBegin;
            if (handler != null)
                handler(currPosition, seekPosition);
        }
        protected virtual void FirePositionSeekEnd(int currPosition)
        {
            var handler = PositionSeekEnd;
            if (handler != null)
                handler(currPosition);
        }
        protected virtual void FirePositionSeekCancel(int currPosition, int seekPosition)
        {
            var handler = PositionSeekCancel;
            if (handler != null)
                handler(currPosition, seekPosition);
        }
        protected virtual void FireChangerError(int errorCode, string message)
        {
            var handler = ChangerError;
            if (handler != null)
                handler(errorCode, message);
        }

        #endregion
    }

    #region Delegates

    public delegate void PositionChangeBeginHandler(int oldPosition, int newPosition);
    public delegate void PositionChangeEndHandler(int newPosition);
    public delegate void PositionSeekBeginHandler(int currPosition, int seekPosition);
    public delegate void PositionSeekEndHandler(int currPosition);
    public delegate void PositionSeekCancelHandler(int currPosition, int seekPosition);
    public delegate void ChangerErrorHandler(int errorCode, string message);

    #endregion
}
