﻿using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.SettingsManager.IntegrationTests.SettingsManager_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_settings_manager_is_initialized
    {
        private static ISettingsManager _settingsManager;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _settingsManager = new SystemSettingsManager(new EmptyLogger(), new SettingsManagerDataService());
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("Settings.xml")]
        [RequirementValue(@"C:\ProgramData\Daxor\Lab\Backups\")]
        public void Then_the_default_customer_automatic_backup_location_matches_requirements()
        {
            var filePath = _settingsManager.GetSetting<string>(SettingKeys.SystemAutomaticBackupLocation);
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), filePath);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("Settings.xml")]
        [RequirementValue(@"C:\ProgramData\Daxor\Lab\Backups\")]
        public void Then_the_default_automatic_backup_location_matches_requirements()
        {
            var filePath = _settingsManager.GetSetting<string>(SettingKeys.SystemDefaultAutomaticBackupLocation);
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), filePath);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("Settings.xml")]
        [RequirementValue(0.001)]
        public void Then_the_default_first_fine_gain_adjustment_matches_requirements()
        {
            var firstFineGainAdjustment = _settingsManager.GetSetting<double>(SettingKeys.QcInitialFineGainAdjustment);
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsDouble(), firstFineGainAdjustment);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("Settings.xml")]
        [RequirementValue(2.0)]
        public void Then_the_default_setup_centroid_precision_in_keV_matches_requirements()
        {
            var firstFineGainAdjustment = _settingsManager.GetSetting<double>(SettingKeys.QcSetupCentroidPrecisionInkeV);
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsDouble(), firstFineGainAdjustment);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("Settings.xml")]
        [RequirementValue("(local)")]
        public void Then_the_database_server_name_matches_requirements()
        {
            var databaseName = _settingsManager.GetSetting<string>(SettingKeys.SystemDatabaseServer);
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), databaseName);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("Settings.xml")]
        [RequirementValue("DaxorDBO")]
        public void Then_the_database_username_matches_requirements()
        {
            var databaseName = _settingsManager.GetSetting<string>(SettingKeys.SystemDatabaseUsername);
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), databaseName);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("Settings.xml")]
        [RequirementValue("QCDatabaseArchive")]
        public void Then_the_QC_archive_database_name_matches_requirements()
        {
            var databaseName = _settingsManager.GetSetting<string>(SettingKeys.SystemQcDatabaseArchiveDatabaseName);
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), databaseName);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [RequirementValue("DaxorLab")]
        public void Then_the_main_database_name_matches_requirements()
        {
            var databaseName = _settingsManager.GetSetting<string>(SettingKeys.SystemDatabaseName);
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), databaseName);
        }
    }
    // ReSharper restore InconsistentNaming
}
