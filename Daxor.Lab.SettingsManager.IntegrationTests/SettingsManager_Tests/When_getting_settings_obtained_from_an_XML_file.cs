using Daxor.Lab.Database.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.SettingsManager.IntegrationTests.SettingsManager_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_settings_obtained_from_an_XML_file
    {
        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("StubXmlFiles/SettingsWithIntDoubleBool.xml")]
        public void And_the_setting_is_an_integer_then_it_is_initialized_properly()
        {
            var settingsManager = new SystemSettingsManager(new EmptyLogger(), Substitute.For<ISettingsManagerDataService>(), "SettingsWithIntDoubleBool.xml");

            Assert.AreEqual(2, settingsManager.GetSetting<int>("Integer"));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("StubXmlFiles/SettingsWithIntDoubleBool.xml")]
        public void And_the_setting_is_a_boolean_then_it_is_initialized_properly()
        {
            var settingsManager = new SystemSettingsManager(new EmptyLogger(), Substitute.For<ISettingsManagerDataService>(), "SettingsWithIntDoubleBool.xml");

            Assert.IsTrue(settingsManager.GetSetting<bool>("Bool"));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("StubXmlFiles/SettingsWithIntDoubleBool.xml")]
        public void And_the_setting_is_a_double_then_it_is_initialized_properly()
        {
            var settingsManager = new SystemSettingsManager(new EmptyLogger(), Substitute.For<ISettingsManagerDataService>(), "SettingsWithIntDoubleBool.xml");

            Assert.AreEqual(1.5, settingsManager.GetSetting<double>("Double"));
        }
    }
    // ReSharper restore InconsistentNaming
}
