using System;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.SettingsManager.IntegrationTests.SettingsManager_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_loading_XML_settings
    {
        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("StubXmlFiles/SettingMissingName.xml")]
        public void And_the_setting_name_element_is_missing_from_the_XML_file_then_an_exception_is_thrown()
        {
            AssertEx.Throws<InvalidOperationException>(() =>
            {
                // ReSharper disable once UnusedVariable
                var settingsManager = new SystemSettingsManager(new EmptyLogger(), new SettingsManagerDataService(), "SettingMissingName.xml");
            });
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("StubXmlFiles/SettingMissingType.xml")]
        public void And_the_setting_type_element_is_missing_from_the_XML_file_then_an_exception_is_thrown()
        {
            AssertEx.Throws<InvalidOperationException>(() =>
            {
                // ReSharper disable once UnusedVariable
                var settingsManager = new SystemSettingsManager(new EmptyLogger(), new SettingsManagerDataService(), "SettingMissingType.xml");
            });
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("StubXmlFiles/SettingMissingValue.xml")]
        public void And_the_setting_value_element_is_missing_from_the_XML_file_then_an_exception_is_thrown()
        {
            AssertEx.Throws<InvalidOperationException>(() =>
            {
                // ReSharper disable once UnusedVariable
                var settingsManager = new SystemSettingsManager(new EmptyLogger(), new SettingsManagerDataService(), "SettingMissingValue.xml");
            });
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("StubXmlFiles/SettingWithEmptyValue.xml")]
        public void And_the_setting_value_element_is_empty_in_the_XML_file_then_an_exception_is_thrown()
        {
            AssertEx.Throws<InvalidOperationException>(() =>
            {
                // ReSharper disable once UnusedVariable
                var settingsManager = new SystemSettingsManager(new EmptyLogger(), new SettingsManagerDataService(), "SettingWithEmptyValue.xml");
            });
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("StubXmlFiles/SettingWithEmptyType.xml")]
        public void And_the_setting_type_element_is_empty_in_the_XML_file_then_an_exception_is_thrown()
        {
            AssertEx.Throws<InvalidOperationException>(() =>
            {
                // ReSharper disable once UnusedVariable
                var settingsManager = new SystemSettingsManager(new EmptyLogger(), new SettingsManagerDataService(), "SettingWithEmptyType.xml");
            });
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [DeploymentItem("StubXmlFiles/SettingWithEmptyName.xml")]
        public void And_the_setting_name_element_is_empty_in_the_XML_file_then_an_exception_is_thrown()
        {
            AssertEx.Throws<InvalidOperationException>(() =>
            {
                // ReSharper disable once UnusedVariable
                var settingsManager = new SystemSettingsManager(new EmptyLogger(), new SettingsManagerDataService(), "SettingWithEmptyName.xml");
            });
        }
    }
    // ReSharper restore InconsistentNaming
}
