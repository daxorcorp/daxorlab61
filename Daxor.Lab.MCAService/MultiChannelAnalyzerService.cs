﻿using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.MCAContracts.Faults;
using Daxor.Lab.MCACore.Exceptions;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Daxor.Lab.MCAService.Utilities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;

namespace Daxor.Lab.MCAService
{
    /// <summary>
    /// WCF service that fulfills the multi-channel analyzer service contract
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class MultiChannelAnalyzerService : IMultiChannelAnalyzerServiceContract
    {
        private readonly IWindowsEventLogger _mcaServiceLogger;
        private readonly IMultiChannelAnalyzerController _mcaController;
        private readonly IOperationContext _operationContext;
        private readonly IThreadPool _threadPool;

        #region Ctors

        /// <summary>
        /// Creates a new MultiChannelAnalyzerService and automatically connects to the MCA Controller
        /// </summary>
        /// <exception cref="FaultException{MultiChannelAnalyzerControllerFaultContract}">If the controller
        /// could not be initialized</exception>
        public MultiChannelAnalyzerService()
        {
            _mcaServiceLogger = CreateAndInitializeServiceLogger(new ServiceLoggerFactory());
            _mcaController = CreateControllerAndSubscribeToEvents(new MultiChannelAnalyzerControllerBuilder(_mcaServiceLogger));
            _operationContext = new CurrentOperationContext();
            _threadPool = new ThreadPoolWrapper();
            SubscribedClients = new ConcurrentBag<IMultiChannelAnalyzerServiceCallbackEvents>();

            WriteMcaStatesToServiceLog();
        }

        /// <summary>
        /// Creates a new MultiChannelAnalyzerService and automatically connects to the MCA Controller
        /// created by the given factories
        /// </summary>
        /// <param name="serviceLoggerFactory">Factory capable of producing a logger instance</param>
        /// <param name="mcaControllerBuilder">Builder capable of producing an MCA Controller instance</param>
        /// <param name="operationContext">Operation context for providing WCF service interaction</param>
        /// <param name="threadPool">ThreadPool wrapper instance</param>
        /// <exception cref="FaultException{MultiChannelAnalyzerControllerFaultContract}">If the controller
        /// could not be initialized</exception>
        /// <remarks>This constructor is used for unit testing</remarks>
        public MultiChannelAnalyzerService(IServiceLoggerFactory serviceLoggerFactory, 
            IMultiChannelAnalyzerControllerBuilder mcaControllerBuilder,
            IOperationContext operationContext,
            IThreadPool threadPool)
        {
            if (serviceLoggerFactory == null) throw new ArgumentNullException("serviceLoggerFactory");
            if (mcaControllerBuilder == null) throw new ArgumentNullException("mcaControllerBuilder");
            if (operationContext == null) throw new ArgumentNullException("operationContext");
            if (threadPool == null) throw new ArgumentNullException("threadPool");

            _mcaServiceLogger = CreateAndInitializeServiceLogger(serviceLoggerFactory);
            _mcaController = CreateControllerAndSubscribeToEvents(mcaControllerBuilder);
            _operationContext = operationContext;
            _threadPool = threadPool;
            SubscribedClients = new ConcurrentBag<IMultiChannelAnalyzerServiceCallbackEvents>();

            WriteMcaStatesToServiceLog();
        }

        #endregion

        #region Service API

        /// <summary>
        /// Gets the list of identifiers for all known multi-channel analyzers,
        /// connected or disconnected
        /// </summary>
        /// <returns>List of identifiers for all MCAs; empty list if no MCAs</returns>
        public IList<string> GetAllMultiChannelAnalyzerIds()
        {
            return _mcaController.GetAllMultiChannelAnalyzerIds();
        }

        /// <summary>
        /// Gets the high voltage for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>High voltage (in volts)</returns>
        /// <exception cref="FaultException">If the high voltage could not be obtained</exception>
        public int GetHighVoltage(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "GetHighVoltage");
            try
            {
                return mca.HighVoltage;
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "high voltage");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Sets the high voltage for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <param name="highVoltage">High voltage to set</param>
        /// <exception cref="FaultException">If the high voltage could not be set or
        /// if the value is out of bounds</exception>
        public void SetHighVoltage(string mcaId, int highVoltage)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "SetHighVoltage");
            try
            {
                _mcaServiceLogger.Log("Setting high voltage to " + highVoltage + " V on MCA '" + mcaId + "'");
                mca.HighVoltage = highVoltage;
            }
            catch (OutOfBoundsException oobe)
            {
                LogOutOfBoundsException(oobe, mcaId, "set high voltage on");
                throw new FaultException(oobe.Message + "; attempted value is " + oobe.OutOfBoundsValue + 
                    ", minimum allowed is " + oobe.MinValue + ", maximum allowed is " + oobe.MaxValue);
            }
            catch (Exception ex)
            {
                LogActionException(ex, mcaId, "set high voltage on");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Gets the fine gain for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>Fine gain</returns>
        /// <exception cref="FaultException">If the fine gain could not be obtained</exception>
        public double GetFineGain(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "GetFineGain");
            try
            {
                return mca.FineGain;
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "fine gain");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Sets the fine gain for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <param name="fineGain">Fine gain to set</param>
        /// <exception cref="FaultException">If the fine gain could not be set or
        /// if the value is out of bounds</exception>
        public void SetFineGain(string mcaId, double fineGain)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "SetFineGain");
            try
            {
                _mcaServiceLogger.Log("Setting fine gain to " + fineGain + " on MCA '" + mcaId + "'");
                mca.FineGain = fineGain;
            }
            catch (OutOfBoundsException oobe)
            {
                LogOutOfBoundsException(oobe, mcaId, "set fine gain on");
                throw new FaultException(oobe.Message + "; attempted value is " + oobe.OutOfBoundsValue +
                    ", minimum allowed is " + oobe.MinValue + ", maximum allowed is " + oobe.MaxValue);
            }
            catch (Exception ex)
            {
                LogActionException(ex, mcaId, "set fine gain on");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Gets the noise level for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>Noise level (channel)</returns>
        /// <exception cref="FaultException">If the noise level could not be obtained</exception>
        public int GetNoiseLevel(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "GetNoiseLevel");
            try
            {
                return mca.NoiseLevel;
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "noise level");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Sets the noise level for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <param name="noiseLevel">Noise level to set</param>
        /// <exception cref="FaultException">If the noise level could not be set or
        /// if the value is out of bounds</exception>
        public void SetNoiseLevel(string mcaId, int noiseLevel)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "SetNoiseLevel");
            try
            {
                _mcaServiceLogger.Log("Setting noise level to " + noiseLevel + " on MCA '" + mcaId + "'");
                mca.NoiseLevel = noiseLevel;
            }
            catch (OutOfBoundsException oobe)
            {
                LogOutOfBoundsException(oobe, mcaId, "set noise level on");
                throw new FaultException(oobe.Message + "; attempted value is " + oobe.OutOfBoundsValue +
                    ", minimum allowed is " + oobe.MinValue + ", maximum allowed is " + oobe.MaxValue);
            }
            catch (Exception ex)
            {
                LogActionException(ex, mcaId, "set noise level on");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Gets the lower-level discriminator for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>Lower-level discriminator (channel)</returns>
        /// <exception cref="FaultException">If the lower-level discriminator could not be obtained</exception>
        public int GetLowerLevelDiscriminator(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "GetLowerLevelDiscriminator");
            try
            {
                return mca.LowerLevelDiscriminator;
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "lower-level discriminator");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Sets the lower-level discriminator for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <param name="lowerLevelDiscriminator">Lower-level discriminator to set</param>
        /// <exception cref="FaultException">If the lower-level discriminator could not be set or
        /// if the value is out of bounds</exception>
        public void SetLowerLevelDiscriminator(string mcaId, int lowerLevelDiscriminator)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "SetLowerLevelDiscriminator");
            try
            {
                _mcaServiceLogger.Log("Setting lower-level discriminator to " + lowerLevelDiscriminator + " on MCA '" + mcaId + "'");
                mca.LowerLevelDiscriminator = lowerLevelDiscriminator;
            }
            catch (OutOfBoundsException oobe)
            {
                LogOutOfBoundsException(oobe, mcaId, "set lower-level discriminator on");
                throw new FaultException(oobe.Message + "; attempted value is " + oobe.OutOfBoundsValue +
                    ", minimum allowed is " + oobe.MinValue + ", maximum allowed is " + oobe.MaxValue);
            }
            catch (Exception ex)
            {
                LogActionException(ex, mcaId, "set lower-level discriminator on");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Gets whether or not the corresponding multi-channel analyzer is acquiring
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if acquisition is in progress; false otherwise</returns>
        /// <exception cref="FaultException">If the acquisition status could not be obtained</exception>
        public bool IsAcquiring(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "IsAcquiring");
            try
            {
                return mca.IsAcquiring;
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "acquisition status");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Gets the serial number for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>Serial number</returns>
        /// <exception cref="FaultException">If the serial number could not be obtained</exception>
        public int GetSerialNumber(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "GetSerialNumber");
            try
            {
                return mca.SerialNumber;
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "serial number");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Gets the spectrum length for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>Spectrum length (in channels)</returns>
        /// <exception cref="FaultException">If the spectrum length could not be obtained</exception>
        public int GetSpectrumLength(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "GetSpectrumLength");
            try
            {
                return mca.SpectrumLength;
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "spectrum length");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Gets the analog-to-digital high voltage for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>ADC high voltage (in volts)</returns>
        /// <exception cref="FaultException">If the ADC high voltage could not be obtained</exception>
        public int GetAnalogToDigitalConverterHighVoltage(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "GetAnalogToDigitalConverterHighVoltage");
            try
            {
                return mca.AnalogToDigitalConverterHighVoltage;
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "analog-to-digital converter high voltage");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Gets whether or not the corresponding multi-channel analyzer is connected
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if the MCA is connected; false otherwise</returns>
        /// <exception cref="FaultException">If the connection status could not be obtained</exception>
        public bool IsConnected(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "IsConnected");
            try
            {
                return mca.IsConnected;
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "connection status");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Gets whether or not the real time is preset for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if the real time is preset; false otherwise</returns>
        /// <exception cref="FaultException">If the preset state could not be obtained</exception>
        public bool IsRealTimePreset(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "IsRealTimePreset");
            try
            {
                return mca.IsRealTimePreset;
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "real-time preset status");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Gets whether or not the live time is preset for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if the live time is preset; false otherwise</returns>
        /// <exception cref="FaultException">If the preset state could not be obtained</exception>
        public bool IsLiveTimePreset(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "IsLiveTimePreset");
            try
            {
                return mca.IsLiveTimePreset;
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "live-time preset status");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Gets whether or not the gross count in the region of interest is preset 
        /// for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if the gross count in the region of interest is preset; false otherwise</returns>
        /// <exception cref="FaultException">If the preset state could not be obtained</exception>
        public bool IsGrossCountPreset(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "IsGrossCountPreset");
            try
            {
                return mca.IsGrossCountPreset;
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "gross count preset status");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Gets whether or not acquisition has completed for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if acquisition has completed; false otherwise</returns>
        /// <exception cref="FaultException">If the acquisition completion state could not be obtained</exception>
        public bool HasAcquisitionCompleted(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "HasAcquisitionCompleted");
            try
            {
                return mca.HasAcquisitionCompleted;
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "acquisition completed status");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Shuts down the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <exception cref="FaultException">If the MCA could not be shut down</exception>
        public void ShutDown(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "ShutDown");
            try
            {
                _mcaServiceLogger.Log("Shutting down MCA '" + mcaId + "'");
                mca.ShutDown();
            }
            catch (Exception ex)
            {
                LogActionException(ex, mcaId, "shut down");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Gets the spectrum data for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>Spectrum data</returns>
        /// <exception cref="FaultException">If the spectrum data could not be obtained</exception>
        public SpectrumData GetSpectrumData(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "GetSpectrumData");
            try
            {
                return mca.GetSpectrumData();
            }
            catch (Exception ex)
            {
                LogStatusException(ex, mcaId, "spectrum data");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Presets the count time for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <param name="isLiveTime">True if live time should be preset; false for real time</param>
        /// <param name="presetTimeInSeconds">Time limit (in seconds)</param>
        /// <exception cref="FaultException">If the time could not be preset</exception>
        public void PresetTime(string mcaId, bool isLiveTime, long presetTimeInSeconds)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "PresetTime");
            var presetType = isLiveTime ? "live" : "real";
            try
            {
                _mcaServiceLogger.Log("Presetting " + presetType + " time of " + presetTimeInSeconds + " sec on MCA '" + mcaId + "'");
                if (isLiveTime)
                    mca.PresetLiveTime(presetTimeInSeconds);
                else
                    mca.PresetRealTime(presetTimeInSeconds);
            }
            catch (Exception ex)
            {
                LogActionException(ex, mcaId, "preset " + presetType + " time on");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Presets the gross counts in the region of interest for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <param name="counts">Count limit</param>
        /// <param name="beginChannel">Beginning channel</param>
        /// <param name="endChannel">Ending channel</param>
        /// <exception cref="FaultException">If the gross count could not be preset</exception>
        public void PresetGrossCountInRegionOfInterest(string mcaId, long counts, int beginChannel, int endChannel)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "PresetGrossCountsInRegionOfInterest");
            try
            {
                _mcaServiceLogger.Log("Presetting gross count of " + counts + " in ROI " + beginChannel + 
                    " to " + endChannel + " on MCA '" + mcaId + "'");
                mca.PresetGrossCountsInRegionOfInterest(counts, beginChannel, endChannel);
            }
            catch (Exception ex)
            {
                LogActionException(ex, mcaId, "preset gross count in ROI on");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Clears the spectrum and preset limits for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <exception cref="FaultException">If the spectrum and presets could not be cleared</exception>
        public void ClearSpectrumAndPresets(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "ClearSpectrumAndPresets");
            try
            {
                _mcaServiceLogger.Log("Clearing spectrum and presets on MCA '" + mcaId + "'");
                mca.ClearSpectrumAndPresets();
            }
            catch (Exception ex)
            {
                LogActionException(ex, mcaId, "clear spectrum and presets on");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Starts acquisition using the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if acquisition started successfully; false otherwise</returns>
        /// <exception cref="FaultException">If the acquisition could not be started because of a
        /// communication issue</exception>
        public bool StartAcquisition(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "StartAcquisition");
            try
            {
                _mcaServiceLogger.Log("Starting acquisition on MCA '" + mcaId + "'");
                return mca.StartAcquisition();
            }
            catch (Exception ex)
            {
                LogActionException(ex, mcaId, "start acquisition on");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Stops acquisition on the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <exception cref="FaultException">If the acquisition could not be stopped</exception>
        public void StopAcquisition(string mcaId)
        {
            var mca = GetMultiChannelAnalyzerWithIdOrThrow(mcaId, "StopAcquisition");
            try
            {
                _mcaServiceLogger.Log("Stopping acquisition on MCA '" + mcaId + "'");
                mca.StopAcquisition();
            }
            catch (Exception ex)
            {
                LogActionException(ex, mcaId, "stop acquisition on");
                throw new FaultException(ex.Message);
            }
        }

        /// <summary>
        /// Subscribes to the service
        /// </summary>
        public void Subscribe()
        {
            var callbackChannel = _operationContext.GetCallbackChannel<IMultiChannelAnalyzerServiceCallbackEvents>();
            if (callbackChannel == null)
            {
                _mcaServiceLogger.Log("Client doesn't implement the expected callback events");
                return;
            }
            if (SubscribedClients.Contains(callbackChannel))
            {
                _mcaServiceLogger.Log("Client is already subscribed");
                return;
            }

            SubscribedClients.Add(callbackChannel);
            _mcaServiceLogger.Log("Client connected from IP " + _operationContext.RemoteEndpointIpAddress + "; total client count is " + SubscribedClients.Count);
        }

        /// <summary>
        /// Unsubscribes from the service
        /// </summary>
        public void Unsubscribe()
        {
            var callbackChannel = _operationContext.GetCallbackChannel<IMultiChannelAnalyzerServiceCallbackEvents>();
            if (callbackChannel == null)
            {
                _mcaServiceLogger.Log("Client doesn't implement the expected callback events");
                return;
            }
            if (!SubscribedClients.Contains(callbackChannel))
            {
                _mcaServiceLogger.Log("Client is not currently subscribed");
                return;
            }

            IMultiChannelAnalyzerServiceCallbackEvents unsubscribedClient;
            SubscribedClients.TryTake(out unsubscribedClient);
            _mcaServiceLogger.Log("Client disconnected from IP " + _operationContext.RemoteEndpointIpAddress + "; total client count is " + SubscribedClients.Count);
        }

        /// <summary>
        /// Saves the configuration files for each multi-channel analyzer
        /// </summary>
        public void SaveConfigurationsForAllMultiChannelAnalyzers()
        {
            _mcaServiceLogger.Log("Saving configurations for all MCAs");
            try
            {
                _mcaController.SaveConfigurationsForAllMultiChannelAnalyzers();
            }
            catch (Exception ex)
            {
                _mcaServiceLogger.Log("Unable to save configurations: " + ex.Message);
            }
        }

        /// <summary>
        /// Resets a given multi-channel analyzer to its default configuration
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA to have its configuration file reset</param>
        public void ResetMultiChannelAnalyzerToDefaultConfiguration(string mcaId)
        {
            _mcaServiceLogger.Log("Resetting MCA '" + mcaId + "' to its default configuration");
            try
            {
                _mcaController.ResetMultiChannelAnalyzerToDefaultConfiguration(mcaId);
            }
            catch (Exception ex)
            {
                _mcaServiceLogger.Log("Unable to reset MCA '" + mcaId + "' to its default configuration: " + ex.Message);
            }
        }

        #endregion

        #region Protected

        protected ConcurrentBag<IMultiChannelAnalyzerServiceCallbackEvents> SubscribedClients { get; private set; }

        #endregion

        #region Helpers

        private static IWindowsEventLogger CreateAndInitializeServiceLogger(IServiceLoggerFactory serviceLoggerFactory)
        {
            var mcaServiceLogger = serviceLoggerFactory.CreateLogger(ServiceLoggerFactory.MultiChannelAnalyzerServiceLogSourceName, ServiceLoggerFactory.MultiChannelAnalyzerServiceLogName);
            mcaServiceLogger.Log("MCA service is initializing");

            return mcaServiceLogger;
        }

        private IMultiChannelAnalyzerController CreateControllerAndSubscribeToEvents(IMultiChannelAnalyzerControllerBuilder mcaControllerBuilder)
        {
            _mcaServiceLogger.Log("Creating MCA Controller");
            IMultiChannelAnalyzerController mcaController;
            try
            {
                mcaController = mcaControllerBuilder.BuildController();
            }
            catch (Exception ex)
            {
                _mcaServiceLogger.Log("Unable to create an MCA controller: " + ex.Message);
                throw new FaultException<MultiChannelAnalyzerControllerFaultContract>(
                    new MultiChannelAnalyzerControllerFaultContract {Message = ex.Message}, "Unable to create an MCA controller");
            }

            mcaController.MultiChannelAnalyzerConnected += OnMultiChannelAnalyzerConnected;
            mcaController.MultiChannelAnalyzerDisconnected += OnMultiChannelAnalyzerDisconnected;
            mcaController.AcquisitionCompleted += OnAcquisitionCompleted;
            mcaController.AcquisitionStarted += OnAcquisitionStarted;
            mcaController.AcquisitionStopped += OnAcquisitionStopped;

            return mcaController;
        }

        private void OnMultiChannelAnalyzerConnected(string mcaId)
        {
            _threadPool.QueueUserWorkItem(() =>
            {
                var unresponsiveClients = new List<IMultiChannelAnalyzerServiceCallbackEvents>();
                foreach (var client in SubscribedClients)
                {
                    try
                    {
                        client.MultiChannelAnalyzerConnected(mcaId);
                    }
                    catch (Exception ex)
                    {
                        _mcaServiceLogger.Log("Exception occurred when raising MultiChannelAnalyzerConnected on client - will be unsubscribed: " + ex.Message);
                        unresponsiveClients.Add(client);
                    }
                }

                foreach (var unresponsiveClient in unresponsiveClients)
                {
                    // ReSharper disable once RedundantAssignment
                    var removedClient = unresponsiveClient;
                    SubscribedClients.TryTake(out removedClient);
                }
            });
        }

        private void OnMultiChannelAnalyzerDisconnected(string mcaId)
        {
            _threadPool.QueueUserWorkItem(() =>
            {
                var unresponsiveClients = new List<IMultiChannelAnalyzerServiceCallbackEvents>();
                foreach (var client in SubscribedClients)
                {
                    try
                    {
                        client.MultiChannelAnalyzerDisconnected(mcaId);
                    }
                    catch (Exception ex)
                    {
                        _mcaServiceLogger.Log("Exception occurred when raising MultiChannelAnalyzerDisconnected on client - will be unsubscribed: " + ex.Message);
                        unresponsiveClients.Add(client);
                    }
                }

                foreach (var unresponsiveClient in unresponsiveClients)
                {
                    // ReSharper disable once RedundantAssignment
                    var removedClient = unresponsiveClient;
                    SubscribedClients.TryTake(out removedClient);
                }
            });
        }

        private void OnAcquisitionCompleted(string mcaId, SpectrumData spectrumData)
        {
            _threadPool.QueueUserWorkItem(() =>
            {
                var unresponsiveClients = new List<IMultiChannelAnalyzerServiceCallbackEvents>();
                foreach (var client in SubscribedClients)
                {
                    try
                    {
                        client.AcquisitionCompleted(mcaId, spectrumData);
                    }
                    catch (Exception ex)
                    {
                        _mcaServiceLogger.Log("Exception occurred when raising AcquisitionCompleted on client - will be unsubscribed: " + ex.Message);
                        unresponsiveClients.Add(client);
                    }
                }

                foreach (var unresponsiveClient in unresponsiveClients)
                {
                    // ReSharper disable once RedundantAssignment
                    var removedClient = unresponsiveClient;
                    SubscribedClients.TryTake(out removedClient);
                }
            });
        }

        private void OnAcquisitionStarted(string mcaId, DateTime startTime)
        {
            _threadPool.QueueUserWorkItem(() =>
            {
                var unresponsiveClients = new List<IMultiChannelAnalyzerServiceCallbackEvents>();
                foreach (var client in SubscribedClients)
                {
                    try
                    {
                        client.AcquisitionStarted(mcaId, startTime);
                    }
                    catch (Exception ex)
                    {
                        _mcaServiceLogger.Log("Exception occurred when raising AcquisitionStarted on client - will be unsubscribed: " + ex.Message);
                        unresponsiveClients.Add(client);
                    }
                }

                foreach (var unresponsiveClient in unresponsiveClients)
                {
                    // ReSharper disable once RedundantAssignment
                    var removedClient = unresponsiveClient;
                    SubscribedClients.TryTake(out removedClient);
                }
            });
        }

        private void OnAcquisitionStopped(string mcaId, DateTime stopTime)
        {
            _threadPool.QueueUserWorkItem(() =>
            {
                var unresponsiveClients = new List<IMultiChannelAnalyzerServiceCallbackEvents>();
                foreach (var client in SubscribedClients)
                {
                    try
                    {
                        client.AcquisitionStopped(mcaId, stopTime);
                    }
                    catch (Exception ex)
                    {
                        _mcaServiceLogger.Log("Exception occurred when raising AcquisitionStopped on client - will be unsubscribed: " + ex.Message);
                        unresponsiveClients.Add(client);
                    }
                }

                foreach (var unresponsiveClient in unresponsiveClients)
                {
                    // ReSharper disable once RedundantAssignment
                    var removedClient = unresponsiveClient;
                    SubscribedClients.TryTake(out removedClient);
                }
            });
        }

        private void WriteMcaStatesToServiceLog()
        {
            var mcaIds = _mcaController.GetAllMultiChannelAnalyzerIds();
            if (mcaIds == null) return;

            foreach (var mcaId in mcaIds)
            {
                var isMcaConnected = _mcaController.GetMultiChannelAnalyzerWithId(mcaId).IsConnected;
                if (isMcaConnected)
                    _mcaServiceLogger.Log("MCA '" + mcaId + "' is connected");
                else
                    _mcaServiceLogger.Log("MCA '" + mcaId + "' is disconnected");
            }
        }

        private IMultiChannelAnalyzer GetMultiChannelAnalyzerWithIdOrThrow(string mcaId, string callerName)
        {
            var mca = _mcaController.GetMultiChannelAnalyzerWithId(mcaId);
            if (mca == null)
                throw new FaultException<MultiChannelAnalyzerNotFoundFaultContract>(new MultiChannelAnalyzerNotFoundFaultContract
                {
                    MultiChannelAnalyzerId = mcaId,
                    Method = callerName
                });

            return mca;
        }

        private void LogStatusException(Exception exception, string mcaId, string context)
        {
            _mcaServiceLogger.Log("Unable to obtain " + context + " for MCA '" + mcaId + "': " + exception.Message);
        }

        private void LogActionException(Exception exception, string mcaId, string context)
        {
            _mcaServiceLogger.Log("Unable to " + context + " MCA '" + mcaId + "': " + exception.Message);
        }

        private void LogOutOfBoundsException(OutOfBoundsException exception, string mcaId, string context)
        {
            _mcaServiceLogger.Log("Unable to " + context + " MCA '" + mcaId + "': attempted value is " +
                exception.OutOfBoundsValue + ", minimum allowed is " + exception.MinValue + 
                ", maximum allowed is " + exception.MaxValue + " (" + exception.Message + ")");
        }

        #endregion
    }
}