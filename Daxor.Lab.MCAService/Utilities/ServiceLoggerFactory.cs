﻿using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAService.Interfaces;

namespace Daxor.Lab.MCAService.Utilities
{
    /// <summary>
    /// Represents a service that can create Windows event loggers for the MCA service
    /// </summary>
    public class ServiceLoggerFactory : IServiceLoggerFactory
    {
        /// <summary>
        /// Identifier for the log source
        /// </summary>
        public const string MultiChannelAnalyzerServiceLogSourceName = "MCAServiceLog";

        /// <summary>
        /// Friendly name of the event log source
        /// </summary>
        public const string MultiChannelAnalyzerServiceLogName = "Multi-channel Analyzer Service Log";

        /// <summary>
        /// Creates a Windows event logger based on the given parameters
        /// </summary>
        /// <param name="logSourceName">Identifier for the log source</param>
        /// <param name="logName">Friendly name of the event log source</param>
        /// <returns>Logger instance capable of logging</returns>
        public IWindowsEventLogger CreateLogger(string logSourceName, string logName)
        {
            return new WindowsEventLogger(logSourceName, logName);
        }
    }
}
