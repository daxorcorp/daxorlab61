﻿using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore;
using Daxor.Lab.MCACore.Utilities;
using Daxor.Lab.MCAService.Interfaces;

namespace Daxor.Lab.MCAService.Utilities
{
    /// <summary>
    /// Represents a service that can create a multi-channel analyzer controller service
    /// </summary>
    public class MultiChannelAnalyzerControllerBuilder : IMultiChannelAnalyzerControllerBuilder
    {
        /// <summary>
        /// Identifier for the log source
        /// </summary>
        public const string HardwareConfigurationManagerLogSourceName = "HardwareConfigurationManagerLog";

        /// <summary>
        /// Friendly name of the event log source
        /// </summary>
        public const string HardwareConfigurationManagerLogName = "DAXOR Hardware Configuration Manager Log";

        private readonly IWindowsEventLogger _mcaServiceLogger;

        /// <summary>
        /// Creates a new MultiChannelAnalyzerControllerFactory instance
        /// </summary>
        /// <param name="mcaServiceLogger">Windows event logger to be used when creating the controller</param>
        public MultiChannelAnalyzerControllerBuilder(IWindowsEventLogger mcaServiceLogger)
        {
            _mcaServiceLogger = mcaServiceLogger;
        }

        /// <summary>
        /// Builds a multi-channel analyzer controller instance
        /// </summary>
        /// <returns>Initialized controller instance</returns>
        public MCACore.Interfaces.IMultiChannelAnalyzerController BuildController()
        {
            var usbDeviceWatcher = new UsbDeviceWatcher(new ManagementEventWatcherFactory());
            var hardwareConfigurationManagerLogger = new WindowsEventLogger(HardwareConfigurationManagerLogSourceName, HardwareConfigurationManagerLogName);
            return new MultiChannelAnalyzerController(usbDeviceWatcher,
                new MultiChannelAnalyzerBuilder(_mcaServiceLogger, hardwareConfigurationManagerLogger), _mcaServiceLogger);
        }
    }
}