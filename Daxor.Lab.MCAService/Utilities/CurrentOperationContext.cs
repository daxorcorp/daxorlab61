﻿using System.ServiceModel;
using System.ServiceModel.Channels;
using Daxor.Lab.MCAService.Interfaces;

namespace Daxor.Lab.MCAService.Utilities
{
    /// <summary>
    /// Wrapper service that exposes parts of the current OperationContext
    /// </summary>
    /// <remarks>This class exists for dependency abstraction, as
    /// OperationContext does not implement an interface to support testing</remarks>
    public class CurrentOperationContext : IOperationContext
    {
        /// <summary>
        /// Gets a channel to the client instance that called the current operation
        /// </summary>
        /// <typeparam name="T">The type of channel used to call back to the client</typeparam>
        /// <returns>A channel to the client instance that called the operation of the type specified
        /// in the CallbackContract property.</returns>
        public T GetCallbackChannel<T>()
        {
            return OperationContext.Current.GetCallbackChannel<T>();
        }

        /// <summary>
        /// Gets the IP address of the remote endpoint
        /// </summary>
        public string RemoteEndpointIpAddress
        {
            get
            {
                return ((RemoteEndpointMessageProperty) OperationContext.Current.IncomingMessageProperties[RemoteEndpointMessageProperty.Name]).Address;
            }
        }
    }
}
