﻿using System;
using System.Threading;
using Daxor.Lab.MCAService.Interfaces;

namespace Daxor.Lab.MCAService.Utilities
{
    /// <summary>
    /// Wraps parts of the ThreadPool class to support testing
    /// </summary>
    public class ThreadPoolWrapper : IThreadPool
    {
        /// <summary>
        /// Queues a method for execution, and specifies an object containing data to
        //  be used by the method. The method executes when a thread pool thread becomes
        //  available.
        /// </summary>
        /// <param name="workItem">Work item (method) to execute</param>
        public void QueueUserWorkItem(Action workItem)
        {
            ThreadPool.QueueUserWorkItem(delegate { workItem(); });
        }
    }
}
