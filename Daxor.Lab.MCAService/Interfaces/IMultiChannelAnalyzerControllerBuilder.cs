﻿using Daxor.Lab.MCACore.Interfaces;

namespace Daxor.Lab.MCAService.Interfaces
{
    /// <summary>
    /// Represents the behavior of a service that can create a multi-channel analyzer controller service
    /// </summary>
    public interface IMultiChannelAnalyzerControllerBuilder
    {
        /// <summary>
        /// Buildss a multi-channel analyzer controller instance
        /// </summary>
        /// <returns>Initialized controller instance</returns>
        IMultiChannelAnalyzerController BuildController();
    }
}
