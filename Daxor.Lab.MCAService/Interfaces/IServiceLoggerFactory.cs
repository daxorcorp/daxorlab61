﻿using Daxor.Lab.HardwareConfigurationManager;

namespace Daxor.Lab.MCAService.Interfaces
{
    /// <summary>
    /// Represents the behavior of service that can create Windows event loggers for the MCA service
    /// </summary>
    public interface IServiceLoggerFactory
    {
        /// <summary>
        /// Creates a Windows event logger based on the given parameters
        /// </summary>
        /// <param name="logSourceName">Identifier for the log source</param>
        /// <param name="logName">Friendly name of the event log source</param>
        /// <returns>Logger instance capable of logging</returns>
        IWindowsEventLogger CreateLogger(string logSourceName, string logName);
    }
}
