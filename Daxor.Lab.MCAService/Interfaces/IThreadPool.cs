﻿using System;

namespace Daxor.Lab.MCAService.Interfaces
{
    /// <summary>
    /// Represents the behavior of a service that uses ThreadPool
    /// </summary>
    public interface IThreadPool
    {
        /// <summary>
        /// Queues a method for execution, and specifies an object containing data to
        //  be used by the method. The method executes when a thread pool thread becomes
        //  available.
        /// </summary>
        /// <param name="workItem">Work item (method) to execute</param>
        void QueueUserWorkItem(Action workItem);
    }
}
