﻿namespace Daxor.Lab.MCAService.Interfaces
{
    /// <summary>
    /// Represents the behavior of a service that provides access to the execution 
    /// context of a service method
    /// </summary>
    public interface IOperationContext
    {
        /// <summary>
        /// Gets a channel to the client instance that called the current operation
        /// </summary>
        /// <typeparam name="T">The type of channel used to call back to the client</typeparam>
        /// <returns>A channel to the client instance that called the operation of the type specified
        /// in the CallbackContract property.</returns>
        T GetCallbackChannel<T>();

        /// <summary>
        /// Gets the IP address of the remote endpoint
        /// </summary>
        string RemoteEndpointIpAddress { get; }
    }
}
