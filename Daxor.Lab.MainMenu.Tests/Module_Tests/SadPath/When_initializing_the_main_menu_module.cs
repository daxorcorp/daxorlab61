﻿using System;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.MainMenu.Services.Navigation;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.MainMenu.Tests.Module_Tests.SadPath
{
	[TestClass]
	public class When_initializing_the_Main_Menu_module
	{
		private Module _module;
        private IUnityContainer _container;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_container = Substitute.For<IUnityContainer>();
		}

		[TestMethod]
		public void And_an_error_occurs_while_resolving_the_main_menu_module_navigator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<IAppModuleNavigator>(Arg.Any<string>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Main Menu", ex.ModuleName);
				Assert.AreEqual("initialize the Module Navigator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

        [TestMethod]
        public void And_an_error_occurs_while_activating_the_main_menu_module_then_the_correct_module_load_exception_is_thrown()
        {
            _container.Resolve<INavigationCoordinator>().Returns((INavigationCoordinator) null);
            _module = new Module(_container);

            try
            {
                _module.InitializeAppModule();
            }
            catch (ModuleLoadException ex)
            {
                Assert.AreEqual("Main Menu", ex.ModuleName);
                Assert.AreEqual("use the Navigation Coordinator to activate the module", ex.FailingAction);
                return;
            }
            Assert.Fail();
        }
	}
}
