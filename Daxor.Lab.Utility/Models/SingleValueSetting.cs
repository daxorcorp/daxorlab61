using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.SettingsManager.Models;

namespace Daxor.Lab.Utility.Models
{
    public class SingleValueSetting : SettingBase
    {
        public SingleValueSetting(string key, object value, string typeAsString, AuthorizationLevel permission) : base(key, value, typeAsString, permission) { }
	   
        public override object ValueToSave
        {
            get { return Value; }
        }

        protected override void UpdateSettingDirtiness()
        {
            IsDirty = CachedOriginalValue != null && !CachedOriginalValue.Equals(Value.ToString());
        }
    }
}