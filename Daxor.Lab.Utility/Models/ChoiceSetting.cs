using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.Utility.Models
{
    public class ChoiceSetting : SingleValueSetting
    {
        IList<KeyValuePair<string, string>> _availableValues = new List<KeyValuePair<string, string>>();
        public ChoiceSetting(string key, object value, string typeAsString, AuthorizationLevel permission, string itemsSource)
            : base(key, value, typeAsString, permission)
        {
            if (!string.IsNullOrEmpty(itemsSource))
            {
                var options = itemsSource.Split('|');
                foreach (var str in options)
                {
                    //Perform triming of the items source
                    var substring = str.Split(':');
                    if (substring.Count() == 2)
                        AvailableValues.Add(new KeyValuePair<string, string>(substring.ElementAt(0).Trim(), substring.ElementAt(1).Trim()));
                }
            }

            ValueType = typeof(IEnumerable);
        }

        public IList<KeyValuePair<string, string>> AvailableValues
        {
            get { return _availableValues; }
            set { SetValue(ref _availableValues, "AvailableValues", value); }
        }
    }
}