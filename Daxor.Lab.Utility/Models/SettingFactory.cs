﻿using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.SettingsManager.Models;
using Daxor.Lab.Utility.Entities;

namespace Daxor.Lab.Utility.Models
{
	public static class SettingFactory
	{
		public static SettingBase Create(tblSETTING dto)
		{
			SettingBase setting;

			if (dto.DATA_TYPE == "collection")
				setting = new MultipleValuesSetting(dto.SETTING_ID, dto.VALUE, dto.DATA_TYPE, User.Int32ToAuthLevel(dto.tlkpAUTHORIZATION_LEVEL.AUTHORIZATION_LEVEL_ID));
			else if (!string.IsNullOrEmpty(dto.ITEM_SOURCE))
				setting = new ChoiceSetting(dto.SETTING_ID, dto.VALUE, dto.DATA_TYPE, User.Int32ToAuthLevel(dto.tlkpAUTHORIZATION_LEVEL.AUTHORIZATION_LEVEL_ID), dto.ITEM_SOURCE);
			else
				setting = new SingleValueSetting(dto.SETTING_ID, dto.VALUE, dto.DATA_TYPE, User.Int32ToAuthLevel(dto.tlkpAUTHORIZATION_LEVEL.AUTHORIZATION_LEVEL_ID));

			setting.AppModuleKey = dto.tlkpMODULE.MODULE;
			setting.OrderWithinGroup = dto.GROUP_DISPLAY_ORDER ?? -1;
			setting.HeaderId = dto.STRING_ID ?? -1;
			setting.GroupId = dto.GROUP_ID ?? -1;
			setting.ModuleId = dto.MODULE_ID;

			return setting;
		}
	}
}
