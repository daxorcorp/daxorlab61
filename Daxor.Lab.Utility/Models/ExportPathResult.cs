﻿using System;
using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.Utility.Models
{
    public class ExportPathResult : ObservableObject
    {
        private String _ePath;
        public String ExportPath
        {
            get { return _ePath; }
            set { base.SetValue(ref _ePath, "ExportPath", value); }
        }
    }
}
