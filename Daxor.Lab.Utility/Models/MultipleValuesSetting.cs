﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.SettingsManager.Models;

namespace Daxor.Lab.Utility.Models
{
    public class MultipleValuesSetting : SettingBase
	{
		public MultipleValuesSetting(string key, object value, string typeAsString, AuthorizationLevel permission):
			base(key, value, typeAsString, permission)
		{
			CachedOriginalValue = value == null ? String.Empty : value.ToString();
		}

		public override object ValueToSave
		{
			get
			{
				var values = Value as ObservableCollection<ObservableValue>;
				return (from v in values select v.Value).ToList();
			}
		}

		protected override object InitializeValue(object value)
		{
			var options = value.ToString().Split('|');

			var cValues = new ObservableCollection<ObservableValue>();

			options.ToList().ForEach(str =>
			{
				if (str.Trim() != string.Empty)
				{
					var val = new ObservableValue { Value = str };
					val.PropertyChanged += val_PropertyChanged;
					cValues.Add(val);
				}
			});

			cValues.CollectionChanged += values_CollectionChanged;

			return cValues;
		}

		void val_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			UpdateSettingDirtiness();
		}

		void values_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			if (e.Action == NotifyCollectionChangedAction.Remove)
			{
				foreach (ObservableValue item in e.OldItems)
					item.PropertyChanged -= val_PropertyChanged;
			}
			else if (e.Action == NotifyCollectionChangedAction.Add)
			{
				foreach (ObservableValue item in e.NewItems)
				{
					item.PropertyChanged += val_PropertyChanged;
				}
			}

			UpdateSettingDirtiness();
		}

		protected override void UpdateSettingDirtiness()
		{
			var vals = Value as ObservableCollection<ObservableValue>;
			if (vals != null)
			{
				var strBuilder = new StringBuilder();
				vals.ToList().ForEach(oVal =>
				{
					if (!string.IsNullOrEmpty(oVal.Value))
						strBuilder.Append(oVal.Value.ToString(CultureInfo.InvariantCulture).Trim() + "|");
				});

				var currentString = strBuilder.ToString().TrimEnd('|');
				var cachedString = CachedOriginalValue.ToString();

				IsDirty = !cachedString.Equals(currentString);
			}
			else
				IsDirty = true;
		}
	}
}