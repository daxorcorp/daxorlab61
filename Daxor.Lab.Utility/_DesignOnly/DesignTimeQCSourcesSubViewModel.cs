﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.ViewModels;
using System.Collections.ObjectModel;
using Daxor.Lab.SettingsManager;

namespace Daxor.Lab.Utility._DesignOnly
{
    //Design time only
	public class DesignTimeQCSourcesSubViewModel : QCSourcesSubViewModel
	{
        public DesignTimeQCSourcesSubViewModel(ISettingsManager settingsManager) : base(settingsManager)
        {
            _qcSources = new ObservableCollection<Database.DataRecord.QCSourceRecord>();
            _qcSources.Add(new Database.DataRecord.QCSourceRecord() { Accuracy = 10, CountLimit = 1500, Activity = 12.4, Description = "QC 1", SerialNumber = "123123", Isotope = "Cs137" });
            _qcSources.Add(new Database.DataRecord.QCSourceRecord() { Accuracy = 90, CountLimit = 1000, Activity = 12.4, Description = "QC 2", SerialNumber = "546465", Isotope = "Cs137" });
            _qcSources.Add(new Database.DataRecord.QCSourceRecord() { Accuracy = 4, CountLimit = 9000, Activity = 12.4, Description = "QC 3", SerialNumber = "9867556", Isotope = "Eu" });
            _qcSources.Add(new Database.DataRecord.QCSourceRecord() { Accuracy = 5, CountLimit = 8000, Activity = 12.4, Description = "QC 4", SerialNumber = "234234", Isotope = "Eu" });
        }
	}
}
