﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Daxor.Lab.Utility.ViewModels;
using Daxor.Lab.Infrastructure.Controls;
using Microsoft.Practices.Composite;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Infrastructure.Interfaces;

namespace Daxor.Lab.Utility
{
    /// <summary>
    /// Interaction logic for ModuleShell.xaml
    /// </summary>
    public partial class ModuleShell : UserControl, IActiveAware, IGlobalNavigationAware
    {
        #region Fields

        readonly IAuthenticationManager _authService;

        #endregion

        #region Ctor

        public ModuleShell()
        {
            InitializeComponent();
        }
        public ModuleShell(ModuleShellViewModel vm, IAuthenticationManager authService)
            : this()
        {
            DataContext = vm;
            _authService = authService;
        }

        #endregion

        #region IActiveAware

        private bool _isActive;
        bool IActiveAware.IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                IActiveAware activeAware = DataContext as IActiveAware;
                if (activeAware != null)
                    activeAware.IsActive = value;
            }
        }
        event EventHandler IActiveAware.IsActiveChanged
        {
            add { }
            remove { }
        }

        #endregion

        #region IGlobalNavigationAware

        void IGlobalNavigationAware.NavigatingFrom()
        {
            radTransitionControl.Duration = new TimeSpan(0, 0, 0, 1, 5);
            _authService.LoginSucceeded -= authService_LoginSucceeded;
        }
        void IGlobalNavigationAware.NavigatingTo()
        {
            if (_authService.LoggedInUser != null)
                radTransitionControl.Duration = new TimeSpan(0, 0, 0, 1, 5);
            
            _authService.LoginSucceeded += authService_LoginSucceeded;
        }

        void authService_LoginSucceeded(object sender, EventArgs e)
        {
            radTransitionControl.Duration = new TimeSpan(0, 0, 0, 1, 5);
        }
        #endregion


    }
}
