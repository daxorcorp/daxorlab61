﻿using Daxor.Lab.Infrastructure.Extensions;
// ReSharper disable InconsistentNaming

namespace Daxor.Lab.Utility.Common
{
    public enum BvaExcelColumnHeader
    {
        [StringAttribute("Analyzed On")]
        AnalyzedOn,

        [StringAttribute("Last Name")]
        LastName,

        [StringAttribute("First Name")]
        FirstName,

        [StringAttribute("Middle Name")]
        MiddleName,

        [StringAttribute("Date Of Birth")]
        DateOfBirth,

        [StringAttribute("LocationLabel")]
        LocationLabel,

        [StringAttribute("Hospital ID")]
        HospitalId,

        [StringAttribute("TestID2Label")]
        TestId2Label,

        [StringAttribute("Patient Sex")]
        PatientSex,

        [StringAttribute("Patient Height (cm)")]
        PatientHeightInCm,

        [StringAttribute("Patient Weight (kg)")]
        PatientWeightInKg,

        [StringAttribute( "Ideal Weight Deviation (%)")]
        IdealWeightDeviation,

		[StringAttribute( "Amputee Status")]
		AmputeeStatus,

		[StringAttribute("Amputee Ideals Correction Factor")]
		AmputeeIdealsCorrectionFactor,

        [StringAttribute("Blood Volume (mL)")]
        BloodVolume,
        
        [StringAttribute("Ideal Blood Volume (mL)")]
        IdealBloodVolume,
        
        [StringAttribute("BV Deviation (mL)")]
        BloodVolumeDeviation,
        
        [StringAttribute("BV Excess/Deficit (%)")]
        BloodVolumeExcessAndDeficit,
        
        [StringAttribute("Plasma Volume (mL)")]
        PlasmaVolume,
        
        [StringAttribute("Ideal Plasma Volume (mL)")]
        IdealPlasmaVolume,
        
        [StringAttribute("PV Deviation (mL)")]
        PlasmaVolumeDeviation,
        
        [StringAttribute("PV Excess/Deficit (%)")]
        PlasmaVolumeExcessAndDeficit,
        
        [StringAttribute("RBC Volume (mL)")]
        RedBloodCellVolume,
        
        [StringAttribute("Ideal RBC Volume (mL)")]
        IdealRedBloodCellVolume,
        
        [StringAttribute("RBC Deviation (mL)")]
        RedBloodCellDeviation,
        
        [StringAttribute("RBC Excess/Deficit (%)")]
        RedBloodCellExcessAndDeficit,
        
        [StringAttribute("Slope (%/min)")]
        Slope,
        
        [StringAttribute("StdDev (%)")]
        StandardDeviation,
        
        [StringAttribute("Normal HCT (%)")]
        NormalHct,
        
        [StringAttribute("Peripheral HCT (%)")]
        PeripheralHct,
        
        [StringAttribute("Referring MD")]
        ReferringMd,
        
        [StringAttribute("CC")]
        CC,
        
        [StringAttribute("Analyst")]
        Analyst,
        
        [StringAttribute("Injectate Lot")]
        InjectateLot,
        
        [StringAttribute("Comments")]
		Comments,
      
		[StringAttribute("Sample 1A Counts")]
	    Sample1A
    }
}
