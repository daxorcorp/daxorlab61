﻿using System;
using System.Windows;
using System.Windows.Data;

namespace Daxor.Lab.Utility.Common
{
    //Converts micro seconds to seconds
    [ValueConversion(typeof(double), typeof(double))]
    public class MicroSecondsToSecondsConverter : IValueConverter
    {
        #region IValueConverter Members
        //Convert from Microseconds to the seconds
        //Data binding engine call this methods when it propogates a value from the Source To The Target.
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                double microseconds = (double)value;

                return microseconds / 1000000f;
            }

            return 0f;
        }
        //Convert back from microseconds to seconds;
        //Data binding engine calls this method when it propogates a value from the Target To The Source.
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                double seconds = (double)value;

                return seconds * 1000000f;
            }

            return DependencyProperty.UnsetValue;
        }

        #endregion
    }
}
