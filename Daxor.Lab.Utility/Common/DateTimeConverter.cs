﻿using System;
using System.Windows.Data;

namespace Daxor.Lab.Utility.Common
{
    public class DateTimeConverter : IValueConverter
    {
        /// <summary>
        /// Converts a string to a DateTime
        /// </summary>
        /// <param name="value">String value to convert</param>
        /// <param name="targetType">Not used</param>
        /// <param name="parameter">Not used</param>
        /// <param name="culture">Not used</param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return null;

            DateTime valueAsDateTime;
            if (!DateTime.TryParse(value.ToString(), out valueAsDateTime))
                return null;

            return valueAsDateTime;
        }

        /// <summary>
        /// Converts a DateTime to a string
        /// </summary>
        /// <param name="value">DateTime value to convert</param>
        /// <param name="targetType">Not used</param>
        /// <param name="parameter">Not used</param>
        /// <param name="culture">Not used</param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return value == null ? String.Empty : value.ToString();
        }
    }
}