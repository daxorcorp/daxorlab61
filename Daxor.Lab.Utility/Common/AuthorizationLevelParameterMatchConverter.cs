﻿using System;
using System.Windows.Data;
using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.Utility.Common
{
    public class AuthorizationLevelParameterMatchConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            AuthorizationLevel paramAuthLevel;
            if (Enum.TryParse<AuthorizationLevel>(parameter.ToString(), out paramAuthLevel))
                return (AuthorizationLevel)value == paramAuthLevel;
            
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            AuthorizationLevel paramAuthLevel;
            if (Enum.TryParse<AuthorizationLevel>(parameter.ToString(), out paramAuthLevel))
                return paramAuthLevel;

            return AuthorizationLevel.Undefined;
        }
    }
}
