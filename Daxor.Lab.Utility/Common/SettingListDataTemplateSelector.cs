﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using System.Windows;
using System.Windows.Controls;
using Daxor.Lab.SettingsManager.Models;
using Daxor.Lab.Utility.Models;

namespace Daxor.Lab.Utility.Common
{
    public class SettingListDataTemplateSelector : DataTemplateSelector
    {
        public override System.Windows.DataTemplate SelectTemplate(object item, System.Windows.DependencyObject container)
        {
            FrameworkElement element = container as FrameworkElement;

            if (!(element != null && item != null && item is SettingBase))
                return null;

            var settingItem = item as SettingBase;

            if (settingItem.ValueType == typeof(IEnumerable))
                return element.FindResource("ComboBoxSettingDataTemplate") as DataTemplate;

            if (settingItem.ValueType == typeof(Boolean))
                return element.FindResource("CheckBoxSettingDataTemplate") as DataTemplate;
                
            if (settingItem.ValueType == typeof(SecureString))
                return element.FindResource("PasswordBoxSettingDataTemplate") as DataTemplate;
                
            if (settingItem.ValueType == typeof(DateTime))
                return element.FindResource("DateTimeSettingDataTemplate") as DataTemplate;
                
            if (settingItem.ValueType == typeof(IList<ObservableValue>))
                return element.FindResource("ListEditorSettingDataTemplate") as DataTemplate;

            if (settingItem.ValueTypeAsString == "folderbrowser")
                return element.FindResource("FolderBrowserDataTemplate") as DataTemplate;

            return element.FindResource("TextBoxSettingDataTemplate") as DataTemplate;
        }
    }
}
