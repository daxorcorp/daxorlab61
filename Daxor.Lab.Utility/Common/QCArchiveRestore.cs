﻿using System;
using System.IO;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Ionic.Zip;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Utility.Common
{
    public class QCArchiveRestore : IDatabaseRestore
    {
        #region Fields

        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly ILoggerFacade _logger;
        private readonly ISettingsManager _settingsManager;
        private readonly IMessageManager _messageManager;
        private readonly IIonicZipWrapper _ionicZipWrapper;

        #endregion

        #region Ctor

        public QCArchiveRestore(IMessageBoxDispatcher messageBoxDispatcher, ILoggerFacade logger, 
                                ISettingsManager settingsManager, IMessageManager messageManager,
                                IZipFileFactory zipFileFactory)
        {
            if (messageManager == null)
                throw new ArgumentNullException("messageManager");
            if (logger == null)
                throw new ArgumentNullException("logger");
            if (settingsManager == null)
                throw new ArgumentNullException("settingsManager");
            if (messageBoxDispatcher == null)
                throw new ArgumentNullException("messageBoxDispatcher");
            if (zipFileFactory == null)
                throw new ArgumentNullException("zipFileFactory");
            _messageBoxDispatcher = messageBoxDispatcher;
            _logger = logger;
            _settingsManager = settingsManager;
            _messageManager = messageManager;
            _ionicZipWrapper = zipFileFactory.CreateZipFileWithPassword(_settingsManager.GetSetting<string>(SettingKeys.SystemPasswordUnsecureExport));
        }

        #endregion

        #region Public API
        public string GetTemporaryFileRestorePathForZipFiles(string zipFilePath)
        {
            if (zipFilePath.EndsWith(".bak") && 
                zipFilePath.Contains(_settingsManager.GetSetting<string>(RuntimeSettingKeys.SystemQcArchiveDatabaseBackupFilename)))
                return zipFilePath;

            if (!zipFilePath.EndsWith(".zip"))
                return null;

            using (var zip = _ionicZipWrapper.Read(zipFilePath))
            {
                var backupFile = zip[@"Backups\QCDatabaseArchive.bak"];
                if (backupFile == null) throw new DatabaseRestoreBackupFileNotFoundException();
                var rootDirectoryName = DetermineRootDirectory(backupFile);
                var tempCopyPath = _settingsManager.GetSetting<String>(SettingKeys.SystemTempFilePath);
                var backupFileName = DetermineBackupFileName(backupFile);

                ExtractBackupToCorrectLocation(backupFile);
                DeleteBackupDirectory(tempCopyPath, rootDirectoryName);
                return tempCopyPath + backupFileName;
            }
        }

        public bool CanExecuteRestore(string path)
        {
            if (!String.IsNullOrWhiteSpace(path))
                return true;

            var errorMessage = _messageManager.GetMessage(MessageKeys.UtilityRestoreQcArchiveFailed).FormattedMessage;
            var systemQcArchiveBackupFilename = _settingsManager.GetSetting<string>(RuntimeSettingKeys.SystemQcArchiveDatabaseBackupFilename);
            errorMessage = String.Format(errorMessage, "\"" + systemQcArchiveBackupFilename + "\"");

            _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, errorMessage, "Error Restoring QC Archive", MessageBoxChoiceSet.Close);
            _logger.Log(errorMessage, Category.Info, Priority.Low);
            return false;
        }

        public void CleanUpTempFiles(string path)
        {
            if (File.Exists(path))
                SafeFileOperations.Delete(path);
        }
        #endregion

        #region Helpers

        internal virtual void DeleteBackupDirectory(string tempCopyPath, string rootDirectoryName)
        {
            SafeFileOperations.DeleteDirectory(tempCopyPath + rootDirectoryName);
        }

        internal virtual void ExtractBackupToCorrectLocation(ZipEntry backupFile)
        {
            var tempCopyPath = _settingsManager.GetSetting<String>(SettingKeys.SystemTempFilePath);
            var zipPassword = _settingsManager.GetSetting<string>(SettingKeys.SystemPasswordUnsecureExport);
            var backupDirectoryPath = DetermineBackupDirectoryPath(backupFile);
            var backupFileName = DetermineBackupFileName(backupFile);

            backupFile.ExtractWithPassword(tempCopyPath, ExtractExistingFileAction.OverwriteSilently, zipPassword);
            SafeFileOperations.Copy(tempCopyPath + backupDirectoryPath + backupFileName, tempCopyPath + backupFileName);
        }

        internal virtual string DetermineRootDirectory(ZipEntry backupFile)
        {
            return backupFile.FileName.Substring(0, backupFile.FileName.IndexOf("/", StringComparison.Ordinal));
        }

        internal virtual string DetermineBackupDirectoryPath(ZipEntry backupFile)
        {
            return backupFile.FileName.Substring(0, backupFile.FileName.LastIndexOf("/", StringComparison.Ordinal) + 1);
        }

        internal virtual string DetermineBackupFileName(ZipEntry backupFile)
        {
            return backupFile.FileName.Substring(backupFile.FileName.LastIndexOf("/", StringComparison.Ordinal) + 1);
        }

        #endregion
    }
}
