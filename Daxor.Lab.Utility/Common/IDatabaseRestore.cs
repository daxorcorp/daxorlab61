﻿namespace Daxor.Lab.Utility.Common
{
    public interface IDatabaseRestore
    {
        string GetTemporaryFileRestorePathForZipFiles(string zipFilePath);
        bool CanExecuteRestore(string path);
        void CleanUpTempFiles(string path);
    }
}
