﻿
namespace Daxor.Lab.Utility.Common
{
    public static class ScopedRegionNames
    {
        public const string UtilitiesWorkRegion = "UtilitiesWorkRegion";
        public const string UtilitiesCommandRegion = "UtilitiesCommandRegion";
    }
}
