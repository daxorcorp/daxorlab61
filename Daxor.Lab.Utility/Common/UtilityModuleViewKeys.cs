﻿
namespace Daxor.Lab.Utility.Common
{
    /// <summary>
    /// String keys used for the module navigator to navigate the different views
    /// </summary>
    public static class UtilityModuleViewKeys
    {
        public const string Empty = "Empty";
        public const string LoadingView = "LoadingView";

        public const string HomeView = "HomeView";
        public const string SettingsView = "SettingsView";
        public const string DatabaseView = "DatabaseView";
        public const string ExportView = "ExportView";
        public const string ServiceView = "ServiceView";
        public const string GreetingsView = "GreetingsView";
        public const string OrderingPhysiciansReportView = "OrderingPhysiciansReportView";
        public const string ManualBackupView = "ManualBackupView";
    }
}
