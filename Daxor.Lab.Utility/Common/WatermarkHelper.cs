﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Daxor.Lab.Utility.Common
{
    /// <summary>
    /// Watermark Helper adds extra behavior for monitoring 
    /// </summary>
    public class WatermarkHelper
    {
        public static bool GetIsMonitoring(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsMonitoringProperty);
        }
        public static void SetIsMonitoring(DependencyObject obj, bool value)
        {
            obj.SetValue(IsMonitoringProperty, value);
        }

        public static bool GetHasText(DependencyObject obj)
        {
            return (bool)obj.GetValue(HasTextProperty);
        }
        private static void SetHasText(DependencyObject obj, bool value)
        {
            obj.SetValue(HasTextProperty, value);
        }

        public static string GetWatermark(DependencyObject obj)
        {
            return (string)obj.GetValue(WatermarkProperty);
        }
        public static void SetWatermark(DependencyObject obj, string value)
        {
            obj.SetValue(WatermarkProperty, value);
        }

        // Using a DependencyProperty as the backing store for IsMonitoring.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsMonitoringProperty =
            DependencyProperty.RegisterAttached("IsMonitoring", typeof(bool), typeof(WatermarkHelper), new UIPropertyMetadata(false, OnIsMonitoringChanged));
        // Using a DependencyProperty as the backing store for HasText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasTextProperty =
            DependencyProperty.RegisterAttached("HasText", typeof(bool), typeof(WatermarkHelper), new UIPropertyMetadata(false));
        // Using a DependencyProperty as the backing store for Watermark.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty WatermarkProperty =
            DependencyProperty.RegisterAttached("Watermark", typeof(string), typeof(WatermarkHelper), new UIPropertyMetadata(String.Empty));

        static void OnIsMonitoringChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //In the future need to use weak event listeners with weak event manager
            if (d is TextBox)
            {
                TextBox pBox = d as TextBox;

                if ((Boolean)e.NewValue)
                    pBox.TextChanged += textChanged;
                else
                    pBox.TextChanged -= textChanged;
            }
            else if (d is PasswordBox)
            {
                PasswordBox pBox = d as PasswordBox;

                if ((Boolean)e.NewValue)
                    pBox.PasswordChanged += passwordChanged;
                else
                    pBox.PasswordChanged -= passwordChanged;
            }

        }
        static void textChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textbox = sender as TextBox;
            if (textbox != null)
            {
                SetHasText(textbox, textbox.Text.Length > 0);
            }
        }
        static void passwordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox pBox = sender as PasswordBox;
            if (pBox != null)
            {
                SetHasText(pBox, pBox.Password.Length > 0);
            }
        }
    }
}
