﻿using System;

namespace Daxor.Lab.Utility.Common
{
    public class OrderingPhysicianTestRecord
    {
        public string TestDate { get; set; }
        public string CcReportTo { get; set; }
        public string Analyst { get; set; }
        public string Location { get; set; }

        protected bool Equals(OrderingPhysicianTestRecord other)
        {
            return string.Equals(TestDate, other.TestDate) && string.Equals(CcReportTo, other.CcReportTo) && string.Equals(Analyst, other.Analyst) && string.Equals(Location, other.Location);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (TestDate != null ? TestDate.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (CcReportTo != null ? CcReportTo.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Analyst != null ? Analyst.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Location != null ? Location.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((OrderingPhysicianTestRecord) obj);
        }
    }
}
