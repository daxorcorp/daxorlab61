﻿using System.Collections.Generic;

namespace Daxor.Lab.Utility.Common
{
    public class OrderingPhysicianDataRecord
    {
        public string ReferringPhysician { get; set; }
        public string PhysicianSpecialty { get; set; }
        public List<OrderingPhysicianTestRecord> TestRecordsList { get; set; }

        protected bool Equals(OrderingPhysicianDataRecord other)
        {
            var areListsEqual = new HashSet<OrderingPhysicianTestRecord>(TestRecordsList).SetEquals(other.TestRecordsList);
            return string.Equals(ReferringPhysician, other.ReferringPhysician) && string.Equals(PhysicianSpecialty, other.PhysicianSpecialty) && areListsEqual;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (ReferringPhysician != null ? ReferringPhysician.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (PhysicianSpecialty != null ? PhysicianSpecialty.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (TestRecordsList != null ? TestRecordsList.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((OrderingPhysicianDataRecord) obj);
        }
    }
}
