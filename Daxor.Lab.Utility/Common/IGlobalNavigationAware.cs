﻿namespace Daxor.Lab.Utility.Common
{
    public interface IGlobalNavigationAware
    {
        void NavigatingFrom();
        void NavigatingTo();
    }
}
