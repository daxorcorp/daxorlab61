﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.SettingsManager.Models;
using Daxor.Lab.Utility.Entities;
using Daxor.Lab.Utility.Models;

namespace Daxor.Lab.Utility.Common
{
    //Data access for settings
    public class SettingsDataAccessLayer
    {
        #region Fields
        private static ISettingsManager SettingsManager { get; set; }
        private static string Connection
        {
            get
            {
                if (SettingsManager == null)
                    throw new Exception("Setting utility intialization failed");

                return String.Format("metadata=res://*/Entities.Settings.csdl|res://*/Entities.Settings.ssdl|res://*/Entities.Settings.msl;provider=System.Data.SqlClient;provider connection string=\"{0};multipleactiveresultsets=True;App=EntityFramework\";", SettingsManager.GetSetting<String>(SettingKeys.SystemLinqConnectionString));
            }
        }
        #endregion

        /// <summary>
        /// Allows for the DataAccessLayer to retrieve information about settings. 
        /// </summary>
        /// <param name="settingManager"></param>
        public static void Initialize(ISettingsManager settingManager)
        {
            if (SettingsManager != null)
                throw new Exception("Setting DAL has been already initialized");

            if (settingManager == null)
                throw new ArgumentNullException("settingManager");

            SettingsManager = settingManager;
        }

        /// <summary>
        /// Retrieves a list containing all of the settings for a given module
        /// </summary>
        /// <param name="moduleName">Name of the module to retrieve the settings for</param>
        /// <returns>
        /// List containing all of the settings for that particular module
        /// </returns>
        public static List<SettingBase> GetSettingsForModule(string moduleName)
        {
            List<SettingBase> result = new List<SettingBase>();

            int languageID = SettingsManager.GetSetting<int>(SettingKeys.SystemDefaultLanguageId);

            using (DaxorLabSettingsContext context = new DaxorLabSettingsContext(Connection))
            {
                var dtoResult = RetrieveModuleSpecificSettings(moduleName, context);
                ConvertSettingsFromDatabaseToSettingObjects(result, dtoResult);
                FormatSettingFromDatabase(result, languageID, context);
            }
            return result;
        }

        /// <summary>
        /// Retrieves a list containing all of the settings
        /// </summary>
        /// <returns>
        /// List containing all of the settings
        /// </returns>
        public static List<SettingBase> GetAllSettings()
        {
            List<SettingBase> result = new List<SettingBase>();

            int languageID = SettingsManager.GetSetting<int>(SettingKeys.SystemDefaultLanguageId);

            using (DaxorLabSettingsContext context = new DaxorLabSettingsContext(Connection))
            {
                var dtoResult = RetrieveAllSettingsFromDatabase(context);
                ConvertSettingsFromDatabaseToSettingObjects(result, dtoResult);
                FormatSettingFromDatabase(result, languageID, context);
            }
            return result;
        }

        #region Helper Methods
        private static IQueryable<tblSETTING> RetrieveModuleSpecificSettings(string moduleName, DaxorLabSettingsContext context)
        {
            var dtoResult = RetrieveAllSettingsFromDatabase(context);
            dtoResult = dtoResult.Where(s => s.tlkpMODULE.MODULE.Equals(moduleName)).Select(s => s);
            return dtoResult;
        }

        private static void ConvertSettingsFromDatabaseToSettingObjects(List<SettingBase> result, IQueryable<tblSETTING> dtoResult)
        {
            dtoResult.ToList().ForEach((s) => result.Add(SettingFactory.Create(s)));
        }

        private static IQueryable<tblSETTING> RetrieveAllSettingsFromDatabase(DaxorLabSettingsContext context)
        {
            return context.tblSETTINGs.Include("tlkpMODULE")
                                        .OrderBy(s => s.GROUP_ID)
                                        .ThenBy(s => s.GROUP_DISPLAY_ORDER)
                                        .Select(s => s);
        }

        private static void FormatSettingFromDatabase(List<SettingBase> result, int languageID, DaxorLabSettingsContext context)
        {
            result.ForEach(x =>
            {
                x.Header = context.tlkpLOCALIZED_SETTING_STRING
                                         .Where((s) => s.STRING_ID == x.HeaderId && s.LANGUAGE_ID == languageID)
                                         .Select(s => s.STRING)
                                         .FirstOrDefault();

                int groupStringID = context.tlkpSETTINGS_GROUP_NAME
                                             .Where(m => m.MODULE_ID == x.ModuleId && m.GROUP_ID == x.GroupId)
                                             .Select(m => m.STRING_ID)
                                             .FirstOrDefault();

                x.Group = context.tlkpLOCALIZED_SETTING_STRING
                                 .Where(s => s.STRING_ID == groupStringID && s.LANGUAGE_ID == languageID)
                                 .Select(s => s.STRING)
                                 .FirstOrDefault();
            });
        }

        #endregion
    }
}
