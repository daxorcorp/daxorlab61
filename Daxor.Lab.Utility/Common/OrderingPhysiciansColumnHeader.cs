﻿using Daxor.Lab.Infrastructure.Extensions;

namespace Daxor.Lab.Utility.Common
{
    public enum OrderingPhysiciansColumnHeader
    {
        [String("Physician Name")]
        PhysiciansName,

        [String("Physician Specialty")]
        PhysiciansSpecialty,

        [String("Test Date")]
        TestDate,

        [String("Analyst")]
        Analyst,

        [String("Report CC'ed To")]
        CcReportTo,

        [String("Test Location")]
        Location
    }
}