﻿using System;
using System.Linq;
using System.Windows.Data;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.SettingsManager.Models;
using Daxor.Lab.Utility.Models;

namespace Daxor.Lab.Utility.Common
{
    public class SettingModifyPermissionIsAllowedBasedOnCurrentUser : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values.Count() != 2) return false;

            if (values[0] is SettingBase && values[1] is User)
            {
                SettingBase settingToEvaluate = (SettingBase)values[0];
                User loggedInUser = (User)values[1];
                if (settingToEvaluate == null || loggedInUser == null) return false;
                return loggedInUser.AuthLevel >= settingToEvaluate.ModifyPermission;
            }
            
            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
