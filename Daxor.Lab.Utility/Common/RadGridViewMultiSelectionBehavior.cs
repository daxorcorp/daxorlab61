﻿using System.Collections;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Interactivity;
using Telerik.Windows.Controls;

namespace Daxor.Lab.Utility.Common
{
    public class RadGridViewMultiSelectionBehavior : Behavior<RadGridView>
    {
        public INotifyCollectionChanged SelectedItems
        {
            get { return (INotifyCollectionChanged)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedItems.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register("SelectedItems", typeof(INotifyCollectionChanged), typeof(RadGridViewMultiSelectionBehavior), new UIPropertyMetadata(null, SelectedItemsChanged));

        static void SelectedItemsChanged(DependencyObject o, DependencyPropertyChangedEventArgs args)
        {
            var prevCollection = args.OldValue as INotifyCollectionChanged;
            var collection = args.NewValue as INotifyCollectionChanged;
            
            if(prevCollection != null)
                prevCollection.CollectionChanged -= ((RadGridViewMultiSelectionBehavior)o).DependecyCollection_CollectionChanged;

            if (collection != null)
                collection.CollectionChanged += ((RadGridViewMultiSelectionBehavior)o).DependecyCollection_CollectionChanged;
        }

        void DependecyCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            UnsubscribeFromEvents();

            Transfer(SelectedItems as IList, AssociatedObject.SelectedItems);
            
            SubscribeToEvents();
        }
        void RadGridViewSelectedItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            UnsubscribeFromEvents();

            Transfer(AssociatedObject.SelectedItems, SelectedItems as IList);

            SubscribeToEvents();
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectedItems.CollectionChanged += RadGridViewSelectedItems_CollectionChanged;
        }
        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.SelectedItems.CollectionChanged -= RadGridViewSelectedItems_CollectionChanged;
        }

        private void SubscribeToEvents()
        {
            AssociatedObject.SelectedItems.CollectionChanged += RadGridViewSelectedItems_CollectionChanged;

            if (SelectedItems != null)
            {
                SelectedItems.CollectionChanged += DependecyCollection_CollectionChanged;
            }
        }
        private void UnsubscribeFromEvents()
        {
            AssociatedObject.SelectedItems.CollectionChanged -= RadGridViewSelectedItems_CollectionChanged;

            if (SelectedItems != null)
            {
                SelectedItems.CollectionChanged -= DependecyCollection_CollectionChanged;
            }
        }
        private void Transfer(IList source, IList target)
        {
            if (source == null || target == null)
                return;

            target.Clear();

            foreach (var item in source)
                target.Add(item);
        }
    }
}
