﻿using System.Windows.Controls;

namespace Daxor.Lab.Utility.SubViews
{
    /// <summary>
    /// Interaction logic for RestoreQCDatabaseArchiveSubView.xaml
    /// </summary>
    public partial class RestoreQCDatabaseArchiveSubView : UserControl
    {
        public RestoreQCDatabaseArchiveSubView()
        {
            InitializeComponent();
        }
    }
}
