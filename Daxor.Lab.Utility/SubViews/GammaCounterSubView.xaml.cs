﻿using System;
using System.Windows.Controls;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite;
using Microsoft.Research.DynamicDataDisplay;
using Microsoft.Research.DynamicDataDisplay.Navigation;

namespace Daxor.Lab.Utility.SubViews
{
    /// <summary>
    /// Interaction logic for GammaCounterSubView.xaml
    /// </summary>
    public partial class GammaCounterSubView : UserControl, IActiveAware
    {
        public GammaCounterSubView()
        {
            InitializeComponent();

            //Removes context menu and legend from the plotter area
            gammaPlotter.Children.RemoveAll<IPlotterElement, DefaultContextMenu>();
            gammaPlotter.Children.RemoveAll<IPlotterElement, Legend>();
        }
        
        public GammaCounterSubView(GammaCounterSubViewModel vm): this()
        {
            DataContext = vm;
        }

        #region IActiveAware

        bool _isActive;
        public event EventHandler IsActiveChanged = delegate { };
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                IActiveAware activeAware = DataContext as IActiveAware;
                if (activeAware != null)
                {
                    activeAware.IsActive = value;
                }
            }
        }

        #endregion
    }
}