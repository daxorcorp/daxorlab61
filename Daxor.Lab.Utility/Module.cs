﻿using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.ReportViewer.Excel_Export;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Services;
using Microsoft.Practices.Unity;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;

namespace Daxor.Lab.Utility
{
    public class Module : AppModule
    {
        public Module(IUnityContainer container)
            : base(AppModuleIds.Utilities, container)
        {
            ShortDisplayName = "Utilities";
            DisplayOrder = 3;
            IsConfigurable = false;
        }

        public override void InitializeAppModule()
        {
            var settingsManager = ResolveSettingManager();

	        try
	        {
		        InitializeSettingsDataAccessLayer(settingsManager);
	        }
	        catch
	        {
		        throw new ModuleLoadException(ShortDisplayName, "initialize the Settings Data Access Layer");
	        }

			RegisterUtilityModuleNavigator();
            InitializeAppModuleNavigator();
            RegisterExcelExportGenerator();
            RegisterReportControllerFactory();
			RegisterUtilityDataMapper();
			RegisterUtilityDataService();
			RegisterReportProcessorWrapper();
			RegisterExcelFileBuilder();
        }

        protected virtual void InitializeSettingsDataAccessLayer(ISettingsManager settingsManager)
        {
            SettingsDataAccessLayer.Initialize(settingsManager);
        }

        private void RegisterExcelFileBuilder()
        {
            try
            {
                UnityContainer.RegisterType<IExcelFileBuilder, ExcelFileBuilder>(new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ShortDisplayName, "initialize the Excel File Builder");
            }
        }

        private void RegisterReportProcessorWrapper()
        {
            try
            {
                UnityContainer.RegisterType<IReportProcessor, ReportProcessorWrapper>(new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ShortDisplayName, "initialize the Report Processor Wrapper");
            }
        }

        private void RegisterUtilityDataService()
        {
            try
            {
                UnityContainer.RegisterType<IUtilityDataService, UtilityDataService>(new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ShortDisplayName, "initialize the Utility Data Service");
            }
        }

        private void RegisterUtilityDataMapper()
        {
            try
            {
                UnityContainer.RegisterType<IUtilityDataMapper, UtilityDataMapper>(new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ShortDisplayName, "initialize the Utility Data Mapper");
            }
        }

        private void RegisterReportControllerFactory()
        {
            try
            {
                UnityContainer.RegisterType<IReportControllerFactory, ReportControllerFactory>(
                    new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ShortDisplayName, "initialize the Report Controller Factory");
            }
        }

        private void RegisterExcelExportGenerator()
        {
            try
            {
                UnityContainer.RegisterType<IExcelReportGenerator, ExcelReportGenerator>(
                    new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ShortDisplayName, "initialize the Excel Report Generator");
            }
        }

        private void InitializeAppModuleNavigator()
        {
            try
            {
                UnityContainer.Resolve<IAppModuleNavigator>(UniqueAppId).Initialize();
            }
            catch
            {
                throw new ModuleLoadException(ShortDisplayName, "initialize the Utility Module Navigator");
            }
        }

        private void RegisterUtilityModuleNavigator()
        {
            try
            {
                UnityContainer.RegisterType<IAppModuleNavigator, UtilityModuleNavigator>(UniqueAppId,
                    new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ShortDisplayName, "register the Utility Module Navigator");
            }
        }

        private ISettingsManager ResolveSettingManager()
        {
            ISettingsManager settingsManager;
            try
            {
                settingsManager = UnityContainer.Resolve<ISettingsManager>();
            }
            catch
            {
                throw new ModuleLoadException(ShortDisplayName, "initialize the Settings Manager");
            }
            return settingsManager;
        }
    }
}
