﻿using Microsoft.Practices.Composite.Presentation.Commands;
using System;
using System.Windows.Input;
using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.Utility
{
    /// <summary>
    /// Navigation route
    /// </summary>
    public class NavigationRoute : ObservableObject
    {
        string _description;
        bool _inUse;
        public NavigationRoute(string navigateTo)
        {
            NavigateTo = navigateTo;
            InvokeCommand = new DelegateCommand<Object>((o) => Invoke(this), (o) => Invoke != null);
        }
        public string NavigateTo
        {
            get;
            private set;
        }
        public string Description
        {
            get { return _description; }
            set { base.SetValue(ref _description, "Description", value); }
        }
        public ICommand InvokeCommand
        {
            get;
            private set;
        }
        public Action<NavigationRoute> Invoke
        {
            get;
            set;
        }
        public bool InUse
        {
            get { return _inUse; }
            set { base.SetValue<Boolean>(ref _inUse, "InUse", value); }
        }
    }
}
