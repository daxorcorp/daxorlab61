﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.RocketDivisionKey;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using RocketDivision.StarBurnX;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Daxor.Lab.Utility.Services;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;
using Telerik.Reporting.Processing;
using SubReport = Telerik.Reporting.SubReport;

namespace Daxor.Lab.Utility.Reports.ReportControllers
{
    public class OrderingPhysiciansReportController : ReportControllerBase
    {
        #region Constants

        private const string ExportChoice = "Export";
        private const string CancelChoice = "Cancel";
        private const string FolderSeparator = @"\";
        public const string ExportCompletedSuccessfullyMessage = "The export has completed successfully.";
        public const string ExportFileNameFormatString = "{0}_{1} to {2}";
        public const int MaximumNumberOfRecordsThatFitOnAPage = 45;
        public const int NumberOfRecordsThatSubReportHeadersOccupy = 3;
        public const int NumberOfRecordsThatSummarySubReportHeadersOccupy = 4;
        public const string SubReportContinuedLabel = "Continued";
        public const string ReportRangeSeperator = " - ";
        public const string ExportOrderingPhysiciansReportMessageBoxCaption = "Export Ordering Physicians Report";
        public const string OrderingPhysiciansSubReportHeaderFormatString = "{0} ({1})";
        public const string ReportDateRangeLabel = "Report Date Range: ";

        #endregion

        #region Fields

        readonly OrderingPhysiciansReport _report = new OrderingPhysiciansReport();
        private readonly IReportProcessor _reportProcessor;
        protected readonly OrderingPhysiciansReportModel OrderingPhysiciansReportModel;
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly IFolderBrowser _folderBrowser;
        private readonly IMessageManager _messageManager;
        private TaskScheduler _taskScheduler;
        private bool _useIdealDispatcher = true;
        private readonly IStarBurnWrapper _opticalDriveWrapper;
        private IExcelFileBuilder _excelFileBuilder;

        #endregion

        #region Ctor

        public OrderingPhysiciansReportController(ILoggerFacade logger,
            IRegionManager regionManager,
            IEventAggregator eventAggregator,
            ISettingsManager settingsManager,
            OrderingPhysiciansReportModel orderingPhysiciansReportModel,
            IReportProcessor reportProcessor,
            IMessageBoxDispatcher messageBoxDispatcher,
            IFolderBrowser folderBrowser,
            IMessageManager messageManager,
            IStarBurnWrapper opticalDriveWrapper)
        {
            if (logger == null) throw new ArgumentNullException("logger");
            if (regionManager == null) throw new ArgumentNullException("regionManager");
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            if (settingsManager == null) throw new ArgumentNullException("settingsManager");
            if (orderingPhysiciansReportModel == null) throw new ArgumentNullException("orderingPhysiciansReportModel");
            if (reportProcessor == null) throw new ArgumentNullException("reportProcessor");
            if (messageBoxDispatcher == null) throw new ArgumentNullException("messageBoxDispatcher");
            if (folderBrowser == null) throw new ArgumentNullException("folderBrowser");
            if (messageManager == null) throw new ArgumentNullException("messageManager");
            if (opticalDriveWrapper==null) throw new ArgumentNullException("opticalDriveWrapper");

            Logger = logger;
            RegionManager = regionManager;
            EventAggregator = eventAggregator;
            SettingsManager = settingsManager;

            OrderingPhysiciansReportModel = orderingPhysiciansReportModel;
            _reportProcessor = reportProcessor;
            _messageBoxDispatcher = messageBoxDispatcher;
            _folderBrowser = folderBrowser;
            _messageManager = messageManager;
            _opticalDriveWrapper = opticalDriveWrapper;
            
            UnitX = Unit.Inch(0);
            UnitY = Unit.Inch(0);
        }

        #endregion

        #region Properties

        public override IReportDocument ReportToView
        {
            get { return _report; }
        }

        public Unit UnitX { get; set; }

        public Unit UnitY { get; set; }

        /// <summary>
        /// Gets or sets the scheduler for running asynchronous tasks. By default, this is the regular thread pool,
        /// but is overridable such that other schedulers can be used (i.e., for unit testing).
        /// </summary>
        /// <remarks>
        /// When using a custom scheduler, the IdealDispatcher (for running code on the UI thread) will not be
        /// used, so that the executing code will run under the custom scheduler.
        /// </remarks>
        public TaskScheduler TaskScheduler
        {
            get
            {
                return _taskScheduler ?? (_taskScheduler = TaskScheduler.Default);
            }
            set
            {
                _taskScheduler = value;
                _useIdealDispatcher = false;
            }
        }

        #endregion

        #region ReportControllerBase Methods

        public override void ExportReport()
        {
            var fileName = BuildFileNameForOrderingPhysiciansReport(SettingsManager.GetSetting<string>(SettingKeys.SystemHospitalName),
                                                                    OrderingPhysiciansReportModel.FromDateTime, OrderingPhysiciansReportModel.ToDateTime);

            var zipFilePath = SettingsManager.GetSetting<string>(SettingKeys.SystemTempFilePath);

            var messageBoxChoices = new[] { ExportChoice, CancelChoice };
            var choiceIndex = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Undefined, _folderBrowser,
                ExportOrderingPhysiciansReportMessageBoxCaption, messageBoxChoices);

            if (messageBoxChoices[choiceIndex] == CancelChoice)
                return;

            if (!_folderBrowser.IsPathValid())
            {
                var errorMessage = _messageManager.GetMessage(MessageKeys.BvaExportPathInvalid).FormattedMessage;
                _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, errorMessage, ExportOrderingPhysiciansReportMessageBoxCaption,
                    MessageBoxChoiceSet.Close);
                Logger.Log(errorMessage, Category.Info, Priority.Low);
                return;
            }

            LoadReportData();

            if (!_folderBrowser.IsOpticalDrive)
            {
                var currentPath = _folderBrowser.CurrentPath;
                if (!currentPath.EndsWith(FolderSeparator))
                    currentPath += FolderSeparator;

                zipFilePath = currentPath;
            }

            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            var exportTask = new Task(() => ExportTask(fileName, _folderBrowser, zipFilePath, tokenSource), token);
            exportTask.ContinueWith(result => OnExportTaskFaulted(result.Exception), token, TaskContinuationOptions.OnlyOnFaulted, TaskScheduler);
            exportTask.ContinueWith(result => OnExportTaskCompleted(), token, TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler);
            exportTask.Start(TaskScheduler);
        }

        public override void PrintReport()
        {
            LoadReportData();
            _reportProcessor.PrintReport(_report);
        }

        public override void LoadReportData()
        {
            LoadReportHeaderData();
            LoadReportFooterData();
            // ReSharper disable once CoVariantArrayConversion
            _report.detail.Items.AddRange(LoadSubReportData().ToArray());
            ClearReportCoordinates();
        }

        #endregion

        #region Private Helper methods

        private void LoadReportHeaderData()
        {
            _report.textBoxHospitalName.Value = SettingsManager.GetSetting<string>(SettingKeys.SystemHospitalName);
            _report.textBoxReportDateRange.Value = ReportDateRangeLabel + OrderingPhysiciansReportModel.FromDateTime.ToString(Constants.DateRangeFormat) +
                                                   ReportRangeSeperator + OrderingPhysiciansReportModel.ToDateTime.ToString(Constants.DateRangeFormat);
        }

        private void LoadReportFooterData()
        {
            _report.textBoxPrintedOn.Value = OrderingPhysiciansReportModel.GeneratePrintedOnDate();
        }

        private void ClearReportCoordinates()
        {
            UnitX = Unit.Inch(0);
            UnitY = Unit.Inch(0);
        }

        protected IEnumerable<SubReport> LoadSubReportData()
        {
            var subReports = new List<SubReport>();

            var physiciansSubReport = GenerateOrderingPhysiciansSummary();

            subReports.Add(physiciansSubReport);

            var numberOfRecordsAlreadyOnThisPage = NumberOfRecordsThatSummarySubReportHeadersOccupy + OrderingPhysiciansReportModel.OrderingPhysicianDataRecords.Count();
            if (numberOfRecordsAlreadyOnThisPage + NumberOfRecordsThatSubReportHeadersOccupy >=
                        MaximumNumberOfRecordsThatFitOnAPage) numberOfRecordsAlreadyOnThisPage %= MaximumNumberOfRecordsThatFitOnAPage - NumberOfRecordsThatSubReportHeadersOccupy;

            subReports.AddRange(GenerateOrderingPhysiciansSubReports(numberOfRecordsAlreadyOnThisPage));

            return subReports;
        }

        protected IEnumerable<SubReport> GenerateOrderingPhysiciansSubReports(int numberOfRecordsAlreadyOnThisPage)
        {
            var subReports = new List<SubReport>();

            foreach (var record in OrderingPhysiciansReportModel.OrderingPhysicianDataRecords)
            {
                var numberOfRecordsLeftForThisPage = MaximumNumberOfRecordsThatFitOnAPage - numberOfRecordsAlreadyOnThisPage;
                var canFitEntireSubReportOnThisPage = (record.TestRecordsList.Count() + NumberOfRecordsThatSubReportHeadersOccupy) <= numberOfRecordsLeftForThisPage;
                var testRecordsLists = new List<List<OrderingPhysicianTestRecord>>();

                // Use the whole list if it will fit
                if (canFitEntireSubReportOnThisPage)
                    testRecordsLists.Add(record.TestRecordsList);
                else
                {
                    // Put part of the data in a list (just enough to cover the headers and at least one row of data)
                    var numberSpacesWeCanUse = numberOfRecordsLeftForThisPage - NumberOfRecordsThatSubReportHeadersOccupy;

                    var firstList = record.TestRecordsList.Take(numberSpacesWeCanUse).ToList();

                    testRecordsLists.Add(firstList);

                    // If we go beyond the max # rows per page, put the max # in subsequent lists
                    var numberToSkip = numberSpacesWeCanUse;
                    var theRestOfTheTestRecords =
                        record.TestRecordsList.Skip(numberToSkip)
                            .Partition(MaximumNumberOfRecordsThatFitOnAPage - NumberOfRecordsThatSubReportHeadersOccupy);
                    testRecordsLists.AddRange(theRestOfTheTestRecords.Select(otherRecord => otherRecord.ToList()));
                }

                // Now that the partitioned lists have been built, turn them into subreports
                var firstTestDataRecord = true;
                foreach (var testRecord in testRecordsLists)
                {
                    numberOfRecordsAlreadyOnThisPage += NumberOfRecordsThatSubReportHeadersOccupy;
                    var orderingPhysiciansSubReport = new SubReport_OrderingPhysician
                    {
                        textBoxPhysiciansName = {Value = GenerateFormattedSubReportHeader(record)},
                        textBoxPhysiciansSpecialty = {Value = record.PhysicianSpecialty},
                        tableTestRecord = {DataSource = testRecord},
                        textBoxContinued = {Value = firstTestDataRecord ? string.Empty : SubReportContinuedLabel}
                    };

                    numberOfRecordsAlreadyOnThisPage += testRecord.Count();
                    if (numberOfRecordsAlreadyOnThisPage + NumberOfRecordsThatSubReportHeadersOccupy >=
                        MaximumNumberOfRecordsThatFitOnAPage) numberOfRecordsAlreadyOnThisPage %= MaximumNumberOfRecordsThatFitOnAPage - NumberOfRecordsThatSubReportHeadersOccupy;

                    var subReport = new SubReport
                    {
                        Location = new PointU(UnitX, UnitY),
                        Size = new SizeU(Unit.Inch(7.94), orderingPhysiciansSubReport.detail.Height),
                    };

                    UnitY = UnitY.Add(orderingPhysiciansSubReport.detail.Height);

                    subReport.ReportSource = new InstanceReportSource {ReportDocument = orderingPhysiciansSubReport};

                    subReports.Add(subReport);

                    firstTestDataRecord = false;
                }
            }

            return subReports;
        }

        protected static string GenerateFormattedSubReportHeader(OrderingPhysicianDataRecord record)
        {
            return String.Format(OrderingPhysiciansSubReportHeaderFormatString, record.ReferringPhysician,record.TestRecordsList.Count);
        }

        protected SubReport GenerateOrderingPhysiciansSummary()
        {
            var physiciansSummaryList = (from r in OrderingPhysiciansReportModel.OrderingPhysicianDataRecords
                orderby r.TestRecordsList.Count descending
                select r);

            var specialtySummaryList = OrderingPhysiciansReportModel.OrderingPhysicianDataRecords.GroupBy(r => r.PhysicianSpecialty)
                                       .Select(g => new SpecialtySummaryItem { 
                                                                                Specialty = g.Key + " (" + g.Select(r => r.ReferringPhysician).Distinct().Count() + ")", 
                                                                                Sum = g.Sum(r => r.TestRecordsList.Count) 
                                                                             }).OrderByDescending(r => r.Sum);

            var totalTests = OrderingPhysiciansReportModel.OrderingPhysicianDataRecords.Sum(r => r.TestRecordsList.Count);

            var physiciansSummary = new SubReport_Summary
            {
                tablePhysiciansSummaryRecord = {DataSource = physiciansSummaryList.ToList()},
                textBoxPhysicianTestTotal = {Value = totalTests.ToString(CultureInfo.InvariantCulture)},
                textBoxSpecialtiesTestTotal = {Value = totalTests.ToString(CultureInfo.InvariantCulture)},
                tableSpecialtySummary = {DataSource = specialtySummaryList.ToList()}
            };

            var physiciansSubReport = new SubReport
            {
                Location = new PointU(UnitX, UnitY),
                Size = new SizeU(Unit.Inch(7.94), physiciansSummary.detail.Height),
            };

            UnitY = UnitY.Add(physiciansSummary.detail.Height);

            physiciansSubReport.ReportSource = new InstanceReportSource {ReportDocument = physiciansSummary};
            return physiciansSubReport;
        }

        protected virtual void SaveExcelReportToFile(string fileName, string path)
        {
            _excelFileBuilder =
                OrderingPhysiciansReportModel.GenerateExcelFileBuilder(
                    SettingsManager.GetSetting<string>(SettingKeys.SystemHospitalName));

            _excelFileBuilder.SaveExcelWorkbook(path + fileName +".xml");

			_excelFileBuilder.AddWorksheet(ExcelReportGenerator.WorksheetName);
			ValueInserters.Initialize(_excelFileBuilder, _excelFileBuilder.GetFirstWorksheet());
        }

        protected virtual void ExportTask(string fileName, IFolderBrowser folderBrowser, string zipFilePath, CancellationTokenSource cancellationTokenSource)
        {
            EventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Exporting Report..."));
            var tempPath = SettingsManager.GetSetting<string>(SettingKeys.SystemTempFilePath);

            SavePdfRenderingToFile(fileName, tempPath);
            SaveExcelReportToFile(fileName, tempPath);

            if (_folderBrowser.IsOpticalDrive)
                BurnToOpticalMedia(fileName, cancellationTokenSource);
            else
                CopyToNonOpticalMedia(fileName, zipFilePath, tempPath);

            CleanUpAfterExport(fileName, tempPath);
        }

        protected virtual void SavePdfRenderingToFile(string fileName, string tempPath)
        {
            var renderedReport = new ReportProcessor().RenderReport("PDF", new InstanceReportSource { ReportDocument = _report }, null);
            using (var fs = new FileStream(tempPath + fileName + ".pdf", FileMode.Create))
                fs.Write(renderedReport.DocumentBytes, 0, renderedReport.DocumentBytes.Length);
        }

        protected virtual void BurnToOpticalMedia(string fileName, CancellationTokenSource cancellationTokenSource)
        {
            var dataBurner = _opticalDriveWrapper.CreateDataBurner();
            dataBurner.OnProgress += OnDataBurnerProgress;

            var dataFolder = dataBurner as IDataFolder;
            dataFolder.AddFile(SettingsManager.GetSetting<string>(SettingKeys.SystemTempFilePath) +
                fileName + ".xml");

            dataFolder.AddFile(SettingsManager.GetSetting<string>(SettingKeys.SystemTempFilePath) +
                fileName + ".pdf");

            try
            {
                dataBurner.Burn(false, RocketDivisionKey.DEF_VOLUME_NAME, "StarBurnX",
                    "StarBurnX Data Burner");
            }
            catch (Exception ex)
            {
                Message errorAlert;
                string message;
                switch (ex.Message)
                {
                    case "Device is not ready:Device is not ready":
                        errorAlert =
                            _messageManager.GetMessage(MessageKeys.BvaExportDeviceNotReady);
                        message = errorAlert.FormattedMessage;
                        break;
                    case "Disk readonly: The media disk is not recordable":
                        errorAlert =
                            _messageManager.GetMessage(MessageKeys.BvaExportDiscReadonly);
                        message = errorAlert.FormattedMessage;
                        break;
                    case
                        "Disc is finalized:The disc is finalized! Note that is not possible to write data on the finalized disc!"
                        :
                        errorAlert =
                            _messageManager.GetMessage(MessageKeys.BvaExportDiscFinalized);
                        message = errorAlert.FormattedMessage;
                        break;
                    default:
                        message = ex.Message;
                        Logger.Log(ex.Message, Category.Exception, Priority.High);
                        break;
                }
                if (_useIdealDispatcher)
                    IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error,
                        message, ExportOrderingPhysiciansReportMessageBoxCaption, MessageBoxChoiceSet.Close));
                else
                    _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error,
                        message, ExportOrderingPhysiciansReportMessageBoxCaption, MessageBoxChoiceSet.Close);

                Logger.Log(message, Category.Info, Priority.Low);
                dataBurner.OnProgress -= OnDataBurnerProgress;
                cancellationTokenSource.Cancel();
            }

            dataBurner.OnProgress -= OnDataBurnerProgress;
            EventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
            dataBurner.Drive.Eject();
        }

        void OnDataBurnerProgress(int percent, int timeRemaining)
        {
            EventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true) { PercentageCompleted = percent, Message = "Burning CD" });
        }

        protected virtual void CopyToNonOpticalMedia(string fileName, string zipFilePath, string tempPath)
        {
            SafeFileOperations.Copy(tempPath + fileName + ".xml", zipFilePath + fileName + ".xml");
            SafeFileOperations.Copy(tempPath + fileName + ".pdf", zipFilePath + fileName + ".pdf");
        }

        protected virtual void CleanUpAfterExport(string fileName, string tempPath)
        {
            SafeFileOperations.Delete(tempPath, fileName + ".*");
        }

        private void DisableBusyIndicator()
        {
            var busyStateChangedEvent = EventAggregator.GetEvent<BusyStateChanged>();
            if (busyStateChangedEvent != null) busyStateChangedEvent.Publish(new BusyPayload(false));
        }

        protected virtual void OnExportTaskFaulted(Exception faultingException)
        {
            var errorMessage = _messageManager.GetMessage(MessageKeys.BvaExportUnsuccessful);

            DisableBusyIndicator();
            if (_useIdealDispatcher)
                IdealDispatcher.BeginInvoke(() => DisplayExportErrorMessage(errorMessage));
            else
                DisplayExportErrorMessage(errorMessage);

            var exceptionToLog = faultingException.InnerException ?? faultingException;
            var logMessage = String.Format("{0}; inner exception: {1}", errorMessage.FormattedMessage, exceptionToLog.Message);
            Logger.Log(logMessage, Category.Exception, Priority.High);
        }

        private void DisplayExportErrorMessage(Message errorMessage)
        {
            _messageBoxDispatcher.ShowMessageBox(
                MessageBoxCategory.Error,
                errorMessage.FormattedMessage,
                ExportOrderingPhysiciansReportMessageBoxCaption, MessageBoxChoiceSet.Close);
        }

        private MessageBoxReturnResult DisplayExportSuccessMessage()
        {
            return _messageBoxDispatcher.ShowMessageBox(
                MessageBoxCategory.Information, ExportCompletedSuccessfullyMessage,
                ExportOrderingPhysiciansReportMessageBoxCaption, MessageBoxChoiceSet.Close);
        }

        protected virtual void OnExportTaskCompleted()
        {
            DisableBusyIndicator();
            
            if(_useIdealDispatcher)
                IdealDispatcher.BeginInvoke(() => DisplayExportSuccessMessage());
            else
                DisplayExportSuccessMessage();
        }

        protected string BuildFileNameForOrderingPhysiciansReport(string hospitalName, DateTime fromDate, DateTime toDate)
        {
            var fileName = string.Format(ExportFileNameFormatString, hospitalName, fromDate.ToString(Constants.DateRangeFormat), toDate.ToString(Constants.DateRangeFormat));

            fileName = ReplaceInvalidCharacters(fileName);
            return fileName;
        }

        #endregion
    }
}
