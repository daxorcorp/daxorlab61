﻿using System;
using System.Drawing.Printing;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Daxor.Lab.Utility.Reports.ReportControllers
{
    public class SettingsReportController : ReportControllerBase
    {
        #region Constants
        
        private const string PrintedLabel = "Printed: ";
        private const string PrintedTimestampFormat = "g";
        
        #endregion

        #region Fields

        private readonly SettingsReport _report = new SettingsReport();
        private readonly SettingsReportModel _settingsReportModel;
        
        #endregion
        
        #region Ctor

        public SettingsReportController(ILoggerFacade logger, IRegionManager regionManager, IEventAggregator eventAggregator, ISettingsManager settingsManager, SettingsReportModel settingsReportModel)
        {
            Logger = logger;
            RegionManager = regionManager;
            EventAggregator = eventAggregator;
            SettingsManager = settingsManager;

            _settingsReportModel = settingsReportModel;
        }

        #endregion

        #region Properties

        public override IReportDocument ReportToView
        {
            get { return _report; }
        }

        #endregion

        #region Methods

        public override void ExportReport()
        {
            throw new NotImplementedException();
        }

        public override void PrintReport()
        {
            var standardPrintController = new StandardPrintController();
            var printerSettings = new PrinterSettings();

            var reportProcessor = new ReportProcessor { PrintController = standardPrintController };
            reportProcessor.PrintReport(new InstanceReportSource {ReportDocument = _report}, printerSettings);
        }

        #endregion

        #region Helper Methods
        
        public override void LoadReportData()
        {
            _report.table_SystemSettings.DataSource = _settingsReportModel.SystemSettings;
            _report.table_CustomerSpecific.DataSource = _settingsReportModel.CustomerSpecificSettings;
            _report.table_AutomaticDailyBackup.DataSource = _settingsReportModel.AutomaticDailyBackupSettings;
            _report.table_Miscellaneous.DataSource = _settingsReportModel.MiscellaneousSettings;
            _report.table_Background.DataSource = _settingsReportModel.BackgroundSettings;
            
            _report.table_BVASettings.DataSource = _settingsReportModel.BvaSettings;
            _report.table_PatientRequiredFields.DataSource = _settingsReportModel.PatientRequiredFieldsSettings;
            _report.table_TestRequiredFields.DataSource = _settingsReportModel.TestRequiredFieldsSettings;
            _report.table_ReportFields.DataSource = _settingsReportModel.ReportFieldsSettings;
            _report.table_Labels.DataSource = _settingsReportModel.LabelsSettings;
            _report.table_AutomaticPointExclusion.DataSource = _settingsReportModel.AutomaticPointExclusionSettings;

            _report.table_General.DataSource = _settingsReportModel.GeneralSettings;
            _report.table_Calibration.DataSource = _settingsReportModel.CalibrationSettings;
            _report.table_Linearity.DataSource = _settingsReportModel.LinearitySettings;
            _report.table_Standards.DataSource = _settingsReportModel.StandardsSettings;
            _report.table_Contamination.DataSource = _settingsReportModel.ContaminationSettings;

            _report.textBoxPrintedOn.Value = PrintedLabel + DateTime.Now.ToString(PrintedTimestampFormat);
        }

        #endregion
    }
}
