﻿using System;

namespace Daxor.Lab.Utility.Reports.Common
{
    public class SpecialtySummaryItem
    {
        public String Specialty { get; set; }
        public int Sum { get; set; }

        protected bool Equals(SpecialtySummaryItem other)
        {
            return string.Equals(Specialty, other.Specialty) && Sum == other.Sum;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((SpecialtySummaryItem) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Specialty != null ? Specialty.GetHashCode() : 0)*397) ^ Sum;
            }
        }
    }
}
