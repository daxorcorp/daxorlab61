﻿using Telerik.Reporting;

namespace Daxor.Lab.Utility.Reports.Common
{
    public interface IReportProcessor
    {
        void PrintReport(Report report);
    }
}
