﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;

namespace Daxor.Lab.Utility.Reports.Common
{
    public interface IReportControllerFactory
    {
        ReportControllerBase CreateOrderingPhysiciansReportController(ILoggerFacade logger,
            IRegionManager regionManager,
            IEventAggregator eventAggregator,
            ISettingsManager settingsManager,
            DateTime fromDateTime,
            DateTime toDateTime,
            List<OrderingPhysicianDataRecord> orderingPhysicianDataRecords,
            IReportProcessor reportProcessor,
            IMessageBoxDispatcher messageBoxDispatcher,
            IFolderBrowser folderBrowser,
            IMessageManager messageManager,
            IStarBurnWrapper starBurnWrapper,
            IExcelFileBuilder excelFileBuilder);
    }
}
