﻿using System.Drawing.Printing;
using Telerik.Reporting;
using Telerik.Reporting.Processing;
using Report = Telerik.Reporting.Report;

namespace Daxor.Lab.Utility.Reports.Common
{
    public class ReportProcessorWrapper : IReportProcessor
    {
        public void PrintReport(Report report)
        {
            var standardPrintController = new StandardPrintController();
            var printerSettings = new PrinterSettings();

            var reportProcessor = new ReportProcessor { PrintController = standardPrintController };
            reportProcessor.PrintReport(new InstanceReportSource { ReportDocument = report }, printerSettings);            
        }
    }
}
