namespace Daxor.Lab.Utility.Reports
{
    partial class OrderingPhysiciansReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderingPhysiciansReport));
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.panelHospitalInformation = new Telerik.Reporting.Panel();
            this.textBoxHospitalName = new Telerik.Reporting.TextBox();
            this.textBoxReportDateRange = new Telerik.Reporting.TextBox();
            this.pictureBoxIcon = new Telerik.Reporting.PictureBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBoxPrintedOn = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1.4000002145767212D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panelHospitalInformation,
            this.pictureBoxIcon,
            this.textBox1});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            this.pageHeaderSection1.Style.Font.Bold = false;
            this.pageHeaderSection1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.pageHeaderSection1.Style.Font.Strikeout = false;
            this.pageHeaderSection1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // panelHospitalInformation
            // 
            this.panelHospitalInformation.Anchoring = Telerik.Reporting.AnchoringStyles.Left;
            this.panelHospitalInformation.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxHospitalName,
            this.textBoxReportDateRange});
            this.panelHospitalInformation.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.71999990940094D), Telerik.Reporting.Drawing.Unit.Inch(0.57007879018783569D));
            this.panelHospitalInformation.Name = "panelHospitalInformation";
            this.panelHospitalInformation.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.5D), Telerik.Reporting.Drawing.Unit.Inch(0.62999987602233887D));
            // 
            // textBoxHospitalName
            // 
            this.textBoxHospitalName.Docking = Telerik.Reporting.DockingStyle.Top;
            this.textBoxHospitalName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBoxHospitalName.Name = "textBoxHospitalName";
            this.textBoxHospitalName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.5D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxHospitalName.Style.Font.Name = "Times New Roman";
            this.textBoxHospitalName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxHospitalName.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxHospitalName.Value = "Hospital Name";
            // 
            // textBoxReportDateRange
            // 
            this.textBoxReportDateRange.Docking = Telerik.Reporting.DockingStyle.Top;
            this.textBoxReportDateRange.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxReportDateRange.Name = "textBoxReportDateRange";
            this.textBoxReportDateRange.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.5D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxReportDateRange.Style.Font.Name = "Times New Roman";
            this.textBoxReportDateRange.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxReportDateRange.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxReportDateRange.Value = "4/26/81 - 12/26/83";
            // 
            // pictureBoxIcon
            // 
            this.pictureBoxIcon.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.17000000178813934D));
            this.pictureBoxIcon.MimeType = "image/jpeg";
            this.pictureBoxIcon.Name = "pictureBoxIcon";
            this.pictureBoxIcon.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8125D), Telerik.Reporting.Drawing.Unit.Inch(0.8125D));
            this.pictureBoxIcon.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBoxIcon.Value = ((object)(resources.GetObject("pictureBoxIcon.Value")));
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(2D);
            this.detail.Name = "detail";
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.33000001311302185D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox21,
            this.textBoxPrintedOn});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.78000020980835D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.9894256591796875D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBox21.Style.Font.Name = "Times New Roman";
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox21.Value = "=\'Page \' + PageNumber + \' of \' + PageCount";
            // 
            // textBoxPrintedOn
            // 
            this.textBoxPrintedOn.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.8257546424865723D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBoxPrintedOn.Name = "textBoxPrintedOn";
            this.textBoxPrintedOn.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9541667699813843D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxPrintedOn.Style.Font.Name = "Times New Roman";
            this.textBoxPrintedOn.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBoxPrintedOn.Value = "textBoxDateTimePrinted";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7200000286102295D), Telerik.Reporting.Drawing.Unit.Inch(0.17000000178813934D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.5D), Telerik.Reporting.Drawing.Unit.Inch(0.40000000596046448D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox1.Value = "BVA Ordering Physicians Report";
            // 
            // OrderingPhysiciansReport
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "OrderingPhysiciansReport";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.37999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.940000057220459D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.Panel panelHospitalInformation;
        public Telerik.Reporting.TextBox textBoxHospitalName;
        public Telerik.Reporting.PictureBox pictureBoxIcon;
        public Telerik.Reporting.TextBox textBoxReportDateRange;
        private Telerik.Reporting.TextBox textBox21;
        public Telerik.Reporting.TextBox textBoxPrintedOn;
        public Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox textBox1;
    }
}