﻿using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Daxor.Lab.Utility.Reports.ReportModels
{
    public class OrderingPhysiciansReportModel
    {
        #region Constants

        public const string PrintedOnDateFormatString = "Printed: {0}";

        #endregion

        #region Fields

        private readonly IExcelFileBuilder _excelFileBuilder;
    
        #endregion

        #region Ctors

        public OrderingPhysiciansReportModel() 
        {
            OrderingPhysicianDataRecords = new List<OrderingPhysicianDataRecord>();
        }

        public OrderingPhysiciansReportModel(DateTime fromDateTime, DateTime toDateTime, List<OrderingPhysicianDataRecord> orderingPhysicianDataRecords, IExcelFileBuilder excelFileBuilder)
        {
            FromDateTime = fromDateTime;
            ToDateTime = toDateTime;
            OrderingPhysicianDataRecords = orderingPhysicianDataRecords;
            _excelFileBuilder = excelFileBuilder;
        }

        #endregion

        #region Properties

        public DateTime FromDateTime { get; set; }
        public DateTime ToDateTime { get; set; }
        public List<OrderingPhysicianDataRecord> OrderingPhysicianDataRecords { get; set; }

        #endregion

        #region API

        public virtual string GeneratePrintedOnDate()
        {
            return String.Format(PrintedOnDateFormatString, DateTime.Now.ToString("g"));
        }

        public IExcelFileBuilder GenerateExcelFileBuilder(string hospitalName)
        {
            _excelFileBuilder.AddWorksheet("Ordering Physicians");
            
            var currentRow = 1;

            var worksheet = _excelFileBuilder.GetFirstWorksheet();

            _excelFileBuilder.InsertStringCell(worksheet, currentRow, 1, "BVA Results for: ", CellStyle.Bold);
            _excelFileBuilder.InsertStringCell(worksheet, currentRow, 2, hospitalName, CellStyle.None);
            _excelFileBuilder.InsertStringCell(worksheet, currentRow, 3, "From: " + FromDateTime.ToString(Constants.DateRangeFormat), CellStyle.None);
            _excelFileBuilder.InsertStringCell(worksheet, currentRow, 4, "To: " + ToDateTime.ToString(Constants.DateRangeFormat), CellStyle.None);
            
            currentRow++;

            _excelFileBuilder.InsertStringCell(worksheet, currentRow, 1, "Exported On: ", CellStyle.Bold);

            _excelFileBuilder.InsertStringCell(worksheet, currentRow, 2, DateTime.Now.ToString("g"), CellStyle.None);
            
            currentRow++;

            _excelFileBuilder.InsertStringCell(worksheet, currentRow, 1, "Version: ", CellStyle.Bold);

            var version = Assembly.GetExecutingAssembly().GetName().Version;
            _excelFileBuilder.InsertStringCell(worksheet, currentRow, 2, version.ToString(), CellStyle.None);

            currentRow += 2;

            var columnHeaders = new List<OrderingPhysiciansColumnHeader>();
            for (var i = 0; i < Enum.GetNames(typeof(OrderingPhysiciansColumnHeader)).Count(); i++)
                columnHeaders.Add((OrderingPhysiciansColumnHeader)i);

            var currentColumn = 0;
            const CellStyle cellStyle = CellStyle.ColumnHeading;

            foreach (var content in columnHeaders.Select(columnHeader => columnHeader.GetStringValue()))
            {
                _excelFileBuilder.InsertStringCell(worksheet, currentRow, ++currentColumn, content, cellStyle);
            }

            currentRow++;

            foreach (var orderingPhysicianDataRecord in OrderingPhysicianDataRecords)
            {
                var physiciansName = orderingPhysicianDataRecord.ReferringPhysician;
                var physiciansSpecialty = orderingPhysicianDataRecord.PhysicianSpecialty;

                foreach (var testRecord in orderingPhysicianDataRecord.TestRecordsList)
                {
                    currentColumn = 0;
                    _excelFileBuilder.InsertStringCell(worksheet, currentRow, ++currentColumn, physiciansName, CellStyle.None);
                    _excelFileBuilder.InsertStringCell(worksheet, currentRow, ++currentColumn, physiciansSpecialty, CellStyle.None);
                    _excelFileBuilder.InsertDateTimeCell(worksheet, currentRow, ++currentColumn, testRecord.TestDate, CellStyle.UsDate);
                    _excelFileBuilder.InsertStringCell(worksheet, currentRow, ++currentColumn, testRecord.Analyst, CellStyle.None);
                    _excelFileBuilder.InsertStringCell(worksheet, currentRow, ++currentColumn, testRecord.CcReportTo, CellStyle.None);
                    _excelFileBuilder.InsertStringCell(worksheet, currentRow, ++currentColumn, testRecord.Location, CellStyle.None);
                    currentRow++;
                }
            }

            return _excelFileBuilder;
        }

        #endregion
    }
}
