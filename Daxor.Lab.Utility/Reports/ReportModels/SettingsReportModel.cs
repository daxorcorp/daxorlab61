﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.SettingsManager.Models;
using Daxor.Lab.Utility.Models;

namespace Daxor.Lab.Utility.Reports.ReportModels
{
    public class SettingsReportModel
    {
        #region Constants

        private const string TimeType = "time";
        private const string DateType = "date";
        private const string CollectionType = "collection";
        private const string SecureStringType = "securestring";

        private const int SystemModuleId = 1;
        private const int BvaModuleId = 2;
        private const int QcModuleId = 3;
        
        private const int SystemSettingsGroupId = 0;
        private const int CustomerSpesificSettingsGroupId = 2;
        private const int AutomaticDailyBackupSettingsGroupId = 4;
        private const int MiscellaneousSettingsGroupId = 5;
        private const int BackgroundSettingsGroupId = 6;

        private const int BvaSettingsGroupId = 0;
        private const int PatientRequiredFieldsSettingsGroupId = 1;
        private const int TestRequiredFieldsSettingsGroupId = 2;
        private const int ReportFieldsSettingsGroupId = 3;
        private const int LabelsSettingsGroupId = 4;
        private const int AutomaticPointExclusionSettingsGroupId = 5;

        private const int GeneralSettingsGroupId = 0;
        private const int CalibrationSettingsGroupId = 1;
        private const int LinearitySettingsGroupId = 2;
        private const int StandardsSettingsGroupId = 3;
        private const int ContaminationSettingsGroupId = 4;

        #endregion

        private readonly List<SettingBase> _settingsCollection; 

        #region CTOR

        public SettingsReportModel(List<SettingBase> settingsCollection)
        {
            _settingsCollection = settingsCollection;

            SystemSettings = RetrieveFormattedSettings(SystemModuleId, SystemSettingsGroupId, SecureStringType);
            CustomerSpecificSettings = RetrieveFormattedSettings(SystemModuleId, CustomerSpesificSettingsGroupId, SecureStringType);
            AutomaticDailyBackupSettings = RetrieveFormattedSettings(SystemModuleId, AutomaticDailyBackupSettingsGroupId, SecureStringType);
            MiscellaneousSettings = RetrieveFormattedSettings(SystemModuleId, MiscellaneousSettingsGroupId, SecureStringType);
            BackgroundSettings = RetrieveFormattedSettings(SystemModuleId, BackgroundSettingsGroupId, SecureStringType);

            BvaSettings = RetrieveFormattedSettings(BvaModuleId, BvaSettingsGroupId, CollectionType);
            PatientRequiredFieldsSettings = RetrieveFormattedSettings(BvaModuleId, PatientRequiredFieldsSettingsGroupId, CollectionType);
            TestRequiredFieldsSettings = RetrieveFormattedSettings(BvaModuleId, TestRequiredFieldsSettingsGroupId, CollectionType);
            ReportFieldsSettings = RetrieveFormattedSettings(BvaModuleId, ReportFieldsSettingsGroupId, CollectionType);
            LabelsSettings = RetrieveFormattedSettings(BvaModuleId, LabelsSettingsGroupId, CollectionType);
            AutomaticPointExclusionSettings = RetrieveFormattedSettings(BvaModuleId, AutomaticPointExclusionSettingsGroupId, CollectionType);

            GeneralSettings = RetrieveFormattedSettings(QcModuleId, GeneralSettingsGroupId);
            CalibrationSettings = RetrieveFormattedSettings(QcModuleId, CalibrationSettingsGroupId);
            LinearitySettings = RetrieveFormattedSettings(QcModuleId, LinearitySettingsGroupId);
            StandardsSettings = RetrieveFormattedSettings(QcModuleId, StandardsSettingsGroupId);
            ContaminationSettings = RetrieveFormattedSettings(QcModuleId, ContaminationSettingsGroupId);
        }

        #endregion

        #region Properties

        public List<KeyValuePair<string, string>> SystemSettings { get; set; }
        public List<KeyValuePair<string, string>> CustomerSpecificSettings { get; set; }
        public List<KeyValuePair<string, string>> AutomaticDailyBackupSettings { get; set; }
        public List<KeyValuePair<string, string>> MiscellaneousSettings { get; set; }
        public List<KeyValuePair<string, string>> BackgroundSettings { get; set; }

        public List<KeyValuePair<string, string>> BvaSettings { get; set; }
        public List<KeyValuePair<string, string>> PatientRequiredFieldsSettings { get; set; }
        public List<KeyValuePair<string, string>> TestRequiredFieldsSettings { get; set; }
        public List<KeyValuePair<string, string>> ReportFieldsSettings { get; set; }
        public List<KeyValuePair<string, string>> LabelsSettings { get; set; }
        public List<KeyValuePair<string, string>> AutomaticPointExclusionSettings { get; set; }

        public List<KeyValuePair<string, string>> GeneralSettings { get; set; }
        public List<KeyValuePair<string, string>> CalibrationSettings { get; set; }
        public List<KeyValuePair<string, string>> LinearitySettings { get; set; }
        public List<KeyValuePair<string, string>> StandardsSettings { get; set; }
        public List<KeyValuePair<string, string>> ContaminationSettings { get; set; }


        #endregion

        #region Helper Methods

        private List<KeyValuePair<string, string>> RetrieveFormattedSettings(int moduleId, int groupId, string valueTypeToExclude = null)
        {
            var formattedSettings = from setting in _settingsCollection
                                    where setting.ModuleId == moduleId && setting.GroupId == groupId
                                          && setting.ValueTypeAsString != valueTypeToExclude
                                    orderby setting.GroupId, setting.OrderWithinGroup
                                    select new KeyValuePair<string, string>(setting.Header, FormatSettingValue(setting));

            return formattedSettings.ToList();
        }

        public static string FormatSettingValue(SettingBase setting)
        {
            if (setting == null)
                throw new ArgumentNullException("setting");

            var choiceSetting = setting as ChoiceSetting;

            if (choiceSetting != null)
                return choiceSetting.AvailableValues.ToDictionary(p => p.Key)[choiceSetting.Value.ToString()].Value;

            if (setting.ValueTypeAsString == TimeType)
                return DateTime.Parse(setting.Value.ToString()).ToShortTimeString();

            return setting.ValueTypeAsString == DateType ? DateTime.Parse(setting.Value.ToString()).ToShortDateString() : setting.Value.ToString();
        }

        #endregion
    }
}
