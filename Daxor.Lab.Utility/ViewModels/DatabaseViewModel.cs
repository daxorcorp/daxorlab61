﻿using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Utility.ViewModels
{
    public class DatabaseViewModel
    {
        public RestoreDatabaseSubViewModel RestoreDatabaseModel { get; set; }

        public RestoreQCDatabaseArchiveSubViewModel RestoreQCDatabaseArchiveModel { get; set; }

        public DatabaseViewModel(IMessageBoxDispatcher messageBoxDispatcher, IEventAggregator eventAggregator, 
                                 ILoggerFacade logger, ISettingsManager settingsManager, IRestoreServiceFactory serviceFactory, 
                                 IMessageManager messageManager, IZipFileFactory zipFileFactory)
        {
            RestoreDatabaseModel = new RestoreDatabaseSubViewModel(messageBoxDispatcher, eventAggregator, logger, settingsManager, serviceFactory, messageManager, zipFileFactory);
            RestoreQCDatabaseArchiveModel = new RestoreQCDatabaseArchiveSubViewModel(messageBoxDispatcher, logger, settingsManager, serviceFactory, messageManager, zipFileFactory);
        }
    }
}
