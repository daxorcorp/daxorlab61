﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Win32;

namespace Daxor.Lab.Utility.ViewModels
{
    public class RestoreQCDatabaseArchiveSubViewModel : ViewModelBase
    {
        #region Fields
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly IDatabaseRestore _qcArchiveRestore;
        private readonly ILoggerFacade _logger;
        private readonly ISettingsManager _settingsManager;
        private readonly IRestoreServiceFactory _serviceFactory;
        private readonly IMessageManager _messageManager;
        private Visibility _mask;
        private string _tempFilePath;
        private string _restoreFilePath;

        #endregion

        #region Properties
        public string RestoreFilePath
        {
            get { return _restoreFilePath; }
            set
            {
                _restoreFilePath = value;
                RestoreDatabaseCommand.RaiseCanExecuteChanged();
                FirePropertyChanged("RestoreFilePath");
            }
        }
        
        public Visibility MaskVisibility
        {
            get { return _mask; }
            set { base.SetValue(ref _mask, "MaskVisibility", value); }
        }

        #endregion

        #region Ctor

        public RestoreQCDatabaseArchiveSubViewModel(IMessageBoxDispatcher messageBoxDispatcher, ILoggerFacade logger, 
                                                    ISettingsManager settingsManager, IRestoreServiceFactory serviceFactory,
                                                    IMessageManager messageManager, IZipFileFactory zipFileFactory)
        {
            _messageBoxDispatcher = messageBoxDispatcher;
            _logger = logger;
            _settingsManager = settingsManager;
            _qcArchiveRestore = new QCArchiveRestore(messageBoxDispatcher, logger, settingsManager, messageManager, zipFileFactory);
            _serviceFactory = serviceFactory;
            _messageManager = messageManager;
            MaskVisibility = Visibility.Collapsed;

            RestoreDatabaseCommand = new DelegateCommand<object>(ExecuteRestoreDatabase,
                           o => File.Exists(RestoreFilePath));
            SelectFilePathCommand = new DelegateCommand<object>(ExecuteSelectFilePath, o => true);
        }

        #endregion

        #region Commands

        public DelegateCommand<object> SelectFilePathCommand
        {
            private set;
            get;
        }

        public DelegateCommand<object> RestoreDatabaseCommand
        {
            private set;
            get;
        }

        #endregion

        #region Helpers

        private void ExecuteSelectFilePath(object arg)
        {
            var openFileDialog = new OpenFileDialog
                {
                    Filter = "All Files (*.*)|*.*|Daxor backup files (*.bak,*.zip)|*.bak;*.zip",
                    FilterIndex = 2,
                    FileName = @"C:\ProgramData\Daxor\Lab\Backups\QCDatabaseArchive.bak"
                };

            if (!((bool) openFileDialog.ShowDialog())) return;

            RestoreFilePath = openFileDialog.FileName;
            try
            {
                _tempFilePath = _qcArchiveRestore.GetTemporaryFileRestorePathForZipFiles(RestoreFilePath);
            }
            catch (Exception ex)
            {
                if (ex is DatabaseRestoreBackupFileNotFoundException)
                {
                    var fileName =
                        _settingsManager.GetSetting<string>(RuntimeSettingKeys.SystemQcArchiveDatabaseBackupFilename);
                    var message =
                        _messageManager.GetMessage(MessageKeys.UtilityRestoreQcArchiveFailed).FormattedMessage;
                    message = String.Format(message, fileName);
                    ShowMessage(message);
                    _logger.Log("The database restore failed because the backup file did not exist in the zip file: " + message, Category.Exception, Priority.High);
                }
                else throw;
            }
        }

        private void ExecuteRestoreDatabase(object arg)
        {

            if (_qcArchiveRestore.CanExecuteRestore(_tempFilePath))
            {
                MaskVisibility = Visibility.Visible;
                Task.Factory.StartNew(() =>
                {
                    var service = _serviceFactory.CreateQcArchiveRestoreService();
                    service.RestoreCompletedHandler = sqlServer_RestoreComplete;
                    try
                    {
                        service.Restore(_tempFilePath);
                    }
                    catch (Exception ex)
                    {
                        var message = _messageManager.GetMessage(MessageKeys.UtilityRestoreQcArchiveFailed).FormattedMessage;
                        ShowMessage(message, ex);
                    }
                });
            }
            else
            {
                var errorMessage = _messageManager.GetMessage(MessageKeys.UtilityRestoreQcArchiveFailed).FormattedMessage;
                var systemQcArchiveBackupFilename = _settingsManager.GetSetting<string>(RuntimeSettingKeys.SystemQcArchiveDatabaseBackupFilename); 
                errorMessage = String.Format(errorMessage, "\"" + systemQcArchiveBackupFilename + "\"");
                ShowMessage(errorMessage);
            }

        }

        private void ShowMessage(string message, Exception ex = null)
        {
            MaskVisibility = Visibility.Collapsed;
            IdealDispatcher.BeginInvoke(() =>
                _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, message, "Database Restore",
                    MessageBoxChoiceSet.Close));
            _qcArchiveRestore.CleanUpTempFiles(_tempFilePath);
            
            if (ex == null)
                _logger.Log(message, Category.Info, Priority.Low);
            else
                _logger.Log(message + "\n" + ex.Message, Category.Exception, Priority.Low);
        }

        private void sqlServer_RestoreComplete(object sender, Microsoft.SqlServer.Management.Common.ServerMessageEventArgs e)
        {
            MaskVisibility = Visibility.Collapsed;
            _qcArchiveRestore.CleanUpTempFiles(_tempFilePath);
            IdealDispatcher.BeginInvoke(() =>
                _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information, "Restore completed.", "Database Restore", MessageBoxChoiceSet.Close));
        }

        #endregion
    }
}
