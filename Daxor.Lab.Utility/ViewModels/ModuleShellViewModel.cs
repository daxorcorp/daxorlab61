﻿using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Services;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;

namespace Daxor.Lab.Utility.ViewModels
{
    public class ModuleShellViewModel : ViewModelBase, IActiveAware
    {
        #region Constants

        public const string CalibrateViewId = "calibrate";
        public const string ResetPrinterViewId = "resetPrinter";
        public const string ManualBackupViewId = "backup";
        public const string QcArchiveViewId = "qcArchive";
        public const string OrderingPhysiciansReportsViewId = "salesReports";
        public const string ExportViewId = "export";
        public const string HomeViewId = "home";
        public const string SettingsViewId = "cusSettings";
        public const string DateAndTimeViewId = "time";
        public const string DatabaseViewId = "database";
        public const string ServicesViewId = "sysSettings";

        public const string CalibrateScreenDescription = "Calibrate Screen";
        public const string ResetPrinterDescription = "Reset Printer";
        public const string ManualBackupDescription = "Manual Backup";
        public const string QcArchivesDescription = "QC Archives";
        public const string OrderingPhysiciansReportsDescription = "Ordering Physicians Report";
        public const string ExportResultsToExcelDescription = "Export Results\nto Excel";
        public const string HomeDescription = "Home";
        public const string SettingsDescription = "Settings";
        public const string DateAndTimeDescription = "Date & Time";
        public const string DatabaseDescription = "Database";
        public const string ServicesDescription = "Services";

        private const string SuccessMessageResetPrinter = "The printer has been reset.";
        private const string CaptionPrinterReset = "Printer Reset";
        private const string TouchscreenCalibrationFailed = "Touchscreen Calibration Failed";
        private const string TouchscreenCalibrationDoesntExist = "Touchscreen Calibration Could Not Be Found";

        #endregion

        #region Fields

        private readonly IAppModuleNavigator _navigator;
        private readonly IAuthenticationManager _authService;
        private readonly ObservableCollection<NavigationRoute> _navigationRoutes = new ObservableCollection<NavigationRoute>();
        private readonly PropertyObserver<IAuthenticationManager> _authServiceObserver;
        private readonly ILoggerFacade _logger;
        private readonly IMessageBoxDispatcher _msgDispatcher;
        private NavigationRoute _activeRoute;
        private SettingObserver<ISettingsManager> _sObserver;
        private readonly IMessageManager _messageManager;

        #endregion
        
        #region Properties

        public ObservableCollection<NavigationRoute> NavigationRoutes
        {
            get { return _navigationRoutes; }
        }

        private bool ShowQcArchivesNavigationRoute { get; set; }
        private bool ShowOrderingPhysiciansReportNavigationRoute { get; set; }
        private bool HasAuthenticatedUserChanged { get; set; }

        private SettingObserver<ISettingsManager> SettingObserver
        {
            get { return _sObserver; }
            set
            {
                // Only get one assignment
                if (_sObserver == null)
                    _sObserver = value;
            }
        }

        #endregion

        #region Ctor

        public ModuleShellViewModel([Dependency(AppModuleIds.Utilities)] IAppModuleNavigator localNavigator, IAuthenticationManager authService,
                                    ILoggerFacade logger, IMessageBoxDispatcher msgDispatcher, IMessageManager messageManager, ISettingsManager settingsManager)
        {
            _navigator = localNavigator;
            _authService = authService;
            _logger = logger;
            _msgDispatcher = msgDispatcher;
            _messageManager = messageManager;
            _authServiceObserver = new PropertyObserver<IAuthenticationManager>(_authService);
            _authServiceObserver.RegisterHandler(s => s.LoggedInUser, OnAuthenticatedUserChanged);

            ShowQcArchivesNavigationRoute = settingsManager.GetSetting<bool>(SettingKeys.SystemShowQcArchivesButton);
            ShowOrderingPhysiciansReportNavigationRoute =settingsManager.GetSetting<bool>(SettingKeys.SystemShowOrderingPhysiciansReportButton);
            SettingObserver = new SettingObserver<ISettingsManager>(settingsManager);
            SettingObserver.RegisterHandler(SettingKeys.SystemShowQcArchivesButton,
                                            s =>
                                                {
                                                    ShowQcArchivesNavigationRoute = s.GetSetting<bool>(SettingKeys.SystemShowQcArchivesButton);
                                                    if (!HasAuthenticatedUserChanged) return;

                                                    OnAuthenticatedUserChanged(_authService);
                                                    HasAuthenticatedUserChanged = false;
                                                });

            SettingObserver.RegisterHandler(SettingKeys.SystemShowOrderingPhysiciansReportButton,
                                            s =>
                                            {
                                                ShowOrderingPhysiciansReportNavigationRoute = s.GetSetting<bool>(SettingKeys.SystemShowOrderingPhysiciansReportButton);
                                                if (!HasAuthenticatedUserChanged) return;

                                                OnAuthenticatedUserChanged(_authService);
                                                HasAuthenticatedUserChanged = false;
                                            });

            NavigationRoutes.AddRange(GetAnonymousNavigationRoutes());
        }
        #endregion

        #region Helpers

        protected IEnumerable<NavigationRoute> GetAnonymousNavigationRoutes()
        {
            yield return new NavigationRoute(HomeViewId) { Description = HomeDescription, Invoke = r => { UpdateActiveRoute(r); _authService.Logout(); _navigator.ActivateView(UtilityModuleViewKeys.HomeView); } };
            yield return new NavigationRoute(CalibrateViewId) { Description = CalibrateScreenDescription, Invoke = r => CalibrateTouchScreen()};
            yield return new NavigationRoute(ResetPrinterViewId) { Description = ResetPrinterDescription, Invoke = r => { UpdateActiveRoute(r); ResetPrinter(); UpdateActiveRoute(null); } };
            yield return new NavigationRoute(ManualBackupViewId) { Description = ManualBackupDescription, Invoke = r => { UpdateActiveRoute(r); _navigator.ActivateView(UtilityModuleViewKeys.ManualBackupView); } };
            if (ShowOrderingPhysiciansReportNavigationRoute)
                yield return new NavigationRoute(OrderingPhysiciansReportsViewId) { Description = OrderingPhysiciansReportsDescription, Invoke = r=>{ UpdateActiveRoute(r); _navigator.ActivateView(UtilityModuleViewKeys.OrderingPhysiciansReportView);} };
            if (ShowQcArchivesNavigationRoute)
                yield return new NavigationRoute(QcArchiveViewId) { Description = QcArchivesDescription, Invoke = r => { UpdateActiveRoute(r); QcArchive(); UpdateActiveRoute(null); } };
        }

        private void QcArchive()
        {
            try
            {
                if (File.Exists(@".\QCTestArchive.exe"))
                {
                    var p = new Process {StartInfo = {FileName = @".\QCTestArchive.exe"}};
                    p.Start();
                }
            }
            catch (Exception ex)
            {
                _logger.Log("Unable to launch the QC test archive application: " + ex.Message, Category.Exception, Priority.High);
            }

        }

        private IEnumerable<NavigationRoute> GetCustomerAdminNavigationRoutes()
        {
            yield return new NavigationRoute(HomeViewId) { Description = HomeDescription, Invoke = r => { UpdateActiveRoute(r); _authService.Logout(); _navigator.ActivateView(UtilityModuleViewKeys.HomeView); } };
            yield return new NavigationRoute(SettingsViewId) { Description = SettingsDescription, Invoke = r => { UpdateActiveRoute(r); _navigator.ActivateView(UtilityModuleViewKeys.SettingsView); } };
            yield return new NavigationRoute(DateAndTimeViewId) { Description = DateAndTimeDescription, Invoke = r => ComputerSystemChanges.LaunchDateAndTimeControlPanel() };
            yield return new NavigationRoute(ExportViewId) { Description = ExportResultsToExcelDescription, Invoke = r => { UpdateActiveRoute(r); _navigator.ActivateView(UtilityModuleViewKeys.ExportView); } };
        }

        private IEnumerable<NavigationRoute> GetServiceNavigationRoutes()
        {
            yield return new NavigationRoute(HomeViewId) { Description = HomeDescription, Invoke = r => { UpdateActiveRoute(r); _authService.Logout(); _navigator.ActivateView(UtilityModuleViewKeys.HomeView); } };
            yield return new NavigationRoute(SettingsViewId) { Description = SettingsDescription, Invoke = r => { UpdateActiveRoute(r); _navigator.ActivateView(UtilityModuleViewKeys.SettingsView); } };
            yield return new NavigationRoute(DateAndTimeViewId) { Description = DateAndTimeDescription, Invoke = r => ComputerSystemChanges.LaunchDateAndTimeControlPanel() };
            yield return new NavigationRoute(ExportViewId) { Description = ExportResultsToExcelDescription, Invoke = r => { UpdateActiveRoute(r); _navigator.ActivateView(UtilityModuleViewKeys.ExportView); } };
            yield return new NavigationRoute(DatabaseViewId) { Description = DatabaseDescription, Invoke = r => { UpdateActiveRoute(r); _navigator.ActivateView(UtilityModuleViewKeys.DatabaseView); } };
            yield return new NavigationRoute(ServicesViewId) { Description = ServicesDescription, Invoke = r => { UpdateActiveRoute(r); _navigator.ActivateView(UtilityModuleViewKeys.ServiceView); } };
        }

        private void OnAuthenticatedUserChanged(IAuthenticationManager authService)
        {
            //Clear current navigation routes
            NavigationRoutes.Clear();

            if (authService.LoggedInUser == null)
            {
                NavigationRoutes.AddRange(GetAnonymousNavigationRoutes());
                UpdateActiveRoute(NavigationRoutes[0]);
                return;
            }

            switch (authService.LoggedInUser.AuthLevel)
            {
                case AuthorizationLevel.CustomerAdmin: NavigationRoutes.AddRange(GetCustomerAdminNavigationRoutes()); break;
                case AuthorizationLevel.Internal: NavigationRoutes.AddRange(GetServiceNavigationRoutes()); break;
                case AuthorizationLevel.Service: NavigationRoutes.AddRange(GetServiceNavigationRoutes()); break;
            }

            HasAuthenticatedUserChanged = true;
        }

        void CalibrateTouchScreen()
        {
            try
            {
                ComputerSystemChanges.CalibrateTouchScreen();
            }
            catch (FileNotFoundException ex)
            {
                string message = _messageManager.GetMessage(MessageKeys.SysTouchScreenCalibrationDoesntExist).FormattedMessage;
                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error, message, TouchscreenCalibrationDoesntExist, MessageBoxChoiceSet.Close);
                _logger.Log(ex.Message, Category.Exception, Priority.High);
            }
            catch (Exception ex)
            {
                string message = _messageManager.GetMessage(MessageKeys.SysTouchScreenCalibrationFailed).FormattedMessage;
                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error, message, TouchscreenCalibrationFailed, MessageBoxChoiceSet.Close);
                _logger.Log(String.Format("{0}\n{1}", ex.Message, ex.StackTrace), Category.Exception, Priority.High);
            }
        }

        void ResetPrinter()
        {
            try
            {
                var computerSystemChanges = new ComputerSystemChanges(_messageManager);
                computerSystemChanges.ResetAllConnectedPrinters();
                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Information, SuccessMessageResetPrinter, CaptionPrinterReset, MessageBoxChoiceSet.Close);
            }
            catch (ApplicationException ex)
            {
                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error, ex.Message, CaptionPrinterReset, MessageBoxChoiceSet.Close);
                _logger.Log(ex.Message, Category.Info, Priority.Low);
            }
            catch (Exception ex)
            {
                string message = _messageManager.GetMessage(MessageKeys.SysPrinterResetFailed).FormattedMessage;
                _logger.Log(String.Format("{0}\n{1}", ex.Message, ex.StackTrace), Category.Exception, Priority.High);
                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error, message, CaptionPrinterReset, MessageBoxChoiceSet.Close);
            }
        }

        void UpdateActiveRoute(NavigationRoute nRoute)
        {
            if (nRoute == _activeRoute)
                return;

            if (_activeRoute != null)
                _activeRoute.InUse = false;

            _activeRoute = nRoute;
            if (_activeRoute != null)
                _activeRoute.InUse = true;
        }
        
        #endregion

        #region IActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;

                if (!_isActive)
                    _authService.Logout();
                else
                    UpdateActiveRoute(NavigationRoutes[0]);
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion

        #region Override

        protected override void OnDispose()
        {
            _authServiceObserver.UnregisterHandler(s => s.LoggedInUser);
            base.OnDispose();
        }
        #endregion
    }
}
