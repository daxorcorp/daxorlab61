﻿using Daxor.Lab.DatabaseServices.Common;
using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.SqlServer.Management.Common;
using Microsoft.Win32;
using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;

namespace Daxor.Lab.Utility.ViewModels
{
    public class RestoreDatabaseSubViewModel : ViewModelBase
    {
        #region DLLImports

        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        internal static extern bool OpenProcessToken(IntPtr h, int acc, ref IntPtr phtok);

        [DllImport("advapi32.dll", SetLastError = true)]
        internal static extern bool LookupPrivilegeValue(string host, string name, ref long pluid);

        [DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
        internal static extern bool AdjustTokenPrivileges(IntPtr htok, bool disall,
            ref TokPriv1Luid newst, int len, IntPtr prev, IntPtr relen);
        [DllImport("user32.dll", ExactSpelling = true, SetLastError = true)]
        internal static extern bool ExitWindowsEx(int flg, int rea);

        [DllImport("kernel32.dll", ExactSpelling = true)]
        internal static extern IntPtr GetCurrentProcess();

        // ReSharper disable InconsistentNaming
        internal const int SE_PRIVILEGE_ENABLED = 0x00000002;
        internal const int TOKEN_QUERY = 0x00000008;
        internal const int TOKEN_ADJUST_PRIVILEGES = 0x00000020;
        internal const string SE_SHUTDOWN_NAME = "SeShutdownPrivilege";
        internal const int EWX_LOGOFF = 0x00000000;
        internal const int EWX_SHUTDOWN = 0x00000001;
        internal const int EWX_REBOOT = 0x00000002;
        internal const int EWX_FORCE = 0x00000004;
        internal const int EWX_POWEROFF = 0x00000008;
        internal const int EWX_FORCEIFHUNG = 0x00000010;
        // ReSharper restore InconsistentNaming

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        internal struct TokPriv1Luid
        {
            public int Count;
            public long Luid;
            public int Attr;
        }
        #endregion

        #region Fields

        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly IEventAggregator _eventAggregator;
        private readonly IDatabaseRestore _databaseRestore;
        private readonly ISettingsManager _settingsManager;
        private readonly IRestoreServiceFactory _serviceFactory;
        private readonly ILoggerFacade _logger;
        private readonly IMessageManager _messageManager;

        private Visibility _mask;
        
        private string _restoreFilePath;
        private string _tempFilePath;

        #endregion
        
        #region Properties

        public string RestoreFilePath
        {
            get { return _restoreFilePath; }
            set
            {
                _restoreFilePath = value;
                RestoreDatabaseCommand.RaiseCanExecuteChanged();
                FirePropertyChanged("RestoreFilePath");
            }
        }

        public Visibility MaskVisibility
        {
            get { return _mask; }
            set { base.SetValue(ref _mask, "MaskVisibility", value); }
        }

        #endregion

        #region Ctor

        public RestoreDatabaseSubViewModel(IMessageBoxDispatcher messageBoxDispatcher, IEventAggregator eventAggregator, 
                                           ILoggerFacade logger, ISettingsManager settingsManager, 
                                           IRestoreServiceFactory serviceFactory, IMessageManager messageManager, IZipFileFactory zipFileFactory)
        {
            _messageBoxDispatcher = messageBoxDispatcher;
            _eventAggregator = eventAggregator;
            _databaseRestore = new DaxorLabRestore(_messageBoxDispatcher, logger, settingsManager, messageManager, zipFileFactory);
            _serviceFactory = serviceFactory;
            _settingsManager = settingsManager;
            _logger = logger;
            _messageManager = messageManager;
            MaskVisibility = Visibility.Collapsed;

            RestoreDatabaseCommand = new DelegateCommand<object>(ExecuteRestoreDatabase, CanExecuteRestoreDatabase);
            SelectFilePathCommand = new DelegateCommand<object>(ExecuteSelectFilePath);
        }

        #endregion

        #region Commands

        public DelegateCommand<object> SelectFilePathCommand
        {
            private set;
            get;
        }

        public DelegateCommand<object> RestoreDatabaseCommand
        {
            private set;
            get;
        }

        #endregion

        #region Helper Methods

        private bool CanExecuteRestoreDatabase(object arg)
        {
            return !String.IsNullOrWhiteSpace(RestoreFilePath);
        }

        private void ExecuteSelectFilePath(object arg)
        {
            var openFileDialog = new OpenFileDialog
                {
                    Filter = "All Files (*.*)|*.*|Daxor backup files (*.bak,*.zip)|*.bak;*.zip",
                    FilterIndex = 2
                };

            if (!((bool) openFileDialog.ShowDialog())) return;

            RestoreFilePath = openFileDialog.FileName;
            try
            {
                _tempFilePath = _databaseRestore.GetTemporaryFileRestorePathForZipFiles(RestoreFilePath);
            }
            catch (Exception ex)
            {
                if (ex is DatabaseRestoreBackupFileNotFoundException)
                {
                    var fileName =
                        _settingsManager.GetSetting<string>(RuntimeSettingKeys.SystemDaxorLabDatabaseBackupFilename);
                    var message =
                        _messageManager.GetMessage(MessageKeys.UtilityRestoreDatebaseFailed).FormattedMessage;
                    message = String.Format(message, fileName);
                    DisplayErrorMessage(message);
                    _logger.Log("The database restore failed because the backup file did not exist in the zip file: " + message, Category.Exception, Priority.High);
                }
                else throw;
            }
        }

        private void ExecuteRestoreDatabase(object arg)
        {
            if (!_databaseRestore.CanExecuteRestore(_tempFilePath)) return;

            MaskVisibility = Visibility.Visible;
            _eventAggregator.GetEvent<ManualRestoreStarted>().Publish(null);

            Task.Factory.StartNew(() =>
            {
                var restore = _serviceFactory.CreateDaxorDatabaseRestoreService();
                restore.RestoreCompletedHandler = OnRestoreCompleted;
                try
                {
                    restore.Restore(_tempFilePath);
                }
                catch (Exception ex)
                {
                    if (ex is DatabaseVersionMismatchException)
                    {
                        var runningVersion = _settingsManager.GetSetting<string>(SettingKeys.BvaModuleVersion);
                        runningVersion = runningVersion.Remove(runningVersion.LastIndexOf(".", StringComparison.Ordinal));

                        var backupVersion = VersionExtractor.GetBackupVersion(_tempFilePath);
                        if (String.IsNullOrWhiteSpace(backupVersion))
                            backupVersion = "Prior to 6.0.3.x";

                        backupVersion = backupVersion.Remove(backupVersion.LastIndexOf(".", StringComparison.Ordinal));

                        var message = _messageManager.GetMessage(MessageKeys.UtilityDatabaseVersionMismatch).FormattedMessage;
                        message = String.Format(message, backupVersion, runningVersion);
                        DisplayErrorMessage(message);
                    }
                    else
                    {
                        var message = _messageManager.GetMessage(MessageKeys.UtilityRestoreDatebaseFailed).FormattedMessage;
                        DisplayErrorMessage(message, ex);
                    }
                }
            });
        }

        private void DisplayErrorMessage(string message, Exception ex = null)
        {
            _eventAggregator.GetEvent<ManualRestoreCompleted>().Publish(null);
            MaskVisibility = Visibility.Collapsed;
            IdealDispatcher.BeginInvoke(() =>
            {
                _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, message, "Database Restore",
                    MessageBoxChoiceSet.Ok);
                _databaseRestore.CleanUpTempFiles(_tempFilePath);
                RestoreFilePath = string.Empty;
            });
            if (ex == null)
                _logger.Log(message, Category.Info, Priority.Low);
            else
                _logger.Log(message + "\n" + ex.Message, Category.Exception, Priority.Low);
        }

        private void OnRestoreCompleted(object sender, ServerMessageEventArgs e)
        {
            _eventAggregator.GetEvent<ManualRestoreCompleted>().Publish(null);
            MaskVisibility = Visibility.Collapsed;
            _databaseRestore.CleanUpTempFiles(_tempFilePath);
            IdealDispatcher.BeginInvoke(() =>
            {
                _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information, "Restore completed.  The system will now reboot.",
                    "Database Restore", new[] { "Reboot" });
                RebootWindows();
            });
        }

        private static void RebootWindows()
        {
            TokPriv1Luid tp;
            var hproc = GetCurrentProcess();
            var htok = IntPtr.Zero;

            OpenProcessToken(hproc, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, ref htok);
            tp.Count = 1;
            tp.Luid = 0;
            tp.Attr = SE_PRIVILEGE_ENABLED;
            LookupPrivilegeValue(null, SE_SHUTDOWN_NAME, ref tp.Luid);
            AdjustTokenPrivileges(htok, false, ref tp, 0, IntPtr.Zero, IntPtr.Zero);
            ExitWindowsEx(EWX_REBOOT, 0);
        }

        #endregion
    }
}
