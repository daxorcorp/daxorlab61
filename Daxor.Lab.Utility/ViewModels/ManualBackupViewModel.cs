﻿using Daxor.Lab.DatabaseServices.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Ionic.Zip;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using RocketDivision.StarBurnX;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Daxor.Lab.Utility.ViewModels
{
    public class ManualBackupViewModel : ViewModelBase
    {
        #region Constants

        private const string DiscVolumeName = "DATA_DISC";
        private const string BackingUpQcArchiveMessage = "Backing up QC Archive";
        private const string BackingUpDaxorLabMessage = "Backing Up DaxorLab Databases";
        private const string QcArchiveDatabaseFilename = "QCDatabaseArchive.bak";
        private const string DaxorLabDatabaseFilename = "DaxorSuite.bak";
        private const string DatabaseBackupFileExtension = ".bak";
        private const string CompressingDatabaseMessage = "Compressing Database";
        private const string LogPath = @"C:\ProgramData\Daxor\Lab\Log";
        private const string LogPathForZipFile = @"Logs\Daxor Logs";
        private const string WindowsEventLogPath = @"C:\Windows\System32\winevt\Logs";
        private const string WindowsEventLogPathForZipFile = @"Logs\OS Logs";
        private const string BackupPathForZipFile = "Backups";
        private const string StarBurnPublisherName = "StarBurnX";
        private const string StarBurnApplicationName = "StarBurnX Data Burner";
        private const string BurningCdMessage = "Burning CD";
        private const string DeviceNotReadyMessage = "Device is not ready:Device is not ready";
        private const string DiscReadonlyMessage = "Disk readonly: The media disk is not recordable";
        private const string DiscFinalizedMessage = "Disc is finalized:The disc is finalized! Note that is not possible to write data on the finalized disc!";
        private const string BackupCompletedMessage = "Backup Completed";
        private const string DatabaseBackupCaption = "Database Backup";

        #endregion 

        #region Fields

        private bool _useIdealDispatcher = true;
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private Visibility _mask;
        private BusyPayload _bContent;
        private Visibility _showStepOne;
        private Visibility _showStepTwo;
        private string _zipFilePath;
        private readonly ILoggerFacade _logger;
        private readonly string _tempDirectory;
        private readonly ISettingsManager _settingsManager;
        private readonly IStarBurnWrapper _opticalDriveWrapper;
        private TaskScheduler _taskScheduler;
        private readonly IMessageManager _messageManager;
        private readonly IBackupServiceFactory _serviceFactory;
        private readonly IZipFileFactory _zipFileFactory;
        #endregion

        #region Ctor

        public ManualBackupViewModel(IMessageBoxDispatcher messageBoxDispatcher, 
                                     ILoggerFacade logger,
                                     ISettingsManager settingsManager,
                                     IStarBurnWrapper driveWrapper, 
                                     IMessageManager messageManager, 
                                     IBackupServiceFactory serviceFactory,
                                     IZipFileFactory zipFileFactory)
        {
            BackupDatabaseCommand = new DelegateCommand<object>(BackupDatabaseCommandExecute, BackupDatabaseCommandCanExecute);
            _messageBoxDispatcher = messageBoxDispatcher;
            _logger = logger;
            _settingsManager = settingsManager;
            _tempDirectory = _settingsManager.GetSetting<String>(SettingKeys.SystemTempFilePath);
            _opticalDriveWrapper = driveWrapper;
            _messageManager = messageManager;
            _serviceFactory = serviceFactory;
            ShowStepOne = Visibility.Visible;
            ShowStepTwo = Visibility.Hidden;
            MaskVisibility = Visibility.Collapsed;
            _zipFileFactory = zipFileFactory;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the scheduler for running asynchronous tasks. By default, this is the regular thread pool,
        /// but is overridable such that other schedulers can be used (i.e., for unit testing).
        /// </summary>
        /// <remarks>
        /// When using a custom scheduler, the IdealDispatcher (for running code on the UI thread) will not be
        /// used, so that the executing code will run under the custom scheduler.
        /// </remarks>
        public TaskScheduler TaskScheduler
        {
            get
            {
                return _taskScheduler ?? (_taskScheduler = TaskScheduler.Default);
            }
            set
            {
                _taskScheduler = value;
                _useIdealDispatcher = false;
            }
        }

        public IStarBurnWrapper OpticalDriveWrapper
        {
            get { return _opticalDriveWrapper; }
        }

        public string ZipFilePath
        {
            get { return _zipFilePath; }
            set
            {
                _zipFilePath = value;
                BackupDatabaseCommand.RaiseCanExecuteChanged();
            }
        }

        public Visibility ShowStepOne
        {
            get { return _showStepOne; }
            set { SetValue(ref _showStepOne, "ShowStepOne", value); }
        }

        public Visibility ShowStepTwo
        {
            get { return _showStepTwo; }
            set { SetValue(ref _showStepTwo, "ShowStepTwo", value); }
        }

        public Visibility MaskVisibility
        {
            get { return _mask; }
            set { SetValue(ref _mask, "MaskVisibility", value); }
        }

        public bool IsOpticalDrive { get; set; }

        public BusyPayload BusyContent
        {
            get { return _bContent; }
            set { SetValue(ref _bContent, "BusyContent", value); }
        }

        public DelegateCommand<object> BackupDatabaseCommand
        {
            private set;
            get;
        }

        #endregion

        #region Private helpers

        private void BackupDatabaseCommandExecute(object arg)
        {
            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;

            var backupTask = new Task(BackupDatabasesTask, token);
            backupTask.ContinueWith(result => OnBackupDatabaseCompleted(), token, TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler);
            // ReSharper disable once PossibleNullReferenceException
            backupTask.ContinueWith(result => OnBackupDatabaseTaskFaulted(result.Exception.InnerException), token, TaskContinuationOptions.OnlyOnFaulted, TaskScheduler);
            backupTask.Start(TaskScheduler);
        }
        
        private bool BackupDatabaseCommandCanExecute(object unused)
        {
            return IsOpticalDrive || Directory.Exists(ZipFilePath);
        }

        protected virtual void BackupDatabasesTask()
        {
            MaskVisibility = Visibility.Visible;
            BusyContent = new BusyPayload(true) { PercentageCompleted = 0, Message = BackingUpQcArchiveMessage };
    
            var qcBackup = _serviceFactory.CreateQcArchiveBackupService();
            qcBackup.PercentCompleteHandler = OnQcArchiveDatabaseBackupPercentComplete;
            qcBackup.Backup(_tempDirectory + QcArchiveDatabaseFilename);
            
            BusyContent = new BusyPayload(true) { PercentageCompleted = 0, Message = BackingUpDaxorLabMessage };
            DeleteTempFiles();

            var daxorBackup = _serviceFactory.CreateDaxorLabBackupService();
            daxorBackup.PercentCompleteHandler = OnDaxorLabDatabaseBackupPercentComplete;
            daxorBackup.Backup(_tempDirectory + DaxorLabDatabaseFilename);
        }

        protected virtual void OnBackupDatabaseTaskFaulted(Exception faultingException)
        {
            _logger.Log(faultingException.Message, Category.Exception, Priority.High);
            var userMessage = _messageManager.GetMessage(MessageKeys.UtilityBackupFailed).FormattedMessage;
            AlertUserAndDisableBusyIndicator(userMessage);
            DeleteTempFiles();
        }

        private void AlertUserAndDisableBusyIndicator(string userMessage)
        {
            if (_useIdealDispatcher)
                IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error,
                                                                                   userMessage, DatabaseBackupCaption,
                                                                                   MessageBoxChoiceSet.Close));
            else
                _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, userMessage, DatabaseBackupCaption, MessageBoxChoiceSet.Close);

            MaskVisibility = Visibility.Collapsed;
        }

        void OnQcArchiveDatabaseBackupPercentComplete(object sender, Microsoft.SqlServer.Management.Smo.PercentCompleteEventArgs e)
        {
            BusyContent = new BusyPayload(true) { PercentageCompleted = e.Percent, Message = BackingUpQcArchiveMessage };
        }

        void OnDaxorLabDatabaseBackupPercentComplete(object sender, Microsoft.SqlServer.Management.Smo.PercentCompleteEventArgs e)
        {
            BusyContent = new BusyPayload(true) { PercentageCompleted = e.Percent, Message = BackingUpDaxorLabMessage };
        }

        protected virtual void OnBackupDatabaseCompleted()
        {
            var tempQcArchivePath = _settingsManager.GetSetting<string>(SettingKeys.SystemTempFilePath) +
                _settingsManager.GetSetting<string>(RuntimeSettingKeys.SystemQcArchiveDatabaseBackupFilename) + DatabaseBackupFileExtension;

            var tempDaxorPath = _settingsManager.GetSetting<string>(SettingKeys.SystemTempFilePath) +
                _settingsManager.GetSetting<string>(RuntimeSettingKeys.SystemDaxorLabDatabaseBackupFilename) + DatabaseBackupFileExtension;

            var backupFileName = CreateBackupFileName();

            var tempDaxorManualBackupFilePath = _settingsManager.GetSetting<string>(SettingKeys.SystemTempFilePath) + backupFileName;

            CreateZipFile(tempDaxorPath, tempQcArchivePath, tempDaxorManualBackupFilePath);

            if (!IsOpticalDrive)
            {
                SafeFileOperations.Copy(tempDaxorManualBackupFilePath, ZipFilePath + backupFileName);
            }
            else
            {
                var dataBurner = _opticalDriveWrapper.CreateDataBurner();
                dataBurner.OnProgress += OnDataBurnerOnProgress;
                var dataFolder = (IDataFolder) dataBurner;
                dataFolder.AddFile(tempDaxorManualBackupFilePath);

                try
                {
                    dataBurner.Burn(false, DiscVolumeName, StarBurnPublisherName, StarBurnApplicationName);
                }
                catch (Exception ex)
                {
                    string message;
                    Message errorAlert;
                    switch (ex.Message)
                    {
                        case DeviceNotReadyMessage:
                            errorAlert = _messageManager.GetMessage(MessageKeys.UtilityExportDeviceNotReady);
                            message = errorAlert.FormattedMessage;
                            break;
                        case DiscReadonlyMessage:
                            errorAlert = _messageManager.GetMessage(MessageKeys.UtilityExportDiscReadonly);
                            message = errorAlert.FormattedMessage;
                            break;
                        case DiscFinalizedMessage:
                            errorAlert = _messageManager.GetMessage(MessageKeys.UtilityExportDiscFinalized);
                            message = errorAlert.FormattedMessage;
                            break;
                        default:
                            message = ex.Message;
                            break;
                    }
                    AlertUserAndDisableBusyIndicator(message);
                    dataBurner.OnProgress -= OnDataBurnerOnProgress;
                    return;
                }

                dataBurner.OnProgress -= OnDataBurnerOnProgress;
                dataBurner.Drive.Eject();
                IsOpticalDrive = false;
                BackupDatabaseCommand.RaiseCanExecuteChanged();
            }
            DeleteTempFiles();
            ZipFilePath = string.Empty;

            MaskVisibility = Visibility.Collapsed;

            IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information,
                BackupCompletedMessage, DatabaseBackupCaption, MessageBoxChoiceSet.Close));
        }

        protected virtual void CreateZipFile(string tempDaxorPath, string tempQcArchivePath, string tempDaxorManualBackupFilePath)
        {
            var zipFile = _zipFileFactory.CreateZipFile();

            zipFile.AttachProgressHandlerToZip(OnZipFileSaveProgress);
            
            // Cameron: These don't need to be password-protected, so we add these before setting password/encryption
            zipFile.AddDirectory(LogPath, LogPathForZipFile);
            zipFile.AddDirectory(WindowsEventLogPath, WindowsEventLogPathForZipFile);

            zipFile.AddPasswordToZip(_settingsManager.GetSetting<string>(SettingKeys.SystemPasswordUnsecureExport));

            zipFile.AddFileToDirectory(tempDaxorPath, BackupPathForZipFile);

            if (File.Exists(tempQcArchivePath))
                zipFile.AddFileToDirectory(tempQcArchivePath, BackupPathForZipFile);

            zipFile.Save(tempDaxorManualBackupFilePath);
        }

        public string CreateBackupFileName()
        {
            return _settingsManager.GetSetting<string>(SettingKeys.SystemUniqueSystemId) + "-" + _settingsManager.GetSetting<string>(RuntimeSettingKeys.SystemManualBackupFileName);
        }

        protected virtual void DeleteTempFiles()
        {
            var tempFileDirectory = _settingsManager.GetSetting<String>(SettingKeys.SystemTempFilePath);
            if (File.Exists(tempFileDirectory + DaxorLabDatabaseFilename + DatabaseBackupFileExtension))
                SafeFileOperations.Delete(tempFileDirectory + DaxorLabDatabaseFilename + DatabaseBackupFileExtension);
            if (File.Exists(tempFileDirectory + QcArchiveDatabaseFilename + DatabaseBackupFileExtension))
                SafeFileOperations.Delete(tempFileDirectory + QcArchiveDatabaseFilename + DatabaseBackupFileExtension);
        }

        void OnDataBurnerOnProgress(int percent, int timeRemaining)
        {
            BusyContent = new BusyPayload(true) { PercentageCompleted = percent, Message = BurningCdMessage };
        }

        void OnZipFileSaveProgress(object sender, SaveProgressEventArgs e)
        {
            var x = Math.Round((e.BytesTransferred / (double)e.TotalBytesToTransfer) * 100, 0);
            BusyContent = new BusyPayload(true) { PercentageCompleted = x, Message = CompressingDatabaseMessage };
        }

        #endregion
    }
}
