using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.QC.Reports;
using System.Collections.ObjectModel;
using Microsoft.Practices.Composite.Presentation.Commands;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace Daxor.Lab.Utility.ViewModels
{
    public class QCSourcesSubViewModel : ViewModelBase, IActiveAware
    {
        #region Fields
        private DelegateCommand<Object> _printCommand;
        private readonly ControlSourceSetupReport _controlSourceSetupReport = new ControlSourceSetupReport();
        private string _systemId;
        private readonly ISettingsManager _settingsManager;

        #endregion

        #region Ctor
        public QCSourcesSubViewModel(ISettingsManager settingsManager)
        {
            _settingsManager = settingsManager;
        }

        #endregion

        #region IActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                bool prevIsActive = _isActive;
                if (_isActive == value)
                    return;

                _isActive = value;

                if (_isActive)
                {
                    _qcSources = _qcSources ?? new ObservableCollection<QCSourceRecord>(QCSourceDataAccess.SelectList());
                    base.FirePropertyChanged("QcSources");
                }
                else if (prevIsActive && !_isActive)
                {
                    foreach (var x in QcSources)
                    {
                        QCSourceDataAccess.Update(x);
                    }
                    
                    _qcSources.Clear();
                    _qcSources = null;
                }
                   
            }
        }
        public event EventHandler IsActiveChanged = delegate { };

        #endregion

        protected ObservableCollection<QCSourceRecord> _qcSources;
        public IEnumerable<QCSourceRecord> QcSources
        {
            get { return _qcSources; }
        }

        #region Commands
        
        public ICommand PrintCommand
        {
            get
            {
                return _printCommand = _printCommand ?? new DelegateCommand<object>(ExecutePrintCommand);
            }
        }

        private void ExecutePrintCommand(object noArgObj)
        {
            LoadReport();
            System.Drawing.Printing.PrintController standardPrintController =
                               new System.Drawing.Printing.StandardPrintController();
            var settings = new System.Drawing.Printing.PrinterSettings();
            
            var proc = new ReportProcessor {PrintController = standardPrintController};

            proc.PrintReport(new InstanceReportSource {ReportDocument = _controlSourceSetupReport}, settings);
        }
        
        private void LoadReport()
        {
             _systemId = _settingsManager.GetSetting<string>(SettingKeys.SystemUniqueSystemId);
             _controlSourceSetupReport.textBoxBVAVersion.Value = _settingsManager.GetSetting<string>(SettingKeys.BvaModuleVersion);
            _controlSourceSetupReport.textBoxPrintedOn.Value = @"Printed: " + DateTime.Now.ToString("g");
            _controlSourceSetupReport.textBoxUnitID.Value = _systemId ?? string.Empty;
            _controlSourceSetupReport.textBoxDepartment.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentName);
            _controlSourceSetupReport.textBoxDepartmentAddress.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentAddress);
            _controlSourceSetupReport.textBoxDepartmentDirector.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentDirector);
            _controlSourceSetupReport.textBoxDepartmentPhoneNumber.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentPhoneNumber);
            _controlSourceSetupReport.textBoxHospitalName.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemHospitalName);

            var standardsFields = from x in QcSources
                                  orderby x.Position
                                  select new QCsourceRecord
                                  {
                                      Name = x.Description,
                                      SourceToc = x.SourceTOC,
                                      Isotope = x.Isotope,
                                      Activity = x.Activity,
                                      SerialNo = x.SerialNumber,
                                      CountLimit = x.CountLimit
                                  };
            var standardsFieldList = standardsFields.ToList();
            _controlSourceSetupReport.tableQCSources.DataSource = standardsFieldList;
        }

        #endregion
    }

    class QCsourceRecord
    {
        public String Name { get; set; }
        public DateTime SourceToc { get; set; }
        public String Isotope { get; set; }
        public double Activity { get; set; }
        public String SerialNo { get; set; }
        public int CountLimit { get; set; }
    }
}
