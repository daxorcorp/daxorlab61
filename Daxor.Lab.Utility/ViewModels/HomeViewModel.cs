﻿using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Utility.Common;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;
using System;
using System.Windows.Input;


namespace Daxor.Lab.Utility.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {
        #region Fields

        private readonly IAuthenticationManager _authService;
        private readonly IAppModuleNavigator _navigator;
        private readonly DelegateCommand<Object> _loginCommand;
        private readonly ITestExecutionController _testExecutionService;
        private string _loginPassword;
        private AuthorizationLevel _loginLevel = AuthorizationLevel.Undefined;

        #endregion

        #region Ctor

        public HomeViewModel([Dependency(AppModuleIds.Utilities)] IAppModuleNavigator localNavigator, IAuthenticationManager authService,
            ITestExecutionController testExecutionService)
        {
            _navigator = localNavigator;
            _authService = authService;
            _testExecutionService = testExecutionService;
            _loginCommand = new DelegateCommand<object>(ExecuteLoginCommand, CanExecuteLoginCommand);

            WireUpAuthenticationService();
            WireUpTestExecutionService();
        }

        #endregion

        #region Properties

        public string LoginPassword
        {
            get { return _loginPassword; }
            set { base.SetValue(ref _loginPassword, "LoginPassword", value); }
        }
      
        public AuthorizationLevel LoginLevel
        {
            get { return _loginLevel; }
            set { base.SetValue(ref _loginLevel, "LoginLevel", value); }
        }

        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            get { return _loginCommand; }
        }
        private void ExecuteLoginCommand(object nullObject)
        {
            if (DidUserSubmitCorrectPasswordForAuthorizationLevel())
            {
                VirtualKeyboardManager.CloseActiveKeyboard();
                _navigator.ActivateView(UtilityModuleViewKeys.GreetingsView);
            }
            ClearPasswordAfterAttempt();
        }
        #endregion

        #region Helper Methods

        private bool DidUserSubmitCorrectPasswordForAuthorizationLevel()
        {
            if (LoginLevel == AuthorizationLevel.Service && DidUserSubmitCorrectPasswordForServiceLevel())
                return true;

            if (_authService.Login(LoginLevel, LoginPassword))
                return true;

            return false;
        }

        private bool DidUserSubmitCorrectPasswordForServiceLevel()
        {
            // Cameron: For the Service Level, we accept either Service or Internal password. 
            return _authService.Login(AuthorizationLevel.Service, LoginPassword) 
                || _authService.Login(AuthorizationLevel.Internal, LoginPassword);
        }

        private void ClearPasswordAfterAttempt()
        {
            LoginPassword = String.Empty;
        }

        private bool CanExecuteLoginCommand(object o)
        {
            return !_testExecutionService.IsExecuting;
        }
        #endregion

        #region IDisposable Implementation
        protected override void OnDispose()
        {
            _authService.LogoutCompleted -= _authService_LogoutCompleted;
            base.OnDispose();
        }
        #endregion

        #region Event Handlers

        void _authService_LogoutCompleted(object sender, EventArgs e)
        {
            LoginLevel = AuthorizationLevel.Undefined;
        }

        private void WireUpTestExecutionService()
        {
            _testExecutionService.TestCompleted += new EventHandler<TestExecutionEventArgs>(_testExecutionService_TestStatusChanged);
            _testExecutionService.TestAborted += new EventHandler<TestExecutionEventArgs>(_testExecutionService_TestStatusChanged);
            _testExecutionService.TestStarted += new EventHandler<TestExecutionEventArgs>(_testExecutionService_TestStatusChanged);
        }

        private void WireUpAuthenticationService()
        {
            _authService.LogoutCompleted += _authService_LogoutCompleted;
        }

        private void _testExecutionService_TestStatusChanged(object sender, TestExecutionEventArgs e)
        {
            _loginCommand.RaiseCanExecuteChanged();
        }
        #endregion
    }
}
