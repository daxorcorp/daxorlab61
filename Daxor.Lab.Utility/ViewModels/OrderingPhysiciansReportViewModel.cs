﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.Common;
using Daxor.Lab.Utility.Services;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Regions;

namespace Daxor.Lab.Utility.ViewModels
{
    public class OrderingPhysiciansReportViewModel : ViewModelBase, IActiveAware
    {
        public enum ReportMode
        {
            Show,
            Print,
            Export
        };

        #region Fields

        private const int OneDay = 1;
        private const int DefaultDateRange = 30;
        public const string PhysiciansListNullErrorCaption = "Ordering Physicians Report";

        private DateTime _fromDateTime;
        private DateTime _toDateTime;
        private TaskScheduler _taskScheduler;
        private bool _useIdealDispatcher = true;
        private readonly IRegionManager _regionManager;
        private readonly IEventAggregator _eventAggregator;
        private readonly ILoggerFacade _logger;
        private readonly ISettingsManager _settingsManager;
        private readonly IReportProcessor _reportProcessor;
        private readonly IReportControllerFactory _reportControllerFactory;
        private readonly IUtilityDataService _utilityDataService;
        private readonly IMessageManager _messageManager;
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private List<OrderingPhysicianDataRecord> _orderingPhysicianDataRecords;
        private readonly IFolderBrowser _folderBrowser;
        private readonly IStarBurnWrapper _starBurnWrapper;
        private readonly IExcelFileBuilder _excelFileBuilder;

        #endregion

        #region Public API

        #region Ctor

        public OrderingPhysiciansReportViewModel(IRegionManager regionManager,
                                                 IEventAggregator eventAggregator,
                                                 ILoggerFacade logger,
                                                 ISettingsManager settingsManager,
                                                 IReportControllerFactory reportControllerFactory,
                                                 IReportProcessor reportProcessor,
                                                 IUtilityDataService utilityDataService,
                                                 IMessageManager messageManager,
                                                 IMessageBoxDispatcher messageBoxDispatcher,
                                                 IFolderBrowser folderBrowser,
                                                 IStarBurnWrapper starBurnWrapper,
                                                 IExcelFileBuilder excelFileBuilder)
        {
            
            if (regionManager == null) throw new ArgumentNullException("regionManager");
            if (eventAggregator == null) throw new ArgumentNullException("eventAggregator");
            if (logger == null) throw new ArgumentNullException("logger");
            if (settingsManager == null) throw new ArgumentNullException("settingsManager");
            if (reportControllerFactory == null) throw new ArgumentNullException("reportControllerFactory");
            if (reportProcessor == null) throw new ArgumentNullException("reportProcessor");
            if (utilityDataService == null) throw new ArgumentNullException("utilityDataService");
            if (messageManager == null) throw new ArgumentNullException("messageManager");
            if (messageBoxDispatcher == null) throw new ArgumentNullException("messageBoxDispatcher");
            if (folderBrowser == null) throw new ArgumentNullException("folderBrowser");
            if (starBurnWrapper == null) throw new ArgumentNullException("starBurnWrapper");
            if (excelFileBuilder == null) throw new ArgumentNullException("excelFileBuilder");

            _regionManager = regionManager;
            _eventAggregator = eventAggregator;
            _logger = logger;
            _settingsManager = settingsManager;
            _reportControllerFactory = reportControllerFactory;
            _utilityDataService = utilityDataService;
            _reportProcessor = reportProcessor;
            _messageManager = messageManager;
            _messageBoxDispatcher = messageBoxDispatcher;
            _folderBrowser = folderBrowser;
            _starBurnWrapper = starBurnWrapper;
            _excelFileBuilder = excelFileBuilder;

            _toDateTime = DateTime.Now;
            _fromDateTime = _toDateTime.AddDays(-DefaultDateRange);
            _orderingPhysicianDataRecords = new List<OrderingPhysicianDataRecord>();

            PropertyChanged += OnPropertyChanged;

            ViewReportCommand = new DelegateCommand<object>(ExecuteViewReport, CanExecuteViewReport);
            PrintReportCommand = new DelegateCommand<object>(ExecutePrintReport, CanExecutePrintReport);
            ExportReportCommand = new DelegateCommand<object>(ExecuteExportReport, CanExecuteExportReport);
        }

        #endregion

        #region Properties

        public DateTime FromDateTime
        {
            get { return _fromDateTime; }
            set
            {
                if (value.Date >= _toDateTime.Date)
                {
                    SetValue(ref _fromDateTime, "FromDateTime", value.Date);
                    ToDateTime = value.AddDays(OneDay);
                }
                else
                    SetValue(ref _fromDateTime, "FromDateTime", value.Date);
            }
        }

        public DateTime ToDateTime
        {
            get { return _toDateTime; }
            set
            {
                var valueToSet = value;
                
                if (value.Date > DateTime.Today)
                {
                    valueToSet = DateTime.Today;
                    SetValue(ref _toDateTime, "ToDateTime", valueToSet.Date);
                    FromDateTime = valueToSet.AddDays(-OneDay);
                }
                else if (value.Date <= _fromDateTime.Date)
                {
                    SetValue(ref _toDateTime, "ToDateTime", valueToSet.Date);
                    FromDateTime = valueToSet.AddDays(-OneDay);
                }
                else
                    SetValue(ref _toDateTime, "ToDateTime", valueToSet.Date);
            }
        }

        public List<OrderingPhysicianDataRecord> OrderingPhysicianDataRecords
        {
            get { return _orderingPhysicianDataRecords; }
            set
            {
                if (value == null)
                    HandleNullOrderingPhysicianDataRecords();
                else
                    _orderingPhysicianDataRecords = value;

                FirePropertyChanged("AreThereTestsInTheRange");
                FirePropertyChanged("NumberOfTestsInRangeText");
                ViewReportCommand.RaiseCanExecuteChanged();
                PrintReportCommand.RaiseCanExecuteChanged();
                ExportReportCommand.RaiseCanExecuteChanged();
            }
        }

        public DelegateCommand<object> ViewReportCommand
        {
            get;
            private set;
        }

        public DelegateCommand<object> PrintReportCommand
        {
            get;
            private set;
        }

        public DelegateCommand<object> ExportReportCommand
        {
            get;
            private set;
        }

        public bool AreThereTestsInTheRange
        {
            get { return DetermineTotalTestCount() > 0; }
        }

        public string NumberOfTestsInRangeText
        {
            get
            {
                var totalTestCount = DetermineTotalTestCount();
                switch (totalTestCount)
                {
                    case 0:
                        return "There were no tests ordered";
                    case 1:
                        return "There was 1 test ordered";
                    default:
                        return "There were " + totalTestCount + " tests ordered";
                }
            }
        }

        /// <summary>
        /// Gets or sets the scheduler for running asynchronous tasks. By default, this is the regular thread pool,
        /// but is overridable such that other schedulers can be used (i.e., for unit testing).
        /// </summary>
        /// <remarks>
        /// When using a custom scheduler, the IdealDispatcher (for running code on the UI thread) will not be
        /// used, so that the executing code will run under the custom scheduler.
        /// </remarks>
        public TaskScheduler TaskScheduler
        {
            get
            {
                return _taskScheduler ?? (_taskScheduler = TaskScheduler.Default);
            }
            set
            {
                _taskScheduler = value;
                _useIdealDispatcher = false;
            }
        }

        #endregion

        #endregion

        #region Private methods

        #region Command methods

        private bool CanExecuteViewReport(object arg)
        {
            return !_regionManager.Regions[RegionNames.ReportRegion].ActiveViews.Any() && AreThereTestsInTheRange;
        }

        private bool CanExecutePrintReport(object arg)
        {
            return AreThereTestsInTheRange;
        }

        private bool CanExecuteExportReport(object arg)
        {
            return AreThereTestsInTheRange;
        }

        private void ExecuteViewReport(object arg)
        {
            ShowPrintOrExportReport(ReportMode.Show);
        }

        private void ExecutePrintReport(object arg)
        {
            ShowPrintOrExportReport(ReportMode.Print);
        }

        private void ExecuteExportReport(object arg)
        {
            ShowPrintOrExportReport(ReportMode.Export);
        }

        #endregion

        #region Helper methods

        private int DetermineTotalTestCount()
        {
            var recordCount = OrderingPhysicianDataRecords.Count;
            var countsForRecordsWithSubRecords = (from r in OrderingPhysicianDataRecords
                where r.TestRecordsList != null
                select r.TestRecordsList.Count).ToArray();

            return recordCount - countsForRecordsWithSubRecords.Count() + countsForRecordsWithSubRecords.Sum();
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (propertyChangedEventArgs.PropertyName == "ToDateTime" || propertyChangedEventArgs.PropertyName == "FromDateTime")
                OrderingPhysicianDataRecords = _utilityDataService.GetOrderingPhysiciansBetween(FromDateTime, ToDateTimeIncludingTheEntireDay());
        }

        private DateTime ToDateTimeIncludingTheEntireDay()
        {
            return ToDateTime.AddDays(1);
        }

        private void ShowPrintOrExportReport(ReportMode mode)
        {
            var busyMessage = string.Empty;
            var logMessage = string.Empty;
            
            switch (mode)
            {
                case ReportMode.Show:
                    busyMessage = "Loading";
                    logMessage = "VIEW";
                    break;
                case ReportMode.Print:
                    busyMessage = "Printing";
                    logMessage = "PRINT";
                    break;
                case ReportMode.Export:
                    busyMessage = "Exporting";
                    logMessage = "EXPORT";
                    break;
            }

            busyMessage += " Report...";
            logMessage += " ORDERING PHYSICIANS REPORT BUTTON CLICKED";

            var reportTask = new Task(() => ShowPrintOrExportReportTask(busyMessage, logMessage, mode));
            reportTask.ContinueWith(result => DisableBusyIndicator(), TaskScheduler);
            reportTask.Start(TaskScheduler);
        }

        private void ShowPrintOrExportReportTask(string busyMessage, string logMessage, ReportMode mode)
        {
            EnableBusyIndicator(busyMessage);
            var reportController = _reportControllerFactory.CreateOrderingPhysiciansReportController(_logger, 
                _regionManager, _eventAggregator, _settingsManager, FromDateTime, ToDateTime, _orderingPhysicianDataRecords, _reportProcessor,
                _messageBoxDispatcher, _folderBrowser, _messageManager, _starBurnWrapper, _excelFileBuilder);
            _logger.Log(logMessage, Category.Info, Priority.None);
            switch (mode)
            {
                case ReportMode.Print: InvokeActionOnDesiredDispatcher(reportController.PrintReport);
                    break;
                case ReportMode.Show: InvokeActionOnDesiredDispatcher(reportController.ShowReport);
                    break;
                case ReportMode.Export: InvokeActionOnDesiredDispatcher(reportController.ExportReport);
                    break;
            }
        }

        private void EnableBusyIndicator(string message)
        {
            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, message));
        }

        private void DisableBusyIndicator()
        {
            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
        }

        private void InvokeActionOnDesiredDispatcher(Action action)
        {
            if (_useIdealDispatcher)
                IdealDispatcher.BeginInvoke(action);
            else
                action();
        }

        private void HandleNullOrderingPhysicianDataRecords()
        {
            var message = _messageManager.GetMessage(MessageKeys.UtilityOrderingPhysiciansLookupFailed).FormattedMessage;
            _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, message, PhysiciansListNullErrorCaption,
                MessageBoxChoiceSet.Close);
            _orderingPhysicianDataRecords = new List<OrderingPhysicianDataRecord>();
        }

        #endregion

        #endregion

        #region IsActiveAware

        private bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }

            set
            {
                _isActive = value;

                if (value)
                {
                    ToDateTime = DateTime.Now;
                    FromDateTime = ToDateTime.AddDays(-DefaultDateRange);
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
