﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Services;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Research.DynamicDataDisplay.DataSources;

namespace Daxor.Lab.Utility.ViewModels
{
    /// <summary>
    /// The Gamma counter subViewModel
    /// </summary>
    public class GammaCounterSubViewModel : ViewModelBase, IActiveAware
    {
        #region Fields
      
        private readonly IDetector _detector;
        private readonly ISpectroscopyService _spectroscopyService;
        private readonly IBackgroundAcquisitionService _backgroundAcquisitionService;
        private readonly ILoggerFacade _logger;
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly IEventAggregator _eventAggregator;
        private readonly ISettingsManager _settingsManager;
        private readonly IMessageManager _messageManager;

        private IPointDataSource _graphDataSource;
        private EnumerableDataSource<uint> _yDataSource;
        private UInt32[] _yArray;

        private bool _isCurrentlyAcquiring;

        readonly List<String> _acquisitionTypes = new List<String>(new[] { "Live Time", "Real Time" });
        string _selectedAcqType = "Live Time";
        
        BackgroundWorker _acquisitionWorker = new BackgroundWorker();

        Int32 _roiLowBound = 1;
        Int32 _roiHighBound = 1;
        Int32 _maximumRoiBound = 1024;
        Int32 _highVoltage;
        Int32 _lowLevelDiscriminator;
        Int32 _noise;
        double _fineGain;
        string _desiredAcquisitionPeriod = String.Empty;
        Int32 _acquisitionTimeInSeconds;

        double _adcCurrent;
        ulong _integral;
        double _centroid, _fullWidthHalfMax, _fullWidthTenthMax, _area;
        Spectrum _currentSpectrum;
        Boolean _shouldAcquireForever;

        DelegateCommand<Object> _startAcquireCommand;
        DelegateCommand<Object> _stopAcquireCommand;
        DelegateCommand<Object> _flushSpectrumCommand;
        DelegateCommand<Object> _resetHardwareSettingsCommand;
        DelegateCommand<Object> _updateHardwareSettingsCommand;

        #endregion

        #region Properties
        
        public Spectrum CurrentSpectrum
        {
            get { return _currentSpectrum; }
            set { base.SetValue(ref _currentSpectrum, "CurrentSpectrum", value); }
        }

        public IPointDataSource GraphDataSource
        {
            get { return _graphDataSource; }
            set { base.SetValue(ref _graphDataSource, "GraphDataSource", value); }
        }

        public List<String> AcquisitionTypes
        {
            get { return _acquisitionTypes; }
        }

        public String SelectedAcquisitionType
        {
            get { return _selectedAcqType; }
            set { base.SetValue(ref _selectedAcqType, "SelectedAcquisitionType", value); }
        }

        public bool IsCurrentlyAcquiring
        {
            get { return _isCurrentlyAcquiring; }
            private set { base.SetValue(ref _isCurrentlyAcquiring, "IsCurrentlyAcquiring", value); }
        }

        public int RoiLowBound
        {
            get { return _roiLowBound; }
            set
            {
                if (base.SetValue(ref _roiLowBound, "RoiLowBound", value))
                {
                    try
                    {
                        RunSpectroscopyAnalysis();
                    }
                    catch (Exception ex)
                    {
                        _logger.Log("GammaCounterSubViewModel: Exception in RoiLowBound: " + ex.Message, Category.Debug, Priority.High);
                    }
                }
            }
        }

        public int RoiHighBound
        {
            get { return _roiHighBound; }
            set
            {
                if (base.SetValue(ref _roiHighBound, "RoiHighBound", value))
                {
                    try
                    {
                        RunSpectroscopyAnalysis();
                    }
                    catch (Exception ex)
                    {
                        _logger.Log("GammaCounterSubViewModel: Exception in RoiHighBound: " + ex.Message, Category.Debug, Priority.High);
                    }
                }
            }
        }

        public int MaximumRoiBound
        {
            get { return _maximumRoiBound; }
            set { base.SetValue(ref _maximumRoiBound, "MaximumRoiBound", value); }
        }

        public string DesiredAcquisitionPeriod
        {
            get { return _desiredAcquisitionPeriod; }
            set
            {
                if (!base.SetValue(ref _desiredAcquisitionPeriod, "DesiredAcquisitionPeriod", value)) return;
                
                if (String.IsNullOrWhiteSpace(value))
                    _acquisitionTimeInSeconds = 0;
                else if (!Int32.TryParse(_desiredAcquisitionPeriod, out _acquisitionTimeInSeconds))
                    _acquisitionTimeInSeconds = 0;
            }
        }

        public bool ShouldAcquireForever
        {
            get { return _shouldAcquireForever; }
            set
            {
                if (!base.SetValue(ref _shouldAcquireForever, "ShouldAcquireForever", value)) return;
                
                if (_shouldAcquireForever)
                    DesiredAcquisitionPeriod = String.Empty;
            }
        }

        public int HighVoltage
        {
            get { return _highVoltage; }
            set 
            { 
                base.SetValue(ref _highVoltage, "HighVoltage", value);
                SettingsHaveChanged = true;
            }
        }

        public int LowerLevelDescriminator
        {
            get { return _lowLevelDiscriminator; }
            set 
            { 
                base.SetValue(ref _lowLevelDiscriminator, "LowerLevelDescriminator", value);
                SettingsHaveChanged = true;
            }
        }

        public int Noise
        {
            get { return _noise; }
            set 
            { 
                base.SetValue(ref _noise, "Noise", value);
                SettingsHaveChanged = true;
            }
        }

        public double FineGain
        {
            get { return _fineGain; }
            set 
            { 
                base.SetValue(ref _fineGain, "FineGain", value);
                SettingsHaveChanged = true;
            }
        }

        public double AdcCurrent
        {
            get { return _adcCurrent; }
            set { base.SetValue(ref _adcCurrent, "AdcCurrent", value); }
        }

        public ulong Integral
        {
            get { return _integral; }
            set { base.SetValue(ref _integral, "Integral", value); }
        }

        public double Centroid
        {
            get { return _centroid; }
            set { base.SetValue(ref _centroid, "Centroid", value); }
        }

        public double FullWidthHalfMax
        {
            get { return _fullWidthHalfMax; }
            set
            {
                value = value < 0 ? 0 : value;
                base.SetValue(ref _fullWidthHalfMax, "FullWidthHalfMax", value);
            }
        }

        public double FullWidthTenthMax
        {
            get { return _fullWidthTenthMax; }
            set
            {
                value = value < 0 ? 0 : value;
                base.SetValue(ref _fullWidthTenthMax, "FullWidthTenthMax", value);
            }
        }

        public double Area
        {
            get { return _area; }
            set { base.SetValue(ref _area, "Area", value); }
        }
       
        #endregion

        #region Ctor

        public GammaCounterSubViewModel(IDetector detector, ISpectroscopyService spectroscopy, IEventAggregator eventAggregator,
                                        ILoggerFacade logger, IBackgroundAcquisitionService bkgService, 
                                        IMessageBoxDispatcher messageBoxDispatcher, ISettingsManager settingsManager,
                                        IMessageManager messageManager)
        {
            _detector = detector;
            _spectroscopyService = spectroscopy;
            _logger = logger;
            _messageBoxDispatcher = messageBoxDispatcher;
            _backgroundAcquisitionService = bkgService;
            _eventAggregator = eventAggregator;
            _settingsManager = settingsManager;
            _messageManager = messageManager;

            WireUpCommands();
            RegisterDetectorEventHandlers();
            SetInitialValues();
            SetupSpectrumAcquisitionWorker();

            GraphDataSource = CreateSpectrumCountsProvider();
        }

        private void WireUpCommands()
        {
            _updateHardwareSettingsCommand = new DelegateCommand<object>(UpdateHardwareSettingsCommandExecute, UpdateHardwareSettingsCommandCanExecute);
            _flushSpectrumCommand = new DelegateCommand<object>(FlushSpectrumCommandExecute);
            _resetHardwareSettingsCommand = new DelegateCommand<object>(ResetHardwareSettingsCommandExecute);
            _startAcquireCommand = new DelegateCommand<object>(StartAcquireCommandExecute, StartAcquireCommandCanExecute);
            _stopAcquireCommand = new DelegateCommand<object>(StopAcquireCommandExecute, StopAcquireCommandCanExecute);
        }

        #endregion

        #region Commands

        public ICommand ResetHardwareSettingsCommand
        {
            get { return _resetHardwareSettingsCommand; }
        }
        void ResetHardwareSettingsCommandExecute(object arg)
        {
            if (!StopBackgroundAcquisitionIfUserAgrees())
                return;

            string message = "To reset the detector's settings, the system must be rebooted. Do you wish to continue?";
            var choices = new[] { "Reboot", "Cancel" };
            int result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Question, message, "Reset Hardware Settings", choices);

            if (choices[result] == "Reboot")
            {
                IConfigurable confDetector = _detector as IConfigurable;
                if (confDetector != null)
                    confDetector.ResetConfiguration();
                ShutdownAndReboot.RebootWindows();
            }
        }

        private bool _enableStartButton;
        public bool EnableStartButton
        {
            get
            {
                return _enableStartButton;
            }

            set
            {
                base.SetValue(ref _enableStartButton, "EnableStartButton", value);
                _startAcquireCommand.RaiseCanExecuteChanged();
                _stopAcquireCommand.RaiseCanExecuteChanged();
            }
        }
        public ICommand StartAcquireCommand
        {
            get { return _startAcquireCommand; }
        }
        bool StartAcquireCommandCanExecute(object arg)
        {
            return EnableStartButton;
        }
        void StartAcquireCommandExecute(object arg)
        {
            if (_detector != null)
            {
                if (!StopBackgroundAcquisitionIfUserAgrees())
                    return;

                try
                {
                    _detector.ResetSpectrum();
                }
                catch (Exception ex)
                {
                    _logger.Log(String.Format("Couldn't reset the spectrum: {0}\n\nStack Trace:\n", ex.Message, ex.StackTrace), Category.Exception, Priority.High);
                }
                try
                {
                    _detector.StartAcquiring(_acquisitionTimeInSeconds, SelectedAcquisitionType.Contains("Live"));
                }
                catch (Exception ex)
                {
                    _logger.Log(String.Format("Couldn't start the acquisition: {0}\n\nStack Trace:\n", ex.Message, ex.StackTrace), Category.Exception, Priority.High);
                }
                EnableStartButton = false;
            }
        }

        // True if user wants to stop acq service
        bool StopBackgroundAcquisitionIfUserAgrees()
        {
            if (!_backgroundAcquisitionService.IsActive)
                return true;

            string content = "Using this feature will temporarily disable the continuous background acquisition.\n\n" +
                "After leaving this screen, background acquisition will resume as normal.\n\n" +
                "Are you sure you want to temporarily disable background acquisition?";

            var choices = new[] { "Stop Acquisition", "Cancel" };
            var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information, content, "Background Acquisition", choices);

            if (choices[result] == "Cancel")
                return false;

            _backgroundAcquisitionService.InvalidatePreviousBackground();
            _backgroundAcquisitionService.StopAsync();

            return true;
        }
        
        public ICommand StopAcquireCommand
        {
            get { return _stopAcquireCommand; }
        }
        bool StopAcquireCommandCanExecute(object arg)
        {
            return !EnableStartButton;
        }
        void StopAcquireCommandExecute(object arg)
        {
            if (_detector != null)
            {
                if (!StopBackgroundAcquisitionIfUserAgrees())
                    return;

                try
                {
                    _detector.StopAcquiring();
                }
                catch (Exception ex)
                {
                    _logger.Log(String.Format("Couldn't stop acquisition: {0}\n\nStack Trace:\n{1}", ex.Message, ex.StackTrace), Category.Exception, Priority.High);
                }
                EnableStartButton = true;
            }
        }
       
        public ICommand FlushSpectrumCommand
        {
            get { return _flushSpectrumCommand; }
        }
        void FlushSpectrumCommandExecute(object arg)
        {
            if (_detector != null)
            {
                if (!StopBackgroundAcquisitionIfUserAgrees())
                    return;
                try
                {
                    _detector.ResetSpectrum();
                }
                catch (Exception ex)
                {
                    _logger.Log(String.Format("Couldn't reset the spectrum: {0}\n\nStack Trace:\n{1}", ex.Message, ex.StackTrace), Category.Exception, Priority.High);
                }
            }
        }
      
        /// <summary>
        /// Updates preset hardware settings
        /// </summary>
        public ICommand UpdateHardwareSettingsCommand
        {
            get { return _updateHardwareSettingsCommand; }
        }

        private bool _settingsHaveChanged;
        public bool SettingsHaveChanged 
        {
            get
            {
                return _settingsHaveChanged;
            }

            set
            {
                base.SetValue(ref _settingsHaveChanged, "SettingsHaveChanged", value);
                _updateHardwareSettingsCommand.RaiseCanExecuteChanged();
            }
        }

        bool UpdateHardwareSettingsCommandCanExecute(object arg)
        {
            return SettingsHaveChanged;
        }
        void UpdateHardwareSettingsCommandExecute(object arg)
        {
            if (!StopBackgroundAcquisitionIfUserAgrees())
                return;

            if (_detector != null)
            {
                BusyPayload bPlayload = new BusyPayload(true, "Updating Detector Settings...");
                _eventAggregator.GetEvent<BusyStateChanged>().Publish(bPlayload);

                Task uTask = Task.Factory.StartNew(() =>
                {
                    //Smooth transition
                    Thread.Sleep(1500);

                    try
                    {
                        _detector.Voltage = HighVoltage;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = String.Format("Couldn't update the voltage: {0}\n\nStack Trace:\n{1}", ex.Message, ex.StackTrace);
                        _logger.Log(errorMessage, Category.Exception, Priority.High);
                    }

                    try
                    {
                        _detector.LowLevelDiscriminator = LowerLevelDescriminator;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = String.Format("Couldn't update the Lower Level Descriminator: {0}\n\nStack Trace:\n{1}", ex.Message, ex.StackTrace);
                        _logger.Log(errorMessage, Category.Exception, Priority.High);
                    }

                    try
                    {
                        _detector.Noise = Noise;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = String.Format("Couldn't update the Noise: {0}\n\nStack Trace:\n{1}", ex.Message, ex.StackTrace);
                        _logger.Log(errorMessage, Category.Exception, Priority.High);
                    }

                    try
                    {
                        _detector.FineGain = FineGain;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = String.Format("Couldn't update the Fine Gain: {0}\n\nStack Trace:\n{1}", ex.Message, ex.StackTrace);
                        _logger.Log(errorMessage, Category.Exception, Priority.High);
                    }
                });

                //and setup a continuation for it only on when faulted
                uTask.ContinueWith((ant) =>
                {
                    //Hide the busy state screen
                    _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
                    AggregateException aggEx = ant.Exception;
                    foreach (Exception ex in aggEx.InnerExceptions)
                    {
                        _logger.Log(uTask.Exception.Message, Category.Exception, Priority.High);
                    }
                    //let the user know
                    string message = _messageManager.GetMessage(MessageKeys.UtilityGammaCounterSettingsUpdateFailed).FormattedMessage;
                    _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning, message, "Hardware Issues", MessageBoxChoiceSet.Close);
                    _logger.Log(message, Category.Info, Priority.Low);

                }, CancellationToken.None, TaskContinuationOptions.OnlyOnFaulted, TaskScheduler.FromCurrentSynchronizationContext());

                //and setup a continuation for it only on ran to completion
                uTask.ContinueWith((ant)=>{
                    //Reload Hardware settings
                    try
                    {
                        HighVoltage = _detector.Voltage;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = String.Format("Couldn't update the Voltage: {0}\n\nStack Trace:\n{1}", ex.Message, ex.StackTrace);
                        _logger.Log(errorMessage, Category.Exception, Priority.High);
                    }

                    try
                    {
                        LowerLevelDescriminator = _detector.LowLevelDiscriminator;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = String.Format("Couldn't update the Lower Level Descriminator: {0}\n\nStack Trace:\n{1}", ex.Message, ex.StackTrace);
                        _logger.Log(errorMessage, Category.Exception, Priority.High);
                    }
                    try
                    {
                        Noise = _detector.Noise;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = string.Format("Couldn't update the Noise: {0}\n\nStack Trace:\n{1}", ex.Message, ex.StackTrace);
                        _logger.Log(errorMessage, Category.Exception, Priority.High);
                    }
                    try
                    {
                        FineGain = _detector.FineGain;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = String.Format("Couldn't update the Fine Gain: {0}\n\nStack Trace:\n{1}", ex.Message, ex.StackTrace);
                        _logger.Log(errorMessage, Category.Exception, Priority.High);
                    }
                    SettingsHaveChanged = false;
                    //Hide the busy state screen
                    _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
                }, CancellationToken.None, TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler.FromCurrentSynchronizationContext());
            }
        }

        #endregion

        #region Helpers

        void RunSpectroscopyAnalysis()
        {
            if (_spectroscopyService != null)
            {
                double computedSpectBackground;
                if (CurrentSpectrum != null)
                {
                    Integral = _spectroscopyService.ComputeIntegral(CurrentSpectrum.SpectrumArray, RoiLowBound, RoiHighBound);
                    Centroid = Math.Round(_spectroscopyService.ComputeCentroid(CurrentSpectrum.SpectrumArray, RoiLowBound, RoiHighBound), 0);
                    FullWidthHalfMax = Math.Round(_spectroscopyService.ComputeFullWidthHalfMax(CurrentSpectrum.SpectrumArray, RoiLowBound, RoiHighBound), 0);
                    FullWidthTenthMax = Math.Round(_spectroscopyService.ComputeFullWidthKMax(CurrentSpectrum.SpectrumArray, 0.1f, RoiLowBound, RoiHighBound, out computedSpectBackground), 0);
                    Area = Math.Round(Integral - computedSpectBackground, 0);
                }
                else
                {
                    Integral = 0;
                    Centroid = 0;
                    FullWidthHalfMax = 0;
                    FullWidthTenthMax = 0;
                    Area = 0;
                }
            }
        }
        IPointDataSource CreateSpectrumCountsProvider()
        {

            uint[] spectrumX = new uint[1024];

            for (int i = 0; i < 1024; i++)
            {
                spectrumX[i] = (uint)(i + 1);
            }
            _yArray = new UInt32[1024];
            EnumerableDataSource<uint> xDataSource = new EnumerableDataSource<uint>(spectrumX);
            xDataSource.SetXMapping(x => x);

            _yDataSource = new EnumerableDataSource<uint>(_yArray);
            _yDataSource.SetYMapping(y => y);

            return new CompositeDataSource(xDataSource, _yDataSource);
        }

        void RegisterDetectorEventHandlers()
        {
            if (_detector == null) return;

            _detector.AcquisitionCompleted += OnAcquisitionCompleted;
            _detector.AcquisitionStarted += OnAcquisitionStarted;
            _detector.AcquisitionStopped += OnAcquisitionStopped;
        }

        void SetInitialValues()
        {
            if (_detector != null)
            {
                try
                {
                    _maximumRoiBound = _detector.SpectrumLength;
                    _roiLowBound = 584;
                    _roiHighBound = 740;
                    _isCurrentlyAcquiring = _detector.IsAcquiring;
                    _highVoltage = _detector.Voltage;
                    _fineGain = _detector.FineGain;
                    _noise = _detector.Noise;
                    _lowLevelDiscriminator = _detector.LowLevelDiscriminator;

                }
                catch (Exception ex)
                {
                    _logger.Log("Couldn't set the initial values\nError Message: " + ex.Message + "\nStack Trace:\n" + ex.StackTrace, Category.Exception, Priority.High);
                }
                SettingsHaveChanged = false;
                EnableStartButton = false;
            }
        }
        
        void SetupSpectrumAcquisitionWorker()
        {
            _acquisitionWorker = new BackgroundWorker {WorkerSupportsCancellation = true, WorkerReportsProgress = true};
            _acquisitionWorker.DoWork += AcquireSpectrum_DoWork;
            _acquisitionWorker.ProgressChanged += AcquireSpectrum_ProgressChanged;
            _acquisitionWorker.RunWorkerCompleted += AcquireSpectrum_RunWorkerCompleted;
        }

        void AcquireSpectrum_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //Raises Data Changed event for D3(Dynamic Data Display) graph to update
            _yDataSource.RaiseDataChanged();
        }

        void AcquireSpectrum_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
                throw e.Error;
        }

        void AcquireSpectrum_DoWork(object sender, DoWorkEventArgs e)
        {
            Thread.Sleep(800);  //Initial sleep; allows Dispatcher to catch up while animating to this screen
            while (true)
            {
                //Pending cancellation; quit acquiring and move
                if (_acquisitionWorker.CancellationPending)
                    break;

                if (_detector == null) continue;
                try
                {
                    if (_detector.IsConnected)
                    {
                        //Update the source from the view model
                        CurrentSpectrum = _detector.Spectrum;
                            
                    }
                    else
                    {
                        CurrentSpectrum = Spectrum.Empty();
                        throw new GammaCounterNotConnectedException("The connection with the gamma counter was lost in "+
                                                                    "AcquireSpectrum_DoWork");
                    }

                    if (CurrentSpectrum != null && CurrentSpectrum.SpectrumArray != null)
                    {
                        try
                        {
                            IsCurrentlyAcquiring = _detector.IsAcquiring;

                            //Copy new spectrum into graph Datasource
                            Array.Copy(CurrentSpectrum.SpectrumArray, _yArray, CurrentSpectrum.SpectrumArray.Length);
                            AdcCurrent = _detector.AdcCurrent;

                            if (RoiHighBound == 0 && RoiLowBound == 0)
                            {
                                Integral = 0;
                                Centroid = 0;
                            }
                            else
                            {
                                RunSpectroscopyAnalysis();
                            }
                        }
                        catch (Exception ex)
                        {
                            _logger.Log("GammaCounterSubViewModel: Exception in AcquireSpectrum_DoWork: "+ ex.Message, 
                                        Microsoft.Practices.Composite.Logging.Category.Exception, Microsoft.Practices.Composite.Logging.Priority.None);
                        }
                        finally
                        {
                            //Calling report progress to marshal back to UI thread and raise event
                            //to update the graph
                            _acquisitionWorker.ReportProgress(0, IsCurrentlyAcquiring);
                        }
                    }
                    else
                    {
                        CurrentSpectrum = Spectrum.Empty();
                        AdcCurrent = 0;
                        Integral = 0;
                        Centroid = 0;
                    }

                }
                catch (GammaCounterNotConnectedException ex)
                {
                    _logger.Log(ex.Message, Category.Exception, Priority.None);
                    throw;
                }
                catch (Exception ex)
                {
                    _logger.Log(ex.Message, Category.Exception, Priority.None);
                }

                Thread.Sleep(300);
            }
        }

        #endregion

        #region EventHandlers

        void OnAcquisitionStopped(string mcaId, DateTime stopTime)
        {
            IsCurrentlyAcquiring = false;
        }

        void OnAcquisitionStarted(string mcaId, DateTime startTime)
        {
            IsCurrentlyAcquiring = true;
        }

        void OnAcquisitionCompleted(string mcaId, Spectrum spectrum)
        {
            IsCurrentlyAcquiring = false;
        }
        
        #endregion

        #region IActiveAware Members

        private bool _isActive;
        public bool IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
                if (value)
                {
                    if (_acquisitionWorker != null && !_acquisitionWorker.IsBusy)
                    {
                        _acquisitionWorker.RunWorkerAsync();
                        if (_detector != null)
                        {
                            HighVoltage = _detector.Voltage;
                            FineGain = _detector.FineGain;
                            LowerLevelDescriminator = _detector.LowLevelDiscriminator;
                            Noise = _detector.Noise;
                            SettingsHaveChanged = false;
                        }

                    }
                }
                else
                {
                    if (_detector != null)
                    {
                        IConfigurable confDetector = _detector as IConfigurable;
                        if (confDetector != null)
                        {
                            confDetector.SaveConfiguration();
                            _settingsManager.SetSetting<int>(SettingKeys.SystemDetectorHighVoltage, _detector.Voltage);
                            _settingsManager.SetSetting<double>(SettingKeys.SystemDetectorFineGain, _detector.FineGain);


                        }
                    }

                    if (_acquisitionWorker != null && _acquisitionWorker.IsBusy)
                        _acquisitionWorker.CancelAsync();

                    if (!_backgroundAcquisitionService.IsActive)
                        _backgroundAcquisitionService.StartAsync();
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
