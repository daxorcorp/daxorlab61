﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.RocketDivisionKey;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageBox;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Services;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using RocketDivision.StarBurnX;

namespace Daxor.Lab.Utility.ViewModels
{
    public class ExportViewModel : ViewModelBase, IActiveAware
    {
        #region Constants

        public const string SuccessfulExportMessage = "The BVA tests have been exported successfully.";
        public const string ExportCaption = "Export BVA Tests";
        public const string ExportDiscErrorCaption = "Disc Error";
        
        #endregion

        #region Fields

        private readonly IBVADataService _bvaDataService;
        private readonly IMessageBoxDispatcher _msgDispatcher;
        private readonly IEventAggregator _eventAggregator;
        private readonly ILoggerFacade _logger;
        private readonly IStarBurnWrapper _opticalDriveWrapper;
        private readonly IFolderChooser _folderChooser;
        private readonly IMeasuredCalcEngineService _measuredCalcEngineService;
        private readonly IIdealsCalcEngineService _idealsCalcEngineService;
        private readonly ISettingsManager _settingsManager;
        private readonly IMessageManager _messageManager;
        private readonly IExcelReportGenerator _excelReportGenerator;
        private TaskScheduler _taskScheduler;
        private readonly IZipFileFactory _zipFileFactory;
        private bool _useIdealDispatcher = true;

        DelegateCommand<Object> _exportCommand;
        Boolean _exportAllTests;

        #endregion

        #region Ctor

        public ExportViewModel(IBVADataService dataService,
                               IMessageBoxDispatcher msgDispatcher,
                               IEventAggregator eventAggregator, 
                               ILoggerFacade logger,
                               IStarBurnWrapper opticalDriveWrapper,
                               IFolderChooser folderChooser,
                               IMeasuredCalcEngineService measuredCalcEngineService,
                               IIdealsCalcEngineService idealsCalcEngineService,
                               ISettingsManager settingsManager,
                               IMessageManager messageManager,
                               IExcelReportGenerator excelReportGenerator,
                               IZipFileFactory zipFileFactory)
        {
            _msgDispatcher = msgDispatcher;
            _eventAggregator = eventAggregator;
            _bvaDataService = dataService;
            _logger = logger;
            _opticalDriveWrapper = opticalDriveWrapper;
            _folderChooser = folderChooser;
            _measuredCalcEngineService = measuredCalcEngineService;
            _idealsCalcEngineService = idealsCalcEngineService;
            _settingsManager = settingsManager;
            _messageManager = messageManager;
            _excelReportGenerator = excelReportGenerator;
            _zipFileFactory = zipFileFactory;
        }

        #endregion

        #region Commands

        public ICommand ExportCommand
        {
            get
            {
                return _exportCommand = _exportCommand ?? new DelegateCommand<object>(ExportCommandExecute, ExportCommandCanExecute);
            }
        }

        private void ExportCommandExecute(object selectItems)
        {
            var selectedBvaTestItems = GetSelectedBvaTestItems(selectItems);
            if (selectedBvaTestItems == null)
                return;
            
            var folderChoice = _folderChooser.GetFolderChoice();
            if (folderChoice == null)
                return;

            if (IsSaveFilePathInvalid(folderChoice.Path))
            {
                var errorMessage = _messageManager.GetMessage(MessageKeys.UtilityExportPathInvalid).FormattedMessage;
                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error, errorMessage, ExportCaption, MessageBoxChoiceSet.Close);
                _logger.Log(errorMessage, Category.Info, Priority.Low);
                return;
            }

            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Exporting Blood Volume Analysis Tests...") { IsIndetermineState = true, PercentageCompleted = 0 });

            if (folderChoice.IsOpticalDrive)
                folderChoice.Path = _settingsManager.GetSetting<string>(SettingKeys.SystemTempFilePath);

            var tempPath = _settingsManager.GetSetting<string>(SettingKeys.SystemTempFilePath);
            var zipFilePath = folderChoice.Path;
            var fileName = GenerateGoodFileName();

            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;
            var exportTask = new Task(() => ExportTask(selectedBvaTestItems, tempPath, fileName, folderChoice.IsOpticalDrive, zipFilePath, tokenSource), token);
            exportTask.ContinueWith(result => OnExportTaskFaulted(result.Exception.InnerException), token,
                TaskContinuationOptions.OnlyOnFaulted, TaskScheduler);
            exportTask.ContinueWith(result => OnExportTaskSuccessful(), token,
                TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler);
            exportTask.Start(TaskScheduler);
        }

        protected virtual void OnExportTaskSuccessful()
        {
            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));

            if (_useIdealDispatcher)
                IdealDispatcher.BeginInvoke(() => _msgDispatcher.ShowMessageBox(MessageBoxCategory.Information,
                    "The BVA tests have been exported successfully.",
                    "Export BVA Tests", MessageBoxChoiceSet.Close));
            else
                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Information,
                    "The BVA tests have been exported successfully.",
                    "Export BVA Tests", MessageBoxChoiceSet.Close);

            ExportAllTests = false;
        }

        protected virtual void OnExportTaskFaulted(Exception exception)
        {
            _logger.Log(exception.Message, Category.Exception, Priority.High);
            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));

            if (_useIdealDispatcher)
            IdealDispatcher.BeginInvoke(() => 
                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error, exception.Message, ExportCaption, MessageBoxChoiceSet.Close));
            else
                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error, exception.Message, ExportCaption, MessageBoxChoiceSet.Close);

            ExportAllTests = false;
        }

        /// <summary>
        /// Performs the main workflow of exporting given BVA tests to the desired location
        /// </summary>
        /// <param name="selectedBvaTestItems">BVA test items to be exported</param>
        /// <param name="tempPath">Full path to the temporary folder</param>
        /// <param name="fileName">Just the name of the file to save (no path)</param>
        /// <param name="isOpticalDrive">True if tests are to be exported to optical media</param>
        /// <param name="zipFilePath">Desired path where the exported files should be written</param>
        /// <param name="tokenSource">Used for cancelling the export task</param>
        protected virtual void ExportTask(List<BVATestItem> selectedBvaTestItems, string tempPath, string fileName, bool isOpticalDrive, string zipFilePath, CancellationTokenSource tokenSource)
        {
            BuildAndSaveExcelReport(selectedBvaTestItems, tempPath, fileName);
            CreateZipFile(tempPath, fileName);

            if (isOpticalDrive)
                BurnToOpticalMedia(tempPath, fileName, tokenSource);
            else
                SaveToNonOpticalMedia(tempPath, fileName, zipFilePath);

            CleanupTempFiles(tempPath + fileName);
        }

        protected virtual void CleanupTempFiles(string filePath)
        {
            if (SafeFileOperations.FileManager.FileExists(filePath + ".zip"))
                SafeFileOperations.Delete(filePath + ".zip");

            if (SafeFileOperations.FileManager.FileExists(filePath + ".xml"))
                SafeFileOperations.Delete(filePath + ".xml");
        }

        protected virtual void SaveToNonOpticalMedia(string tempPath, string fileName, string zipFilePath)
        {
            SafeFileOperations.Copy(tempPath + fileName + ".zip", zipFilePath + fileName + ".zip");
        }

        protected virtual void BurnToOpticalMedia(string tempPath, string fileName, CancellationTokenSource tokenSource)
        {
            var dataBurner = _opticalDriveWrapper.CreateDataBurner();
            dataBurner.OnProgress += dataBurner_OnProgress;

            IDataFolder dataFolder = dataBurner;

            dataFolder.AddFile(tempPath + fileName + ".zip");

            try
            {
                dataBurner.Burn(false, RocketDivisionKey.DEF_VOLUME_NAME, "StarBurnX", "StarBurnX Data Burner");
            }
            catch (Exception ex)
            {
                string message;
                Message errorAlert;

                switch (ex.Message)
                {
                    case "Device is not ready:Device is not ready":
                        errorAlert = _messageManager.GetMessage(MessageKeys.UtilityExportDeviceNotReady);
                        message = errorAlert.FormattedMessage;
                        break;
                    case "Disk readonly: The media disk is not recordable":
                        errorAlert = _messageManager.GetMessage(MessageKeys.UtilityExportDiscReadonly);
                        message = errorAlert.FormattedMessage;
                        break;
                    case "Disc is finalized:The disc is finalized! Note that is not possible to write data on the finalized disc!":
                        errorAlert = _messageManager.GetMessage(MessageKeys.UtilityExportDiscFinalized);
                        message = errorAlert.FormattedMessage;
                        break;
                    default:
                        message = ex.Message;
                        break;
                }
                _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
                if (_useIdealDispatcher)
                    IdealDispatcher.BeginInvoke(() => _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error,
                        message, ExportDiscErrorCaption, MessageBoxChoiceSet.Close));
                else
                    _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error,
                        message, ExportDiscErrorCaption, MessageBoxChoiceSet.Close);
                
                _logger.Log(message, Category.Exception, Priority.High);
                dataBurner.OnProgress -= dataBurner_OnProgress;
                
                tokenSource.Cancel();
            }
            dataBurner.OnProgress -= dataBurner_OnProgress;
            dataBurner.Drive.Eject();
        }

        protected virtual void CreateZipFile(string tempPath, string fileName)
        {
            var zipFile =_zipFileFactory.CreateZipFileWithPassword(
                    _settingsManager.GetSetting<string>(SettingKeys.SystemPasswordCustomerAdmin));

            zipFile.AddFileToRoot(tempPath + fileName + ".xml");
            zipFile.Save(tempPath + fileName + ".zip");
        }

        protected virtual void BuildAndSaveExcelReport(List<BVATestItem> selectedBvaTestItems, string tempPath, string fileName)
        {
            var columnHeaders = new List<BvaExcelColumnHeader>();
            for (var i = 0; i < Enum.GetNames(typeof (BvaExcelColumnHeader)).Count(); i++)
                columnHeaders.Add((BvaExcelColumnHeader) i);

            _excelReportGenerator.BuildMetadata();
            _excelReportGenerator.BuildColumnHeaders(columnHeaders);

            foreach (var testItem in selectedBvaTestItems)
            {
                var selectedTest = _bvaDataService.SelectTest(testItem.TestID);

                var idealsResults = _idealsCalcEngineService.GetIdeals(selectedTest.Patient.WeightInKg,
                    selectedTest.Patient.HeightInCm,
                    (int) selectedTest.Patient.Gender);

                selectedTest.Volumes[BloodType.Ideal].RedCellCount = idealsResults.IdealRedCellVolume;
                selectedTest.Volumes[BloodType.Ideal].PlasmaCount = idealsResults.IdealPlasmaVolume;

                var measuredResults = GetMeasuredResults(selectedTest, _measuredCalcEngineService);

                selectedTest.Volumes[BloodType.Measured].RedCellCount = (int) Math.Round(measuredResults.TimeZeroRedCellVolume, 0);
                selectedTest.Volumes[BloodType.Measured].PlasmaCount = (int) Math.Round(measuredResults.TimeZeroPlasmaVolume, 0);

                selectedTest.Patient.DeviationFromIdealWeight = idealsResults.WeightDeviation;

                _excelReportGenerator.BuildPatientData(selectedTest, measuredResults);
            }
            _excelReportGenerator.SaveReport(tempPath + fileName + ".xml");
        }

        protected virtual List<BVATestItem> GetSelectedBvaTestItems(object selectedTestItems)
        {
            List<BVATestItem> testItemList = null;
            
            if (selectedTestItems is Collection<object>)
            {
                var objectCollection = selectedTestItems as Collection<object>;
                testItemList = objectCollection.Cast<BVATestItem>().ToList();
            }

            if (ExportAllTests)
                testItemList = BVATestItems.ToList();

            return testItemList;
        }

        private bool ExportCommandCanExecute(object noArgObject)
        {
            return SelectedItems.Count != 0;
        }

        #endregion

        #region Properties

        public Collection<object> SelectedItems { get; set; }

        public ObservableCollection<BVATestItem> BVATestItems
        {
            get { return _bvaItems; }
            private set { base.SetValue(ref _bvaItems, "BVATestItems", value); }
        }
        
        public bool ExportAllTests
        {
            get { return _exportAllTests; }
            set
            {
                base.SetValue(ref _exportAllTests, "ExportAllTests", value);
            }
        }

        public string TestID2Label
        {
            get { return _settingsManager.GetSetting<string>(SettingKeys.BvaTestId2Label); }
           
        }

        /// <summary>
        /// Gets or sets the scheduler for running asynchronous tasks. By default, this is the regular thread pool,
        /// but is overridable such that other schedulers can be used (i.e., for unit testing).
        /// </summary>
        /// <remarks>
        /// When using a custom scheduler, the IdealDispatcher (for running code on the UI thread) will not be
        /// used, so that the executing code will run under the custom scheduler.
        /// </remarks>
        public TaskScheduler TaskScheduler
        {
            get
            {
                return _taskScheduler ?? (_taskScheduler = TaskScheduler.Default);
            }
            set
            {
                _taskScheduler = value;
                _useIdealDispatcher = false;
            }
        }

        private ObservableCollection<BVATestItem> _bvaItems;

        #endregion

        #region EventHandlers
       
        void dataBurner_OnProgress(int percent, int timeRemaining)
        {
            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true) { PercentageCompleted = percent, Message = "Burning CD" });
        }

        #endregion

        #region IActiveAware

        private bool _isActive;

        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                if (_isActive)
                {
                    if (BVATestItems != null)
                    {
                        BVATestItems.Clear();
                        BVATestItems = null;
                    }
                    var temp = _bvaDataService.SelectBVATestItems();
                    BVATestItems = new ObservableCollection<BVATestItem>(from x in temp
                                   where x.Status != TestStatus.Aborted &&
                                   x.Status != TestStatus.Pending &&
                                   x.Status != TestStatus.Deleted &&
                                   x.Status != TestStatus.Running &&
                                   x.TestStatusDescription!="Needs Data" &&
                                   x.TestModeDescription == "Automatic" 
                                   select x);

                    FirePropertyChanged("TestID2Label");
                }
                else
                {
                    //Clear your own collection
                    if (BVATestItems != null)
                    {
                        BVATestItems.Clear();
                        BVATestItems = null;
                    }
                    ExportAllTests = false;
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion

        #region Helper Methods

        public void EnableExportButton()
        {
            _exportCommand.RaiseCanExecuteChanged();
        }

        private bool IsSaveFilePathInvalid(string filePath)
        {
            return string.IsNullOrWhiteSpace(filePath);
        }

        private string BuildFileName()
        {
            const string formatString = "BVAResults_{0}_{1}";
            var hospitalName = _settingsManager.GetSetting<String>(SettingKeys.SystemHospitalName);
            var formattedDateTime = DateTime.Now.ToString("MM-dd-yyyy hh-mm-ss tt");

            return String.Format(formatString, hospitalName, formattedDateTime);
        }

        private string StripInvalidCharacters(string fileName)
        {
            fileName = fileName.Replace("\\", "-");
            fileName = fileName.Replace("/", "-");
            fileName = fileName.Replace(":", "-");
            fileName = fileName.Replace("*", "-");
            fileName = fileName.Replace("?", "-");
            fileName = fileName.Replace("\"", "-");
            fileName = fileName.Replace(">", "-");
            fileName = fileName.Replace("<", "-");
            fileName = fileName.Replace("|", "-");

            return fileName;
        }

        private string GenerateGoodFileName()
        {
            var fileName = BuildFileName();
            return StripInvalidCharacters(fileName);
        }

        private MeasuredCalcEngineResults GetMeasuredResults(BVATest selectedTest, IMeasuredCalcEngineService measuredCalcEngineService)
        {
            var baseline = GetFirstBaselineCompositeSample(selectedTest);

            var mParams = new MeasuredCalcEngineParams
            {
                ReferenceVolume = selectedTest.RefVolume,
                AnticoagulantFactor = selectedTest.Tube.AnticoagulantFactor,
                Background = selectedTest.DurationAdjustedBackgroundCounts,
                StandardCounts = selectedTest.StandardCompositeSample.AverageCounts,
                BaselineCounts = baseline.AverageCounts,
                BaselineHematocrit = baseline.AverageHematocrit,
                IdealTotalBloodVolume = selectedTest.Volumes[BloodType.Ideal].WholeCount,
                Points = (from samp in selectedTest.PatientCompositeSamples
                          where samp.AverageCounts > -1 && samp.PostInjectionTimeInSeconds > 0
                          select new BloodVolumePoint(samp.SampleType, samp.IsWholeSampleExcluded, samp.AreBothHematocritsExcluded,
                              samp.AreBothCountsExcluded, samp.PostInjectionTimeInSeconds,
                              samp.AverageCounts, samp.AverageHematocrit,
                              samp.UnadjustedBloodVolume)).ToArray()
            };
            return measuredCalcEngineService.GetAllResults(mParams);
        }

        private static BVACompositeSample GetFirstBaselineCompositeSample(BVATest selectedTest)
        {
            return (from x in selectedTest.CompositeSamples
                    where x.SampleType == SampleType.Baseline
                    select x).FirstOrDefault<BVACompositeSample>();
        }
        #endregion
    }
}
