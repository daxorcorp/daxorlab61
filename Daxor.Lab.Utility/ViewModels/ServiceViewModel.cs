﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.Base;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.Utility.ViewModels
{
    public class ServiceViewModel : ViewModelBase, IActiveAware
    {
        public ServiceViewModel(QCSourcesSubViewModel qVm, GammaCounterSubViewModel gVm)
        {
            QCSourcesViewModel = qVm;
            GammaCounterViewModel = gVm;
        }

        public ServiceViewModel(GammaCounterSubViewModel gVm)
        {
            GammaCounterViewModel = gVm;
        }

        private object _sTabViewModel;
        private object _pTabViewModel;

        public Object SelectedTabViewModel
        {
            get { return _sTabViewModel; }
            set
            {
                _pTabViewModel = _sTabViewModel;

                if (base.SetValue(ref _sTabViewModel, "SelectedTabViewModel", value))
                {
                    IActiveAware pActiveAware = _pTabViewModel as IActiveAware;
                    if (pActiveAware != null)
                        pActiveAware.IsActive = false;

                    IActiveAware sActiveAware = _sTabViewModel as IActiveAware;
                    if (sActiveAware != null)
                        sActiveAware.IsActive = true;
                }
            }
        }

        public QCSourcesSubViewModel QCSourcesViewModel
        {
            get;
            private set;
        }
        public GammaCounterSubViewModel GammaCounterViewModel
        {
            get;
            private set;
        }

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;

                if (_isActive)
                    SelectedTabViewModel = QCSourcesViewModel;
                else
                {
                    IActiveAware sActiveAware = SelectedTabViewModel as IActiveAware;
                    sActiveAware.IsActive = value;
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate { };
    }
}
