﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.SettingsManager.Models;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Reports.ReportControllers;
using Daxor.Lab.Utility.Reports.ReportModels;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Regions;

namespace Daxor.Lab.Utility.ViewModels
{
    public class SettingsViewModel : ViewModelBase, IActiveAware
    {
        #region Fields

        readonly IAuthenticationManager _authService;
        readonly ThreadSafeObservableCollection<SettingBase> _settingsCollection = new ThreadSafeObservableCollection<SettingBase>();
        readonly ObservableCollection<IAppModuleInfo> _appModuleInfos = new ObservableCollection<IAppModuleInfo>();
        readonly IAppModuleInfoCatalog _moduleCatalog;
        private readonly IRegionManager _regionManager;
        private readonly ILoggerFacade _logger;
        private readonly IEventAggregator _eventAggregator;
        private readonly ISettingsManager _settingsManager;
       
        BackgroundTaskManager<IList<SettingBase>> _bkgSettingsLoader;
        User _loggedInUser;
        IAppModuleInfo _selectedAppModule;
        bool _isBusyLoading;

        #endregion

        #region Ctor

        public SettingsViewModel(IAppModuleInfoCatalog appModuleCatalog, IAuthenticationManager authService, IRegionManager regionManager,
            ILoggerFacade logger, IEventAggregator eventAggregator, ISettingsManager settingsManager)
        {
            _authService = authService;
            _moduleCatalog = appModuleCatalog;
            _moduleCatalog.Items.CollectionChanged += Items_CollectionChanged;
            _regionManager = regionManager;
            _logger = logger;
            _eventAggregator = eventAggregator;
            _settingsManager = settingsManager;
            
            ViewReportCommand = new DelegateCommand<object>(ExecuteViewReport, CanExecuteViewReport);
            
            var appModulesWithSettings = from app in _moduleCatalog.Items
                                         where app.IsConfigurable orderby app.DisplayOrder
                                         select app;

            AppModuleInfos.Add(new AppModuleInfo(null, null) { AppModuleKey = "SYSTEM", FullDisplayName = "System", ShortDisplayName="SYS"});
            AppModuleInfos.AddRange(appModulesWithSettings);

            ConfigureBackgroundTaskManager();
        }
        
        #endregion

        #region EventHandler
        public DelegateCommand<object> ViewReportCommand
        {
            get;
            private set;
        }

        void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (IAppModuleInfo app in e.NewItems)
                    if (app.AppModuleKey != AppModuleIds.MainMenu && app.AppModuleKey != AppModuleIds.Utilities)
                        AppModuleInfos.Add(app);
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (IAppModuleInfo app in e.OldItems)
                    if (app.AppModuleKey != AppModuleIds.MainMenu && app.AppModuleKey != AppModuleIds.Utilities)
                        AppModuleInfos.Remove(app);
            }

            var sortedInfos = AppModuleInfos.OrderBy(a => a.DisplayOrder).ToList();
            AppModuleInfos.Clear();
            AppModuleInfos.AddRange(sortedInfos);
        }

        #endregion

        #region Properties

        public ThreadSafeObservableCollection<SettingBase> SettingsCollection
        {
            get { return _settingsCollection; }
        }
        public ObservableCollection<IAppModuleInfo> AppModuleInfos
        {
            get { return _appModuleInfos; }
        }
        public IAppModuleInfo SelectedAppModuleInfo
        {
            get { return _selectedAppModule; }
            set
            {
                var prevAppModule = _selectedAppModule;

                if (!SetValue(ref _selectedAppModule, "SelectedAppModuleInfo", value)) return;
                
                //Save all the changes in previous setting setup
                if (prevAppModule != null)
                {
                    var dirtySettings = (from s in SettingsCollection
                        where s.IsDirty
                        select s).ToList();

                    _settingsManager.SetSettings(dirtySettings);
                }

                //Disposing settings, and eventing
                SettingsCollection.Clear();
                _bkgSettingsLoader.RunBackgroundTask();
            }
        }
        public User LoggedInUser
        {
            get { return _loggedInUser; }
            private set { SetValue(ref _loggedInUser, "LoggedInUser", value); }

        }
        public bool IsBusyLoading
        {
            get { return _isBusyLoading; }
            set { SetValue(ref _isBusyLoading, "IsBusyLoading", value); }
        }
       
        #endregion

        private bool CanExecuteViewReport(object arg)
        {
            return !_regionManager.Regions[RegionNames.ReportRegion].ActiveViews.Any();
        }
        private void ExecuteViewReport(object chartUris)
        {
            Task.Factory.StartNew(() =>
            {
                _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Loading Report..."));
                var reportController = new SettingsReportController(_logger, _regionManager, _eventAggregator, _settingsManager, new SettingsReportModel(SettingsDataAccessLayer.GetAllSettings()));
                _logger.Log("VIEW SETTINGS REPORT BUTTON CLICKED", Category.Info, Priority.None);

                IdealDispatcher.BeginInvoke(reportController.ShowReport);
            }).ContinueWith(answer => _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false)));
        }

        #region Helpers

        private void ConfigureBackgroundTaskManager()
        {
            _bkgSettingsLoader = new BackgroundTaskManager<IList<SettingBase>>(
                () =>
                {
                    List<SettingBase> settingsForModule = null;
                    try
                    {
                        IsBusyLoading = true;
                        if (SelectedAppModuleInfo != null)
                            settingsForModule = SettingsDataAccessLayer.GetSettingsForModule(SelectedAppModuleInfo.AppModuleKey);
                    }
                    catch
                    {
                        settingsForModule = null;
                    }

                    Thread.Sleep(1000);
                    return settingsForModule;
                },
                 settingsForModule =>
                 {
                     IsBusyLoading = false;
                     if (settingsForModule == null) return;
                     if (settingsForModule.FirstOrDefault() == null)    return;
                     
                     //If user has moved from this screen ignore and continue
                     if (!IsActive) return;
                     var firstSettingForModule = settingsForModule.FirstOrDefault();
                     if (firstSettingForModule == null || firstSettingForModule.AppModuleKey != SelectedAppModuleInfo.AppModuleKey)
                         return;
                     SettingsCollection.Clear();
                     SettingsCollection.AddRange(settingsForModule);
                 });
        }

        #endregion

        #region IsActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                if (!value)
                {
                    _moduleCatalog.Items.CollectionChanged -= Items_CollectionChanged;
                    SelectedAppModuleInfo = null;
                }
                else
                {
                    if (_authService.LoggedInUser == null)
                        throw new NullReferenceException("LoggedIn user should not be null");

                    LoggedInUser = _authService.LoggedInUser;
                }
                
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }

}
