﻿using System;
using System.Windows;
using System.Windows.Media.Animation;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Utility.Views
{
    /// <summary>
    /// Interaction logic for HomeView.xaml
    /// </summary>
    public partial class HomeView : IActiveAware
    {
        #region Fields

        private readonly Storyboard _badPasswordStoryboard;
        private readonly IAuthenticationManager _authService;
        private readonly ITestExecutionController _testExecutionController;
        private readonly IMessageBoxDispatcher _msgDispatcher;
        private readonly ILoggerFacade _logger;
        private readonly INavigationCoordinator _navCoordinator;
        private readonly IMessageManager _messageManager;

        #endregion

        #region Ctor

        public HomeView()
        {
            InitializeComponent();

            _badPasswordStoryboard = FindResource("BadPasswordStoryboard") as Storyboard;
        }
        public HomeView(HomeViewModel vm, IAuthenticationManager authService, ITestExecutionController testExecutionService,
            IMessageBoxDispatcher msgDispatcher, INavigationCoordinator navCoordinator, ILoggerFacade logger, IMessageManager messageManager)
            : this()
        {
            DataContext = vm;
            _authService = authService;
            _testExecutionController = testExecutionService;
            _msgDispatcher = msgDispatcher;
            _navCoordinator = navCoordinator;
            _logger = logger;
            _messageManager = messageManager;
        }

     
        #endregion

        #region EventHandlers

        void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            if (_testExecutionController.IsExecuting)
            {
                var errMessage = _messageManager.GetMessage(MessageKeys.UtilityLoginUnavailable).FormattedMessage;
                (DataContext as HomeViewModel).LoginLevel = AuthorizationLevel.Undefined;
                _msgDispatcher.ShowMessageBox(MessageBoxCategory.Error, errMessage, "Utilities Login Currently Unavailable", new [] {"Return to Previous Screen"});
                _logger.Log(errMessage, Category.Info, Priority.Low);
                _navCoordinator.ActivateAppModule(_navCoordinator.PreviousAppModuleKey, null);
            }
            else
            {
                uiPasswordBox.Text = "";
                if (IsActive)
                    VirtualKeyboardManager.OpenKeyboard(uiPasswordBox, e);
            }
        }
        void _authService_LoginFailed(object sender, EventArgs e)
        {
            VirtualKeyboardManager.CloseActiveKeyboard();
            uiUserNameBox.IsHitTestVisible = false;
            _badPasswordStoryboard.Completed += new StoryboardEventHandlerWrapper<HomeView>(this, _badPasswordStoryboard,
                        o =>
                        {
                            if (o == null) return;
                            o.uiUserNameBox.IsHitTestVisible = true;
                            uiBadPassword.Opacity = 1;

                            //By the time animation finishes, we might have a logged in user
                            if (_authService.LoggedInUser == null)
                                VirtualKeyboardManager.OpenKeyboard(uiPasswordBox, e);

                        }).CustomHandler;

            _badPasswordStoryboard.Begin();
        }
        void _authService_LoginSucceeded(object sender, EventArgs e)
        {
            uiBadPassword.Opacity = 0;
            uiGoodPassword.Opacity = 1;
        }
       
        #endregion

        #region IActiveAware

        bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                if (_isActive)
                {
                    uiBadPassword.Opacity = 0;
                    uiGoodPassword.Opacity = 0;

                    _authService.LoginFailed += _authService_LoginFailed;
                    _authService.LoginSucceeded += _authService_LoginSucceeded;
                }
                else
                {
                    _authService.LoginFailed -= _authService_LoginFailed;
                    _authService.LoginSucceeded -= _authService_LoginSucceeded;
                }
            }
        }


      
        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
