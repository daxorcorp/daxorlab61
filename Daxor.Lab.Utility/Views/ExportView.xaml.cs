﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.Utility.Views
{
    /// <summary>
    /// Interaction logic for ExportView.xaml
    /// </summary>
    public partial class ExportView : UserControl, IActiveAware
    {
        public ExportView()
        {
            InitializeComponent();
        }
        public ExportView(ExportViewModel vm)
            : this()
        {
            DataContext = vm;
            vm.SelectedItems = uiBVAExportGrid.SelectedItems;
        }


        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                IActiveAware vm = DataContext as IActiveAware;
                if (vm != null) 
                {
                    vm.IsActive = value;
                }
                if (!_isActive) // This clears out the checkbox upon leaving the view
                {
                    checkBoxSelectAll.IsChecked = false;
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        private void uiBVAExportGrid_SelectionChanged(object sender, Telerik.Windows.Controls.SelectionChangeEventArgs e)
        {
            ((ExportViewModel)DataContext).EnableExportButton();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if ((bool)checkBoxSelectAll.IsChecked)
            {
                uiBVAExportGrid.SelectAll();
            }
            else
            {
                uiBVAExportGrid.UnselectAll();
            }
        }
    }
}
