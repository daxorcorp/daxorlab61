﻿using System;
using System.Windows.Controls;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.Utility.Views
{
    /// <summary>
    /// Interaction logic for OrderingPhysiciansReportView.xaml
    /// </summary>
    public partial class OrderingPhysiciansReportView : UserControl, IActiveAware
    {
        public OrderingPhysiciansReportView()
        {
            InitializeComponent();

            //Clear design time data
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                ClearValue(DataContextProperty);
        }

        public OrderingPhysiciansReportView(OrderingPhysiciansReportViewModel viewModel)
            : this()
        {
            DataContext = viewModel;
        }

        #region IsActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                var dataContext = DataContext as IActiveAware;
                if (dataContext != null)
                    dataContext.IsActive = value;

                _isActive = value;
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
