﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using RocketDivision.StarBurnX;

namespace Daxor.Lab.Utility.Views
{
    /// <summary>
    /// Interaction logic for BackupDatabase.xaml
    /// </summary>
    public partial class ManualBackupView 
    {
        #region Fields

        private IDataBurner _dataBurner;
        private static ManualBackupViewModel _vm;
        private readonly IDriveAccessorService _driveAccessorService;

        #endregion

        #region Properties
        public IRegion Region
        {
            get;
            set;
        }

        #region DrivesTitle

        public string DrivesTitle
        {
            get { return (string)GetValue(DrivesTitleProperty); }
            set { SetValue(DrivesTitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DrivesTitle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DrivesTitleProperty =
            DependencyProperty.Register("DrivesTitle", typeof(string), typeof(ManualBackupView), new UIPropertyMetadata("Available Drives"));

        #endregion

        #region CurrentPath

        public String CurrentPath
        {
            get { return (String)GetValue(CurrentPathProperty); }
            set { SetValue(CurrentPathProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CurrentPath.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurrentPathProperty =
            DependencyProperty.Register("CurrentPath", typeof(String), typeof(ManualBackupView), new UIPropertyMetadata(String.Empty, OnPropertyChangedCurrentPath));
        private static void OnPropertyChangedCurrentPath(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            _vm.ZipFilePath = e.NewValue + @"\";
        }
        #endregion

        #endregion

        #region Ctor
        public ManualBackupView()
        {
            InitializeComponent();
        }

        public ManualBackupView(ManualBackupViewModel vm, ILoggerFacade logger, IDriveAccessorService driveAccessorService)
            : this()
        {
            DataContext = vm;
            _vm = vm;
            _driveAccessorService = driveAccessorService;
            AddDrives();
            driveList.SelectionChanged += driveList_SelectionChanged;
            treeView.SelectedItemChanged += treeView_SelectedItemChanged;

            try
            {
                _dataBurner = vm.OpticalDriveWrapper.CreateDataBurner();
            }
            catch
            {
                logger.Log("Unable to create a DataBurner via StarBurn", Category.Exception, Priority.High);
            }
        }
        #endregion

        #region Dtor
        ~ManualBackupView()
        {
            if (_dataBurner != null)
            {
                Marshal.ReleaseComObject(_dataBurner);
                _dataBurner = null;
            }
        }
        #endregion

        #region Private Helpers

        private void treeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var treeItem = e.NewValue as TreeViewItem;
            if (treeItem != null)
                CurrentPath = treeItem.Tag as String;
        }

        private void driveList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            treeView.Items.Clear();
            CurrentPath = String.Empty;

            if (e.AddedItems.Count == 0)
            {
                _vm.ShowStepOne = Visibility.Visible;
                _vm.ShowStepTwo = Visibility.Hidden;
                return;
            }

            var item = e.AddedItems[0] as ExportDrive;
            if (item == null)
                return;

            if (item.IsOpticalDrive)
            {
                _dataBurner.LockTray = false;
                _dataBurner.Drive.Eject();
                _vm.IsOpticalDrive = true;
            }
            else
                _vm.IsOpticalDrive = false;

            var treeItem = new TreeViewItem {Header = item.Tag, Tag = item.Tag, FontWeight = FontWeights.Bold};

            treeItem.Items.Add(null);
            treeItem.IsExpanded = false;
            treeItem.IsSelected = true;
            treeItem.Expanded += TreeViewItem_Expanded;
            treeView.Items.Add(treeItem);
            _vm.ShowStepOne = Visibility.Hidden;
            _vm.ShowStepTwo = Visibility.Visible;
        }

        private void AddDrives()
        {
            treeView.Items.Clear();
            driveList.Items.Clear();

            foreach (var drive in _driveAccessorService.GetAvailableDrives())
            {
                var exportDrive = new ExportDrive {Content = drive.Name, Tag = drive.Name};
                if (drive.DriveType == DriveTypeEnum.Optical)
                    exportDrive.IsOpticalDrive = true;
                if (drive.DriveType == DriveTypeEnum.Usb)
                    exportDrive.IsUsb = true;

                driveList.Items.Add(exportDrive);
            }

            //foreach (var driveInformation in DriveInfo.GetDrives())
            //{
            //    if (driveInformation.Name.Contains(@"C:\"))
            //        continue;

            //    if ((driveInformation.DriveType == DriveType.Network || driveInformation.DriveType == DriveType.Removable) && driveInformation.IsReady)
            //        driveList.Items.Add(new ExportDrive { Content = driveInformation.Name, Tag = driveInformation.Name, IsUsb = driveInformation.DriveType == DriveType.Removable });

            //    else if (driveInformation.DriveType == DriveType.Fixed)
            //        driveList.Items.Add(new ExportDrive()
            //        {
            //            Content = driveInformation.Name,
            //            Tag = driveInformation.Name,
            //            IsUsb = false
            //        });

            //    else if (driveInformation.DriveType == DriveType.CDRom)
            //        driveList.Items.Add(new ExportDrive { Content = driveInformation.Name, Tag = driveInformation.Name, IsOpticalDrive = true });
            //}

            noDrivesAvailable.Visibility = driveList.HasItems ? Visibility.Collapsed : Visibility.Visible;
        }

        private void TreeViewItem_Expanded(object sender, RoutedEventArgs e)
        {
            var rootItem = (TreeViewItem)sender;

            if (rootItem.Items.Count == 1 && rootItem.Items[0] == null)
            {
                rootItem.Items.Clear();

                string[] dirs;
                try
                {
                    dirs = Directory.GetDirectories((string)rootItem.Tag);
                }
                catch
                {
                    return;
                }

                foreach (var dir in dirs)
                {
                    var subItem = new TreeViewItem
                    {
                        Style = treeView.ItemContainerStyle,
                        Header = new DirectoryInfo(dir).Name,
                        Tag = dir
                    };
                    try
                    {
                        if (Directory.GetDirectories(dir).Length > 0)
                            subItem.Items.Add(null);
                    }
                    catch { }
                    subItem.Expanded += TreeViewItem_Expanded;
                    rootItem.Items.Add(subItem);
                }
            }
        }

        #endregion
    }
}