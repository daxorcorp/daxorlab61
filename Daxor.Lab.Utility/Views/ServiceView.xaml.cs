﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.Utility.Views
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ServiceView : UserControl, IActiveAware
    {
        public ServiceView()
        {
            InitializeComponent();
        }
        public ServiceView(ServiceViewModel vm)
            : this()
        {
            DataContext = vm;
        }



        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                IActiveAware vm = DataContext as IActiveAware;
                if (vm != null)
                    vm.IsActive = value;

            }
        }

        public event EventHandler IsActiveChanged = delegate { };
    }
}
