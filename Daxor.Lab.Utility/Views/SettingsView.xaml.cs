﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Models;
using Daxor.Lab.Utility.Models;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite;
using UserControl = System.Windows.Controls.UserControl;

namespace Daxor.Lab.Utility.Views
{
    /// <summary>
    /// Interaction logic for SettingsView.xaml
    /// </summary>
    public partial class SettingsView : UserControl, IActiveAware
    {
        public SettingsView()
        {
            InitializeComponent();

            //Clear design time data
            if (!System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
                ClearValue(DataContextProperty);

            uiOptionsList.SelectedIndex = 0;

        }
        public SettingsView(SettingsViewModel viewModel)
            : this()
        {
            DataContext = viewModel;
            uiOptionsList.SelectedIndex = 0;
        }

        #region IsActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                var dataContext = DataContext as IActiveAware;
                if (dataContext != null)
                    dataContext.IsActive = value;

                _isActive = value;

                if (_isActive)
                    uiOptionsList.SelectedIndex = 0;
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion

        /// <summary>
        /// Handles the situation where the user enters a null, an empty string or a string with only white space.
        /// </summary>
        /// <remarks>
        /// This is necessary because the commands bound to the RadDataForm are not easily accessible
        ///</remarks>
        /// <param name="sender">The RadDataForm submitting the text to the setting</param>
        /// <param name="e">Arguments for the event</param>
        private void uiDataForm_EditEnding(object sender, Telerik.Windows.Controls.Data.DataForm.EditEndingEventArgs e)
        {
            var settingsViewModel = DataContext as SettingsViewModel;
            var senderAsRadDataForm = sender as Telerik.Windows.Controls.RadDataForm;
            if (settingsViewModel == null || senderAsRadDataForm == null) return;

            foreach (ObservableValue item in senderAsRadDataForm.ItemsSource)
            {
                if (item.Value == null || item.Value.Trim() == String.Empty)
                {
                    var referringPhysician = settingsViewModel.SettingsCollection.FirstOrDefault(setting => setting.Key == SettingKeys.BvaReferringPhysicians) as MultipleValuesSetting;
                    var refferingPhysicianList = referringPhysician.Value as ObservableCollection<ObservableValue>;
                    refferingPhysicianList.Remove(item);
                    return;
                }
            }
        }

        private void UiLaunchFolderBrowser_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            var result = dialog.ShowDialog();
            if (result != DialogResult.OK)
                return;

            var settingsViewModel = DataContext as SettingsViewModel;
            if (settingsViewModel == null)
                return;

            var newPath = dialog.SelectedPath + "\\";
            var automaticBackupLocation = settingsViewModel.SettingsCollection.FirstOrDefault(setting => setting.Key == SettingKeys.SystemAutomaticBackupLocation) as SingleValueSetting;
            
            if (automaticBackupLocation != null) 
                automaticBackupLocation.Value = newPath;
        }
    }
}
