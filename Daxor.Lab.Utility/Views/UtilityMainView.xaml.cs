﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Daxor.Lab.Utility.ViewModels;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.Utility.Views
{
    /// <summary>
    /// Interaction logic for UtilityMainView.xaml
    /// </summary>
    public partial class UtilityMainView : UserControl, IActiveAware
    {
        public UtilityMainView()
        {
            InitializeComponent();
        }

        #region IActiveAware

        private bool _isActive;
        public event EventHandler IsActiveChanged = delegate { };
        public bool IsActive
        {
            get
            {
                return _isActive;
            }

            set
            {
                _isActive = value;
                IActiveAware activeAware = DataContext as IActiveAware;
                if (activeAware != null)
                {
                    activeAware.IsActive = value;
                }
            }
        }

        #endregion
    }
}
