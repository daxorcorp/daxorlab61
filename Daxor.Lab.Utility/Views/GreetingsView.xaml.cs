﻿using System;
using System.Windows.Controls;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.Utility.Views
{
    /// <summary>
    /// Interaction logic for GreetingsView.xaml
    /// </summary>
    public partial class GreetingsView : UserControl, IActiveAware
    {
        private readonly IAuthenticationManager _authService;
        public GreetingsView()
        {
            InitializeComponent();
        }
        public GreetingsView(IAuthenticationManager authService)
            : this()
        {
            _authService = authService;
        }


        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                if (_isActive)
                {
                    uiWelcomeTextBlock.Text = char.ToUpper(_authService.LoggedInUser.UserName[0]) + _authService.LoggedInUser.UserName.Substring(1) +" Admin";
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate{};
    }
}
