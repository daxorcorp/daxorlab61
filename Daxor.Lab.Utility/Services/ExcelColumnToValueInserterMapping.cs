﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Utility.Common;

namespace Daxor.Lab.Utility.Services
{
    public class ExcelColumnToValueInserterMapping
    {
        #region Fields

        private Dictionary<BvaExcelColumnHeader, Action<ExcelRowConfiguration, int>> _excelColumnToValueInserterMapping;

        #endregion

        #region Ctor

        public ExcelColumnToValueInserterMapping()
        {
            InitializeMappings();
        }

        #endregion

        #region Public API

        /// <summary>
        /// Retrieves the delegate responsible for inserting the value corresponding
        /// to the given column name into the worksheet
        /// </summary>
        /// <param name="excelColumnName">Column whose value should be inserted</param>
        /// <returns>Delegate responsible for inserting the value</returns>
        public Action<ExcelRowConfiguration, int> GetValueInserterForColumn(BvaExcelColumnHeader excelColumnName)
        {
            Action<ExcelRowConfiguration, int> result;
            if (_excelColumnToValueInserterMapping.TryGetValue(excelColumnName, out result) == false)
                throw new ArgumentOutOfRangeException("There is no value inserter associated with column '" + excelColumnName + "'");

            return result;
        }

        #endregion

        #region Helpers

        private void InitializeMappings()
        {
            _excelColumnToValueInserterMapping = new Dictionary<BvaExcelColumnHeader, Action<ExcelRowConfiguration, int>>
            {
                {BvaExcelColumnHeader.AnalyzedOn, ValueInserters.InsertAnalzyedOn},
                {BvaExcelColumnHeader.LastName, ValueInserters.InsertLastName},
                {BvaExcelColumnHeader.FirstName, ValueInserters.InsertFirstName},
                {BvaExcelColumnHeader.MiddleName, ValueInserters.InsertMiddleName},
                {BvaExcelColumnHeader.DateOfBirth, ValueInserters.InsertDateOfBirth},
                {BvaExcelColumnHeader.LocationLabel, ValueInserters.InsertLocationLabel},
                {BvaExcelColumnHeader.HospitalId, ValueInserters.InsertHospitalPatientId},
                {BvaExcelColumnHeader.TestId2Label, ValueInserters.InsertTestId2Label},
                {BvaExcelColumnHeader.PatientSex, ValueInserters.InsertPatientGender},
                {BvaExcelColumnHeader.PatientHeightInCm, ValueInserters.InsertPatientHeightInCm},
                {BvaExcelColumnHeader.PatientWeightInKg, ValueInserters.InsertPatientWeightInKg},
                {BvaExcelColumnHeader.IdealWeightDeviation, ValueInserters.InsertIdealWeightDeviation},
				{BvaExcelColumnHeader.AmputeeStatus, ValueInserters.InsertAmputeeStatus},
				{BvaExcelColumnHeader.AmputeeIdealsCorrectionFactor, ValueInserters.InsertAmputeeIdealsCorrectionFactor},
                {BvaExcelColumnHeader.BloodVolume, ValueInserters.InsertBloodVolumeInMl},
                {BvaExcelColumnHeader.IdealBloodVolume, ValueInserters.InsertIdealBloodVolumeInMl},
                {BvaExcelColumnHeader.BloodVolumeDeviation, ValueInserters.InsertBloodVolumeDeviationInMl},
                {BvaExcelColumnHeader.BloodVolumeExcessAndDeficit, ValueInserters.InsertBloodVolumeExcessOrDeficit},
                {BvaExcelColumnHeader.PlasmaVolume, ValueInserters.InsertPlasmaVolumeInMl},
                {BvaExcelColumnHeader.IdealPlasmaVolume, ValueInserters.InsertIdealPlasmaVolumeInMl},
                {BvaExcelColumnHeader.PlasmaVolumeDeviation, ValueInserters.InsertPlasmaVolumeDeviation},
                {BvaExcelColumnHeader.PlasmaVolumeExcessAndDeficit, ValueInserters.InsertPlasmaVolumeExcessOrDeficit},
                {BvaExcelColumnHeader.RedBloodCellVolume, ValueInserters.InsertRedBloodCellVolumeInMl},
                {BvaExcelColumnHeader.IdealRedBloodCellVolume, ValueInserters.InsertIdealRedBloodCellVolumeInMl},
                {BvaExcelColumnHeader.RedBloodCellDeviation, ValueInserters.InsertRedBloodCellDeviation},
                {BvaExcelColumnHeader.RedBloodCellExcessAndDeficit, ValueInserters.InsertRedBloodCellExcessOrDeficit},
                {BvaExcelColumnHeader.Slope, ValueInserters.InsertSlope},
                {BvaExcelColumnHeader.StandardDeviation, ValueInserters.InsertStandardDeviation},
                {BvaExcelColumnHeader.NormalHct, ValueInserters.InsertNormalizedHematocrit},
                {BvaExcelColumnHeader.PeripheralHct, ValueInserters.InsertPeripheralHematocrit},
                {BvaExcelColumnHeader.ReferringMd, ValueInserters.InsertReferringPhysician},
                {BvaExcelColumnHeader.CC, ValueInserters.InsertCcReportTo},
                {BvaExcelColumnHeader.Analyst, ValueInserters.InsertAnalyst},
                {BvaExcelColumnHeader.InjectateLot, ValueInserters.InsertInjectateLot},
                {BvaExcelColumnHeader.Comments, ValueInserters.InsertComments}
            };
        }

        #endregion
    }
}
