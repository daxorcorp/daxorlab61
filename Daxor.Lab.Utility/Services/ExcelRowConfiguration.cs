using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;

namespace Daxor.Lab.Utility.Services
{
    public class ExcelRowConfiguration
    {
        private readonly IIdealsCalcEngineService _idealsCalcEngineService;
        private BVATest _test;
        private MeasuredCalcEngineResults _measuredCalcEngineResults;

        public ExcelRowConfiguration(IIdealsCalcEngineService idealsCalcEngineService)
        {
            _idealsCalcEngineService = idealsCalcEngineService;
        }
        
        public int Row { get; set; }

        public BVATest Test 
        {
            get
            {
                if (_test == null)
                    throw new InvalidOperationException("Test cannot be null");

                if (_test.Patient == null)
                    throw new InvalidOperationException("Test cannot contain a null patient");

                return _test;
            }
            set
            {
                _test = value;
            }
        }

        public MeasuredCalcEngineResults MeasuredCalcEngineResults
        {
            get
            {
                if (_measuredCalcEngineResults == null)
                    throw new InvalidOperationException("MeasuredCalcEngineResults cannot be null");

                return _measuredCalcEngineResults;
            }
            set
            {
                _measuredCalcEngineResults = value;
            }
        }
        
        public virtual bool AreIdealsResultsAvailable()
        {
            if (_idealsCalcEngineService == null) throw new InvalidOperationException("Ideals calc engine service cannot be null");

            var idealsResults = _idealsCalcEngineService.GetIdeals(Test.Patient.WeightInKg, Test.Patient.HeightInCm, (int)Test.Patient.Gender);

            if (idealsResults.WeightDeviation > BvaDomainConstants.MaximumWeightDeviation)
                return false;

            if (idealsResults.WeightDeviation < BvaDomainConstants.MinimumWeightDeviation)
                return false;

            if (Test.Patient.HeightInCm > _idealsCalcEngineService.MaximumHeightInCm)
                return false;

            return !(Test.Patient.HeightInCm < _idealsCalcEngineService.MinimumHeightInCm);
        }
    }
}