﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Utility.Common;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Utility.Services
{
    public class UtilityDataService : IUtilityDataService
    {
        #region Fields

        private readonly ILoggerFacade _logger;
        private readonly IUtilityDataMapper _dataMapper;

        #endregion

        #region Ctor

        public UtilityDataService(ILoggerFacade logger, IUtilityDataMapper dataMapper)
        {
            _logger = logger;
            _dataMapper = dataMapper;
        }

        #endregion

        public List<OrderingPhysicianDataRecord> GetOrderingPhysiciansBetween(DateTime dateFromDateTime, DateTime dateToDateTime)
        {
            IEnumerable<GetOrderingPhysiciansBetweenResult> records;
            try
            {
                records = GetOrderingPhysiciansFromDatabase(dateFromDateTime, dateToDateTime);
            }
            catch (Exception ex)
            {
                _logger.Log("Exception: "+ ex.Message, Category.Exception, Priority.High);
                throw;
            }
            return _dataMapper.MapOrderingPhysicianDataRecords(records);
        }

        protected virtual IEnumerable<GetOrderingPhysiciansBetweenResult> GetOrderingPhysiciansFromDatabase(DateTime dateFromDateTime, DateTime dateToDateTime)
        {
            var context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            var records = context.GetOrderingPhysiciansBetween(dateFromDateTime.Date, dateToDateTime.Date);
            return records;
        }
    }
}
