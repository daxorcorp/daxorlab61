using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Utility.Common;

namespace Daxor.Lab.Utility.Services
{
    public class UtilityDataMapper : IUtilityDataMapper
    {
        public const string UnknownPhysicianLabel = "Unknown Physician";
        public const string UnknownSpecialityLabel = "Unknown Specialty";

        public List<OrderingPhysicianDataRecord> MapOrderingPhysicianDataRecords(IEnumerable<GetOrderingPhysiciansBetweenResult> recordEnumerable)
        {
            var records = recordEnumerable.ToList();
            
            var physiciansList = (from r in records
                orderby r.TEST_DATE descending
                select r.REFERRING_PHYSICIAN).Distinct().ToList();

            var orderingPhysiciansDataRecords = new List<OrderingPhysicianDataRecord>();

            foreach (var referringPhysician in physiciansList)
            {
                var myReferringPhysician = referringPhysician;
                var physiciansSpecialty = (from r in records
                    where myReferringPhysician == r.REFERRING_PHYSICIAN
                    where !String.IsNullOrEmpty(r.PHYSICIANS_SPECIALTY)
                    orderby r.TEST_DATE descending
                    select r.PHYSICIANS_SPECIALTY).FirstOrDefault();

                var testRecordsList = from r in records
                    where myReferringPhysician == r.REFERRING_PHYSICIAN
                    orderby r.TEST_DATE descending
                    select new OrderingPhysicianTestRecord
                    {
                        TestDate = r.TEST_DATE.ToString("M/d/yy"),
                        CcReportTo = r.CC_REPORT_TO,
                        Analyst = r.ANALYST,
                        Location = r.LOCATION,
                    };

                orderingPhysiciansDataRecords.Add(new OrderingPhysicianDataRecord
                {
                    ReferringPhysician = string.IsNullOrWhiteSpace(referringPhysician) ? UnknownPhysicianLabel : referringPhysician,
                    PhysicianSpecialty = string.IsNullOrWhiteSpace(physiciansSpecialty)  ? UnknownSpecialityLabel : physiciansSpecialty,
                    TestRecordsList = testRecordsList.ToList(),
                });
            }

            return orderingPhysiciansDataRecords;
        }
    }
}