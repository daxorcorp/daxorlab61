﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.Utility.Common;

namespace Daxor.Lab.Utility.Services
{
    public class ExcelReportGenerator : IExcelReportGenerator
    {
        #region Constants

        public const string WorksheetName = "DAXOR BVA Data";
        
        #endregion

        #region Fields

        private readonly ExcelColumnToValueInserterMapping _excelColumnToValueInserterMapping;
        private readonly IExcelFileBuilder _excelFileBuilder;
        private List<BvaExcelColumnHeader> _columnHeaders;
        private int _currentRow;
        private readonly ISettingsManager _settingsManager;
        private readonly IIdealsCalcEngineService _idealsCalcEngineService;

        #endregion

        #region Ctor

        public ExcelReportGenerator(ISettingsManager settingsManager, IIdealsCalcEngineService idealsCalcEngine, IExcelFileBuilder excelFileBuilder)
        {
            _settingsManager = settingsManager;

            _excelFileBuilder = excelFileBuilder;
            _excelFileBuilder.AddWorksheet(WorksheetName);

            _idealsCalcEngineService = idealsCalcEngine;

            ValueInserters.Initialize(_excelFileBuilder, _excelFileBuilder.GetFirstWorksheet());
            _excelColumnToValueInserterMapping = new ExcelColumnToValueInserterMapping();
        }
        #endregion

        #region Public API

        /// <summary>
        /// Includes ExportedOn and Version attributes
        /// </summary>
        public void BuildMetadata()
        {
            _currentRow = 1;

            var worksheet = _excelFileBuilder.GetFirstWorksheet();

            _excelFileBuilder.InsertStringCell(worksheet, _currentRow, 1, "BVA Results for: ", CellStyle.Bold);
            _excelFileBuilder.InsertStringCell(worksheet, _currentRow, 2, _settingsManager.GetSetting<String>(SettingKeys.SystemHospitalName), CellStyle.None);
            _currentRow++;

            _excelFileBuilder.InsertStringCell(worksheet, _currentRow, 1, "Exported On: ", CellStyle.Bold);
            _excelFileBuilder.InsertDateTimeCell(worksheet, _currentRow, 2, DateTime.Now.ToString("g"), CellStyle.UsDateTime);
            _currentRow++;

            _excelFileBuilder.InsertStringCell(worksheet, _currentRow, 1, "Version: ", CellStyle.Bold);

            var version = Assembly.GetExecutingAssembly().GetName().Version;
            _excelFileBuilder.InsertStringCell(worksheet, _currentRow, 2, version.ToString(), CellStyle.None);
        }

        /// <summary>
        /// Writes the column headers to the Excel Report
        /// </summary>
        /// <param name="columnHeaders">List of column headers in the order to be written to</param>
        /// <remarks>Must be called before any calls to WritePatientData</remarks>
        public void BuildColumnHeaders(List<BvaExcelColumnHeader> columnHeaders)
        {
            _columnHeaders = columnHeaders;
            _currentRow = 5;
            
            var column = 0;
            const CellStyle cellStyle = CellStyle.ColumnHeading;
            var worksheet = _excelFileBuilder.GetFirstWorksheet();

            foreach (var columnHeader in columnHeaders)
            {
                string content = columnHeader.GetStringValue();

                if (columnHeader == BvaExcelColumnHeader.LocationLabel)
                    content = _settingsManager.GetSetting<string>(SettingKeys.BvaLocationLabel);
                else if (columnHeader == BvaExcelColumnHeader.TestId2Label)
                    content = _settingsManager.GetSetting<string>(SettingKeys.BvaTestId2Label);

                _excelFileBuilder.InsertStringCell(worksheet, _currentRow, ++column, content, cellStyle);
            }
            _currentRow++;
        }

        /// <summary>
        /// Writes one line of information using the patient data based from the column headers used in the report
        /// </summary>
        /// <param name="bvaTest"></param>
        /// <param name="measuredResults"></param>
        /// <exception cref="InvalidOperationException">Thrown if the column headers have not been set up</exception>
        /// <remarks>WriteColumnHeaders must have been called before using this method</remarks>
        public void BuildPatientData(BVATest bvaTest, MeasuredCalcEngineResults measuredResults)
        {
            if (_columnHeaders == null)
                throw new InvalidOperationException("Cannot write patient data because columns have not been setup");
            
            var column = 1;
            foreach (var columnHeader in _columnHeaders)
            {
                var valueInserter = _excelColumnToValueInserterMapping.GetValueInserterForColumn(columnHeader);

                valueInserter(
                    new ExcelRowConfiguration(_idealsCalcEngineService)
                    {
                        MeasuredCalcEngineResults = measuredResults,
                        Row = _currentRow,
                        Test = bvaTest
                    }, column);

                column++;
            }
            _currentRow++;
        }
        
        /// <summary>
        /// Saves the report to the specified location
        /// </summary>
        /// <param name="filePath"></param>
        /// <exception cref="ArgumentNullException">Thrown if filePath is null or whitespace</exception>
        public void SaveReport(string filePath)
        {
            if (String.IsNullOrWhiteSpace(filePath))
                throw new ArgumentNullException("filePath");

           _excelFileBuilder.SaveExcelWorkbook(filePath);

		   _excelFileBuilder.AddWorksheet(WorksheetName);
		   ValueInserters.Initialize(_excelFileBuilder, _excelFileBuilder.GetFirstWorksheet());
        }
        #endregion
    }
}
