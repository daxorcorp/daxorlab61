using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Utility.Common;

namespace Daxor.Lab.Utility.Services
{
    public interface IUtilityDataMapper
    {
        List<OrderingPhysicianDataRecord> MapOrderingPhysicianDataRecords(IEnumerable<GetOrderingPhysiciansBetweenResult> recordEnumerable);
    }
}