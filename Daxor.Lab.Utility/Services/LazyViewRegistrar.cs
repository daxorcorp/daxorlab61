﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.Utility.Services
{
    public class LazyViewRegistrar
    {
        readonly Dictionary<String, Dictionary<String,Func<Object>>> _lazyViews;

        public LazyViewRegistrar()
        {
            _lazyViews = new Dictionary<string, Dictionary<string, Func<object>>>();
        }

        public void RegisterViewWithRegion(string regionName, Func<Object> viewInstantiator, string viewName)
        {
            if (regionName == null)
                throw new ArgumentNullException("region");
            if (viewInstantiator == null)
                throw new ArgumentNullException("viewInstantiator");
            if (String.IsNullOrEmpty(viewName))
                throw new ArgumentOutOfRangeException("viewName");

            if (_lazyViews.ContainsKey(regionName) && _lazyViews[regionName].ContainsKey(viewName) &&
                _lazyViews[regionName][viewName] != null)
                throw new Exception(viewName + " already contains initializer");

            //Do the actual registration
            if (!_lazyViews.ContainsKey(regionName))
                _lazyViews.Add(regionName, new Dictionary<string, Func<object>>());

            _lazyViews[regionName][viewName] = viewInstantiator;
        }
        public int NumRegisteredViewWithRegion(string regionName)
        {
            if (regionName == null)
                throw new ArgumentNullException("regionName");

            return !_lazyViews.ContainsKey(regionName) ? 0 : _lazyViews[regionName].Count;
        }
   
        public IEnumerable<String> ViewNamesInRegion(string regionName)
        {
            if (regionName == null)
                throw new ArgumentNullException("regionName");

            if (_lazyViews[regionName] == null)
                throw new NotSupportedException("LazyViewRegistrar.ViewNamesInRegion() -- no dictionary for region '" + regionName + "'");
            return _lazyViews[regionName].Keys.AsEnumerable();
        }
        public Object GetInstanceOfViewInRegion(string regionName, string viewName)
        {
            if (regionName == null)
                throw new ArgumentNullException("regionName");
            if (String.IsNullOrEmpty(viewName))
                throw new ArgumentOutOfRangeException("viewName");

            Object instance;
            try
            {
                instance = _lazyViews[regionName][viewName]();
            }
            catch
            {
                instance = null;
            }

            return instance;
        }
    }
}
