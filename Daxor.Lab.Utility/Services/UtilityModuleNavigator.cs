﻿using System;
using System.Windows;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Utility.Common;
using Daxor.Lab.Utility.Views;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using System.Linq;
using Daxor.Lab.Domain.Entities.Base;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Utility.Services
{
    public class UtilityModuleNavigator : ObservableObject, IAppModuleNavigator
    {
        #region Fields

        readonly LazyViewRegistrar _lazyViewRegistrar = new LazyViewRegistrar();
        readonly string _moduleKey;
        readonly IUnityContainer _container;
        readonly IRegionManager _regionManager;
        readonly IEventAggregator _eventAggregator;
        readonly ILoggerFacade _logger;
        readonly string[] validViewKeys = new string[] { UtilityModuleViewKeys.Empty, UtilityModuleViewKeys.DatabaseView, UtilityModuleViewKeys.ExportView, UtilityModuleViewKeys.LoadingView, UtilityModuleViewKeys.HomeView, UtilityModuleViewKeys.ServiceView, UtilityModuleViewKeys.SettingsView };
        string _currentViewKey = UtilityModuleViewKeys.Empty;

        #endregion

        #region Ctor

        public UtilityModuleNavigator(IUnityContainer container, IEventAggregator eventAggregator, IRegionManager regionManager, 
                                      INavigationCoordinator navigationCoordinator, ILoggerFacade logger)
        {
            _container = container;
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _logger = logger;

            //Set my local application key
            _moduleKey = AppModuleIds.Utilities;

            //Register myself with a global application coordinator
            navigationCoordinator.RegisterAppModuleNavigator(_moduleKey, this);
            NavigationCoordinator = navigationCoordinator;
        }

        #endregion

        #region Properties

        private string CurrentViewKey
        {
            get { return _currentViewKey; }
            set {  _currentViewKey = value; }
        }
        public bool IsActive
        {
            get { return NavigationCoordinator.ActiveAppModuleKey.Equals(_moduleKey); }
        }
        public INavigationCoordinator NavigationCoordinator
        {
            get;
            private set;
        }
        public FrameworkElement RootViewHost
        {
            get;
            private set;
        }
        public string AppModuleKey
        {
            get { return _moduleKey; }
        }
        public IRegion InnerRegion
        {
            get;
            private set;
        }
        

        #endregion

        #region Methods

        public bool CanDeactivate()
        {
            var iAware = RootViewHost as IGlobalNavigationAware;
            if (iAware != null)
                iAware.NavigatingFrom();

            //deal with root host view
            return true;
        }
        public bool CanActivate()
        {
            var iAware = RootViewHost as IGlobalNavigationAware;
            if (iAware != null)
                iAware.NavigatingTo();

            return true;
        }

        public void Activate(DisplayViewOption displayViewOption, object payload)
        {
            //Fire aggregate event to display MainMenuDropDown part of application
            _eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(ApplicationParts.MainMenuDropDown, true));
            _eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(ApplicationParts.Footer, false));

            if (IsActive)
                return;

            ActivateView(UtilityModuleViewKeys.HomeView, payload);
        }
        public void Deactivate(Action onDeactivateCallback)
        {
            //Run storyboard, and on storyboard complete remove the view;
            if (CurrentViewKey != UtilityModuleViewKeys.Empty)
            {
                //First remove active view and sub it with loading view
                InnerRegion.Deactivate(InnerRegion.GetView(CurrentViewKey));
                
                _regionManager.Regions[RegionNames.WorkspaceRegion].Deactivate(RootViewHost);

                //Destroy all lazy views, if we navigating away
                foreach(var lazyViewName in _lazyViewRegistrar.ViewNamesInRegion(InnerRegion.Name)){

                    var view = InnerRegion.GetView(lazyViewName);
                    if (view != null)
                    {
                        InnerRegion.Deactivate(view);
                        InnerRegion.Remove(view);
                    }

                    var element = view as FrameworkElement;
                    if (element != null)
                        element.DataContext = null;
                }

                CurrentViewKey = UtilityModuleViewKeys.Empty;
                _eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(ApplicationParts.Footer, true));
            }
            onDeactivateCallback();
        }

        public void ActivateView(string viewKey)
        {
            ActivateView(viewKey, null);
        }

        public void ActivateView(string viewKey, object payload)
        {
            try
            {
                if (viewKey == UtilityModuleViewKeys.Empty)
                    throw new ArgumentException("UtilitiesModuleNavigator.ActivateView(): The view key '" + viewKey + "' is not valid.");

                //Notify of AppModuleModelChanged
                if (_eventAggregator == null)
                    throw new NotSupportedException("UtilitiesModuleNavigator.ActivateView() - cannot work with a null event aggregator instance");
                if (payload != null)
                    _eventAggregator.GetEvent<AppModuleModelChanged>().Publish(new ModelChangedPayload(_moduleKey, payload));

                if (InnerRegion == null)
                    throw new NotSupportedException("UtilitiesModuleNavigator.ActivateView() - cannot work with a null InnerRegion");

                var requestedView = InnerRegion.GetView(viewKey);
                if (requestedView == null)
                {
                    if (_lazyViewRegistrar == null)
                        throw new NotSupportedException("UtilitiesModuleNavigator.ActivateView() - cannot work with a null lazy view registrar");
                    
                    var viewNamesInInnerRegion = _lazyViewRegistrar.ViewNamesInRegion(InnerRegion.Name);
                    if (viewNamesInInnerRegion == null)
                        throw new NotSupportedException("UtilitiesModuleNavigator.ActivateView() - no view names correspond to InnerRegion");

                    if (viewNamesInInnerRegion.Contains(viewKey))
                    {
                        foreach (var viewName in viewNamesInInnerRegion)
                        {
                            //Add all views to the region
                            try
                            {
                                var rView = _lazyViewRegistrar.GetInstanceOfViewInRegion(InnerRegion.Name, viewName);
                                if (rView == null)
                                {
                                    _logger.Log("UtilitiesModuleNavigator.ActivateView() - view name '" + viewName + "' could not be constructed because GetInstanceOfViewInRegion() returned null.", Category.Warn, Priority.High);
                                    continue;
                                }

                                if (!InnerRegion.Views.Contains(rView))
                                    InnerRegion.Add(rView, viewName);
                            
                                if(viewName.Equals(viewKey))
                                    requestedView = rView;
                            }
                            catch (Exception ex)
                            {
                                _logger.Log("UtilitiesModuleNavigator.ActivateView() - " + ex.Message, Category.Exception, Priority.High);
                            }
                        }
                    }
                }

                // Don't continue navigation process if a view is already active.
                if (requestedView == null)
                    throw new NullReferenceException("UtilitiesModuleNavigator.ActivateView() - requested view '" + viewKey + "' cannot be found");

                EnsureRootViewRegionExistence();

                try
                {
                    InnerRegion.Activate(requestedView);
                }
                catch (Exception ex)
                {
                    _logger.Log("Utilities -- ActivateView(" + viewKey + ") unable to activate requested view: " + ex.Message + "; Stack Trace from exception: " + ex.StackTrace, Category.Exception, Priority.High);
                    throw;
                }

                CurrentViewKey = viewKey;
                _eventAggregator.GetEvent<ActiveViewTitleChanged>().Publish("Utilities");

            }
            catch (Exception ex)
            {
                _logger.Log("Utilities -- ActivateView(" + viewKey + "): " + ex.Message + "; Stack Trace from exception: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
        }

        public void Initialize()
        {
            //Resolve module shell and resolve=>register default HomeView with a region
            RootViewHost = _container.Resolve<ModuleShell>();
            InnerRegion = _regionManager.Regions[RegionNames.WorkspaceRegion].Add(RootViewHost, String.Empty, true).Regions[ScopedRegionNames.UtilitiesWorkRegion];
            InnerRegion.Add(_container.Resolve<HomeView>(), UtilityModuleViewKeys.HomeView);

            //Lazy view registrar
            _lazyViewRegistrar.RegisterViewWithRegion(ScopedRegionNames.UtilitiesWorkRegion, () => _container.Resolve<GreetingsView>(), UtilityModuleViewKeys.GreetingsView);
            _lazyViewRegistrar.RegisterViewWithRegion(ScopedRegionNames.UtilitiesWorkRegion, () => _container.Resolve<SettingsView>(), UtilityModuleViewKeys.SettingsView);
            _lazyViewRegistrar.RegisterViewWithRegion(ScopedRegionNames.UtilitiesWorkRegion, () => _container.Resolve<DatabaseView>(), UtilityModuleViewKeys.DatabaseView);
            _lazyViewRegistrar.RegisterViewWithRegion(ScopedRegionNames.UtilitiesWorkRegion, () => _container.Resolve<ExportView>(), UtilityModuleViewKeys.ExportView);
            _lazyViewRegistrar.RegisterViewWithRegion(ScopedRegionNames.UtilitiesWorkRegion, () => _container.Resolve<ServiceView>(), UtilityModuleViewKeys.ServiceView);
            _lazyViewRegistrar.RegisterViewWithRegion(ScopedRegionNames.UtilitiesWorkRegion, () => _container.Resolve<OrderingPhysiciansReportView>(), UtilityModuleViewKeys.OrderingPhysiciansReportView);
            _lazyViewRegistrar.RegisterViewWithRegion(ScopedRegionNames.UtilitiesWorkRegion, () => _container.Resolve<ManualBackupView>(), UtilityModuleViewKeys.ManualBackupView);
        }

        #endregion

        #region Helpers

        void EnsureRootViewRegionExistence()
        {
            if (_regionManager == null)
                throw new NotSupportedException("UtilitiesModuleNavigator.EnsureRootViewRegionExistence() - region manager is null");
            if (_regionManager.Regions == null)
                throw new NotSupportedException("UtilitiesModuleNavigator.EnsureRootViewRegionExistence() - Regions collection on the region manager is null");

            var workspaceRegion = _regionManager.Regions[RegionNames.WorkspaceRegion];
            if (workspaceRegion == null)
                throw new NotSupportedException("UtilitiesModuleNavigator.EnsureRootViewRegionExistence() - no region corresponds to WorkspaceRegion");
            if (workspaceRegion.Views == null)
                throw new NotSupportedException("UtilitiesModuleNavigator.EnsureRootViewRegionExistence() - no views available for WorkspaceRegion");
            if (RootViewHost == null)
                throw new NotSupportedException("UtilitiesModuleNavigator.EnsureRootViewRegionExistence() - RootViewHost instance is null");

            // Check if the View was added to the region; if not add and activate
            if (!workspaceRegion.Views.Contains(RootViewHost))
                workspaceRegion.Add(RootViewHost);
            workspaceRegion.Activate(RootViewHost);
        }
        
        #endregion
    }
}
