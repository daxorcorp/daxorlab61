﻿using System;
using System.Globalization;
using CarlosAg.ExcelXmlWriter;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.ReportViewer;

namespace Daxor.Lab.Utility.Services
{
    public static class ValueInserters
    {
        public static IExcelFileBuilder ExcelFileBuilder { get; private set; }
        public static Worksheet Worksheet { get; private set; }
        public static bool IsInitialized { get; private set; }

        public static void Initialize(IExcelFileBuilder excelFileBuilder, Worksheet worksheet)
        {
            if (excelFileBuilder == null)
            {
                IsInitialized = false;
                throw new ArgumentNullException("excelFileBuilder");
            }
            if (worksheet == null)
            {
                IsInitialized = false;
                throw new ArgumentNullException("worksheet");
            }

            ExcelFileBuilder = excelFileBuilder;
            Worksheet = worksheet;

            IsInitialized = true;
        }
        
        #region Value inserters

        public static void InsertRedBloodCellExcessOrDeficit(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = "N/A";
            if (rowConfiguration.Test.Volumes[BloodType.Ideal].RedCellCount != 0)
            {
                var deviation = rowConfiguration.Test.Volumes[BloodType.Measured].RedCellCount -
                                rowConfiguration.Test.Volumes[BloodType.Ideal].RedCellCount;
                var percentDeviation = deviation / (double)rowConfiguration.Test.Volumes[BloodType.Ideal].RedCellCount;
                content = (percentDeviation * 100).RoundAndFormat(1);
                ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
            else
                ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertRedBloodCellDeviation(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            string content;
            if (!rowConfiguration.AreIdealsResultsAvailable())
            {
                content = "N/A";
                ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
            else
            {
                content = (rowConfiguration.Test.Volumes[BloodType.Measured].RedCellCount - rowConfiguration.Test.Volumes[BloodType.Ideal].RedCellCount).ToString(CultureInfo.InvariantCulture);
                ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
        }

        public static void InsertIdealRedBloodCellVolumeInMl(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            string content;
            if (!rowConfiguration.AreIdealsResultsAvailable())
            {
                content = "N/A";
                ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
            else
            {
                content = rowConfiguration.Test.Volumes[BloodType.Ideal].RedCellCount.ToString(CultureInfo.InvariantCulture);
                ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
        }

        public static void InsertRedBloodCellVolumeInMl(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Volumes[BloodType.Measured].RedCellCount.ToString(CultureInfo.InvariantCulture);
            ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertNormalizedHematocrit(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = "N/A";

            if (Double.IsNaN(rowConfiguration.MeasuredCalcEngineResults.NormalizedHematocrit) == false &&
                rowConfiguration.AreIdealsResultsAvailable())
            {
                content = rowConfiguration.MeasuredCalcEngineResults.NormalizedHematocrit.RoundAndFormat(1);
                ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
            else
                ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertStandardDeviation(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = "0";
            if (Double.IsNaN(rowConfiguration.MeasuredCalcEngineResults.StdDev) == false)
                content = (rowConfiguration.MeasuredCalcEngineResults.StdDev * 100).RoundAndFormat(3);

            ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertSlope(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = (rowConfiguration.MeasuredCalcEngineResults.Slope * 100).RoundAndFormat(2);
            ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertPeripheralHematocrit(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = "0";
            if (Double.IsNaN(rowConfiguration.Test.PatientHematocritResult) == false)
                content = rowConfiguration.Test.PatientHematocritResult.RoundAndFormat(1);

            ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertReferringPhysician(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.RefPhysician;
            ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertCcReportTo(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.CcReportTo;
            ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertAnalyst(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Analyst;
            ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertInjectateLot(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.InjectateLot;
            ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertComments(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Comments;
            ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertPlasmaVolumeExcessOrDeficit(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = "N/A";
            if (rowConfiguration.Test.Volumes[BloodType.Ideal].PlasmaCount != 0)
            {
                var plasmaBloodDev = rowConfiguration.Test.Volumes[BloodType.Measured].PlasmaCount -
                                     rowConfiguration.Test.Volumes[BloodType.Ideal].PlasmaCount;
                var deviation = plasmaBloodDev / (double)rowConfiguration.Test.Volumes[BloodType.Ideal].PlasmaCount;
                content = (deviation*100).RoundAndFormat(1);
                ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
            else
                ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);

        }

        public static void InsertPlasmaVolumeDeviation(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            string content;
            if (!rowConfiguration.AreIdealsResultsAvailable())
            {
                content = "N/A";
                ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
            else
            {
                content =
                    (rowConfiguration.Test.Volumes[BloodType.Measured].PlasmaCount - rowConfiguration.Test.Volumes[BloodType.Ideal].PlasmaCount).ToString(
                        CultureInfo.InvariantCulture);
                ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
        }

        public static void InsertIdealPlasmaVolumeInMl(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            string content;
            if (!rowConfiguration.AreIdealsResultsAvailable())
            {
                content = "N/A";
                ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
            else
            {
                content = rowConfiguration.Test.Volumes[BloodType.Ideal].PlasmaCount.ToString(CultureInfo.InvariantCulture);
                ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
        }

        public static void InsertPlasmaVolumeInMl(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Volumes[BloodType.Measured].PlasmaCount.ToString(CultureInfo.InvariantCulture);
            ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertBloodVolumeExcessOrDeficit(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = "N/A";
            if (rowConfiguration.Test.Volumes[BloodType.Ideal].WholeCount != 0)
            {
                var wholeBloodDev = rowConfiguration.Test.Volumes[BloodType.Measured].WholeCount -
                                    rowConfiguration.Test.Volumes[BloodType.Ideal].WholeCount;
                var deviation = wholeBloodDev / (double)rowConfiguration.Test.Volumes[BloodType.Ideal].WholeCount;
                content = (deviation*100).RoundAndFormat(1);
                ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
            else
                ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertBloodVolumeDeviationInMl(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            string content;
            if (!rowConfiguration.AreIdealsResultsAvailable())
            {
                content = "N/A";
                ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
            else
            {
                content =
                    (rowConfiguration.Test.Volumes[BloodType.Measured].WholeCount - rowConfiguration.Test.Volumes[BloodType.Ideal].WholeCount).ToString(
                        CultureInfo.InvariantCulture);
                ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
        }

        public static void InsertIdealBloodVolumeInMl(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            string content;
            if (!rowConfiguration.AreIdealsResultsAvailable())
            {
                content = "N/A";
                ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
            else
            {
                content = rowConfiguration.Test.Volumes[BloodType.Ideal].WholeCount.ToString(CultureInfo.InvariantCulture);
                ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
            }
        }

        public static void InsertBloodVolumeInMl(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Volumes[BloodType.Measured].WholeCount.ToString(CultureInfo.InvariantCulture);
            ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertIdealWeightDeviation(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Patient.DeviationFromIdealWeight.RoundAndFormat(1);
            ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertAmputeeStatus(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Patient.IsAmputee.ToString();
            ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

	    public static void InsertAmputeeIdealsCorrectionFactor(ExcelRowConfiguration rowConfiguration, int column)
	    {
		    EnsureProperArgumentsAndInitializationState(rowConfiguration);

			string content;

			switch (rowConfiguration.Test.AmputeeIdealsCorrectionFactor)
			{
				case 0:
					content = "0%";
					break;
				case -1:
					content = "Unselected";
					break;
				default:
					content = "-" + rowConfiguration.Test.AmputeeIdealsCorrectionFactor + "%";
					break;
			}

			ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
	    }

        public static void InsertPatientWeightInKg(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Patient.WeightInKg.RoundAndFormat(2);
            ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertPatientHeightInCm(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Patient.HeightInCm.RoundAndFormat(2);
            ExcelFileBuilder.InsertNumberCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertPatientGender(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Patient.Gender.ToString();
            ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertTestId2Label(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.TestID2;
            ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertHospitalPatientId(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Patient.HospitalPatientId;
            ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertLocationLabel(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Location;
            ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertDateOfBirth(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            if (rowConfiguration.Test.Patient.DateOfBirth == null)
                ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, String.Empty, CellStyle.None);
            else
                ExcelFileBuilder.InsertDateTimeCell(Worksheet, rowConfiguration.Row, column, rowConfiguration.Test.Patient.DateOfBirth.Value.ToShortDateString(),
                    CellStyle.UsDate);
        }

        public static void InsertAnalzyedOn(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Date.ToString("g");
            ExcelFileBuilder.InsertDateTimeCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.UsDateTime);
        }

        public static void InsertLastName(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Patient.LastName;
            ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertFirstName(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Patient.FirstName;
            ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        public static void InsertMiddleName(ExcelRowConfiguration rowConfiguration, int column)
        {
            EnsureProperArgumentsAndInitializationState(rowConfiguration);

            var content = rowConfiguration.Test.Patient.MiddleName;
            ExcelFileBuilder.InsertStringCell(Worksheet, rowConfiguration.Row, column, content, CellStyle.None);
        }

        #endregion

        private static void EnsureProperArgumentsAndInitializationState(ExcelRowConfiguration rowConfiguration)
        {
            if (rowConfiguration == null) throw new ArgumentNullException("rowConfiguration");
            if (!IsInitialized) throw new InvalidOperationException("ValueInserters has not been initialized"); 
        }
    }
}
