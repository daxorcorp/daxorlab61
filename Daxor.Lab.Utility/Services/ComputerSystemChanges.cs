﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;

namespace Daxor.Lab.Utility.Services
{
    public class ComputerSystemChanges
    {
        private const string FilePathForScreenCalibration = @"C:\Program Files\Elo TouchSystems\EloVa.exe";
        private const string CancelAllJobsCommand = "CancelAllJobs";
        private const string ControlPanelExecutableFileName = "control.exe";
        private const string DateAndTimeControlPanelFileName = "timedate.cpl";
        private const string PrinterManagementClass = "Win32_Printer";
        private readonly IMessageManager _messageManager;

        public ComputerSystemChanges(IMessageManager messageManager)
        {
            _messageManager = messageManager;
        }
        /// <summary>
        /// Starts the Elo Touchscreen calibration program
        /// </summary>
        /// <exception cref="FileNotFoundException">Throws exception if the calibration program could not be found</exception>
        public static void CalibrateTouchScreen()
        {

            if (!File.Exists(FilePathForScreenCalibration))
                throw new FileNotFoundException("Calibration program doesn't exist at " + FilePathForScreenCalibration);

            var calibrationProcess = new Process {StartInfo = {FileName = FilePathForScreenCalibration}};
            calibrationProcess.Start();
        }

        /// <summary>
        /// Iterates through all printers connected to the computer and cancels all running jobs
        /// </summary>
        /// <exception cref="System.ApplicationException">Throws exception if there are no printers connected</exception>
        public void ResetAllConnectedPrinters()
        {
            var printerClass = new ManagementClass(PrinterManagementClass);
            ManagementObjectCollection printers = printerClass.GetInstances();

            if (printers == null)
                throw new ApplicationException(_messageManager.GetMessage(MessageKeys.SysNoPrintersConnected).FormattedMessage);

            printers.AsParallel().OfType<ManagementObject>().ForAll(p => p.InvokeMethod(CancelAllJobsCommand, null));
        }

        /// <summary>
        /// Launches the date and time control from Windows. 
        /// </summary>
        /// <remarks>Consumes exception if thrown will running the date time control</remarks>
        public static void LaunchDateAndTimeControlPanel()
        {
            try
            {
                var timeDateProcess = new Process
                {
                    StartInfo = {FileName = ControlPanelExecutableFileName, Arguments = DateAndTimeControlPanelFileName}
                };

                timeDateProcess.Start();
            }
            catch
            { }
        }
    }
}
