﻿using System;
using System.Collections.Generic;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Utility.Common;

namespace Daxor.Lab.Utility.Services
{
    public interface IExcelReportGenerator
    {
        /// <summary>
        /// Includes ExportedOn and Version attributes
        /// </summary>
        void BuildMetadata();

        /// <summary>
        /// Writes the column headers to the Excel Report
        /// </summary>
        /// <param name="columnHeaders">List of column headers in the order to be written to</param>
        /// <remarks>Must be called before any calls to WritePatientData</remarks>
        void BuildColumnHeaders(List<BvaExcelColumnHeader> columnHeaders);

        /// <summary>
        /// Writes one line of information using the patient data based from the column headers used in the report
        /// </summary>
        /// <param name="bvaTest"></param>
        /// <param name="measuredResults"></param>
        /// <exception cref="InvalidOperationException">Thrown if the column headers have not been set up</exception>
        /// <remarks>WriteColumnHeaders must have been called before using this method</remarks>
        void BuildPatientData(BVATest bvaTest, MeasuredCalcEngineResults measuredResults);

        /// <summary>
        /// Saves the report to the specified location
        /// </summary>
        /// <param name="filePath"></param>
        /// <exception cref="ArgumentNullException">Thrown if filePath is null or whitespace</exception>
        void SaveReport(string filePath);
    }
}