﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Utility.Common;

namespace Daxor.Lab.Utility.Services
{
    public interface IUtilityDataService
    {
        List<OrderingPhysicianDataRecord> GetOrderingPhysiciansBetween(DateTime dateFromDateTime, DateTime dateToDateTime);
    }
}
