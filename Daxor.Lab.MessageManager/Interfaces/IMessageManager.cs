﻿namespace Daxor.Lab.MessageManager.Interfaces
{
    public interface IMessageManager
    {
        Message GetMessage(string messageName);
    }
}
