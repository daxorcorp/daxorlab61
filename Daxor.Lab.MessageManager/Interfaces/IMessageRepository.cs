﻿using System.Collections.Generic;

namespace Daxor.Lab.MessageManager.Interfaces
{
    public interface IMessageRepository
    {
        Dictionary<string, Message> GetMessages();
    }
}
