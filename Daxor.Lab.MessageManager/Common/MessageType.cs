﻿namespace Daxor.Lab.MessageManager.Common
{
    public enum MessageType
    {
        Undefined,
        Alert,
        Error
    }
}
