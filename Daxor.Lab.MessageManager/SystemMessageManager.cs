﻿using System;
using System.Collections.Generic;
using Daxor.Lab.MessageManager.Interfaces;

namespace Daxor.Lab.MessageManager
{
    /// <summary>
    /// Represents an entity that can manage alert texts, error texts, and their corresponding IDs for the system
    /// </summary>
    public class SystemMessageManager : IMessageManager
    {
        #region Fields

        private readonly Dictionary<string, Message> _messageCollection = new Dictionary<string, Message>();
        private static volatile IMessageRepository _messageRepository;

        #endregion

        #region Ctor

        /// <summary>
        /// Creates a new MessageManager instance and gets messages from the repository.
        /// </summary>
        public SystemMessageManager(IMessageRepository messageRepository)
        {
            MessageRepository = messageRepository;

            _messageCollection = MessageRepository.GetMessages();
        }

        #endregion
        
        #region Properties

        /// <summary>
        /// Gets or sets an IMessageRepository instance that this class can use
        /// for obtaining messages.
        /// </summary>
        public static IMessageRepository MessageRepository
        {
            get { return _messageRepository; }
            set { _messageRepository = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the message corresponding to the given ID
        /// </summary>
        /// <exception cref="KeyNotFoundException">If there is no message corresponding to the given ID</exception>
        /// <param name="messageId">Message ID as defined in Daxor.Lab.MessageManager.Common.MessageKeys</param>
        /// <returns>Message instance corresponding to the given ID</returns>
        public Message GetMessage(string messageId)
        {
            Message message;
            if (!_messageCollection.TryGetValue(messageId, out message))
                throw new KeyNotFoundException(String.Format("SystemMessageManager: No such message ID: {0}", messageId));

            return message;
        }

        #endregion
    }
}
