﻿using System;
using System.Text;
using Daxor.Lab.MessageManager.Common;

namespace Daxor.Lab.MessageManager
{
    public class Message
    {
        readonly string _key;
        readonly string _description;
        readonly string _resolution;
        readonly string _messageId;
        readonly MessageType _messageType;

        public Message(string key, string description, string resolution, string messageId, MessageType msgType)
        {
            if (String.IsNullOrEmpty(key))
                throw new ArgumentNullException("key");

            _key = key;
            _description = description;
            _resolution = resolution;
            _messageId = messageId;
            _messageType = msgType;
        }

        public string Key
        {
            get { return _key; }
        }

        public string Description
        {
            get 
            {
                return _description.Replace("\\n", "\n").Replace("\\\"", "\"");
            }
        }

        public string Resolution
        {
            get { return _resolution; }
        }

        public string MessageId
        {
            get { return _messageId; }
        }

        public MessageType MessageType
        {
            get { return _messageType; }
        }

        public virtual string FormattedMessage
        {
            get
            {
                var messageType = string.Empty;
                if (MessageType != MessageType.Undefined)
                    messageType = _messageType == MessageType.Alert ? "Alert: " : "Error: ";

                var formattedMessageToReturn = new StringBuilder();

                formattedMessageToReturn.Append(Description);
                formattedMessageToReturn.Append("\n\n");
                if (!string.IsNullOrWhiteSpace(Resolution))
                {
                    formattedMessageToReturn.Append(Resolution);
                }
                formattedMessageToReturn.Append("\n");
                formattedMessageToReturn.Append("[");
                formattedMessageToReturn.Append(messageType);
                formattedMessageToReturn.Append(MessageId);
                formattedMessageToReturn.Append("]");

                return formattedMessageToReturn.ToString();
            }
        }
    }
}
