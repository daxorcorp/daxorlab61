﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.MessageManager
{
    public class DatabaseMessageRepository : IMessageRepository
    {
        private readonly Dictionary<string, Message> _messageCollection = new Dictionary<string, Message>();
        private readonly ISettingsManager _settingsManager;

        public DatabaseMessageRepository(ISettingsManager settingsManager)
        {
            _settingsManager = settingsManager;
        }

        public Dictionary<string, Message> GetMessages()
        {
            var context = GetDatabaseContext();

            LoadAlertMessages(context);
            LoadErrorMessages(context);

            return _messageCollection;
        }

        private DaxorLabDatabaseLinqDataContext GetDatabaseContext()
        {
            return new DaxorLabDatabaseLinqDataContext(_settingsManager.GetSetting<String>(SettingKeys.SystemLinqConnectionString));
        }

        private void LoadAlertMessages(DaxorLabDatabaseLinqDataContext context)
        {
            IEnumerable<GetAlertDataResult> result = context.GetAlertData();

            foreach (var alertResult in result)
            {
                var alert = new Message(alertResult.ALERT_ID, alertResult.DESCRIPTION,
                    alertResult.RESOLUTION == "None" ? String.Empty : alertResult.RESOLUTION,
                    alertResult.ALERT_ID_NUMBER, Common.MessageType.Alert);
                AddErrorAlert(alert);
            }
        }

        private void LoadErrorMessages(DaxorLabDatabaseLinqDataContext context)
        {
            IEnumerable<GetErrorDataResult> errorResult = context.GetErrorData();

            foreach (var errResult in errorResult)
            {
                var error = new Message(errResult.ALERT_ID, errResult.DESCRIPTION, errResult.RESOLUTION,
                    errResult.ERROR_ID_NUMBER == "None" ? String.Empty : errResult.ERROR_ID_NUMBER,
                    Common.MessageType.Error);
                AddErrorAlert(error);
            }
        }

        private void AddErrorAlert(Message alert)
        {
            if (!_messageCollection.ContainsKey(alert.Key))
                _messageCollection.Add(alert.Key, alert);
        }
    }
}
