﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.SCCore;
using Daxor.Lab.SCContracts;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Reflection;
using System.Threading;
using Daxor.Lab.SCCore.Utilities;

namespace Daxor.Lab.SCService
{
    //A single instance is shared between all the clients, acts as a RESOURCE
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class SampleChangerService : ISampleChangerService
    {
        #region Fields

        private readonly List<ISampleChangerServiceCallbackEvents> _clients = new List<ISampleChangerServiceCallbackEvents>();
        private readonly SMController m_SampleChangerController;

        #endregion

        #region Ctor

        public SampleChangerService()
        {
            try
            {
                m_SampleChangerController = SMController.GetSampleChangerControler();

                //Wiring all the events that we could possible receive from the SampleChangerController object. 
                m_SampleChangerController.ChangerError += OnSCChangerError;
                m_SampleChangerController.PositionChangeBegin += OnSCPositionChangeBegin;
                m_SampleChangerController.PositionChangeEnd += OnSCPositionChangeEnd;
                m_SampleChangerController.PositionSeekBegin += OnSCPositionSeekBegin;
                m_SampleChangerController.PositionSeekCancel += OnSCPositionSeekCancel;
                m_SampleChangerController.PositionSeekEnd += OnSCPositionSeekEnd;
            }
            catch (SampleChangerInitializationException ex)
            {
                OnSCChangerError(ex.ErrorCode, ex.Message);
            }
            catch (Exception ex)
            {
                OnSCChangerError(-1, ex.Message);
            }
        }

        #endregion

        #region Properties
        
        /// <summary>
        /// Get property for SampleChangerController; implements singleton pattern
        /// </summary>
        private SMController SampleChangerController
        {
            get { return m_SampleChangerController; }
        }
       
        #endregion

        #region ISubscriber

        public void Subscribe()
        {
            ISampleChangerServiceCallbackEvents client =
               OperationContext.Current.GetCallbackChannel<ISampleChangerServiceCallbackEvents>();

            lock (_clients)
            {
                if (!_clients.Contains(client))
                {
                    _clients.Add(client);
                    EventLogger.WriteErrorLogEntry("Client connected.\nIP: " + GetClientRemoteAddr() + ".\nTotal clients: " + _clients.Count);
                }
            }
        }
        public void Unsubscribe()
        {
            ISampleChangerServiceCallbackEvents client =
                  OperationContext.Current.GetCallbackChannel<ISampleChangerServiceCallbackEvents>();

            lock (_clients)
            {
                if (_clients.Contains(client))
                {
                    _clients.Remove(client);
                    EventLogger.WriteErrorLogEntry("Client disconnected.\nIP: " + GetClientRemoteAddr() + ".\nTotal clients: " + _clients.Count);
                }
            }
        }

        #endregion

        #region EventHandlers

        private void OnSCPositionSeekEnd(int CurrentPosition)
        {
            //Going through the list of the interested subscribers; notify them
            //of PositionSeekEnd event. If a given subscriber has closed its endpoint
            //without a service request, simple delete it from the subscribers list.
            EventLogger.WriteErrorLogEntry("Callback PositionSeekEnd. CurrentPosition: " + CurrentPosition);
            ThreadPool.QueueUserWorkItem(
             delegate
             {
                 lock (_clients)
                 {
                     List<ISampleChangerServiceCallbackEvents> disconnectedClients = new List<ISampleChangerServiceCallbackEvents>();
                     foreach (var client in _clients)
                     {
                         try
                         {
                             client.OnPositionSeekEndCallbackEvent(CurrentPosition);
                         }
                         catch (Exception)
                         {
                             disconnectedClients.Add(client);
                         }
                     }

                     //delete disconnected subscribers
                     foreach (var deadClient in disconnectedClients)
                         _clients.Remove(deadClient);
                 }
             });
        }
        private void OnSCPositionSeekCancel(int CurrentPosition, int SeekingPosition)
        {
            //Going through the list of the interested subscribers; notify them
            //of PositionSeekCancel event. If a given subscriber has closed its endpoint
            //without a service request, simple delete it from the subscribers list.
            EventLogger.WriteErrorLogEntry("Callback PositionSeekCancel. CurrentPosition: " + CurrentPosition + ". SeekingPosition: " + SeekingPosition);
            ThreadPool.QueueUserWorkItem(
            delegate
            {
                lock (_clients)
                {
                    List<ISampleChangerServiceCallbackEvents> disconnectedClients = new List<ISampleChangerServiceCallbackEvents>();
                    foreach (var client in _clients)
                    {
                        try
                        {
                            client.OnPositionSeekCancelCallbackEvent(CurrentPosition, SeekingPosition);
                        }
                        catch (Exception)
                        {
                            disconnectedClients.Add(client);
                        }
                    }

                    //delete disconnected subscribers
                    foreach (var deadClient in disconnectedClients)
                        _clients.Remove(deadClient);
                }
            });
        }
        private void OnSCPositionSeekBegin(int CurrentPosition, int SeekingPosition)
        {
            EventLogger.WriteErrorLogEntry("Callback PositionSeekBegin. CurrentPosition: " + CurrentPosition + ". SeekingPosition: " + SeekingPosition);
            ThreadPool.QueueUserWorkItem(
                delegate
                {
                    lock (_clients)
                    {
                        List<ISampleChangerServiceCallbackEvents> disconnectedClients = new List<ISampleChangerServiceCallbackEvents>();
                        foreach (var client in _clients)
                        {
                            try
                            {
                                client.OnPositionSeekBeginCallbackEvent(CurrentPosition, SeekingPosition);
                            }
                            catch (Exception)
                            {
                                disconnectedClients.Add(client);
                            }
                        }

                        //delete disconnected subscribers
                        foreach (var deadClient in disconnectedClients)
                            _clients.Remove(deadClient);
                    }
                });
        }
        private void OnSCPositionChangeEnd(int NewPosition)
        {
            //Going through the list of the interested subscribers; notify them
            //of PositionChangeEnd event. If a given subscriber has closed its endpoint
            //without a service request, simple delete it from the subscribers list.
            EventLogger.WriteErrorLogEntry("Callback PositionChangeEnd. NewPosition: " + NewPosition);
            ThreadPool.QueueUserWorkItem(
                delegate
                {
                    lock (_clients)
                    {
                        List<ISampleChangerServiceCallbackEvents> disconnectedClients = new List<ISampleChangerServiceCallbackEvents>();
                        foreach (var client in _clients)
                        {
                            try
                            {
                                client.OnPositionChangeEndCallbackEvent(NewPosition);
                            }
                            catch (Exception)
                            {
                                disconnectedClients.Add(client);
                            }
                        }

                        //delete disconnected subscribers
                        foreach (var deadClient in disconnectedClients)
                            _clients.Remove(deadClient);
                    }
                });
        }
        private void OnSCPositionChangeBegin(int OldPosition, int NewPosition)
        {
            //Going through the list of the interested subscribers; notify them
            //of PositionChangeBegin event. If a given subscriber has closed its endpoint
            //without a service request, simple delete it from the subscribers list.
            EventLogger.WriteErrorLogEntry("Callback PositionChangeBegin. OldPosition: " + OldPosition + ".\nNewPosition: " + NewPosition);
            ThreadPool.QueueUserWorkItem(
                delegate
                {
                    lock (_clients)
                    {
                        List<ISampleChangerServiceCallbackEvents> disconnectedClients = new List<ISampleChangerServiceCallbackEvents>();
                        foreach (var client in _clients)
                        {
                            try
                            {
                                client.OnPositionChangeBeginCallbackEvent(OldPosition, NewPosition);
                            }
                            catch (Exception)
                            {
                                disconnectedClients.Add(client);
                            }
                        }

                        //delete disconnected subscribers
                        foreach (var deadClient in disconnectedClients)
                        {
                            _clients.Remove(deadClient);
                        }
                    }
                });
        }
        private void OnSCChangerError(int ErrorCode, string Message)
        {
            //Going through the list of the interested subscribers; notify them
            //of PositionChangerError event. If a given subscriber has closed its endpoint
            //without a service request, simple delete it from the subscribers list.
            EventLogger.WriteErrorLogEntry("Callback ChangerError. ErrorCode: " + ErrorCode + ".\nMessage: " + Message);
            ThreadPool.QueueUserWorkItem(
                delegate
                {
                    lock (_clients)
                    {
                        List<ISampleChangerServiceCallbackEvents> disconnectedClients = new List<ISampleChangerServiceCallbackEvents>();
                        foreach (var client in _clients)
                        {
                            try
                            {
                                client.OnChangeErrorCallbackEvent(ErrorCode, Message);
                            }
                            catch (Exception)
                            {
                                disconnectedClients.Add(client);
                            }
                        }

                        //delete disconnected subscribers
                        foreach (var deadClient in disconnectedClients)
                            _clients.Remove(deadClient);
                    }
                });
        }

        #endregion

        #region Public Methods

        public SensorData GetRawSensorData()
        {
            return SensorData.ConvertFrom(SampleChangerController.ReadRawSensorData());
        }

        public void StopSampleChanger()
        {
            SampleChangerController.Position = -1;
        }

        public void PulseSampleChanger()
        {
            SampleChangerController.PulseSampleChangerMotor();
        }

        public void MoveToPosition(int goalPosition)
        {
            EventLogger.WriteErrorLogEntry("Client requested MoveToPosition: " + goalPosition + ".\n" + "Client IP: " + GetClientRemoteAddr());
            SampleChangerController.Position = goalPosition;
        }

        public void SetSleepTime(TimeSpan tSpan)
        {
            SampleChangerController.SleepTime = tSpan;
        }
        
        public void Terminate()
        {
            if (SampleChangerController != null)
                SampleChangerController.Closed = true;
        }

        public bool IsMoving()
        {
            return SampleChangerController.IsMoving;
        }

        public bool IsOpened()
        {
            return !SampleChangerController.Closed;
        }

        public int GetCurrentPosition()
        {
            return SampleChangerController.Position;
        }

        public TimeSpan GetSleepTime()
        {
            return SampleChangerController.SleepTime;
        }

        public DateTime GetTimeLastPositionAttained()
        {
            return SampleChangerController.LastPositionAttained;
        }

        public void SetTestDelay(double tDelay)
        {
            SampleChangerController.TestDelay = tDelay;
        }

        public double GetTestDelay()
        {
            return SampleChangerController.TestDelay;
        }


        public string GetVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        public bool IsPoweredUp()
        {
            //If any bit is true, then we were able to read the position, which we can only do if the power is supplied.
            var bits = SampleChangerController.ReadRawSensorData();
            for (int i = 0; i < bits.Length; i++)
            {
                if (bits[i])
                    return true;
            }
            return false;
        }

        #endregion

        #region Helpers

        string GetClientRemoteAddr()
        {
            //Remote Endpoint Properties
            RemoteEndpointMessageProperty prop = (RemoteEndpointMessageProperty)OperationContext.Current.IncomingMessageProperties[RemoteEndpointMessageProperty.Name];

            return prop.Address;
        }

        #endregion
    }
}
