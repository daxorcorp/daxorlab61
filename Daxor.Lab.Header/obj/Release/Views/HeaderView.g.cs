﻿#pragma checksum "..\..\..\Views\HeaderView.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "930784D36BFE5EA23A9954CFD308767CF3BCD0EC"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Daxor.Lab.Infrastructure.Commands;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Controls;
using Daxor.Lab.Infrastructure.Converters;
using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.RuleFramework;
using Daxor.Lab.Infrastructure.TouchClick;
using Daxor.Lab.Infrastructure.UserControls;
using Daxor.Lab.Infrastructure.Utilities;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Daxor.Lab.Header.Views {
    
    
    /// <summary>
    /// HeaderView
    /// </summary>
    public partial class HeaderView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 22 "..\..\..\Views\HeaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.VisualStateGroup MainMenuDropDownVisualStateGroup;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\Views\HeaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.VisualState MainMenuDeactivated;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\Views\HeaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.VisualState MainMenuActivated;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\..\Views\HeaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ShutdownButton;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\Views\HeaderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox uiMainMenu;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Daxor.Lab.Header;component/views/headerview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\HeaderView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.MainMenuDropDownVisualStateGroup = ((System.Windows.VisualStateGroup)(target));
            return;
            case 2:
            this.MainMenuDeactivated = ((System.Windows.VisualState)(target));
            return;
            case 3:
            this.MainMenuActivated = ((System.Windows.VisualState)(target));
            return;
            case 4:
            this.ShutdownButton = ((System.Windows.Controls.Button)(target));
            return;
            case 5:
            this.uiMainMenu = ((System.Windows.Controls.ComboBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

