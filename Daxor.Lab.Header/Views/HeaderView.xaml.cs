﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Daxor.Lab.Header.ViewModels;

namespace Daxor.Lab.Header.Views
{
    /// <summary>
    /// Interaction logic for HeaderView.xaml
    /// </summary>
    public partial class HeaderView : UserControl
    {
        private HeaderViewModel _vm;

        public HeaderView()
        {
            InitializeComponent();
            uiMainMenu.SelectionChanged += uiMainMenu_SelectionChanged;
            VisualStateManager.GoToState(this, "MainMenuDeactivated", false);
        }
        public HeaderView(HeaderViewModel vm)
            : this()
        {
            _vm = vm;
            DataContext = _vm;
            _vm.PropertyChanged += vm_PropertyChanged;
        }

        void vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsMainMenuActivated")
            {
                if (_vm.IsMainMenuActivated)
                {

                    VisualStateManager.GoToState(this, "MainMenuActivated", true);
                }
                else
                {
                    VisualStateManager.GoToState(this, "MainMenuDeactivated", true);
                }
            }
        }

        void uiMainMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            uiMainMenu.SelectionChanged -= uiMainMenu_SelectionChanged;
            uiMainMenu.SelectedIndex = -1;
            uiMainMenu.SelectionChanged += uiMainMenu_SelectionChanged;
        }
    }
}
