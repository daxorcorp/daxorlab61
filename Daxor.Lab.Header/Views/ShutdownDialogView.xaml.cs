﻿using System.Windows.Controls;
using Daxor.Lab.Header.ViewModels;
using System.Windows.Media.Animation;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.Header.Views
{
    /// <summary>
    /// Interaction logic for ShutdownDialogInterface.xaml
    /// </summary>
    public partial class ShutdownDialogView : UserControl
    {
        private readonly Storyboard _showPasswordBox;
        private readonly Storyboard _hidePasswordBox;
        private readonly ShutdownDialogViewModel _vm;
        private readonly ISettingsManager _settingsManager;

        public ShutdownDialogView()
        {
            InitializeComponent();

            _showPasswordBox = FindResource("ShowPassword") as Storyboard;
            _hidePasswordBox = FindResource("HidePassword") as Storyboard;
        }

        public ShutdownDialogView(ShutdownDialogViewModel vm, ISettingsManager settingsManager)
            : this()
        {
            _vm = vm;
            DataContext = vm;
            _settingsManager = settingsManager;
        }

        private void uiShutDownRadioButton_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            if (_showPasswordBox != null)
            {
                _showPasswordBox.Begin();
                uiPasswordBox.Focus();

#if DEBUG
                uiPasswordBox.Password = _settingsManager.GetSetting<string>(SettingKeys.SystemPasswordService);
#endif
            }
        }

        private void uiShutDownRadioButton_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            if (_hidePasswordBox != null)
                _hidePasswordBox.Begin();
        }

        private void PasswordBox_PasswordChanged(object sender, System.Windows.RoutedEventArgs e)
        {

            _vm.PasswordToShutDown = uiPasswordBox.Password;
        }
    }
}
