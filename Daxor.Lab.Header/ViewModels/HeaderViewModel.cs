﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Printing;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Header.Views;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Commands;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Services;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.Header.ViewModels
{
    /// <summary>
    /// ViewModel for the HeaderView.
    /// </summary>
    public class HeaderViewModel : ViewModelBase
    {
         private static readonly object Locker = new object();

        #region Fields

        private readonly ObservableCollection<IAppModuleInfo> _sortedMenuItems = new ObservableCollection<IAppModuleInfo>();

        private readonly IEventAggregator _eventAggregator;
        private readonly IAppModuleInfoCatalog _appModuleCatalog;
        private readonly ILoggerFacade _customLoggerFacade;
        private readonly ISettingsManager _settingsManager;
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly IUnityContainer _container;
        private readonly ITestExecutionController _testExecutionService;
        private readonly IMessageManager _messageManager;

        private string _activeViewTitle = "Daxor.Labs";
        private bool _isPrivacyActivated = true;
        private bool _isMainMenuActivated;
        private bool _isMainMenuOpen;
        private bool _isUnitInDemoMode;
        private int _numOfPagesPrinting;

        private IAppModuleInfo _selectedAppModuleInfo;

        #endregion

        #region Ctor

        public HeaderViewModel(IEventAggregator eventAggregator, IAppModuleInfoCatalog appModuleCatalog, 
                               ILoggerFacade customLoggerFacade, IUnityContainer container, 
                               IMessageBoxDispatcher messageBoxDispatcher, ITestExecutionController testExecutionController, 
                               ISettingsManager settingsManager, IMessageManager messageManager)
        {
            _eventAggregator = eventAggregator;
            _appModuleCatalog = appModuleCatalog;
            _customLoggerFacade = customLoggerFacade;
            _container = container;
            _settingsManager = settingsManager;
            _messageBoxDispatcher = messageBoxDispatcher;
            _testExecutionService = testExecutionController;
            _messageManager = messageManager;
            
            _appModuleCatalog.Items.CollectionChanged += AppModuleInfoCatalog_CollectionChanged;

            AddExistingMenuItems();
            SubscribeToAggregateEvents();
            IsUnitInDemoMode = _settingsManager.GetSetting<bool>(SettingKeys.SystemDetectorDemoModeEnabled) ||
                _settingsManager.GetSetting<bool>(SettingKeys.SystemSampleChangerDemoModeEnabled);

            //Registers global commands
            GlobalCommands.OpenPrivacyScreenCommand.RegisterCommand(ShowPrivacyViewCommand);
        }

        #endregion

        #region Properties

        public string ActiveViewTitle
        {
            get { return _activeViewTitle; }
            set
            {
                if (_activeViewTitle == value)
                    return;

                _activeViewTitle = value;
                FirePropertyChanged("ActiveViewTitle");
            }
        }

        public bool IsPrivacyActivated
        {
            get { return _isPrivacyActivated; }
            set
            {
                if (_isPrivacyActivated == value)
                    return;

                _isPrivacyActivated = value;
                FirePropertyChanged("IsPrivacyActivated");
            }
        }
        public bool IsMainMenuActivated
        {
            get { return _isMainMenuActivated; }
            set
            {
                if (_isMainMenuActivated == value)
                    return;

                _isMainMenuActivated = value;
                FirePropertyChanged("IsMainMenuActivated");
            }
        }
        public bool IsMainMenuOpen
        {
            get { return _isMainMenuOpen; }
            set
            {
                _isMainMenuOpen = value;
                FirePropertyChanged("IsMainMenuOpen");
            }
        }
        public bool IsUnitInDemoMode
        {
            get { return _isUnitInDemoMode; }
            set
            {
                _isUnitInDemoMode = value;
                FirePropertyChanged("IsUnitInDemoMode");
            }
        }

        public int NumOfPagesPrinting
        {
            get { return _numOfPagesPrinting; }
            set
            {
                lock (Locker)
                {
                    base.SetValue(ref _numOfPagesPrinting, "NumOfPagesPrinting", value);
                }
            }
        }

        public IAppModuleInfo SelectedAppModuleInfo
        {
            get { return _selectedAppModuleInfo; }
            set
            {
                _selectedAppModuleInfo = value;
                if (_selectedAppModuleInfo != null)
                {
                    if (_selectedAppModuleInfo.OpenAppModuleCommand.CanExecute(null))
                        _selectedAppModuleInfo.OpenAppModuleCommand.Execute(DisplayViewOption.Default);
                }
                FirePropertyChanged("SelectedAppModuleInfo");
            }
        }
        public ObservableCollection<IAppModuleInfo> AppModuleInfos
        {
            get { return _sortedMenuItems; }
        }

        #endregion

        #region Helpers

        private void AppModuleInfoCatalog_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            //New item has been added
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (IAppModuleInfo appInfo in e.NewItems)
                {
                    _sortedMenuItems.Add(appInfo);
                }

                SortMenuItems();
            }

            //An item has been removed
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (IAppModuleInfo appModule in e.OldItems)
                {
                    _sortedMenuItems.Remove(appModule);
                }
            }
        }

        private void SortMenuItems()
        {
            IAppModuleInfo menuModuleInfo = null;

            //Sort menu items, last should be MainMenu
            var tempList = (from appModule in _sortedMenuItems
                            orderby appModule.DisplayOrder
                            select appModule).ToList();

            _sortedMenuItems.Clear();
            //Adds to the list
            tempList.ForEach(appModule =>
            {
                if (appModule.AppModuleKey != AppModuleIds.MainMenu)
                    _sortedMenuItems.Add(appModule);
                else
                    menuModuleInfo = appModule;
            });

            //Lastly add mainMenuItem if exists
            if (menuModuleInfo != null)
                _sortedMenuItems.Add(menuModuleInfo);

        }

        private void AddExistingMenuItems()
        {
            if (_appModuleCatalog.Items.Count > 0 && _sortedMenuItems.Count == 0)
            {
                foreach (IAppModuleInfo appModule in _appModuleCatalog.Items)
                {
                    _sortedMenuItems.Add(appModule);
                }

                SortMenuItems();
            }
        }

        private void SubscribeToAggregateEvents()
        {
            //Updates title base on new visible view
            _eventAggregator.GetEvent<ActiveViewTitleChanged>().Subscribe(title => ActiveViewTitle = title, ThreadOption.UIThread, false);
            
            //Activates and deactivates application part
            _eventAggregator.GetEvent<AppPartActivationChanged>().Subscribe(
                aPayload =>
                {
                    switch (aPayload.AppPartName)
                    {
                        case ApplicationParts.MainMenuDropDown: IsMainMenuActivated = true; break;
                        case ApplicationParts.ShutdownButton: IsMainMenuActivated = false; break;
                    }

                },
                ThreadOption.UIThread,
                false);
        }

        #endregion

        #region Commands

        private DelegateCommand<object> _shutdownCmd;
        private DelegateCommand<object> _printScreenCmd;
        private DelegateCommand<object> _showPrvViewCmd;

        public DelegateCommand<object> ShutdownCommand
        {
            get { return _shutdownCmd ?? (_shutdownCmd = new DelegateCommand<object>(OnShutdown)); }
        }
        private void OnShutdown(object arg)
        {
            _customLoggerFacade.Log("***************SHUTDOWN BUTTON CLICKED", Category.Info, Priority.None);

            FrameworkElement uiDialog = _container.Resolve<ShutdownDialogView>();
            

            var choices = new[] { "Proceed", "Cancel" };
            var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information, uiDialog, "Shutdown System", choices);
            var vm = uiDialog.DataContext as ShutdownDialogViewModel;
            if (_testExecutionService.IsExecuting && choices[result] != "Cancel")
            {
                var shutdownAction = "none";
                var shutdownMessage = "none";
                if (vm != null)
                    switch (vm.SelectedAction)
                    {
                        case ShutdownDialogActions.ShutdownSystem:
                            shutdownAction = "Shutdown System";
                            shutdownMessage = "Shutting the system down";
                            break;
                        case ShutdownDialogActions.RebootSystem:
                            shutdownAction = "Reboot System";
                            shutdownMessage = "Rebooting the system";
                            break;
                        case ShutdownDialogActions.CloseApplication:
                            shutdownAction = "Exit Application";
                            shutdownMessage = "Exiting the application";
                            break;
                    }
                var continueChoices = new[] { shutdownAction, "Cancel" };
                var errorMessage = _messageManager.GetMessage(MessageKeys.SysTestIsExecuting).FormattedMessage;
                var choice = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning, String.Format(errorMessage, shutdownMessage), "Shutdown System", continueChoices);
                _customLoggerFacade.Log(String.Format(errorMessage, shutdownMessage), Category.Info, Priority.Low);
                if (continueChoices[choice] == "Cancel")
                    result = 1;
            }

            if (choices[result] != "Proceed") return;
            
            _eventAggregator.GetEvent<GlobalShutdownInitiated>().Publish(null);

            if (vm == null) return;

            switch (vm.SelectedAction)
            {
                case ShutdownDialogActions.ShutdownSystem:

                    _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true) { Message = "Shutting down..." });
                    ShutdownAndReboot.ShutdownWindows();
                    break;

                case ShutdownDialogActions.RebootSystem:
                    _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true) { Message = "Restarting..." });
                    ShutdownAndReboot.RebootWindows();
                    break;
                case ShutdownDialogActions.CloseApplication:
                    var servicePassword = _settingsManager.GetSetting<String>(SettingKeys.SystemPasswordService);
                    if (!String.IsNullOrEmpty(vm.PasswordToShutDown) && vm.PasswordToShutDown.Equals(servicePassword))
                    {
                        _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true) { Message = "Closing application..." });
                        Application.Current.MainWindow.Close();
                    }
                    else
                    {
                        string message = _messageManager.GetMessage(MessageKeys.SysShutdownPasswordInvalid).FormattedMessage;
                        _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, message, "Invalid Password", MessageBoxChoiceSet.Close);
                        _customLoggerFacade.Log(message, Category.Info, Priority.Low);
                    }
                    break;
            }
        }
        

        private DelegateCommand<object> ShowPrivacyViewCommand
        {
            get { return _showPrvViewCmd ?? (_showPrvViewCmd = new DelegateCommand<object>(OnShowPrivacyView)); }
        }

        private void OnShowPrivacyView(object arg)
        {
            _customLoggerFacade.Log("SHOW PRIVACY BUTTON CLICKED", Category.Info, Priority.None);

            _eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(ApplicationParts.Privacy, true));
        }

        public DelegateCommand<object> PrintScreenCommand
        {
            get { return _printScreenCmd ?? (_printScreenCmd = new DelegateCommand<object>(ExecutePrintScreenCommand)); }
        }

        private void ExecutePrintScreenCommand(object arg)
        {
            _customLoggerFacade.Log("PRINT SCREEN BUTTON CLICKED", Category.Info, Priority.None);

            var win = Application.Current.MainWindow;
            var winWidth = (int)win.Width;
            var winHeight = (int)win.Height;

            var bmp = new RenderTargetBitmap(winWidth, winHeight, 96, 96, PixelFormats.Pbgra32);
            bmp.Render(win);

            var bmpHeight = bmp.PixelHeight;
            var bmpWidth = bmp.PixelWidth;
            var stride = bmpWidth * ((bmp.Format.BitsPerPixel + 7) / 8);
            var bits = new byte[bmpHeight * stride];
            bmp.CopyPixels(bits, stride, 0);


            var bInfo = new BitmapInfo
            {
                BitsPerPixel = bmp.Format.BitsPerPixel,
                Stride = stride,
                Height = bmpHeight,
                Width = bmpWidth,
                Bits = bits
            };

            var cThread = new Thread(SetupBitmapForPrinting);
            cThread.SetApartmentState(ApartmentState.STA);
            cThread.IsBackground = true;
            cThread.Start(bInfo);
        }

        #endregion

        #region PrintingMethods 

        private void SetupBitmapForPrinting(object parameter)
        {
            try
            {
                var bInfo = parameter as BitmapInfo;
                if (bInfo != null)
                    SnapScrenShotWorkerThread(bInfo);
            }
            catch (Exception ex)
            {

                _customLoggerFacade.Log("PRINT SCREEN HAS FAILED: Exception: " + ex.Message, Category.Exception, Priority.High);
            }
        }

        internal class BitmapInfo
        {
            public int Stride { get; set; }
            public int Height { get; set; }
            public int Width { get; set; }
            public int BitsPerPixel { get; set; }
            public byte[] Bits { get; set; }
        }

        private void SnapScrenShotWorkerThread(object arg)
        {
            //Set SPX doc, content and dixed page
            var fixedDoc = new FixedDocument();
            var pageContent = new PageContent();
            var fixedPage = new FixedPage();


            var image = new Image();
            var bInfo = (BitmapInfo)arg;
            image.Source = BitmapSource.Create(bInfo.Width, bInfo.Height,
                96, 96, PixelFormats.Pbgra32, null, bInfo.Bits, bInfo.Stride);

            var dialog = new PrintDialog();
            var outputSize = new Size(dialog.PrintableAreaWidth, dialog.PrintableAreaHeight);

            image.Measure(outputSize);
            image.Arrange(new Rect(outputSize));
            image.UpdateLayout();

            var scaleX = 0.95;
            var scaleY = 0.95;
            if (dialog.PrintableAreaWidth < image.ActualWidth) scaleX = dialog.PrintableAreaWidth / image.ActualWidth;
            if (dialog.PrintableAreaHeight < image.ActualHeight) scaleY = Math.Floor(dialog.PrintableAreaHeight / image.ActualHeight);

            image.RenderTransform = new ScaleTransform(scaleX, scaleY);
            image.Margin = new Thickness(35, 0, 0, 0);
            image.HorizontalAlignment = HorizontalAlignment.Center;
            image.UpdateLayout();

            var gd = new Grid();
            var headingOne = new TextBlock
            {
                HorizontalAlignment = HorizontalAlignment.Left,
                Text = "Daxor.Lab",
                Margin = new Thickness(5, 0, 0, 0),
                FontSize = 20,
                FontWeight = FontWeights.Bold
            };

            var headingTwo = new TextBlock
            {
                Margin = new Thickness(0, 0, 20, 0),
                HorizontalAlignment = HorizontalAlignment.Right,
                Text = DateTime.Now.ToString("MM-dd-yy - H:mm:ss"),
                FontSize = 12
            };

            gd.Children.Add(headingOne);
            gd.Children.Add(headingTwo);

            //Create Heading text box
            var bd = new Border
            {
                BorderThickness = new Thickness(0, 0, 0, 1),
                Width = dialog.PrintableAreaWidth,
                BorderBrush = Brushes.Black,
                VerticalAlignment = VerticalAlignment.Top,
                Child = gd
            };

            FixedPage.SetTop(bd, 20);
            fixedPage.Children.Add(bd);
            FixedPage.SetTop(image, 100);
            fixedPage.Children.Add(image);

            ((IAddChild)pageContent).AddChild(fixedPage);
            fixedDoc.Pages.Add(pageContent);


            NumOfPagesPrinting++;

            //Prints to the screen
           dialog.PrintVisual(image, "Daxor.Lab ScreenCapture");

        }

        /// <summary> 
        /// Perform a Print Preview on GridView source 
        /// </summary> 
        /// <param name="source">Input GridView</param> 
        /// <param name="orientation">Page Orientation (i.e. Portrait vs. Landscape)</param>
        public void PrintPreview(FrameworkElement source, PageOrientation orientation = PageOrientation.Landscape)
        {
            var window = new Window {Title = "Print Preview"};
            if (!string.IsNullOrWhiteSpace(source.ToolTip as string)) window.Title += " of " + source.ToolTip;
            window.Width = SystemParameters.PrimaryScreenWidth * 0.92;
            window.Height = SystemParameters.WorkArea.Height;


            var viewer = new DocumentViewer
            {
                Document = ToFixedDocument(Application.Current.MainWindow, new PrintDialog(), orientation)
            };
            window.Content = viewer;

            window.ShowDialog();
        }

        private static FixedDocument ToFixedDocument(FrameworkElement element, PrintDialog dialog, PageOrientation orientation = PageOrientation.Portrait)
        {
            dialog.PrintTicket.PageOrientation = orientation;
            var capabilities = dialog.PrintQueue.GetPrintCapabilities(dialog.PrintTicket);
            var pageSize = new Size(dialog.PrintableAreaWidth, dialog.PrintableAreaHeight);
            // ReSharper disable once PossibleNullReferenceException
            var extentSize = new Size(capabilities.PageImageableArea.ExtentWidth, capabilities.PageImageableArea.ExtentHeight);
            var fixedDocument = new FixedDocument();

            element.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            element.Arrange(new Rect(new Point(0, 0), element.DesiredSize));

            for (double y = 0; y < element.DesiredSize.Height; y += extentSize.Height)
            {
                for (double x = 0; x < element.DesiredSize.Width; x += extentSize.Width)
                {
                    var brush = new VisualBrush(element)
                    {
                        Stretch = Stretch.None,
                        AlignmentX = AlignmentX.Left,
                        AlignmentY = AlignmentY.Top,
                        ViewboxUnits = BrushMappingMode.Absolute,
                        TileMode = TileMode.None,
                        Viewbox = new Rect(x, y, extentSize.Width, extentSize.Height)
                    };

                    var pageContent = new PageContent();
                    var page = new FixedPage();
                    ((IAddChild)pageContent).AddChild(page);

                    fixedDocument.Pages.Add(pageContent);
                    page.Width = pageSize.Width;
                    page.Height = pageSize.Height;

                    var canvas = new Canvas();
                    FixedPage.SetLeft(canvas, capabilities.PageImageableArea.OriginWidth);
                    FixedPage.SetTop(canvas, capabilities.PageImageableArea.OriginHeight);
                    canvas.Width = extentSize.Width;
                    canvas.Height = extentSize.Height;
                    canvas.Background = brush;

                    page.Children.Add(canvas);
                }
            }
            return fixedDocument;
        }

        #endregion
    }
}
