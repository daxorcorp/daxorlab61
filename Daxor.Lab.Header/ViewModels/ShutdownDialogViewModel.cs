﻿using System;
using Daxor.Lab.Infrastructure.Entities;

namespace Daxor.Lab.Header.ViewModels
{
    public class ShutdownDialogViewModel
    {
        public ShutdownDialogActions SelectedAction { get; set; }
        public ShutdownDialogViewModel()
        {
            SelectedAction = ShutdownDialogActions.RebootSystem;
#if DEBUG
            SelectedAction = ShutdownDialogActions.CloseApplication;
#endif
        }

        public String PasswordToShutDown { get; set; }
    }
}
