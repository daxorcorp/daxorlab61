﻿using Daxor.Lab.Header.Views;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Exceptions;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.Header
{
    public class Module : IModule
    {
	    private const string ModuleName = "Header";

        private readonly IRegionManager _regionManager;
        private readonly IUnityContainer _container;

        public Module(IRegionManager regionManager, IUnityContainer container)
        {
            _regionManager = regionManager;
            _container = container;
        }

        public void Initialize()
        {
	        HeaderView view;
	        try
	        {
		        view = _container.Resolve<HeaderView>();
	        }
	        catch
	        {
		        throw new ModuleLoadException(ModuleName, "resolve the Header view");
	        }

	        _regionManager.Regions[RegionNames.HeaderRegion].Add(view);
            _regionManager.Regions[RegionNames.HeaderRegion].Activate(view);
        }
    }
}
