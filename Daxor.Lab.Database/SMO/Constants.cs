﻿namespace Daxor.Lab.Database.SMO
{
    public static class Constants
    {
        public static string UserName = "DaxorDBO";
        public static string DatabaseServer = "(local)";
        public static string DaxorDatabase = "DaxorLab";
        public static string QcDatabase = "QCDatabaseArchive";
    }
}
