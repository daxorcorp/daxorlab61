﻿using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities.Base;
using System;

namespace Daxor.Lab.Database.CommonBLLs
{
    public class BVATestInfoDTO : ObservableObject
    {
        #region Fields

        private Guid _testId;
        private DateTime _createdDate;
        private DateTime? _patientDOB;
        private string _testId2 = String.Empty;
        private string _patientHospitalId = String.Empty;
        private Guid _patientPid;
        private string _patientFullName = String.Empty;
        private TestStatus _testStatus = TestStatus.Pending;
        private string _testModeDescription;
        private bool _hasSufficientData;

        #endregion

        public Guid TestID
        {
            get { return _testId; }
            set { base.SetValue(ref _testId, "TestID", value);}
        }
        public Guid PatientPID
        {
            get { return _patientPid; }
            set { base.SetValue(ref _patientPid, "PatientPID", value);}
        }
        public DateTime CreatedDate
        {
            get { return _createdDate; }
            set { base.SetValue(ref _createdDate, "CreatedDate", value);}
        }
        public DateTime? PatientDOB
        {
            get { return _patientDOB; }
            set { base.SetValue(ref _patientDOB, "PatientDOB", value); }
        }
        public string PatientFullName
        {
            get { return _patientFullName; }
            set { base.SetValue(ref _patientFullName, "PatientFullName", value); }
        }
        public string TestID2
        {
            get { return _testId2; }
            set { base.SetValue(ref _testId2, "TestID2", value); }
        }
        public string PatientHospitalID
        {
            get { return _patientHospitalId; }
            set { base.SetValue(ref _patientHospitalId, "PatientHospitalID", value); }
        }
        public TestStatus Status 
        {
            get { return _testStatus; }
            set
            {
                if (base.SetValue(ref _testStatus, "Status", value))
                    FirePropertyChanged("TestStatusDescription");
            }
        }
        public string TestModeDescription
        {
            get { return _testModeDescription; }
            set 
            {
                if (base.SetValue(ref _testModeDescription, "TestModeDescription", value))
                    FirePropertyChanged("TestStatusDescription");
            }
        }
        public string TestStatusDescription
        {
            get
            {
                return GetTestStatusDescription(Status, TestModeDescription, HasSufficientData); 
            }
        }
        public bool HasSufficientData
        {
            get { return _hasSufficientData; }
            set
            {
                if (base.SetValue(ref _hasSufficientData, "HasSufficientData", value))
                    FirePropertyChanged("TestStatusDescription");
            }
        }

        #region Ctor

        public BVATestInfoDTO() { }
        public BVATestInfoDTO(GetBvaTestListResult record)
        {
            CreatedDate = record.TEST_DATE;
            PatientDOB = record.DOB;
            PatientFullName = RenderFullName(record.LAST_NAME, record.FIRST_NAME, record.MIDDLE_NAME);
            PatientHospitalID = record.PATIENT_HOSPITAL_ID;
            PatientPID = record.PID;
            TestID = record.TEST_ID;
            TestID2 = record.ID2;
            TestModeDescription = record.TEST_MODE_DESCRIPTION;
            Status = (TestStatus)record.STATUS_ID;
            HasSufficientData = record.HAS_SUFFICIENT_DATA == 'T' ? true : false;
        }

        public BVATestInfoDTO(GetBvaTestListItemResult record)
        {
            CreatedDate = record.TEST_DATE;
            PatientDOB = record.DOB;
            PatientFullName = RenderFullName(record.LAST_NAME, record.FIRST_NAME, record.MIDDLE_NAME);
            PatientHospitalID = record.PATIENT_HOSPITAL_ID;
            PatientPID = record.PID;
            TestID = record.TEST_ID;
            TestID2 = record.ID2;
            TestModeDescription = record.TEST_MODE_DESCRIPTION;
            Status = (TestStatus)record.STATUS_ID;
            HasSufficientData = record.HAS_SUFFICIENT_DATA == 'T' ? true : false;
        }
        #endregion

        private static string RenderFullName(string lastName, string firstName, string middleName)
        {

            if (lastName != String.Empty && firstName != String.Empty)
            {
                return (lastName + ", " + firstName + " " + middleName).ToString().Trim();
            }
            else if (lastName != String.Empty || firstName != String.Empty)
            {
                if (lastName == String.Empty)
                {
                    return firstName;
                }
                else
                {
                    return lastName;
                }
            }
            else
            {
                return String.Empty;
            }
        }
        private string GetTestStatusDescription(TestStatus status, string testModeDescription, bool hasSufficientData)
        {
            string retString = string.Empty;

            switch (status)
            {
                case TestStatus.Running:
                    retString = "In Progress";
                    break;
                case TestStatus.Completed:
                    switch (testModeDescription)
                    {
                        case "Automatic":   
                        case "Training":
                            if (hasSufficientData)
                            {
                                retString = "Completed";
                            }
                            else
                            {
                                retString = "Needs Data";
                            }

                            break;
                        default:
                            retString = testModeDescription;
                            break;
                    }

                    break;
                case TestStatus.Pending:
                    switch (testModeDescription)
                    {
                        case "Automatic":
                            retString = status.ToString();
                            break;
                        case "Training":
                            retString = "Pending Training";
                            break;
                        case "Manual":
                            retString = "Pending Manual";
                            break;
                        case "VOPS":
                            retString = "Pending VOPS";
                            break;
                        default:
                            retString = status.ToString();
                            break;
                    }

                    break;
                default:
                    retString = status.ToString();
                    break;
            }

            return retString;
        }

        public override bool Equals(object obj)
        {
            if (obj is BVATestInfoDTO)
            {
                BVATestInfoDTO bvaTestInfo = (BVATestInfoDTO)obj;
                return TestID == bvaTestInfo.TestID;
            }
            else
            {
                return false;
            }
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
