﻿using System;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.Database.CommonBLLs
{
    public class QCTestInfoDTO : ObservableObject
    {
        #region Fields

        private Guid _testId;
        private DateTime _testDate;
        private TestStatus _testStatus;
        private string _analyst;
        private bool? _pass;
        private string _qcTestType;

        #endregion

        #region Properties

        public Guid TestID
        {
            get { return _testId; }
            set
            {
                if (_testId == value)
                    return;

                _testId = value;
                FirePropertyChanged("TestID");
            }
        }
        public DateTime TestDate
        {
            get { return _testDate; }

            set
            {
                if (_testDate == value)
                    return;

                _testDate = value;
                FirePropertyChanged("TestDate");
            }
        }
        public TestStatus Status
        {
            get { return _testStatus; }
            set
            {
                _testStatus = value;
                FirePropertyChanged("Status");
                FirePropertyChanged("TestStatusDescription");
                FirePropertyChanged("Result");
            }
        }
        public string TestStatusDescription
        {
            get
            {
                return GetTestStatusDescription(Status);
            }
        }
        public string Analyst
        {
            get { return _analyst; }
            set
            {
                if (_analyst == value)
                    return;

                _analyst = value;
                FirePropertyChanged("Analyst");
            }
        }
        public bool? Pass
        {
            get { return _pass; }
            set 
            {
                if (base.SetValue(ref _pass, "Pass", value))
                {
                    FirePropertyChanged("Result");
                }
            }
        }
        public string QCTestType
        {
            get { return _qcTestType; }
            set
            {
                if (_qcTestType == value)
                    return;

                _qcTestType = value;
                FirePropertyChanged("QCTestType");
            }
        }

        public string Result
        {
            get
            {
                switch (TestStatusDescription)
                {
                    case "Pending":
                    case "In Progress":
                        return string.Empty;
                    case "Aborted":
                        return "Failed";
                    default:
                        if (Pass != null)
                            return (bool)Pass ? "Pass" : "Failed";
                        else
                            return string.Empty;
                }
            }
        }

        #endregion

        #region Ctor

        public QCTestInfoDTO() {}
        public QCTestInfoDTO(GetQCTestListResult record)
        {
            Analyst = record.ANALYST;
            Pass = record.PASS == 'T' ? true : false;
            QCTestType = record.QC_TEST_TYPE;
            TestDate = record.TEST_DATE;
            Status = (TestStatus)record.STATUS_ID;
            TestID = record.TEST_ID;
        }
        public QCTestInfoDTO(GetQCTestListItemResult record)
        {
            Analyst = record.ANALYST;
            Pass = record.PASS == 'T' ? true : false;
            QCTestType = record.QC_TEST_TYPE;
            TestDate = record.TEST_DATE;
            Status = (TestStatus)record.STATUS_ID;
            TestID = record.TEST_ID;
        }

        #endregion

        #region Helpers

        private string GetTestStatusDescription(TestStatus status)
        {
            string retString = string.Empty;

            switch (status)
            {
                case TestStatus.Running:
                    retString = "In Progress";
                    break;
                default:
                    retString = status.ToString();
                    break;

            }

            return retString;
        }
        #endregion

        public override bool Equals(object obj)
        {
            if (obj is QCTestInfoDTO)
            {
                QCTestInfoDTO qcTestInfo = (QCTestInfoDTO)obj;
                return TestID == qcTestInfo.TestID;
            }
            else
            {
                return false;
            }
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
