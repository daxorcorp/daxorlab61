﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.DataObjects;
using System.Data.Linq;
using Daxor.Lab.Database;

namespace  Daxor.Lab.DataObjects.BaseBLLs
{
    public class PatientDTO 
    {
        public Sex Sex { get; set; }

        public DateTime DOB { get; set; }

        public Guid PID { get; set; }
        
        public string PatientHospitalId { get; set; }
        
        public string FirstName { get; set; }
        
        public string MiddleName { get; set; }
        
        public string LastName { get; set; }

        public Binary Timestamp { get; set; }
    }
}
