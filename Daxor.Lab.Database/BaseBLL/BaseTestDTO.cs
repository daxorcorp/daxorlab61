﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using Daxor.Lab.DataObjects.BaseBLL;

namespace  Daxor.Lab.DataObjects.BvaBLLs
{
    public abstract class BaseTestDTO
    {
        public Guid TestId { get; set; }

        public int StatusId { get; set; }

        public string StatusName { get; set; }

        public DateTime TestDate { get; set; }

        public int TestType { get; set; }

        public string Analyst { get; set; }

        public string Comment { get; set; }

        public string UniqueSystemId { get; set; }

        public int BackgroundCount { get; set; }

        public double BackgroundTime { get; set; }

        public int Duration { get; set; }

        public Binary TestHeaderTimestamp { get; set; }

        public List<BaseSampleDTO> Samples { get; set; }
    }
}
