﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace  Daxor.Lab.DataObjects.BaseBLL
{
    public abstract class BaseSampleDTO
    {
        public Guid Id { get; set; }
        public int Position { get; set; }
        public int SampleLayoutId { get; set; }

        public double PresetLiveTime { get; set; }
        public double ActualLiveTime { get; set; }
        public double ActualRealTime { get; set; }

        public string PositionName { get; set; } //Pat-1A, Ctl-A, Std-A, etc.
    }
}
