﻿using System;
using System.Data.Linq;
using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{

    public class BarCodeRecord : DataRecordBase
    {
        public Guid TestId { get; set; }

        public string BID { get; set; }

        public int PositionId { get; set; }

        public string PositionName { get; set; }

        public Binary Timestamp { get; set; }

        public BarCodeRecord() { }

        public BarCodeRecord(GetBarcodeDataResult record)
        {
            TestId = record.TEST_ID;
            BID = record.BID;
            PositionId = record.POSITION_ID;
            PositionName = record.POSITION_NAME;
            Timestamp = record.ROW_TIMESTAMP;
        }

        public override bool Equals(object obj)
        {
            BarCodeRecord b = obj as BarCodeRecord;

            if (b == null)
            {
                return false;
            }

            return TestId == b.TestId && BID == b.BID && PositionId == b.PositionId && PositionName == b.PositionName;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
