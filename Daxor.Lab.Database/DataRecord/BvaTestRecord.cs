﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using Daxor.Lab.Database.LINQ;
// ReSharper disable InconsistentNaming

namespace Daxor.Lab.Database.DataRecord
{
    public class BvaTestRecord : TestRecord
	{
		#region Ctor

		public BvaTestRecord()
		{
			PACS = "";
		}

		public BvaTestRecord(GetBVATestDataResult record)
		{
			Analyst = record.ANALYST;
			BackgroundCount = record.BACKGROUND_COUNT;
			// ReSharper disable once PossibleInvalidOperationException
			BackgroundTime = (double) record.BACKGROUND_TIME;
			BloodDrawCount = record.BLOOD_DRAW_COUNT;
			BvaTestTimestamp = record.BVA_TEST_TIMESTAMP;
			CCReportTo = record.CC_REPORT_TO;
			Comment = record.COMMENT;
			ConsiderationId = record.CONSIDERATION_ID;
			MeasurementSystem = record.DISPLAY_UNITS;
			Dose = record.DOSE;
			HematocritFillType = record.HEMATOCRIT_FILL_TYPE;
			ID2 = record.ID2;
			InjectateLot = record.INJECTATE_LOT;
			Location = record.LOCATION;
			PACS = record.PACS;
			PacsTimestamp = record.PACS_TIMESTAMP;
			PatientHeight = record.PATIENT_HEIGHT;
			PatientWeight = record.PATIENT_WEIGHT;
			PID = record.PID;
			// ReSharper disable once PossibleInvalidOperationException
			AmputeeIdealsCorrectionFactor = (int) record.AMPUTEE_IDEALS_CORRECTION_FACTOR;
			ReferenceVolume = record.REFERENCE_VOLUME;
			ReferringPhysician = record.REFERRING_PHYSICIAN;
			PhysiciansSpecialty = record.PHYSICIANS_SPECIALTY;
			SampleLayoutId = record.SC_LAYOUT_ID;
			StatusId = record.STATUS_ID;
			StatusName = record.STATUS_NAME;
			TestDate = record.TEST_DATE;
			TestHeaderTimestamp = record.TEST_HEADER_TIMESTAMP;
			TestId = record.TEST_ID;
			TestMode = record.MODE_ID;
			TestModeDescription = record.TEST_MODE_DESCRIPTION;
			TestType = record.TYPE_ID;
			TubeDescription = record.TUBE_DESCRIPTION;
			TubeId = record.TUBE_ID;
			UniqueSystemId = record.UNIQUE_SYSTEM_ID;
			HasSufficientData = record.HAS_SUFFICIENT_DATA == 'T';
			SampleDurationInSeconds = record.SAMPLE_DURATION_IN_SECONDS;
		}

		#endregion

		#region Properties

		public int SampleLayoutId { get; set; }

        public Guid PID { get; set; }

        public PatientRecord Patient { get; set; }

        public List<BvaSampleRecord> Samples { get; set; }

        public string ID2 { get; set; }

        public string Location { get; set; }

        public int BloodDrawCount { get; set; }
       
        public double PatientHeight { get; set; }

        public double PatientWeight { get; set; }

        public double Dose { get; set; }

        public string InjectateLot { get; set; }

        public string ReferringPhysician { get; set; }

        public string PhysiciansSpecialty { get; set; }

        public string CCReportTo { get; set; }
        
        public int HematocritFillType { get; set; }

        public int ReferenceVolume { get; set; }

        public int TestMode { get; set; }

		public int AmputeeIdealsCorrectionFactor { get; set; }

        public string TestModeDescription { get; set; }

        public string PACS { get; set; }

        public string MeasurementSystem { get; set; }

        public TubeTypeRecord TubeTypeRecord { get; set; }

        public int TubeId { get; set; }

        public string TubeDescription { get; set; }

        public int ConsiderationId { get; set; }

        public string InjectateBarcode { get; set; }

        public string StandardABarcode { get; set; }

        public string StandardBBarcode { get; set; }

        public Binary BvaTestTimestamp { get; set; }

        public Binary PacsTimestamp { get; set; }

        public bool HasSufficientData { get; set; }

        public int SampleDurationInSeconds { get; set; }

		#endregion
	}
}
