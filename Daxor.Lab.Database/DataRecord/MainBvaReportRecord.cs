﻿using System;
using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
    public class MainBvaReportRecord : DataRecordBase 
    {
        public Guid TestId { get; set; }

        public Guid PID { get; set; }

        public string HospitalName { get; set; }

        public string DepartmentName { get; set; }

        public string DepartmentAddress { get; set; }

        public string DepartmentDirector { get; set; }

        public string DepartmentPhoneNumber { get; set; }

        public string PatientHospitalId { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Gender { get; set; }

        public string DOB { get; set; }

        public string StatusId { get; set; }

        public string TestDate { get; set; }

        public string Analyst { get; set; }

        public string Comment { get; set; }

        public string UniqueSystemId { get; set; }

        public string BackgroundCount { get; set; }

        public string BackgroundTime { get; set; }

        public string Duration { get; set; }

        public string Accession { get; set; }

        public string Location { get; set; }

        public string SampleCount { get; set; }

        public string PatientHeight { get; set; }

        public string PatientWeight { get; set; }

        public string Dose { get; set; }

        public string InjectateLot { get; set; }

        public string ReferringPhysician { get; set; }

        public string CCReportTo { get; set; }

        public string MeasurementSystem { get; set; }

        public string Pacs { get; set; }
    }
}
