﻿using System;
using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
    public class HospitalPatientIdRecord : DataRecordBase
    {
        public Guid PatientId { get; set; }

        public string HospitalId { get; set; }

        public HospitalPatientIdRecord(GetPatientHospitalIDListResult record)
        {
            HospitalId = record.PATIENT_HOSPITAL_ID;
            PatientId = record.PID;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((HospitalPatientIdRecord) obj);
        }

        protected bool Equals(HospitalPatientIdRecord other)
        {
            return PatientId.Equals(other.PatientId) && string.Equals(HospitalId, other.HospitalId);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (PatientId.GetHashCode() * 397) ^ (HospitalId != null ? HospitalId.GetHashCode() : 0);
            }
        }

        public override string ToString()
        {
            return HospitalId;
        }
    }
}

