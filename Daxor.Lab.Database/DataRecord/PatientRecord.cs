﻿using System;
using System.Data.Linq;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.DataObjects;

namespace Daxor.Lab.Database.DataRecord
{
    public class PatientRecord : DataRecordBase
	{
		#region Ctors

		public PatientRecord() { }

		public PatientRecord(GetPatientDataResult record)
		{
			PID = record.PID;
			PatientHospitalId = record.PATIENT_HOSPITAL_ID;
			FirstName = record.FIRST_NAME;
			MiddleName = record.MIDDLE_NAME;
			LastName = record.LAST_NAME;
			IsAmputee = record.IS_AMPUTEE;

			switch (record.GENDER.ToString())
			{
				case "M":
					Gender = Sex.Male;
					break;

				case "F":
					Gender = Sex.Female;
					break;

				default:
					Gender = Sex.Unknown;
					break;
			}

			DOB = record.DOB;
			if (DOB != null && DOB.Value.Date == new DateTime(1753, 1, 1).Date)
			{
				DOB = null;
			}

			Timestamp = record.ROW_TIMESTAMP;
		}

		public PatientRecord(GetPatientDataFromHospitalIDResult record)
		{
			PID = record.PID;
			PatientHospitalId = record.PATIENT_HOSPITAL_ID;
			FirstName = record.FIRST_NAME;
			MiddleName = record.MIDDLE_NAME;
			LastName = record.LAST_NAME;
			IsAmputee = record.IS_AMPUTEE;

			switch (record.GENDER.ToString())
			{
				case "M":
					Gender = Sex.Male;
					break;

				case "F":
					Gender = Sex.Female;
					break;

				default:
					Gender = Sex.Unknown;
					break;
			}

			DOB = record.DOB;
			if (DOB != null && DOB.Value.Date == new DateTime(1753, 1, 1).Date)
			{
				DOB = null;
			}

			Timestamp = record.ROW_TIMESTAMP;
		}

		public PatientRecord(GetAllPatientDataResult record)
		{
			PID = record.PID;
			PatientHospitalId = record.PATIENT_HOSPITAL_ID;
			FirstName = record.FIRST_NAME;
			MiddleName = record.MIDDLE_NAME;
			LastName = record.LAST_NAME;
			IsAmputee = record.IS_AMPUTEE;

			switch (record.GENDER.ToString())
			{
				case "M":
					Gender = Sex.Male;
					break;

				case "F":
					Gender = Sex.Female;
					break;

				default:
					Gender = Sex.Unknown;
					break;
			}

			DOB = record.DOB;
			if (DOB != null && DOB.Value.Date == new DateTime(1753, 1, 1).Date)
			{
				DOB = null;
			}

			Timestamp = record.ROW_TIMESTAMP;
		}

		#endregion

		#region Properties

		private Sex _gender;

        public Guid PID { get; set; }

        public string PatientHospitalId { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public Sex Gender 
        {
            get
            {
                return _gender;
            }

            set
            {
                _gender = value;
                OnPropertyChanged("Gender");
            }
        }

        public DateTime? DOB { get; set; }

        public Binary Timestamp { get; set; }

		public bool IsAmputee { get; set; }

		#endregion

		public override bool Equals(object obj)
        {
            PatientRecord b = obj as PatientRecord;

            if (b == null)
            {
                return false;
            }

            return PID == b.PID && PatientHospitalId == b.PatientHospitalId && FirstName == b.FirstName && MiddleName == b.MiddleName 
                && LastName == b.LastName && DOB == b.DOB && Gender == b.Gender && IsAmputee == b.IsAmputee;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
