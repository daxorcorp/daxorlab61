﻿using System.Collections.Generic;
using System.Data.Linq;
using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
    public class QCTestRecord : TestRecord 
    {
        public int SampleLayoutId { get; set; }

        public List<QCSampleRecord> Samples { get; set; }

        public string InjectateLot { get; set; }

        public string StandardABarcode { get; set; }

        public string StandardBBarcode { get; set; }

        public string QCTestType { get; set; }

        public bool Pass { get; set; }

        public double InitialFineGain { get; set; }

        public double EndFineGain { get; set; }

        public double HighVoltage { get; set; }

        public int CalibrationCurveId { get; set; }

        public CalibrationCurveRecord CalibrationRecord { get; set; }

        public Binary QCTestTimestamp { get; set; }

        public QCTestRecord() {}

        public QCTestRecord(GetQCTestDataResult record)
        {
            Analyst = record.ANALYST;
            BackgroundCount = record.BACKGROUND_COUNT;
            BackgroundTime = (double)record.BACKGROUND_TIME;
            Comment = record.COMMENT;
            SampleLayoutId = record.SC_LAYOUT_ID;
            StatusId = record.STATUS_ID;
            StatusName = record.STATUS_NAME;
            TestDate = record.TEST_DATE;
            TestHeaderTimestamp = record.TEST_HEADER_TIMESTAMP;
            TestId = record.TEST_ID;
            TestType = record.TYPE_ID;
            UniqueSystemId = record.UNIQUE_SYSTEM_ID;
            InjectateLot = record.INJECTATE_LOT;
            Pass = record.PASS == 'T' ? true : false;
            InitialFineGain = record.INITIAL_FINE_GAIN;
            EndFineGain = record.END_FINE_GAIN;
            HighVoltage = record.HIGH_VOLTAGE;
            QCTestTimestamp = record.QC_TEST_TIMESTAMP;
            QCTestType = record.QC_TEST_TYPE;
            CalibrationCurveId = record.CALIBRATION_CURVE_ID;
        }
    }
}
