﻿using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
    public class TestModeRecord : DataRecordBase
    {
        public int ModeId { get; set; }
        public string Description { get; set; }

        public TestModeRecord(GetTestModeDataResult record)
        {
            Description = record.MODE_DESCRIPTION;
            ModeId = record.MODE_ID;
        }

        public TestModeRecord(GetTestModeListResult record)
        {
            Description = record.MODE_DESCRIPTION;
            ModeId = record.MODE_ID;
        }
    }
}
