﻿using System;
using System.Text.RegularExpressions;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Domain.Services;

namespace Daxor.Lab.Database.DataRecord
{

    public class AuditTrailRecord : DataRecordBase
	{
		#region Ctor

		public AuditTrailRecord()
		{

		}

		public AuditTrailRecord(GetAuditTrailResult record)
		{
			TestId = record.TEST_ID;
			BvaTestMeasurementSystem = record.DISPLAY_UNITS;
			SampleId = record.SAMPLE_ID ?? Guid.Empty;
			ChangedBy = record.CHANGED_BY;
			PositionData = record.POSITION_DATA;

			NotFriendlyChangeField = record.CHANGE_FIELD;
			ChangeField = GetUserFriendlyFieldName(record.CHANGE_FIELD);
			ChangeTable = record.CHANGE_TABLE;
			ChangeTime = record.CHANGE_TIME.ToShortDateString() + " " + record.CHANGE_TIME.ToShortTimeString();
			NewValue = ConvertValueToCurrentContext(record.NEW_VALUE, record.TEST_ID, record.CHANGE_FIELD);
			OldValue = ConvertValueToCurrentContext(record.OLD_VALUE, record.TEST_ID, record.CHANGE_FIELD);
		}

		#endregion

		#region Properties

		public Guid TestId { get; set; }
        
        public Guid SampleId { get; set; }

        public string ChangeTime { get; set; }

        public string ChangeTable { get; set; }

        public string ChangeField { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }

        public string ChangedBy { get; set; }

        public string PositionData { get; set; }

        public string NotFriendlyChangeField { get; set; }

        public string BvaTestMeasurementSystem { get; set; }

		#endregion

		#region Helpers

		private static string SecondsToMinutes(string secondsString)
        {
            int rawSeconds;
            if (!Int32.TryParse(secondsString, out rawSeconds)) return String.Empty;
            
            var postInjTime = TimeSpan.FromSeconds(rawSeconds);
            if (postInjTime.TotalHours < 1.0)
                return postInjTime.ToString(@"%m\:ss") + " min:sec";
            
            return postInjTime.ToString(@"%h\:mm\:ss") + " hrs:min:sec";
        }

        protected string ConvertValueToCurrentContext(string value, Guid testId, string fieldName)
        {
            if (value == "-1")
                return String.Empty;

            Regex regex;
            Match match;

            switch (fieldName)
            {
                case "TIME_POST_INJECTION":
                    return SecondsToMinutes(value);
                case "PATIENT_HEIGHT":
                    regex = new Regex(@"^-?\d+(?:\.\d+)?");
                    match = regex.Match(value);
                    double rawHeight;
                    switch(BvaTestMeasurementSystem.Trim())
                    {
                        case "US":
                            if (Double.TryParse(match.ToString(), out rawHeight))
                                return (Math.Round((UnitsOfMeasurementConverter.ConvertHeightToEnglish(rawHeight)), 2 , MidpointRounding.AwayFromZero)) + " in";
                            return String.Empty;
                        case "METRIC":
                            if (Double.TryParse(match.ToString(), out rawHeight))
                                return (Math.Round(rawHeight, 2, MidpointRounding.AwayFromZero)) + " cm";
                            return String.Empty;
                    }
                    return String.Empty;
                case "PATIENT_WEIGHT":
                    regex = new Regex(@"^-?\d+(?:\.\d+)?");
                    match = regex.Match(value);
                    double rawWeight;
                    switch (BvaTestMeasurementSystem.Trim())
                    {
                        case "US":
                            if (Double.TryParse(match.ToString(), out rawWeight))
                                return (Math.Round((UnitsOfMeasurementConverter.ConvertWeightToEnglish(rawWeight)), 2 , MidpointRounding.AwayFromZero)) + " lb";
                            return String.Empty;
                        case "METRIC":
                            if (Double.TryParse(match.ToString(), out rawWeight))
                                return (Math.Round(rawWeight , 2, MidpointRounding.AwayFromZero)) + " kg";
                            return String.Empty;
                    }
                    return String.Empty;
                case "IS_SAMPLE_EXCLUDED":
                case "IS_COUNT_EXCLUDED":
                case "IS_COUNT_AUTO_EXCLUDED":
                case "IS_HEMATOCRIT_EXCLUDED":
                    switch (value)
                    {
                        case "T":
                            return "True";
                        case "F":
                            return "False";
                    }
                    return String.Empty;
				case "IS_AMPUTEE":
		            switch (value)
		            {
			            case "1":
				            return "Amputee";
						case "0":
				            return "Non-amputee";
		            }
		            return string.Empty;
				case "AMPUTEE_IDEALS_CORRECTION_FACTOR":
		            if (value == "0")
			            return "0%";
		            return "-" + value + "%";
	            default:
                    return value;
            }

        }

        private string GetUserFriendlyFieldName(string fieldName)
        {
            string retVal = fieldName;

            switch (fieldName)
            {
                case "GENDER":
                    retVal = "Gender";
                    break;
                case "DISPLAY_UNITS":
                    retVal = "Units of Measure";
                    break;
                case "BACKGROUND_COUNT":
                    retVal = "Background Counts";
                    break;
                case "TUBE_ID":
                    retVal = "Tube Type";
                    break;
                case "DOB":
                    retVal = "Date of Birth";
                    break;
                case "ID2":
                    retVal = DatabaseSettingsHelper.ID2Label;
                    break;
                case "LOCATION":
                    retVal = DatabaseSettingsHelper.LocationLabel;
                    break;
                case "BLOOD_DRAW_COUNT":
                    retVal = "Protocol";
                    break;
                case "PATIENT_HEIGHT":
                    retVal = "Patient Height";
                    break;
                case "PATIENT_WEIGHT":
                    retVal = "Patient Weight";
                    break;
                case "DOSE":
                    retVal = "Dose";
                    break;
                case "INJECTATE_LOT":
                    retVal = "Injectate Lot";
                    break;
                case "REFERRING_PHYSICIAN":
                    retVal = "Referring MD";
                    break;
                case "PHYSICIANS_SPECIALTY":
                    retVal = "MD Specialty";
                    break;
                case "CC_REPORT_TO":
                    retVal = "CC";
                    break;
                case "HEMATOCRIT_FILL_TYPE":
                    retVal = "Hematocrit Fill Type";
                    break;
                case "SAMPLE_DURATION_IN_SECONDS":
                    retVal = "Sample Acquisition Time";
                    break;
                case "ANALYST":
                    retVal = "Analyst";
                    break;
                case "COMMENT":
                    retVal = "Comments";
                    break;            
                case "IS_SAMPLE_EXCLUDED":
                    retVal = "Sample Excluded " + PositionData;
                    break;
                case "IS_COUNT_EXCLUDED":
                    retVal = "Count Excluded " + PositionData;
                    break;
                case "IS_COUNT_AUTO_EXCLUDED":
                    retVal = "Count Automatically Excluded " + PositionData;
                    break;
                case "IS_HEMATOCRIT_EXCLUDED":
                    retVal = "Hematocrit Excluded " + PositionData;
                    break;
                case "HEMATOCRIT":
                    retVal = "Hematocrit " + PositionData;
                    break;
                case "COUNTS":
                    retVal = "Counts " + PositionData;
                    break;
                case "TIME_POST_INJECTION":
                    retVal = "Post-Injection Time " + PositionData;
                    break;
				case "AMPUTEE_IDEALS_CORRECTION_FACTOR":
					retVal = "Amputee Ideals Correction Factor";
		            break;
				case "IS_AMPUTEE":
		            retVal = "Amputee Status";
		            break;
            }
            
            return retVal;
		}

		#endregion
	}
}
