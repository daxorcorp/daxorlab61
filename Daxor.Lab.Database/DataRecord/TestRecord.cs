﻿using System;
using System.Collections.Generic;
using System.Data.Linq;

namespace Daxor.Lab.Database.DataRecord
{
    public abstract class TestRecord
    {
        public Guid TestId { get; set; }

        public int StatusId { get; set; }

        public string StatusName { get; set; }

        public DateTime TestDate { get; set; }
        
        public int TestType { get; set; }

        public string Analyst { get; set; }

        public string Comment { get; set; }

        public string UniqueSystemId { get; set; }
        
        public int BackgroundCount { get; set; }

        public double BackgroundTime { get; set; }
 
        public Binary TestHeaderTimestamp { get; set; }

        public object ConcurrencyObject
        {
            get
            {
                return (object)ConcurrencyManagement;
            }

            set
            {
                ConcurrencyManagement = value as Dictionary<string, Binary>;
            }
        }

        internal Dictionary<string, Binary> ConcurrencyManagement = new Dictionary<string,Binary>();
    }
}
