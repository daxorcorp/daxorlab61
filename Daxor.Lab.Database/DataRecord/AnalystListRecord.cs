﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
    public class AnalystListRecord
    {
        public string Analyst { get; set; }

        public AnalystListRecord() { }

        public AnalystListRecord(GetAnalystListResult record)
        {
            Analyst = record.ANALYST;
        }
    }
}
