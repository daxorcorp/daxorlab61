﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
    public class QCSourceRecord
    {
         public string Description { get; set; }

        public DateTime SourceTOC { get; set; }

        public string Isotope { get; set; }

        public double Activity { get; set; }

        public double Accuracy { get; set; }

        public double HalfLife { get; set; }

        public string SerialNumber { get; set; }

        public int CountLimit { get; set; }

        public int Position { get; set; }

        public QCSourceRecord() {}

        public QCSourceRecord(GetQCSourcesResult record)
        {
            Accuracy = record.ACCURACY;
            Activity = record.ACTIVITY;
            CountLimit = record.COUNT_LIMIT;
            HalfLife = record.HALF_LIFE;
            Isotope = record.ISOTOPE;
            Position = record.SC_POSITION;
            SerialNumber = record.SERIAL_NUMBER;
            Description = record.POSITION_DESCRIPTION;
            SourceTOC = record.SOURCE_TOC;

        }

        public QCSourceRecord(GetQCSourceDataResult record)
        {
            Accuracy = record.ACCURACY;
            Activity = record.ACTIVITY;
            CountLimit = record.COUNT_LIMIT;
            HalfLife = record.HALF_LIFE;
            Isotope = record.ISOTOPE;
            Position = record.SC_POSITION;
            SerialNumber = record.SERIAL_NUMBER;
            Description = record.POSITION_DESCRIPTION;
            SourceTOC = record.SOURCE_TOC;

        }

    }
}
