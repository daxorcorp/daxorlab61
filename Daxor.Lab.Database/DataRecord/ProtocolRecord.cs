﻿using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
    public class ProtocolRecord : DataRecordBase
    {
        public int Protocol { get; set; }
        public string Description { get; set; }

        public ProtocolRecord(GetProtocolListResult record)
        {
            Protocol = record.PROTOCOL;
            Description = record.DESCRIPTION;
        }
    }
}
