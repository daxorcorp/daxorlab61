﻿using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{    
    public class HematocritFillTypeRecord : DataRecordBase
    {
        public int HematocritFillType { get; set; }
        public string Description { get; set; }

        public HematocritFillTypeRecord(GetHematocritFillTypeListResult record)
        {
            Description = record.DESCRIPTION;
            HematocritFillType = record.HEMATOCRIT_FILL_TYPE;
        }
    }
}
