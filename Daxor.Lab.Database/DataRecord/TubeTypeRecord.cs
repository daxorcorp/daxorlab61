﻿using Daxor.Lab.Database.LINQ;    

namespace Daxor.Lab.Database.DataRecord
{ 
    public class TubeTypeRecord
    {
        public string Description { get; set; }
        public int TubeId { get; set; }
        public double Anticoagulant { get; set; }

        public TubeTypeRecord()
        {

        }

        public TubeTypeRecord(GetTubeTypeDataResult record)
        {
            Anticoagulant = record.ANTICOAGULANT;
            Description = record.TYPE;
            TubeId = record.TUBE_ID;
                
        }

        public TubeTypeRecord(GetTubeTypeListResult record)
        {
            Anticoagulant = record.ANTICOAGULANT;
            Description = record.TYPE;
            TubeId = record.TUBE_ID;
        }
    }
}
