﻿using System;
using System.Data.Linq;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Infrastructure.Constants;

namespace Daxor.Lab.Database.DataRecord
{
    public class QCSampleRecord : SampleRecord 
    {
        public int Counts { get; set; }

        public double FWHM { get; set; }

        public double FWTM { get; set; }

        public double Centroid { get; set; }

        public bool? Pass { get; set; }

        public DateTime SourceTOC { get; set; }

        public string Isotope { get; set; }

        public double Activity { get; set; }

        public double Accuracy { get; set; }

        public double HalfLife { get; set; }

        public string SerialNumber { get; set; }

        public int CountLimit { get; set; }
        
        public int RoiHighBound { get; set; }
        
        public int RoiLowBound { get; set; }

        public string ErrorDetails { get; set; }

        public Binary SampleHeaderTimestamp { get; set; }

        public Binary QCSampleTimestamp { get; set; }

        public QCSampleRecord() : base() {}

        public QCSampleRecord(GetQCTestSampleResult record)
            : base()
        {
            Centroid = record.CENTROID;
            Counts = record.COUNTS;
            FWHM = record.FWHM;
            FWTM = record.FWTM;
            SampleHeaderTimestamp = record.SAMPLE_HEADER_TIMESTAMP;
            QCSampleTimestamp = record.QC_SAMPLE_TIMESTAMP;
            SampleId = record.SAMPLE_ID;
            PositionRecord = new PositionRecord()
            {
                SampleFunction = record.POSITION_NAME,
                Position = record.SAMPLE_POSITION,
                ShortFriendlyName = record.POSITION_DESCRIPTION,
                SortOrder = record.SORT_ORDER == null ? 0 : (int)record.SORT_ORDER
            };
            SampleLayoutId = record.SC_LAYOUT_ID;
            TestId = record.TEST_ID;
            LiveTime = record.LIVE_TIME;
            PresetLiveTime = record.PRESET_LIVE_TIME;
            RealTime = record.REAL_TIME;
            
            Pass = record.PASS == 'T' ? true : false;

            SourceTOC = record.SOURCE_TOC == null ? DateTime.Parse(DomainConstants.NullDateTime) : (DateTime)record.SOURCE_TOC;
            Accuracy = record.ACCURACY == null ? 0.0 : (double)record.ACCURACY;
            Activity = record.ACTIVITY == null ? 0.0 : (double)record.ACTIVITY;
            CountLimit = record.COUNT_LIMIT == null ? 0 : (int)record.COUNT_LIMIT;
            RoiHighBound = (int)record.ROI_HIGH_BOUND;
            RoiLowBound = (int)record.ROI_LOW_BOUND;
            HalfLife = record.HALF_LIFE == null ? 0.0 : (double)record.HALF_LIFE;
            Isotope = record.ISOTOPE == null ? string.Empty : record.ISOTOPE.ToString();
            SerialNumber = record.SERIAL_NUMBER == null ? string.Empty : record.SERIAL_NUMBER.ToString();
            ErrorDetails = record.ERROR_DETAILS == null ? string.Empty : record.ERROR_DETAILS.ToString();
            
            SpectrumToArray(record.SPECTRUM);
        }

        public QCSampleRecord(GetQCTestSamplesResult record)
            : base()
        {
            Centroid = record.CENTROID;
            Counts = record.COUNTS;
            FWHM = record.FWHM;
            FWTM = record.FWTM;
            SampleHeaderTimestamp = record.SAMPLE_HEADER_TIMESTAMP;
            QCSampleTimestamp = record.QC_SAMPLE_TIMESTAMP;
            SampleId = record.SAMPLE_ID;
            PositionRecord = new PositionRecord()
            {
                SampleFunction = record.POSITION_NAME,
                Position = record.SAMPLE_POSITION,
                ShortFriendlyName= record.POSITION_DESCRIPTION, 
                SortOrder = record.SORT_ORDER == null ? 0 : (int)record.SORT_ORDER
            };
            SampleLayoutId = record.SC_LAYOUT_ID;
            TestId = record.TEST_ID;
            LiveTime = record.LIVE_TIME;
            PresetLiveTime = record.PRESET_LIVE_TIME;
            RealTime = record.REAL_TIME;
            
            if (record.PASS != null)
                Pass = record.PASS == 'T' ? true : false;
            else
                Pass = null; 
            
            SourceTOC = record.SOURCE_TOC == null ? DateTime.Parse(DomainConstants.NullDateTime) : (DateTime)record.SOURCE_TOC;
            Accuracy = record.ACCURACY == null ? 0.0 : (double)record.ACCURACY;
            Activity = record.ACTIVITY == null ? 0.0 : (double)record.ACTIVITY;
            CountLimit = record.COUNT_LIMIT == null ? 0 : (int)record.COUNT_LIMIT;
            RoiHighBound = (int)record.ROI_HIGH_BOUND;
            RoiLowBound = (int)record.ROI_LOW_BOUND;
            HalfLife = record.HALF_LIFE == null ? 0.0 : (double)record.HALF_LIFE;
            Isotope = record.ISOTOPE == null ? string.Empty : record.ISOTOPE.ToString();
            SerialNumber = record.SERIAL_NUMBER == null ? string.Empty : record.SERIAL_NUMBER.ToString();
            ErrorDetails = record.ERROR_DETAILS == null ? string.Empty : record.ERROR_DETAILS.ToString();

            SpectrumToArray(record.SPECTRUM);
        }

    }
}
