﻿using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
    public class SpecialConsiderationsRecord : DataRecordBase 
    {
        public int ConsiderationId { get; set; }

        public string Description { get; set; }

        public double Factor { get; set; }

        public SpecialConsiderationsRecord(GetSpecialConsiderationsListResult record)
        {
            ConsiderationId = record.CONSIDERATION_ID;
            Description = record.DESCRIPTION;
            Factor = record.FACTOR;
        }

        public override bool Equals(object obj)
        {
            SpecialConsiderationsRecord b = obj as SpecialConsiderationsRecord;

            if (b == null)
            {
                return false;
            }

            return ConsiderationId == b.ConsiderationId;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
