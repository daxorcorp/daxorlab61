﻿using System;
using System.Text;

namespace Daxor.Lab.Database.DataRecord
{
    public class SampleRecord : DataRecordBase
    {
        private PositionRecord _positionRecord = new PositionRecord();

        public Guid TestId { get; set; }

        public Guid SampleId { get; set; }

        public PositionRecord PositionRecord
        {
            get
            {
                return _positionRecord;
            }

            set
            {
                _positionRecord = value;
                OnPropertyChanged("PositionRecord");
            }
        }

        public int SampleLayoutId { get; set; }

        public double PresetLiveTime { get; set; }

        public double LiveTime { get; set; }

        public double RealTime { get; set; }

        public uint[] Spectrum { get; set; }

        public SampleRecord()
        {
            Spectrum = new uint[1024];
        }

        internal void SpectrumToArray(string spectrum)
        {
            if (Spectrum == null || spectrum == "")
            {
                return;
            }

            string[] spectrumStrings = spectrum.Split(',');
            int index = 0;
            foreach (string s in spectrumStrings)
            {
                Spectrum[index] = uint.Parse(s);
                index++;
            }
        }

        public string SpectrumToString()
        {
            if (Spectrum == null)
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder();
            string prefix = "";
            foreach (uint i in Spectrum)
            {
                sb.Append(prefix);
                sb.Append(i.ToString());
                prefix = ",";
            }

            return sb.ToString();
        }
    }
}
