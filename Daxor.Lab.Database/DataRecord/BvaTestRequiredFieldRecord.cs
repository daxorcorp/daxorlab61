﻿using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
    public class BvaTestRequiredFieldRecord : DataRecordBase
    {
        public string FieldName { get; set; }

        public bool SystemRequired { get; set; }

        public bool CustomerRequired { get; set; }

        public string MissingValue { get; set; }

        public BvaTestRequiredFieldRecord() { }

        public BvaTestRequiredFieldRecord(GetBVATestRequiredFieldsListResult record)
        {
            CustomerRequired = record.CUSTOMER_REQUIRED == 'T' ? true : false;
            FieldName = record.FIELD_NAME;
            SystemRequired = record.SYSTEM_REQUIRED == 'T' ? true : false;
            MissingValue = record.IS_MISSING_VALUE;
        }

        public override bool Equals(object obj)
        {
            BvaTestRequiredFieldRecord b = obj as BvaTestRequiredFieldRecord;

            if (b == null)
            {
                return false;
            }

            return FieldName == b.FieldName && SystemRequired == b.SystemRequired && CustomerRequired == b.CustomerRequired;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
