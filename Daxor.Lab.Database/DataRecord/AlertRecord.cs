﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
    public class AlertRecord
    {
        public string AlertId { get; set; }

        public string Description { get; set; }
        public string Resolution { get; set; }
        public string ResolutionType { get; set; }

        public AlertRecord(){}

        public AlertRecord(GetAlertDataResult record)
        {
            AlertId = record.ALERT_ID;
            Description = record.DESCRIPTION;
            Resolution = record.RESOLUTION;
            ResolutionType = record.RESOLUTION_TYPE;

        }

        public AlertRecord(GetSpecificAlertDataResult record)
        {
            AlertId = record.ALERT_ID;
            Description = record.DESCRIPTION;
            Resolution = record.RESOLUTION;
            ResolutionType = record.RESOLUTION_TYPE;
        }
    }
}
