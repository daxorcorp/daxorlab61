﻿using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
    /// <summary>
    /// Position record
    /// </summary>
    public class PositionRecord
    {
        public int Position { get; set; }
        public string SampleFunction { get; set; }
        public int SampleNumber { get; set; }
        public string Range { get; set; }
        public string ShortFriendlyName { get; set; }
		public string QcShortFriendlyName { get; set; }
	    // ReSharper disable once InconsistentNaming
        public int LayoutID { get; set; }
        public int SortOrder { get; set; }

        public PositionRecord()
        {

        }
        public PositionRecord(GetQCLinearityTestPositionListResult qcResult)
        {
            Position = qcResult.SAMPLE_POSITION;
            SampleFunction = qcResult.SAMPLE_TYPE;
            Range = qcResult.RANGE.ToString();
            ShortFriendlyName = qcResult.POSITION_NAME;
            SampleNumber = qcResult.SAMPLE_NUMBER ?? -1;
            LayoutID = qcResult.SC_LAYOUT_ID;
        }
        public PositionRecord(GetQCCalibrationTestPositionListResult qcResult)
        {
            Position = qcResult.SAMPLE_POSITION;
            SampleFunction = qcResult.SAMPLE_TYPE;
            Range = qcResult.RANGE.ToString();
			ShortFriendlyName = qcResult.QC_POSITION_NAME;
            SampleNumber = qcResult.SAMPLE_NUMBER ?? -1;
            LayoutID = qcResult.SC_LAYOUT_ID;
        }
        public PositionRecord(GetQCContaminationTestPositionListResult qcResult)
        {
            Position = qcResult.SAMPLE_POSITION;
            SampleFunction = qcResult.SAMPLE_TYPE;
            Range = qcResult.RANGE.ToString();
			ShortFriendlyName = qcResult.QC_POSITION_NAME;
            SampleNumber = qcResult.SAMPLE_NUMBER ?? -1;
            LayoutID = qcResult.SC_LAYOUT_ID;
        }
        public PositionRecord(GetQCStandardsTestPositionListResult qcResult)
        {
            Position = qcResult.SAMPLE_POSITION;
            SampleFunction = qcResult.SAMPLE_TYPE;
            Range = qcResult.RANGE.ToString();
			ShortFriendlyName = qcResult.QC_POSITION_NAME;
            SampleNumber = qcResult.SAMPLE_NUMBER ?? -1;
            LayoutID = qcResult.SC_LAYOUT_ID;
        }

        public PositionRecord(GetPositionsListResult pResult)
        {
            Position = pResult.SAMPLE_POSITION;
            SampleFunction = pResult.SAMPLE_TYPE;
            Range = pResult.RANGE.ToString();
            ShortFriendlyName = pResult.POSITION_NAME;
            SampleNumber = pResult.SAMPLE_NUMBER ?? -1;
            LayoutID = pResult.SC_LAYOUT_ID;
	        QcShortFriendlyName = pResult.QC_POSITION_NAME;
        }
    }
}
