﻿using System;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Database.Utility;

namespace Daxor.Lab.Database.DataRecord
{
    public class PatientHeightRecord
    {
        public Guid PID { get; set; }

        public double HeightInCm { get; set; }

        public Daxor.Lab.Domain.Common.MeasurementSystem MeasurementSystem { get; set; }

        public PatientHeightRecord(Guid pid)
        {
            PID = pid;
            MeasurementSystem = ConvertDisplayUnitsToMeasurementSystem(DatabaseSettingsHelper.DefaultMeasurementSystem);
        }

        public PatientHeightRecord(GetPatientHeightResult record)
        {
            HeightInCm = record.PATIENT_HEIGHT;
            PID = record.PID;
            MeasurementSystem = ConvertDisplayUnitsToMeasurementSystem(record.DISPLAY_UNITS);
        }

        private Daxor.Lab.Domain.Common.MeasurementSystem ConvertDisplayUnitsToMeasurementSystem(string displayUnits)
        {
            if (displayUnits.Trim() == "US")
                return Daxor.Lab.Domain.Common.MeasurementSystem.English;

            return Daxor.Lab.Domain.Common.MeasurementSystem.Metric;
        }
    }
}
