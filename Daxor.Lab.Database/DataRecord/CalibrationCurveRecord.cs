﻿using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
	public class CalibrationCurveRecord
	{
		public int CalibrationCurveId { get; set; }

		public double Slope { get; set; }

		public double OffsetValue { get; set; }

		public CalibrationCurveRecord(){}

		public CalibrationCurveRecord (GetQCCalibrationCurveResult record)
		{
			CalibrationCurveId = record.CALIBRATION_CURVE_ID;
			Slope = record.SLOPE;
			OffsetValue = record.OFFSET_VALUE;
		}

		public CalibrationCurveRecord(GetCurrentQCCalibrationCurveResult record)
		{
			CalibrationCurveId = record.CALIBRATION_CURVE_ID;
			Slope = record.SLOPE;
			OffsetValue = record.OFFSET_VALUE;
		}
	}
}
