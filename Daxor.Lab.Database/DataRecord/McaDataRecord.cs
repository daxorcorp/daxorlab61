﻿using System;
using System.Data.Linq;
using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
    public class McaDataRecord : DataRecordBase
    {
        public Guid TestId { get; set; }

        public double FineGain { get; set; }

        public double Voltage { get; set; }

        public Binary Timestamp { get; set; }

        public McaDataRecord() {}

        public McaDataRecord(GetMCADataResult record)
        {
            FineGain = record.FINE_GAIN;
            TestId = record.TEST_ID;
            Timestamp = record.ROW_TIMESTAMP;
            Voltage = record.VOLTAGE;
        }

        public override bool Equals(object obj)
        {
            McaDataRecord b = obj as McaDataRecord;

            if (b == null)
            {
                return false;
            }

            return TestId == b.TestId && FineGain == b.FineGain && Voltage == b.Voltage;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
