﻿using System.Collections.Generic;

namespace Daxor.Lab.Database.DataRecord
{
    public class BvaTestListRecord : DataRecordBase
    {
        private Samples _samples = new Samples();
        
        #region Patient Data
        public string PatientName 
        { 
            get
            {
                return Patient.LastName + ", " + Patient.FirstName + " " + Patient.MiddleName;      
            }
        }

        #endregion

        public string Status
        {
            get { return Test.StatusName; }
            set { Test.StatusName = value; }
        }

        public PatientRecord Patient { get; set; }

        public BvaTestRecord Test { get; set; }

        public override bool Equals(object obj)
        {
            BvaTestListRecord b = obj as BvaTestListRecord;

            if (b == null)
            {
                return false;
            }

            return Patient.Equals(b.Patient) && Test.Equals(b.Test);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        

        public class Samples : List<Sample>
        {
            public Samples()
                : base()
            {
            }
        }
        
        public class Sample
        {
            public string Name { get; set; }
            
            public string Time { get; set; }
            
            public string HCTA { get; set; }
            
            public string HCTA_OOR { get; set; }  // Out Of Range
            
            public string HCTA_Ex { get; set; }  // Excluded
            
            public string HCTB { get; set; }
            
            public string HCTB_OOR { get; set; } // Out Of Range
            
            public string HCTB_Ex { get; set; } // Excluded
            
            public string CountsA { get; set; }
            
            public string CountsA_OOR { get; set; } // Out Of Range
            
            public string CountsA_Ex { get; set; } // Excluded
            
            public string CountsB { get; set; }
            
            public string CountsB_OOR { get; set; } // Out Of Range
            
            public string CountsB_Ex { get; set; } // Excluded
            
            public string BV { get; set; }
            
            public string IsDisabled { get; set; }
        }
    }
}
