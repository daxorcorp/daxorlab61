﻿using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.Database.DataRecord
{
    public class FieldDataRecord : ObservableObject
    {
        public string FieldName { get; private set; }

        private bool _isSystemRequired;
        public bool IsSystemRequired 
        {
            get
            {
                return _isSystemRequired;
            }

            set
            {
                _isSystemRequired = value;
                FirePropertyChanged("IsSystemRequired");
                FirePropertyChanged("IsRequired");
            }
        }

        private bool _isCustomerRequired;
        public bool IsCustomerRequired
        {
            get
            {
                return _isCustomerRequired;
            }

            set
            {
                _isCustomerRequired = value;
                FirePropertyChanged("IsCustomerRequired");
                FirePropertyChanged("IsRequired");
            }
        }


        public bool IsRequired { get { return IsSystemRequired || IsCustomerRequired; } }

        public string MissingValue { get; set; }

        public string FieldView { get; private set; }

        public FieldDataRecord(GetBVATestRequiredFieldsListResult record)
        {
            FieldName = record.FIELD_NAME;
            IsSystemRequired = record.SYSTEM_REQUIRED == 'T' ? true : false;
            IsCustomerRequired = record.CUSTOMER_REQUIRED == 'T' ? true : false;
            MissingValue = record.IS_MISSING_VALUE;
            FieldView = record.FIELD_VIEW;
        }
    }
}
