﻿using System.Data.Linq;
using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.DataRecord
{
    public class BvaSampleRecord : SampleRecord 
    {
        public bool IsSampleExcluded { get; set; }

        public bool IsCountExcluded { get; set; }

        public bool IsCountAutoExcluded { get; set; }

        public bool IsHematocritExcluded { get; set; }

        public double Hematocrit { get; set; }

        public int RoiHighBound { get; set; }

        public int RoiLowBound { get; set; }

        public int Counts { get; set; }

        public double FWHM { get; set; }

        public double FWTM { get; set; }

        public double Centroid { get; set; }

        public int TimePostInjection { get; set; }

        public Binary SampleHeaderTimestamp { get; set; }

        public Binary BvaSampleTimestamp { get; set; }

        public BvaSampleRecord() : base() { }

        public BvaSampleRecord(GetBVATestSampleResult record)
            : base()
        {
            BvaSampleTimestamp = record.BVA_SAMPLE_TIMESTAMP;
            Centroid = record.CENTROID;
            Counts = record.COUNTS;
            FWHM = record.FWHM;
            FWTM = record.FWTM;
            Hematocrit = record.HEMATOCRIT;
            IsCountExcluded = record.IS_COUNT_EXCLUDED == 'T';
            IsCountAutoExcluded = record.IS_COUNT_AUTO_EXCLUDED == 'T';
            IsHematocritExcluded = record.IS_HEMATOCRIT_EXCLUDED == 'T';
            IsSampleExcluded = record.IS_SAMPLE_EXCLUDED == 'T';
            LiveTime = record.LIVE_TIME;
            PositionRecord = new PositionRecord()
                                      {
                                          SampleFunction = record.POSITION_NAME,
                                          Position = record.SAMPLE_POSITION
                                      };
            PresetLiveTime = record.PRESET_LIVE_TIME;
            RealTime = record.REAL_TIME;
            RoiHighBound = record.ROI_HIGH_BOUND;
            RoiLowBound = record.ROI_LOW_BOUND;
            SampleHeaderTimestamp = record.SAMPLE_HEADER_TIMESTAMP;
            SampleId = record.SAMPLE_ID;
            SampleLayoutId = record.SC_LAYOUT_ID;
            TestId = record.TEST_ID;
            TimePostInjection = record.TIME_POST_INJECTION;

            SpectrumToArray(record.SPECTRUM);
        }

        public BvaSampleRecord(GetBVATestSamplesResult record)
            : base()
        {
            BvaSampleTimestamp = record.BVA_SAMPLE_TIMESTAMP;
            Centroid = record.CENTROID;
            Counts = record.COUNTS;
            FWHM = record.FWHM;
            FWTM = record.FWTM;
            Hematocrit = record.HEMATOCRIT;
            IsCountExcluded = record.IS_COUNT_EXCLUDED == 'T';
            IsCountAutoExcluded = record.IS_COUNT_AUTO_EXCLUDED == 'T';
            IsHematocritExcluded = record.IS_HEMATOCRIT_EXCLUDED == 'T';
            IsSampleExcluded = record.IS_SAMPLE_EXCLUDED == 'T';
            LiveTime = record.LIVE_TIME;
            PositionRecord = new PositionRecord()
            {
                SampleFunction = record.POSITION_NAME,
                Position = record.SAMPLE_POSITION
            };
            PresetLiveTime = record.PRESET_LIVE_TIME;
            RealTime = record.REAL_TIME;
            RoiHighBound = record.ROI_HIGH_BOUND;
            RoiLowBound = record.ROI_LOW_BOUND;
            SampleHeaderTimestamp = record.SAMPLE_HEADER_TIMESTAMP;
            SampleId = record.SAMPLE_ID;
            SampleLayoutId = record.SC_LAYOUT_ID;
            TestId = record.TEST_ID;
            TimePostInjection = record.TIME_POST_INJECTION;

            SpectrumToArray(record.SPECTRUM);
        }
    }
}
