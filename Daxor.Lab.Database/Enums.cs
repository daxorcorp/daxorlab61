﻿namespace Daxor.Lab.Database
{
    public enum Sex
    {
        Unknown = -1,
        Female = 0,
        Male = 1
    }
}
