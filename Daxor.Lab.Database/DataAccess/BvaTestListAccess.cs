﻿using System.Collections.Generic;
using Daxor.Lab.Database.CommonBLLs;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using System;
using System.Data.Linq;
using System.Linq;
using System.Data.SqlClient;


namespace Daxor.Lab.Database.DataAccess
{
    public static class BvaTestListAccess
    {
        public static BVATestInfoDTO Select(Guid key)
        {   
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            BVATestInfoDTO record = null;
            try
            {
                var result = context.GetBvaTestListItem(key);
                var test = result.FirstOrDefault<GetBvaTestListItemResult>();
                if (test != null)
                    record = new BVATestInfoDTO(test);
            }
            catch
            {
                return BvaTestListAccess.Select(key);
            }
            return record;
        

        }

        public static IEnumerable<BVATestInfoDTO> SelectList()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            IEnumerable<GetBvaTestListResult> result = context.GetBvaTestList();

            foreach (GetBvaTestListResult record in result)
            {
                yield return new BVATestInfoDTO(record);
            }

            yield break;
        }

 
    }
}
