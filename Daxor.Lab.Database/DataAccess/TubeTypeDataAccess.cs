﻿using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.DataAccess
{

    public static class TubeTypeDataAccess
    {
        public static TubeTypeRecord Select(int key)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            IEnumerable<GetTubeTypeDataResult> result = context.GetTubeTypeData(key);

            TubeTypeRecord record = null;

            var test = result.FirstOrDefault<GetTubeTypeDataResult>();
            if (test != null)
            {
                record = new TubeTypeRecord(test);
            }

            return record;
        }

        public static IEnumerable<TubeTypeRecord> SelectList()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            var result = context.GetTubeTypeList();

            foreach (GetTubeTypeListResult  record in result)
            {
                yield return new TubeTypeRecord(record);
            }

            yield break;
        }

        public static int GetTubeTypeID(string tubeType)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            return context.GetTubeTypeId(tubeType);
        }

    }
}
