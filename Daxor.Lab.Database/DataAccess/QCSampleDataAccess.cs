﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.Practices.Composite.Logging;


namespace Daxor.Lab.Database.DataAccess
{
    public class QCSampleDataAccess
    {
        public static QCSampleRecord Insert(QCSampleRecord classT, object concurrencyManagement, ILoggerFacade logger)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            Binary timeStamp = null;
            Dictionary<string, Binary> cm = (Dictionary<string, Binary>)concurrencyManagement;

            try
            {
                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction(System.Data.IsolationLevel.Serializable);
                int result = context.InsertSampleHeader(classT.SampleId, classT.PositionRecord.Position, classT.SampleLayoutId,
                    classT.PresetLiveTime, classT.LiveTime, classT.RealTime, classT.SpectrumToString(), ref timeStamp);

                if (result != 1)
                    throw new DBInsertFailedException("Insert failed for sample - " + classT.PositionRecord.Position + " ROWCOUNT = " + result);
                
                else
                    classT.SampleHeaderTimestamp = timeStamp;

                result = context.InsertQCSample(classT.SampleId, classT.Counts, classT.FWHM, classT.FWTM, classT.Centroid, classT.Pass != null ? (bool)classT.Pass ? 'T' : 'F' : ' ',
                   null, classT.Isotope, classT.Activity, classT.Accuracy, classT.HalfLife, classT.CountLimit, classT.RoiHighBound,
                   classT.RoiLowBound, classT.SerialNumber, classT.ErrorDetails, ref timeStamp);
                
                if (result != 1)
                    throw new DBInsertFailedException("Insert failed for sample - " + classT.PositionRecord.Position + " ROWCOUNT = " + result);
                
                else
                    classT.QCSampleTimestamp = timeStamp;

                context.InsertTestSamples(classT.SampleId, classT.TestId);
                context.Transaction.Commit();
                
                return classT;
            }
            catch (DBInsertFailedException ex)
            {
                context.Transaction.Rollback();
                logger.Log(ex.Message, Category.Exception, Priority.High);
                throw;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                logger.Log("Error occurred while inserting QC Sample: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
            finally
            {
                context.Connection.Close();
            }
        }

        public static void Update(QCSampleRecord sampleRecord, object concurrencyManagement, ILoggerFacade logger)
        {

            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            Binary timeStamp = null;
            Dictionary<string, Binary> cm = (Dictionary<string, Binary>)concurrencyManagement;

            try
            {
                context.Connection.Open();
                int result = 0;
                context.Transaction = context.Connection.BeginTransaction(System.Data.IsolationLevel.Serializable);

                result = context.UpdateSampleHeader(sampleRecord.SampleId, sampleRecord.PresetLiveTime, sampleRecord.LiveTime, 
                                    sampleRecord.RealTime, sampleRecord.SpectrumToString(), null, ref timeStamp);

                if (result != 1)
                    throw new DBUpdateFailedException("Update failed for sample - " + sampleRecord.PositionRecord.Position + " ROWCOUNT = " + result);

                else
                {
                    sampleRecord.SampleHeaderTimestamp = timeStamp;
                    if (cm != null)
                        cm[MemberName.AsString(() => sampleRecord.SampleHeaderTimestamp) + " Position " + sampleRecord.PositionRecord.Position] = sampleRecord.SampleHeaderTimestamp;
                }

                result = context.UpdateQCSample(sampleRecord.SampleId, sampleRecord.Counts, sampleRecord.FWHM, sampleRecord.FWTM,
                            sampleRecord.Centroid, sampleRecord.Pass != null ? (bool)sampleRecord.Pass ? 'T' : 'F' : ' ', null, 
                            sampleRecord.Isotope, sampleRecord.Activity, sampleRecord.Accuracy, sampleRecord.HalfLife, sampleRecord.CountLimit, 
                            sampleRecord.RoiHighBound, sampleRecord.RoiLowBound, sampleRecord.SerialNumber, sampleRecord.ErrorDetails, null, ref timeStamp);

                if (result != 1)
                    throw new DBUpdateFailedException("Update failed for sample - " + sampleRecord.PositionRecord.Position + " ROWCOUNT = " + result);

                else
                {
                    sampleRecord.QCSampleTimestamp = timeStamp;
                    if (cm != null)
                        cm[MemberName.AsString(() => sampleRecord.QCSampleTimestamp) + " Position " + sampleRecord.PositionRecord.Position] = sampleRecord.QCSampleTimestamp;
                }

                context.Transaction.Commit();
            }
            catch (DBUpdateFailedException ex)
            {
                context.Transaction.Rollback();
                logger.Log(ex.Message, Category.Exception, Priority.High);
                throw;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                logger.Log("Error occurred while updating QC sample: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
            finally
            {
                context.Connection.Close();
            }

        }

        public static void Delete(Guid key)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            context.DeleteQCSample(key);
        }


        public static QCSampleRecord Select(Guid sampleID, object concurrencyManagement)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            var result = context.GetQCTestSample(sampleID);

            QCSampleRecord record = null;

            var test = result.FirstOrDefault<GetQCTestSampleResult>();
            if (test != null && test.SAMPLE_ID != Guid.Empty)
                record = new QCSampleRecord(test);

            return record;
        }


        public static IEnumerable<QCSampleRecord> SelectList(Guid key)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            IEnumerable<GetQCTestSamplesResult> result = context.GetQCTestSamples(key);

            foreach (GetQCTestSamplesResult record in result)
            {
                yield return new QCSampleRecord(record);
            }

            yield break;
        }
    }
}
