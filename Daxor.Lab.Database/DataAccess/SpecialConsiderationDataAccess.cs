﻿using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.DataAccess
{

    public static class SpecialConsiderationsDataAccess 
    {
        public static IEnumerable<SpecialConsiderationsRecord> SelectList()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            var result = context.GetSpecialConsiderationsList();

            foreach (GetSpecialConsiderationsListResult record in result)
            {
                yield return new SpecialConsiderationsRecord(record);
            }

            yield break;
        }
    }
}
