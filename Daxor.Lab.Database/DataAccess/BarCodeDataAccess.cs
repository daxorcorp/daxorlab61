﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.DataAccess
{
    public class BarCodeDataAccess
    {
        public static BarCodeRecord Insert(BarCodeRecord classT)
        {
            if (classT == null) return classT;

            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            Binary timeStamp = null;
            int result = context.InsertBarCode(classT.TestId, classT.BID, classT.PositionId, ref timeStamp);

            if (result != 1)
            {
                throw new DBInsertFailedException("Insert failed for Barcode - " + classT.BID + " ROWCOUNT = " + result);
            }
            else
            {
                classT.Timestamp = timeStamp;
            }

            return classT;
        }

        public static IEnumerable<BarCodeRecord> SelectList(Guid key)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            IEnumerable<GetBarcodeDataResult> result = context.GetBarcodeData(key);

            foreach (GetBarcodeDataResult record in result)
            {
                yield return new BarCodeRecord(record);
            }

            yield break;
            
         }
    }
}
