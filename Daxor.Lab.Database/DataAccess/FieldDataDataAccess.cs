﻿using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.DataAccess
{
    public class BvaFieldDataDataAccess
    {
        public static void Update(FieldDataRecord classT)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            context.UpdateBVARequiredFields(classT.FieldName, classT.IsSystemRequired ? 'T' : 'F',
                classT.IsCustomerRequired ? 'T' : 'F', classT.MissingValue);
        }

        public static IEnumerable<FieldDataRecord> SelectList()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            var result = context.GetBVATestRequiredFieldsList();

            foreach (GetBVATestRequiredFieldsListResult record in result)
            {
                yield return new FieldDataRecord(record);
            }

            yield break;
        }

    }
}
