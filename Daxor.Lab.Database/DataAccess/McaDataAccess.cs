﻿using System;
using System.Data.Linq;
using System.Linq;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.DataAccess
{

    public static class McaDataAccess 
    {
        public static McaDataRecord Insert(McaDataRecord classT)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            Binary timeStamp = null;

            int result = context.InsertMCAData(classT.TestId, classT.FineGain, classT.Voltage, ref timeStamp);
            if (result != 1)
            {
                throw new DBInsertFailedException("Insert failed for MCA Data - " + classT.TestId + " ROWCOUNT = " + result);
            }
            else
            {
                classT.Timestamp = timeStamp;
            }

            return classT;
        }

        public static void Update(McaDataRecord classT)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            Binary timeStamp = null;

            int result = context.UpdateMCAData(classT.TestId, classT.FineGain, classT.Voltage, classT.Timestamp, ref timeStamp);
            if (result != 1)
            {
                throw new DBUpdateFailedException("Update failed for MCA Data - " + classT.TestId + " ROWCOUNT = " + result);
            }
            else
            {
                classT.Timestamp = timeStamp;
            }

        }

        public static void Delete(Guid key)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            context.DeleteMCAData(key);
        }

        public static McaDataRecord Select(Guid key)
        {

            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            var result = context.GetMCAData(key);
            var test = result.FirstOrDefault<GetMCADataResult>();
             
            McaDataRecord record = null;

            if (test != null && test.TEST_ID != Guid.Empty)
            {
                record = new McaDataRecord(test);
            }

            return record;
        }
    }
}
