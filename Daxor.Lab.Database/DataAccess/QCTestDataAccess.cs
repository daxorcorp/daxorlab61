﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Database.DataAccess
{
    public class QCTestDataAccess
    {
        private const int QcTestType = 3;

        public static QCTestRecord Insert(QCTestRecord testRecord, object concurrencyManagement, ILoggerFacade logger)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            Binary timeStamp = null;
            Dictionary<string, Binary> cm = (Dictionary<string, Binary>)concurrencyManagement;

            try
            {
                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction();

                int result = context.InsertTestHeader(testRecord.TestId, DateTime.Now, QcTestType, testRecord.Analyst != null ? testRecord.Analyst : string.Empty,
                    testRecord.Comment != null ? testRecord.Comment : string.Empty, testRecord.BackgroundCount, testRecord.BackgroundTime, testRecord.SampleLayoutId,
                    testRecord.UniqueSystemId, ref timeStamp);

                if (result != 1)
                    throw new DBInsertFailedException("InsertTestHeader failed for test - " + testRecord.TestId + " ROWCOUNT = " + result);
                else
                {
                    testRecord.TestHeaderTimestamp = timeStamp;
                    if (cm != null)
                        cm.Add(MemberName.AsString(() => testRecord.TestHeaderTimestamp), testRecord.TestHeaderTimestamp);
                }

                result = context.InsertQCTest(testRecord.TestId, testRecord.InjectateLot,
                    testRecord.QCTestType, testRecord.InitialFineGain, testRecord.EndFineGain, testRecord.HighVoltage, testRecord.CalibrationCurveId,
                    ref timeStamp);

                if (result != 1)
                    throw new DBInsertFailedException("InsertQCTest failed for test - " + testRecord.TestId + " ROWCOUNT = " + result);
                
                else
                {
                    testRecord.QCTestTimestamp = timeStamp;
                    if (cm != null)
                        cm.Add(MemberName.AsString(() => testRecord.QCTestTimestamp), testRecord.QCTestTimestamp);
                }

                //Insert barcodes if relevant
                InsertBarcodeIfRelevant(testRecord, context);


                context.Transaction.Commit();
                if (testRecord.StatusId != (int)TestStatus.Pending)
                    Update(testRecord, concurrencyManagement, logger);
                
                return testRecord;
            }
            catch (DBInsertFailedException ex)
            {
                context.Transaction.Rollback();
                logger.Log(ex.Message, Category.Exception, Priority.High);
                throw;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                logger.Log("Error occurred while inserting QC Test: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
            finally
            {
                context.Connection.Close();
            }

            
        }
        public static void Update(QCTestRecord testRecord, object concurrencyManagement, ILoggerFacade logger)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            Binary timeStamp = null;
            Dictionary<string, Binary> cm = (Dictionary<string, Binary>)concurrencyManagement;

            try
            {
                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction();
                string s = MemberName.AsString(() => testRecord.TestHeaderTimestamp);

                int result = context.UpdateTestHeader(testRecord.TestId, testRecord.StatusId, testRecord.TestDate, QcTestType,
                   testRecord.Analyst, testRecord.Comment == null ? string.Empty : testRecord.Comment, testRecord.BackgroundCount,
                   testRecord.BackgroundTime,
                   null, ref timeStamp);

                if (result != 1)
                    throw new DBUpdateFailedException("UpdateTestHeader failed for test - " + testRecord.TestId + " ROWCOUNT = " + result);
                else
                {
                    testRecord.TestHeaderTimestamp = timeStamp;
                    if (cm != null)
                        cm[MemberName.AsString(() => testRecord.TestHeaderTimestamp)] = testRecord.TestHeaderTimestamp;
                }

                context.UpdateQCTest(testRecord.TestId, testRecord.InjectateLot,
                  testRecord.Pass ? 'T' : 'F', testRecord.InitialFineGain, testRecord.EndFineGain, testRecord.HighVoltage, testRecord.CalibrationCurveId,
                   null, ref timeStamp);

                if (result != 1)
                    throw new DBUpdateFailedException("UpdateQCTest failed for test - " + testRecord.TestId + " ROWCOUNT = " + result);

                else
                {
                    testRecord.QCTestTimestamp = timeStamp;
                    if (cm != null)
                        cm[MemberName.AsString(() => testRecord.QCTestTimestamp)] = testRecord.QCTestTimestamp;
                }

                context.Transaction.Commit();
            }
            catch (DBUpdateFailedException ex)
            {
                context.Transaction.Rollback();
                logger.Log(ex.Message, Category.Exception, Priority.High);
                throw;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                logger.Log("Error occurred while updating QC Test: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
            finally
            {
                context.Connection.Close();
            }
        }
    
        public static void Delete(Guid key)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            context.DeleteTestHeader(key);
        }
        public static QCTestRecord Select(Guid key)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            var result = context.GetQCTestData(key);
            QCTestRecord record = null;

            var test = result.FirstOrDefault();
            if (test != null && test.TEST_ID != Guid.Empty)
            {
                record = new QCTestRecord(test);
                Dictionary<string, Binary> cm = (Dictionary<string, Binary>)record.ConcurrencyObject;

                if (cm.ContainsKey(MemberName.AsString(() => record.TestHeaderTimestamp)))
                    cm[MemberName.AsString(() => record.TestHeaderTimestamp)] = record.TestHeaderTimestamp;
                else
                    cm.Add(MemberName.AsString(() => record.TestHeaderTimestamp), record.TestHeaderTimestamp);

                if (cm.ContainsKey(MemberName.AsString(() => record.QCTestTimestamp)))
                    cm[MemberName.AsString(() => record.QCTestTimestamp)] = record.QCTestTimestamp;
                else
                    cm.Add(MemberName.AsString(() => record.QCTestTimestamp), record.QCTestTimestamp);

                LoadBarcodes(record, cm);
            }

            return record;
        }
        /// <summary>
        /// Inserts Barcode record if it needs to. Assumes that a transaction is in progress.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="context"></param>
        /// <remarks>Assumes that a transaction is in progress</remarks>
        private static void InsertBarcodeIfRelevant(QCTestRecord record, DaxorLabDatabaseLinqDataContext context)
        {
            Binary timeStamp = null;
            int result;

            if (record.StandardABarcode != null)
            {
                result = context.InsertBarCode(record.TestId, record.StandardABarcode, 2, ref timeStamp);
                if (result != 1)
                    throw new DBInsertFailedException("Insert failed for StandardA barcode - " + record.StandardABarcode + " ROWCOUNT = " + result);

            }
            if (record.StandardBBarcode != null)
            {
                result = context.InsertBarCode(record.TestId, record.StandardBBarcode, 3, ref timeStamp);
                if (result != 1)
                    throw new DBInsertFailedException("Insert failed for StandardB barcode - " + record.StandardBBarcode + " ROWCOUNT = " + result);
            }
        }
        private static void LoadBarcodes(QCTestRecord record, Dictionary<string, Binary> concurrencyManagement)
        {
            IEnumerable<BarCodeRecord> barcodes = BarCodeDataAccess.SelectList(record.TestId);

            foreach (BarCodeRecord barcode in barcodes)
            {
                switch (barcode.PositionId)
                {
                    case 1:
                        break;
                    case 2:
                        record.StandardABarcode = barcode.BID;
                        break;
                    case 3:
                        record.StandardBBarcode = barcode.BID;
                        break;
                }
            }
        }
    }
}
