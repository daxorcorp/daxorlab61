﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.DataAccess
{
    public static class AlertsDataAccess
    {
        public static IEnumerable<AlertRecord> SelectList()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            IEnumerable<GetAlertDataResult> result = context.GetAlertData();

            foreach (GetAlertDataResult record in result)
            {
                yield return new AlertRecord(record);
            }

            yield break;
        }
 
    }
}
