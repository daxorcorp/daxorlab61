﻿using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.DataAccess
{
    public static class HospitalPatientIdDataAccess
    {
        public static IEnumerable<HospitalPatientIdRecord> SelectList()
        {
            var context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            IEnumerable<GetPatientHospitalIDListResult> patientHospitalIdListResults = context.GetPatientHospitalIDList();

            foreach (var record in patientHospitalIdListResults)
            {
                yield return new HospitalPatientIdRecord(record);
            }
        }
    }
}
