﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.Database.DataRecord;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Database.DataAccess
{
    public class BvaSampleDataAccess
    {
        public static BvaSampleRecord Insert(BvaSampleRecord classT, object concurrencyManagement, ILoggerFacade logger)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            Binary timeStamp = null;
            Dictionary<string, Binary> cm = (Dictionary<string, Binary>)concurrencyManagement;

            try
            {
                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction();
                int result = context.InsertSampleHeader(classT.SampleId, classT.PositionRecord.Position, classT.SampleLayoutId,
                    classT.PresetLiveTime, classT.LiveTime, classT.RealTime, classT.SpectrumToString(), ref timeStamp);
                
                if (result != 1)
                    throw new DBInsertFailedException("Insert failed for sample - " + classT.PositionRecord.Position + " ROWCOUNT = " + result);
                
                classT.SampleHeaderTimestamp = timeStamp;
                cm.Add(MemberName.AsString(() => classT.SampleHeaderTimestamp) + " Position " + classT.PositionRecord.Position, classT.SampleHeaderTimestamp);

                result = context.InsertBVASample(classT.SampleId, classT.IsSampleExcluded  ? 'T' : 'F', classT.IsCountExcluded  ? 'T' : 'F',
                    classT.IsCountAutoExcluded ? 'T' : 'F',
                    classT.IsHematocritExcluded ? 'T' : 'F',
                    classT.Hematocrit, classT.Counts, classT.FWHM, classT.FWTM, classT.Centroid,
                    classT.TimePostInjection, classT.RoiHighBound, classT.RoiLowBound, ref timeStamp);

                if (result != 1)
                    throw new DBInsertFailedException("Insert failed for sample - " + classT.PositionRecord.Position + " ROWCOUNT = " + result);

                classT.BvaSampleTimestamp = timeStamp;
                cm.Add(MemberName.AsString(() => classT.BvaSampleTimestamp) + " Position " + classT.PositionRecord.Position, classT.BvaSampleTimestamp);

                context.InsertTestSamples(classT.SampleId, classT.TestId);
                context.Transaction.Commit();
                return classT;
            }
            catch (DBInsertFailedException ex)
            {
                context.Transaction.Rollback();
                logger.Log(ex.Message, Category.Exception, Priority.High);
                throw;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                logger.Log("Error occurred while inserting BVA sample: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
            finally
            {
                context.Connection.Close();
            }
        }

        public static void Update(BvaSampleRecord sampleRecord, object concurrencyManagement, ILoggerFacade logger)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            Binary timeStamp = null;
            Dictionary<string, Binary> cm = (Dictionary<string, Binary>)concurrencyManagement;

            try
            {
                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction();
                int result = context.UpdateSampleHeader(sampleRecord.SampleId, sampleRecord.PresetLiveTime, sampleRecord.LiveTime, sampleRecord.RealTime, sampleRecord.SpectrumToString(),
                    cm[MemberName.AsString(() => sampleRecord.SampleHeaderTimestamp) + " Position " + sampleRecord.PositionRecord.Position], ref timeStamp);
                
                if (result != 1)
                    throw new DBUpdateFailedException("Update failed for sample - " + sampleRecord.PositionRecord.Position + " ROWCOUNT = " + result);
                
                sampleRecord.SampleHeaderTimestamp = timeStamp;
                cm[MemberName.AsString(() => sampleRecord.SampleHeaderTimestamp) + " Position " + sampleRecord.PositionRecord.Position] = sampleRecord.SampleHeaderTimestamp;

                result = context.UpdateBVASample(sampleRecord.SampleId, sampleRecord.IsSampleExcluded ? 'T' : 'F',
                    sampleRecord.IsCountExcluded ? 'T' : 'F', 
                    sampleRecord.IsCountAutoExcluded ? 'T' : 'F', sampleRecord.IsHematocritExcluded  ? 'T' : 'F',
                    sampleRecord.Hematocrit, sampleRecord.Counts, sampleRecord.FWHM, sampleRecord.FWTM, sampleRecord.Centroid, sampleRecord.TimePostInjection, 
                    sampleRecord.RoiHighBound, sampleRecord.RoiLowBound,
                    cm[MemberName.AsString(() => sampleRecord.BvaSampleTimestamp) + " Position " + sampleRecord.PositionRecord.Position], ref timeStamp);
                
                if (result != 1)
                    throw new DBUpdateFailedException("Update failed for sample - " + sampleRecord.PositionRecord.Position + " ROWCOUNT = " + result);

                sampleRecord.BvaSampleTimestamp = timeStamp;
                cm[MemberName.AsString(() => sampleRecord.BvaSampleTimestamp) + " Position " + sampleRecord.PositionRecord.Position] = sampleRecord.BvaSampleTimestamp;

                context.Transaction.Commit();
            }
            catch (DBUpdateFailedException ex)
            {
                context.Transaction.Rollback();
                logger.Log(ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                logger.Log("Error occurred while updating BVA sample: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
            finally
            {
                context.Connection.Close();
            }
        }

        public static void Delete(Guid key, ILoggerFacade logger)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            try
            {
                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction();
                context.DeleteBVASample(key);
                context.DeleteTestSamples(key);
                context.DeleteSampleHeader(key);

                context.Transaction.Commit();
            }
            catch(Exception ex)
            {
                context.Transaction.Rollback();
                logger.Log("Error occurred while deleting BVA samples: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
            finally
            {
                context.Connection.Close();
            }
        }

        public static BvaSampleRecord Select(Guid sampleId, object concurrencyManagement, ILoggerFacade logger)
        {
            var context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            try
            {
                var result = context.GetBVATestSample(sampleId);

                BvaSampleRecord record = null;

                var test = result.FirstOrDefault();
                if (test != null && test.SAMPLE_ID != Guid.Empty)
                {
                    record = new BvaSampleRecord(test);
                    var cm = (Dictionary<string, Binary>)concurrencyManagement;
                    if (cm.ContainsKey(MemberName.AsString(() => record.SampleHeaderTimestamp) + " Position " + record.PositionRecord.Position))
                        cm[MemberName.AsString(() => record.SampleHeaderTimestamp) + " Position " + record.PositionRecord.Position] = record.SampleHeaderTimestamp;
                    else
                        cm.Add(MemberName.AsString(() => record.SampleHeaderTimestamp) + " Position " + record.PositionRecord.Position, record.SampleHeaderTimestamp);

                    if (cm.ContainsKey(MemberName.AsString(() => record.BvaSampleTimestamp) + " Position " + record.PositionRecord.Position))
                        cm[MemberName.AsString(() => record.BvaSampleTimestamp) + " Position " + record.PositionRecord.Position] = record.BvaSampleTimestamp;
                    else
                        cm.Add(MemberName.AsString(() => record.BvaSampleTimestamp) + " Position " + record.PositionRecord.Position, record.BvaSampleTimestamp);
                }
                return record;
            }
            catch (Exception ex)
            {
                logger.Log("Error occurred while selecting BVA sample: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
        }
    

        public static IEnumerable<BvaSampleRecord> SelectList(Guid key)
        {
            var context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            IEnumerable<GetBVATestSamplesResult> result = context.GetBVATestSamples(key);

            foreach (GetBVATestSamplesResult record in result)
            {
                yield return new BvaSampleRecord(record);
            }
        }
    }
}
