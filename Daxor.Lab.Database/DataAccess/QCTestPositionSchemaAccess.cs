﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;

namespace Daxor.Lab.Database.DataAccess
{
    public static class QCTestPositionSchemaAccess
    {
        public enum QCTestType
        {
            Calibration,
            Standards,
            ContaminationFull,
            Linearity
        }

        public static IEnumerable<PositionRecord> SelectList(QCTestType type)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            List<PositionRecord> positionRecords = new List<PositionRecord>();
            switch(type){

                case QCTestType.Calibration:
                    context.GetQCCalibrationTestPositionList().ToList().ForEach((r)=>positionRecords.Add(new PositionRecord(r)));
                    break;

                case QCTestType.ContaminationFull:
                    context.GetQCContaminationTestPositionList().ToList().ForEach((r)=>positionRecords.Add(new PositionRecord(r)));
                    break;

                case QCTestType.Linearity:
                    context.GetQCLinearityTestPositionList().ToList().ForEach((r)=>positionRecords.Add(new PositionRecord(r)));
                    break;

                case QCTestType.Standards:
                    context.GetQCStandardsTestPositionList().ToList().ForEach((r)=>positionRecords.Add(new PositionRecord(r)));
                    break;

             
            }

            return positionRecords;
        }
    }
}
