﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;

namespace Daxor.Lab.Database.DataAccess
{
    public static class QCTestItemsDataAccess
    {
        public static IQueryable<GetQCTestItemsResult> Select()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            return context.GetQCTestItems();
        }
    }
}
