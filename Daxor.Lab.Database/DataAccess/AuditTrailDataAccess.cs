﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Interfaces;

namespace Daxor.Lab.Database.DataAccess
{
    public class AuditTrailDataAccess : IAuditTrailDataAccess
    {
        private readonly DaxorLabDatabaseLinqDataContext _context;

        public AuditTrailDataAccess(DaxorLabDatabaseLinqDataContext context)
        {
            _context = context;
        }
   
        public AuditTrailRecord Insert(AuditTrailRecord classT)
        {
            DateTime tempTime;
            DateTime? changeTime;

            if (DateTime.TryParse(classT.ChangeTime, out tempTime))
                changeTime = tempTime;
            else
                changeTime = null;
            
            _context.InsertAuditTrail(classT.TestId, classT.ChangeTable, classT.ChangeField, classT.OldValue, classT.NewValue, classT.ChangedBy, changeTime);
            return classT;
        }

        public IEnumerable<AuditTrailRecord> SelectList(Guid key)
        {
            IEnumerable<GetAuditTrailResult> result = _context.GetAuditTrail(key);

            foreach(var record in result)
            {
                 yield return new AuditTrailRecord(record);
            }
        }
    }
}
