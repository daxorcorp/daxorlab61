﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Database.DataAccess
{
 
    public class BvaTestDataAccess
    {
        private const int BvaTestType = 1;

        public static BvaTestRecord Insert(BvaTestRecord testRecord, object concurrencyManagement, ILoggerFacade logger)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            Binary timeStamp = null;
            Dictionary<string, Binary> cm = (Dictionary<string, Binary>)concurrencyManagement;

            try
            {
                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction(System.Data.IsolationLevel.Serializable);

                int result = context.InsertTestHeader(testRecord.TestId, DateTime.Now, BvaTestType, testRecord.Analyst ?? string.Empty,
                    testRecord.Comment ?? string.Empty, testRecord.BackgroundCount, testRecord.BackgroundTime, testRecord.SampleLayoutId,
                    testRecord.UniqueSystemId, ref timeStamp);

                if (result != 1)
                    throw new DBInsertFailedException("InsertTestHeader failed for test - " + testRecord.TestId + " ROWCOUNT = " + result);
                else
                {
                    testRecord.TestHeaderTimestamp = timeStamp;
	                // ReSharper disable once AccessToModifiedClosure
                    cm.Add(MemberName.AsString(() => testRecord.TestHeaderTimestamp), testRecord.TestHeaderTimestamp);
                }

                result = context.InsertBVATest(testRecord.TestId, testRecord.PID, testRecord.ID2, testRecord.Location, testRecord.BloodDrawCount,
                    testRecord.PatientHeight, testRecord.PatientWeight, testRecord.Dose, testRecord.InjectateLot, testRecord.ReferringPhysician,
                    testRecord.PhysiciansSpecialty, testRecord.CCReportTo, testRecord.HematocritFillType, testRecord.TestMode, testRecord.MeasurementSystem, testRecord.TubeId,
                    testRecord.ConsiderationId, testRecord.HasSufficientData ? 'T' : 'F', testRecord.SampleDurationInSeconds,
                    ref timeStamp, testRecord.AmputeeIdealsCorrectionFactor);
                if (result != 1)
                    throw new DBInsertFailedException("InsertBVATest failed for test - " + testRecord.TestId + " ROWCOUNT = " + result);

                else
                {
                    testRecord.BvaTestTimestamp = timeStamp;
	                // ReSharper disable once AccessToModifiedClosure
                    cm.Add(MemberName.AsString(() => testRecord.BvaTestTimestamp), testRecord.BvaTestTimestamp);
                }

                result = context.InsertTestAtt("PACS", testRecord.TestId, testRecord.PACS, ref timeStamp);
                if (result != 1)
                    throw new DBInsertFailedException("InsertTestAtt failed for test - " + testRecord.TestId + " ROWCOUNT = " + result);
                else
                {
                    testRecord.PacsTimestamp = timeStamp;
	                // ReSharper disable once AccessToModifiedClosure
                    cm.Add(MemberName.AsString(() => testRecord.PacsTimestamp), testRecord.PacsTimestamp);
                }


                context.Transaction.Commit();
                if (testRecord.StatusId != (int)TestStatus.Pending)
                    Update(testRecord, concurrencyManagement, logger);

                testRecord = Select(testRecord.TestId);
                return testRecord;
            }
            catch (DBInsertFailedException ex)
            {
                context.Transaction.Rollback();
                logger.Log(ex.Message, Category.Exception, Priority.High);
                throw;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                logger.Log("Error occurred while inserting BVA test: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
            finally
            {
                context.Connection.Close();
            }
        }

        public static void Update(BvaTestRecord testRecord, object concurrencyManagement, ILoggerFacade logger)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            Binary timeStamp = null;
            Dictionary<string, Binary> cm = (Dictionary<string, Binary>)concurrencyManagement;

            try
            {
                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction();

                string s = MemberName.AsString(() => testRecord.TestHeaderTimestamp);
                int result = context.UpdateTestHeader(testRecord.TestId, testRecord.StatusId, testRecord.TestDate, BvaTestType,
                    testRecord.Analyst, testRecord.Comment ?? string.Empty, testRecord.BackgroundCount, testRecord.BackgroundTime, cm[s],
                    ref timeStamp);
                
                if (result != 1)
                    throw new DBUpdateFailedException("UpdateTestHeader failed for test - " + testRecord.TestId + " ROWCOUNT = " + result);
                
                else
                {
                    testRecord.TestHeaderTimestamp = timeStamp;
                    cm[MemberName.AsString(() => testRecord.TestHeaderTimestamp)] = testRecord.TestHeaderTimestamp;
                }

                context.UpdateBVATest(testRecord.TestId, testRecord.PID, testRecord.ID2, testRecord.Location,
                    testRecord.BloodDrawCount, testRecord.PatientHeight, testRecord.PatientWeight, testRecord.Dose, testRecord.InjectateLot,
                    testRecord.ReferringPhysician, testRecord.PhysiciansSpecialty, testRecord.CCReportTo, testRecord.HematocritFillType, testRecord.TestMode, testRecord.MeasurementSystem,
                    testRecord.TubeId, testRecord.ConsiderationId, testRecord.HasSufficientData ? 'T' : 'F', testRecord.SampleDurationInSeconds,
                    cm[MemberName.AsString(() => testRecord.BvaTestTimestamp)], ref timeStamp, testRecord.AmputeeIdealsCorrectionFactor);
                
                if (result != 1)
                    throw new DBUpdateFailedException("UpdateBVATest failed for test - " + testRecord.TestId + " ROWCOUNT = " + result);
                else
                {
                    testRecord.BvaTestTimestamp = timeStamp;
                    cm[MemberName.AsString(() => testRecord.BvaTestTimestamp)] = testRecord.BvaTestTimestamp;
                }

                context.UpdateTestAtt("PACS", testRecord.TestId, testRecord.PACS, testRecord.PacsTimestamp, ref timeStamp);

                if (result != 1)
                    throw new DBUpdateFailedException("UpdateTestAtt failed for test - " + testRecord.TestId + " ROWCOUNT = " + result);
                else
                {
                    testRecord.PacsTimestamp = timeStamp;
                    cm[MemberName.AsString(() => testRecord.PacsTimestamp)] = testRecord.PacsTimestamp;
                }
                context.Transaction.Commit();
            }
            catch (DBUpdateFailedException ex)
            {
                context.Transaction.Rollback();
                logger.Log(ex.Message, Category.Exception, Priority.High);
                throw;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                logger.Log("Error occurred while updating BVA Test record: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
            finally
            {
                context.Connection.Close();
            }
        }

        public static void Delete(Guid key)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            context.DeleteTestHeader(key);
        }

        public static BvaTestRecord Select(Guid key)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            var result = context.GetBVATestData(key);
            BvaTestRecord record = null;

            var test = result.FirstOrDefault();
            if (test != null && test.TEST_ID != Guid.Empty)
            {
                record = new BvaTestRecord(test);
                Dictionary<string, Binary> cm = (Dictionary<string, Binary>)record.ConcurrencyObject;

                if (cm.ContainsKey(MemberName.AsString(() => record.TestHeaderTimestamp)))
                    cm[MemberName.AsString(() => record.TestHeaderTimestamp)] = record.TestHeaderTimestamp;
                else
                    cm.Add(MemberName.AsString(() => record.TestHeaderTimestamp), record.TestHeaderTimestamp);

                if (cm.ContainsKey(MemberName.AsString(() => record.BvaTestTimestamp)))
                    cm[MemberName.AsString(() => record.BvaTestTimestamp)] = record.BvaTestTimestamp;
                else
                    cm.Add(MemberName.AsString(() => record.BvaTestTimestamp), record.BvaTestTimestamp);

                if (cm.ContainsKey(MemberName.AsString(() => record.PacsTimestamp)))
                    cm[MemberName.AsString(() => record.PacsTimestamp)] = record.PacsTimestamp;
                else
                    cm.Add(MemberName.AsString(() => record.PacsTimestamp), record.PacsTimestamp);
 
                LoadBarcodes(record, cm);
            }

            return record;
        }

        /// <summary>
        /// Returns whether or not if there are tests that have been ran on the patient with InternalId
        /// </summary>
        /// <param name="internalId"></param>
        /// <returns>True if there are tests that have been ran on the patient, false otherwise.</returns>
        /// <remarks>If internalId is Guid.Empty, this will return true</remarks>
        public static bool AreThereAnyTestsThatRanOnPatient(Guid internalId)
        {
            if (internalId == Guid.Empty)
                return true;

            var context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            var result = context.GetTestsRanOnPatient(internalId);
            var test = result.FirstOrDefault();

            return (test != null && test.TEST_ID != Guid.Empty);
        }

	    // ReSharper disable once UnusedParameter.Local
        private static void LoadBarcodes(BvaTestRecord record, Dictionary<string, Binary> concurrencyManagement)
        {
            IEnumerable<BarCodeRecord> barcodes = BarCodeDataAccess.SelectList(record.TestId);

            foreach (BarCodeRecord barcode in barcodes)
            {
                switch (barcode.PositionId)
                {
                    case 1:
                        record.InjectateBarcode = barcode.BID;
                        break;
                    case 2:
                        record.StandardABarcode = barcode.BID;
                        break;
                    case 3:
                        record.StandardBBarcode = barcode.BID;
                        break;
                }
            }
        }
     }
}
