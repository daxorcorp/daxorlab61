﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Linq;
using System.Linq;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Microsoft.Practices.Composite.Logging;
// ReSharper disable InconsistentNaming

namespace Daxor.Lab.Database.DataAccess
{
    public static class PatientDataAccess
    {
        public static PatientRecord Insert(PatientRecord patient, object concurrencyManagement, ILoggerFacade logger)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            try
            {
                context.Connection.Open();
                context.Transaction = context.Connection.BeginTransaction(IsolationLevel.Serializable);
                string sex;

                switch (patient.Gender)
                {
                    case Sex.Male:
                        sex = "M";
                        break;
                    case Sex.Female:
                        sex = "F";
                        break;
                    default:
                        sex = "U";
                        break;
                }

                Binary timeStamp = null;
                int result = context.InsertPatient(
                    patient.PID,
                    patient.PatientHospitalId,
                    patient.FirstName ?? string.Empty,
                    patient.MiddleName ?? string.Empty,
                    patient.LastName ?? string.Empty,
                    sex.ToCharArray()[0],
                    patient.DOB ?? new DateTime(1753, 1, 1),
					ref timeStamp,
					patient.IsAmputee
                    );

                if (result != 1)
                    throw new DBInsertFailedException("Insert failed for Patient - " + patient.PatientHospitalId);
                else
                {

                    patient.Timestamp = timeStamp;
                    Dictionary<string, Binary> cm = (Dictionary<string, Binary>)concurrencyManagement;
                    if (cm.ContainsKey("PatientTimestamp"))
                        cm["PatientTimestamp"] = patient.Timestamp;
                    else
                        cm.Add("PatientTimestamp", patient.Timestamp);
                }

                context.Transaction.Commit();
            }
            catch (DBInsertFailedException ex)
            {
                context.Transaction.Rollback();
                logger.Log(ex.Message, Category.Exception, Priority.High);
                throw;
            }
            catch (Exception ex)
            {
                context.Transaction.Rollback();
                logger.Log("Error occurred while inserting Patient record: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
            finally
            {
                context.Connection.Close();
            }
            return patient;
        }

        public static void Update(PatientRecord patient, object concurrencyManagement)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            Dictionary<string, Binary> cm = (Dictionary<string, Binary>)concurrencyManagement;

            string sex;

            switch (patient.Gender)
            {
                case Sex.Male:
                    sex = "M";
                    break;
                case Sex.Female:
                    sex = "F";
                    break;
                default:
                    sex = "U";
                    break;
            }

            Binary timeStamp = null;
            int result = context.UpdatePatient(
                patient.PID,
                patient.PatientHospitalId,
                patient.FirstName ?? string.Empty,
                patient.MiddleName ?? string.Empty,
                patient.LastName ?? string.Empty,
                sex.ToCharArray()[0],
                patient.DOB ?? new DateTime(1753, 1, 1),
                cm["PatientTimestamp"],
                ref timeStamp,
                patient.IsAmputee
            );

            if (result != 1)
                throw new DBUpdateFailedException("Update failed for Patient - " + patient.PatientHospitalId);
            else 
            {
                patient.Timestamp = timeStamp;
                cm["PatientTimestamp"] = patient.Timestamp;
            }
        }

        public static void Delete(Guid patientID)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            context.DeletePatient(patientID);
        }

        public static PatientRecord Select(Guid patientID, object concurrencyManagement)
        {
            var context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            
            IEnumerable<GetPatientDataResult> result = context.GetPatientData(patientID);
            
            PatientRecord record = null;

            var test = result.FirstOrDefault();
            if (test != null && test.PID != Guid.Empty)
            {
                record = new PatientRecord(test);
                Dictionary<string, Binary> cm = (Dictionary<string, Binary>)concurrencyManagement;
                
                if (cm.ContainsKey("PatientTimestamp"))
                    cm["PatientTimestamp"] = record.Timestamp;
                else
                    cm.Add("PatientTimestamp", record.Timestamp);
            }
 
            return record;
        }

        public static PatientRecord Select(string patientHospitalID, object concurrencyManagement)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            IEnumerable<GetPatientDataFromHospitalIDResult> result = context.GetPatientDataFromHospitalID(patientHospitalID);

            PatientRecord record = null;

            var test = result.FirstOrDefault();
            if (test != null && test.PID != Guid.Empty)
            {
                record = new PatientRecord(test);
                Dictionary<string, Binary> cm = (Dictionary<string, Binary>)concurrencyManagement;

                if (cm.ContainsKey("PatientTimestamp"))
                    cm["PatientTimestamp"] = record.Timestamp;
                else
                    cm.Add("PatientTimestamp", record.Timestamp);
            }

            return record;
        }

        public static IEnumerable<PatientRecord> SelectList()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            var result = context.GetAllPatientData();

            return result.Select(record => new PatientRecord(record));
        }

    }

}
