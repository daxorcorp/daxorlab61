﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.DataAccess
{
    public static class SpecificAlertsDataAccess
    {
        public static IEnumerable<AlertRecord> SelectList(string key)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            IEnumerable<GetSpecificAlertDataResult> result = context.GetSpecificAlertData(key);

            foreach (GetSpecificAlertDataResult record in result)
            {
                yield return new AlertRecord(record);
            }

            yield break;
        }
    }
}
