﻿using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.DataAccess
{

    public static class HematocritFillTypeDataAccess 
    {
        public static IEnumerable<HematocritFillTypeRecord> SelectList()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            IEnumerable<GetHematocritFillTypeListResult> result = context.GetHematocritFillTypeList();

            foreach (GetHematocritFillTypeListResult record in result)
            {
                yield return new HematocritFillTypeRecord(record);
            }

            yield break;
        }
    }
}
