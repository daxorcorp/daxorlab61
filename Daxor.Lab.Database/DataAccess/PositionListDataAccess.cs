﻿using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.DataAccess
{
    public class PositionListDataAccess
    {
        public static IEnumerable<PositionRecord> SelectList()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            var result = context.GetPositionsList();

            foreach (GetPositionsListResult record in result)
            {
                yield return new PositionRecord(record);
            }

            yield break;
        }
   }
}
