﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.SMO;
using Daxor.Lab.Infrastructure.Constants;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace Daxor.Lab.Database.DataAccess
{
    public class SettingsManagerDataService : ISettingsManagerDataService
    {
        public IEnumerable<GetSettingsResult> GetSettingsFromDatabase(string databaseConnectionString)
        {
            var context = new DaxorLabDatabaseLinqDataContext(databaseConnectionString);

            context.CleanUpBlankPatientIds();
            context.CleanUpRunningTests();

            IEnumerable<GetSettingsResult> result = context.GetSettings();
            
            return result;
        }

        /// <summary>
        /// Brings the DaxorLab and QC Archives databases online if they are not online already.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// If the database does not exist, or some other issue where the database cannot be brought online occurs, 
        /// an InvalidOperationException is thrown stating which database had the problem.
        /// </exception>
        public void BringAllDatabasesOnline()
        {
            var server = GetConfiguredServer();
            try
            {
                server.Databases[Constants.DaxorDatabase].SetOnline();
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Could not bring DaxorLab online", ex);
            }
            try
            {
                server.Databases[Constants.QcDatabase].SetOnline();
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("Could not bring QC Archive online", ex);
            }
        }

        private static Server GetConfiguredServer()
        {
            var connection = new ServerConnection
            {
                ServerInstance = Constants.DatabaseServer,
                LoginSecure = false,
                Login = Constants.UserName,
                Password = DomainConstants.DaxorDatabasePassword,
                DatabaseName = "master" // You cannot restore a database that you are connected to
            };
            return new Server(connection) { LoginMode = ServerLoginMode.Mixed };
        }
    }
}
