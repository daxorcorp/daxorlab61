﻿using System;
using System.Linq;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Database.DataAccess
{
    public class CalibrationCurveDataAccess : ICalibrationCurveDataAccess
    {
        public CalibrationCurveRecord Insert(CalibrationCurveRecord record, ILoggerFacade logger)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            try
            {
                int? id = 0;
                context.InsertQCCalibrationCurve(record.Slope, record.OffsetValue, ref id);

                if (id != null)
                    record.CalibrationCurveId = (int)id;

                return record;
            }
            catch (Exception ex)
            {
                logger.Log("Error occurred while inserting QC Calibration curve record: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
        }

        public void Update(CalibrationCurveRecord record, ILoggerFacade logger)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            try
            {
                context.UpdateQCCalibrationCurve(record.CalibrationCurveId, record.Slope, record.OffsetValue);
            }
            catch (Exception ex)
            {
                logger.Log("Error occurred while updating calibration curve: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
        }

	    public CalibrationCurveRecord SelectCurrent(ILoggerFacade logger)
		{
			var context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

			try
			{
				var result = context.GetCurrentQCCalibrationCurve();

				CalibrationCurveRecord record = null;

				var currentCurveResult = result.FirstOrDefault();
				if (currentCurveResult != null)
					record = new CalibrationCurveRecord(currentCurveResult);

				return record;
			}
			catch (Exception ex)
			{
				logger.Log("Error occurred while selecting current QC calibration curve: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
				throw;
			}
	    }

        public CalibrationCurveRecord Select(int id, ILoggerFacade logger)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            try
            {
                var result = context.GetQCCalibrationCurve(id);

                CalibrationCurveRecord record = null;

                var test = result.FirstOrDefault();
                if (test != null)
                    record = new CalibrationCurveRecord(test);

                return record;
            }
            catch (Exception ex)
            {
                logger.Log("Error occurred while selecting QC calibration curve: " + ex.Message + "\nStack Trace: " + ex.StackTrace, Category.Exception, Priority.High);
                throw;
            }
        }
    }
}
