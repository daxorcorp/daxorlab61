﻿using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.DataAccess
{
    public static class ProtocolDataAccess
    {
        public static IEnumerable<ProtocolRecord> SelectList()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            var result = context.GetProtocolList();

            foreach (GetProtocolListResult record in result)
            {
                yield return new ProtocolRecord(record);
            }

            yield break;
        }
    }
}
