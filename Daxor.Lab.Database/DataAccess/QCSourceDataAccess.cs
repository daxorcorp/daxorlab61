﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;

namespace Daxor.Lab.Database.DataAccess
{
    public class QCSourceDataAccess
    {
        public static void Update(QCSourceRecord classT)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            context.UpdateQCSource(classT.SourceTOC, classT.Isotope, classT.Activity,
                classT.Accuracy, classT.HalfLife, classT.SerialNumber, classT.CountLimit, classT.Position);
        }

        public static QCSourceRecord Select(int samplePosition)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            IEnumerable<GetQCSourceDataResult> result = context.GetQCSourceData(samplePosition);
            QCSourceRecord record = null;

            var test = result.FirstOrDefault<GetQCSourceDataResult>();
            if (test != null)
            {
                record = new QCSourceRecord(test);
            }

            return record;
        }
        
        public static IEnumerable<QCSourceRecord> SelectList()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            IEnumerable<GetQCSourcesResult> result = context.GetQCSources();

            foreach (GetQCSourcesResult record in result)
            {
                yield return new QCSourceRecord(record);
            }

            yield break;
        }

    }
}
