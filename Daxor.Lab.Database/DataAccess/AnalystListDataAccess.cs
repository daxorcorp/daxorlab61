﻿using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.DataAccess
{
    public static class AnalystListDataAccess
    {
        public static IEnumerable<AnalystListRecord> SelectList()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            IEnumerable<GetAnalystListResult> result = context.GetAnalystList();

            foreach (GetAnalystListResult record in result)
            {
                yield return new AnalystListRecord(record);
            }

            yield break;
        }
    }
}
