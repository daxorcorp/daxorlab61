﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Database.CommonBLLs;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;

namespace Daxor.Lab.Database.DataAccess
{
    public static class QCTestListDataAccess
    {
        public static QCTestInfoDTO Select(Guid key)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            var result = context.GetQCTestListItem(key);

            QCTestInfoDTO record = null;

            var test = result.FirstOrDefault<GetQCTestListItemResult>();
            if (test != null)
            {
                record = new QCTestInfoDTO(test);
            }

            return record;
        }
        
        public static IEnumerable<QCTestInfoDTO> SelectList()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            
            IEnumerable<GetQCTestListResult> result = context.GetQCTestList();

            foreach (GetQCTestListResult record in result)
            {
                yield return new QCTestInfoDTO(record);
            }

            yield break;
        }
    }
}
