﻿using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.DataAccess
{
    public static class TestModeDataAccess
    {
        public static TestModeRecord Select(int key)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            IEnumerable<GetTestModeDataResult> result = context.GetTestModeData(key);

            TestModeRecord record = null;

            var test = result.FirstOrDefault<GetTestModeDataResult>();
            if (test != null)
            {
                record = new TestModeRecord(test);
            }

            return record;
        }

        public static IEnumerable<TestModeRecord> SelectList()
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            var result = context.GetTestModeList();

            foreach (GetTestModeListResult record in result)
            {
                yield return new TestModeRecord(record);
            }

            yield break;
        }
    }
}
