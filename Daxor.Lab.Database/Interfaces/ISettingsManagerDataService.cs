﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Database.LINQ;

namespace Daxor.Lab.Database.Interfaces
{
    public interface ISettingsManagerDataService
    {
        IEnumerable<GetSettingsResult> GetSettingsFromDatabase(string databaseConnectionString);

        /// <summary>
        /// Brings the DaxorLab and QC Archives databases online if they are not online already.
        /// </summary>
        /// <exception cref="InvalidOperationException">
        /// If the database does not exist, or some other issue where the database cannot be brought online occurs, 
        /// an InvalidOperationException is thrown stating which database had the problem.
        /// </exception>
        void BringAllDatabasesOnline();
    }
}
