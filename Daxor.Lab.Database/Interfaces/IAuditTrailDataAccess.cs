﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.Interfaces
{
    public interface IAuditTrailDataAccess
    {
        AuditTrailRecord Insert(AuditTrailRecord record);
        IEnumerable<AuditTrailRecord> SelectList(Guid key);
    }
}
