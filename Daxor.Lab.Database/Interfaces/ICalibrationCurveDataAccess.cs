﻿using Daxor.Lab.Database.DataRecord;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.Database.Interfaces
{
	public interface ICalibrationCurveDataAccess
	{
		CalibrationCurveRecord Insert(CalibrationCurveRecord record, ILoggerFacade logger);
		CalibrationCurveRecord Select(int id, ILoggerFacade logger);
		CalibrationCurveRecord SelectCurrent(ILoggerFacade logger);
		void Update(CalibrationCurveRecord record, ILoggerFacade logger);
	}
}
