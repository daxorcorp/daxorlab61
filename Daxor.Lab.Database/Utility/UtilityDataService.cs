﻿using System;
using System.Linq;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.DataRecord;

namespace Daxor.Lab.Database.Utility
{
    public static class UtilityDataService
    {
        public static object GetSetting(string settingName)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            var result = context.GetSettingValue(settingName);

            GetSettingValueResult record = result.First<GetSettingValueResult>();

            return record.SETTING_VALUE;
        }

        public static void SetSetting(string settingName, object settingValue)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            context.SetSettingValue(settingName, settingValue.ToString());
        }

        public static PatientHeightRecord GetPatientHeight(Guid pid)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            var result = context.GetPatientHeight(pid);

            GetPatientHeightResult record = result.FirstOrDefault<GetPatientHeightResult>();
            if (record != null)
            {
                return new PatientHeightRecord(record);
            }
            else
            {
                return new PatientHeightRecord(pid);
            }
        }

        public static Guid CheckPatientHospitalId(string patientHospitalId)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
            Guid? result = Guid.Empty;

            context.CheckPatientHospitalId(patientHospitalId, ref result);

            if (result == null)
            {
                return Guid.Empty;
            }
            else
            {
                return (Guid)result;
            }
        }


        public static bool CheckIfInjectateLotHasBeenQCd(string injectateLot)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);
     
            var result = context.CheckIfInjectateLotHasBeenQCd(injectateLot);

            CheckIfInjectateLotHasBeenQCdResult test = result.FirstOrDefault<CheckIfInjectateLotHasBeenQCdResult>();

            if (test == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        
        public static bool IsInjectateKeyUsedByAnotherPatient(string injectateId, Guid patientId)
        {
            DaxorLabDatabaseLinqDataContext context = new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString);

            var result = context.IsInjectateUsedByAnotherPatient(patientId, injectateId);

            IsInjectateUsedByAnotherPatientResult test = result.FirstOrDefault<IsInjectateUsedByAnotherPatientResult>();

            if (test != null)
            {
                if(test.RECORD_COUNT > 0)
                    return true;
            }
            
            return false;
        }
    }
}
