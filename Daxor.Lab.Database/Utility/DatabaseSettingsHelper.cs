﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.Database.Utility
{
    public static class DatabaseSettingsHelper
    {
        public static string LINQConnectionString { get; set; }

        public static int SCLayoutID { get; set; }

        public static string DefaultMeasurementSystem { get; set; }

        public static string ID2Label { get; set; }

        public static string LocationLabel { get; set; }

    }
}
