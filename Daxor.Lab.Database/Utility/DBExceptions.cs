﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.Database.Utility
{
    public class DBInsertFailedException : ApplicationException 
    {
        private readonly string _message;

        public DBInsertFailedException(string message)
        {
            _message = message;
        }

        public override string Message
        {
            get
            {
                return _message;
            }
        }
    }

    public class DBUpdateFailedException : ApplicationException
    {
        private readonly string _message;

        public DBUpdateFailedException(string message)
        {
            _message = message;
        }

        public override string Message
        {
            get { return _message; }
        }
    }

}
