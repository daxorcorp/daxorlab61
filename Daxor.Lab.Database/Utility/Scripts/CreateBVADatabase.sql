USE [master]
GO

DECLARE @dbname nvarchar(128)
SET @dbname = N'DaxorLab'

IF (NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE ('[' + name + ']' = @dbname OR name = @dbname)))
RESTORE DATABASE [DaxorLab] FROM  DISK = N'C:\ProgramData\Daxor\Lab\InitialDatabase\Empty Daxor Lab Database.bak' 
WITH  FILE = 1,  
MOVE N'Daxor Diagnostic Suite' TO N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DaxorLab.mdf',  
MOVE N'Daxor Diagnostic Suite_log' TO N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DaxorLab_1.ldf',  
NOUNLOAD,  REPLACE,  STATS = 10

GO


