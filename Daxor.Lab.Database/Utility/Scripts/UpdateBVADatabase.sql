BEGIN TRANSACTION;

USE [DaxorLab]

declare @numrow int;

set @numrow =(Select count(*) from [DaxorLab].[dbo].[tblSETTING] where [SETTING_ID]=N'BVA_MODULE_VERSION' AND [VALUE] LIKE N'6.1.0%')

if(@numrow = 1)
	set noexec on

UPDATE [dbo].[tblSETTING] SET [VALUE]=N'6.0.3.4051' WHERE [SETTING_ID]=N'BVA_MODULE_VERSION'
UPDATE [dbo].[tblSETTING] SET [GROUP_DISPLAY_ORDER]=1 WHERE [SETTING_ID]=N'SYSTEM_DEPARTMENT_ADDRESS'
UPDATE [dbo].[tblSETTING] SET [GROUP_DISPLAY_ORDER]=4 WHERE [SETTING_ID]=N'SYSTEM_DEPARTMENT_DIRECTOR'
UPDATE [dbo].[tblSETTING] SET [GROUP_DISPLAY_ORDER]=3 WHERE [SETTING_ID]=N'SYSTEM_DEPARTMENT_NAME'
UPDATE [dbo].[tblSETTING] SET [GROUP_DISPLAY_ORDER]=2 WHERE [SETTING_ID]=N'SYSTEM_DEPARTMENT_PHONE_NUMBER'
UPDATE [dbo].[tblSETTING] SET [VALUE]=N'8/8/2013 10:11:04 AM' WHERE [SETTING_ID]=N'SYSTEM_LAST_AUTOMATED_BACKUP'
UPDATE [dbo].[tblSETTING] SET [GROUP_ID] = 5, [GROUP_DISPLAY_ORDER] = 0 WHERE SETTING_ID = N'SYSTEM_DEFAULT_MEASUREMENT_SYSTEM'
UPDATE [dbo].[tblSETTING] SET [GROUP_ID] = 1, [GROUP_DISPLAY_ORDER] = 8, [DATA_TYPE] = N'double' WHERE SETTING_ID = N'QC_RESOLUTION_CENTROID_TOLERANCE'
UPDATE [dbo].[tblSETTING] SET [GROUP_ID] = 6, [GROUP_DISPLAY_ORDER] = 2 WHERE SETTING_ID = N'SYSTEM_BACKGROUND_ACQUISITION_TIME_IN_SEC'
UPDATE [dbo].[tblSETTING] SET [GROUP_ID] = 5, [GROUP_DISPLAY_ORDER] = 1 WHERE SETTING_ID = N'SYSTEM_DEFAULT_LANGUAGE_ID'
UPDATE [dbo].[tblSETTING] SET [GROUP_ID] = 6, [GROUP_DISPLAY_ORDER] = 0 WHERE SETTING_ID = N'SYSTEM_HIGH_CPM_BOUNDARY'
UPDATE [dbo].[tblSETTING] SET [GROUP_ID] = 6, [GROUP_DISPLAY_ORDER] = 1 WHERE SETTING_ID = N'SYSTEM_HIGH_CPM_RESTART_TIME_INTERVAL_IN_SEC'
UPDATE [dbo].[tblSETTING] SET [GROUP_ID] = 5, [GROUP_DISPLAY_ORDER] = 3 WHERE SETTING_ID = N'SYSTEM_SHOW_QC_ARCHIVES_BUTTON'
UPDATE [dbo].[tblSETTING] SET [GROUP_ID] = 5, [GROUP_DISPLAY_ORDER] = 2 WHERE SETTING_ID = N'SYSTEM_SOUND_ENABLED'

IF EXISTS (SELECT * FROM [dbo].[tblSETTING] WHERE SETTING_ID = N'SYSTEM_DEFAULT_REPORT_ZOOM')
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'SYSTEM_DEFAULT_REPORT_ZOOM'

IF EXISTS (SELECT * FROM [dbo].[tblSETTING] WHERE SETTING_ID = N'SYSTEM_DETECTOR_LLD')
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'SYSTEM_DETECTOR_LLD'

IF EXISTS (SELECT * FROM [dbo].[tblSETTING] WHERE SETTING_ID = N'SYSTEM_DETECTOR_NOISE')
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'SYSTEM_DETECTOR_NOISE'

DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'BVA_ANALYST_FIELD_REQUIRED'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'BVA_AUTOMATIC_POINT_EXCLUSION_MAX_NUM_EXCLUSIONS'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'BVA_CC_FIELD_REQUIRED'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'BVA_INJECTATE_DOSE_FIELD_REQUIRED'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'BVA_LOCATION_FIELD_REQUIRED'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'BVA_LOCATION_LABEL'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'BVA_PHYSICIANS_SPECIALTY_FIELD_REQUIRED'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'BVA_REFERRING_PHYSICIAN_FIELD_REQUIRED'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'BVA_TEST_ID2_LABEL'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_CENTROID_PRECISION_IN_KEV'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_COUNT_TIME_IN_SEC'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_MAX_FINE_GAIN'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_MAX_NUMBER_OF_FINE_GAIN_ADJUSTMENTS'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_MAX_PASSING_FWHM'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_MIN_COUNTS'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_MIN_FINE_GAIN'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_MIN_PASSING_FWHM'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_TEST_MAX_FINE_GAIN'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_TEST_MAX_PASSING_FWHM'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_TEST_MIN_COUNTS'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_TEST_MIN_FINE_GAIN'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_TEST_MIN_PASSING_FWHM'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_CALIBRATION_THRESHOLD_COUNT_TIME_IN_SEC'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_INITIAL_FINE_GAIN_ADJUSTMENT'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_MAX_NUMBER_OF_FINE_GAIN_ADJUSTMENTS'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_SETUP_CENTROID_PRECISION_IN_KEV'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_SETUP_SAMPLE_COUNT_TIME_IN_SEC'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'QC_SETUP_SAMPLE_THRESHOLD_COUNT_TIME_IN_SEC'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'SYSTEM_AUTOMATIC_BACKUP_LOCATION'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'SYSTEM_DEFAULT_AUTOMATIC_BACKUP_LOCATION'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'SYSTEM_LAST_AUTOMATED_BACKUP'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'SYSTEM_NUMBER_OF_BACKUPS_TO_KEEP'
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'SYSTEM_SAMPLE_CHANGER_TYPE'

IF EXISTS (SELECT * FROM [dbo].[tblSETTING] WHERE SETTING_ID = N'BVA_PHYSICIANS_SPECIALTIES')
DELETE [dbo].[tblSETTING] WHERE SETTING_ID = N'BVA_PHYSICIANS_SPECIALTIES'


INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'QC_SETUP_CENTROID_PRECISION_IN_KEV', 3, N'2.0', N'double', NULL, 1, 1, NULL, 6017, 3)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'SYSTEM_LAST_AUTOMATED_BACKUP', -1, N'2/21/2014 12:11:15 PM', N'datetime', NULL, NULL, NULL, NULL, NULL, 3)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'SYSTEM_DEFAULT_REPORT_ZOOM', 1, N'75', N'integer', NULL, 5, 4, NULL, 50005, 2)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'QC_CALIBRATION_TEST_MAX_FINE_GAIN', 3, N'1.625', N'double', NULL, 1, 7, NULL, 6013, 3)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'QC_CALIBRATION_TEST_MAX_PASSING_FWHM', 3, N'10', N'double', NULL, 1, 9, NULL, 6024, 3)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'QC_CALIBRATION_TEST_MIN_COUNTS', 3, N'10000', N'integer', NULL, 1, 11, NULL, 6027, 5)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'QC_CALIBRATION_TEST_MIN_FINE_GAIN', 3, N'0.722', N'double', NULL, 1, 4, NULL, 6014, 3)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'QC_CALIBRATION_TEST_MIN_PASSING_FWHM', 3, N'4', N'double', NULL, 1, 10, NULL, 6025, 3)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'QC_INITIAL_FINE_GAIN_ADJUSTMENT', 3, N'0.001', N'double', NULL, 1, 13, NULL, 6032, 3)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'QC_MAX_NUMBER_OF_FINE_GAIN_ADJUSTMENTS', 3, N'10', N'integer', NULL, 1, 4, NULL, 6007, 3)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'QC_SETUP_SAMPLE_COUNT_TIME_IN_SEC', 3, N'60', N'integer', NULL, 1, 0, NULL, 6000, 3)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'QC_SETUP_SAMPLE_THRESHOLD_COUNT_TIME_IN_SEC', 3, N'180', N'integer', NULL, 1, 12, NULL, 6028, 5)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'SYSTEM_AUTOMATIC_BACKUP_LOCATION', 1, N'C:\ProgramData\Daxor\Lab\Backups\', N'folderbrowser', NULL, 4, 2, NULL, 108, 3)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'SYSTEM_DEFAULT_AUTOMATIC_BACKUP_LOCATION', -1, N'C:\ProgramData\Daxor\Lab\Backups\', N'string', NULL, NULL, NULL, NULL, NULL, 3)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'SYSTEM_NUMBER_OF_BACKUPS_TO_KEEP', 1, N'180', N'integer', NULL, 4, 1, NULL, 107, 2)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'BVA_REFERRING_PHYSICIAN_FIELD_REQUIRED', 2, N'True', N'boolean', NULL, 2, 0, NULL, 2021, 2)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'BVA_LOCATION_LABEL', 2, N'Location', N'string', NULL, 4, 1, NULL, 2006, 2)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'BVA_TEST_ID2_LABEL', 2, N'Accession', N'string', NULL, 4, 0, NULL, 2014, 2)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'BVA_ANALYST_FIELD_REQUIRED', 2, N'False', N'boolean', NULL, 2, 3, NULL, 2023, 2)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'BVA_CC_FIELD_REQUIRED', 2, N'False', N'boolean', NULL, 2, 2, NULL, 2022, 2)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'BVA_INJECTATE_DOSE_FIELD_REQUIRED', 2, N'False', N'boolean', NULL, 2, 5, NULL, 2025, 2)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'BVA_LOCATION_FIELD_REQUIRED', 2, N'False', N'boolean', NULL, 2, 4, NULL, 2024, 2)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'BVA_PHYSICIANS_SPECIALTY_FIELD_REQUIRED', 2, N'False', N'boolean', NULL, 2, 1, NULL, 2045, 2)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'BVA_PHYSICIANS_SPECIALTIES', 2, N'Unknown Specialty|Anesthesiology|Internal Medicine|Cardiology |Interventional Cardiology|Endocrinology|Pulmonology|Neurology|Nephrology|Hospitalist|Surgeon|Cardio-Thoracic Surgeon|Trauma Surgeon|Orthopedic Surgeon|Hematology|Oncology|Emergency Medicine|Nuclear Medicine|Nuclear Cardiology|Nurse Practitioner|Geriatric|Other', N'collection', NULL, 0, 1, NULL, 2046, 3)
INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'BVA_AUTOMATIC_POINT_EXCLUSION_MAX_NUM_EXCLUSIONS', 2, N'3', N'integer', NULL, 5, 1, NULL, 2038, 3)

GO

USE [DaxorLab]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tlkpALERTS]') AND type in (N'U'))
DROP TABLE [dbo].[tlkpALERTS]

USE [DaxorLab]


SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[tlkpALERTS](
	[ALERT_ID] [nvarchar](50) NOT NULL,
	[DESCRIPTION] [nvarchar](max) NOT NULL,
	[RESOLUTION] [nvarchar](max) NOT NULL,
	[RESOLUTION_TYPE] [nvarchar](50) NOT NULL,
	[ALERT_ID_NUMBER] [nvarchar](50) NULL,
 CONSTRAINT [PK_tlkpALERTS] PRIMARY KEY CLUSTERED 
(
	[ALERT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_APE_COULD_NOT_LOWER_STDDEV', N'Automatic point exclusion could not find a set of candidate points that, when excluded, brings the standard deviation below 3.9%.', N'Please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'BVA 63')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_APE_HIGH_RSE', N'Samples have been automatically excluded to bring the standard deviation below 3.9%. Although the test results are reportable, DAXOR would like to review the results.', N'Please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'BVA 64')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_DAILY_QC_DUE', N'{0} QC is due at this time. Do you want to run {0} QC now or continue with this Blood Volume Analysis?', N'Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.', N'string', N'BVA 51')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_DIFFERING_PATIENT_INFORMATION', N'The patient information entered differs from what''s in the database.|Do you want to update existing information about this patient to use the entered information instead?\n\n(''Keep Changes'' will update this patient''s information for this and all prior tests. ''Discard Changes'' will restore the patient information for this test to what''s in the database and prior tests will not be affected.', N'None', N'string', N'BVA 46')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_HEIGHT_MAXSUPPORTED', N'The entered height is greater than the maximum supported height of 86.0 in. or 218.44 cm.', N'Please verify the height. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 5')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_HEIGHT_MINSUPPORTED', N'The entered height is less than the minimum supported height of 48.0 in. or 121.92 cm.', N'Please verify the height. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 6')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_HEMATOCRIT_AEXCEEDSAVG', N'Hematocrit A exceeds the average test hematocrit by more than 2 points.', N'Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 7')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_HEMATOCRIT_BEXCEEDSAVG', N'Hematocrit B exceeds the average test hematocrit by more than 2 points.', N'Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 8')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_HEMATOCRIT_HIGHA', N'Hematocrit A is unusually high.', N'Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 9')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_HEMATOCRIT_HIGHB', N'Hematocrit B is unusually high.', N'Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 10')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_HEMATOCRIT_LOWA', N'Hematocrit A is below 20%.', N'Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 11')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_HEMATOCRIT_LOWB', N'Hematocrit B is below 20%.', N'Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 12')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_HEMATOCRIT_MISMATCHAB', N'Hematocrit A should not differ from Hematocrit B by more than two (2) points.', N'Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 13')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_HEMATOCRIT_MISMATCHBA', N'Hematocrit B should not differ from Hematocrit A by more than two (2) points.', N'Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 14')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_MEASUREDBV_MAX', N'The measured blood volume is greater than 14,500 mL.', N'Please call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 15')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_MEASUREDBV_MIN', N'The measured blood volume is less than 1,700 mL.', N'Please call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 16')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_POSTINJTIME_LONG', N'Blood draws greater than 75 minutes post injection may adversely affect test results.', N'Please verify the post-injection time. For specimens collected after 75 minutes, please contact Daxor Clinical Support at 1-888-774-3268 for additional guidance.', N'string', N'BVA 18')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_POSTINJTIME_SHORT', N'Blood draws less than 10 minutes post-injection may adversely affect test results.', N'Please verify the post-injection time. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 19')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_SAMPLE_DURATION_TOO_LONG', N'An extremely long duration (sample count time) may decrease the accuracy of test results.', N'Please verify the duration. If assistance is needed, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'BVA 20')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_SAMPLES_ACLOSETOBASELINE', N'The counts for Sample A are less than twice the average baseline counts.', N'Please call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 21')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_SAMPLES_BCLOSETOBASELINE', N'The counts for Sample B are less than twice the average baseline counts.', N'Please call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 22')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_SAMPLES_DIFFER', N'The difference between counts for Sample A and Sample B is higher than expected.', N'The difference is most likely due to an error that occurred while pipetting one of the samples. For assistance, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'BVA 23')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_STANDARD_KEYS_IDENTICAL', N'Do you want to proceed with using the same standard for Standard A and Standard B?', N'Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.', N'string', N'BVA 62')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_STANDARDS_DIFFER', N'The difference between the counts for Standard A and Standard B is higher than expected.', N'Please centrifuge the standards for approximately 30 seconds and re-run the test. If the issue persists, call DAXOR Customer Support at 1-866-548-7282. ', N'string', N'BVA 24')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_STANDARDS_LOWA', N'Standard A is below 10,000 counts.', N'Please centrifuge the standards for approximately 30 seconds and re-run the test. If the issue persists, call DAXOR Customer Support at 1-866-548-7282. ', N'string', N'BVA 25')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_STANDARDS_LOWB', N'Standard B is below 10,000 counts.', N'Please centrifuge the standards for approximately 30 seconds and re-run the test. If the issue persists, call DAXOR Customer Support at 1-866-548-7282. ', N'string', N'BVA 26')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_STDDEV_EXTREME', N'The standard deviation cannot exceed 3.9%.', N'Please examine the test data for outliers. If assistance is needed, call DAXOR Clinical Support at 1-888-774-3268. ', N'string', N'BVA 27')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_TRANSUDATION_REVERSE', N'The transudation rate (slope) is negative.', N'Please verify that the patient samples are in the correct positions. If they are correct, call DAXOR Clinical Support at 1-888-774-3268. ', N'string', N'BVA 29')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_WEIGHT_HIGHDEV', N'The deviation from the ideal weight is greater than the supported range of -40% to +260%.', N'Please verify the height and weight. If they are correct, call DAXOR Clinical Support at 1-888-774-3268. ', N'string', N'BVA 31')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_WEIGHT_MAXDEV', N'The deviation from the ideal weight is greater than the supported maximum of +350%.', N'Please verify the height and weight. If they are correct, call DAXOR Clinical Support at 1-888-774-3268. ', N'string', N'BVA 32')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_WEIGHT_MINDEV', N'The deviation from the ideal weight is less than the supported minimum of -40%.', N'Please verify the height and weight. If they are correct, call DAXOR Clinical Support at 1-888-774-3268. ', N'string', N'BVA 33')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_SAMPLEA_LESS_THAN_BASELINE', N'The counts for Sample A are less than the average baseline counts.', N'Please call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 66')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_SAMPLEB_LESS_THAN_BASELINE', N'The counts for Sample B are less than the average baseline counts.', N'Please call DAXOR Clinical Support at 1-888-774-3268.', N'string', N'BVA 67')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_APE_NO_FURTHER_EVALUATION', N'Re-including a sample that was automatically excluded will disable any further automatic point exclusion on this BVA test.', N'Are you sure you want to override the exclusion?', N'string', N'BVA 43')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_CHANGING_EXISTING_PID_TO_NEW_PID', N'The entered patient ID ({0}) does not exist in the database. Do you want to create a new patient with that ID?\n\n(Choosing ''Use Selected'' will undo the change to the patient ID.)', N'None', N'string', N'BVA 49')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_CHANGING_EXISTING_PID_TO_OTHER_EXISTING_PID', N'The patient ID you have entered is already associated with the selected patient shown below.|Do you want to use the selected patient?\n\n(Choosing ''Use Selected'' will discard the patient information already entered and replace it with the selected patient''s information.)', N'None', N'string', N'BVA 48')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_CHANGING_NEW_PID_TO_EXISTING_PID', N'The patient ID you have entered is already associated with the selected patient shown below.|Do you want to use the selected patient?\n\n(Choosing ''Selected Patient'' will discard the patient information already entered and replace it with the selected patient''s information.)', N'None', N'string', N'BVA 47')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_DOB_100YEARSOLD', N'The entered date of birth indicates that the patient is more than 100 years old.', N'Please verify that the date of birth has been entered correctly.', N'string', N'BVA 1')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_DOB_AFTERTESTDATE', N'The date of birth cannot be after the current test date.', N'Please correct the date of birth field.', N'string', N'BVA 2')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_DOB_INVALID', N'The entered date of birth is not valid.', N'Please correct the date of birth field.', N'string', N'BVA 3')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_DOB_MAX', N'The entered date of birth indicates that the patient is more than 150 years old.', N'Please verify that the date of birth has been entered correctly.', N'string', N'BVA 4')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_HEMATOCRIT_EXCLUSION_NOT_ALLOWED', N'The hematocrit cannot be excluded unless the Hematocrit Entry field is set to "Two Per Sample."', N'None', N'string', N'BVA 50')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_LOCKED_PATIENT_FIELDS', N'There is currently another BVA test being run for this patient. Until that test completes, patient-specific fields on this test cannot be modified.', N'None', N'string', N'BVA 44')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_NO_BLANK_PATIENT_ID', N'The patient ID cannot be blank for non-pending tests.', N'None', N'string', N'BVA 45')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_POSTINJTIME_LESSTHANPREV', N'The post-injection time should not be less than the previous sample''s post-injection time.', N'Please verify the post-injection time.', N'string', N'BVA 17')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_POSTINJTIME_MORETHANSUBSEQUENT', N'The post-injection time should not be more than the subsequent sample''s post-injection time.', N'Please verify the post-injection time.', N'string', N'BVA 42')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'SYS_TEST_IS_EXECUTING', N'A test is in progress. {0} will abort the test.', N'None', N'string', N'SYS 13')
INSERT INTO [dbo].[tlkpALERTS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ALERT_ID_NUMBER]) VALUES (N'BVA_TEST_MODE_CHANGING', N'Once the test mode has changed, it cannot be changed again.\n\nAre you sure you want to change the test mode?', N'Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.', N'string', N'BVA 60')

GO

USE [DaxorLab]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tlkpERRORS]') AND type in (N'U'))
DROP TABLE [dbo].[tlkpERRORS]

USE [DaxorLab]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[tlkpERRORS](
	[ALERT_ID] [nvarchar](50) NOT NULL,
	[DESCRIPTION] [nvarchar](max) NOT NULL,
	[RESOLUTION] [nvarchar](max) NULL,
	[RESOLUTION_TYPE] [nvarchar](50) NOT NULL,
	[ERROR_ID_NUMBER] [nvarchar](50) NULL,
 CONSTRAINT [PK_tlkpERRORS] PRIMARY KEY CLUSTERED 
(
	[ALERT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_APE_COMPLETION_FAILED', N'Automatic point exclusion did not complete successfully.', N'The original exclusion state of the samples has been restored. Please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'BVA 34')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_CREATE_TEST_FAILED', N'A new BVA test could not be created.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'BVA 35')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_EXPORT_DEVICE_NOT_READY', N'Device is not ready: There does not appear to be a writable disc in the drive.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'BVA 39')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_EXPORT_DISC_FINALIZED', N'A blank disc is required for exporting reports.', N'Please try the export operation with another disc.', N'string', N'BVA 41')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_EXPORT_DISC_READONLY', N'This disc cannot be written to.', N'Please try the export operation with another disc.', N'string', N'BVA 40')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_FAILED_QC_STATE', N'A Blood Volume Analysis cannot be performed at this time because the system is in a failed QC state. Please address the QC failure before attempting to proceed.', N'If the QC failure cannot be resolved, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'BVA 37')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_INJECTATE_DOSE_IN_USE', N'This injectate dose is in use by another patient.\n\nThe injectate key will now be cleared.', N'Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.', N'string', N'BVA 59')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_INJECTATE_KEY_INVALID', N'The {0} key entered {1} is not valid. Please rescan or manually enter the key.', N'Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.', N'string', N'BVA 55')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_INJECTATE_LOT_NUMBER_INVALID', N'The lot number for {0} is not valid. Please rescan or manually enter the lot number.', N'Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.', N'string', N'BVA 54')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_LOAD_TEST_FAILED', N'The requested test could not be loaded from the database.', N'If the problem persists, please call DAXOR Customer Support 1-866-548-7282.', N'string', N'BVA 36')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_LOT_NUMBERS_MISMATCHED', N'The lot numbers for the {0} do not match.\n\n{1}\n\nPlease check that the lot numbers match and rescan or manually enter\n the {2} keys.\n\nAll product keys will now be cleared.', N'Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.', N'string', N'BVA 58')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_SAVING_TEST_WILL_CLEAR_KEYS', N'Injectate/Standard keys have been entered. Leaving this section, will erase the injectate lot and the injectate/standard keys.\n\nAre you sure you want to proceed?', N'Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.', N'string', N'BVA 61')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_SCANNER_INPUT_NOT_ALLOWED', N'This field does not support scanner input.\n\nValid input fields are Injectate Key, Standard A Key, and Standard B Key.', N'Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.', N'string', N'BVA 57')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_TEST_DELETION_FAILED', N'An error occurred while deleting the BVA test.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'BVA 53')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_UNEXPECTED_PRODUCT_KEY_TYPE', N'A key for the {0} was entered, but a key for the {1} was expected.\n\n Please select the appropriate input field and rescan or manually enter the {2} key.', N'Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.', N'string', N'BVA 56')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'QC_ABORT_TEST_TIMED_OUT', N'The QC test is taking longer than expected to abort.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'QC 9')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'QC_EXPORT_DEVICE_NOT_READY', N'Device is not ready: There does not appear to be a writable disc in the drive.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'QC 3')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'QC_EXPORT_DISC_FINALIZED', N'A blank disc is required for exporting reports.', N'Please try the export operation with another disc.', N'string', N'QC 5')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'QC_EXPORT_DISC_READONLY', N'This disc cannot be written to.', N'Please try the export operation with another disc.', N'string', N'QC 4')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'QC_LOAD_TEST_FAILED', N'The QC test could not be loaded.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'QC 6')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'QC_SAVE_TEST_FAILED', N'The QC test could not be saved.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'QC 2')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'QC_TEST_EXECUTION_FAILED', N'An error occurred while executing the QC Test.', N'The test has been aborted. Please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'QC 1')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'QC_TEST_START_FAILED', N'An error occurred while starting QC Test.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'QC 8')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_GAMMA_COUNTER_DISCONNECTED', N'The connection to the gamma counter was lost. Any currently running test will be aborted.', N'Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing. Choosing ''Reboot'' will reboot the system.', N'string', N'SYS 4')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_GENERAL_ERROR', N'A general error has occurred and the application will close.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'SYS 2')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_MODULE_LOAD_FAILED', N'The application could not be started because one or more modules could not be initialized properly.', N'The application will now close. If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'SYS 1')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_MOTOR_STALLED', N'The sample changer motor has stalled and cannot move. Any currently running test will be aborted.', N'Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.', N'string', N'SYS 7')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_NO_PRINTERS_CONNECTED', N'There are no printers that are properly configured.', N'Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.', N'string', N'SYS 8')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_PRINTER_RESET_FAILED', N'The printer could not be reset.', N'Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.', N'string', N'SYS 9')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_SAMPLE_CHANGER_DISCONNECTED', N'The connection to the sample changer was lost. Any currently running test will be aborted.', N'Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing. Choosing ''Reboot'' will reboot the system.', N'string', N'SYS 5')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_SAMPLE_CHANGER_UNEXPECTED_MOVEMENT', N'The sample changer made an unexpected move. Any currently running test will be aborted.', N'Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.', N'string', N'SYS 6')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_SHUTDOWN_PASSWORD_INVALID', N'The password provided is incorrect.', N'Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.', N'string', N'SYS 10')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_SQL_CONNECTION_FAILED', N'A connection to the database could not be established.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'SYS 3')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_TOUCHSCREEN_CALIBRATION_DOESNT_EXIST', N'The touchscreen calibration program could not be found.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'SYS 11')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_TOUCHSCREEN_CALIBRATION_FAILED', N'The touchscreen calibration program encountered an error. The calibration has failed.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'SYS 12')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'UTILITY_EXPORT_DEVICE_NOT_READY', N'Device is not ready: There does not appear to be a writable disc in the drive.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'UTIL 4')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'UTILITY_EXPORT_DISC_FINALIZED', N'A blank disc is required for exporting reports.', N'Please try the export operation with another disc.', N'string', N'UTIL 6')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'UTILITY_EXPORT_DISC_READONLY', N'This disc cannot be written to.', N'Please try the export operation with another disc.', N'string', N'UTIL 5')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'UTILITY_GAMMA_COUNTER_SETTINGS_UPDATE_FAILED', N'An error has occurred when attempting to update the gamma counter settings.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'UTIL 8')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'UTILITY_PASSWORD_INVALID', N'The password provided is incorrect.', N'Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.', N'string', N'UTIL 1')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'UTILITY_RESTORE_DB_FAILED', N'An error occurred when attempting to restore the system databases. This error most likely occurred because the wrong backup file was chosen.', N'Please select a backup file containing {0} in the name.', N'string', N'UTIL 2')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'UTILITY_RESTORE_QC_ARCHIVE_FAILED', N'An error occurred when attempting to restore the QC test archives. This error most likely occurred because the wrong backup file was chosen. ', N'Please select a backup file containing {0} in the name.', N'string', N'UTIL 3')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_EXPORT_UNSUCCESSFUL', N'The export did not complete successfully.', N'Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.', N'string', N'BVA 65')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'UTILITY_BACKUP_FAILED', N'An error occurred while attempting to backup the databases.', N'Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.', N'string', N'UTIL 10')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_EXPORT_PATH_INVALID', N'The selected location is not valid.', N'Please choose another file location.', N'string', N'BVA 38')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_LAST_TWO_HEMATOCRITS_TOO_DIFFERENT', N'To use the \"Last Two\" hematocrit entry option, the hematocrit results for the last two samples must agree within {0:0}%.\n\nBoth hematocrit results will now be cleared.', N'Please call DAXOR Customer Support at 1-888-774-3268 for any further assistance.', N'string', N'BVA 52')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'QC_EXPORT_PATH_INVALID', N'The selected location is not valid.', N'Please choose another file location.', N'string', N'QC 7')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'UTILITY_EXPORT_PATH_INVALID', N'The selected location is not valid.', N'Please choose another file location.', N'string', N'UTIL 7')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'UTILITY_LOGIN_UNAVAILABLE', N'This section of Utilities cannot be accessed while a test is in progress.', N'Please wait until the current test completes before accessing this section of Utilities.', N'string', N'UTIL 9')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_AUTOMATIC_BACKUP_FAILURE', N'The automatic backup failed to complete.', N'Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue.', N'string', N'SYS 15')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_AUTOMATIC_BACKUP_LOCATION_NOT_FOUND', N'The location {0} could not be found. Saving backup to default location.', N'Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue.', N'string', N'SYS 16')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'SYS_DEFAULT_BACKUP_LOCATION_NOT_FOUND', N'The location {0} could not be found. Could not find the default location.', N'Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue.', N'string', N'SYS 14')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'UTILITY_DATABASE_VERSION_MISMATCH', N'The backup version you are currently trying to restore is not compatible with this version of the software.\n\nBackup version: {0}\n\nCurrent version: {1}', N'Please select another backup.', N'string', N'UTIL 11')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'UTILITY_ORDERING_PHYSICIANS_LOOKUP_FAILED', N'The list of ordering physicians could not be obtained from the database.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'UTIL 12')
INSERT INTO [dbo].[tlkpERRORS] ([ALERT_ID], [DESCRIPTION], [RESOLUTION], [RESOLUTION_TYPE], [ERROR_ID_NUMBER]) VALUES (N'BVA_SAVE_TEST_FAILED', N'The BVA test could not be saved.', N'If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.', N'string', N'BVA 68')

GO

DELETE FROM [dbo].[tlkpLOCALIZED_SETTING_STRING] WHERE [STRING_ID] = 50005
DELETE FROM [dbo].[tlkpLOCALIZED_SETTING_STRING] WHERE [STRING_ID] = 102
DELETE FROM [dbo].[tlkpLOCALIZED_SETTING_STRING] WHERE [STRING_ID] = 107
DELETE FROM [dbo].[tlkpLOCALIZED_SETTING_STRING] WHERE [STRING_ID] = 307
DELETE FROM [dbo].[tlkpLOCALIZED_SETTING_STRING] WHERE [STRING_ID] = 6017
DELETE FROM [dbo].[tlkpLOCALIZED_SETTING_STRING] WHERE [STRING_ID] = 12
DELETE FROM [dbo].[tlkpLOCALIZED_SETTING_STRING] WHERE [STRING_ID] = 108
DELETE FROM [dbo].[tlkpLOCALIZED_SETTING_STRING] WHERE [STRING_ID] = 6032
DELETE FROM [dbo].[tlkpLOCALIZED_SETTING_STRING] WHERE [STRING_ID] = 2045
DELETE FROM [dbo].[tlkpLOCALIZED_SETTING_STRING] WHERE [STRING_ID] = 2046

GO

INSERT INTO [dbo].[tlkpLOCALIZED_SETTING_STRING] ([STRING_ID], [LANGUAGE_ID], [STRING]) VALUES (50005, 1, N'Default report zoom (%)')
INSERT INTO [dbo].[tlkpLOCALIZED_SETTING_STRING] ([STRING_ID], [LANGUAGE_ID], [STRING]) VALUES (102, 1, N'Start time')
INSERT INTO [dbo].[tlkpLOCALIZED_SETTING_STRING] ([STRING_ID], [LANGUAGE_ID], [STRING]) VALUES (107, 1, N'Number of backups to keep')
INSERT INTO [dbo].[tlkpLOCALIZED_SETTING_STRING] ([STRING_ID], [LANGUAGE_ID], [STRING]) VALUES (307, 1, N'QC Archives button is shown on main Utilities screen')
INSERT INTO [dbo].[tlkpLOCALIZED_SETTING_STRING] ([STRING_ID], [LANGUAGE_ID], [STRING]) VALUES (6017, 1, N'Centroid precision (keV)')
INSERT INTO [dbo].[tlkpLOCALIZED_SETTING_STRING] ([STRING_ID], [LANGUAGE_ID], [STRING]) VALUES (12, 1, N'Automatic Daily Backup')
INSERT INTO [dbo].[tlkpLOCALIZED_SETTING_STRING] ([STRING_ID], [LANGUAGE_ID], [STRING]) VALUES (108, 1, N'Location')
INSERT INTO [dbo].[tlkpLOCALIZED_SETTING_STRING] ([STRING_ID], [LANGUAGE_ID], [STRING]) VALUES (6032, 1, N'Initial fine gain adjustment')
INSERT INTO [dbo].[tlkpLOCALIZED_SETTING_STRING] ([STRING_ID], [LANGUAGE_ID], [STRING]) VALUES (2045, 1, N'Physician''s specialty is required.')
INSERT INTO [dbo].[tlkpLOCALIZED_SETTING_STRING] ([STRING_ID], [LANGUAGE_ID], [STRING]) VALUES (2046, 1, N'Specialty picklist')

GO

DELETE FROM [dbo].[tlkpSETTINGS_GROUP_NAME] WHERE [GROUP_ID] = 6 AND [MODULE_ID] = 1
INSERT INTO [dbo].[tlkpSETTINGS_GROUP_NAME] ([GROUP_ID], [MODULE_ID], [STRING_ID]) VALUES (6, 1, 11)
UPDATE [dbo].[tlkpSETTINGS_GROUP_NAME] SET [STRING_ID] = 12 WHERE [GROUP_ID] = 4 AND [MODULE_ID] = 1
UPDATE [dbo].[tlkpSETTINGS_GROUP_NAME] SET [STRING_ID] = 5 WHERE [GROUP_ID] = 5 AND [MODULE_ID] = 1
 
GO

ALTER TABLE [dbo].[tblTEST_HEADER] ALTER COLUMN [UNIQUE_SYSTEM_ID] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL

-- Stored Procedure

-- =============================================
-- Author:		Phillip Andreason
-- Create date: 2/20/2014
-- Description:	Checks all the appropriate tables to see that the tables
--				for the BackgroundWorker that loads the BVATestList are available
-- =============================================

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckIfBVATestTablesOnline]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[CheckIfBVATestTablesOnline]

GO

CREATE PROCEDURE [dbo].[CheckIfBVATestTablesOnline] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @NUMROWS as INT;

	SELECT @NUMROWS = COUNT (*) FROM [DaxorLab].INFORMATION_SCHEMA.TABLES
	WHERE TABLE_NAME IN ('tblTEST_HEADER', 'tblBVA_TEST', 'tblPATIENT', 'tlkpSTATUS');

	SELECT CASE WHEN @NUMROWS = 4 THEN 'true' ELSE 'false' END
END
GO

-- Stored Procedure

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRecentQCSamples]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[GetRecentQCSamples]	

GO

CREATE PROCEDURE [dbo].[GetRecentQCSamples]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	declare @SAMPLE_POSITION_TO_LOOK_FOR as int
	set @SAMPLE_POSITION_TO_LOOK_FOR = 1

	DECLARE @TEMPTABLE TABLE (
			TEST_DATE DATETIME NOT NULL
		,   SAMPLE_POSITION INT NOT NULL
		,   PASS NCHAR(1)
	)

	WHILE @SAMPLE_POSITION_TO_LOOK_FOR < 26
	BEGIN 
		INSERT INTO @TEMPTABLE Select top 1  [TEST_DATE]
									, SAMPLE_POSITION
									, [PASS]
		FROM  [tblTEST_HEADER]
			JOIN tblTEST_SAMPLES ON [tblTEST_HEADER].TEST_ID = tblTEST_SAMPLES.TEST_ID
			JOIN tblSAMPLE_HEADER ON tblTEST_SAMPLES.SAMPLE_ID = tblSAMPLE_HEADER.SAMPLE_ID
			JOIN tblQC_SAMPLE ON tblQC_SAMPLE.SAMPLE_ID = tblSAMPLE_HEADER.SAMPLE_ID
			WHERE STATUS_ID = 4 and SAMPLE_POSITION = @SAMPLE_POSITION_TO_LOOK_FOR
			ORDER BY [TEST_DATE] desc, SAMPLE_POSITION

		set @SAMPLE_POSITION_TO_LOOK_FOR = @SAMPLE_POSITION_TO_LOOK_FOR + 1

	END

SELECT* FROM @TEMPTABLE

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetQCContaminationFullTestPositionList]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[GetQCContaminationFullTestPositionList]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSettingCategoryId]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[GetSettingCategoryId]

GO

--BEGIN 6.1.0 updates---------------------------------------------------------------------------------------------------------------------------------

IF COL_LENGTH('[dbo].[tblBVA_TEST]','PHYSICIANS_SPECIALTY') IS NULL
BEGIN
ALTER TABLE [dbo].[tblBVA_TEST] ADD [PHYSICIANS_SPECIALTY] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_tblBVA_TEST_PHYSICIANS_SPECIALTY] DEFAULT (N'')
END

GO

IF COL_LENGTH('[dbo].[tblBVA_TEST]','AMPUTEE_IDEALS_CORRECTION_FACTOR') IS NULL
BEGIN
ALTER TABLE [dbo].[tblBVA_TEST] ADD [AMPUTEE_IDEALS_CORRECTION_FACTOR] [int] NOT NULL DEFAULT 0
END

GO

IF COL_LENGTH('[dbo].[tblPATIENT]','IS_AMPUTEE') IS NULL
BEGIN
ALTER TABLE [dbo].[tblPATIENT] ADD [IS_AMPUTEE] [bit] NOT NULL CONSTRAINT [DF_tblPATIENT_IS_AMPUTEE] DEFAULT ((0))
END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetSpecialtyList]') and type in (N'FN',N'IF', N'TF'))
DROP FUNCTION [dbo].[GetSpecialtyList]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAllBVATestData]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[GetAllBVATestData]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBVATestData]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[GetBVATestData]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertBVATest]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[InsertBVATest]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdateBVATest]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[UpdateBVATest]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPhysician]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[GetPhysician]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPhysicianList]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[GetPhysicianList]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPatientData]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[GetPatientData]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertPatient]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[InsertPatient]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UpdatePatient]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[UpdatePatient]
IF EXISTS (SELECT * FROM sys.objects WHERE name like '%ManageAuditTrailForBVATest%')
DROP TRIGGER [dbo].[ManageAuditTrailForBVATest]
IF EXISTS (SELECT * FROM sys.objects WHERE name like '%ManageAuditTrailForPaitient%')
DROP TRIGGER [dbo].[ManageAuditTrailForPaitient]

GO

CREATE PROCEDURE [dbo].[GetPatientData] 
(
	@PID AS Uniqueidentifier
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [PID]
		  ,[PATIENT_HOSPITAL_ID]
		  ,[FIRST_NAME]
		  ,[MIDDLE_NAME]
		  ,[LAST_NAME]
		  ,[GENDER]
		  ,[DOB]
		  ,[ROW_TIMESTAMP]
		  ,[IS_AMPUTEE]
	FROM [dbo].[tblPATIENT]
	WHERE PID = @PID
END
GO

CREATE PROCEDURE [dbo].[InsertPatient]
(
	@PID As Uniqueidentifier,
	@PATIENT_HOSPITAL_ID As nvarchar(50),
	@FIRST_NAME As nvarchar(50),
	@MIDDLE_NAME As nvarchar(50),
	@LAST_NAME As nvarchar(50),
	@GENDER As nchar(1),
	@DOB As DateTime,
	@TIMESTAMP AS timestamp OUTPUT,
	@AMPUTEE_STATUS AS bit
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblPATIENT (PID, PATIENT_HOSPITAL_ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME, GENDER, DOB, IS_AMPUTEE)
	VALUES (@PID, @PATIENT_HOSPITAL_ID, @FIRST_NAME, @MIDDLE_NAME, @LAST_NAME, @GENDER, @DOB, @AMPUTEE_STATUS)
	DECLARE @ROWCOUNT AS INT
	
	SET @ROWCOUNT = @@ROWCOUNT 
	
	SELECT @TIMESTAMP = (SELECT tblPATIENT.ROW_TIMESTAMP FROM tblPATIENT WHERE PID = @PID)
	return @ROWCOUNT
END
GO

CREATE PROCEDURE [dbo].[UpdatePatient]
(
	@PID As Uniqueidentifier,
	@PATIENT_HOSPITAL_ID As nvarchar(50),
	@FIRST_NAME As nvarchar(50),
	@MIDDLE_NAME As nvarchar(50),
	@LAST_NAME As nvarchar(50),
	@GENDER As nchar(1),
	@DOB As DateTime,
	@OLD_TIMESTAMP as TIMESTAMP,
	@TIMESTAMP AS TIMESTAMP OUTPUT,
	@AMPUTEE_STATUS as bit
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblPATIENT 
	SET PATIENT_HOSPITAL_ID = @PATIENT_HOSPITAL_ID,
		FIRST_NAME = @FIRST_NAME,
		MIDDLE_NAME = @MIDDLE_NAME,
		LAST_NAME = @LAST_NAME,
		GENDER = @GENDER,
		DOB = @DOB,
		IS_AMPUTEE = @AMPUTEE_STATUS
	WHERE
		PID = @PID 
		-- AND ROW_TIMESTAMP = @OLD_TIMESTAMP
		
	DECLARE @ROWCOUNT AS INT
	
	SET @ROWCOUNT = @@ROWCOUNT 
	
	SELECT @TIMESTAMP = (SELECT tblPATIENT.ROW_TIMESTAMP FROM tblPATIENT WHERE PID = @PID)
	return @ROWCOUNT

END
GO

CREATE PROCEDURE [dbo].[GetAllBVATestData]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	SELECT tblTEST_HEADER.[TEST_ID]
	  ,[STATUS_ID]
	  ,(SELECT [dbo].[fn_GetStatusName] ([STATUS_ID])) AS STATUS_NAME
	  ,[TEST_DATE]
	  ,[TYPE_ID]
	  ,[DISPLAY_UNITS]
	  ,[ANALYST]
	  ,[COMMENT]
	  ,[TUBE_ID]
	  , (SELECT [dbo].[fn_GetTubeDescription] ([STATUS_ID])) AS TUBE_DESCRIPTION
	  ,[SC_LAYOUT_ID]
	  ,[UNIQUE_SYSTEM_ID]
	  ,[PID]
	  ,[ID2]
	  ,[LOCATION]
	  ,[BLOOD_DRAW_COUNT]
	  ,[PATIENT_HEIGHT]
	  ,[PATIENT_WEIGHT]
	  ,[DOSE]
	  ,[INJECTATE_LOT]
	  ,[REFERRING_PHYSICIAN]
	  ,[PHYSICIANS_SPECIALTY]
	  ,[CC_REPORT_TO]
	  ,[HEMATOCRIT_FILL_TYPE]
	  ,[REFERENCE_VOLUME]
	  ,[MODE_ID]
	  ,[CONSIDERATION_ID]
	  ,(SELECT [dbo].[fn_GetTestModeDescription] ([MODE_ID])) AS TEST_MODE_DESCRIPTION
	  ,(SELECT [dbo].[fn_GetTestAtt] ('PACS', tblTEST_HEADER.[TEST_ID])) AS PACS
	FROM tblTEST_HEADER JOIN tblBVA_TEST On tblTEST_HEADER.TEST_ID =  tblBVA_TEST.TEST_ID 
	WHERE TYPE_ID = (SELECT [TYPE_ID] FROM tlkpTYPE WHERE TYPE = 'BVA Test')
END
GO

CREATE PROCEDURE [dbo].[GetBVATestData]
(
	@TEST_ID AS Uniqueidentifier
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tblTEST_HEADER.[TEST_ID]
      ,[STATUS_ID]
      ,(SELECT [dbo].[fn_GetStatusName] ([STATUS_ID])) AS STATUS_NAME
      ,[TEST_DATE]
      ,[TYPE_ID]
      ,[DISPLAY_UNITS]
      ,[ANALYST]
      ,[COMMENT]
      ,BACKGROUND_COUNT 
      ,BACKGROUND_TIME
      ,[TUBE_ID]
      , (SELECT [dbo].[fn_GetTubeDescription] ([TUBE_ID])) AS TUBE_DESCRIPTION
      ,[SC_LAYOUT_ID]
      ,[UNIQUE_SYSTEM_ID]
      ,[PID]
      ,[ID2]
      ,[LOCATION]
      ,[BLOOD_DRAW_COUNT]
      ,[PATIENT_HEIGHT]
      ,[PATIENT_WEIGHT]
      ,[DOSE]
      ,[INJECTATE_LOT]
      ,[REFERRING_PHYSICIAN]
      ,[PHYSICIANS_SPECIALTY]
      ,[CC_REPORT_TO]
      ,[HEMATOCRIT_FILL_TYPE]
      ,[REFERENCE_VOLUME]
      ,[MODE_ID]
      ,CONSIDERATION_ID 
      ,(SELECT [dbo].[fn_GetTestModeDescription] ([MODE_ID])) AS TEST_MODE_DESCRIPTION
      ,(SELECT [dbo].[fn_GetTestAtt] ('PACS', tblTEST_HEADER.[TEST_ID])) AS PACS,
      tblTEST_HEADER.ROW_TIMESTAMP AS TEST_HEADER_TIMESTAMP,
      tblBVA_TEST.ROW_TIMESTAMP AS BVA_TEST_TIMESTAMP,
      HAS_SUFFICIENT_DATA,
      SAMPLE_DURATION_IN_SECONDS,
      (SELECT ROW_TIMESTAMP FROM tblTEST_ATT WHERE ATT_NAME = 'PACS' AND TEST_ID = @TEST_ID) AS PACS_TIMESTAMP,
      (SELECT AMPUTEE_IDEALS_CORRECTION_FACTOR FROM tblBVA_TEST WHERE TEST_ID = @TEST_ID) AS AMPUTEE_IDEALS_CORRECTION_FACTOR
	FROM tblTEST_HEADER JOIN tblBVA_TEST On tblTEST_HEADER.TEST_ID =  tblBVA_TEST.TEST_ID 
	WHERE TYPE_ID = (SELECT [TYPE_ID] FROM tlkpTYPE WHERE TYPE = 'BVA Test') AND
	tblTEST_HEADER.TEST_ID = @TEST_ID
END
GO

CREATE PROCEDURE [dbo].[InsertBVATest]
(
	@TEST_ID				As uniqueidentifier,
	@PID					As Uniqueidentifier,
	@ID2					AS NVARCHAR(50),
	@LOCATION				AS NVARCHAR(50),
	@BLOOD_DRAW_COUNT		As Int,
	@PATIENT_HEIGHT			As Float,
	@PATIENT_WEIGHT			As Float,
	@DOSE					As Float,
	@INJECTATE_LOT			AS NVARCHAR(50),
	@REFERRING_PHYSICIAN	AS NVARCHAR(50),
	@PHYSICIANS_SPECIALTY   AS NVARCHAR(50),
	@CC_REPORT_TO			AS NVARCHAR(50),
	@HEMATOCRIT_FILL_TYPE	As INT,
	@MODE_ID				AS INT,
	@DISPLAY_UNITS			as nchar(6),
    @TUBE_ID				as int,
    @CONSIDERATION_ID		as int,
    @HAS_SUFFICIENT_DATA	as nchar(1),
    @SAMPLE_DURATION_IN_SECONDS AS INT,
	@TIMESTAMP AS timestamp OUTPUT,
	@AMPUTEE_IDEALS_CORRECTION_FACTOR   as int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO tblBVA_TEST (TEST_ID, PID, ID2, LOCATION, BLOOD_DRAW_COUNT, PATIENT_HEIGHT, PATIENT_WEIGHT,
		DOSE, INJECTATE_LOT, REFERRING_PHYSICIAN, PHYSICIANS_SPECIALTY, CC_REPORT_TO, HEMATOCRIT_FILL_TYPE, MODE_ID, 
		DISPLAY_UNITS, TUBE_ID, CONSIDERATION_ID, REFERENCE_VOLUME, HAS_SUFFICIENT_DATA, SAMPLE_DURATION_IN_SECONDS,
		AMPUTEE_IDEALS_CORRECTION_FACTOR)
	VALUES
		(@TEST_ID,					 
		 @PID,					
		 @ID2,
		 @LOCATION,
		 @BLOOD_DRAW_COUNT,		
		 @PATIENT_HEIGHT,			
		 @PATIENT_WEIGHT,			
		 @DOSE,					
		 @INJECTATE_LOT,			
		 @REFERRING_PHYSICIAN,
		 @PHYSICIANS_SPECIALTY,	
		 @CC_REPORT_TO,			
		 @HEMATOCRIT_FILL_TYPE,
		 @MODE_ID,	
		 @DISPLAY_UNITS,
		 @TUBE_ID,
		 @CONSIDERATION_ID,
		 (SELECT [dbo].[fn_GetSettingValue]  ('BVA_REFERENCE_VOLUME_IN_ML')),
		 @HAS_SUFFICIENT_DATA,
		 @SAMPLE_DURATION_IN_SECONDS,
		 @AMPUTEE_IDEALS_CORRECTION_FACTOR)	
	DECLARE @ROWCOUNT AS INT
	
	SET @ROWCOUNT = @@ROWCOUNT 
	
	SELECT @TIMESTAMP = (SELECT tblBVA_TEST.ROW_TIMESTAMP FROM tblBVA_TEST WHERE TEST_ID = @TEST_ID)
	return @ROWCOUNT
	
END
GO

CREATE PROCEDURE [dbo].[UpdateBVATest]
(	@TEST_ID				As uniqueidentifier,
	@PID					As Uniqueidentifier,
	@ID2					AS NVARCHAR(50),
	@LOCATION				AS NVARCHAR(50),
	@BLOOD_DRAW_COUNT		As Int,
	@PATIENT_HEIGHT			As Float,
	@PATIENT_WEIGHT			As Float,
	@DOSE					As Float,
	@INJECTATE_LOT			AS NVARCHAR(50),
	@REFERRING_PHYSICIAN	AS NVARCHAR(50),
	@PHYSICIANS_SPECIALTY   AS NVARCHAR(50),
	@CC_REPORT_TO			AS NVARCHAR(50),
	@HEMATOCRIT_FILL_TYPE	As INT,
	@MODE_ID				AS INT,
	@DISPLAY_UNITS			as nchar(6),
     @TUBE_ID				as int, 
     @CONSIDERATION_ID		as int,
     @HAS_SUFFICIENT_DATA	as nchar(1),
     @SAMPLE_DURATION_IN_SECONDS as Int,
	@OLD_TIMESTAMP as TIMESTAMP,
	@TIMESTAMP AS TIMESTAMP OUTPUT,
	@AMPUTEE_IDEALS_CORRECTION_FACTOR   as int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE tblBVA_TEST SET PID = @PID,
			ID2 = @ID2,
			LOCATION = @LOCATION,
			BLOOD_DRAW_COUNT = @BLOOD_DRAW_COUNT,		
			PATIENT_HEIGHT = @PATIENT_HEIGHT,			
			PATIENT_WEIGHT = @PATIENT_WEIGHT,			
			DOSE = @DOSE,					
			INJECTATE_LOT = @INJECTATE_LOT,			
			REFERRING_PHYSICIAN = @REFERRING_PHYSICIAN,
			PHYSICIANS_SPECIALTY = @PHYSICIANS_SPECIALTY,	
			CC_REPORT_TO =  @CC_REPORT_TO,			
			HEMATOCRIT_FILL_TYPE = @HEMATOCRIT_FILL_TYPE,
			MODE_ID = @MODE_ID,
			DISPLAY_UNITS = @DISPLAY_UNITS,
			TUBE_ID = @TUBE_ID,
			CONSIDERATION_ID = @CONSIDERATION_ID, 
			HAS_SUFFICIENT_DATA = @HAS_SUFFICIENT_DATA,
			SAMPLE_DURATION_IN_SECONDS= @SAMPLE_DURATION_IN_SECONDS,
			AMPUTEE_IDEALS_CORRECTION_FACTOR = @AMPUTEE_IDEALS_CORRECTION_FACTOR
	WHERE
		TEST_ID = @TEST_ID 	
		--AND ROW_TIMESTAMP = @OLD_TIMESTAMP
		
	DECLARE @ROWCOUNT AS INT
	
	SET @ROWCOUNT = @@ROWCOUNT 
	
	SELECT @TIMESTAMP = (SELECT tblBVA_TEST.ROW_TIMESTAMP FROM tblBVA_TEST WHERE TEST_ID = @TEST_ID )
	return @ROWCOUNT

END
GO

CREATE TRIGGER [dbo].[ManageAuditTrailForBVATest] 
   ON  [dbo].[tblBVA_TEST]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
    DECLARE @TEST_ID AS UNIQUEIDENTIFIER
    DECLARE @STATUS AS INT
    
    SET @TEST_ID = (SELECT TEST_ID FROM INSERTED)
    SET @STATUS = (SELECT STATUS_ID FROM [tblTEST_HEADER]
					WHERE TEST_ID = @TEST_ID)
	IF @STATUS IS NOT NULL AND
		@STATUS = (SELECT [dbo].[fn_GetStatusID] ('Completed'))
	BEGIN
	  Declare @ID2 AS NVARCHAR(50)
           ,@LOCATION AS NVARCHAR(50)
           ,@BLOOD_DRAW_COUNT AS INT
           ,@PATIENT_HEIGHT AS FLOAT
           ,@PATIENT_WEIGHT AS FLOAT
           ,@DOSE AS FLOAT
           ,@INJECTATE_LOT AS NVARCHAR(50)
           ,@REFERRING_PHYSICIAN AS NVARCHAR(50)
           ,@PHYSICIANS_SPECIALTY AS NVARCHAR(50)
           ,@CC_REPORT_TO AS NVARCHAR(50)
           ,@HEMATOCRIT_FILL_TYPE AS INT
           ,@REFERENCE_VOLUME AS INT
           ,@MODE_ID AS INT
           ,@TUBE_ID AS INT
           ,@DISPLAY_UNITS AS NCHAR(6)
           ,@CONSIDERATION_ID AS INT
           ,@AMPUTEE_IDEALS_CORRECTION_FACTOR AS INT
 
	  Declare @ID2_OLD AS NVARCHAR(50)
           ,@LOCATION_OLD AS NVARCHAR(50)
           ,@BLOOD_DRAW_COUNT_OLD AS INT
           ,@PATIENT_HEIGHT_OLD AS FLOAT
           ,@PATIENT_WEIGHT_OLD AS FLOAT
           ,@DOSE_OLD AS FLOAT
           ,@INJECTATE_LOT_OLD AS NVARCHAR(50)
           ,@REFERRING_PHYSICIAN_OLD AS NVARCHAR(50)
           ,@PHYSICIANS_SPECIALTY_OLD AS NVARCHAR(50)
           ,@CC_REPORT_TO_OLD AS NVARCHAR(50)
           ,@HEMATOCRIT_FILL_TYPE_OLD AS INT
           ,@REFERENCE_VOLUME_OLD AS INT
           ,@MODE_ID_OLD AS INT
           ,@TUBE_ID_OLD AS INT
           ,@DISPLAY_UNITS_OLD AS NCHAR(6)
           ,@CONSIDERATION_ID_OLD AS INT
           ,@AMPUTEE_IDEALS_CORRECTION_FACTOR_OLD AS INT
     
		DECLARE @CHANGE_TABLE nvarchar(50)
		DECLARE @CHANGE_FIELD nvarchar(50)
		DECLARE @OLD_VALUE nvarchar(50)
		DECLARE @NEW_VALUE nvarchar(50)
		DECLARE @CHANGED_BY nvarchar(50)
	  
		SELECT @CHANGE_TABLE = 'tblBVA_TEST'
		SET @CHANGED_BY = (SELECT ANALYST FROM [tblTEST_HEADER]
					WHERE TEST_ID = @TEST_ID)
					
		SELECT 
			@ID2 = [ID2],
            @LOCATION = [LOCATION], 
            @BLOOD_DRAW_COUNT = [BLOOD_DRAW_COUNT],
            @PATIENT_HEIGHT = [PATIENT_HEIGHT],
            @PATIENT_WEIGHT = [PATIENT_WEIGHT],
            @DOSE = [DOSE], 
            @INJECTATE_LOT = [INJECTATE_LOT],
            @REFERRING_PHYSICIAN = [REFERRING_PHYSICIAN],
            @PHYSICIANS_SPECIALTY = [PHYSICIANS_SPECIALTY],
            @CC_REPORT_TO = [CC_REPORT_TO],
            @HEMATOCRIT_FILL_TYPE = [HEMATOCRIT_FILL_TYPE],
            @REFERENCE_VOLUME = [REFERENCE_VOLUME],
            @MODE_ID = [MODE_ID],
            @TUBE_ID = [TUBE_ID],
            @DISPLAY_UNITS = [DISPLAY_UNITS],
            @CONSIDERATION_ID = [CONSIDERATION_ID],
            @AMPUTEE_IDEALS_CORRECTION_FACTOR = [AMPUTEE_IDEALS_CORRECTION_FACTOR]
		FROM INSERTED
		
		SELECT 
			@ID2_OLD = [ID2],
            @LOCATION_OLD = [LOCATION], 
            @BLOOD_DRAW_COUNT_OLD = [BLOOD_DRAW_COUNT],
            @PATIENT_HEIGHT_OLD = [PATIENT_HEIGHT],
            @PATIENT_WEIGHT_OLD = [PATIENT_WEIGHT],
            @DOSE_OLD = [DOSE], 
            @INJECTATE_LOT_OLD = [INJECTATE_LOT],
            @REFERRING_PHYSICIAN_OLD = [REFERRING_PHYSICIAN],
            @PHYSICIANS_SPECIALTY_OLD = [PHYSICIANS_SPECIALTY],
            @CC_REPORT_TO_OLD = [CC_REPORT_TO],
            @HEMATOCRIT_FILL_TYPE_OLD = [HEMATOCRIT_FILL_TYPE],
            @REFERENCE_VOLUME_OLD = [REFERENCE_VOLUME],
            @MODE_ID_OLD = [MODE_ID],
            @TUBE_ID_OLD = [TUBE_ID],
            @DISPLAY_UNITS_OLD = [DISPLAY_UNITS],
            @CONSIDERATION_ID_OLD = [CONSIDERATION_ID],
            @AMPUTEE_IDEALS_CORRECTION_FACTOR_OLD = [AMPUTEE_IDEALS_CORRECTION_FACTOR]
		FROM DELETED
		
		if @ID2 IS NOT NULL
		BEGIN
			if @ID2 <> @ID2_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'ID2'
				SELECT @OLD_VALUE = @ID2_OLD
				SELECT @NEW_VALUE = @ID2
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  
		
		if @LOCATION IS NOT NULL
		BEGIN
			if @LOCATION <> @LOCATION_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'LOCATION'
				SELECT @OLD_VALUE = @LOCATION_OLD
				SELECT @NEW_VALUE = @LOCATION
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  

		if @BLOOD_DRAW_COUNT IS NOT NULL
		BEGIN
			if @BLOOD_DRAW_COUNT <> @BLOOD_DRAW_COUNT_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'BLOOD_DRAW_COUNT'
				SELECT @OLD_VALUE = (SELECT [DESCRIPTION] 
									 FROM [dbo].[tlkpPROTOCOL]
									 WHERE [PROTOCOL] = @BLOOD_DRAW_COUNT_OLD)
				SELECT @NEW_VALUE = (SELECT [DESCRIPTION] 
									 FROM [dbo].[tlkpPROTOCOL]
									 WHERE [PROTOCOL] = @BLOOD_DRAW_COUNT)
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END  
		END 
		
		if @PATIENT_HEIGHT IS NOT NULL
		BEGIN
			if @PATIENT_HEIGHT <> @PATIENT_HEIGHT_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'PATIENT_HEIGHT'
				SELECT @OLD_VALUE = @PATIENT_HEIGHT_OLD
				SELECT @NEW_VALUE = @PATIENT_HEIGHT
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  

		if @PATIENT_WEIGHT IS NOT NULL
		BEGIN
			IF @PATIENT_WEIGHT <> @PATIENT_WEIGHT_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'PATIENT_WEIGHT'
				SELECT @OLD_VALUE = @PATIENT_WEIGHT_OLD
				SELECT @NEW_VALUE = @PATIENT_WEIGHT
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  

		if @DOSE IS NOT NULL
		BEGIN
			IF @DOSE <> @DOSE_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'DOSE'
				SELECT @OLD_VALUE = @DOSE_OLD
				SELECT @NEW_VALUE = @DOSE
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  

		if @INJECTATE_LOT IS NOT NULL
		BEGIN
			IF @INJECTATE_LOT <> @INJECTATE_LOT_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'INJECTATE_LOT'
				SELECT @OLD_VALUE = @INJECTATE_LOT_OLD
				SELECT @NEW_VALUE = @INJECTATE_LOT
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  

		if @REFERRING_PHYSICIAN IS NOT NULL
		BEGIN
			IF @REFERRING_PHYSICIAN <> @REFERRING_PHYSICIAN_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'REFERRING_PHYSICIAN'
				SELECT @OLD_VALUE = @REFERRING_PHYSICIAN_OLD
				SELECT @NEW_VALUE = @REFERRING_PHYSICIAN
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  
		
		if @PHYSICIANS_SPECIALTY IS NOT NULL
		BEGIN
			IF @PHYSICIANS_SPECIALTY <> @PHYSICIANS_SPECIALTY_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'PHYSICIANS_SPECIALTY'
				SELECT @OLD_VALUE = @PHYSICIANS_SPECIALTY_OLD
				SELECT @NEW_VALUE = @PHYSICIANS_SPECIALTY
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  

		if @CC_REPORT_TO IS NOT NULL
		BEGIN
			IF @CC_REPORT_TO <> @CC_REPORT_TO_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'CC_REPORT_TO'
				SELECT @OLD_VALUE = @CC_REPORT_TO_OLD
				SELECT @NEW_VALUE = @CC_REPORT_TO
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  

		if @HEMATOCRIT_FILL_TYPE IS NOT NULL
		BEGIN
			IF @HEMATOCRIT_FILL_TYPE <> @HEMATOCRIT_FILL_TYPE_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'HEMATOCRIT_FILL_TYPE'
				SELECT @OLD_VALUE = (SELECT [DESCRIPTION] 
									 FROM [dbo].[tlkpHEMATOCRIT_FILL_TYPE]
									 WHERE [HEMATOCRIT_FILL_TYPE] = @HEMATOCRIT_FILL_TYPE_OLD)
				SELECT @NEW_VALUE = (SELECT [DESCRIPTION] 
									 FROM [dbo].[tlkpHEMATOCRIT_FILL_TYPE]
									 WHERE [HEMATOCRIT_FILL_TYPE] = @HEMATOCRIT_FILL_TYPE)
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  

		if @REFERENCE_VOLUME IS NOT NULL
		BEGIN
			IF @REFERENCE_VOLUME <> @REFERENCE_VOLUME_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'REFERENCE_VOLUME'
				SELECT @OLD_VALUE = @REFERENCE_VOLUME_OLD
				SELECT @NEW_VALUE = @REFERENCE_VOLUME
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  

		if @MODE_ID IS NOT NULL
		BEGIN
			IF @MODE_ID <> @MODE_ID_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'MODE_ID'
				SELECT @OLD_VALUE = (SELECT [MODE_DESCRIPTION] 
									 FROM [dbo].[tlkpTEST_MODE]
									 WHERE [MODE_ID] = @MODE_ID_OLD)
				SELECT @NEW_VALUE = (SELECT [MODE_DESCRIPTION] 
									 FROM [dbo].[tlkpTEST_MODE]
									 WHERE [MODE_ID] = @MODE_ID)
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  

		if @TUBE_ID IS NOT NULL
		BEGIN
			IF @TUBE_ID <> @TUBE_ID_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'TUBE_ID'
				SELECT @OLD_VALUE = (SELECT [TYPE] 
									 FROM [dbo].[tlkpTUBE_TYPE]
									 WHERE [TUBE_ID] = @TUBE_ID_OLD)
				SELECT @NEW_VALUE = (SELECT [TYPE] 
									 FROM [dbo].[tlkpTUBE_TYPE]
									 WHERE [TUBE_ID] = @TUBE_ID)
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  

		if @DISPLAY_UNITS IS NOT NULL
		BEGIN
			IF @DISPLAY_UNITS <> @DISPLAY_UNITS_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'DISPLAY_UNITS'
				SELECT @OLD_VALUE = @DISPLAY_UNITS_OLD
				SELECT @NEW_VALUE = @DISPLAY_UNITS
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  

		if @CONSIDERATION_ID IS NOT NULL
		BEGIN
			IF @CONSIDERATION_ID <> @CONSIDERATION_ID_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'CONSIDERATION_ID'
				SELECT @OLD_VALUE = (SELECT [DESCRIPTION] 
									 FROM [dbo].[tlkpSPECIAL_CONSIDERATIONS]
									 WHERE [CONSIDERATION_ID] = @CONSIDERATION_ID_OLD)
				SELECT @NEW_VALUE = (SELECT [DESCRIPTION] 
									 FROM [dbo].[tlkpSPECIAL_CONSIDERATIONS]
									 WHERE [CONSIDERATION_ID] = @CONSIDERATION_ID)
				
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END  
		
		if @AMPUTEE_IDEALS_CORRECTION_FACTOR IS NOT NULL
		BEGIN
			IF @AMPUTEE_IDEALS_CORRECTION_FACTOR <> @AMPUTEE_IDEALS_CORRECTION_FACTOR_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'AMPUTEE_IDEALS_CORRECTION_FACTOR'
				SELECT @OLD_VALUE = @AMPUTEE_IDEALS_CORRECTION_FACTOR_OLD
				SELECT @NEW_VALUE = @AMPUTEE_IDEALS_CORRECTION_FACTOR
				EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
			END
		END

	END
END
GO

CREATE TRIGGER [dbo].[ManageAuditTrailForPaitient] 
   ON  [dbo].[tblPATIENT] 
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here

	DECLARE @TEST_ID AS UNIQUEIDENTIFIER
	declare @TEST_TABLE TABLE (
		idx smallint Primary Key IDENTITY(1,1)
		, test_id uniqueidentifier
	)
	DECLARe @PID as uniqueidentifier
	
	SET @PID = (SELECT PID from inserted)
	
	DECLARE @CHANGE_TABLE nvarchar(50)
	DECLARE @CHANGE_FIELD nvarchar(50)
	DECLARE @OLD_VALUE nvarchar(50)
	DECLARE @NEW_VALUE nvarchar(50)
	DECLARE @CHANGED_BY nvarchar(50)
	
	DECLARE @DOB As DATE
	DECLARE @Gender AS nchar(1)
	DECLARE @IS_AMPUTE AS bit
	
	DECLARE @DOB_OLD As DATE
	DECLARE @Gender_OLD AS nchar(1)
	DECLARE @IS_AMPUTEE_OLD AS bit
	  
	SELECT @CHANGE_TABLE = 'tblPATIENT'

	select
	@DOB = [DOB],
	@Gender = [GENDER],
	@IS_AMPUTE = [IS_AMPUTEE]
	from inserted
	
	select
	@DOB_OLD = [DOB],
	@Gender_OLD = [GENDER],
	@IS_AMPUTEE_OLD = [IS_AMPUTEE]
	from deleted
	
	
	insert @TEST_TABLE
	select TEST_ID from [dbo].tblBVA_TEST where [dbo].tblBVA_TEST.PID = @PID
	
	Declare @i int
	declare @numrows int
	
	set @i = 1
	set @numrows = (select COUNT(*) from @TEST_TABLE)
	
	if @numrows>0
		while (@i <= (select MAX(idx) from @TEST_TABLE))
		begin
		
			set @TEST_ID = (Select TEST_ID from @TEST_TABLE where idx = @i)
			SET @CHANGED_BY = (SELECT ANALYST FROM [tblTEST_HEADER]
					WHERE TEST_ID = @TEST_ID)
			if @DOB IS NOT NULL
			BEGIN
				if @DOB <> @DOB_OLD
				BEGIN
					SELECT @CHANGE_FIELD = 'DOB'
					SELECT @OLD_VALUE = @DOB_OLD
					SELECT @NEW_VALUE = @DOB
					EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
				END
			END  
			
			if @Gender IS NOT NULL
			BEGIN
				if @Gender <> @Gender_OLD
				BEGIN
					SELECT @CHANGE_FIELD = 'GENDER'
					SELECT @OLD_VALUE = @Gender_OLD
					SELECT @NEW_VALUE = @Gender
					EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
				END
			END  
			
			if @IS_AMPUTE IS NOT NULL
			BEGIN
				if @IS_AMPUTE <> @IS_AMPUTEE_OLD
				BEGIN
					SELECT @CHANGE_FIELD = 'IS_AMPUTEE'
					SELECT @OLD_VALUE = @IS_AMPUTEE_OLD
					SELECT @NEW_VALUE = @IS_AMPUTE
					EXECUTE [dbo].[InsertAuditTrail] 
										   @TEST_ID
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
				END
			END
set @i = @i+1 

end
END
GO

DELETE FROM [dbo].[tblSETTING] WHERE [SETTING_ID] = N'SYSTEM_SHOW_ORDERING_PHYSICIANS_REPORT_BUTTON'
DELETE FROM [dbo].[tlkpLOCALIZED_SETTING_STRING] WHERE [STRING_ID] = 308

GO

INSERT INTO [dbo].[tblSETTING] ([SETTING_ID], [MODULE_ID], [VALUE], [DATA_TYPE], [VALIDATION_REGEX], [GROUP_ID], [GROUP_DISPLAY_ORDER], [ITEM_SOURCE], [STRING_ID], [AUTHORIZATION_LEVEL_ID]) VALUES (N'SYSTEM_SHOW_ORDERING_PHYSICIANS_REPORT_BUTTON', 1, N'True', N'boolean', NULL, 5, 3, NULL, 308, 2)
INSERT INTO [dbo].[tlkpLOCALIZED_SETTING_STRING] ([STRING_ID], [LANGUAGE_ID], [STRING]) VALUES (308, 1, N'Ordering Physicians Report button is shown on main Utilities screen')

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRecentPhysiciansSpecialties]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[GetRecentPhysiciansSpecialties]

GO

CREATE PROCEDURE [dbo].[GetRecentPhysiciansSpecialties]
	@PHYSICIANS_NAME NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [PHYSICIANS_SPECIALTY] 
	FROM [DaxorLab].[dbo].[tblBVA_TEST] 
	WHERE [REFERRING_PHYSICIAN] = @PHYSICIANS_NAME
	AND [TEST_ID] IN (
				SELECT TOP 1 [TEST_ID]
				FROM [DaxorLab].[dbo].[tblTEST_HEADER]
				WHERE [TEST_ID] IN (
								  SELECT [TEST_ID]
								  FROM [DaxorLab].[dbo].[tblBVA_TEST]
								  WHERE [REFERRING_PHYSICIAN] = @PHYSICIANS_NAME
								  AND NOT [PHYSICIANS_SPECIALTY] = ''
								)
				ORDER BY [TEST_DATE] DESC
				)
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetOrderingPhysiciansBetween]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[GetOrderingPhysiciansBetween]

GO

CREATE PROCEDURE [dbo].[GetOrderingPhysiciansBetween]
@DateFrom DATETIME,
@DateTo DATETIME
AS
BEGIN
	SET NOCOUNT ON;
	SELECT [tblTEST_HEADER].[TEST_DATE]
	  ,[tblTEST_HEADER].[ANALYST]
	  ,[tblBVA_TEST].[REFERRING_PHYSICIAN]
	  ,[tblBVA_TEST].[PHYSICIANS_SPECIALTY]
	  ,[tblBVA_TEST].[CC_REPORT_TO]
	  ,[tblBVA_TEST].[LOCATION]
	FROM [tblTEST_HEADER],[tblBVA_TEST] 
	where [tblTEST_HEADER].[TEST_ID] = [tblBVA_TEST].[TEST_ID]
	AND [tblBVA_TEST].[MODE_ID] = 0
	AND [tblTEST_HEADER].[STATUS_ID] = 4
	AND [tblTEST_HEADER].TEST_DATE BETWEEN @DateFrom AND @DateTo
END

GO

USE [DaxorLab]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tlkpPOSITION_tblSC_LAYOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tlkpPOSITION]'))
ALTER TABLE [dbo].[tlkpPOSITION] DROP CONSTRAINT [FK_tlkpPOSITION_tblSC_LAYOUT]
GO
USE [DaxorLab]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tlkpPOSITION_tblSC_LAYOUT]') AND parent_object_id = OBJECT_ID(N'[dbo].[tlkpPOSITION]'))
ALTER TABLE [dbo].[tlkpPOSITION] DROP CONSTRAINT [FK_tlkpPOSITION_tblSC_LAYOUT]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tblSAMPLE_HEADER_tlkpPOSITION]') AND parent_object_id = OBJECT_ID(N'[dbo].[tblSAMPLE_HEADER]'))
ALTER TABLE [dbo].[tblSAMPLE_HEADER] DROP CONSTRAINT [FK_tblSAMPLE_HEADER_tlkpPOSITION]
GO

USE [DaxorLab]
GO

/****** Object:  Table [dbo].[tlkpPOSITION]    Script Date: 03/12/2015 12:48:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tlkpPOSITION]') AND type in (N'U'))
DROP TABLE [dbo].[tlkpPOSITION]
GO

USE [DaxorLab]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tlkpPOSITION](
	[SAMPLE_POSITION] [int] NOT NULL,
	[SC_LAYOUT_ID] [int] NOT NULL,
	[SAMPLE_TYPE] [nvarchar](50) NOT NULL,
	[RANGE] [nchar](1) NOT NULL,
	[POSITION_NAME] [nvarchar](50) NOT NULL,
	[SAMPLE_NUMBER] [int] NULL,
	[SORT_ORDER] [int] NULL,
	[QC_VALUE] [nvarchar](50) NULL,
	[QC_POSITION_NAME] [nvarchar] (50) NULL,
 CONSTRAINT [PK_tlkpPOSITION] PRIMARY KEY CLUSTERED 
(
	[SAMPLE_POSITION] ASC,
	[SC_LAYOUT_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tlkpPOSITION]  WITH CHECK ADD  CONSTRAINT [FK_tlkpPOSITION_tblSC_LAYOUT] FOREIGN KEY([SC_LAYOUT_ID])
REFERENCES [dbo].[tlkpSC_LAYOUT] ([SC_LAYOUT_ID])
GO

ALTER TABLE [dbo].[tlkpPOSITION] CHECK CONSTRAINT [FK_tlkpPOSITION_tblSC_LAYOUT]
GO

INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (1, 1, N'Standard', N'A', N'Standard A', NULL, NULL, NULL, N'Standard A Carrier')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (1, 2, N'QC', N' ', N'QC 1', 1, 1, N'Calibration', N'QC 1')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (2, 1, N'Baseline', N'A', N'Baseline A', NULL, NULL, NULL, N'Baseline A Carrier')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (2, 2, N'QC', N' ', N'QC 2', 2, 2, N'Verification', N'QC 2')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (3, 1, N'Sample', N'A', N'Sample 1A', 1, NULL, NULL, N'Carrier 1A')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (3, 2, N'Standard', N'A', N'Standard A', NULL, 2, NULL, N'Standard A Carrier')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (4, 1, N'Sample', N'A', N'Sample 2A', 2, NULL, NULL, N'Carrier 2A')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (4, 2, N'Baseline', N'A', N'Baseline A', NULL, 3, NULL, N'Baseline A Carrier')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (5, 1, N'Sample', N'A', N'Sample 3A', 3, NULL, NULL, N'Carrier 3A')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (5, 2, N'Sample', N'A', N'Sample 1A', 1, 4, NULL, N'Carrier 1A')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (6, 1, N'Sample', N'A', N'Sample 4A', 4, NULL, NULL, N'Carrier 4A')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (6, 2, N'Sample', N'A', N'Sample 2A', 2, 5, NULL, N'Carrier 2A')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (7, 1, N'Sample', N'A', N'Sample 5A', 5, NULL, NULL, N'Carrier 5A')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (7, 2, N'Sample', N'A', N'Sample 3A', 3, 6, NULL, N'Carrier 3A')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (8, 1, N'Sample', N'A', N'Sample 6A', 6, NULL, NULL, N'Carrier 6A')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (8, 2, N'Sample', N'A', N'Sample 4A', 4, 7, NULL, N'Carrier 4A')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (9, 1, N'QCStandard', N'A', N'Standard A', NULL, NULL, NULL, N'Standard A Carrier')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (9, 2, N'Sample', N'A', N'Sample 5A', 5, 8, NULL, N'Carrier 5A')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (10, 1, N'QC', N' ', N'QC 1', 1, NULL, NULL, N'QC 1')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (10, 2, N'Sample', N'A', N'Sample 6A', 6, 9, NULL, N'Carrier 6A')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (11, 1, N'QC', N' ', N'QC 2', 2, NULL, NULL, N'QC 2')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (11, 2, N'QCStandard', N'A', N'Standard A', NULL, NULL, NULL, N'Standard A')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (12, 1, N'QC', N' ', N'QC 3', 3, NULL, NULL, N'QC 3')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (12, 2, N'QCStandard', N'B', N'Standard B', NULL, NULL, NULL, N'Standard B')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (13, 1, N'QC', N' ', N'QC 4', 4, NULL, NULL, N'QC 4')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (13, 2, N'Sample', N'B', N'Sample 6B', 6, 12, NULL, N'Carrier 6B')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (14, 1, N'QCStandard', N'B', N'Standard B', NULL, NULL, NULL, N'Standard B Carrier')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (14, 2, N'Sample', N'B', N'Sample 5B', 5, 11, NULL, N'Carrier 5B')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (15, 1, N'Sample', N'B', N'Sample 6B', 6, NULL, NULL, N'Carrier 6B')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (15, 2, N'Sample', N'B', N'Sample 4B', 4, 10, NULL, N'Carrier 4B')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (16, 1, N'Sample', N'B', N'Sample 5B', 5, NULL, NULL, N'Carrier 5B')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (16, 2, N'Sample', N'B', N'Sample 3B', 3, 8, NULL, N'Carrier 3B')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (17, 1, N'Sample', N'B', N'Sample 4B', 4, NULL, NULL, N'Carrier 4B')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (17, 2, N'Sample', N'B', N'Sample 2B', 2, 7, NULL, N'Carrier 2B')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (18, 1, N'Sample', N'B', N'Sample 3B', 3, NULL, NULL, N'Carrier 3B')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (18, 2, N'Sample', N'B', N'Sample 1B', 1, 6, NULL, N'Carrier 1B')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (19, 1, N'Sample', N'B', N'Sample 2B', 2, NULL, NULL, N'Carrier 2B')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (19, 2, N'Baseline', N'B', N'Baseline B', NULL, 5, NULL, N'Baseline B Carrier')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (20, 1, N'Sample', N'B', N'Sample 1B', 1, NULL, NULL, N'Carrier 1B')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (20, 2, N'Standard', N'B', N'Standard B', NULL, 4, NULL, N'Standard B Carrier')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (21, 1, N'Baseline', N'B', N'Baseline B', NULL, NULL, NULL, N'Baseline B Carrier')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (21, 2, N'QC', N' ', N'QC 3', 3, 3, N'Verification', N'QC 3')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (22, 1, N'Standard', N'B', N'Standard B', NULL, NULL, NULL, N'Standard B Carrier')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (22, 2, N'QC', N' ', N'QC 4', 4, 2, N'Verification', N'QC 4')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (23, 1, N'Empty', N' ', N'Empty Carrier', NULL, NULL, NULL, N'Empty Carrier')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (23, 2, N'Empty', N' ', N'Empty Carrier', NULL, 1, NULL, N'Empty Carrier')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (24, 1, N'Background', N' ', N'Background', NULL, NULL, NULL, N'Background')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (24, 2, N'Background', N' ', N'Background', NULL, NULL, NULL, N'Background')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (25, 1, N'Empty', N' ', N'Empty Carrier', NULL, NULL, NULL, N'Empty Carrier')
INSERT INTO [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID], [SAMPLE_TYPE], [RANGE], [POSITION_NAME], [SAMPLE_NUMBER], [SORT_ORDER], [QC_VALUE], [QC_POSITION_NAME]) VALUES (25, 2, N'Empty', N' ', N'Empty Carrier', NULL, 1, NULL, N'Empty Carrier')
GO

ALTER TABLE [dbo].[tblSAMPLE_HEADER]  WITH CHECK ADD  CONSTRAINT [FK_tblSAMPLE_HEADER_tlkpPOSITION] FOREIGN KEY([SAMPLE_POSITION], [SC_LAYOUT_ID])
REFERENCES [dbo].[tlkpPOSITION] ([SAMPLE_POSITION], [SC_LAYOUT_ID])
GO

ALTER TABLE [dbo].[tblSAMPLE_HEADER] CHECK CONSTRAINT [FK_tblSAMPLE_HEADER_tlkpPOSITION]
GO

USE [DaxorLab]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER TRIGGER [dbo].[ManageAuditTrailForSampleHeader] 
   ON  [dbo].[tblBVA_SAMPLE]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here
    DECLARE @TEST_ID AS UNIQUEIDENTIFIER
    DECLARE @SAMPLE_ID AS UNIQUEIDENTIFIER
    DECLARE @STATUS AS INT
    DECLARE @POSITION AS INT

    SET @SAMPLE_ID = (SELECT SAMPLE_ID FROM INSERTED)

    SET @TEST_ID = (SELECT TEST_ID FROM INSERTED join tblTEST_SAMPLES ON INSERTED.SAMPLE_ID = tblTEST_SAMPLES.SAMPLE_ID)

    SET @POSITION = (SELECT SAMPLE_POSITION FROM INSERTED join tblTEST_SAMPLES ON INSERTED.SAMPLE_ID = tblTEST_SAMPLES.SAMPLE_ID 
					JOIN tblSAMPLE_HEADER ON tblSAMPLE_HEADER.SAMPLE_ID = tblTEST_SAMPLES.SAMPLE_ID)

	DECLARE @POSITION_DATA AS NVARCHAR(50)

	SELECT @POSITION_DATA = '(' + dbo.fn_GetPositionDescription(@POSITION, dbo.fn_GetSettingValue('SYSTEM_SAMPLE_CHANGER_LAYOUT_ID')) + ')'			 
    SET @STATUS = (SELECT STATUS_ID FROM [tblTEST_HEADER]
					WHERE TEST_ID = @TEST_ID)
	IF @STATUS IS NOT NULL AND
		@STATUS = (SELECT [dbo].[fn_GetStatusID] ('Completed'))
	BEGIN
		DECLARE @CHANGE_TABLE nvarchar(50)
		DECLARE @CHANGE_FIELD nvarchar(50)
		DECLARE @OLD_VALUE nvarchar(50)
		DECLARE @NEW_VALUE nvarchar(50)
		DECLARE @CHANGED_BY nvarchar(50)

		SELECT @CHANGE_TABLE = 'tblBVA_SAMPLE'

		DECLARE @IS_SAMPLE_EXCLUDED AS NCHAR(1)
		DECLARE @IS_COUNT_EXCLUDED AS NCHAR(1)
		DECLARE @IS_COUNT_AUTO_EXCLUDED AS NCHAR(1)
		DECLARE @IS_HEMATOCRIT_EXCLUDED AS NCHAR(1)
		DECLARE @HEMATOCRIT AS FLOAT
		DECLARE @TIME_POST_INJECTION AS INT
		DECLARE @COUNTS AS INT


		DECLARE @IS_SAMPLE_EXCLUDED_OLD AS NCHAR(1)
		DECLARE @IS_COUNT_EXCLUDED_OLD AS NCHAR(1)
		DECLARE @IS_COUNT_AUTO_EXCLUDED_OLD AS NCHAR(1)
		DECLARE @IS_HEMATOCRIT_EXCLUDED_OLD AS NCHAR(1)
		DECLARE @HEMATOCRIT_OLD AS FLOAT
		DECLARE @TIME_POST_INJECTION_OLD AS INT
		DECLARE @COUNTS_OLD AS INT

		SELECT @CHANGED_BY = (SELECT [ANALYST] FROM tblTEST_HEADER WHERE TEST_ID = @TEST_ID)

		SELECT 
		@IS_SAMPLE_EXCLUDED = [IS_SAMPLE_EXCLUDED],
		@IS_COUNT_EXCLUDED = [IS_COUNT_EXCLUDED],
		@IS_COUNT_AUTO_EXCLUDED = [IS_COUNT_AUTO_EXCLUDED],
		@IS_HEMATOCRIT_EXCLUDED = [IS_HEMATOCRIT_EXCLUDED],
		@HEMATOCRIT = [HEMATOCRIT],
		@TIME_POST_INJECTION = [TIME_POST_INJECTION],
		@COUNTS = [COUNTS]
		FROM INSERTED


		SELECT 
		@IS_SAMPLE_EXCLUDED_OLD = [IS_SAMPLE_EXCLUDED],
		@IS_COUNT_EXCLUDED_OLD = [IS_COUNT_EXCLUDED],
		@IS_COUNT_AUTO_EXCLUDED_OLD = [IS_COUNT_AUTO_EXCLUDED],
		@IS_HEMATOCRIT_EXCLUDED_OLD = [IS_HEMATOCRIT_EXCLUDED],
		@HEMATOCRIT_OLD = [HEMATOCRIT],
		@TIME_POST_INJECTION_OLD = [TIME_POST_INJECTION],
		@COUNTS_OLD = [COUNTS]
		FROM DELETED

		if @COUNTS is not null
		BEGIN
			if @COUNTS <> @COUNTS_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'COUNTS'
				SELECT @OLD_VALUE = @COUNTS_OLD
				SELECT @NEW_VALUE = @COUNTS

				EXECUTE [dbo].[InsertSampleAuditTrail] 
										   @TEST_ID
										  ,@SAMPLE_ID  
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
										  ,@POSITION_DATA
			END
		END  

		if @IS_SAMPLE_EXCLUDED IS NOT NULL
		BEGIN
			if @IS_SAMPLE_EXCLUDED <> @IS_SAMPLE_EXCLUDED_OLD
			BEGIN
				SELECT @CHANGE_FIELD = 'IS_SAMPLE_EXCLUDED'
				SELECT @OLD_VALUE = @IS_SAMPLE_EXCLUDED_OLD
				SELECT @NEW_VALUE = @IS_SAMPLE_EXCLUDED

				EXECUTE [dbo].[InsertSampleAuditTrail] 
										   @TEST_ID
										  ,@SAMPLE_ID  
										  ,@CHANGE_TABLE
										  ,@CHANGE_FIELD
										  ,@OLD_VALUE
										  ,@NEW_VALUE
										  ,@CHANGED_BY
										  ,@POSITION_DATA
			END
		END  

	END

	if @IS_COUNT_EXCLUDED IS NOT NULL
	BEGIN
		if @IS_COUNT_EXCLUDED <> @IS_COUNT_EXCLUDED_OLD and not (@IS_COUNT_AUTO_EXCLUDED <> @IS_COUNT_AUTO_EXCLUDED_OLD)
		BEGIN
			SELECT @CHANGE_FIELD = 'IS_COUNT_EXCLUDED'
			SELECT @OLD_VALUE = @IS_COUNT_EXCLUDED_OLD
			SELECT @NEW_VALUE = @IS_COUNT_EXCLUDED

			EXECUTE [dbo].[InsertSampleAuditTrail] 
									   @TEST_ID
									  ,@SAMPLE_ID  
									  ,@CHANGE_TABLE
									  ,@CHANGE_FIELD
									  ,@OLD_VALUE
									  ,@NEW_VALUE
									  ,@CHANGED_BY
									  ,@POSITION_DATA
		END
	END  

	if @IS_COUNT_AUTO_EXCLUDED IS NOT NULL 
	BEGIN
		if @IS_COUNT_AUTO_EXCLUDED <> @IS_COUNT_AUTO_EXCLUDED_OLD
		BEGIN
			SELECT @CHANGE_FIELD = 'IS_COUNT_AUTO_EXCLUDED'
			SELECT @OLD_VALUE = @IS_COUNT_AUTO_EXCLUDED_OLD
			SELECT @NEW_VALUE = @IS_COUNT_AUTO_EXCLUDED

			EXECUTE [dbo].[InsertSampleAuditTrail] 
									   @TEST_ID
									  ,@SAMPLE_ID  
									  ,@CHANGE_TABLE
									  ,@CHANGE_FIELD
									  ,@OLD_VALUE
									  ,@NEW_VALUE
									  ,@CHANGED_BY
									  ,@POSITION_DATA
		END
	END  

	if @IS_HEMATOCRIT_EXCLUDED IS NOT NULL
	BEGIN
		if @IS_HEMATOCRIT_EXCLUDED <> @IS_HEMATOCRIT_EXCLUDED_OLD
		BEGIN
			SELECT @CHANGE_FIELD = 'IS_HEMATOCRIT_EXCLUDED'
			SELECT @OLD_VALUE = @IS_HEMATOCRIT_EXCLUDED_OLD
			SELECT @NEW_VALUE = @IS_HEMATOCRIT_EXCLUDED

			EXECUTE [dbo].[InsertSampleAuditTrail] 
									   @TEST_ID
									  ,@SAMPLE_ID  
									  ,@CHANGE_TABLE
									  ,@CHANGE_FIELD
									  ,@OLD_VALUE
									  ,@NEW_VALUE
									  ,@CHANGED_BY
									  ,@POSITION_DATA
		END
	END  

	if @TIME_POST_INJECTION IS NOT NULL
	BEGIN
		if @TIME_POST_INJECTION <> @TIME_POST_INJECTION_OLD
		BEGIN
			SELECT @CHANGE_FIELD = 'TIME_POST_INJECTION'
			SELECT @OLD_VALUE = @TIME_POST_INJECTION_OLD
			SELECT @NEW_VALUE = @TIME_POST_INJECTION

			EXECUTE [dbo].[InsertSampleAuditTrail] 
									   @TEST_ID
									  ,@SAMPLE_ID  
									  ,@CHANGE_TABLE
									  ,@CHANGE_FIELD
									  ,@OLD_VALUE
									  ,@NEW_VALUE
									  ,@CHANGED_BY
									  ,@POSITION_DATA
		END
	END  

	if @HEMATOCRIT IS NOT NULL
	BEGIN
		if @HEMATOCRIT <> @HEMATOCRIT_OLD
		BEGIN
			SELECT @CHANGE_FIELD = 'HEMATOCRIT'
			SELECT @OLD_VALUE = @HEMATOCRIT_OLD
			SELECT @NEW_VALUE = @HEMATOCRIT

			EXECUTE [dbo].[InsertSampleAuditTrail] 
									   @TEST_ID
									  ,@SAMPLE_ID  
									  ,@CHANGE_TABLE
									  ,@CHANGE_FIELD
									  ,@OLD_VALUE
									  ,@NEW_VALUE
									  ,@CHANGED_BY
									  ,@POSITION_DATA
		END
	END  

END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBVATestSample]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[GetBVATestSample]
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBVATestSamples]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[GetBVATestSamples]

GO
-- Stored Procedure

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetBVATestSample] 
(
	@SAMPLE_ID AS Uniqueidentifier
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [tblSAMPLE_HEADER].[SAMPLE_ID]
      ,[SAMPLE_POSITION]
      , (SELECT [dbo].[fn_GetPositionDescription] ([SAMPLE_POSITION], [SC_LAYOUT_ID])) AS POSITION_NAME
      ,[SC_LAYOUT_ID]
      ,[PRESET_LIVE_TIME]
      ,[LIVE_TIME]
      ,[REAL_TIME]
      ,[IS_SAMPLE_EXCLUDED]
      ,[IS_COUNT_EXCLUDED]
      ,[IS_COUNT_AUTO_EXCLUDED]
      ,[IS_HEMATOCRIT_EXCLUDED]
      ,[HEMATOCRIT]
      ,[ROI_LOW_BOUND]
      ,[ROI_HIGH_BOUND]
      ,[COUNTS]
      ,[FWHM]
      ,[FWTM]
      ,[CENTROID]
      ,[TEST_ID]
      ,[TIME_POST_INJECTION]
      ,[tblSAMPLE_HEADER].SPECTRUM
      ,[tblSAMPLE_HEADER].ROW_TIMESTAMP AS SAMPLE_HEADER_TIMESTAMP
      ,[tblBVA_SAMPLE].ROW_TIMESTAMP AS BVA_SAMPLE_TIMESTAMP
	FROM [tblSAMPLE_HEADER] JOIN [tblBVA_SAMPLE] ON [tblSAMPLE_HEADER].SAMPLE_ID  = [tblBVA_SAMPLE].SAMPLE_ID
		JOIN [tblTEST_SAMPLES] ON [tblTEST_SAMPLES].SAMPLE_ID = [tblSAMPLE_HEADER].SAMPLE_ID
	WHERE [tblSAMPLE_HEADER].[SAMPLE_ID] = @SAMPLE_ID 
END
GO

-- Stored Procedure

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetBVATestSamples]
(
	@TEST_ID AS Uniqueidentifier
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TEST_ID  
		,[tblSAMPLE_HEADER].[SAMPLE_ID]
      ,[SAMPLE_POSITION]
      , (SELECT [dbo].[fn_GetPositionDescription] ([SAMPLE_POSITION], [SC_LAYOUT_ID])) AS POSITION_NAME
      ,[SC_LAYOUT_ID]
      ,[PRESET_LIVE_TIME]
      ,[LIVE_TIME]
      ,[REAL_TIME]
      ,[IS_SAMPLE_EXCLUDED]
      ,[IS_COUNT_EXCLUDED]
      ,[IS_COUNT_AUTO_EXCLUDED]
      ,[IS_HEMATOCRIT_EXCLUDED]
      ,[HEMATOCRIT]
      ,[ROI_LOW_BOUND]
      ,[ROI_HIGH_BOUND]
      ,[COUNTS]
      ,[FWHM]
      ,[FWTM]
      ,[CENTROID]
	  ,[TEST_ID]
      ,[TIME_POST_INJECTION]
      ,[tblSAMPLE_HEADER].SPECTRUM
      ,[tblSAMPLE_HEADER].ROW_TIMESTAMP AS SAMPLE_HEADER_TIMESTAMP
      ,[tblBVA_SAMPLE].ROW_TIMESTAMP AS BVA_SAMPLE_TIMESTAMP
	FROM [tblSAMPLE_HEADER] JOIN [tblBVA_SAMPLE] ON [tblSAMPLE_HEADER].SAMPLE_ID  = [tblBVA_SAMPLE].SAMPLE_ID
		JOIN [tblTEST_SAMPLES] ON [tblTEST_SAMPLES].SAMPLE_ID = [tblSAMPLE_HEADER].SAMPLE_ID
	WHERE TEST_ID = @TEST_ID 
	ORDER BY SAMPLE_POSITION 
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCurrentQCCalibrationCurve]') and type in (N'P',N'PC'))
DROP PROCEDURE [dbo].[GetCurrentQCCalibrationCurve]

GO

CREATE PROCEDURE [dbo].[GetCurrentQCCalibrationCurve] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP 1 [CALIBRATION_CURVE_ID] 
	  ,[SLOPE]
      ,[OFFSET_VALUE]
	FROM [tblQC_CALIBRATION_CURVE]
	ORDER BY [CALIBRATION_CURVE_ID] DESC
END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPositionsList]'))
DROP PROCEDURE [dbo].[GetPositionsList]

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GetPositionsList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @SC_LAYOUT_ID AS INTEGER
    -- Insert statements for procedure here
    SELECT [SAMPLE_POSITION]
      ,[SC_LAYOUT_ID]
      ,[SAMPLE_TYPE]
      ,[RANGE]
      ,[POSITION_NAME]
      ,[SAMPLE_NUMBER]
      ,[QC_POSITION_NAME]
	FROM [dbo].[tlkpPOSITION]
	ORDER BY [SC_LAYOUT_ID]
END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetQCCalibrationTestPositionList]'))
DROP PROCEDURE [dbo].[GetQCCalibrationTestPositionList]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetQCCalibrationTestPositionList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [SAMPLE_POSITION]
      ,[SC_LAYOUT_ID]
      ,[SAMPLE_TYPE]
      ,[RANGE]
      ,[QC_POSITION_NAME]
      ,[SAMPLE_NUMBER]
      ,[SORT_ORDER]
	FROM [dbo].[tlkpPOSITION]
	WHERE [SAMPLE_POSITION] IN (24, 1, 2)
	AND [SC_LAYOUT_ID] = (SELECT [dbo].[fn_GetSettingValue] ('SYSTEM_SAMPLE_CHANGER_LAYOUT_ID'))
	ORDER BY ([SAMPLE_POSITION] + 1) % 25
END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetQCContaminationTestPositionList]'))
DROP PROCEDURE [dbo].[GetQCContaminationTestPositionList]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetQCContaminationTestPositionList] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [SAMPLE_POSITION]
      ,[SC_LAYOUT_ID]
      ,[SAMPLE_TYPE]
      ,[RANGE]
      ,[QC_POSITION_NAME]
      ,[SAMPLE_NUMBER]
      ,[SORT_ORDER]
	FROM [dbo].[tlkpPOSITION]
	WHERE [SAMPLE_POSITION] IN (24, 25, 3, 4, 5, 6, 7, 8, 9, 10, 13, 14, 15, 16, 17, 18, 19, 20, 23)
	AND [SC_LAYOUT_ID] = (SELECT [dbo].[fn_GetSettingValue] ('SYSTEM_SAMPLE_CHANGER_LAYOUT_ID'))
	ORDER BY ([SAMPLE_POSITION] + 1) % 25
  END

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetQCStandardsTestPositionList]'))
DROP PROCEDURE [dbo].[GetQCStandardsTestPositionList]

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetQCStandardsTestPositionList]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [SAMPLE_POSITION]
      ,[SC_LAYOUT_ID]
      ,[SAMPLE_TYPE]
      ,[RANGE]
      ,[QC_POSITION_NAME]
      , (SELECT [dbo].[fn_GetPositionDescription] ([SAMPLE_POSITION], [SC_LAYOUT_ID])) AS POSITION_DESCRIPTION
      ,[SAMPLE_NUMBER]
      ,[SORT_ORDER]
	FROM [dbo].[tlkpPOSITION]
	WHERE [SAMPLE_POSITION] IN (24, 11, 12)
	AND [SC_LAYOUT_ID] = (SELECT [dbo].[fn_GetSettingValue] ('SYSTEM_SAMPLE_CHANGER_LAYOUT_ID'))
	ORDER BY ([SAMPLE_POSITION] + 1) % 25
END

GO

set noexec off

COMMIT TRANSACTION;