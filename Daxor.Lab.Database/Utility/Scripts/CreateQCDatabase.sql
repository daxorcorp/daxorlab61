USE [master]
GO

DECLARE @dbname nvarchar(128)
SET @dbname = N'QCDatabaseArchive'

IF (NOT EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE ('[' + name + ']' = @dbname OR name = @dbname)))
RESTORE DATABASE [QCDatabaseArchive] FROM  DISK = N'C:\ProgramData\Daxor\Lab\InitialDatabase\Empty QC Database Archive.bak' 
WITH  FILE = 1,  
MOVE N'QCDatabaseArchive' TO N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\QCDatabaseArchive.mdf',  
MOVE N'QCDatabaseArchive_log' TO N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\QCDatabaseArchive_1.ldf',  
NOUNLOAD,  REPLACE,  STATS = 10

GO
