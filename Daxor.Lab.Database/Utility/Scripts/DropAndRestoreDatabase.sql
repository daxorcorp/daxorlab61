﻿EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'DaxorLab'
GO
USE [master]
GO

DROP DATABASE [DaxorLab]
GO

RESTORE DATABASE [DaxorLab] FROM  DISK = N'C:\ProgramData\Daxor\Lab\InitialDatabase\Empty Daxor Lab Database.bak' 
WITH  FILE = 1,  
MOVE N'Daxor Diagnostic Suite' TO N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DaxorLab.mdf',  
MOVE N'Daxor Diagnostic Suite_log' TO N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\DaxorLab_1.ldf',  
NOUNLOAD,  REPLACE,  STATS = 10
GO

EXEC msdb.dbo.sp_delete_database_backuphistory @database_name = N'QCDatabaseArchive'
GO
USE [master]
GO
DROP DATABASE [QCDatabaseArchive]
GO

RESTORE DATABASE [QCDatabaseArchive] FROM  DISK = N'C:\ProgramData\Daxor\Lab\InitialDatabase\Empty QC Database Archive.bak' 
WITH  FILE = 1,  
MOVE N'QCDatabaseArchive' TO N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\QCDatabaseArchive.mdf',  
MOVE N'QCDatabaseArchive_log' TO N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\QCDatabaseArchive_1.ldf',  
NOUNLOAD,  REPLACE,  STATS = 10
GO