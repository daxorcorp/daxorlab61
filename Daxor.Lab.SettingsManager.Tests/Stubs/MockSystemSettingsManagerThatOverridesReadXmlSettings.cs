using Daxor.Lab.Database.Interfaces;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.SettingsManager.Tests.Stubs
{
    public class MockSystemSettingsManagerThatOverridesReadXmlSettings : SystemSettingsManager
    {
        public MockSystemSettingsManagerThatOverridesReadXmlSettings(ILoggerFacade logger, ISettingsManagerDataService settingsManagerDataService):
            base(logger, settingsManagerDataService) { }

        protected override void ReadXmlSettings(string fileName) { }
    }
}