﻿using System;
using System.ComponentModel;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.SettingsManager.Tests.Stubs
{
	public class MockSetting : ISetting
	{
		public event PropertyChangedEventHandler PropertyChanged;
		public string Key { get; private set; }
		public object Value { get; set; }
		public SettingSource Source { get; private set; }
		public Type Type { get; private set; }

		public MockSetting(string key, SettingSource source, Type type)
		{
			Key = key;
			Source = source;
			Type = type;
		}

	    // ReSharper disable once UnusedMember.Local
	    private void IgnoreToSatisfyCompiler()
	    {
	        if (PropertyChanged != null)
	            PropertyChanged(this, null);
	    }
	}
}
