using System;
using Daxor.Lab.Database.Interfaces;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.SettingsManager.Tests.Stubs
{
    internal class MockSystemSettingsManagerThatThrowsAnExceptionDuringInstantiation : SystemSettingsManager
    {
        public MockSystemSettingsManagerThatThrowsAnExceptionDuringInstantiation(ILoggerFacade logger, ISettingsManagerDataService settingsManagerDataService) :
                base(logger, settingsManagerDataService)
        {}

        protected override void ReadXmlSettings(string fileName)
        {
            throw new InvalidOperationException(SettingsManager_Tests.SadPath.When_the_settings_manager_is_initialized.ExpectedExceptionMessage);
        }
    }
}