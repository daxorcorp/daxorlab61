﻿using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.SettingsManager.Tests.Stubs
{
	public class MockSystemSettingsManagerThatOverridesAllCtorOverheadMethods : SystemSettingsManager
	{
		public MockSystemSettingsManagerThatOverridesAllCtorOverheadMethods(ILoggerFacade logger, ISettingsManagerDataService settingsManagerDataService, string fileName = ".\\Settings.xml") : base(logger, settingsManagerDataService, fileName)
		{
		}

		protected override void ConfigureDatabaseSettingsHelper()
		{
			var intSetting = new MockSetting("int", SettingSource.RunTime, typeof(int)) { Value = 1 };
			var doubleSetting = new MockSetting("double", SettingSource.RunTime, typeof(double)) { Value = 2.5 };
			var boolSetting = new MockSetting("bool", SettingSource.RunTime, typeof(bool)) { Value = false };
			var stringSetting = new MockSetting("string", SettingSource.RunTime, typeof(string)) { Value = "mockValue" };
			var databaseSetting = new MockSetting("database", SettingSource.Database, typeof(string)) { Value = "mockValue" };

			AddSetting(intSetting);
			AddSetting(doubleSetting);
			AddSetting(boolSetting);
			AddSetting(stringSetting);
			AddSetting(databaseSetting);
		}

		protected override void ConfigureSystemSettingsManager()
		{
		}
	}
}
