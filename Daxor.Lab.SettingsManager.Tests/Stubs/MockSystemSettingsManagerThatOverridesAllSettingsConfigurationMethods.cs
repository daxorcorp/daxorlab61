﻿using Daxor.Lab.Database.Interfaces;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.SettingsManager.Tests.Stubs
{
    internal class MockSystemSettingsManagerThatOverridesAllSettingsConfigurationMethods : SystemSettingsManager
    {
        public MockSystemSettingsManagerThatOverridesAllSettingsConfigurationMethods(ILoggerFacade logger, ISettingsManagerDataService settingsManagerDataService):
            base(logger, settingsManagerDataService)
        { }

        public bool ReadXmlSettingsWasCalled { get; set; }
        public bool ReadDatabaseSettingsWasCalled { get; set; }
        public bool SetRuntimeSettingsWasCalled { get; set; }

        protected override void ReadXmlSettings(string fileName)
        {
            ReadXmlSettingsWasCalled = true;
        }

        protected override void ReadDatabaseSettings()
        {
            ReadDatabaseSettingsWasCalled = true;
        }

        protected override void SetRunTimeSettings()
        {
            SetRuntimeSettingsWasCalled = true;
        }
    }
}