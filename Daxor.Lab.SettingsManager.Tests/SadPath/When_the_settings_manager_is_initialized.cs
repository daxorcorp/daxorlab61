﻿using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.SettingsManager.Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    // ReSharper disable ObjectCreationAsStatement
    [TestClass]
    public class When_the_settings_manager_is_initialized
    {
        [TestMethod]
        public void And_the_logger_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() => { new SystemSettingsManager(null,  Substitute.For<ISettingsManagerDataService>()); }, "logger");
        }

        [TestMethod]
        public void And_the_database_stauts_service_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(() => { new SystemSettingsManager(Substitute.For<ILoggerFacade>(), null); }, "settingsManagerDataService");
        }
    }
    // ReSharper restore ObjectCreationAsStatement
    // ReSharper restore InconsistentNaming
}
