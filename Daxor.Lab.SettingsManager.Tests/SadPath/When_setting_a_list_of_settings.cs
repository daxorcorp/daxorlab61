﻿using System.Collections.Generic;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.SettingsManager.Models;
using Daxor.Lab.SettingsManager.Tests.Stubs;
using Daxor.Lab.Utility.Models;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.SettingsManager.Tests.SadPath
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_setting_a_list_of_settings
	{
		private ILoggerFacade _mockLogger;
		private ISettingsManagerDataService _stubDataService;

		private MockSystemSettingsManagerThatOverridesAllCtorOverheadMethods _settingsManager;

		[TestInitialize]
		public void Initialize()
		{
			_mockLogger = Substitute.For<ILoggerFacade>();
			_stubDataService = Substitute.For<ISettingsManagerDataService>();

			_settingsManager = new MockSystemSettingsManagerThatOverridesAllCtorOverheadMethods(_mockLogger, _stubDataService);
		}

		[TestMethod]
		public void And_the_list_is_null_then_nothing_is_changed()
		{
			var expected = _settingsManager.SettingsCollection;

			_settingsManager.SetSettings(null);

			var actual = _settingsManager.SettingsCollection;

			Assert.AreEqual(expected, actual);
		}

		[TestMethod]
		public void And_the_list_is_empty_then_nothing_is_changed()
		{
			var expected = _settingsManager.SettingsCollection;

			_settingsManager.SetSettings(new List<SettingBase>());

			var actual = _settingsManager.SettingsCollection;

			Assert.AreEqual(expected, actual);
		}

		[TestMethod]
		public void And_an_exception_is_thrown_then_it_is_logged()
		{
			var newSetting = new SingleValueSetting("database", "blah", "string", AuthorizationLevel.Service);

			var list = new List<SettingBase> {newSetting};

			_settingsManager.SetSettings(list);

			_mockLogger.Received(1).Log("SetSettings. Value cannot be null.\r\nParameter name: fileOrServerOrConnection", Category.Exception, Priority.High);
		}
	}
	// ReSharper restore InconsistentNaming
}