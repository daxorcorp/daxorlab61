using System.IO;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.SettingsManager.Tests.Stubs;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.SettingsManager.Tests.SettingsManager_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_settings_manager_is_initialized
    {
        private ILoggerFacade _logger;
        private ISettingsManagerDataService _settingsManagerDataService;

        public const string ExpectedExceptionMessage = "This is an exception";

        [TestInitialize]
        public void BeforeEachTest()
        {
            _logger = Substitute.For<ILoggerFacade>();
            _settingsManagerDataService = Substitute.For<ISettingsManagerDataService>();
        }

        [TestMethod]
        public void And_an_exception_is_thrown_then_it_is_logged()
        {
            AssertEx.Throws(() =>
            {
                // ReSharper disable once UnusedVariable
                var notUsed = new MockSystemSettingsManagerThatThrowsAnExceptionDuringInstantiation(_logger, _settingsManagerDataService);
            });

            _logger.Received().Log(ExpectedExceptionMessage, Category.Exception, Priority.High);
        }

        [TestMethod]
        public void And_the_XML_file_cannot_be_found_then_an_exception_is_thrown()
        {
            AssertEx.Throws<FileNotFoundException>(() =>
            {
                // ReSharper disable once UnusedVariable
                var notUsed = new SystemSettingsManager(_logger, _settingsManagerDataService, "some-missing-file.png");
            });
        }
    }
    // ReSharper restore InconsistentNaming
}
