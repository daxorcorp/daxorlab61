﻿using System.Collections.Generic;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.SettingsManager.Models;
using Daxor.Lab.SettingsManager.Tests.Stubs;
using Daxor.Lab.Utility.Models;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.SettingsManager.Tests.SettingsManager_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_setting_a_list_of_settings
	{
		private ILoggerFacade _stubLogger;
		private ISettingsManagerDataService _stubDataService;

		private MockSystemSettingsManagerThatOverridesAllCtorOverheadMethods _settingsManager;

		[TestInitialize]
		public void Initialize()
		{
			_stubLogger = Substitute.For<ILoggerFacade>();
			_stubDataService = Substitute.For<ISettingsManagerDataService>();

			_settingsManager = new MockSystemSettingsManagerThatOverridesAllCtorOverheadMethods(_stubLogger, _stubDataService);
		}

		[TestMethod]
		public void And_trying_to_change_an_integer_setting_to_another_integer_then_the_setting_is_changed()
		{
			//Arrange
			var before = _settingsManager.GetSetting<int>("int");

			Assert.AreEqual(1, before);

			var newIntSetting = new SingleValueSetting("int", 3, "integer", AuthorizationLevel.Service);

			var list = new List<SettingBase> {newIntSetting};

			//Act
			_settingsManager.SetSettings(list);

			//Assert
			var after = _settingsManager.GetSetting<int>("int");

			Assert.AreEqual(3, after);
		}

        [TestMethod]
        public void And_trying_to_change_an_integer_setting_to_the_string_representation_of_another_integer_then_the_setting_is_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(1, before);

            var newIntSetting = new SingleValueSetting("int", "3", "integer", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newIntSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(3, after);
        }

        [TestMethod]
        public void And_trying_to_change_an_integer_setting_to_null_then_the_setting_is_not_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(1, before);

            var newIntSetting = new SingleValueSetting("int", null, "integer", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newIntSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(1, after);
        }

		[TestMethod]
		public void And_trying_to_change_an_integer_setting_to_include_a_string_with_no_numbers_then_the_setting_is_not_changed()
		{
			//Arrange
			var before = _settingsManager.GetSetting<int>("int");

			Assert.AreEqual(1, before);

			var newIntSetting = new SingleValueSetting("int", "abc", "integer", AuthorizationLevel.Service);

			var list = new List<SettingBase> { newIntSetting };

			//Act
			_settingsManager.SetSettings(list);

			//Assert
			var after = _settingsManager.GetSetting<int>("int");

			Assert.AreEqual(1, after);
		}

        [TestMethod]
        public void And_trying_to_change_an_integer_setting_to_include_a_string_with_numbers_and_alpha_characters_then_the_setting_is_not_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(1, before);

            var newIntSetting = new SingleValueSetting("int", "1a2b3c", "integer", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newIntSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(1, after);
        }

        [TestMethod]
        public void And_trying_to_change_an_integer_setting_to_include_a_string_with_only_symbol_characters_then_the_setting_is_not_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(1, before);

            var newIntSetting = new SingleValueSetting("int", ".", "integer", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newIntSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(1, after);
        }

        [TestMethod]
        public void And_trying_to_change_an_integer_setting_to_the_empty_string_then_the_setting_is_not_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(1, before);

            var newIntSetting = new SingleValueSetting("int", string.Empty, "integer", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newIntSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(1, after);
        }

        [TestMethod]
        public void And_trying_to_change_an_integer_setting_to_a_string_representation_of_a_double_then_the_setting_is_not_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(1, before);

            var newIntSetting = new SingleValueSetting("int", "12.95", "integer", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newIntSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(1, after);
        }

        [TestMethod]
        public void And_trying_to_change_an_integer_setting_to_a_double_then_the_setting_is_not_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(1, before);

            var newIntSetting = new SingleValueSetting("int", 12.95, "integer", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newIntSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<int>("int");

            Assert.AreEqual(1, after);
        }

		[TestMethod]
		public void And_trying_to_change_a_double_setting_to_another_double_then_the_setting_is_changed()
		{
			//Arrange
			var before = _settingsManager.GetSetting<double>("double");

			Assert.AreEqual(2.5, before);

			var newDoubleSetting = new SingleValueSetting("double", 3.5, "double", AuthorizationLevel.Service);

			var list = new List<SettingBase> { newDoubleSetting };

			//Act
			_settingsManager.SetSettings(list);

			//Assert
			var after = _settingsManager.GetSetting<double>("double");

			Assert.AreEqual(3.5, after);
		}

        [TestMethod]
        public void And_trying_to_change_a_double_setting_to_the_string_representation_of_another_double_then_the_setting_is_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(2.5, before);

            var newDoubleSetting = new SingleValueSetting("double", "3.5", "double", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newDoubleSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(3.5, after);
        }

        [TestMethod]
        public void And_trying_to_change_a_double_setting_to_an_integer_then_the_setting_is_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(2.5, before);

            var newDoubleSetting = new SingleValueSetting("double", 3 , "double", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newDoubleSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(3, after);
        }

        [TestMethod]
        public void And_trying_to_change_a_double_setting_to_the_string_representation_of_an_integer_then_the_setting_is_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(2.5, before);

            var newDoubleSetting = new SingleValueSetting("double", "3", "double", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newDoubleSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(3, after);
        }

        [TestMethod]
        public void And_trying_to_change_a_double_setting_to_null_then_the_setting_is_not_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(2.5, before);

            var newDoubleSetting = new SingleValueSetting("double", null, "double", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newDoubleSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(2.5, after);
        }

        [TestMethod]
        public void And_trying_to_change_a_double_setting_to_an_empty_string_then_the_setting_is_not_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(2.5, before);

            var newDoubleSetting = new SingleValueSetting("double", string.Empty, "double", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newDoubleSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(2.5, after);
        }

		[TestMethod]
		public void And_trying_to_change_a_double_setting_to_a_string_with_no_numbers_then_the_setting_is_not_changed()
		{
			//Arrange
			var before = _settingsManager.GetSetting<double>("double");

			Assert.AreEqual(2.5, before);

			var newDoubleSetting = new SingleValueSetting("double", "abc", "double", AuthorizationLevel.Service);

			var list = new List<SettingBase> { newDoubleSetting };

			//Act
			_settingsManager.SetSettings(list);

			//Assert
			var after = _settingsManager.GetSetting<double>("double");

			Assert.AreEqual(2.5, after);
		}

        [TestMethod]
        public void And_trying_to_change_a_double_setting_to_a_string_with_numbers_and_alpha_characters_then_the_setting_is_not_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(2.5, before);

            var newDoubleSetting = new SingleValueSetting("double", "1a2b3c", "double", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newDoubleSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(2.5, after);
        }

        [TestMethod]
        public void And_trying_to_change_a_double_setting_to_a_string_with_only_symbol_characters_then_the_setting_is_not_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(2.5, before);

            var newDoubleSetting = new SingleValueSetting("double", ".", "double", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newDoubleSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<double>("double");

            Assert.AreEqual(2.5, after);
        }

		[TestMethod]
		public void And_trying_to_change_a_boolean_setting_to_another_boolean_then_the_setting_is_changed()
		{
			//Arrange
			var before = _settingsManager.GetSetting<bool>("bool");

			Assert.IsFalse(before);

			var newBoolSetting = new SingleValueSetting("bool", true, "boolean", AuthorizationLevel.Service);

			var list = new List<SettingBase> { newBoolSetting };

			//Act
			_settingsManager.SetSettings(list);

			//Assert
			var after = _settingsManager.GetSetting<bool>("bool");

			Assert.IsTrue(after);
		}

        [TestMethod]
        public void And_trying_to_change_a_boolean_setting_to_the_string_representation_of_another_boolean_then_the_setting_is_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<bool>("bool");

            Assert.IsFalse(before);

            var newBoolSetting = new SingleValueSetting("bool", "true", "boolean", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newBoolSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<bool>("bool");

            Assert.IsTrue(after);
        }

        [TestMethod]
        public void And_trying_to_change_a_boolean_setting_to_the_single_character_representation_of_another_boolean_then_the_setting_is_changed()
        {
            //Arrange
            var before = _settingsManager.GetSetting<bool>("bool");

            Assert.IsFalse(before);

            var newBoolSetting = new SingleValueSetting("bool", "t", "boolean", AuthorizationLevel.Service);

            var list = new List<SettingBase> { newBoolSetting };

            //Act
            _settingsManager.SetSettings(list);

            //Assert
            var after = _settingsManager.GetSetting<bool>("bool");

            Assert.IsTrue(after);
        }

		[TestMethod]
		public void And_trying_to_change_a_boolean_setting_to_a_string_then_the_setting_is_not_changed()
		{
			//Arrange
			var before = _settingsManager.GetSetting<bool>("bool");

			Assert.IsFalse(before);

			var newBoolSetting = new SingleValueSetting("bool", "maybe", "boolean", AuthorizationLevel.Service);

			var list = new List<SettingBase> { newBoolSetting };

			//Act
			_settingsManager.SetSettings(list);

			//Assert
			var after = _settingsManager.GetSetting<bool>("bool");

			Assert.IsFalse(after);
		}

		[TestMethod]
		public void And_trying_to_change_a_boolean_setting_to_a_number_then_the_setting_is_not_changed()
		{
			//Arrange
			var before = _settingsManager.GetSetting<bool>("bool");

			Assert.IsFalse(before);

			var newBoolSetting = new SingleValueSetting("bool", 3, "boolean", AuthorizationLevel.Service);

			var list = new List<SettingBase> { newBoolSetting };

			//Act
			_settingsManager.SetSettings(list);

			//Assert
			var after = _settingsManager.GetSetting<bool>("bool");

			Assert.IsFalse(after);
		}

		[TestMethod]
		public void And_changing_a_string_setting_to_any_other_string_then_the_setting_is_changed()
		{
			//Arrange
			var before = _settingsManager.GetSetting<string>("string");

			Assert.AreEqual("mockValue", before);

			var newStringSetting = new SingleValueSetting("string", "blah", "string", AuthorizationLevel.Service);

			var list = new List<SettingBase> { newStringSetting };

			//Act
			_settingsManager.SetSettings(list);

			//Assert
			var after = _settingsManager.GetSetting<string>("string");

			Assert.AreEqual("blah", after);
		}

		[TestMethod]
		public void And_only_one_setting_can_not_update_then_the_others_are_changed()
		{
			//Arrange
			var beforeInt = _settingsManager.GetSetting<int>("int");
			var beforeBool = _settingsManager.GetSetting<bool>("bool");
			var beforeDouble = _settingsManager.GetSetting<double>("double");

			Assert.AreEqual(1, beforeInt);
			Assert.AreEqual(false, beforeBool);
			Assert.AreEqual(2.5, beforeDouble);

			var newIntSetting = new SingleValueSetting("int", 3, "int", AuthorizationLevel.Service);
			var newBoolSetting = new SingleValueSetting("bool", "maybe", "boolean", AuthorizationLevel.Service);
			var newDoubleSetting = new SingleValueSetting("double", 3.5, "double", AuthorizationLevel.Service);

			var list = new List<SettingBase>
			{
				newIntSetting, 
				newBoolSetting, 
				newDoubleSetting
			};

			//Act
			_settingsManager.SetSettings(list);

			//Assert
			var afterInt = _settingsManager.GetSetting<int>("int");
			var afterBool = _settingsManager.GetSetting<bool>("bool");
			var afterDouble = _settingsManager.GetSetting<double>("double");

			Assert.AreEqual(3, afterInt);
			Assert.AreEqual(false, afterBool);
			Assert.AreEqual(3.5, afterDouble);
		}
	}
	// ReSharper restore InconsistentNaming
}
