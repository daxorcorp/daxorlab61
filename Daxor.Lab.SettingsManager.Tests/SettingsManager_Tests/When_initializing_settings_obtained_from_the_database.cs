using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.SettingsManager.Tests.Stubs;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Daxor.Lab.SettingsManager.Tests.SettingsManager_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_settings_obtained_from_the_database
    {
        private ILoggerFacade _logger;
        private ISettingsManagerDataService _settingsManagerDataService;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _logger = Substitute.For<ILoggerFacade>();
            _settingsManagerDataService = Substitute.For<ISettingsManagerDataService>();
        }

        [TestMethod]
        public void And_the_setting_type_is_boolean_and_the_value_is_lowercase_t_then_the_setting_is_true()
        {
            _settingsManagerDataService.GetSettingsFromDatabase(Arg.Any<string>())
                .Returns(new List<GetSettingsResult>
                {
                    new GetSettingsResult {SETTING_ID = "LowerBool", DATA_TYPE = "boolean", VALUE = "t"}
                });

            var settingsManager = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger, _settingsManagerDataService);

            Assert.IsTrue(settingsManager.GetSetting<bool>("LowerBool"));
        }

        [TestMethod]
        public void And_the_setting_type_is_boolean_and_the_value_is_uppercase_t_then_the_setting_is_true()
        {
            _settingsManagerDataService.GetSettingsFromDatabase(Arg.Any<string>())
                .Returns(new List<GetSettingsResult>
                {
                    new GetSettingsResult {SETTING_ID = "UpperBool", DATA_TYPE = "boolean", VALUE = "T"}
                });

            var settingsManager = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger,
                _settingsManagerDataService);

            Assert.IsTrue(settingsManager.GetSetting<bool>("UpperBool"));
        }

        [TestMethod]
        public void And_the_setting_type_is_boolean_and_the_value_is_the_lowercase_string_true_then_the_setting_is_true()
        {
            _settingsManagerDataService.GetSettingsFromDatabase(Arg.Any<string>())
                .Returns(new List<GetSettingsResult>
                {
                    new GetSettingsResult {SETTING_ID = "LowerBool", DATA_TYPE = "boolean", VALUE = "true"}
                });

            var settingsManager = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger, _settingsManagerDataService);

            Assert.IsTrue(settingsManager.GetSetting<bool>("LowerBool"));
        }

        [TestMethod]
        public void And_the_setting_type_is_boolean_and_the_value_is_the_uppercase_string_true_then_the_setting_is_true()
        {
            _settingsManagerDataService.GetSettingsFromDatabase(Arg.Any<string>())
                .Returns(new List<GetSettingsResult>
                {
                    new GetSettingsResult {SETTING_ID = "LowerBool", DATA_TYPE = "boolean", VALUE = "TRUE"}
                });

            var settingsManager = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger, _settingsManagerDataService);

            Assert.IsTrue(settingsManager.GetSetting<bool>("LowerBool"));
        }

        [TestMethod]
        public void And_the_setting_type_is_boolean_and_the_value_is_not_some_representation_of_true_then_the_setting_is_false()
        {
            _settingsManagerDataService.GetSettingsFromDatabase(Arg.Any<string>())
                .Returns(new List<GetSettingsResult>
                {
                    new GetSettingsResult {SETTING_ID = "LowerBool", DATA_TYPE = "boolean", VALUE = "tu"}
                });

            var settingsManager = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger,
                _settingsManagerDataService);

            Assert.IsFalse(settingsManager.GetSetting<bool>("LowerBool"));
        }

        [TestMethod]
        public void And_the_setting_type_is_not_boolean_or_a_collection_then_the_setting_is_set_to_the_string_value()
        {
            _settingsManagerDataService.GetSettingsFromDatabase(Arg.Any<string>())
                .Returns(new List<GetSettingsResult>
                {
                    new GetSettingsResult {SETTING_ID = "LowerString", DATA_TYPE = "string", VALUE = "tu"}
                });

            var settingsManager = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger,
                _settingsManagerDataService);

            Assert.AreEqual("tu", settingsManager.GetSetting<string>("LowerString"));
        }

        [TestMethod]
        public void And_the_setting_type_is_collection_then_the_setting_is_a_properly_initialized_collection()
        {
            _settingsManagerDataService.GetSettingsFromDatabase(Arg.Any<string>())
                .Returns(new List<GetSettingsResult>
                {
                    new GetSettingsResult {SETTING_ID = "SomeCollection", DATA_TYPE = "collection", VALUE = "tu|you"}
                });

            var settingsManager = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger,
                _settingsManagerDataService);

            var observedCollection = settingsManager.GetSetting<IEnumerable>("SomeCollection").OfType<string>().ToArray();

            CollectionAssert.AreEqual(new [] { "tu", "you" }, observedCollection);
        }
    }
    // ReSharper restore InconsistentNaming
}
