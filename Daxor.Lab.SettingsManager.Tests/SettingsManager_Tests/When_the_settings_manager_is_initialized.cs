﻿using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Tests.Stubs;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.SettingsManager.Tests.SettingsManager_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_settings_manager_is_initialized
    {
        private ILoggerFacade _logger;
        private ISettingsManagerDataService _settingsManagerDataService;
        
        [TestInitialize]
        public void BeforeEachTest()
        {
            _logger = Substitute.For<ILoggerFacade>();
            _settingsManagerDataService = Substitute.For<ISettingsManagerDataService>();
        }

        [TestMethod]
        public void Then_the_settings_from_XML_are_obtained()
        {
            var settingsManager = new MockSystemSettingsManagerThatOverridesAllSettingsConfigurationMethods(_logger, _settingsManagerDataService);
            Assert.IsTrue(settingsManager.ReadXmlSettingsWasCalled);
        }

        [TestMethod]
        public void Then_the_settings_from_the_database_are_obtained()
        {
            var settingsManager = new MockSystemSettingsManagerThatOverridesAllSettingsConfigurationMethods(_logger, _settingsManagerDataService);
            Assert.IsTrue(settingsManager.ReadDatabaseSettingsWasCalled);
        }

        [TestMethod]
        public void Then_the_runtime_settings_are_obtained()
        {
            var settingsManager = new MockSystemSettingsManagerThatOverridesAllSettingsConfigurationMethods(_logger, _settingsManagerDataService);
            Assert.IsTrue(settingsManager.SetRuntimeSettingsWasCalled);
        }

        [TestMethod]
        public void And_the_settings_are_being_read_from_the_database_then_the_databases_are_brought_online()
        {
            // ReSharper disable once UnusedVariable
            var notUsed = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger, _settingsManagerDataService);
            _settingsManagerDataService.Received().BringAllDatabasesOnline();
        }

        [TestMethod]
        [RequirementValue("Data Source=;Initial Catalog=;Persist Security Info=True;User ID=;Password=daxor")]
        public void Then_the_database_connection_string_matches_requirements()
        {
            var settingsManager = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger, _settingsManagerDataService);

            // Data Source, Initial Catalog, and User ID have no value because of mocks
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), 
                settingsManager.GetSetting<string>(SettingKeys.SystemLinqConnectionString));
        }

        [TestMethod]
        public void Then_the_system_database_password_setting_is_initialized()
        {
            var settingsManager = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger, _settingsManagerDataService);
            Assert.AreEqual(DomainConstants.DaxorDatabasePassword, settingsManager.GetSetting<string>(SettingKeys.SystemDatabasePassword));
        }

        [TestMethod]
        public void Then_the_export_password_setting_is_initialized()
        {
            var settingsManager = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger, _settingsManagerDataService);

            Assert.AreEqual(string.Empty, settingsManager.GetSetting<string>(RuntimeSettingKeys.SystemPasswordSecureExport));
        }

        [TestMethod]
        [RequirementValue("DaxorSuite")]
        public void Then_the_system_backup_filename_setting_matches_requirements()
        {
            var settingsManager = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger, _settingsManagerDataService);

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), settingsManager.GetSetting<string>(RuntimeSettingKeys.SystemDaxorLabDatabaseBackupFilename));
        }

        [TestMethod]
        [RequirementValue("QCDatabaseArchive")]
        public void Then_the_QC_archive_backup_file_name_setting_matches_requirements()
        {
            var settingsManager = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger, _settingsManagerDataService);

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), settingsManager.GetSetting<string>(RuntimeSettingKeys.SystemQcArchiveDatabaseBackupFilename));
        }

        [TestMethod]
        [RequirementValue("DaxorLabSuite.zip")]
        public void Then_the_manual_backup_filename_setting_is_initialized()
        {
            var settingsManager = new MockSystemSettingsManagerThatOverridesReadXmlSettings(_logger, _settingsManagerDataService);

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), settingsManager.GetSetting<string>(RuntimeSettingKeys.SystemManualBackupFileName));
        }
    }
    // ReSharper restore RedundantArgumentName
}