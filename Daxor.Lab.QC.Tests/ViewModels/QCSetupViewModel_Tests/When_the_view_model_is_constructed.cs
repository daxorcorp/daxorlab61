﻿using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.QC.ViewModels;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Tests.ViewModels.QCSetupViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_view_model_is_constructed
    {
        [TestMethod]
        public void And_injection_verification_is_enabled_then_the_flag_is_also_enabled()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var dummyController = MockRepository.GenerateStub<IQualityTestController>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(Arg.Is(SettingKeys.SystemInjectateVerificationEnabled))).Return(true);

            var viewModel = new QCSetupViewModel(dummyController, stubSettingsManager);

            Assert.IsTrue(viewModel.IsInjectateVerificationEnabled);
        }

        [TestMethod]
        public void And_injection_verification_is_disabled_then_the_flag_is_also_disabled()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            var dummyController = MockRepository.GenerateStub<IQualityTestController>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(Arg.Is(SettingKeys.SystemInjectateVerificationEnabled))).Return(false);

            var viewModel = new QCSetupViewModel(dummyController, stubSettingsManager);

            Assert.IsFalse(viewModel.IsInjectateVerificationEnabled);
        }
    }
    // ReSharper restore InconsistentNaming
}
