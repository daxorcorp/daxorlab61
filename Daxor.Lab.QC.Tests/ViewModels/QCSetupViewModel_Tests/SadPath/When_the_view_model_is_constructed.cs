﻿using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.QC.ViewModels;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Tests.ViewModels.QCSetupViewModel_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_view_model_is_constructed
    {
        [TestMethod]
        public void And_the_controller_instance_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var viewModel = new QCSetupViewModel(null, MockRepository.GenerateStub<ISettingsManager>());
            }, "controller");
            // ReSharper restore UnusedVariable
        }

        [TestMethod]
        public void And_the_settings_manager_instance_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable UnusedVariable
            AssertEx.ThrowsArgumentNullException(() =>
            {
                var viewModel = new QCSetupViewModel(MockRepository.GenerateStub<IQualityTestController>(), null);
            }, "settingsManager");
            // ReSharper restore UnusedVariable
        }
    }
    // ReSharper restore InconsistentNaming
}
