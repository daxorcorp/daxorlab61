﻿using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.QC.ViewModels;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Tests.ViewModels.QCSetupViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_injectate_verification_enabled_setting_changes
    {
        private readonly IQualityTestController stubController = MockRepository.GenerateStub<IQualityTestController>();

        [TestMethod]
        public void And_it_is_being_enabled_then_the_property_changes_accordingly()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(Arg.Is(SettingKeys.SystemInjectateVerificationEnabled))).Return(false);
            var viewModel = new QCSetupViewModel(stubController, stubSettingsManager);

            stubSettingsManager.Expect(
                m => m.GetSetting<bool>(Arg.Is(SettingKeys.SystemInjectateVerificationEnabled))).Return(true).Repeat.Any();
            stubSettingsManager.Raise(m => m.SettingsChanged += null, stubSettingsManager,
                                       new SettingsChangedEventArgs(new[] { SettingKeys.SystemInjectateVerificationEnabled }));

            Assert.IsTrue(viewModel.IsInjectateVerificationEnabled);
        }

        [TestMethod]
        public void And_it_is_being_disabled_then_the_property_changes_accordingly()
        {
            var stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            stubSettingsManager.Expect(m => m.GetSetting<bool>(Arg.Is(SettingKeys.SystemInjectateVerificationEnabled))).Return(true);
            var viewModel = new QCSetupViewModel(stubController, stubSettingsManager);

            stubSettingsManager.Expect(
                m => m.GetSetting<bool>(Arg.Is(SettingKeys.SystemInjectateVerificationEnabled))).Return(false).Repeat.Any();
            stubSettingsManager.Raise(m => m.SettingsChanged += null, stubSettingsManager,
                                       new SettingsChangedEventArgs(new[] { SettingKeys.SystemInjectateVerificationEnabled }));

            Assert.IsFalse(viewModel.IsInjectateVerificationEnabled);
        }
    }
    // ReSharper restore InconsistentNaming
}
