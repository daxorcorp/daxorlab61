﻿using System;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.QC.Tests.Module_Tests
{
	internal class ModuleThatDoesNotAddResourceDictionariesOrInitializeCommands : Module
	{
		public ModuleThatDoesNotAddResourceDictionariesOrInitializeCommands(IUnityContainer container) : base(container)
		{
			ThrowExceptionOnAddToMergedDictionaries = false;
		}

		public bool ThrowExceptionOnAddToMergedDictionaries { get; set; }
        public bool ThrowExceptionOnInitializingAndConfiguringNavigationCommands { get; set; }

		protected override void AddResourceDictionaryToMergedDictionaries()
		{
			if (ThrowExceptionOnAddToMergedDictionaries)
				throw new Exception();
		}

		protected override void InitializeAndConfigureNavigationCommands()
		{
		    if (ThrowExceptionOnInitializingAndConfiguringNavigationCommands)
		        throw new Exception();
		}
	}
}
