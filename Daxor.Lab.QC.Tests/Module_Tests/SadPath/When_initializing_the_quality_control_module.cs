﻿using System;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.QC.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.QC.Tests.Module_Tests.SadPath
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_the_Quality_Control_module
	{
		private ModuleThatDoesNotAddResourceDictionariesOrInitializeCommands _module;
        private IUnityContainer _container;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_container = Substitute.For<IUnityContainer>();
		}

		[TestMethod]
		public void And_an_error_is_thrown_when_adding_resource_dictionaries_then_the_correct_module_load_exception_is_thrown()
		{
			_module = new ModuleThatDoesNotAddResourceDictionariesOrInitializeCommands(_container)
			{
				ThrowExceptionOnAddToMergedDictionaries = true
			};

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control", ex.ModuleName);
				Assert.AreEqual("add the resource dictionary", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_is_thrown_when_registering_the_quality_control_module_navigator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IAppModuleNavigator, QCModuleNavigator>(Arg.Any<string>(),
						Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new ModuleThatDoesNotAddResourceDictionariesOrInitializeCommands(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control", ex.ModuleName);
				Assert.AreEqual("initialize the Module Navigator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_is_thrown_when_registering_the_quality_control_rule_engine_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IRuleEngine, QCRuleEngine>(Arg.Any<string>(),
						Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new ModuleThatDoesNotAddResourceDictionariesOrInitializeCommands(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control", ex.ModuleName);
				Assert.AreEqual("register the Rule Engine", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_is_thrown_when_resolving_and_initializing_the_quality_control_rule_engine_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<IRuleEngine>(Arg.Any<string>())).Throw(new Exception());
			_module = new ModuleThatDoesNotAddResourceDictionariesOrInitializeCommands(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control", ex.ModuleName);
				Assert.AreEqual("initialize the Rule Engine", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_is_thrown_when_resolving_and_initializing_the_quality_control_module_navigator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<IAppModuleNavigator>(Arg.Any<string>())).Throw(new Exception());
			_container.Resolve<IRuleEngine>(Arg.Any<string>()).Returns(new QCRuleEngine(Substitute.For<ISettingsManager>()));
			_module = new ModuleThatDoesNotAddResourceDictionariesOrInitializeCommands(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Quality Control", ex.ModuleName);
				Assert.AreEqual("initialize the Module Navigator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

        [TestMethod]
        public void And_an_error_is_thrown_when_initializing_and_configuring_navigation_commands_then_the_correct_module_load_exception_is_thrown()
        {
            _container.WhenForAnyArgs(c => c.Resolve<IAppModuleNavigator>(Arg.Any<string>())).Throw(new Exception());
            _container.Resolve<IRuleEngine>(Arg.Any<string>()).Returns(new QCRuleEngine(Substitute.For<ISettingsManager>()));
            _module = new ModuleThatDoesNotAddResourceDictionariesOrInitializeCommands(_container);
            _module.ThrowExceptionOnInitializingAndConfiguringNavigationCommands = true;

            try
            {
                _module.InitializeAppModule();
            }
            catch (ModuleLoadException ex)
            {
                Assert.AreEqual("Quality Control", ex.ModuleName);
                Assert.AreEqual("initialize and configure navigation commands", ex.FailingAction);
                return;
            }
            Assert.Fail();
        }
	}
	// ReSharper restore InconsistentNaming
}