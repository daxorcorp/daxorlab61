﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QCTestArchive.Classes
{
    public class LinearitySample
    {
        public string ControlSampleId { get; set; }

        public string ChangerLocation { get; set; }

        public string NominalActivity { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public double MeasuredRate { get; set; }

        public string MeasuredRatioOfRates { get; set; }

        public string Pass { get; set; }
    }
}
