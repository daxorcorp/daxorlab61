﻿
namespace QCTestArchive.Classes
{
    public class ContaminationSample
    {
        public string SampleName { get; set; }

        public int ChangerLocation { get; set; }

        public int Rate { get; set; }

        public string Pass { get; set; }
    }
}
