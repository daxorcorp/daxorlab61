﻿
namespace QCTestArchive.Classes
{
    public class FailureDetails
    {
        public string Id { get; set; }

        public string FailItem { get; set; }

        public string Value { get; set; }

        public string AcceptanceRangeFrom { get; set; }

        public string AcceptanceRangeTo { get; set; }
    }
}
