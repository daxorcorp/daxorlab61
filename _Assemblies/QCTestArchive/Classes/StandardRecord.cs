﻿
namespace QCTestArchive.Classes
{
    public class StandardRecord
    {
        public string SampleName { get; set; }

        public int ChangerLocation { get; set; }

        public int Rate { get; set; }

        public int ActualCounts { get; set; }

        public string Pass { get; set; }
    }
}
