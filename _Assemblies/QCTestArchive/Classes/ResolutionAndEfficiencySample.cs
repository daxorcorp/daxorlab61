﻿
namespace QCTestArchive.Classes
{
    public class ResolutionAndEfficiencySample
    {
        public string ControlSampleId { get; set; }

        public string ChangerLocation { get; set; }

        public string SerialNumber { get; set; }

        public string Centroid { get; set; }

        public string FWHM { get; set; }

        public int ActualCounts { get; set; }

        public string Pass { get; set; }
    }
}
