namespace QCTestArchive.Report
{
    partial class LinearityReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            textBox2 = new Telerik.Reporting.TextBox();
            textBox3 = new Telerik.Reporting.TextBox();
            textBox4 = new Telerik.Reporting.TextBox();
            textBox18 = new Telerik.Reporting.TextBox();
            textBox10 = new Telerik.Reporting.TextBox();
            textBox12 = new Telerik.Reporting.TextBox();
            textBox14 = new Telerik.Reporting.TextBox();
            textBox16 = new Telerik.Reporting.TextBox();
            textBox25 = new Telerik.Reporting.TextBox();
            textBox26 = new Telerik.Reporting.TextBox();
            textBox27 = new Telerik.Reporting.TextBox();
            textBox28 = new Telerik.Reporting.TextBox();
            textBox29 = new Telerik.Reporting.TextBox();
            pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            detail = new Telerik.Reporting.DetailSection();
            textBoxContaminationAcceptanceCriteria = new Telerik.Reporting.TextBox();
            textBoxLinearityTestPassFail = new Telerik.Reporting.TextBox();
            shape12 = new Telerik.Reporting.Shape();
            textBox1 = new Telerik.Reporting.TextBox();
            tableLinearitySamples = new Telerik.Reporting.Table();
            textBox5 = new Telerik.Reporting.TextBox();
            textBox6 = new Telerik.Reporting.TextBox();
            textBox7 = new Telerik.Reporting.TextBox();
            textBox9 = new Telerik.Reporting.TextBox();
            textBox11 = new Telerik.Reporting.TextBox();
            textBox13 = new Telerik.Reporting.TextBox();
            textBox15 = new Telerik.Reporting.TextBox();
            textBox17 = new Telerik.Reporting.TextBox();
            panelFailureDetails = new Telerik.Reporting.Panel();
            tableFailureDetails = new Telerik.Reporting.Table();
            textBox20 = new Telerik.Reporting.TextBox();
            textBox21 = new Telerik.Reporting.TextBox();
            textBox22 = new Telerik.Reporting.TextBox();
            textBox23 = new Telerik.Reporting.TextBox();
            textBox24 = new Telerik.Reporting.TextBox();
            textBoxAcceptanceRangeLabel = new Telerik.Reporting.TextBox();
            textBoxFailureDetailsLabel = new Telerik.Reporting.TextBox();
            textBox8 = new Telerik.Reporting.TextBox();
            pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            shape1 = new Telerik.Reporting.Shape();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox2
            // 
            textBox2.Name = "textBox2";
            textBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.89583337306976318D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox2.Value = "Control Sample ID";
            // 
            // textBox3
            // 
            textBox3.Name = "textBox3";
            textBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0625001192092896D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox3.Value = "Changer Location";
            // 
            // textBox4
            // 
            textBox4.Name = "textBox4";
            textBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.86458319425582886D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox4.Value = "Nominal Activity nCi";
            // 
            // textBox18
            // 
            textBox18.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.9229161739349365D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.55007869005203247D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox18.Name = "textBox18";
            textBox18.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox18.Style.Font.Bold = false;
            textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBox18.StyleName = "";
            textBox18.Value = "From";
            // 
            // textBox10
            // 
            textBox10.Name = "textBox10";
            textBox10.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox10.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBox10.StyleName = "";
            textBox10.Value = "To";
            // 
            // textBox12
            // 
            textBox12.Name = "textBox12";
            textBox12.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            textBox12.StyleName = "";
            textBox12.Value = "Measured Rate CPM";
            // 
            // textBox14
            // 
            textBox14.Name = "textBox14";
            textBox14.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox14.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox14.StyleName = "";
            textBox14.Value = "Measured Ratio of Rates";
            // 
            // textBox16
            // 
            textBox16.Name = "textBox16";
            textBox16.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.1875001192092896D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox16.StyleName = "";
            textBox16.Value = "Pass/Fail";
            // 
            // textBox25
            // 
            textBox25.Name = "textBox25";
            textBox25.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2604166567325592D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox25.Style.Font.Bold = true;
            textBox25.Value = "ID";
            // 
            // textBox26
            // 
            textBox26.Name = "textBox26";
            textBox26.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2604166567325592D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox26.Style.Font.Bold = true;
            textBox26.Value = "Fail Item";
            // 
            // textBox27
            // 
            textBox27.Name = "textBox27";
            textBox27.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0833333730697632D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.26041668653488159D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox27.Style.Font.Bold = true;
            textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox27.Value = "Value";
            // 
            // textBox28
            // 
            textBox28.Name = "textBox28";
            textBox28.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2604166567325592D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox28.Style.Font.Bold = true;
            textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            textBox28.StyleName = "";
            textBox28.Value = "From";
            // 
            // textBox29
            // 
            textBox29.Name = "textBox29";
            textBox29.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2604166567325592D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox29.Style.Font.Bold = true;
            textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            textBox29.StyleName = "";
            textBox29.Value = "To";
            // 
            // pageHeaderSection1
            // 
            pageHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.0520833320915699D, Telerik.Reporting.Drawing.UnitType.Inch);
            pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // detail
            // 
            detail.Height = new Telerik.Reporting.Drawing.Unit(2.5479166507720947D, Telerik.Reporting.Drawing.UnitType.Inch);
            detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBoxContaminationAcceptanceCriteria,
            textBoxLinearityTestPassFail,
            shape12,
            textBox1,
            tableLinearitySamples,
            panelFailureDetails,
            textBox8});
            detail.Name = "detail";
            // 
            // textBoxContaminationAcceptanceCriteria
            // 
            textBoxContaminationAcceptanceCriteria.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34999999403953552D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxContaminationAcceptanceCriteria.Name = "textBoxContaminationAcceptanceCriteria";
            textBoxContaminationAcceptanceCriteria.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9957542419433594D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxContaminationAcceptanceCriteria.Style.Font.Name = "Times New Roman";
            textBoxContaminationAcceptanceCriteria.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxContaminationAcceptanceCriteria.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxContaminationAcceptanceCriteria.Value = "Acceptance Criteria:  Measured ratio of rates to lie within range of Acceptable R" +
                "atio of Rates";
            // 
            // textBoxLinearityTestPassFail
            // 
            textBoxLinearityTestPassFail.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.1875D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.047916650772094727D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxLinearityTestPassFail.Name = "textBoxLinearityTestPassFail";
            textBoxLinearityTestPassFail.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.7999998927116394D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxLinearityTestPassFail.Style.Font.Bold = true;
            textBoxLinearityTestPassFail.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxLinearityTestPassFail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            textBoxLinearityTestPassFail.Value = "";
            // 
            // shape12
            // 
            shape12.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.24583332240581513D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape12.Name = "shape12";
            shape12.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            shape12.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9957542419433594D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.079166412353515625D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox1
            // 
            textBox1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.047916650772094727D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox1.Name = "textBox1";
            textBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.3000001907348633D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox1.Style.Font.Bold = true;
            textBox1.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBox1.Value = "Linearity Test";
            // 
            // tableLinearitySamples
            // 
            tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(0.89583295583724976D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.0625001192092896D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(0.86458325386047363D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.0000002384185791D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.0000002384185791D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.0000002384185791D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.0000002384185791D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.1875002384185791D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableLinearitySamples.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableLinearitySamples.Body.SetCellContent(0, 0, textBox5);
            tableLinearitySamples.Body.SetCellContent(0, 1, textBox6);
            tableLinearitySamples.Body.SetCellContent(0, 2, textBox7);
            tableLinearitySamples.Body.SetCellContent(0, 3, textBox9);
            tableLinearitySamples.Body.SetCellContent(0, 4, textBox11);
            tableLinearitySamples.Body.SetCellContent(0, 5, textBox13);
            tableLinearitySamples.Body.SetCellContent(0, 6, textBox15);
            tableLinearitySamples.Body.SetCellContent(0, 7, textBox17);
            tableGroup1.ReportItem = textBox2;
            tableGroup2.ReportItem = textBox3;
            tableGroup3.ReportItem = textBox4;
            tableGroup4.Name = "Group1";
            tableGroup4.ReportItem = textBox18;
            tableGroup5.Name = "Group2";
            tableGroup5.ReportItem = textBox10;
            tableGroup6.Name = "Group3";
            tableGroup6.ReportItem = textBox12;
            tableGroup7.Name = "Group4";
            tableGroup7.ReportItem = textBox14;
            tableGroup8.Name = "Group5";
            tableGroup8.ReportItem = textBox16;
            tableLinearitySamples.ColumnGroups.Add(tableGroup1);
            tableLinearitySamples.ColumnGroups.Add(tableGroup2);
            tableLinearitySamples.ColumnGroups.Add(tableGroup3);
            tableLinearitySamples.ColumnGroups.Add(tableGroup4);
            tableLinearitySamples.ColumnGroups.Add(tableGroup5);
            tableLinearitySamples.ColumnGroups.Add(tableGroup6);
            tableLinearitySamples.ColumnGroups.Add(tableGroup7);
            tableLinearitySamples.ColumnGroups.Add(tableGroup8);
            tableLinearitySamples.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBox5,
            textBox6,
            textBox7,
            textBox9,
            textBox11,
            textBox13,
            textBox15,
            textBox17,
            textBox2,
            textBox3,
            textBox4,
            textBox18,
            textBox10,
            textBox12,
            textBox14,
            textBox16});
            tableLinearitySamples.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.9378803194267675E-05D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.64791673421859741D, Telerik.Reporting.Drawing.UnitType.Inch));
            tableLinearitySamples.Name = "tableLinearitySamples";
            tableGroup9.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping("")});
            tableGroup9.Name = "DetailGroup";
            tableLinearitySamples.RowGroups.Add(tableGroup9);
            tableLinearitySamples.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(8.0104169845581055D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.6875D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox5
            // 
            textBox5.Name = "textBox5";
            textBox5.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.89583337306976318D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox5.Value = "=Fields.ControlSampleId";
            // 
            // textBox6
            // 
            textBox6.Name = "textBox6";
            textBox6.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0625001192092896D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox6.Value = "=Fields.ChangerLocation";
            // 
            // textBox7
            // 
            textBox7.Name = "textBox7";
            textBox7.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.86458319425582886D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox7.Value = "=Fields.NominalActivity";
            // 
            // textBox9
            // 
            textBox9.Name = "textBox9";
            textBox9.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox9.StyleName = "";
            textBox9.Value = "=Fields.From";
            // 
            // textBox11
            // 
            textBox11.Name = "textBox11";
            textBox11.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox11.StyleName = "";
            textBox11.Value = "=Fields.To";
            // 
            // textBox13
            // 
            textBox13.Format = "{0:N0}";
            textBox13.Name = "textBox13";
            textBox13.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox13.StyleName = "";
            textBox13.Value = "=Fields.MeasuredRate";
            // 
            // textBox15
            // 
            textBox15.Format = "{0:N2}";
            textBox15.Name = "textBox15";
            textBox15.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox15.StyleName = "";
            textBox15.Value = "=Fields.MeasuredRatioOfRates";
            // 
            // textBox17
            // 
            textBox17.Name = "textBox17";
            textBox17.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.1875001192092896D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox17.StyleName = "";
            textBox17.Value = "=Fields.Pass";
            // 
            // panelFailureDetails
            // 
            panelFailureDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            tableFailureDetails,
            textBoxAcceptanceRangeLabel,
            textBoxFailureDetailsLabel});
            panelFailureDetails.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.40000000596046448D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.4479166269302368D, Telerik.Reporting.Drawing.UnitType.Inch));
            panelFailureDetails.Name = "panelFailureDetails";
            panelFailureDetails.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.4000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch));
            panelFailureDetails.Style.Visible = false;
            // 
            // tableFailureDetails
            // 
            tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(2.0000009536743164D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(2.0000009536743164D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.0833334922790527D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.0000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFailureDetails.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.28125020861625671D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFailureDetails.Body.SetCellContent(0, 0, textBox20);
            tableFailureDetails.Body.SetCellContent(0, 1, textBox21);
            tableFailureDetails.Body.SetCellContent(0, 2, textBox22);
            tableFailureDetails.Body.SetCellContent(0, 3, textBox23);
            tableFailureDetails.Body.SetCellContent(0, 4, textBox24);
            tableGroup10.ReportItem = textBox25;
            tableGroup11.ReportItem = textBox26;
            tableGroup12.ReportItem = textBox27;
            tableGroup13.Name = "Group1";
            tableGroup13.ReportItem = textBox28;
            tableGroup14.Name = "Group2";
            tableGroup14.ReportItem = textBox29;
            tableFailureDetails.ColumnGroups.Add(tableGroup10);
            tableFailureDetails.ColumnGroups.Add(tableGroup11);
            tableFailureDetails.ColumnGroups.Add(tableGroup12);
            tableFailureDetails.ColumnGroups.Add(tableGroup13);
            tableFailureDetails.ColumnGroups.Add(tableGroup14);
            tableFailureDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBox20,
            textBox21,
            textBox22,
            textBox23,
            textBox24,
            textBox25,
            textBox26,
            textBox27,
            textBox28,
            textBox29});
            tableFailureDetails.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.15625D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20011822879314423D, Telerik.Reporting.Drawing.UnitType.Inch));
            tableFailureDetails.Name = "tableFailureDetails";
            tableGroup15.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping("")});
            tableGroup15.Name = "DetailGroup";
            tableFailureDetails.RowGroups.Add(tableGroup15);
            tableFailureDetails.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.0833353996276855D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.54166686534881592D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox20
            // 
            textBox20.Name = "textBox20";
            textBox20.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.28125017881393433D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox20.Value = "=Fields.Id";
            // 
            // textBox21
            // 
            textBox21.Name = "textBox21";
            textBox21.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.28125017881393433D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox21.Value = "=Fields.FailItem";
            // 
            // textBox22
            // 
            textBox22.Name = "textBox22";
            textBox22.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0833333730697632D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.28125020861625671D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox22.Value = "=Fields.Value";
            // 
            // textBox23
            // 
            textBox23.Name = "textBox23";
            textBox23.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.28125020861625671D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox23.StyleName = "";
            textBox23.Value = "=Fields.AcceptanceRangeFrom";
            // 
            // textBox24
            // 
            textBox24.Name = "textBox24";
            textBox24.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.28125020861625671D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox24.StyleName = "";
            textBox24.Value = "=Fields.AcceptanceRangeTo";
            // 
            // textBoxAcceptanceRangeLabel
            // 
            textBoxAcceptanceRangeLabel.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.3000006675720215D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.1000000610947609D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxAcceptanceRangeLabel.Name = "textBoxAcceptanceRangeLabel";
            textBoxAcceptanceRangeLabel.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.0000007152557373D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxAcceptanceRangeLabel.Style.Font.Bold = true;
            textBoxAcceptanceRangeLabel.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxAcceptanceRangeLabel.Value = "Acceptance Range";
            // 
            // textBoxFailureDetailsLabel
            // 
            textBoxFailureDetailsLabel.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxFailureDetailsLabel.Name = "textBoxFailureDetailsLabel";
            textBoxFailureDetailsLabel.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.95833325386047363D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000012218952179D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxFailureDetailsLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            textBoxFailureDetailsLabel.Style.Visible = true;
            textBoxFailureDetailsLabel.Value = "Failure Details";
            // 
            // textBox8
            // 
            textBox8.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.4479166567325592D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox8.Name = "textBox8";
            textBox8.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.8000005483627319D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.5437501072883606D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox8.Style.Font.Bold = false;
            textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            textBox8.StyleName = "";
            textBox8.Value = "Acceptable Ratio of Rates";
            // 
            // pageFooterSection1
            // 
            pageFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(0.0520833320915699D, Telerik.Reporting.Drawing.UnitType.Inch);
            pageFooterSection1.Name = "pageFooterSection1";
            // 
            // shape1
            // 
            shape1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.3333333730697632D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape1.Name = "shape1";
            shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            shape1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.4000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.079166412353515625D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            shape1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            // 
            // LinearityReport
            // 
            Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            pageHeaderSection1,
            detail,
            pageFooterSection1});
            PageSettings.Landscape = false;
            PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(0.25D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            Style.BackgroundColor = System.Drawing.Color.White;
            Width = new Telerik.Reporting.Drawing.Unit(8.0104570388793945D, Telerik.Reporting.Drawing.UnitType.Inch);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        public Telerik.Reporting.TextBox textBoxContaminationAcceptanceCriteria;
        public Telerik.Reporting.TextBox textBoxLinearityTestPassFail;
        private Telerik.Reporting.Shape shape12;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.Shape shape1;
        public Telerik.Reporting.TextBox textBox18;
        public Telerik.Reporting.Panel panelFailureDetails;
        public Telerik.Reporting.Table tableFailureDetails;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        public Telerik.Reporting.TextBox textBoxAcceptanceRangeLabel;
        public Telerik.Reporting.TextBox textBoxFailureDetailsLabel;
        public Telerik.Reporting.TextBox textBox8;
        public Telerik.Reporting.Table tableLinearitySamples;
    }
}