namespace QCTestArchive.Report
{
    partial class StandardsOnlyReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.InstanceReportSource instanceReportSource1 = new Telerik.Reporting.InstanceReportSource();
            Telerik.Reporting.InstanceReportSource instanceReportSource2 = new Telerik.Reporting.InstanceReportSource();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.textBoxVoltage = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBoxFineGain = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBoxAnalyst = new Telerik.Reporting.TextBox();
            this.textBoxCoarseGainLabel = new Telerik.Reporting.TextBox();
            this.textBoxCoarseGain = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBoxInjectateLot = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBoxComment = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBoxROI = new Telerik.Reporting.TextBox();
            this.textBoxTestDate = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBoxDepartmentDirector = new Telerik.Reporting.TextBox();
            this.textBoxDepartment = new Telerik.Reporting.TextBox();
            this.textBoxDepartmentPhoneNumber = new Telerik.Reporting.TextBox();
            this.textBoxDepartmentAddress = new Telerik.Reporting.TextBox();
            this.textBoxHospitalName = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBoxCumulativePassFail = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.subReport1 = new Telerik.Reporting.SubReport();
            this.backgroundReport = new QCTestArchive.Report.BackgroundReport();
            this.subReport2 = new Telerik.Reporting.SubReport();
            this.standardsReport = new QCTestArchive.Report.StandardsReport();
            this.shape1 = new Telerik.Reporting.Shape();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.shape2 = new Telerik.Reporting.Shape();
            this.shape3 = new Telerik.Reporting.Shape();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.shape4 = new Telerik.Reporting.Shape();
            this.shape5 = new Telerik.Reporting.Shape();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.shape6 = new Telerik.Reporting.Shape();
            this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBoxBVAVersion = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBoxUnitID = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBoxPrintedOn = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.backgroundReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.standardsReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(2.8000001907348633D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxVoltage,
            this.textBox10,
            this.textBoxFineGain,
            this.textBox4,
            this.textBox9,
            this.textBoxAnalyst,
            this.textBoxCoarseGainLabel,
            this.textBoxCoarseGain,
            this.textBox7,
            this.textBoxInjectateLot,
            this.textBox6,
            this.textBoxComment,
            this.textBox5,
            this.textBoxROI,
            this.textBoxTestDate,
            this.textBox3,
            this.textBoxDepartmentDirector,
            this.textBoxDepartment,
            this.textBoxDepartmentPhoneNumber,
            this.textBoxDepartmentAddress,
            this.textBoxHospitalName,
            this.textBox2,
            this.textBoxCumulativePassFail,
            this.textBox1});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBoxVoltage
            // 
            this.textBoxVoltage.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.6937499046325684D), Telerik.Reporting.Drawing.Unit.Inch(2.1000001430511475D));
            this.textBoxVoltage.Name = "textBoxVoltage";
            this.textBoxVoltage.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70000028610229492D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBoxVoltage.Value = "";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.09375D), Telerik.Reporting.Drawing.Unit.Inch(2.1000001430511475D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59992074966430664D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox10.Value = "Voltage:";
            // 
            // textBoxFineGain
            // 
            this.textBoxFineGain.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.28125D), Telerik.Reporting.Drawing.Unit.Inch(2.1000001430511475D));
            this.textBoxFineGain.Name = "textBoxFineGain";
            this.textBoxFineGain.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70000028610229492D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBoxFineGain.Style.Visible = true;
            this.textBoxFineGain.Value = "";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.28125D), Telerik.Reporting.Drawing.Unit.Inch(2.1000001430511475D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992161989212036D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox4.Style.Visible = true;
            this.textBox4.Value = "Fine Gain:";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.2083334922790527D), Telerik.Reporting.Drawing.Unit.Inch(1.9020833969116211D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox9.Value = "Analyst:";
            // 
            // textBoxAnalyst
            // 
            this.textBoxAnalyst.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(1.9020833969116211D));
            this.textBoxAnalyst.Name = "textBoxAnalyst";
            this.textBoxAnalyst.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999211072921753D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBoxAnalyst.Value = "";
            // 
            // textBoxCoarseGainLabel
            // 
            this.textBoxCoarseGainLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(2.1000001430511475D));
            this.textBoxCoarseGainLabel.Name = "textBoxCoarseGainLabel";
            this.textBoxCoarseGainLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0916663408279419D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBoxCoarseGainLabel.Value = "Coarse Gain:";
            // 
            // textBoxCoarseGain
            // 
            this.textBoxCoarseGain.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.5D), Telerik.Reporting.Drawing.Unit.Inch(2.1000001430511475D));
            this.textBoxCoarseGain.Name = "textBoxCoarseGain";
            this.textBoxCoarseGain.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70000028610229492D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBoxCoarseGain.Value = "";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(1.9020833969116211D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99158746004104614D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox7.Value = "Injectate Lot #:";
            // 
            // textBoxInjectateLot
            // 
            this.textBoxInjectateLot.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.5D), Telerik.Reporting.Drawing.Unit.Inch(1.9020833969116211D));
            this.textBoxInjectateLot.Name = "textBoxInjectateLot";
            this.textBoxInjectateLot.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999211072921753D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBoxInjectateLot.Value = "";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(2.3083333969116211D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox6.Value = "Comment:";
            // 
            // textBoxComment
            // 
            this.textBoxComment.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.79166668653488159D), Telerik.Reporting.Drawing.Unit.Inch(2.3083333969116211D));
            this.textBoxComment.Name = "textBoxComment";
            this.textBoxComment.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.19992208480835D), Telerik.Reporting.Drawing.Unit.Inch(0.39996051788330078D));
            this.textBoxComment.Value = "";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(2.1000001430511475D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox5.Value = "ROI:";
            // 
            // textBoxROI
            // 
            this.textBoxROI.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.79166668653488159D), Telerik.Reporting.Drawing.Unit.Inch(2.1000001430511475D));
            this.textBoxROI.Name = "textBoxROI";
            this.textBoxROI.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999211072921753D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBoxROI.Value = "";
            // 
            // textBoxTestDate
            // 
            this.textBoxTestDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.79166668653488159D), Telerik.Reporting.Drawing.Unit.Inch(1.9020833969116211D));
            this.textBoxTestDate.Name = "textBoxTestDate";
            this.textBoxTestDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999211072921753D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBoxTestDate.Value = "";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.9020833969116211D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox3.Value = "Test Date:";
            // 
            // textBoxDepartmentDirector
            // 
            this.textBoxDepartmentDirector.KeepTogether = true;
            this.textBoxDepartmentDirector.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.6104167699813843D));
            this.textBoxDepartmentDirector.Name = "textBoxDepartmentDirector";
            this.textBoxDepartmentDirector.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.2041667252779007D));
            this.textBoxDepartmentDirector.Style.Font.Bold = true;
            this.textBoxDepartmentDirector.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBoxDepartmentDirector.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxDepartmentDirector.Value = "Department Director";
            // 
            // textBoxDepartment
            // 
            this.textBoxDepartment.KeepTogether = true;
            this.textBoxDepartment.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.4020833969116211D));
            this.textBoxDepartment.Name = "textBoxDepartment";
            this.textBoxDepartment.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.2041667252779007D));
            this.textBoxDepartment.Style.Font.Bold = true;
            this.textBoxDepartment.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBoxDepartment.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxDepartment.Value = "Department";
            // 
            // textBoxDepartmentPhoneNumber
            // 
            this.textBoxDepartmentPhoneNumber.KeepTogether = true;
            this.textBoxDepartmentPhoneNumber.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.2041667699813843D));
            this.textBoxDepartmentPhoneNumber.Name = "textBoxDepartmentPhoneNumber";
            this.textBoxDepartmentPhoneNumber.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.2041667252779007D));
            this.textBoxDepartmentPhoneNumber.Style.Font.Bold = true;
            this.textBoxDepartmentPhoneNumber.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBoxDepartmentPhoneNumber.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxDepartmentPhoneNumber.Value = "Department Phone Number";
            // 
            // textBoxDepartmentAddress
            // 
            this.textBoxDepartmentAddress.KeepTogether = true;
            this.textBoxDepartmentAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.0166667699813843D));
            this.textBoxDepartmentAddress.Name = "textBoxDepartmentAddress";
            this.textBoxDepartmentAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.19575436413288117D));
            this.textBoxDepartmentAddress.Style.Font.Bold = true;
            this.textBoxDepartmentAddress.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBoxDepartmentAddress.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxDepartmentAddress.Value = "Department Address";
            // 
            // textBoxHospitalName
            // 
            this.textBoxHospitalName.KeepTogether = true;
            this.textBoxHospitalName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.80833339691162109D));
            this.textBoxHospitalName.Name = "textBoxHospitalName";
            this.textBoxHospitalName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.1999211311340332D));
            this.textBoxHospitalName.Style.Font.Bold = true;
            this.textBoxHospitalName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBoxHospitalName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxHospitalName.Value = "Hospital Name";
            // 
            // textBox2
            // 
            this.textBox2.KeepTogether = true;
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.49583339691162109D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Value = "Daxor BVA 100 - Standards Only - Analysis Report";
            // 
            // textBoxCumulativePassFail
            // 
            this.textBoxCumulativePassFail.KeepTogether = false;
            this.textBoxCumulativePassFail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.09375D), Telerik.Reporting.Drawing.Unit.Inch(0.11041673272848129D));
            this.textBoxCumulativePassFail.Name = "textBoxCumulativePassFail";
            this.textBoxCumulativePassFail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79992103576660156D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
            this.textBoxCumulativePassFail.Style.Font.Bold = true;
            this.textBoxCumulativePassFail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBoxCumulativePassFail.Value = "";
            // 
            // textBox1
            // 
            this.textBox1.KeepTogether = true;
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.1875D), Telerik.Reporting.Drawing.Unit.Inch(0.11041673272848129D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9000002145767212D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox1.Value = "Cumulative Result - ";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(2.7999999523162842D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.subReport1,
            this.subReport2,
            this.shape1,
            this.textBox11,
            this.textBox12,
            this.shape2,
            this.shape3,
            this.textBox13,
            this.textBox14,
            this.shape4,
            this.shape5,
            this.textBox15,
            this.textBox16,
            this.shape6});
            this.detail.Name = "detail";
            // 
            // subReport1
            // 
            this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
            this.subReport1.Name = "subReport1";
            instanceReportSource1.ReportDocument = this.backgroundReport;
            this.subReport1.ReportSource = instanceReportSource1;
            this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9915885925292969D), Telerik.Reporting.Drawing.Unit.Inch(0.7999998927116394D));
            // 
            // backgroundReport
            // 
            this.backgroundReport.Name = "backgroundReport";
            // 
            // subReport2
            // 
            this.subReport2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.80011814832687378D));
            this.subReport2.Name = "subReport2";
            instanceReportSource2.ReportDocument = this.standardsReport;
            this.subReport2.ReportSource = instanceReportSource2;
            this.subReport2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9915885925292969D), Telerik.Reporting.Drawing.Unit.Inch(0.89988201856613159D));
            // 
            // standardsReport
            // 
            this.standardsReport.Name = "standardsReport";
            // 
            // shape1
            // 
            this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.79166668653488159D), Telerik.Reporting.Drawing.Unit.Inch(2D));
            this.shape1.Name = "shape1";
            this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0104167461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.3125D));
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0416666679084301D), Telerik.Reporting.Drawing.Unit.Inch(2D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.74791669845581055D), Telerik.Reporting.Drawing.Unit.Inch(0.3125D));
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox11.Value = "Reviewed:";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8958332538604736D), Telerik.Reporting.Drawing.Unit.Inch(2D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39375004172325134D), Telerik.Reporting.Drawing.Unit.Inch(0.3125D));
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox12.Value = "Title:";
            // 
            // shape2
            // 
            this.shape2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.2916667461395264D), Telerik.Reporting.Drawing.Unit.Inch(2D));
            this.shape2.Name = "shape2";
            this.shape2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6353384256362915D), Telerik.Reporting.Drawing.Unit.Inch(0.3125D));
            // 
            // shape3
            // 
            this.shape3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.4479165077209473D), Telerik.Reporting.Drawing.Unit.Inch(2D));
            this.shape3.Name = "shape3";
            this.shape3.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6353384256362915D), Telerik.Reporting.Drawing.Unit.Inch(0.3125D));
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.0729165077209473D), Telerik.Reporting.Drawing.Unit.Inch(2D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39375004172325134D), Telerik.Reporting.Drawing.Unit.Inch(0.3125D));
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox13.Value = "Date:";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.0625D), Telerik.Reporting.Drawing.Unit.Inch(2.40625D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39375004172325134D), Telerik.Reporting.Drawing.Unit.Inch(0.3125D));
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox14.Value = "Date:";
            // 
            // shape4
            // 
            this.shape4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.4479165077209473D), Telerik.Reporting.Drawing.Unit.Inch(2.40625D));
            this.shape4.Name = "shape4";
            this.shape4.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6353384256362915D), Telerik.Reporting.Drawing.Unit.Inch(0.3125D));
            // 
            // shape5
            // 
            this.shape5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.2916667461395264D), Telerik.Reporting.Drawing.Unit.Inch(2.40625D));
            this.shape5.Name = "shape5";
            this.shape5.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6353384256362915D), Telerik.Reporting.Drawing.Unit.Inch(0.3125D));
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.8958332538604736D), Telerik.Reporting.Drawing.Unit.Inch(2.40625D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.39375004172325134D), Telerik.Reporting.Drawing.Unit.Inch(0.3125D));
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox15.Value = "Title:";
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0416666679084301D), Telerik.Reporting.Drawing.Unit.Inch(2.40625D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.74791669845581055D), Telerik.Reporting.Drawing.Unit.Inch(0.3125D));
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox16.Value = "Reviewed:";
            // 
            // shape6
            // 
            this.shape6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.79166668653488159D), Telerik.Reporting.Drawing.Unit.Inch(2.40625D));
            this.shape6.Name = "shape6";
            this.shape6.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0104167461395264D), Telerik.Reporting.Drawing.Unit.Inch(0.3125D));
            // 
            // pageFooterSection1
            // 
            this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.40000024437904358D);
            this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17,
            this.textBoxBVAVersion,
            this.textBox20,
            this.textBoxUnitID,
            this.textBox21,
            this.textBoxPrintedOn});
            this.pageFooterSection1.Name = "pageFooterSection1";
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.3958333432674408D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox17.Value = "BVA:";
            // 
            // textBoxBVAVersion
            // 
            this.textBoxBVAVersion.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.3999999463558197D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
            this.textBoxBVAVersion.Name = "textBoxBVAVersion";
            this.textBoxBVAVersion.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.85609245300292969D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxBVAVersion.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBoxBVAVersion.Value = "v1.2.0.0";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.3999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.54158782958984375D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox20.Value = "Unit ID:";
            // 
            // textBoxUnitID
            // 
            this.textBoxUnitID.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.9416666030883789D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
            this.textBoxUnitID.Name = "textBoxUnitID";
            this.textBoxUnitID.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0644257068634033D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxUnitID.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBoxUnitID.Value = "-";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.5D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2852592468261719D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox21.Value = "=\'Page \' + PageNumber + \' of \' + PageCount";
            // 
            // textBoxPrintedOn
            // 
            this.textBoxPrintedOn.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.0999999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.0833333358168602D));
            this.textBoxPrintedOn.Name = "textBoxPrintedOn";
            this.textBoxPrintedOn.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2000000476837158D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxPrintedOn.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBoxPrintedOn.Value = "";
            // 
            // StandardsOnlyReport
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
            this.Name = "StandardsOnlyReport";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
            ((System.ComponentModel.ISupportInitialize)(this.backgroundReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.standardsReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        public Telerik.Reporting.TextBox textBoxVoltage;
        private Telerik.Reporting.TextBox textBox10;
        public Telerik.Reporting.TextBox textBoxFineGain;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox9;
        public Telerik.Reporting.TextBox textBoxAnalyst;
        public Telerik.Reporting.TextBox textBoxCoarseGain;
        private Telerik.Reporting.TextBox textBox7;
        public Telerik.Reporting.TextBox textBoxInjectateLot;
        private Telerik.Reporting.TextBox textBox6;
        public Telerik.Reporting.TextBox textBoxComment;
        private Telerik.Reporting.TextBox textBox5;
        public Telerik.Reporting.TextBox textBoxROI;
        public Telerik.Reporting.TextBox textBoxTestDate;
        private Telerik.Reporting.TextBox textBox3;
        public Telerik.Reporting.TextBox textBoxDepartmentDirector;
        public Telerik.Reporting.TextBox textBoxDepartment;
        public Telerik.Reporting.TextBox textBoxDepartmentPhoneNumber;
        public Telerik.Reporting.TextBox textBoxDepartmentAddress;
        public Telerik.Reporting.TextBox textBoxHospitalName;
        private Telerik.Reporting.TextBox textBox2;
        public Telerik.Reporting.TextBox textBoxCumulativePassFail;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.SubReport subReport1;
        private Telerik.Reporting.SubReport subReport2;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.Shape shape2;
        private Telerik.Reporting.Shape shape3;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.Shape shape4;
        private Telerik.Reporting.Shape shape5;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.Shape shape6;
        private Telerik.Reporting.TextBox textBox17;
        public Telerik.Reporting.TextBox textBoxBVAVersion;
        private Telerik.Reporting.TextBox textBox20;
        public Telerik.Reporting.TextBox textBoxUnitID;
        private Telerik.Reporting.TextBox textBox21;
        public StandardsReport standardsReport;
        public BackgroundReport backgroundReport;
        public Telerik.Reporting.TextBox textBoxPrintedOn;
        public Telerik.Reporting.TextBox textBoxCoarseGainLabel;
    }
}