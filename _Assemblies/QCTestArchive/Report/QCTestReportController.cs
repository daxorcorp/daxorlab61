﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using QCTestArchive.Classes;
using QCTestArchive.Common;
using QCTestArchive.Database;
using Telerik.Reporting;
using Telerik.Reporting.Processing;

namespace QCTestArchive.Report
{
    public class QCTestReportController : ReportControllerBase
    {
        private QCTestDTO ReportTest { get; set; }

        private readonly FullQCReport _fullQcReport = new FullQCReport();
        private readonly ContaminationOnlyReport _contaminationOnlyReport = new ContaminationOnlyReport();
        private readonly StandardsOnlyReport _standardsOnlyReport = new StandardsOnlyReport();
        private readonly QCSourcesOnlyReport _qcSourcesOnlyReport = new QCSourcesOnlyReport();
 
        public QCTestReportController(IRegionManager regionManager, QCTestDTO test, IEventAggregator eventAggregator, ISettingsManager settingsManager)
        {
            RegionManager = regionManager;
            ReportTest = test;
            SettingsManager = settingsManager;
            EventAggregator = eventAggregator;
            MaskVisibility = Visibility.Collapsed;
            CreateFrameBrush();
        }

        public void CreateFrameBrush()
        {
            var backgroundLgb = new LinearGradientBrush
            {
                StartPoint = new Point(0.5, 0),
                EndPoint = new Point(0.5, 1)
            };

            backgroundLgb.GradientStops.Add(new GradientStop(Colors.White, 0));

            backgroundLgb.GradientStops.Add(new GradientStop(Colors.YellowGreen, 1));


            FrameColorBrush = backgroundLgb;
        }
        [SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.UnmanagedCode)]
        public void DoEvents()
        {
            var frame = new DispatcherFrame();
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Background,
                new DispatcherOperationCallback(ExitFrame), frame);
            Dispatcher.PushFrame(frame);
        }

        public object ExitFrame(object f)
        {
            ((DispatcherFrame)f).Continue = false;

            return null;
        }

        private BusyPayload _bContent;
        public BusyPayload BusyContent
        {
            get { return _bContent; }
            set
            {
                _bContent = value;
                FirePropertyChanged("BusyContent");
            }
        }

        private Visibility _mask;
        public Visibility MaskVisibility
        {
            get { return _mask; }
            set
            {
                _mask = value;
                FirePropertyChanged("MaskVisibility");
            }
        }
        public override void LoadReportData()
        {
            LoadHeaderData();
            LoadBackgroundData();
            LoadContaminationData();
            LoadStandardsData();
            LoadResolutionAndEfficiencyData();
            LoadLinearityData();
        }

        protected void LoadLinearityData()
        {
            LinearityReport rpt;

            switch (ReportTest.TestType)
            {
                case Constants.FullQCTest:
                    rpt = _fullQcReport.linearityReport;
                    break;

                case Constants.QCSourcesOnly:
                    rpt = _qcSourcesOnlyReport.linearityReport;
                    break;

                case Constants.ContaminationOnly:
                    return;

                case Constants.StandardsOnly:
                    return;
                    
                default:
                    throw new NotSupportedException("Unsupported QC Test Type: " + ReportTest.TestType);
            }

            int[] qcSourcesList = { 10, 11, 12, 13 };

            var linearityFields = from x in ReportTest.Samples
                                                where qcSourcesList.Contains(x.Position)
                                                orderby x.Position
                                                select new LinearitySample
                                                {
                                                    ControlSampleId = x.PositionDescription,
                                                    ChangerLocation = x.Position.ToString(CultureInfo.InvariantCulture),
                                                    NominalActivity = x.NominalActivity.ToString(CultureInfo.InvariantCulture),
                                                    MeasuredRate = Math.Round((x.Counts / x.LiveTime * 60), 0),
                                                    Pass = x.Pass ? "Pass" : "Fail"
                                                };

            var linearityList = new List<LinearitySample>();
            var failureDetails = new List<FailureDetails>();
            double startSourceActivity = 0; 
            double startRate = 0;

            foreach (var s in linearityFields)
            {
                double from;
                double to;
                switch (s.ChangerLocation)
                {
                    case "10":
                        s.From = "N/A";
                        s.To = "N/A";
                        s.MeasuredRatioOfRates = "1.00";
                        startSourceActivity = double.Parse(s.NominalActivity);
                        startRate = s.MeasuredRate;
                        break;
                    case "11":
                        CalculateQCRatioBounds(startSourceActivity, double.Parse(s.NominalActivity), out from, out to);
                        s.From = from.ToString(CultureInfo.InvariantCulture);
                        s.To = to.ToString(CultureInfo.InvariantCulture);
                        s.MeasuredRatioOfRates = Math.Round(startRate / s.MeasuredRate, 2).ToString(CultureInfo.InvariantCulture);
                        break;
                    case "12":
                        CalculateQCRatioBounds(startSourceActivity, double.Parse(s.NominalActivity), out from, out to);
                        s.From = from.ToString(CultureInfo.InvariantCulture);
                        s.To = to.ToString(CultureInfo.InvariantCulture);
                        s.MeasuredRatioOfRates = Math.Round(startRate / s.MeasuredRate, 2).ToString(CultureInfo.InvariantCulture);

                        break;
                    case "13":
                        CalculateQCRatioBounds(startSourceActivity, double.Parse(s.NominalActivity), out from, out to);
                        s.From = from.ToString(CultureInfo.InvariantCulture);
                        s.To = to.ToString(CultureInfo.InvariantCulture);
                        s.MeasuredRatioOfRates = Math.Round(startRate / s.MeasuredRate, 2).ToString(CultureInfo.InvariantCulture);

                        break;
                }
                 
                if(s.Pass == "Fail")
                {
                    var d = new FailureDetails
                                {
                                    Id = s.ControlSampleId,
                                    FailItem = "Rate Ratio",
                                    Value = s.MeasuredRatioOfRates,
                                    AcceptanceRangeFrom = s.From,
                                    AcceptanceRangeTo = s.To 
                                };
                    failureDetails.Add(d);

                }

                linearityList.Add(s);
            }

            rpt.tableLinearitySamples.DataSource = linearityList;
            if(failureDetails.Count > 0)
            {
                rpt.textBoxLinearityTestPassFail.Value = @"FAIL";

                rpt.tableFailureDetails.DataSource = failureDetails;
                rpt.panelFailureDetails.Visible = true;
            }
            else
            {
                rpt.textBoxLinearityTestPassFail.Value = @"PASS";
            }
         }

        private void LoadResolutionAndEfficiencyData()
        {
            ResolutionAndEfficiencyReport rpt;

            switch (ReportTest.TestType)
            {
                case Constants.FullQCTest:
                    rpt = _fullQcReport.resolutionAndEfficiencyReport;
                    break;

                case Constants.QCSourcesOnly:
                    rpt = _qcSourcesOnlyReport.resolutionAndEfficiencyReport;
                    break;

                case Constants.ContaminationOnly:
                    return;
                    
                case Constants.StandardsOnly:
                    return;

                default:
                    throw new NotSupportedException("Unsupported QC Test Type: " + ReportTest.TestType.ToString(CultureInfo.InvariantCulture));
            }

            int[] qcSourcesList = { 10, 11, 12, 13 };

            var resolutionAndEfficiencyFields = from x in ReportTest.Samples
                                  where qcSourcesList.Contains(x.Position)
                                  orderby x.Position
                                  select new ResolutionAndEfficiencySample
                                  {
                                      ActualCounts = x.Counts,
                                      ChangerLocation = x.Position.ToString(CultureInfo.InvariantCulture),
                                      Centroid = Math.Round(x.Centroid, 1).ToString(CultureInfo.InvariantCulture),
                                      FWHM = Math.Round(x.FWHM, 1).ToString(CultureInfo.InvariantCulture),
                                      SerialNumber = x.SerialNumber,
                                      Pass = x.Pass ? "Pass" : "Fail",
                                      ControlSampleId = x.PositionDescription
                                  };
            var resolutionAndEfficiencyFieldList = resolutionAndEfficiencyFields.ToList();
            rpt.tableResolutionAndEfficiency.DataSource = resolutionAndEfficiencyFieldList;

            var failed = from x in ReportTest.Samples
                         where qcSourcesList.Contains(x.Position) && x.Pass == false
                         select x;
            rpt.textBoxResolutionAndEfficiencyPassFail.Value = failed.Any() ? "FAIL" : "PASS";
            
        }

        private void LoadStandardsData()
        {
            StandardsReport rpt;

            switch (ReportTest.TestType)
            {
                case Constants.FullQCTest:
                    rpt = _fullQcReport.standardsReport;
                    break;

                case Constants.QCSourcesOnly:
                    return;

                case Constants.ContaminationOnly:
                    return;
                    
                case Constants.StandardsOnly:
                    rpt = _standardsOnlyReport.standardsReport;
                    break;

                default:
                    throw new NotSupportedException("Unsupported QC Test Type: " + ReportTest.TestType);
            }

            rpt.textBoxCountTime.Value = @"3.0 Minute Count Time";

            rpt.textBoxStandardsAcceptanceCriteria.Value = @"Acceptance Criteria: Difference between A & B counts to be within 3.89 x the standard deviation of the combined count and systematic errors, and the minimum rate for each standard is 500 counts per minute";

            int[] standardsList = { 9, 14 };

            var standardsFields = from x in ReportTest.Samples
                                  where standardsList.Contains(x.Position)
                                  orderby x.Position
                                  select new StandardRecord
                                  {
                                      ChangerLocation = x.Position,
                                      Pass = x.Pass ? "Pass" : "Fail",
                                      Rate = x.Counts < 0 ? 0 : (int)Math.Round(x.Counts / x.LiveTime * 60, 0),
                                      SampleName = x.PositionDescription,
                                      ActualCounts = x.Counts
                                  };

            var standardsFieldList = standardsFields.ToList();
            rpt.tableStandardsSamples.DataSource = standardsFieldList;

            var failed = from x in ReportTest.Samples
                         where standardsList.Contains(x.Position) && x.Pass == false
                         select x;
            rpt.textBoxStandardsPassFail.Value = failed.Any() ? "FAIL" : "PASS";
                                  
            if (ReportTest.StandardsFailureDetails.Count != 0)
            {
                rpt.panelFailureDetails.Visible = true;
                rpt.tableFailureDetails.DataSource = ReportTest.StandardsFailureDetails;
            }

        }

        private void LoadContaminationData()
        {
            ContaminationReport rpt;
            switch (ReportTest.TestType)
            {
                case Constants.FullQCTest:
                    rpt = _fullQcReport.contaminationReport;
                    break;

                case Constants.QCSourcesOnly:
                    return;

                case Constants.ContaminationOnly:
                    rpt = _contaminationOnlyReport.contaminationReport;
                    break;

                case Constants.StandardsOnly:
                    return;

                default:
                    throw new NotSupportedException("Unsupported QC Test Type: " + ReportTest.TestType);
            }

            rpt.textBoxCountTime.Value = @"1.0 Minute Count Time";

            rpt.textBoxContaminationAcceptanceCriteria.Value = @"Acceptance Criteria: <= 2 x background rate or " +
                 GetSettingValue("BACKGROUND_TOLERANCE") + @" CPM, whichever is the larger";

            int[] firstHalfList = { 25, 1, 2,  3, 4, 5, 6, 7, 8 };

            var firstHalfFields = from x in ReportTest.Samples
                                  where firstHalfList.Contains(x.Position)
                                  orderby x.SortOrder 
                                  select new ContaminationSample
                                  {
                                      ChangerLocation = x.Position,
                                      Pass = x.Pass ? "Pass" : "Fail",
                                      Rate = (int)Math.Round(x.Counts/x.LiveTime * 60, 0),
                                      SampleName = x.PositionDescription
                                  };

            var firstHalfFieldList = firstHalfFields.ToList();
            rpt.tableFirstHalf.DataSource = firstHalfFieldList;

            int[] secondHalfList = { 23, 22, 21, 20, 19, 18, 17, 16, 15};

            var secondHalfFields = from x in ReportTest.Samples
                                   where secondHalfList.Contains(x.Position)
                                   orderby x.SortOrder 
                                   select new ContaminationSample
                                   {
                                       ChangerLocation = x.Position,
                                       Pass = x.Pass ? "Pass" : "Fail",
                                       Rate = (int)Math.Round(x.Counts / x.LiveTime * 60, 0),
                                       SampleName = x.PositionDescription
                                   };

            var secondHalfFieldsList = secondHalfFields.ToList();
            rpt.tableSecondHalf.DataSource = secondHalfFieldsList;
            
            var allPositions = firstHalfList.Union(secondHalfList);
            var failed = from x in ReportTest.Samples
                         where allPositions.Contains(x.Position) && x.Pass == false
                         select x;
            rpt.textBoxContaminationPassFail.Value = failed.Any() ? @"FAIL" : @"PASS";
        }

        private void LoadBackgroundData()
        {
            BackgroundReport rpt;

           switch (ReportTest.TestType)
            {
                case Constants.FullQCTest:
                    rpt = _fullQcReport.backgroundReport;
                    break;

                case Constants.QCSourcesOnly:
                    return;

                case Constants.ContaminationOnly:
                    rpt = _contaminationOnlyReport.backgroundReport;
                    break;

                case Constants.StandardsOnly:
                    rpt = _standardsOnlyReport.backgroundReport;
                    break;

                default:
                    throw new NotSupportedException("Unsupported QC Test Type: " + ReportTest.TestType);
            }

            var rateCpm = (int)Math.Round(((ReportTest.BackgroundCount / ReportTest.BackgroundTime) * 60), 0);
            
            rpt.textBoxBackgroundAcceptanceCriteria.Value = @"Acceptance Criteria: Rate to be < " +
                 GetSettingValue("BACKGROUND_TOLERANCE") + @" CPM";
           
            rpt.textBoxBackgroundPassFail.Value = rpt.textBoxBackgroundPass.Value = rateCpm <
              int.Parse(GetSettingValue("BACKGROUND_TOLERANCE")) ? "Pass" : "Fail";
            
            rpt.textBoxBackgroundRate.Value = rateCpm.ToString(CultureInfo.InvariantCulture);
            rpt.textBoxBackgroundPosition.Value = @"24";

            rpt.textBoxCountTime.Value = Math.Round(double.Parse(GetSettingValue("BACKGROUND_COUNT_DURATION")) / 60, 1).ToString("##.0") +
                @" Minute Count Time";
        }

        private void LoadHeaderData()
        {
            _qcSourcesOnlyReport.textBoxBVAVersion.Value = _standardsOnlyReport.textBoxBVAVersion.Value = _contaminationOnlyReport.textBoxBVAVersion.Value = _fullQcReport.textBoxBVAVersion.Value = @"5.4";
            _qcSourcesOnlyReport.textBoxUnitID.Value = _standardsOnlyReport.textBoxUnitID.Value = _contaminationOnlyReport.textBoxUnitID.Value = _fullQcReport.textBoxUnitID.Value = ReportTest.UniqueSystemId ?? string.Empty;
            _qcSourcesOnlyReport.textBoxAnalyst.Value = _standardsOnlyReport.textBoxAnalyst.Value = _contaminationOnlyReport.textBoxAnalyst.Value = _fullQcReport.textBoxAnalyst.Value = ReportTest.Analyst;
            _qcSourcesOnlyReport.textBoxComment.Value = _standardsOnlyReport.textBoxComment.Value = _contaminationOnlyReport.textBoxComment.Value = _fullQcReport.textBoxComment.Value = ReportTest.Comment;
            _qcSourcesOnlyReport.textBoxCumulativePassFail.Value = _standardsOnlyReport.textBoxCumulativePassFail.Value = _contaminationOnlyReport.textBoxCumulativePassFail.Value = _fullQcReport.textBoxCumulativePassFail.Value = ReportTest.Pass ? "PASS" : "FAIL";
            _qcSourcesOnlyReport.textBoxDepartment.Value = _standardsOnlyReport.textBoxDepartment.Value = _contaminationOnlyReport.textBoxDepartment.Value = _fullQcReport.textBoxDepartment.Value = GetSettingValue("DEPARTMENT_NAME");
            _qcSourcesOnlyReport.textBoxDepartmentAddress.Value = _standardsOnlyReport.textBoxDepartmentAddress.Value = _contaminationOnlyReport.textBoxDepartmentAddress.Value = _fullQcReport.textBoxDepartmentAddress.Value = GetSettingValue("DEPARTMENT_ADDRESS");
            _qcSourcesOnlyReport.textBoxDepartmentDirector.Value = _standardsOnlyReport.textBoxDepartmentDirector.Value = _contaminationOnlyReport.textBoxDepartmentDirector.Value = _fullQcReport.textBoxDepartmentDirector.Value = GetSettingValue("DEPARTMENT_DIRECTOR");
            _qcSourcesOnlyReport.textBoxDepartmentPhoneNumber.Value = _standardsOnlyReport.textBoxDepartmentPhoneNumber.Value = _contaminationOnlyReport.textBoxDepartmentPhoneNumber.Value = _fullQcReport.textBoxDepartmentPhoneNumber.Value = GetSettingValue("DEPARTMENT_PHONE_NUMBER");
            _qcSourcesOnlyReport.textBoxHospitalName.Value = _standardsOnlyReport.textBoxHospitalName.Value = _contaminationOnlyReport.textBoxHospitalName.Value = _fullQcReport.textBoxHospitalName.Value = GetSettingValue("HOSPITAL_NAME");
             _standardsOnlyReport.textBoxInjectateLot.Value = _fullQcReport.textBoxInjectateLot.Value = ReportTest.InjectateLotNumber;
            _qcSourcesOnlyReport.textBoxROI.Value = _standardsOnlyReport.textBoxROI.Value = _contaminationOnlyReport.textBoxROI.Value = _fullQcReport.textBoxROI.Value = GetSettingValue("PAT_ROI_LOW_BOUND") + @" to " + GetSettingValue("PAT_ROI_HIGH_BOUND");
            _qcSourcesOnlyReport.textBoxFineGain.Value = _standardsOnlyReport.textBoxFineGain.Value = _contaminationOnlyReport.textBoxFineGain.Value = _fullQcReport.textBoxFineGain.Value = ReportTest.FineGain.ToString(CultureInfo.InvariantCulture);
            _qcSourcesOnlyReport.textBoxTestDate.Value = _standardsOnlyReport.textBoxTestDate.Value = _contaminationOnlyReport.textBoxTestDate.Value = _fullQcReport.textBoxTestDate.Value = ReportTest.TestDate.ToShortDateString() + @" " + ReportTest.TestDate.ToShortTimeString();
            _qcSourcesOnlyReport.textBoxVoltage.Value = _standardsOnlyReport.textBoxVoltage.Value = _contaminationOnlyReport.textBoxVoltage.Value = _fullQcReport.textBoxVoltage.Value = ReportTest.Voltage.ToString(CultureInfo.InvariantCulture);
            if (ReportTest.CoarseGain != 0)
            {
                _qcSourcesOnlyReport.textBoxCoarseGain.Value = _standardsOnlyReport.textBoxCoarseGain.Value = _contaminationOnlyReport.textBoxCoarseGain.Value = _fullQcReport.textBoxCoarseGain.Value = ReportTest.CoarseGain.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                _qcSourcesOnlyReport.textBoxCoarseGainLabel.Visible = _standardsOnlyReport.textBoxCoarseGainLabel.Visible = _contaminationOnlyReport.textBoxCoarseGainLabel.Visible = _fullQcReport.textBoxCoarseGainLabel.Visible = false;
            
            }
            _qcSourcesOnlyReport.textBoxPrintedOn.Value = _standardsOnlyReport.textBoxPrintedOn.Value = _contaminationOnlyReport.textBoxPrintedOn.Value = _fullQcReport.textBoxPrintedOn.Value = @"Printed " + DateTime.Now.ToString("g");
           
        }

        #region IReportController Members

        public override void PrintReport()
        {
            LoadReportData();

            PrintController standardPrintController =
                               new StandardPrintController();
            var settings = new PrinterSettings();


            var proc = new ReportProcessor {PrintController = standardPrintController};

            switch (ReportTest.TestType)
            {
                case Constants.FullQCTest:
                    proc.PrintReport(new InstanceReportSource {ReportDocument = _fullQcReport}, settings);
                    break;

                case Constants.QCSourcesOnly:
                    proc.PrintReport(new InstanceReportSource {ReportDocument = _qcSourcesOnlyReport}, settings);
                    break;

                case Constants.ContaminationOnly:
                    proc.PrintReport(new InstanceReportSource {ReportDocument = _contaminationOnlyReport}, settings);
                    break;

                case Constants.StandardsOnly:
                    proc.PrintReport(new InstanceReportSource {ReportDocument = _standardsOnlyReport}, settings);
                    break;

                default:
                    throw new NotSupportedException("Unsupported QC Test Type: " + ReportTest.TestType);
            }
        }

        public override IReportDocument ReportToView
        {
            get 
            {
                switch (ReportTest.TestType)
                {
                    case Constants.FullQCTest:
                        return _fullQcReport;

                    case Constants.QCSourcesOnly:
                        return _qcSourcesOnlyReport;
                        //return null;

                    case Constants.ContaminationOnly:
                        return _contaminationOnlyReport;

                    case Constants.StandardsOnly:
                        return _standardsOnlyReport;

                    default:
                        throw new NotSupportedException("Unsupported QC Test Type: " + ReportTest.TestType);
                }
            }
        }

        LinearGradientBrush _frameColorBrush;
        public LinearGradientBrush FrameColorBrush
        {
            get
            {
                return _frameColorBrush;
            }
            set
            {
                _frameColorBrush = value;
                FirePropertyChanged("FrameColorBrush");
            }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
        }

        #endregion

        private static string GetSettingValue(string setting)
        {
            var context = new QCArchiveDataContext(Globals.ConnectionString);

            var result = context.GetSettingValue(setting);

            var settingValue = result.FirstOrDefault();

            return settingValue != null ? settingValue.ATTRIBUTE_VALUE : string.Empty;
        }


        private void CalculateQCRatioBounds(double startSourceActivity, double nominalActivity, out double ratioLowbound, out double ratioHighbound)
        {
            const double ratioFactor = 0.22;
    
            var nominalRatio = Math.Round(startSourceActivity / nominalActivity, 2);
            var adjustmentFactor = Math.Round((nominalRatio * ratioFactor), 2);
            ratioLowbound = nominalRatio - adjustmentFactor;
            ratioHighbound =  nominalRatio + adjustmentFactor;
        }


        public override void ExportReport()
        {
            throw new NotImplementedException();
        }
    }
}
