namespace QCTestArchive.Report
{
    partial class ContaminationReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            textBox14 = new Telerik.Reporting.TextBox();
            textBox15 = new Telerik.Reporting.TextBox();
            textBox16 = new Telerik.Reporting.TextBox();
            textBox17 = new Telerik.Reporting.TextBox();
            textBox2 = new Telerik.Reporting.TextBox();
            textBox3 = new Telerik.Reporting.TextBox();
            textBox4 = new Telerik.Reporting.TextBox();
            textBox8 = new Telerik.Reporting.TextBox();
            pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            detail = new Telerik.Reporting.DetailSection();
            textBoxCountTime = new Telerik.Reporting.TextBox();
            shape1 = new Telerik.Reporting.Shape();
            tableSecondHalf = new Telerik.Reporting.Table();
            textBox10 = new Telerik.Reporting.TextBox();
            textBox11 = new Telerik.Reporting.TextBox();
            textBox12 = new Telerik.Reporting.TextBox();
            textBox13 = new Telerik.Reporting.TextBox();
            tableFirstHalf = new Telerik.Reporting.Table();
            textBox5 = new Telerik.Reporting.TextBox();
            textBox6 = new Telerik.Reporting.TextBox();
            textBox7 = new Telerik.Reporting.TextBox();
            textBox9 = new Telerik.Reporting.TextBox();
            textBoxContaminationAcceptanceCriteria = new Telerik.Reporting.TextBox();
            textBoxContaminationPassFail = new Telerik.Reporting.TextBox();
            shape12 = new Telerik.Reporting.Shape();
            textBox1 = new Telerik.Reporting.TextBox();
            pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox14
            // 
            textBox14.Name = "textBox14";
            textBox14.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3032608032226563D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.35937497019767761D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox14.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBox14.Value = "Sample";
            // 
            // textBox15
            // 
            textBox15.Name = "textBox15";
            textBox15.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.066645622253418D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.35937497019767761D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox15.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBox15.Value = "Changer Location";
            // 
            // textBox16
            // 
            textBox16.Name = "textBox16";
            textBox16.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.54834592342376709D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.35937497019767761D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBox16.Value = "Rate CPM";
            // 
            // textBox17
            // 
            textBox17.Name = "textBox17";
            textBox17.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0816689729690552D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.35937497019767761D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBox17.StyleName = "";
            textBox17.Value = "Pass/Fail";
            // 
            // textBox2
            // 
            textBox2.Name = "textBox2";
            textBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.204861044883728D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.359375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBox2.Value = "Sample";
            // 
            // textBox3
            // 
            textBox3.Name = "textBox3";
            textBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.986111044883728D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.359375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBox3.Value = "Changer Location";
            // 
            // textBox4
            // 
            textBox4.Name = "textBox4";
            textBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.50694435834884644D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.359375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBox4.Value = "Rate CPM";
            // 
            // textBox8
            // 
            textBox8.Name = "textBox8";
            textBox8.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.15625D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.359375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBox8.StyleName = "";
            textBox8.Value = "Pass/Fail";
            // 
            // pageHeaderSection1
            // 
            pageHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.0520833320915699D, Telerik.Reporting.Drawing.UnitType.Inch);
            pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // detail
            // 
            detail.Height = new Telerik.Reporting.Drawing.Unit(3.5145835876464844D, Telerik.Reporting.Drawing.UnitType.Inch);
            detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBoxCountTime,
            shape1,
            tableSecondHalf,
            tableFirstHalf,
            textBoxContaminationAcceptanceCriteria,
            textBoxContaminationPassFail,
            shape12,
            textBox1});
            detail.Name = "detail";
            // 
            // textBoxCountTime
            // 
            textBoxCountTime.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.5208332538604736D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.058333396911621094D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxCountTime.Name = "textBoxCountTime";
            textBoxCountTime.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.1000003814697266D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxCountTime.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxCountTime.Value = "1.0 Minute Count Time";
            // 
            // shape1
            // 
            shape1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.9166667461395264D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.66250008344650269D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape1.Name = "shape1";
            shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.NS);
            shape1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.099999748170375824D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(2.7999210357666016D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // tableSecondHalf
            // 
            tableSecondHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.3032608032226563D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableSecondHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.066645622253418D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableSecondHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(0.54834592342376709D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableSecondHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.0816690921783447D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableSecondHalf.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.23437504470348358D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableSecondHalf.Body.SetCellContent(0, 0, textBox10);
            tableSecondHalf.Body.SetCellContent(0, 1, textBox11);
            tableSecondHalf.Body.SetCellContent(0, 2, textBox12);
            tableSecondHalf.Body.SetCellContent(0, 3, textBox13);
            tableGroup1.ReportItem = textBox14;
            tableGroup2.ReportItem = textBox15;
            tableGroup3.ReportItem = textBox16;
            tableGroup4.Name = "Group1";
            tableGroup4.ReportItem = textBox17;
            tableSecondHalf.ColumnGroups.Add(tableGroup1);
            tableSecondHalf.ColumnGroups.Add(tableGroup2);
            tableSecondHalf.ColumnGroups.Add(tableGroup3);
            tableSecondHalf.ColumnGroups.Add(tableGroup4);
            tableSecondHalf.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBox10,
            textBox11,
            textBox12,
            textBox13,
            textBox14,
            textBox15,
            textBox16,
            textBox17});
            tableSecondHalf.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.0208334922790527D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.66250008344650269D, Telerik.Reporting.Drawing.UnitType.Inch));
            tableSecondHalf.Name = "tableSecondHalf";
            tableGroup5.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping("")});
            tableGroup5.Name = "DetailGroup";
            tableSecondHalf.RowGroups.Add(tableGroup5);
            tableSecondHalf.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.9999215602874756D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.59375D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox10
            // 
            textBox10.Name = "textBox10";
            textBox10.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3032608032226563D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23437502980232239D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox10.Value = "=Fields.SampleName";
            // 
            // textBox11
            // 
            textBox11.Name = "textBox11";
            textBox11.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.066645622253418D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23437502980232239D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox11.Value = "=Fields.ChangerLocation";
            // 
            // textBox12
            // 
            textBox12.CanGrow = false;
            textBox12.Name = "textBox12";
            textBox12.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.54834592342376709D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23437502980232239D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox12.Value = "=Fields.Rate";
            // 
            // textBox13
            // 
            textBox13.Name = "textBox13";
            textBox13.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0816689729690552D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23437502980232239D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox13.StyleName = "";
            textBox13.Value = "=Fields.Pass";
            // 
            // tableFirstHalf
            // 
            tableFirstHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.204861044883728D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFirstHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(0.986111044883728D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFirstHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(0.50694441795349121D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFirstHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.15625D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFirstHalf.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.23437504470348358D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFirstHalf.Body.SetCellContent(0, 0, textBox5);
            tableFirstHalf.Body.SetCellContent(0, 1, textBox6);
            tableFirstHalf.Body.SetCellContent(0, 2, textBox7);
            tableFirstHalf.Body.SetCellContent(0, 3, textBox9);
            tableGroup6.ReportItem = textBox2;
            tableGroup7.ReportItem = textBox3;
            tableGroup8.ReportItem = textBox4;
            tableGroup9.Name = "Group1";
            tableGroup9.ReportItem = textBox8;
            tableFirstHalf.ColumnGroups.Add(tableGroup6);
            tableFirstHalf.ColumnGroups.Add(tableGroup7);
            tableFirstHalf.ColumnGroups.Add(tableGroup8);
            tableFirstHalf.ColumnGroups.Add(tableGroup9);
            tableFirstHalf.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBox5,
            textBox6,
            textBox7,
            textBox9,
            textBox2,
            textBox3,
            textBox4,
            textBox8});
            tableFirstHalf.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.66250008344650269D, Telerik.Reporting.Drawing.UnitType.Inch));
            tableFirstHalf.Name = "tableFirstHalf";
            tableGroup10.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping("")});
            tableGroup10.Name = "DetailGroup";
            tableFirstHalf.RowGroups.Add(tableGroup10);
            tableFirstHalf.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.8541667461395264D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.59375D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox5
            // 
            textBox5.Name = "textBox5";
            textBox5.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.2048611640930176D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.234375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox5.Value = "=Fields.SampleName";
            // 
            // textBox6
            // 
            textBox6.Name = "textBox6";
            textBox6.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.986111044883728D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.234375D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox6.Value = "=Fields.ChangerLocation";
            // 
            // textBox7
            // 
            textBox7.CanGrow = false;
            textBox7.Name = "textBox7";
            textBox7.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.50694435834884644D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23437501490116119D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox7.Value = "=Fields.Rate";
            // 
            // textBox9
            // 
            textBox9.Name = "textBox9";
            textBox9.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.15625D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23437501490116119D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox9.StyleName = "";
            textBox9.Value = "=Fields.Pass";
            // 
            // textBoxContaminationAcceptanceCriteria
            // 
            textBoxContaminationAcceptanceCriteria.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.36041674017906189D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxContaminationAcceptanceCriteria.Name = "textBoxContaminationAcceptanceCriteria";
            textBoxContaminationAcceptanceCriteria.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9957542419433594D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxContaminationAcceptanceCriteria.Style.Font.Name = "Times New Roman";
            textBoxContaminationAcceptanceCriteria.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxContaminationAcceptanceCriteria.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxContaminationAcceptanceCriteria.Value = "Acceptance Criteria: <= 2 x background rate or 100 CPM, whichever is larger";
            // 
            // textBoxContaminationPassFail
            // 
            textBoxContaminationPassFail.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.21875D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.058333396911621094D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxContaminationPassFail.Name = "textBoxContaminationPassFail";
            textBoxContaminationPassFail.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.7999998927116394D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxContaminationPassFail.Style.Font.Bold = true;
            textBoxContaminationPassFail.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxContaminationPassFail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            textBoxContaminationPassFail.Value = "";
            // 
            // shape12
            // 
            shape12.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2562500536441803D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape12.Name = "shape12";
            shape12.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            shape12.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9957542419433594D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.079166412353515625D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox1
            // 
            textBox1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.02083333395421505D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.058333396911621094D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox1.Name = "textBox1";
            textBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.7000001668930054D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox1.Style.Font.Bold = true;
            textBox1.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBox1.Value = "Contamination";
            // 
            // pageFooterSection1
            // 
            pageFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(0.0520833320915699D, Telerik.Reporting.Drawing.UnitType.Inch);
            pageFooterSection1.Name = "pageFooterSection1";
            // 
            // ContaminationReport
            // 
            Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            pageHeaderSection1,
            detail,
            pageFooterSection1});
            PageSettings.Landscape = false;
            PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(0.25D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            Style.BackgroundColor = System.Drawing.Color.White;
            Width = new Telerik.Reporting.Drawing.Unit(8.02075481414795D, Telerik.Reporting.Drawing.UnitType.Inch);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        public Telerik.Reporting.TextBox textBoxCountTime;
        private Telerik.Reporting.Shape shape1;
        public Telerik.Reporting.Table tableSecondHalf;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        public Telerik.Reporting.Table tableFirstHalf;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox8;
        public Telerik.Reporting.TextBox textBoxContaminationAcceptanceCriteria;
        public Telerik.Reporting.TextBox textBoxContaminationPassFail;
        private Telerik.Reporting.Shape shape12;
        private Telerik.Reporting.TextBox textBox1;
    }
}