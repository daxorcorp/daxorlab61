namespace QCTestArchive.Report
{
    partial class ResolutionAndEfficiencyReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            textBox2 = new Telerik.Reporting.TextBox();
            textBox3 = new Telerik.Reporting.TextBox();
            textBox4 = new Telerik.Reporting.TextBox();
            textBox8 = new Telerik.Reporting.TextBox();
            textBox10 = new Telerik.Reporting.TextBox();
            textBox12 = new Telerik.Reporting.TextBox();
            textBox14 = new Telerik.Reporting.TextBox();
            textBox16 = new Telerik.Reporting.TextBox();
            textBox18 = new Telerik.Reporting.TextBox();
            pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            detail = new Telerik.Reporting.DetailSection();
            textBox1 = new Telerik.Reporting.TextBox();
            shape12 = new Telerik.Reporting.Shape();
            textBoxResolutionAndEfficiencyPassFail = new Telerik.Reporting.TextBox();
            textBoxContaminationAcceptanceCriteria = new Telerik.Reporting.TextBox();
            textBoxCountTime = new Telerik.Reporting.TextBox();
            tableResolutionAndEfficiency = new Telerik.Reporting.Table();
            textBox5 = new Telerik.Reporting.TextBox();
            textBox6 = new Telerik.Reporting.TextBox();
            textBox7 = new Telerik.Reporting.TextBox();
            textBox9 = new Telerik.Reporting.TextBox();
            textBox11 = new Telerik.Reporting.TextBox();
            textBox13 = new Telerik.Reporting.TextBox();
            textBox15 = new Telerik.Reporting.TextBox();
            textBox17 = new Telerik.Reporting.TextBox();
            textBox19 = new Telerik.Reporting.TextBox();
            pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox2
            // 
            textBox2.Name = "textBox2";
            textBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.89583331346511841D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox2.Value = "Control Sample ID";
            // 
            // textBox3
            // 
            textBox3.Name = "textBox3";
            textBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.76041662693023682D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox3.Value = "Changer Location";
            // 
            // textBox4
            // 
            textBox4.Name = "textBox4";
            textBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0520830154418945D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox4.Value = "Serial Number";
            // 
            // textBox8
            // 
            textBox8.Name = "textBox8";
            textBox8.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.6875D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox8.StyleName = "";
            textBox8.Value = "Centroid chs";
            // 
            // textBox10
            // 
            textBox10.Name = "textBox10";
            textBox10.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.70833331346511841D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox10.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox10.StyleName = "";
            textBox10.Value = "FWHM chs";
            // 
            // textBox12
            // 
            textBox12.Name = "textBox12";
            textBox12.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.1041668653488159D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox12.StyleName = "";
            // 
            // textBox14
            // 
            textBox14.Name = "textBox14";
            textBox14.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.88541668653488159D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox14.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox14.StyleName = "";
            textBox14.Value = "Actual Counts";
            // 
            // textBox16
            // 
            textBox16.Name = "textBox16";
            textBox16.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.78124988079071045D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox16.StyleName = "";
            // 
            // textBox18
            // 
            textBox18.Name = "textBox18";
            textBox18.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.1145833730697632D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox18.StyleName = "";
            textBox18.Value = "Pass/Fail";
            // 
            // pageHeaderSection1
            // 
            pageHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.1000000610947609D, Telerik.Reporting.Drawing.UnitType.Inch);
            pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // detail
            // 
            detail.Height = new Telerik.Reporting.Drawing.Unit(1.5D, Telerik.Reporting.Drawing.UnitType.Inch);
            detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBox1,
            shape12,
            textBoxResolutionAndEfficiencyPassFail,
            textBoxContaminationAcceptanceCriteria,
            textBoxCountTime,
            tableResolutionAndEfficiency});
            detail.Name = "detail";
            // 
            // textBox1
            // 
            textBox1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox1.Name = "textBox1";
            textBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.3000001907348633D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox1.Style.Font.Bold = true;
            textBox1.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBox1.Value = "Resolution and Efficiency";
            // 
            // shape12
            // 
            shape12.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.29791656136512756D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape12.Name = "shape12";
            shape12.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            shape12.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9957542419433594D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.079166412353515625D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBoxResolutionAndEfficiencyPassFail
            // 
            textBoxResolutionAndEfficiencyPassFail.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.1979165077209473D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxResolutionAndEfficiencyPassFail.Name = "textBoxResolutionAndEfficiencyPassFail";
            textBoxResolutionAndEfficiencyPassFail.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.7999998927116394D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxResolutionAndEfficiencyPassFail.Style.Font.Bold = true;
            textBoxResolutionAndEfficiencyPassFail.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxResolutionAndEfficiencyPassFail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            textBoxResolutionAndEfficiencyPassFail.Value = "";
            // 
            // textBoxContaminationAcceptanceCriteria
            // 
            textBoxContaminationAcceptanceCriteria.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40208324790000916D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxContaminationAcceptanceCriteria.Name = "textBoxContaminationAcceptanceCriteria";
            textBoxContaminationAcceptanceCriteria.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9957542419433594D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxContaminationAcceptanceCriteria.Style.Font.Name = "Times New Roman";
            textBoxContaminationAcceptanceCriteria.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxContaminationAcceptanceCriteria.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxContaminationAcceptanceCriteria.Value = "Acceptance Criteria: Centroid to be 356 +/- 6 channels and FWHM to be in the rang" +
                "e of 14 to 42 channels and counts to be > 10,000";
            // 
            // textBoxCountTime
            // 
            textBoxCountTime.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.4895832538604736D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999904632568359D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxCountTime.Name = "textBoxCountTime";
            textBoxCountTime.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.1000003814697266D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxCountTime.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxCountTime.Value = "Isotope: Ba-133";
            // 
            // tableResolutionAndEfficiency
            // 
            tableResolutionAndEfficiency.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(0.895833432674408D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableResolutionAndEfficiency.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(0.76041650772094727D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableResolutionAndEfficiency.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.0520827770233154D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableResolutionAndEfficiency.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(0.68750017881393433D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableResolutionAndEfficiency.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(0.70833331346511841D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableResolutionAndEfficiency.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.1041668653488159D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableResolutionAndEfficiency.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(0.88541674613952637D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableResolutionAndEfficiency.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(0.78124994039535522D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableResolutionAndEfficiency.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.1145833730697632D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableResolutionAndEfficiency.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableResolutionAndEfficiency.Body.SetCellContent(0, 0, textBox5);
            tableResolutionAndEfficiency.Body.SetCellContent(0, 1, textBox6);
            tableResolutionAndEfficiency.Body.SetCellContent(0, 2, textBox7);
            tableResolutionAndEfficiency.Body.SetCellContent(0, 3, textBox9);
            tableResolutionAndEfficiency.Body.SetCellContent(0, 4, textBox11);
            tableResolutionAndEfficiency.Body.SetCellContent(0, 5, textBox13);
            tableResolutionAndEfficiency.Body.SetCellContent(0, 6, textBox15);
            tableResolutionAndEfficiency.Body.SetCellContent(0, 7, textBox17);
            tableResolutionAndEfficiency.Body.SetCellContent(0, 8, textBox19);
            tableGroup1.ReportItem = textBox2;
            tableGroup2.ReportItem = textBox3;
            tableGroup3.ReportItem = textBox4;
            tableGroup4.Name = "Group1";
            tableGroup4.ReportItem = textBox8;
            tableGroup5.Name = "Group2";
            tableGroup5.ReportItem = textBox10;
            tableGroup6.Name = "Group3";
            tableGroup6.ReportItem = textBox12;
            tableGroup7.Name = "Group4";
            tableGroup7.ReportItem = textBox14;
            tableGroup8.Name = "Group5";
            tableGroup8.ReportItem = textBox16;
            tableGroup9.Name = "Group6";
            tableGroup9.ReportItem = textBox18;
            tableResolutionAndEfficiency.ColumnGroups.Add(tableGroup1);
            tableResolutionAndEfficiency.ColumnGroups.Add(tableGroup2);
            tableResolutionAndEfficiency.ColumnGroups.Add(tableGroup3);
            tableResolutionAndEfficiency.ColumnGroups.Add(tableGroup4);
            tableResolutionAndEfficiency.ColumnGroups.Add(tableGroup5);
            tableResolutionAndEfficiency.ColumnGroups.Add(tableGroup6);
            tableResolutionAndEfficiency.ColumnGroups.Add(tableGroup7);
            tableResolutionAndEfficiency.ColumnGroups.Add(tableGroup8);
            tableResolutionAndEfficiency.ColumnGroups.Add(tableGroup9);
            tableResolutionAndEfficiency.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBox5,
            textBox6,
            textBox7,
            textBox9,
            textBox11,
            textBox13,
            textBox15,
            textBox17,
            textBox19,
            textBox2,
            textBox3,
            textBox4,
            textBox8,
            textBox10,
            textBox12,
            textBox14,
            textBox16,
            textBox18});
            tableResolutionAndEfficiency.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.59999996423721313D, Telerik.Reporting.Drawing.UnitType.Inch));
            tableResolutionAndEfficiency.Name = "tableResolutionAndEfficiency";
            tableGroup10.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping("")});
            tableGroup10.Name = "DetailGroup";
            tableResolutionAndEfficiency.RowGroups.Add(tableGroup10);
            tableResolutionAndEfficiency.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9895825386047363D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.7999998927116394D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox5
            // 
            textBox5.Name = "textBox5";
            textBox5.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.89583331346511841D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox5.Value = "=Fields.ControlSampleId";
            // 
            // textBox6
            // 
            textBox6.Name = "textBox6";
            textBox6.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.76041662693023682D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox6.Value = "=Fields.ChangerLocation";
            // 
            // textBox7
            // 
            textBox7.Name = "textBox7";
            textBox7.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0520830154418945D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox7.Value = "=Fields.SerialNumber";
            // 
            // textBox9
            // 
            textBox9.Name = "textBox9";
            textBox9.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.6875D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox9.StyleName = "";
            textBox9.Value = "=Fields.Centroid";
            // 
            // textBox11
            // 
            textBox11.Name = "textBox11";
            textBox11.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.70833331346511841D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox11.StyleName = "";
            textBox11.Value = "=Fields.FWHM";
            // 
            // textBox13
            // 
            textBox13.Name = "textBox13";
            textBox13.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.1041668653488159D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox13.StyleName = "";
            // 
            // textBox15
            // 
            textBox15.Format = "{0:N0}";
            textBox15.Name = "textBox15";
            textBox15.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.88541668653488159D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox15.StyleName = "";
            textBox15.Value = "=Fields.ActualCounts";
            // 
            // textBox17
            // 
            textBox17.Name = "textBox17";
            textBox17.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.78124988079071045D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox17.StyleName = "";
            // 
            // textBox19
            // 
            textBox19.Name = "textBox19";
            textBox19.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.1145833730697632D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox19.StyleName = "";
            textBox19.Value = "=Fields.Pass";
            // 
            // pageFooterSection1
            // 
            pageFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(0.0520833320915699D, Telerik.Reporting.Drawing.UnitType.Inch);
            pageFooterSection1.Name = "pageFooterSection1";
            // 
            // ResolutionAndEfficiencyReport
            // 
            Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            pageHeaderSection1,
            detail,
            pageFooterSection1});
            PageSettings.Landscape = false;
            PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(0.25D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            Style.BackgroundColor = System.Drawing.Color.White;
            Width = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Inch);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.Shape shape12;
        public Telerik.Reporting.TextBox textBoxResolutionAndEfficiencyPassFail;
        public Telerik.Reporting.TextBox textBoxContaminationAcceptanceCriteria;
        public Telerik.Reporting.TextBox textBoxCountTime;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox18;
        public Telerik.Reporting.Table tableResolutionAndEfficiency;
    }
}