namespace QCTestArchive.Report
{
    partial class BackgroundReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            detail = new Telerik.Reporting.DetailSection();
            textBoxCountTime = new Telerik.Reporting.TextBox();
            textBoxBackgroundPass = new Telerik.Reporting.TextBox();
            textBoxBackgroundRate = new Telerik.Reporting.TextBox();
            textBoxBackgroundPosition = new Telerik.Reporting.TextBox();
            textBox6 = new Telerik.Reporting.TextBox();
            shape2 = new Telerik.Reporting.Shape();
            textBox5 = new Telerik.Reporting.TextBox();
            textBox4 = new Telerik.Reporting.TextBox();
            textBox3 = new Telerik.Reporting.TextBox();
            textBox2 = new Telerik.Reporting.TextBox();
            shape1 = new Telerik.Reporting.Shape();
            textBoxBackgroundAcceptanceCriteria = new Telerik.Reporting.TextBox();
            textBox1 = new Telerik.Reporting.TextBox();
            textBoxBackgroundPassFail = new Telerik.Reporting.TextBox();
            shape12 = new Telerik.Reporting.Shape();
            pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeaderSection1
            // 
            pageHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.0520833320915699D, Telerik.Reporting.Drawing.UnitType.Inch);
            pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // detail
            // 
            detail.Height = new Telerik.Reporting.Drawing.Unit(1.3958333730697632D, Telerik.Reporting.Drawing.UnitType.Inch);
            detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBoxCountTime,
            textBoxBackgroundPass,
            textBoxBackgroundRate,
            textBoxBackgroundPosition,
            textBox6,
            shape2,
            textBox5,
            textBox4,
            textBox3,
            textBox2,
            shape1,
            textBoxBackgroundAcceptanceCriteria,
            textBox1,
            textBoxBackgroundPassFail,
            shape12});
            detail.Name = "detail";
            // 
            // textBoxCountTime
            // 
            textBoxCountTime.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.5D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.060416858643293381D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxCountTime.Name = "textBoxCountTime";
            textBoxCountTime.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.1000003814697266D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxCountTime.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxCountTime.Value = "1.0 Minute Count Time";
            // 
            // textBoxBackgroundPass
            // 
            textBoxBackgroundPass.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.25D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.97708350419998169D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxBackgroundPass.Name = "textBoxBackgroundPass";
            textBoxBackgroundPass.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.78125D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2395833283662796D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxBackgroundPass.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxBackgroundPass.Value = "textBox7";
            // 
            // textBoxBackgroundRate
            // 
            textBoxBackgroundRate.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.2916667461395264D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.97708350419998169D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxBackgroundRate.Name = "textBoxBackgroundRate";
            textBoxBackgroundRate.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.49999967217445374D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2395833283662796D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxBackgroundRate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxBackgroundRate.Value = "textBox7";
            // 
            // textBoxBackgroundPosition
            // 
            textBoxBackgroundPosition.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.4166666269302368D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.97708350419998169D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxBackgroundPosition.Name = "textBoxBackgroundPosition";
            textBoxBackgroundPosition.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.78125D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2395833283662796D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxBackgroundPosition.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxBackgroundPosition.Value = "textBox7";
            // 
            // textBox6
            // 
            textBox6.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.0520833320915699D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.97708350419998169D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox6.Name = "textBox6";
            textBox6.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0395833253860474D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2395833283662796D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox6.Value = "Background";
            // 
            // shape2
            // 
            shape2.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.010416666977107525D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.2583335638046265D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape2.Name = "shape2";
            shape2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            shape2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(8.0374212265014648D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.079166412353515625D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox5
            // 
            textBox5.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.25D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.66458350419998169D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox5.Name = "textBox5";
            textBox5.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.7999998927116394D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox5.Value = "Pass/Fail";
            // 
            // textBox4
            // 
            textBox4.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.2916667461395264D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.53958350419998169D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox4.Name = "textBox4";
            textBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.49999985098838806D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.32067617774009705D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox4.Value = "Rate CPM";
            // 
            // textBox3
            // 
            textBox3.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.3958333730697632D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.53958350419998169D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox3.Name = "textBox3";
            textBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.32067617774009705D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox3.Value = "Changer Location";
            // 
            // textBox2
            // 
            textBox2.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.0416666679084301D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.66458350419998169D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox2.Name = "textBox2";
            textBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.80000013113021851D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox2.Value = "Sample";
            // 
            // shape1
            // 
            shape1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.010416666977107525D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.86250019073486328D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape1.Name = "shape1";
            shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            shape1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(8.0374212265014648D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.079166412353515625D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBoxBackgroundAcceptanceCriteria
            // 
            textBoxBackgroundAcceptanceCriteria.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.010416666977107525D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.34166684746742249D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxBackgroundAcceptanceCriteria.Name = "textBoxBackgroundAcceptanceCriteria";
            textBoxBackgroundAcceptanceCriteria.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(8.05825424194336D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxBackgroundAcceptanceCriteria.Style.Font.Name = "Times New Roman";
            textBoxBackgroundAcceptanceCriteria.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxBackgroundAcceptanceCriteria.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxBackgroundAcceptanceCriteria.Value = "Acceptance Criteria: Rate to be < 100 CPM";
            // 
            // textBox1
            // 
            textBox1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.0416666679084301D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.060416858643293381D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox1.Name = "textBox1";
            textBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.7000001668930054D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox1.Style.Font.Bold = true;
            textBox1.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBox1.Value = "Background";
            // 
            // textBoxBackgroundPassFail
            // 
            textBoxBackgroundPassFail.CanGrow = true;
            textBoxBackgroundPassFail.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.25D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.060416858643293381D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxBackgroundPassFail.Name = "textBoxBackgroundPassFail";
            textBoxBackgroundPassFail.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.7999998927116394D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxBackgroundPassFail.Style.Font.Bold = true;
            textBoxBackgroundPassFail.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxBackgroundPassFail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            textBoxBackgroundPassFail.Value = "";
            // 
            // shape12
            // 
            shape12.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.010416666977107525D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.25833353400230408D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape12.Name = "shape12";
            shape12.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            shape12.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(8.0374212265014648D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.079166412353515625D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // pageFooterSection1
            // 
            pageFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(0.0520833320915699D, Telerik.Reporting.Drawing.UnitType.Inch);
            pageFooterSection1.Name = "pageFooterSection1";
            // 
            // BackgroundReport
            // 
            Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            pageHeaderSection1,
            detail,
            pageFooterSection1});
            PageSettings.Landscape = false;
            PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(0.25D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            Style.BackgroundColor = System.Drawing.Color.White;
            Width = new Telerik.Reporting.Drawing.Unit(8.0686712265014648D, Telerik.Reporting.Drawing.UnitType.Inch);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        public Telerik.Reporting.TextBox textBoxCountTime;
        public Telerik.Reporting.TextBox textBoxBackgroundPass;
        public Telerik.Reporting.TextBox textBoxBackgroundRate;
        public Telerik.Reporting.TextBox textBoxBackgroundPosition;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.Shape shape2;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.Shape shape1;
        public Telerik.Reporting.TextBox textBoxBackgroundAcceptanceCriteria;
        private Telerik.Reporting.TextBox textBox1;
        public Telerik.Reporting.TextBox textBoxBackgroundPassFail;
        private Telerik.Reporting.Shape shape12;
    }
}