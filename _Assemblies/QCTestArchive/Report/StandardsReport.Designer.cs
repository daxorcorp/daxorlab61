namespace QCTestArchive.Report
{
    partial class StandardsReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StandardsReport));
            textBox10 = new Telerik.Reporting.TextBox();
            textBox11 = new Telerik.Reporting.TextBox();
            textBox12 = new Telerik.Reporting.TextBox();
            textBox16 = new Telerik.Reporting.TextBox();
            textBox18 = new Telerik.Reporting.TextBox();
            textBox2 = new Telerik.Reporting.TextBox();
            textBox3 = new Telerik.Reporting.TextBox();
            textBox8 = new Telerik.Reporting.TextBox();
            textBox4 = new Telerik.Reporting.TextBox();
            pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            detail = new Telerik.Reporting.DetailSection();
            textBoxCountTime = new Telerik.Reporting.TextBox();
            panelFailureDetails = new Telerik.Reporting.Panel();
            tableFailureDetails = new Telerik.Reporting.Table();
            textBox13 = new Telerik.Reporting.TextBox();
            textBox14 = new Telerik.Reporting.TextBox();
            textBox15 = new Telerik.Reporting.TextBox();
            textBox17 = new Telerik.Reporting.TextBox();
            textBox19 = new Telerik.Reporting.TextBox();
            textBoxAcceptanceRangeLabel = new Telerik.Reporting.TextBox();
            textBoxFailureDetailsLabel = new Telerik.Reporting.TextBox();
            tableStandardsSamples = new Telerik.Reporting.Table();
            textBox5 = new Telerik.Reporting.TextBox();
            textBox6 = new Telerik.Reporting.TextBox();
            textBox7 = new Telerik.Reporting.TextBox();
            textBox9 = new Telerik.Reporting.TextBox();
            shape12 = new Telerik.Reporting.Shape();
            textBoxStandardsPassFail = new Telerik.Reporting.TextBox();
            textBox1 = new Telerik.Reporting.TextBox();
            textBoxStandardsAcceptanceCriteria = new Telerik.Reporting.TextBox();
            pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox10
            // 
            textBox10.Name = "textBox10";
            textBox10.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2604166567325592D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox10.Style.Font.Bold = true;
            textBox10.Value = "ID";
            // 
            // textBox11
            // 
            textBox11.Name = "textBox11";
            textBox11.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2604166567325592D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox11.Style.Font.Bold = true;
            textBox11.Value = "Fail Item";
            // 
            // textBox12
            // 
            textBox12.Name = "textBox12";
            textBox12.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0833333730697632D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.26041668653488159D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox12.Style.Font.Bold = true;
            textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox12.Value = "Value";
            // 
            // textBox16
            // 
            textBox16.Name = "textBox16";
            textBox16.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2604166567325592D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox16.Style.Font.Bold = true;
            textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            textBox16.StyleName = "";
            textBox16.Value = "From";
            // 
            // textBox18
            // 
            textBox18.Name = "textBox18";
            textBox18.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2604166567325592D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox18.Style.Font.Bold = true;
            textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            textBox18.StyleName = "";
            textBox18.Value = "To";
            // 
            // textBox2
            // 
            textBox2.Name = "textBox2";
            textBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.5446571111679077D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.30000004172325134D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox2.Style.Padding.Bottom = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
            textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            textBox2.Value = "Sample";
            // 
            // textBox3
            // 
            textBox3.Name = "textBox3";
            textBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0180695056915283D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.30000004172325134D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            textBox3.Value = "Rate CPM";
            // 
            // textBox8
            // 
            textBox8.Name = "textBox8";
            textBox8.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.4971612691879273D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.30000004172325134D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            textBox8.StyleName = "";
            textBox8.Value = "Actual Counts";
            // 
            // textBox4
            // 
            textBox4.Name = "textBox4";
            textBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.9401123523712158D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.30000004172325134D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            textBox4.Value = "Pass/Fail";
            // 
            // pageHeaderSection1
            // 
            pageHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(0.0520833320915699D, Telerik.Reporting.Drawing.UnitType.Inch);
            pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // detail
            // 
            detail.Height = new Telerik.Reporting.Drawing.Unit(2.6000001430511475D, Telerik.Reporting.Drawing.UnitType.Inch);
            detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBoxCountTime,
            panelFailureDetails,
            tableStandardsSamples,
            shape12,
            textBoxStandardsPassFail,
            textBox1,
            textBoxStandardsAcceptanceCriteria});
            detail.Name = "detail";
            // 
            // textBoxCountTime
            // 
            textBoxCountTime.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.6979167461395264D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10833358764648438D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxCountTime.Name = "textBoxCountTime";
            textBoxCountTime.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.1000003814697266D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxCountTime.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxCountTime.Value = "3.0 Minute Count Time";
            // 
            // panelFailureDetails
            // 
            panelFailureDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            tableFailureDetails,
            textBoxAcceptanceRangeLabel,
            textBoxFailureDetailsLabel});
            panelFailureDetails.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.2916666567325592D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.5041669607162476D, Telerik.Reporting.Drawing.UnitType.Inch));
            panelFailureDetails.Name = "panelFailureDetails";
            panelFailureDetails.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.4000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch));
            panelFailureDetails.Style.Visible = false;
            // 
            // tableFailureDetails
            // 
            tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(2.0000009536743164D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(2.0000009536743164D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.0833334922790527D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.0000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFailureDetails.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.28125020861625671D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableFailureDetails.Body.SetCellContent(0, 0, textBox13);
            tableFailureDetails.Body.SetCellContent(0, 1, textBox14);
            tableFailureDetails.Body.SetCellContent(0, 2, textBox15);
            tableFailureDetails.Body.SetCellContent(0, 3, textBox17);
            tableFailureDetails.Body.SetCellContent(0, 4, textBox19);
            tableGroup1.ReportItem = textBox10;
            tableGroup2.ReportItem = textBox11;
            tableGroup3.ReportItem = textBox12;
            tableGroup4.Name = "Group1";
            tableGroup4.ReportItem = textBox16;
            tableGroup5.Name = "Group2";
            tableGroup5.ReportItem = textBox18;
            tableFailureDetails.ColumnGroups.Add(tableGroup1);
            tableFailureDetails.ColumnGroups.Add(tableGroup2);
            tableFailureDetails.ColumnGroups.Add(tableGroup3);
            tableFailureDetails.ColumnGroups.Add(tableGroup4);
            tableFailureDetails.ColumnGroups.Add(tableGroup5);
            tableFailureDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBox13,
            textBox14,
            textBox15,
            textBox17,
            textBox19,
            textBox10,
            textBox11,
            textBox12,
            textBox16,
            textBox18});
            tableFailureDetails.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.15625D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20011822879314423D, Telerik.Reporting.Drawing.UnitType.Inch));
            tableFailureDetails.Name = "tableFailureDetails";
            tableGroup6.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping("")});
            tableGroup6.Name = "DetailGroup";
            tableFailureDetails.RowGroups.Add(tableGroup6);
            tableFailureDetails.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.0833353996276855D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.54166686534881592D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox13
            // 
            textBox13.Name = "textBox13";
            textBox13.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.28125017881393433D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox13.Value = "=Fields.Id";
            // 
            // textBox14
            // 
            textBox14.Name = "textBox14";
            textBox14.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.28125017881393433D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox14.Value = "=Fields.FailItem";
            // 
            // textBox15
            // 
            textBox15.Name = "textBox15";
            textBox15.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0833333730697632D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.28125020861625671D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox15.Value = "=Fields.Value";
            // 
            // textBox17
            // 
            textBox17.Name = "textBox17";
            textBox17.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.28125020861625671D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox17.StyleName = "";
            textBox17.Value = "=Fields.AcceptanceRangeFrom";
            // 
            // textBox19
            // 
            textBox19.Name = "textBox19";
            textBox19.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.28125020861625671D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox19.StyleName = "";
            textBox19.Value = "=Fields.AcceptanceRangeTo";
            // 
            // textBoxAcceptanceRangeLabel
            // 
            textBoxAcceptanceRangeLabel.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.3000006675720215D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.1000000610947609D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxAcceptanceRangeLabel.Name = "textBoxAcceptanceRangeLabel";
            textBoxAcceptanceRangeLabel.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.0000007152557373D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.19999997317790985D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxAcceptanceRangeLabel.Style.Font.Bold = true;
            textBoxAcceptanceRangeLabel.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxAcceptanceRangeLabel.Value = "Acceptance Range";
            // 
            // textBoxFailureDetailsLabel
            // 
            textBoxFailureDetailsLabel.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxFailureDetailsLabel.Name = "textBoxFailureDetailsLabel";
            textBoxFailureDetailsLabel.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.95833325386047363D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000012218952179D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxFailureDetailsLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            textBoxFailureDetailsLabel.Style.Visible = true;
            textBoxFailureDetailsLabel.Value = "Failure Details";
            // 
            // tableStandardsSamples
            // 
            tableStandardsSamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.5446571111679077D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableStandardsSamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.0180695056915283D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableStandardsSamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.4971612691879273D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableStandardsSamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(3.9401123523712158D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableStandardsSamples.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.30000004172325134D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableStandardsSamples.Body.SetCellContent(0, 0, textBox5);
            tableStandardsSamples.Body.SetCellContent(0, 1, textBox6);
            tableStandardsSamples.Body.SetCellContent(0, 3, textBox7);
            tableStandardsSamples.Body.SetCellContent(0, 2, textBox9);
            tableGroup7.ReportItem = textBox2;
            tableGroup8.ReportItem = textBox3;
            tableGroup9.Name = "Group1";
            tableGroup9.ReportItem = textBox8;
            tableGroup10.ReportItem = textBox4;
            tableStandardsSamples.ColumnGroups.Add(tableGroup7);
            tableStandardsSamples.ColumnGroups.Add(tableGroup8);
            tableStandardsSamples.ColumnGroups.Add(tableGroup9);
            tableStandardsSamples.ColumnGroups.Add(tableGroup10);
            tableStandardsSamples.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBox5,
            textBox6,
            textBox7,
            textBox9,
            textBox2,
            textBox3,
            textBox8,
            textBox4});
            tableStandardsSamples.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.84791690111160278D, Telerik.Reporting.Drawing.UnitType.Inch));
            tableStandardsSamples.Name = "tableStandardsSamples";
            tableGroup11.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping("")});
            tableGroup11.Name = "DetailGroup";
            tableStandardsSamples.RowGroups.Add(tableGroup11);
            tableStandardsSamples.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.60000008344650269D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox5
            // 
            textBox5.Name = "textBox5";
            textBox5.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.5446571111679077D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.30000004172325134D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox5.Value = "=Fields.SampleName";
            // 
            // textBox6
            // 
            textBox6.Name = "textBox6";
            textBox6.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.0180695056915283D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.30000004172325134D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox6.Value = "=Fields.Rate";
            // 
            // textBox7
            // 
            textBox7.Name = "textBox7";
            textBox7.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.9401123523712158D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.30000004172325134D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            textBox7.Value = "=Fields.Pass";
            // 
            // textBox9
            // 
            textBox9.Format = "{0:N0}";
            textBox9.Name = "textBox9";
            textBox9.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.4971612691879273D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.30000004172325134D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox9.StyleName = "";
            textBox9.Value = "=Fields.ActualCounts";
            // 
            // shape12
            // 
            shape12.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.31666693091392517D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape12.Name = "shape12";
            shape12.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            shape12.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(8.0999212265014648D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.079166412353515625D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBoxStandardsPassFail
            // 
            textBoxStandardsPassFail.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7.25D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.11875025182962418D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxStandardsPassFail.Name = "textBoxStandardsPassFail";
            textBoxStandardsPassFail.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.7999998927116394D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxStandardsPassFail.Style.Font.Bold = true;
            textBoxStandardsPassFail.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxStandardsPassFail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            textBoxStandardsPassFail.Value = "";
            // 
            // textBox1
            // 
            textBox1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.0416666679084301D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.11875025182962418D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox1.Name = "textBox1";
            textBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.7000001668930054D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox1.Style.Font.Bold = true;
            textBox1.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBox1.Value = "Standards";
            // 
            // textBoxStandardsAcceptanceCriteria
            // 
            textBoxStandardsAcceptanceCriteria.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.40000024437904358D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxStandardsAcceptanceCriteria.Name = "textBoxStandardsAcceptanceCriteria";
            textBoxStandardsAcceptanceCriteria.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(8.0999212265014648D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.44783797860145569D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxStandardsAcceptanceCriteria.Style.Font.Name = "Times New Roman";
            textBoxStandardsAcceptanceCriteria.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxStandardsAcceptanceCriteria.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxStandardsAcceptanceCriteria.Value = resources.GetString("textBoxStandardsAcceptanceCriteria.Value");
            // 
            // pageFooterSection1
            // 
            pageFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(0.1000000610947609D, Telerik.Reporting.Drawing.UnitType.Inch);
            pageFooterSection1.Name = "pageFooterSection1";
            // 
            // StandardsReport
            // 
            Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            pageHeaderSection1,
            detail,
            pageFooterSection1});
            PageSettings.Landscape = false;
            PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(0.25D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            Style.BackgroundColor = System.Drawing.Color.White;
            Width = new Telerik.Reporting.Drawing.Unit(8.0999212265014648D, Telerik.Reporting.Drawing.UnitType.Inch);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        public Telerik.Reporting.TextBox textBoxCountTime;
        public Telerik.Reporting.Panel panelFailureDetails;
        public Telerik.Reporting.Table tableFailureDetails;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox18;
        public Telerik.Reporting.TextBox textBoxAcceptanceRangeLabel;
        public Telerik.Reporting.TextBox textBoxFailureDetailsLabel;
        public Telerik.Reporting.Table tableStandardsSamples;
        public Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.Shape shape12;
        private Telerik.Reporting.TextBox textBox1;
        public Telerik.Reporting.TextBox textBoxStandardsAcceptanceCriteria;
        public Telerik.Reporting.TextBox textBoxStandardsPassFail;
    }
}