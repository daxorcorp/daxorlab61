﻿using System;
using System.Configuration;
using System.Windows;
using System.Windows.Controls;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Infrastructure.Controls;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.RegionAdapters;
using Daxor.Lab.Infrastructure.Services;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.MessageBox;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Composite.Presentation.Regions;
using Microsoft.Practices.Composite.UnityExtensions;
using Microsoft.Practices.Unity;
using QCTestArchive.Views;

namespace QCTestArchive
{
    /// <summary>
    /// Bootstrapper for Daxor.Labs
    /// 
    /// *** Bootstrapper steps ***
    /// 1. Create a LoggerFacade
    /// 2. Create and configure a module catalog
    /// 3. Create and configure the container
    /// 4. Configure default region adapter mappings
    /// 5. Configure default region behaviors
    /// 6. Register framework exception types
    /// 7. Create the shell
    /// 8. Initialize the shell
    /// 9. Initialize the modules
    /// </summary>
    internal class DaxorLabBootstrapper : UnityBootstrapper
    {
        public ShellView Shell { get; private set; }

        protected override DependencyObject CreateShell()
        {
            Shell = Container.Resolve<ShellView>();

            //Initializes custom dispatcher to use throughout the application
            IdealDispatcher.Initialize(Shell.Dispatcher);

            Shell.Show();
            return Shell;
        }
        protected override IModuleCatalog GetModuleCatalog()
        {
            return new ConfigurationModuleCatalog();
        }
        protected override RegionAdapterMappings ConfigureRegionAdapterMappings()
        {
            // Create default region adapters for ContentControl, Selector, ItemsControl
            RegionAdapterMappings mappings = base.ConfigureRegionAdapterMappings();

            //mappings.RegisterMapping(typeof(ModuleSwitcher), Container.Resolve<ModuleSwitcherRegionAdapter>());
            mappings.RegisterMapping(typeof(ViewSwitcher), Container.Resolve<ViewSwitcherRegionAdapter>());
            mappings.RegisterMapping(typeof(Grid), Container.Resolve<GridRegionAdapter>());

            return mappings;
        }
        protected override IUnityContainer CreateContainer()
        {
            //Create a container
            ((App)Application.Current).Container = base.CreateContainer();

            return ((App)Application.Current).Container;
        }
        protected override void ConfigureContainer()
        {
            //Base registration will not override custom shared services
            base.ConfigureContainer();

            //Registers core application services
            //SystemSettingsManager.ShouldCleanUpRunningTestsOnInitialization = false;
            RegisterCoreServices();
        }
        protected override void InitializeModules()
        {
            try
            {
                base.InitializeModules();
                Container.Resolve<IEventAggregator>();
            }
            catch (Exception ex)
            {
                Container.Resolve<ILoggerFacade>().Log("QCTestArchive: Module Initialization Failed: "+ ex.Message, 
                    Category.Exception, Priority.High);
                throw;
            }
           
        }

        private void RegisterCoreServices()
        {
            Container.RegisterType<IMessageBoxUiRegistrar, MessageBoxUIRegistrar>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IMessageBoxDispatcher, XamlMessageBoxDispatcher>(new ContainerControlledLifetimeManager());
            
            //Register shared INavigationCoordinator use to navigate between AppModules (singleton)
            Container.RegisterType<INavigationCoordinator, AppModuleNavigationCoordinator>(new ContainerControlledLifetimeManager());

            //Register logger
            Container.RegisterType<ILoggerFacade, CustomFileLogger>(new ContainerControlledLifetimeManager(),
             new InjectionConstructor(ConfigurationManager.AppSettings["LogPath"]));

            Container.RegisterType<ISettingsManagerDataService, SettingsManagerDataService>(new ContainerControlledLifetimeManager());
            Container.RegisterInstance(typeof(ISettingsManager), new SystemSettingsManager(Container.Resolve<ILoggerFacade>(),
                    Container.Resolve<ISettingsManagerDataService>()), new ContainerControlledLifetimeManager());
        }
    }
}
