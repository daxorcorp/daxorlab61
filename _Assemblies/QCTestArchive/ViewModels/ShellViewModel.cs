﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using QCTestArchive.Common;
using QCTestArchive.Database;
using QCTestArchive.Report;
using Telerik.Reporting;
using Telerik.Reporting.Processing;
using Telerik.Windows.Data;

namespace QCTestArchive.ViewModels
{
    public class ShellViewModel : ViewModelBase
    {
        #region Fields
        private readonly ILoggerFacade _customLoggerFacade;
        private readonly IRegionManager _regionManager;
        private readonly IUnityContainer _container;
        private readonly ISettingsManager _settingsManager;
        private BackgroundTaskManager<QueryableCollectionView> _bkgTestsFetcher = null;
        private string _testsInformation;
        
        private QueryableCollectionView _qcTestInfos;
        private bool _isInitialDataLoading;
        private QCTestInfoDTO _selectedTestInfo;
        private readonly DelegateCommand<object> _printQCTestListCommand;
        private QCTestListReport _qcTestListReport;

        private bool _isBusy;

        #endregion

        #region CTOR
        public ShellViewModel(ILoggerFacade customLoggerFacade, IRegionManager regionManager, IUnityContainer container, ISettingsManager settingsManager)
        {
            _customLoggerFacade = customLoggerFacade;
            _regionManager = regionManager;
            _container = container;
            _settingsManager = settingsManager;
            _printQCTestListCommand = new DelegateCommand<object>(PrintQCListReportCommandExecute, PrintQCListReportCommandCanExecute);

            IsBusy = false;
            ConfigureViewModel();
        }
        #endregion
        
        #region Properties

        public int EndingTestItemIndex { get; set; }
        public int BeginningTestItemIndex { get; set; }

        public bool IsDatabaseEmpty
        {
            get
            {
                if (QCTestInfos != null)
                    return QCTestInfos.ItemCount > 0;
                return false;
            }
        }

        public QueryableCollectionView QCTestInfos
        {
            get { return _qcTestInfos; }
            set
            {
                _qcTestInfos = value;
                FirePropertyChanged("QCTestInfos");
                ((DelegateCommand<object>)PrintQCListReportCommand).RaiseCanExecuteChanged();
            }
        }

        public bool IsInitialDataLoading
        {
            get { return _isInitialDataLoading; }
            set
            {
                _isInitialDataLoading = value;
            }
        }

        public QCTestInfoDTO SelectedTestInfo
        {
            get { return _selectedTestInfo; }
            set 
            {
                _selectedTestInfo = value;
                ViewQCReportCommand.RaiseCanExecuteChanged();
                FirePropertyChanged("SelectedTestInfo");
            }
        }
        public string TestsInformation
        {
            get { return _testsInformation; }
            set { base.SetValue<String>(ref _testsInformation, "TestsInformation", value); }
        }

        public bool IsBusy
        {
            get { return _isBusy; }
            set { base.SetValue<bool>(ref _isBusy, "IsBusy", value); }
        }

        #endregion

        #region Commands
        public DelegateCommand<object> ViewQCReportCommand
        {
            get;
            private set;
        }

        private void ExecuteViewQCReportCommand(object arg)
        {
            if (SelectedTestInfo != null)
            {
                QCTestDTO test = GetQCTest(SelectedTestInfo.TestID);

                if (test != null)
                {
                    IsBusy = true;
                    Task.Factory.StartNew(() =>
                        {
                            QCTestReportController controller = new QCTestReportController(_regionManager, test, _container.Resolve<IEventAggregator>(), _settingsManager);
                            IdealDispatcher.BeginInvoke(() =>
                            {
                                controller.ShowReport();
                                IsBusy = false;
                            });
                        }
                    );
                }
            }

        }

        public DelegateCommand<object> CloseCommand
        {
            get;
            private set;
        }

        private void ExecuteCloseCommand(object arg)
        {
            Application.Current.MainWindow.Close();
        }

        public ICommand PrintQCListReportCommand { get { return _printQCTestListCommand; } }
        void PrintQCListReportCommandExecute(object obj)
        {
            Task.Factory.StartNew(() =>
             {
                 IsBusy = true;

                 _qcTestListReport = new QCTestListReport();
                 
                 LoadHeaderData();

                 var zeroBasedBeginningIndex = BeginningTestItemIndex - 1;
                 var qcTestListItemsList = QCTestInfos.Cast<QCTestInfoDTO>().Skip(zeroBasedBeginningIndex).Take(EndingTestItemIndex - zeroBasedBeginningIndex).ToList<QCTestInfoDTO>();
                 _qcTestListReport.table1.DataSource = qcTestListItemsList;

                 PrintController standardPrintController = new StandardPrintController();
                 PrinterSettings settings = new PrinterSettings();

                 ReportProcessor proc = new ReportProcessor();
                 proc.PrintController = standardPrintController;
                 proc.PrintReport(new InstanceReportSource {ReportDocument = _qcTestListReport}, settings);

             }).ContinueWith(x => IsBusy = false, TaskScheduler.FromCurrentSynchronizationContext());
        }

        bool PrintQCListReportCommandCanExecute(object obj)
        {
            if(QCTestInfos != null)
                return QCTestInfos.ItemCount > 0;
            return false;
        }
        #endregion

        #region Helpers
        private void ConfigureViewModel()
        {
            Globals.ConnectionString = "Data Source=" + _settingsManager.GetSetting<string>(SettingKeys.SystemDatabaseServer) +
           ";Initial Catalog=QCDatabaseArchive;Persist Security Info=True;User ID=" +
            _settingsManager.GetSetting<string>(SettingKeys.SystemDatabaseUsername) +
           ";Password=daxor";

            ConfigureCommands();
            SetupBackgroundWorkers();

            IsBusy = true;
            _bkgTestsFetcher.RunBackgroundTask();

        }
        
        private void SetupBackgroundWorkers()
        {
            //Setting up background tests fetcher; fetches all BVA test from the database
            _bkgTestsFetcher = new BackgroundTaskManager<QueryableCollectionView>(

                () =>
                {
                    IsInitialDataLoading = true;
                    Thread.Sleep(5000);
                    return new QueryableCollectionView(GetQCTestList().ToList<QCTestInfoDTO>());
                },

                (result) =>
                {
                    QCTestInfos = result;
                    TestsInformation = QCTestInfos.ItemCount == 0 ? "Database Is Empty" : String.Empty;
                    IsInitialDataLoading = false;
                    IsBusy = false;
                });
        }

        private IEnumerable<QCTestInfoDTO> GetQCTestList()
        {
            QCArchiveDataContext context = new QCArchiveDataContext(Globals.ConnectionString);

            IEnumerable<GetQCTestListResult> result = context.GetQCTestList();

            foreach (GetQCTestListResult record in result)
            {
                yield return new QCTestInfoDTO(record);
            }

            yield break;

        }

        private IEnumerable<QCSampleDTO> GetQCTestSamples(int testId)
        {
            QCArchiveDataContext context = new QCArchiveDataContext(Globals.ConnectionString);

            IEnumerable<GetQCTestSamplesResult> result = context.GetQCTestSamples(testId);

            foreach (GetQCTestSamplesResult record in result)
            {
                yield return new QCSampleDTO(record);
            }

            yield break;

        }

        private void ConfigureCommands()
        {
            ViewQCReportCommand = new DelegateCommand<object>(ExecuteViewQCReportCommand, (o) => { return _selectedTestInfo != null; });
            CloseCommand = new DelegateCommand<object>(ExecuteCloseCommand, (o) => { return true; });
        }

        private QCTestDTO GetQCTest(int testId)
        {
            QCArchiveDataContext context = new QCArchiveDataContext(Globals.ConnectionString);

            var result = context.GetQCTest(testId);

            var test = result.FirstOrDefault<GetQCTestResult>();

            if (test != null)
            {
                QCTestDTO dto = new QCTestDTO(test);
                var samples = GetQCTestSamples(dto.TestId);
                foreach (var x in samples)
                {
                    dto.Samples.Add(x);
                }
                return dto;
            }

            return null;

        }

        private void LoadHeaderData()
        {
            _qcTestListReport.textBoxBVAVersion.Value = _settingsManager.GetSetting<string>(SettingKeys.BvaModuleVersion);
            _qcTestListReport.textBoxUnitID.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemUniqueSystemId);
            _qcTestListReport.textBoxDepartment.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentName);
            _qcTestListReport.textBoxDepartmentAddress.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentAddress);
            _qcTestListReport.textBoxDepartmentDirector.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentDirector);
            _qcTestListReport.textBoxDepartmentPhoneNumber.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentPhoneNumber);
            _qcTestListReport.textBoxHospitalName.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemHospitalName);
            _qcTestListReport.textBoxPrintedOn.Value = "Printed: " + DateTime.Now.ToString("g");
        }

        #endregion

    }
}
