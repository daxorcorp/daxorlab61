﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using QCTestArchive.ViewModels;

namespace QCTestArchive.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ShellView : Window
    {
        private ScrollViewer scrollViewer;

        public ShellView()
        {
            InitializeComponent();
        }

        public ShellView(ShellViewModel vm) : this()
        {
            DataContext = vm;
        }

        void uiTopButton_Click(object sender, RoutedEventArgs e)
        {
            scrollViewer.ScrollToTop();
        }
        
        void uiPageUpButton_Click(object sender, RoutedEventArgs e)
        {
            scrollViewer.PageUp();

        }
        
        void uiPageDownButton_Click(object sender, RoutedEventArgs e)
        {
            scrollViewer.PageDown();
        }
        
        void uiBottomButton_Click(object sender, RoutedEventArgs e)
        {
            scrollViewer.ScrollToBottom();

        }

        void CalculatePageInfo()
        {
            if (scrollViewer == null) return;

            double itemHeight = scrollViewer.ExtentHeight / (double)MainTelerikGrid.Items.Count;
            double pageHeight = scrollViewer.ViewportHeight;
            int endingIndex;

            uiStaringIndexTextBlock.Text = ((int)Math.Round(scrollViewer.VerticalOffset / itemHeight) + 1).ToString();
            endingIndex = ((int)Math.Round((scrollViewer.VerticalOffset + pageHeight) / itemHeight));
            if (endingIndex > MainTelerikGrid.Items.Count)
            {
                uiEndingIndexTextBlock.Text = MainTelerikGrid.Items.Count.ToString();
            }
            else
            {
                uiEndingIndexTextBlock.Text = ((int)Math.Round((scrollViewer.VerticalOffset + pageHeight) / itemHeight)).ToString();
            }

            uiTotalItems.Text = MainTelerikGrid.Items.Count.ToString();
            //   uiTotalSearchItems.Text = MainTelerikGrid.Items.Count.ToString();
            //   uiTotalOveralItems.Text = MainTelerikGrid.Items.TotalItemCount.ToString();

            if (MainTelerikGrid.Items.Count == 0)
            {
                uiEndingIndexTextBlock.Text = "0";
                uiStaringIndexTextBlock.Text = "0";
                //     uiTotalSearchItems.Text = "0";
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            scrollViewer = (ScrollViewer)MainTelerikGrid.Template.FindName("PART_ItemsScrollViewer", MainTelerikGrid);
            if (scrollViewer == null) throw new NullReferenceException("ItemsScrollViewer is not present in VisualTree");

            CalculatePageInfo();
            scrollViewer.ScrollChanged += (s, e2) => { CalculatePageInfo(); };
        }

    }
}
