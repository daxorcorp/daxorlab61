﻿using System;
using Daxor.Lab.ApplicationServices.Interfaces;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Domain.Services;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.ApplicationServices.Tests.Module_Tests.SadPath
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_the_Application_Services_module
	{
		private Module _module;
		private ILoggerFacade _logger;
		private ISampleChanger _sampleChanger;
		private ISettingsManager _settingsManager;
		private IUnityContainer _container;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_container = Substitute.For<IUnityContainer>();
			_logger = Substitute.For<ILoggerFacade>();
			_sampleChanger = Substitute.For<ISampleChanger>();
			_settingsManager = Substitute.For<ISettingsManager>();

			_sampleChanger.IsPoweredUp().Returns(true);
			_container.Resolve<ISampleChanger>().Returns(_sampleChanger);
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_calibration_curve_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterInstance<ICalibrationCurve>(Arg.Any<CalibrationCurve>())).Throw(new Exception());
			_module = new Module(_container, _logger, _settingsManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Application Services", ex.ModuleName);
				Assert.AreEqual("initialize the Calibration Curve", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_spectroscopy_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<ISpectroscopyService, SpectroscopyService>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container, _logger, _settingsManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Application Services", ex.ModuleName);
				Assert.AreEqual("initialize the Spectroscopy Service", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_execution_time_helper_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IExecutionTimeHelper, ExecutionTimeHelper>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container, _logger, _settingsManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Application Services", ex.ModuleName);
				Assert.AreEqual("initialize the Execution Time Helper", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_background_acquisition_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IBackgroundAcquisitionService, BackgroundAcquisitionService>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container, _logger, _settingsManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Application Services", ex.ModuleName);
				Assert.AreEqual("initialize the Background Acquisition Service", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_starting_the_background_acquisition_service_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<IBackgroundAcquisitionService>()).Throw(new Exception());
			_module = new Module(_container, _logger, _settingsManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Application Services", ex.ModuleName);
				Assert.AreEqual("start the Background Acquisition Service", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_trying_to_register_and_initialize_the_aggregate_event_publisher_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IHardwareAggregateEventPublisher, ClrToAggregateEventPublisher>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container, _logger, _settingsManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Application Services", ex.ModuleName);
				Assert.AreEqual("initialize the Aggregate Event Publisher", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_trying_to_register_the_test_execution_controller_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<ITestExecutionController, TestExecutionController>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container, _logger, _settingsManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Application Services", ex.ModuleName);
				Assert.AreEqual("initialize the Test Execution Controller", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_when_trying_to_register_the_authentication_manager_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IAuthenticationManager, AuthenticationManager>(Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container, _logger, _settingsManager);

			try
			{
				_module.Initialize();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Application Services", ex.ModuleName);
				Assert.AreEqual("initialize the Authentication Manager", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}
	}
	// ReSharper restore InconsistentNaming
}