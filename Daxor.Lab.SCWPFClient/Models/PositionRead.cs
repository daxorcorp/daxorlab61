﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.SCWPFClient.Models
{
    public class PositionRead
    {
        public Int32 Position { get; private set; }
        public DateTime DetectedTime { get; private set; }
        
        public PositionRead(Int32 pos, DateTime detectedAt)
        {
            Position = pos;
            DetectedTime = detectedAt;
        }
    }
}
