﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.SCWPFClient.Common;

namespace Daxor.Lab.SCWPFClient.Models
{
    /// <summary>
    /// Represents parallel port reading. We are using 5 pins to identify the position of sample changer.
    /// 2^5 = 32 positions - 1 = 31 positions (0 is not valid)
    /// In Binary pins are positions as follows
    /// Pin:        11 10 12 13 15
    /// Decimal:    16  8  4  2  1
    /// 
    /// Example:
    /// Pin:        11 10 12 13 15
    /// Decimal:    16  8  4  2  1
    /// Binary:      0  1  0  1  1
    /// Position: 11
    /// </summary>
    public class ParallelPortReading : ObservableObject
    {
        #region Fields
        
        private Boolean _pin10IsOn;      
        private Boolean _pin11IsOn;
        private Boolean _pin12IsOn;
        private Boolean _pin13IsOn;
        private Boolean _pin15IsOn;
        private Int32 _decimalPosition;

        #endregion

        public Boolean Pin10IsOn
        {
            get { return _pin10IsOn; }
            set { base.SetValue(ref _pin10IsOn, "Pin10IsOn", value); }
        }
        public Boolean Pin11IsOn
        {
            get { return _pin11IsOn; }
            set { base.SetValue(ref _pin11IsOn, "Pin11IsOn", value); }
        }
        public Boolean Pin12IsOn
        {
            get { return _pin12IsOn; }
            set { base.SetValue(ref _pin12IsOn, "Pin12IsOn", value); }
        }
        public Boolean Pin13IsOn
        {
            get { return _pin13IsOn; }
            set { base.SetValue(ref _pin13IsOn, "Pin13IsOn", value); }
        }
        public Boolean Pin15IsOn
        {
            get { return _pin15IsOn; }
            set { base.SetValue(ref _pin15IsOn, "Pin15IsOn", value); }
        }

        public Int32 DecimalPosition
        {
            get { return _decimalPosition; }
            set { base.SetValue(ref _decimalPosition, "DecimalPosition", value); }
        }
    }
}
