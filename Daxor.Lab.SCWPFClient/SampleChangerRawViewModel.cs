﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Daxor.Lab.SCWPFClient.Common;
using System.Windows.Input;
using Daxor.Lab.SCWPFClient.Models;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.Windows.Threading;

namespace Daxor.Lab.SCWPFClient
{
    /// <summary>
    /// The sample changer raw view model.
    /// </summary>
    public class SampleChangerRawViewModel : ObservableObject
    {
        #region Fields

        private readonly DispatcherTimer _timer;
        private readonly SimpleCommand _startChangerCommand;            
        private readonly SimpleCommand _stopChangerCommand;
        private readonly SimpleCommand _unhookFromChangerCommand;            

        private readonly SampleChangerCallback _changerObserver = new SampleChangerCallback();
        private readonly DSMChanger.SampleChangerServiceClient _changer;
        private readonly ObservableCollection<ParallelPortReading> _portReadings = new ObservableCollection<ParallelPortReading>();
        private Int32 _currentPosition;
        
        #endregion

        public SampleChangerRawViewModel()
        {
            #region Instantiate Commands

            _startChangerCommand = new SimpleCommand()
            {
                CanExecuteDelegate = StartChangerCommandCanExecute,
                ExecuteDelegate = StartChangerCommandExecute
            };

            _stopChangerCommand = new SimpleCommand()
            {
                CanExecuteDelegate = StopChangerCommandCanExecute,
                ExecuteDelegate = StopChangerCommandExecute
            };
            _unhookFromChangerCommand = new SimpleCommand()
            {
                CanExecuteDelegate = (s)=> true,
                ExecuteDelegate = UnhookFromSampleChangerCommandExecute
            };
            #endregion

            #region Observing Changer

            _changerObserver.PositionChangeBegin += _changerObserver_PositionChangeBegin;
            _changerObserver.PositionChangeEnd += _changerObserver_PositionChangeEnd;
            _changerObserver.PositionSeekBegin += _changerObserver_PositionSeekBegin;
            _changerObserver.PositionSeekEnd += _changerObserver_PositionSeekEnd;
            _changerObserver.PositionSeekCancel += _changerObserver_PositionSeekCancel;
            _changerObserver.ChangerError += _changerObserver_ChangerError;

            #endregion

            #region SampleChangerProxy

            InstanceContext ctx = new InstanceContext(_changerObserver);
            _changer = new DSMChanger.SampleChangerServiceClient(ctx, "dsmcNetTcp");
            _changer.Subscribe();
            _currentPosition = _changer.GetCurrentPosition();

            #endregion

            #region Instantiate Timer

            _timer = new DispatcherTimer();
            _timer.Interval = new TimeSpan(0, 0, 0, 0, 2000);
            _timer.Tick += new EventHandler(_runningTimer_Tick);

            #endregion
        }

        #region Helpers

        void _runningTimer_Tick(object sender, EventArgs e)
        {
            _timer.Stop();
            CommandManager.InvalidateRequerySuggested();

            DSMChanger.SensorData sData = _changer.GetRawSensorData();
            CurrentPosition = sData.PositionInDecimal;
            PortReadings.Add(new ParallelPortReading()
            {
               Pin10IsOn = sData.Pin10x8,
               Pin11IsOn = sData.Pin11x16,
               Pin12IsOn = sData.Pin12x4,
               Pin13IsOn = sData.Pin13x2,
               Pin15IsOn = sData.Pin15x1,
               DecimalPosition = sData.PositionInDecimal,
            });

            if (_changer.IsMoving())
                _timer.Start();
        }

        #endregion

        #region Properties

        public Int32 CurrentPosition
        {
            get { return _currentPosition; }
            set { base.SetValue(ref _currentPosition, "CurrentPosition", value); }
        }
        public ObservableCollection<ParallelPortReading> PortReadings
        {
            get { return _portReadings; }
        }

        #endregion

        #region Commands

        public ICommand StartChangerCommand
        {
            get { return _startChangerCommand; }
        }
        public ICommand StopChangerCommand
        {
            get { return _stopChangerCommand; }
        }
        public ICommand UnhookFromChangerCommand
        {
            get { return _unhookFromChangerCommand; }
        }

        void UnhookFromSampleChangerCommandExecute(object arg)
        {
            StopChangerCommand.Execute(arg);
            _changer.Unsubscribe();
        }

        bool StartChangerCommandCanExecute(object arg)
        {
            return _changer != null && !_changer.IsMoving();
        }
        void StartChangerCommandExecute(object arg)
        {
            if (CurrentPosition == 1)
            {
                _changer.MoveToPosition(25);
            }
            else
                _changer.MoveToPosition((CurrentPosition + 24) % 25);

            _timer.Start();


        }

        bool StopChangerCommandCanExecute(object arg)
        {
            return _changer != null && _changer.IsMoving();
        }
        void StopChangerCommandExecute(object arg)
        {
            _changer.StopSampleChanger();
            _timer.Stop();
        }

        #endregion

        #region Changer Handlers

        void _changerObserver_ChangerError(int errorCode, string message)
        {
            CommandManager.InvalidateRequerySuggested();
        }
        void _changerObserver_PositionSeekCancel(int currPosition, int seekPosition)
        {
            CommandManager.InvalidateRequerySuggested();
        }
        void _changerObserver_PositionSeekEnd(int currPosition)
        {
            _changer.MoveToPosition((currPosition + 24) % 25);
            CommandManager.InvalidateRequerySuggested();
        }
        void _changerObserver_PositionSeekBegin(int currPosition, int seekPosition)
        {
            CommandManager.InvalidateRequerySuggested();
        }
        void _changerObserver_PositionChangeEnd(int newPosition)
        {
            
        }
        void _changerObserver_PositionChangeBegin(int oldPosition, int newPosition)
        {
            
        }

        #endregion
    }
}
