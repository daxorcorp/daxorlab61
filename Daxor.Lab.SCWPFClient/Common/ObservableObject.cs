﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;

namespace Daxor.Lab.SCWPFClient.Common
{
    /// <summary>
    /// Base class for all Model classes in the application.
    /// It provides support for property change notifications.  
    /// This class is abstract.
    /// </summary>
    public abstract class ObservableObject : INotifyPropertyChanged, IDisposable
    {
        #region Ctor

        protected ObservableObject()
        {
        }

        #endregion


        /// <summary>
        /// Sets new value for a given field
        /// </summary>
        /// <typeparam name="T">Type of the field</typeparam>
        /// <param name="propField">Backing field of property</param>
        /// <param name="propName">Property name</param>
        /// <param name="newValue">New Value</param>
        /// <returns>Returns true if the value has been changed; false otherwise</returns>
        protected virtual bool SetValue<T>(ref T propField, string propName, T newValue)
        {
            if (propField != null && propField.Equals(newValue)) return false;

            propField = newValue;
            FirePropertyChanged(propName);
            return true;
        }

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Returns whether an exception is thrown, or if a Debug.Fail() is used
        /// when an invalid property name is passed to the VerifyPropertyName method.
        /// The default value is false, but subclasses used by unit tests might 
        /// override this property's getter to return true.
        /// </summary>
        protected virtual bool ThrowOnInvalidPropertyName { get; private set; }

        #region DEBUGGIN AIDES
        
        /// <summary>
        /// Warns the developer if this object does not have
        /// a public property with the specified name. This 
        /// method does not exist in a Release build.
        /// </summary>
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real,  
            // public, instance property on this object.
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                string msg = "Invalid property name: " + propertyName;

                if (this.ThrowOnInvalidPropertyName)
                {
                    throw new Exception(msg);
                }
                else
                {
                    Debug.Fail(msg);
                }
            }
        }
       
        #endregion // Debugging Aides

        /// <summary>
        /// Invoked when this object is being removed from the application
        /// and will be subject to garbage collection.
        /// </summary>
        public void Dispose()
        {
            this.OnDispose();
        }

        #region INotifyPropertyChanged Members
        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected virtual void FirePropertyChanged(string propertyName)
        {
            this.VerifyPropertyName(propertyName);
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        protected virtual void FirePropertyChanged(params string[] propertyNames)
        {
            if (propertyNames == null)
                throw new ArgumentNullException("propertyNames");
            if (propertyNames.Length == 0)
                throw new ArgumentOutOfRangeException("propertyNames", "propertyNames should contain at least one property");

            foreach (var pName in propertyNames)
                this.FirePropertyChanged(pName);
        }

        #endregion // INotifyPropertyChanged Members

        /// <summary>
        /// Child classes can override this method to perform 
        /// clean-up logic, such as removing event handlers.
        /// </summary>
        protected virtual void OnDispose(){}
    }
}
