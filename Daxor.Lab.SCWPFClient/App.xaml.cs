﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace Daxor.Lab.SCWPFClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        SampleChangerRawViewModel vm = null;

        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
                base.OnStartup(e);
                vm = new SampleChangerRawViewModel();
                SampleChangerRawView view = new SampleChangerRawView(vm);
                view.Show();

            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message+ex.StackTrace);            	
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            vm.UnhookFromChangerCommand.Execute(null);
        }
    }
}
