﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Daxor.Lab.BVA.Reports;
using Daxor.Lab.BVA.Reports.Interfaces;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.BVA.SubViews;
using Daxor.Lab.Database.CommonBLLs;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Database.LINQ;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.RuleFramework.Rules;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.BVA
{
    public class Module : AppModule
    {
        #region Constants
        private const string BvaRuleIdPrefix = "BVA_";
	    private const string ModuleName = "BVA";
        #endregion

        #region Ctor
        public Module(IUnityContainer container) : base(AppModuleIds.BloodVolumeAnalysis, container)
        {
            ShortDisplayName = "BVA\nTest";
            FullDisplayName = "Blood Volume Analysis";
            DisplayOrder = 1;
            IsConfigurable = true;
        }
        #endregion

        public override void InitializeAppModule()
        {
            //Initialize local resource dictionary
	        try
	        {
		        AddResourceDictionaryToMergedDictionaries();
	        }
	        catch
	        {
				throw new ModuleLoadException(ModuleName, "add the resource dictionary");
	        }

	        // Data access registration
	        try
	        {
		        RegisterAuditTrailDataAccess();
	        }
	        catch
	        {
		        throw new ModuleLoadException(ModuleName, "initialize the Audit Trail Data Access");
	        }

	        // Registering repositories
	        try
	        {
		        RegisterPatientRepository();
	        }
	        catch
	        {
				throw new ModuleLoadException(ModuleName, "initialize the Patient Repository");
	        }

	        try
	        {
		        RegisterSampleRepository();
	        }
	        catch
	        {
				throw new ModuleLoadException(ModuleName, "initialize the Sample Repository");
	        }

	        try
	        {
		        RegisterTestInfoRepository();
	        }
	        catch
	        {
				throw new ModuleLoadException(ModuleName, "initialize the BVA Test Information Repository");
	        }

	        try
	        {
		        RegisterTestRepository();
	        }
	        catch
	        {
				throw new ModuleLoadException(ModuleName, "initialize the BVA Test Repository");
	        }

	        ConfigureModuleAlerts();
	        RegisterModuleScopedPopupController();
	        RegisterBvaResultAlertService();
	        RegisterPdfPrinter();
	        RegisterBvaModuleNavigator();
            InitializeAppModuleNavigator();
            RegisterPopupViewWithName();
        }

        protected virtual void RegisterTestRepository()
	    {
		    UnityContainer.RegisterType(typeof (Infrastructure.Interfaces.IRepository<BvaTestRecord>), typeof (BVATestRepository));
	    }

	    protected virtual void RegisterTestInfoRepository()
	    {
		    UnityContainer.RegisterType(typeof (ITestInfoRepository<BVATestInfoDTO>), typeof (BVATestInfoRepository));
	    }

	    protected virtual void RegisterSampleRepository()
	    {
		    UnityContainer.RegisterType(typeof (ISampleRepository<BvaSampleRecord>), typeof (SampleRepository));
	    }

	    protected virtual void RegisterPatientRepository()
	    {
		    UnityContainer.RegisterType(typeof (Infrastructure.Interfaces.IRepository<>), typeof (PatientRepository));
	    }

	    protected virtual void RegisterAuditTrailDataAccess()
	    {
		    UnityContainer.RegisterType<IAuditTrailDataAccess, AuditTrailDataAccess>(
			    new ContainerControlledLifetimeManager(),
			    new InjectionConstructor(new DaxorLabDatabaseLinqDataContext(DatabaseSettingsHelper.LINQConnectionString)));
	    }

	    protected virtual void AddResourceDictionaryToMergedDictionaries()
	    {
		    Application.Current.Resources.MergedDictionaries
			    .Add(new ResourceDictionary
			    {
				    Source = new Uri(@"pack://application:,,,/Daxor.Lab.BVA;component/Common/Resources.xaml")
			    });
	    }

	    private void ConfigureModuleAlerts()
        {
            try
            {
                // Get all rules that support resolution data
                var metadataRules = from r in UnityContainer.Resolve<IRuleEngine>(UniqueAppId).Rules
                                    where r is IExposeAlertResolutionMetadata
                                    select new KeyValuePair<String, IExposeAlertResolutionMetadata>(r.RuleId, (IExposeAlertResolutionMetadata)r);

                var alertResolutions = SpecificAlertsDataAccess.SelectList(BvaRuleIdPrefix);

                foreach (var r in metadataRules)
                {
                    // ReSharper disable PossibleMultipleEnumeration
                    var alert = (from a in alertResolutions
                                 where r.Key == a.AlertId
                                 select new AlertResolutionMetadata(a.AlertId, a.Description, a.Resolution, a.ResolutionType)).FirstOrDefault();
                    // ReSharper restore PossibleMultipleEnumeration
                    if (alert != null)
                        r.Value.Metadata = alert;
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Failed to load BVA rule metadata. Error: " + ex.Message, Category.Exception, Priority.High);

				throw new ModuleLoadException(ModuleName, "configure the Module Alerts");
            }
        }

        private void RegisterPopupViewWithName()
        {
            // Register possible popup within module popup controller
            try
            {
                UnityContainer.Resolve<IPopupController>(UniqueAppId)
                    .RegisterPopupViewWithName(BVAModuleViewKeys.BloodPointsPopup,
                        () => UnityContainer.Resolve<BloodPointsZoomedSubView>());
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the Popup Controller");
            }
        }

        private void InitializeAppModuleNavigator()
        {
            try
            {
                UnityContainer.Resolve<IAppModuleNavigator>(UniqueAppId).Initialize();
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the App Module Navigator");
            }
        }

        private void RegisterBvaModuleNavigator()
        {
            // Registers local navigator with Unity, followed by view instantiation of Views->ViewModels
            try
            {
                UnityContainer.RegisterType<IAppModuleNavigator, BVAModuleNavigator>(UniqueAppId,
                    new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the BVA Module Navigator");
            }
        }

        private void RegisterPdfPrinter()
        {
            try
            {
                UnityContainer.RegisterType<IPdfPrinter, PdfPrinter>();
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the PDF Printer");
            }
        }

        private void RegisterBvaResultAlertService()
        {
            try
            {
                UnityContainer.RegisterType<IBvaResultAlertService, BvaResultAlertService>(UniqueAppId,
                    new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the BVA Result Alert Service");
            }
        }

        private void RegisterModuleScopedPopupController()
        {
            try
            {
                UnityContainer.RegisterType<IPopupController, ModuleScopedPopupController>(UniqueAppId,
                    new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "register the Popup Controller");
            }
        }
    }
}
