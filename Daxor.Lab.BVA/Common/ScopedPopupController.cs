﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Composite;
using Daxor.Lab.BVA.Common;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using System.Windows;

namespace Daxor.Lab.BVA.Services
{
    public class ModuleScopedPopupController : IPopupController
    {
        private readonly IDictionary<String, Func<Object>> registeredRegionContent
        = new Dictionary<String, Func<Object>>();

        private readonly IRegion _popupRegion;

        public ModuleScopedPopupController([Dependency(ScopedRegionNames.BVAPopupRegion)] IRegion region)
        {
            _popupRegion = region;
            _popupRegion.Behaviors.Add(RegionTransitionAwareBehavior.BehaviorKey, new RegionTransitionAwareBehavior());
        }

        public bool OpenPopup(string name, Object dataContext = null)
        {
            //Close already open popup
            ClosePopup();
            Func<Object> activator = null;

            if (registeredRegionContent.TryGetValue(name, out activator))
            {
                var newView = activator();

                FrameworkElement fElement = newView as FrameworkElement;
                if (fElement != null)
                    fElement.DataContext = dataContext;

                _popupRegion.Add(newView);
                _popupRegion.Activate(newView);

                return true;
            }
            return false;
        }
        public void ClosePopup()
        {
            if (_popupRegion.ActiveViews.Count() == 0)
                return;

            var openView = _popupRegion.ActiveViews.FirstOrDefault();
            if (openView != null)
            {
                _popupRegion.Deactivate(openView);
                _popupRegion.Remove(openView);
                GC.SuppressFinalize(openView);
                GC.Collect();
            }
        }

        public IPopupController RegisterPopupViewWithName(string name, Func<Object> createPopup)
        {
            if (String.IsNullOrEmpty(name))
                throw new ArgumentOutOfRangeException("name");
            if (createPopup == null)
                throw new NullReferenceException("createPopup");
            if (registeredRegionContent.ContainsKey(name))
                throw new Exception("key " + name + " already exists");

            registeredRegionContent.Add(name, createPopup);
            return this;
        }
    }

    public interface IPopupController
    {
        bool OpenPopup(string popupName, Object dataContext = null);
        void ClosePopup();
        IPopupController RegisterPopupViewWithName(string popupName, Func<Object> createPopup);
    }
}
