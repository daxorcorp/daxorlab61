﻿using System;
using System.Windows;
using System.Windows.Input;
using Daxor.Lab.Infrastructure.Keyboards;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;

namespace Daxor.Lab.BVA.Common.Commands
{
    /// <summary>
    /// Exposes Command attached property in order to forward MouseLeftButtonDown event on
    /// GridViewRow object, passes GridViewRow.Item to the Command's CanExecute() and Execute() methods
    /// Now our ViewModel can simply bind its ICommand command to this attached property on Telerik DataGrid control.
    /// </summary>
    public class AttachedCommandBinding
    {
        #region Fields
        // These fields are used for detecting double clicks.
        private static DateTime lastTimeCommandWasInvoked = DateTime.MinValue;
        private static TimeSpan doubleClickDelay = TimeSpan.FromMilliseconds(System.Windows.Forms.SystemInformation.DoubleClickTime);
        #endregion

        #region RowActivated Command
        public static ICommand GetRowActivatedCommand(DependencyObject obj)
        {
            return (ICommand)obj.GetValue(RowActivatedCommandProperty);
        }

        public static void SetRowActivatedCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(RowActivatedCommandProperty, value);
        }

        // Using a DependencyProperty as the backing store for RowActivatedCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RowActivatedCommandProperty =
            DependencyProperty.RegisterAttached("RowActivatedCommand", typeof(ICommand),
            typeof(AttachedCommandBinding), new FrameworkPropertyMetadata(CommandChanged));


        private static void CommandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            FrameworkElement fe = d as FrameworkElement;
            fe.AddHandler(FrameworkElement.PreviewMouseLeftButtonDownEvent, new RoutedEventHandler(MouseLeftButtonDownHandler), true);
        }


        private static void MouseLeftButtonDownHandler(object sender, RoutedEventArgs args)
        {
            // Don't execute command if we are processing a double click
            if (DateTime.Now - lastTimeCommandWasInvoked <= doubleClickDelay)
                return;
            else
                lastTimeCommandWasInvoked = DateTime.Now;

            DependencyObject d = sender as DependencyObject;
            if (d == null) return;
            FrameworkElement fe = args.OriginalSource as FrameworkElement;
            GridViewRow gRow = fe.ParentOfType<GridViewRow>();

            if (gRow != null)
            {
                //Handle tunneling even yourself. Set the actuall row to being selected.
                args.Handled = true;
                gRow.IsSelected = true;

                ICommand cmd = GetRowActivatedCommand(d);
                if (cmd != null && cmd.CanExecute(gRow.Item))
                {
                    VirtualKeyboardManager.CloseActiveKeyboard();
                    cmd.Execute(gRow.Item);
                }
            }
        }
        #endregion
    }
}
