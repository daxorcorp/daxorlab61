﻿using System;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;

namespace Daxor.Lab.BVA.Common.Events
{
    /// <summary>
    /// Aggregate event that gets sent when a ViewModel wants to be added itself to the 
    /// TestSetupView as a tab.
    /// </summary>
    public class TestSetupTabItemAdded : CompositePresentationEvent<TestSetupTabItemPayload>{}

    /// <summary>
    /// AddTestSetupTabItem event payload
    /// </summary>
    public class TestSetupTabItemPayload
    {
        public TestSetupTabItemPayload(Guid id)
        {
            UniqueID = id;
        }

        public DelegateCommand<object> NavigationCommand
        {
            get;
            set;
        }
        public string Header
        {
            get;
            set;
        }
        public int TabPlacementIndex
        {
            get;
            set;
        }
        //Unique ID of the tab
        public Guid UniqueID
        {
            get;
            private set;
        }
    }
}
