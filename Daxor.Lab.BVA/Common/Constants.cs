﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.BVA.Common
{
    public class Constants
    {
        #region Regular expressions for height and weight input

        /// <summary>
        /// Regular expression for enforcing valid height measurements in inches
        /// </summary>
        /// <remarks>
        /// 1-10, 10-99, 1.0-9.9, 10.0-98.9, 99.0-99.9
        /// </remarks>
        public static readonly string EnglishHeightRegEx = @"^([0-9]\.?|[1-9][0-9]\.?|[0-9]\.[0-9]{0,1}|[1-9][0-9]\.[0-9]{0,1})$";

        /// <summary>
        /// Regular expression for enforcing valid weight measurements in pounds
        /// </summary>
        /// <remarks>
        /// 0-9, 10-99, 100-999, 1000-1399, 1400-1429, 1430
        /// 0.02-0.09, 0.10-9.99, 10.00-99.99, 100.00-999.99, 1000.00-1399.99, 1400.00-1429.99, 1430.00
        /// </remarks>
        public static readonly string EnglishWeightRegEx = @"^(0\.0{0,2}|[1-9]\.?[0-9]{0,2}|[0-9]\.?|[1-9][0-9]\.?|[1-9][0-9]{2}\.?|1[0-3][0-9]{2}\.?|14[0-2][0-9]\.?|1430\.?|[0-9]\.0[2-9]?|[0-9]\.[1-9][0-9]?|[1-9][0-9]\.[0-9]{0,2}|[1-9][0-9]{2}\.[0-9]{0,2}|1[0-3][0-9]{2}\.[0-9]{0,2}|14[0-2][0-9]\.[0-9]{0,2}|1430\.[0]{0,2})$";

        /// <summary>
        /// Regular expression for enforcing valid height measurements in centimeters
        /// </summary>
        /// <remarks>
        /// 0-9, 10-99, 100-199, 200-249, 250-253
        /// 0.30-0.99, 1.00-9.99, 10.00-99.99, 100.00-199.99, 200.00-249.99, 250.00-252.99, 253.00-253.69, 253.7
        /// </remarks>
        public static readonly string MetricHeightRegEx = @"^(0\.0{0,2}|[0-9]\.?|[1-9][0-9]\.?|1[0-9]{2}\.?|2[0-4][0-9]\.?|25[0-3]\.?|253\.[0-6][0-9]|253\.7|0\.[3-9][0-9]?|[1-9]\.[0-9]{0,2}|[1-9][0-9]\.[0-9]{0,2}|1[0-9]{2}\.[0-9]{0,2}|2[0-4][0-9]\.[0-9]{0,2}|25[0-2]\.[0-9]{0,2}|253\.[0-6][0-9]?|253\.7[0]?)$";

        /// <summary>
        /// Regular expression for enforcing valid weight measurements in kilograms
        /// </summary>
        /// <remarks>
        /// 0-9, 10-99, 100-599, 600-639, 640-648
        /// 0.00-9.99, 10.00-99.99, 100.00-599.99, 600.00-639.99, 640.00-647.99, 648.00-648.59, 648.6, 648.60-648.64
        /// </remarks>
        public static readonly string MetricWeightRegEx = @"^([0-9]\.?|[1-9][0-9]\.?|[1-5][0-9]{2}\.?|6[0-3][0-9]\.?|64[0-8]\.?|[0-9]\.[0-9]{0,2}|[1-9][0-9]\.[0-9]{0,2}|[1-5][0-9]{2}\.[0-9]{0,2}|6[0-3][0-9]\.[0-9]{0,2}|64[0-7]\.[0-9]{0,2}|648\.[0-5][0-9]?|648\.6|648\.6[0-4])$";

        #endregion

        #region Error messages

        /// <summary>
        /// Message box caption to be used with error messages
        /// </summary>
        public static readonly string ErrorMessageCaption = "BVA Test Error";

        #endregion

        #region Non-error messages

        /// <summary>
        /// Official name of the BV test
        /// </summary>
        public static readonly string TestName = "Blood Volume Analysis";

        /// <summary>
        /// Message pertaining to loading a BV test
        /// </summary>
        public static readonly string LoadingSelectedTestMessage = "Loading " + TestName + "...";

        #endregion

        #region LocalizedStrings

        /// <summary>
        /// Message pertaining to QC being due when trying to run a BV test
        /// </summary>
        public static readonly string QCDueBeforeTestMessage = "Daily QC is due at this time. Do you want to run Daily QC now or continue with this Blood Volume Analysis?";

        /// <summary>
        /// Message pertaining to saving a BV test
        /// </summary>
        public static readonly string SavingTestMessage = "Saving " + TestName + "...";

        /// <summary>
        /// Message pertaining to starting a BV test
        /// </summary>
        public static readonly string StartingTestMessage = "Starting " + TestName + "...";

        /// <summary>
        /// Message corresponding to the analyst proceeding with the test even if QC is due
        /// </summary>
        public static readonly string QCDueNoticeOverriddenFormatMessage = "{0} QC due notice overridden by the analyst.";

        #endregion
    }
}
