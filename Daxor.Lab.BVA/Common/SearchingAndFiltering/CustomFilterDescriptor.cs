using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using Telerik.Windows.Controls;
using Telerik.Windows.Data;

namespace Daxor.Lab.BVA.Common.SearchingAndFiltering
{
    public class CustomFilterDescriptor : FilterDescriptorBase
    {
        private readonly CompositeFilterDescriptor _compositeFilterDesriptor;
        private static readonly ConstantExpression TrueExpression = Expression.Constant(true);
        private string _filterValue;

        public CustomFilterDescriptor(IEnumerable<GridViewColumn> columns)
        {
            _compositeFilterDesriptor = new CompositeFilterDescriptor
            {
                LogicalOperator = FilterCompositionLogicalOperator.Or
            };

            foreach (var column in columns.Cast<GridViewDataColumn>())
            {
                _compositeFilterDesriptor.FilterDescriptors.Add(CreateFilterForColumn(column));
            }
        }

        public string FilterValue
        {
            get
            {
                return _filterValue;
            }
            set
            {
                if (_filterValue != value)
                {
                    _filterValue = value;
                    UpdateCompositeFilterValues();
                    OnPropertyChanged("FilterValue");
                }
            }
        }

        protected override Expression CreateFilterExpression(ParameterExpression parameterExpression)
        {
            if (string.IsNullOrEmpty(FilterValue))
            {
                return TrueExpression;
            }
            try
            {
                return _compositeFilterDesriptor.CreateFilterExpression(parameterExpression);
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch (Exception)
            {

            }

            return TrueExpression;
        }

        private IFilterDescriptor CreateFilterForColumn(GridViewDataColumn column)
        {
            var filterOperator = GetFilterOperatorForType(column.DataType);
            var descriptor = new FilterDescriptor(column.UniqueName, filterOperator, _filterValue)
            {
                MemberType = column.DataType
            };

            return descriptor;
        }

        private static FilterOperator GetFilterOperatorForType(Type dataType)
        {
            return dataType == typeof(string) ? FilterOperator.Contains : FilterOperator.IsEqualTo;
        }

        private void UpdateCompositeFilterValues()
        {
            foreach (var filterDescriptor in _compositeFilterDesriptor.FilterDescriptors)
            {
                var descriptor = (FilterDescriptor) filterDescriptor;
                // ReSharper disable once RedundantAssignment
                var convertedValue = DefaultValue(descriptor.MemberType);

                try
                {
                    convertedValue = Convert.ChangeType(FilterValue, descriptor.MemberType, CultureInfo.InvariantCulture);
                }
                catch
                {
                    convertedValue = OperatorValueFilterDescriptorBase.UnsetValue;
                }

                if (descriptor.MemberType.IsAssignableFrom(typeof(DateTime)))
                {
                    DateTime date;
                    if (DateTime.TryParse(FilterValue, out date))
                    {
                        convertedValue = date;
                    }
                }

                descriptor.Value = convertedValue;
            }
        }

        private static object DefaultValue(Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }

            return null;
        }
    }
}