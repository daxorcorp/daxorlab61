﻿using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace Daxor.Lab.BVA.Common.SearchingAndFiltering
{
    public class PropertyChangedUpdateTriggerBehavior : Behavior<TextBox>
    {
        protected override void OnAttached()
        {
            AssociatedObject.TextChanged += OnTextBoxTextChanged;
        }

        void OnTextBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            var bindingExpression = AssociatedObject.ReadLocalValue(TextBox.TextProperty) as BindingExpression;
            if (bindingExpression != null)
            {
                bindingExpression.UpdateSource();
            }
        }

        protected override void OnDetaching()
        {
            AssociatedObject.TextChanged -= OnTextBoxTextChanged;
        }
    }
}
