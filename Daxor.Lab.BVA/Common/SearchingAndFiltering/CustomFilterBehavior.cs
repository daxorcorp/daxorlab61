﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

namespace Daxor.Lab.BVA.Common.SearchingAndFiltering
{
    public class CustomFilterBehavior : ViewModelBase
    {
        private readonly RadGridView _gridView;
        private readonly TextBox _tb;

        private CustomFilterDescriptor _customFilterDescriptor;
        public CustomFilterDescriptor CustomFilterDescriptor
        {
            get
            {
                if (_customFilterDescriptor != null) return _customFilterDescriptor;

                // ReSharper disable once RedundantEnumerableCastCall
                _customFilterDescriptor = new CustomFilterDescriptor(_gridView.Columns.OfType<Telerik.Windows.Controls.GridViewColumn>());
                _gridView.FilterDescriptors.Add(_customFilterDescriptor);
                return _customFilterDescriptor;
            }
        }

        public static readonly DependencyProperty TextBoxProperty =
        DependencyProperty.RegisterAttached("TextBox", typeof(TextBox), typeof(CustomFilterBehavior),
            new PropertyMetadata(OnTextBoxPropertyChanged));

        public static void SetTextBox(DependencyObject dependencyObject, TextBox tb)
        {
            dependencyObject.SetValue(TextBoxProperty, tb);
        }

        public static TextBox GetTextBox(DependencyObject dependencyObject)
        {
            return (TextBox)dependencyObject.GetValue(TextBoxProperty);
        }

        public static void OnTextBoxPropertyChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var grid = dependencyObject as RadGridView;
            var tb = e.NewValue as TextBox;

            if (grid != null && tb != null)
            {
                // ReSharper disable once UnusedVariable
                // ReSharper disable once SuggestUseVarKeywordEvident
                CustomFilterBehavior behavior = new CustomFilterBehavior(grid, tb);
            }
        }

        public CustomFilterBehavior(RadGridView gridView, TextBox tb)
        {
            _gridView = gridView;
            _tb = tb;

            _tb.TextChanged -= FilterValue_TextChanged;
            _tb.TextChanged += FilterValue_TextChanged;
        }

        private void FilterValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            CustomFilterDescriptor.FilterValue = _tb.Text;
            _tb.Focus();
        }
    }
}

   