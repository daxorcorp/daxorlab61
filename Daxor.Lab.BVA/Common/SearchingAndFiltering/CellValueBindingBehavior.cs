﻿using System.Windows;
using Daxor.Lab.Infrastructure.Controls;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.GridView;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Common.SearchingAndFiltering
{
    public class CellValueBindingBehavior : LoadBehavior<FrameworkElement>
    {
        protected override void OnAssociatedObjectLoaded()
        {
            base.OnAssociatedObjectLoaded();

            var cell = AssociatedObject.ParentOfType<GridViewCell>();
            if (cell != null)
            {
                AssociatedObject.SetBinding(SearchableTextControl.TextProperty, new Binding("Value") { Source = cell, Mode = BindingMode.TwoWay });
            }
        }

        protected override void OnDetaching()
        {
            AssociatedObject.ClearValue(SearchableTextControl.TextProperty);

            base.OnDetaching();
        }
    }
}
