﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace Daxor.Lab.BVA.Common.SearchingAndFiltering
{
    public class FilterableNotifiableCollection<T> : INotifyCollectionChanged, INotifyPropertyChanged, IEnumerable<T>
    {
        #region Class Level Variables

        private readonly ObservableCollection<T> _sourceCollection;
        private Predicate<T> _currentFilter;

        #endregion

        #region Events

        // ReSharper disable EventNeverInvoked
        // ReSharper disable InconsistentNaming
        private event NotifyCollectionChangedEventHandler _collectionChanged;
        private event PropertyChangedEventHandler _propertyChanged;
        // ReSharper restore InconsistentNaming
        // ReSharper restore EventNeverInvoked

        public event NotifyCollectionChangedEventHandler CollectionChanged
        {
            add { _collectionChanged += value; }
            remove { _collectionChanged -= value; }
        }
        public event PropertyChangedEventHandler PropertyChanged
        {
            add { _propertyChanged += value; }
            remove { _propertyChanged -= value; }
        }

        #endregion

        #region Properties

        public Predicate<T> Filter
        {
            get { return _currentFilter; }
            set
            {
                _currentFilter = value;
                RefreshCollection();
            }
        }

        public ObservableCollection<T> CoreCollection
        {
            get
            {
                return _sourceCollection;
            }
        }

        public T this[int index]
        {
            get
            {
                if (_currentFilter == null)
                {
                    return _sourceCollection[index];
                }
                
                var currentIndex = 0;

                foreach (var indexitem in _sourceCollection.Where(indexitem => _currentFilter(indexitem)))
                {
                    if (currentIndex.Equals(index))
                    {
                        return indexitem;
                    }
                    currentIndex++;
                }
                throw new ArgumentOutOfRangeException();
            }
            set
            {
                if (_currentFilter == null)
                {
                    _sourceCollection[index] = value;
                }
                else if (!_currentFilter(value))
                {
                    throw new InvalidOperationException("Value doesn't match the filter criteria.");
                }
                else
                {
                    var valueNotSet = true;
                    var currentIndex = 0;

                    for (var i = 0; i < _sourceCollection.Count; i++)
                    {
                        var indexitem = _sourceCollection[i];
                        if (!_currentFilter(indexitem)) continue;
                        
                        if (currentIndex.Equals(index))
                        {
                            _sourceCollection[i] = value;
                            valueNotSet = false;
                            break;
                        }
                        currentIndex++;
                    }

                    if (valueNotSet)
                    {
                        throw new ArgumentOutOfRangeException();
                    }

                }
            }
        }

        #endregion

        #region Constructors

        public FilterableNotifiableCollection(ObservableCollection<T> collectionParam)
        {
            _sourceCollection = collectionParam;
            _currentFilter = null;
            _sourceCollection.CollectionChanged += SourceCollection_CollectionChanged;
            ((INotifyPropertyChanged)_sourceCollection).PropertyChanged += SourceCollection_PropertyChanged;
        }

        #endregion

        #region Event Handlers

        void SourceCollection_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (_propertyChanged != null)
            {
                _propertyChanged(this, e);
            }
        }

        void SourceCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (_collectionChanged == null) return;
            
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    _collectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                    break;
                case NotifyCollectionChangedAction.Remove:
                    _collectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                    break;
                case NotifyCollectionChangedAction.Replace:
                    _collectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                    break;
                case NotifyCollectionChangedAction.Reset:
                    _collectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                    break;
            }
        }

        #endregion

        #region Public Methods

        public void Add(T item)
        {
            _sourceCollection.Add(item);
        }

        public void Remove(T item)
        {
            _sourceCollection.Remove(item);
        }

        public int IndexOf(T item)
        {
            if (_currentFilter == null)
                return _sourceCollection.IndexOf(item);
            
            var count = 0;
            
            foreach (var indexitem in _sourceCollection.Where(indexitem => _currentFilter(indexitem)))
            {
                if (indexitem.Equals(item))
                    return count;
                
                count++;
            }
            return -1;
        }

        public void Clear()
        {
            if (_currentFilter == null)
            {
                _sourceCollection.Clear();
            }
            else
            {
                for (int i = 0; i < _sourceCollection.Count; )
                {
                    T item = _sourceCollection[i];

                    if (_currentFilter(item))
                    {
                        _sourceCollection.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                }
            }

        }

        public bool Contains(T item)
        {
            if (_currentFilter != null && _currentFilter(item) == false)
            {
                return false;
            }

            return _sourceCollection.Contains(item);
        }

        public int Count
        {
            get
            {
                if (_currentFilter == null)
                {
                    return _sourceCollection.Count;
                }

                return _sourceCollection.Count(item => _currentFilter(item));
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new FilterableEnumerator(this, _sourceCollection.GetEnumerator());
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new FilterableEnumerator(this, _sourceCollection.GetEnumerator());
        }

        public IEnumerable<T> GetFilteredEnumerableGeneric()
        {
            return new FilterableEnumerable(this, _sourceCollection.GetEnumerator());
        }

        public IEnumerable GetFilteredEnumerable()
        {
            return new FilterableEnumerable(this, _sourceCollection.GetEnumerator());
        }

        #endregion

        #region Private Methods

        private void RefreshCollection()
        {
            if (_collectionChanged != null)
            {
                _collectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
        }

        #endregion

        #region Inner Classes

        private class FilterableEnumerable : IEnumerable<T>
        {
            private readonly FilterableNotifiableCollection<T> _filterableNotifiableCollection;
            private readonly IEnumerator<T> _enumerator;

            public FilterableEnumerable(FilterableNotifiableCollection<T> filterablecollection, IEnumerator<T> enumeratorParam)
            {
                _filterableNotifiableCollection = filterablecollection;
                _enumerator = enumeratorParam;
            }

            public IEnumerator<T> GetEnumerator()
            {
                return new FilterableEnumerator(_filterableNotifiableCollection, _enumerator);
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return new FilterableEnumerator(_filterableNotifiableCollection, _enumerator);
            }
        }

        private class FilterableEnumerator : IEnumerator<T>
        {
            private readonly FilterableNotifiableCollection<T> _filterableObservableCollection;
            private readonly IEnumerator<T> _enumerator;

            public FilterableEnumerator(FilterableNotifiableCollection<T> filterablecollection, IEnumerator<T> enumeratorParam)
            {
                _filterableObservableCollection = filterablecollection;
                _enumerator = enumeratorParam;
            }

            public T Current
            {
                get
                {
                    if (_filterableObservableCollection.Filter == null)
                        return _enumerator.Current;
                    
                    if (_filterableObservableCollection.Filter(_enumerator.Current) == false)
                        throw new InvalidOperationException();
                    
                    return _enumerator.Current;
                }
            }

            public void Dispose()
            {
                _enumerator.Dispose();
            }

            object IEnumerator.Current
            {
                get { return Current; }
            }

            public bool MoveNext()
            {
                while (true)
                {
                    if (_enumerator.MoveNext() == false)
                        return false;
                    if (_filterableObservableCollection.Filter == null
                        || _filterableObservableCollection.Filter(_enumerator.Current))
                        return true;
                }
            }

            public void Reset()
            {
                _enumerator.Reset();
            }
        }

        #endregion
    }
}
