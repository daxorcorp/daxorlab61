﻿using System.Windows;
using System.Windows.Interactivity;

namespace Daxor.Lab.BVA.Common.SearchingAndFiltering
{
    public abstract class LoadBehavior<TFrameworkElement> : Behavior<TFrameworkElement> where TFrameworkElement : FrameworkElement
    {
        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.Loaded += OnAssociatedObjectLoaded;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            AssociatedObject.Loaded -= OnAssociatedObjectLoaded;
        }

        private void OnAssociatedObjectLoaded(object sender, RoutedEventArgs e)
        {
            OnAssociatedObjectLoaded();
        }

        protected virtual void OnAssociatedObjectLoaded()
        {
        }
    }
}
