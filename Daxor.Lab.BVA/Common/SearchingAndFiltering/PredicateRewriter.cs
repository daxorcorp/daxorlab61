using System.Linq.Expressions;

namespace Daxor.Lab.BVA.Common.SearchingAndFiltering
{
    public class PredicateRewriter : ExpressionVisitor
    {
        private readonly ParameterExpression _newParameter;

        private PredicateRewriter(ParameterExpression newParameter)
        {
            _newParameter = newParameter;
        }

        protected override Expression VisitParameter(ParameterExpression p)
        {
            return _newParameter;
        }

        public static Expression Rewrite(LambdaExpression lambda, ParameterExpression newParameter)
        {
            var rewriter = new PredicateRewriter(newParameter);
            return rewriter.Visit(lambda.Body);
        }
    }
}