using System;
using System.Linq.Expressions;
using Telerik.Windows.Data;

namespace Daxor.Lab.BVA.Common.SearchingAndFiltering
{
    public class PredicateFilterDescriptor<T> : FilterDescriptorBase
    {
        private Expression<Func<T, bool>> _predicate;

        public Expression<Func<T, bool>> Predicate
        {
            get { return _predicate; }
            set
            {
                if (_predicate != value)
                {
                    _predicate = value;
                    OnPropertyChanged("Predicate");
                }
            }
        }
        protected override Expression CreateFilterExpression(ParameterExpression parameterExpression)
        {
            return PredicateRewriter.Rewrite(Predicate, parameterExpression);
        }
    }
}