﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.DomainModels;
using System.Windows;

namespace Daxor.Lab.BVA.Common
{
    public class Tuple : DependencyObject
    {
        public string DisplayValue
        {
            get { return (string)GetValue(DisplayValueProperty); }
            set { SetValue(DisplayValueProperty, value); }
        }

        public string DisplayOrder
        {
            get { return (string)GetValue(DisplayOrderProperty); }
            set { SetValue(DisplayOrderProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DisplayValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisplayValueProperty =
            DependencyProperty.Register("DisplayValue", typeof(string), typeof(Tuple), new UIPropertyMetadata(String.Empty));

        // Using a DependencyProperty as the backing store for DisplayOrder.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DisplayOrderProperty =
            DependencyProperty.Register("DisplayOrder", typeof(string), typeof(Tuple), new UIPropertyMetadata(String.Empty));
    }
}
