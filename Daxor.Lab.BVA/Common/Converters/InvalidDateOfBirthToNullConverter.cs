﻿using System;
using System.Windows.Data;
using Daxor.Lab.Infrastructure.Constants;

namespace Daxor.Lab.BVA.Common.Converters
{
    /// <summary>
    /// Converts a invalid DateTime to null so that invalid dates of birth do not appear
    /// in the UI.
    /// </summary>
    public class InvalidDateOfBirthToNullConverter : IValueConverter
    {
        /// <summary>
        /// Converts a known invalid date of birth for a patient to null.
        /// </summary>
        /// <param name="value">DateTime value</param>
        /// <param name="targetType">Type that this method returns (i.e., nullable DateTime)</param>
        /// <param name="parameter">Conversion parameter (ignored)</param>
        /// <param name="culture">Culture parameter (ignored)</param>
        /// <returns>Null if the given date of birth is a known invalid date; the specified value otherwise</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            DateTime? valueAsDateTime = value as DateTime?;
            if (valueAsDateTime == DomainConstants.InvalidDateOfBirth)
                return null;

            return value;
        }

        /// <summary>
        /// Not implemented.
        /// </summary>
        /// <param name="value">Not implemented.</param>
        /// <param name="targetType">Not implemented.</param>
        /// <param name="parameter">Not implemented.Not implemented.</param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
