﻿using System;
using System.Windows.Data;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.Entities;

namespace Daxor.Lab.BVA.Common.Converters
{
    /// <summary>
    /// Converts a TestStatus enumeration and TestMode enumeration into a bool so that the 
    /// injectate verification fields can be enabled/disabled at the appropriate times.
    /// </summary>
    public class InjectateVerificationFieldEnabledMultiValueConverter : IMultiValueConverter
    {
        /// <summary>
        /// Converts TestStatus and TestMode enumeration values to bool.
        /// </summary>
        /// <param name="values">A three-element array with the first element being a 
        /// TestStatus enumeration value, the second element being a TestMode enumeration
        /// value, the thrid being whether manual entry is required</param>
        /// <param name="targetType">Type that this method returns</param>
        /// <param name="parameter">Conversion parameter (ignored)</param>
        /// <param name="culture">Culture parameter (ignored)</param>
        /// <returns>True (enabled) if the test is pending, the mode is automatic, and
        /// manual entry is not required; false for all other combinations; false if 
        /// the values cannot be used (e.g., wrong type, null)</returns>
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            // If the values aren't what we expected, have the field be disabled by default.
            if (!(values[0] is Domain.Common.TestStatus) || !(values[1] is TestMode))
                return false;
           
            var testStatus = (Domain.Common.TestStatus)values[0];
            var testMode = (TestMode)values[1];
            var manualEntryRequired = values[2] as Boolean?;
            
            if (manualEntryRequired.HasValue && manualEntryRequired.Value)
                return false;

            // The field should only be enabled for pending tests and automatic tests.
            return (testStatus == Domain.Common.TestStatus.Pending) && (testMode == TestMode.Automatic);
        }

        /// <summary>
        /// Converts bool to TestStatus and TestMode enumeration values.
        /// </summary>
        /// <remarks>Not used</remarks>
        /// <param name="value">Bool to convert (ignored)</param>
        /// <param name="targetTypes">Types that this method returns</param>
        /// <param name="parameter">Conversion parameter (ignored)</param>
        /// <param name="culture">Culture parameter (ignored)</param>
        /// <returns>Object array with the given value present twice</returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return new[] { value, value };
        }
    }
}
