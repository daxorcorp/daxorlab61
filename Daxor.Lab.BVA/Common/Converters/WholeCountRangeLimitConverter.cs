﻿using System;
using System.Windows.Data;
using Daxor.Lab.BVA.Core;

namespace Daxor.Lab.BVA.Common.Converters
{
    public class WholeCountRangeLimitConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var wholeCount = value as int?;
            if (wholeCount == null || wholeCount.Value <= 0 || Double.IsNaN(wholeCount.Value) || Double.IsInfinity(wholeCount.Value))
                return 0;

            if ((wholeCount.Value > BvaDomainConstants.MaximumBloodVolumeInmL) ||
                (wholeCount.Value < BvaDomainConstants.MinimumBloodVolumeInmL))
                return 0;

            return wholeCount.Value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
