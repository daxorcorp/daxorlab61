﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.BVA.Core;
using System.Windows;
using System.Globalization;

namespace Daxor.Lab.BVA.Common.Converters
{
    public class TestToVisibilityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            TestStatus status = (TestStatus)values[0];
            TestMode mode = (TestMode)values[1];
            if (mode == TestMode.Manual)
                return Visibility.Visible;

            return (status == TestStatus.Completed) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("Not supported");
        }
    }
}
