﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Common.Converters
{
    public class DeviationValueBasedOnMeasuredAndIdealVolume : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            String result = "-";
            try
            {
                if (values != null && values.Count() == 3)
                    result = ((int)values[0] <= 0 || (int)values[1] <= 0) ? result : values[2].ToString();
            }
            catch
            {
                result = "-"; 
            }

            return result;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
