﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Daxor.Lab.Infrastructure.Extensions;

namespace Daxor.Lab.BVA.Common.Converters
{
    public class StringFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            String paramString = parameter as String;
            String valString = value as String;

            if (String.IsNullOrWhiteSpace(valString))
                return String.Empty;
            if (String.IsNullOrWhiteSpace(paramString))
                return valString;

            string result;
            switch (paramString)
            {
                case "ToUpperFirstLetter": result = valString.ToUpperFirstLetter(); break;
                case "ToUpper": result = valString.ToUpper(); break;
                default: result = valString; break;
            }

            return result;
           
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
