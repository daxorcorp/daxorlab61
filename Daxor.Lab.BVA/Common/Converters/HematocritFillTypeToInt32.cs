﻿using System;
using System.Windows.Data;
using Daxor.Lab.BVA.Core;

namespace Daxor.Lab.BVA.Common.Converters
{
    public class HematocritFillTypeToInt32 : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var fillType = (HematocritFillType)value;
            return (Int32)fillType;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            HematocritFillType fillType;
            if (!Enum.TryParse(value.ToString(), out fillType))
                fillType = HematocritFillType.Unknown;

            return fillType;
        }
    }
}
