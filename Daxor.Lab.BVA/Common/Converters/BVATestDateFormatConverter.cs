﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Common.Converters
{
    /// <summary>
    /// Converts Test StartedTime to appropriate value
    /// </summary>
    public class BVATestDateFormatConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is DateTime))
                return null;

            DateTime dateTimeValue = (DateTime)value;
            return dateTimeValue.ToString("MM/dd/yyyy h:mmt").ToLower();
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
