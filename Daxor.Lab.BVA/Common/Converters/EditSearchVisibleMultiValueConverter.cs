﻿using System;
using System.Windows.Data;
using System.Windows;

namespace Daxor.Lab.BVA.Common.Converters
{
    public class EditSearchVisibleMultiValueConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(values[0] is bool) || !(values[1] is bool))
            {
                return Visibility.Collapsed;
            }
            bool isSearchEnabled = (bool)values[0];
            bool isKeyboardClosed = (bool)values[1];

            if (isSearchEnabled && isKeyboardClosed)
            {
                return Visibility.Visible;
            }

            return Visibility.Collapsed; ;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            return new object[] { value, value };
        }

        #endregion
    }
}
