﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Common.Converters
{
	public class BooleanToAmputeeStatusConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null) throw new ArgumentNullException("value");
			if (!(value is bool)) throw new NotSupportedException();

			var valueAsBool = (bool) value;

			return valueAsBool ? "Amputee" : "Non-amputee";
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value == null) throw new ArgumentNullException("value");
			if (!(value is string)) throw new NotSupportedException();

			var valueAsString = (string) value;

			return valueAsString == "Amputee";
		}
	}
}
