﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Common.Converters
{

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class TestCommentStringLengthUsedToRemainingCharactersConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int length = 0;
            if (!Int32.TryParse(value.ToString(), out length))
                return 0;
            else
                return (100 - length);
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
