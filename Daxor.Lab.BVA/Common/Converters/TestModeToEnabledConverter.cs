﻿//-----------------------------------------------------------------------
// <copyright file="TestModeToEnabledConverter.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Windows.Data;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.BVA.Core;

namespace Daxor.Lab.BVA.Common.Converters
{
    /// <summary>
    /// Converts a TestMode enumeration into a bool so that the Injectate Lot field can
    /// be enabled/disabled based on the current test mode.
    /// </summary>
    public class TestModeToEnabledConverter : IValueConverter
    {
        /// <summary>
        /// Converts a TestMode enumeration value to bool.
        /// </summary>
        /// <param name="value">One of the TestMode values that defines the test mode</param>
        /// <param name="targetType">Type that this method returns</param>
        /// <param name="parameter">Conversion parameter (ignored)</param>
        /// <param name="culture">Culture parameter (ignored)</param>
        /// <returns>False (disabled) if the test mode is "Automatic"; true for all other
        /// test modes; false if the value is null or not a TestMode value</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is TestMode))
                return false;

            TestMode tMode = (TestMode)value;
            return tMode != TestMode.Automatic;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
