﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Common.Converters
{
    /// <summary>
    /// Converts Red cell Volume to bar height
    /// </summary>
    public class RedCellVolumeToBarHeightConverter : IMultiValueConverter
    {
        #region IMultiValueConverter Members

        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double actualTotalHeight;
            int totalVolumeCount;
            int redCellVolumeCount;
            double resultHeight = 0;
            Daxor.Lab.BVA.Core.BloodType name = Core.BloodType.Unknown;

            double rowGridActualHeight = Double.NaN;
            double rowGridHeight = Double.NaN;
            
            try
            {
                actualTotalHeight = Math.Round((double)values[0], 0);
                totalVolumeCount = (int)values[1];
                redCellVolumeCount = (int)values[2];
                name = (Daxor.Lab.BVA.Core.BloodType)values[3];
                if (values[4] != System.Windows.DependencyProperty.UnsetValue)
                {
                    rowGridActualHeight = (double)values[4];
                }
                if (values[5] != System.Windows.DependencyProperty.UnsetValue)
                {
                    rowGridHeight = (double)values[5];
                }

                resultHeight = Math.Round((double)(redCellVolumeCount * actualTotalHeight) / (double)totalVolumeCount, 0);

                //System.Diagnostics.Debug.Print("-------------------------------------- {0} --------------------------------------", name.ToString());
                //System.Diagnostics.Debug.Print("TotalVolume={0}  RBCVolume ={1}  PlasmaVolume={2}", totalVolumeCount, redCellVolumeCount, totalVolumeCount - redCellVolumeCount);
                //System.Diagnostics.Debug.Print("TotalHeight={0}  RowAHeight={1}  RowHeight   ={2}", actualTotalHeight, rowGridActualHeight, rowGridHeight);
                //System.Diagnostics.Debug.Print("PlasmaHeight={0}  RBCHeight={1}", actualTotalHeight - resultHeight, resultHeight);
                //System.Diagnostics.Debug.Print("");
            }
            catch
            {
            }

            return resultHeight;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
