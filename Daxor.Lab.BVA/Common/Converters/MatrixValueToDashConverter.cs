﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Common.Converters
{
    public class MatrixValueToDashConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((string)parameter == "PercentDeviation")
            {
                var pdResult = value as double?;
                return (pdResult == null || pdResult.Value >= 100 || pdResult.Value <= -100 || 
                    Double.IsNaN(pdResult.Value) || Double.IsInfinity(pdResult.Value)) ? "-" : pdResult.Value.ToString();
            }
            else if ((string)parameter == "NumericDeviation")
            {
                var iResult = value as int?;
                return (iResult == null || Double.IsNaN(iResult.Value) || Double.IsInfinity(iResult.Value) || iResult.Value <= 0) ? "-" : iResult.Value.ToString();
            }
            else
            {
                var result = value as int?;
                return (result == null || result.Value <= 0 || Double.IsNaN(result.Value) || Double.IsInfinity(result.Value) || result.Value <= 0) ? "-" : result.Value.ToString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
