﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Common.Converters
{
	public class AmputeeIdealsCorrectionFactorToFormattedAmputeeIdealsCorrectionFactorConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var correctionFactor = (int) value;

			switch (correctionFactor)
			{
				case 0:
					return "0%";
				case -1:
					return string.Empty;
				default:
					return "-" + correctionFactor + "%";
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
