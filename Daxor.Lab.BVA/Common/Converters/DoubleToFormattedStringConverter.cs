﻿using System;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Common.Converters
{
    /// <summary>
    /// Converts between a double (i.e., double-precision floating point number) and 
    /// its string equivalent after rounding.
    /// </summary>
    public class DoubleToFormattedStringConverter : IValueConverter
    {
        /// <summary>
        /// Number of decimal places the converted value should have
        /// </summary>
        public static readonly int DECIMAL_PLACES = 2;

        /// <summary>
        /// Converts a double into its string equivalent (based on the number of decimal
        /// places specified in this class).
        /// </summary>
        /// <param name="value">Value to convert (e.g., 1.2345)</param>
        /// <param name="targetType">Type this method returns (i.e., string)</param>
        /// <param name="parameter">Converter parameter (ignored)</param>
        /// <param name="culture">Culture parameter (ignored)</param>
        /// <returns>The value as a string rounded to a specific number of decimal places.
        /// A zero-value is represented as string.Empty. Note: There are no padded zeroes.
        /// </returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double numericValue = 0f;

            // Error check: 'value' is not a valid double
            try { numericValue = System.Convert.ToDouble(value); }
            catch { return string.Empty; }

            // Error check: number of decimal places to round is out of range
            if (DECIMAL_PLACES < 0) return string.Empty;

            numericValue = Math.Round(numericValue, DECIMAL_PLACES, MidpointRounding.AwayFromZero);
            
            // Zeroes equate to empty strings
            if (numericValue == 0f)
                return string.Empty;

            return numericValue.ToString();
        }

        /// <summary>
        /// Converts a formatted string to a double (not implemented).
        /// </summary>
        /// <param name="value">Ignored -- method not implemented</param>
        /// <param name="targetType">Ignored -- method not implemented</param>
        /// <param name="parameter">Ignored -- method not implemented</param>
        /// <param name="culture">Ignored -- method not implemented</param>
        /// <returns>Ignored -- method not implemented</returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
