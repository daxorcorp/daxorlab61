﻿using System;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Common.Converters
{
    /// <summary>
    /// Converts between an object and a null value.
    /// </summary>
    public class ValueToNullConverter : IValueConverter
    {
        /// <summary>
        /// Converts an object (based on ToString()) to null if it matches its 
        /// parameter's ToString() value.
        /// </summary>
        /// <param name="value">Value to convert (i.e., an object)</param>
        /// <param name="targetType">Type this method returns (i.e., object reference)</param>
        /// <param name="parameter">Converter parameter to be compared to the value to convert</param>
        /// <param name="culture">Culture parameter (ignored)</param>
        /// <returns>Null-reference if (1) the given value to convert is null, or (2) value.ToString() 
        /// is the same as parameter.ToString(). Returns 'value' if (1) the parameter is null, or
        /// (2) value.ToString() differs from parameter.ToString().</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) return null;
            if (parameter == null) return value;
            return value.ToString().Equals(parameter.ToString()) ? null : value;
        }

        /// <summary>
        /// Converts a formatted string to a double (not implemented).
        /// </summary>
        /// <param name="value">Ignored -- method not implemented</param>
        /// <param name="targetType">Ignored -- method not implemented</param>
        /// <param name="parameter">Ignored -- method not implemented</param>
        /// <param name="culture">Ignored -- method not implemented</param>
        /// <returns>Ignored -- method not implemented</returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
