﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.BVA.Core;

namespace Daxor.Lab.BVA.Common.Converters
{
    /// <summary>
    /// Converts test status pending and manual test to true
    /// </summary>
    public class PendingTestStatusAndManualTestModelToBoolean : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (values == null || values.Count() != 2)
                return true;

            TestStatus status = (TestStatus)values[0];
            TestMode mode = (TestMode)values[1];

            if (status == TestStatus.Pending || mode == TestMode.Manual)
                return true;

            return false;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
