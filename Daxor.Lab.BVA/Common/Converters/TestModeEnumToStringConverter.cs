﻿//-----------------------------------------------------------------------
// <copyright file="TestModeEnumToStringConverter.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Common.Converters
{
    /// <summary>
    /// Converts a TestMode enumeration into a string representing that value.
    /// </summary>
    public class TestModeEnumToStringConverter : IValueConverter
    {
        /// <summary>
        /// Converts a TestMode enumeration value to a corresponding string.
        /// </summary>
        /// <param name="value">One of the TestMode values that defines the test mode</param>
        /// <param name="targetType">Type that this method returns</param>
        /// <param name="parameter">Conversion parameter (ignored)</param>
        /// <param name="culture">Culture parameter (ignored)</param>
        /// <returns>String value corresponding to the test mode; String.Empty if the test
        /// mode is unrecognized</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if ((value == null) || !(value is Core.TestMode))
            {
                return String.Empty;
            }

            Core.TestMode theMode = (Core.TestMode)value;
            string modeDescription = String.Empty;

            switch (theMode)
            {
                case Core.TestMode.Automatic: modeDescription = "Automatic"; break;
                case Core.TestMode.Manual: modeDescription = "Manual"; break;
                case Core.TestMode.TrainingHighSlopeTest:
                case Core.TestMode.TrainingHighStd:
                case Core.TestMode.Training:
                case Core.TestMode.TrainingReverseSlope:
                    modeDescription = "Training"; break;
                case Core.TestMode.VOPS: modeDescription = "VOPS"; break;
            }

            return modeDescription;
        }

        /// <summary>
        /// Converts a string to a TestMode.
        /// </summary>
        /// <remarks>Not implemented; throws an exception</remarks>
        /// <param name="value">String to be converted (ignored)</param>
        /// <param name="targetType">Type that this method returns</param>
        /// <param name="parameter">Conversion parameter (ignored)</param>
        /// <param name="culture">Culture parameter (ignored)</param>
        /// <returns>N/A</returns>
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
