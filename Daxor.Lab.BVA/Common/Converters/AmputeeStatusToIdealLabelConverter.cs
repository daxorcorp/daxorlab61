﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Common.Converters
{
	public class AmputeeStatusToIdealLabelConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var isAmputee = (bool) value;

			return isAmputee ? "Ideal*" : "Ideal";
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
