﻿using System;
using Microsoft.Practices.Composite.Presentation.Regions;
using Microsoft.Practices.Composite.Presentation.Regions.Behaviors;
using Telerik.Windows.Controls;

namespace Daxor.Lab.BVA.Common
{
    public class RegionTransitionAwareBehavior : RegionBehavior, IHostAwareRegionBehavior
    {
        RadTransitionControl _hostControl;

        public const string BehaviorKey = "RegionTransitionAwareBehavior";

        protected override void OnAttach()
        {
            Region.ActiveViews.CollectionChanged += ActiveViews_CollectionChanged;
        }

        void ActiveViews_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {

        }

        public System.Windows.DependencyObject HostControl
        {
            get { return _hostControl; }
            set
            {
                var newHost = value as RadTransitionControl;
                if (newHost == null)
                    throw new NotSupportedException("RegionTransitionAwareBehavior support only RadTransitionControl");
                if (IsAttached)
                    throw new InvalidOperationException("RegionTransitionAwareBehavior can only be attached once");

                _hostControl = newHost;
            }
        }
    }
}
