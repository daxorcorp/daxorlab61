﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.BVA.Common
{
    public static class MessageBoxUIKeys
    {
        static Guid _exportTestsWizardView = Guid.NewGuid();
        public static Guid ExportTestsWizard
        {
            get { return _exportTestsWizardView; }
        }

    }
}
