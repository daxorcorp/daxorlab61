﻿using System;
using System.Diagnostics;
using System.Windows;
using Daxor.Lab.BVA.Common;
using Daxor.Lab.BVA.Views;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using Daxor.Lab.BVA.SubViews;
using Microsoft.Practices.Composite;
using System.Collections.Generic;
using System.Linq;

namespace Daxor.Lab.BVA.Services
{
    /// <summary>
    /// BVA Module Navigator - responsible for navigating between BVA screens.
    /// Uses two internal regions (inner, outer) that are manipulated based
    /// on user selection.
    /// </summary>
    public class BVAModuleNavigator : IAppModuleNavigator
    {
        #region Fields

        private readonly string _moduleKey;
        private bool _wasActivatedExternally = false;

        private readonly IEventAggregator _eventAggregator;
        private readonly IUnityContainer _container;
        private readonly IRegionManager _regionManager;
        private readonly string _setupViewKey;          //unique test setup view key used internally only 
        private readonly ILoggerFacade _customLoggerFacade;

        private string _currentViewKey = BVAModuleViewKeys.Empty;
        private bool _isInitialized = false;

        #endregion

        #region Ctor

        public BVAModuleNavigator(IUnityContainer container, IEventAggregator eventAggregator,
                                  IRegionManager regionManager, INavigationCoordinator navigationCoordinator, 
                                  ILoggerFacade customLoggerFacade)
        {
            //Cache all my dependencies
            _container = container;
            _eventAggregator = eventAggregator;
            _regionManager = regionManager;
            _customLoggerFacade = customLoggerFacade;

            //Set my local application key
            _moduleKey = AppModuleIds.BloodVolumeAnalysis;
            _setupViewKey = GetHashCode().ToString();

            navigationCoordinator.RegisterAppModuleNavigator(_moduleKey, this);
            NavigationCoordinator = navigationCoordinator;
        }

        #endregion

        #region Properties

        public bool IsActive
        {
            get
            {
                return NavigationCoordinator.ActiveAppModuleKey.Equals(_moduleKey);
            }
        }
        public INavigationCoordinator NavigationCoordinator
        {
            get;
            private set;
        }
        public string AppModuleKey  //Unique application module key
        {
            get { return _moduleKey; }
        }
        public FrameworkElement RootViewHost
        {
            get;
            private set;
        }
        public IRegion OuterRegion //Region switches TestSelection, TestSetup, TestReview screens
        {
            get;
            private set;
        }
        public IRegion InnerRegion //Region switches PatientDemographics, TestParameters, SampleValues screens
        {
            get;
            private set;
        }
        public IRegion PopupRegion
        {
            get;
            private set;
        } //Region sits on top of OutterRegion => InnerRegion
        #endregion

        #region Methods

        public bool CanDeactivate()
        {
            return true;
        }
        public bool CanActivate()
        {
            return true;
        }

        public void Activate(DisplayViewOption displayViewOption, object payload)
        {
            ThrowsExceptionIfNotInitialized();
            _wasActivatedExternally = true;

            //Fire aggregate event to display MainMenuDropDown part of application
            _eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(ApplicationParts.MainMenuDropDown, true));
            _eventAggregator.GetEvent<ActiveViewTitleChanged>().Publish("Blood Volume Analysis");

            //Select display option
            switch (displayViewOption)
            {
                case DisplayViewOption.Result:
                    ActivateView(BVAModuleViewKeys.TestSampleValues, payload);
                    break;

                default:
                    //ActivateInternalView(BVModuleViewKeys.TestSelection, payload);
                    if (_currentViewKey == BVAModuleViewKeys.Empty)
                        ActivateView(BVAModuleViewKeys.TestSelection, payload);
                    else
                        ActivateView(_currentViewKey, payload);
                    break;
            }
        }
        public void Deactivate(Action onDeactivateCallback)
        {
            //Hide current active keyboard
            VirtualKeyboardManager.CloseActiveKeyboard();

            //View needs to be deactivated
            IRegion cViewRegion = null;
            FrameworkElement cView = GetKeyedViewWithRegion(_currentViewKey, out cViewRegion);
            if (cViewRegion != null && cView != null)
            {
                cViewRegion.Deactivate(cView);
            }

            _wasActivatedExternally = false;
            //Callback
            onDeactivateCallback();

        }

        public void ActivateView(string viewKey, object payload)
        {
            //Close active keyboard when trying to navigate to some screen
            VirtualKeyboardManager.CloseActiveKeyboard();

            if (_wasActivatedExternally == false && IsActive == false)
                return;

            //Throws exception if view/viewModels are not initialized
            ThrowsExceptionIfNotInitialized();
            
            if (viewKey == BVAModuleViewKeys.Empty)
                throw new ArgumentException("BVApplicationNavigator. View key is not valid. Provide a valid key.");

            //Notify of AppModuleModelChanged
            if (payload != null)
                _eventAggregator.GetEvent<AppModuleModelChanged>().Publish(new ModelChangedPayload(_moduleKey, payload));

            //Gets view instance based on keyed object
            IRegion requestViewRegion = null;
            FrameworkElement requestedView = GetKeyedViewWithRegion(viewKey, out requestViewRegion);
            if (requestedView == null)
            {
                throw new NullReferenceException("BVAModuleNavigator. Requested view |" + viewKey + "| cannot be found");
            }
            
            //Don't continue navigation process if a view is already active
            if (requestViewRegion != null && requestViewRegion.ActiveViews.Contains(requestedView))
            {
                return;
            }

            //Need to navigate to outer region
            if (requestViewRegion == OuterRegion)
            {
                //First need to deactivate currentView inside the innerRegion
                object view = InnerRegion.GetView(_currentViewKey);
                if (view != null)
                    InnerRegion.Deactivate(view);
            }
            else if (requestViewRegion == InnerRegion)
            {
                //Ensures that TestSetupView is activated first. InnerRegion is part of TestSetupView
                if (!OuterRegion.ActiveViews.Contains(OuterRegion.GetView(_setupViewKey)))
                {
                    OuterRegion.Activate(OuterRegion.GetView(_setupViewKey));
                }
            }

            //Activate view in requested region
            requestViewRegion.Activate(requestedView);
            EnsureRootViewRegionExistence();
            _currentViewKey = viewKey;
        }
        public void ActivateView(string viewKey)
        {
            ActivateView(viewKey, null);
        }

        public void Initialize()
        {
            if (_isInitialized) return;
            try
            {
                InitViews(true);
                _isInitialized = true;
            }
            catch (Exception ex)
            {
                _isInitialized = false;
                Debug.Print(ex.Message);
                _customLoggerFacade.Log(ex.Message, Microsoft.Practices.Composite.Logging.Category.Exception, Microsoft.Practices.Composite.Logging.Priority.None);
                throw new ApplicationException(ex.InnerException.Message);
            }
        }

        #endregion

        #region Helpers

        private FrameworkElement GetKeyedViewWithRegion(string uniqueViewKeyBetweenRegions, out IRegion viewRegion)
        {
            object view = null;
            viewRegion = null;

            if (OuterRegion != null)
            {
                view = OuterRegion.GetView(uniqueViewKeyBetweenRegions);
                viewRegion = OuterRegion;
            }

            if (view == null && InnerRegion != null)
            {
                view = InnerRegion.GetView(uniqueViewKeyBetweenRegions);
                viewRegion = InnerRegion;
            }
            if (view == null)
            {
                viewRegion = null;
                return null;
            }
            else
            {
                return (FrameworkElement)view;
            }
        }
        private void EnsureRootViewRegionExistence()
        {
            IRegion workspaceRegion = _regionManager.Regions[RegionNames.WorkspaceRegion];

            //Check if the View was added to the region; if not add and activate
            if (!workspaceRegion.Views.Contains(RootViewHost))
            {
                workspaceRegion.Add(RootViewHost);

            }
            workspaceRegion.Activate(RootViewHost);
        }
        private void ThrowsExceptionIfNotInitialized()
        {
            if (!_isInitialized)
            {
                throw new ApplicationException("BVApplicationNavigator is not initialized.");
            }
        }

        /// <summary>
        /// Initialize all UI screens for BVA module.
        /// </summary>
        /// <param name="withDI">
        /// True  : every UI screen will be initialized with Dependency Injection using Unity container
        /// False : every UI screen will be newed(Ui screen = new Ui())
        ///  </param>
        private void InitViews(bool withDI)
        {
            //FrameworkElement selectionView, setupView, reviewView;
            //FrameworkElement demographicsView, parametersView, sampleValuesView;
           
            //Views belong to the outer region
            //selectionView = _container.Resolve<TestSelectionView>();
            //setupView = _container.Resolve<TestSetupView>();
            //reviewView = _container.Resolve<TestReviewView>();

            //Views belong to the inner region
            //demographicsView = _container.Resolve<PatientDemographicsView>();
            //parametersView = _container.Resolve<TestParametersView>();
            //sampleValuesView = _container.Resolve<TestSampleValuesView>();

            //Get RootViewHost
            RootViewHost = _container.Resolve<BVAModuleViewHost>();

            //Create outer scoped region
            IRegionManager outerRegionMgr = _regionManager.Regions[RegionNames.WorkspaceRegion].Add(RootViewHost, String.Empty, true);
          
            OuterRegion = outerRegionMgr.Regions[ScopedRegionNames.BVAOuterRegion];
            PopupRegion = outerRegionMgr.Regions[ScopedRegionNames.BVAPopupRegion];

            //Register with container
            _container.RegisterInstance<IRegion>(ScopedRegionNames.BVAPopupRegion, PopupRegion, new ContainerControlledLifetimeManager());
            

            //First screen for UI
            //Views belong to the outer region
            //selectionView = 
            OuterRegion.Add(_container.Resolve<TestSelectionView>(), BVAModuleViewKeys.TestSelection);

            //Create inner scoped region
            IRegionManager innerRegionMgr = outerRegionMgr.Regions[ScopedRegionNames.BVAOuterRegion].Add(_container.Resolve<TestSetupView>(), _setupViewKey, true);
            InnerRegion = innerRegionMgr.Regions[ScopedRegionNames.BVAInnerRegion];

            InnerRegion.Add(_container.Resolve<PatientDemographicsView>(), BVAModuleViewKeys.PatientDemographics);
            InnerRegion.Add(_container.Resolve<TestParametersView>(), BVAModuleViewKeys.TestParameters);
            InnerRegion.Add(_container.Resolve<TestSampleValuesView>(), BVAModuleViewKeys.TestSampleValues);

            //Last UI screen of the application
            OuterRegion.Add(_container.Resolve<TestReviewView>(), BVAModuleViewKeys.TestReview);
        }

        #endregion
     
    }

    #region ViewKeys

    /// <summary>
    /// Unique view keys used to identify each UserControl.
    /// </summary>
    public static class BVAModuleViewKeys
    {
        public const string Empty = "Empty";
        public const string TestSelection = "TestSelection";
        public const string PatientDemographics = "PatientDemographics";
        public const string TestParameters = "TestParameters";
        public const string TestSampleValues = "TestSampleValues";
        public const string TestReview = "TestReview";
        public const string BloodPointsPopup = "BloodPointsPopup";
    }

    #endregion
}
