﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.BVA.Common
{
    /// <summary>
    /// BVA module internal regions.
    /// </summary>
    public static class ScopedRegionNames
    {
        public const string BVAOuterRegion = "BVAOuterRegion";
        public const string BVAInnerRegion = "BVAInnerRegion";
        public const string BVAPopupRegion = "BVAPopupRegion";
    }
}
