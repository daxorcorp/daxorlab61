﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.Extensions;

namespace Daxor.Lab.BVA.Common
{
    /// <summary>
    /// Formats labels/watermark to expected format defined by UX for all labels.
    /// </summary>
    public class LabelFormatter
    {
        public LabelFormatter(string label)
        {
            FormattedHeader = label.ToUpperInvariant();
            FormattedWatermark = label.ToLowerInvariant().ToFirstUpperInvariant();
        }

        public String FormattedHeader { get; private set; }
        public String FormattedWatermark { get; private set; }
    }
}
