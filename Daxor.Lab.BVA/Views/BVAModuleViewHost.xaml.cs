﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Composite;
using Telerik.Windows.Controls;
using Daxor.Lab.Infrastructure.TouchClick;

namespace Daxor.Lab.BVA
{
    /// <summary>
    /// Interaction logic for ModuleViewsHost.xaml
    /// Visual Host for the BloodVolumeAnalysis app.
    /// </summary>
    public partial class BVAModuleViewHost : UserControl
    {
        static Delegate buttonClickHandler = new RoutedEventHandler((o, arg) => TouchClick.Click());
       
        public BVAModuleViewHost()
        {
            InitializeComponent();
            EventManager.RegisterClassHandler(typeof(RadRowItem), RadRowItem.PreviewMouseLeftButtonDownEvent, buttonClickHandler);
        }
    }
}
