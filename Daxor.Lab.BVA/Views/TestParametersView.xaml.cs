﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Infrastructure.Controls;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.Practices.Composite;
using Daxor.Lab.Infrastructure.TouchClick;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Views
{
    /// <summary>
    /// Interaction logic for TestParametersView.xaml
    /// </summary>
    public partial class TestParametersView : UserControl, IActiveAware
    {
        #region Fields

        private bool _isActive;
        private readonly TestParametersViewModel _viewModel;
        private readonly PropertyObserver<TestParametersViewModel> _testParamObserver;

        #endregion

        #region Ctor

        public TestParametersView()
        {
            InitializeComponent();
            EventManager.RegisterClassHandler(typeof(ModuleSwitcher), ModuleSwitcher.CurrentModuleChangedEvent, new RoutedEventHandler(ModuleChanged));

            // For handling scanner input, we need to know what scanner-friendly fields
            // get/lose focus.
          
            // The referring physician field uses a pick list, so we need to make sure
            // the caret is in the correct position.
            uiReferringPhysician.TextChanged += uiReferringPhysician_TextChanged;
            uiCcReportTo.TextChanged += uiCcReportTo_TextChanged;
        }

        public TestParametersView(TestParametersViewModel viewModel) : this()
        {
            _viewModel = viewModel;
            DataContext = viewModel;
            _testParamObserver = new PropertyObserver<TestParametersViewModel>(viewModel);
            _testParamObserver.RegisterHandler(vm => vm.DesiredTestMode, OnDesiredTestModeChange);
        }
       
        #endregion
        
        #region IActiveAware

        public event EventHandler IsActiveChanged = delegate { };
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                if (value && _viewModel.CurrentTest != null && _viewModel.CurrentTest.Status == Domain.Common.TestStatus.Pending
                    && String.IsNullOrEmpty(_viewModel.CurrentTest.RefPhysician))
                {
                    //Disable sound manager when manually trying to set focus on a given textbox element
                    TouchClick.SetIsSoundEnabled(uiReferringPhysician, false);
                    uiReferringPhysician.Focus();
                    Keyboard.Focus(uiReferringPhysician);
                    TouchClick.SetIsSoundEnabled(uiReferringPhysician, true);
                }
                else if (!value)
                    VirtualKeyboardManager.CloseActiveKeyboard();

                _isActive = value;
                IActiveAware activeAware = DataContext as IActiveAware;
                if (activeAware != null)
                {
                    activeAware.IsActive = value;
                }
            }
        }
        
        #endregion

        #region Event handlers

        /// <summary>
        /// Handles the DropDownOpened event for combo boxes on the test parameters view
        /// by closing the active keyboard.
        /// </summary>
        /// <param name="sender">Combo box being opened</param>
        /// <param name="e">Arguments for the event</param>
        void ComboBox_DropDownOpened(object sender, EventArgs e)
        {
            VirtualKeyboardManager.CloseActiveKeyboard();
        }

        /// <summary>
        /// Handles the SelectionChanged for the test selection combo box by undoing
        /// the change if requested by the view model.
        /// </summary>
        /// <remarks>Use case: User has injectate lot entered, but wants to change 
        /// the test mode to "Training". User is warned that the injectate lot information
        /// will be cleared if he/she proceeds. User is presented with a dialog box: "Yes"
        /// to lose the lot and change the mode; "No" to keep the mode the same. This method
        /// handles the "No" scenario, so the combo box has to be reverted to the original
        /// state.</remarks>
        /// <param name="sender">Combo box whose selection is changing</param>
        /// <param name="e">Arguments for the event</param>
        void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // Note from Geoff: There is no PreviewSelectionChanged for the ComboBox
            // control. This method of communicating between the code-behind and the
            // view model is a workaround to allow UI changes to be undone.
            // 
            // Reference: http://amazedsaint.blogspot.com/2008/06/wpf-combo-box-cancelling-selection.html
            //
            // Note: The blog post mentions using e.RemovedItems[0], but I found this
            // wasn't working reliably.

            TestParametersViewModel viewModel = (TestParametersViewModel)DataContext;

            // The view model is requesting that the selection-change be prevented.
            if (viewModel.PreventSelectionFromChangingTestMode)
            {
                ComboBox combo = (ComboBox)sender;

                // We're currently handling this, so revert the flag back to its initial state.
                viewModel.PreventSelectionFromChangingTestMode = false;

                // Let the view model know the event handler is triggering the change, not the user.
                viewModel.IsComboBoxHandlerTriggeringTestModeChange = true;

                // Find which item the user had selected before this and then set the selected
                // index back to that value.
                if (e.RemovedItems.Count != 0)
                {
                    for (int i = 0; i < combo.Items.Count; i++)
                    {
                        if (combo.Items[i] == e.RemovedItems[0])
                        {
                            combo.SelectedIndex = i;
                            break;
                        }
                    }
                }

                // The view model has already been notified of the change, so revert the
                // flag back to its initial state.
                viewModel.IsComboBoxHandlerTriggeringTestModeChange = false;
            }
        }

        void ModuleChanged(object sender, RoutedEventArgs e)
        {
            if (e.Source.GetType().Equals(typeof(BVAModuleViewHost)) && IsActive)
            {
                IsActive = true;
                e.Handled = true;
            }
        }

        /// <summary>
        /// Handles the GotFocus event for a text box by letting the FocusHelper know that
        /// the given text box now has focus.
        /// </summary>
        /// <param name="sender">Text box gaining focus</param>
        /// <param name="e">Arguments for the event</param>
        void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            FocusHelper.SetIsFocused((DependencyObject)sender, true);
        }

        /// <summary>
        /// Handles the LostFocus event for a text box by letting the FocusHelper know that
        /// the given text box no longer has focus.
        /// </summary>
        /// <param name="sender">Text box losing focus</param>
        /// <param name="e">Arguments for the event</param>
        void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            FocusHelper.SetIsFocused((DependencyObject)sender, false);
        }

        /// <summary>
        /// Delegate called when uiPatientID text is changed, places cursor at the end of uiPatientID
        /// </summary>
        void uiReferringPhysician_TextChanged(object sender, TextChangedEventArgs e)
        {
            uiReferringPhysician.CaretIndex = uiReferringPhysician.Text.Length;
        }

        void uiCcReportTo_TextChanged(object sender, TextChangedEventArgs e)
        {
            uiCcReportTo.CaretIndex = uiCcReportTo.Text.Length;
        }
        #endregion

        #region Helpers

        void OnDesiredTestModeChange(TestParametersViewModel viewModel)
        {
            if (viewModel == null || viewModel.CurrentTest == null)
                return;
            
            // Open login keyboard only if not already a VOPS test
            if (viewModel.CurrentTest.Mode != Core.TestMode.VOPS && viewModel.DesiredTestMode == Core.TestMode.VOPS)
                VirtualKeyboardManager.OpenKeyboard(uiVopsPasswordBox, null);
        }

        #endregion

        private void HeaderedTextBox_PreviewLostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            BindingExpression be = ((FrameworkElement)sender).GetBindingExpression(HeaderedTextBox.TextProperty);
            be.UpdateSource();

            if (_viewModel != null && _viewModel.UserWantsToPerformQc)
            {
                e.Handled = true;
            }
        }

        private void HeaderedTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            BindingExpression be = ((FrameworkElement)sender).GetBindingExpression(HeaderedTextBox.TextProperty);
            be.UpdateSource();
        }
    }
}
