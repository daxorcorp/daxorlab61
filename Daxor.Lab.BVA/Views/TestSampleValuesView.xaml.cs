﻿using System;
using System.Windows.Controls;
using Daxor.Lab.BVA.ViewModels;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.BVA.Views
{
    /// <summary>
    /// Interaction logic for TestSampleValuesView.xaml
    /// </summary>
    public partial class TestSampleValuesView : UserControl, IActiveAware
    {
        TestSampleValuesViewModel _vm;

        #region Ctor

        public TestSampleValuesView()
        {
            InitializeComponent();
        }
        public TestSampleValuesView(TestSampleValuesViewModel vm) : this()
        {
            _vm = vm;
            DataContext = _vm;
            _vm.PropertyChanged += vm_PropertyChanged;
        }

        #endregion

        void vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            //Refreshing dataGridItems here in order to re-draw the last two columns
            //Bug in Microsoft DataGrid implementation, will be fixed in .NET 4.1 (AK 6/8/2011)
            if (e.PropertyName.Equals("AreCountsAndUnadjustedVolumesVisibile"))
            {
                if (_vm.AreCountsAndUnadjustedVolumesVisibile)
                {
                    Dispatcher.Invoke((Action)delegate
                    {
                        uiCountsTemplateColumn.Visibility = System.Windows.Visibility.Visible;
                        uiUBVTemplateColumn.Visibility = System.Windows.Visibility.Visible;
                    });
                }
                else
                {
                    Dispatcher.Invoke((Action)delegate
                    {
                        uiCountsTemplateColumn.Visibility = System.Windows.Visibility.Hidden;
                        uiUBVTemplateColumn.Visibility = System.Windows.Visibility.Hidden;
                    });

                }
            }
        }
        
        #region IActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;

                uiSampleValuesDataGrid.IsTabStop = value;
                IActiveAware vmAware = DataContext as IActiveAware;
                if (vmAware != null)
                {
                    vmAware.IsActive = value;
                }

            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
