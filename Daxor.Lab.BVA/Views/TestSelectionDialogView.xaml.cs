﻿using System.Windows.Controls;
using Daxor.Lab.BVA.ViewModels;

namespace Daxor.Lab.BVA.Views
{
    /// <summary>
    /// Interaction logic for TestSelectionDialogInterface.xaml
    /// </summary>
    public partial class TestSelectionDialogView : UserControl
    {
        #region Ctor

        public TestSelectionDialogView()
        {
            InitializeComponent();

         
        }
        public TestSelectionDialogView(TestSelectionDialogViewModel vm)
            : this()
        {
            DataContext = vm;
        }

        #endregion
    }
}
