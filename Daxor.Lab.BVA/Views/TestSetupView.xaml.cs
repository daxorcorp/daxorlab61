﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Daxor.Lab.BVA.ViewModels;
using Microsoft.Practices.Composite;
using System.Diagnostics;

namespace Daxor.Lab.BVA.Views
{
    /// <summary>
    /// Interaction logic for TestSetupView.xaml
    /// </summary>
    public partial class TestSetupView : UserControl, IActiveAware
    {
        #region Ctor

        public TestSetupView()
        {
            InitializeComponent();
        }
        public TestSetupView(TestSetupViewModel vm) : this()
        {
            DataContext = vm;
        }

        #endregion

        #region IActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                IActiveAware vmAware = DataContext as IActiveAware;
                if (vmAware != null)
                {
                    vmAware.IsActive = value;
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
