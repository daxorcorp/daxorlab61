﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Daxor.Lab.Infrastructure.Keyboards;
using System.Windows.Data;

namespace Daxor.Lab.BVA.Views
{
    /// <summary>
    /// Interaction logic for TestAnalysisViewModel.xaml
    /// </summary>
    public partial class TestAnalysisView : UserControl
    {
        public TestAnalysisView()
        {
            InitializeComponent();
            
           //Custom template applies to ChartArea -- Telerik Chart does not support 
           //this type of modification to the style in markup (XAML). Have to do in code. AK-9/25/2011
            ApplyStyle();
        }
        
        private void uiSideTabs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //Close any active keyboard (if one exist).. primarly used for Notes keyboard
            VirtualKeyboardManager.CloseActiveKeyboard();
        }

        private void Grid_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            uiSideTabs.SelectedIndex = 0;
        }

        private void ApplyStyle()
        {
            
            uiRegressionRadChart.DefaultView.ChartArea.AxisY.AxisStyles.AlternateStripLineStyle = Resources["HorizontalWhiteStripLineStyle"] as Style;
            uiRegressionRadChart.DefaultView.ChartArea.AxisY.AxisStyles.GridLineStyle = Resources["GridLineStyle"] as Style;
            uiRegressionRadChart.DefaultView.ChartArea.AxisX.AxisStyles.GridLineStyle = Resources["GridLineStyle"] as Style;

            //Remove data label from the last element
            uiRegressionRadChart.DataBound += (o, e) =>
            {
                if (uiRegressionRadChart.DefaultView.ChartArea.DataSeries.Count == 1)
                    return;

                var lineDS = uiRegressionRadChart.DefaultView.ChartArea.DataSeries.FirstOrDefault();
                if (lineDS != null && lineDS.Count >= 2)
                {
                    for (int i = 1; i < lineDS.Count; i++)
                        lineDS[i].Label = " ";
                }
            };
        }
    }
}
