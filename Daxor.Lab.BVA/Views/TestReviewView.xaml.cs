﻿using System;
using System.Windows.Controls;
using Daxor.Lab.BVA.ViewModels;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.BVA.Views
{
    /// <summary>
    /// Interaction logic for ReviewTestSetupView.xaml
    /// </summary>
    public partial class TestReviewView : UserControl, IActiveAware
    {
        #region Ctor

        public TestReviewView()
        {
            InitializeComponent();
        }
        public TestReviewView(TestReviewViewModel vm) : this()
        {
            DataContext = vm;
        }

        #endregion

        #region IActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                IActiveAware vmAware = DataContext as IActiveAware;
                if (vmAware != null)
                {
                    vmAware.IsActive = value;
                }
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
