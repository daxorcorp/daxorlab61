﻿using System;
using System.Windows;
using System.Windows.Controls;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Keyboards;
using Microsoft.Practices.Composite.Events;
using IActiveAware = Microsoft.Practices.Composite.IActiveAware;

namespace Daxor.Lab.BVA.Views
{
    /// <summary>
    /// Interaction logic for TestSelectionView.xaml
    /// </summary>
    public partial class TestSelectionView : IActiveAware
    {
        #region Fields

        private ScrollViewer _scrollViewer;
        private readonly IEventAggregator _eventAggregator;

        #endregion

        #region Ctors

        public TestSelectionView()
        {
            InitializeComponent();
            Loaded += OnTestSelectionViewLoaded;
        }

        public TestSelectionView(TestSelectionViewModel vm, IEventAggregator eventAggregator) : this()
        {
            _eventAggregator = eventAggregator;
            DataContext = vm;
        }

        #endregion

        #region Event handlers
       
        /// <summary>
        /// Handles the Loaded event for the view by setting up event handlers
        /// for the DataLoaded and ScrollChanged events on the ScrollViewer so
        /// that aggregate events can be published, thereby facilitating communication
        /// with the view model.
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        void OnTestSelectionViewLoaded(object sender, RoutedEventArgs e)
        {
            _scrollViewer = (ScrollViewer)MainTelerikGrid.Template.FindName("PART_ItemsScrollViewer", MainTelerikGrid);
            if (_scrollViewer == null) throw new NullReferenceException("PART_ItemsScrollViewer is not present in visual tree");

            MainTelerikGrid.DataLoaded += (s, e1) => _eventAggregator.GetEvent<ScrollViewerPageInfoChanged>()
                .Publish(new ScrollViewerPageInfoChangedPayload(MainTelerikGrid.Items.Count, MainTelerikGrid.Items.TotalItemCount, _scrollViewer.ExtentHeight, _scrollViewer.ViewportHeight, _scrollViewer.VerticalOffset, AppModuleIds.BloodVolumeAnalysis));
           
            _scrollViewer.ScrollChanged += (s, e2) => _eventAggregator.GetEvent<ScrollViewerPageInfoChanged>()
                .Publish(new ScrollViewerPageInfoChangedPayload(MainTelerikGrid.Items.Count, MainTelerikGrid.Items.TotalItemCount, _scrollViewer.ExtentHeight, _scrollViewer.ViewportHeight, _scrollViewer.VerticalOffset, AppModuleIds.BloodVolumeAnalysis));
        }

        /// <summary>
        /// Handler for the aggregate event published when the scroll changes.
        /// The events are essentially forwarded to the actual scroll viewer (UI component).
        /// </summary>
        /// <param name="scrollEvent"></param>
        void OnScrollViewerScrollChanged(ScrollViewerScrollChangedEventPayload scrollEvent)
        {
            switch (scrollEvent)
            {
                case ScrollViewerScrollChangedEventPayload.ScrollTop:
                    _scrollViewer.ScrollToTop();
                    break;
                case ScrollViewerScrollChangedEventPayload.ScrollPageUp:
                    _scrollViewer.PageUp();
                    break;
                case ScrollViewerScrollChangedEventPayload.ScrollPageDown:
                    _scrollViewer.PageDown();
                    break;
                case ScrollViewerScrollChangedEventPayload.ScrollBottom:
                    _scrollViewer.ScrollToBottom();
                    break;
            }
        }
        
        void uiSearchButton_Click(object sender, RoutedEventArgs e)
        {
            VirtualKeyboardManager.OpenKeyboard(uiSearchText, e);
        }
        
        #endregion

        #region IActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                var vmAware = DataContext as IActiveAware;
                if (vmAware != null)
                    vmAware.IsActive = value;

                if (value) _eventAggregator.GetEvent<ScrollViewerScrollChanged>().Subscribe(OnScrollViewerScrollChanged);
                else _eventAggregator.GetEvent<ScrollViewerScrollChanged>().Unsubscribe(OnScrollViewerScrollChanged);
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion
    }
}
