﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Infrastructure.Controls;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.TouchClick;
using Microsoft.Practices.Composite;

namespace Daxor.Lab.BVA.Views
{
    /// <summary>
    /// Interaction logic for PatientDemographicsView.xaml
    /// </summary>
    public partial class PatientDemographicsView : IActiveAware
    {
        #region Fields

        readonly PatientDemographicsViewModel _vm;

        #endregion

        #region Ctor

        public PatientDemographicsView()
        {
            InitializeComponent();

            //Hook up to ModuleSwitcher routed event
            EventManager.RegisterClassHandler(typeof(ModuleSwitcher), ModuleSwitcher.CurrentModuleChangedEvent, new RoutedEventHandler(ModuleChanged));
           
            //Hooksup uiPatientId headeredtextbox to receive the following events:
            //TextChanged, LostFocus, GotFocus
            uiPatientId.TextChanged += OnUiPatientIdTextChanged;
            uiPatientId.LostFocus += OnUiPatientIdLostFocus;
            uiPatientId.GotFocus += OnUiPatientIdGotFocus;

            // We need to know if the height/weight text changes so that the
            // caret can be positioned correctly.
            uiHeight.TextChanged += OnUiHeightTextChanged;
            uiWeight.TextChanged += OnUiWeightTextChanged;
        }
        public PatientDemographicsView(PatientDemographicsViewModel vm) : this()
        {
            _vm = vm;
            DataContext = _vm;
        }

        #endregion

        #region EventHandlers

        /// <summary>
        /// Delegate called when moduleSwitcher switches to BVAModuleViewHost.
        /// </summary>
        void ModuleChanged(object sender, RoutedEventArgs e)
        {
            if (e.Source.GetType() == typeof(BVAModuleViewHost) && IsActive)
            {
                IsActive = true;
                e.Handled = true;
            }
        }
        /// <summary>
        /// Delegate called when uiPatientID loses focus.
        /// </summary>
        void OnUiPatientIdLostFocus(object sender, RoutedEventArgs e)
        {
            _vm.IsPatientIdFieldFocused = false;
        }
        /// <summary>
        /// Delegate called when uiPatientID gets Focus
        /// </summary>
        void OnUiPatientIdGotFocus(object sender, RoutedEventArgs e)
        {
            _vm.IsPatientIdFieldFocused = true;
        }
        /// <summary>
        /// Delegate called when uiPatientID text is changed, places cursor at the end of uiPatientID
        /// </summary>
        void OnUiPatientIdTextChanged(object sender, TextChangedEventArgs e)
        {
            if (_vm != null && _vm.SelectedHospitalPatientIdItem != null)
                uiPatientId.CaretIndex = uiPatientId.Text.Length;
        }

        /// <summary>
        /// Delegate called when the TextChanged event fires for the height textbox.
        /// </summary>
        /// <param name="sender">Sender of the TextChanged event</param>
        /// <param name="e">Event arguments</param>
        void OnUiHeightTextChanged(object sender, TextChangedEventArgs e)
        {
            // The height changed because the measurement system changed. Put the
            // caret position at the far right and clear the flag.
            if (_vm != null && _vm.IsMeasurementSystemChanging && uiHeight.IsFocused)
            {
                uiHeight.CaretIndex = uiHeight.Text.Length;
                _vm.IsMeasurementSystemChanging = false;
            }
        }

        /// <summary>
        /// Delegate called when the TextChanged event fires for the weight textbox.
        /// </summary>
        /// <param name="sender">Sender of the TextChanged event</param>
        /// <param name="e">Event arguments</param>
        void OnUiWeightTextChanged(object sender, TextChangedEventArgs e)
        {
            // The weight changed because the measurement system changed. Put the
            // caret position at the far right and clear the flag.
            if (_vm != null && _vm.IsMeasurementSystemChanging && uiWeight.IsFocused)
            {
                uiWeight.CaretIndex = uiWeight.Text.Length;
                _vm.IsMeasurementSystemChanging = false;
            }
        }

        #endregion

        #region IActiveAware

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                //Force patient hospital id control to acquire focus
                if (value && _vm.CurrentTest != null && _vm.CurrentTest.Status == Domain.Common.TestStatus.Pending
                    && String.IsNullOrWhiteSpace(_vm.CurrentTest.Patient.HospitalPatientId))
                {
                    //Disable and enable sound 
                    TouchClick.SetIsSoundEnabled(uiPatientId, false);
                    uiPatientId.Focus();
                    Keyboard.Focus(uiPatientId);
                    TouchClick.SetIsSoundEnabled(uiPatientId, true);
                }
                else if(!value)
                    VirtualKeyboardManager.CloseActiveKeyboard();

                _isActive = value;
                var activeAware = DataContext as IActiveAware;
                if (activeAware != null)
                    activeAware.IsActive = value;
            }
        }

        public event EventHandler IsActiveChanged = delegate { };
        #endregion
    }
}
