﻿#pragma checksum "..\..\..\Views\PatientDemographicsView.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7FF1E448E49F97843DE7B63B69D0815DDA242437"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Daxor.Lab.BVA.Common;
using Daxor.Lab.Infrastructure.Commands;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Controls;
using Daxor.Lab.Infrastructure.Converters;
using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.RuleFramework;
using Daxor.Lab.Infrastructure.TouchClick;
using Daxor.Lab.Infrastructure.UserControls;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.Expression.Interactivity.Core;
using Microsoft.Expression.Interactivity.Input;
using Microsoft.Expression.Interactivity.Layout;
using Microsoft.Expression.Interactivity.Media;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Daxor.Lab.BVA.Views {
    
    
    /// <summary>
    /// PatientDemographicsView
    /// </summary>
    public partial class PatientDemographicsView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid uiMainGrid;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.VisualStateGroup States;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.VisualState Default;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.VisualState StateWeightWarning;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button NextScreenButton;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Daxor.Lab.Infrastructure.Controls.HeaderedTextBox uiPatientId;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Daxor.Lab.Infrastructure.Controls.HeaderedTextBox textBoxID2;
        
        #line default
        #line hidden
        
        
        #line 100 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Daxor.Lab.Infrastructure.Controls.HeaderedTextBox DateOfBirth;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Daxor.Lab.Infrastructure.Controls.HeaderedTextBox LastName;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Daxor.Lab.Infrastructure.Controls.HeaderedTextBox FirstName;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Daxor.Lab.Infrastructure.Controls.HeaderedTextBox uiHeight;
        
        #line default
        #line hidden
        
        
        #line 201 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Daxor.Lab.Infrastructure.Controls.HeaderedTextBox uiWeight;
        
        #line default
        #line hidden
        
        
        #line 238 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CorrectionFactorComboBox;
        
        #line default
        #line hidden
        
        
        #line 288 "..\..\..\Views\PatientDemographicsView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Path uiAlert;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Daxor.Lab.BVA;component/views/patientdemographicsview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\PatientDemographicsView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.uiMainGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.States = ((System.Windows.VisualStateGroup)(target));
            return;
            case 3:
            this.Default = ((System.Windows.VisualState)(target));
            return;
            case 4:
            this.StateWeightWarning = ((System.Windows.VisualState)(target));
            return;
            case 5:
            this.NextScreenButton = ((System.Windows.Controls.Button)(target));
            return;
            case 6:
            this.uiPatientId = ((Daxor.Lab.Infrastructure.Controls.HeaderedTextBox)(target));
            return;
            case 7:
            this.textBoxID2 = ((Daxor.Lab.Infrastructure.Controls.HeaderedTextBox)(target));
            return;
            case 8:
            this.DateOfBirth = ((Daxor.Lab.Infrastructure.Controls.HeaderedTextBox)(target));
            return;
            case 9:
            this.LastName = ((Daxor.Lab.Infrastructure.Controls.HeaderedTextBox)(target));
            return;
            case 10:
            this.FirstName = ((Daxor.Lab.Infrastructure.Controls.HeaderedTextBox)(target));
            return;
            case 11:
            this.uiHeight = ((Daxor.Lab.Infrastructure.Controls.HeaderedTextBox)(target));
            return;
            case 12:
            this.uiWeight = ((Daxor.Lab.Infrastructure.Controls.HeaderedTextBox)(target));
            return;
            case 13:
            this.CorrectionFactorComboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 14:
            this.uiAlert = ((System.Windows.Shapes.Path)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

