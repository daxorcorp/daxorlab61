﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Daxor.Lab.BVA.SubViews
{
    /// <summary>
    /// Interaction logic for BVBarChart.xaml
    /// </summary>
    public partial class BVBarChart : UserControl
    {
        public BVBarChart()
        {
            InitializeComponent();
            
            // Note from Geoff: This pretty much mimics what the actual Test Analysis view does,
            // however, I couldn't get anything to display in the XAML designer. At least this 
            // revision doesn't cause the XAML designer to throw an exception (like the previous
            // version).
            //DataContext = new TestVolumeCollection(); // Testing purposes
        }
    }

    #region UI Design Collection
    public class TestVolumeCollection : Dictionary<Core.BloodType, TestVolume>
    {
        public TestVolumeCollection()
        {
            base.Add(Core.BloodType.Measured, new TestVolume() { RedCellVolume = 10000, PlasmaVolume = 4432 });
            base.Add(Core.BloodType.Ideal, new TestVolume() { RedCellVolume = 1560, PlasmaVolume = 15000 });
        }
    }
    public class TestVolume
    {
        public int RedCellVolume
        {
            get;
            set;
        }
        public int PlasmaVolume
        {

            get;
            set;
        }
        public int WholeVolume
        {
            get { return RedCellVolume + PlasmaVolume; }
        }
    }

    #endregion
}
