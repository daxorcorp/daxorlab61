﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Daxor.Lab.BVA.SubViews
{
    /// <summary>
    /// Interaction logic for ExampleTestSetup.xaml
    /// </summary>
    public partial class VerifyTestSetupContent : UserControl
    {
        public VerifyTestSetupContent()
        {
            InitializeComponent();
        }

        // I know this is nasty to have on the code behind, but this view is only used for
        // a message box. GMazeroff 11/9/2011

        public VerifyTestSetupContent(Core.BVATest currentTest) : this()
        {
            uiCarousel.TestProtocol = currentTest.Protocol;
            uiPatientName.Text = currentTest.Patient.FullName;
            uiPatientID.Text = currentTest.Patient.HospitalPatientId;
            uiInjectateLotNumber.Text = currentTest.InjectateLot;
            uiNumberSamples.Text = currentTest.Protocol + " Patient Samples";
        }
    }
}
