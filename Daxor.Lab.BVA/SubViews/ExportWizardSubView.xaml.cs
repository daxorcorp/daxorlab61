﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Daxor.Lab.Infrastructure.Interfaces;

namespace Daxor.Lab.BVA.SubViews
{
    /// <summary>
    /// Interaction logic for ExportWizardSubView.xaml
    /// </summary>
    public partial class ExportWizardSubView : UserControl, IModalView
    {
        public ExportWizardSubView()
        {
            InitializeComponent();
            wizard.FinishClick += new RoutedEventHandler(wizard_FinishClick);
            this.Loaded += new RoutedEventHandler(ExportWizardSubView_Loaded);
        }

        void ExportWizardSubView_Loaded(object sender, RoutedEventArgs e)
        {
            this.DataContext = State;
        }

        void wizard_FinishClick(object sender, RoutedEventArgs e)
        {
            FireClosing();
        }

        public event EventHandler Closing;

        public object State
        {
            get;
            set;
        }

        private void FireClosing()
        {
            var handler = Closing;
            if (handler != null)
                handler(this, null);
        }
        
        public IEnumerable<string> Choices
        {
            get { return Enumerable.Empty<string>(); }
        }

        public int SelectedChoice
        {
            get;
            set;
        }
    }
}
