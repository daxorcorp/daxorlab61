﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Practices.Composite.Regions;
using Daxor.Lab.BVA.Common;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Unity;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.BVA.Services;

namespace Daxor.Lab.BVA.SubViews
{
    /// <summary>
    /// Interaction logic for BloodPointsZoomedSubView.xaml
    /// </summary>
    public partial class BloodPointsZoomedSubView : UserControl
    {
        private readonly IPopupController _popupController;
        public BloodPointsZoomedSubView()
        {
            InitializeComponent();

            //Remove data label from the last element
            uiRadChart.DataBound += (o, e) =>
            {
                if (uiRadChart.DefaultView.ChartArea.DataSeries.Count == 1)
                    return;

                var lineDS = uiRadChart.DefaultView.ChartArea.DataSeries.FirstOrDefault();
                if (lineDS != null && lineDS.Count >= 2)
                {
                    for (int i = 1; i < lineDS.Count; i++)
                        lineDS[i].Label = " ";
                }
            };
        }
        public BloodPointsZoomedSubView([Dependency(AppModuleIds.BloodVolumeAnalysis)] IPopupController popupController)
            : this()
        {
            _popupController = popupController;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _popupController.ClosePopup();
        }
    }
}
