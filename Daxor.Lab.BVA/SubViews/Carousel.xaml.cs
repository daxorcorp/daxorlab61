﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Daxor.Lab.BVA.SubViews
{
    /// <summary>
    /// Interaction logic for Carousel.xaml  
    //***********TODO: Need to make Carousel a WPF Control and move UI specific stuff into a ControlTemplate*********
    /// </summary>
    public partial class Carousel : UserControl
    {
        public Carousel()
        {
            InitializeComponent();
        }

        #region DependencyProperties

        #region RotateAnglePerPosition
        public double RotateAnglePerPosition
        {
            get { return (double)GetValue(RotateAnglePerPositionProperty); }
            set { SetValue(RotateAnglePerPositionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DistanceBetweenPositions.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RotateAnglePerPositionProperty =
            DependencyProperty.Register("RotateAnglePerPosition", typeof(double), typeof(Carousel), new UIPropertyMetadata(14.4));

        #endregion

        #region TotalPositions
        public int TotalPositions
        {
            get { return (int)GetValue(TotalPositionsProperty); }
            set { SetValue(TotalPositionsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TotalPositions.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TotalPositionsProperty =
            DependencyProperty.Register("TotalPositions", typeof(int), typeof(Carousel), new UIPropertyMetadata(0));
        #endregion

        #region CurrentPosition

        public int CurrentPosition
        {
            get { return (int)GetValue(CurrentPositionProperty); }
            set { SetValue(CurrentPositionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for CurrentPosition.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty CurrentPositionProperty =
            DependencyProperty.Register("CurrentPosition", typeof(int), typeof(Carousel), new UIPropertyMetadata(24, OnCurrentPositionChanged));

        #endregion

        #region IsAnimationRequired

        public bool IsAnimationRequired
        {
            get { return (bool)GetValue(IsAnimationRequiredProperty); }
            set { SetValue(IsAnimationRequiredProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsAnimationRequired.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsAnimationRequiredProperty =
            DependencyProperty.Register("IsAnimationRequired", typeof(bool), typeof(Carousel), new UIPropertyMetadata(false));

        #endregion

        #region PositionWithZeroAngle

        public int PositionWithZeroAngle
        {
            get { return (int)GetValue(PositionWithZeroAngleProperty); }
            set { SetValue(PositionWithZeroAngleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PositionWithZeroAngle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PositionWithZeroAngleProperty =
            DependencyProperty.Register("PositionWithZeroAngle", typeof(int), typeof(Carousel), new UIPropertyMetadata(24));

        #endregion

        #region TestProtocol


        public int TestProtocol
        {
            get { return (int)GetValue(TestProtocolProperty); }
            set { SetValue(TestProtocolProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TestProtocol.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TestProtocolProperty =
            DependencyProperty.Register("TestProtocol", typeof(int), typeof(Carousel), new UIPropertyMetadata(3, OnTestProtocolChanged, CoerceTestProtocol));

        #endregion

        #region Top/Middle/Bottom Text

        public string TopText
        {
            get { return (string)GetValue(TopTextProperty); }
            set { SetValue(TopTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TopText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TopTextProperty =
            DependencyProperty.Register("TopText", typeof(string), typeof(Carousel), new UIPropertyMetadata(String.Empty));



        public string MiddleText
        {
            get { return (string)GetValue(MiddleTextProperty); }
            set { SetValue(MiddleTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MiddleText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MiddleTextProperty =
            DependencyProperty.Register("MiddleText", typeof(string), typeof(Carousel), new UIPropertyMetadata(String.Empty));


        public string BottomText
        {
            get { return (string)GetValue(BottomTextProperty); }
            set { SetValue(BottomTextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for BottomText.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty BottomTextProperty =
            DependencyProperty.Register("BottomText", typeof(string), typeof(Carousel), new UIPropertyMetadata(String.Empty));



        #endregion

        #endregion

        #region DP Handlers
        static void OnCurrentPositionChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            Carousel THIS = sender as Carousel;
            int newPosition = (Int32)args.NewValue;
            if (newPosition > THIS.TotalPositions)
                return;

            //Move to a correct angle
            THIS.WheelRotateTransform.Angle = ((newPosition + (THIS.TotalPositions - THIS.PositionWithZeroAngle)) % THIS.TotalPositions) * THIS.RotateAnglePerPosition;
        }
        static void OnTestProtocolChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            Carousel THIS = sender as Carousel;

            //Hide existing protocol
            switch ((Int32)args.OldValue)
            {
                case 3:
                    THIS.uiCarouselBackground3.Visibility = Visibility.Collapsed;
                    break;
                case 4:
                    THIS.uiCarouselBackground4.Visibility = Visibility.Collapsed;
                    break;
                case 5:
                    THIS.uiCarouselBackground5.Visibility = Visibility.Collapsed;
                    break;
                case 6:
                    THIS.uiCarouselBackground6.Visibility = Visibility.Collapsed;
                    break;

                default: break;
            }


            //Set appropriate UI to visible
            switch ((Int32)args.NewValue)
            {
                case 3:
                    THIS.uiCarouselBackground3.Visibility = Visibility.Visible;
                    break;
                case 4:
                    THIS.uiCarouselBackground4.Visibility = Visibility.Visible;
                    break;
                case 5:
                    THIS.uiCarouselBackground5.Visibility = Visibility.Visible;
                    break;
                case 6:
                    THIS.uiCarouselBackground6.Visibility = Visibility.Visible;
                    break;

                default: break;
            }
        }
        static object CoerceTestProtocol(DependencyObject sender, object value)
        {
            Carousel THIS = sender as Carousel;
            int testProtocol = (Int32)value;
            if (testProtocol < 3) testProtocol = 3;
            if (testProtocol > 6) testProtocol = 6;

            return testProtocol;
        }
        #endregion
    }
}
