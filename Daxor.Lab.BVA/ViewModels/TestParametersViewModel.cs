﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Daxor.Lab.BVA.Common;
using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.BVA.ViewModels
{
	/// <summary>
	/// The test parameters view model
	/// </summary>
	public class TestParametersViewModel : ViewModelBase, IActiveAware, IDataErrorInfo
	{
		#region Fields

		private readonly Interfaces.IBloodVolumeTestController _controller;
		private readonly IAppModuleNavigator _localNavigator;
		private readonly IEventAggregator _eventAggregator;
		private readonly IMessageBoxDispatcher _messageBoxDispatcher;
		private readonly IPharmaceuticalKeyDecoder _keyDecoder;     //Decodes pharmaceutical key strings into Pharmaceutical instances
		private readonly ILotNumberGenerator _lotNumberGenerator;
		private readonly ILoggerFacade _logger;
		private readonly IAuthenticationManager _authManager;
		private readonly INavigationCoordinator _navigationCoordinator;
		private readonly IMessageManager _messageManager;
		private DelegateCommand<object> _navToMyselfCommand;
		private DelegateCommand<object> _navToSampleValuesCommand;
		private DelegateCommand<object> _loginCommand;

		private bool _isLoginVopsKeyboardOpen;
		private bool _injectateKeyHasFocus, _standardAKeyHasFocus, _standardBKeyHasFocus, _iLotEntryRequired, _userAcknowledgedIdenticalStandards;

		private BVATest _testModel;
		private BVAPatient _patientModel;
		private PropertyObserver<BVAPatient> _pObserver;
		private PropertyObserver<BVATest> _tObserver;

		private TestModeRecord _selectedTestModeRecord;

		private List<HematocritFillTypeRecord> _hematocritFills;
		private List<ProtocolRecord> _protocols;
		private List<TestModeRecord> _testModes;
		private string _selectedAnalyst;
		private LabelFormatter _locationLabel;
		private SettingObserver<ISettingsManager> _settingObserver;

		private string _injectateKeyLotNumber, _standardAKeyLotNumber, _standardBKeyLotNumber;
		private string _injectateDoseAdapter = string.Empty;
		private string _vopsPassword;
		private bool _userWantsToPerformQcTestNow;

		private readonly ISettingsManager _settingsManager;

		/// <summary>
		/// Denotes whether the SelectionChanged event handler in the code-behind should prevent 
		/// the selected item from changing. Used for communicating with the "SelectedTestModeRecord"
		/// property on this view model.
		/// </summary>
		internal bool PreventSelectionFromChangingTestMode;

		/// <summary>
		/// Denotes whether the SelectionChanged event handler is changing the selected index,
		/// which will trigger the "SelectedTestModeRecord" property on this view model.
		/// </summary>
		/// <remarks>
		/// Scenario: Injectate information was present, the user chose a different test
		/// mode, and then when asked whether to proceed with the change *decided not to
		/// change*. To keep the UI up-to-date, the combo box handler has to undo the
		/// selection by changing the selected item back to its previous state.
		/// </remarks>
		internal bool IsComboBoxHandlerTriggeringTestModeChange = false;

		/// <summary>
		/// Denotes whether the current test instance is being changed. (This impacts the
		/// SelectedTestModeRecord property.)
		/// </summary>
		internal bool IsCurrentTestInstanceChanging;

		private TestMode _desiredTestMode;

		#endregion

		#region Ctor

		public TestParametersViewModel([Dependency(AppModuleIds.BloodVolumeAnalysis)] IAppModuleNavigator localNavigator,
									   Interfaces.IBloodVolumeTestController controller,
									   IMessageBoxDispatcher messageBoxDispatcher,
									   IPharmaceuticalKeyDecoder keyDecoder,
									   ILotNumberGenerator lotNumberGenerator,
									   IAuthenticationManager authManager,
									   INavigationCoordinator navigationCoordinator,
									   ISettingsManager settingsManager,
									   IEventAggregator eventAggregator,
									   IMessageManager messageManager)
		{
			_localNavigator = localNavigator;
			_controller = controller;
			_messageBoxDispatcher = messageBoxDispatcher;
			_keyDecoder = keyDecoder;
			_lotNumberGenerator = lotNumberGenerator;
			_authManager = authManager;
			_settingsManager = settingsManager;
			_logger = _controller.Logger;
			_eventAggregator = eventAggregator;
			_navigationCoordinator = navigationCoordinator;
			_messageManager = messageManager;

			// ReSharper disable once DoNotCallOverridableMethodsInConstructor
			InitializeViewModel();
		}

		protected virtual void InitializeViewModel()
		{
			SubscribeToBvaTestSelectedEvent();
			NavigateToSampleValuesDelegateCommand = new DelegateCommand<object>(NavigateToSampleValuesCommandExecute, NavigateToSampleValuesCanExecute);
			NavigateToMyselfDelegateCommand = new DelegateCommand<object>(NavigateToMyselfCommandExecute, NavigateToMyselfCanExecute);
			LoginDelegateCommand = new DelegateCommand<object>(LoginCommandExecute, LoginCommandCanExecute);
			SettingObserver = new SettingObserver<ISettingsManager>(_settingsManager);

			ConfigureViewModel();
		}

		protected void SubscribeToBvaTestSelectedEvent()
		{
			var event1 = _eventAggregator.GetEvent<BvaTestSelected>();
			event1.Subscribe(BVATestSelectedEventHandler, ThreadOption.PublisherThread, false);
		}

		#endregion

		#region Properties

		/// <summary>
		/// Location label
		/// </summary>
		public LabelFormatter LocationLabel
		{
			get { return _locationLabel; }
			set { SetValue(ref _locationLabel, "LocationLabel", value); }
		}

		/// <summary>
		/// Selected referring physician
		/// </summary>
		private string _sReferringPhysician;
		public string SelectedReferringPhysician
		{
			get { return _sReferringPhysician; }
			set
			{
				_sReferringPhysician = value;

				if (CurrentTest != null && value != null)
					CurrentTest.RefPhysician = value;

				FirePropertyChanged("SelectedReferringPhysician");
			}
		}

		private string _selectedCcReportTo;
		public string SelectedCcReportTo
		{
			get { return _selectedCcReportTo; }
			set
			{
				_selectedCcReportTo = value;
				if (CurrentTest != null && value != null)
					CurrentTest.CcReportTo = value;

				FirePropertyChanged("SelectedCcReportTo");
			}
		}

		private List<string> _physiciansSpecialties;
		public List<string> PhysiciansSpecialties
		{
			get { return _physiciansSpecialties ?? (_physiciansSpecialties = _settingsManager.GetSetting<IEnumerable>(SettingKeys.BvaPhysiciansSpecialties).OfType<string>().ToList()); }
			set { SetValue(ref _physiciansSpecialties, "PhysiciansSpecialties", value); }
		}

		private readonly ObservableCollection<string> _referringPhysicians = new ObservableCollection<string>();
		public ObservableCollection<string> ReferringPhysicians
		{
			get { return _referringPhysicians; }
		}

		private readonly ObservableCollection<string> _analystsCollection = new ObservableCollection<string>();
		public string SelectedAnalyst
		{
			get { return _selectedAnalyst; }
			set
			{
				_selectedAnalyst = value;

				if (CurrentTest != null && value != null)
					CurrentTest.Analyst = value;

				FirePropertyChanged("SelectedAnalyst");
			}
		}
		public ObservableCollection<string> Analysts
		{
			get
			{
				return _analystsCollection;
			}
		}

		public string LoginVopsPassword
		{
			get { return _vopsPassword; }
			set
			{
				if (SetValue(ref _vopsPassword, "LoginVopsPassword", value))
					LoginDelegateCommand.RaiseCanExecuteChanged();
			}
		}
		public bool IsLoginVopsKeyboardOpen
		{
			get { return _isLoginVopsKeyboardOpen; }
			set
			{

				if (SetValue(ref _isLoginVopsKeyboardOpen, "IsLoginVopsKeyboardOpen", value) && !value)
				{
					DesiredTestMode = TestMode.Automatic;
					//Means it was open and now is closed
					_eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
				}
			}
		}

		public BVATest CurrentTest
		{
			get { return _testModel; }
			set
			{
				if (SetValue(ref _testModel, "CurrentTest", value))
					OnCurrentTestChanged(_testModel);
			}
		}

		public BVAPatient CurrentPatient
		{
			get { return _patientModel; }
			set
			{
				if (_patientModel == value) return;

				_patientModel = value;
				if (_patientModel != null)
				{
					_pObserver = new PropertyObserver<BVAPatient>(_patientModel);
					_pObserver.RegisterHandler(p => p.HospitalPatientId, p => NavigateToMyselfDelegateCommand.RaiseCanExecuteChanged());
				}
				FirePropertyChanged("CurrentPatient");
				NavigateToMyselfDelegateCommand.RaiseCanExecuteChanged();
			}
		}

		public string InjectateDoseAdapter
		{
			get { return _injectateDoseAdapter; }
			set
			{
				double dValue;
				if (!Double.TryParse(value, out dValue) || dValue <= 0)
				{
					value = string.Empty;
				}
				_injectateDoseAdapter = value;
				CurrentTest.InjectateDose = dValue;
				FirePropertyChanged("InjectateDoseAdapter");
			}
		}
		private string InjectateKeyLotNumber
		{
			get
			{
				if (!string.IsNullOrEmpty(CurrentTest.InjectateKey))
					return _injectateKeyLotNumber;

				_injectateKeyLotNumber = string.Empty;
				return _injectateKeyLotNumber;
			}
			set
			{
				CurrentTest.InjectateLot = value;
				_injectateKeyLotNumber = value;

			}
		}
		private string StandardAKeyLotNumber
		{
			get
			{
				if (!string.IsNullOrEmpty(CurrentTest.StandardAKey))
					return _standardAKeyLotNumber;

				_standardAKeyLotNumber = string.Empty;
				return _standardAKeyLotNumber;
			}
			set
			{
				_standardAKeyLotNumber = value;
			}
		}
		private string StandardBKeyLotNumber
		{
			get
			{
				if (!string.IsNullOrEmpty(CurrentTest.StandardBKey))
				{
					return _standardBKeyLotNumber;
				}
				_standardBKeyLotNumber = string.Empty;
				return _standardBKeyLotNumber;
			}
			set
			{
				_standardBKeyLotNumber = value;
			}
		}

		private SettingObserver<ISettingsManager> SettingObserver
		{
			get { return _settingObserver; }
			set { if (_settingObserver == null) _settingObserver = value; }
		}

		/// <summary>
		/// Gets the number of non-empty product keys (injectate, standard A, standard B) entered .
		/// </summary>
		/// <remarks>
		/// Minimum is zero (0), maximum is three (3).
		/// </remarks>
		private int NumberOfNonEmptyKeys
		{
			get
			{
				var numberOfNonEmptyKeys = 0;

				if (!string.IsNullOrEmpty(InjectateKeyLotNumber)) numberOfNonEmptyKeys++;
				if (!string.IsNullOrEmpty(StandardAKeyLotNumber)) numberOfNonEmptyKeys++;
				if (!string.IsNullOrEmpty(StandardBKeyLotNumber)) numberOfNonEmptyKeys++;

				return numberOfNonEmptyKeys;
			}
		}

		/// <summary>
		/// Gets the number of matching lot number pairs based on the product keys (injectate, 
		/// standard A, standard B) entered.
		/// </summary>
		private int NumberOfMatchingLotNumberPairs
		{
			get
			{
				var numberOfMatchingLots = 0;

				if (InjectateKeyLotNumber == StandardAKeyLotNumber && !string.IsNullOrEmpty(InjectateKeyLotNumber) && !string.IsNullOrEmpty(StandardAKeyLotNumber))
					numberOfMatchingLots++;
				if (InjectateKeyLotNumber == StandardBKeyLotNumber && !string.IsNullOrEmpty(InjectateKeyLotNumber) && !string.IsNullOrEmpty(StandardBKeyLotNumber))
					numberOfMatchingLots++;
				if (StandardAKeyLotNumber == StandardBKeyLotNumber && !string.IsNullOrEmpty(StandardAKeyLotNumber) && !string.IsNullOrEmpty(StandardBKeyLotNumber))
					numberOfMatchingLots++;

				return numberOfMatchingLots;
			}
		}

		public List<ProtocolRecord> Protocols
		{
			get { return _protocols ?? (_protocols = ProtocolDataAccess.SelectList().ToList()); }
		}

		public List<HematocritFillTypeRecord> HematocritFills
		{
			get { return _hematocritFills ?? (_hematocritFills = HematocritFillTypeDataAccess.SelectList().ToList()); }
		}

		#region NewTubes

		private List<Tube> _tubes;
		private Tube _sTube;

		public List<Tube> Tubes
		{
			get
			{
				if (_tubes != null) return _tubes;
				try
				{
					_tubes = new List<Tube>(_controller.DataService.SelectTubes());
				}
				catch
				{
					_tubes = null;
				}
				return _tubes;
			}
		}
		public Tube SelectedTube
		{
			get { return _sTube; }
			set
			{
				_sTube = value;
				CurrentTest.Tube = _sTube;
				FirePropertyChanged("SelectedTube");
			}
		}

		#endregion

		public List<TestModeRecord> TestModes
		{
			get { return _testModes ?? (_testModes = TestModeDataAccess.SelectList().ToList()); }
		}

		public TestModeRecord SelectedTestModeRecord
		{
			get { return _selectedTestModeRecord; }
			set
			{
				if (IsComboBoxHandlerTriggeringTestModeChange)
					return;

				DesiredTestMode = (TestMode) value.ModeId;

				// Only prompt to clear injectate information if the current test instance has not changed.
				if (IsCurrentTestInstanceChanging)
				{
					// This property is setting the test mode of the newly changed "current test".
					// Once that has taken place, subsequent calls to this property need to respond
					// differently to the test mode changing.
					IsCurrentTestInstanceChanging = false;
				}
				else
				{
					if (HaveInjectateFieldsBeenEntered())
					{
						if (DoesUserWantToChangeTestMode())
						{
							ClearInjectateFields();

							// Let the combo box process the SelectionChanged event normally.
							PreventSelectionFromChangingTestMode = false;
						}
						else
						{
							PreventSelectionFromChangingTestMode = true;
							return;
						}
					}
				}

				if (IsVopsModeDesired())
				{
					AllowUserToLogin();
					return;
				}

				if (IsManualModeDesired())
				{
					MarkTestAsCompleted();
				}

				_selectedTestModeRecord = value;
				CurrentTest.Mode = DesiredTestMode;
				FirePropertyChanged("SelectedTestModeRecord");
			}
		}



		/// <summary>
		/// Desired test mode selected by user
		/// </summary>
		public TestMode DesiredTestMode
		{
			get { return _desiredTestMode; }
			private set { SetValue(ref _desiredTestMode, "DesiredTestMode", value); }
		}
		/// <summary>
		/// Gets the IMessageBoxDispatcher instance used for displaying message boxes to
		/// the user
		/// </summary>
		public IMessageBoxDispatcher MessageBoxDispatcher
		{
			get { return _messageBoxDispatcher; }
		}

		/// <summary>
		/// Gets the IPharmaceuticalKeyDecoder instance used for decoding pharmaceutical key
		/// strings into Pharmaceutical instances
		/// </summary>
		private IPharmaceuticalKeyDecoder KeyDecoder
		{
			get { return _keyDecoder; }
		}

		/// <summary>
		/// Gets the ILotNumberGenerator instance used for creating lot numbers based on a
		/// given Pharmaceutical instance.
		/// </summary>
		private ILotNumberGenerator LotNumberGenerator
		{
			get { return _lotNumberGenerator; }
		}

		/// <summary>
		/// Gets or sets whether the bar code scanner was the source of a product key
		/// </summary>
		private bool IsScannerKeyInputSource { get; set; }

		/// <summary>
		/// Gets or sets whether the Injectate Key field has focus.
		/// </summary>
		/// <remarks>
		/// If focus is being lost, the input from the injectate key field will be processed.
		/// </remarks>
		public bool InjectateKeyHasFocus
		{
			get { return _injectateKeyHasFocus; }
			set
			{
				// No change in focus
				if (_injectateKeyHasFocus == value) return;

				// Injectate key is losing focus
				if (_injectateKeyHasFocus && !value)
				{
					// User typed in the injectate key
					if (!IsScannerKeyInputSource)
					{
						// Something was entered, so try to process it.
						if (!string.IsNullOrEmpty(CurrentTest.InjectateKey))
						{
							ProcessProductKeyInput(CurrentTest.InjectateKey);
						}
						// Field is blank, so make sure the lot number is cleared out.
						else
						{
							InjectateKeyLotNumber = string.Empty;
							CurrentTest.InjectateLot = string.Empty;
						}
					}
					// User scanned the injectate key
					else
					{
						// ProcessProductKeyInput() was already called by OnScannerInputReceived()
						// so there's nothing left to do.
						IsScannerKeyInputSource = false;
					}

					// ReSharper disable once ConditionIsAlwaysTrueOrFalse
					_injectateKeyHasFocus = value;
					FirePropertyChanged("InjectateKeyHasFocus");

					return;
				}

				// Injectate key is gaining focus
				_injectateKeyHasFocus = value;
				FirePropertyChanged("InjectateKeyHasFocus");
			}
		}

		/// <summary>
		/// Gets or sets whether the Standard A Key field has focus.
		/// </summary>
		/// <remarks>
		/// If focus is being lost, the input from the Standard A key field will be processed.
		/// </remarks>
		public bool StandardAKeyHasFocus
		{
			get { return _standardAKeyHasFocus; }
			set
			{
				// No change in focus
				if (_standardAKeyHasFocus == value) return;

				// Standard A key is losing focus
				if (_standardAKeyHasFocus && !value)
				{
					// User typed in the standard A key
					if (!IsScannerKeyInputSource)
					{
						if (!string.IsNullOrEmpty(CurrentTest.StandardAKey))
							ProcessProductKeyInput(CurrentTest.StandardAKey);
					}
					// User scanned the standard A key
					else
					{
						// ProcessProductKeyInput() was already called by OnScannerInputReceived()
						// so there's nothing left to do.
						IsScannerKeyInputSource = false;
					}

					// ReSharper disable once ConditionIsAlwaysTrueOrFalse
					_standardAKeyHasFocus = value;
					FirePropertyChanged("StandardAKeyHasFocus");

					return;
				}

				// Standard A key is gaining focus
				_standardAKeyHasFocus = value;
				FirePropertyChanged("StandardAKeyHasFocus");
			}
		}

		/// <summary>
		/// Gets or sets whether the Standard B Key field has focus.
		/// </summary>
		/// <remarks>
		/// If focus is being lost, the input from the Standard B key field will be processed.
		/// </remarks>
		public bool StandardBKeyHasFocus
		{
			get { return _standardBKeyHasFocus; }
			set
			{
				// No change in focus
				if (_standardBKeyHasFocus == value) return;

				// Standard B key is losing focus
				if (_standardBKeyHasFocus && !value)
				{
					if (!string.IsNullOrEmpty(CurrentTest.StandardBKey))
						ProcessProductKeyInput(CurrentTest.StandardBKey);
					IsScannerKeyInputSource = false;

					// ReSharper disable once ConditionIsAlwaysTrueOrFalse
					_standardBKeyHasFocus = value;
					FirePropertyChanged("StandardBKeyHasFocus");

					return;
				}

				_standardBKeyHasFocus = value;
				FirePropertyChanged("StandardBKeyHasFocus");
			}
		}


		/// <summary>
		/// Gets or sets wether injectate lot entry is required or not.
		/// </summary>
		/// <remarks>
		/// Injectate lot entry is required when:
		/// Injectate verification is OFF AND
		/// BVAtest mode is Automatic AND
		/// BVAtest status is Pending
		/// </remarks>
		public bool InjectateLotEntryRequired
		{
			get { return _iLotEntryRequired; }
			private set
			{
				SetValue(ref _iLotEntryRequired, "InjectateLotEntryRequired", value);
			}

		}

		public bool UserWantsToPerformQc
		{
			get { return _userWantsToPerformQcTestNow; }
			private set { SetValue(ref _userWantsToPerformQcTestNow, "UserWantsToPerformQc", value); }
		}

		private DelegateCommand<object> NavigateToSampleValuesDelegateCommand
		{
			get { return _navToSampleValuesCommand; }
			set
			{
				// Only get one assignment
				if (_navToSampleValuesCommand == null)
					_navToSampleValuesCommand = value;
			}
		}

		private DelegateCommand<object> NavigateToMyselfDelegateCommand
		{
			get { return _navToMyselfCommand; }
			set
			{
				// Only get one assignment
				if (_navToMyselfCommand == null)
					_navToMyselfCommand = value;
			}
		}

		private DelegateCommand<object> LoginDelegateCommand
		{
			get { return _loginCommand; }
			set
			{
				// Only get one assignment
				if (_loginCommand == null)
					_loginCommand = value;
			}
		}

		#endregion

		#region Commands

		public ICommand NavigateToSampleValuesCommand
		{
			get { return NavigateToSampleValuesDelegateCommand; }
		}
		bool NavigateToSampleValuesCanExecute(object o)
		{
			return CurrentTest != null && CurrentTest.Protocol != -1;
		}
		void NavigateToSampleValuesCommandExecute(object arg)
		{
			_logger.Log("NAVIGATE TO SAMPLE VALUES TAB CLICKED", Category.Info, Priority.None);
			_localNavigator.ActivateView(BVAModuleViewKeys.TestSampleValues);
		}

		public ICommand NavigateToMyselfCommand
		{
			get { return NavigateToMyselfDelegateCommand; }
		}
		bool NavigateToMyselfCanExecute(object arg)
		{
			return CurrentTest != null && CurrentTest.Patient != null && !string.IsNullOrEmpty(CurrentTest.Patient.HospitalPatientId);
		}
		void NavigateToMyselfCommandExecute(object arg)
		{
			_logger.Log("NAVIGATE TO TEST PARAMETERS (SELF) TAB CLICKED", Category.Info, Priority.None);
			_localNavigator.ActivateView(BVAModuleViewKeys.TestParameters);
		}

		public ICommand LoginCommand
		{
			get { return LoginDelegateCommand; }
		}
		bool LoginCommandCanExecute(object arg)
		{
			return !string.IsNullOrEmpty(LoginVopsPassword);
		}
		private void LoginCommandExecute(object arg)
		{
			try
			{
				//If successfull allow the change
				if (_authManager.Login(AuthorizationLevel.Service, LoginVopsPassword))
				{
					MessageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information,
						"The test mode has been changed to VOPS.",
						"Changing Test Mode", MessageBoxChoiceSet.Close);

					CurrentTest.Mode = TestMode.VOPS;
					_selectedTestModeRecord = (from x in TestModes where x.ModeId == (int) TestMode.VOPS select x).FirstOrDefault();
					FirePropertyChanged("SelectedTestModeRecord");
				}
				else
				{
					//Error MessageBox
					Message errorAlert = _messageManager.GetMessage(MessageKeys.UtilityPasswordInvalid);
					MessageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning, errorAlert.FormattedMessage, "Unsuccessful Changing Test Mode", MessageBoxChoiceSet.Close);
					PreventSelectionFromChangingTestMode = true;
					FirePropertyChanged("SelectedTestModeRecord");
					_logger.Log(errorAlert.FormattedMessage, Category.Info, Priority.Low);
				}
			}
			catch (Exception ex)
			{
				//Log here
				string err = string.Format("BVATest loggin execution threw an exception. Message: {0}. StackTrace: {1}", ex.Message, ex.StackTrace);
				_logger.Log(err, Category.Exception, Priority.High);
			}
			finally
			{
				VirtualKeyboardManager.CloseActiveKeyboard();

				//Set desired test mode to automatic
				DesiredTestMode = TestMode.Automatic;
				LoginVopsPassword = string.Empty;

				_eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
				_authManager.Logout();
			}
		}

		#endregion

		#region IActiveAware Members

		private bool _isActive;
		public event EventHandler IsActiveChanged = delegate { };
		public bool IsActive
		{
			get { return _isActive; }
			set
			{
				// Don't do anything if we're not changing the active state. (Otherwise events get
				// subscribed to multiple times.)
				if (_isActive == value) return;

				_isActive = value;

				if (_isActive)
				{
					FirePropertyChanged("CurrentTest"); // Forces rule re-evaluation if required fields have been enabled/disabled
					UserWantsToPerformQc = false;
					_eventAggregator.GetEvent<ScannerInputReceived>().Subscribe(OnScannerInputReceived, ThreadOption.PublisherThread, false);
					LocationLabel = new LabelFormatter(_settingsManager.GetSetting<string>(SettingKeys.BvaLocationLabel));
					EvaluateInjectateLotPhysicalEntryRequired();
				}
				else
				{
					_eventAggregator.GetEvent<ScannerInputReceived>().Unsubscribe(OnScannerInputReceived);
					VirtualKeyboardManager.CloseActiveKeyboard();
				}

				_logger.Log("TestParametersViewModel: TestParametersViewModel active: " + _isActive, Category.Debug, Priority.Low);
			}
		}

		#endregion

		#region Helpers

		void ConfigureViewModel()
		{
			_eventAggregator.GetEvent<TestSetupTabItemAdded>().Publish(new TestSetupTabItemPayload(Guid.NewGuid())
			{
				Header = "2. Test Parameters",
				TabPlacementIndex = 1,
				NavigationCommand = NavigateToMyselfDelegateCommand
			});

			SettingObserver.RegisterHandler(SettingKeys.BvaPhysiciansSpecialties,
											s => PhysiciansSpecialties = s.GetSetting<IEnumerable>(SettingKeys.BvaPhysiciansSpecialties).OfType<string>().ToList());
		}

		protected virtual void OnCurrentTestChanged(BVATest test)
		{
			if (test != null)
			{
				_tObserver = new PropertyObserver<BVATest>(test);
				_tObserver.RegisterHandler(t => t.Patient, t => CurrentPatient = t.Patient);
				_tObserver.RegisterHandler(t => t.RefPhysician, t => ReferringPhysicianChangedHandler(t, t.RefPhysician));
				_tObserver.RegisterHandler(t => t.CcReportTo, t => CcReportToChangedHandler(t, t.CcReportTo));
				_tObserver.RegisterHandler(t => t.Analyst, t => AnalystChangedHandler(t, t.Analyst));
				_tObserver.RegisterHandler(t => t.Protocol, t => TestProtocolChanged());
				_tObserver.RegisterHandler(t => t.InjectateDose, t => FirePropertyChanged("InjectateDoseAdapter"));
				_tObserver.RegisterHandler(t => t.Status, t => EvaluateInjectateLotPhysicalEntryRequired());
				_tObserver.RegisterHandler(t => t.Mode, OnTestModeChanged);
				_tObserver.RegisterHandler(t => t.StandardAKey, t => _userAcknowledgedIdenticalStandards = false);
				_tObserver.RegisterHandler(t => t.StandardBKey, t => _userAcknowledgedIdenticalStandards = false);
				_tObserver.RegisterHandler(t => t.InjectateLot, t =>
				{
					UserWantsToPerformQc = false;

					if (IsActive && !string.IsNullOrEmpty(t.InjectateLot) &&
						t.Status == TestStatus.Pending &&
						!UtilityDataService.CheckIfInjectateLotHasBeenQCd(t.InjectateLot))
					{
						var choices = new[] { "Run QC", "Proceed Anyway" };
						int result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning,
							"Injectate lot " + CurrentTest.InjectateLot + " has not been verified by a Quality Control test. Do you want to run a QC Test?", "Unverified Injectate Lot",
							choices);

						if (choices[result] == "Run QC")
						{
							UserWantsToPerformQc = true;
							var navCoordinator = _navigationCoordinator;
							_controller.DataService.SaveTest(CurrentTest);
							navCoordinator.ActivateAppModule(AppModuleIds.QualityControl, null, navCoordinator.ActiveAppModuleKey);
							VirtualKeyboardManager.CloseActiveKeyboard();
						}
					}
				});

				CurrentPatient = test.Patient;
				InjectateDoseAdapter = test.InjectateDose.ToString(CultureInfo.InvariantCulture);
			}

			FirePropertyChanged("CurrentTest");
			IsCurrentTestInstanceChanging = true;

			EnsureDefaultSelectionValues();

			NavigateToSampleValuesDelegateCommand.RaiseCanExecuteChanged();
			NavigateToMyselfDelegateCommand.RaiseCanExecuteChanged();
		}

		/// <summary>
		/// Handler for the CcReportTo setting change. This will modify the ReferringPhysicians property. Since the user can't edit the ReferringPhysician field
		/// and the CC Report To field at the same time, we can use this list.
		/// </summary>
		/// <param name="test"></param>
		/// <param name="newCcReportTo"></param>
		private void CcReportToChangedHandler(BVATest test, string newCcReportTo)
		{
			ReferringPhysicians.Clear();

			// Nothing to do if there's no test or any partial text to match.
			if (test == null || string.IsNullOrEmpty(CurrentTest.CcReportTo))
				return;

			if (!string.IsNullOrEmpty(newCcReportTo))
				ReferringPhysicians.AddRange(FindReferringPhysiciansLike(newCcReportTo));
		}

		protected virtual void EnsureDefaultSelectionValues()
		{
			if (CurrentTest == null) return;

			HematocritFillTypeRecord newHematocritFill;

			// All this does is ensure that the Analyst and Referring Physician lists get refereshed

			if (CurrentTest.HematocritFill == HematocritFillType.Unknown)
				newHematocritFill = (from hema in HematocritFills where hema.Description.Contains("2 Per") select hema).FirstOrDefault();
			else
				newHematocritFill = (from hema in HematocritFills
									 where hema.HematocritFillType == (int) CurrentTest.HematocritFill
									 select hema).FirstOrDefault();

			if (newHematocritFill != null)
				CurrentTest.HematocritFill = (HematocritFillType) newHematocritFill.HematocritFillType;

			//Sets selectd tube from the list of Tubes
			SelectedTube = (from t in Tubes where t.Id == CurrentTest.Tube.Id select t).FirstOrDefault();

			//Setting default test mode type
			var newTestMode = (from mode in TestModes where mode.ModeId == (int) CurrentTest.Mode select mode).FirstOrDefault();

			if (newTestMode != null)
				SelectedTestModeRecord = newTestMode;
		}

		void TestProtocolChanged()
		{
			NavigateToSampleValuesDelegateCommand.RaiseCanExecuteChanged();
		}

		void EvaluateInjectateLotPhysicalEntryRequired()
		{
			bool entryRequired;

			if (CurrentTest == null)
				entryRequired = false;
            else if (_settingsManager.GetSetting<bool>(SettingKeys.SystemInjectateVerificationEnabled) && CurrentTest.Mode != TestMode.Manual)
                entryRequired = false;
			else if (CurrentTest.Status == TestStatus.Pending || CurrentTest.Mode == TestMode.Manual)
				entryRequired = true;
			else
				entryRequired = false;

			// Force validation on the following properties of BVATest
			InjectateLotEntryRequired = entryRequired;
			if (CurrentTest != null) CurrentTest.EvaluatePropertyByName("InjectateKey", "StandardAKey", "StandardBKey");
		}

		void OnTestModeChanged(BVATest bvaTest)
		{
			EvaluateInjectateLotPhysicalEntryRequired();
		}

		private bool IsManualModeDesired()
		{
			return DesiredTestMode == TestMode.Manual && DesiredTestMode != CurrentTest.Mode;
		}

		private bool IsVopsModeDesired()
		{
			return DesiredTestMode == TestMode.VOPS && DesiredTestMode != CurrentTest.Mode;
		}

		private void MarkTestAsCompleted()
		{
			CurrentTest.Status = TestStatus.Completed;
			foreach (var sample in CurrentTest.Samples)
				sample.ExecutionStatus = SampleExecutionStatus.Completed;
		}

		private void AllowUserToLogin()
		{
			_eventAggregator.GetEvent<BusyStateChanged>()
							.Publish(new BusyPayload(true, "VOPS requires service password.") { IsIndetermineState = true });
			PreventSelectionFromChangingTestMode = true;
		}

		private void ClearInjectateFields()
		{
			CurrentTest.InjectateKey = string.Empty;
			CurrentTest.StandardAKey = string.Empty;
			CurrentTest.StandardBKey = string.Empty;
		}

		private bool DoesUserWantToChangeTestMode()
		{
			var message = _messageManager.GetMessage(MessageKeys.BvaTestModeChanging).FormattedMessage;
			var choices = new[] { "Change Test Mode", "Cancel" };
			var result = MessageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning, message, "Changing Test Mode", choices);
			_logger.Log(message, Category.Info, Priority.Low);

			return choices[result] == "Change Test Mode";
		}

		private bool HaveInjectateFieldsBeenEntered()
		{
			return !string.IsNullOrEmpty(CurrentTest.InjectateLot) || !string.IsNullOrEmpty(CurrentTest.InjectateKey) ||
				   !string.IsNullOrEmpty(CurrentTest.StandardAKey) || !string.IsNullOrEmpty(CurrentTest.StandardBKey);
		}

		#endregion


		/// <summary>
		/// Called when new BVATest is selected.
		/// </summary>
		/// <param name="test"></param>
		void BVATestSelectedEventHandler(BVATest test)
		{
			CurrentTest = test;
		}

		void AnalystChangedHandler(BVATest test, string newAnalyst)
		{
			Analysts.Clear();
			if (test != null && !string.IsNullOrEmpty(newAnalyst))
				Analysts.AddRange(_controller.DataService.FindAnalysts(newAnalyst));
		}

		void ReferringPhysicianChangedHandler(BVATest test, string newReferringPhysician)
		{
			ReferringPhysicians.Clear();

			if (test == null || string.IsNullOrEmpty(newReferringPhysician))
				return;

			ReferringPhysicians.AddRange(FindReferringPhysiciansLike(newReferringPhysician));
			CurrentTest.PhysiciansSpecialty = _controller.DataService.GetMostRecentSpecialty(newReferringPhysician);
		}

		IEnumerable<string> FindReferringPhysiciansLike(string text)
		{
			IEnumerable<string> allPhysicians = new List<string>();
			try
			{
				allPhysicians = _settingsManager.GetSetting<IEnumerable>(SettingKeys.BvaReferringPhysicians).OfType<string>();
			}
			catch (Exception ex)
			{
				_logger.Log("Unable to load referring physicians from settings DB: " + ex.Message, Category.Exception, Priority.High);
			}

			return allPhysicians.Where(physician => physician.IndexOf(text, StringComparison.OrdinalIgnoreCase) >= 0).ToList();
		}

		#region Injectate verification methods

		#region UnexpectedKeyTypeScenario
		/// <summary>
		/// Specifies constants defining the scenarios of unexpected product key types
		/// </summary>
		private enum UnexpectedKeyTypeScenario
		{
			/// <summary>
			/// User entered a standard key when an injectate key was expected
			/// </summary>
			ExpectedInjectate = 0,

			/// <summary>
			/// User entered an injectate key when a standard key was expected
			/// </summary>
			ExpectedStandard = 1
		}

		#endregion

		#region Error messages

		/// <summary>
		/// Displays a message box indicating that both of the standards have
		/// the same key.
		/// </summary>
		/// <returns>True if user wants to keep the duplicate standard key</returns>
		private bool ShowIdenticalStandardKeysMessageBox()
		{
			var message = _messageManager.GetMessage(MessageKeys.BvaStandardKeysIdentical).FormattedMessage;
			var choices = new[] { "Use Duplicate Keys", "Clear Standard Keys" };
			var result = MessageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Question, message, "Duplicate Standard Keys", choices);
			_logger.Log(message, Category.Info, Priority.Low);

			return choices[result] == "Use Duplicate Keys";
		}

		/// <summary>
		/// Displays a message box indicating that an invalid key has been entered.
		/// </summary>
		/// <remarks>
		/// It doesn't matter why the key is invalid (e.g., too short, invalid checksum) or what
		/// the input source is (e.g., keyboard, scanner).
		/// </remarks>
		private void ShowInvalidProductKeyMessageBox(string invalidKey)
		{
			string focusedKeyType = GetFocusedProductKeyFieldName();
			string alertMessage = _messageManager.GetMessage(MessageKeys.BvaInjectateKeyInvalid).FormattedMessage;
			var caption = new StringBuilder();

			caption.Append("Invalid ");
			caption.Append(focusedKeyType);
			caption.Append(" Key");

			alertMessage = string.Format(alertMessage, focusedKeyType, invalidKey);

			MessageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, alertMessage, caption.ToString(), new[] { "Clear Recently Scanned Key" });
			_logger.Log(alertMessage, Category.Info, Priority.Low);
		}

		/// <summary>
		/// Displays a message box indicating that a lot number for a given key could not be created.
		/// </summary>
		/// <remarks>
		/// It doesn't matter why the key is invalid (e.g., too short, invalid checksum) or what
		/// the input source is (e.g., keyboard, scanner).
		/// </remarks>
		private void ShowInvalidLotNumberMessageBox()
		{
			var focusedKeyType = GetFocusedProductKeyFieldName();
			var alertMessage = _messageManager.GetMessage(MessageKeys.BvaInjecateLotNumberInvalid).FormattedMessage;

			var caption = new StringBuilder();

			caption.Append("Invalid ");
			caption.Append(focusedKeyType);
			caption.Append(" Lot Number");

			alertMessage = string.Format(alertMessage, focusedKeyType);

			MessageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, alertMessage, caption.ToString(), new[] { "Clear Recently Scanned Key" });
			_logger.Log(alertMessage, Category.Info, Priority.Low);
		}

		/// <summary>
		/// Displays a message box indicating that the type of key observed was not the type of key
		/// expected (e.g., injectate key observed when expecting standard key).
		/// </summary>
		private void ShowUnexpectedProductKeyTypeMessageBox(UnexpectedKeyTypeScenario scenario)
		{
			var observedTypeName = string.Empty;
			var expectedTypeName = string.Empty;
			var alertMessage = _messageManager.GetMessage(MessageKeys.BvaUnexpectedProductKeyType).FormattedMessage;

			switch (scenario)
			{
				case UnexpectedKeyTypeScenario.ExpectedInjectate:
					observedTypeName = "standard";
					expectedTypeName = "injectate";
					break;
				case UnexpectedKeyTypeScenario.ExpectedStandard:
					observedTypeName = "injectate";
					expectedTypeName = "standard";
					break;
			}
			alertMessage = string.Format(alertMessage, observedTypeName, expectedTypeName, expectedTypeName);

			var caption = new StringBuilder();
			caption.Append("Unexpected ");
			caption.Append(char.ToUpper(observedTypeName[0]) + observedTypeName.Substring(1));
			caption.Append(" Key");

			MessageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, alertMessage, caption.ToString(), new[] { "Clear Recently Scanned Key" });
			_logger.Log(alertMessage, Category.Info, Priority.Low);
		}

		/// <summary>
		/// Displays a message box indicating that scanner input was observed for a field not
		/// associated with product keys.
		/// </summary>
		private void ShowScannerInputNotAllowedMessageBox()
		{
			string alertMessage = _messageManager.GetMessage(MessageKeys.BvaScannerInputNotAllowed).FormattedMessage;
			MessageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, alertMessage, "Scanner Input", MessageBoxChoiceSet.Close);
			_logger.Log(alertMessage, Category.Info, Priority.Low);
		}

		/// <summary>
		/// Displays a message box indicating that the lot numbers for the two keys
		/// do not match.
		/// </summary>
		private void ShowLotNumberMismatchMessageBox()
		{
			bool hasInjectateKeyLotNumber = !string.IsNullOrEmpty(InjectateKeyLotNumber);
			bool hasStandardAKeyLotNumber = !string.IsNullOrEmpty(StandardAKeyLotNumber);
			bool hasStandardBKeyLotNumber = !string.IsNullOrEmpty(StandardBKeyLotNumber);
			string mismatchDescription = string.Empty;

			// Scenario 1: Injectate + Standard A
			if (hasInjectateKeyLotNumber && hasStandardAKeyLotNumber && !hasStandardBKeyLotNumber &&
				(InjectateKeyLotNumber != StandardAKeyLotNumber))
			{
				mismatchDescription = "Injectate and Standard A";
			}
			// Scenario 2: Injectate + Standard B
			else if (hasInjectateKeyLotNumber && hasStandardBKeyLotNumber && !hasStandardAKeyLotNumber &&
				(InjectateKeyLotNumber != StandardBKeyLotNumber))
			{
				mismatchDescription = "Injectate and Standard B";
			}
			// Scenario 3: Injectate + Standard A + Standard B
			else if (hasInjectateKeyLotNumber && hasStandardAKeyLotNumber && hasStandardBKeyLotNumber &&
				((InjectateKeyLotNumber != StandardAKeyLotNumber) ||
				 (InjectateKeyLotNumber != StandardBKeyLotNumber) ||
				 (StandardAKeyLotNumber != StandardBKeyLotNumber)))
			{
				mismatchDescription = "Injectate and Standards";
			}
			// Scenario 4: Standard A + Standard B
			else if (!hasInjectateKeyLotNumber && hasStandardAKeyLotNumber && hasStandardBKeyLotNumber &&
				(StandardAKeyLotNumber != StandardBKeyLotNumber))
			{
				mismatchDescription = "Standard A and Standard B";
			}
			string alertMessage = _messageManager.GetMessage(MessageKeys.BvaLotNumbersMismatched).FormattedMessage;

			var message = new StringBuilder();
			if (hasInjectateKeyLotNumber)
				message.Append("Injectate lot number: " + InjectateKeyLotNumber + "\n");
			if (hasStandardAKeyLotNumber)
				message.Append("Standard A lot number: " + StandardAKeyLotNumber + "\n");
			if (hasStandardBKeyLotNumber)
				message.Append("Standard B lot number: " + StandardBKeyLotNumber + "\n");

			alertMessage = string.Format(alertMessage, mismatchDescription, message, mismatchDescription);

			MessageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, alertMessage, "Mismatched Lot Numbers", new[] { "Clear All Keys and Lot Number" });
			_logger.Log(alertMessage, Category.Info, Priority.Low);
		}

		/// <summary>
		/// Displays a message box indicating that the injectate is in use by another patient
		/// Problem: what if that *is* what is going on?  If the scanner says it, do we
		/// really want to block them from continuing?  OTOH, if we *don't*, what good is this?
		/// </summary>
		private void ShowInjectateInUseByAnotherPatientMessageBox()
		{
			var errorMessage = _messageManager.GetMessage(MessageKeys.BvaInjectateDoseInUse).FormattedMessage;
			const string caption = "Test Parameters";

			MessageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, errorMessage, caption, new[] { "Clear Injectate Key" });
			_logger.Log(errorMessage, Category.Info, Priority.Low);

		}

		#endregion

		/// <summary>
		/// Handles scanner input events for this view model. If a field corresponding to product keys
		/// has focus, the scanner input will be processed. Otherwise, the user will be informed that
		/// this action is not allowed.
		/// </summary>
		/// <param name="payload"></param>
		private void OnScannerInputReceived(ScannerInputPayload payload)
		{
			// If we're not on a field that accepts something from the scanner, inform the user.
			if (!ScannableFieldHasFocus())
			{
				ShowScannerInputNotAllowedMessageBox();
				return;
			}

			// Note that the input is coming from the scanner and then process the input.
			IsScannerKeyInputSource = true;
			ProcessProductKeyInput(payload.DecodedValue);
		}

		/// <summary>
		/// Gets the name of the injectate verification specific field that has focus.
		/// </summary>
		/// <remarks>
		/// This method is used to construct the error messages shown to the user so that he/she
		/// knows to which key the message pertains.</remarks>
		/// <returns>Name of the field that has focus</returns>
		private string GetFocusedProductKeyFieldName()
		{
			string keyType = string.Empty;
			if (InjectateKeyHasFocus)
				keyType = "Injectate";
			else if (StandardAKeyHasFocus)
				keyType = "Standard A";
			else if (StandardBKeyHasFocus)
				keyType = "Standard B";
			return keyType;
		}

		/// <summary>
		/// Attempts to create a Pharmaceutical instance given a key. If the Pharmaceutical cannot
		/// be created, a message box is shown and the field with the erroneous key is cleared.
		/// </summary>
		/// <param name="productKey">Key to decode</param>
		/// <param name="pharmaceutical">Pharmaceutical instance to create and return</param>
		/// <returns>True if the key was successfully decoded; false otherwise</returns>
		private bool TryAndCreatePharmaceutical(string productKey, out Pharmaceutical pharmaceutical)
		{
			pharmaceutical = null;

			try
			{
				pharmaceutical = KeyDecoder.Decode(productKey);
			}
			catch (Exception ex)
			{
				_logger.Log(ex.Message, Category.Exception, Priority.None);
				ShowInvalidProductKeyMessageBox(productKey);
				ClearFocusedInjectateVerificationField();
				return false;
			}

			return true;
		}

		/// <summary>
		/// Attempts to create a lot number given a Pharmaceutical instance. If the lot number 
		/// cannot not be created, a message box is shown and the field with the erroneous key is 
		/// cleared.
		/// </summary>
		/// <param name="lotNumber">lotNumber to decode</param>
		/// <param name="pharmaceutical">Pharmaceutical instance to create and return</param>
		/// <returns>True if the key was successfully decoded; false otherwise</returns>
		private bool TryAndCreateLotNumber(Pharmaceutical pharmaceutical, out string lotNumber)
		{
			lotNumber = string.Empty;

			try
			{
				lotNumber = LotNumberGenerator.Generate(pharmaceutical);
			}
			catch (Exception ex)
			{
				_logger.Log(ex.Message, Category.Exception, Priority.None);
				ShowInvalidLotNumberMessageBox();
				ClearFocusedInjectateVerificationField();
				return false;
			}

			return true;
		}

		/// <summary>
		/// Processes a given product key based on what input field has focus and sets the
		/// appropriate properties if the product key could be processed without errors.
		/// </summary>
		/// <remarks>
		/// (1) This method displays error messages if the Pharmaceutical or its corresponding
		///     lot numbers could not be generated.
		/// (2) This method displays error messages if the observed product key doesn't correspond
		///     the expected type of key. (For example, if the Standard A key was expected and the 
		///     user provided an injectate key.)
		/// </remarks>
		/// <param name="productKey">Product key to process</param>
		private void ProcessProductKeyInput(string productKey)
		{
			Pharmaceutical tempParmaceutical;
			if (!TryAndCreatePharmaceutical(productKey, out tempParmaceutical))
				return;

			// Pharmaceutical is valid, so see if we're in the correct field for its type.
			if (tempParmaceutical.Type == Pharmaceutical.PharmaceuticalType.Injectate)
			{
				// Injectate was scanned and that's what was expected, so try to process it.
				if (InjectateKeyHasFocus)
				{

					CurrentTest.InjectateKey = productKey;
					string injectateKeyLotNumber;
					if (!TryAndCreateLotNumber(tempParmaceutical, out injectateKeyLotNumber))
						return;

					// Validate that this dose is only used for this patient
					// Validate on CurrentTest.InjectateKey  If true
					// ClearFocusedInjectateVerificationField 

					if (UtilityDataService.IsInjectateKeyUsedByAnotherPatient(CurrentTest.InjectateKey, CurrentTest.Patient.InternalId))
					{
						ShowInjectateInUseByAnotherPatientMessageBox();
						ClearFocusedInjectateVerificationField();
						return;
					}

					// Lot number can be generated, so set its value in the properties that check
					// matching lot numbers and for the current test.
					InjectateKeyLotNumber = injectateKeyLotNumber;
					CurrentTest.InjectateLot = InjectateKeyLotNumber;
				}
				// Scanned injectate but was expecting a standard
				else
				{
					ShowUnexpectedProductKeyTypeMessageBox(UnexpectedKeyTypeScenario.ExpectedStandard);
					ClearFocusedInjectateVerificationField();
					return;
				}
			}
			else if (tempParmaceutical.Type == Pharmaceutical.PharmaceuticalType.Standard)
			{
				// Standard was scanned and that's what was expected, so try to process it.
				if (StandardAKeyHasFocus)
				{
					CurrentTest.StandardAKey = productKey;
					string standardAKeyLotNumber;
					if (!TryAndCreateLotNumber(tempParmaceutical, out standardAKeyLotNumber))
						return;

					// Lot number can be generated, so set its value in the properties that check
					// matching lot numbers.
					StandardAKeyLotNumber = standardAKeyLotNumber;
				}
				else if (StandardBKeyHasFocus)
				{
					CurrentTest.StandardBKey = productKey;
					string standardBKeyLotNumber;
					if (!TryAndCreateLotNumber(tempParmaceutical, out standardBKeyLotNumber))
						return;

					// Lot number can be generated, so set its value in the properties that check
					// matching lot numbers.
					StandardBKeyLotNumber = standardBKeyLotNumber;
				}
				// Scanned standard but was expecting injectate
				else
				{
					ShowUnexpectedProductKeyTypeMessageBox(UnexpectedKeyTypeScenario.ExpectedInjectate);
					ClearFocusedInjectateVerificationField();
					return;
				}
			}

			// Ensure that lot number matches the other lot numbers.
			if (EnsureMatchingLotNumbers())
			{
				if (InjectateKeyHasFocus)
				{
					// If we're at Standard A, move to Standard B

					if (IsScannerKeyInputSource)
						StandardAKeyHasFocus = true;
				}
				else
				{
					// If we're at Injectate, move to Standard A

					if (IsScannerKeyInputSource)
						StandardBKeyHasFocus = true;
				}
			}
		}

		/// <summary>
		/// Clears the Injectate, Standard A, or Standard B key fields depending on which field
		/// has focus.
		/// </summary>
		/// <remarks>
		/// The properties corresponding to lot numbers are also cleared.
		/// </remarks>
		private void ClearFocusedInjectateVerificationField()
		{
			if (InjectateKeyHasFocus)
			{
				CurrentTest.InjectateKey = string.Empty;
				InjectateKeyLotNumber = string.Empty;
				CurrentTest.InjectateLot = string.Empty;
			}
			if (StandardAKeyHasFocus)
			{
				CurrentTest.StandardAKey = string.Empty;
				StandardAKeyLotNumber = string.Empty;
				_userAcknowledgedIdenticalStandards = false;
			}
			if (StandardBKeyHasFocus)
			{
				CurrentTest.StandardBKey = string.Empty;
				StandardBKeyLotNumber = string.Empty;
				_userAcknowledgedIdenticalStandards = false;
			}
		}

		/// <summary>
		/// Checks the lot numbers of the product keys currently entered to make sure that all of
		/// them correspond to the same lot. An error message will be displayed if there are any
		/// mismatches.
		/// </summary>
		/// <returns>True if lot numbers match properly; false otherwise</returns>
		private bool EnsureMatchingLotNumbers()
		{
			bool returnResult = true;

			// We don't check for matching lot numbers if the number of non-empty keys is...
			//   (1) zero; this is the degenerative case where there's nothing to match
			//   (2) one; this is the identity case where the sole lot number matches itself

			if (NumberOfNonEmptyKeys == 2)
			{
				// Two product keys are entered, so to be valid, they both must match (i.e., one matching pair).
				if (NumberOfMatchingLotNumberPairs != 1)
				{
					ShowLotNumberMismatchMessageBox();
					ClearAllLotNumbersOnError();
					returnResult = false;
				}
			}

			if (NumberOfNonEmptyKeys == 3)
			{
				// Three product keys are entered, so to be valid, all must match (i.e., injectate with standard A,
				// injecate with standard B, and standard A with standard B).
				if (NumberOfMatchingLotNumberPairs != 3)
				{
					ShowLotNumberMismatchMessageBox();
					ClearAllLotNumbersOnError();
					returnResult = false;
				}
			}

			// Both standard keys are present and they are the same.
			if (!string.IsNullOrEmpty(CurrentTest.StandardAKey) && !string.IsNullOrEmpty(CurrentTest.StandardAKey))
			{
				if (CurrentTest.StandardAKey == CurrentTest.StandardBKey)
				{

					// User hasn't seen the message box about identical standards.
					if (!_userAcknowledgedIdenticalStandards)
					{
						_userAcknowledgedIdenticalStandards = true;
						if (!ShowIdenticalStandardKeysMessageBox())
						{
							ClearFocusedInjectateVerificationField();
							return false;
						}
					}
				}
				else
				{
					_userAcknowledgedIdenticalStandards = false;
				}
			}

			return returnResult;
		}

		/// <summary>
		/// Clears all lot numbers associated with any product keys entered thus far, including the
		/// lot number associated with the current test.
		/// </summary>
		private void ClearAllLotNumbersOnError()
		{
			CurrentTest.InjectateKey = string.Empty;
			InjectateKeyLotNumber = string.Empty;
			CurrentTest.StandardAKey = string.Empty;
			StandardAKeyLotNumber = string.Empty;
			CurrentTest.StandardBKey = string.Empty;
			StandardBKeyLotNumber = string.Empty;
			CurrentTest.InjectateLot = string.Empty;
		}

		/// <summary>
		/// Determines if any of the three fields related to injectate verification have input
		/// focus.
		/// </summary>
		/// <returns>
		/// True if the Injectate Key, Standard A Key, or Standard B Key field has focus;
		/// false otherwise</returns>
		private bool ScannableFieldHasFocus()
		{
			return InjectateKeyHasFocus || StandardAKeyHasFocus || StandardBKeyHasFocus;
		}

		#endregion

		#region IDataErrorInfo Members

		public string Error
		{
			get { return string.Empty; }
		}
		public string this[string columnName]
		{
			get
			{
				if (columnName.Equals("InjectateDoseAdapter") && CurrentTest != null)
					return CurrentTest["InjectateDose"];

				return null;
			}
		}

		#endregion
	}
}
