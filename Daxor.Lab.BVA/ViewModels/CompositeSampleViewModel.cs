﻿using System;
using System.ComponentModel;
using System.Globalization;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager.Interfaces;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.BVA.ViewModels
{
    /// <summary>
    /// ViewModel for BVA composite sample model
    /// </summary>
    public class CompositeSampleViewModel : Domain.Entities.Base.ObservableObject, IDataErrorInfo
    {
        #region Constants
        private const int EmptyValue = -1;
        #endregion

        #region Fields

        readonly IMeasuredCalcEngineService _mCalcEngine;

        bool _isEnabled;
        HematocritFillType _htcFillType = HematocritFillType.Unknown;
        BVASample _sampleA, _sampleB;
        BVACompositeSample _compositeSample;

        bool _countingSampleA;
        bool _countingSampleB;

        bool _isReadOnly;
        bool _manualCountsRequired;
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly ILoggerFacade _logger;
        private readonly IMessageManager _messageManager;

        #endregion

        #region Ctor

        public CompositeSampleViewModel(IMeasuredCalcEngineService mCalcEngine, BVACompositeSample cSample, IMessageBoxDispatcher messageBoxDispatcher,
            ILoggerFacade logger, IMessageManager messageManager, bool isEnabled = true)
        {
            _mCalcEngine = mCalcEngine;
            _messageBoxDispatcher = messageBoxDispatcher;
            CompositeSample = cSample;
            _logger = logger;
            _messageManager = messageManager;
            IsEnabled = isEnabled;
        }

        #endregion

        #region Properties

        public BVACompositeSample CompositeSample
        {
            get { return _compositeSample; }
            protected set
            {
                if (_compositeSample == value)
                    return;

                if (_compositeSample != null)
                    _compositeSample.PropertyChanged -= CompositeSamplePropertyChanged;

                _compositeSample = value;

                if (_compositeSample != null)
                {
                    SampleA = _compositeSample.SampleA;
                    SampleB = _compositeSample.SampleB;
                }
                else
                {
                    SampleA = null;
                    SampleB = null;
                }

                if (_compositeSample != null)
                    _compositeSample.PropertyChanged += CompositeSamplePropertyChanged;

                FirePropertyChanged("PostInjectionTimeInSeconds", "IsWholeSampleExcluded");
            }
        }
        public BVASample SampleA
        {
            get { return _sampleA; }
            set
            {
                if (_sampleA == value)
                    return;

                //Hookup event handler for property changed
                if (_sampleA != null)
                    _sampleA.PropertyChanged -= SampleAPropertyChanged;

                _sampleA = value;

                if (_sampleA != null)
                    _sampleA.PropertyChanged += SampleAPropertyChanged;

                FirePropertyChanged("SampleACounts", "IsSampleACountsExcluded", "HematocritA", "IsSampleAHematocritExcluded", "SampleBCounts");
            }
        }
        public BVASample SampleB
        {
            get { return _sampleB; }
            set
            {
                if (_sampleB == value)
                    return;


                //Hookup event handler for property changed
                if (_sampleB != null)
                    _sampleB.PropertyChanged -= SampleBPropertyChanged;

                _sampleB = value;
               

                if (_sampleB != null)
                    _sampleB.PropertyChanged += SampleBPropertyChanged;

                FirePropertyChanged("SampleBCounts", "IsSampleBCountsExcluded", "HematocritB", "IsSampleBHematocritExcluded", "SampleACounts");
               
            }
        }

        public HematocritFillType HematocritFill
        {
            get { return _htcFillType; }
            set { base.SetValue(ref _htcFillType, "HematocritFill", value); }
        }
        public SampleType Type
        {
            get { return _compositeSample.SampleType; }
        }
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                base.SetValue(ref _isEnabled, "IsEnabled", value);
                if (CompositeSample != null) 
                    CompositeSample.IsEvaluationEnabled = value;
            }
        }
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
            set { base.SetValue(ref _isReadOnly, "IsReadOnly", value); }
        }

        public bool IsSampleACountsExcluded
        {
            get
            {
                if (_compositeSample.SampleA == null) 
                    return false;
                return _compositeSample.SampleA.IsCountExcluded;

            }
            set
            {
                _compositeSample.SampleA.IsCountExcluded = value;
            }
        }
        public bool IsSampleBCountsExcluded
        {
            get
            {
                if (_compositeSample.SampleB == null)
                    return false;
                return _compositeSample.SampleB.IsCountExcluded;
            }
            set
            {
                _compositeSample.SampleB.IsCountExcluded = value;
            }
        }

        public string SampleACounts
        {
            get
            {
                if (_compositeSample.SampleA == null || _compositeSample.SampleA.Counts <= EmptyValue)
                {
                    _logger.Log("CompositeSampleViewModel: "+_compositeSample.SampleType.ToString() + " SampleACounts - EMPTY", Category.Debug, Priority.Low);
                    return String.Empty;
                }

                _logger.Log("CompositeSampleViewModel: " + _compositeSample.SampleType + " SampleACounts - " + _compositeSample.SampleA.Counts, Category.Debug, Priority.Low);
                return _compositeSample.SampleA.Counts.ToString(CultureInfo.InvariantCulture);
            }
            set
            {
                Int32 counts;

                if (!Int32.TryParse(value, out counts))
                    counts = EmptyValue;

                _logger.Log("CompositeSampleViewModel: " + "SET " + _compositeSample.SampleType.ToString() + " SampleACounts - " + counts, Category.Debug, Priority.Low);
                _compositeSample.SampleA.Counts = counts;
            }
        }
        public string SampleBCounts
        {
            get
            {
                if (_compositeSample.SampleB == null || _compositeSample.SampleB.Counts <= EmptyValue)
                {
                    _logger.Log("CompositeSampleViewModel: " + _compositeSample.SampleType.ToString() + " SampleBCounts - EMPTY", Category.Debug, Priority.Low);
                    return String.Empty;
                }

                _logger.Log("CompositeSampleViewModel: " + _compositeSample.SampleType + " SampleBCounts - " + _compositeSample.SampleB.Counts, Category.Debug, Priority.Low);
                return _compositeSample.SampleB.Counts.ToString(CultureInfo.InvariantCulture);
            }
            set
            {
                Int32 counts;

                if (!Int32.TryParse(value, out counts))
                    counts = EmptyValue;

                _logger.Log("CompositeSampleViewModel: " + "SET " + _compositeSample.SampleType.ToString() + " SampleBCounts - " + counts, Category.Debug, Priority.Low);
                _compositeSample.SampleB.Counts = counts;
            }
        }

        public bool IsSampleAHematocritExcluded
        {
            get
            {
                if (_compositeSample.SampleA == null) 
                    return false;

                return _compositeSample.SampleA.IsHematocritExcluded;
            }
            set
            {
                if (_compositeSample.Test.HematocritFill != HematocritFillType.TwoPerSample)
                {
                    var hematocritExclusionNotAllowedAlert = _messageManager.GetMessage(MessageKeys.BvaHematocritExclusionNotAllowed);
                    var messageContent = hematocritExclusionNotAllowedAlert.Description + " [" +
                                         hematocritExclusionNotAllowedAlert.MessageId + "]";
                    _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information, messageContent, "Hematocrit Exclusion",
                                                         MessageBoxChoiceSet.Close);
                    _logger.Log(messageContent, Category.Info, Priority.Low);
                }
                _compositeSample.SampleA.IsHematocritExcluded = value;
            }
        }
        public bool IsSampleBHematocritExcluded
        {
            get
            {
                if (_compositeSample.SampleB == null)
                    return false;

                return _compositeSample.SampleB.IsHematocritExcluded;
            }
            set
            {
                _compositeSample.SampleB.IsHematocritExcluded = value;
            }
        }

        public string HematocritA
        {
            get
            {
                // ReSharper disable CompareOfFloatsByEqualityOperator
                if (_compositeSample.SampleA == null || _compositeSample.SampleA.Hematocrit == EmptyValue)
                    return String.Empty;
                // ReSharper restore CompareOfFloatsByEqualityOperator

                var roundedValue = Math.Round(_compositeSample.SampleA.Hematocrit, 1, MidpointRounding.AwayFromZero);
                return roundedValue.ToString(CultureInfo.InvariantCulture);
            }
            set
            {
               
                if (_compositeSample.SampleA != null)
                {
                    double result; 
                    _compositeSample.SampleA.Hematocrit = Double.TryParse(value, out result) ? result : EmptyValue;
                    _compositeSample.EvaluatePropertyByName("HematocritA", "HematocritB");
                }
            }
        }
        public string HematocritB
        {
            get
            {
                // ReSharper disable CompareOfFloatsByEqualityOperator
                if (_compositeSample.SampleB == null || _compositeSample.SampleB.Hematocrit == EmptyValue)
                    return String.Empty;
                // ReSharper restore CompareOfFloatsByEqualityOperator

                var roundedValue = Math.Round(_compositeSample.SampleB.Hematocrit, 1, MidpointRounding.AwayFromZero);
                return roundedValue.ToString(CultureInfo.InvariantCulture);
            }
            set
            {
                if (_compositeSample.SampleB != null)
                {
                    double result;
                    _compositeSample.SampleB.Hematocrit = Double.TryParse(value, out result) ? result : EmptyValue;
                    _compositeSample.EvaluatePropertyByName("HematocritA", "HematocritB");
                }
            }
        }

        public double UnadjustedBloodVolume
        {
            get { return _compositeSample.UnadjustedBloodVolume; }
            set { _compositeSample.UnadjustedBloodVolume = value; }
        }
        
        string _iTime = String.Empty;
        public string PostInjectionTimeInSeconds
        {
            get
            {
                if (_compositeSample.PostInjectionTimeInSeconds == 0)
                    return String.Empty;
               
                return _iTime;
            }
            set
            {
                int postTimeInSec;

                if (Int32.TryParse(value, out postTimeInSec))
                {
                    _sampleA.PostInjectionTimeInSeconds = postTimeInSec;
                    _sampleB.PostInjectionTimeInSeconds = postTimeInSec;
                }

                base.SetValue(ref _iTime, "PostInjectionTimeInSeconds", value);
            }
        }

        public bool IsWholeSampleExcluded
        {
            get { return _compositeSample.IsWholeSampleExcluded; }
            set { _compositeSample.IsWholeSampleExcluded = value; }
        }

        public bool CountingSampleA
        {
            get { return _countingSampleA; }
            set
            {
                if (base.SetValue(ref _countingSampleA, "CountingSampleA", value))
                {
                    if (!_countingSampleA)
                        _compositeSample.EvaluatePropertyByName("CountsA", "CountsB");
                }
            }
        }
        public bool CountingSampleB
        {
            get { return _countingSampleB; }
            set
            {
                if (base.SetValue(ref _countingSampleB, "CountingSampleB", value))
                {
                    if (!_countingSampleB)
                        _compositeSample.EvaluatePropertyByName("CountsA", "CountsB");
                }
            }
        }

        /// <summary>
        /// Set to true, when BVATest is in manual mode
        /// </summary>
        public bool ManualCountsRequired
        {
            get { return _manualCountsRequired; }
            set { base.SetValue(ref _manualCountsRequired, "ManualCountsRequired", value); }
        }
       
        #endregion

        #region Methods

        public void UpdateCompositeSample(BVACompositeSample newCompositeSample, bool isEnabled = true)
        {
            ContractHelper.ArgumentNotNull(newCompositeSample, "newCompositeSample");

            IsEnabled = isEnabled;
            CompositeSample = newCompositeSample;
            PostInjectionTimeInSeconds = CompositeSample.PostInjectionTimeInSeconds.ToString(CultureInfo.InvariantCulture);

            HematocritA = newCompositeSample.SampleA.Hematocrit.ToString(CultureInfo.InvariantCulture);
            HematocritB = newCompositeSample.SampleB.Hematocrit.ToString(CultureInfo.InvariantCulture);

            FirePropertyChanged("CompositeSample");
            FirePropertyChanged("UnadjustedBloodVolume", "HematocritFill", "SampleACounts", "SampleBCounts");
        }

        #endregion

        #region Helpers

        void SampleAPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Counts")
            {
                FirePropertyChanged("SampleACounts", "SampleBCounts");
            }
            else if (e.PropertyName == "IsHematocritExcluded")
            {
                FirePropertyChanged("IsSampleAHematocritExcluded","HematocritA", "HematocritB");
            }
            else if (e.PropertyName == "IsCountExcluded")
            {
                FirePropertyChanged("IsSampleACountsExcluded", "SampleACounts", "SampleBCounts");
            }
        }
        void SampleBPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Counts")
                FirePropertyChanged("SampleBCounts", "SampleACounts");

            else if (e.PropertyName == "IsHematocritExcluded")
                FirePropertyChanged("IsSampleBHematocritExcluded","HematocritB", "HematocritA");

            else if (e.PropertyName == "IsCountExcluded")
                FirePropertyChanged("IsSampleBCountsExcluded", "SampleBCounts", "SampleACounts");
        }
        void CompositeSamplePropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "UnadjustedBloodVolume")
                FirePropertyChanged("UnadjustedBloodVolume");

            else if (e.PropertyName == "IsWholeSampleExcluded")
            {
                FirePropertyChanged(e.PropertyName);
                FirePropertyChanged("PostInjectionTimeInSeconds", "HematocritA", "HematocritB", "SampleACounts", "SampleBCounts");
            }
            else if (e.PropertyName == "AverageHematocrit")
                FirePropertyChanged("HematocritA", "HematocritB");

            //Don't attempt to compute in the following scenarious
            if (CompositeSample == null || CompositeSample.Test == null)
                return;
            if(CompositeSample.Test.Status == Domain.Common.TestStatus.Running)
                return;

            _logger.Log("CompositeSampleViewModel: CompositeSamplePropertyChanged. SampleType: " + CompositeSample.SampleType + " property: " + e.PropertyName,
                Category.Debug, Priority.Low);
            //Starts recalculations, maybe even step back on ui Thread
            if (CompositeSample.SampleType != SampleType.Background &&
                CompositeSample.SampleType != SampleType.Baseline &&
                CompositeSample.SampleType != SampleType.Standard &&
                CompositeSample.Test != null &&
                BVACompositeSample.PropertiesThatInfluenceUnadjustedBloodVolume.Contains(e.PropertyName))
            {

                //Run Unadjusted blood volume calculation if 
                if ((CompositeSample.AverageCounts > 0 || CompositeSample.AreBothCountsExcluded) &&
                    CompositeSample.AverageHematocrit > 0 && CompositeSample.PostInjectionTimeInSeconds > 0 &&
                    CompositeSample.Test.StandardCompositeSample.AverageCounts > 0
                    && CompositeSample.SampleA.IsCounting == false &&
                    CompositeSample.SampleB.IsCounting == false)
                {

                    //Calculate new unadjusted blood volume
                    var result = _mCalcEngine.GetOnlyUbvResult
                         (CompositeSample.Test.RefVolume,
                         CompositeSample.Test.Tube.AnticoagulantFactor,
                         CompositeSample.Test.StandardCompositeSample.AverageCounts,
                         CompositeSample.Test.DurationAdjustedBackgroundCounts,
                         CompositeSample.Test.PatientBaselineCompositeSample.AverageCounts,
                         new BloodVolumePoint(
                             null,
                             _compositeSample.IsWholeSampleExcluded,
                             _compositeSample.AreBothHematocritsExcluded,
                             _compositeSample.AreBothCountsExcluded,
                             _compositeSample.PostInjectionTimeInSeconds,
                             _compositeSample.AverageCounts,
                             _compositeSample.AverageHematocrit, -1
                             ));

                    UnadjustedBloodVolume = result.UnadjustedBloodVolume > 0 ? Math.Round(result.UnadjustedBloodVolume, 0) : 0;
                }
                else
                {
                    UnadjustedBloodVolume = 0;
                }
            }
        }

        #endregion

        #region Overrides
        protected override void OnDispose()
        {
            if (_compositeSample.SampleA != null)
                _compositeSample.SampleA.PropertyChanged -= SampleAPropertyChanged;

            if (_compositeSample != null)
                _compositeSample.PropertyChanged -= CompositeSamplePropertyChanged;

            _compositeSample = null;

            base.OnDispose();
        }

        #endregion

        #region IDataErrorInfo

        string IDataErrorInfo.Error
        {
            get { return null; }
        }
        string IDataErrorInfo.this[string columnName]
        {
            get
            {
                if (_compositeSample == null)
                    return null;

                string error = String.Empty;
                switch (columnName)
                {
                    case "PostInjectionTimeInSeconds": error = _compositeSample["PostInjectionTimeInSeconds"]; break;
                    case "SampleACounts": error = _compositeSample["CountsA"]; break;
                    case "SampleBCounts": error = _compositeSample["CountsB"]; break;
                    case "HematocritA": error = _compositeSample["HematocritA"]; break;
                    case "HematocritB": error = _compositeSample["HematocritB"]; break;
                }
                
                return error;
            }
        }

        #endregion

        /// <summary>
        /// Fires PropertyChangedEvents to force the rule engine to evaulate rules.
        /// </summary>
        public void ForceRuleEvaluation()
        {
            FirePropertyChanged("HematocritA");
            FirePropertyChanged("HematocritB");
        }
    }
}
