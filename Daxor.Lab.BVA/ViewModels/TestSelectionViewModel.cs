﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Daxor.Lab.BVA.Common;
using Daxor.Lab.BVA.Common.SearchingAndFiltering;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.BVA.Views;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Entities;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;
using Telerik.Windows.Data;

namespace Daxor.Lab.BVA.ViewModels
{
    /// <summary>
    /// The test selection view model
    /// </summary>
    public class TestSelectionViewModel : ViewModelBase, IActiveAware
    {
        #region Fields

        // ReSharper disable once InconsistentNaming
        protected readonly PredicateFilterDescriptor<BVATestItem> _searchFilter;

        private static readonly object LockObject = new object();
        private readonly IBloodVolumeTestController _testController;
        private readonly IAppModuleNavigator _localNavigator;
        protected readonly IEventAggregator EventAggregator;
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly ILoggerFacade _logger;
        private readonly ISettingsManager _settingsManager;
        private readonly IUnityContainer _container;
        private readonly SettingObserver<ISettingsManager> _sObserver;
        private readonly DelegateCommand<Object> _createNewTestCommand;
        private readonly DelegateCommand<BVATestItem> _selectExistingTestCommand;
        private readonly DelegateCommand<Object> _startSearchCommand;
        private readonly DelegateCommand<Object> _endSearchCommand;
        private readonly DelegateCommand<Object> _scrollPageTopCommand;
        private readonly DelegateCommand<Object> _scrollPageUpCommand;
        private readonly DelegateCommand<Object> _scrollPageBottomCommand;
        private readonly DelegateCommand<Object> _scrollPageDownCommand;
        private readonly IPatientSpecificFieldCache _patientSpecificFieldCache;
        private readonly IMessageManager _messageManager;

        private BackgroundTaskManager<QueryableCollectionView> _testsFetcher; 
        private QueryableCollectionView _testInfoItems;
        private string _searchText;
        private string _testsInformation;
        private string _testId2Label;
        private string _searchButtonContent = "SEARCH";
        private bool _isSearchActive;
        private bool _isDatabaseEmpty;
        private bool _isBusyLoading;
        private bool _isInitialDataLoading;
        private BVATestItem _selectedTestItem;
        private BVATestItem _selectedItemBeforeSearch;  //Items used to restore after a user finishes searching.
        private string _startingIndex;
        private string _endingIndex;
        private string _totalItemsInScrollViewer;
        private string _totalSearchItems;
        private string _totalItemsInScrollviewerSourceCollection;
        
        #endregion

        #region Ctor

        public TestSelectionViewModel([Dependency(AppModuleIds.BloodVolumeAnalysis)] IAppModuleNavigator localNavigator, ITestExecutionController testExecutionController,
                                      IEventAggregator eventAggregator, IMessageBoxDispatcher messageBoxDispatcher,
                                      IBloodVolumeTestController testController,
                                      IUnityContainer container, ILoggerFacade logger, IPatientSpecificFieldCache patientSpecificFieldCache, 
                                      ISettingsManager settingsManager, IMessageManager messageManager)
        {
            _testController = testController;
            _localNavigator = localNavigator;
            EventAggregator = eventAggregator;
            _messageBoxDispatcher = messageBoxDispatcher;
            _container = container;
            _logger = logger;
            _settingsManager = settingsManager;
            _patientSpecificFieldCache = patientSpecificFieldCache;
            _messageManager = messageManager;

            if (!_testController.DataService.AreBvaTestTablesAreOnline())
            {
                const string message = "TestSelectionViewModel: The tables that the BackgroundManager needs to build the BVATestList are not all available!";
                _logger.Log(message, Category.Warn, Priority.High);
                throw new ApplicationException(message);
            }

            _sObserver = new SettingObserver<ISettingsManager>(_settingsManager);
            _searchFilter = new PredicateFilterDescriptor<BVATestItem>();
            
            _createNewTestCommand = new DelegateCommand<Object>(CreateNewTestCommandExecute);
            _selectExistingTestCommand = new DelegateCommand<BVATestItem>(SelectExistingTestCommandExecute);

            _startSearchCommand = new DelegateCommand<object>(StartSearchCommandExecute, StartSearchCommandCanExecute);
            _endSearchCommand = new DelegateCommand<object>(EndSearchCommandExecute);

            _scrollPageTopCommand = new DelegateCommand<object>(ScrollPageTopCommandExecute, ScrollPageTopCommandCanExecute);
            _scrollPageUpCommand = new DelegateCommand<object>(ScrollPageUpCommandExecute, ScrollPageUpCommandCanExecute);
            _scrollPageDownCommand = new DelegateCommand<object>(ScrollPageDownCommandExecute, ScrollPageDownCommandCanExecute);
            _scrollPageBottomCommand = new DelegateCommand<object>(ScrollPageBottomCommandExecute, ScrollPageBottomCommandCanExecute);

            // ReSharper disable once DoNotCallOverridableMethodsInConstructor
            SubscribeToBvaTestSavedEvent();
            testExecutionController.TestCompleted += OnTestCompleted;

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            UpdateSearchFilterPredicate();
            ConfigureViewModel();
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Properties

        // ReSharper disable once InconsistentNaming
        public string TestID2Label
        {
            get { return _testId2Label; }
            set
            {
                if (value != null)
                    value = value.ToUpperInvariant();

                SetValue(ref _testId2Label, "TestID2Label", value);
            }
        }

        public QueryableCollectionView TestInfoItems
        {
            get { return _testInfoItems; }
            set
            {
                if (_testInfoItems != null)
                {
                    _testInfoItems.CollectionChanged -= TestInfoItems_CollectionChanged;
                    _testInfoItems.PropertyChanged -= TestInfoItems_PropertyChanged;
                }

                SetValue(ref _testInfoItems, "TestInfoItems", value);

                if (_testInfoItems != null)
                {
                    _testInfoItems.CollectionChanged += TestInfoItems_CollectionChanged;
                    _testInfoItems.PropertyChanged += TestInfoItems_PropertyChanged;
                }

                IsDatabaseEmpty = !(_testInfoItems != null && _testInfoItems.TotalItemCount > 0);
            }
        }

        public BVATestItem SelectedTestItem
        {
            get { return _selectedTestItem; }
            set { SetValue(ref _selectedTestItem, "SelectedTestItem", value); }
        }

        public bool IsSearchActive
        {
            get { return _isSearchActive; }
            set
            {
                SetValue(ref _isSearchActive, "IsSearchActive", value);

                if (value)
                {
                    TestsInformation = TestInfoItems.ItemCount <= 0 ? String.Format("{0}\n{1}", "NO", "SEARCH RESULTS") : "SEARCH RESULTS";
                    SearchButtonContent = "CLEAR SEARCH";
                }
                else
                {
                    TestsInformation = String.Empty;
                    SearchButtonContent = "SEARCH";

                }
            }
        }

        public bool IsInitialDataLoading
        {
            get { return _isInitialDataLoading; }
            set
            {
                SetValue(ref _isInitialDataLoading, "IsInitialDataLoading", value);
                _startSearchCommand.RaiseCanExecuteChanged();
                IsBusyLoading = _isInitialDataLoading;
            }
        }

        public bool IsBusyLoading
        {
            get { return _isBusyLoading; }
            set { SetValue(ref _isBusyLoading, "IsBusyLoading", value); }
        }

        public bool IsDatabaseEmpty
        {
            get { return _isDatabaseEmpty; }
            set
            {
                SetValue(ref _isDatabaseEmpty, "IsDatabaseEmpty", value);

                if (!IsSearchActive)
                    TestsInformation = value ? "Database Is Empty" : String.Empty;

                _startSearchCommand.RaiseCanExecuteChanged();
            }
        }

        public string TestsInformation
        {
            get { return _testsInformation; }
            set { SetValue(ref _testsInformation, "TestsInformation", value); }
        }

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (SetValue(ref _searchText, "SearchText", value))
                    UpdateSearchFilterPredicate();
            }
        }

        public string SearchButtonContent
        {
            get { return _searchButtonContent; }
            set { SetValue(ref _searchButtonContent, "SearchButtonContent", value); }
        }

        public string StartingIndex
        {
            get { return _startingIndex; }
            set { SetValue(ref _startingIndex, "StartingIndex", value); }
        }

        public string EndingIndex
        {
            get { return _endingIndex; }
            set { SetValue(ref _endingIndex, "EndingIndex", value); }
        }

        public string TotalItemsInScrollViewer
        {
            get { return _totalItemsInScrollViewer; }
            set { SetValue(ref _totalItemsInScrollViewer, "TotalItemsInScrollViewer", value); }
        }

        public string TotalSearchItems
        {
            get { return _totalSearchItems; }
            set { SetValue(ref _totalSearchItems, "TotalSearchItems", value); }
        }

        public string TotalItemsInScrollviewerSourceCollection
        {
            get { return _totalItemsInScrollviewerSourceCollection; }
            set { SetValue(ref _totalItemsInScrollviewerSourceCollection, "TotalItemsInScrollviewerSourceCollection", value); }
        }

        #endregion

        #region Commands

        public ICommand CreateNewTestCommand
        {
            get { return _createNewTestCommand; }
        }

        void CreateNewTestCommandExecute(Object nullObject)
        {
            try
            {
                _logger.Log("BEGIN NEW BVA TEST.", Category.Info, Priority.None);

                if (_testController.CreateTestCommand.CanExecute(nullObject))
                    _testController.CreateTestCommand.Execute(nullObject);

                //Start navigation process
                _localNavigator.ActivateView(BVAModuleViewKeys.PatientDemographics);
            }
            catch (Exception ex)
            {
                var message = _messageManager.GetMessage(MessageKeys.BvaCreateTestFailed).FormattedMessage;
                _logger.Log("CreateNewTestCommandExecute failed: " + ex.Message + "; Stack trace: " + ex.StackTrace, Category.Exception, Priority.High);
                _logger.Log(message, Category.Info, Priority.Low);
                _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning, message, Constants.ErrorMessageCaption, new [] {"Return to BVA Test List"});
            }
        }

        public ICommand SelectExistingTestCommand
        {
            get { return _selectExistingTestCommand; }
        }

        void SelectExistingTestCommandExecute(BVATestItem selectedItem)
        {
            if (selectedItem == null)
                return;

            var choices = new[] { "Proceed", "Cancel" };
            object vm = null;
            Func<int> askUserIfTheyWantToOpenOrDeleteSelectedTest = (
                () => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information, _container.Resolve<TestSelectionDialogView>(), "Open Test", choices, out vm));

            if (selectedItem.Status == TestStatus.Pending)
            {
                //Ask user if he/she would like to open or delete a given test
               // MessageBoxReturnResult result = AskUserIfTheyWantToOpenOrDeleteSelectedTest();
                if (choices[askUserIfTheyWantToOpenOrDeleteSelectedTest()] == "Cancel")
                    return;

                var specific = vm as TestSelectionDialogViewModel;
                if (specific == null)
                    return;

                //User wants to delete a given test
                try
                {
                    if (specific.SelectedAction == TestSelectionDialogActions.DeleteTest)
                    {
                        _testController.DataService.DeleteTest(selectedItem.TestID);

                        //Remove from the testinfo list
                        lock (LockObject)
                        {
                            //Removes currently selected item
                            TestInfoItems.Remove(selectedItem);
                            return;
                        }
                    }
                }
                catch
                {
                    var message = _messageManager.GetMessage(MessageKeys.BvaTestDeletionFailed).FormattedMessage;
                    _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, message, "Delete Test Issues", new[] { "Return to BVA Test List" });
                    _logger.Log(message, Category.Info, Priority.Low);
                    return;
                }
            }

            //Need to load it and navigate 
            var task = Task.Factory.StartNew(objectState =>
            {
                EventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, Constants.LoadingSelectedTestMessage));
                var testItem = (BVATestItem)objectState;

                if (_testController.SelectTestCommand.CanExecute(testItem))
                {
                    _testController.SelectTestCommand.Execute(testItem);
                    return _testController.SelectedTest;
                }

                return null;

            }, selectedItem);
            
            //and setup continuation only when failed
            task.ContinueWith(ant =>
            {
                EventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
                var aggEx = ant.Exception;

                if (aggEx != null)
                    foreach (var ex in aggEx.InnerExceptions)
                    {
                        _logger.Log(ex.Message, Category.Exception, Priority.High);
                    }

                var message = _messageManager.GetMessage(MessageKeys.BvaLoadTestFailed).FormattedMessage;
                _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning, message, Constants.ErrorMessageCaption, new [] {"Return to BVA Test List"});
                _logger.Log(message, Category.Info, Priority.Low);

            }, CancellationToken.None, TaskContinuationOptions.OnlyOnFaulted, TaskScheduler.FromCurrentSynchronizationContext());

            //and setup continuation only on run to completion
            task.ContinueWith(ant =>
            {
                EventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));

                //Begin Navigation
                var selectedTest = ant.Result;
                if (selectedItem.Status == TestStatus.Completed && selectedTest.Mode != Core.TestMode.Manual)
                {
                    _localNavigator.ActivateView(BVAModuleViewKeys.TestSampleValues);
                }
                else
                {
                    if (selectedTest.Mode == Core.TestMode.Manual && selectedTest.Protocol > 0)
                        _localNavigator.ActivateView(BVAModuleViewKeys.TestSampleValues);
                    else if (selectedItem.Status == TestStatus.Pending)
                        _localNavigator.ActivateView(BVAModuleViewKeys.TestReview);
                    else
                        _localNavigator.ActivateView(BVAModuleViewKeys.TestSampleValues);
                }

            }, CancellationToken.None, TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler.FromCurrentSynchronizationContext());
        }

        public ICommand StartSearchCommand
        {
            get { return _startSearchCommand; }
        }

        void StartSearchCommandExecute(object arg)
        {
            //Stop search if search is active
            if (IsSearchActive)
                EndSearchCommandExecute(null);
            else
                IsSearchActive = true;

            _selectedItemBeforeSearch = SelectedTestItem;
            _logger.Log("SEARCH BUTTON CLICKED", Category.Info, Priority.None);
        }

        bool StartSearchCommandCanExecute(object arg)
        {
            return !IsDatabaseEmpty && !IsInitialDataLoading;
        }

        public ICommand EndSearchCommand
        {
            get { return _endSearchCommand; }
        }

        void EndSearchCommandExecute(object arg)
        {
            IsSearchActive = false;
            SearchText = String.Empty;

            VirtualKeyboardManager.CloseActiveKeyboard();
            _logger.Log("SEARCH BUTTON CLOSED", Category.Info, Priority.None);

            if (SelectedTestItem == null)
            {
                SelectedTestItem = _selectedItemBeforeSearch;
                _selectedItemBeforeSearch = null;
            }
        }

        public ICommand ScrollPageTopCommand { get { return _scrollPageTopCommand; } }

        void ScrollPageTopCommandExecute(object arg)
        {
            EventAggregator.GetEvent<ScrollViewerScrollChanged>().Publish(ScrollViewerScrollChangedEventPayload.ScrollTop);
        }

        bool ScrollPageTopCommandCanExecute(object arg)
        {
            return !IsInitialDataLoading && StartingIndex != "1" && StartingIndex != "-" && StartingIndex != "0";
        }

        public ICommand ScrollPageUpCommand { get { return _scrollPageUpCommand; } }

        void ScrollPageUpCommandExecute(object arg)
        {
            EventAggregator.GetEvent<ScrollViewerScrollChanged>().Publish(ScrollViewerScrollChangedEventPayload.ScrollPageUp);
        }

        bool ScrollPageUpCommandCanExecute(object arg)
        {
            return !IsInitialDataLoading && StartingIndex != "1" && StartingIndex != "-" && StartingIndex != "0";
        }

        public ICommand ScrollPageDownCommand { get { return _scrollPageDownCommand; } }

        void ScrollPageDownCommandExecute(object arg)
        {
            EventAggregator.GetEvent<ScrollViewerScrollChanged>().Publish(ScrollViewerScrollChangedEventPayload.ScrollPageDown);
        }

        bool ScrollPageDownCommandCanExecute(object arg)
        {
            return !IsInitialDataLoading && EndingIndex != TotalItemsInScrollViewer;
        }

        public ICommand ScrollPageBottomCommand { get { return _scrollPageBottomCommand; } }

        void ScrollPageBottomCommandExecute(object arg)
        {
            EventAggregator.GetEvent<ScrollViewerScrollChanged>().Publish(ScrollViewerScrollChangedEventPayload.ScrollBottom);
        }

        bool ScrollPageBottomCommandCanExecute(object arg)
        {
            return !IsInitialDataLoading && EndingIndex != TotalItemsInScrollViewer;
        }

        #endregion
        
        #region IActiveAware Members

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                _patientSpecificFieldCache.UninitializePatientSpecificFieldCache();
                _logger.Log("TestSelectionViewModel: TestSelectionViewModel active: " + _isActive, Category.Debug, Priority.Low);
            }
        }
        public event EventHandler IsActiveChanged = delegate { };

        #endregion

        #region Helpers

        protected virtual void SubscribeToBvaTestSavedEvent()
        {
            EventAggregator.GetEvent<BvaTestSaved>().Subscribe(OnBvaTestSaved, ThreadOption.UIThread, false);
        }

        void OnTestCompleted(object sender, TestExecutionEventArgs e)
        {
            (from t in TestInfoItems.SourceCollection.AsParallel().Cast<BVATestItem>()
             where t.TestID == e.ExecutingTestId
             select t).ForAll(bvaTestItem =>
             {
                 bvaTestItem.Status = TestStatus.Completed;
                 if (_testController.RunningTest != null)
                     bvaTestItem.HasSufficientData = _testController.RunningTest.HasSufficientData;
             });
        }
        
        protected virtual void ConfigureViewModel()
        {
            TestID2Label = _settingsManager.GetSetting<String>(SettingKeys.BvaTestId2Label);

            //Starts observing system settings
            _sObserver.RegisterHandler(SettingKeys.BvaTestId2Label, s => TestID2Label = s.GetSetting<String>(SettingKeys.BvaTestId2Label));
            _sObserver.RegisterHandler(SettingKeys.BvaTestListVopsTestsDisplayed, s => _testsFetcher.RunBackgroundTask());
            _sObserver.RegisterHandler(SettingKeys.BvaTestListAbortedTestsDisplayed, s => _testsFetcher.RunBackgroundTask());

            //Configures background workers and starts fetching BVA tests.
            SetupBackgroundTestLoaders();
            _testsFetcher.RunBackgroundTask();
            EventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Subscribe(OnScrollViewerPageInfoChanged);
        }

        protected void SetupBackgroundTestLoaders()
        {
            const string testRetrievalErrorMessage = "TestSelectionViewModel: Could not retrieve the BVATestList in the BackgroundTaskManager. ";
            //Setting up background tests fetcher; fetches all BVA test from the database
            _testsFetcher = new BackgroundTaskManager<QueryableCollectionView>( () => GetBvaTestItemsList(testRetrievalErrorMessage),
                                                                                result => ProcessBvaTestItemsList(testRetrievalErrorMessage, result));
        }

        protected virtual void ProcessBvaTestItemsList(string testRetrievalErrorMessage, QueryableCollectionView result)
        {
            if (result == null)
                throw new ApplicationException(testRetrievalErrorMessage);

            TestInfoItems = result;
            IsInitialDataLoading = false;
            TestInfoItems.FilterDescriptors.Add(_searchFilter);
        }

        protected virtual QueryableCollectionView GetBvaTestItemsList(string testRetrievalErrorMessage)
        {
            //Wrapping in ObservableCollection to be able to remove/add items.

            QueryableCollectionView bvaTestItemList;

            IsInitialDataLoading = true;
            try
            {
                bvaTestItemList = new QueryableCollectionView(new ObservableCollection<BVATestItem>(_testController.DataService.SelectBVATestItems()));
            }
            catch (Exception ex)
            {
                bvaTestItemList = null;
                _testController.Logger.Log(testRetrievalErrorMessage + ex.Message, Category.Exception, Priority.High);
            }

            return bvaTestItemList;
        }

        protected virtual void UpdateSearchFilterPredicate()
        {
            if (_searchFilter == null)
                return;

            _searchFilter.Predicate = t => DoesTestMatchSearchCriteria(t);
        }

        private bool DoesTestMatchSearchCriteria(BVATestItem t)
        {
            if (string.IsNullOrWhiteSpace(SearchText))
                return true;
            if (t.PatientFullName.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                return true;
            if (t.PatientHospitalID.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                return true;
            if (t.TestID2.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                return true;
            if (t.TestStatusDescription.Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                return true;
            if (t.PatientDOB.HasValue && t.PatientDOB.Value.ToString("MM/dd/yyyy").Contains(SearchText, StringComparison.OrdinalIgnoreCase))
                return true;
            var matchesCreationDate = t.CreatedDate.ToString("MM/dd/yyyy h:mmt").Contains(SearchText, StringComparison.OrdinalIgnoreCase);

            return matchesCreationDate;
        }

        void TestInfoItems_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            IsDatabaseEmpty = !(_testInfoItems != null && _testInfoItems.TotalItemCount > 0);
        }

        private void TestInfoItems_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (IsSearchActive)
                TestsInformation = TestInfoItems.ItemCount <= 0 ? String.Format("{0}\n{1}", "NO", "SEARCH RESULTS") : "SEARCH RESULTS";
        }

        protected virtual void EvaluateScrollCommandCanExecute()
        {
            _scrollPageBottomCommand.RaiseCanExecuteChanged();
            _scrollPageDownCommand.RaiseCanExecuteChanged();
            _scrollPageTopCommand.RaiseCanExecuteChanged();
            _scrollPageUpCommand.RaiseCanExecuteChanged();
        }

        private void CalculatePageInfoForScrollViewer(ScrollViewerPageInfoChangedPayload scrollViewerPageInfo)
        {
            var itemCount = scrollViewerPageInfo.ItemCount;
            var totalItemCount = scrollViewerPageInfo.TotalItemCount;
            var extentHeight = scrollViewerPageInfo.ScrollViewerExtentHeight;
            var viewportHeight = scrollViewerPageInfo.ScrollViewerViewportHeight;
            var verticalOffset = scrollViewerPageInfo.ScrollViewerVerticalOffset;

            var itemHeight = extentHeight/itemCount;
            var pageHeight = viewportHeight;

            StartingIndex = ((int) Math.Round(verticalOffset/itemHeight) + 1).ToString(CultureInfo.InvariantCulture);
            var endingIndex = ((int) Math.Round((verticalOffset + pageHeight)/itemHeight));

            // ReSharper disable once ConvertIfStatementToConditionalTernaryExpression
            if (endingIndex > itemCount)
                EndingIndex = itemCount.ToString(CultureInfo.InvariantCulture);
            else
                EndingIndex = ((int) Math.Round((verticalOffset + pageHeight)/itemHeight)).ToString(CultureInfo.InvariantCulture);

            TotalItemsInScrollViewer = itemCount.ToString(CultureInfo.InvariantCulture);
            TotalSearchItems = itemCount.ToString(CultureInfo.InvariantCulture);
            TotalItemsInScrollviewerSourceCollection = totalItemCount.ToString(CultureInfo.InvariantCulture);

            if (itemCount != 0) return;

            EndingIndex = "0";
            StartingIndex = "0";
            TotalSearchItems = "0";
        }

        #endregion

        #region Aggregate event handlers

        void OnScrollViewerPageInfoChanged(ScrollViewerPageInfoChangedPayload scrollViewerPageInfo)
        {
            if (scrollViewerPageInfo.AppModuleId != AppModuleIds.BloodVolumeAnalysis) return;

            CalculatePageInfoForScrollViewer(scrollViewerPageInfo);
            EvaluateScrollCommandCanExecute();
        }

        protected void OnBvaTestSaved(Guid savedTestId)
        {
            var currentTestItem = _testController.DataService.SelectBVATestItem(savedTestId);
            
            var matchingTestItem = (from t in TestInfoItems.SourceCollection.AsParallel().Cast<BVATestItem>()
                    where t.TestID == currentTestItem.TestID
                    select t).FirstOrDefault<BVATestItem>();

            if (matchingTestItem == null)
            {
                lock (TestInfoItems)
                {
                    TestInfoItems.AddNew(currentTestItem);
                    TestInfoItems.CommitNew();
                    SelectedTestItem = currentTestItem;
                }
            }
            else
            {
                matchingTestItem.CreatedDate = currentTestItem.CreatedDate;
                matchingTestItem.HasSufficientData = currentTestItem.HasSufficientData;
                matchingTestItem.PatientDOB = currentTestItem.PatientDOB;
                matchingTestItem.PatientFullName = currentTestItem.PatientFullName;
                matchingTestItem.PatientHospitalID = currentTestItem.PatientHospitalID;
                matchingTestItem.PatientPID = currentTestItem.PatientPID;
	            matchingTestItem.AmputeeStatus = currentTestItem.AmputeeStatus;
                matchingTestItem.TestID = currentTestItem.TestID;
                matchingTestItem.TestID2 = currentTestItem.TestID2;
                matchingTestItem.TestModeDescription = currentTestItem.TestModeDescription;
                matchingTestItem.Status = currentTestItem.Status;
                matchingTestItem.TestStatusDescription = currentTestItem.TestStatusDescription;
                matchingTestItem.WasSaved = currentTestItem.WasSaved;
            }

            (from t in TestInfoItems.SourceCollection.AsParallel().Cast<BVATestItem>()
             where t.PatientPID == currentTestItem.PatientPID
             select t).ForAll(testItem =>
             {
                 testItem.PatientFullName = currentTestItem.PatientFullName;
                 testItem.PatientDOB = currentTestItem.PatientDOB;
                 testItem.PatientHospitalID = currentTestItem.PatientHospitalID;
             });
        }

        #endregion
    }
}
