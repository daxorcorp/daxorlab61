﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.BVA.ViewModels
{
    public class InnerMatrixVolumeViewModel : ObservableObject
    {
        #region Fields

        string _title;
        int _measuredVolume;
        int _idealVolume;

        #endregion

        #region Properties

        public string Title
        {
            get { return _title; }
            set
            {
                SetValue(ref _title, "Title", value);
            }
        }

        public int MeasuredVolume
        {
            get { return _measuredVolume; }
            set
            {
                SetValue(ref _measuredVolume, "MeasuredVolume", value);
                FirePropertyChanged("NumericDeviation", "PercentDeviation");
            }
        }

        public int IdealVolume
        {
            get { return _idealVolume; }
            set
            {
                SetValue(ref _idealVolume, "IdealVolume", value);
                FirePropertyChanged("NumericDeviation", "PercentDeviation");
            }
        }

        public int NumericDeviation
        {
            get
            {
                return MeasuredVolume - IdealVolume;
            }
        }

        public double PercentDeviation
        {
            get
            {
                return NumericDeviation / (double)IdealVolume * 100.00;
            }
        }

        public VolumeType VolumeType { get; set; }

        #endregion
    }
}