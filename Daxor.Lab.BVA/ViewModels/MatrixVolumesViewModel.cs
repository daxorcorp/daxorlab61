﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Daxor.Lab.BVA.Core;

namespace Daxor.Lab.BVA.ViewModels
{
    public class MatrixVolumesViewModel
    {
        #region Fields

        private readonly IDictionary<VolumeType, InnerMatrixVolumeViewModel> _innerVolumeCollection = new Dictionary<VolumeType, InnerMatrixVolumeViewModel>();
        private readonly WholeBloodViewModel _mBloodVolume;
        private readonly WholeBloodViewModel _iBloodVolume;

        #endregion

        #region Ctor

        public MatrixVolumesViewModel(WholeBloodViewModel measuredBlood, WholeBloodViewModel idealBlood)
        {
            EnsureValidArguments(measuredBlood, idealBlood);

            _mBloodVolume = measuredBlood;
            _iBloodVolume = idealBlood;

            //Builds matrix volume
            BuildMatrix();

            //Hook up to the property changed event of blood volume
            _mBloodVolume.PropertyChanged += BloodVolume_PropertyChanged;
            _iBloodVolume.PropertyChanged += BloodVolume_PropertyChanged;
        }

        #endregion

        #region Indexers

        public InnerMatrixVolumeViewModel this[VolumeType key]
        {
            get { return _innerVolumeCollection[key]; }
        }

        public WholeBloodViewModel this[BloodType key]
        {
            get
            {
                switch (key)
                {
                    case BloodType.Measured:
                        return _mBloodVolume;
                    case BloodType.Ideal:
                        return _iBloodVolume;
                }
                return null;
            }
        }

        #endregion

        #region Helpers

        void BloodVolume_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            int newValue;
            VolumeType uVolumeType;

            if (e.PropertyName == "RedCellCount")
            {
                uVolumeType = VolumeType.RedCell;
                newValue = ((WholeBloodViewModel)sender).RedCellCount;
            }
            else if (e.PropertyName == "PlasmaCount")
            {
                uVolumeType = VolumeType.Plasma;
                newValue = ((WholeBloodViewModel)sender).PlasmaCount;
            }
            else
            {
                uVolumeType = VolumeType.WholeBlood;
                newValue = ((WholeBloodViewModel)sender).WholeCount;
            }

            //Update appropriate volume; either measured or ideal
            if (!_innerVolumeCollection.Keys.Contains(uVolumeType)) return;

            if (sender == _mBloodVolume)
                _innerVolumeCollection[uVolumeType].MeasuredVolume = newValue;
            else
                _innerVolumeCollection[uVolumeType].IdealVolume = newValue;
        }

	    // ReSharper disable UnusedParameter.Local
        void EnsureValidArguments(WholeBloodViewModel mBlood, WholeBloodViewModel iBlood)
		// ReSharper restore UnusedParameter.Local
        {
            if (mBlood == null)
                throw new ArgumentNullException("mBlood");
            if (iBlood == null)
                throw new ArgumentNullException("iBlood");
            if (mBlood.Type != BloodType.Measured)
                throw new ArgumentException("MeasuredBlood argument is of a wrong type.");
            if (iBlood.Type != BloodType.Ideal)
                throw new ArgumentException("IdealBlood argument is of a wrong type.");
        }

        void BuildMatrix()
        {
            //Add volumes to the internal dictionary
            _innerVolumeCollection[VolumeType.WholeBlood] = new InnerMatrixVolumeViewModel
                {
                    VolumeType = VolumeType.WholeBlood,
                    Title = "Blood Vol.",
                    MeasuredVolume = _mBloodVolume.WholeCount,
                    IdealVolume = _iBloodVolume.WholeCount
                };

            _innerVolumeCollection[VolumeType.Plasma] = new InnerMatrixVolumeViewModel
                {
                    VolumeType = VolumeType.Plasma,
                    Title = "Plasma Vol.",
                    MeasuredVolume = _mBloodVolume.PlasmaCount,
                    IdealVolume = _iBloodVolume.PlasmaCount
                };

            _innerVolumeCollection[VolumeType.RedCell] = new InnerMatrixVolumeViewModel
                {
                    VolumeType = VolumeType.RedCell,
                    Title = "Red Cell Vol.",
                    MeasuredVolume = _mBloodVolume.RedCellCount,
                    IdealVolume = _iBloodVolume.RedCellCount
                };
        }

        #endregion
    }
}