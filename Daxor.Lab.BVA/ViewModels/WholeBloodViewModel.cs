﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.BVA.ViewModels
{
    public class WholeBloodViewModel : ObservableObject
    {
        #region Fields

        int _redCellCount;
        int _plasmaCount;

        #endregion

        #region Properties
        public string BloodTypeDescription
        {
            get
            {
                var typeName = Type.ToString();
                if (Type == BloodType.Measured)
                    typeName = "BVA";
                return typeName + " (mL)";
            }
        }

        public BloodType Type
        {
            set;
            get;
        }
        public int RedCellCount
        {
            get { return _redCellCount; }
            set
            {
                if (_redCellCount == value)
                    return;

                _redCellCount = value;
                FirePropertyChanged("RedCellCount");
                FirePropertyChanged("WholeCount");
            }
        }
        public int PlasmaCount
        {
            get { return _plasmaCount; }
            set
            {
                if (_plasmaCount == value)
                    return;

                _plasmaCount = value;
                FirePropertyChanged("PlasmaCount");
                FirePropertyChanged("WholeCount");
            }
        }
        public int WholeCount
        {
            get { return _redCellCount + _plasmaCount; }
        }
        #endregion
    }
}