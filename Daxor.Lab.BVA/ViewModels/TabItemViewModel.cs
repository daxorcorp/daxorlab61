﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.Base;
using Microsoft.Practices.Composite.Presentation.Commands;

namespace Daxor.Lab.BVA.ViewModels
{
    public class TabItemViewModel : ViewModelBase
    {
        #region Fields

        private string _header;
        private DelegateCommand<object> _tabNavigationCommand;

        #endregion

        #region Ctor
        
        public TabItemViewModel(Guid uniqueID)
        {
            UniqueID = uniqueID;
        }
        
        #endregion

        #region Properties
        public DelegateCommand<object> TabNavigationCommand
        {
            get { return _tabNavigationCommand; }
            set { base.SetValue<DelegateCommand<object>>(ref _tabNavigationCommand, "TabNavigationCommand", value); }
        }
        public string Header
        {
            get { return _header; }
            set { base.SetValue<string>(ref _header, "Header", value); }
        }
        public Guid UniqueID
        {
            private set;
            get;
        }

        #endregion

        #region Ovverides

        public override string ToString()
        {
            return _header;
        }

        #endregion
    }
}
