﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Reports.Interfaces;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.BVA.Reports.ReportControllers;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;

// TODO: See if we should get the BvaReportController from Unity instead of newing it up. (Geoff; 6/13/2012)

namespace Daxor.Lab.BVA.ViewModels
{
    /// <summary>
    /// The test analysis view model
    /// </summary>
    public class TestAnalysisViewModel : ViewModelBase, IDataErrorInfo
    {
        #region Constants

        public static readonly string BloodVolumeFormatString = "{0:g0}";
        public static readonly string BloodVolumeUnits = "mL";
        public static readonly string MinimumBloodVolumeWithoutUnits = String.Format("<" + BloodVolumeFormatString, BvaDomainConstants.MinimumBloodVolumeInmL);
        public static readonly string MaximumBloodVolumeWithoutUnits = String.Format(">" + BloodVolumeFormatString, BvaDomainConstants.MaximumBloodVolumeInmL);
        public static readonly string MinimumBloodVolume = MinimumBloodVolumeWithoutUnits + " " + BloodVolumeUnits;
        public static readonly string MaximumBloodVolume = MaximumBloodVolumeWithoutUnits + " " + BloodVolumeUnits;
        public static readonly string ValueNotDisplayed = "-";
        public static readonly string AutomaticPointExclusionCaption = "Automatic Point Exclusion";
        public static readonly int AutomaticPointExclusionExecutionDelayInMilliseconds = 250;
        public static readonly string LogFormatString = "TestAnalysisViewModel; Current test ID = {0}; Method = {1}; {2}";

        #endregion

        #region Fields

        const double MaxXValIncrement = 5;

        readonly IPopupController _popupController;
        readonly IEventAggregator _eventAggregator;
        readonly ITestExecutionController _testExecutionService;
        readonly ILoggerFacade _logger;
        readonly IMessageBoxDispatcher _messageBoxDispatcher;
        readonly IRegionManager _regionManager;
        readonly IBloodVolumeTestController _controller;
        readonly ObservableCollection<TabItemViewModel> _testResultsTabItems = new ObservableCollection<TabItemViewModel>();
        readonly IDictionary<BloodType, WholeBloodViewModel> _wholeBloodVolumes = new Dictionary<BloodType, WholeBloodViewModel>();
        readonly IBvaResultAlertService _resultAlertService;
        private readonly IZipFileFactory _zipFileFactory;

        readonly AddRangeObservableCollection<BestFitLinePoint> _bestFitLinePoints = new AddRangeObservableCollection<BestFitLinePoint>();
        readonly AddRangeObservableCollection<UnadjustedCompositeBloodPointViewModel> _ubvPoints = new AddRangeObservableCollection<UnadjustedCompositeBloodPointViewModel>();
        private MatrixVolumesViewModel _matrixVolumesViewModel;
        readonly IIdealsCalcEngineService _calcEngine;
        private SettingObserver<ISettingsManager> _sObserver;

        BVATest _currentTest;
        PropertyObserver<BVATest> _testObserver;
        // ReSharper disable once CollectionNeverQueried.Local
        List<PropertyObserver<BVACompositeSample>> _compositeSampleObservers;
        // ReSharper disable once CollectionNeverQueried.Local
        List<PropertyObserver<BVASample>> _sampleObservers;
        private List<BVACompositeSample> _cachedCompositeSamples;
        BVAVolume _idealVolume;
        BVAVolume _measuredVolume;

        bool _autoRangeOn;
        bool _isTestAnalysisActive;
        int _scPosition;
        bool _isReportBeingViewed;


        string _sModeDescription, _sampleName, _sampleProgress;

        private bool _automaticPointExclusionFinished = true;
        private bool _isAutomaticExclusionDisabledForThisTest;
        private int _automaticPointExclusionSemaphore;

        private readonly IStarBurnWrapper _opticalDriveWrapper;
        private readonly IAuditTrailDataAccess _auditTrailDataAccess;
        private readonly IFolderBrowser _folderBrowser;
        private readonly ISettingsManager _settingsManager;
        private readonly IMessageManager _messageManager;
	    private readonly IPdfPrinter _pdfPrinter;

        #endregion

        #region Ctor

        public TestAnalysisViewModel([Dependency(AppModuleIds.BloodVolumeAnalysis)] IPopupController popupController,
                                     IEventAggregator eventAggregator, ITestExecutionController testExecutionService,
                                     ILoggerFacade customLoggerFacade, IMessageBoxDispatcher messageBoxDispatcher,
                                     IRegionManager regionManager, IBloodVolumeTestController controller, 
                                     IIdealsCalcEngineService calcEngine,
                                     [Dependency(AppModuleIds.BloodVolumeAnalysis)] IBvaResultAlertService resultAlertService,
                                     IBestFitLinePointsCalculator bestFitLinePointsCalculator,
                                     IAutomaticPointExclusionService automaticPointExclusionService,
                                     IStarBurnWrapper driveWrapper, 
                                     IAuditTrailDataAccess auditTrailDataAccess,
                                     IFolderBrowser folderBrowser,
                                     IMessageManager messageManager,
                                     ISettingsManager settingsManager,
                                     IZipFileFactory zipFileFactory,
									 IPdfPrinter pdfPrinter)
        {
            _popupController = popupController;
            _eventAggregator = eventAggregator;
            _testExecutionService = testExecutionService;
            _logger = customLoggerFacade;
            _messageBoxDispatcher = messageBoxDispatcher;
            _regionManager = regionManager;
            _controller = controller;
            _calcEngine = calcEngine;
            _resultAlertService = resultAlertService;
            _opticalDriveWrapper = driveWrapper;
            _auditTrailDataAccess = auditTrailDataAccess;
            _folderBrowser = folderBrowser;
            _settingsManager = settingsManager;
            _messageManager = messageManager;
            AutomaticPointExclusionService = automaticPointExclusionService;
            BestFitLinePointsCalculator = bestFitLinePointsCalculator;
            _zipFileFactory = zipFileFactory;
	        _pdfPrinter = pdfPrinter;

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InitializeViewModel();
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        protected virtual void InitializeViewModel()
        {
            if (BestFitLinePointsCalculator == null)
                throw new NotSupportedException("IBestFitLinePointsCalculator instance cannot be null");

            var pointExclusionAlert = _messageManager.GetMessage(MessageKeys.BvaAutomaticPointExclusionCompletionFailed);
            AutomaticPointExclusionErrorMessage = pointExclusionAlert.FormattedMessage;

            var pointExclusionHighStdDevAlert = _messageManager.GetMessage(MessageKeys.BvaAutomaticPointExclusionCouldNotLowerStdDev);
            AutomaticPointExclusionCouldNotLowerStdDevMessage = pointExclusionHighStdDevAlert.FormattedMessage;

            var noFurtherEvaluationAlert = _messageManager.GetMessage(MessageKeys.BvaAutomaticPointExclusionNoFurtherEvaluation);
            AutomaticPointExclusionNoFurtherEvaluationMessage = noFurtherEvaluationAlert.FormattedMessage;

            var pointExclusionHighRseAlert = _messageManager.GetMessage(MessageKeys.BvaAutomaticPointExclusionHighRse);
            AutomaticPointExclusionHighRseMessage = pointExclusionHighRseAlert.FormattedMessage;

            SettingObserver = new SettingObserver<ISettingsManager>(_settingsManager);

            _wholeBloodVolumes[BloodType.Measured] = new WholeBloodViewModel { Type = BloodType.Measured, PlasmaCount = 0, RedCellCount = 0 };
            _wholeBloodVolumes[BloodType.Ideal] = new WholeBloodViewModel { Type = BloodType.Ideal, PlasmaCount = 0, RedCellCount = 0 };

            //Instantiate matrix volumes
            VolumesMatrix = new MatrixVolumesViewModel(_wholeBloodVolumes[BloodType.Measured], _wholeBloodVolumes[BloodType.Ideal]);
            _wholeBloodVolumes[BloodType.Measured].PropertyChanged += OnBloodVolumesPropertyChanged;
	        _wholeBloodVolumes[BloodType.Ideal].PropertyChanged += OnBloodVolumesPropertyChanged;
            OnBloodVolumesPropertyChanged(null, null);

            ConfigureViewModel();
            SubscribeToAggregateEvents();
        }

        void OnBloodVolumesPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // If the blood volumes change, the blood volume matrix chart needs to
            // know to get potentially updated values.
            FirePropertyChanged("MatrixMeasuredBloodVolume");
            FirePropertyChanged("MatrixDeviationMillilitersBloodVolume");
            FirePropertyChanged("MatrixDeviationPercentageBloodVolume");

            FirePropertyChanged("MatrixMeasuredPlasmaVolume");
            FirePropertyChanged("MatrixDeviationMillilitersPlasmaVolume");
            FirePropertyChanged("MatrixDeviationPercentagePlasmaVolume");

            FirePropertyChanged("MatrixMeasuredRedCellVolume");
            FirePropertyChanged("MatrixDeviationMillilitersRedCellVolume");
            FirePropertyChanged("MatrixDeviationPercentageRedCellVolume");
        }

        #endregion

        #region Properties

        public string AutomaticPointExclusionErrorMessage { get; protected set; }

        public string AutomaticPointExclusionNoFurtherEvaluationMessage { get; protected set; }

        public string AutomaticPointExclusionCouldNotLowerStdDevMessage { get; protected set; }

        public string AutomaticPointExclusionHighRseMessage { get; protected set; }

        private SettingObserver<ISettingsManager> SettingObserver
        {
            get { return _sObserver; }
            set
            {
                // Only get one assignment
                if (_sObserver == null)
                    _sObserver = value;
            }
        }

        public IBestFitLinePointsCalculator BestFitLinePointsCalculator { get; private set; }

        public IAutomaticPointExclusionService AutomaticPointExclusionService { get; private set; }

        protected MatrixVolumesViewModel VolumesMatrix
        {
            get { return _matrixVolumesViewModel; }
            set
            {
                // Only get one assignment
                if (_matrixVolumesViewModel == null)
                    _matrixVolumesViewModel = value;
            }
        }

        public BVATest CurrentTest
        {
            get { return _currentTest; }
            set
            {
                if (SetValue(ref _currentTest, "CurrentTest", value))
                {
                    InitializeTestPropertyObserver(_currentTest);
                    InitializeCompositeSampleCache(_currentTest.PatientCompositeSamples);
                    InitializeCompositeSamplePropertyObservers(_currentTest);
                    _isAutomaticExclusionDisabledForThisTest = false;
                    InitializeSamplePropertyObservers(_currentTest);
                    UpdateMeasuredAndIdealVolumes(_currentTest);

                    FirePropertyChanged("CurrentTest");
                    FirePropertyChanged("MaxYValue");
                    FirePropertyChanged("MinYValue");
                    FirePropertyChanged("YAxisLabelStep");

                    RefreshBloodVolumeAlert();
                    RefreshTransudationAlerts();
                    RefreshStandardDeviationAlert();
                }
            }
        }

        public BVAVolume MeasuredVolume
        {
            get { return _measuredVolume; }
            set
            {
                if (_measuredVolume == value)
                    return;

                if (_measuredVolume != null)
                    _measuredVolume.PropertyChanged -= OnMeasuredVolumeChanged;

                _measuredVolume = value;

                if (_measuredVolume != null)
                {
                    _measuredVolume.PropertyChanged += OnMeasuredVolumeChanged;
                    FirePropertyChanged("TimeZeroBloodVolume");
                    FirePropertyChanged("StdDev");
                    FirePropertyChanged("TransudationRate");

                    VolumesMatrix[BloodType.Measured].PlasmaCount = _measuredVolume.PlasmaCount;
                    VolumesMatrix[BloodType.Measured].RedCellCount = _measuredVolume.RedCellCount;
                }

                FirePropertyChanged("MeasuredVolume");
            }
        }
        public BVAVolume IdealVolume
        {
            get { return _idealVolume; }
            set
            {
                if (_idealVolume == value)
                    return;

                if (_idealVolume != null)
                    _idealVolume.PropertyChanged -= OnIdealVolumeChanged;

                _idealVolume = value;

                if (_idealVolume != null)
                {
                    _idealVolume.PropertyChanged += OnIdealVolumeChanged;

                    VolumesMatrix[BloodType.Ideal].PlasmaCount = _idealVolume.PlasmaCount;
                    VolumesMatrix[BloodType.Ideal].RedCellCount = _idealVolume.RedCellCount;
                }

                FirePropertyChanged("IdealVolume");
            }
        }

        public string TestId2Label
        {
            get
            {
                return _settingsManager.GetSetting<string>(SettingKeys.BvaTestId2Label);
            }

            // ReSharper disable ValueParameterNotUsed (value isn't actually stored here)
            set
            // ReSharper restore ValueParameterNotUsed
            {
                FirePropertyChanged("TestId2Label");
            }
        }

        /// <summary>
        /// Upper and Lower limits based on Math.Abs(max_allowed_standard_dev)
        /// </summary>

        public ObservableCollection<TabItemViewModel> TestResultsTabItems
        {
            get { return _testResultsTabItems; }
        }
        public AddRangeObservableCollection<UnadjustedCompositeBloodPointViewModel> UnadjustedBloodVolumePoints
        {
            get { return _ubvPoints; }
        }
        public AddRangeObservableCollection<BestFitLinePoint> BestFitLinePoints
        {
            get { return _bestFitLinePoints; }
        }
        public IDictionary<BloodType, WholeBloodViewModel> WholeBloodVolumes
        {
            get { return _wholeBloodVolumes; }
        }
        public MatrixVolumesViewModel MatrixVolumes
        {
            get { return _matrixVolumesViewModel; }
        }

        public bool IsTestAnalysisActive
        {
            get { return _isTestAnalysisActive; }
            set { SetValue(ref _isTestAnalysisActive, "IsTestAnalysisActive", value); }
        }

        public int MaxXValue
        {
            get
            {
                try
                {
                    var lPoint = (from p in UnadjustedBloodVolumePoints.AsParallel()
                                  where Math.Abs(p.PostInjectionTimeInMinutes - UnadjustedBloodVolumePoints.Max(point => point.PostInjectionTimeInMinutes)) < 1E-300
                                  select p.PostInjectionTimeInMinutes).FirstOrDefault();

                    var maxXValue = MaxXValIncrement;
                    maxXValue += lPoint;

                    return (int)maxXValue;
                }
                catch (AggregateException ex)
                {
                    LogExceptionMessage("MaxXValue.get", "MaxXValue has no data: " + ex.Message);
                    throw;
                }
            }
        }

        public double MaxYValue
        {
            get
            {
                try
                {
                    // ReSharper disable CompareOfFloatsByEqualityOperator
                    var highYBoundPoint = (from p in UnadjustedBloodVolumePoints.AsParallel()
                                           where p.ObservableCompositePatientSample.UnadjustedBloodVolume == UnadjustedBloodVolumePoints.Max(point => point.ObservableCompositePatientSample.UnadjustedBloodVolume)
                                           select p.ObservableCompositePatientSample.UnadjustedBloodVolume).FirstOrDefault();
                    // ReSharper restore CompareOfFloatsByEqualityOperator

                    if (CurrentTest != null)
                    {
                        AutoRangeOn = false;
                        double transudationRate = CurrentTest.TransudationRateResult;
                        double wholeCount = CurrentTest.Volumes[BloodType.Measured].WholeCount;
                        var yValue = Math.Max(highYBoundPoint, (Math.Exp((transudationRate * (MaxXValue - 5)) + Math.Log(wholeCount))));
                        return Math.Log(yValue + 200);
                    }

                    return 0;
                }
                catch (AggregateException ex)
                {
                    LogExceptionMessage("MaxYValue.get", "highYBoundPoint has no data: " + ex.Message);
                    throw;
                }
            }
        }

        public double MinYValue
        {
            get
            {
                try
                {
                    // ReSharper disable CompareOfFloatsByEqualityOperator
                    var lowYBoundPoint = (from p in UnadjustedBloodVolumePoints.AsParallel()
                                          where p.ObservableCompositePatientSample.UnadjustedBloodVolume == UnadjustedBloodVolumePoints.Min(point => point.ObservableCompositePatientSample.UnadjustedBloodVolume)
                                          select p.ObservableCompositePatientSample.UnadjustedBloodVolume).FirstOrDefault();
                    // ReSharper restore CompareOfFloatsByEqualityOperator
                    if (CurrentTest != null)
                    {
                        double wholeCount = CurrentTest.Volumes[BloodType.Measured].WholeCount;
                        var yValue = Math.Min(lowYBoundPoint - 400, (wholeCount - 400));
                        return Math.Log(yValue);
                    }

                    return 0;
                }
                catch (AggregateException ex)
                {
                    LogExceptionMessage("MinYValue", "lowYBoundPoint has no data: " + ex.Message);
                    throw;
                }
            }
        }

        public bool AutoRangeOn
        {
            get 
            {
                return _autoRangeOn;
            }
            set
            {
                _autoRangeOn = value;
                FirePropertyChanged("AutoRangeOn");
            }
        }

        public double YAxisLabelStep
        {
            get
            {
                var min = MinYValue;
                var max = MaxYValue;
                var step = (max - min) / 5;
                return step;
            }
        }
        public string TimeZeroBloodVolume
        {
            get
            {
                double bvResult = 0f;
                if (CurrentTest != null)
                    bvResult = CurrentTest.Volumes[BloodType.Measured].WholeCount;

                if (Math.Abs(bvResult - 0) < 1E-300)
                    return String.Empty;
                if (bvResult > BvaDomainConstants.MaximumBloodVolumeInmL)
                    return MaximumBloodVolume;
                if (bvResult < BvaDomainConstants.MinimumBloodVolumeInmL)
                    return MinimumBloodVolume;
                return bvResult.ToString(CultureInfo.InvariantCulture) + " " + BloodVolumeUnits;

            }
        }
        public string StdDev
        {
            get
            {
                double result = 0f;
                if (CurrentTest != null)
                    result = CurrentTest.StandardDevResult;

                if (Double.IsNaN(result) || Double.IsInfinity(result))
                {
                    result = 0;
                }

                return Math.Round(100 * result, 3).ToString(CultureInfo.InvariantCulture) + "%";
            }
        }
        public string TransudationRate
        {
            get
            {
                double result = 0f;
                if (CurrentTest != null)
                    result = CurrentTest.TransudationRateResult;

                if (Double.IsNaN(result) || Double.IsInfinity(result))
                {
                    result = 0;
                }
                result *= 100;
                return ((Math.Round(result, 2).ToString(CultureInfo.InvariantCulture)) + "%/min");
            }
        }

        public string SampleModeDescription
        {
            get { return _sModeDescription; }
            set { SetValue(ref _sModeDescription, "SampleModeDescription", value); }
        }
        public string SampleName
        {
            get { return _sampleName; }
            set { SetValue(ref _sampleName, "SampleName", value); }
        }
        public string SampleProgressDescription
        {
            get { return _sampleProgress; }
            set { SetValue(ref _sampleProgress, "SampleProgressDescription", value); }
        }
        public int SampleChangerPosition
        {
            get { return _scPosition; }
            set { SetValue(ref _scPosition, "SampleChangerPosition", value); }
        }

        public string MatrixMeasuredBloodVolume
        {
            get
            {
                var toReturn = ValueNotDisplayed;
                if ((MatrixVolumes != null) && (MatrixVolumes[BloodType.Measured] != null))
                {
                    var volume = MatrixVolumes[BloodType.Measured].WholeCount;

                    if (volume > BvaDomainConstants.MaximumBloodVolumeInmL)
                        toReturn = MaximumBloodVolumeWithoutUnits;
                    else if (volume < BvaDomainConstants.MinimumBloodVolumeInmL)
                        toReturn = MinimumBloodVolumeWithoutUnits;
                    else
                        toReturn = volume.ToString(CultureInfo.InvariantCulture);
                }

                return toReturn;
            }
        }
        public string MatrixDeviationMillilitersBloodVolume
        {
            get
            {
                string toReturn = ValueNotDisplayed;
                if ((MatrixVolumes != null) && (MatrixVolumes[BloodType.Measured] != null))
                {
                    int volume = MatrixVolumes[BloodType.Measured].WholeCount;

                    if (volume > BvaDomainConstants.MaximumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else if (volume < BvaDomainConstants.MinimumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else
                    {
                        var measuredVolume = MatrixVolumes[VolumeType.WholeBlood].MeasuredVolume;
                        var numericDeviation = MatrixVolumes[VolumeType.WholeBlood].NumericDeviation;
                        var idealVolume = MatrixVolumes[VolumeType.WholeBlood].IdealVolume;
                        if (measuredVolume <= 0 || idealVolume <= 0)
                            toReturn = ValueNotDisplayed;
                        else
                            toReturn = numericDeviation.ToString(CultureInfo.InvariantCulture);
                    }
                }

                return toReturn;
            }
        }
        public string MatrixDeviationPercentageBloodVolume
        {
            get
            {
                var toReturn = ValueNotDisplayed;
                if ((MatrixVolumes != null) && (MatrixVolumes[BloodType.Measured] != null))
                {
                    var volume = MatrixVolumes[BloodType.Measured].WholeCount;

                    if (volume > BvaDomainConstants.MaximumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else if (volume < BvaDomainConstants.MinimumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else
                    {
                        var measuredVolume = MatrixVolumes[VolumeType.WholeBlood].MeasuredVolume;
                        var percentDeviation = MatrixVolumes[VolumeType.WholeBlood].PercentDeviation;
                        var idealVolume = MatrixVolumes[VolumeType.WholeBlood].IdealVolume;
                        if (measuredVolume <= 0 || idealVolume <= 0)
                            toReturn = ValueNotDisplayed;
                        else
                            toReturn = Math.Round(percentDeviation, 1, MidpointRounding.AwayFromZero).ToString("#0.0") + "%";
                    }
                }

                return toReturn;
            }
        }

        public string MatrixMeasuredPlasmaVolume
        {
            get
            {
                var toReturn = ValueNotDisplayed;
                if ((MatrixVolumes != null) && (MatrixVolumes[BloodType.Measured] != null))
                {
                    var volume = MatrixVolumes[BloodType.Measured].WholeCount;

                    if (volume > BvaDomainConstants.MaximumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else if (volume < BvaDomainConstants.MinimumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else
                    {
                        int plasmaCount = MatrixVolumes[BloodType.Measured].PlasmaCount;
                        if (Double.IsNaN(plasmaCount) || Double.IsInfinity(plasmaCount) || plasmaCount <= 0)
                            toReturn = ValueNotDisplayed;
                        else
                            toReturn = plasmaCount.ToString(CultureInfo.InvariantCulture);
                    }
                }

                return toReturn;
            }
        }
        public string MatrixDeviationMillilitersPlasmaVolume
        {
            get
            {
                var toReturn = ValueNotDisplayed;
                if ((MatrixVolumes != null) && (MatrixVolumes[BloodType.Measured] != null))
                {
                    var volume = MatrixVolumes[BloodType.Measured].WholeCount;

                    if (volume > BvaDomainConstants.MaximumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else if (volume < BvaDomainConstants.MinimumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else
                    {
                        var measuredVolume = MatrixVolumes[VolumeType.Plasma].MeasuredVolume;
                        var numericDeviation = MatrixVolumes[VolumeType.Plasma].NumericDeviation;
                        var idealVolume = MatrixVolumes[VolumeType.Plasma].IdealVolume;
                        if (measuredVolume <= 0 || idealVolume <= 0)
                            toReturn = ValueNotDisplayed;
                        else
                            toReturn = numericDeviation.ToString(CultureInfo.InvariantCulture);
                    }
                }

                return toReturn;
            }
        }
        public string MatrixDeviationPercentagePlasmaVolume
        {
            get
            {
                string toReturn = ValueNotDisplayed;
                if ((MatrixVolumes != null) && (MatrixVolumes[BloodType.Measured] != null))
                {
                    var volume = MatrixVolumes[BloodType.Measured].WholeCount;

                    if (volume > BvaDomainConstants.MaximumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else if (volume < BvaDomainConstants.MinimumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else
                    {
                        var measuredVolume = MatrixVolumes[VolumeType.Plasma].MeasuredVolume;
                        var percentDeviation = MatrixVolumes[VolumeType.Plasma].PercentDeviation;
                        var idealVolume = MatrixVolumes[VolumeType.Plasma].IdealVolume;
                        if (measuredVolume <= 0 || idealVolume <= 0)
                            toReturn = ValueNotDisplayed;
                        else
                            toReturn = Math.Round(percentDeviation, 1, MidpointRounding.AwayFromZero).ToString("#0.0") + "%";
                    }
                }

                return toReturn;
            }
        }

        public string MatrixMeasuredRedCellVolume
        {
            get
            {
                var toReturn = ValueNotDisplayed;
                if ((MatrixVolumes != null) && (MatrixVolumes[BloodType.Measured] != null))
                {
                    var volume = MatrixVolumes[BloodType.Measured].WholeCount;

                    if (volume > BvaDomainConstants.MaximumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else if (volume < BvaDomainConstants.MinimumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else
                    {
                        var redCellCount = MatrixVolumes[BloodType.Measured].RedCellCount;
                        if (Double.IsNaN(redCellCount) || Double.IsInfinity(redCellCount) || redCellCount <= 0)
                            toReturn = ValueNotDisplayed;
                        else
                            toReturn = redCellCount.ToString(CultureInfo.InvariantCulture);
                    }
                }

                return toReturn;
            }
        }
        public string MatrixDeviationMillilitersRedCellVolume
        {
            get
            {
                var toReturn = ValueNotDisplayed;
                if ((MatrixVolumes != null) && (MatrixVolumes[BloodType.Measured] != null))
                {
                    var volume = MatrixVolumes[BloodType.Measured].WholeCount;

                    if (volume > BvaDomainConstants.MaximumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else if (volume < BvaDomainConstants.MinimumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else
                    {
                        var measuredVolume = MatrixVolumes[VolumeType.RedCell].MeasuredVolume;
                        var numericDeviation = MatrixVolumes[VolumeType.RedCell].NumericDeviation;
                        var idealVolume = MatrixVolumes[VolumeType.RedCell].IdealVolume;
                        if (measuredVolume <= 0 || idealVolume <= 0)
                            toReturn = ValueNotDisplayed;
                        else
                            toReturn = numericDeviation.ToString(CultureInfo.InvariantCulture);
                    }
                }

                return toReturn;
            }
        }
        public string MatrixDeviationPercentageRedCellVolume
        {
            get
            {
                string toReturn = ValueNotDisplayed;
                if ((MatrixVolumes != null) && (MatrixVolumes[BloodType.Measured] != null))
                {
                    int volume = MatrixVolumes[BloodType.Measured].WholeCount;

                    if (volume > BvaDomainConstants.MaximumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else if (volume < BvaDomainConstants.MinimumBloodVolumeInmL)
                        toReturn = ValueNotDisplayed;
                    else
                    {
                        int measuredVolume = MatrixVolumes[VolumeType.RedCell].MeasuredVolume;
                        double percentDeviation = MatrixVolumes[VolumeType.RedCell].PercentDeviation;
                        int idealVolume = MatrixVolumes[VolumeType.RedCell].IdealVolume;
                        if (measuredVolume <= 0 || idealVolume <= 0)
                            toReturn = ValueNotDisplayed;
                        else
                            toReturn = Math.Round(percentDeviation, 1, MidpointRounding.AwayFromZero).ToString("#0.0") + "%";
                    }
                }

                return toReturn;
            }
        }

        /// <summary>
        /// Determines whether or not the total blood volume is outside the normal range.
        /// </summary>
        public bool IsTotalBloodVolumeOutOfRange
        {
            get
            {
                if ((CurrentTest == null) || (CurrentTest.Volumes == null))
                    return false;

                const string propertyName = "WholeCount";
                // ReSharper disable UnusedVariable
                var tmp = CurrentTest.Volumes[BloodType.Measured][propertyName]; // Force the rule to evaluate
                // ReSharper restore UnusedVariable
                if (CurrentTest.Volumes[BloodType.Measured].BrokenRules.Count(r => r.PropertyName == propertyName) > 0)
                    return true;

                return false;
            }
        }

        /// <summary>
        /// Determines whether or not the standard deviation is outside the normal range.
        /// </summary>
        public bool IsStdDevOutOfRange
        {
            get
            {
                if (CurrentTest == null)
                    return false;

                const string propertyName = "StandardDevResult";
                // ReSharper disable UnusedVariable
                var tmp = CurrentTest[propertyName]; // Force the rule to evaluate
                // ReSharper restore UnusedVariable

                if (CurrentTest.BrokenRules.Count(r => r.PropertyName == propertyName) > 0)
                    return true;

                return false;
            }
        }

        /// <summary>
        /// Determines whether or not the transudation rate is negative.
        /// </summary>
        public bool IsTransudationRateNegative
        {
            get
            {
                if (CurrentTest == null)
                    return false;

                const string propertyName = "TransudationRateResult";
                // ReSharper disable UnusedVariable
                var tmp = CurrentTest[propertyName]; // Force the rule to evaluate 
                // ReSharper restore UnusedVariable

                if (CurrentTest.BrokenRules.Count(r => r.PropertyName == propertyName && r.RuleId == MessageKeys.BvaTransudationReverse) > 0)
                    return true;

                return false;
            }
        }

        #endregion

        #region Commands

        #region Blood volume alert

        /// <summary>
        /// Gets or sets the command associated with the blood volume alert button
        /// </summary>
        public DelegateCommand<object> TotalBloodVolumeAlertCommand
        {
            get;
            private set;
        }

        /// <summary>
        /// Determines whether or not the blood volume alert button command can
        /// be executed
        /// </summary>
        /// <param name="arg">(Ignored)</param>
        /// <returns>True if the total blood volume is out of range</returns>
        private bool CanExecuteTotalBloodVolumeAlert(object arg)
        {
            return IsTotalBloodVolumeOutOfRange;
        }

        /// <summary>
        /// Displays a message box with information about the total blood volume alert
        /// </summary>
        /// <param name="arg">(Ignored)</param>
        private void ExecuteTotalBloodVolumeAlert(object arg)
        {
            const string propertyName = "WholeCount";

            // Force the rule to evaluate 
            // ReSharper disable UnusedVariable
            var tmp = CurrentTest.Volumes[BloodType.Measured][propertyName];
            // ReSharper restore UnusedVariable

            string cause = "Total Blood Volume: " + CurrentTest.Volumes[BloodType.Measured].WholeCount.ToString(CultureInfo.InvariantCulture) + " " + BloodVolumeUnits;

            _resultAlertService.ShowAlertMessageBox(CurrentTest.Volumes[BloodType.Measured], propertyName, cause);
        }

        #endregion

        #region Transudation rate alert

        /// <summary>
        /// Gets or sets the command associated with the transudation rate alert button
        /// </summary>
        public DelegateCommand<object> TransudationRateAlertCommand
        {
            get;
            private set;
        }

        /// <summary>
        /// Determines whether or not the transudation rate alert button command 
        /// can be executed
        /// </summary>
        /// <param name="arg">(Ignored)</param>
        /// <returns>True if the transudation rate is high, unusually high, or negative</returns>
        private bool CanExecuteTransudationRateAlert(object arg)
        {
            return IsTransudationRateNegative;
        }

        /// <summary>
        /// Displays a message box with information about the transudation rate alert
        /// </summary>
        /// <param name="arg">(Ignored)</param>
        private void ExecuteTransudationRateAlert(object arg)
        {
            const string propertyName = "TransudationRateResult";

            // Force the rule to evaluate 
            // ReSharper disable UnusedVariable
            var tmp = CurrentTest[propertyName];
            // ReSharper restore UnusedVariable

            string cause = "Slope: " + TransudationRate.Remove(TransudationRate.IndexOf('%')) + "%/minute";

            _resultAlertService.ShowAlertMessageBox(CurrentTest, propertyName, cause);
        }

        #endregion

        #region Standard deviation alert

        /// <summary>
        /// Gets or sets the command associated with the standard deviation alert button
        /// </summary>
        public DelegateCommand<object> StandardDeviationAlertCommand
        {
            get;
            private set;
        }

        /// <summary>
        /// Determines whether or not the standard deviation alert button command 
        /// can be executed
        /// </summary>
        /// <param name="arg">(Ignored)</param>
        /// <returns>True if the standard deviation is too high</returns>
        private bool CanExecuteStandardDeviationAlert(object arg)
        {
            return IsStdDevOutOfRange;
        }

        /// <summary>
        /// Displays a message box with information about the transudation rate alert
        /// </summary>
        /// <param name="arg">(Ignored)</param>
        private void ExecuteStandardDeviationAlert(object arg)
        {
            const string propertyName = "StandardDevResult";

            // Force the rule to evaluate 
            // ReSharper disable UnusedVariable
            var tmp = CurrentTest[propertyName];
            // ReSharper restore UnusedVariable

            string cause = "Standard Deviation: " + StdDev;

            _resultAlertService.ShowAlertMessageBox(CurrentTest, propertyName, cause);
        }

        #endregion

        #region View report button

        public DelegateCommand<object> ViewReportCommand
        {
            get;
            private set;
        }

        private bool CanExecuteViewReport(object arg)
        {
            return !_isReportBeingViewed;
        }

        private void ExecuteViewReport(object chartUris)
        {
            Task.Factory.StartNew(stateObj =>
                {
                    _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Loading Blood Volume Test Report..."));
                    IdealDispatcher.BeginInvoke(() =>
                    {
                        LogInformationMessage("ExecuteViewReport", "View Report button was clicked");
                        _controller.DataService.SaveTest(CurrentTest);
                        ReportControllerBase reportController = new BvaQuickReportController(_controller.Logger, _messageBoxDispatcher, _regionManager,
                            _calcEngine, _controller.EventAggregator, CurrentTest, _folderBrowser, _messageManager, _opticalDriveWrapper, _settingsManager, 
							_auditTrailDataAccess, _zipFileFactory, _pdfPrinter);
                        reportController.ShowReport();
                    });
                    return true;
                }, _controller.SelectedTest).ContinueWith(answer => _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false)), TaskScheduler.FromCurrentSynchronizationContext());
        }

        #endregion

        #region Full report button

        public DelegateCommand<object> PrintReportCommand
        {
            private set;
            get;

        }
        private void ExecutePrintReport(object arg)
        {
             Task.Factory.StartNew(() =>
             {
                 _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Printing Report..."));
                 IdealDispatcher.BeginInvoke(() =>
                     {
                        LogInformationMessage("ExecutePrintReport", "Print Report button clicked");
                        _controller.DataService.SaveTest(CurrentTest);
                        ReportControllerBase reportController = new BvaReportController(
							_controller.Logger, _messageBoxDispatcher, _regionManager,
                            _calcEngine, _controller.EventAggregator, CurrentTest, _folderBrowser, _messageManager, _opticalDriveWrapper,
							_settingsManager, _auditTrailDataAccess, _zipFileFactory, _pdfPrinter);
                        reportController.PrintReport();
                     });
                 return true;
             }).ContinueWith(answer => _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false)), TaskScheduler.FromCurrentSynchronizationContext());

        }
        private bool CanPrintReport(object arg)
        {
            return true;
        }

        #endregion

		#region Quick report button

		public DelegateCommand<object> PrintQuickReportCommand
		{
			private set;
			get;

		}
		private void ExecutePrintQuickReport(object arg)
		{
			Task.Factory.StartNew(() =>
			{
				_eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Printing Report..."));
				IdealDispatcher.BeginInvoke(() =>
				{
					LogInformationMessage("ExecutePrintQuickReport", "Print Report button clicked");
					_controller.DataService.SaveTest(CurrentTest);
					ReportControllerBase reportController = new BvaQuickReportController(_controller.Logger, _messageBoxDispatcher, _regionManager,
						_calcEngine, _controller.EventAggregator, CurrentTest, _folderBrowser, _messageManager, _opticalDriveWrapper,
						_settingsManager, _auditTrailDataAccess, _zipFileFactory, _pdfPrinter);
					reportController.PrintReport();
				});
				return true;
			}).ContinueWith(answer => _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false)), TaskScheduler.FromCurrentSynchronizationContext());

		}
		private bool CanPrintQuickReport(object arg)
		{
			return true;
		}

		#endregion

        #region Export report button

        public DelegateCommand<object> ExportReportCommand
        {
            private set;
            get;
        }

        private void ExecuteExportReport(object arg)
        {
            LogInformationMessage("ExecuteExportReport", "Export Report button clicked");
           
            try
            {
                _controller.DataService.SaveTest(CurrentTest);
                ReportControllerBase reportController = new BvaQuickReportController(_controller.Logger, _messageBoxDispatcher, _regionManager,
					_calcEngine, _controller.EventAggregator, CurrentTest, _folderBrowser, _messageManager, _opticalDriveWrapper, _settingsManager, _auditTrailDataAccess, _zipFileFactory, _pdfPrinter);
                reportController.ExportReport();
            }
            catch (Exception ex)
            {
                _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error,
                         ex.Message, "Export Exception", MessageBoxChoiceSet.Close);
            }
           
        }
        private bool CanExportReport(object arg)
        {
            return true;
        }

        #endregion

        #region Abort test button

        public DelegateCommand<object> AbortTestCommand
        {
            private set;
            get;
        }
        private void ExecuteAbortTest(object arg)
        {
            LogInformationMessage("ExecuteAbortTest", "Abort button clicked");
            const string message = "Are you sure you want to abort this test?";

            var choices = new[] { "Abort", "Cancel" };
            int result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Question, message, "Abort Test", choices); 
            if (choices[result] == "Abort")
                _testExecutionService.Abort();
        }
        private bool CanAbortTest(object arg)
        {
            return _testExecutionService != null && _testExecutionService.IsExecuting &&
                _testExecutionService.GetExecutingTestType() == typeof(BVATest);
        }

        #endregion

        #region Zoom chart button

        public DelegateCommand<Object> ZoomChartCommand
        {
            private set;
            get;

        }
        private void ExecuteZoomChart(object arg)
        {
            _popupController.OpenPopup(BVAModuleViewKeys.BloodPointsPopup, this);
        }
        private bool CanZoomChart(object arg)
        {
            return true;
        }

        #endregion

        #endregion

        #region Helpers

        void ConfigureViewModel()
        {
            DisplayName = "Blood Volume Analysis";

            //Commands init
            AbortTestCommand = new DelegateCommand<object>(ExecuteAbortTest, CanAbortTest);
            ViewReportCommand = new DelegateCommand<object>(ExecuteViewReport, CanExecuteViewReport);
            PrintReportCommand = new DelegateCommand<object>(ExecutePrintReport, CanPrintReport);
			PrintQuickReportCommand = new DelegateCommand<object>(ExecutePrintQuickReport, CanPrintQuickReport);
            ExportReportCommand = new DelegateCommand<object>(ExecuteExportReport, CanExportReport);
            ZoomChartCommand = new DelegateCommand<object>(ExecuteZoomChart, CanZoomChart);

            // Alerts for computed results
            TotalBloodVolumeAlertCommand = new DelegateCommand<object>(ExecuteTotalBloodVolumeAlert, CanExecuteTotalBloodVolumeAlert);
            TransudationRateAlertCommand = new DelegateCommand<object>(ExecuteTransudationRateAlert, CanExecuteTransudationRateAlert);
            StandardDeviationAlertCommand = new DelegateCommand<object>(ExecuteStandardDeviationAlert, CanExecuteStandardDeviationAlert);

            //Add tabs to the Analysis View
            _testResultsTabItems.Add(new TabItemViewModel(Guid.NewGuid()) { Header = "Test Analysis" });
            _testResultsTabItems.Add(new TabItemViewModel(Guid.NewGuid()) { Header = "Notes" });

            _eventAggregator.GetEvent<ViewReportExecuteChanged>().Subscribe(OnViewReportExecuteChanged);

            SettingObserver.RegisterHandler(SettingKeys.BvaTestId2Label, s => TestId2Label = s.GetSetting<string>(SettingKeys.BvaTestId2Label));
        }

        protected virtual void UpdateMeasuredAndIdealVolumes(BVATest test)
        {
            if (test != null)
            {
                MeasuredVolume = test.Volumes[BloodType.Measured];
                IdealVolume = test.Volumes[BloodType.Ideal];
                VolumesMatrix[BloodType.Measured].PlasmaCount = MeasuredVolume.PlasmaCount;
                VolumesMatrix[BloodType.Measured].RedCellCount = MeasuredVolume.RedCellCount;
            }
            else
            {
                MeasuredVolume = null;
                IdealVolume = null;
            }
        }

        protected virtual void InitializeTestPropertyObserver(BVATest test)
        {
            _testObserver = new PropertyObserver<BVATest>(test);

            _testObserver.RegisterHandler(t => t.TransudationRateResult,
                t =>
                {
                    FirePropertyChanged("TransudationRate");
                    RefreshTransudationAlerts();
                });
            _testObserver.RegisterHandler(t => t.StandardDevResult,
                t =>
                {
                    FirePropertyChanged("StdDev");
                    RefreshStandardDeviationAlert();
                });
        }

        /// <summary>
        /// This method is needed because listening to PropertyChanged on the composite samples doesn't
        /// work the *first* time the values are loaded. Another view model raises PropertyChanged on them
        /// to force rule validation, not because the value actually changed. Also, this method does not
        /// initialize the cache with the actual references; it creates "clones" that have the same GUID,
        /// hematocrit, and post-injection times.
        /// </summary>
        /// <param name="compositeSamples">Composite samples for the current test</param>
        private void InitializeCompositeSampleCache(IEnumerable<BVACompositeSample> compositeSamples)
        {
            _cachedCompositeSamples = new List<BVACompositeSample>();
            foreach (var compositeSample in compositeSamples)
            {
                var sampleAClone = new BVASample(compositeSample.SampleA.InternalId, compositeSample.SampleA.Position, null)
                    {
                        Range = compositeSample.SampleA.Range,
                        Type = compositeSample.SampleA.Type,
                        PostInjectionTimeInSeconds = compositeSample.SampleA.PostInjectionTimeInSeconds,
                        Hematocrit = compositeSample.SampleA.Hematocrit,
                        Counts = compositeSample.SampleA.Counts
                    };
                var sampleBClone = new BVASample(compositeSample.SampleB.InternalId, compositeSample.SampleB.Position, null)
                    {
                        Range = compositeSample.SampleB.Range,
                        Type = compositeSample.SampleB.Type,
                        PostInjectionTimeInSeconds = compositeSample.SampleB.PostInjectionTimeInSeconds,
                        Hematocrit = compositeSample.SampleB.Hematocrit,
                        Counts = compositeSample.SampleB.Counts
                    };
                var compositeSampleClone = new BVACompositeSample(compositeSample.Test, sampleAClone, sampleBClone, null);
                _cachedCompositeSamples.Add(compositeSampleClone);
            }
        }

        protected virtual void InitializeCompositeSamplePropertyObservers(BVATest test)
        {
            _compositeSampleObservers = new List<PropertyObserver<BVACompositeSample>>();
            foreach (var patientCompositeSample in test.PatientCompositeSamples)
            {
                var compositeSampleObserver = new PropertyObserver<BVACompositeSample>(patientCompositeSample);
                var localCompositeSample = patientCompositeSample;
                compositeSampleObserver.RegisterHandler(s => s.HematocritA, 
                    s => 
                    {
                        var cachedSample = (from c in _cachedCompositeSamples where c.SampleA.InternalId == localCompositeSample.SampleA.InternalId select c).FirstOrDefault();
                        if (cachedSample == null)
                        {
                            LogWarningMessage("InitializeCompositeSamplePropertyObservers", "Cannot find " + localCompositeSample.SampleType + " in the cache.");
                            return;
                        }
                        if (Math.Abs(cachedSample.HematocritA - localCompositeSample.HematocritA) <= 1E-100)
                            return;
                        cachedSample.SampleA.Hematocrit = s.HematocritA;
                        LogInformationMessage("InitializeCompositeSamplePropertyObservers", "Hct A for composite patient sample " + localCompositeSample.SampleType + " has changed");
                        ApplyAutomaticPointExclusion();
                    });
                compositeSampleObserver.RegisterHandler(s => s.HematocritB, 
                    s =>
                    {
                        var cachedSample = (from c in _cachedCompositeSamples where c.SampleB.InternalId == localCompositeSample.SampleB.InternalId select c).FirstOrDefault();
                        if (cachedSample == null)
                        {
                            LogWarningMessage("InitializeCompositeSamplePropertyObservers", "Cannot find " + localCompositeSample.SampleType + " in the cache.");
                            return;
                        }
                        if (Math.Abs(cachedSample.HematocritB - localCompositeSample.HematocritB) <= 1E-100)
                            return;
                        cachedSample.SampleB.Hematocrit = s.HematocritB;
                        LogInformationMessage("InitializeCompositeSamplePropertyObservers", "Hct B for composite patient sample " + localCompositeSample.SampleType + " has changed");
                        ApplyAutomaticPointExclusion();
                    });
                compositeSampleObserver.RegisterHandler(s => s.PostInjectionTimeInSeconds, 
                    s =>
                    {
                        var cachedSample = (from c in _cachedCompositeSamples where c.SampleA.InternalId == localCompositeSample.SampleA.InternalId select c).FirstOrDefault();
                        if (cachedSample == null)
                        {
                            LogWarningMessage("InitializeCompositeSamplePropertyObservers", "Cannot find " + localCompositeSample.SampleType + " in the cache.");
                            return;
                        }
                        if (cachedSample.PostInjectionTimeInSeconds == localCompositeSample.PostInjectionTimeInSeconds)
                            return;
                        cachedSample.SampleA.PostInjectionTimeInSeconds = s.PostInjectionTimeInSeconds;
                        LogInformationMessage("InitializeCompositeSamplePropertyObservers", "Post-injection time for composite patient sample " + localCompositeSample.SampleType + " has changed");
                        ApplyAutomaticPointExclusion();
                    });
                compositeSampleObserver.RegisterHandler(s => s.CountsA, 
                    s =>
                    {
                        var cachedSample = (from c in _cachedCompositeSamples where c.SampleA.InternalId == localCompositeSample.SampleA.InternalId select c).FirstOrDefault();
                        if (cachedSample == null)
                        {
                            LogWarningMessage("InitializeCompositeSamplePropertyObservers", "Cannot find " + localCompositeSample.SampleType + " in the cache.");
                            return;
                        }
                        if (cachedSample.CountsA == localCompositeSample.CountsA)
                            return;
                        cachedSample.SampleA.Counts = s.CountsA;
                        LogInformationMessage("InitializeCompositeSamplePropertyObservers", "A-side counts for composite patient sample " + localCompositeSample.SampleType + " has changed");
                        ApplyAutomaticPointExclusion();
                    });
                compositeSampleObserver.RegisterHandler(s => s.CountsB, 
                    s =>
                    {
                        var cachedSample = (from c in _cachedCompositeSamples where c.SampleA.InternalId == localCompositeSample.SampleA.InternalId select c).FirstOrDefault();
                        if (cachedSample == null)
                        {
                            LogWarningMessage("InitializeCompositeSamplePropertyObservers", "Cannot find " + localCompositeSample.SampleType + " in the cache.");
                            return;
                        }
                        if (cachedSample.CountsB == localCompositeSample.CountsB)
                            return;
                        cachedSample.SampleA.Counts = s.CountsB;
                        LogInformationMessage("InitializeCompositeSamplePropertyObservers", "B-side counts for composite patient sample " + localCompositeSample.SampleType + " has changed");
                        ApplyAutomaticPointExclusion();
                    });

                _compositeSampleObservers.Add(compositeSampleObserver);
            }
        }

        protected virtual void InitializeSamplePropertyObservers(BVATest test)
        {
            _sampleObservers = new List<PropertyObserver<BVASample>>();
            foreach (var patientSample in test.PatientSamples)
            {
                var sampleObserver = new PropertyObserver<BVASample>(patientSample);
                var localSample = patientSample;
                sampleObserver.RegisterHandler(s => s.IsCountExcluded,
                    s =>
                    {
                        if (!s.IsCountAutoExcluded || s.IsCountExcluded) return;
                        LogInformationMessage("InitializeSamplePropertyObservers", "Unexcluding sample " + localSample.FullName + " that was automatically excluded");
                        
                        if (!_isAutomaticExclusionDisabledForThisTest)
                        {
                            var choices = new[] { "Override Exclusion", "Keep Exclusion" };
                            var result =  _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Question,
                                                                              AutomaticPointExclusionNoFurtherEvaluationMessage,
                                                                              AutomaticPointExclusionCaption,
                                                                              choices);
                            if (choices[result] == "Keep Exclusion")
                            {
                                s.IsCountExcluded = true;
                                LogInformationMessage("InitializeSamplePropertyObservers", "User chose NOT to undo automatic point exclusion for " + localSample.FullName);
                            }
                            else
                            {
                                _isAutomaticExclusionDisabledForThisTest = true;
                                LogInformationMessage("InitializeSamplePropertyObservers", "User chose to undo automatic point exclusion for " + localSample.FullName);
                            }
                        }
                        else
                        {
                            LogInformationMessage("InitializeSamplePropertyObservers", "The user already acknowledged that automatic point exclusion is disabled for this test");
                        }
                    });
                _sampleObservers.Add(sampleObserver);
            }
        }

        void OnMeasuredVolumeChanged(object sender, PropertyChangedEventArgs args)
        {
            if (VolumesMatrix[BloodType.Measured].WholeCount == MeasuredVolume.WholeCount)
                return;

            LogInformationMessage("OnMeasuredVolumeChanged", "The measured blood volume for the current test ('" + CurrentTest.InternalId + "') has changed.");
            ApplyAutomaticPointExclusion();

            UpdateUnadjustedBloodPointViewModels();
            RefreshBestFitLinePoints();

            VolumesMatrix[BloodType.Measured].PlasmaCount = MeasuredVolume.PlasmaCount;
            VolumesMatrix[BloodType.Measured].RedCellCount = MeasuredVolume.RedCellCount;

            FirePropertyChanged("TimeZeroBloodVolume");
            if (CurrentTest.Status == TestStatus.Completed)
            {
                FirePropertyChanged("MaxYValue");
                FirePropertyChanged("MinYValue");
                FirePropertyChanged("YAxisLabelStep");
            }
            RefreshBloodVolumeAlert();
        }
        void OnIdealVolumeChanged(object sender, PropertyChangedEventArgs args)
        {
            // Ideal volumes have changed, so update the volumes matrix property (which the subview binds to)
            // so that the correct values come through.
            VolumesMatrix[BloodType.Ideal].PlasmaCount = IdealVolume.PlasmaCount;
            VolumesMatrix[BloodType.Ideal].RedCellCount = IdealVolume.RedCellCount;

            // The other parts of that subview also depend on these properties having updated values as well.
            FirePropertyChanged("MatrixDeviationMillilitersBloodVolume", "MatrixDeviationMillilitersPlasmaVolume", 
                "MatrixDeviationMillilitersRedCellVolume", "MatrixDeviationPercentageBloodVolume", 
                "MatrixDeviationPercentagePlasmaVolume", "MatrixDeviationPercentageRedCellVolume");
        }

        void UpdateUnadjustedBloodPointViewModels()
        {
            if (CurrentTest != null)
            {
                var pCompositeSamples = (from s in CurrentTest.PatientCompositeSamples
                                         orderby s.SampleType
                                         select s).ToList();

                //Using custom AddRange which fires OnCollectionChanged at the end of all adds.
                UnadjustedBloodVolumePoints.ClearAndAddRange(
                    pCompositeSamples.
                    Where(p => p.PostInjectionTimeInSeconds > 0 && p.UnadjustedBloodVolume > 0)
                    .Select((point, index) => new UnadjustedCompositeBloodPointViewModel(point, (point.SampleType - SampleType.Sample1 + 1).ToString(CultureInfo.InvariantCulture))));

                FirePropertyChanged("MaxXValue");
            }
        }
        protected void SubscribeToBvaTestSelectedEvent()
        {
            _eventAggregator.GetEvent<BvaTestSelected>().Subscribe(BVATestSelectedEventHandler, ThreadOption.PublisherThread, false);
        }

        void SubscribeToAggregateEvents()
        {
            SubscribeToBvaTestSelectedEvent();

            //Subscribe to AppPartActivationChanged event
            _eventAggregator.GetEvent<AppPartActivationChanged>().Subscribe(
                OnTestSampleValuesActivationChanged,
                ThreadOption.UIThread,
                false,
                m => m.AppPartName == BVAModuleViewKeys.TestSampleValues);

            _eventAggregator.GetEvent<TestStarted>().Subscribe(OnTestStarted, ThreadOption.UIThread, false, t => t is BVATest);
            _eventAggregator.GetEvent<TestAborted>().Subscribe(OnTestAborted, ThreadOption.UIThread, false, t => t is BVATest);
            _eventAggregator.GetEvent<TestCompleted>().Subscribe(OnTestFinished, ThreadOption.UIThread, false, t => t is BVATest);
            _eventAggregator.GetEvent<SampleChangerPositionChangeCompleted>().Subscribe(OnSampleChangerPositionChangeEnd, ThreadOption.UIThread);
        }

        /// <summary>
        /// Applies automatic point exclusion to the current test (if possible).
        /// </summary>
        /// <param name="isCalledByDelayThread">True if the caller is the delay/semaphore thread; 
        /// false (by default for the public API) otherwise</param>
        public void ApplyAutomaticPointExclusion(bool isCalledByDelayThread = false)
        {
            // Only deal with delaying if not being called after the delay's execution and if 
            // we're not already in the middle of excluding points.
            if (!isCalledByDelayThread && _automaticPointExclusionFinished)
                HandleSemaphoreForAutomaticPointExclusion();

            if (!CanAutomaticPointExclusionBeApplied()) return;

            // Manual tests are inherently "completed", but for the audit trail to pick up
            // on "is count auto-excluded" changes for the audit trail, the test must be
            // saved *before* point exclusion.
            if (CurrentTest.Mode == TestMode.Manual)
                _controller.DataService.SaveTest(CurrentTest);

            if (TryToResetAutomaticallyExcludedPoints())
                ExecuteAutomaticPointExclusionServiceAsTask();
        }

        /// <summary>
        /// Executes automatic point exclusion as a (parallel) task. Once the task is complete,
        /// the busy indicator is removed.
        /// </summary>
        private void ExecuteAutomaticPointExclusionServiceAsTask()
        {
            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Applying automatic point exclusion..."));
            _automaticPointExclusionFinished = false;

            Task.Factory.StartNew(() =>
                {
                    AutomaticPointExclusionMetadata serviceMetadata;
                    try
                    {
                        serviceMetadata = AutomaticPointExclusionService.ExcludePoints(CurrentTest);
                    }
                    catch (Exception ex)
                    {
                        LogExceptionMessage("ExecuteAutomaticPointExclusionServiceAsTask", "An exception occurred when using the automatic point exclusion service: " + ex.Message);
                        IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, AutomaticPointExclusionErrorMessage,
                                                                 AutomaticPointExclusionCaption, MessageBoxChoiceSet.Close));
                        return;
                    }
                    if (serviceMetadata == null)
                        LogExceptionMessage("ExecuteAutomaticPointExclusionServiceAsTask", "Null metadata was return by the automatic point exclusion service");
                    else
                    {
                        LogInformationMessage("ExecuteAutomaticPointExclusionServiceAsTask",
                                              "Automatic point exclusion metadata: " + serviceMetadata);
                        if (serviceMetadata.ReasonServiceNotRunToCompletion.Contains("exception"))
                            IdealDispatcher.BeginInvoke(
                                () => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error,
                                                                           AutomaticPointExclusionErrorMessage,
                                                                           AutomaticPointExclusionCaption,
                                                                           MessageBoxChoiceSet.Close));
                        InformUserAboutPointExclusionResultsIfRequired(serviceMetadata);
                    }
                }).ContinueWith(answer =>
                    {
                        _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
                        _automaticPointExclusionFinished = true;
                    });
        }
        
        /// <summary>
        /// Displays a message box to the user in scenarios where automatic point exclusion 
        /// encountered an exceptional case. Special cases include: not being able to lower
        /// the standard deviation below the threshold.
        /// </summary>
        /// <param name="serviceMetadata">Metadata from the automatic point exclusion service</param>
        private void InformUserAboutPointExclusionResultsIfRequired(AutomaticPointExclusionMetadata serviceMetadata)
        {
            if (serviceMetadata == null || !serviceMetadata.WasServiceRunToCompletion)
                return;

            var standardDeviationThreshold = _settingsManager.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionStdDevThreshold);
            var residualStandardErrorThreshold = _settingsManager.GetSetting<double>(SettingKeys.BvaAutomaticPointExclusionRseThreshold);
            if (serviceMetadata.NumberOfPointsExcluded == 0 && serviceMetadata.LastComputedStandardDeviation >= standardDeviationThreshold)
            {
                IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information,
                                                                         AutomaticPointExclusionCouldNotLowerStdDevMessage,
                                                                         AutomaticPointExclusionCaption, MessageBoxChoiceSet.Close));
                _logger.Log(AutomaticPointExclusionCouldNotLowerStdDevMessage, Category.Info, Priority.Low);
            }
            else if (serviceMetadata.NumberOfPointsExcluded > 0 && serviceMetadata.LastComputedResidualStandardError >= residualStandardErrorThreshold)
            {
                IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning,
                                                                         AutomaticPointExclusionHighRseMessage,
                                                                         AutomaticPointExclusionCaption, MessageBoxChoiceSet.Close));
                _logger.Log(AutomaticPointExclusionHighRseMessage, Category.Info, Priority.Low);
            }
        }

        /// <summary>
        /// Handles the production and consumption of a semaphore that prevents the automatic
        /// point exclusion service from being called too soon.
        /// </summary>
        /// <remarks>
        /// The reason for the semaphore is because when a whole sample is being reincluded,
        /// Sample A is included first, which triggers a measured blood volume change, which
        /// invokes the service. At this point, Sample B hasn't yet been included, so the 
        /// service ignores it. The main problem is that multiple changes to the samples
        /// need to have time to propogate before point exclusion is applied.
        /// 
        /// The semaphore is produced on (incremented) every time the view model tries to
        /// apply point exclusion. After a delay, the semaphore is consumed (decremented),
        /// and we try to apply point exclusion again. (The CanAutomaticPointExclusionBeApplied()
        /// method prevents the service from running if the semphore is non-zero.) Thus,
        /// the only the last call to ApplyPointExclusionService() will be honored.
        /// </remarks>
        private void HandleSemaphoreForAutomaticPointExclusion()
        {
            LogDebugMessage("HandleSemaphoreForAutomaticPointExclusion",
                            "ApplyAutomaticPointExclusion() called; producing on semaphore (was " +
                            _automaticPointExclusionSemaphore + ")");
            _automaticPointExclusionSemaphore++;
            Task.Factory.StartNew(() =>
                {
                    LogDebugMessage("HandleSemaphoreForAutomaticPointExclusion",
                                    "ApplyAutomaticPointExclusion() delay thread sleeping for " +
                                    AutomaticPointExclusionExecutionDelayInMilliseconds + " ms");
                    Thread.Sleep(AutomaticPointExclusionExecutionDelayInMilliseconds);
                    LogDebugMessage("HandleSemaphoreForAutomaticPointExclusion", "ApplyAutomaticPointExclusion() delay thread done sleeping");
                }).ContinueWith(answer =>
                    {
                        _automaticPointExclusionSemaphore--;
                        LogDebugMessage("HandleSemaphoreForAutomaticPointExclusion",
                                        "ApplyAutomaticPointExclusion() delay thread consuming on semaphore (was " +
                                        _automaticPointExclusionSemaphore + ")");
                        ApplyAutomaticPointExclusion(true);
                    });
        }

        /// <summary>
        /// Determines whether or not the criteria for apply automatic point exclusion have been met.
        /// (1) There must be a non-null service instance, (2) the semaphore must be fully consumed,
        /// (3) the service cannot already be in use, (4) there must be a current test instance,
        /// (5) the test must be "Completed", (6) point exclusion must be enabled for this test.
        /// </summary>
        /// <remarks>Some of these criteria would be handled by the automatic point exclusion
        /// service; however, adding the check here prevents the service from being called unnecessarily
        /// to help with performance</remarks>
        /// <returns>True if all criteria have been met; false if one more criteria have not been met</returns>
        private bool CanAutomaticPointExclusionBeApplied()
        {
            if (AutomaticPointExclusionService == null)
            {
                LogInformationMessage("CanAutomaticPointExclusionBeApplied", "Cannot apply automatic point exclusion if the APE service reference is null");
                return false;
            }
            if (_automaticPointExclusionSemaphore != 0)
            {
                LogInformationMessage("CanAutomaticPointExclusionBeApplied", "Automatic point exclusion not being run because the semaphore has not been fully consumed");
                return false;
            }
            if (!_automaticPointExclusionFinished)
            {
                LogInformationMessage("CanAutomaticPointExclusionBeApplied", "Automatic point exclusion not being run because the previous run has not yet completed");
                return false;
            }
            if (CurrentTest == null)
            {
                LogInformationMessage("CanAutomaticPointExclusionBeApplied", "Cannot apply automatic point exclusion if the current test is null");
                return false;
            }
            if (CurrentTest.Status != TestStatus.Completed)
            {
                LogInformationMessage("CanAutomaticPointExclusionBeApplied", "Cannot apply automatic point exclusion if the test status is not 'Completed'");
                return false;
            }
            if (_isAutomaticExclusionDisabledForThisTest)
            {
                LogInformationMessage("CanAutomaticPointExclusionBeApplied", "Cannot apply automatic point exclusion because it was disabled for this test because the user re-included an automatically excluded sample");
                return false;
            }
            if (TestHasIncompleteFields())
            {
                LogInformationMessage("CanAutomaticPointExclusionBeApplied", "Cannot apply automatic point exclusion because the test has incomplete fields");
                return false;
            }
            return true;
        }

        private bool TestHasIncompleteFields()
        {
            var numMissingPostInjectionTimes = CurrentTest.PatientCompositeSamples.Count(s => s.PostInjectionTimeInSeconds < 0);
            if (numMissingPostInjectionTimes > 0) return true;

            var numMissingHematocritAValues = CurrentTest.PatientCompositeSamples.Count(s => s.HematocritA < 0);
            if (numMissingHematocritAValues > 0) return true;
            var numMissingHematocritBValues = CurrentTest.HematocritFill == HematocritFillType.TwoPerSample ? CurrentTest.PatientCompositeSamples.Count(s => s.HematocritB < 0) : 0;
            if (numMissingHematocritBValues > 0) return true;
            var numMissingBaselineHematocrits = CurrentTest.BaselineSamples.Count(s => s.Hematocrit < 0);
            if (numMissingBaselineHematocrits > 0) return true;

            var numMissingACounts = CurrentTest.PatientCompositeSamples.Count(s => s.CountsA < 0);
            if (numMissingACounts > 0) return true;
            var numMissingBCounts = CurrentTest.PatientCompositeSamples.Count(s => s.CountsB < 0);
            if (numMissingBCounts > 0) return true;
            if (CurrentTest.BackgroundCount < 0) return true;
            var numMissingStandardCounts = CurrentTest.StandardSamples.Count(s => s.Counts < 0);
            if (numMissingStandardCounts > 0) return true;
            var numMissingBaselineCounts = CurrentTest.BaselineSamples.Count(s => s.Counts < 0);
            if (numMissingBaselineCounts > 0) return true;

            return false;
        }

        /// <summary>
        /// Attempt to re-include any points that are marked as auto-excluded. If there are any
        /// points where the count-excluded and count-auto-excluded states do not match (i.e.,
        /// the user has either excluded a point or re-included a point that was auto-excluded),
        /// no reset will be performed.
        /// </summary>
        /// <returns>True if the auto-exclusion states were reset; false if there are samples
        /// where the count-excluded and count-auto-excluded states do not match</returns>
        private bool TryToResetAutomaticallyExcludedPoints()
        {
            if ((from s in CurrentTest.Samples
                 where s.Type != SampleType.Standard && s.IsCountAutoExcluded != s.IsCountExcluded
                 select s).Any())
            {
                LogInformationMessage("TryToResetAutomaticallyExcludedPoints",
                                      "Not resetting automatically excluded points because the user excluded points manually or undid an auto-exclusion");
                return false;
            }

            foreach (var sample in CurrentTest.PatientSamples)
                sample.IsCountAutoExcluded = false;
            LogInformationMessage("TryToResetAutomaticallyExcludedPoints", "Previous automatic exclusion points have been reset");
            return true;
        }

        /// <summary>
        /// Commits a log entry used for debugging. The message being logged will contain the name
        /// of this view model, the current test's internal ID, the given method name, and
        /// the given message.
        /// </summary>
        /// <param name="methodName">Method where the logged action is taking place</param>
        /// <param name="message">Message to be logged</param>
        private void LogDebugMessage(string methodName, string message)
        {
            _logger.Log(String.Format(LogFormatString, CurrentTest.InternalId, methodName, message), Category.Debug, Priority.Low);
        }

        /// <summary>
        /// Commits a log entry used for information. The message being logged will contain the name
        /// of this view model, the current test's internal ID, the given method name, and
        /// the given message.
        /// </summary>
        /// <param name="methodName">Method where the logged action is taking place</param>
        /// <param name="message">Message to be logged</param>
        private void LogInformationMessage(string methodName, string message)
        {
            _logger.Log(String.Format(LogFormatString, CurrentTest.InternalId, methodName, message), Category.Info, Priority.Low);
        }

        /// <summary>
        /// Commits a log entry used for capturing exception information. The message being logged 
        /// will contain the name  of this view model, the current test's internal ID, the given 
        /// method name, and the given message.
        /// </summary>
        /// <param name="methodName">Method where the logged action is taking place</param>
        /// <param name="message">Message to be logged</param>
        private void LogExceptionMessage(string methodName, string message)
        {
            _logger.Log(String.Format(LogFormatString, CurrentTest.InternalId, methodName, message), Category.Exception, Priority.High);
        }

        /// <summary>
        /// Commits a log entry used for capturing warning information. The message being logged 
        /// will contain the name  of this view model, the current test's internal ID, the given 
        /// method name, and the given message.
        /// </summary>
        /// <param name="methodName">Method where the logged action is taking place</param>
        /// <param name="message">Message to be logged</param>
        private void LogWarningMessage(string methodName, string message)
        {
            _logger.Log(String.Format(LogFormatString, CurrentTest.InternalId, methodName, message), Category.Warn, Priority.High);
        }

        #endregion

        #region BestFitLine
        
        public void RefreshBestFitLinePoints()
        {
            if (CurrentTest == null)
                return;
            if ((CurrentTest.Mode != TestMode.Manual) && (CurrentTest.Status != TestStatus.Completed))
                return;
            if (BestFitLinePointsCalculator == null)
                return;
            if (MeasuredVolume == null)
                return;

            //Get all ubv points that will be used to calculate best fitted line
            var bestFittedLineUnadjustedBloodVolumePoints = (from ubvPoint in UnadjustedBloodVolumePoints.AsParallel()
                                           where !ubvPoint.ObservableCompositePatientSample.IsWholeSampleExcluded && ubvPoint.ObservableCompositePatientSample.UnadjustedBloodVolume > 0
                                           select ubvPoint.ObservableCompositePatientSample).ToList();

            try
            {
                //only run best fit line calculator if we have 2 or more points.
                if (bestFittedLineUnadjustedBloodVolumePoints.Count >= 2)
                {
                    BestFitLinePoints.ClearAndAddRange(BestFitLinePointsCalculator.CalculateBestFitLinePoints(bestFittedLineUnadjustedBloodVolumePoints,
                                                                               CurrentTest.TransudationRateResult,
                                                                               MeasuredVolume.WholeCount));
                }
            }
            catch (Exception ex)
            {
                BestFitLinePoints.Clear();
                LogExceptionMessage("RefreshBestFitLinePoints", ex.Message);
            }
        }

        #endregion

        #region AggregateEventHandlers

        /// <summary>
        /// Called when new BVATest is selected.
        /// </summary>
        /// <param name="test"></param>
        void BVATestSelectedEventHandler(BVATest test)
        {
            CurrentTest = test;
            FirePropertyChanged("MatrixDeviationMillilitersBloodVolume", "MatrixDeviationMillilitersPlasmaVolume",
                "MatrixDeviationMillilitersRedCellVolume", "MatrixDeviationPercentageBloodVolume",
                "MatrixDeviationPercentagePlasmaVolume", "MatrixDeviationPercentageRedCellVolume");
        }

        void OnTestSampleValuesActivationChanged(ActivationChangedPayload activationPayload)
        {
            if (!activationPayload.IsActivated) IsTestAnalysisActive = false;
            else
            {
                _logger.Log("TestAnalysisViewModel: TestAnalysisViewModel has been activated", Category.Debug, Priority.Low);

                //Update the current test 
                if (CurrentTest.Mode == TestMode.Manual ||
                   CurrentTest.Status != TestStatus.Pending)
                {
                    IsTestAnalysisActive = true;
                    UpdateUnadjustedBloodPointViewModels();
                    RefreshBestFitLinePoints();
                }
            }
        }

        void OnViewReportExecuteChanged(bool isReportShowing)
        {
            _isReportBeingViewed = isReportShowing;
            ViewReportCommand.RaiseCanExecuteChanged();
        }

        void OnTestStarted(ITest test)
        {
            AbortTestCommand.RaiseCanExecuteChanged();
            ExportReportCommand.RaiseCanExecuteChanged();
        }

        void OnTestAborted(ITest test)
        {
            AbortTestCommand.RaiseCanExecuteChanged();
            ExportReportCommand.RaiseCanExecuteChanged();
        }

        void OnTestFinished(ITest test)
        {
            LogInformationMessage("OnTestFinished", "The test has completed.");
            AbortTestCommand.RaiseCanExecuteChanged();
            ExportReportCommand.RaiseCanExecuteChanged();
            ApplyAutomaticPointExclusion();
            FirePropertyChanged("MaxYValue");
            FirePropertyChanged("MinYValue");
            FirePropertyChanged("YAxisLabelStep");

            // we should update the scale only if we are currently viewing the test
            // that just completed
            if (test.InternalId == CurrentTest.InternalId && IsTestAnalysisActive)
            {
                UpdateUnadjustedBloodPointViewModels();
                RefreshBestFitLinePoints();
            }
        }

        void OnSampleChangerPositionChangeEnd(SampleChangerPositionChangeCompletedPayload payload)
        {
            SampleChangerPosition = payload.NewPosition;
        }

        #endregion

        #region IDataErrorInfo Members
        /// <summary>
        /// Helps to surface errors found in the model
        /// </summary>
        public string Error
        {
            get { return String.Empty; }
        }
        public string this[string columnName]
        {
            get
            {

                if (columnName == "TimeZeroBloodVolume")
                {
                    return CurrentTest != null ? CurrentTest.Volumes[BloodType.Measured]["WholeCount"] : String.Empty;
                }
                if (columnName == "TransudationRate")
                {
                    return CurrentTest != null ? CurrentTest["TransudationRateResult"] : String.Empty;
                }
                if (columnName == "StdDev")
                {
                    return CurrentTest != null ? CurrentTest["StandardDevResult"] : String.Empty;
                }
                
                return String.Empty;
            }
        }

        #endregion

        #region Methods pertaining to total BV, slope, and std dev alerts

        protected virtual void RefreshBloodVolumeAlert()
        {
            FirePropertyChanged("IsTotalBloodVolumeOutOfRange");
            TotalBloodVolumeAlertCommand.RaiseCanExecuteChanged();
        }

        protected virtual void RefreshTransudationAlerts()
        {
            FirePropertyChanged("IsTransudationRateNegative");

            TransudationRateAlertCommand.RaiseCanExecuteChanged();
        }

        protected virtual void RefreshStandardDeviationAlert()
        {
            FirePropertyChanged("IsStdDevOutOfRange");

            StandardDeviationAlertCommand.RaiseCanExecuteChanged();
        }

        #endregion
    }
}
