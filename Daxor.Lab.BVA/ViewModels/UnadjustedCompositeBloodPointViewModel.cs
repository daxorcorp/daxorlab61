﻿using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Utilities;

namespace Daxor.Lab.BVA.ViewModels
{
    public class UnadjustedCompositeBloodPointViewModel : ViewModelBase
    {
        #region Fields

        readonly string _pointId;
	    // ReSharper disable once PrivateFieldCanBeConvertedToLocalVariable
        readonly PropertyObserver<BVACompositeSample> _sampleObserver;
        readonly BVACompositeSample _compositeSample;

        #endregion

        #region Properties

        public double PostInjectionTimeInMinutes
        {
            get { return _compositeSample == null ? -1 : _compositeSample.PostInjectionTimeInSeconds / 60.0; }
        }

        public string PointId
        {
            get { return _pointId; }
        }

        public BVACompositeSample ObservableCompositePatientSample
        {
            get { return _compositeSample; }
        }

        #endregion

        #region Ctor

        public UnadjustedCompositeBloodPointViewModel(BVACompositeSample compositeSample, string pointId)
        {
            if (compositeSample == null)
                throw new ArgumentNullException("compositeSample");
            if (String.IsNullOrEmpty(pointId))
                throw new ArgumentException("pointId cannot be null or empty");

            _compositeSample = compositeSample;
            _pointId = pointId;

            _sampleObserver = new PropertyObserver<BVACompositeSample>(_compositeSample);
            _sampleObserver.RegisterHandler(sample => compositeSample.PostInjectionTimeInSeconds, sample => FirePropertyChanged("PostInjectionTimeInMinutes"));
        }

        #endregion
    }
}