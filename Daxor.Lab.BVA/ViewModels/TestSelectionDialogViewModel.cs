﻿using Daxor.Lab.Infrastructure.Entities;

namespace Daxor.Lab.BVA.ViewModels
{
    public class TestSelectionDialogViewModel
    {
        public TestSelectionDialogActions SelectedAction { get; set; }

        public TestSelectionDialogViewModel()
        {
            SelectedAction = TestSelectionDialogActions.OpenTest;
        }
    }
}
