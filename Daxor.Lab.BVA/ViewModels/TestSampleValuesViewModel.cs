﻿using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Eventing;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Windows.Input;

namespace Daxor.Lab.BVA.ViewModels
{
    /// <summary>
    /// The test samples values view model.
    /// </summary>
    public class TestSampleValuesViewModel : ViewModelBase, IActiveAware
    {
        #region Constants

        private const int EmptyValue = -1;

        #endregion

        #region Fields

        private bool _areCountsAndUnadjustedVolumesVisible;
        private readonly ObservableCollection<CompositeSampleViewModel> _compositeSampleViewModels = new ObservableCollection<CompositeSampleViewModel>();
        private readonly IAppModuleNavigator _localNavigator;
        private readonly IEventAggregator _eventAggregator;
        private readonly IMeasuredCalcEngineService _mCalcEngine;
        private readonly IIdealsCalcEngineService _iCalcEngine;
        readonly ITestExecutionController _testExecutioner;
        private readonly ILoggerFacade _logger;
        private readonly IBackgroundAcquisitionService _backgroundAcquisitionService;
        private DelegateCommand<Object> _navigateToMyselfCommand;
        private DelegateCommand<Object> _overrideBackgroundCommand;
        private BVATest _testModel;
        private PropertyObserver<BVATest> _testObserver;
        private PropertyObserver<BVAPatient> _patientObserver;
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly IMessageManager _messageManager;

        #endregion

        #region Ctor

        public TestSampleValuesViewModel([Dependency(AppModuleIds.BloodVolumeAnalysis)] IAppModuleNavigator localNavigator,
                                         [Dependency(AppModuleIds.BloodVolumeAnalysis)] IRuleEngine ruleEngine,
                                         IEventAggregator eventAggregator,
                                         ITestExecutionController testExecutioner,
                                         IIdealsCalcEngineService iCalcEngine,
                                         IMeasuredCalcEngineService mCalcEngine,
                                         ILoggerFacade customLoggerFacade,
                                         IBackgroundAcquisitionService backgroundAcquisitionService,
                                         IMessageBoxDispatcher messageBoxDispatcher,
                                         IMessageManager messageManager)
        {
            _localNavigator = localNavigator;
            _eventAggregator = eventAggregator;
            _testExecutioner = testExecutioner;
            _iCalcEngine = iCalcEngine;
            _mCalcEngine = mCalcEngine;
            _logger = customLoggerFacade;
            _backgroundAcquisitionService = backgroundAcquisitionService;
            _messageBoxDispatcher = messageBoxDispatcher;
            _messageManager = messageManager;
            RuleEngine = ruleEngine;

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InitializeViewModel();
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        protected virtual void InitializeCommands()
        {
            NavigateToMyselfDelegateCommand = new DelegateCommand<object>(ExecuteNavigateToMyself, CanNavigateToMyself);
            OverrideBackgroundDelegateCommand = new DelegateCommand<object>(ExecuteOverrideBackground, CanOverrideBackground);
        }

        protected virtual void InitializeViewModel()
        {
            InitializeCommands();
            ConfigureViewModel();
            SubscribeToAggregateEvents();
        }

        #endregion

        #region Properties

        private IRuleEngine RuleEngine { get; set; }
        
        private DelegateCommand<object> NavigateToMyselfDelegateCommand
        {
            get { return _navigateToMyselfCommand; }
            set
            {
                // Only get one assignment
                if (_navigateToMyselfCommand == null)
                    _navigateToMyselfCommand = value;
            }
        }

        private DelegateCommand<object> OverrideBackgroundDelegateCommand
        {
            get { return _overrideBackgroundCommand; }
            set
            {
                // Only get one assignment
                if (_overrideBackgroundCommand == null)
                    _overrideBackgroundCommand = value;
            }
        }

        public bool AreCountsAndUnadjustedVolumesVisibile
        {
            get { return _areCountsAndUnadjustedVolumesVisible; }
            set { SetValue(ref _areCountsAndUnadjustedVolumesVisible, "AreCountsAndUnadjustedVolumesVisibile", value); }
        }
        public BVATest CurrentTest
        {
            get { return _testModel; }
            set
            {
                if (!SetValue(ref _testModel, "CurrentTest", value)) return;

                InitializeTestPropertyObserver(_testModel);
                FirePropertyChanged("CurrentTest");
                NavigateToMyselfDelegateCommand.RaiseCanExecuteChanged();
            }
        }

        public ObservableCollection<CompositeSampleViewModel> CompositeSampleViewModels
        {
            get { return _compositeSampleViewModels; }
        }

        #endregion

        #region Commands

        public ICommand NavigateToMyself
        {
            get { return NavigateToMyselfDelegateCommand; }
        }
        private void ExecuteNavigateToMyself(object arg)
        {
            _logger.Log("NAVIGATE TO TEST SAMPLES (SELF) TAB CLICKED", Category.Info, Priority.None);
            _localNavigator.ActivateView(BVAModuleViewKeys.TestSampleValues);
        }
        private bool CanNavigateToMyself(object arg)
        {
            return CurrentTest != null && CurrentTest.Protocol != -1 && !String.IsNullOrWhiteSpace(CurrentTest.Patient.HospitalPatientId);
        }

        public ICommand OverrideBackground
        {
            get { return OverrideBackgroundDelegateCommand; }
        }
        private void ExecuteOverrideBackground(object arg)
        {
            int newBackground;
            if ((_backgroundAcquisitionService.Status == BackgroundAcquisitionServiceStatus.Acquiring ||
               _backgroundAcquisitionService.Status == BackgroundAcquisitionServiceStatus.SeekingBackgroundPosition) &&
               int.TryParse(arg.ToString(), out newBackground))
            {
                _backgroundAcquisitionService.OverrideBackground(newBackground);
            }

            VirtualKeyboardManager.CloseActiveKeyboard();
        }
        private bool CanOverrideBackground(object arg)
        {
            return CurrentTest != null && !CurrentTest.HasGoodBackground
                && CurrentTest.Status == TestStatus.Running &&
               _backgroundAcquisitionService.Status == BackgroundAcquisitionServiceStatus.Acquiring ||
               _backgroundAcquisitionService.Status == BackgroundAcquisitionServiceStatus.SeekingBackgroundPosition;
        }

        #endregion

        #region IActiveAware Members

        private bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                var prevIsActive = _isActive;
                _isActive = value;

                if (_isActive && prevIsActive != _isActive)
                {
                    _logger.Log("TestSampleValuesViewModel: TestSampleValues isActive: " + _isActive + " wasActive: " + prevIsActive,
                        Category.Debug, Priority.Low);
                    SyncSampleViewModelsWithCurrentTest();
                    SetCountsAndUnadjustedVolumesVisiblility(CurrentTest);
                    TryUpdateIdealVolumes();
                    TryUpdateMeasuredVolumes();

                }
                //If isActive changed
                if (prevIsActive != _isActive)
                    WireEachCompositeSampleEvents(_isActive);

                if (CurrentTest.Mode == TestMode.Manual)
                {
                    CurrentTest.Status = TestStatus.Completed;
                }

                VirtualKeyboardManager.CloseActiveKeyboard();
                _eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(BVAModuleViewKeys.TestSampleValues, value));
            }
        }
        public event EventHandler IsActiveChanged = delegate { };

        #endregion

        #region Helpers

        protected virtual void InitializeTestPropertyObserver(BVATest test)
        {
            if (test != null)
            {
                _testObserver = new PropertyObserver<BVATest>(test);
                _testObserver.RegisterHandler(t => t.Protocol, OnTestProtocolChanged);
                _testObserver.RegisterHandler(t => t.DurationAdjustedBackgroundCounts, OnDurationAdjustedBackgroundCountsChanged);
                _testObserver.RegisterHandler(t => t.HasGoodBackground, t => OverrideBackgroundDelegateCommand.RaiseCanExecuteChanged());
                _testObserver.RegisterHandler(t => t.Status, OnCurrentTestStatusChanged);
                _testObserver.RegisterHandler(t => t.Mode, OnTestModeChanged);
                _testObserver.RegisterHandler(t => t.Patient, t =>
                    {
                        _patientObserver = new PropertyObserver<BVAPatient>(t.Patient);
                        _patientObserver.RegisterHandler(p => p.HospitalPatientId, p => NavigateToMyselfDelegateCommand.RaiseCanExecuteChanged());
                        NavigateToMyselfDelegateCommand.RaiseCanExecuteChanged();
                    });

                //Start observing patient
                if (test.Patient != null)
                {
                    _patientObserver = new PropertyObserver<BVAPatient>(test.Patient);
                    _patientObserver.RegisterHandler(p => p.HospitalPatientId, p => NavigateToMyselfDelegateCommand.RaiseCanExecuteChanged());
                }

                SyncSampleViewModelsWithCurrentTest();
            }
        }

        void ConfigureViewModel()
        {
            DisplayName = "Test Setup: Sample Values";

            //Subscribe to AppPartActivationChanged event
            _eventAggregator.GetEvent<TestSetupTabItemAdded>().Publish(new TestSetupTabItemPayload(Guid.NewGuid())
            {
                Header = "3. Sample Values",
                TabPlacementIndex = 2,
                NavigationCommand = NavigateToMyselfDelegateCommand
            });

            //Creates default sampleViewModels and then adds background
            //populate CompositeSamples from Background to Sample6
            for (var startIndex = (int)SampleType.Background; startIndex <= (int)SampleType.Sample6; startIndex++)
            {
                var cSample = new CompositeSampleViewModel(
                    _mCalcEngine,
                    new BVACompositeSample(
                        CurrentTest,
                        new BVASample(0, RuleEngine) { Type = (SampleType)startIndex, Range = SampleRange.A },
                        new BVASample(0, RuleEngine) { Type = (SampleType)startIndex, Range = SampleRange.B }, 
                        RuleEngine
                        ), _messageBoxDispatcher, _logger, _messageManager, false);
                CompositeSampleViewModels.Add(cSample);
            }
        }

        protected void SubscribeToBvaTestSelectedEvent()
        {
            _eventAggregator.GetEvent<BvaTestSelected>().Subscribe(BVATestSelectedEventHandler, ThreadOption.PublisherThread, false);
        }

        void SubscribeToAggregateEvents()
        {
            SubscribeToBvaTestSelectedEvent();

            //TestExecutionProgressChanged
            _eventAggregator.GetEvent<TestExecutionProgressChanged>().Subscribe(
                 OnAggregateTestExecutionProgressChanged,
                 ThreadOption.UIThread,
                 false,
                 tPayload => tPayload.ExecutingTestType == typeof(BVATest));

            //SampleAcquisitionCompelted
            _eventAggregator.GetEvent<SampleAcquisitionCompleted>().Subscribe(
                OnAggregateSampleAcquisitionCompleted,
                ThreadOption.UIThread,
                false,
                sPayload => sPayload.Sample is BVASample);
        }
        void SyncSampleViewModelsWithCurrentTest()
        {
            Dictionary<SampleType, BVACompositeSample> currentTestSampleMap = CurrentTest.CompositeSamples.ToDictionary(s => s.SampleType);

            //Go through existing observable collection of CompositeSampleViewModels
            //and update to new BVACompositeSample.
            foreach (var cSampleVm in CompositeSampleViewModels)
            {
                cSampleVm.ManualCountsRequired = CurrentTest.Mode == TestMode.Manual;
                if (cSampleVm.Type == SampleType.Background)
                {
                    var background = new BVACompositeSample(
                         CurrentTest,
                         new BVASample(24, RuleEngine) { Type = SampleType.Background, Range = SampleRange.A },
                         new BVASample(24, RuleEngine) { Type = SampleType.Background, Range = SampleRange.B }, 
                         RuleEngine);

                    cSampleVm.UpdateCompositeSample(background);
                    cSampleVm.CountingSampleA = false;
                    cSampleVm.CountingSampleB = false;
                    cSampleVm.SampleACounts = cSampleVm.CompositeSample.Test.DurationAdjustedBackgroundCounts.ToString(CultureInfo.InvariantCulture);
                    cSampleVm.IsReadOnly = !(cSampleVm.CompositeSample.Test.Mode == TestMode.Manual || !cSampleVm.CompositeSample.Test.HasGoodBackground);
                    continue;
                }

                cSampleVm.CountingSampleA = false;
                cSampleVm.CountingSampleB = false;
                cSampleVm.IsReadOnly = CurrentTest.Mode != TestMode.Manual;
                cSampleVm.IsEnabled = false;

                if (currentTestSampleMap.ContainsKey(cSampleVm.Type))
                {
                    cSampleVm.UpdateCompositeSample(currentTestSampleMap[cSampleVm.Type]);
                }
                else
                {
                    _logger.Log("TestSamplesViewModel: UpdateCompositeSample of type: " + cSampleVm.Type, Category.Debug, Priority.Low);

                    //Put a new placeholder in place
                    cSampleVm.UpdateCompositeSample(new BVACompositeSample(
                       CurrentTest,
                       new BVASample(0, RuleEngine) { Type = cSampleVm.Type, Range = SampleRange.A, Counts = 0 },
                       new BVASample(0, RuleEngine) { Type = cSampleVm.Type, Range = SampleRange.B, Counts = 0 }, 
                       RuleEngine), false);
                }
            }
        }
        void WireEachCompositeSampleEvents(bool shouldWire)
        {
            if (shouldWire)
            {
                foreach (var sample in CompositeSampleViewModels)
                    sample.PropertyChanged += CompositeSampleViewModelPropertyChanged;
            }
            else
            {
                foreach (var sample in CompositeSampleViewModels)
                    sample.PropertyChanged -= CompositeSampleViewModelPropertyChanged;
            }
        }

        #endregion

        #region EventHandlers

        void CompositeSampleViewModelPropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            var viewModelWithPropertyChanged = (CompositeSampleViewModel)sender;
            if (IsActive && (args.PropertyName == "UnadjustedBloodVolume" || args.PropertyName == "IsWholeSampleExcluded" ||
                args.PropertyName == "PostInjectionTimeInSeconds" || args.PropertyName == "SampleACounts" || args.PropertyName == "SampleBCounts"))
            {
                TryUpdateMeasuredVolumes();
            }

            if (args.PropertyName == "HematocritA")
            {
                if (viewModelWithPropertyChanged.CompositeSample.Test.HematocritFill == HematocritFillType.LastTwo)
                    VerifyLastTwoHematocritsWithinRange(viewModelWithPropertyChanged);

                if ((viewModelWithPropertyChanged.CompositeSample.Test.HematocritFill == HematocritFillType.OnePerSample || 
                    viewModelWithPropertyChanged.CompositeSample.Test.HematocritFill == HematocritFillType.OnePerTest ||
                    viewModelWithPropertyChanged.CompositeSample.Test.HematocritFill == HematocritFillType.LastTwo))
                {
                    CurrentTest.UpdateSamplesHematocritBasedOnFill();
                }
                EvaluateOtherHematocritA(viewModelWithPropertyChanged);
            }

            //Update background sample counts
            if (CurrentTest.Mode == TestMode.Manual && viewModelWithPropertyChanged.Type == SampleType.Background
                && args.PropertyName == "SampleACounts")
            {
                int backgroundCounts;
                CurrentTest.BackgroundCount = Int32.TryParse(viewModelWithPropertyChanged.SampleACounts, out backgroundCounts) ? backgroundCounts : EmptyValue;
            }
        }

        private void VerifyLastTwoHematocritsWithinRange(CompositeSampleViewModel compositeSampleViewModel)
        {
            var continueEvaluation = false;
            var firstSample = new BVASample(0, RuleEngine);  // references get replaced with actual samples below
            var secondSample = new BVASample(0, RuleEngine);
            switch (CurrentTest.Protocol)
            {
                case 3: if (compositeSampleViewModel.Type == SampleType.Sample2 || compositeSampleViewModel.Type == SampleType.Sample3)
                        {
                            continueEvaluation = true;
                            firstSample = (from s in CurrentTest.Samples where s.Type == SampleType.Sample2 select s).FirstOrDefault();
                            secondSample = (from s in CurrentTest.Samples where s.Type == SampleType.Sample3 select s).FirstOrDefault();
                            if (firstSample == null || secondSample == null) return;
                        }
                        break;
                case 4: if (compositeSampleViewModel.Type == SampleType.Sample3 || compositeSampleViewModel.Type == SampleType.Sample4)
                        {
                            continueEvaluation = true;
                            firstSample = (from s in CurrentTest.Samples where s.Type == SampleType.Sample3 select s).FirstOrDefault();
                            secondSample = (from s in CurrentTest.Samples where s.Type == SampleType.Sample4 select s).FirstOrDefault();
                            if (firstSample == null || secondSample == null) return;
                        }
                        break;
                case 5: if (compositeSampleViewModel.Type == SampleType.Sample4 || compositeSampleViewModel.Type == SampleType.Sample5)
                        {
                            continueEvaluation = true;
                            firstSample = (from s in CurrentTest.Samples where s.Type == SampleType.Sample4 select s).FirstOrDefault();
                            secondSample = (from s in CurrentTest.Samples where s.Type == SampleType.Sample5 select s).FirstOrDefault();
                            if (firstSample == null || secondSample == null) return;
                        }
                        break;
                case 6: if (compositeSampleViewModel.Type == SampleType.Sample5 || compositeSampleViewModel.Type == SampleType.Sample6)
                        {
                            continueEvaluation = true;
                            firstSample = (from s in CurrentTest.Samples where s.Type == SampleType.Sample5 select s).FirstOrDefault();
                            secondSample = (from s in CurrentTest.Samples where s.Type == SampleType.Sample6 select s).FirstOrDefault();
                            if (firstSample == null || secondSample == null) return;
                        }
                        break;
            }

            // If not one of the last two, the update was triggered from a helper method, not user input.
            if (!continueEvaluation) return;

            // Both need to be defined
            // ReSharper disable CompareOfFloatsByEqualityOperator
            if ((firstSample.Hematocrit == -1) || (secondSample.Hematocrit == -1)) return;
            // ReSharper restore CompareOfFloatsByEqualityOperator

            // If too different, clear and display message box
            if (Math.Abs(firstSample.Hematocrit - secondSample.Hematocrit) > Core.StatisticalConstants.MaximumPercentageDifferenceForLastTwoHematocrits)
            {
                var message = _messageManager.GetMessage(MessageKeys.BvaLastTwoHematocritsTooDifferent).FormattedMessage;
                message = String.Format(message, Core.StatisticalConstants.MaximumPercentageDifferenceForLastTwoHematocrits);

                _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning, message, "Hematocrit Difference", new [] {"Clear Hematocrits"});
                _logger.Log(message, Category.Info, Priority.Low);
                firstSample.Hematocrit = -1;
                secondSample.Hematocrit = -1;
            }
        }

        void SetCountsAndUnadjustedVolumesVisiblility(BVATest test)
        {
            AreCountsAndUnadjustedVolumesVisibile = test.Mode == TestMode.Manual || test.Status != TestStatus.Pending;
            CompositeSampleViewModels.ToList().ForEach(c => c.ManualCountsRequired = test.Mode == TestMode.Manual);
        }

        private void EvaluateOtherHematocritA(CompositeSampleViewModel viewModelWithPropertyChanged)
        {
            WireEachCompositeSampleEvents(shouldWire: false);   // Need to remove all events so we don't spawn an infinite loop

            foreach (var viewModel in CompositeSampleViewModels)
            {
                if (viewModel == viewModelWithPropertyChanged)
                    continue;

                viewModel.ForceRuleEvaluation();
            }

            WireEachCompositeSampleEvents(shouldWire: true);
        }

        #endregion

        #region PropertyObservers

        void OnCurrentTestStatusChanged(BVATest test)
        {
            CompositeSampleViewModels.ToList().ForEach(c => c.CountingSampleA = c.CountingSampleB = false);
        }
        void OnDurationAdjustedBackgroundCountsChanged(BVATest test)
        {
            var backgroundViewModel = CompositeSampleViewModels[(int)SampleType.Background];
            if (backgroundViewModel.CompositeSample != null && backgroundViewModel.CompositeSample.Test != null
                && backgroundViewModel.CompositeSample.Test == test)
            {
                backgroundViewModel.SampleACounts = test.DurationAdjustedBackgroundCounts.ToString(CultureInfo.InvariantCulture);
            }

            OverrideBackgroundDelegateCommand.RaiseCanExecuteChanged();
        }
        void OnTestProtocolChanged(BVATest test)
        {
            NavigateToMyselfDelegateCommand.RaiseCanExecuteChanged();
            SyncSampleViewModelsWithCurrentTest();
        }
        void OnTestModeChanged(BVATest test)
        {
            CompositeSampleViewModels.ToList().ForEach(c => c.IsReadOnly = test.Mode != TestMode.Manual);

        }

        #endregion

        #region AggregateEventHandlers

        /// <summary>
        /// Called when new BVATest is selected.
        /// </summary>
        /// <param name="test"></param>
        void BVATestSelectedEventHandler(BVATest test)
        {
            CurrentTest = test;

            TryUpdateIdealVolumes();
            if (CurrentTest.Status != TestStatus.Pending)
                TryUpdateMeasuredVolumes();

            Debug.Print("TestSampleValuesViewModel received CurrentTest {0}", CurrentTest.GetHashCode().ToString(CultureInfo.InvariantCulture));
        }

        void OnAggregateTestExecutionProgressChanged(TestExecutionProgressChangedEventArgs tExecutionProgressChanged)
        {
            //Setting background countingSampleA to true/false & updating background viewModel CountsA
            var backgroundViewModel = CompositeSampleViewModels[(int)SampleType.Background];
            backgroundViewModel.CountingSampleA = tExecutionProgressChanged.CurrentExecutionStep == TestExecutionStep.CountBackground && CurrentTest.Status == TestStatus.Running;
            var cSample = (BVASample)(from s in _testExecutioner.ExecutingTest.Samples
                                      where s.InternalId == tExecutionProgressChanged.ExecutingSampleId
                                      select s).FirstOrDefault();

            //If this sample is not part of executing sample return
            if (cSample == null)
                return;

            if (tExecutionProgressChanged.CurrentExecutionStep == TestExecutionStep.CountSample)
            {
                var cSampleViewModel = CompositeSampleViewModels[(int)cSample.Type];

                if (cSampleViewModel.SampleA != null && cSampleViewModel.SampleB != null)
                {
                    if (cSampleViewModel.SampleA.InternalId == cSample.InternalId)
                        cSampleViewModel.CountingSampleA = true;
                    else if (cSampleViewModel.SampleB.InternalId == cSample.InternalId)
                        cSampleViewModel.CountingSampleB = true;
                }
            }
            else if (tExecutionProgressChanged.CurrentExecutionStep == TestExecutionStep.CountSampleCompleted)
            {
                var cSampleViewModel = CompositeSampleViewModels[(int)cSample.Type];

                if (cSampleViewModel.SampleA != null && cSampleViewModel.SampleA.InternalId == cSample.InternalId)
                {
                    cSampleViewModel.CountingSampleA = false;
                }
                else if (cSampleViewModel.SampleB != null && cSampleViewModel.SampleB.InternalId == cSample.InternalId)
                {
                    cSampleViewModel.CountingSampleB = false;
                }
            }
        }
        void OnAggregateSampleAcquisitionCompleted(SampleAcquisitionPayload sInfo)
        {
            var cSample = (BVASample)sInfo.Sample;

            if (cSample == null)
                return;

            //Remove counting sample UI
            var cSampleViewModel = CompositeSampleViewModels[(int)cSample.Type];

            if (cSampleViewModel.SampleA != null && cSampleViewModel.SampleB != null)
            {
                if (cSampleViewModel.SampleA.InternalId == cSample.InternalId) cSampleViewModel.CountingSampleA = false;
                else if (cSampleViewModel.SampleB.InternalId == cSample.InternalId) cSampleViewModel.CountingSampleB = false;

                //Update MeasuredVolumes
                TryUpdateMeasuredVolumes();
            }
        }

        #endregion

        #region Calc Ideal/Measured

        protected virtual void TryUpdateIdealVolumes()
        {
            int idealPlasmaVolume;
            var idealRedCellVolume = idealPlasmaVolume = 0;

            if (CurrentTest == null || CurrentTest.Patient == null || CurrentTest.Volumes == null) return;
            
            try
            {
                var iResults = _iCalcEngine.GetIdeals(CurrentTest.Patient.WeightInKg,
                                                        CurrentTest.Patient.HeightInCm,
                                                        (int)CurrentTest.Patient.Gender);
                if (iResults.WeightDeviation <= BvaDomainConstants.MaximumWeightDeviation && iResults.WeightDeviation > BvaDomainConstants.MinimumWeightDeviation)
                {
                    idealRedCellVolume = iResults.IdealRedCellVolume;
                    idealPlasmaVolume = iResults.IdealPlasmaVolume;
                }
                CurrentTest.Patient.DeviationFromIdealWeight = iResults.WeightDeviation;
            }
            catch (Exception e)
            {
                _logger.Log(e.Message, Category.Exception, Priority.None);
            }
            finally
            {
                if (idealPlasmaVolume <= 0 || idealRedCellVolume <= 0)
                    idealRedCellVolume = idealPlasmaVolume = 0;

                CurrentTest.Volumes[BloodType.Ideal].PlasmaCount = idealPlasmaVolume;
                CurrentTest.Volumes[BloodType.Ideal].RedCellCount = idealRedCellVolume;
            }
        }

        protected virtual void TryUpdateMeasuredVolumes()
        {
            int measuredPlasmaVolume;
            var measuredRedCellVolume = measuredPlasmaVolume = 0;

            if (CurrentTest == null)
                return;

            try
            {
                Dictionary<SampleType, BVACompositeSample> sampleMap = CurrentTest.CompositeSamples.ToDictionary(s => s.SampleType);

                var mParams = new MeasuredCalcEngineParams
                {
                    ReferenceVolume = CurrentTest.RefVolume,
                    AnticoagulantFactor = CurrentTest.Tube.AnticoagulantFactor,
                    Background = CurrentTest.DurationAdjustedBackgroundCounts,
                    StandardCounts = sampleMap[SampleType.Standard].AverageCounts,
                    BaselineCounts = sampleMap[SampleType.Baseline].AverageCounts,
                    BaselineHematocrit = sampleMap[SampleType.Baseline].AverageHematocrit,
                    IdealTotalBloodVolume = CurrentTest.Volumes[BloodType.Ideal].WholeCount,
                    Points = (from s in CurrentTest.PatientCompositeSamples
                              where s.AverageCounts > -1 && s.PostInjectionTimeInSeconds > 0
                              select new BloodVolumePoint(s.SampleType, s.IsWholeSampleExcluded, s.AreBothHematocritsExcluded,
                                  s.AreBothCountsExcluded, s.PostInjectionTimeInSeconds,
                                  s.AverageCounts, s.AverageHematocrit,
                                  s.UnadjustedBloodVolume)).ToArray()
                };
                var mResults = _mCalcEngine.GetAllResults(mParams);

                CurrentTest.StandardDevResult = mResults.StdDev;
                CurrentTest.TransudationRateResult = mResults.Slope;
                CurrentTest.NormalizeHematocritResult = mResults.NormalizedHematocrit;
                CurrentTest.RegressionStandardDevResult = mResults.NormStdDev;

                // Set to false if there are no adj. volumes
                CurrentTest.HasSufficientData = false;

                if (!Double.IsNaN(mResults.TimeZeroPlasmaVolume) && !Double.IsNaN(mResults.TimeZeroRedCellVolume))
                {
                    measuredPlasmaVolume = (int)Math.Round(mResults.TimeZeroPlasmaVolume, 0);
                    measuredRedCellVolume = (int)Math.Round(mResults.TimeZeroRedCellVolume, 0);

                    if (measuredPlasmaVolume > 0 && measuredRedCellVolume > 0)
                    {
                        CurrentTest.HasSufficientData = true;
                    }
                }

                mResults.UnadjustedBloodVolumeResults.ToList().ForEach(unadjustedBloodVolume =>
                {
                    sampleMap[(SampleType)unadjustedBloodVolume.UniqueId].UnadjustedBloodVolume = Math.Round(unadjustedBloodVolume.UnadjustedBloodVolume, 0);
                });
            }
            catch (Exception e)
            {
                //Log exception, continue
                _logger.Log(e.Message, Category.Exception, Priority.None);
            }
            finally
            {
                if (measuredPlasmaVolume <= 0 || measuredRedCellVolume <= 0)
                    measuredRedCellVolume = measuredPlasmaVolume = 0;

                CurrentTest.Volumes[BloodType.Measured].PlasmaCount = measuredPlasmaVolume;
                CurrentTest.Volumes[BloodType.Measured].RedCellCount = measuredRedCellVolume;
            }
        }

        #endregion
    }
}
