﻿using System;
using System.ComponentModel;
using Daxor.Lab.BVA.Common;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Extensions;
using Daxor.Lab.Infrastructure.Utilities;

namespace Daxor.Lab.BVA.ViewModels
{
    /// <summary>
    /// View model that encapsultes certain attributes of a BVAPatient to 
    /// help keep the view, that view's view model, and the model in synch.
    /// </summary>
    public class PatientViewModel : ViewModelBase, IDataErrorInfo
    {
        #region Constants

        public const int NumDecimalPlacesForInches = 1;

        public const int NumDecimalPlacesForCentimeters = 2;

        public const int NumDecimalPlacesForPounds = 2;

        public const int NumDecimalPlacesForKilograms = 2;

        #endregion

        #region Fields

        /// <summary>
        /// Private member variable representing the current patient (model).
        /// </summary>
        private readonly BVAPatient _currentPatient;

        /// <summary>
        /// Private member variable representing the data of birth as a string.
        /// </summary>
        private string _dateOfBirthAsString = string.Empty;

        /// <summary>
        /// Private member variable representing the current patient/test's height in centimeters.
        /// </summary>
        private string _heightInMetric;

        /// <summary>
        /// Private member variable representing the current patient/test's height in inches.
        /// </summary>
        private string _heightInEnglish;

        /// <summary>
        /// Private member variable representing the current patient/test's weight in kilograms.
        /// </summary>
        private string _weightInMetric;

        /// <summary>
        /// Private member variable representing the current patient/test's weight in pounds.
        /// </summary>
        private string _weightInEnglish;

        /// <summary>
        /// Private member variable representing a property observer for this view model's
        /// BVAPatient.
        /// </summary>
        // ReSharper disable PrivateFieldCanBeConvertedToLocalVariable
        private readonly PropertyObserver<BVAPatient> _patientObserver;
        // ReSharper restore PrivateFieldCanBeConvertedToLocalVariable

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new PatientViewModel instance based on a given BVAPatient.
        /// </summary>
        /// <param name="currentPatient">Patient on which this view model is based</param>
        /// <exception cref="ArgumentNullException">If the specified patient instance is null</exception>
        public PatientViewModel(BVAPatient currentPatient)
        {
            if (currentPatient == null)
                throw new ArgumentNullException("currentPatient", @"Failed to create patient view model; specified patient model cannot be null.");

            _currentPatient = currentPatient;

            // The PatientDemographicsView binds to this view model. Because ideal weight 
            // deviations are affected by gender and height in addition to weight, this
            // view model needs to inform the view (for rule-validation purposes) if the
            // gender and height change.
            // [GMazeroff and AKhomenko 5/16/2011]
            _patientObserver = new PropertyObserver<BVAPatient>(_currentPatient);
            _patientObserver.RegisterHandler(p => p.Gender, p => FirePropertyChanged("Weight"));
            _patientObserver.RegisterHandler(p => p.HeightInCm, p => FirePropertyChanged("Weight"));
            _patientObserver.RegisterHandler(p => p.DateOfBirth, p => FirePropertyChanged("DateOfBirth"));

            // Store the height and weight in both measurement systems.
            _heightInEnglish = FormatValue(_currentPatient.HeightInInches, NumDecimalPlacesForInches);
            _heightInMetric = FormatValue(_currentPatient.HeightInCm, NumDecimalPlacesForCentimeters);
            _weightInEnglish = FormatValue(_currentPatient.WeightInLb, NumDecimalPlacesForPounds);
            _weightInMetric = FormatValue(_currentPatient.WeightInKg, NumDecimalPlacesForKilograms);

            if (_currentPatient.DateOfBirth.HasValue)
                _dateOfBirthAsString = ((DateTime)_currentPatient.DateOfBirth).ToString("MM/dd/yyyy");
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the current date of birth.
        /// </summary>
        /// <remarks>
        /// (1) If the given value to set is not a valid date (e.g. "5///3//123"), the current 
        ///     patient's date of birth is set to DomainConstants.InvalidDateOfBirth.
        /// (2) If the given value to set is string.Empty, the patient's date of birth is set
        ///     to null.
        /// </remarks>
        public string DateOfBirth
        {
            get
            {
                return _dateOfBirthAsString;
            }
            set
            {
                DateTime? valueToSet = CreateDateTime(value);
                
                if (valueToSet != null)
                {
                    _dateOfBirthAsString = value;
                    _currentPatient.DateOfBirth = valueToSet;
                }
                else
                {
                    _dateOfBirthAsString = value;
                    if (value == string.Empty)
                        _currentPatient.DateOfBirth = null;
                    else
                        _currentPatient.DateOfBirth = DomainConstants.InvalidDateOfBirth;
                }

               // ASK -- look at property observer !!!! we are notifying WPF binding engine on every change.
               // Also this setup allows us to have rules be enabled/disabled at run time and reflect the UI
               // FirePropertyChanged("DateOfBirth");
            }
        }

        /// <summary>
        /// Gets or sets the current patient's height based on the current measurement system.
        /// </summary>
        /// <remarks>
        /// (1) When getting the height, if a measurement system conversion is required and
        ///     hasn't yet been performed, the height will automatically be converted.
        /// (2) When setting the height:
        ///     (a) Values that aren't valid for the 'double' type will be ignored.
        ///     (b) Only the height value for the current measurement system will be stored
        ///         in this instance.
        ///     (c) The underlying patient's height (in metric) will be updated.
        ///     (d) If the weight is set to zero, the underlying strings for weight values
        ///         will be set to String.Empty
        ///     (e) OnPropertyChanged is called for this property.
        /// </remarks>
        public string Height
        {
            get
            {
                if (MeasurementSystem == MeasurementSystem.Metric)
                {
                    // We don't yet have the height in metric, so make the conversion first.
                    if (_heightInMetric == String.Empty)
                        ConvertHeight();
                    return _heightInMetric;
                }
                
                // We don't yet have the height in English, so make the conversion first.
                if (_heightInEnglish == String.Empty)
                    ConvertHeight();
                return _heightInEnglish;
            }
            set
            {
                double newHeight;
                // Don't allow things that can't be converted to doubles.
                // Empty strings should be treated as zero.
                if ((value != string.Empty) && (!Double.TryParse(value, out newHeight)))
                    return;
                var newValue = value;

                // Setting a new height and it's in centimeters.
                if (MeasurementSystem == MeasurementSystem.Metric)
                {
                    _heightInMetric = newValue;
                    _heightInEnglish = String.Empty;   // Old English value no longer relevant so blank it out.
                    _currentPatient.HeightInCm = newValue != String.Empty ? Convert.ToDouble(newValue) : 0f;
                }
                // Setting a new height and it's in inches.
                else
                {
                    _heightInEnglish = newValue;
                    _heightInMetric = String.Empty;    // Old metric value no longer relevant so blank it out.
                    _currentPatient.HeightInCm = newValue != String.Empty ? UnitsConversionHelper.ConvertHeightToMetric(Convert.ToDouble(newValue)) : 0f;
                }

                FirePropertyChanged("Height");
            }
        }

        /// <summary>
        /// Gets the regular expression defining valid height input for the current
        /// measurement system.
        /// </summary>
        public string HeightInputFormat
        {
            get
            {
                return MeasurementSystem == MeasurementSystem.English ? Constants.EnglishHeightRegEx : Constants.MetricHeightRegEx;
            }
        }

        /// <summary>
        /// Gets or sets the current patient's weight based on the current measurement system.
        /// </summary>
        /// <remarks>
        /// (1) When getting the weight, if a measurement system conversion is required and
        ///     hasn't yet been performed, the height will automatically be converted.
        /// (2) When setting the weight:
        ///     (a) Values that aren't valid for the 'double' type will be ignored.
        ///     (b) Only the weight value for the current measurement system will be stored
        ///         in this instance.
        ///     (c) The underlying patient's weight (in metric) will be updated.
        ///     (d) If the weight is set to zero, the underlying strings for weight values
        ///         will be set to String.Empty
        ///     (e) OnPropertyChanged is called for this property.
        /// </remarks>
        public string Weight
        {
            get
            {
                if (MeasurementSystem == MeasurementSystem.Metric)
                {
                    // We don't yet have the weight in metric, so make the conversion first.
                    if (_weightInMetric == String.Empty)
                        ConvertWeight();
                    return _weightInMetric;
                }
                
                // We don't yet have the weight in English, so make the conversion first.
                if (_weightInEnglish == String.Empty)
                    ConvertWeight();
                return _weightInEnglish;
            }
            set
            {
                double newWeight;

                // Don't allow things that can't be converted to doubles.
                // Empty strings should be treated as zero.
                if ((value != string.Empty) && (!Double.TryParse(value, out newWeight)))
                    return;

                string newValue = value;

                // Setting a new weight and it's in kgs.
                if (MeasurementSystem == MeasurementSystem.Metric)
                {
                    _weightInMetric = newValue;
                    _weightInEnglish = String.Empty;    // Old English value no longer relevant so blank it out.
                    _currentPatient.WeightInKg = newValue != String.Empty ? Convert.ToDouble(newValue) : 0f;
                }
                // Setting a new weight and it's in lbs.
                else
                {
                    _weightInEnglish = newValue;
                    _weightInMetric = String.Empty;     // Old metric value no longer relevant so blank it out.
                    _currentPatient.WeightInKg = newValue != String.Empty ? UnitsConversionHelper.ConvertWeightToMetric(Convert.ToDouble(newValue)) : 0f;
                }

                FirePropertyChanged("Weight");
            }
        }

        /// <summary>
        /// Gets the regular expression defining valid weight input for the current
        /// measurement system.
        /// </summary>
        public string WeightInputFormat
        {
            get
            {
                return MeasurementSystem == MeasurementSystem.English ? Constants.EnglishWeightRegEx : Constants.MetricWeightRegEx;
            }
        }

        /// <summary>
        /// Gets or sets the current measurement system (i.e., metric or English).
        /// </summary>
        /// <remarks>
        /// When setting the measurement system:
        ///   (1) The underlying measurement system for the test will only change if the value to be
        ///       set differs from the current measurement system.
        ///   (2) OnPropertyChanged is called for Height, Weight, MeasurementSystem, HeightInputFormat,
        ///       and WeightInputFormat.
        /// </remarks>
        public MeasurementSystem MeasurementSystem
        {
            get
            {
                return _currentPatient.MeasurementSystem;
            }
            set
            {
                if (_currentPatient.MeasurementSystem == value)
                    return;

                _currentPatient.MeasurementSystem = value;

                HandleHeightAndWeightAtExtremes(value);

                FirePropertyChanged("Height", "Weight", "MeasurementSystem", "HeightInputFormat", "WeightInputFormat");
            }
        }

        private void HandleHeightAndWeightAtExtremes(MeasurementSystem value)
        {
            if (value == MeasurementSystem.English)
            {
                if (_weightInMetric == BvaDomainConstants.MaximumWeightInKilograms)
                {
                    _weightInEnglish = BvaDomainConstants.MaximumWeightInPounds;
                }
                if (_weightInMetric == BvaDomainConstants.MinimumWeightInKilograms)
                {
                    _weightInEnglish = BvaDomainConstants.MinimumWeightInPounds;
                }
                if (_heightInMetric == BvaDomainConstants.MaximumHeightInCentimeters)
                {
                    _heightInEnglish = BvaDomainConstants.MaximumHeightInInches;
                }
                else if (_heightInMetric == BvaDomainConstants.MinimumHeightInCentimeters)
                {
                    _heightInEnglish = BvaDomainConstants.MinimumHeightInInches;
                }
            }
            else
            {
                if (_weightInEnglish == BvaDomainConstants.MaximumWeightInPounds)
                {
                    _weightInMetric = BvaDomainConstants.MaximumWeightInKilograms;
                }
                else if (_weightInEnglish == BvaDomainConstants.MinimumWeightInPounds)
                {
                    _weightInMetric = BvaDomainConstants.MinimumWeightInKilograms;
                }

                if (_heightInEnglish == BvaDomainConstants.MaximumHeightInInches)
                {
                    _heightInMetric = BvaDomainConstants.MaximumHeightInCentimeters;
                }
                else if (_heightInEnglish == BvaDomainConstants.MinimumHeightInInches)
                {
                    _heightInMetric = BvaDomainConstants.MinimumHeightInCentimeters;
                }
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Performs a unit conversion for the height value (backing field) if it is
        /// not already defined. If no height value is defined for either metric or English,
        /// no conversion is performed.
        /// </summary>
        private void ConvertHeight()
        {
            // One of the heights must be empty, otherwise there's (1) no value to convert, or
            // (2) the conversion has already been performed.
            if (!((_heightInMetric == String.Empty) ^ (_heightInEnglish == String.Empty)))   // Yes, that's an exclusive-nor. :-)
            {
                return;
            }

            // We have the English version, so convert it to metric.
            if (_heightInMetric == String.Empty)
            {
                if (_heightInEnglish == BvaDomainConstants.MaximumHeightInInches)
                {
                    _heightInMetric = BvaDomainConstants.MaximumHeightInCentimeters;
                }
                else if (_heightInEnglish == BvaDomainConstants.MinimumHeightInInches)
                {
                    _heightInMetric = BvaDomainConstants.MinimumHeightInCentimeters;
                }
                else
                {
                    double usHeight;
                    if (Double.TryParse(_heightInEnglish, out usHeight))
                    {
                        _heightInMetric = FormatValue(UnitsConversionHelper.ConvertHeightToMetric(usHeight), NumDecimalPlacesForCentimeters);
                    }
                }
            }
            // We have the metric version, so convert it to English.
            else
            {
                if (_heightInMetric == BvaDomainConstants.MaximumHeightInCentimeters)
                {
                    _heightInEnglish = BvaDomainConstants.MaximumHeightInInches;
                }
                else if (_heightInMetric == BvaDomainConstants.MinimumHeightInCentimeters)
                {
                    _heightInEnglish = BvaDomainConstants.MinimumHeightInInches;
                }
                else
                {
                    double metricHeight;
                    if (Double.TryParse(_heightInMetric, out metricHeight))
                    {
                        _heightInEnglish = FormatValue(UnitsConversionHelper.ConvertHeightToEnglish(metricHeight), NumDecimalPlacesForInches);
                    }
                }
            }
        }

        /// <summary>
        /// Performs a unit conversion for the weight value (backing field) if it is
        /// not already defined. If no weight value is defined for either metric or English,
        /// no conversion is performed.
        /// </summary>
        private void ConvertWeight()
        {
            // One of the weights must be empty, otherwise there's (1) nothing value convert, or
            // (2) the conversion has already been performed.
            if (!((_weightInMetric == String.Empty) ^ (_weightInEnglish == String.Empty)))
            {
                return;
            }

            // We have the US version, so convert it to metric.
            if (_weightInMetric == String.Empty)
            {
                if (_weightInEnglish == BvaDomainConstants.MaximumWeightInPounds)
                {
                    _weightInMetric = BvaDomainConstants.MaximumWeightInKilograms;
                }
                else if (_weightInEnglish == BvaDomainConstants.MinimumWeightInPounds)
                {
                    _weightInMetric = BvaDomainConstants.MinimumWeightInKilograms;
                }
                else
                {
                    double usWeight;
                    Double.TryParse(_weightInEnglish, out usWeight);
                    _weightInMetric = FormatValue(UnitsConversionHelper.ConvertWeightToMetric(usWeight), NumDecimalPlacesForKilograms);
                }
            }
            // We have the metric version, so convert it to English.
            else
            {
                if (_weightInMetric == BvaDomainConstants.MaximumWeightInKilograms)
                {
                    _weightInEnglish = BvaDomainConstants.MaximumWeightInPounds;
                }
                if (_weightInMetric == BvaDomainConstants.MinimumWeightInKilograms)
                {
                    _weightInEnglish = BvaDomainConstants.MinimumWeightInPounds;
                }
                else
                {
                    double metricWeight;
                    Double.TryParse(_weightInMetric, out metricWeight);
                    _weightInEnglish = FormatValue(UnitsConversionHelper.ConvertWeightToEnglish(metricWeight), NumDecimalPlacesForPounds);
                }
            }
        }

        private static string FormatValue(double value, int numDecimalPlaces)
        {
            var emptyResult = "0.";
            for (var i = 0; i < numDecimalPlaces; i++)
                emptyResult += "0";

            var result = value.RoundAndFormat(numDecimalPlaces);
            return result == emptyResult ? "" : result;
        }

        /// <summary>
        /// Attempts to create a DateTime instance based on a specified 'm(m)/d(d)/yy(yy)'
        /// string.
        /// </summary>
        /// <param name="date">String to convert to a DateTime</param>
        /// <returns>DateTime instance representing the specified date string; null if
        /// the string cannot be converted to a valid DateTime</returns>
        /// <remarks>
        /// If only two digits are specified for the year portion, the DateTime returned
        /// will be the century before today's date if using the current century would
        /// put that date in the future. For example if today is 6/15/2011 and "6/16/11" 
        /// is specified, the returned date will be 6/16/1911.
        /// </remarks>
        private DateTime? CreateDateTime(string date)
        {
            var tokens = date.Split('/');
            int month;
            int day;
            int year;

            // Need at least month, day, and year separated by slashes to create a date.
            if (tokens.Length != 3)
                return null;

            // There's a month, but it's an empty string or not a valid number.
            if ((tokens[0] == string.Empty) || !int.TryParse(tokens[0], out month))
                return null;

            // There's a numeric month, but it's not valid for a month.
            if ((month < 1) || (month > 12))
                return null;

            // There's a day, but it's an empty string or not a valid number.
            if ((tokens[1] == string.Empty) || !int.TryParse(tokens[1], out day))
                return null;

            // There's a numeric day, but it's not valid for a day.
            if ((day < 1) || (day > 31))
                return null;

            // There's a year, but it's an empty string or not a valid number.
            if ((tokens[2] == string.Empty) || !int.TryParse(tokens[2], out year))
                return null;

            // Single- and three-digit years aren't acceptable.
            if ((tokens[2].Length != 2) && (tokens[2].Length != 4))
                return null;

            // If it's a two digit year, give it the full four digits such that the
            // date will be in the past.
            if (year <= 99)
            {
                var todayYearPrefix = (int)Math.Floor(DateTime.Today.Year / 100.0f);
                DateTime? testDate;
                try
                {
                    testDate = new DateTime(todayYearPrefix * 100 + year, month, day);
                }
                catch
                {
                    testDate = null;
                }

                // Date couldn't be created.
                if (testDate == null)
                    return null;
                
                if (testDate > DateTime.Today)
                    todayYearPrefix--;
                year += todayYearPrefix * 100;
            }

            // Try and create the DateTime. (This part catches Novmber 31, dates not correct for
            // leap years, etc.)
            DateTime? dateTimeToReturn;
            try
            {
                dateTimeToReturn = new DateTime(year, month, day);
            }
            catch
            {
                dateTimeToReturn = null;
            }

            return dateTimeToReturn;
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Formats the height value when the focus on the Height textbox in the UI is lost.
        /// </summary>
        /// <remarks>Invoked by an interaction trigger in the PatientDemographicsView XAML.</remarks>
        public void OnHeightFocusLost()
        {
            if (_currentPatient.MeasurementSystem == MeasurementSystem.English)
            {
                if (Height != BvaDomainConstants.MaximumHeightInInches && Height != BvaDomainConstants.MinimumHeightInInches)
                    Height = FormatValue(_currentPatient.HeightInInches, NumDecimalPlacesForInches);
            }
            else
            {
                if (Height != BvaDomainConstants.MaximumHeightInCentimeters && Height != BvaDomainConstants.MinimumHeightInCentimeters)
                    Height = FormatValue(_currentPatient.HeightInCm, NumDecimalPlacesForCentimeters);
            }
        }

        /// <summary>
        /// Formats the weight value when the focus on the Height textbox in the UI is lost.
        /// </summary>
        /// <remarks>Invoked by an interaction trigger in the PatientDemographicsView XAML.</remarks>
        public void OnWeightFocusLost()
        {
            if (_currentPatient.MeasurementSystem == MeasurementSystem.English)
            {
                if (Weight != BvaDomainConstants.MaximumWeightInPounds && Weight != BvaDomainConstants.MinimumWeightInPounds)
                    Weight = FormatValue(_currentPatient.WeightInLb, NumDecimalPlacesForPounds);
            }
            else
            {
                if (Weight != BvaDomainConstants.MaximumWeightInKilograms && Weight != BvaDomainConstants.MinimumWeightInKilograms)
                    Weight = FormatValue(_currentPatient.WeightInKg, NumDecimalPlacesForKilograms);
            }
        }

        public void OnDateOfBirthFocusLost()
        {
            var valueToSet = CreateDateTime(DateOfBirth);

            if (valueToSet != null)
            {
                _dateOfBirthAsString = ((DateTime)valueToSet).ToString("MM/dd/yyyy");
                FirePropertyChanged("DateOfBirth");
            }
        }

        #endregion

        #region IDataErrorInfo Members

        /// <summary>
        /// Gets an error message indicating what is wrong with this object.
        /// </summary>
        /// <remarks>Not used; returns string.Empty</remarks>
        public string Error
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Gets the error message for the property with the given name.
        /// </summary>
        /// <param name="columnName">Property on this view model that has an error</param>
        /// <returns>The error of the underlying BVAPatient for the Height and Weight fields;
        /// null if the given name has no errors</returns>
        public string this[string columnName]
        {
            get
            {
                if (columnName == "Height")
                    return _currentPatient["HeightInCurrentMeasurementSystem"];
                if (columnName == "Weight")
                    return _currentPatient["WeightInCurrentMeasurementSystem"];
                if (columnName == "DateOfBirth")
                    return _currentPatient["DateOfBirth"];

                return null;
            }
        }

        #endregion
    }
}
