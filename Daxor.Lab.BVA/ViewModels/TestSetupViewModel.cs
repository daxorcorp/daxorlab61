﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Daxor.Lab.BVA.Common;
using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.BVA.ViewModels
{
	/// <summary>
	/// The test setup view model.
	/// </summary>
	public class TestSetupViewModel : ViewModelBase
	{
		#region Constants

		public const string ClearKeysAndSaveChoice = "Clear Keys and Save";
		public const string CancelChoice = "Cancel";
		public const string SaveChoice = "Save";
		public const string DiscardChoice = "Discard";
		public const string DoYouWantToSaveMessage = "Do you want to save your changes?";
		public const string BvaTestCreationHeader = "BVA Test Creation";
		public const string EnteredKeysFormatString = "You have entered injectate/standard key(s). " +
													  "If you exit this section, you will lose this data" +
													  "\nas well as the injectate lot ({0}).\n\nDo you want to continue?";

		#endregion

		#region Fields

		private readonly IBloodVolumeTestController _controller;
		private readonly IAppModuleNavigator _localNavigator;
		private readonly IMessageBoxDispatcher _messageBoxDispatcher;
		private readonly ObservableCollection<TabItemViewModel> _testSetupTabItems = new ObservableCollection<TabItemViewModel>();
		private readonly TestAnalysisViewModel _analysisViewModel;
		private readonly IPatientSpecificFieldCache _patientSpecificFieldCache;
		private readonly IMessageManager _messageManager;
		private TaskScheduler _taskScheduler;
		private bool _useIdealDispatcher = true;

		#endregion

		#region Properties
		public ObservableCollection<TabItemViewModel> TestSetupTabItems
		{
			get { return _testSetupTabItems; }
		}
		public TestAnalysisViewModel TestAnalysisViewModel
		{
			get { return _analysisViewModel; }
		}

		public TaskScheduler TaskScheduler
		{
			get
			{
				return _taskScheduler ?? (_taskScheduler = TaskScheduler.Default);
			}
			set
			{
				_taskScheduler = value;
				_useIdealDispatcher = false;
			}
		}

		#endregion

		#region Ctor

		public TestSetupViewModel([Dependency(AppModuleIds.BloodVolumeAnalysis)] IAppModuleNavigator localNavigator, IMessageBoxDispatcher messageBoxDispatcher,
								   IBloodVolumeTestController controller, TestAnalysisViewModel analysisVm, IPatientSpecificFieldCache patientSpecificFieldCache,
								   IMessageManager messageManager)
		{
			_controller = controller;
			_localNavigator = localNavigator;
			_analysisViewModel = analysisVm;
			_messageBoxDispatcher = messageBoxDispatcher;
			_patientSpecificFieldCache = patientSpecificFieldCache;
			_messageManager = messageManager;

			NavigateToTestSelection = new DelegateCommand<object>(ExecuteNavigateToTestSelection);
			NavigateToTestReview = new DelegateCommand<object>(ExecuteNavigateToTestReview);

			//Listens for AddTestSetupTabItems and modelChanged aggregate events 
			_controller.EventAggregator.GetEvent<TestSetupTabItemAdded>().Subscribe(OnAddTestSetupTabItem, false);
		}

		#endregion

		#region Helpers

		private void OnAddTestSetupTabItem(TestSetupTabItemPayload tabPayload)
		{
			//Check if tabPayload exists
			TabItemViewModel tabVm = (from tab in TestSetupTabItems
									  where tab.UniqueID == tabPayload.UniqueID
									  select tab).FirstOrDefault();

			//Existing tab, update fields
			if (tabVm != null)
			{
				//currentIndex - zero based
				int index = TestSetupTabItems.IndexOf(tabVm);
				if (index != tabPayload.TabPlacementIndex)
				{
					//Remove and re-insert
					TestSetupTabItems.RemoveAt(index);
					TestSetupTabItems.Insert(tabPayload.TabPlacementIndex, tabVm);
				}
			}
			else
			{
				tabVm = new TabItemViewModel(tabPayload.UniqueID);
				TestSetupTabItems.Insert(tabPayload.TabPlacementIndex, tabVm);
			}

			//Update tab values
			tabVm.Header = tabPayload.Header;
			tabVm.TabNavigationCommand = tabPayload.NavigationCommand;
		}

		/// <summary>
		/// Checks to see if any tests have been ran on the patient with internalId.
		/// If no other tests have been run, it will remove the patient from the database.
		/// </summary>
		/// <param name="internalId"></param>
		private void RemoveAnyOrphanedPatients(Guid internalId)
		{
			if (!_controller.DataService.AreThereAnyTestsThatRanOnPatient(internalId))
				_controller.DataService.DeletePatient(internalId);
		}

		/// <summary>
		/// Attempts to save the current test, then cleans up any orphaned patients.
		/// </summary>
		/// <returns>True, or throws an exception</returns>
		protected virtual void SaveTestAndRemoveOrphanedPatients()
		{
			var testPatientInternalId = Guid.Empty;
			var selectedTest = _controller.DataService.SelectTest(_controller.SelectedTest.InternalId);

			if (selectedTest != null)
				testPatientInternalId = selectedTest.Patient.InternalId;

			_controller.DataService.SaveTest(_controller.SelectedTest);
			if (testPatientInternalId != _controller.SelectedTest.Patient.InternalId)
				RemoveAnyOrphanedPatients(testPatientInternalId);
		}

		private void SetInvalidDateOfBirthToNullIfNotValid()
		{
			if (_controller.SelectedTest.Patient.DateOfBirth.HasValue &&
				(_controller.SelectedTest.Patient.DateOfBirth == DomainConstants.InvalidDateOfBirth))
				_controller.SelectedTest.Patient.DateOfBirth = null;
		}

		protected virtual void SaveTestOnBackgroundThread()
		{
			var saveBvaTestTask = new Task(SaveTask);
			saveBvaTestTask.ContinueWith(result => OnSaveTaskCompleted(), TaskScheduler);
			saveBvaTestTask.Start(TaskScheduler);
		}

		private void OnSaveTaskCompleted()
		{
			if (_useIdealDispatcher)
				IdealDispatcher.BeginInvoke(() => _localNavigator.ActivateView(BVAModuleViewKeys.TestSelection));
			else
				_localNavigator.ActivateView(BVAModuleViewKeys.TestSelection);
			_controller.EventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
		}

		protected virtual void SaveTask()
		{
			try
			{
				SaveTestAndRemoveOrphanedPatients();
			}
			// ReSharper disable once UnusedVariable
			catch
			{
				var message = _messageManager.GetMessage(MessageKeys.BvaSaveTestFailed).FormattedMessage;

				if (_useIdealDispatcher)
					IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, message, BvaTestCreationHeader,
								MessageBoxChoiceSet.Ok));
				else
					_messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, message, BvaTestCreationHeader,
						MessageBoxChoiceSet.Ok);

				_controller.Logger.Log(message, Category.Exception, Priority.High);
			}
		}

		public virtual bool InjectateKeysHaveBeenEntered()
		{
			return !string.IsNullOrEmpty(_controller.SelectedTest.InjectateKey) ||
				   !string.IsNullOrEmpty(_controller.SelectedTest.StandardAKey) ||
				   !string.IsNullOrEmpty(_controller.SelectedTest.StandardBKey);
		}

		protected virtual void CloseKeyboard()
		{
			VirtualKeyboardManager.CloseActiveKeyboard();
		}

		private void CloseKeyboardAndNavigateToTestSelection()
		{
			CloseKeyboard();
			_localNavigator.ActivateView(BVAModuleViewKeys.TestSelection);
		}

		#endregion

		#region Commands

		public DelegateCommand<object> NavigateToTestSelection
		{
			get;
			private set;
		}

		protected virtual void ExecuteNavigateToTestSelection(object arg)
		{
			_controller.Logger.Log("TEST LIST BUTTON CLICKED", Category.Info, Priority.None);
			if (string.IsNullOrEmpty(_controller.SelectedTest.Patient.HospitalPatientId))
			{
				CloseKeyboardAndNavigateToTestSelection();
				return;
			}

			if (InjectateKeysHaveBeenEntered())
			{
				var choices = new[] {ClearKeysAndSaveChoice, CancelChoice};
				var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Question,
					String.Format(EnteredKeysFormatString, _controller.SelectedTest.InjectateLot),
					DisplayName,
					choices);

				if (choices[result] != ClearKeysAndSaveChoice) return;

				if (_controller.SelectedTest.Mode == TestMode.Manual)
					_controller.SelectedTest.Status = TestStatus.Completed;

				_controller.EventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true));
				_controller.SelectedTest.ClearBarcodes();

				CloseKeyboard();
				SaveTestOnBackgroundThread();
			}
			else
			{
				var currentPatient = _controller.SelectedTest.Patient;
				var existingPatient = _controller.DataService.SelectPatient(currentPatient.HospitalPatientId,
					_controller.SelectedTest.ConcurrencyObject);
				_patientSpecificFieldCache.RestoreOrOverrideBasedOnUserChoice(existingPatient, currentPatient);

				if (_controller.SelectedTest.Status == TestStatus.Pending)
				{
					var choices = new[] {SaveChoice, DiscardChoice, CancelChoice};
					var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Question,
						DoYouWantToSaveMessage,
						BvaTestCreationHeader, choices);

					if (choices[result] == CancelChoice)
						return;

					if (choices[result] == DiscardChoice)
					{
						SetInvalidDateOfBirthToNullIfNotValid();
						CloseKeyboardAndNavigateToTestSelection();
						return;
					}
				}
				SetInvalidDateOfBirthToNullIfNotValid();
				if (_controller.SelectedTest.Mode == TestMode.Manual)
					_controller.SelectedTest.Status = TestStatus.Completed;

				_controller.EventAggregator.GetEvent<BusyStateChanged>()
					.Publish(new BusyPayload(true, Constants.SavingTestMessage));
				CloseKeyboard();
				SaveTestOnBackgroundThread();
			}
		}

		public DelegateCommand<object> NavigateToTestReview
		{
			get;
			private set;
		}

		private void ExecuteNavigateToTestReview(object arg)
		{
			VirtualKeyboardManager.CloseActiveKeyboard();
			_controller.Logger.Log("TEST REVIEW BUTTON CLICKED", Category.Info, Priority.None);
			_localNavigator.ActivateView(BVAModuleViewKeys.TestReview, null);
		}

		#endregion
	}
}
