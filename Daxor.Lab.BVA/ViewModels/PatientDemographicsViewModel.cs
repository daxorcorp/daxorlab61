﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using Daxor.Lab.BVA.Common;
using Daxor.Lab.BVA.Common.Events;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.Database.Utility;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Domain.Services;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.RuleFramework.Extensions;
using Daxor.Lab.Infrastructure.SubViews;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.BVA.ViewModels
{
    public class PatientDemographicsViewModel : ViewModelBase, IActiveAware, IDataErrorInfo
    {
        #region Fields

        private readonly IBloodVolumeTestController _bvTestController;
        private readonly IAppModuleNavigator _localNavigator;
        private readonly IEventAggregator _eventAggregator;
        private readonly IIdealsCalcEngineService _idealsCalcEngineService;
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly ILoggerFacade _logger;
        private readonly ISettingsManager _settingsManager;
        private readonly ITestExecutionController _testExecutionController;
        private SettingObserver<ISettingsManager> _settingObserver;
        private DelegateCommand<Object> _navigateToTestParametersCommand;
        private BVAPatient _currentPatient;
        private BVATest _currentTest;
        private PatientViewModel _patientViewModel;
        private bool _isPatientIdFieldFocused;
        private bool _isIdealDeviationHigh;
        private bool _isPacsEnabled;
        private bool _isActive;
        private bool _isPatientSpecificDataLocked;
        private double _idealDeviationMaximumPercentage = 380f;
        private LabelFormatter _id2Label;
        private string _hospitalPatientId;
        private HospitalPatientIdItem _selectedHospitalPatientIdItem;
        private PropertyObserver<BVAPatient> _patientPropertyObserver;
        private PropertyObserver<BVATest> _testPropertyObserver;
        private ThreadSafeObservableCollection<HospitalPatientIdItem> _hospitalPatientIdItemList;
        private readonly IPatientSpecificFieldCache _patientSpecificFieldCache;
        private readonly IMessageManager _messageManager;

        /// <summary>
        /// Indicates whether or not the user has requested that the measurement system
        /// change (i.e., metric to English or vice versa).
        /// </summary>
        /// <remarks>This is used by the code-behind to help place the caret in the
        /// correct location when switching measurement systems.</remarks>
        public bool IsMeasurementSystemChanging;

        #endregion

        #region Ctor

        public PatientDemographicsViewModel(IBloodVolumeTestController bvTestController,
                                            [Dependency(AppModuleIds.BloodVolumeAnalysis)] IAppModuleNavigator localNavigator,
                                            [Dependency(AppModuleIds.BloodVolumeAnalysis)] IRuleEngine ruleEngine,
                                            IMessageBoxDispatcher messageBoxDispatcher, ITestExecutionController testExecutionController,
                                            IIdealsCalcEngineService idealsCalcEngineService, IPatientSpecificFieldCache patientSpecificFieldCache,
                                            ISettingsManager settingsManager, IMessageManager messageManager)
        {
            _bvTestController = bvTestController;
            _localNavigator = localNavigator;
            _messageBoxDispatcher = messageBoxDispatcher;
            _testExecutionController = testExecutionController;
            _idealsCalcEngineService = idealsCalcEngineService;
            _eventAggregator = bvTestController.EventAggregator;
            _logger = bvTestController.Logger;
            _settingsManager = settingsManager;
            _patientSpecificFieldCache = patientSpecificFieldCache;
            _messageManager = messageManager;

            RuleEngine = ruleEngine;

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InitializeViewModel();
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Properties

        private IRuleEngine RuleEngine { get; set; }

        private SettingObserver<ISettingsManager> SettingObserver
        {
            get { return _settingObserver; }
            set
            {
                // Only get one assignment
                if (_settingObserver == null)
                    _settingObserver = value;
            }
        }

        protected DelegateCommand<Object> NavigateToTestParametersDelegateCommand
        {
            get { return _navigateToTestParametersCommand; }
            set
            {
                // Only get one assignment
                if (_navigateToTestParametersCommand == null)
                    _navigateToTestParametersCommand = value;
            }
        }

        /// <summary>
        /// Gets or sets whether or not patient-specific data fields should be
        /// locked (i.e., disabled in the UI to prevent editing).
        /// </summary>
        public bool IsPatientSpecificDataLocked
        {
            get { return _isPatientSpecificDataLocked; }
            set
            {
                if (_isPatientSpecificDataLocked == value)
                    return;
                _isPatientSpecificDataLocked = value;
                FirePropertyChanged("IsPatientSpecificDataLocked");
            }
        }

        /// <summary>
        /// Gets or sets the hospital patient ID item (i.e., GUID/string pair) for the
        /// patient associated with the current test. Setting this property also sets
        /// the HospitalPatientId property (for the UI).
        /// </summary>
        /// <remarks>If there's another executing BVA test whose patient matches this test, 
        /// any changes and lock fields as appropriate.</remarks>
        public HospitalPatientIdItem SelectedHospitalPatientIdItem
        {
            get { return _selectedHospitalPatientIdItem; }
            set
            {
                if (_selectedHospitalPatientIdItem != null && _selectedHospitalPatientIdItem.Equals(value)) 
                    return;

                _selectedHospitalPatientIdItem = value;
                FirePropertyChanged("SelectedHospitalPatientIdItem");

                if (_selectedHospitalPatientIdItem == null || CurrentTest == null || CurrentTest.Patient == null)
                    return;

                HospitalPatientId = _selectedHospitalPatientIdItem.HospitalPatientId;
                
                // If there's another executing BVA test whose patient matches /this/ test, prevent
                // any changes and lock fields as appropriate.
                IsPatientSpecificDataLocked = false;
                if (IsThereATestExecutingWithTheSameHospitalPatientId())
                {
                    var lockedPatientFieldsAlert = _messageManager.GetMessage(MessageKeys.BvaLockedPatientFields);
                    var message = lockedPatientFieldsAlert.Description + " [Alert: " + lockedPatientFieldsAlert.MessageId + "]";
                    _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning, message, "BVA Test", MessageBoxChoiceSet.Close);
                    _logger.Log(message, Category.Info, Priority.Low);
                    IsPatientSpecificDataLocked = true;
                }
            }
        }

        /// <summary>
        /// Gets or sets the hospital patient ID (i.e., ID assigned to the patient by the hospital).
        /// </summary>
        /// <remarks>The setter populates the pick list.</remarks>
        public string HospitalPatientId
        {
            get { return _hospitalPatientId; }
            set
            {
                if (!SetValue(ref _hospitalPatientId, "HospitalPatientId", value)) return;

                if (HospitalPatientIdItemList.Count > 0)
                    HospitalPatientIdItemList.Clear();

                if (String.IsNullOrWhiteSpace(_hospitalPatientId)) return;

                var items = _bvTestController.DataService.FindHospitalPatientIdItems(value);
                HospitalPatientIdItemList.AddRange(items);
            }
        }

        /// <summary>
        /// Gets or sets the list of hospital patient ID items (i.e., GUID/string pairs) for
        /// the pick list.
        /// </summary>
        public ThreadSafeObservableCollection<HospitalPatientIdItem> HospitalPatientIdItemList
        {
            get { return _hospitalPatientIdItemList; }
            protected set { SetValue(ref _hospitalPatientIdItemList, "HospitalPatientIdItemList", value); }
        }

        public PatientViewModel PatientViewModel
        {
            get { return _patientViewModel; }
            set { SetValue(ref _patientViewModel, "PatientViewModel", value); }
        }

        /// <summary>
        /// Gets or sets the current patient instance
        /// </summary>
        /// <remarks>
        /// Setting the patient...
        /// (1) triggers a recalculation of weight deviation
        /// (2) re-evaluates the NavigateToTestParameters command
        /// (3) re-evaluates whether patient-specific fields should be locked because
        /// a running test is associated with the same patient
        /// (4) Instantiates a new patient view model for the given patient
        /// </remarks>
        public virtual BVAPatient CurrentPatient
        {
            get { return _currentPatient; }
            set
            {
                if (_currentPatient == value) 
                    return;

                _currentPatient = value;

                // A new patient model requires a new patient view model.
                PatientViewModel = new PatientViewModel(_currentPatient);

                if (_currentPatient != null)
                {
                    SetUpPatientPropertyObserver();
                    SetHospitalPatientIdBasedOnCurrentTest();
                }

                // Load the cache with the new patient (only if the cache is uninitialized).
                // Note: The test selection view model marks the cache as uninitialized, so
                // if this view model is becoming active from that pathway, the following will
                // initialize the cache with the selected patient. Otherwise, the HospitalPatientIdEndEdit()
                // manages the cache.
                _patientSpecificFieldCache.InitializePatientSpecificFieldCache(_currentPatient);
                _patientSpecificFieldCache.IsNewPatient = false;

                CalculateWeightDeviation();
                NavigateToTestParametersDelegateCommand.RaiseCanExecuteChanged();
                IsPatientSpecificDataLocked = IsThereATestExecutingWithTheSameHospitalPatientId();

                FirePropertyChanged("CurrentPatient");
            }
        }

        public virtual BVATest CurrentTest
        {
            get { return _currentTest; }
            set
            {
                if (!SetValue(ref _currentTest, "CurrentTest", value)) return;
                
                SetCurrentPatientBasedOnTest(_currentTest);
                FirePropertyChanged("CurrentTest");

                RuleEngine.InjectRuleDependency(_currentTest);

                if (_currentTest != null)
                    InitializeTestPropertyObserver(_currentTest);
            }
        }

        public bool IsPatientIdFieldFocused
        {
            get { return _isPatientIdFieldFocused; }
            set
            {
                if (_isPatientIdFieldFocused == value) return;

                // Patient ID is losing focus
                if (!value && _isPatientIdFieldFocused)
                    InternalUpdateSelectedHospitalPatientIdItem(HospitalPatientId);

                _isPatientIdFieldFocused = value;
                FirePropertyChanged("IsPatientIdFieldFocused");
            }
        }

        public bool IsIdealDeviationHigh
        {
            get { return _isIdealDeviationHigh; }
            set
            {
                if (_isIdealDeviationHigh == value)
                    return;
                _isIdealDeviationHigh = value;
                FirePropertyChanged("IsIdealDeviationHigh");
            }
        }

        public bool IsPacsEnabled
        {
            get { return _isPacsEnabled; }
            set { SetValue(ref _isPacsEnabled, "IsPacsEnabled", value); }
        }

        public double IdealDeviationMaximumPercentage
        {
            get { return _idealDeviationMaximumPercentage; }
            set
            {
                if (Math.Abs(_idealDeviationMaximumPercentage - value) < 1E-300)
                    return;

                _idealDeviationMaximumPercentage = value;
                FirePropertyChanged("IdealDeviationMaximumPercentage");
            }
        }

        public LabelFormatter Id2Label
        {
            get { return _id2Label; }
            private set { SetValue(ref _id2Label, "Id2Label", value); }
        }

        #endregion

        #region Commands

        public ICommand NavigateToTestParametersCommand
        {
            get { return NavigateToTestParametersDelegateCommand; }
        }

        void NavigateToTestParametersCommandExecute(object arg)
        {
            _logger.Log("NAVIGATE TO TEST PARAMETERS BUTTON CLICKED", Category.Info, Priority.None);
            _localNavigator.ActivateView(BVAModuleViewKeys.TestParameters);
        }

        bool NavigateToTestParametersCommandCanExecute(object o)
        {
            return CurrentPatient != null && !String.IsNullOrWhiteSpace(CurrentPatient.HospitalPatientId);
        }

        void ExecuteNavigateToMyself(object arg)
        {
            _logger.Log("NAVIGATE TO PATIENT DEMOGRAPHICS (SELF) TAB CLICKED", Category.Info, Priority.None);
            _localNavigator.ActivateView(BVAModuleViewKeys.PatientDemographics);
        }

        #endregion

        #region IActiveAware Members

        public event EventHandler IsActiveChanged = delegate { };

        /// <summary>
        /// Gets or sets whether the patient demographics view is active.
        /// </summary>
        /// <remarks>
        /// When being activated, rule evaluation will take place. When being deactivated
        /// the current patient will be checked against the cached version for changes. If
        /// there are changes, the user will be prompted regarding how to proceed.
        /// </remarks>
        public bool IsActive
        {
            get { return _isActive; }
            set
            {
                _isActive = value;
                
                // Arriving
                if (value)
                {
                    FirePropertyChanged("HospitalPatientId");
                    ForceRuleEvaluation();
                    _logger.Log("PatientDemographicViewModel: PatientDemographicsViewModel has been activated", Category.Debug, Priority.Low);
                }
                else
                {
                    var existingPatient = _bvTestController.DataService.SelectPatient(CurrentPatient.HospitalPatientId, _bvTestController.SelectedTest.ConcurrencyObject);
                    _patientSpecificFieldCache.RestoreOrOverrideBasedOnUserChoice(existingPatient, CurrentPatient);
                }
                _logger.Log("PatientDemographicsViewModel: PatientDemographicsViewModel active: { " + _isActive + "} ForcePatientIDToHaveFocused=" + IsPatientIdFieldFocused,
                    Category.Debug, Priority.Low);
            }
        }

        #endregion

        #region Protected/private helpers

        protected virtual void InitializeViewModel()
        {
            SubscribeToBvaTestSelectedEvent();

            SettingObserver = new SettingObserver<ISettingsManager>(_settingsManager);
            NavigateToTestParametersDelegateCommand = new DelegateCommand<object>(NavigateToTestParametersCommandExecute, NavigateToTestParametersCommandCanExecute);
            
            //Happens on UI Thread
            HospitalPatientIdItemList = new ThreadSafeObservableCollection<HospitalPatientIdItem>();
            _id2Label = new LabelFormatter(_settingsManager.GetSetting<String>(SettingKeys.BvaTestId2Label));

            //Satisfying rules dependency that relies on PatientDemographicsViewModel
            ConfigureViewModel();
        }

        protected void SubscribeToBvaTestSelectedEvent()
        {
            _eventAggregator.GetEvent<BvaTestSelected>().Subscribe(OnBvaTestSelectedEvent, ThreadOption.PublisherThread, false);
        }

        protected virtual void SetCurrentPatientBasedOnTest(BVATest test)
        {
            CurrentPatient = test == null ? null : test.Patient;
        }

        protected virtual void InitializeTestPropertyObserver(BVATest test)
        {
            _testPropertyObserver = new PropertyObserver<BVATest>(test);
            _testPropertyObserver.RegisterHandler(t => t.Patient, OnTestPatientChanged);
        }

        protected virtual void SetUpPatientPropertyObserver()
        {
            _patientPropertyObserver = new PropertyObserver<BVAPatient>(_currentPatient);

            // Weight deviation is dependent on three variables: height, weight, and gender.
            //
            // NOTE: Validation must be forced to be re-evaluated for height and gender because
            // the rule for weight deviation is associated in the UI with the *weight* field.
            // See Daxor.Lab.BVA.Services.BVARuleEngine.
            // [GMazeroff and AKhomenko 5/16/2011]
            _patientPropertyObserver.RegisterHandler(p => p.HeightInCm, p =>
            {
                CalculateWeightDeviation();
                CurrentTest.Patient.EvaluatePropertyByName("WeightInCurrentMeasurementSystem");
            });
            _patientPropertyObserver.RegisterHandler(p => p.WeightInKg, p => CalculateWeightDeviation());
            _patientPropertyObserver.RegisterHandler(p => p.Gender, p =>
            {
                CalculateWeightDeviation();
                CurrentTest.Patient.EvaluatePropertyByName("WeightInCurrentMeasurementSystem");
            });

            _patientPropertyObserver.RegisterHandler(p => p.HospitalPatientId, p => OnHospitalPatientIdChanged());
            _patientPropertyObserver.RegisterHandler(p => p.MeasurementSystem, PatientMeasurementSystemChanged);
        }

        void ConfigureViewModel()
        {
            IsPacsEnabled = _settingsManager.GetSetting<Boolean>(SettingKeys.BvaPacsEnabled);

            SettingObserver.RegisterHandler(SettingKeys.BvaPacsEnabled, s => IsPacsEnabled = s.GetSetting<Boolean>(SettingKeys.BvaPacsEnabled));
            SettingObserver.RegisterHandler(SettingKeys.BvaTestId2Label, s => Id2Label = new LabelFormatter(s.GetSetting<String>(SettingKeys.BvaTestId2Label)));
            _eventAggregator.GetEvent<TestSetupTabItemAdded>().Publish(new TestSetupTabItemPayload(Guid.NewGuid())
            {
                Header = "1. Patient Profile",
                TabPlacementIndex = 0,
                NavigationCommand = new DelegateCommand<object>(ExecuteNavigateToMyself)
            });

            _eventAggregator.GetEvent<TestCompleted>().Subscribe(OnTestFinished, ThreadOption.UIThread, false, t => t is BVATest);
        }

        void OnTestFinished(ITest test)
        {
            IsPatientSpecificDataLocked = false;
            var bvaTest = test as BVATest;
            if (bvaTest == null) return;

            if (SelectedHospitalPatientIdItem != null && SelectedHospitalPatientIdItem.HospitalPatientId != null &&
                (bvaTest).Patient.HospitalPatientId == SelectedHospitalPatientIdItem.HospitalPatientId)
            {
                var weight = PatientViewModel.Weight;
                var testId2 = CurrentTest.TestID2;

                PatientViewModel.Weight = weight;
                CurrentTest.TestID2 = testId2;
            }
        }

        void CalculateWeightDeviation()
        {
            if (CurrentPatient == null)
                return;

            IsIdealDeviationHigh = false;

            if (CurrentPatient.Gender != GenderType.Unknown && CurrentPatient.HeightInCm > 0 && CurrentPatient.WeightInKg > 0)
            {
                var idealResults = _idealsCalcEngineService.GetIdeals(CurrentPatient.WeightInKg, CurrentPatient.HeightInCm, (int) CurrentPatient.Gender);
                IsIdealDeviationHigh = idealResults.WeightDeviation < 0;
                if (CurrentTest != null && CurrentTest.Patient != null)
                {
                    if (idealResults.WeightDeviation <= BvaDomainConstants.MaximumWeightDeviation && idealResults.WeightDeviation > BvaDomainConstants.MinimumWeightDeviation)
                    {
                        CurrentTest.Volumes[BloodType.Ideal].PlasmaCount = idealResults.IdealPlasmaVolume;
                        CurrentTest.Volumes[BloodType.Ideal].RedCellCount = idealResults.IdealRedCellVolume;
                    }
                    else
                    {
                        CurrentTest.Volumes[BloodType.Ideal].PlasmaCount = 0;
                        CurrentTest.Volumes[BloodType.Ideal].RedCellCount = 0;
                    }
                }
            }
        }

        void OnHospitalPatientIdChanged()
        {
            NavigateToTestParametersDelegateCommand.RaiseCanExecuteChanged();
        }

        void OnTestPatientChanged(BVATest test)
        {
            CurrentPatient = test.Patient;
            InternalUpdateSelectedHospitalPatientIdItem(test.Patient.HospitalPatientId);
        }

        /// <summary>
        /// Determines whether or not there is a currently executing test whose patient ID (GUID)
        /// matches this patient's ID (GUID).
        /// </summary>
        /// <returns></returns>
        private bool IsThereATestExecutingWithTheSameHospitalPatientId()
        {
            if (_testExecutionController.IsExecuting && _testExecutionController.GetExecutingTestType() == typeof(BVATest))
            {
                var test = _testExecutionController.ExecutingTest as BVATest;
                if (test != null && HospitalPatientId == test.Patient.HospitalPatientId)
                   return true;

                return false;
            }
            return false;
        }

        /// <summary>
        /// Assigns the selected hospital patient ID based on the given value. 
        /// </summary>
        /// <remarks>Uses DataAccess to find the hospital patient ID that matches the given patient's
        /// internal ID. This method is called when the patient changes or when the patient ID field
        ///  is losing focus.</remarks>
        /// <param name="hospitalPatientId">Hospital patient ID to assign</param>
        void InternalUpdateSelectedHospitalPatientIdItem(string hospitalPatientId)
        {
            if (String.IsNullOrEmpty(hospitalPatientId)) return;

            // Find the given patient's hospital ID in the list of all patients.
            var allPatientRecords = HospitalPatientIdDataAccess.SelectList();
            var targetPatientRecord = (from p in allPatientRecords where p.HospitalId == hospitalPatientId select p).FirstOrDefault();

            // The patient isn't in the database yet, so construct an item based on known information.
            if (targetPatientRecord == null)
            {
                SelectedHospitalPatientIdItem = new HospitalPatientIdItem(CurrentPatient.InternalId, HospitalPatientId);
                return;
            }

            // The patient is in the database, so change the selected item if we haven't picked one before or if we're picking a new one.
            if (SelectedHospitalPatientIdItem == null || (SelectedHospitalPatientIdItem.HospitalPatientId != hospitalPatientId))
                SelectedHospitalPatientIdItem = new HospitalPatientIdItem(targetPatientRecord.PatientId, targetPatientRecord.HospitalId);
        }

        /// <summary>
        /// Delegate that responds to the BVAPatient's measurement system being changed
        /// by setting the IsMeasurementSystemChanging flag.
        /// </summary>
        /// <param name="patient">Ignored</param>
        void PatientMeasurementSystemChanged(BVAPatient patient)
        {
            IsMeasurementSystemChanging = true;
        }

        private void ForceRuleEvaluation()
        {
            FirePropertyChanged("CurrentPatient");
            FirePropertyChanged("CurrentTest");
            IsPatientSpecificDataLocked = IsThereATestExecutingWithTheSameHospitalPatientId();
        }

        /// <summary>
        /// Called when new BVATest is selected.
        /// </summary>
        /// <param name="test">Test that was selected</param>
        void OnBvaTestSelectedEvent(BVATest test)
        {
            if (test == null)
                throw new ArgumentNullException("test");
            if (test.Patient == null)
                throw new NotSupportedException("Patient in the given BVATest instance cannot be null");

            CurrentTest = test;
        }

        /// <summary>
        /// Sets the hospital patient ID based on the current patient's ID assigned by the hospital.
        /// </summary>
        /// <remarks>Virtual for unit testing purposes</remarks>
        protected virtual void SetHospitalPatientIdBasedOnCurrentTest()
        {
            HospitalPatientId = CurrentTest.Patient.HospitalPatientId;
        }

        /// <summary>
        /// Handles the various scenarios where the hospital patient ID is possibly being
        /// changed.
        /// </summary>
        /// <remarks>
        /// Called when focus is removed from hospital patient ID field.
        /// 1. The ID cannot be blank unless this is a pending test. (User is warned.)
        /// 2. Current patient exists in the database
        ///    a. Selected patient exists, then the user is prompted to keep the current or use the selected patient.
        ///    b. Selected patient doesn't exist, then the user is prompted to keep the current or create a new patient.
        /// 3. Current patient doesn't exist in the database
        ///    a. Selected patient exists, then the user is prompted to keep the current or use the selected patient.
        ///    b. Selected patient doesn't exist either, no prompts needed.
        /// </remarks>
        public void HospitalPatientIdEndEdit()
        {
            if (CurrentPatient.HospitalPatientId == HospitalPatientId)
                return;

            // The patient ID cannot be empty unless this is a pending test.
            if (CurrentTest.Status != TestStatus.Pending && String.IsNullOrWhiteSpace(HospitalPatientId))
            {
                const string caption = @"Empty Patient ID";
                var noBlankPatientIdAlert = _messageManager.GetMessage(MessageKeys.BvaNoBlankPatientId);
                var message = noBlankPatientIdAlert.Description + " [Alert: " + noBlankPatientIdAlert.MessageId + "]";
                _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning, message, caption, new [] {"Restore Patient ID"});
                _logger.Log(message, Category.Info, Priority.Low);
                HospitalPatientId = CurrentPatient.HospitalPatientId;
                return;
            }

            var selectedPatient = _bvTestController.DataService.SelectPatient(HospitalPatientId, _bvTestController.SelectedTest.ConcurrencyObject);
            var selectedPatientExists = selectedPatient.InternalId != Guid.Empty;
            var currentPatientExists = _bvTestController.DataService.SelectPatient(CurrentPatient.HospitalPatientId,
                _bvTestController.SelectedTest.ConcurrencyObject).InternalId != Guid.Empty;
            
            if (currentPatientExists)
            {
                if (selectedPatientExists)
                    HandleExistingCurrentPatientWithExistingSelectedPatient(selectedPatient);
                else
                    HandleExistingCurrentPatientWithNewSelectedPatient();
            }
            else 
            {
                if (selectedPatientExists)
                    HandleNewCurrentPatientWithExistingSelectedPatient(selectedPatient);
                else
                    HandleNewCurrentPatientWithNewSelectedPatient();
            }

            ForceRuleEvaluation();
            FirePropertyChanged("HospitalPatientId");
        }

        /// <summary>
        /// Handles the scenario where neither the current nor selected patients exist in the database
        /// by setting the current patient's hospital-given ID to what the user entered, and denoting
        /// that this patient is new to the cache.
        /// </summary>
        private void HandleNewCurrentPatientWithNewSelectedPatient()
        {
            CurrentPatient.HospitalPatientId = HospitalPatientId;
                _patientSpecificFieldCache.IsNewPatient = true;
        }

        /// <summary>
        /// Handles the scenario where the current patient doesn't exist in the database, but the
        /// selected one does. If no patient-specific data has been entered, we populate fields
        /// just as if they had chosen the selected patient from the beginning. Otherwise, we
        /// ask the user if the selected patient should be loaded.
        /// </summary>
        /// <param name="selectedPatient">Patient being selected</param>
        private void HandleNewCurrentPatientWithExistingSelectedPatient(BVAPatient selectedPatient)
        {
            var result = 0;
            var choices = new[] { "Use Selected", "Keep Entered" };

            // If the user has entered information, we need to ask if we should blow the information away.
            if (HavePatientDemographicFieldsBeenEntered())
            {
                var changingNewPatientIdToExistingPatientIdAlert = _messageManager.GetMessage(MessageKeys.BvaChangingNewPatientIdToExistingPatientId);
                const string caption = "Changing Patient ID";
                var escapedDescription = changingNewPatientIdToExistingPatientIdAlert.Description;
                var alertDescriptionComponents = escapedDescription.Split('|');
                var intro = alertDescriptionComponents.FirstOrDefault();
                var promptAndAlertId = alertDescriptionComponents.LastOrDefault() + " [Alert: " + changingNewPatientIdToExistingPatientIdAlert.MessageId + "]";
                const string patientAHeader = "Entered Patient";
                const string patientBHeader = "Selected Patient";
                
                result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Question,
                    new PatientComparisonSubView(intro, patientAHeader, patientBHeader, promptAndAlertId, CurrentPatient, selectedPatient),
                    caption, choices);

                _logger.Log(promptAndAlertId, Category.Info, Priority.Low);
            }

            // User chose to not to change the hospital patient ID
            if (choices[result] == "Keep Entered")
            {
                HospitalPatientId = CurrentPatient.HospitalPatientId; 
                _patientSpecificFieldCache.UninitializePatientSpecificFieldCache();
            }
            // User chose to change the hospital patient ID
            else
            {
                _patientSpecificFieldCache.OverridePatientSpecificFieldCache(selectedPatient);
                _patientSpecificFieldCache.IsNewPatient = false;
                selectedPatient.RuleEngine = RuleEngine;

                var heightRecord = UtilityDataService.GetPatientHeight(selectedPatient.InternalId);
                selectedPatient.HeightInCm = heightRecord.HeightInCm;
                selectedPatient.MeasurementSystem = heightRecord.MeasurementSystem;

                CurrentTest.Patient = selectedPatient;
                CurrentPatient = selectedPatient;
            }
        }

        /// <summary>
        /// Handles the scenario where the current patient already exists in the database, and so
        /// does the selected one. The user is prompted to confirm whether to keep the existing 
        /// information or take the selected patient's information instead.
        /// </summary>
        /// <param name="selectedPatient">Patient being selected</param>
        private void HandleExistingCurrentPatientWithExistingSelectedPatient(BVAPatient selectedPatient)
        {
            var changingExistingIdToOtherExistingPatientIdAlert = _messageManager.GetMessage(MessageKeys.BvaChangingExistingPatientIdToOtherExistingPatientId);
            const string caption = "Changing Patient ID";
            var escapedDescription = changingExistingIdToOtherExistingPatientIdAlert.Description;
            var alertDescriptionComponents = escapedDescription.Split('|');
            var intro = alertDescriptionComponents.FirstOrDefault();
            var promptAndAlertId = alertDescriptionComponents.LastOrDefault() + " [Alert: " + changingExistingIdToOtherExistingPatientIdAlert.MessageId + "]";
            const string patientAHeader = "Entered Patient";
            const string patientBHeader = "Selected Patient";
            var choices = new[] { "Use Selected", "Keep Entered" };
            var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Question,
                new PatientComparisonSubView(intro, patientAHeader, patientBHeader, promptAndAlertId, CurrentPatient, selectedPatient),
                caption, choices);

            _logger.Log(promptAndAlertId, Category.Info, Priority.Low);
            // User chose to not change the hospital patient ID.
            if (choices[result] == "Keep Entered")
            {
                HospitalPatientId = CurrentPatient.HospitalPatientId;
                _patientSpecificFieldCache.RestorePatientFieldsToMatchCache(CurrentPatient);
            }
            // User chose to change the hospital patient ID
            else
            {
                _patientSpecificFieldCache.OverridePatientSpecificFieldCache(selectedPatient);
                _patientSpecificFieldCache.IsNewPatient = false;
                selectedPatient.RuleEngine = RuleEngine;

                var heightRecord = UtilityDataService.GetPatientHeight(selectedPatient.InternalId);
                selectedPatient.HeightInCm = heightRecord.HeightInCm;
                selectedPatient.MeasurementSystem = heightRecord.MeasurementSystem;

                if (CurrentTest.Status == TestStatus.Completed)
                    selectedPatient.WeightInKg = CurrentPatient.WeightInKg;

                CurrentTest.Patient = selectedPatient;
                CurrentPatient = selectedPatient;
            }
        }

        /// <summary>
        /// Handles the scenario where the current patient already exists in the database, but the
        /// selected one doesn't. The user is prompted to confirm whether to keep the existing 
        /// information or create a new one. (If creating a new patient and patient-specific data
        /// has been entered, it will be transferred to the new patient.)
        /// </summary>
        private void HandleExistingCurrentPatientWithNewSelectedPatient()
        {
            const string caption = "Changing Patient ID";
            var changingExistingIdToNewPatientIdAlert = _messageManager.GetMessage(MessageKeys.BvaChangingExistingPatientIdToNewPatientId);
            var escapedDescription = changingExistingIdToNewPatientIdAlert.Description;
            var message = String.Format(escapedDescription, HospitalPatientId) +
                          " [Alert: " + changingExistingIdToNewPatientIdAlert.MessageId + "]";
            var choices = new[] { "Use Selected", "Create New" };
            var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Question, message, caption, choices);
            _logger.Log(message, Category.Info, Priority.Low);
            // User chose to create a new patient
            if (choices[result] == "Create New")
            {
                var selectedPatient = new BVAPatient(IdentityGenerator.NewSequentialGuid(), RuleEngine) { HospitalPatientId = HospitalPatientId };

                // Copy the information over into a new patient
                if (HavePatientDemographicFieldsBeenEntered())
                {
                    selectedPatient.MeasurementSystem = ConvertDisplayUnitsToMeasurementSystem(_settingsManager.GetSetting<string>(SettingKeys.SystemDefaultMeasurementSystem));
                    selectedPatient.HeightInCm = CurrentPatient.HeightInCm;
                    selectedPatient.WeightInKg = CurrentPatient.WeightInKg;
                    selectedPatient.FirstName = CurrentPatient.FirstName;
                    selectedPatient.LastName = CurrentPatient.LastName;
                    selectedPatient.MiddleName = CurrentPatient.MiddleName;
                    selectedPatient.Gender = CurrentPatient.Gender;
                    selectedPatient.DateOfBirth = CurrentPatient.DateOfBirth;
	                selectedPatient.IsAmputee = CurrentPatient.IsAmputee;
                }
                // Nothing to copy; it's just like a new test.
                else
                {
                    selectedPatient.HeightInCm = 0;
                    selectedPatient.MeasurementSystem = ConvertDisplayUnitsToMeasurementSystem(_settingsManager.GetSetting<string>(SettingKeys.SystemDefaultMeasurementSystem));
                }

                // Now that the new patient's fields are set, sync the cache with it.
                _patientSpecificFieldCache.OverridePatientSpecificFieldCache(selectedPatient);
                _patientSpecificFieldCache.IsNewPatient = true;

                CurrentTest.Patient = selectedPatient;
                CurrentPatient = selectedPatient;
            }
            // User chose to not change the hospital patient ID
            else
            {
                HospitalPatientId = CurrentPatient.HospitalPatientId;
                _patientSpecificFieldCache.RestorePatientFieldsToMatchCache(CurrentPatient);
            }
        }

        /// <summary>
        /// Determines whether or not height, weight, last name, first name, middle name, 
        /// gender, or date of birth have non-default values already entered.
        /// </summary>
        /// <returns>True if non-default values exist for any of the aforementioned fields;
        /// false otherwise</returns>
        private bool HavePatientDemographicFieldsBeenEntered()
        {
            if (CurrentPatient == null)
                return false;

            // ReSharper disable CompareOfFloatsByEqualityOperator
            return CurrentPatient.HeightInCm != 0 || CurrentPatient.WeightInKg != 0 || !String.IsNullOrEmpty(CurrentPatient.LastName) ||
                   !String.IsNullOrEmpty(CurrentPatient.FirstName) || !String.IsNullOrEmpty(CurrentPatient.MiddleName) || 
                   CurrentPatient.Gender != GenderType.Unknown || CurrentPatient.DateOfBirth != null;
            // ReSharper restore CompareOfFloatsByEqualityOperator
        }

        private static MeasurementSystem ConvertDisplayUnitsToMeasurementSystem(string displayUnits)
        {
            return displayUnits.Trim() == "US" ? MeasurementSystem.English : MeasurementSystem.Metric;
        }

        #endregion

        #region IDataErrorInfo

        string IDataErrorInfo.Error
        {
            get { return null; }
        }

        string IDataErrorInfo.this[string columnName]
        {
            get
            {
                if (columnName == "HospitalPatientId" && _currentPatient != null)
                    return _currentPatient["HospitalPatientId"];
                if (columnName == "GenderForUi" && _currentPatient != null)
                    return _currentPatient["Gender"];

                return String.Empty;
            }
        }

        #endregion
    }
}
