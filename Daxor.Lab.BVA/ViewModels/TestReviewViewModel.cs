﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Daxor.Lab.BVA.Common;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.Services;
using Daxor.Lab.BVA.SubViews;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Alerts;
using Daxor.Lab.Infrastructure.Base;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.Keyboards;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Commands;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.BVA.ViewModels
{
    /// <summary>
    /// The test review view model
    /// </summary>
    public class TestReviewViewModel : ViewModelBase, IActiveAware
    {
        #region Fields

        private readonly IBloodVolumeTestController _controller;
        private readonly IAppModuleNavigator _localNavigator;
        private readonly IEventAggregator _eventAggregator;
        readonly ITestExecutionController _testExecutioner;
        private readonly ILoggerFacade _logger;
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly IAuditTrailDataAccess _auditTrailDataAccess;
        private readonly ISettingsManager _settingsManager;
        private readonly IMessageManager _messageManager;
        private readonly INavigationCoordinator _navCoordinator;
        
        private readonly ThreadSafeObservableCollection<TestReviewSampleViewModel> _threadSafeSamplesOneThroughThree = new ThreadSafeObservableCollection<TestReviewSampleViewModel>();
        private readonly ThreadSafeObservableCollection<TestReviewSampleViewModel> _threadSafeSamplesFourThroughSix = new ThreadSafeObservableCollection<TestReviewSampleViewModel>();

        private string _alertResolutionPayload;
        private BVATest _testModel;
        private DispatcherTimer _timer;
        private SettingObserver<ISettingsManager> _sObserver;
        private PropertyObserver<BVATest> _testObserver;
        private PropertyObserver<BVAPatient> _patientObserver;

        private Boolean _isPacsEnabled;
        private Boolean _canViewTestResults;
        private LabelFormatter _id2Label;
        private LabelFormatter _locationLabel;

        private DelegateCommand<object> _navigateToPatientDemographicsCommand;
        private DelegateCommand<object> _navigateToTestParametersCommand;
        private DelegateCommand<object> _navigateToTestSelectionCommand;
        private DelegateCommand<object> _navigateToTestSamplesCommand;
        private DelegateCommand<object> _saveTestAndCloseCommand;
        private DelegateCommand<object> _startTestCommand;

        #endregion

        #region Ctor

        public TestReviewViewModel([Dependency(AppModuleIds.BloodVolumeAnalysis)] IAppModuleNavigator localNavigator,
                                   IBloodVolumeTestController controller,
                                   ITestExecutionController testExecutioner, 
                                   IMessageBoxDispatcher messageBoxDispatcher,
                                   IAuditTrailDataAccess auditTrailDataAccess, ISettingsManager settingsManager,
                                   IMessageManager messageManager, ILoggerFacade logger, INavigationCoordinator navCoordinator, IEventAggregator eventAggregator)
        {
            _localNavigator = localNavigator;
            _controller = controller;
            _testExecutioner = testExecutioner;
            _messageBoxDispatcher = messageBoxDispatcher;
            _auditTrailDataAccess = auditTrailDataAccess;
            _settingsManager = settingsManager;
            _logger = logger;
            _messageManager = messageManager;
            _navCoordinator = navCoordinator;
            _eventAggregator = eventAggregator;
            
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InitializeViewModel();
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        protected virtual void InitializeViewModel()
        {
            SettingObserver = new SettingObserver<ISettingsManager>(_settingsManager);

            DisplayName = "Review BVA Test Setup";

            InitializeDelegateCommands();

            //Verify if PACS is enabled or not
            IsPacsEnabled = _settingsManager.GetSetting<Boolean>(SettingKeys.BvaPacsEnabled);

            SettingObserver.RegisterHandler(SettingKeys.BvaPacsEnabled, s => { IsPacsEnabled = s.GetSetting<Boolean>(SettingKeys.BvaPacsEnabled); });

            SubscribeToAggregateEvents();
        }

        protected void InitializeDelegateCommands()
        {
            // Initialize internal commands
            NavigateToPatientDemographicsDelegateCommand = new DelegateCommand<object>(NavigateToPatientDemographicsCommandExecute);
            NavigateToTestParametersDelegateCommand = new DelegateCommand<object>(NavigateToTestParametersCommandExecute, NavigateToTestParametersCommandCanExecute);
            NavigateToTestSamplesDelegateCommand = new DelegateCommand<object>(NavigateToSamplesCommandExecute, NavigateToSamplesCommandCanExecute);
            NavigateToTestSelectionDelegateCommand = new DelegateCommand<object>(NavigateToTestSelectionCommandExecute);
            SaveTestAndCloseDelegateCommand = new DelegateCommand<object>(SaveTestAndCloseCommandExecute, SaveTestAndCloseCommandCanExecute);
            StartTestDelegateCommand = new DelegateCommand<object>(StartTestCommandExecute, StartTestCommandCanExecute);
        }

        #endregion

        #region Properties

        private DelegateCommand<object> SaveTestAndCloseDelegateCommand
        {
            get { return _saveTestAndCloseCommand; }
            set
            {
                // Only get one assignment
                if (_saveTestAndCloseCommand == null)
                    _saveTestAndCloseCommand = value;
            }
        }

        private DelegateCommand<object> NavigateToTestSelectionDelegateCommand
        {
            get { return _navigateToTestSelectionCommand; }
            set
            {
                // Only get one assignment
                if (_navigateToTestSelectionCommand == null)
                    _navigateToTestSelectionCommand = value;
            }
        }

        private DelegateCommand<object> NavigateToPatientDemographicsDelegateCommand
        {
            get { return _navigateToPatientDemographicsCommand; }
            set
            {
                // Only get one assignment
                if (_navigateToPatientDemographicsCommand == null)
                    _navigateToPatientDemographicsCommand = value;
            }
        }

        private DelegateCommand<object> NavigateToTestParametersDelegateCommand
        {
            get { return _navigateToTestParametersCommand; }
            set
            {
                // Only get one assignment
                if (_navigateToTestParametersCommand == null)
                    _navigateToTestParametersCommand = value;
            }
        }

        private DelegateCommand<object> NavigateToTestSamplesDelegateCommand
        {
            get { return _navigateToTestSamplesCommand; }
            set
            {
                // Only get one assignment
                if (_navigateToTestSamplesCommand == null)
                    _navigateToTestSamplesCommand = value;
            }
        }

        private DelegateCommand<object> StartTestDelegateCommand
        {
            get { return _startTestCommand; }
            set
            {
                // Only get one assignment
                if (_startTestCommand == null)
                    _startTestCommand = value;
            }
        }

        private SettingObserver<ISettingsManager> SettingObserver
        {
            get { return _sObserver; }
            set
            {
                // Only get one assignment
                if (_sObserver == null)
                    _sObserver = value;
            }
        }

        public BVATest CurrentTest
        {
            get { return _testModel; }
            set
            {
                if (SetValue(ref _testModel, "CurrentTest", value))
                    InitializeTestAndPatientPropertyObservers(_testModel);
            }
        }

        public ThreadSafeObservableCollection<TestReviewSampleViewModel> SamplesOneThroughThree
        {
            get { return _threadSafeSamplesOneThroughThree; }
        }
        public ThreadSafeObservableCollection<TestReviewSampleViewModel> SamplesFourThroughSix
        {
            get { return _threadSafeSamplesFourThroughSix; }
        }

        public LabelFormatter Id2Label
        {
            get { return _id2Label; }
            set { SetValue(ref _id2Label, "Id2Label", value); }
        }
        public LabelFormatter LocationLabel
        {
            get { return _locationLabel; }
            set { SetValue(ref _locationLabel, "LocationLabel", value); }
        }
        public Boolean IsPacsEnabled
        {
            get { return _isPacsEnabled; }
            set { SetValue(ref _isPacsEnabled, "IsPacsEnabled", value); }
        }
        public Boolean CanViewTestResults
        {
            get { return _canViewTestResults; }
            private set { SetValue(ref _canViewTestResults, "CanViewTestResults", value); }
        }

        #endregion

        #region Commands
       
        public ICommand SaveTestAndCloseCommand
        {
            get { return SaveTestAndCloseDelegateCommand; }
        }
        void SaveTestAndCloseCommandExecute(object arg)
        {
            //Busy state starts here
            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, Constants.SavingTestMessage));
            
            // Get the UI thread's context
            var context = TaskScheduler.FromCurrentSynchronizationContext();
            
            if (CurrentTest.Status == TestStatus.Pending &&
                (!string.IsNullOrEmpty(CurrentTest.InjectateKey) ||
                !string.IsNullOrEmpty(CurrentTest.StandardAKey) ||
                !string.IsNullOrEmpty(CurrentTest.StandardBKey)))
            {
                string alertMessage = _messageManager.GetMessage(MessageKeys.BvaSavingTestWillClearKeys).FormattedMessage;
                var choices = new[] { "Clear Keys and Save", "Cancel" };
                int result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Question, alertMessage , DisplayName, choices);
                _logger.Log(alertMessage, Category.Info, Priority.Low);
                if (choices[result] == "Clear Keys and Save")
                {
                    var saveTask = Task.Factory.StartNew(() =>
                    {
                        CurrentTest.ClearBarcodes();
                        _controller.DataService.SaveTest(CurrentTest);
                        _logger.Log("SAVE AND CLOSE BUTTON CLICKED", Category.Info, Priority.None);
                    });
                    saveTask.ContinueWith(
                        t =>
                        {
                            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
                            _localNavigator.ActivateView(BVAModuleViewKeys.TestSelection, null);

                        }, context);
                }
            }
            else
            {

                var saveTask = Task.Factory.StartNew(() =>
               {
                   _controller.DataService.SaveTest(CurrentTest);
                   _logger.Log("SAVE AND CLOSE BUTTON CLICKED", Category.Info, Priority.None);
               });

                saveTask.ContinueWith(t =>
                {
                    _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
                    _localNavigator.ActivateView(BVAModuleViewKeys.TestSelection);
                  
                }, context);
            }
        }
        bool SaveTestAndCloseCommandCanExecute(object arg)
        {
            return CurrentTest != null && CurrentTest.Patient != null && !String.IsNullOrEmpty(CurrentTest.Patient.HospitalPatientId);
        }

        public ICommand NavigateToTestSelectionCommand
        {
            get { return NavigateToTestSelectionDelegateCommand; }
        }
        void NavigateToTestSelectionCommandExecute(object arg)
        {
            const string content = "Are you sure you want to cancel changes and return to the test list?";
            const string caption = "Cancel Changes";
            var choices = new[] { "Return to Test List", "Cancel" };
            int result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Question,content, caption, choices);

            if (choices[result] == "Cancel") 
                return;

            _localNavigator.ActivateView(BVAModuleViewKeys.TestSelection, null);
            _logger.Log("NAVIGATE TO TEST SELECTION (CANCEL) BUTTON CLICKED", Category.Info, Priority.None);
        }

        public ICommand NavigateToPatientDemographicsCommand
        {
            get { return NavigateToPatientDemographicsDelegateCommand; }
        }
        void NavigateToPatientDemographicsCommandExecute(object arg)
        {
            _localNavigator.ActivateView(BVAModuleViewKeys.PatientDemographics);
            _logger.Log("NAVIGATE TO PATIENT DEMOGRAPHICS BUTTON CLICKED", Category.Info, Priority.None);
        }

        public ICommand NavigateToTestParametersCommand
        {
            get { return NavigateToTestParametersDelegateCommand; }
        }
        void NavigateToTestParametersCommandExecute(object arg)
        {
            _localNavigator.ActivateView(BVAModuleViewKeys.TestParameters);
            _logger.Log("NAVIGATE TO TEST PARAMETERS BUTTON CLICKED",Category.Info, Priority.None);
        }
        bool NavigateToTestParametersCommandCanExecute(object o)
        {
            return CurrentTest != null && !String.IsNullOrEmpty(CurrentTest.Patient.HospitalPatientId);
        }
       
        public ICommand NavigateToTestSamplesCommand
        {
            get { return NavigateToTestSamplesDelegateCommand; }
        }
        void NavigateToSamplesCommandExecute(object arg)
        {
            _localNavigator.ActivateView(BVAModuleViewKeys.TestSampleValues);
            _logger.Log("NAVIGATE TO SAMPLE VALUES BUTTON CLICKED", Category.Info, Priority.None);
        }
        bool NavigateToSamplesCommandCanExecute(object o)
        {
            return CurrentTest != null && CurrentTest.Protocol != -1 && !String.IsNullOrEmpty(CurrentTest.Patient.HospitalPatientId);
        }
       
        public ICommand StartTestCommand
        {
            get { return StartTestDelegateCommand; }
        }
        void StartTestCommandExecute(object arg)
        {
            _logger.Log("START TEST BUTTON CLICKED", Category.Info, Priority.None);
            var choices = new[] { "Start Test", "Return" };
            var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information, CreateVerifyTestSetupContent(), "Verify Test Sample Loading", choices);
            
            if (choices[result] != "Start Test") 
                return;

			_controller.DataService.SaveTest(CurrentTest);

            if (!_settingsManager.GetSetting<bool>(SettingKeys.QcLastQcTestPassed))
            {
                HandleQcInFailedState();
                return;
            }

            if (_settingsManager.GetSetting<bool>(SettingKeys.QcTestDue))
                HandleQcInDueState();
            else
                StartTest();

            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
        }

        protected virtual VerifyTestSetupContent CreateVerifyTestSetupContent()
        {
            return new VerifyTestSetupContent(CurrentTest);
        }

        private void StartTest()
        {
            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, Constants.StartingTestMessage));
            _controller.StartSelectedTest();
			_controller.DataService.SaveTest(CurrentTest, true);
            _localNavigator.ActivateView(BVAModuleViewKeys.TestSampleValues);
        }

        private void HandleQcInDueState()
        {
            var overridenTestLabel = DetermineQcOverridenLabel();

            var errorCaption = overridenTestLabel + " QC Due";
            var errorMessage = String.Format(_messageManager.GetMessage(MessageKeys.BvaDailyQCDue).FormattedMessage, overridenTestLabel);

            var additionalComment = String.Format(Constants.QCDueNoticeOverriddenFormatMessage, overridenTestLabel);

            string[] choices = {"Run " + overridenTestLabel + " QC", "Continue"};
            var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Warning, errorMessage, errorCaption, choices);
            _logger.Log(errorMessage, Category.Info, Priority.Low);

            if (choices[result] == "Continue")
            {
                // Insert audit trail, has been overwritten
                _auditTrailDataAccess.Insert(new AuditTrailRecord
                {
                    TestId = CurrentTest.InternalId,
                    ChangeTable = "tblTEST_HEADER",
                    ChangeTime = DateTime.Now.ToString("M/d/yyyy H:mm:ss"),
                    OldValue = CurrentTest.Comments,
                    NewValue = CurrentTest.Comments + additionalComment,
                    ChangedBy = CurrentTest.Analyst ?? string.Empty,
                    ChangeField = "COMMENT"
                });

                CurrentTest.Comments += additionalComment;
                StartTest();
            }
            else
            {
                NavigateToQcTestList();
            }
        }

        private string DetermineQcOverridenLabel()
        {
            var nameOfOverridenTest = "Full";
            if (_alertResolutionPayload == AlertResolutionPayload.ContaminationQCAlert)
                nameOfOverridenTest = "Contamination";
            if (_alertResolutionPayload == AlertResolutionPayload.LinearityQCAlert)
                nameOfOverridenTest = "Linearity";
            if (_alertResolutionPayload == AlertResolutionPayload.StandardsQCAlert)
                nameOfOverridenTest = "Standards";
            
            return nameOfOverridenTest;
        }

        private void HandleQcInFailedState()
        {
            const string errorCaption = "Unable to Proceed";
            var errorMessage = _messageManager.GetMessage(MessageKeys.BvaFailedQCState).FormattedMessage;
            string[] choices = {"Run QC", "Close"};
            
            var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, errorMessage, errorCaption, choices);
            _logger.Log(errorMessage, Category.Info, Priority.Low);
            
            if (choices[result] == "Run QC")
                NavigateToQcTestList();
            else
                _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
        }

        bool StartTestCommandCanExecute(object arg)
        {
            if (!IsActive || CurrentTest == null || CurrentTest.Mode == TestMode.Manual || CurrentTest.Status != TestStatus.Pending ||
                _testExecutioner == null || _testExecutioner.IsExecuting || !CurrentTest.CanStartExecution())
            {
               
               return false;
            }

            return true; // If we get here, all required fields have been satisfied
        }

        #endregion

        #region IActiveAware Members
        
        private bool _isActive;
        public bool IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;
                if (value)
                {
                    Id2Label = new LabelFormatter(_settingsManager.GetSetting<String>(SettingKeys.BvaTestId2Label));
                    LocationLabel = new LabelFormatter(_settingsManager.GetSetting<String>(SettingKeys.BvaLocationLabel));
                    EvaluateCommands(NavigateToTestParametersDelegateCommand, NavigateToTestSamplesDelegateCommand, SaveTestAndCloseDelegateCommand, StartTestDelegateCommand);
                    CanViewTestResults = CurrentTest.Status != TestStatus.Pending;

                    UpdateSampleValueList();
                }

                UpdateFooterPartActivation(value, !value);
                CloseActiveKeyboard();
            }
        }

        public event EventHandler IsActiveChanged = delegate { };

        #endregion

        #region Helpers

        protected virtual void InitializeTestAndPatientPropertyObservers(BVATest test)
        {
            if (test != null)
            {
                _testObserver = new PropertyObserver<BVATest>(test);
                _testObserver.RegisterHandler(t => t.Patient, PatientChanged);
                _testObserver.RegisterHandler(t => t.Protocol, t => NavigateToTestSamplesDelegateCommand.RaiseCanExecuteChanged());

                if (test.Patient != null)
                {
                    _patientObserver = new PropertyObserver<BVAPatient>(test.Patient);
                    _patientObserver.RegisterHandler(p => p.HospitalPatientId, OnPatientHospitalIdChanged);
                }
            }
        }

        void EvaluateCommands(params DelegateCommand<Object>[] commands)
        {
            //If the viewModel is not active, don't try to evaluate commands
            if (!IsActive)
                return;

            foreach (var command in commands)
                command.RaiseCanExecuteChanged();
        }
      
        /// <summary>
        /// Note: Dirty way of closing the footerPart until
        /// the animation completes
        /// </summary>
        void UpdateFooterPartActivation(bool shouldDelay, bool newValue)
        {
            if (shouldDelay == false)
            {
                _eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(ApplicationParts.Footer, newValue));
                return;
            }

            //Delays hiding of footer until animation finishes
            if (_timer == null)
            {
                _timer = new DispatcherTimer {Interval = new TimeSpan(0, 0, 0, 0, 300)};
                _timer.Tick += (s, e) =>
                {
                    _eventAggregator.GetEvent<AppPartActivationChanged>().Publish(new ActivationChangedPayload(ApplicationParts.Footer, newValue));
                    _timer.Stop();
                };
            }

            if (!_timer.IsEnabled)
            {
                _timer.Start();
            }
        }

        protected virtual void UpdateSampleValueList()
        {
            // Clear both of the lists
            if (SamplesOneThroughThree.Count > 0)
                SamplesOneThroughThree.Clear();
            if (SamplesFourThroughSix.Count > 0)
                SamplesFourThroughSix.Clear();

            // Updates 1 through 3
            if (!CurrentTest.PatientSamples.Any()) return;

            var groupedSamples = from sample in CurrentTest.Samples
                                 where (sample.Type != SampleType.Background && sample.Type != SampleType.Standard)
                                 group sample by sample.Type;

            foreach (var sample in groupedSamples)
            {
                var sampleA = (from s in sample where s.Range == SampleRange.A select s).FirstOrDefault();
                var sampleB = (from s in sample where s.Range == SampleRange.B select s).FirstOrDefault();

                if (sampleA == null)
                    throw new NotSupportedException("Unable to retrieve A-side sample from grouped CurrentTest.Samples");

                if (sampleA.Type == SampleType.Sample4 || sampleA.Type == SampleType.Sample5 || sampleA.Type == SampleType.Sample6)
                    SamplesFourThroughSix.Add(new TestReviewSampleViewModel(sampleA, sampleB, CurrentTest.HematocritFill));
                else
                    SamplesOneThroughThree.Add(new TestReviewSampleViewModel(sampleA, sampleB, CurrentTest.HematocritFill));
            }
        }

        protected void SubscribeToBvaTestSelectedEvent()
        {
            _eventAggregator.GetEvent<BvaTestSelected>().Subscribe(BVATestSelectedEventHandler, ThreadOption.PublisherThread, false);
        }

        protected virtual void SubscribeToAggregateEvents()
        {
            SubscribeToBvaTestSelectedEvent();
            SubscribeToAlertViolationChangedEvent();

            _eventAggregator.GetEvent<TestStarted>().Subscribe(test => StartTestDelegateCommand.RaiseCanExecuteChanged(), ThreadOption.UIThread, false);
            _eventAggregator.GetEvent<TestAborted>().Subscribe(test => StartTestDelegateCommand.RaiseCanExecuteChanged(), ThreadOption.UIThread, false);
            _eventAggregator.GetEvent<TestCompleted>().Subscribe(test => StartTestDelegateCommand.RaiseCanExecuteChanged(), ThreadOption.UIThread, false);
	        _eventAggregator.GetEvent<BvaTestSaved>().Subscribe(InsertFirstAmputeeStatusChangeIntoAuditTrailIfNecessary);
        }

	    private void InsertFirstAmputeeStatusChangeIntoAuditTrailIfNecessary(Guid testGuid)
	    {
		    if (_testModel.InternalId == testGuid && _testModel.PatientIsAmputee)
			{
				//If patient amputee status is now true and a change to the amputee status 
				//has not been already inserted in the audit trail for this test, then
				//insert this change into the audit trail.
				var currentAuditTrail = _auditTrailDataAccess.SelectList(_testModel.InternalId);

				if (currentAuditTrail.All(record => record.ChangeField != "Amputee Status"))
				{
					_auditTrailDataAccess.Insert(new AuditTrailRecord
					{
						TestId = CurrentTest.InternalId,
						ChangeTable = "tblPATIENT",
						ChangeTime = DateTime.Now.ToString("M/d/yyyy H:mm:ss"),
						OldValue = "Non-amputee",
						NewValue = "Amputee",
						ChangedBy = CurrentTest.Analyst ?? string.Empty,
						ChangeField = "Amputee Status"
					});

					if (_testModel.AmputeeIdealsCorrectionFactor > 0)
						_auditTrailDataAccess.Insert(new AuditTrailRecord
						{
							TestId = CurrentTest.InternalId,
							ChangeTable = "tblBVA_TEST",
							ChangeTime = DateTime.Now.ToString("M/d/yyyy H:mm:ss"),
							OldValue = "0%",
							NewValue = "-" + _testModel.AmputeeIdealsCorrectionFactor + "%",
							ChangedBy = CurrentTest.Analyst ?? string.Empty,
							ChangeField = "Amputee Ideals Correction Factor"
						});
				}
			}
	    }

	    protected void SubscribeToAlertViolationChangedEvent()
        {
            _eventAggregator.GetEvent<AlertViolationChanged>().Subscribe(ProcessAlertViolationPayload, false);
        }

        private void ProcessAlertViolationPayload(AlertViolationPayload alertPayload)
        {
            _alertResolutionPayload = alertPayload.Resolution;
        }

        void PatientChanged(BVATest test)
        {
            if (test.Patient == null) return;
            
            //Evaluates a set of commands
            EvaluateCommands(NavigateToTestParametersDelegateCommand, NavigateToTestSamplesDelegateCommand, SaveTestAndCloseDelegateCommand);
            _patientObserver = new PropertyObserver<BVAPatient>(test.Patient);
            _patientObserver.RegisterHandler(p => p.HospitalPatientId, OnPatientHospitalIdChanged);
        }

        void OnPatientHospitalIdChanged(BVAPatient patient)
        {
            EvaluateCommands(NavigateToTestParametersDelegateCommand, NavigateToTestSamplesDelegateCommand, SaveTestAndCloseDelegateCommand);
        }

        /// <summary>
        /// Called when new BVATest is selected.
        /// </summary>
        /// <param name="test"></param>
        void BVATestSelectedEventHandler(BVATest test)
        {
            CurrentTest = test;

            if (CurrentTest == null) return;

            Debug.Print("TestReviewViewModel received CurrentTest {0}", CurrentTest.GetHashCode().ToString(CultureInfo.InvariantCulture));
            UpdateSampleValueList();
            NavigateToTestParametersDelegateCommand.RaiseCanExecuteChanged();
            NavigateToTestSamplesDelegateCommand.RaiseCanExecuteChanged();
        }

        private void NavigateToQcTestList()
        {
			_controller.DataService.SaveTest(CurrentTest);
            _eventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(false));
            
            _navCoordinator.ActivateAppModule(AppModuleIds.QualityControl, null, _navCoordinator.ActiveAppModuleKey);
            CloseActiveKeyboard();
        }

        protected virtual void CloseActiveKeyboard()
        {
            VirtualKeyboardManager.CloseActiveKeyboard();
        }

        #endregion

        #region TestReviewSampleViewModel

        /// <summary>
        /// View model responsible for displaying test review samples
        /// </summary>
        public class TestReviewSampleViewModel
        {
            readonly BVASample _sampleA;
            readonly BVASample _sampleB;
            readonly HematocritFillType _hctFill;

            public TestReviewSampleViewModel(BVASample sampleA, BVASample sampleB, HematocritFillType hctFill)
            {
                _sampleA = sampleA;
                _sampleB = sampleB;
                _hctFill = hctFill;
            }

            public SampleType Type
            {
                get { return _sampleA.Type; }
            }
            public HematocritFillType HctFill
            {
                get { return _hctFill; }
            }
            public double HctA
            {
                get { return _sampleA.Hematocrit; }
            }
            public double HctB
            {
                get { return _sampleB.Hematocrit; }
            }
            public string FormattedTime
            {
                get
                {
                    var timeInSec = _sampleA.PostInjectionTimeInSeconds;
                    var hours = timeInSec / 3600;
                    var min = (timeInSec - hours * 3600) / 60;
                    var sec = (timeInSec - hours * 3600) % 60;

                    return timeInSec == -1 ? null : string.Format("{0}:{1}", hours * 60 + min, sec.ToString(CultureInfo.InvariantCulture).PadLeft(2, '0'));
                }
            }
        }

        #endregion
    }
}
