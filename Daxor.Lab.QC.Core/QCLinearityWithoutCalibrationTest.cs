using System;
using System.Linq;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Domain.Services;
using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Core
{
    public class QcLinearityWithoutCalibrationTest : QcTest
    {
        #region Ctors

        public QcLinearityWithoutCalibrationTest(Guid testId, IRuleEngine ruleEngine) : base(testId, ruleEngine)
        {
            QCType =TestType.LinearityWithoutCalibration;
        }

        public QcLinearityWithoutCalibrationTest(IRuleEngine ruleEngine) : this(IdentityGenerator.NewSequentialGuid(), ruleEngine) { }

        public QcLinearityWithoutCalibrationTest(SamplesSchema schema, IRuleEngine ruleEngine)
            : this(ruleEngine)
        {
            if (schema == null)
                throw new ArgumentNullException("schema");

            Schema = schema;
        }

        public QcLinearityWithoutCalibrationTest(Guid testId, string systemId, SamplesSchema schema, IRuleEngine ruleEngine)
            : this(schema, ruleEngine)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InternalId = testId;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
            SystemId = systemId;
        }

        #endregion

        #region Properties

        public double MinimumPassingFullWidthHalfMax { get; set; }
        public double MaximumPassingFullWidthHalfMax { get; set; }
        public double ExpectedCentroid { get; set; }
        public double MaximumCentroidDifferenceInChannels { get; set; }
        public double StartSourceActivity { get; set; }
        public double StartRate { get; set; }
        public int MinimumCounts { get; set; }
        public int EstimatedQC2ExecutionTimeInSeconds { get; set; }
        public int EstimatedQC3ExecutionTimeInSeconds { get; set; }
        public int EstimatedQC4ExecutionTimeInSeconds { get; set; }

        #endregion

        #region Public overrides

        public override int GetEstimatedExecutionTimeInSeconds(QcSample sample)
        {
            switch(sample.TypeOfSample)
            {
                case SampleType.QC2:
                    return EstimatedQC2ExecutionTimeInSeconds;
                case SampleType.QC3:
                    return EstimatedQC3ExecutionTimeInSeconds;
                case SampleType.QC4:
                    return EstimatedQC4ExecutionTimeInSeconds;
                default:
                    return GetDesiredExecutionTimeInSeconds(sample);
            }
        }

        public override void ProcessSample(QcSample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");

            // If we have this sample let's process
            if (!ContainsTypeOfSample(sample.TypeOfSample))
                return;

            ProcessSampleIfTestRunning(sample);

            // Internal sample
            var iSample = (from s in InnerSamples where s.Position == sample.Position select s).FirstOrDefault();
            if (iSample == null) return;

            iSample.CountsInRegionOfInterest = sample.CountsInRegionOfInterest;
            iSample.FullWidthHalfMax = sample.FullWidthHalfMax;
            iSample.FullWidthTenthMax = sample.FullWidthTenthMax;
            iSample.Centroid = sample.Centroid;
            iSample.ExecutionStatus = sample.ExecutionStatus;
            iSample.IsPassed = sample.IsPassed;
            iSample.Source = sample.Source;
            iSample.Spectrum.LiveTimeInSeconds = sample.Spectrum.LiveTimeInSeconds;
            iSample.Spectrum.RealTimeInSeconds = sample.Spectrum.RealTimeInSeconds;
            iSample.Spectrum.SpectrumArray = sample.Spectrum.SpectrumArray;
            iSample.PresetCountsLimit = iSample.IsSource ? iSample.Source.CountLimit : -1;
            iSample.PresetCountLimitReached = sample.PresetCountLimitReached;
            iSample.StartChannel = sample.StartChannel;
            iSample.EndChannel = sample.EndChannel;
            iSample.ErrorDetails = sample.ErrorDetails;
        }

        public override QcSampleEvaluationMetadata EvaluateSample(QcSample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");
            if (sample.InternalId == null)
                throw new NullReferenceException("sample.InteralID");

            QcSampleEvaluationMetadata eMetadata = null;

            // Process extra time
            ProcessSample(sample);

            // If we have this sample let's process
            if (!ContainsTypeOfSample(sample.TypeOfSample))
                return new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Undetermined);

            var mSample = sample;

            if (mSample.TypeOfSample != SampleType.Background)
            {
                if (mSample.CountsInRegionOfInterest < MinimumCounts)
                    eMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.FailedButContinue, "Linearity: QC2 counts in ROI are less than " + mSample.PresetCountsLimit);
                else if (mSample.FullWidthHalfMax < MinimumPassingFullWidthHalfMax)
                    eMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.FailedButContinue, "Linearity: " + mSample.TypeOfSample + " FWHM is less than " + MinimumPassingFullWidthHalfMax + "%");
                else if (mSample.FullWidthHalfMax > MaximumPassingFullWidthHalfMax)
                    eMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.FailedButContinue, "Linearity: " + mSample.TypeOfSample + " FWHM is greater than " + MaximumPassingFullWidthHalfMax + "%");
                else if (Math.Abs(mSample.Centroid - ExpectedCentroid) > MaximumCentroidDifferenceInChannels)
                    eMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.FailedButContinue, "Linearity: " + mSample.TypeOfSample + " centroid difference is greater than " + MaximumCentroidDifferenceInChannels);

                switch (mSample.Position)
                {
                    case 2:
                        StartSourceActivity = mSample.Source.Activity;
                        StartRate = Math.Round((mSample.CountsInRegionOfInterest / mSample.Spectrum.LiveTimeInSeconds * 60), 0);
                        break;
                    case 21:
                    case 22:
                        double from;
                        double to;
                        CalculateQCRatioBounds(StartSourceActivity, mSample.Source.Activity, out from, out to);

                        if (Math.Round(StartRate / (mSample.CountsInRegionOfInterest / mSample.Spectrum.LiveTimeInSeconds * 60), 2) < from)
                        {
                            eMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.FailedButContinue, "Linearity: " + mSample.TypeOfSample +
                                                                                                                                      " measured ratio of rates is less than " + from);
                        }
                        else if (Math.Round(StartRate / (mSample.CountsInRegionOfInterest / mSample.Spectrum.LiveTimeInSeconds * 60), 2) > to)
                        {
                            eMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.FailedButContinue, "Linearity: " + mSample.TypeOfSample +
                                                                                                                                      " measured ratio of rates is greater than " + to);
                        }
                        break;
                }

                if (eMetadata != null)
                {
                    sample.ErrorDetails = eMetadata.Message;
                    AddEvalResult(eMetadata);
                    mSample.IsPassed = false;
                    mSample.IsEvaluationCompleted = true;
                }
                else
                {
                    mSample.IsPassed = true;
                    mSample.IsEvaluationCompleted = true;
                }
            }
            else if (mSample.TypeOfSample == SampleType.Background)
            {
                mSample.IsPassed = true;
                mSample.IsEvaluationCompleted = true;
            }

            // Return evaluation result
            if (eMetadata == null)
                return new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Pass);

            return eMetadata;
        }

        #endregion

        #region Helper methods

        protected virtual void ProcessSampleIfTestRunning(QcSample sample)
        {
            if (Status != Domain.Common.TestStatus.Running)
                return;


            sample.StartChannel = CalibrationCurve.GetChannel(QcDomainConstants.DampedBa133LowerEnergyBound);
            sample.EndChannel = CalibrationCurve.GetChannel(QcDomainConstants.DampedBa133UpperEnergyBound);

            sample.CountsInRegionOfInterest = (int) SpectroscopyService.ComputeIntegral(sample.Spectrum.SpectrumArray, sample.StartChannel, sample.EndChannel);
            sample.Centroid = Math.Round(SpectroscopyService.ComputeCentroid(sample.Spectrum.SpectrumArray, sample.StartChannel, sample.EndChannel), 2);
            sample.Centroid = CalibrationCurve.GetEnergy(sample.Centroid); // Convert to energy
            sample.FullWidthHalfMax = Math.Round(SpectroscopyService.ComputeFullWidthHalfMax(sample.Spectrum.SpectrumArray, sample.StartChannel, sample.EndChannel), 2);
            sample.FullWidthHalfMax = (sample.FullWidthHalfMax / sample.Centroid) * 100;  // Convert to a percent
            sample.FullWidthTenthMax = Math.Round(SpectroscopyService.ComputeFullWidthTenthMax(sample.Spectrum.SpectrumArray, sample.StartChannel, sample.EndChannel), 2);
            
            // Set if preset count limit reached
            sample.PresetCountLimitReached = sample.CountsInRegionOfInterest >= (MinimumCounts + 1000);
        }

        private void CalculateQCRatioBounds(double startSourceActivity, double nominalActivity, out double ratioLowbound, out double ratioHighbound)
        {
            const double ratioFactor = 0.22;

            var nominalRatio = Math.Round(startSourceActivity / nominalActivity, 2);
            var adjustmentFactor = Math.Round((nominalRatio * ratioFactor), 2);
            ratioLowbound = nominalRatio - adjustmentFactor;
            ratioHighbound = nominalRatio + adjustmentFactor;
        }

        #endregion
    }
}