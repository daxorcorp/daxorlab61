﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Domain.Services;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;

namespace Daxor.Lab.QC.Core
{
    public class QcLinearityTest : QcTest
    {
        #region Fields

        readonly Dictionary<TestType, QcTest> _subTests = new Dictionary<TestType, QcTest>();

        #endregion

        #region Ctors

        public QcLinearityTest(IRuleEngine ruleEngine) : this(IdentityGenerator.NewSequentialGuid(), ruleEngine) { }

        public QcLinearityTest(Guid testId, IRuleEngine ruleEngine)
            : base(testId, ruleEngine)
        {
            QCType = TestType.Linearity;
        }

        public QcLinearityTest(ISchemaProvider layoutProvider, IRuleEngine ruleEngine): this(ruleEngine)
        {
            if (layoutProvider == null)
                throw new ArgumentNullException("layoutProvider");

            //Builds tests
            _subTests.Add(TestType.Calibration, Factories.QcTestFactory.CreateTest(TestType.Calibration, layoutProvider, ruleEngine));
            _subTests.Add(TestType.Linearity, Factories.QcTestFactory.CreateTest(TestType.LinearityWithoutCalibration, layoutProvider, ruleEngine));
        }

        public QcLinearityTest(Guid testId, string systemId, ISchemaProvider layoutProvider, IRuleEngine ruleEngine): this(layoutProvider, ruleEngine)
        {
            // ReSharper disable once DoNotCallOverridableMethodsInConstructor
            InternalId = testId;
            SystemId = systemId;
        }

        public QcLinearityTest(Guid testId, IEnumerable<QcTest> subTests, IRuleEngine ruleEngine)
            : this(testId, ruleEngine)
        {
            if (subTests == null)
                throw new ArgumentNullException("subTests");

            foreach (var test in subTests)
                _subTests.Add(test.QCType, test);
        }

        #endregion

        #region Properties

        public IEnumerable<QcTest> SubTests
        {
            get { return _subTests.Values; }
        }

        #endregion

        #region Overrides

        public override void BuildSamples()
        {
            //Builds sub tests
            foreach (var test in _subTests)
            {
                test.Value.BuildSamples();
                
                //Unique positions
                var newSamples = from r in test.Value.Samples where InnerSamples.All(s => s.Position != r.Position) select r;
                InnerSamples.AddRange(newSamples);
            }

            SampleLayoutId = _subTests.First().Value.SampleLayoutId;
        }

        public override void ProcessSample(QcSample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");

            foreach (var subTest in _subTests)
            {
                var test = subTest.Value;

                if (!test.ContainsTypeOfSample(sample.TypeOfSample)) continue;
                test.ProcessSample(sample);

                //Internal sample
                var innerSample = (from s in InnerSamples where s.Position == sample.Position select s).FirstOrDefault();
                if (innerSample == null) continue;
                innerSample.CountsInRegionOfInterest = sample.CountsInRegionOfInterest;
                innerSample.FullWidthHalfMax = sample.FullWidthHalfMax;
                innerSample.FullWidthTenthMax = sample.FullWidthTenthMax;
                innerSample.Centroid = sample.Centroid;
                innerSample.ExecutionStatus = sample.ExecutionStatus;
                innerSample.IsPassed = sample.IsPassed;
                innerSample.SerialNumber = sample.SerialNumber;
                innerSample.Source = sample.Source;
                innerSample.Spectrum.SpectrumArray = sample.Spectrum.SpectrumArray;
                innerSample.PresetCountsLimit = innerSample.IsSource ? innerSample.Source.CountLimit : -1;
                innerSample.Spectrum.LiveTimeInSeconds = sample.Spectrum.LiveTimeInSeconds;
                innerSample.Spectrum.RealTimeInSeconds = sample.Spectrum.RealTimeInSeconds;
                innerSample.StartChannel = sample.StartChannel;
                innerSample.EndChannel = sample.EndChannel;
            }
        }

        public override QcSampleEvaluationMetadata EvaluateSample(QcSample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");
            if (sample.InternalId == null)
                throw new NullReferenceException("sample.InteralID");

            var eMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Undetermined);
            foreach (var t in _subTests)
            {
                var test = t.Value;

                if (!test.ContainsTypeOfSample(sample.TypeOfSample)) continue;

                var numEvals = test.EvaluationResultCount;
                   
                eMetadata = test.EvaluateSample(sample);

                if (numEvals != test.EvaluationResultCount)
                    RefreshEvaluationResults();
                    
                if (eMetadata.Result == QcSampleEvaluationResult.Failed)
                    break;

                //Update fine gain only if test is calibration test.
                if (test is QcCalibrationTest)
                    CurrentFineGain = test.CurrentFineGain;

                //If we just processed calibration test and it's finished, let's update each subtest with new calibration curve
                if (test is QcCalibrationTest && test.IsPassed)
                {
                    _subTests.Values.Where(sTest => sTest.QCType != TestType.Calibration).ToList().ForEach(sTest => sTest.CalibrationCurve = test.CalibrationCurve);
                    CalibrationCurve = test.CalibrationCurve;
                }
            }
            
            return eMetadata;
        }
       
        public override IEnumerable<QcSampleEvaluationMetadata> EvaluationResults
        {
            get
            {
                return InnerEvaluationResults;
            }
        }

        private void RefreshEvaluationResults()
        {
            InnerEvaluationResults.Clear();
            foreach (var test in _subTests)
                InnerEvaluationResults.AddRange(test.Value.EvaluationResults);

            EvaluationResultCount = InnerEvaluationResults.Count;
        }

        public override void ConfigureForExecution(IEnumerable<IConfigurator> configurators)
        {
            foreach (var test in _subTests.Where(test => test.Value != null))
                // ReSharper disable once PossibleMultipleEnumeration
                test.Value.ConfigureForExecution(configurators);
        }

        public override int GetDesiredExecutionTimeInSeconds(QcSample sample)
        {
            var maxAcquisitionTime = 0;

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var test in SubTests)
            {
                var tAcqTime = test.GetDesiredExecutionTimeInSeconds(sample);
                if (tAcqTime > maxAcquisitionTime)
                    maxAcquisitionTime = tAcqTime;
            }
            // ReSharper restore LoopCanBeConvertedToQuery

            return maxAcquisitionTime;
        }

        public override int GetEstimatedExecutionTimeInSeconds(QcSample sample)
        {
            var maxAcquisitionTime = 0;

            // ReSharper disable LoopCanBeConvertedToQuery
            foreach (var test in SubTests)
            {
                var tAcqTime = test.GetEstimatedExecutionTimeInSeconds(sample);
                if (tAcqTime > maxAcquisitionTime)
                    maxAcquisitionTime = tAcqTime;
            }
            // ReSharper restore LoopCanBeConvertedToQuery

            return maxAcquisitionTime;
        }

        protected override void FirePropertyChanged(string propertyName)
        {
            base.FirePropertyChanged(propertyName);
            if (propertyName == "InjectateLot")
            {
                foreach (var sTest in _subTests)
                    if (sTest.Value != null)
                        sTest.Value.InjectateLot = InjectateLot;
            }
            if (propertyName == "StandardAKey")
            {
                foreach (var sTest in _subTests)
                    if (sTest.Value != null)
                        sTest.Value.StandardAKey = StandardAKey;
            }
            if (propertyName == "StandardBKey")
            {
                foreach (var sTest in _subTests)
                    if (sTest.Value != null)
                        sTest.Value.StandardBKey = StandardBKey;
            }
            if (propertyName == "Status")
            {
                foreach (var sTest in _subTests)
                    if (sTest.Value != null)
                        sTest.Value.Status = Status;
            }
        }
      
        #endregion
    }
}
