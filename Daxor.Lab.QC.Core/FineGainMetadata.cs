﻿namespace Daxor.Lab.QC.Core
{
    public class FineGainMetadata
    {
        public bool IsFirstAdjustment { get; set; }
        public double MinFineGain { get; set; }
        public double MaxFineGain { get; set; }
        public double CurrentFineGain { get; set; }
    }
}
