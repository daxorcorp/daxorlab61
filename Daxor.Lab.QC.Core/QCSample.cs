﻿using System;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.QC.Core
{
    /// <summary>
    /// Quality control sample
    /// </summary>
    public class QcSample : Sample
    {
        #region Fields

        private double _fullWidthHalfMax, _fullWidthTenthMax, _centroid;
        private Int32 _countsInRegionOfInterest, _startChannel, _endChannel, _countsInWholeSpectrum;
        private SampleType _sampleType;
        private string _serialNumber = String.Empty;
        private bool? _isPassed;
        private bool _presetCountLimitReached;
        private int _presetCountsLimit = -1;
        private QcSource _source;
        private string _errorDetails;

        #endregion

        #region Ctors

        public QcSample() : this(Isotope.Empty()) { }

        public QcSample(Isotope containingIsotope)
        {
            Isotope = containingIsotope;
        }
        
        public QcSample(QcSource source)
        {
            _source = source;

            if (_source != null)
                Isotope = _source.Isotope;
        }
        
        #endregion

        #region Properties 

        public Isotope Isotope { get; set; }

        public double FullWidthHalfMax
        {
            get { return _fullWidthHalfMax; }
            set { SetValue(ref _fullWidthHalfMax, "FullWidthHalfMax", Math.Round(value, 2)); }
        }
        
        public double FullWidthTenthMax
        {
            get { return _fullWidthTenthMax; }
            set { SetValue(ref _fullWidthTenthMax, "FullWidthTenthMax", Math.Round(value, 2)); }
        }
        
        public double Centroid
        {
            get { return _centroid; }
            set { SetValue(ref _centroid, "Centroid", Math.Round(value, 2)); }
        }
        
        public int PresetCountsLimit
        {
            get { return _presetCountsLimit; }
            set { SetValue(ref _presetCountsLimit, "PresetCountsLimit", value); }
        }

        public int CountsInRegionOfInterest
        {
            get { return _countsInRegionOfInterest; }
            set
            {
                if (SetValue(ref _countsInRegionOfInterest, "CountsInRegionOfInterest", value))
                    base.FirePropertyChanged("CountsPerMinute");
            }
        }
        
        public int CountsInWholeSpectrum
        {
            get { return _countsInWholeSpectrum; }
            set { SetValue(ref _countsInWholeSpectrum, "CountsInWholeSpectrum", value); }
        }

        public int StartChannel
        {
            get { return _startChannel; }
            set { SetValue(ref _startChannel, "StartChannel", value); }
        }
        
        public int EndChannel
        {
            get { return _endChannel; }
            set { SetValue(ref _endChannel, "EndChannel", value); }
        }

        public virtual string SerialNumber
        {
            get { return _serialNumber; }
            set { SetValue(ref _serialNumber, "SerialNumber", value); }
        }
        
        public bool? IsPassed
        {
            get { return _isPassed; }
            set { SetValue(ref _isPassed, "IsPassed", value); }
        }
        
        public bool IsEvaluationCompleted
        {
            get;
            set;
        }
        
        public bool PresetCountLimitReached
        {
            get { return _presetCountLimitReached; }
            set { SetValue(ref _presetCountLimitReached, "PresetCountLimitReached", value); }
        }
       
        public bool IsSource
        {
            get { return _source != null; }
        }
        
        public QcSource Source
        {
            get { return _source; }
            set { _source = value; }
        }

        public SampleType TypeOfSample
        {
            get { return _sampleType; }
            set { SetValue(ref _sampleType, "TypeOfSample", value); }

        }

        public int SampleLayoutId { get; set; }

        public string ErrorDetails
        {
            get { return _errorDetails; }
            set { SetValue(ref _errorDetails, "ErrorDetails", value); }
        }

        public double CountsPerMinute
        {
            get
            {
                return Math.Round(CountsInRegionOfInterest * TimeConstants.SecondsPerMinute / Spectrum.LiveTimeInSeconds);
            }
        }

        #endregion

        protected override void FirePropertyChanged(string propertyName)
        {
            base.FirePropertyChanged(propertyName);
            if (propertyName == "LiveTimeInSeconds")
                FirePropertyChanged("CountsPerMinute");
        } 
    }
}
