﻿using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Core.Interfaces
{
    /// <summary>
    /// Provides schema of sample layout based on type of test
    /// </summary>
    public interface ISchemaProvider
    {
        //Returns sample layout schema
        SamplesSchema GetSamplesLayoutSchema(TestType qcTest);
    }
}
