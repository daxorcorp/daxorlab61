﻿using System;
using System.Collections.Generic;

namespace Daxor.Lab.QC.Core.Interfaces
{
    /// <summary>
    /// The peak finder
    /// </summary>
    public interface IPeakFinder
    {
        /// <summary>
        /// Finds peaks for a given spectrum, assuming expected FWHM of 14 channels
        /// </summary>
        /// <param name="spect">Spectrum used to find peaks</param>
        /// <returns>Collection of peaks found</returns>
        IEnumerable<Peak> FindPeaks(UInt32[] spect);

        /// <summary>
        /// Returns found peaks from spectrum within a given percent of max found signficance.
        /// </summary>
        /// <param name="spect">Spectrum used to find peaks</param>
        /// <param name="withinMaxSignificancePercent">Percent below the maximum signifance level of found peaks (example. 20, 25, 30, 40)</param>
        /// <returns>Collection of found peaks below withinMaxSignificancePercent</returns>
        IEnumerable<Peak> FindPeaks(UInt32[] spect, Int32 withinMaxSignificancePercent);

        /// <summary>
        /// Returns found peaks from spectrum within a given percent of max found signficance.
        /// </summary>
        /// <param name="spectrum">Spectrum used to find peaks</param>
        /// <param name="withinMaxSignificancePercent">Percent below the maximum signifance level of found peaks (example. 20, 25, 30, 40)</param>
        /// <param name="numHighSignificancePeaks">Number of highest-significance peaks to return</param>
        /// <returns>Collection of found peaks (with at most numHighSignificancePeaks members) 
        /// below withinMaxSignificancePercent sorted ascendingly</returns>
        IEnumerable<Peak> FindPeaksSortedByCentroid(UInt32[] spectrum, Int32 withinMaxSignificancePercent, int numHighSignificancePeaks);
    }
}
