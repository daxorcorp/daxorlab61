﻿using Daxor.Lab.QC.Core.Payloads;

namespace Daxor.Lab.QC.Core.Interfaces
{
    public interface ISampleEvaluator
    {
        PostEvaluationPayload EvaluateSample(PreEvaluationPayload payload);
    }
}
