﻿namespace Daxor.Lab.QC.Core.Interfaces
{
    public interface IFineGainAdjustmentService
    {
        double InitialFineGainAdjustment { get; set; }
        FineGainMetadata DetermineNewFineGainMetadata(double expectedCentroidInkeV, double actualCentroidInkeV, FineGainMetadata currentFineGainMetadata);
    }
}