﻿namespace Daxor.Lab.QC.Core.Interfaces
{
    /// <summary>
    /// Allows objects to be configured by some other entities
    /// </summary>
    public interface IConfigurator
    {
        void Configure(object test);
        bool CanConfigure(object test);
    }
}
