﻿using Daxor.Lab.QC.Core.Payloads;

namespace Daxor.Lab.QC.Core.Interfaces
{
    /// <summary>
    /// Represents an service that can process pre- and post- sample evaluation for the
    /// samples that are part of the calibration QC test
    /// </summary>
    public interface IQcCalibrationTestPayloadProcessor
    {
        /// <summary>
        /// Builds up all the inputs and instances needed to process the given sample for the
        /// given calibration test
        /// </summary>
        /// <param name="sample">Sample to be processed</param>
        /// <param name="test">Calibration test to which the sample belongs</param>
        /// <returns></returns>
        PreEvaluationPayload CreatePreEvaluationPayload(QcSample sample, QcCalibrationTest test);

        /// <summary>
        /// Takes the results of the sample evaluation and does any post-evaluation processing
        /// with respect to the given calibration test
        /// </summary>
        /// <param name="payload">Post-evaluation payload</param>
        /// <param name="test">Calibration test to which the sample that was evaluated belongs</param>
        void ProcessPostEvaluationPayload(PostEvaluationPayload payload, QcCalibrationTest test);
    }
}
