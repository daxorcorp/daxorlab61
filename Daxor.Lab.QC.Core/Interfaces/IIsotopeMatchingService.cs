﻿using System.Collections.Generic;

namespace Daxor.Lab.QC.Core.Interfaces
{
    public interface IIsotopeMatchingService
    {
        bool IsMatching(Isotope isotope, IEnumerable<Peak> peaks);
    }
}
