﻿namespace Daxor.Lab.QC.Core.Payloads
{
    public class BackgroundPostEvaluationPayload : PostEvaluationPayload
    {
        protected bool Equals(BackgroundPostEvaluationPayload other)
        {
            return Equals(Metadata, other.Metadata);
        }

        public override int GetHashCode()
        {
            return (Metadata != null ? Metadata.GetHashCode() : 0);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((BackgroundPostEvaluationPayload) obj);
        }
    }
}
