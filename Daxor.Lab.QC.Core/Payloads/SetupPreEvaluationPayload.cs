﻿using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Interfaces;

namespace Daxor.Lab.QC.Core.Payloads
{
    public class SetupPreEvaluationPayload : PreEvaluationPayload
    {
        public int SampleDurationInSeconds { get; set; }
        public bool IsFineGainFinishedAdjusting { get; set; }
        public int PriorSampleDurationInSeconds { get; set; }
        public int SetupSourceCountThreshold { get; set; }
        public double MinimumPassingSetupFullWidthHalfMax { get; set; }
        public double MaximumPassingSetupFullWidthHalfMax { get; set; }
        public int SetupSourceCountThresholdAcquisitionTimeInSeconds { get; set; }
        public uint NumberOfFineGainAdjustments { get; set; }
        public int MaxAllowableFineGainAdjustments { get; set; }
        public IPeakFinder PeakFinder { get; set; }
        public IIsotopeMatchingService IsotopeMatchingService { get; set; }
        public double SetupPeakEnergy { get; set; }
        public int IndexOfEnergyForSetupPeak { get; set; }
        public ISpectroscopyService SpectroscopyService { get; set; }
        public double SetupPrecisionInkeV { get; set; }
        public IFineGainAdjustmentService FineGainAdjustmentService { get; set; }
        public FineGainMetadata FineGainMetadata { get; set; }
        public double CurrentFineGain { get; set; }
    }
}
