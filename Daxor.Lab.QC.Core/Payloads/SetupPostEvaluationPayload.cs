﻿namespace Daxor.Lab.QC.Core.Payloads
{
    public class SetupPostEvaluationPayload : PostEvaluationPayload
    {
        public int SampleDurationInSeconds { get; set; }
        public bool IsFineGainFinishedAdjusting { get; set; }    
        public int PriorSampleDurationInSeconds { get; set; }
        public double SetupActualCentroidInkeV { get; set; }
        public FineGainMetadata FineGainMetadata { get; set; }
        public uint NumberOfFineGainAdjustments { get; set; }
        public double CurrentFineGain { get; set; }
    }
}
