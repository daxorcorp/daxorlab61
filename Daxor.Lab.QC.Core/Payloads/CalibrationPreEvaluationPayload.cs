﻿using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Interfaces;

namespace Daxor.Lab.QC.Core.Payloads
{
    public class CalibrationPreEvaluationPayload : PreEvaluationPayload
    {
        public IPeakFinder PeakFinder { get; set; }
        public ISpectroscopyService SpectroscopyService { get; set; }
        public double SetupActualCentroidInkeV { get; set; }
        public double SetupPeakEnergy { get; set; }
        public double CalibrationPeakEnergy { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((CalibrationPreEvaluationPayload) obj);
        }

        protected bool Equals(CalibrationPreEvaluationPayload other)
        {
            // ReSharper disable CompareOfFloatsByEqualityOperator
            return ReferenceEquals(PeakFinder, other.PeakFinder) &&
                   ReferenceEquals(SpectroscopyService, other.SpectroscopyService) && 
                   ReferenceEquals(Sample, other.Sample) &&
                   SetupActualCentroidInkeV == other.SetupActualCentroidInkeV && 
                   SetupPeakEnergy == other.SetupPeakEnergy && 
                   CalibrationPeakEnergy == other.CalibrationPeakEnergy;
            // ReSharper restore CompareOfFloatsByEqualityOperator
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (PeakFinder != null ? PeakFinder.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (SpectroscopyService != null ? SpectroscopyService.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ SetupActualCentroidInkeV.GetHashCode();
                hashCode = (hashCode * 397) ^ SetupPeakEnergy.GetHashCode();
                hashCode = (hashCode * 397) ^ CalibrationPeakEnergy.GetHashCode();
                return hashCode;
            }
        }
    }
}
