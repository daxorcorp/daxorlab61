﻿using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Core.Payloads
{
    public abstract class PostEvaluationPayload
    {
        public QcSampleEvaluationMetadata Metadata { get; set; }
    }
}
