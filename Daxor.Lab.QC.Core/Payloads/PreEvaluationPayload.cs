﻿namespace Daxor.Lab.QC.Core.Payloads
{
    public abstract class PreEvaluationPayload
    {
        public QcSample Sample { get; set; }
    }
}
