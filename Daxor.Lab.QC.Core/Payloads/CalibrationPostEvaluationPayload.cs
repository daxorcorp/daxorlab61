﻿using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.QC.Core.Payloads
{
    public class CalibrationPostEvaluationPayload : PostEvaluationPayload
    {
        public ICalibrationCurve CalibrationCurve { get; set; }
    }
}
