﻿using System;
using System.Linq;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Domain.Services;
using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Core
{
    /// <summary>
    /// Contamination Test
    /// </summary>
    public class QcContaminationTest : QcTest
    {
        private Double _countsPerMinuteCutoff;

        #region Ctor

        public QcContaminationTest(Guid testId, IRuleEngine ruleEngine)
            : base(testId, ruleEngine)
        {
            QCType = TestType.Contamination;
        }

        public QcContaminationTest(IRuleEngine ruleEngine)
            : this(IdentityGenerator.NewSequentialGuid(), ruleEngine)
        {
            _countsPerMinuteCutoff = -1;
        }

        public QcContaminationTest(SamplesSchema schema, IRuleEngine ruleEngine)
            : this(ruleEngine)
        {
            if (schema == null)
                throw new ArgumentNullException("schema");

            Schema = schema;
        }

        public QcContaminationTest(Guid testId, string systemId, SamplesSchema schema, IRuleEngine ruleEngine)
            : this(schema, ruleEngine)
        {
            // ReSharper disable once DoNotCallOverridableMethodsInConstructor
            InternalId = testId;
            SystemId = systemId;
        }

        #endregion

        #region Public properties

        public Double ContaminationMultiplier
        {
            get;
            set;
        }
        
        public Double DefaultContaminationCpm
        {
            get;
            set;
        }

        #endregion

        #region Override

        public override void ProcessSample(QcSample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");

            //If test is running, we will compute integral, etc, ourself
            ProcessSampleIfTestRunning(sample);

            var innerSample = (from s in InnerSamples where s.Position == sample.Position select s).FirstOrDefault();
            if (innerSample == null) return;

            innerSample.CountsInRegionOfInterest = sample.CountsInRegionOfInterest;
            innerSample.FullWidthHalfMax = sample.FullWidthHalfMax;
            innerSample.FullWidthTenthMax = sample.FullWidthTenthMax;
            innerSample.Centroid = sample.Centroid;
            innerSample.ExecutionStatus = sample.ExecutionStatus;
            innerSample.IsPassed = sample.IsPassed;
            innerSample.Spectrum.SpectrumArray = sample.Spectrum.SpectrumArray;
            innerSample.Spectrum.LiveTimeInSeconds = sample.Spectrum.LiveTimeInSeconds;
            innerSample.Spectrum.RealTimeInSeconds = sample.Spectrum.RealTimeInSeconds;
            innerSample.StartChannel = sample.StartChannel;
            innerSample.EndChannel = sample.EndChannel;
            innerSample.ErrorDetails = sample.ErrorDetails;
        }

        public override QcSampleEvaluationMetadata EvaluateSample(QcSample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");
            if (sample.InternalId == null)
                throw new NullReferenceException("sample.InteralID");

            var eMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Undetermined);

            //Use background to get evaluation done
            if (sample.TypeOfSample == SampleType.Background) 
            {
                if (sample.ExecutionStatus == Domain.Common.SampleExecutionStatus.Completed)
                {
                    var maxAcceptedCpm = sample.CountsPerMinute * ContaminationMultiplier;
                    _countsPerMinuteCutoff = Math.Max(DefaultContaminationCpm, maxAcceptedCpm);
                    sample.IsPassed = true;
                    eMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Pass);
                }
            }
            //All other samples are contamination only
            else if (sample.ExecutionStatus == Domain.Common.SampleExecutionStatus.Completed)
            {
                sample.IsPassed = sample.CountsPerMinute <= _countsPerMinuteCutoff;
                if (sample.IsPassed.Value)
                    eMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Pass);
                else
                {
                    eMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.FailedButContinue, "Counts per minute is greater than maximum allowed. (Max: " + _countsPerMinuteCutoff + " cpm).");
                    sample.ErrorDetails = eMetadata.Message;
                    AddEvalResult(eMetadata);
                }
            }

            //Execution status is completed
            sample.IsEvaluationCompleted = sample.ExecutionStatus == Domain.Common.SampleExecutionStatus.Completed;
            return eMetadata;
        }

        #endregion

        #region Helper methods

        protected virtual void ProcessSampleIfTestRunning(QcSample rSample)
        {
            try
            {
                if (Status != Domain.Common.TestStatus.Running)
                    return;
                if (rSample == null)
                    return;

                //Should be based on calibration curve....
                rSample.StartChannel = CalibrationCurve.GetChannel(QcDomainConstants.DampedBa133LowerEnergyBound);
                rSample.EndChannel = CalibrationCurve.GetChannel(QcDomainConstants.DampedBa133UpperEnergyBound);

                rSample.CountsInRegionOfInterest = (int) SpectroscopyService.ComputeIntegral(rSample.Spectrum.SpectrumArray, rSample.StartChannel, rSample.EndChannel);
                rSample.Centroid = Math.Round(SpectroscopyService.ComputeCentroid(rSample.Spectrum.SpectrumArray, rSample.StartChannel, rSample.EndChannel), 2);
                rSample.Centroid = CalibrationCurve.GetEnergy(rSample.Centroid); // Convert to energy
                rSample.FullWidthHalfMax = Math.Round(SpectroscopyService.ComputeFullWidthHalfMax(rSample.Spectrum.SpectrumArray, rSample.StartChannel, rSample.EndChannel), 2);
                rSample.FullWidthHalfMax = (rSample.FullWidthHalfMax / rSample.Centroid) * 100;  // Convert to a percent
                rSample.FullWidthTenthMax = Math.Round(SpectroscopyService.ComputeFullWidthTenthMax(rSample.Spectrum.SpectrumArray, rSample.StartChannel, rSample.EndChannel), 2);
            }
            catch { }
        }

        #endregion
    }
}
