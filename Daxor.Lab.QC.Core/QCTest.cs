﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;

namespace Daxor.Lab.QC.Core
{
    /// <summary>
    /// Abstract quality of control test, designed to evaluate system quality
    /// </summary>
    public abstract class QcTest : Test<QcSample>
    {
        #region Fields

        private TestType _type = TestType.Undefined;
        private string _injectateLot, _standardAKey, _standardBKey;
        private bool _injetateVerificationEnabled;
        private int _evalResultCount;
        private ICalibrationCurve _calCurve;

        #endregion

        #region Ctor

        protected QcTest(Guid testId, IRuleEngine ruleEngine)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InternalId = testId;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor

            RuleEngine = ruleEngine;
        }

        protected QcTest(Guid testId, IEnumerable<QcSampleEvaluationMetadata> existingEvalResults, IRuleEngine ruleEngine)
        {
            if (testId == null)
                throw new ArgumentNullException("testId");
            if (existingEvalResults == null)
                throw new ArgumentNullException("existingEvalResults");

            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InternalId = testId;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
            
            RuleEngine = ruleEngine;

            existingEvalResults.ToList().ForEach(AddEvalResult);
        }

        #endregion

        #region Properties

        public ISpectroscopyService SpectroscopyService { get; set; }
        public ICalibrationCurve CalibrationCurve
        {
            get { return _calCurve; }
            set { base.SetValue(ref _calCurve, "CalibrationCurve", value); }
        }

        public virtual IEnumerable<QcSampleEvaluationMetadata> EvaluationResults
        {
            get { return InnerEvaluationResults; }
        }
        public int EvaluationResultCount
        {
            get { return _evalResultCount; }
            protected set { base.SetValue(ref _evalResultCount, "EvaluationResultCount", value); }
        }
        public SamplesSchema Schema
        {
            get;
            protected set;
        }
        public TestType QCType
        {
            get { return _type; }
            protected set { base.SetValue(ref _type, "QCType", value); }
        }
        
        public string InjectateLot
        {
            get { return _injectateLot; }
            set { base.SetValue(ref _injectateLot, "InjectateLot", value); }
        }
        public string StandardAKey
        {
            get { return _standardAKey; }
            set { base.SetValue(ref _standardAKey, "StandardAKey", value); }
        }
        public string StandardBKey
        {
            get { return _standardBKey; }
            set { base.SetValue(ref _standardBKey, "StandardBKey", value); }
        }
        public bool InjectateVerificationEnabled
        {
            get { return _injetateVerificationEnabled; }
            set { base.SetValue(ref _injetateVerificationEnabled, "InjectateVerificationEnabled", value); }
        }
        public bool IsPassed
        {
            get
            {
                //If not samples, then test is not finished
                return InnerSamples != null && InnerSamples.All(s => s.IsPassed == true);
            }
        }

        public virtual double StartFineGain { get; set; }
        public virtual double CurrentFineGain { get; set; }
        public virtual double EndFineGain { get; set; }
        public virtual double Voltage { get; set; }

        /// <summary>
        /// Returns sample ordered 24, 25, 1, 2 ... 22, 23
        /// </summary>
        public override IEnumerable<QcSample> Samples
        {
            get
            {
                return from s in InnerSamples orderby ((s.Position + 1) % 25) select s;
            }
        }

        protected List<QcSampleEvaluationMetadata> InnerEvaluationResults = new List<QcSampleEvaluationMetadata>();

        #endregion

        #region Methods

        /// <summary>
        /// Returns an execution result for a given sample id
        /// </summary>
        /// <param name="sampleId">Sample Id</param>
        /// <returns></returns>
        public virtual IEnumerable<QcSampleEvaluationMetadata> GetEvaluationResultsForSample(Guid sampleId)
        {
            return from e in InnerEvaluationResults where e.SampleId == sampleId select e;
        }

        /// <summary>
        /// Processes sample; computes integral, fwhm, fwtm, centroid, etc.
        /// </summary>
        /// <param name="sample">Sample to process</param>
        public abstract void ProcessSample(QcSample sample);

        /// <summary>
        /// Evaluates sample, during execution
        /// </summary>
        /// <param name="sample">Sample to evaluate</param>
        /// <returns>Evaluation result</returns>
        public abstract QcSampleEvaluationMetadata EvaluateSample(QcSample sample);

        /// <summary>
        /// Builds test specific samples based on sample layout schema
        /// </summary>
        public virtual void BuildSamples()
        {
            if (Schema == null)
                throw new NullReferenceException("BuildSamples: Missing sample schema.");

            if (Status != Domain.Common.TestStatus.Pending)
                throw new NotSupportedException("BuildSamples: TestStatus is not Pending.");

            InnerSamples.Clear();

            foreach (var sItem in Schema.Items)
            {
                var sample = new QcSample(Factories.IsotopeFactory.CreateIsotope(sItem.ContainingIsotope))
                    {
                        Position = sItem.Position,
                        SerialNumber = sItem.SerialNumber,
                        TypeOfSample = sItem.Type,
                        DisplayName = sItem.FriendlyName,
                        SampleLayoutId = Schema.Id
                    };
                InnerSamples.Add(sample);
            }

            SampleLayoutId = Schema.Id;
        }

        /// <summary>
        /// Assigns sources to desired samples based on sample position
        /// </summary>
        /// <param name="sources"></param>
        public virtual void AssignSourcesToSamples(IEnumerable<QcSource> sources)
        {
            // ReSharper disable PossibleMultipleEnumeration
            if (sources == null)
                throw new ArgumentNullException("sources");
            if (!sources.Any())
                throw new NotSupportedException("Cannot assign sources to samples when there are no sources");
            if (InnerSamples == null || InnerSamples.Count == 0)
                throw new NotSupportedException("Samples cannot be null/empty.");

            //Sources to samples mappings
            var sourceWithSampleQuery = (from s in InnerSamples
                                        join src in sources on s.Position equals src.SamplePosition into outer
                                        from src in outer.DefaultIfEmpty()
                                        select new { Sample = s, Source = src }).ToList();
            // ReSharper restore PossibleMultipleEnumeration

            if (sourceWithSampleQuery.Count > 0)
            {
                sourceWithSampleQuery.ForEach(s =>
                {
                    //If we have a source for a given sample
                    if (s.Source != null)
                    {
                        s.Sample.Source = s.Source;
                        s.Sample.PresetCountsLimit = s.Source.CountLimit;
                        s.Sample.SerialNumber = s.Source.SerialNumber;
                    }
                });
            }
        }
       
        /// <summary>
        /// Computes background count per minute rate.
        /// </summary>
        /// <returns>Background counts per minute (cpm)</returns>
        public virtual double BackgroundCountsPerMinute()
        {
            return (BackgroundCount / BackgroundTimeInSeconds) * 60;
        }

        public void AddEvalResult(QcSampleEvaluationMetadata eMetadata)
        {
            if (eMetadata == null)
                throw new ArgumentNullException("eMetadata");

            InnerEvaluationResults.Add(eMetadata);
            EvaluationResultCount = InnerEvaluationResults.Count;
        }

        public void RemoveAllEvalResults()
        {
            InnerEvaluationResults.Clear();
            EvaluationResultCount = InnerEvaluationResults.Count;
        }

        public bool ContainsTypeOfSample(SampleType type)
        {
            return InnerSamples != null && InnerSamples.Any(s => s.TypeOfSample == type);
        }

        /// <summary>
        /// Configures test with appropriate parameters/settings values for execution
        /// </summary>
        /// <param name="configurators">Collection of available configurators</param>
        public virtual void ConfigureForExecution(IEnumerable<IConfigurator> configurators)
        {
            foreach (var confg in configurators)
            {
                if (confg.CanConfigure(this))
                    confg.Configure(this);
            }
        }
        public virtual int GetDesiredExecutionTimeInSeconds(QcSample sample)
        {
            var exeTime = 0;
            if (sample != null && ContainsTypeOfSample(sample.TypeOfSample))
                exeTime = SampleDurationInSeconds;
            return exeTime;
        }

        public virtual int GetEstimatedExecutionTimeInSeconds(QcSample sample)
        {
            return GetDesiredExecutionTimeInSeconds(sample);
        }

        #endregion

        #region Protected

        /// <summary>
        /// Determines if a sample is part of the test
        /// </summary>
        /// <param name="sample">Sample to check against</param>
        /// <returns>Returns true if a given sample is part of the test, false otherwise</returns>
        protected virtual bool ContainsSample(QcSample sample)
        {
            return InnerSamples.Any(s => s.InternalId == sample.InternalId);
        }

        #endregion
    }
}
