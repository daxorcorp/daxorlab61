using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Interfaces;

namespace Daxor.Lab.QC.Core
{
    /// <summary>
    /// Abstract test configuration base.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class TestConfigurator<T> : IConfigurator where T: ITest
    {
        public void Configure(object test)
        {
            ConfigureTest((T)test);
        }

        public virtual bool CanConfigure(object test)
        {
            return test is T;
        }
        
        protected abstract void ConfigureTest(T test);
    }
}