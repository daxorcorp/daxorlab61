﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.QC.Core
{
    /// <summary>
    /// Test execution stategy
    /// </summary>
    public interface IExecutionStrategy
    {
        event EventHandler ProgressChanged;
        event EventHandler FineGainAdjusted;
    }
}
