﻿using System;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Domain.Services;
using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Core
{
    /// <summary>
    /// Quality Control test capable of perfoming test on standards
    /// standards are of the sample type of isotope, Iodine133
    /// </summary>
    public class QcStandardsTest : QcTest
    {
        #region Ctor

        public QcStandardsTest(Guid testId, IRuleEngine ruleEngine) : base(testId, ruleEngine)
        {
            QCType = TestType.Standards;
        }

        public QcStandardsTest(IRuleEngine ruleEngine) : this(IdentityGenerator.NewSequentialGuid(), ruleEngine) { }

        public QcStandardsTest(SamplesSchema schema, IRuleEngine ruleEngine): this(ruleEngine)
        {
            if (schema == null)
                throw new ArgumentNullException("schema");

            Schema = schema;
        }

        public QcStandardsTest(Guid testId, string systemId, SamplesSchema schema, IRuleEngine ruleEngine): this(schema, ruleEngine)
        {
            // ReSharper disable once DoNotCallOverridableMethodsInConstructor
            InternalId = testId;
            SystemId = systemId;
        }

        #endregion

        #region Properties 

        public double MinimumAllowableCountPerMinute { get; set; }
        public double StandardsDeviationMultiplier { get; set; }

        #endregion

        #region Overrides

        /// <summary>
        /// For StandardTest the following evaluation need to happen
        /// 1. Evaluate each sample based on StandardMinimumPassableCPM, if CPM of a given standard is below StandardMinimumPassableCPM, the result is fail
        /// 2. When all samples finished acquiring we need to check the difference between their actual counts
        /// if they are greater than StandardDev of highest Counts => sqrt(maxCounts) * STD_DEV_DIFFERENCE_MULTIPLIER => failed
        /// </summary>
        /// <param name="sample"></param>
        public override void ProcessSample(QcSample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");

            ProcessSampleIfTestRunning(sample);

            var innerSample = (from s in InnerSamples where s.Position == sample.Position select s).FirstOrDefault();
            if (innerSample == null) return;

            innerSample.CountsInRegionOfInterest = sample.CountsInRegionOfInterest;
            innerSample.FullWidthHalfMax = sample.FullWidthHalfMax;
            innerSample.FullWidthTenthMax = sample.FullWidthTenthMax;
            innerSample.Centroid = sample.Centroid;
            innerSample.ExecutionStatus = sample.ExecutionStatus;
            innerSample.IsPassed = sample.IsPassed;
            innerSample.Spectrum.SpectrumArray = sample.Spectrum.SpectrumArray;
            innerSample.Spectrum.LiveTimeInSeconds = sample.Spectrum.LiveTimeInSeconds;
            innerSample.Spectrum.RealTimeInSeconds = sample.Spectrum.RealTimeInSeconds;
            innerSample.StartChannel = sample.StartChannel;
            innerSample.EndChannel = sample.EndChannel;
            innerSample.ErrorDetails = sample.ErrorDetails;
        }

        public override QcSampleEvaluationMetadata EvaluateSample(QcSample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");
            if (sample.InternalId == null)
                throw new NullReferenceException("sample.InteralID");

            var evaluationMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Undetermined);

            //Background evaluation is completed, no special processing
            if (sample.TypeOfSample == SampleType.Background)
            {
                if (sample.ExecutionStatus != SampleExecutionStatus.Completed) return evaluationMetadata;

                sample.IsPassed = true;
                sample.IsEvaluationCompleted = true;
            }

            //All other samples in standard only test (Standard A and Standard B)
            else if (sample.ExecutionStatus == SampleExecutionStatus.Completed)
            {
                if (sample.CountsPerMinute < MinimumAllowableCountPerMinute)
                {
                    evaluationMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.FailedButContinue, "Counts per minute is below a minimum allowable rate. (MinCPM - " + MinimumAllowableCountPerMinute + ")");
                    sample.ErrorDetails = evaluationMetadata.Message;
                    AddEvalResult(evaluationMetadata);
                    sample.IsPassed = false;
                    sample.IsEvaluationCompleted = true;
                }
                else
                {
                    evaluationMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Pass);
                    sample.IsPassed = null;
                    sample.IsEvaluationCompleted = true;
                }

                // All samples must be completed before final evaluation
                if (InnerSamples.Any(s => s.ExecutionStatus != SampleExecutionStatus.Completed)) 
                    return evaluationMetadata;
                
                // If acquisition of all samples is completed, let's do final evaluation
                var standardA = InnerSamples.Where(s => s.TypeOfSample == SampleType.StandardA).Select(s => s).FirstOrDefault();
                if (standardA == null)
                {
                    sample.IsPassed = false;
                    sample.IsEvaluationCompleted = true;
                    return null;
                }

                // 1. Check if both samples passed minimum allowable counts, don't proceed otherwise
                if ((standardA.IsPassed.HasValue && !standardA.IsPassed.Value) ||
                    sample.IsPassed.HasValue && !sample.IsPassed.Value)
                {
                    //None of the samples are passed.
                    standardA.IsPassed = sample.IsPassed = false;
                    
                    // ReSharper disable ConvertIfStatementToConditionalTernaryExpression
                    if (sample.CountsPerMinute < MinimumAllowableCountPerMinute)
                    {
                        sample.ErrorDetails = evaluationMetadata.Message;
                    }
                    else
                    {
                        sample.ErrorDetails = "Difference between Standards A and B counts exceeds maximum allowable difference";
                    }

                    if (standardA.CountsPerMinute < MinimumAllowableCountPerMinute)
                    {
                        standardA.ErrorDetails = evaluationMetadata.Message;
                    }
                    else
                    {
                        standardA.ErrorDetails = "Difference between Standards A and B counts exceeds maximum allowable difference";
                    }
                    // ReSharper restore ConvertIfStatementToConditionalTernaryExpression

                    return evaluationMetadata;
                }

                // Find maximum between two samples
                var maximumAllowableDifference = StatisticsHelpers.ComputeAllowableDifference(standardA.CountsInRegionOfInterest,
                    sample.CountsInRegionOfInterest);

                double actualCountDifference = Math.Abs(standardA.CountsInRegionOfInterest - sample.CountsInRegionOfInterest);

                // Determine if we failed and why
                if (actualCountDifference > maximumAllowableDifference)
                {
                    standardA.IsPassed = false;
                    sample.IsPassed = false;

                    evaluationMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.FailedButContinue, "Difference between Standards A and B counts exceeds maximum allowable difference (Max " + maximumAllowableDifference + ")");
                    sample.ErrorDetails = evaluationMetadata.Message;
                    standardA.ErrorDetails = evaluationMetadata.Message;
                    AddEvalResult(evaluationMetadata);
                }
                else
                {
                    evaluationMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Pass);
                    standardA.IsPassed = true;
                    sample.IsPassed = true;
                }

                sample.IsEvaluationCompleted = true;
            }

            return evaluationMetadata;
        }


        protected override void FirePropertyChanged(string propertyName)
        {
            base.FirePropertyChanged(propertyName);

            //Deal with standardAKey, BKey and injectate lot
            if (propertyName == "StandardAKey" && InnerSamples.Count > 0 && InjectateVerificationEnabled)
            {
                var standardA = InnerSamples.FirstOrDefault(s => s.TypeOfSample == SampleType.StandardA);

                if (standardA != null)
                    standardA.SerialNumber = StandardAKey;
            }
            else if (propertyName == "StandardBKey" && InnerSamples.Count > 0 && InjectateVerificationEnabled)
            {
                var standardB = InnerSamples.FirstOrDefault(s => s.TypeOfSample == SampleType.StandardB);

                if (standardB != null)
                    standardB.SerialNumber = StandardAKey;
            }
            else if (propertyName == "InjectateLot" && InnerSamples.Count > 0 && !InjectateVerificationEnabled)
            {
                InnerSamples.Where(s => s.TypeOfSample != SampleType.Background).ToList().ForEach(s => s.SerialNumber = InjectateLot);
            }
        }

        #endregion

        #region Helper methods

        protected virtual void ProcessSampleIfTestRunning(QcSample sample)
        {
            if (Status != TestStatus.Running)
                return;
            if (sample == null)
                return;

            try
            {
                //Should be based on calibration curve....
                sample.StartChannel = CalibrationCurve.GetChannel(QcDomainConstants.DampedBa133LowerEnergyBound);
                sample.EndChannel = CalibrationCurve.GetChannel(QcDomainConstants.DampedBa133UpperEnergyBound);

                sample.CountsInWholeSpectrum = (int) SpectroscopyService.ComputeIntegral(sample.Spectrum.SpectrumArray, 1, 1023);
                sample.CountsInRegionOfInterest = (int) SpectroscopyService.ComputeIntegral(sample.Spectrum.SpectrumArray, sample.StartChannel, sample.EndChannel);
                sample.Centroid = Math.Round(SpectroscopyService.ComputeCentroid(sample.Spectrum.SpectrumArray, sample.StartChannel, sample.EndChannel), 2);
                sample.Centroid = CalibrationCurve.GetEnergy(sample.Centroid); // Convert to energy
                sample.FullWidthHalfMax = Math.Round(SpectroscopyService.ComputeFullWidthHalfMax(sample.Spectrum.SpectrumArray, sample.StartChannel, sample.EndChannel), 2);
                sample.FullWidthHalfMax = (sample.FullWidthHalfMax / sample.Centroid) * 100;  // Convert to a percent
                sample.FullWidthTenthMax = Math.Round(SpectroscopyService.ComputeFullWidthTenthMax(sample.Spectrum.SpectrumArray, sample.StartChannel, sample.EndChannel), 2);
            }
            catch { }
        }

        #endregion
    }
}
