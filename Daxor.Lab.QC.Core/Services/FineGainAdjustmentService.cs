﻿using System;
using Daxor.Lab.QC.Core.Interfaces;

namespace Daxor.Lab.QC.Core.Services
{
    public class FineGainAdjustmentService : IFineGainAdjustmentService
    {
        #region Constants

        public const string AdjustmentBelowMinimumFormatString = "New fine gain ({0}) would be below the minimum ({1})";
        public const string AdjustmentAboveMaximumFormatString = "New fine gain ({0}) would be above the maximum ({1})";
        public const double CentroidEqualityThreshold = 1E-6;
        public const int NumberOfDecimalPlacesForRounding = 3;
        public static readonly double MinimumAdjustment = Math.Pow(10, -NumberOfDecimalPlacesForRounding);
        
        public double InitialFineGainAdjustment { get; set; }

        #endregion

        public FineGainMetadata DetermineNewFineGainMetadata(double expectedCentroidInkeV, double actualCentroidInkeV, FineGainMetadata currentFineGainMetadata)
        {
            if (currentFineGainMetadata == null) throw new ArgumentNullException("currentFineGainMetadata");
            var fineGainAdjustment = InitialFineGainAdjustment;

            currentFineGainMetadata.CurrentFineGain = Math.Round(currentFineGainMetadata.CurrentFineGain, NumberOfDecimalPlacesForRounding, 
                MidpointRounding.AwayFromZero);
            var newFineGainMetadata = new FineGainMetadata
            {
                MaxFineGain = currentFineGainMetadata.MaxFineGain,
                MinFineGain = currentFineGainMetadata.MinFineGain,
                CurrentFineGain = currentFineGainMetadata.CurrentFineGain
            };

            if (Math.Abs(expectedCentroidInkeV - actualCentroidInkeV) < CentroidEqualityThreshold)
                return newFineGainMetadata;

            if (!currentFineGainMetadata.IsFirstAdjustment)
            {
                if (actualCentroidInkeV > expectedCentroidInkeV)  // Decrease fine gain
                    newFineGainMetadata.MaxFineGain = currentFineGainMetadata.CurrentFineGain;
                else 
                    newFineGainMetadata.MinFineGain = currentFineGainMetadata.CurrentFineGain;

                fineGainAdjustment = Math.Abs((currentFineGainMetadata.CurrentFineGain - ((newFineGainMetadata.MaxFineGain + newFineGainMetadata.MinFineGain) / 2.0)));
            }

            // Negative or position fine gain adjustment based on actual vs. expected
            var fineGainDifference = fineGainAdjustment * ((actualCentroidInkeV > expectedCentroidInkeV) ? -1 : 1);

            if (Math.Abs(fineGainDifference) < MinimumAdjustment)
                fineGainDifference = 0;

            var newFineGain = newFineGainMetadata.CurrentFineGain + fineGainDifference;
            if (newFineGain < newFineGainMetadata.MinFineGain)
                throw new InvalidOperationException(String.Format(AdjustmentBelowMinimumFormatString, newFineGain, currentFineGainMetadata.MinFineGain));
            if (newFineGain > newFineGainMetadata.MaxFineGain)
                throw new InvalidOperationException(String.Format(AdjustmentAboveMaximumFormatString, newFineGain, currentFineGainMetadata.MaxFineGain));
            
            newFineGainMetadata.CurrentFineGain = newFineGain;
            return newFineGainMetadata;
        }
    }
}
