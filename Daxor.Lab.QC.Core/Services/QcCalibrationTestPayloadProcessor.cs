﻿using System;
using System.Linq;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Payloads;

namespace Daxor.Lab.QC.Core.Services
{
    public class QcCalibrationTestPayloadProcessor : IQcCalibrationTestPayloadProcessor
    {
        public PreEvaluationPayload CreatePreEvaluationPayload(QcSample sample, QcCalibrationTest test)
        {
            if (sample == null) throw new ArgumentNullException("sample");
            if (test == null) throw new ArgumentNullException("test");

            if (sample.TypeOfSample == SampleType.Background)
                return new BackgroundPreEvaluationPayload { Sample = sample };
            if (sample.TypeOfSample == SampleType.QC1)
                return CreateSetupPreEvaluationPayload(sample, test);
            if (sample.TypeOfSample == SampleType.QC2)
                return CreateCalibrationPreEvaluationPayload(sample, test);

            throw new NotSupportedException("There is no pre-evaluation payload available for the type '" + sample.TypeOfSample + "'");
        }

        public void ProcessPostEvaluationPayload(PostEvaluationPayload payload, QcCalibrationTest test)
        {
            if (payload is BackgroundPostEvaluationPayload) return;

            if (payload is SetupPostEvaluationPayload)
                ProcessSetupPostEvaluationPayload(payload, test);
            else if (payload is CalibrationPostEvaluationPayload)
                ProcessCalibrationPostEvaluationPayload(payload, test);
            else
                throw new NotSupportedException("There is no way to process post-evaluation payload for type '" + payload.GetType() + "'");
        }

        #region Private helpers

        private static double ComputeActualCentroidInkeV(QcTest test)
        {
            var setupSample = test.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC1);
            if (setupSample == null) throw new InvalidOperationException("Unable to find the sample type of 'QC1' on the test");

            return test.SpectroscopyService.ComputeCentroid(setupSample.Spectrum.SpectrumArray,
                setupSample.StartChannel, setupSample.EndChannel);
        }

        #endregion

        #region Private pre-eval helpers

        private static PreEvaluationPayload CreateSetupPreEvaluationPayload(QcSample sample, QcCalibrationTest test)
        {
            return new SetupPreEvaluationPayload
            {
                Sample = sample,
                SampleDurationInSeconds = test.SampleDurationInSeconds,
                IsFineGainFinishedAdjusting = test.IsFineGainFinishedAdjusting,
                PriorSampleDurationInSeconds = test.PriorSampleDurationInSeconds,
                SetupSourceCountThreshold = test.SetupSourceCountThreshold,
                MinimumPassingSetupFullWidthHalfMax = test.MinimumPassingSetupFullWidthHalfMax,
                MaximumPassingSetupFullWidthHalfMax = test.MaximumPassingSetupFullWidthHalfMax,
                SetupSourceCountThresholdAcquisitionTimeInSeconds = test.SetupSourceCountThresholdAcquisitionTimeInSeconds,
                NumberOfFineGainAdjustments = test.NumberOfFineGainAdjustments,
                MaxAllowableFineGainAdjustments = test.MaxAllowableFineGainAdjustments,
                PeakFinder = test.PeakFinder,
                IsotopeMatchingService = test.IsotopeMatchingService,
                SetupPeakEnergy = test.SetupPeakEnergy,
                IndexOfEnergyForSetupPeak = test.IndexOfEnergyForSetupPeak,
                SpectroscopyService = test.SpectroscopyService,
                SetupPrecisionInkeV = test.SetupPrecisionInkeV,
                FineGainAdjustmentService = test.FineGainAdjustmentService,
                FineGainMetadata = test.FineGainMetadata,
                CurrentFineGain = test.CurrentFineGain
            };
        }

        private static PreEvaluationPayload CreateCalibrationPreEvaluationPayload(QcSample sample, QcCalibrationTest test)
        {
            return new CalibrationPreEvaluationPayload
            {
                Sample = sample,
                PeakFinder = test.PeakFinder,
                SpectroscopyService = test.SpectroscopyService,
                SetupActualCentroidInkeV = ComputeActualCentroidInkeV(test),
                SetupPeakEnergy = test.SetupPeakEnergy,
                CalibrationPeakEnergy = test.CalibrationPeakEnergy,
            };
        }

        #endregion

        #region Private post-processor helpers

        private static void ProcessSetupPostEvaluationPayload(PostEvaluationPayload payload, QcCalibrationTest test)
        {
            var setupPostEvaluationPayload = payload as SetupPostEvaluationPayload;

            // ReSharper disable once PossibleNullReferenceException
            if (setupPostEvaluationPayload.Metadata.Result == QcSampleEvaluationResult.Failed)
                test.AddEvalResult(setupPostEvaluationPayload.Metadata);

            test.SampleDurationInSeconds = setupPostEvaluationPayload.SampleDurationInSeconds;
            test.IsFineGainFinishedAdjusting = setupPostEvaluationPayload.IsFineGainFinishedAdjusting;
            test.PriorSampleDurationInSeconds = setupPostEvaluationPayload.PriorSampleDurationInSeconds;
            test.SetupActualCentroidInkeV = setupPostEvaluationPayload.SetupActualCentroidInkeV;
            test.FineGainMetadata = setupPostEvaluationPayload.FineGainMetadata;
            test.NumberOfFineGainAdjustments = setupPostEvaluationPayload.NumberOfFineGainAdjustments;
            test.CurrentFineGain = setupPostEvaluationPayload.CurrentFineGain;
        }

        private static void ProcessCalibrationPostEvaluationPayload(PostEvaluationPayload payload, QcTest test)
        {
            var calibrationPayload = payload as CalibrationPostEvaluationPayload;

            // ReSharper disable once PossibleNullReferenceException
            if (calibrationPayload.Metadata.Result == QcSampleEvaluationResult.FailedButContinue)
                test.AddEvalResult(calibrationPayload.Metadata);
            else if (calibrationPayload.Metadata.Result == QcSampleEvaluationResult.Pass)
            {
                test.RemoveAllEvalResults();
                test.CalibrationCurve = calibrationPayload.CalibrationCurve;
            }
        }

        #endregion
    }
}
