﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.QC.Core.Interfaces;

namespace Daxor.Lab.QC.Core.Services
{
    public class IsotopeMatchingService : IIsotopeMatchingService
    {
        #region IIsotopeMatchingService Members

        public bool IsMatching(Isotope isotope, IEnumerable<Peak> peaks)
        {
            if (isotope == null) throw new ArgumentNullException("isotope");
            if (peaks == null) throw new ArgumentNullException("peaks");

            var numOfExpectedPeaks = isotope.Energies.Count();

            // check if we have enough peaks in the found spectrum
            // ReSharper disable PossibleMultipleEnumeration
            if (peaks.Count() != numOfExpectedPeaks)
                return false;

            // get the most significant peaks (top signature.NumberOfPeaks)
            var maxCentroid = peaks.Last().Centroid;
            var listofPeaks = peaks.ToList();
            // ReSharper restore PossibleMultipleEnumeration

            // ReSharper disable LoopCanBeConvertedToQuery
            for (int i = 0; i < listofPeaks.Count; i++)
            {
                var currRatio = listofPeaks[i].Centroid / maxCentroid;

                // compare if ratio of a given peak compared to the heighest peak
                // is within 5% of the expected ratio for that peak
                if ((Math.Abs(currRatio - isotope.RatiosOfEnergies.ElementAt(i)) < 0.05) == false)
                    return false;
            }
            // ReSharper restore LoopCanBeConvertedToQuery

            return true;
        }

        #endregion
    }
}
