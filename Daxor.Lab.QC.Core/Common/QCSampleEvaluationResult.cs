namespace Daxor.Lab.QC.Core.Common
{
    public enum QcSampleEvaluationResult
    {
        FailedButContinue,
        FailedButRepeat,
        Failed,
        Pass,
        Undetermined,
    }
}