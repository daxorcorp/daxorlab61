﻿namespace Daxor.Lab.QC.Core.Common
{
	public enum SampleType
	{
        // ReSharper disable InconsistentNaming
        Background,
		QC1,
		QC2,
		StandardA,
		StandardB,
		Contamination,
		QC3,
		QC4,
		Undefined,
        // ReSharper restore InconsistentNaming
	}
}
