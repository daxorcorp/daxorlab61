﻿namespace Daxor.Lab.QC.Core.Common
{
    public enum UnitOfTime
    {
        Minute,
        Hour,
        Day,
        Month,
        Year,
    }
}
