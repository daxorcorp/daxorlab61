﻿namespace Daxor.Lab.QC.Core.Common
{
    /// <summary>
    /// Supported Isotopes used in the system
    /// Isotopes are only used in QC BOUNDED-CONTEXT (DDD)
    /// </summary>
    public enum IsotopeType
    {
        Eu152,
        Cs137,
        Ba133,
        I131,
        Empty,
    }
}
