﻿using System;

namespace Daxor.Lab.QC.Core.Common
{
    public class QcSampleEvaluationMetadata
    {
        public QcSampleEvaluationMetadata(Guid sampleId, QcSampleEvaluationResult result, string message = "")
        {
            SampleId = sampleId;
            Result = result;
            Message = message;
        }

        public string Message { get; private set; }
        public Guid SampleId { get; private set; }
        public QcSampleEvaluationResult Result { get; private set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((QcSampleEvaluationMetadata) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Message != null ? Message.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ SampleId.GetHashCode();
                hashCode = (hashCode*397) ^ (int) Result;
                return hashCode;
            }
        }

        protected bool Equals(QcSampleEvaluationMetadata other)
        {
             return string.Equals(Message, other.Message) && SampleId.Equals(other.SampleId) && Result == other.Result;
        }

        public override string ToString()
        {
            return "Sample ID: " + SampleId + "; Message: " + Message + "; Result: " + Result;
        }
    }
}
