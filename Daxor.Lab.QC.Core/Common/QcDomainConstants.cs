﻿namespace Daxor.Lab.QC.Core.Common
{
    public static class QcDomainConstants
    {
        public const double DampedBa133LowerEnergyBound = 308;
        public const double DampedBa133UpperEnergyBound = 420;
    }
}
