﻿namespace Daxor.Lab.QC.Core.Common
{
    /// <summary>
    /// Type of QC test
    /// </summary>
    public enum TestType
    {
        Full = 0,
        Standards = 1,
        Contamination = 2,
        Linearity = 3,
        Calibration = 4,
        LinearityWithoutCalibration = 5,
        Undefined = 6,
    }
}
