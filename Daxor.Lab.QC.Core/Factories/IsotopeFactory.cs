﻿using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Core.Factories
{
    /// <summary>
    /// Factory creating Isotopes used in QC (maybe will be moved to Shared Domain Layer based on what other modules
    /// might need)
    /// </summary>
    public static class IsotopeFactory
    {
        public static Isotope CreateIsotope(IsotopeType iType)
        {
            Isotope isotope;

            switch (iType)
            {
                case IsotopeType.Ba133:

                    isotope = new Isotope(iType, 10.500, UnitOfTime.Year, new[] { 31.60, 81.00, 356.00 });
                    break;
                
                case IsotopeType.Cs137:

                    isotope = new Isotope(iType, 30.000, UnitOfTime.Year, new[] { 32.85, 661.66 });
                    break;

                case IsotopeType.Eu152:
                    isotope = new Isotope(iType, 13.400, UnitOfTime.Year, new[] { 41.00, 122.00, 344.28 });
                    break;

                case IsotopeType.I131:
                    isotope = new Isotope(iType, 8.021, UnitOfTime.Day, new[] { 284.30, 364.48, 637.14 });
                    break;

                default: 
                    isotope = Isotope.Empty();
                    break;
            }

            return isotope;
        }
    }
}
