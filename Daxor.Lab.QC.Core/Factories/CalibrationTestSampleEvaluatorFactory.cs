﻿using System;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.SampleEvaluators;

namespace Daxor.Lab.QC.Core.Factories
{
    public static class CalibrationTestSampleEvaluatorFactory
    {
        public static ISampleEvaluator GetSampleEvaluator(QcSample sampleToEvaluate)
        {
            if (sampleToEvaluate == null) throw new ArgumentNullException("sampleToEvaluate");

            if (sampleToEvaluate.TypeOfSample == SampleType.Background)
                return new BackgroundSampleEvaluator();
            if (sampleToEvaluate.TypeOfSample == SampleType.QC1)
                return new SetupSampleEvaluator();
            if (sampleToEvaluate.TypeOfSample == SampleType.QC2)
                return new CalibrationSampleEvaluator();

            throw new NotSupportedException("Could not find sample evaluator for sample type " +
                                          sampleToEvaluate.TypeOfSample);
        }

    }
}
