﻿using System;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;

namespace Daxor.Lab.QC.Core.Factories
{
    /// <summary>
    /// Factory for creating QCTests
    /// </summary>
    public static class QcTestFactory
    {
        /// <summary>
        /// Creates QCTest based on type, using schemaProvider
        /// </summary>
        /// <param name="type">Type of QCTest</param>
        /// <param name="schemaProvider">Sample schema provider</param>
        /// <param name="ruleEngine">Rule engine that corresponding tests use to evaluate certain properties</param>
        /// <returns></returns>
        public static QcTest CreateTest(TestType type, ISchemaProvider schemaProvider, IRuleEngine ruleEngine)
        {
            QcTest test;

            switch (type)
            {
                case TestType.Full:
                    test = new QcFullTest(schemaProvider, ruleEngine);
                    break;
                case TestType.Contamination:
                    test = new QcContaminationTest(schemaProvider.GetSamplesLayoutSchema(type), ruleEngine);
                    break;
                case TestType.Standards:
                    test = new QcStandardsTest(schemaProvider.GetSamplesLayoutSchema(type), ruleEngine);
                    break;
                case TestType.Linearity:
                    test = new QcLinearityTest(schemaProvider, ruleEngine);
                    break;
                case TestType.LinearityWithoutCalibration:
                    test = new QcLinearityWithoutCalibrationTest(schemaProvider.GetSamplesLayoutSchema(type), ruleEngine);
                    break;
                case TestType.Calibration:
                    test = new QcCalibrationTest(schemaProvider.GetSamplesLayoutSchema(type), ruleEngine);
                    break;
                default:
                    throw new NotSupportedException(String.Format("QCTest of type '{0}' is not supported.", type));
            }

            return test;
        }
       
        /// <summary>
        /// Creates QCTest based on type, schema
        /// </summary>
        /// <param name="type">Type of QCTest</param>
        /// <param name="schemaProvider">Sample schema provider</param>
        /// <param name="systemId">Hardware system ID</param>
        /// <param name="testId">Desired test ID</param>
        /// <param name="ruleEngine">Rule engine that corresponding tests use to evaluate certain properties</param>
        /// <returns></returns>
        public static QcTest CreateTest(TestType type, ISchemaProvider schemaProvider, string systemId, Guid testId, IRuleEngine ruleEngine)
        {
            QcTest test;

            switch (type)
            {
                case TestType.Full:
                    test = new QcFullTest(testId, systemId, schemaProvider, ruleEngine);
                    break;
                case TestType.Contamination:
                    test = new QcContaminationTest(testId, systemId, schemaProvider.GetSamplesLayoutSchema(type), ruleEngine);
                    break;
                case TestType.Standards:
                    test = new QcStandardsTest(testId, systemId, schemaProvider.GetSamplesLayoutSchema(type), ruleEngine);
                    break;
                case TestType.Linearity:
                    test = new QcLinearityTest(testId, systemId, schemaProvider, ruleEngine);
                    break;
                case TestType.LinearityWithoutCalibration:
                    test = new QcLinearityWithoutCalibrationTest(testId, systemId, schemaProvider.GetSamplesLayoutSchema((type)), ruleEngine);
                    break;
                case TestType.Calibration:
                    test = new QcCalibrationTest(testId, systemId, schemaProvider.GetSamplesLayoutSchema(type), ruleEngine);
                    break;
                default:
                    throw new NotSupportedException(String.Format("QCTest of type '{0}' is not supported.", type.ToString()));
            }

            return test;
        }
    }
}
