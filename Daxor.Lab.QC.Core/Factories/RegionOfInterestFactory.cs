﻿using System;
using System.Collections.Generic;
using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Core.Factories
{
    /// <summary>
    /// Creates map between energy and rois
    /// </summary>
    public static class RegionOfInterestFactory
    {
        /// <summary>
        /// Returns maps between energies and expected rois. Could be moved as database ranges
        /// </summary>
        /// <param name="isotopeType"></param>
        /// <returns></returns>
        public static Dictionary<double, Tuple<Int32, Int32>> CreateRegionOfInterest(IsotopeType isotopeType)
        {

            var regionOfInterest = new Dictionary<double, Tuple<int, int>>();

            switch (isotopeType)
            {
                case IsotopeType.Ba133:

                    regionOfInterest.Add(31.60, new Tuple<int, int>(14, 48));
                    regionOfInterest.Add(81.00, new Tuple<int, int>(54, 108));
                    regionOfInterest.Add(356.00, new Tuple<int, int>(308, 420));
                    break;

                case IsotopeType.Cs137:
                    regionOfInterest.Add(32.85, new Tuple<int, int>(14, 50));
                    regionOfInterest.Add(661.66, new Tuple<int, int>(584, 740));
                    break;
                case IsotopeType.Eu152:
                    regionOfInterest.Add(41.00, new Tuple<int, int>(22, 60));
                    regionOfInterest.Add(122.00, new Tuple<int, int>(88, 156));
                    regionOfInterest.Add(344.28, new Tuple<int, int>(288, 400));
                    break;

                case IsotopeType.I131:
                    regionOfInterest.Add(284.30, new Tuple<int, int>(232, 334));
                    regionOfInterest.Add(364.48, new Tuple<int, int>(308, 420));
                    regionOfInterest.Add(637.14, new Tuple<int, int>(562, 712));
                    break;

                default:
                    regionOfInterest = null;
                    break;
            }

            return regionOfInterest;
        }
    }
}
