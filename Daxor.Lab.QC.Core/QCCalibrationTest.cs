﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Domain.Services;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Factories;
using Daxor.Lab.QC.Core.Interfaces;

namespace Daxor.Lab.QC.Core
{
    /// <summary>
    /// The calibration test
    /// </summary>
    public class QcCalibrationTest : QcTest
    {
        #region Fields

        private bool _isFineGainMetadataInitialized;
        private int _indexOfEnergyForSetupPeak = -1;    //Energy index of setup peak => based on setupPeakEnergy
        
        #endregion

        #region Ctors

        public QcCalibrationTest(IRuleEngine ruleEngine)
            : base(IdentityGenerator.NewSequentialGuid(), ruleEngine)
        {
            QCType = TestType.Calibration;
            CalibrationCurve = new CalibrationCurve(0, 1, 0);
        }

        public QcCalibrationTest(SamplesSchema schema, IRuleEngine ruleEngine)
            : this(ruleEngine)
        {
            if (schema == null)
                throw new ArgumentNullException("schema");

            Schema = schema;
        }

        public QcCalibrationTest(Guid testId, string systemId, SamplesSchema schema, IRuleEngine ruleEngine)
            : this(schema, ruleEngine)
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InternalId = testId;
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
            SystemId = systemId;
        }

        #endregion

        #region Public properties

        public Int32 MaxAllowableFineGainAdjustments { get; set; }
        public Int32 SetupCountTimeSecs { get; set; }
        public double SetupPeakEnergy { get; set; }
        public double CalibrationPeakEnergy { get; set; }
        public double SetupPrecisionInkeV { get; set; }
        public double MaxFineGainAllowed { get; set; }
        public double MinFineGainAllowed { get; set; }
        public IPeakFinder PeakFinder { get; set; }
        public double MinimumPassingSetupFullWidthHalfMax { get; set; }
        public double MaximumPassingSetupFullWidthHalfMax { get; set; }
        public int SetupSourceCountThreshold { get; set; }
        public int SetupSourceCountThresholdAcquisitionTimeInSeconds { get; set; }
        public IFineGainAdjustmentService FineGainAdjustmentService { get; set; }
        public IIsotopeMatchingService IsotopeMatchingService { get; set; }
        public uint NumberOfFineGainAdjustments { get; set; }
        public IQcCalibrationTestPayloadProcessor PayloadProcessor { get; set; }
        public int IndexOfEnergyForSetupPeak
        {
            get { return _indexOfEnergyForSetupPeak; }
            set { _indexOfEnergyForSetupPeak = value; }
        }
        public bool IsFineGainFinishedAdjusting { get; set; }
        public int PriorSampleDurationInSeconds { get; set; }
        public FineGainMetadata FineGainMetadata { get; set; }
        public double SetupActualCentroidInkeV { get; set; }

        #endregion

        #region Public (overrides)

        public override void ProcessSample(QcSample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");
            
            if (!ContainsSample(sample))
                return;

            ProcessSampleIfTestRunning(sample);

            var innerSample = (from s in InnerSamples where s.Position == sample.Position select s).FirstOrDefault();
            if (innerSample == null) return;

            innerSample.CountsInRegionOfInterest = sample.CountsInRegionOfInterest;
            innerSample.FullWidthHalfMax = sample.FullWidthHalfMax;
            innerSample.FullWidthTenthMax = sample.FullWidthTenthMax;
            innerSample.Centroid = sample.Centroid;
            innerSample.ExecutionStatus = sample.ExecutionStatus;
            innerSample.IsPassed = sample.IsPassed;
            innerSample.SerialNumber = sample.SerialNumber;
            innerSample.Source = sample.Source;
            innerSample.Spectrum.SpectrumArray = sample.Spectrum.SpectrumArray;
            innerSample.PresetCountsLimit = innerSample.IsSource ? innerSample.Source.CountLimit : -1;
            innerSample.Spectrum.LiveTimeInSeconds = sample.Spectrum.LiveTimeInSeconds;
            innerSample.Spectrum.RealTimeInSeconds = sample.Spectrum.RealTimeInSeconds;
            innerSample.StartChannel = sample.StartChannel;
            innerSample.EndChannel = sample.EndChannel;
            innerSample.ErrorDetails = sample.ErrorDetails;
        }

        public override QcSampleEvaluationMetadata EvaluateSample(QcSample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");

            var evaluationMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Undetermined);
            if (sample.ExecutionStatus != SampleExecutionStatus.Completed)
                return evaluationMetadata;

            try
            {
                if (!_isFineGainMetadataInitialized)
                {
                    FindIndexOfEnergyForSetupPeak();
                    InitializeFineGainMetadata();
                }

                var sampleEvaluator = CalibrationTestSampleEvaluatorFactory.GetSampleEvaluator(sample);
                var prePayload = PayloadProcessor.CreatePreEvaluationPayload(sample, this);
                var postPayload = sampleEvaluator.EvaluateSample(prePayload);
                PayloadProcessor.ProcessPostEvaluationPayload(postPayload, this);
                return postPayload.Metadata;
            }
            catch (Exception ex)
            {
                sample.IsPassed = false;
                sample.IsEvaluationCompleted = true;
                evaluationMetadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Failed, ex.Message);
                sample.ErrorDetails = evaluationMetadata.Message;
                AddEvalResult(evaluationMetadata);
            }

            return evaluationMetadata;
        }

        public virtual bool SamplePeaksMatchTheSetupIsotopePeaks(Isotope setupIsotope, List<Peak> peaksSortedByCentroid)
        {
            if (setupIsotope == null) throw new ArgumentNullException("setupIsotope");
            if (peaksSortedByCentroid == null) throw new ArgumentNullException("peaksSortedByCentroid");
            if (IsotopeMatchingService == null) throw new InvalidOperationException("Unable to match isotopes with a null matching service");

            return IsotopeMatchingService.IsMatching(setupIsotope, peaksSortedByCentroid);
        }

        protected virtual void FindIndexOfEnergyForSetupPeak()
        {
            var setupSample = Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC1);
            if (setupSample == null) throw new NotSupportedException("Unable to find the setup sample in the test");

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            IndexOfEnergyForSetupPeak = setupSample.Source.Isotope.Energies
                .Select((kEv, index) => new { Energy = kEv, Index = index })
                .Where(x => x.Energy == SetupPeakEnergy)
                .Select(x => x.Index)
                .FirstOrDefault();  
        }

        protected virtual void InitializeFineGainMetadata()
        {
            FineGainMetadata = new FineGainMetadata
            {
                CurrentFineGain = StartFineGain,
                MinFineGain = MinFineGainAllowed,
                MaxFineGain = MaxFineGainAllowed,
                IsFirstAdjustment = true
            };
            _isFineGainMetadataInitialized = true;
        }

        public override int GetEstimatedExecutionTimeInSeconds(QcSample sample)
        {
            return GetDesiredExecutionTimeInSeconds(sample);
        }

        public override int GetDesiredExecutionTimeInSeconds(QcSample sample)
        {
            if (sample != null && ContainsTypeOfSample(sample.TypeOfSample))
            {
                switch (sample.TypeOfSample)
                {
                    case SampleType.QC1:
                        return SampleDurationInSeconds;
                    case SampleType.QC2:
                        return SampleDurationInSeconds * 2;
                }
            }

            return 0;
        }

        #endregion

        #region Private helpers

        protected virtual void ProcessSampleIfTestRunning(QcSample rSample)
        {
            if (Status != TestStatus.Running)
                return;

            // Background sample
            var startChannel = CalibrationCurve.GetChannel(QcDomainConstants.DampedBa133LowerEnergyBound);
            var endChannel = CalibrationCurve.GetChannel(QcDomainConstants.DampedBa133UpperEnergyBound);

            switch (rSample.TypeOfSample)
            {
                case SampleType.QC1:
                    startChannel = (int)Math.Round(SetupPeakEnergy - 3 * Math.Sqrt(SetupPeakEnergy), MidpointRounding.ToEven);
                    endChannel = (int)Math.Round(SetupPeakEnergy + 3 * Math.Sqrt(SetupPeakEnergy), MidpointRounding.AwayFromZero);
                    break;
                case SampleType.QC2:
                    startChannel = 308;
                    endChannel = 420;
                    break;
            }

            rSample.StartChannel = startChannel;
            rSample.EndChannel = endChannel;

            rSample.CountsInRegionOfInterest = (int) SpectroscopyService.ComputeIntegral(rSample.Spectrum.SpectrumArray, rSample.StartChannel, rSample.EndChannel);
            rSample.Centroid = Math.Round(SpectroscopyService.ComputeCentroid(rSample.Spectrum.SpectrumArray, rSample.StartChannel, rSample.EndChannel), 2);
            rSample.FullWidthHalfMax = Math.Round(SpectroscopyService.ComputeFullWidthHalfMax(rSample.Spectrum.SpectrumArray, rSample.StartChannel, rSample.EndChannel), 2);
            rSample.FullWidthHalfMax = (rSample.FullWidthHalfMax / rSample.Centroid) * 100;  // Convert to a percent
            rSample.FullWidthTenthMax = Math.Round(SpectroscopyService.ComputeFullWidthTenthMax(rSample.Spectrum.SpectrumArray, rSample.StartChannel, rSample.EndChannel), 2);

            // Set if preset count limit reached
            rSample.PresetCountLimitReached = rSample.CountsInRegionOfInterest >= SetupSourceCountThreshold;
        }

        #endregion
    }
}
