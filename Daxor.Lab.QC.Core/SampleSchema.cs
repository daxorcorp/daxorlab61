﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Core
{
    public class SamplesSchema
    {
        //Key is position of sample; Value is SampleSchemaItem
        private readonly IDictionary<int, SampleSchemaItem> _mappings = new Dictionary<int, SampleSchemaItem>();
       
        public SamplesSchema(int id, IDictionary<int, SampleSchemaItem> layout)
        {
            Id = id;
            _mappings = layout;
        }

        #region Methods

        public IEnumerable<SampleSchemaItem> Items
        {
            get { return _mappings.Values; }
        }
        public IEnumerable<SampleSchemaItem> ItemsFor(SampleType type)
        {
            if (_mappings == null || _mappings.Count < 0)
                return Enumerable.Empty<SampleSchemaItem>();

            return from keyValue in _mappings where keyValue.Value.Type == type select keyValue.Value;
        }

        public IEnumerable<Int32> GetPositionFor(SampleType type)
        {
            if (_mappings == null || _mappings.Count < 0)
                return Enumerable.Empty<Int32>();

            return from keyValue in _mappings where keyValue.Value.Type == type select keyValue.Value.Position;
        }
        public SampleType GetSampleTypeForPosition(int position)
        {
            SampleSchemaItem item;

            if (_mappings.TryGetValue(position, out item))
                return item.Type;

            return SampleType.Undefined;
        }
        public int Id { get; private set; }

        public bool IsPositionPartOfSchema(int position)
        {
            return _mappings.ContainsKey(position);
        }

        #endregion
    }
}
