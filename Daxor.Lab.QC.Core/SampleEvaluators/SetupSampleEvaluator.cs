﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Entities.Base;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Payloads;

namespace Daxor.Lab.QC.Core.SampleEvaluators
{
    public class SetupSampleEvaluator : ISampleEvaluator
    {
        #region Constants

        public const string SetupIsotopeNotDetectedMessage = "Setup isotope was not detected.";
        public const string SetupFwhmIsNotWithinToleranceFormatString = "FWHM ({0:0}) is outside the allowable range ({1:0} to {2:0}).";
        public const string SetupSourceCountThresholdFailureFormatString = "Setup source count threshold ({0}) was not reached.";
        public const string MaximumFineGainAdjustmentsExceededFormatString = "Exceeded maximum fine gain adjustments. (Max = {0}).";
        public const string SetupFailedOnFwhmMessage = "Setup fails on FWHM.";
        public const string FineGainNotChangedFormatString = "Further fine gain adjustments will have no effect ({0} adjustment(s) made).";

        #endregion

        public PostEvaluationPayload EvaluateSample(PreEvaluationPayload payload)
        {
            var prePayload = VerifyAndCastPayload(payload);

            var sample = payload.Sample;
            var postEvaluationPayload = CreateInitialPostEvaluationPayload(prePayload);

            if (prePayload.IsFineGainFinishedAdjusting)
            {
                SetFineGainFinishedAdjustingState(postEvaluationPayload, prePayload, sample);

                if (sample.CountsInRegionOfInterest >= prePayload.SetupSourceCountThreshold)
                {
                    if (IsSampleFwhmNotWithinRange(sample, prePayload))
                        SetFwhmNotWithinRangeFailureState(sample, postEvaluationPayload, prePayload);
                    else
                        SetPassState(sample, postEvaluationPayload);
                }
                else
                    SetCountsNotReachedFailureState(sample, postEvaluationPayload, prePayload);

                return postEvaluationPayload;
            }

            if (prePayload.NumberOfFineGainAdjustments >= prePayload.MaxAllowableFineGainAdjustments)
            {
                SetTooManyFineGainAdjustmentsFailureState(sample, postEvaluationPayload, prePayload);
                return postEvaluationPayload;
            }

            #region Attempt to match isotope to most significant peaks

            var peaksSortedByCentroid = FindPeaksSortedByCentroid(prePayload, sample);

            // ReSharper disable PossibleMultipleEnumeration
            if (SamplePeaksMatchTheSetupIsotopePeaks(sample.Source.Isotope, peaksSortedByCentroid,
                prePayload.IsotopeMatchingService))
            {
                var foundSetupPeak = peaksSortedByCentroid.ElementAtOrDefault(prePayload.IndexOfEnergyForSetupPeak);
                if (foundSetupPeak == null) throw new NotSupportedException("there is no peak at the desired index");
                // ReSharper restore PossibleMultipleEnumeration

                #region Find out if peak is in ROI and if centroid is within tolerance

                var expectedCentroidInkeV = prePayload.SetupPeakEnergy;
                // ReSharper disable once PossibleNullReferenceException
                var foundCentroidInkeV = foundSetupPeak.Centroid;
                postEvaluationPayload.SetupActualCentroidInkeV = prePayload.SpectroscopyService.ComputeCentroid(sample.Spectrum.SpectrumArray,
                        sample.StartChannel, sample.EndChannel);

                var isFoundPeakWithinExpectedRegionOfInterest = IsSetupPeakWithinRoi(sample, foundSetupPeak);
                var isActualCentroidWithinDesiredPrecision = IsCentroidWithinPrecision(postEvaluationPayload.SetupActualCentroidInkeV,
                        prePayload.SetupPeakEnergy, prePayload.SetupPrecisionInkeV);

                #endregion

                if (isFoundPeakWithinExpectedRegionOfInterest && isActualCentroidWithinDesiredPrecision)
                {
                    sample.IsEvaluationCompleted = true;
                    if (!IsSampleFwhmNotWithinRange(sample, prePayload))
                        SetCountSampleOneMoreTimeState(postEvaluationPayload, sample, prePayload);
                    else
                        SetCouldNotCalibrateOnFwhmFailureState(sample, postEvaluationPayload);
                }
                else
                {
                    DetermineNewFineGainAdjustmentAndSetState(prePayload, postEvaluationPayload, expectedCentroidInkeV, 
                        foundCentroidInkeV, sample);
                }
                return postEvaluationPayload;
            }

            #endregion

            postEvaluationPayload.NumberOfFineGainAdjustments = prePayload.NumberOfFineGainAdjustments + 1;
            SetSetupIsotopeNotFoundFailureState(sample, postEvaluationPayload);

            return postEvaluationPayload;
        }

        #region Public Helpers

        public static bool IsCentroidWithinPrecision(double setupActualCentroidInkeV, double setupPeakEnergy,
            double setupPrecisionInkeV)
        {
            return Math.Abs(setupActualCentroidInkeV - setupPeakEnergy) <= setupPrecisionInkeV;
        }

        public static bool IsSetupPeakWithinRoi(QcSample sample, Peak setupPeak)
        {
            if (sample == null) throw new ArgumentNullException("sample");
            if (setupPeak == null) throw new ArgumentNullException("setupPeak");

            return setupPeak.Centroid > sample.StartChannel && setupPeak.Centroid < sample.EndChannel;
        }

        #endregion

        #region Private helpers

        private static void DetermineNewFineGainAdjustmentAndSetState(SetupPreEvaluationPayload prePayload,
                SetupPostEvaluationPayload postEvaluationPayload, double expectedCentroidInkeV,
                double foundCentroidInkeV, QcSample sample)
        {
            var fineGainAdjustmentService = prePayload.FineGainAdjustmentService;
            if (fineGainAdjustmentService == null)
                throw new InvalidOperationException(
                    "Could not calculate new fine gain with a null adjustment service");

            postEvaluationPayload.FineGainMetadata =
                fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroidInkeV, foundCentroidInkeV,
                    prePayload.FineGainMetadata);
            postEvaluationPayload.NumberOfFineGainAdjustments =
                prePayload.NumberOfFineGainAdjustments + 1;

            if (Math.Abs(prePayload.CurrentFineGain - postEvaluationPayload.FineGainMetadata.CurrentFineGain) < 1E-6)
            {
                SetFineGainDidNotChangeFailureState(sample, postEvaluationPayload);
            }
            else
            {
                postEvaluationPayload.CurrentFineGain =
                    postEvaluationPayload.FineGainMetadata.CurrentFineGain;
                postEvaluationPayload.Metadata = new QcSampleEvaluationMetadata(sample.InternalId,
                    QcSampleEvaluationResult.FailedButRepeat);
            }
        }

        private static void SetFineGainFinishedAdjustingState(SetupPostEvaluationPayload postEvaluationPayload,
            SetupPreEvaluationPayload prePayload, QcSample sample)
        {
            postEvaluationPayload.IsFineGainFinishedAdjusting = false;
            postEvaluationPayload.SampleDurationInSeconds = prePayload.PriorSampleDurationInSeconds;
            sample.IsEvaluationCompleted = true;
        }

        private static void SetPassState(QcSample sample, PostEvaluationPayload postEvaluationPayload)
        {
            sample.IsPassed = true;
            postEvaluationPayload.Metadata = new QcSampleEvaluationMetadata(sample.InternalId,
                QcSampleEvaluationResult.Pass);
        }

        private static void SetFineGainDidNotChangeFailureState(QcSample sample,
            SetupPostEvaluationPayload postEvaluationPayload)
        {
            sample.IsEvaluationCompleted = true;
            sample.IsPassed = false;
            sample.ErrorDetails = String.Format(FineGainNotChangedFormatString,
                postEvaluationPayload.NumberOfFineGainAdjustments);
            postEvaluationPayload.Metadata = new QcSampleEvaluationMetadata(sample.InternalId,
                QcSampleEvaluationResult.Failed, sample.ErrorDetails);
        }

        private static void SetCountSampleOneMoreTimeState(SetupPostEvaluationPayload postEvaluationPayload,
            QcSample sample, SetupPreEvaluationPayload prePayload)
        {
            postEvaluationPayload.IsFineGainFinishedAdjusting = true;
            sample.PresetCountsLimit = prePayload.SetupSourceCountThreshold;
            postEvaluationPayload.PriorSampleDurationInSeconds =
                prePayload.SampleDurationInSeconds;
            postEvaluationPayload.SampleDurationInSeconds =
                prePayload.SetupSourceCountThresholdAcquisitionTimeInSeconds;

            postEvaluationPayload.Metadata = new QcSampleEvaluationMetadata(sample.InternalId,
                QcSampleEvaluationResult.FailedButRepeat);
        }

        private static void SetCouldNotCalibrateOnFwhmFailureState(QcSample sample,
            PostEvaluationPayload postEvaluationPayload)
        {
            sample.IsPassed = false;
            postEvaluationPayload.Metadata = new QcSampleEvaluationMetadata(sample.InternalId,
                QcSampleEvaluationResult.Failed,
                SetupFailedOnFwhmMessage);
            sample.ErrorDetails = postEvaluationPayload.Metadata.Message;
        }

        private static void SetTooManyFineGainAdjustmentsFailureState(QcSample sample, PostEvaluationPayload postEvaluationPayload, SetupPreEvaluationPayload prePayload)
        {
            sample.IsPassed = false;
            sample.IsEvaluationCompleted = true;

            postEvaluationPayload.Metadata = new QcSampleEvaluationMetadata(sample.InternalId,
                QcSampleEvaluationResult.Failed,
                String.Format(MaximumFineGainAdjustmentsExceededFormatString,
                    prePayload.MaxAllowableFineGainAdjustments));
            sample.ErrorDetails = postEvaluationPayload.Metadata.Message;
        }

        private static void SetFwhmNotWithinRangeFailureState(QcSample sample, PostEvaluationPayload postEvaluationPayload, SetupPreEvaluationPayload prePayload)
        {
            sample.IsPassed = false;
            postEvaluationPayload.Metadata = CreateFwhmNotWithinRangeMetadata(sample,
                prePayload.MinimumPassingSetupFullWidthHalfMax, prePayload.MaximumPassingSetupFullWidthHalfMax);
            sample.ErrorDetails = postEvaluationPayload.Metadata.Message;
        }

        private void SetCountsNotReachedFailureState(QcSample sample, PostEvaluationPayload postEvaluationPayload, SetupPreEvaluationPayload prePayload)
        {
            sample.IsPassed = false;
            postEvaluationPayload.Metadata = CreateSetupSourceCountThresholdMetadata(sample,
                prePayload.SetupSourceCountThreshold);
            sample.ErrorDetails = postEvaluationPayload.Metadata.Message;
        }

        private static bool IsSampleFwhmNotWithinRange(QcSample sample, SetupPreEvaluationPayload prePayload)
        {
            return sample.FullWidthHalfMax < prePayload.MinimumPassingSetupFullWidthHalfMax ||
                   sample.FullWidthHalfMax > prePayload.MaximumPassingSetupFullWidthHalfMax;
        }

        private static IEnumerable<Peak> FindPeaksSortedByCentroid(SetupPreEvaluationPayload prePayload, QcSample sample)
        {
            var peaksSortedByCentroid = prePayload.PeakFinder.FindPeaksSortedByCentroid(sample.Spectrum.SpectrumArray, 20,
                sample.Source.Isotope.Energies.Count());
            if (peaksSortedByCentroid == null) throw new InvalidOperationException("Peaks sorted by centroid returned null");
            peaksSortedByCentroid = peaksSortedByCentroid.ToList();
            return peaksSortedByCentroid;
        }

        private static void SetSetupIsotopeNotFoundFailureState(QcSample sample, PostEvaluationPayload postEvaluationPayload)
        {
            sample.IsEvaluationCompleted = true;
            sample.IsPassed = false;
            postEvaluationPayload.Metadata = new QcSampleEvaluationMetadata(sample.InternalId,
                QcSampleEvaluationResult.Failed,
                SetupIsotopeNotDetectedMessage);
            sample.ErrorDetails = postEvaluationPayload.Metadata.Message;
        }

        private static SetupPostEvaluationPayload CreateInitialPostEvaluationPayload(SetupPreEvaluationPayload prePayload)
        {
            return new SetupPostEvaluationPayload
            {
                SampleDurationInSeconds = prePayload.SampleDurationInSeconds,
                IsFineGainFinishedAdjusting = prePayload.IsFineGainFinishedAdjusting,
                PriorSampleDurationInSeconds = prePayload.PriorSampleDurationInSeconds,
                FineGainMetadata = prePayload.FineGainMetadata,
                NumberOfFineGainAdjustments = prePayload.NumberOfFineGainAdjustments,
                CurrentFineGain = prePayload.CurrentFineGain
            };
        }

        private static SetupPreEvaluationPayload VerifyAndCastPayload(PreEvaluationPayload payload)
        {
            if (payload == null) throw new ArgumentNullException("payload");
            if (payload.Sample == null) throw new ArgumentException("payload cannot have a null sample");
            if (payload.Sample.Isotope == null) throw new NotSupportedException("sample's isotope cannot be null");
            if (payload.Sample.TypeOfSample != SampleType.QC1)
                throw new NotSupportedException("payload sample is not the setup sample");

            var prePayload = payload as SetupPreEvaluationPayload;
            if (prePayload == null) throw new NotSupportedException("payload is not a setup payload");

            return prePayload;
        }

        private static QcSampleEvaluationMetadata CreateFwhmNotWithinRangeMetadata(QcSample sample, double minimumFwhm, double maximumFwhm)
        {
            return new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Failed,
                String.Format(SetupFwhmIsNotWithinToleranceFormatString, sample.FullWidthHalfMax, minimumFwhm, maximumFwhm));
        }

        private QcSampleEvaluationMetadata CreateSetupSourceCountThresholdMetadata(Entity sample, double setupSourceCountThreshold)
        {
            return new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Failed,
                String.Format(SetupSourceCountThresholdFailureFormatString, setupSourceCountThreshold));
        }

        private static bool SamplePeaksMatchTheSetupIsotopePeaks(Isotope setupIsotope, IEnumerable<Peak> peaksSortedByCentroid,
            IIsotopeMatchingService isotopeMatchingService)
        {
            if (isotopeMatchingService == null) throw new InvalidOperationException("Unable to match isotopes with a null matching service");

            return isotopeMatchingService.IsMatching(setupIsotope, peaksSortedByCentroid);
        }

        #endregion
    }
}
