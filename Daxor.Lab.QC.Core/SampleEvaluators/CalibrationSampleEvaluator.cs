﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Payloads;

namespace Daxor.Lab.QC.Core.SampleEvaluators
{
    public class CalibrationSampleEvaluator : ISampleEvaluator
    {
        public const string CalibrationPeakNotFoundMessage = "Calibration peak was not found.";
        public const string CalibrationPeakBelowStartChannelFormatString = "Found peak is outside of expected range in channels (Low: {0}).";
        public const string CalibrationPeakAboveEndChannelFormatString = "Found peak is outside of expected range in channels (High: {0}).";

        public PostEvaluationPayload EvaluateSample(PreEvaluationPayload payload)
        {
            var calibrationPayload = VerifyAndCastPayload(payload);

            var spectroscopyService = calibrationPayload.SpectroscopyService;
            var peakFinder = calibrationPayload.PeakFinder;
            var setupActualCentroidInkeV = calibrationPayload.SetupActualCentroidInkeV;
            var setupPeakEnergy = calibrationPayload.SetupPeakEnergy;
            var calibrationPeakEnergy = calibrationPayload.CalibrationPeakEnergy;
            var sample = payload.Sample;

            var calibrationCentroidInkeV = spectroscopyService.ComputeCentroid(sample.Spectrum.SpectrumArray, sample.StartChannel, sample.EndChannel);

            var calibrationCurve = new CalibrationCurve(-1,
                new[] {setupActualCentroidInkeV, calibrationCentroidInkeV},
                new[] {setupPeakEnergy, calibrationPeakEnergy});

            var postEvalPayload = new CalibrationPostEvaluationPayload
            {
                Metadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Undetermined)
            };

            sample.IsEvaluationCompleted = true;
            var highestMeasuredPeakInChannels = FindHighestMeasuredPeakInChannels(peakFinder, sample);

            if (IsMeasuredPeakInBounds(highestMeasuredPeakInChannels, sample))
            {
                SetCalibrationCurveAndMetadata(sample, postEvalPayload, calibrationCurve);
                return postEvalPayload;
            }

            SetFailureState(sample, highestMeasuredPeakInChannels, postEvalPayload);
            return postEvalPayload;
        }

        #region Private Helpers

        private static void SetFailureState(QcSample sample, Peak highestMeasuredPeakInChannels, PostEvaluationPayload postEvalPayload)
        {
            sample.IsPassed = false;
            if (highestMeasuredPeakInChannels == null)
                postEvalPayload.Metadata = new QcSampleEvaluationMetadata(sample.InternalId,
                    QcSampleEvaluationResult.FailedButContinue, CalibrationPeakNotFoundMessage);

            else if (highestMeasuredPeakInChannels.Centroid < sample.StartChannel)
                postEvalPayload.Metadata = new QcSampleEvaluationMetadata(sample.InternalId,
                    QcSampleEvaluationResult.FailedButContinue,
                    String.Format(CalibrationPeakBelowStartChannelFormatString, sample.StartChannel));

            else
                postEvalPayload.Metadata = new QcSampleEvaluationMetadata(sample.InternalId,
                    QcSampleEvaluationResult.FailedButContinue,
                    String.Format(CalibrationPeakAboveEndChannelFormatString, sample.EndChannel));

            sample.ErrorDetails = postEvalPayload.Metadata.Message;
        }

        private static CalibrationPreEvaluationPayload VerifyAndCastPayload(PreEvaluationPayload payload)
        {
            if (payload == null)
                throw new ArgumentNullException("payload");
            if (payload.Sample == null)
                throw new ArgumentException("payload sample cannot be null");
            if (payload.Sample.TypeOfSample != SampleType.QC2)
                throw new NotSupportedException("payload sample must be of type calibration");

            var calibrationPayload = payload as CalibrationPreEvaluationPayload;
            if (calibrationPayload == null)
                throw new NotSupportedException("must use calibration prepayload");

            if (calibrationPayload.SpectroscopyService == null)
                throw new NotSupportedException("Spectroscopy service cannot be null");
            if (calibrationPayload.PeakFinder == null)
                throw new NotSupportedException("Peak finding service cannot be null");
            return calibrationPayload;
        }

        private static bool IsMeasuredPeakInBounds(Peak highestMeasuredPeakInChannels, QcSample sample)
        {
            return highestMeasuredPeakInChannels != null && highestMeasuredPeakInChannels.Centroid >= sample.StartChannel &&
                   highestMeasuredPeakInChannels.Centroid <= sample.EndChannel;
        }

        private static void SetCalibrationCurveAndMetadata(QcSample sample, CalibrationPostEvaluationPayload postEvalPayload, ICalibrationCurve calibrationCurve)
        {
            sample.IsPassed = true;
            postEvalPayload.CalibrationCurve = calibrationCurve;
            postEvalPayload.Metadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Pass);
        }

        private static Peak FindHighestMeasuredPeakInChannels(IPeakFinder peakFinder, QcSample sample)
        {
            var foundPeaksInChannels = new List<Peak>(peakFinder.FindPeaks(sample.Spectrum.SpectrumArray, 20));

            var expectedNumPeaks = sample.Source.Isotope.Energies.Count();
            var peaksOfInterestInChannels =
                (from p in foundPeaksInChannels orderby p.Significance descending select p).Take(expectedNumPeaks);
            var highestMeasuredPeakInChannels = peaksOfInterestInChannels.FirstOrDefault();
            return highestMeasuredPeakInChannels;
        }
        
        #endregion
    }
}
