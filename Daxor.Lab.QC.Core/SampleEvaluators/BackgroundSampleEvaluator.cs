using System;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Payloads;

namespace Daxor.Lab.QC.Core.SampleEvaluators
{
    public class BackgroundSampleEvaluator : ISampleEvaluator
    {
        public PostEvaluationPayload EvaluateSample(PreEvaluationPayload payload)
        {
            if (payload == null) throw new ArgumentNullException("payload");
            if (payload.Sample == null) throw new ArgumentException("payload cannot have a null sample");
            if (payload.Sample.TypeOfSample != SampleType.Background) throw new NotSupportedException("payload sample is not background");

            var sample = payload.Sample;
            sample.IsEvaluationCompleted = true;
            sample.IsPassed = true;
            return new BackgroundPostEvaluationPayload
            {
                Metadata = new QcSampleEvaluationMetadata(sample.InternalId, QcSampleEvaluationResult.Pass)
            };
        }
    }
}