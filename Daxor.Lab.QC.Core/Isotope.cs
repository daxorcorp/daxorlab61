﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Entities.Base;
using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Core
{
    /// <summary>
    /// Value object in DDD terms, has no specific id
    /// </summary>
    public class Isotope : ValueObject<Isotope>
    {
        #region Fields

        private readonly List<double> _energies = new List<double>();
        private readonly List<double> _ratiosOfEnergies = new List<double>();

        #endregion

        #region Ctor

        public Isotope(IsotopeType name, double hLife, UnitOfTime hLifeLength, IEnumerable<double> energies)
        {
            if (energies == null)
                throw new ArgumentNullException("energies");

            Name = name;
            HalfLife = hLife;
            HalfLifeLength = hLifeLength;
            _energies.AddRange(energies);
            
            GenerateRatios();
        }

        #endregion

        #region Properties

        public double HalfLife { get; private set; }
        public UnitOfTime HalfLifeLength { get; private set; }
        public IsotopeType Name { get; private set; }
        public IEnumerable<double> Energies { get { return _energies; } }
        public IEnumerable<double> RatiosOfEnergies { get { return _ratiosOfEnergies; } }

        #endregion

        #region Methods

        public static Isotope Empty()
        {
            return new Isotope(IsotopeType.Empty, -1, UnitOfTime.Minute, Enumerable.Empty<Double>());
        }

        void GenerateRatios()
        {
            // orders ascending
            var orderedPeaks = _energies.OrderBy(e => e).ToList();
            
            if (orderedPeaks.Count == 0) return;

            var hEnergy = orderedPeaks.LastOrDefault();
            if (hEnergy <= 0) return;
            foreach (var e in _energies)
                _ratiosOfEnergies.Add(e / hEnergy);
        }

        #endregion
    }
}
