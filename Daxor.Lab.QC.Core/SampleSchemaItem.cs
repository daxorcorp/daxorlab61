﻿using System;
using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Core
{
    //Schema item
    public class SampleSchemaItem
    {
        public SampleSchemaItem()
        {
            FriendlyName = String.Empty;
            SerialNumber = String.Empty;
            ContainingIsotope = IsotopeType.Empty;
        }

        public IsotopeType ContainingIsotope { get; set; }
        public SampleType Type { get; set; }
        public int Position { get; set; }
        public string SerialNumber { get; set; }
        public string FriendlyName { get; set; }
    }
}
