﻿using System;

namespace Daxor.Lab.QC.Core
{
    /// <summary>
    /// Represents a source used during CalibrationTest and LinearityTest
    /// </summary>
    public class QcSource
    {       
        public QcSource(Isotope isotope, DateTime timeOfCalibration, Int32 sPosition, double activity, double accuracy, string serialNumber, Int32 countLimit)
        {
            Isotope = isotope;
            TimeOfCalibration = timeOfCalibration;
            SamplePosition = sPosition;
            Activity = activity;
            Accuracy = accuracy;
            SerialNumber = serialNumber;
            CountLimit = countLimit;
        }

        public DateTime TimeOfCalibration { get; private set; }
        public Isotope Isotope { get; private set; }
        public Int32 SamplePosition { get; private set; }
        public double Activity { get; private set; }
        public double Accuracy { get; private set; }
        public string SerialNumber { get; private set; }
        public Int32 CountLimit { get; private set; }
    }
}
