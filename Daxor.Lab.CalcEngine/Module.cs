﻿using System.Runtime.CompilerServices;
using Daxor.Lab.CalcEngine.Common;
using Daxor.Lab.CalcEngine.Services;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Composite.Modularity;
using Microsoft.Practices.Unity;

[assembly: InternalsVisibleTo("CalcEngine.Tests")]
namespace Daxor.Lab.CalcEngine
{
    public class Module : IModule
    {
	    private const string ModuleName = "Calculation Engine";

        private readonly IUnityContainer _container;

        public Module(IUnityContainer container)
        {
            _container = container;
        }

        #region IModule Members

        public void Initialize()
        {
            var assemblyLocation = System.Reflection.Assembly.GetExecutingAssembly().Location;
            var assemblyDirectory = System.IO.Path.GetDirectoryName(assemblyLocation);
            var xmlDirectory = assemblyDirectory + @"\XML\";

	        RegisterIdealBloodVolumeXmlReader(xmlDirectory);
	        RegisterIdealsCalcEngineService();
	        RegisterMeasuredCalcEngineService();
        }

        
        #endregion

        #region Helpers

        private void RegisterMeasuredCalcEngineService()
        {
            try
            {
                _container.RegisterType<IMeasuredCalcEngineService, MeasuredCalcEngineService>();
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the Measured Calculation Engine");
            }
        }

        private void RegisterIdealsCalcEngineService()
        {
            try
            {
                _container.RegisterType<IIdealsCalcEngineService, IdealsCalcEngineService>(
                    new ContainerControlledLifetimeManager());
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the Ideals Calculation Engine");
            }
        }

        private void RegisterIdealBloodVolumeXmlReader(string xmlDirectory)
        {
            try
            {
                _container.RegisterType<IdealBloodVolumeXMLReader, IdealBloodVolumeXMLReader>(
                    new InjectionConstructor(xmlDirectory));
            }
            catch
            {
                throw new ModuleLoadException(ModuleName, "initialize the Ideal Blood Volume XML Reader");
            }
        }

        #endregion
    }
}
