﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.CalcEngine.Common;
using Daxor.Lab.CalcEngine.Controllers;

namespace Daxor.Lab.CalcEngine.Services
{
    public class MeasuredCalcEngineService : IMeasuredCalcEngineService
    {
        public MeasuredCalcEngineResults GetAllResults(MeasuredCalcEngineParams parms)
        {
            return new MeasuredCalcEngineTestObject(parms).RunBVACalculationsWithParms();
        }

        public UnadjustedBloodVolumeResult GetOnlyUbvResult(int refVolume, double antiCouglant, double standardCounts, int background, double controlCounts, BloodVolumePoint p)
        {
            return new UnadjustedBloodVolumeResult(p.UniqueId, BVAMeasuredCalculator.calcUBV(refVolume, antiCouglant, standardCounts, background, controlCounts, p));
        }
    }
}
