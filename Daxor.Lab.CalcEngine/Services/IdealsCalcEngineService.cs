﻿using Daxor.Lab.CalcEngine.Common;
using Daxor.Lab.CalcEngine.Controllers;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;

namespace Daxor.Lab.CalcEngine.Services
{
    public class IdealsCalcEngineService : IIdealsCalcEngineService
    {
        #region Fields

        private readonly IdealBloodVolumeXMLReader _ibvReader = null;
        private readonly BVAIdealsCalculator _idealsCalc;

        #endregion

        public IdealsCalcEngineService(IdealBloodVolumeXMLReader reader)
        {
            _ibvReader = reader;
            _idealsCalc = new BVAIdealsCalculator(_ibvReader);
        }
        public IdealsCalcEngineResults GetIdeals(double weightKg, double heightCm, int gender)
        {
            IdealsCalcEngineResults iResults = null;
            try
            {
                iResults = _idealsCalc.ComputeIdeals(weightKg, heightCm, gender);

            }
            catch
            {
                iResults = null;
            }

            return iResults;
        }

        #region Properties

        public double MinimumHeightInCm
        {
            get { return _idealsCalc.MinimumHeightInCm; }
        }
        public double MaximumHeightInCm
        {
            get { return _idealsCalc.MaximumHeightInCm; }
        }
        public double MinimumWeightInKg
        {
            get { return _idealsCalc.MinimumWeightInKg; }
        }
        public double MaximumWeightInKg
        {
            get { return _idealsCalc.MaximumWeightInKg; }
        }

        #endregion


        public double MaximumWeightDeviation
        {
            get { return _idealsCalc.MaximumWeightDeviation; }
        }

        public double MinimumWeightDeviation
        {
            get { return _idealsCalc.MinimumWeightDeviation; }
        }
    }
}



