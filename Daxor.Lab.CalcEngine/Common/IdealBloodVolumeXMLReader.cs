﻿using System;
using System.Collections.Generic;
using System.Xml;
using Daxor.Lab.CalcEngine.Controllers;

namespace Daxor.Lab.CalcEngine.Common
{
    // Used to extract xml data for HW_Metric_Signed.xml, and 
    // WDVF_signed table.xml XML files, and caches them in lists for future reference
    public class IdealBloodVolumeXMLReader
    {
        #region Fields
        //list of all Height-IdealWeight pairs
        private readonly List<IdealHeightWeightPair> _idealHeightWeightPairs = new List<IdealHeightWeightPair>();

        //List of all WeightDifferences and corrosponding Volume factors pairs
        private readonly List<WeightDifferenceVolumeFactorPair> _volumeFactors = new List<WeightDifferenceVolumeFactorPair>();

        #endregion

        #region Properties
        public IEnumerable<IdealHeightWeightPair> IdealHeightWeightPairs
        {
            get
            {
                return _idealHeightWeightPairs;
            }
        }

        public IEnumerable<WeightDifferenceVolumeFactorPair> IdealWeightDifferencePairs
        {
            get
            {
                return _volumeFactors;
            }
        }


        #endregion

        #region Ctor

        public IdealBloodVolumeXMLReader()
        {
            // cache the WeightDifferenceVolumeFactor and HeightWeightMetric XML files
            // read files from current folder
            GetHeightWeightParameters();
            GetWeightDifferenceVolumeFactorParameters();

        }

        // overload ctor
        public IdealBloodVolumeXMLReader(string path)
        {
            // cache the WeightDifferenceVolumeFactor and HeightWeightMetric XML files
            // read files from the "path" forlder
            GetHeightWeightParameters(path);
            GetWeightDifferenceVolumeFactorParameters(path);

        }

        #endregion

        #region Helper Methods

        #region GetHeightWeightParameters - from current working directory or from path

        public void GetHeightWeightParameters()
        {
            //Extract data from HeightWeight xml file, and stores in list

            // get current working directory
            string cwd = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string HWMetricSignedXmlPath = cwd+@"\data\HW_Metric_Signed.xml";
           
            //xml file reader
            XmlReader heightWeightXmlReader;
   
            //xml file setting
            XmlReaderSettings heightWeightSettings = new XmlReaderSettings();
  
            //set xml file settings
            heightWeightSettings.IgnoreComments = true;
            heightWeightSettings.IgnoreWhitespace = true;

            try //attempt to open and read XML file 
            {
                try//Catch file not found exception
                {
                    heightWeightXmlReader = XmlReader.Create(HWMetricSignedXmlPath, heightWeightSettings);
                }
                catch (Exception exception)
                {
                    throw new BVAIdealsCalculator.IdealBloodVolumeFileException(exception.Message);
                }

                //read to first xml "Table" tag
                heightWeightXmlReader.ReadToDescendant("Table");

                //read HWMetricSignedXml contents, must use do_while in order to get first table node
                do
                {
                    //Get subtree containing Height, IdealWeight pairs
                    XmlReader subtree = heightWeightXmlReader.ReadSubtree();

                    //temp variables
                    double height, idealWeight;

                    //read to Height node by reading past table node
                    subtree.ReadStartElement("Table");

                    //capture Height value
                    height = subtree.ReadElementContentAsDouble();

                    //capture IdealWeight value
                    idealWeight = subtree.ReadElementContentAsDouble();

                    //create new object for list
                    IdealHeightWeightPair HtIdWt = new IdealHeightWeightPair(height, idealWeight);

                    //Console.Out.Write("Height = " + Height + ", IdealWeight = " + IdealWeight + "\n");

                    //append to list
                    _idealHeightWeightPairs.Add(HtIdWt);

                }//if while fails(i.e. error or EOF), XMLreader automatically closes xmlfile
                while (heightWeightXmlReader.ReadToNextSibling("Table"));

                heightWeightXmlReader.Close();
            }
            catch (XmlException exception)
            {
                //rethrow 
                throw new XmlException(exception.Message);
            }
        }

        // This implementation is going to be the parameterless method implementation
        public void GetHeightWeightParameters(string filePath)
        {
            string HWMetricSignedXmlPath = filePath + @"HW_Metric_Signed.xml";

            //xml file reader
            XmlReader heightWeightXmlReader;

            //xml file setting
            XmlReaderSettings heightWeightSettings = new XmlReaderSettings();

            //set xml file settings
            heightWeightSettings.IgnoreComments = true;
            heightWeightSettings.IgnoreWhitespace = true;

            try //attempt to open and read XML file 
            {
                try//Catch file not found exception
                {
                    heightWeightXmlReader = XmlReader.Create(HWMetricSignedXmlPath, heightWeightSettings);
                }
                catch (Exception exception)
                {
                    throw new BVAIdealsCalculator.IdealBloodVolumeFileException(exception.Message);
                }

                //read to first xml "Table" tag
                heightWeightXmlReader.ReadToDescendant("Table");

                //read HWMetricSignedXml contents, must use do_while in order to get first table node
                do
                {
                    //Get subtree containing Height, IdealWeight pairs
                    XmlReader subtree = heightWeightXmlReader.ReadSubtree();

                    //temp variables
                    double height, idealWeight;

                    //read to Height node by reading past table node
                    subtree.ReadStartElement("Table");

                    //capture Height value
                    height = subtree.ReadElementContentAsDouble();

                    //capture IdealWeight value
                    idealWeight = subtree.ReadElementContentAsDouble();

                    //create new object for list
                    IdealHeightWeightPair HtIdWt = new IdealHeightWeightPair(height, idealWeight);

                    //Console.Out.Write("Height = " + Height + ", IdealWeight = " + IdealWeight + "\n");

                    //append to list
                    _idealHeightWeightPairs.Add(HtIdWt);

                }//if while fails(i.e. error or EOF), XMLreader automatically closes xmlfile
                while (heightWeightXmlReader.ReadToNextSibling("Table"));

                heightWeightXmlReader.Close();
            }
            catch (XmlException exception)
            {
                //rethrow 
                throw new XmlException(exception.Message);
            }
        }

        #endregion

        #region GetWeightDifferenceVolumeFactorParameters - from current folder or from path

        public void GetWeightDifferenceVolumeFactorParameters()
        {
            //Extract data from WeightDifferenceVolumeFactor xml file, and store in list

            // get current working directory
            string cwd = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string WDFVXmlPath = cwd+@".\data\WDVF_signed table.xml";

            //xml file reader
            XmlReader weightDiffVolFactorXmlReader;

            //xml file settings
            XmlReaderSettings weightDiffVolFactorSettings = new XmlReaderSettings();

            //set xml file settings
            weightDiffVolFactorSettings.IgnoreComments = true;
            weightDiffVolFactorSettings.IgnoreWhitespace = true;

            try //attempt to open and read XML file 
            {
                weightDiffVolFactorXmlReader = XmlReader.Create(WDFVXmlPath, weightDiffVolFactorSettings);
               

                //read to first xml "Table" tag
                weightDiffVolFactorXmlReader.ReadToDescendant("Table");

                //read WeightDiffVolFactorXml contents, must use do_while in order to get first table node
                do
                {
                    //Get subtree containing Height, IdealWeight pairs
                    XmlReader subtree = weightDiffVolFactorXmlReader.ReadSubtree();

                    //temp variables
                    double weightDiff, volFactor;

                    //read to Height node by reading past table node
                    subtree.ReadStartElement("Table");

                    //capture WeighDiff value
                    weightDiff = subtree.ReadElementContentAsDouble();

                    //capture VolFactor value
                    volFactor = subtree.ReadElementContentAsDouble();

                    //create new object for list
                    WeightDifferenceVolumeFactorPair WtVolF = new WeightDifferenceVolumeFactorPair(weightDiff, volFactor);

                    //Console.Out.Write("WeightDiff = " + WeightDiff + ", VolFactor = " + VolFactor + "\n");

                    //append to list
                    _volumeFactors.Add(WtVolF);

                }//if while fails(i.e. error or EOF), XMLreader automatically closes xmlfile
                while (weightDiffVolFactorXmlReader.ReadToNextSibling("Table"));

                weightDiffVolFactorXmlReader.Close();
            }
            catch (XmlException exception)
            {
                //rethrow 
                throw new XmlException(exception.Message);
            }
        }

        // This implementation is going to be the parameterless method implementation
        public void GetWeightDifferenceVolumeFactorParameters(string filePath)
        {
           // string cwd = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
           // string WDFVXmlPath = cwd + @".\data\WDVF_signed table.xml";
            string WDFVXmlPath = filePath + @"WDVF_signed.xml";

            //xml file reader
            XmlReader weightDiffVolFactorXmlReader;

            //xml file settings
            XmlReaderSettings weightDiffVolFactorSettings = new XmlReaderSettings();

            //set xml file settings
            weightDiffVolFactorSettings.IgnoreComments = true;
            weightDiffVolFactorSettings.IgnoreWhitespace = true;

            try //attempt to open and read XML file 
            {
                weightDiffVolFactorXmlReader = XmlReader.Create(WDFVXmlPath, weightDiffVolFactorSettings);


                //read to first xml "Table" tag
                weightDiffVolFactorXmlReader.ReadToDescendant("Table");

                //read WeightDiffVolFactorXml contents, must use do_while in order to get first table node
                do
                {
                    //Get subtree containing Height, IdealWeight pairs
                    XmlReader subtree = weightDiffVolFactorXmlReader.ReadSubtree();

                    //temp variables
                    double weightDiff, volFactor;

                    //read to Height node by reading past table node
                    subtree.ReadStartElement("Table");

                    //capture WeighDiff value
                    weightDiff = subtree.ReadElementContentAsDouble();

                    //capture VolFactor value
                    volFactor = subtree.ReadElementContentAsDouble();

                    //create new object for list
                    WeightDifferenceVolumeFactorPair WtVolF = new WeightDifferenceVolumeFactorPair(weightDiff, volFactor);

                    //Console.Out.Write("WeightDiff = " + WeightDiff + ", VolFactor = " + VolFactor + "\n");

                    //append to list
                    _volumeFactors.Add(WtVolF);

                }//if while fails(i.e. error or EOF), XMLreader automatically closes xmlfile
                while (weightDiffVolFactorXmlReader.ReadToNextSibling("Table"));

                weightDiffVolFactorXmlReader.Close();
            }
            catch (XmlException exception)
            {
                //rethrow 
                throw new XmlException(exception.Message);
            }
        }
        
        #endregion

        #region GetIdealWeight 
       
        //  calculate patient's Ideal weight 
        //  Used to calculate ideal weight by quering extracted xml data, and using formula
        public double GetIdealWeight(double patientHeightCM)
        {
            double idealWt=0;
            IdealHeightWeightPair lowIdealWt = GetLowIdealWeight(patientHeightCM);
            IdealHeightWeightPair highIdealWt = GetHighIdealWeight(patientHeightCM);

            //If they are the same, no need to compute Ideal, table already contained the ideal
            if (lowIdealWt == highIdealWt)
            {
                idealWt = lowIdealWt.IdealWeightInKg;
                return idealWt;
            }

            //calculate ideal weight by interpolation
            idealWt = lowIdealWt.IdealWeightInKg +
                (highIdealWt.IdealWeightInKg - lowIdealWt.IdealWeightInKg)*
                    ((patientHeightCM - lowIdealWt.HeightInCm)/(highIdealWt.HeightInCm - lowIdealWt.HeightInCm));

            //Console.Out.Write("Ideal Weight = "+IdealWt+"\n");
            return idealWt;

        }

        // Retrieves low ideal weight from list
        private IdealHeightWeightPair GetLowIdealWeight(double patientHeightCM)
        {
            double difference = 1000; //set to large number for comparison reasons
            IdealHeightWeightPair idealLow = new IdealHeightWeightPair(0,0);
            
            //iterate through Ideal weight and find ideal low
            foreach( IdealHeightWeightPair weight in _idealHeightWeightPairs)
            {
                //check to see if new difference is less then old
                if (difference > Math.Abs( patientHeightCM - weight.HeightInCm ) && patientHeightCM >= weight.HeightInCm )
                {
                    difference = patientHeightCM - weight.HeightInCm;
                    idealLow = weight;
                }
                  
            }

            return idealLow;
        }

        // Retrieves high ideal weight from list
        private IdealHeightWeightPair GetHighIdealWeight(double PatientHeightCM)
        {
            double difference = 1000; //set to large number for comparison reasons
            IdealHeightWeightPair idealHigh = new IdealHeightWeightPair(0,0);

            //iterate through Ideal weight and find ideal high
            foreach (IdealHeightWeightPair weight in _idealHeightWeightPairs)
            {
                //check to see if new difference is less then old
                if (difference > Math.Abs(PatientHeightCM - weight.HeightInCm) && PatientHeightCM <= weight.HeightInCm)
                {
                    difference = PatientHeightCM - weight.HeightInCm;
                    idealHigh = weight;
                }

            }

            return idealHigh;
        }
        
        #endregion

        #region GetVolFactor
        // calculate patient's volume factor. 
        // Interpolates Volume Factor 
        public double GetVolFactor(double percentWeightDev)
        {
            double volumeFactor = 0;

            WeightDifferenceVolumeFactorPair lowVolFac = GetLowVolFactor(percentWeightDev);
            WeightDifferenceVolumeFactorPair highVolFac = GetHighVolFactor(percentWeightDev);

            //If they are the same, no need to interpolate, table already contained the exact value
            if (lowVolFac == highVolFac)
            {
                volumeFactor = lowVolFac.VolumeFactor;
                return volumeFactor;
            }

            //calculate volumefactor by interpolation
            volumeFactor = lowVolFac.VolumeFactor +
                (highVolFac.VolumeFactor - lowVolFac.VolumeFactor) *
                    ((percentWeightDev - lowVolFac.WeightDifference) / (highVolFac.WeightDifference - lowVolFac.WeightDifference));

            //Console.Out.Write("VolumeFacor = " + VolumeFactor + "\n");
            return volumeFactor;

        }

        // Retrieves low volfactor from list
        private WeightDifferenceVolumeFactorPair GetLowVolFactor(double percentWeightDev)
        {
            double difference = 1000; //set to large number for comparison reasons
            WeightDifferenceVolumeFactorPair lowVolFactor = new WeightDifferenceVolumeFactorPair(0, 0);

            //iterate through Ideal weight and find ideal low
            foreach (WeightDifferenceVolumeFactorPair volfac in _volumeFactors)
            {
                //check to see if new difference is less then old
                if (difference > Math.Abs(percentWeightDev - volfac.WeightDifference) && percentWeightDev >= volfac.WeightDifference)
                {
                    difference = percentWeightDev - volfac.WeightDifference;
                    lowVolFactor = volfac;
                }

            }

            return lowVolFactor;
        }

        //Purpose: Retrieves high volfactor from list
        private WeightDifferenceVolumeFactorPair GetHighVolFactor(double percentWeightDev)
        {
            double difference = 1000; //set to large number for comparison reasons
            WeightDifferenceVolumeFactorPair highVolFactor = new WeightDifferenceVolumeFactorPair(0, 0);

            //iterate through Ideal weight and find ideal low
            foreach (WeightDifferenceVolumeFactorPair volfac in _volumeFactors)
            {
                //check to see if new difference is less then old
                if (difference > Math.Abs(percentWeightDev - volfac.WeightDifference) && percentWeightDev <= volfac.WeightDifference)
                {
                    difference = percentWeightDev - volfac.WeightDifference;
                    highVolFactor = volfac;
                }

            }

            return highVolFactor;
        }

        #endregion

        #endregion
    }
}


   
