﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Daxor.Lab.CalcEngine.Controllers;
using Daxor.Lab.Infrastructure.DomainModels;

[assembly: InternalsVisibleTo("Daxor.Lab.CalcEngine.Tests")]
namespace Daxor.Lab.CalcEngine.Common
{
    public class MeasuredCalcEngineTestObject
    {
        #region Constants
        public const double PlasmaPackRatio = 0.99;
        public const double WBtoPHRatio = 0.91;
        #endregion

        #region Readonly
        
        private readonly MeasuredCalcEngineParams _measuredParms;
        
        #endregion

        #region Properties

        public int Background
        {
            get { return _measuredParms.Background; }
        }
        public double StandardCounts
        {
            get { return _measuredParms.StandardCounts; }
        }
        public double BaselineCounts
        {
            get { return _measuredParms.BaselineCounts; }
        }
        public double BaselineHCT
        {
            get { return _measuredParms.BaselineHematocrit; }
        }

        public BloodVolumePoint[] Points
        {
            get { return _measuredParms.Points; }
        }
        public double AntiCouglant
        {
            get { return _measuredParms.AnticoagulantFactor; }
        }
        public int ReferenceVolume
        {
            get { return _measuredParms.ReferenceVolume; }
        }
        #endregion

        #region Ctor

        public MeasuredCalcEngineTestObject(MeasuredCalcEngineParams parms)
        {
            _measuredParms = parms;
        }

        #endregion

        #region Methods

        public MeasuredCalcEngineResults RunBVACalculationsWithParms()
        {
            double timeZeroBV = 0;
            double timeZeroPV = 0;
            double timeZeroRCV = 0;
            double normalizeHCT = 0;
            double transudationRate = 0;   //Also known as slope
            double standardDeviation = 0;
            double hematocritForTheTest = 0;
            double normStandardDev = 0;
            List<UnadjustedBloodVolumeResult> ubv = new List<UnadjustedBloodVolumeResult>();

            hematocritForTheTest = ComputeTestHematocrit();

            //Calculate Unadjusted Blood Volume for each Point
            foreach (BloodVolumePoint p in Points)
            {
                if (p == null) continue;

                // calculates UBV and UPV: Not rounded
                // UPV is not part of BloodVolumePoint anymore! Not needed for any calculations.
                // it was needed for TBA calculations
                p.UnadjustedBloodVolume = BVAMeasuredCalculator.calcUBV(ReferenceVolume, AntiCouglant, StandardCounts, Background, BaselineCounts, p);
            }

            timeZeroBV = BVAMeasuredCalculator.calcFinalTBV(this, out transudationRate, out standardDeviation, out normStandardDev);
            timeZeroPV = BVAMeasuredCalculator.calcFinalPV(timeZeroBV, hematocritForTheTest);
            timeZeroRCV = timeZeroBV - timeZeroPV;

            if (_measuredParms.IdealTotalBloodVolume != 0)
            {
                normalizeHCT = ((timeZeroBV / _measuredParms.IdealTotalBloodVolume) * hematocritForTheTest);
            }
            foreach (BloodVolumePoint p in Points)
            {
                ubv.Add(new UnadjustedBloodVolumeResult(p.UniqueId, p.UnadjustedBloodVolume));
            }

            return new MeasuredCalcEngineResults(ubv, timeZeroBV, timeZeroPV,
                timeZeroRCV, transudationRate, standardDeviation, normalizeHCT, normStandardDev);
        }

        #endregion

        #region Helpers
        
        internal double ComputeTestHematocrit()
        {
            double hematocritSum = 0;
            double hematocritCount = 0;

            // Go through each Point and find hematocrit for the test
            foreach (BloodVolumePoint p in Points)
            {
                if (p == null || p.AreBothHematocritsExcluded) continue;

                if (!p.IsPointRejected)
                {
                    hematocritSum += p.Hematocrit;
                    hematocritCount++;
                }
            }

            // Can't take an average of zero things.
            if (hematocritCount == 0)
                return 0;

            return hematocritSum / hematocritCount;
        }
       
        #endregion

    }
}



