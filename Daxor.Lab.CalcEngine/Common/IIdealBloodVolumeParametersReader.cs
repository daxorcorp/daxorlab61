﻿using System.Collections.Generic;

namespace Daxor.Lab.CalcEngine.Common
{
    public interface IIdealBloodVolumeParametersReader
    {
        string FilePath { get; }

        List<IdealHeightWeightPair> GetHeightWeightParameters();

        List<WeightDifferenceVolumeFactorPair> GetWeightDifferenceVolumeFactorParameters();
    }
}
