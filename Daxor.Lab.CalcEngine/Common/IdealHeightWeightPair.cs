﻿namespace Daxor.Lab.CalcEngine.Common
{
    /// <summary>
    /// Represents an ordered pair of ideal weight and patient height.
    /// </summary>
    public class IdealHeightWeightPair
    {
        /// <summary>
        /// Initializes a new instance of the IdealHeightWeightPair class with
        /// the specified height and ideal weight.
        /// </summary>
        /// <param name="height">Height (in centimeters)</param>
        /// <param name="idealWeight">Ideal weight (in kilograms)</param>
        public IdealHeightWeightPair(double heightInCm, double idealWeightInKg)
        {
            HeightInCm = heightInCm;
            IdealWeightInKg = idealWeightInKg;
        }

        /// <summary>
        /// Gets or sets the height (in centimeters)
        /// </summary>
        public double HeightInCm { get; private set; }

        /// <summary>
        /// Gets or sets the ideal weight (in kilograms)
        /// </summary>
        public double IdealWeightInKg { get; private set; }

    }
}
