﻿namespace Daxor.Lab.CalcEngine.Common
{
    /// <summary>
    /// Represents an ordered pair of weight difference and volume factor.
    /// </summary>
    public class WeightDifferenceVolumeFactorPair
    {
        /// <summary>
        /// Initializes a new instance of the WeightDiffVolumeFactorPair class with
        /// the specified weight difference and volume factor.
        /// </summary>
        /// <param name="weightDifference">Weight difference</param>
        /// <param name="volumeFactor">Volume factor</param>
        public WeightDifferenceVolumeFactorPair(double weightDifference, double volumeFactor)
        {
            WeightDifference = weightDifference;
            VolumeFactor = volumeFactor;
        }

        /// <summary>
        /// Gets or sets the volume factor.
        /// </summary>
        public double VolumeFactor { get; private set; }

        /// <summary>
        /// Gets or sets the weight difference
        /// </summary>
        public double WeightDifference { get; private set; }
    }
}
