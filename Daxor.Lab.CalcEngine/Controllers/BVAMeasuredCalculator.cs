﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.CalcEngine.Common;
using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.CalcEngine.Controllers
{
    public class BVAMeasuredCalculator
    {
        #region CalcEngine Constants
        /// <summary>
        /// PlazmaPackRation  - Plazma Pack Ration is used in calculation of Unadjusted BVs.
        /// 
        /// WBtoPHRatio       - Whole Body to Peripheral Hematocrit Ratio is
        ///                     used in calculation of Unadjusted Blood Volume(s)(BV).
        ///                     
        /// PositionConverter - Position Converter is used to determine the wheel position based
        ///                     on patient sample number (1-6). In order to find the wheel position for: 
        ///                     SampleA the following calculation would be required: [sampleNumber + PositionConverter/10]
        ///                     SampleB the following calculation would be required: [PositionConverter - sampleNumber]
        ///                  
        /// Constant          - A hash which contains various BVA calculation constants
        /// </summary> 
        public const double PlasmaPackRatio = 0.99;
        public const double WBtoPHRatio = 0.91;
        public const double MaxUnadjusted = 30000; //double the max WBV so that we don't exclude some result data

        #endregion

        #region StaticMethods
       
        #region calcFinalPV
        /// <summary>
        /// Method:     CalculationEngine.CalcEngine.calcFinalPV
        /// 
        /// Summary:    Calculates time-zero Plasma Volume
        /// 
        /// Args:       DOUBLE timeZeroBV
        ///                 Time-zero(final) blood volume
        ///                 
        ///             DOUBLE test_Hematocrit
        ///                 Hematocrit value for the test
        /// 
        /// Modifies:   N/A
        /// 
        /// Returns:    DOUBLE
        ///                 Final (time-zero) Plasma Volume for a given test
        /// </summary>
        /// TimeZeroBloodVolume, m_hematocrit
        public static double calcFinalPV(double timeZeroBV, double test_Hematocrit)
        {

            // Check to see if time zero Blood Volume Exists
            if (timeZeroBV <= 0 || test_Hematocrit <= 0) return 0;

            double temp = timeZeroBV * test_Hematocrit * PlasmaPackRatio * WBtoPHRatio / 100;

            return timeZeroBV * (1.0 - (test_Hematocrit * PlasmaPackRatio * WBtoPHRatio / 100));
        }
        #endregion

        #region calcFinalTBV
        /// <summary>
        /// 
        /// Method:     CalculationEngine.CalcEngine.calcFinalTBV
        /// 
        /// Summary:    Calculates final Total Blood Volume for a given Blood Volume test
        /// 
        /// Args:       TBABVATEST bvaTest
        ///                 An instance of Blood Volume Test
        ///                 
        ///             OUT DOUBLE transudationRate
        ///                 Transudation Rate
        /// 
        /// Modifies:   transudationRate
        ///                 Calculated and filled inside the method
        /// 
        /// Returns:    DOUBLE
        ///                 Total Blood Volume (TBV) if no errors were encountered
        /// </summary>
        public static double calcFinalTBV(MeasuredCalcEngineTestObject bvaTest, out double transudationRate, out double stdDeviation, out double normStdDev)
        {
            double timeZeroBV = 0;
            double LogBV = 0;
            double tRate = 0;
            double stdDev = 0;
            double realStdDev = 0;
            try
            {
                // Number of Points which are used in BVA study; which is
                // 1. Total Number of Blood Draws for a given test minus 
                //      a. A point that has been rejected
                //      b. A point with both counts (A and B) excluded
                //       int numPoints = bvaTest.NumBloodDraw - bvaTest.NumExcluded - bvaTest.GetNumPointsWithExcludedCounts;

                //int numPoints = bvaTest.NumberOfGoodPoints;
                //int numPoints = bvaTest.NumberOfBloodDraws - bvaTest.NumberOfRejectedPoints - bvaTest.NumberOfBothCountsExcluded;
                var goodPoints = from point in bvaTest.Points
                                 where !point.IsPointRejected && !point.AreBothCountsExcluded && point.UnadjustedBloodVolume > 0
                                 select point;

                int numPoints = goodPoints.Count();

                // Every Single Points was excluded
                if (numPoints <= 0)
                {
                    transudationRate = tRate;
                    stdDeviation = Double.NaN;
                    normStdDev = Double.NaN;
                    return timeZeroBV;
                }
                // Calc. BV based on a single Point
                if (numPoints == 1)
                {
                    // Gets first good point in the test
                    BloodVolumePoint p = goodPoints.FirstOrDefault();

                    // Check to see if the time of post injection is greater than 0
                    // and Unadjusted Blood Volume is greater than 0
                    if (p.TimePostInjectionInSeconds > 0 && p.UnadjustedBloodVolume > 0)
                    {
                        // Need to change  Make it constant or move somewhere
                        tRate = 0.0025003;
                        timeZeroBV = Math.Exp(Math.Log(p.UnadjustedBloodVolume) - (tRate * p.TimePostInjectionInSeconds / 60));
                    }

                }
                else if (numPoints == 2)
                {
                    //Use straight line approximation for best BV
                    var fPoint = goodPoints.FirstOrDefault();
                    var lPoint = goodPoints.LastOrDefault();

                    //Make sure time have been entered
                    if (fPoint != null && lPoint != null && fPoint.TimePostInjectionInSeconds > 0 && lPoint.TimePostInjectionInSeconds > 0)
                    {
                        tRate = (Math.Log(lPoint.UnadjustedBloodVolume) - Math.Log(fPoint.UnadjustedBloodVolume)) / (lPoint.TimePostInjectionInSeconds - fPoint.TimePostInjectionInSeconds);
                        timeZeroBV = Math.Exp(Math.Log(lPoint.UnadjustedBloodVolume) - (tRate * lPoint.TimePostInjectionInSeconds));
                        tRate *= 60;
                        stdDev = 0;
                        normStdDev = 0;
                    }
                    else
                    {
                        tRate = 0;
                        stdDev = Double.NaN;
                        normStdDev = Double.NaN;
                    }
                }
                // calculate Blood Volume based on more than 2 points
                else
                {
                    // If calcBVtimeZero
                    if (calcIntermediateBVresults(bvaTest, out LogBV, out tRate, out stdDev) == 1)
                    {
                        realStdDev = stdDev;
                        stdDev = 0.5 * (Math.Exp(LogBV + stdDev) - Math.Exp(LogBV - stdDev));
                        stdDev /= Math.Exp(LogBV);
                        tRate *= 60;
                        timeZeroBV = Math.Exp(LogBV);

                    }
                }

                //Assign return values
                transudationRate = tRate;
                stdDeviation = stdDev;
                normStdDev = realStdDev;
                return timeZeroBV;
            }
            catch
            {
                // If exception has been thrown catch here
                // and set all the derived values to 0
                transudationRate = 0;
                stdDeviation = Double.NaN;
                normStdDev = Double.NaN;
                return 0;
            }

        }
        #endregion

        #region calcIntermediateBVresults
        /// <summary>
        /// 
        /// Method:     CalculationEngine.CalcEngine.calcIntermediateBVResults
        /// 
        /// Summary:    Calculates time-zero blood volume and transudation rate for a given BVA study.
        /// 
        /// Args:       TBABVATEST bvaTest
        ///                 Instance of the Blood Volume Test
        ///                 
        ///             OUT DOUBLE log_timeZeroBV
        ///                 Log of final Blood Volume
        ///                 
        ///             OUT DOUBLE transudationRate
        ///                 Transudation rate
        /// 
        /// Modifies:   log_timeZeroBV, transudationRate
        ///                 Both args gets filled insided the method
        /// 
        /// Returns:    DOUBLE
        ///                 Greater than zero implies calculation were successful.
        ///                 Less then zero otherwise.
        /// </summary>
        private static double calcIntermediateBVresults(MeasuredCalcEngineTestObject bvaTest, out double log_timeZeroBV, out double transudationRate, out double standardDev)
        {

            double sumOfPostInjectionTimes = 0;             // Sum of injection times
            double sumOfLogsUnadjustedBloodVolumes = 0;     // Sum of Unadjusted BV
            double avgPostInjectionTime;                    // Average Post Injection Time
            double sumOfSquareTimeVariance = 0;             // Sum of squares of time variance from mean time
            double tRate = 0;                               // Local variable that holds transudation rate
            double logBV = 0;                               // Local variable that holds blood volume result
            double stdDev = double.NaN;
            double numTotalGoodCounts = 0;                  // Num Total Good Counts
            List<object> PointsIDwithNegativeUBVs = new List<object>();

            try
            {

                // Number of Total Good Count is Total Number of Blood Draws for a given test minus 
                // 1. A point that has been rejected
                // 2. A point with both counts (A and B) excluded
                //numTotalGoodCounts = bvaTest.NumberOfGoodPoints;  //.NumBloodDraw - bvaTest.NumExcluded - bvaTest.GetNumPointsWithExcludedCounts;

                //numTotalGoodCounts = bvaTest.NumberOfBloodDraws - bvaTest.NumberOfRejectedPoints - bvaTest.NumberOfBothCountsExcluded;
                numTotalGoodCounts = (from point in bvaTest.Points
                                      where !point.IsPointRejected && !point.AreBothCountsExcluded && point.UnadjustedBloodVolume > 0
                                      select point).Count();

                // Calculate sum of Post Injection Times and sum of Logs of Unadjusted BVs.
                foreach (BloodVolumePoint p in bvaTest.Points)
                {
                    // First Point does not exists
                    if (p == null) continue;

                    if (!p.IsPointRejected)
                    {

                        // Determine if unadjusted blood volume was successfully calculated
                        // If not, excluded a given point from final calculation of TBV
                        if (p.UnadjustedBloodVolume <= 0)
                        {
                            PointsIDwithNegativeUBVs.Add(p.UniqueId);
                        }
                        else
                        {
                            sumOfPostInjectionTimes += p.TimePostInjectionInSeconds;
                            sumOfLogsUnadjustedBloodVolumes += Math.Log(p.UnadjustedBloodVolume);
                        }
                    }
                }


                // Calculate average postinjection time
                avgPostInjectionTime = sumOfPostInjectionTimes / numTotalGoodCounts;

                // Compute Sum of Squared Time Variance and intermediate
                // transudation rate (slope)
                double tempTimeVarience;
                foreach (BloodVolumePoint p in bvaTest.Points)
                {
                    if (p == null || PointsIDwithNegativeUBVs.Contains(p.UniqueId)) continue;

                    if (!p.IsPointRejected)
                    {

                        tempTimeVarience = p.TimePostInjectionInSeconds - avgPostInjectionTime;
                        sumOfSquareTimeVariance = sumOfSquareTimeVariance + tempTimeVarience * tempTimeVarience;
                        tRate = tRate + tempTimeVarience * Math.Log(p.UnadjustedBloodVolume);

                    }
                }

                // Final transudation Rate and blood volume calculation
                tRate = tRate / sumOfSquareTimeVariance;
                logBV = (sumOfLogsUnadjustedBloodVolumes - sumOfPostInjectionTimes * tRate) / numTotalGoodCounts;

                // Calculate sigmaA and simgaB as well as Chi Squared to
                // calculate the final standard deviation.
                double sigmaA = Math.Sqrt((1.0 + sumOfPostInjectionTimes * sumOfPostInjectionTimes / (numTotalGoodCounts * sumOfSquareTimeVariance)) / numTotalGoodCounts);
                double sigmaB = Math.Sqrt(1.0 / sumOfSquareTimeVariance);
                double chiSquare = 0;
                foreach (BloodVolumePoint p in bvaTest.Points)
                {
                    if (p == null) continue;

                    if (!p.IsPointRejected && !p.AreBothCountsExcluded && !PointsIDwithNegativeUBVs.Contains(p.UniqueId))
                    {
                        chiSquare += Math.Pow((Math.Log(p.UnadjustedBloodVolume) - logBV - tRate * p.TimePostInjectionInSeconds), 2);

                    }
                }
                // Minus 2 is because I am not using an array with based index 0 to store my sample(points)
                // Turtle: in WinBVA 5.4 has done so, and therefore had implemented a similar technique to 
                // calculate standard deviation.
                stdDev = sigmaA * (Math.Sqrt(chiSquare / (numTotalGoodCounts - 2)));

                // Assign final values:
                // 1. Slope of the Log function
                // 2. Log of Time Zero Blood Volume (later will take exp() to get rid of Log)
                // 3. Standard deviation.
                // Return 1 to signfy a success
                transudationRate = tRate;
                log_timeZeroBV = logBV;
                standardDev = stdDev;
                return 1;

            }
            catch
            {
                transudationRate = tRate;
                log_timeZeroBV = logBV;
                standardDev = stdDev;
                return -1;
            }


        }
        #endregion

        #region calcUBV
        /// <summary>
        /// Method:     CalculationEngine.CalcEngine.CalcUBV
        /// 
        /// Summary:    Calculates the unadjusted blood volume(UBV) for a given point/sample
        /// 
        /// Args:       INT refVolume
        ///                  Dilution reference volume for injectate
        ///                 
        ///             DOUBLE antiCouglant
        ///                 Anticouaglant value based on tube type for a given BVA
        ///                 
        ///             DOUBLE  standardCounts
        ///                 Average standard counts for a given point
        ///             
        ///             INT background
        ///                 BVA background counts
        ///                 
        ///             DOUBLE controlCounts
        ///                 BVA patient's control counts or baseline counts
        ///                 
        ///             bvPoint p
        ///                 An instance of BVA point
        ///                 
        /// Modifies:   N/A
        /// 
        /// Returns:    DOUBLE
        ///                 Returns unadjusted blood volume for a given point. If the return
        ///                 value is a negative number then:
        ///                     -11   : Invalid hematocrit
        ///                     -12   : Invalid reference volume; reference volume is outside the norm.
        ///                     -13   : Invalid antoCoaglant; antiCouagulant is outside the norm.
        ///                     -14   : Patient sample counts missing
        ///                     -15   : Control counts too close to patient sample counts
        ///                     -16   : Unadjusted Blood Volume less than 0
        ///                     -17   : Unadjusted Blood Volume greater than value for MaxUnadjusted element in DaxorConstants.xml
        /// </summary>
        public static double calcUBV(int refVolume, double antiCouglant, double standardCounts,
                                     int background, double controlCounts, BloodVolumePoint p)
        {
            /// Check if average hematocrit for a given point is valid
            if (p.Hematocrit <= 0 || p.Hematocrit > 100)
            {
                return -11;
            }

            /// Check if reference volume is invalid less than or equals to 0
            if (refVolume <= 0)
            {
                return -12;
            }

            /// Check if anti couaglant is outside the norm
            if (antiCouglant <= 0)
            {
                return -13;
            }

            /// Check if patient's sample counts are missing
            if (p.Counts == 0)
            {
                return -14;
            }

            /// Check if average Counts for a given point is less than baseline(control) counts
            if (p.Counts < controlCounts)
            {
                return -15;
            }



            /// Main Formula: C1 * V1 = C2 * V2  |==>  V2 = (C1 * V1)/C2 
            ///
            /// 
            /// Step 1: calculate C1 * V1 of standard that gets injected into patient
            p.C1TimesV1 = refVolume * antiCouglant * (standardCounts - background);


            /// Step 2: calculate Plasma VOlume which is V2 = (C1 * V1)/C2
            /// where C1 * V1 => C1TimesV1
            p.C2 = p.Counts - controlCounts;
            p.PlasmaVolume = p.C1TimesV1 / p.C2;

            /// Step 3: calculate Total Blood Volume (TBV) using plasmaVolume (PV) and RCV(Red Blood Cells Volume)
            /// (I) TBV = RCV + PV, where (II) RCV = Hematocrit * TBV. Let's substitute RCV in formula II
            /// TBV = (Hematocrit * TBV) + PV, implies TBV - Hematocrit * TBV) = PV. Next let's simplify
            /// TBV(1 - Hematocrit) = PV  ==> ### TBV = PV/(1 - Hematocrit) ### 
            ///
            p.InternalUnadjustedBloodVolume = p.PlasmaVolume / (1 - p.Hematocrit * PlasmaPackRatio * WBtoPHRatio / 100.0);

            // Determine if unadjusted blood volume is invalid
            if (p.InternalUnadjustedBloodVolume < 0) { return -16; }
            else if (p.InternalUnadjustedBloodVolume > MaxUnadjusted) return -17;

            return p.InternalUnadjustedBloodVolume;
        }

        #endregion

        #region calcUPV
        /// <summary>
        /// Method:     CalculationEngine.CalcEngine.CalcUPV
        /// 
        /// Summary:    Calculate Unadjusted Plazma Volume(UPV)
        ///             for a given sample number or point number
        /// 
        /// Args:       INT refVolume
        ///                 Dilution reference volume for injectate
        ///                 
        ///             DOUBLE antiCouglant
        ///                 Anticouaglant value based on tube type for a given BVA
        ///                 
        ///             DOUBLE standardCounts
        ///                 Standard counts for a given Blood Volume Analysis
        ///                 
        ///             INT backgroundCounts
        ///                 Background counts for a given Blood Volume Analysis
        ///                 
        ///             DOUBLE controlCounts
        ///                 Control/Baseline counts for a given Blood Volume Analysis
        ///                 
        ///             bvPoint p
        ///                 Sample/Point which is associated with this Unadjusted Plasma Volume
        /// 
        /// Modifies:   N/A
        /// 
        /// Returns:    DOUBLE
        ///                 Returns unadjusted plazma volume for a given sample/point number
        /// </summary>
        public static double calcUPV(int refVolume, double antiCouglant, double standardCounts,
                                     int backgroundCounts, double controlCounts, BloodVolumePoint p)
        {

            // If unadjusted blood volume has not been calculated; calculate it first
            if (p.UnadjustedBloodVolume < 0)
            {
                p.UnadjustedBloodVolume = calcUBV(refVolume, antiCouglant, standardCounts, backgroundCounts, controlCounts, p);
            }

            return p.UnadjustedBloodVolume * (1 - (p.Hematocrit * PlasmaPackRatio * WBtoPHRatio / 100));
        }
        #endregion

        #endregion
    }
}



