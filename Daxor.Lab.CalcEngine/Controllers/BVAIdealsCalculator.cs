﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.CalcEngine.Common;

namespace Daxor.Lab.CalcEngine.Controllers
{
    public class BVAIdealsCalculator
    {
        #region Constants
        //Conversion factor for Ideal female weight to ideal male weight
        public const double WeightCorrelationFactor = 1.08;

        //Ideal female HCT
        public const double IdealFemaleHCT = 0.40;

        //Ideal Male HCT
        public const double IdealMaleHCT = 0.45;

        #endregion

        private readonly IdealBloodVolumeXMLReader _xmlReader;

        public BVAIdealsCalculator(IdealBloodVolumeXMLReader reader)
        {
            _xmlReader = reader;
        }

        #region Properties

        /// <summary>
        /// Returns Double.MinValue if _idealWeights collection has not been filled, otherwise returns
        /// heightCm of the first element in the collection.
        /// </summary>
        public double MinimumHeightInCm
        {
            get { return (_xmlReader.IdealHeightWeightPairs.Count() == 0) ? Double.MinValue : _xmlReader.IdealHeightWeightPairs.First().HeightInCm; }
        }

        /// <summary>
        /// Returns Double.MaxValueif _idealWeights collection has not been filled, otherwise returns
        /// heightCm of the last element in the collection.
        /// </summary>
        public double MaximumHeightInCm
        {
            get { return (_xmlReader.IdealHeightWeightPairs.Count() == 0) ? Double.MaxValue : _xmlReader.IdealHeightWeightPairs.Last().HeightInCm; }
        }

        /// <summary>
        /// Returns Double.MinValue if _idealWeights collection has not been filled, otherwise returns
        /// IdealWeightKG of the first element in the collection.
        /// </summary>
        public double MinimumWeightInKg
        {
            get { return (_xmlReader.IdealHeightWeightPairs.Count() == 0) ? Double.MinValue : _xmlReader.IdealHeightWeightPairs.First().IdealWeightInKg; }
        }

        /// <summary>
        /// Returns Double.MinValue if _volumeFactors collection has not been filled, otherwise returns
        /// MinimumWeightDeviation of the first element in the collection.
        /// </summary>
        public double MaximumWeightDeviation
        {
            get { return (_xmlReader.IdealWeightDifferencePairs.Count() == 0) ? Double.MinValue : _xmlReader.IdealWeightDifferencePairs.Last().WeightDifference; }
        }

        /// <summary>
        /// Returns Double.MaxValueif _idealWeights collection has not been filled, otherwise returns
        /// IdealWeightKG of the last element in the collection.
        /// </summary>
        public double MaximumWeightInKg
        {
            get { return (_xmlReader.IdealHeightWeightPairs.Count() == 0) ? Double.MaxValue : _xmlReader.IdealHeightWeightPairs.Last().IdealWeightInKg; }
        }
        
        /// <summary>
        /// Returns Double.MinValue if _volumeFactors collection has not been filled, otherwise returns
        /// MinimumWeightDeviation of the first element in the collection.
        /// </summary>
        public double MinimumWeightDeviation
        {
            get { return (_xmlReader.IdealWeightDifferencePairs.Count() == 0) ? Double.MinValue : _xmlReader.IdealWeightDifferencePairs.First().WeightDifference; }
        }
        #endregion

        #region Methods

        public IdealsCalcEngineResults ComputeIdeals(double patientWeightKG, double patientHeightCM, int gender)
        {
            //Gender 1 - Male; 0 - Female; -1 - Undefined
            double idealWeight, percentWeightDeviation, volumeFactor;
            double idealBV, idealPV, idealRCV;


            //If gender is not valid
            if (gender == -1)
                return new IdealsCalcEngineResults(0, 0, 0, 0);

            //if not valid height, throw IdealBloodVolumeException 
            if (!IsValidHeight(patientHeightCM))
                return new IdealsCalcEngineResults(0, 0, 0, 0);


            //obtain the female ideal weight
            idealWeight = _xmlReader.GetIdealWeight(patientHeightCM);

            //if gender is male, adjust accordingly 
            if (gender == 1) 
                idealWeight *= WeightCorrelationFactor;

            //calculate percent weight deviation
            percentWeightDeviation = (patientWeightKG - idealWeight) / idealWeight * 100.0;

            if (!IsValidWeightDev(percentWeightDeviation))
                return new IdealsCalcEngineResults(0, 0, 0, percentWeightDeviation);

            //obtain volume factor for patient
            volumeFactor = _xmlReader.GetVolFactor(Math.Round(percentWeightDeviation, 1));

            //calculate Total BloodVolume
            idealBV = volumeFactor * patientWeightKG;

            //calculate Red Cell Volume
            if (gender == 0)
               idealRCV = idealBV * IdealFemaleHCT;

            else
                idealRCV = idealBV * IdealMaleHCT;

            //Adjustment factor
            idealRCV *= .99 * .91;

            //calculate Plasma Volume
            idealPV = idealBV - idealRCV;

            //Returns rounded values, as in WinBVA 5.3 - 5.4
            return new IdealsCalcEngineResults((int)Math.Round(idealPV) + (int)Math.Round(idealRCV), (int)Math.Round(idealPV), (int)Math.Round(idealRCV), percentWeightDeviation);
        }

        #endregion

        #region Helper Exception Classes
        //embedded exception class
        public class IdealBloodVolumeException : System.Exception
        {
            //extends the message ;
            public IdealBloodVolumeException(string message)
                : base(message)
            {
            }

        }
        //embedded exception class for file handling
        public class IdealBloodVolumeFileException : System.Exception
        {
            //extends the message ;
            public IdealBloodVolumeFileException(string message)
                : base(message)
            {
            }

        }

        #endregion

        #region Helper Validation Methods

        private bool IsValidHeight(double PatientHeightCM)
        {
            //if below Minimum and above maximum return false
            if (PatientHeightCM < MinimumHeightInCm || PatientHeightCM > MaximumHeightInCm)
                return false;

            return true;
        }
        private bool IsValidWeightDev(double WeightDev)
        {
            //if below Minimum and above maximum return false
            if (WeightDev < MinimumWeightDeviation || WeightDev > MaximumWeightDeviation)
                return false;

            return true;
        }

        #endregion

    }
}





