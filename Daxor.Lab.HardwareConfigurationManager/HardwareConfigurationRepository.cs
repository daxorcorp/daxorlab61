﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace Daxor.Lab.HardwareConfigurationManager
{
    /// <summary>
    /// A repository for a hardware configuration implemented with 
    /// XML stored on disk.
    /// </summary>
    public class HardwareConfigurationRepository : IHardwareConfigurationRepository
    {
        /// <summary>
        /// Creates a new HardwareConfigurationRepository instance
        /// </summary>
        /// <param name="configurationFilename">Name of the file that has the configuration XML</param>
        /// <param name="defaultFilename">Name of a file that has default configuration XML</param>
        /// <exception cref="ArgumentException">If either argument is null or empty</exception>
        public HardwareConfigurationRepository(string configurationFilename, string defaultFilename)
        {
            if (string.IsNullOrEmpty(configurationFilename)) throw new ArgumentException("configurationFileName");
            if (string.IsNullOrEmpty(defaultFilename)) throw new ArgumentException("defaultConfigurationFilename");

            Filename = configurationFilename;
            DefaultFilename = defaultFilename;
        }

        /// <summary>
        /// Gets the filename where the configuration is stored
        /// </summary>
        public string Filename { get; private set; }

        /// <summary>
        /// Gets the filename where the default configuration is stored
        /// </summary>
        public string DefaultFilename { get; private set; }

        /// <summary>
        /// Copies the XML found in the default file into the file used as the repository
        /// </summary>
        public void CreateDefaultConfiguration()
        {
            File.Copy(DefaultFilename, Filename, true);
        }

        /// <summary>
        /// Parses the XML found at Filename and builds a dictionary of HardwareSettings
        /// keyed on a string name
        /// </summary>
        /// <remarks>
        /// Any exceptions that occur when dealing with loading the file or parsing
        /// the contents are NOT CONSUMED (i.e., they are passed to the caller).
        /// </remarks>
        /// <returns>Dictionary of HardwareSettings</returns>
        public Dictionary<string, HardwareSetting> ReadAll()
        {
            var configurationXmlRoot = XElement.Load(Filename);
            var settingElements = from e in configurationXmlRoot.Elements("setting") select e;

            var configuration = new Dictionary<string, HardwareSetting>();

            foreach (var settingElement in settingElements)
            {
                var settingName = GetNameFor(settingElement);

                var setting = new HardwareSetting(settingName, GetTypeFor(settingElement).ToLower());
                switch (setting.Type)
                {
                    case HardwareSetting.StringType:
                        setting.SetValue(GetValueFor(settingElement));
                        break;
                    case HardwareSetting.IntegerType:
                        setting.SetValue(int.Parse(GetValueFor(settingElement)));
                        break;
                    case HardwareSetting.DoubleType:
                        setting.SetValue(double.Parse(GetValueFor(settingElement)));
                        break;
                    case HardwareSetting.BooleanType:
                        if (string.IsNullOrEmpty(GetValueFor(settingElement)))
                            throw new NotSupportedException("Value (boolean) cannot be empty");
                        setting.SetValue(GetValueFor(settingElement).ToUpperInvariant() == "T");
                        break;
                }

                setting.Description = GetDescriptionFor(settingElement);

                configuration.Add(setting.Name, setting);
            }

            return configuration;
        }

        /// <summary>
        /// Writes the given collection of settings in XML format to the file
        /// used as the repository
        /// </summary>
        /// <remarks>
        /// Any exceptions that occur when dealing with writing the file
        /// are NOT CONSUMED (i.e., they are passed to the caller).
        /// </remarks>
        /// <param name="configuration">Dictionary of HardwareSettings</param>
        /// <exception cref="ArgumentNullException">If the dictionary instance is null</exception>
        public void WriteAll(Dictionary<string, HardwareSetting> configuration)
        {
            if (configuration == null) throw new ArgumentNullException("configuration");

            var configurationXmlRoot = new XElement("HardwareConfiguration");

            foreach (var setting in configuration)
            {
                var hardwareSetting = setting.Value;
                var settingElement = new XElement("setting");
                settingElement.Add(new XAttribute("name", hardwareSetting.Name));
                settingElement.Add(new XAttribute("description", hardwareSetting.Description ?? string.Empty));
                settingElement.Add(new XAttribute("value", hardwareSetting.GetValue<object>() ?? string.Empty));
                settingElement.Add(new XAttribute("type", hardwareSetting.Type));
                configurationXmlRoot.Add(settingElement);
            }

            configurationXmlRoot.Save(Filename);
        }

        /// <summary>
        /// Deletes the configuration store if possible
        /// </summary>
        /// <returns>True if the file existed and has been removed; false
        /// if the file did not exist</returns>
        public bool TryAndDelete()
        {
            if (!File.Exists(Filename)) return false;

            File.Delete(Filename);
            return true;
        }

        /// <summary>
        /// Determines if the configuration store exists on disk (at Filename)
        /// </summary>
        /// <returns>True if the file exists; false otherwise</returns>
        public bool Exists()
        {
            return File.Exists(Filename);
        }

        #region Static helpers

        private static string GetNameFor(XElement parent)
        {
            // ReSharper disable once PossibleNullReferenceException
            return parent.Attribute("name").Value;
        }

        private static string GetTypeFor(XElement parent)
        {
            // ReSharper disable once PossibleNullReferenceException
            return parent.Attribute("type").Value;
        }

        private static string GetValueFor(XElement parent)
        {
            // ReSharper disable once PossibleNullReferenceException
            return parent.Attribute("value").Value;
        }

        private static string GetDescriptionFor(XElement parent)
        {
            // ReSharper disable once PossibleNullReferenceException
            return parent.Attribute("description").Value;
        }

        #endregion
    }
}
