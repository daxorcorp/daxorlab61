﻿namespace Daxor.Lab.HardwareConfigurationManager
{
    /// <summary>
    /// Defines the behavior of a service that manages hardware setting configuration
    /// state. A configuration is a collection of settings.
    /// </summary>
    public interface IHardwareConfigurationManager
    {
        /// <summary>
        /// Gets the setting based on the given key/name
        /// </summary>
        /// <param name="key">Identifier of the desired setting</param>
        /// <returns>HardwareSetting instance</returns>
        HardwareSetting GetSetting(string key);

        /// <summary>
        /// Saves the current configuration
        /// </summary>
        void SaveConfiguration();

        /// <summary>
        /// Replaces the current configuration with the default configuration
        /// </summary>
        void ResetToDefaultConfiguration();
    }
}
