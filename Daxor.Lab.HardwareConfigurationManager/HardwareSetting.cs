﻿using System;

namespace Daxor.Lab.HardwareConfigurationManager
{
    /// <summary>
    /// Represents a hardware setting
    /// </summary>
    public class HardwareSetting
    {
        /// <summary>
        /// Type name for string-based settings
        /// </summary>
        public const string StringType = "string";

        /// <summary>
        /// Type name for integer-based settings
        /// </summary>
        public const string IntegerType = "integer";

        /// <summary>
        /// Type name for decimal-based settings
        /// </summary>
        public const string DoubleType = "double";

        /// <summary>
        /// Type name for Boolean settings
        /// </summary>
        public const string BooleanType = "boolean";
        
        private object _value;

        /// <summary>
        /// Creates a new HardwareSetting instance based on the name (ID) and data type name
        /// </summary>
        /// <param name="name">Identifier for the setting</param>
        /// <param name="type">Type name for the setting</param>
        /// <exception cref="ArgumentException">
        /// If either argument is null or whitespace; if the given type is
        /// not supported</exception>
        public HardwareSetting(string name, string type)
        {
            if (string.IsNullOrWhiteSpace(name)) throw new ArgumentException("Name must be non-empty", "name");
            if (string.IsNullOrWhiteSpace(type)) throw new ArgumentException("Type must be non-empty", "type");

            Name = name;
            Type = type;

            switch (type)
            {
                case StringType: InnerType = typeof (string); break;
                case IntegerType: InnerType = typeof (int); break;
                case DoubleType: InnerType = typeof (double); break;
                case BooleanType: InnerType = typeof (bool); break;
                default: throw new ArgumentException("Type not supported", "type");
            }
        }

        /// <summary>
        /// Gets the setting name
        /// </summary>
        /// <remarks>By design, the name should not be changed once initialized</remarks>
        public string Name { get; private set; }

        /// <summary>
        /// Gets and sets the setting description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets the setting type name
        /// </summary>
        /// <remarks>By design, the type should not be changed once initialized</remarks>
        public string Type { get; private set; }

        private Type InnerType { get; set; }

        /// <summary>
        /// Gets the value of the setting cast as the given type
        /// </summary>
        /// <typeparam name="T">Desired type for the setting value</typeparam>
        /// <returns>The setting value; if the value has not been set, returns
        /// default(T)</returns>
        public T GetValue<T>()
        {
            if (_value == null)
                return default(T);

            return (T) _value;
        }

        /// <summary>
        /// Sets the value of the setting
        /// </summary>
        /// <typeparam name="T">Desired type for the setting value</typeparam>
        /// <param name="value">Value to set</param>
        /// <exception cref="InvalidCastException">If the type T doesn't match
        /// the existing setting type</exception>
        public void SetValue<T>(T value)
        {
            if (!InnerType.IsInstanceOfType(value)) 
                throw new InvalidCastException("Cannot set value because this setting is of type '" + Type + "'");

            _value = value;
        }
    }
}
