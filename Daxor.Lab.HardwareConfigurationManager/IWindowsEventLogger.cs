﻿namespace Daxor.Lab.HardwareConfigurationManager
{
    /// <summary>
    /// Defines behavior for a Windows event logger
    /// </summary>
    public interface IWindowsEventLogger
    {
        /// <summary>
        /// Logs the given message
        /// </summary>
        /// <param name="message">Message contents</param>
        void Log(string message);
    }
}
