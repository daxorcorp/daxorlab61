﻿using System.Diagnostics;

namespace Daxor.Lab.HardwareConfigurationManager
{
    /// <summary>
    /// A logger that writes messages to a named Windows event log
    /// </summary>
    public class WindowsEventLogger : IWindowsEventLogger
    {
        /// <summary>
        /// Maximum log size (in KB)
        /// </summary>
        public const int MaximumLogSizeInKb = 1024*1024;

        /// <summary>
        /// Number of days to retain log entries before purging
        /// </summary>
        public const int NumberOfDaysToRetainLogs = 0;

        /// <summary>
        /// Creates a new WindowsEventLogger instance. If the event log source
        /// doesn't already exist, it will be created.
        /// </summary>
        /// <param name="logSourceName">Identifier for the log source</param>
        /// <param name="logName">Friendly name of the event log source</param>
        public WindowsEventLogger(string logSourceName, string logName)
        {
            if (!EventLog.SourceExists(logSourceName))
                EventLog.CreateEventSource(logSourceName, logName);

            EventLog = new EventLog
            {
                Source = logSourceName,
                MaximumKilobytes = MaximumLogSizeInKb,
            };
            EventLog.ModifyOverflowPolicy(OverflowAction.OverwriteAsNeeded, NumberOfDaysToRetainLogs);
        }

        protected EventLog EventLog { get; set; }

        /// <summary>
        /// Logs the given message to the event log
        /// </summary>
        /// <param name="message">Message contents</param>
        public void Log(string message)
        {
            EventLog.WriteEntry(message);
        }
    }
}
