﻿using System.Collections.Generic;

namespace Daxor.Lab.HardwareConfigurationManager
{
    /// <summary>
    /// Defines the behavior of a repository for a hardware configuration
    /// (i.e., collection of hardware-specific settings)
    /// </summary>
    public interface IHardwareConfigurationRepository
    {
        /// <summary>
        /// Gets the filename where the configuration is stored
        /// </summary>
        string Filename { get; }

        /// <summary>
        /// Gets the filename where the default configuration is stored
        /// </summary>
        string DefaultFilename { get; }

        /// <summary>
        /// Creates a file (at Filename) with the contents found in the
        /// file stored at DefaultFilename
        /// </summary>
        void CreateDefaultConfiguration();

        /// <summary>
        /// Reads all settings from the configuration
        /// </summary>
        /// <returns>Dictionary of HardwareSettings keyed on a string name</returns>
        Dictionary<string, HardwareSetting> ReadAll();

        /// <summary>
        /// Writes the given configuration to disk
        /// </summary>
        /// <param name="configuration"></param>
        void WriteAll(Dictionary<string, HardwareSetting> configuration);

        /// <summary>
        /// Deletes the configuration store if possible
        /// </summary>
        /// <returns>True if the file existed and has been removed; false
        /// if the file did not exist</returns>
        bool TryAndDelete();

        /// <summary>
        /// Determines if the configuration store exists on disk (at Filename)
        /// </summary>
        /// <returns>True if the file exists; false otherwise</returns>
        bool Exists();
    }
}
