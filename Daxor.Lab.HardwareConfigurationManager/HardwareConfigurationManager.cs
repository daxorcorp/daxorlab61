﻿using System;
using System.Collections.Generic;

namespace Daxor.Lab.HardwareConfigurationManager
{
    /// <summary>
    /// A service that acts as cache for hardware settings (e.g., MCA fine gain).
    /// 
    /// The intent is that each unique hardware instance has its own 
    /// HardwareConfigurationManager to keep settings separated.
    /// 
    /// By design, new settings cannot be added via the manager API as this
    /// is not functionality needed by the system.
    /// </summary>
    public class HardwareConfigurationManager : IHardwareConfigurationManager
    {
        /// <summary>
        /// Creates a new HardwareConfigurationManager instance and loads the current configuration
        /// </summary>
        /// <param name="eventLogger">Instance of a Windows event logger</param>
        /// <param name="configurationRepository">Instance of a repository to
        /// handle manipulating the configuration files</param>
        /// <exception cref="ArgumentNullException">If either argument is null</exception>
        public HardwareConfigurationManager(IWindowsEventLogger eventLogger, IHardwareConfigurationRepository configurationRepository)
        {
            if (eventLogger == null) throw new ArgumentNullException("eventLogger");
            if (configurationRepository == null) throw new ArgumentNullException("configurationRepository");

            EventLogger = eventLogger;
            HardwareConfigurationRepository = configurationRepository;

            LoadConfiguration();
        }

        /// <summary>
        /// Gets the setting based on the given key/name
        /// </summary>
        /// <param name="key">Identifier of the desired setting</param>
        /// <returns>HardwareSetting instance</returns>
        /// <exception cref="ArgumentException">If the given key doesn't exist
        /// in the configuration</exception>
        public HardwareSetting GetSetting(string key)
        {
            if(!Configuration.ContainsKey(key)) 
                throw new ArgumentException("No setting exists under the name '" + key + "'", "key");

            return Configuration[key];
        }

        /// <summary>
        /// Saves the current configuration using the repository
        /// </summary>
        public void SaveConfiguration()
        {
            EventLogger.Log("A request was made to write settings.");
            HardwareConfigurationRepository.WriteAll(Configuration);
            EventLogger.Log("'" + HardwareConfigurationRepository.Filename + "' has been saved.");
        }

        /// <summary>
        /// Replaces the current configuration with the default configuration by
        /// deleting the existing configuration, creating a default configuration, and
        /// then reading in the newly created configuration.
        /// </summary>
        public void ResetToDefaultConfiguration()
        {
            EventLogger.Log("A request was made to reset the hardware settings.");

            if (HardwareConfigurationRepository.TryAndDelete())
                EventLogger.Log("'" + HardwareConfigurationRepository.Filename + "' has been deleted.");
            else
                EventLogger.Log("'" + HardwareConfigurationRepository.Filename + "' did not exist. No modifications made.");

            HardwareConfigurationRepository.CreateDefaultConfiguration();
            Configuration = HardwareConfigurationRepository.ReadAll();

            EventLogger.Log("The default configuration has been restored.");
        }

        protected Dictionary<string, HardwareSetting> Configuration { get; set; }

        protected IWindowsEventLogger EventLogger { get; set; }

        protected IHardwareConfigurationRepository HardwareConfigurationRepository { get; set; }

        private void LoadConfiguration()
        {
            EventLogger.Log("A request was made to read the hardware settings.");
            if (!HardwareConfigurationRepository.Exists())
            {
                EventLogger.Log("'" + HardwareConfigurationRepository.Filename +
                                "' does not exist, so " + HardwareConfigurationRepository.DefaultFilename + " will be used as defaults.");
                HardwareConfigurationRepository.CreateDefaultConfiguration();
            }
            else
            {
                EventLogger.Log("'" + HardwareConfigurationRepository.Filename + "' exists, so it will be loaded.");
            }

            Configuration = HardwareConfigurationRepository.ReadAll() ?? new Dictionary<string, HardwareSetting>();
        }
    }
}
