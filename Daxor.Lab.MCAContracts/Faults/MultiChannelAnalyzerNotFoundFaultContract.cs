﻿using System.Runtime.Serialization;

namespace Daxor.Lab.MCAContracts.Faults
{
    /// <summary>
    /// Fault (exception) payload for when a multi-channel analyzer was not found
    /// by the service
    /// </summary>
    [DataContract(Namespace = "http://daxor.com/mca/2010/08/exceptions")]
    public class MultiChannelAnalyzerNotFoundFaultContract : FaultContract
    {
        /// <summary>
        /// Creates a new fault contract instance with the message appropriate for the fault
        /// </summary>
        public MultiChannelAnalyzerNotFoundFaultContract()
        {
            Message = "Multichannel analyzer not found";
        }

        /// <summary>
        /// Gets or sets the identifier for the multi-channel analyzer
        /// </summary>
        [DataMember(IsRequired = true)]
        public string MultiChannelAnalyzerId { get; set; }
    }
}
