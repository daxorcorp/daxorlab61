﻿using System.Runtime.Serialization;

namespace Daxor.Lab.MCAContracts.Faults
{
    /// <summary>
    /// Represents the minimum payload for a service-level fault (exception)
    /// </summary>
    [DataContract(Namespace = "http://daxor.com/mca/2010/08/exceptions")]
    public abstract class FaultContract
    {
        /// <summary>
        /// Get or sets the fault message
        /// </summary>
        [DataMember(IsRequired = true)]
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the name of the method that produced the fault
        /// </summary>
        [DataMember(IsRequired = false)]
        public string Method { get; set; }
    }
}
