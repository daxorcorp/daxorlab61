﻿using System.Runtime.Serialization;

namespace Daxor.Lab.MCAContracts.Faults
{
    /// <summary>
    /// Fault (exception) payload for when the multi-channel analyzer controller
    /// could not be initialized
    /// </summary>
    [DataContract(Namespace = "http://daxor.com/mca/2010/08/exceptions")]
    public class MultiChannelAnalyzerControllerFaultContract : FaultContract
    {
    }
}
