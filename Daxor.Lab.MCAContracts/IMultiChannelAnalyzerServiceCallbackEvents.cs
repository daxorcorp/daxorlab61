﻿using System;
using System.ServiceModel;

namespace Daxor.Lab.MCAContracts
{
    /// <summary>
    /// Represents the events raised by the multi-channel analyzer service
    /// </summary>
    public interface IMultiChannelAnalyzerServiceCallbackEvents
    {
        /// <summary>
        /// Occurs when a multi-channel analyzer is connected
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that was connected</param>
        [OperationContract(IsOneWay = true)]
        void MultiChannelAnalyzerConnected(string mcaId);

        /// <summary>
        /// Occurs when a multi-channel analyzer is disconnected
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that was disconnected</param>
        [OperationContract(IsOneWay = true)]
        void MultiChannelAnalyzerDisconnected(string mcaId);

        /// <summary>
        /// Occurs when a multi-channel analyzer has completed its acquisition
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that completed its acquisition</param>
        /// <param name="completedSpectrum"></param>
        [OperationContract(IsOneWay = true)]
        void AcquisitionCompleted(string mcaId, SpectrumData completedSpectrum);

        /// <summary>
        /// Occurs when a multi-channel analyzer has started acquiring
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that started acquiring</param>
        /// <param name="startTime">Time that acquisition started</param>
        [OperationContract(IsOneWay = true)]
        void AcquisitionStarted(string mcaId, DateTime startTime);

        /// <summary>
        /// Occurs when a multi-channel analyzer has stopped acquiring
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA that stopped acquiring</param>
        /// <param name="endTime">Time the acquisition stopped</param>
        [OperationContract(IsOneWay = true)]
        void AcquisitionStopped(string mcaId, DateTime endTime);
    }
}
