﻿using System.ServiceModel;

namespace Daxor.Lab.MCAContracts
{
    /// <summary>
    /// Represents the behavior of an WCF entity that can save and reset configurations
    /// for a multi-channel analyzer
    /// </summary>
    [ServiceContract(Namespace = "http://daxor.com/mca/2010/08")]
    public interface IConfigurableContract
    {
        /// <summary>
        /// Saves the configuration files for each multi-channel analyzer
        /// </summary>
        [OperationContract]
        void SaveConfigurationsForAllMultiChannelAnalyzers();

        /// <summary>
        /// Resets a given multi-channel analyzer to its default configuration
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA to have its configuration file reset</param>
        [OperationContract]
        void ResetMultiChannelAnalyzerToDefaultConfiguration(string mcaId);
    }
}
