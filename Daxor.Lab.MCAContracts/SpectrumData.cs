﻿using System;
using System.Runtime.Serialization;

namespace Daxor.Lab.MCAContracts
{
    /// <summary>
    /// Represents a set of properties associated with a measured spectrum
    /// </summary>
    [DataContract(Namespace = "http://daxor.com/mca/2010/08/schemas")]
    public class SpectrumData
    {
        #region Fields

        #endregion

        #region Ctor

        /// <summary>
        /// Creates a new SpectrumData instance based on the given parameters
        /// </summary>
        /// <param name="realTimeInMicroseconds">Real time (in microseconds)</param>
        /// <param name="liveTimeInMicroseconds">Live time (in microseconds)</param>
        /// <param name="deadTimeInMicroseconds">Dead time (in microseconds)</param>
        /// <param name="spectrumArray">Array of count data</param>
        public SpectrumData(double realTimeInMicroseconds, double liveTimeInMicroseconds, 
            double deadTimeInMicroseconds, UInt32[] spectrumArray)
        {
            RealTimeInMicroseconds = realTimeInMicroseconds;
            LiveTimeInMicroseconds = liveTimeInMicroseconds;
            DeadTimeInMicroseconds = deadTimeInMicroseconds;
            SpectrumArray = spectrumArray;
        }

        /// <summary>
        /// Creates an empty spectrum with zero count times and a null array of count data
        /// </summary>
        /// <returns>See summary</returns>
        public static SpectrumData Empty()
        {
            return new SpectrumData(0, 0, 0, null);
        }

        #endregion

        /// <summary>
        /// Real time (in microseconds) for the measured spectrum
        /// </summary>
        [DataMember(Order = 1, IsRequired = true)]
        public double RealTimeInMicroseconds { get; set; }

        /// <summary>
        /// Live time (in microseconds) for the measured spectrum
        /// </summary>
        [DataMember(Order = 2, IsRequired = true)]
        public double LiveTimeInMicroseconds { get; set; }

        /// <summary>
        /// Dead time (in microseconds) for the measured spectrum
        /// </summary>
        [DataMember(Order = 3, IsRequired = true)]
        public double DeadTimeInMicroseconds { get; set; }

        /// <summary>
        /// Spectrum array (i.e., counts) for the measured spectrum
        /// </summary>
        [DataMember(Order = 4, IsRequired = true)]
        public UInt32[] SpectrumArray { get; set; }

        /// <summary>
        /// Creates a new instance with the times copied from this instance and
        /// the spectrum array pointing to this array
        /// </summary>
        /// <returns>See summary</returns>
        public SpectrumData GetShallowCopy()
        {
            return (SpectrumData)MemberwiseClone();
        }
    }
}
