﻿using System.ServiceModel;

namespace Daxor.Lab.MCAContracts
{
    /// <summary>
    /// Represents the behavior of an WCF entity that can subscribe to and 
    /// unsubscribe from a WCF service
    /// </summary>
    [ServiceContract(Namespace = "http://daxor.com/mca/2010/08")]
    public interface IServiceSubscriberContract
    {
        /// <summary>
        /// Subscribes to the service
        /// </summary>
        [OperationContract]
        void Subscribe();

        /// <summary>
        /// Unsubscribes from the service
        /// </summary>
        [OperationContract]
        void Unsubscribe();
    }
}
