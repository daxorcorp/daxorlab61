﻿using System.Collections.Generic;
using System.ServiceModel;

namespace Daxor.Lab.MCAContracts
{
    /// <summary>
    /// Represents the behavior of a WCF entity that manages multiple multi-channel
    /// analyzers
    /// </summary>
    [ServiceContract(Namespace = "http://daxor.com/mca/2010/08",
                     CallbackContract = typeof(IMultiChannelAnalyzerServiceCallbackEvents),
                     SessionMode = SessionMode.Required)]
    public interface IMultiChannelAnalyzerServiceContract : IMultiChannelAnalyzerContract, IServiceSubscriberContract, IConfigurableContract
    {
        /// <summary>
        /// Gets the list of identifiers for all known multi-channel analyzers,
        /// connected or disconnected
        /// </summary>
        /// <returns>List of identifiers for all MCAs; empty list if no MCAs</returns>
        [OperationContract]
        IList<string> GetAllMultiChannelAnalyzerIds();
    }
}
