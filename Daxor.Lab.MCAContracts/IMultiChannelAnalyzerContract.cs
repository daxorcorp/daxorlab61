﻿using System.ServiceModel;
using Daxor.Lab.MCAContracts.Faults;

namespace Daxor.Lab.MCAContracts
{
    /// <summary>
    /// Represents the behavior of an WCF entity that communicates with a multi-channel analyzer
    /// </summary>
    [ServiceContract(Namespace = "http://daxor.com/mca/2010/08")]
    public interface IMultiChannelAnalyzerContract
    {
        /// <summary>
        /// Gets the high voltage for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>High voltage (in volts)</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        int GetHighVoltage(string mcaId);

        /// <summary>
        /// Sets the high voltage for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <param name="highVoltage">High voltage to set</param>
        [OperationContract(IsOneWay = false)]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        void SetHighVoltage(string mcaId, int highVoltage);

        /// <summary>
        /// Gets the fine gain for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>Fine gain</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        double GetFineGain(string mcaId);

        /// <summary>
        /// Sets the fine gain for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <param name="fineGain">Fine gain to set</param>
        [OperationContract(IsOneWay = false)]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        void SetFineGain(string mcaId, double fineGain);

        /// <summary>
        /// Gets the noise level for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>Noise level (channel)</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        int GetNoiseLevel(string mcaId);

        /// <summary>
        /// Sets the noise level for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <param name="noiseLevel">Noise level to set</param>
        [OperationContract(IsOneWay = false)]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        void SetNoiseLevel(string mcaId, int noiseLevel);

        /// <summary>
        /// Gets the lower-level discriminator for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>Lower-level discriminator (channel)</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        int GetLowerLevelDiscriminator(string mcaId);

        /// <summary>
        /// Sets the lower-level discriminator for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <param name="lowerLevelDiscriminator">Lower-level discriminator to set</param>
        [OperationContract(IsOneWay = false)]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        void SetLowerLevelDiscriminator(string mcaId, int lowerLevelDiscriminator);

        /// <summary>
        /// Gets whether or not the corresponding multi-channel analyzer is acquiring
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if acquisition is in progress; false otherwise</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        bool IsAcquiring(string mcaId);

        /// <summary>
        /// Gets the serial number for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>Serial number</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        int GetSerialNumber(string mcaId);

        /// <summary>
        /// Gets the spectrum length for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>Spectrum length (in channels)</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        int GetSpectrumLength(string mcaId);

        /// <summary>
        /// Gets the analog-to-digital high voltage for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>ADC high voltage (in volts)</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        int GetAnalogToDigitalConverterHighVoltage(string mcaId);

        /// <summary>
        /// Gets whether or not the corresponding multi-channel analyzer is connected
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if the MCA is connected; false otherwise</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        bool IsConnected(string mcaId);

        /// <summary>
        /// Gets whether or not the real time is preset for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if the real time is preset; false otherwise</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        bool IsRealTimePreset(string mcaId);

        /// <summary>
        /// Gets whether or not the live time is preset for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if the live time is preset; false otherwise</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        bool IsLiveTimePreset(string mcaId);

        /// <summary>
        /// Gets whether or not the gross count in the region of interest is preset 
        /// for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if the gross count in the region of interest is preset; false otherwise</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        bool IsGrossCountPreset(string mcaId);

        /// <summary>
        /// Gets whether or not acquisition has completed for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if acquisition has completed; false otherwise</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        bool HasAcquisitionCompleted(string mcaId);

        /// <summary>
        /// Shuts down the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        [OperationContract(IsOneWay = false)]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        void ShutDown(string mcaId);

        /// <summary>
        /// Gets the spectrum data for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>Spectrum data</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        SpectrumData GetSpectrumData(string mcaId);

        /// <summary>
        /// Presets the count time for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <param name="isLiveTime">True if live time should be preset; false for real time</param>
        /// <param name="presetTimeInSeconds">Time limit (in seconds)</param>
        [OperationContract(IsOneWay = false)]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        void PresetTime(string mcaId, bool isLiveTime, long presetTimeInSeconds);

        /// <summary>
        /// Presets the gross counts in the region of interest for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <param name="counts">Count limit</param>
        /// <param name="beginChannel">Beginning channel</param>
        /// <param name="endChannel">Ending channel</param>
        [OperationContract(IsOneWay = false)]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        void PresetGrossCountInRegionOfInterest(string mcaId, long counts, int beginChannel, int endChannel);

        /// <summary>
        /// Clears the spectrum and preset limits for the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        [OperationContract(IsOneWay = false)]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        void ClearSpectrumAndPresets(string mcaId);

        /// <summary>
        /// Starts acquisition using the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        /// <returns>True if acquisition started successfully; false otherwise</returns>
        [OperationContract]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        bool StartAcquisition(string mcaId);

        /// <summary>
        /// Stops acquisition on the corresponding multi-channel analyzer
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA</param>
        [OperationContract(IsOneWay = false)]
        [FaultContract(typeof(MultiChannelAnalyzerNotFoundFaultContract))]
        void StopAcquisition(string mcaId);
    }
}
