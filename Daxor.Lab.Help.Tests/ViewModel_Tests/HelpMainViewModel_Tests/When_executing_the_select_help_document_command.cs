﻿using System.IO;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpMainViewModel_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_executing_the_select_help_document_command
	{
		private IAppModuleNavigator _mockAppModuleNavigator;
		private IEventAggregator _stubAggregator;
		private IHelpDocumentManager _stubDocumentManager;

		private FileInfo _stubFileInfo;

		// ReSharper disable once NotAccessedField.Local
		private HelpMainViewModel _viewModel;

		[TestMethod]
		public void The_select_help_document_command_activates_the_document_view()
		{
			//Arrange
			_mockAppModuleNavigator = Substitute.For<IAppModuleNavigator>();
			_stubDocumentManager = Substitute.For<IHelpDocumentManager>();
			_stubAggregator = Substitute.For<IEventAggregator>();
			_stubFileInfo = new FileInfo("TestFile.cs");

			_stubAggregator.GetEvent<HelpViewChanged>().Returns(Substitute.For<HelpViewChanged>());
			_stubAggregator.GetEvent<ScrollViewerPageInfoChanged>().Returns(Substitute.For<ScrollViewerPageInfoChanged>());

			//Act
			//The command is called in SelectedFile's setter.
			_viewModel = new HelpMainViewModel(_mockAppModuleNavigator, _stubDocumentManager, _stubAggregator, Substitute.For<IMessageBoxDispatcher>()) { SelectedFile = _stubFileInfo };

			//Assert
			_mockAppModuleNavigator.Received(1).ActivateView(HelpModuleViewKeys.DocumentView, _stubFileInfo);
		}
	}
	// ReSharper restore InconsistentNaming
}
