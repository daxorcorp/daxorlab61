﻿using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpMainViewModel_Tests
{
	public class HelpMainViewModelThatDoesNotEvaluateScrollCommandCanExecute : HelpMainViewModel
	{
		public bool EvaluateCommandCanExecuteWasCalled;

		public HelpMainViewModelThatDoesNotEvaluateScrollCommandCanExecute([Dependency(AppModuleIds.Help)] IAppModuleNavigator appModuleNavigator, IHelpDocumentManager helpDocumentManager, IEventAggregator eventAggregator, IMessageBoxDispatcher messageBoxDispatcher)
			: base(appModuleNavigator, helpDocumentManager, eventAggregator, messageBoxDispatcher)
		{
		}

		protected override void EvaluateScrollCommandCanExecute()
		{
			EvaluateCommandCanExecuteWasCalled = true;
		}
	}
}
