﻿using System;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpMainViewModel_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_using_the_page_navigation_commands
	{
		private IAppModuleNavigator _stubNavigator;
		private IHelpDocumentManager _stubHelpDocumentManager;
		private IEventAggregator _mockEventAggregator;

		private HelpViewChanged _stubHelpViewChangedEvent;
		private ScrollViewerScrollChanged _mockScrollViewerScrollChangedEvent;
		private ScrollViewerPageInfoChanged _stubScrollViewerPageInfoChangedEvent;

		private HelpMainViewModel _helpMainViewModel;

		[TestInitialize]
		public void Initialize()
		{
			_stubNavigator = Substitute.For<IAppModuleNavigator>();
			_stubHelpDocumentManager = Substitute.For<IHelpDocumentManager>();
			_mockEventAggregator = Substitute.For<IEventAggregator>();

			_stubHelpViewChangedEvent = Substitute.For<HelpViewChanged>();
			_stubHelpViewChangedEvent.Subscribe(Arg.Any<Action<object>>()).Returns(Substitute.For<SubscriptionToken>());

			_stubScrollViewerPageInfoChangedEvent = Substitute.For<ScrollViewerPageInfoChanged>();
			_stubScrollViewerPageInfoChangedEvent.Subscribe(Arg.Any<Action<object>>()).Returns(Substitute.For<SubscriptionToken>());

			_mockScrollViewerScrollChangedEvent = Substitute.For<ScrollViewerScrollChanged>();
			_mockScrollViewerScrollChangedEvent.Subscribe(Arg.Any<Action<ScrollViewerScrollChangedEventPayload>>()).Returns(Substitute.For<SubscriptionToken>());

			_mockEventAggregator.GetEvent<HelpViewChanged>().Returns(_stubHelpViewChangedEvent);
			_mockEventAggregator.GetEvent<ScrollViewerScrollChanged>().Returns(_mockScrollViewerScrollChangedEvent);
			_mockEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Returns(_stubScrollViewerPageInfoChangedEvent);

			_helpMainViewModel = new HelpMainViewModel(_stubNavigator, _stubHelpDocumentManager, _mockEventAggregator, Substitute.For<IMessageBoxDispatcher>());
		}

		[TestMethod]
		public void And_the_data_is_still_loading_then_the_scroll_page_top_command_is_disabled()
		{
			_helpMainViewModel.IsInitialDataLoading = true;
			_helpMainViewModel.StartingIndex = "2";

			Assert.IsFalse(_helpMainViewModel.ScrollPageTopCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_data_is_finished_loading_and_the_starting_index_is_one_then_the_scroll_page_top_command_is_disabled()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.StartingIndex = "1";

			Assert.IsFalse(_helpMainViewModel.ScrollPageTopCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_data_is_finished_loading_and_the_starting_index_is_a_dash_then_the_scroll_page_top_command_is_disabled()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.StartingIndex = "-";

			Assert.IsFalse(_helpMainViewModel.ScrollPageTopCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_data_is_finished_loading_and_the_starting_index_is_zero_then_the_scroll_page_top_command_is_disabled()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.StartingIndex = "0";

			Assert.IsFalse(_helpMainViewModel.ScrollPageTopCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_data_is_finished_loading_and_the_starting_index_is_not_1_or_dash_or_zero_then_the_scroll_page_top_command_is_enabled()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.StartingIndex = "10";

			Assert.IsTrue(_helpMainViewModel.ScrollPageTopCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_page_top_command_is_enabled_then_executing_it_publishes_the_scroll_viewer_event_with_the_correct_payload()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.StartingIndex = "10";

			_helpMainViewModel.ScrollPageTopCommand.Execute(new object());

			_mockEventAggregator.Received().GetEvent<ScrollViewerScrollChanged>();
			_mockScrollViewerScrollChangedEvent.Received().Publish(ScrollViewerScrollChangedEventPayload.ScrollTop);
		}

		[TestMethod]
		public void And_the_data_is_still_loading_then_the_scroll_page_up_command_is_disabled()
		{
			_helpMainViewModel.IsInitialDataLoading = true;
			_helpMainViewModel.StartingIndex = "2";

			Assert.IsFalse(_helpMainViewModel.ScrollPageUpCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_data_is_finished_loading_and_the_starting_index_is_one_then_the_scroll_page_up_command_is_disabled()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.StartingIndex = "1";

			Assert.IsFalse(_helpMainViewModel.ScrollPageUpCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_data_is_finished_loading_and_the_starting_index_is_a_dash_then_the_scroll_page_up_command_is_disabled()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.StartingIndex = "-";

			Assert.IsFalse(_helpMainViewModel.ScrollPageUpCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_data_is_finished_loading_and_the_starting_index_is_zero_then_the_scroll_page_up_command_is_disabled()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.StartingIndex = "0";

			Assert.IsFalse(_helpMainViewModel.ScrollPageUpCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_data_is_finished_loading_and_the_starting_index_is_not_1_or_dash_or_zero_then_the_scroll_page_up_command_is_enabled()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.StartingIndex = "10";

			Assert.IsTrue(_helpMainViewModel.ScrollPageUpCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_page_up_command_is_enabled_then_executing_it_publishes_the_scroll_viewer_event_with_the_correct_payload()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.StartingIndex = "10";

			_helpMainViewModel.ScrollPageUpCommand.Execute(new object());

			_mockEventAggregator.Received().GetEvent<ScrollViewerScrollChanged>();
			_mockScrollViewerScrollChangedEvent.Received().Publish(ScrollViewerScrollChangedEventPayload.ScrollPageUp);
		}

		[TestMethod]
		public void And_the_data_is_still_loading_then_the_scroll_page_down_command_is_disabled()
		{
			_helpMainViewModel.IsInitialDataLoading = true;
			_helpMainViewModel.EndingIndex = "2";
			_helpMainViewModel.TotalItemsInScrollViewer = "10";

			Assert.IsFalse(_helpMainViewModel.ScrollPageDownCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_data_is_finished_loading_and_the_ending_index_is_equal_to_the_total_items_in_the_scroll_viewer_then_the_scroll_page_down_command_is_disabled()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.EndingIndex = "10";
			_helpMainViewModel.TotalItemsInScrollViewer = "10";

			Assert.IsFalse(_helpMainViewModel.ScrollPageDownCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_data_is_finished_loading_and_the_ending_index_is_not_equal_to_the_total_items_in_the_scroll_viewer_then_the_scroll_page_down_command_is_enabled()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.EndingIndex = "2";
			_helpMainViewModel.TotalItemsInScrollViewer = "10";

			Assert.IsTrue(_helpMainViewModel.ScrollPageDownCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_page_down_command_is_enabled_then_executing_it_publishes_the_scroll_viewer_event_with_the_correct_payload()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.EndingIndex = "2";
			_helpMainViewModel.StartingIndex = "10";

			_helpMainViewModel.ScrollPageDownCommand.Execute(new object());

			_mockEventAggregator.Received().GetEvent<ScrollViewerScrollChanged>();
			_mockScrollViewerScrollChangedEvent.Received().Publish(ScrollViewerScrollChangedEventPayload.ScrollPageDown);
		}

		[TestMethod]
		public void And_the_data_is_still_loading_then_the_scroll_page_bottom_command_is_disabled()
		{
			_helpMainViewModel.IsInitialDataLoading = true;
			_helpMainViewModel.EndingIndex = "2";
			_helpMainViewModel.TotalItemsInScrollViewer = "10";

			Assert.IsFalse(_helpMainViewModel.ScrollPageBottomCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_data_is_finished_loading_and_the_ending_index_is_equal_to_the_total_items_in_the_scroll_viewer_then_the_scroll_page_bottom_command_is_disabled()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.EndingIndex = "10";
			_helpMainViewModel.TotalItemsInScrollViewer = "10";

			Assert.IsFalse(_helpMainViewModel.ScrollPageBottomCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_data_is_finished_loading_and_the_ending_index_is_not_equal_to_the_total_items_in_the_scroll_viewer_then_the_scroll_page_bottom_command_is_enabled()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.EndingIndex = "2";
			_helpMainViewModel.TotalItemsInScrollViewer = "10";

			Assert.IsTrue(_helpMainViewModel.ScrollPageBottomCommand.CanExecute(new object()));
		}

		[TestMethod]
		public void And_the_page_bottom_command_is_enabled_then_executing_it_publishes_the_scroll_viewer_event_with_the_correct_payload()
		{
			_helpMainViewModel.IsInitialDataLoading = false;
			_helpMainViewModel.EndingIndex = "2";
			_helpMainViewModel.StartingIndex = "10";

			_helpMainViewModel.ScrollPageBottomCommand.Execute(new object());

			_mockEventAggregator.Received().GetEvent<ScrollViewerScrollChanged>();
			_mockScrollViewerScrollChangedEvent.Received().Publish(ScrollViewerScrollChangedEventPayload.ScrollBottom);
		}
	}
	// ReSharper restore InconsistentNaming
}
