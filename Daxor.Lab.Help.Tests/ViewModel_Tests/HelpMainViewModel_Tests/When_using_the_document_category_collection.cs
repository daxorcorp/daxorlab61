﻿using System.Collections.Generic;
using System.IO;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Models;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpMainViewModel_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_using_the_document_category_collection
	{
		private IEventAggregator _stubEventAggregator;
		private IHelpDocumentManager _stubHelpDocumentManager;

		private HelpMainViewModel _viewModel;

		[TestInitialize]
		public void Initialize()
		{
			_stubHelpDocumentManager = Substitute.For<IHelpDocumentManager>();
			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_stubEventAggregator.GetEvent<HelpViewChanged>().Returns(Substitute.For<HelpViewChanged>());
			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Returns(Substitute.For<ScrollViewerPageInfoChanged>());
			_stubHelpDocumentManager.RetrieveHelpDocumentCategories(Arg.Any<string>()).ReturnsForAnyArgs(new List<DocumentCategory>());

			_viewModel = new HelpMainViewModel(Substitute.For<IAppModuleNavigator>(), _stubHelpDocumentManager, _stubEventAggregator, Substitute.For<IMessageBoxDispatcher>());
		}

		[TestMethod]
		public void Then_the_collection_can_be_set()
		{
			var categories = new List<DocumentCategory>
			{
				new DocumentCategory("Test", new List<FileInfo>())
			};

			_viewModel.Categories = categories;

			CollectionAssert.AreEqual(categories, _viewModel.Categories);
		}

		[TestMethod]
		public void Then_setting_the_collection_raises_PropertyChanged()
		{
			var wasRaised = false;

			_viewModel.PropertyChanged += (sender, args) => { wasRaised = args.PropertyName == "Categories"; };

			_viewModel.Categories = new List<DocumentCategory>();

			Assert.IsTrue(wasRaised);
		}
	}
	// ReSharper restore InconsistentNaming
}
