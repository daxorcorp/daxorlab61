﻿using System;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpMainViewModel_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_constructing_the_Help_main_view_model
	{
		private IAppModuleNavigator _stubAppModuleNavigator;
		private IEventAggregator _stubAggregator;
		private IHelpDocumentManager _mockHelpDocumentManager;

		private HelpViewChanged _mockHelpViewChangedEvent;
		private ScrollViewerPageInfoChanged _mockScrollViewerPageInfoChangedEvent;

		[TestInitialize]
		public void Initialize()
		{
			_stubAppModuleNavigator = Substitute.For<IAppModuleNavigator>();
			_mockHelpDocumentManager = Substitute.For<IHelpDocumentManager>();
			_stubAggregator = Substitute.For<IEventAggregator>();
			_mockHelpViewChangedEvent = Substitute.For<HelpViewChanged>();
			_mockHelpViewChangedEvent.Subscribe(Arg.Any<Action<object>>()).Returns(Substitute.For<SubscriptionToken>());
			_mockScrollViewerPageInfoChangedEvent = Substitute.For<ScrollViewerPageInfoChanged>();
			_mockScrollViewerPageInfoChangedEvent.Subscribe(Arg.Any<Action<object>>()).Returns(Substitute.For<SubscriptionToken>());

			_stubAggregator.GetEvent<HelpViewChanged>().Returns(_mockHelpViewChangedEvent);
			_stubAggregator.GetEvent<ScrollViewerPageInfoChanged>().Returns(_mockScrollViewerPageInfoChangedEvent);
		}

		[TestMethod]
		public void The_view_model_subscribes_to_the_return_to_view_changed_event()
		{
			// ReSharper disable once UnusedVariable
			var vm = new HelpMainViewModel(_stubAppModuleNavigator, _mockHelpDocumentManager, _stubAggregator, Substitute.For<IMessageBoxDispatcher>());

			_mockHelpViewChangedEvent.Received(1).Subscribe(Arg.Any<Action<string>>());
		}

		[TestMethod]
		public void The_view_model_subscribed_to_the_scroll_viewer_page_info_changed_event()
		{
			// ReSharper disable once UnusedVariable
			var vm = new HelpMainViewModel(_stubAppModuleNavigator, _mockHelpDocumentManager, _stubAggregator, Substitute.For<IMessageBoxDispatcher>());

			_mockScrollViewerPageInfoChangedEvent.Received(1).Subscribe(Arg.Any<Action<ScrollViewerPageInfoChangedPayload>>());
        }    

	    [TestMethod]
	    public void Then_the_document_manager_creates_the_manual_signoff_sheet()
	    {
            // ReSharper disable once UnusedVariable
            var vm = new HelpMainViewModel(_stubAppModuleNavigator, _mockHelpDocumentManager, _stubAggregator, Substitute.For<IMessageBoxDispatcher>());

            _mockHelpDocumentManager.Received(1).CreateManualSignoffSheet();
	    }
	}
	// ReSharper restore InconsistentNaming
}
