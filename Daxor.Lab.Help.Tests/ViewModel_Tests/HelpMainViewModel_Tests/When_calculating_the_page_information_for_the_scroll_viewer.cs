﻿using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpMainViewModel_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_calculating_the_page_information_for_the_scroll_viewer
	{
		private IAppModuleNavigator _stubAppModuleNavigator;
		private IEventAggregator _stubEventAggregator;
		private IHelpDocumentManager _stubHelpDocumentManager;

		private HelpViewChanged _stubHelpViewChangedEvent;
		private ScrollViewerPageInfoChanged _stubScrollViewerPageInfoChangedEvent;
		private ScrollViewerScrollChanged _stubScrollViewerScrollChangedEvent;

		private ScrollViewerPageInfoChangedPayload _stubPayload;

		private HelpMainViewModelThatDoesNotEvaluateScrollCommandCanExecute _viewModel;

		[TestInitialize]
		public void Initialize()
		{
			_stubPayload = new ScrollViewerPageInfoChangedPayload(0, 0, 0, 0, 0, AppModuleIds.Help);

			_stubHelpViewChangedEvent = new HelpViewChanged();
			_stubScrollViewerPageInfoChangedEvent = new ScrollViewerPageInfoChanged();
			_stubScrollViewerScrollChangedEvent = new ScrollViewerScrollChanged();

			_stubAppModuleNavigator = Substitute.For<IAppModuleNavigator>();
			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_stubHelpDocumentManager = Substitute.For<IHelpDocumentManager>();

			_stubEventAggregator.GetEvent<HelpViewChanged>().Returns(_stubHelpViewChangedEvent);
			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Returns(_stubScrollViewerPageInfoChangedEvent);
			_stubEventAggregator.GetEvent<ScrollViewerScrollChanged>().Returns(_stubScrollViewerScrollChangedEvent);

			_viewModel = new HelpMainViewModelThatDoesNotEvaluateScrollCommandCanExecute(_stubAppModuleNavigator, _stubHelpDocumentManager, _stubEventAggregator, Substitute.For<IMessageBoxDispatcher>());
		}

		[TestMethod]
		public void And_a_scroll_event_occurs_then_each_of_the_scroll_commands_are_reevaluated()
		{
			Assert.IsFalse(_viewModel.EvaluateCommandCanExecuteWasCalled);

			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.IsTrue(_viewModel.EvaluateCommandCanExecuteWasCalled);
		}

		[TestMethod]
		public void And_the_item_count_is_zero_then_the_starting_index_is_the_string_0()
		{
			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.AreEqual("0", _viewModel.StartingIndex);
		}

		[TestMethod]
		public void And_the_item_count_is_zero_then_the_ending_index_is_the_string_0()
		{
			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.AreEqual("0", _viewModel.EndingIndex);
		}

		[TestMethod]
		public void And_the_item_count_is_zero_then_the_total_items_in_the_scroll_viewer_is_the_string_0()
		{
			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.AreEqual("0", _viewModel.TotalItemsInScrollViewer);
		}

		[TestMethod]
		public void And_the_total_item_count_is_zero_then_the_total_items_in_the_scroll_viewer_source_collection_is_the_string_0()
		{
			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.AreEqual("0", _viewModel.TotalItemsInScrollviewerSourceCollection);
		}

		// SMullinax 4/1/2015 (Copied from Daxor.Lab.BVA.Tests.ViewModels.TestSelectionViewModel_Tests.When_calculating_the_page_information_for_the_scroll_viewer
		//									.And_the_item_count_is_one_and_the_extent_height_is_greater_than_one_and_the_vertical_offset_is_greater_than_one_then_the_starting_index_is_the_string_1())
		// This is brittle ... 
		// Hypothesis: in real execution vertical offset is never greater than or equal to item height...
		[TestMethod]
		public void And_the_item_count_is_one_and_the_extent_height_is_greater_than_one_and_the_vertical_offset_is_greater_than_one_then_the_starting_index_is_the_string_1()
		{
			_stubPayload.ItemCount = 1;
			_stubPayload.ScrollViewerExtentHeight = 10;
			_stubPayload.ScrollViewerVerticalOffset = 2;

			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.AreEqual("1", _viewModel.StartingIndex);
		}

		[TestMethod]
		public void And_the_item_count_is_one_and_the_extent_height_is_greater_than_one_and_the_viewport_height_is_greater_than_one_and_the_vertical_offset_is_greater_than_one_then_the_ending_index_is_the_string_1()
		{
			_stubPayload.ItemCount = 1;
			_stubPayload.ScrollViewerViewportHeight = 10;
			_stubPayload.ScrollViewerExtentHeight = 10;
			_stubPayload.ScrollViewerVerticalOffset = 2;

			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.AreEqual("1", _viewModel.EndingIndex);
		}

		[TestMethod]
		public void And_the_item_count_is_1_then_the_total_items_in_the_scroll_viewer_is_the_string_1()
		{
			_stubPayload.ItemCount = 1;

			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.AreEqual("1", _viewModel.TotalItemsInScrollViewer);
		}

		[TestMethod]
		public void And_the_total_item_count_is_1_then_the_total_items_in_the_scroll_viewer_source_collection_is_the_string_1()
		{
			_stubPayload.TotalItemCount = 1;

			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.AreEqual("1", _viewModel.TotalItemsInScrollviewerSourceCollection);
		}

		[TestMethod]
		public void And_the_item_count_is_two_and_the_extent_height_is_greater_than_one_and_the_vertical_offset_is_greater_than_one_then_the_starting_index_is_the_string_1()
		{
			_stubPayload.ItemCount = 2;
			_stubPayload.ScrollViewerExtentHeight = 10;
			_stubPayload.ScrollViewerVerticalOffset = 2;

			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.AreEqual("1", _viewModel.StartingIndex);
		}

		[TestMethod]
		public void And_the_item_count_is_two_and_the_extent_height_is_greater_than_one_and_the_viewport_height_is_greater_than_one_and_the_vertical_offset_is_greater_than_one_then_the_ending_index_is_the_string_2()
		{
			_stubPayload.ItemCount = 2;
			_stubPayload.ScrollViewerViewportHeight = 10;
			_stubPayload.ScrollViewerExtentHeight = 10;
			_stubPayload.ScrollViewerVerticalOffset = 2;

			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.AreEqual("2", _viewModel.EndingIndex);
		}

		[TestMethod]
		public void And_the_item_count_is_2_then_the_total_items_in_the_scroll_viewer_is_the_string_2()
		{
			_stubPayload.ItemCount = 2;

			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.AreEqual("2", _viewModel.TotalItemsInScrollViewer);
		}

		[TestMethod]
		public void And_the_total_item_count_is_2_then_the_total_items_in_the_scroll_viewer_source_collection_is_the_string_2()
		{
			_stubPayload.TotalItemCount = 2;

			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.AreEqual("2", _viewModel.TotalItemsInScrollviewerSourceCollection);
		}

		// PAndreason 3/31/2015
		// This creates the situation where the number of items in the collection extends past the end of the screen
		[TestMethod]
		public void And_the_item_count_is_12_and_the_extent_height_is_greater_than_one_and_the_viewport_height_is_greater_than_one_and_the_vertical_offset_is_greater_than_one_then_the_ending_index_is_the_string_8()
		{
			_stubPayload.ItemCount = 12;
			_stubPayload.ScrollViewerViewportHeight = 1;
			_stubPayload.ScrollViewerExtentHeight = 3;
			_stubPayload.ScrollViewerVerticalOffset = 1;

			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Publish(_stubPayload);

			Assert.AreEqual("8", _viewModel.EndingIndex);
		}
	}
	// ReSharper restore InconsistentNaming
}
