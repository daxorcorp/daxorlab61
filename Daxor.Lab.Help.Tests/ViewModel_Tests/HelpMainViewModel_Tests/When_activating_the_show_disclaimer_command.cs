﻿using System;
using Daxor.Lab.Help.Common;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpMainViewModel_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_activating_the_show_disclaimer_command
	{
		private IAppModuleNavigator _stubNavigator;
		private IHelpDocumentManager _stubHelpDocumentManager;
		private IEventAggregator _stubEventAggregator;
		private IMessageBoxDispatcher _mockMessageBoxDispatcher;

		private HelpViewChanged _stubHelpViewChangedEvent;
		private ScrollViewerScrollChanged _stubScrollViewerScrollChangedEvent;
		private ScrollViewerPageInfoChanged _stubScrollViewerPageInfoChangedEvent;

		private HelpMainViewModel _helpMainViewModel;

		[TestInitialize]
		public void Initialize()
		{
			_stubNavigator = Substitute.For<IAppModuleNavigator>();
			_stubHelpDocumentManager = Substitute.For<IHelpDocumentManager>();
			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_mockMessageBoxDispatcher = Substitute.For<IMessageBoxDispatcher>();

			_stubHelpViewChangedEvent = Substitute.For<HelpViewChanged>();
			_stubHelpViewChangedEvent.Subscribe(Arg.Any<Action<object>>()).Returns(Substitute.For<SubscriptionToken>());

			_stubScrollViewerPageInfoChangedEvent = Substitute.For<ScrollViewerPageInfoChanged>();
			_stubScrollViewerPageInfoChangedEvent.Subscribe(Arg.Any<Action<object>>()).Returns(Substitute.For<SubscriptionToken>());

			_stubScrollViewerScrollChangedEvent = Substitute.For<ScrollViewerScrollChanged>();
			_stubScrollViewerScrollChangedEvent.Subscribe(Arg.Any<Action<ScrollViewerScrollChangedEventPayload>>()).Returns(Substitute.For<SubscriptionToken>());

			_stubEventAggregator.GetEvent<HelpViewChanged>().Returns(_stubHelpViewChangedEvent);
			_stubEventAggregator.GetEvent<ScrollViewerScrollChanged>().Returns(_stubScrollViewerScrollChangedEvent);
			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Returns(_stubScrollViewerPageInfoChangedEvent);

			_helpMainViewModel = new HelpMainViewModel(_stubNavigator, _stubHelpDocumentManager, _stubEventAggregator, _mockMessageBoxDispatcher);
		}

		[TestMethod]
		public void Then_a_message_box_is_shown_that_contains_the_disclaimer_text()
		{
			_helpMainViewModel.ShowDisclaimerCommand.Execute(null);

			_mockMessageBoxDispatcher.Received(1).ShowMessageBox(MessageBoxCategory.Information, HelpConstants.DisclaimerText, HelpConstants.DisclaimerName, MessageBoxChoiceSet.Ok);
		}
	}
	// ReSharper restore InconsistentNaming
}
