﻿using System.IO;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpMainViewModel_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_using_the_selected_file
	{
		private IHelpDocumentManager _stubDocumentManager;
		private IEventAggregator _stubEventAggregator;

		private HelpMainViewModel _viewModel;

		[TestInitialize]
		public void Initialize()
		{
			_stubDocumentManager = Substitute.For<IHelpDocumentManager>();
			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_stubEventAggregator.GetEvent<HelpViewChanged>().Returns(Substitute.For<HelpViewChanged>());
			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Returns(Substitute.For<ScrollViewerPageInfoChanged>());
			_viewModel = new HelpMainViewModel(Substitute.For<IAppModuleNavigator>(), _stubDocumentManager, _stubEventAggregator, Substitute.For<IMessageBoxDispatcher>());
		}

		[TestMethod]
		public void Then_the_file_can_be_set()
		{
			var file = new FileInfo("Test");

			_viewModel.SelectedFile = file;

			Assert.AreEqual(file, _viewModel.SelectedFile);
		}

		[TestMethod]
		public void Then_setting_the_file_raises_PropertyChanged()
		{
			var wasRaised = false;

			_viewModel.PropertyChanged += (sender, args) => { wasRaised = args.PropertyName == "SelectedFile"; };

			_viewModel.SelectedFile = new FileInfo("Test");

			Assert.IsTrue(wasRaised);
		}
	}
	// ReSharper restore InconsistentNaming
}
