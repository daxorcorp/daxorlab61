﻿using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
// ReSharper disable ObjectCreationAsStatement

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpMainViewModel_Tests.SadPath
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_constructing_the_Help_main_view_model
	{
		[TestMethod]
		public void And_the_app_module_navigator_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new HelpMainViewModel(null, Substitute.For<IHelpDocumentManager>(), Substitute.For<IEventAggregator>(), Substitute.For<IMessageBoxDispatcher>()), "appModuleNavigator");
		}

		[TestMethod]
		public void And_the_help_document_manager_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new HelpMainViewModel(Substitute.For<IAppModuleNavigator>(), null, Substitute.For<IEventAggregator>(), Substitute.For<IMessageBoxDispatcher>()), "helpDocumentManager");
		}

		[TestMethod]
		public void And_the_event_aggregator_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new HelpMainViewModel(Substitute.For<IAppModuleNavigator>(), Substitute.For<IHelpDocumentManager>(), null, Substitute.For<IMessageBoxDispatcher>()), "eventAggregator");
		}

		[TestMethod]
		public void And_the_message_box_dispatcher_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new HelpMainViewModel(Substitute.For<IAppModuleNavigator>(), Substitute.For<IHelpDocumentManager>(), Substitute.For<IEventAggregator>(), null), "messageBoxDispatcher");
		}
	}
	// ReSharper restore InconsistentNaming
}
