﻿using System;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpMainViewModel_Tests.SadPath
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_using_the_page_navigation_commands
	{
		private IAppModuleNavigator _stubNavigator;
		private IHelpDocumentManager _stubHelpDocumentManager;
		private IEventAggregator _stubEventAggregator;

		private HelpViewChanged _stubHelpViewChangedEvent;

		private HelpMainViewModel _vm;

		[TestInitialize]
		public void Initialize()
		{
			_stubNavigator = Substitute.For<IAppModuleNavigator>();
			_stubHelpDocumentManager = Substitute.For<IHelpDocumentManager>();
			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_stubHelpViewChangedEvent = Substitute.For<HelpViewChanged>();
			_stubHelpViewChangedEvent.Subscribe(Arg.Any<Action<object>>()).Returns(Substitute.For<SubscriptionToken>());

			_stubEventAggregator.GetEvent<HelpViewChanged>().Returns(_stubHelpViewChangedEvent);

			_vm = new HelpMainViewModel(_stubNavigator, _stubHelpDocumentManager, _stubEventAggregator);
		}

		[TestMethod]
		public void And_the_scroll_viewer_is_null_and_trying_to_go_to_the_top_then_an_argument_null_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => _vm.GoToTopCommand.Execute(null), "ScrollViewer");
		}

		[TestMethod]
		public void And_the_scroll_viewer_is_null_and_trying_to_page_up_then_an_argument_null_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => _vm.PageUpCommand.Execute(null), "ScrollViewer");
		}

		[TestMethod]
		public void And_the_scroll_viewer_is_null_and_trying_to_page_down_then_an_argument_null_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => _vm.PageDownCommand.Execute(null), "ScrollViewer");
		}

		[TestMethod]
		public void And_the_scroll_viewer_is_null_and_trying_to_go_to_the_bottom_then_an_argument_null_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => _vm.GoToBottomCommand.Execute(null), "ScrollViewer");
		}
	}
	// ReSharper restore InconsistentNaming
}
