﻿using System;
using System.IO;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpMainViewModel_Tests.SadPath
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_using_the_Help_main_view_model_commands
	{
		private IAppModuleNavigator _mockAppModuleNavigator;
		private IEventAggregator _stubEventAggregator;
		private IHelpDocumentManager _stubDocumentManager;

		private HelpMainViewModel _viewModel;

		[TestMethod]
		public void And_no_file_is_selected_then_the_select_help_document_command_does_nothing()
		{
			//Arrange
			_mockAppModuleNavigator = Substitute.For<IAppModuleNavigator>();
			_stubDocumentManager = Substitute.For<IHelpDocumentManager>();

			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_stubEventAggregator.GetEvent<HelpViewChanged>().Returns(Substitute.For<HelpViewChanged>());
			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Returns(Substitute.For<ScrollViewerPageInfoChanged>());

			_viewModel = new HelpMainViewModel(_mockAppModuleNavigator, _stubDocumentManager, _stubEventAggregator, Substitute.For<IMessageBoxDispatcher>()) { SelectedFile = null };

			//Act
			_viewModel.SelectHelpDocumentCommand.Execute(null);

			//Assert
			_mockAppModuleNavigator.Received(0).ActivateView(HelpModuleViewKeys.DocumentView, Arg.Any<FileInfo>());
		}
	}
	// ReSharper restore InconsistentNaming
}
