﻿using System.Collections.Generic;
using System.IO;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Models;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpMainViewModel_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_using_the_selected_category
	{
		private IEventAggregator _stubEventAggregator;
		private IHelpDocumentManager _stubDocumentManager;

		private HelpMainViewModel _viewModel;

		[TestInitialize]
		public void Initialize()
		{
			_stubDocumentManager = Substitute.For<IHelpDocumentManager>();
			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_stubEventAggregator.GetEvent<HelpViewChanged>().Returns(Substitute.For<HelpViewChanged>());
			_stubEventAggregator.GetEvent<ScrollViewerPageInfoChanged>().Returns(Substitute.For<ScrollViewerPageInfoChanged>());
			_viewModel = new HelpMainViewModel(Substitute.For<IAppModuleNavigator>(), _stubDocumentManager, _stubEventAggregator, Substitute.For<IMessageBoxDispatcher>());
		}

		[TestMethod]
		public void Then_the_category_can_be_set()
		{
			var category = new DocumentCategory("Test", new List<FileInfo>());

			_viewModel.SelectedCategory = category;

			Assert.AreEqual(category, _viewModel.SelectedCategory);
		}

		[TestMethod]
		public void Then_setting_the_category_raises_PropertyChanged()
		{
			var wasRaised = false;
			
			_viewModel.PropertyChanged += (sender, args) => { wasRaised = args.PropertyName == "SelectedCategory"; };

			_viewModel.SelectedCategory = new DocumentCategory("Test", new List<FileInfo>());

			Assert.IsTrue(wasRaised);
		}
	}
	// ReSharper restore InconsistentNaming
}
