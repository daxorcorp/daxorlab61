﻿using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpDocumentViewModel_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_executing_the_go_to_previous_screen_command 
	{
		private IAppModuleNavigator _mockAppModuleNavigator;
		private IEventAggregator _stubEventAggregator;

		private HelpDocumentViewModel _viewModel;

		[TestMethod]
		public void The_go_to_previous_screen_command_activates_the_main_view()
		{
			_mockAppModuleNavigator = Substitute.For<IAppModuleNavigator>();
			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_stubEventAggregator.GetEvent<HelpDocumentSelected>().Returns(new HelpDocumentSelected());
			_stubEventAggregator.GetEvent<HelpViewChanged>().Returns(new HelpViewChanged());

			//Arrange
			_viewModel = new HelpDocumentViewModel(_mockAppModuleNavigator, _stubEventAggregator);

			//Act
			_viewModel.GoToPreviousScreenCommand.Execute(null);

			//Assert
			_mockAppModuleNavigator.Received(1).ActivateView(HelpModuleViewKeys.MainView);
		}
	}
	// ReSharper restore InconsistentNaming
}
