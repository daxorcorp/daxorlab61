﻿using System;
using System.IO;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpDocumentViewModel_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_constructing_the_Help_document_view_model
	{
		[TestMethod]
		public void The_view_model_subscribes_to_the_help_document_selected_event()
		{
			var stubAppModuleNavigator = Substitute.For<IAppModuleNavigator>();
			var mockEventAggregator = Substitute.For<IEventAggregator>();
			var mockEvent = Substitute.For<HelpDocumentSelected>();

			mockEventAggregator.GetEvent<HelpDocumentSelected>().Returns(mockEvent);
			mockEventAggregator.GetEvent<HelpViewChanged>().Returns(new HelpViewChanged());

			// ReSharper disable once UnusedVariable
			var viewModel = new HelpDocumentViewModel(stubAppModuleNavigator, mockEventAggregator);

			mockEvent.Received(1).Subscribe(Arg.Any<Action<FileInfo>>());
		}

		[TestMethod]
		public void The_view_model_subscribes_to_the_help_view_changed_event()
		{
			var stubAppModuleNavigator = Substitute.For<IAppModuleNavigator>();
			var mockEventAggregator = Substitute.For<IEventAggregator>();
			var mockEvent = Substitute.For<HelpViewChanged>();

			mockEventAggregator.GetEvent<HelpDocumentSelected>().Returns(new HelpDocumentSelected());
			mockEventAggregator.GetEvent<HelpViewChanged>().Returns(mockEvent);

			// ReSharper disable once UnusedVariable
			var viewModel = new HelpDocumentViewModel(stubAppModuleNavigator, mockEventAggregator);

			mockEvent.Received(1).Subscribe(Arg.Any<Action<string>>());
		}
	}
	// ReSharper restore InconsistentNaming
}
