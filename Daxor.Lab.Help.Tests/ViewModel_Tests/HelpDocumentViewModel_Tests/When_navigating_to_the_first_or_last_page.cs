﻿using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using Telerik.Windows.Controls;
using Telerik.Windows.Documents.Fixed.Model;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpDocumentViewModel_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_navigating_to_the_first_or_last_page
	{
		private IAppModuleNavigator _stubAppModuleNavigator;
		private IEventAggregator _stubEventAggregator;

		private FixedDocumentViewerBase _mockPdfViewer;
		private RadFixedDocument _stubDocument;

		private HelpDocumentViewModel _viewModel;

		[TestInitialize]
		public void Initialize()
		{
			_stubAppModuleNavigator = Substitute.For<IAppModuleNavigator>();

			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_stubEventAggregator.GetEvent<HelpDocumentSelected>().Returns(new HelpDocumentSelected());
			_stubEventAggregator.GetEvent<HelpViewChanged>().Returns(new HelpViewChanged());

			_stubDocument = Substitute.For<RadFixedDocument>();
			_stubDocument.Pages.AddPage();
			_stubDocument.Pages.AddPage();
			_stubDocument.Pages.AddPage();
			_stubDocument.Pages.AddPage();
			_stubDocument.Pages.AddPage();

			_mockPdfViewer = Substitute.For<FixedDocumentViewerBase>();
			_mockPdfViewer.Document = _stubDocument;

			_viewModel = new HelpDocumentViewModel(_stubAppModuleNavigator, _stubEventAggregator);
		}

		[TestMethod]
		public void And_navigating_to_the_first_page_then_the_document_viewer_is_updated_to_the_first_page()
		{
			_viewModel.GoToFirstPage(_mockPdfViewer);

			_mockPdfViewer.Received(1).GoToPage(1);
		}

		[TestMethod]
		public void And_navigating_to_the_last_page_then_the_document_viewer_is_updated_to_the_last_page()
		{
			_viewModel.GoToLastPage(_mockPdfViewer);

			_mockPdfViewer.Received(1).GoToPage(5);
		}
	}
	// ReSharper restore InconsistentNaming
}
