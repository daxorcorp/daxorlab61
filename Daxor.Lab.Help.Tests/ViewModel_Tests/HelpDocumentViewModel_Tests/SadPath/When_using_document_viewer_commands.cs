using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpDocumentViewModel_Tests.SadPath
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_using_document_viewer_commands
	{
		private IAppModuleNavigator _stubAppModuleNavigator;
		private IEventAggregator _stubEventAggregator;
		private HelpDocumentViewModel _viewModel;

		[TestInitialize]
		public void Initialize()
		{
			_stubAppModuleNavigator = Substitute.For<IAppModuleNavigator>();

			_stubEventAggregator = Substitute.For<IEventAggregator>();
			_stubEventAggregator.GetEvent<HelpDocumentSelected>().Returns(new HelpDocumentSelected());
			_stubEventAggregator.GetEvent<HelpViewChanged>().Returns(new HelpViewChanged());

			_viewModel = new HelpDocumentViewModel(_stubAppModuleNavigator, _stubEventAggregator);
		}

		[TestMethod]
		public void And_going_to_the_first_page_and_the_document_viewer_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => _viewModel.GoToFirstPage(null), "pdfViewer");
		}

		[TestMethod]
		public void And_going_to_the_last_page_and_the_document_viewer_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => _viewModel.GoToLastPage(null), "pdfViewer");
		}

		[TestMethod]
		public void And_printing_all_pages_and_the_document_viewer_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => _viewModel.PrintAll(null), "pdfViewer");
		}

		[TestMethod]
		public void And_printing_the_current_page_and_the_document_viewer_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => _viewModel.PrintCurrentPage(null), "pdfViewer");
		}
	}
	// ReSharper restore InconsistentNaming
}
