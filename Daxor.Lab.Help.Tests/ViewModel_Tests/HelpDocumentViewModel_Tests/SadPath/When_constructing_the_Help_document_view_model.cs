﻿using Daxor.Lab.Help.ViewModels;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.ViewModel_Tests.HelpDocumentViewModel_Tests.SadPath
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_constructing_the_Help_document_view_model
	{
		[TestMethod]
		public void And_the_app_module_navigator_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new HelpDocumentViewModel(null, Substitute.For<IEventAggregator>()), "appModuleNavigator");
		}

		[TestMethod]
		public void And_the_event_aggregator_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new HelpDocumentViewModel(Substitute.For<IAppModuleNavigator>(), null), "eventAggregator");
		}
	}
	// ReSharper restore InconsistentNaming
}
