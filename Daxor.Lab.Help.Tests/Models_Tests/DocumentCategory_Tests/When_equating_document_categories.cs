﻿using System.Collections.Generic;
using System.IO;
using Daxor.Lab.Help.Common;
using Daxor.Lab.Help.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Help.Tests.Models_Tests.DocumentCategory_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_equating_document_categories
	{
		private DocumentCategory a, b;

		[TestMethod]
		public void And_they_are_the_same_then_true_is_returned()
		{
			a = new DocumentCategory("Test", new List<FileInfo>());
			b = new DocumentCategory("Test", new List<FileInfo>());

			Assert.AreEqual(a, b);
		}

		[TestMethod]
		public void And_they_are_different_then_false_is_returned()
		{
			a = new DocumentCategory("Test", new List<FileInfo>());
			b = new DocumentCategory("Test2", new List<FileInfo>());

			Assert.AreNotEqual(a, b);

			b = new DocumentCategory("Test", new List<FileInfo>{new FileInfo(HelpConstants.DocumentPath)});

			Assert.AreNotEqual(a, b);
		}
	}
	// ReSharper restore InconsistentNaming
}
