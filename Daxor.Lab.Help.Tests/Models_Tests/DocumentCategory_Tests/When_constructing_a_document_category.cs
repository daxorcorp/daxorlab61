﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Daxor.Lab.Help.Common;
using Daxor.Lab.Help.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Help.Tests.Models_Tests.DocumentCategory_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_constructing_a_document_category
	{
		private const string name = "Test";

		private DocumentCategory category;

		[TestMethod]
		public void Then_the_category_name_is_set()
		{
			category = new DocumentCategory(name, new List<FileInfo>());

			Assert.AreEqual(name, category.Name);
		}

		[TestMethod]
		public void Then_the_collection_of_files_is_set()
		{
			var files = new List<FileInfo> {new FileInfo(HelpConstants.DocumentPath)};

			category = new DocumentCategory(name, files);

			CollectionAssert.AreEqual(files, category.Files.ToList());
		}
	}
	// ReSharper restore InconsistentNaming
}
