﻿using Daxor.Lab.Help.Common.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Help.Tests.Common_Tests.Converter_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_converting_filenames_with_extensions
	{
		private const string _filenameWithExtension = "Filename.txt";
		private const string _filenameWithoutExtension = "Filename";

		private FileNameToFileNameWithoutExtensionConverter _converter;

		[TestInitialize]
		public void Initialize()
		{
			_converter = new FileNameToFileNameWithoutExtensionConverter();
		}

		[TestMethod]
		public void And_given_a_filename_with_an_extension_then_that_extension_is_removed()
		{
			Assert.AreEqual(_filenameWithoutExtension, _converter.Convert(_filenameWithExtension, null, null, null));
		}

		[TestMethod]
		public void And_given_a_filename_without_an_extension_then_the_same_filename_is_returned()
		{
			Assert.AreEqual(_filenameWithoutExtension, _converter.Convert(_filenameWithoutExtension, null, null, null));
		}
	}
	// ReSharper restore InconsistentNaming
}
