﻿using System.Collections.Generic;
using System.IO;
using Daxor.Lab.Help.Common;
using Daxor.Lab.Help.Common.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Help.Tests.Common_Tests.Converter_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_converting_help_documents_to_watermark_text
	{
		private IEnumerable<FileInfo> _listOfDocuments;

		private HelpDocumentsToWatermarkTextConverter _converter;

		[TestInitialize]
		public void Initialize()
		{
			_converter = new HelpDocumentsToWatermarkTextConverter();
		}

		[TestMethod]
		public void And_given_a_non_empty_document_collection_then_the_watermark_is_empty()
		{
			_listOfDocuments = new List<FileInfo>
			{
				new FileInfo("Meh"),
			};

			Assert.AreEqual(HelpConstants.EmptyWatermarkText, _converter.Convert(_listOfDocuments, null, null, null));
		}

		[TestMethod]
		public void And_given_an_empty_document_collection_then_the_watermark_contains_text()
		{
			_listOfDocuments = new List<FileInfo>();

			Assert.AreEqual(HelpConstants.NoDocumentsFoundText, _converter.Convert(_listOfDocuments, null, null, null));
		}
	}
	// ReSharper restore InconsistentNaming
}
