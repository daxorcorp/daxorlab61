﻿using System;
using Daxor.Lab.Help.Common.Converters;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Help.Tests.Common_Tests.Converter_Tests.SadPath
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_converting_watermark_text_to_help_documents
	{
		private HelpDocumentsToWatermarkTextConverter _converter;

		[TestInitialize]
		public void Initialize()
		{
			_converter = new HelpDocumentsToWatermarkTextConverter();
		}

		[TestMethod]
		public void And_converting_from_a_watermark_to_a_document_collection_then_an_exception_is_thrown()
		{
			AssertEx.Throws<NotImplementedException>(() => _converter.ConvertBack(null, null, null, null));
		}
	}
	// ReSharper restore InconsistentNaming
}
