﻿using System;
using Daxor.Lab.Help.Common.Converters;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Help.Tests.Common_Tests.Converter_Tests.SadPath
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_converting_filenames_without_extensions
	{
		private FileNameToFileNameWithoutExtensionConverter _converter;
		
		[TestMethod]
		public void And_converting_from_a_filename_with_no_extension_to_one_with_an_extension_then_an_exception_is_thrown()
		{
			_converter = new FileNameToFileNameWithoutExtensionConverter();

			const string value = "Filename";

			AssertEx.Throws<NotImplementedException>(() => _converter.ConvertBack(value, null, null, null));
		}
	}
	// ReSharper restore InconsistentNaming
}
