﻿using System;
using Daxor.Lab.Help.Common.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Help.Tests.Common_Tests.Converter_Tests.SadPath
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_converting_filenames_with_extensions
	{
		private FileNameToFileNameWithoutExtensionConverter _converter;
		
		[TestMethod]
		public void And_not_given_a_filename_as_a_string_then_the_empty_string_is_returned()
		{
			_converter = new FileNameToFileNameWithoutExtensionConverter();

			var results = _converter.Convert(2, null, null, null);

			Assert.AreEqual(String.Empty, results);
		}
	}
	// ReSharper restore InconsistentNaming
}
