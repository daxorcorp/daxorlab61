﻿using Daxor.Lab.Help.Common;
using Daxor.Lab.Help.Common.Converters;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Help.Tests.Common_Tests.Converter_Tests.SadPath
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_converting_help_documents_to_watermark_text
	{
		private HelpDocumentsToWatermarkTextConverter _converter;

		[TestInitialize]
		public void Initialize()
		{
			_converter = new HelpDocumentsToWatermarkTextConverter();
		}

		[TestMethod]
		public void And_given_an_incorrect_type_then_the_watermark_is_empty()
		{
			Assert.AreEqual(HelpConstants.EmptyWatermarkText, _converter.Convert("string", null, null, null));
		}

		[TestMethod]
		public void And_the_document_collection_is_null_then_the_watermark_is_empty()
		{
			Assert.AreEqual(HelpConstants.EmptyWatermarkText, _converter.Convert(null, null, null, null));
		}
	}
	// ReSharper restore InconsistentNaming
}
