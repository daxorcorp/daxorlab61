﻿using Daxor.Lab.Help.Common;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Help.Tests.Common_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_using_help_constants
	{
		[TestMethod]
		[RequirementValue("Disclaimer")]
		public void Then_the_disclaimer_name_matches_requirements()
		{
			Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), HelpConstants.DisclaimerName);
		}

		[TestMethod]
		[RequirementValue(@"C:\ProgramData\Daxor\Lab\Help")]
		public void Then_the_document_path_matches_requirements()
		{
			Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), HelpConstants.DocumentPath);
		}

		[TestMethod]
		[RequirementValue("")]
		public void Then_the_empty_watermark_matches_requirements()
		{
			Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), HelpConstants.EmptyWatermarkText);
		}

		[TestMethod]
		[RequirementValue("No Help Documents Found")]
		public void Then_the_no_documents_watermark_matches_requirements()
		{
			Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), HelpConstants.NoDocumentsFoundText);
		}

		[TestMethod]
		[RequirementValue(".pdf")]
		public void Then_document_file_extension_matches_requirements()
		{
			Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), HelpConstants.PdfFileExtension);
		}

		[TestMethod]
		[RequirementValue("BVA-100 Operations Manual Sign-off Form.pdf")]
		public void Then_the_BVA_laboratory_operations_manual_signoff_sheet_name_matches_requirements()
		{
			Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), HelpConstants.BvaManualSignoffFormName);
		}

		[TestMethod]
		[RequirementValue(@"C:\ProgramData\Daxor\Lab\Help\Process Forms\BVA-100 Operations Manual Sign-off Form.pdf")]
		public void Then_the_BVA_laboratory_operations_manual_signoff_sheet_location_matches_requirements()
		{
			Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), HelpConstants.BvaManualSignoffFormFileName);
		}

		[TestMethod]
		[RequirementValue(
			@"Help Documents are current as of the most recent service date. To check for additional updates, contact DAXOR Customer Service at 1-866-548-7282."
			)]
		public void Then_the_disclaimer_text_matches_requirements()
		{
			Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), HelpConstants.DisclaimerText);
		}
	}
	// ReSharper restore InconsistentNaming
}
