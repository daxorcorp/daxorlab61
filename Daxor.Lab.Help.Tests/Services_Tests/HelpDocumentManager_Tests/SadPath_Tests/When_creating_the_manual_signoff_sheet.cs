using System;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Services;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Services_Tests.HelpDocumentManager_Tests.SadPath_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_the_manual_signoff_sheet
    {
        [TestMethod]
        public void And_the_document_generator_throws_an_exception_then_the_exception_is_logged()
        {
            var stubFileManager = Substitute.For<IFileManager>();

            var stubDocumentGenerator = Substitute.For<IDocumentGenerator>();
            stubDocumentGenerator.When(x => x.Generate()).Do(x => {throw new Exception();});
            
            var mockLogger = Substitute.For<ILoggerFacade>();
            
            var helpDocumentManager = new HelpDocumentManager(stubFileManager, stubDocumentGenerator, mockLogger, Substitute.For<ISettingsManager>());

            helpDocumentManager.CreateManualSignoffSheet();

            mockLogger.Received(1).Log(Arg.Any<string>(), Category.Exception, Priority.High);
        }
    }
    // ReSharper restore InconsistentNaming
}
