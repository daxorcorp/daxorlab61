﻿using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Services;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
// ReSharper disable ObjectCreationAsStatement

namespace Daxor.Lab.Help.Tests.Services_Tests.HelpDocumentManager_Tests.SadPath_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_constructing_a_help_document_manager
	{
		[TestMethod]
		public void And_the_file_manager_is_null_then_an_exception_is_thrown()
		{
            AssertEx.ThrowsArgumentNullException(() => new HelpDocumentManager(null, Substitute.For<IDocumentGenerator>(), Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>()), "fileManager");
		}

		[TestMethod]
		public void And_the_document_generator_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new HelpDocumentManager(Substitute.For<IFileManager>(), null, Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>()), "bvaSignoffDocumentGenerator");
		}

		[TestMethod]
		public void And_the_logger_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new HelpDocumentManager(Substitute.For<IFileManager>(), Substitute.For<IDocumentGenerator>(), null, Substitute.For<ISettingsManager>()), "logger");
		}

		[TestMethod]
		public void And_the_settings_manager_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new HelpDocumentManager(Substitute.For<IFileManager>(), Substitute.For<IDocumentGenerator>(), null, Substitute.For<ISettingsManager>()), "logger");
		}
	}
	// ReSharper restore InconsistentNaming
}
