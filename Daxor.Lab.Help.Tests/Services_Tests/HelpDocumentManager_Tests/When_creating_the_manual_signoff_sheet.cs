﻿using Daxor.Lab.Help.Common;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Services;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Services_Tests.HelpDocumentManager_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_creating_the_manual_signoff_sheet
	{
		private IFileManager _mockFileManager;
		private IDocumentGenerator _mockDocumentGenerator;

		private HelpDocumentManager _helpDocumentManager;

		[TestInitialize]
		public void Initialize()
		{
			_mockFileManager = Substitute.For<IFileManager>();
			_mockFileManager.FileExists(HelpConstants.BvaManualSignoffFormFileName).Returns(false);

			_mockDocumentGenerator = Substitute.For<IDocumentGenerator>();

			_helpDocumentManager = new HelpDocumentManager(_mockFileManager, _mockDocumentGenerator, Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>());
		}

		[TestMethod]
		public void Then_the_document_generator_is_used_to_generate_the_document()
		{
			_helpDocumentManager.CreateManualSignoffSheet();

			_mockDocumentGenerator.Received(1).Generate();
		}
	}
	// ReSharper restore InconsistentNaming
}
