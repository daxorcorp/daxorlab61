﻿using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Services;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Services_Tests.HelpDocumentManager_Tests
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_changing_hospital_information_settings
	{
		// ReSharper disable once NotAccessedField.Local
		private HelpDocumentManager _helpDocumentManager;
		
		private IDocumentGenerator _documentGenerator;
		private IFileManager _fileManager;
		private ILoggerFacade _loggerFacade;
		private ISettingsManager _settingsManager;

		[TestInitialize]
		public void Before_every_test()
		{
			_documentGenerator = Substitute.For<IDocumentGenerator>();
			_fileManager = Substitute.For<IFileManager>();
			_loggerFacade = Substitute.For<ILoggerFacade>();
			_settingsManager = Substitute.For<ISettingsManager>();

			_helpDocumentManager = new HelpDocumentManager(_fileManager, _documentGenerator, _loggerFacade, _settingsManager);
		}

		[TestMethod]
		public void And_the_department_address_changes_then_the_signoff_document_is_regenerated()
		{
			//Act
			_settingsManager.SettingsChanged += Raise.EventWith(new SettingsChangedEventArgs(new[] {SettingKeys.SystemDepartmentName}));

			//Assert
			_documentGenerator.Received(1).Generate();
		}

		[TestMethod]
		public void And_the_department_director_changes_then_the_signoff_document_is_regenerated()
		{
			//Act
			_settingsManager.SettingsChanged += Raise.EventWith(new SettingsChangedEventArgs(new[] { SettingKeys.SystemDepartmentDirector }));

			//Assert
			_documentGenerator.Received(1).Generate();
		}

		[TestMethod]
		public void And_the_department_name_changes_then_the_signoff_document_is_regenerated()
		{
			//Act
			_settingsManager.SettingsChanged += Raise.EventWith(new SettingsChangedEventArgs(new[] { SettingKeys.SystemDepartmentName }));

			//Assert
			_documentGenerator.Received(1).Generate();
		}

		[TestMethod]
		public void And_the_department_phone_number_changes_then_the_signoff_document_is_regenerated()
		{
			//Act
			_settingsManager.SettingsChanged += Raise.EventWith(new SettingsChangedEventArgs(new[] { SettingKeys.SystemDepartmentPhoneNumber }));

			//Assert
			_documentGenerator.Received(1).Generate();
		}

		[TestMethod]
		public void And_the_hospital_name_changes_then_the_signoff_document_is_regenerated()
		{
			//Act
			_settingsManager.SettingsChanged += Raise.EventWith(new SettingsChangedEventArgs(new[] { SettingKeys.SystemHospitalName }));

			//Assert
			_documentGenerator.Received(1).Generate();
		}
	}
	// ReSharper restore InconsistentNaming
}