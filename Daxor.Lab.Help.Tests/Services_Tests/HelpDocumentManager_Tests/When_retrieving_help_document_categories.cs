﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Services;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Services_Tests.HelpDocumentManager_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_retrieving_help_document_categories
	{
		private IFileManager _mockFileManager;

		private HelpDocumentManager _documentManager;

		private const string TestPath = "TestPath";

		[TestMethod]
		public void And_the_given_path_does_not_exist_then_the_category_collection_is_empty()
		{
			_mockFileManager = Substitute.For<IFileManager>();
			_mockFileManager.DirectoryExists(TestPath).Returns(false);
			_documentManager = new HelpDocumentManager(_mockFileManager, Substitute.For<IDocumentGenerator>(), Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>());

			Assert.AreEqual(0, _documentManager.RetrieveHelpDocumentCategories(TestPath).Count());
		}

		[TestMethod]
		public void And_there_are_no_subdirectories_in_the_given_path_then_the_collection_is_empty()
		{
			_mockFileManager = Substitute.For<IFileManager>();
			_mockFileManager.DirectoryExists(TestPath).Returns(true);
			_mockFileManager.GetSubDirectoriesInDirectory(TestPath).Returns(new List<DirectoryInfo>());
			_documentManager = new HelpDocumentManager(_mockFileManager, Substitute.For<IDocumentGenerator>(), Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>());

			Assert.AreEqual(0, _documentManager.RetrieveHelpDocumentCategories(TestPath).Count());
		}
	}
	// ReSharper restore InconsistentNaming
}
