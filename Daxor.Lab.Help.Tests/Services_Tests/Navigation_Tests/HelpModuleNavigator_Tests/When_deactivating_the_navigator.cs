﻿using System.Windows;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Services_Tests.Navigation_Tests.HelpModuleNavigator_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_deactivating_the_navigator
	{
		private IEventAggregator _stubAggregator;
		private INavigationCoordinator _stubCoordinator;
		private IRegion _mockRegion;
		private IRegionManager _stubRegionManager;
		private IUnityContainer _stubContainer;

		private ActiveViewTitleChanged _stubActiveViewTitleChangedEvent;
		private AppPartActivationChanged _stubAppPartActivationChangedEvent;
		private HelpViewChanged _mockReturnToHelpMenuSelectedEvent;

		private HelpModuleNavigator _navigator;

		[TestInitialize]
		public void Initialize()
		{
			_stubCoordinator = Substitute.For<INavigationCoordinator>();
			_stubAggregator = Substitute.For<IEventAggregator>();
			_stubActiveViewTitleChangedEvent = Substitute.For<ActiveViewTitleChanged>();
			_stubAppPartActivationChangedEvent = Substitute.For<AppPartActivationChanged>();
			_mockReturnToHelpMenuSelectedEvent = Substitute.For<HelpViewChanged>();
			_stubContainer = Substitute.For<IUnityContainer>();
			_stubRegionManager = Substitute.For<IRegionManager>();
			_mockRegion = Substitute.For<IRegion>();

			_stubActiveViewTitleChangedEvent.Publish(Arg.Any<string>());
			_stubAppPartActivationChangedEvent.Publish(Arg.Any<ActivationChangedPayload>());

			_stubAggregator.GetEvent<ActiveViewTitleChanged>().Returns(_stubActiveViewTitleChangedEvent);
			_stubAggregator.GetEvent<AppPartActivationChanged>().Returns(_stubAppPartActivationChangedEvent);
			_stubAggregator.GetEvent<HelpViewChanged>().Returns(_mockReturnToHelpMenuSelectedEvent);
			_stubRegionManager.Regions[RegionNames.WorkspaceRegion].Returns(_mockRegion);

			_navigator = new HelpModuleNavigator(_stubContainer, _stubAggregator, _stubRegionManager, _stubCoordinator);
		}

		[TestMethod]
		public void And_the_current_view_is_empty_only_the_deactivation_callback_is_invoked()
		{
			// Arrange
			Assert.AreEqual(HelpModuleViewKeys.Empty, _navigator.CurrentViewKey);

			// Act
			var wasCallbackInvoked = false;
			_navigator.Deactivate(() => { wasCallbackInvoked = true; });

			// Assert
			_mockRegion.DidNotReceive().Deactivate(Arg.Any<FrameworkElement>());
			Assert.IsTrue(wasCallbackInvoked);
		}

		[TestMethod]
		public void And_the_current_view_is_not_empty_then_the_region_is_deactivated()
		{
			// Arrange
			Assert.AreEqual(HelpModuleViewKeys.Empty, _navigator.CurrentViewKey);
			_navigator.Initialize();
			_navigator.Activate(new DisplayViewOption(), null);
			Assert.AreNotEqual(HelpModuleViewKeys.Empty, _navigator.CurrentViewKey);

			// Act
			_navigator.Deactivate(() => { });

			// Assert
			_mockRegion.Received(1).Deactivate(null);
		}

		[TestMethod]
		public void And_the_current_view_is_not_empty_then_the_deactivation_callback_is_invoked()
		{
			// Arrange
			Assert.AreEqual(HelpModuleViewKeys.Empty, _navigator.CurrentViewKey);
			_navigator.Initialize();
			_navigator.Activate(new DisplayViewOption(), null);
			Assert.AreNotEqual(HelpModuleViewKeys.Empty, _navigator.CurrentViewKey);

			// Act
			var wasCallbackInvoked = false;
			_navigator.Deactivate(() => { wasCallbackInvoked = true; });

			// Assert
			Assert.IsTrue(wasCallbackInvoked);
		}

		[TestMethod]
		public void And_the_current_view_is_not_empty_then_the_empty_view_is_activated()
		{
			// Arrange
			_navigator.Initialize();
			_navigator.Activate(new DisplayViewOption(), null);
			Assert.AreNotEqual(HelpModuleViewKeys.Empty, _navigator.CurrentViewKey);

			// Act
			_navigator.Deactivate(() => { });

			//Assert
			Assert.AreEqual(HelpModuleViewKeys.Empty, _navigator.CurrentViewKey);
		}

		[TestMethod]
		public void Then_a_help_view_changed_event_is_published_with_the_empty_key()
		{
			_navigator.Deactivate(() => { });

			_mockReturnToHelpMenuSelectedEvent.Received(1).Publish(HelpModuleViewKeys.Empty);
		}
	}
	// ReSharper restore InconsistentNaming
}
