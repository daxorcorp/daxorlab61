﻿using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Services_Tests.Navigation_Tests.HelpModuleNavigator_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_constructing_the_navigator
	{
		private IEventAggregator _stubAggregator;
		private INavigationCoordinator _mockCoordinator;
		private IRegionManager _stubManager;
		private IUnityContainer _mockContainer;

		private HelpModuleNavigator _navigator;

		[TestInitialize]
		public void Initialize()
		{
			_mockCoordinator = Substitute.For<INavigationCoordinator>();
			_stubAggregator = Substitute.For<IEventAggregator>();
			_mockContainer = Substitute.For<IUnityContainer>();
			_stubManager = Substitute.For<IRegionManager>();
		}

		#region Ctor

		[TestMethod]
		public void Then_the_module_is_registered_with_the_navigation_coordinator()
		{
			_navigator = new HelpModuleNavigator(_mockContainer, _stubAggregator, _stubManager, _mockCoordinator);

			_mockCoordinator.Received(1).RegisterAppModuleNavigator(AppModuleIds.Help, _navigator);
		}

		[TestMethod]
		public void Then_the_coordinator_sent_in_is_returned_when_asked()
		{
			_navigator = new HelpModuleNavigator(_mockContainer, _stubAggregator, _stubManager, _mockCoordinator);

			Assert.AreEqual(_mockCoordinator, _navigator.NavigationCoordinator);
		}

		#endregion

		[TestMethod]
		public void Then_the_navigator_can_be_activated()
		{
			_navigator = new HelpModuleNavigator(_mockContainer, _stubAggregator, _stubManager, _mockCoordinator);

			Assert.IsTrue(_navigator.CanActivate());
		}

		[TestMethod]
		public void Then_the_navigator_can_be_deactivated()
		{
			_navigator = new HelpModuleNavigator(_mockContainer, _stubAggregator, _stubManager, _mockCoordinator);

			Assert.IsTrue(_navigator.CanDeactivate());
		}
	}
	// ReSharper restore InconsistentNaming
}
