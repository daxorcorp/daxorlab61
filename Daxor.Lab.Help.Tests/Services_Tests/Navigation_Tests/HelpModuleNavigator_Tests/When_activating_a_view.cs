﻿using System.IO;
using System.Windows;
using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Services_Tests.Navigation_Tests.HelpModuleNavigator_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_activating_a_view
	{
		private IEventAggregator _mockAggregator;
		private INavigationCoordinator _stubCoordinator;
		private IRegion _mockRegion;
		private IRegionManager _stubRegionManager;
		private IUnityContainer _stubContainer;

		private ActiveViewTitleChanged _mockActiveViewTitleChangedEvent;
		private AppPartActivationChanged _stubAppPartActivationChangedEvent;
		private HelpViewChanged _stubHelpViewChangedEvent;

		private HelpModuleNavigator _navigator;

		[TestInitialize]
		public void Initialize()
		{
			_stubCoordinator = Substitute.For<INavigationCoordinator>();
			_mockAggregator = Substitute.For<IEventAggregator>();
			_mockActiveViewTitleChangedEvent = Substitute.For<ActiveViewTitleChanged>();
			_stubAppPartActivationChangedEvent = Substitute.For<AppPartActivationChanged>();
			_stubHelpViewChangedEvent = Substitute.For<HelpViewChanged>();
			_stubContainer = Substitute.For<IUnityContainer>();
			_stubRegionManager = Substitute.For<IRegionManager>();
			_mockRegion = Substitute.For<IRegion>();

			_mockActiveViewTitleChangedEvent.Publish(Arg.Any<string>());
			_stubAppPartActivationChangedEvent.Publish(Arg.Any<ActivationChangedPayload>());
			_mockRegion.ActiveViews.Contains(Arg.Any<FrameworkElement>()).Returns(false);
			_mockRegion.Views.Contains(Arg.Any<FrameworkElement>()).Returns(false);

			_mockAggregator.GetEvent<ActiveViewTitleChanged>().Returns(_mockActiveViewTitleChangedEvent);
			_mockAggregator.GetEvent<AppPartActivationChanged>().Returns(_stubAppPartActivationChangedEvent);
			_mockAggregator.GetEvent<HelpViewChanged>().Returns(_stubHelpViewChangedEvent);
			_stubRegionManager.Regions[RegionNames.WorkspaceRegion].Returns(_mockRegion);

			_navigator = new HelpModuleNavigator(_stubContainer, _mockAggregator, _stubRegionManager, _stubCoordinator);
		}

		[TestMethod]
		public void And_the_view_is_the_empty_view_then_nothing_happens()
		{
			_navigator.ActivateView(HelpModuleViewKeys.Empty);

			_mockActiveViewTitleChangedEvent.DidNotReceive().Publish("Help");
		}

		[TestMethod]
		public void And_the_view_is_unknown_then_nothing_happens()
		{
			Assert.IsFalse(_navigator.Views.ContainsKey(HelpModuleViewKeys.MainView));

			_navigator.ActivateView(HelpModuleViewKeys.MainView);

			_mockActiveViewTitleChangedEvent.DidNotReceive().Publish("Help");
		}

		[TestMethod]
		public void And_the_view_is_already_active_then_nothing_happens()
		{
			// Arrange
			_mockRegion.ActiveViews.Contains(Arg.Any<FrameworkElement>()).Returns(true);
			_stubRegionManager.Regions[RegionNames.WorkspaceRegion].Returns(_mockRegion);
			_navigator = new HelpModuleNavigator(_stubContainer, _mockAggregator, _stubRegionManager, _stubCoordinator);
			_navigator.Initialize();

			// Act
			_navigator.ActivateView(HelpModuleViewKeys.MainView);

			// Assert
			_mockActiveViewTitleChangedEvent.DidNotReceive().Publish("Help");
		}

		[TestMethod]
		public void And_the_current_view_is_the_empty_view_then_it_is_not_deactivated()
		{
			// Arrange
			_navigator.Initialize();
			Assert.AreEqual(HelpModuleViewKeys.Empty, _navigator.CurrentViewKey);

			// Act
			_navigator.ActivateView(HelpModuleViewKeys.MainView);

			// Assert
			_mockRegion.DidNotReceive().Deactivate(Arg.Any<FrameworkElement>());
			_mockActiveViewTitleChangedEvent.Received(1).Publish("Help");
		}

		[TestMethod]
		public void And_the_current_view_is_not_the_empty_view_then_it_is_deactivated()
		{
			//Arrange
			_navigator.Initialize();

			//Act
			_navigator.ActivateView(HelpModuleViewKeys.MainView);
			_navigator.ActivateView(HelpModuleViewKeys.DocumentView);

			//Assert
			_mockRegion.Received(1).Deactivate(Arg.Any<FrameworkElement>());
			_mockActiveViewTitleChangedEvent.Received(2).Publish("Help");
		}

		[TestMethod]
		public void And_the_view_is_already_in_the_view_collection_then_it_is_not_readded()
		{
			// Arrange
			_mockRegion.Views.Contains(Arg.Any<FrameworkElement>()).Returns(true);
			_stubRegionManager.Regions[RegionNames.WorkspaceRegion].Returns(_mockRegion);
			_navigator = new HelpModuleNavigator(_stubContainer, _mockAggregator, _stubRegionManager, _stubCoordinator);
			_navigator.Initialize();

			// Act
			_navigator.ActivateView(HelpModuleViewKeys.MainView);

			// Assert
			_mockRegion.Received(2).Add(Arg.Any<FrameworkElement>());
		}

		[TestMethod]
		public void And_the_view_is_not_in_view_collection_then_it_is_added()
		{
			_navigator.Initialize();

			_navigator.ActivateView(HelpModuleViewKeys.MainView);

			_mockRegion.Received(3).Add(Arg.Any<FrameworkElement>());
		}

		[TestMethod]
		public void Then_the_view_is_activated_for_the_region()
		{
			_navigator.Initialize();

			_navigator.ActivateView(HelpModuleViewKeys.MainView);

			_mockRegion.Received(1).Activate(Arg.Any<FrameworkElement>());
		}

		[TestMethod]
		public void Then_the_current_view_key_is_updated()
		{
			// Arrange
			_navigator.Initialize();
			Assert.AreNotEqual(HelpModuleViewKeys.MainView, _navigator.CurrentViewKey);

			// Act
			_navigator.ActivateView(HelpModuleViewKeys.MainView);

			// Assert
			Assert.AreEqual(HelpModuleViewKeys.MainView, _navigator.CurrentViewKey);
		}

		[TestMethod]
		public void And_the_view_is_activated_then_the_ActiveViewTitleChanged_event_is_published()
		{
			// Arrange
			_navigator.Initialize();
			Assert.AreNotEqual(HelpModuleViewKeys.MainView, _navigator.CurrentViewKey);

			// Act
			_navigator.ActivateView(HelpModuleViewKeys.MainView);

			// Assert
			_mockActiveViewTitleChangedEvent.Received(1).Publish("Help");
		}

		[TestMethod]
		public void And_the_view_is_not_the_main_view_nor_the_document_view_then_no_event_is_published()
		{
			_navigator.ActivateView(HelpModuleViewKeys.MainView);

			_mockAggregator.DidNotReceive().GetEvent<HelpViewChanged>();
		}

		[TestMethod]
		public void And_the_view_is_the_main_view_then_a_view_changed_event_is_published()
		{
			var mockViewChangedEvent = Substitute.For<HelpViewChanged>();
			var mockDocumentSelectedEvent = Substitute.For<HelpDocumentSelected>();
			_mockAggregator.GetEvent<HelpViewChanged>().Returns(mockViewChangedEvent);
			_mockAggregator.GetEvent<HelpDocumentSelected>().Returns(mockDocumentSelectedEvent);
			_navigator = new HelpModuleNavigator(_stubContainer, _mockAggregator, _stubRegionManager, _stubCoordinator);

			_navigator.Initialize();

			_navigator.ActivateView(HelpModuleViewKeys.MainView);

			mockViewChangedEvent.Received(1).Publish(Arg.Any<string>());
			mockDocumentSelectedEvent.DidNotReceive().Publish(Arg.Any<FileInfo>());
		}

		[TestMethod]
		public void And_the_view_is_the_document_view_and_the_payload_is_not_null_then_both_view_changed_and_document_selected_events_are_published()
		{
			var mockViewChangedEvent = Substitute.For<HelpViewChanged>();
			var mockDocumentSelectedEvent = Substitute.For<HelpDocumentSelected>();
			var stubFile = new FileInfo("TestFile.pdf");
			_mockAggregator.GetEvent<HelpViewChanged>().Returns(mockViewChangedEvent);
			_mockAggregator.GetEvent<HelpDocumentSelected>().Returns(mockDocumentSelectedEvent);
			_navigator = new HelpModuleNavigator(_stubContainer, _mockAggregator, _stubRegionManager, _stubCoordinator);

			_navigator.Initialize();

			_navigator.ActivateView(HelpModuleViewKeys.DocumentView, stubFile);

			mockViewChangedEvent.Received(1).Publish(HelpModuleViewKeys.DocumentView);
			mockDocumentSelectedEvent.Received(1).Publish(stubFile);
		}
	}
	// ReSharper restore InconsistentNaming
}
