﻿using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Services_Tests.Navigation_Tests.HelpModuleNavigator_Tests
{
    internal class HelpModuleNavigatorWithMockActivateView : HelpModuleNavigator
    {
        internal HelpModuleNavigatorWithMockActivateView(string currentViewKey, IEventAggregator eventAggregator)
            : base(Substitute.For<IUnityContainer>(), eventAggregator,
                Substitute.For<IRegionManager>(), Substitute.For<INavigationCoordinator>())
        {
            _currentViewKey = currentViewKey;
        }

        internal string ActivatedViewKey
        {
            get { return _currentViewKey; }
        }

        public override void ActivateView(string viewKey, object payload)
        {
            _currentViewKey = viewKey;
        }
    }
}