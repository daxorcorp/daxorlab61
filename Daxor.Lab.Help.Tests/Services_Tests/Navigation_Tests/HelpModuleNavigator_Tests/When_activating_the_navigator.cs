﻿using Daxor.Lab.Help.Common.Events;
using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Services_Tests.Navigation_Tests.HelpModuleNavigator_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_activating_the_navigator
	{
		private IEventAggregator _stubAggregator;
		private INavigationCoordinator _stubCoordinator;
		private IRegionManager _stubRegionManager;
		private IUnityContainer _stubContainer;

		private HelpModuleNavigator _navigator;

		[TestInitialize]
		public void Initialize()
		{
			_stubCoordinator = Substitute.For<INavigationCoordinator>();
			_stubAggregator = Substitute.For<IEventAggregator>();
			_stubAggregator.GetEvent<ActiveViewTitleChanged>().Returns(new ActiveViewTitleChanged());
			_stubAggregator.GetEvent<AppPartActivationChanged>().Returns(new AppPartActivationChanged());
			_stubAggregator.GetEvent<HelpViewChanged>().Returns(new HelpViewChanged());
			_stubContainer = Substitute.For<IUnityContainer>();
			_stubRegionManager = Substitute.For<IRegionManager>();
		}

		[TestMethod]
		public void Then_the_AppPartActivationChanged_event_is_published()
		{
			// Arrange
			var _mockEvent = Substitute.For<AppPartActivationChanged>();
			_stubAggregator.GetEvent<AppPartActivationChanged>().Returns(_mockEvent);

			_navigator = new HelpModuleNavigator(_stubContainer, _stubAggregator, _stubRegionManager, _stubCoordinator);

			// Act
			_navigator.Activate(new DisplayViewOption(), null);

			// Assert
			_mockEvent.Received(1).Publish(Arg.Any<ActivationChangedPayload>());
		}

		[TestMethod]
		public void And_the_current_view_is_the_empty_view_then_the_main_view_is_activated()
		{
			var navigator = new HelpModuleNavigatorWithMockActivateView(HelpModuleViewKeys.Empty, _stubAggregator);

			navigator.Activate(DisplayViewOption.Default, null);

			Assert.AreEqual(HelpModuleViewKeys.MainView, navigator.ActivatedViewKey);
		}
		
		// If the current key is the main view, show that
		[TestMethod]
		public void And_the_current_view_is_the_main_view_then_the_main_view_is_activated()
		{
			var navigator = new HelpModuleNavigatorWithMockActivateView(HelpModuleViewKeys.MainView, _stubAggregator);

			navigator.Activate(DisplayViewOption.Default, null);

			Assert.AreEqual(HelpModuleViewKeys.MainView, navigator.ActivatedViewKey);
		}

		[TestMethod]
		public void And_the_navigator_is_not_active_then_IsActive_returns_false()
		{
			var navigator = new HelpModuleNavigatorWithMockActivateView(HelpModuleViewKeys.MainView, _stubAggregator);

			Assert.IsFalse(navigator.IsActive);
		}
	}
	// ReSharper restore InconsistentNaming
}
