﻿using System.Collections.Generic;
using System.Text;
using Daxor.Lab.Help.Services;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Services_Tests.LomSignoffFormGenerator_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_generating_a_laboratory_operations_manual_signoff_form
	{
		private ISettingsManager _mockSettingsManager;

		[TestInitialize]
		public void Initialize()
		{
			_mockSettingsManager = Substitute.For<ISettingsManager>();
			_mockSettingsManager.GetSetting<string>(Arg.Any<string>()).Returns("");
		}

		[TestMethod]
		public void Then_the_settings_manager_is_used_to_get_location_settings()
        {
            var signoffSheetGenerator = new LomSignoffFormGenerator(_mockSettingsManager);
			
            signoffSheetGenerator.Generate();

			_mockSettingsManager.Received(1).GetSetting<string>(SettingKeys.SystemHospitalName);
			_mockSettingsManager.Received(1).GetSetting<string>(SettingKeys.SystemDepartmentAddress);
			_mockSettingsManager.Received(1).GetSetting<string>(SettingKeys.SystemDepartmentName);
			_mockSettingsManager.Received(1).GetSetting<string>(SettingKeys.SystemDepartmentPhoneNumber);
			_mockSettingsManager.Received(1).GetSetting<string>(SettingKeys.SystemDepartmentDirector);
		}

	    [TestMethod]
	    public void Then_the_document_is_generated_in_the_correct_order()
	    {
	        var signoffSheetGenerator = new LomSignoffFormGeneratorThatDoesNotMakeADocument(_mockSettingsManager);

            signoffSheetGenerator.Generate();

	        var expectedOperations = new List<string>
	        {
	            LomSignoffFormGeneratorThatDoesNotMakeADocument.AddHospitalInformationMethod,
	            LomSignoffFormGeneratorThatDoesNotMakeADocument.AddDisclaimerTextMethod,
                LomSignoffFormGeneratorThatDoesNotMakeADocument.AddEmptyRowsMethod,
                LomSignoffFormGeneratorThatDoesNotMakeADocument.SavePdfMethod,
	        };

	        CollectionAssert.AreEqual(expectedOperations, signoffSheetGenerator.Operations,
	            "Expected: " + ListToString(expectedOperations) + " --- Observed: " +
	            ListToString(signoffSheetGenerator.Operations));
	    }

	    private static string ListToString(IEnumerable<string> list)
	    {
	        var stringBuilder = new StringBuilder();
	        stringBuilder.Append("[ ");
	        foreach (var element in list) stringBuilder.Append("\"" + element + "\", ");
            stringBuilder.Append("]");
	        return stringBuilder.ToString();
	    }
	}
	// ReSharper restore InconsistentNaming
}
