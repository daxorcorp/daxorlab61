﻿using System.Collections.Generic;
using Daxor.Lab.Help.Services;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.Help.Tests.Services_Tests.LomSignoffFormGenerator_Tests
{
	public class LomSignoffFormGeneratorThatDoesNotMakeADocument : LomSignoffFormGenerator
	{
	    public const string AddDisclaimerTextMethod = "AddDisclaimerText";
	    public const string AddHospitalInformationMethod = "AddHospitalInformation";
	    public const string AddEmptyRowsMethod = "AddEmptyRows";
	    public const string SavePdfMethod = "SavePdf";

		public LomSignoffFormGeneratorThatDoesNotMakeADocument(ISettingsManager settingsManager) : base(settingsManager)
		{
            Operations = new List<string>();
        }

        public List<string> Operations { get; private set; }

		protected override void AddStatementTextToDocument()
		{
			Operations.Add(AddDisclaimerTextMethod);
		}

		protected override void AddHospitalInformation()
		{
			Operations.Add(AddHospitalInformationMethod);
		}

	    protected override void AddEmptyRowsToSignatureTable()
	    {
            Operations.Add(AddEmptyRowsMethod);
	    }

	    protected override void SavePdfRenderingToFile()
	    {
            Operations.Add(SavePdfMethod);
	    }
	}
}
