﻿using Daxor.Lab.Help.Services;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Help.Tests.Services_Tests.LomSignoffFormGenerator_Tests.SadPath_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_creating_a_LOM_signoff_form_generator
	{
		[TestMethod]
		public void And_the_settings_manager_is_null_then_an_exception_is_thrown()
		{
			AssertEx.ThrowsArgumentNullException(() => new LomSignoffFormGenerator(null), "settingsManager");
		}
	}
	// ReSharper restore InconsistentNaming
}
