﻿using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Services_Tests.LomSignoffFormGenerator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_form_statement_text
    {
        private const string _testDepartmentName = "Test Department";

        private ISettingsManager _stubSettingsManager;

        private LomSignoffFormGeneratorThatDoesNotMakeADocument _generator;

        [TestInitialize]
        public void Initialize()
        {
            _stubSettingsManager = Substitute.For<ISettingsManager>();
            _stubSettingsManager.GetSetting<string>(SettingKeys.SystemDepartmentName).Returns(_testDepartmentName);

            _generator = new LomSignoffFormGeneratorThatDoesNotMakeADocument(_stubSettingsManager);
            _generator.Generate();
        }

        [TestMethod]
        [RequirementValue("1.    " + _testDepartmentName + " testing personnel's authentic signatures and initials.")]
        public void Then_the_first_statement_text_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), _generator.Statement1);
        }

        [TestMethod]
        [RequirementValue("2.    Acknowledgement that the " + _testDepartmentName +
                       " testing personnel have read and are familiar with the contents of the Laboratory Operations Manual.")]
        public void Then_the_second_statement_text_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), _generator.Statement2);
        }

        [TestMethod]
        [RequirementValue("3.    That all work done in the " + _testDepartmentName + " is done in accordance with these procedures.")]
        public void Then_the_third_statement_text_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), _generator.Statement3);
        }
    }
    // ReSharper restore RedundantArgumentName
}