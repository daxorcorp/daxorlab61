﻿using System.Text;
using Daxor.Lab.Help.Services;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Services_Tests.LomSignoffFormGenerator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_adding_line_breaks_and_indenting
    {
        private LomSignoffFormGenerator _generator;
        private const string tenCharacterString = "1234567890";

        [TestInitialize]
        public void Initialize()
        {
            _generator = new LomSignoffFormGenerator(Substitute.For<ISettingsManager>());
        }

        [TestMethod]
        public void And_given_a_string_that_is_less_than_120_characters_then_that_string_is_returned()
        {
            Assert.AreEqual(tenCharacterString, _generator.AddLineBreaksAndIndent(tenCharacterString));
        }

        [TestMethod]
        public void And_given_a_string_that_is_exactly_120_characters_then_that_string_is_returned()
        {
            var hundredTwentyTestString = new StringBuilder();
            hundredTwentyTestString.Insert(0, tenCharacterString, 12);
            Assert.AreEqual(hundredTwentyTestString.ToString(), _generator.AddLineBreaksAndIndent(hundredTwentyTestString.ToString()));
        }

        [TestMethod]
        public void And_given_a_string_that_is_greater_then_120_characters_then_a_line_break_and_indentation_are_added()
        {
            var hundredThirtyCharacterString = new StringBuilder();
            hundredThirtyCharacterString.Insert(0, tenCharacterString, 10).Append(" ").Append(tenCharacterString).Append(tenCharacterString).Append(tenCharacterString);
            
            var adjustedHundredThirtyCharacterString = new StringBuilder();
            adjustedHundredThirtyCharacterString.Insert(0, tenCharacterString, 10).Append("\n       ").Append(tenCharacterString).Append(tenCharacterString).Append(tenCharacterString);
            Assert.AreEqual(adjustedHundredThirtyCharacterString.ToString(), _generator.AddLineBreaksAndIndent(hundredThirtyCharacterString.ToString()));
        }
    }
    // ReSharper restore RedundantArgumentName
}