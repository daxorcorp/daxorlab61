﻿using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Services;
using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageBox;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Module_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_initializing_the_Help_module
	{
		private IUnityContainer _mockContainer;

		private Module _helpModule;

		[TestInitialize]
		public void Initialize()
		{
			_mockContainer = Substitute.For<IUnityContainer>();
			_helpModule = new Module(_mockContainer);
		}

		[TestMethod]
		public void Then_the_help_module_navigator_is_registered_with_the_container()
		{
			_helpModule.InitializeAppModule();

			_mockContainer.Received(1).RegisterType<IAppModuleNavigator, HelpModuleNavigator>(AppModuleIds.Help, Arg.Any<ContainerControlledLifetimeManager>());
		}

		[TestMethod]
		public void Then_the_file_manager_is_registered_with_the_container()
		{
			_helpModule.InitializeAppModule();

			_mockContainer.Received(1).RegisterType<IFileManager, SystemFileManager>();
		}

		[TestMethod]
		public void Then_the_help_document_manager_is_registered_with_the_container()
		{
			_helpModule.InitializeAppModule();

			_mockContainer.Received(1).RegisterType<IHelpDocumentManager, HelpDocumentManager>();
		}

		[TestMethod]
		public void Then_the_app_module_navigator_is_initialized()
		{
			var mockAppModuleNavigator = Substitute.For<IAppModuleNavigator>();
			_mockContainer.Resolve<IAppModuleNavigator>(AppModuleIds.Help).Returns(mockAppModuleNavigator);
			
			var helpModule = new Module(_mockContainer);
			helpModule.InitializeAppModule();

			mockAppModuleNavigator.Received(1).Initialize();
		}

		[TestMethod]
		public void Then_the_message_box_dispatcher_is_resolved()
		{
			_helpModule.InitializeAppModule();

			_mockContainer.Received(1).RegisterType<IMessageBoxDispatcher, XamlMessageBoxDispatcher>();
		}
	}
	// ReSharper restore InconsistentNaming
}
