using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Module_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_the_module
    {
        private Module _helpModule;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _helpModule = new Module(Substitute.For<IUnityContainer>());
        }

        [TestMethod]
        [RequirementValue("Help")]
        public void Then_the_short_display_name_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), _helpModule.ShortDisplayName);
        }

        [TestMethod]
        [RequirementValue("Help")]
        public void Then_the_full_display_name_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), _helpModule.ShortDisplayName);
        }

        [TestMethod]
        public void Then_it_is_configured_as_the_fourth_module()
        {
            Assert.AreEqual(4, _helpModule.DisplayOrder);
        }

        [TestMethod]
        public void Then_the_module_is_not_configurable()
        {
            Assert.IsFalse(_helpModule.IsConfigurable);
        }
}
    // ReSharper restore InconsistentNaming
}
