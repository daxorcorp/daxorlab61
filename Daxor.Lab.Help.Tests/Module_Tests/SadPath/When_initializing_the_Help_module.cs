﻿using System;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Services;
using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Infrastructure.Exceptions;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageBox;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.Tests.Module_Tests.SadPath
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_the_Help_module
	{
		private Module _module;
		private IUnityContainer _container;

		[TestInitialize]
		public void BeforeEachTest()
		{
			_container = Substitute.For<IUnityContainer>();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_LOM_signoff_form_generator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IDocumentGenerator, LomSignoffFormGenerator>()).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Help", ex.ModuleName);
				Assert.AreEqual("initialize the LOM Signoff Form Generator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_system_file_manager_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IFileManager, SystemFileManager>()).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Help", ex.ModuleName);
				Assert.AreEqual("initialize the System File Manager", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_help_document_manager_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IHelpDocumentManager, HelpDocumentManager>()).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Help", ex.ModuleName);
				Assert.AreEqual("initialize the Help Document Manager", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_XAML_message_box_dispatcher_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IMessageBoxDispatcher, XamlMessageBoxDispatcher>()).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Help", ex.ModuleName);
				Assert.AreEqual("initialize the Message Box Dispatcher", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_registering_the_help_module_navigator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.RegisterType<IAppModuleNavigator, HelpModuleNavigator>(Arg.Any<string>(), Arg.Any<ContainerControlledLifetimeManager>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Help", ex.ModuleName);
				Assert.AreEqual("register the Help Module Navigator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}

		[TestMethod]
		public void And_an_error_occurs_while_resolving_the_help_module_navigator_then_the_correct_module_load_exception_is_thrown()
		{
			_container.WhenForAnyArgs(c => c.Resolve<IAppModuleNavigator>(Arg.Any<string>())).Throw(new Exception());
			_module = new Module(_container);

			try
			{
				_module.InitializeAppModule();
			}
			catch (ModuleLoadException ex)
			{
				Assert.AreEqual("Help", ex.ModuleName);
				Assert.AreEqual("initialize the Help Module Navigator", ex.FailingAction);
				return;
			}
			Assert.Fail();
		}
	}
	// ReSharper restore InconsistentNaming
}