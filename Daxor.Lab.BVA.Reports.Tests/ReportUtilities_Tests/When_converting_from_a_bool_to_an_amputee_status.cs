﻿using Daxor.Lab.BVA.Reports.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.BVA.Reports.Tests.ReportUtilities_Tests
{
	[TestClass]
	public class When_converting_from_a_bool_to_an_amputee_status
	{
		[TestMethod]
		public void Then_true_returns_a_positive_amputee_status()
		{
			Assert.AreEqual("Amputee", ReportUtilities.BoolToAmputeeStatus(true));
		}

		[TestMethod]
		public void Then_false_returns_a_negative_amputee_status()
		{
			Assert.AreEqual("Non-amputee", ReportUtilities.BoolToAmputeeStatus(false));
		}
	}
}
