﻿using System;
using Daxor.Lab.BVA.Reports.Interfaces;
using Daxor.Lab.BVA.Reports.ReportControllers;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using NSubstitute;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Reports.Tests.ReportControllers_Tests.BvaReportController_Tests.Stubs
{
    public class StubBvaReportControllerWithCustomizedExportFaulted : BvaQuickReportController
    {
        public const string FailureMessage = "Turtles";

        public StubBvaReportControllerWithCustomizedExportFaulted(IStarBurnWrapper driveWrapper, IEventAggregator eventAggregator, IFolderBrowser folderBrowser, IAuditTrailDataAccess auditTrailDataAccess)
            : base(MockRepository.GenerateStub<ILoggerFacade>(),
                MockRepository.GenerateStub<IMessageBoxDispatcher>(),
                MockRepository.GenerateStub<IRegionManager>(),
                MockRepository.GenerateStub<IIdealsCalcEngineService>(),
                eventAggregator,
                BvaTestFactory.CreateBvaTest(4, 60),
                folderBrowser,
                MockRepository.GenerateStub<IMessageManager>(),
                driveWrapper,
                MockRepository.GenerateStub<ISettingsManager>(),
                auditTrailDataAccess,
				Substitute.For<IZipFileFactory>(), Substitute.For<IPdfPrinter>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }
        
        public bool FailedWithCorrectMessage { get; set; }
        
        protected override void OnExportTaskFaulted(Exception faultingException)
        {
            FailedWithCorrectMessage = faultingException.InnerException.Message == FailureMessage;
        }

        protected override void OnExportTaskCompleted()
        {
            FailedWithCorrectMessage = false;
        }

        protected override void SavePdfRenderingToFile(string fileName, string tempPath)
        {
        }

        protected override void SaveReportXmlToFile(string fileName, string tempPath)
        {
        }

        protected override void SaveZipFile(string fileName, string zipFilePath,
            string tempPath)
        {
        }

        protected override void CleanUpAfterExport(string fileName, string tempPath)
        {
        }
    }
}