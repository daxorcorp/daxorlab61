﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Reports.Interfaces;
using Daxor.Lab.BVA.Reports.ReportControllers;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using NSubstitute;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Reports.Tests.ReportControllers_Tests.BvaReportController_Tests.Stubs
{
    public class StubBvaReportControllerWithTestOnlyCtor : BvaQuickReportController
    {
        private readonly BVATest _test;

        public string FileName { get; private set; }

        public StubBvaReportControllerWithTestOnlyCtor(BVATest test, IAuditTrailDataAccess auditTrailDataAccess)
            : base(MockRepository.GenerateStub<ILoggerFacade>(),
                MockRepository.GenerateStub<IMessageBoxDispatcher>(), MockRepository.GenerateStub<IRegionManager>(),
                MockRepository.GenerateStub<IIdealsCalcEngineService>(), MockRepository.GenerateStub<IEventAggregator>(),
                test, MockRepository.GenerateStub<IFolderBrowser>(), MockRepository.GenerateStub<IMessageManager>(), MockRepository.GenerateStub<IStarBurnWrapper>(),
                MockRepository.GenerateStub<ISettingsManager>(), auditTrailDataAccess, Substitute.For<IZipFileFactory>(), Substitute.For<IPdfPrinter>())
        {
            _test = test;
        }

        public override void ExportReport()
        {
            FileName = BuildFileName(_test.Patient.HospitalPatientId);
        }
    }
}
