﻿using System;
using System.Threading;
using Daxor.Lab.BVA.Reports.Interfaces;
using Daxor.Lab.BVA.Reports.ReportControllers;
using Daxor.Lab.BVA.TestHelpers;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using NSubstitute;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Reports.Tests.ReportControllers_Tests.BvaReportController_Tests.Stubs
{
    public class StubBvaReportControllerThatFailsToExport : BvaQuickReportController
    {
        public const string ExceptionMessage = "An exception was thrown";

        public StubBvaReportControllerThatFailsToExport(IEventAggregator eventAggregator, IFolderBrowser folderBrowser, IAuditTrailDataAccess auditTrailDataAccess)
            : base(MockRepository.GenerateStub<ILoggerFacade>(),
                MockRepository.GenerateStub<IMessageBoxDispatcher>(),
                MockRepository.GenerateStub<IRegionManager>(),
                MockRepository.GenerateStub<IIdealsCalcEngineService>(),
                eventAggregator,
                BvaTestFactory.CreateBvaTest(4, 60),
                folderBrowser,
                MockRepository.GenerateStub<IMessageManager>(), null, MockRepository.GenerateStub<ISettingsManager>(),
				auditTrailDataAccess, Substitute.For<IZipFileFactory>(), Substitute.For<IPdfPrinter>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public StubBvaReportControllerThatFailsToExport(IFolderBrowser folderBrowser, IMessageBoxDispatcher messageBoxDispatcher, IMessageManager messageManager,
            IAuditTrailDataAccess auditTrailDataAccess)
            : base(MockRepository.GenerateStub<ILoggerFacade>(),
                   messageBoxDispatcher,
                   MockRepository.GenerateStub<IRegionManager>(),
                   MockRepository.GenerateStub<IIdealsCalcEngineService>(),
                   MockRepository.GenerateStub<IEventAggregator>(),
                   BvaTestFactory.CreateBvaTest(4, 60),
                   folderBrowser,
                   messageManager, null, MockRepository.GenerateStub<ISettingsManager>(),
				   auditTrailDataAccess, Substitute.For<IZipFileFactory>(), Substitute.For<IPdfPrinter>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        public StubBvaReportControllerThatFailsToExport(IFolderBrowser folderBrowser, IMessageManager messageManager, ILoggerFacade logger, IAuditTrailDataAccess auditTrailDataAccess)
            : base(logger,
                   MockRepository.GenerateStub<IMessageBoxDispatcher>(),
                   MockRepository.GenerateStub<IRegionManager>(),
                   MockRepository.GenerateStub<IIdealsCalcEngineService>(),
                   MockRepository.GenerateStub<IEventAggregator>(),
                   BvaTestFactory.CreateBvaTest(4, 60),
                   folderBrowser,
                   messageManager, 
                   null,
                   MockRepository.GenerateStub<ISettingsManager>(),
				   auditTrailDataAccess, Substitute.For<IZipFileFactory>(), Substitute.For<IPdfPrinter>())
        {
            TaskScheduler = new CurrentThreadTaskScheduler();
        }

        protected override void ExportTask(string fileName, IFolderBrowser folderBrowser, string zipFilePath,
            CancellationTokenSource cancellationTokenSource)
        {
            throw new ArgumentException(ExceptionMessage);
        }
    }
}
