﻿using System;
using System.Collections.Generic;
using System.Text;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Reports.Tests.ReportControllers_Tests.BvaReportController_Tests.Stubs;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Reports.Tests.ReportControllers_Tests.BvaReportController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_the_export_report
    {
        [TestMethod]
        public void Then_the_file_name_contains_the_patient_id_and_the_properly_formatted_time()
        {
            const string expectedPatientId = "SAMPLE PATIENT";
            var bvTest = new BVATest(null) {Patient = new BVAPatient(null) {HospitalPatientId = expectedPatientId}};
            var stubAuditTrailDataAccess = MockRepository.GenerateStub<IAuditTrailDataAccess>();
            stubAuditTrailDataAccess.Expect(e => e.SelectList(new Guid())).IgnoreArguments().Return(new List<AuditTrailRecord>());

            var reportController = new StubBvaReportControllerWithTestOnlyCtor(bvTest, stubAuditTrailDataAccess);
            reportController.ExportReport();

            var expectedDatetime = DateTime.Now;
            const string expectedIdWithHyphen = expectedPatientId + "-";
            var actualFileName = reportController.FileName;

            var actualIdWithHyphen = actualFileName.Substring(0, expectedPatientId.Length + 1);

            var actualTime = actualFileName.Substring(expectedPatientId.Length + 1);
            var actualDateTime = FixTimeFormat(actualTime);
            var timeDifference = actualDateTime - expectedDatetime;

            Assert.AreEqual("MM-dd-yyyy hh-mm-ss tt".Length, actualTime.Length);
            Assert.IsTrue(Math.Abs(timeDifference.Seconds) < 3, "Too much time between BVA test creation date and exported file time");
            Assert.AreEqual(expectedIdWithHyphen, actualIdWithHyphen);
        }

        private static DateTime FixTimeFormat(string actualTime)
        {
            var timeWithColons = new StringBuilder(actualTime);
            timeWithColons[13] = ':';
            timeWithColons[16] = ':';
            return DateTime.Parse(timeWithColons.ToString());
        }
    }
    // ReSharper restore InconsistentNaming
}
