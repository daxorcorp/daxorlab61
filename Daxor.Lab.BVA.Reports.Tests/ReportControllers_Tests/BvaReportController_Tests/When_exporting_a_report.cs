using System;
using System.Collections.Generic;
using Daxor.Lab.BVA.Reports.Tests.ReportControllers_Tests.BvaReportController_Tests.Stubs;
using Daxor.Lab.Database.DataRecord;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.Reports.Tests.ReportControllers_Tests.BvaReportController_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_exporting_a_report
    {
        private IFolderBrowser _stubFolderBrowser;
        private IAuditTrailDataAccess _stubAuditTrailDataAccess;

        [TestInitialize]
        public void TestInitialize()
        {
            _stubFolderBrowser = MockRepository.GenerateStub<IFolderBrowser>();
            _stubFolderBrowser.Stub(fb => fb.IsPathValid()).Return(true);
            _stubFolderBrowser.IsOpticalDrive = true;

            _stubAuditTrailDataAccess = MockRepository.GenerateStub<IAuditTrailDataAccess>();
            _stubAuditTrailDataAccess.Expect(a => a.SelectList(new Guid()))
                .IgnoreArguments()
                .Return(new List<AuditTrailRecord>());
        }
        
        [TestMethod]
        public void And_the_export_fails_then_the_busy_indicator_is_disabled()
        {
            var mockEventAggregator = MockRepository.GenerateMock<IEventAggregator>();
            mockEventAggregator.Expect(e => e.GetEvent<BusyStateChanged>().Publish(Arg<BusyPayload>.Matches(x => !x.IsBusy)));

            var failingExportController = new StubBvaReportControllerThatFailsToExport(mockEventAggregator, _stubFolderBrowser, _stubAuditTrailDataAccess);

            failingExportController.ExportReport();

            mockEventAggregator.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_export_fails_then_the_correct_message_is_displayed()
        {
            var errorMessage = new Message("key", "description", "fix it", "id", MessageType.Alert);
            var stubMessageManager = MockRepository.GenerateStub<IMessageManager>();
            stubMessageManager.Expect(x => x.GetMessage(MessageKeys.BvaExportUnsuccessful)).Return(errorMessage);

            var mockMessageBoxDispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            mockMessageBoxDispatcher.Expect(d => d.ShowMessageBox(
                Arg<MessageBoxCategory>.Is.Equal(MessageBoxCategory.Error),
                Arg<string>.Is.Equal(errorMessage.FormattedMessage),
                Arg<string>.Is.Equal("Export Blood Volume Analysis Report"),
                Arg<MessageBoxChoiceSet>.Is.Equal(MessageBoxChoiceSet.Close)));

            var failingExportController = new StubBvaReportControllerThatFailsToExport(_stubFolderBrowser, mockMessageBoxDispatcher,
                stubMessageManager, _stubAuditTrailDataAccess);

            failingExportController.ExportReport();

            mockMessageBoxDispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_export_fails_then_the_exception_message_is_logged()
        {
            var errorMessage = new Message("key", "description", "fix it", "id", MessageType.Alert);
            var stubMessageManager = MockRepository.GenerateStub<IMessageManager>();
            stubMessageManager.Expect(x => x.GetMessage(MessageKeys.BvaExportUnsuccessful)).Return(errorMessage);

            var logMessage = String.Format("{0}; inner exception: {1}", errorMessage.FormattedMessage,
                StubBvaReportControllerThatFailsToExport.ExceptionMessage);

            var mockLoggerFacade = MockRepository.GenerateMock<ILoggerFacade>();
            mockLoggerFacade.Expect(x => x.Log(logMessage, Category.Exception, Priority.High));

            var failingExportController = new StubBvaReportControllerThatFailsToExport(_stubFolderBrowser,
                stubMessageManager, mockLoggerFacade, _stubAuditTrailDataAccess);

            failingExportController.ExportReport();

            mockLoggerFacade.VerifyAllExpectations();
        }

        [TestMethod]
        public void And_the_optical_drive_fails_then_the_export_fails()
        {
            var stubOpticalDriveWriter = MockRepository.GenerateStub<IStarBurnWrapper>();
            stubOpticalDriveWriter.Expect(w => w.CreateDataBurner()).Throw(new Exception(StubBvaReportControllerWithCustomizedExportFaulted.FailureMessage));

            var stubEventAggregator = MockRepository.GenerateStub<IEventAggregator>();
            stubEventAggregator.Expect(x => x.GetEvent<BusyStateChanged>()).Return(new BusyStateChanged());

            var failingExportController = new StubBvaReportControllerWithCustomizedExportFaulted(stubOpticalDriveWriter, stubEventAggregator, _stubFolderBrowser, _stubAuditTrailDataAccess);

            failingExportController.ExportReport();

            Assert.IsTrue(failingExportController.FailedWithCorrectMessage);
        }
    }
    // ReSharper restore InconsistentNaming
}
