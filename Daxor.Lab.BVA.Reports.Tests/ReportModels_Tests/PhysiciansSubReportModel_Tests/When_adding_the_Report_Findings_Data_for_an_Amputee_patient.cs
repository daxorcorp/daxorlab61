﻿using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.Reports.ReportModels;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

// ReSharper disable InconsistentNaming

namespace Daxor.Lab.BVA.Reports.Tests.ReportModels_Tests.PhysiciansSubReportModel_Tests
{
	[TestClass]
	public class When_adding_the_Report_Findings_Data_for_an_Amputee_patient
	{
		private PhysiciansSubReportModel _model;

		private BVATest _test;

		private IIdealsCalcEngineService _calcEngineService;
		private IRuleEngine _ruleEngine;
		private ISettingsManager _settingsManager;

		[TestInitialize]
		public void InitWithGoodVolumeData()
		{
			_calcEngineService = Substitute.For<IIdealsCalcEngineService>();
			_ruleEngine = Substitute.For<IRuleEngine>();
			_settingsManager = Substitute.For<ISettingsManager>();

			_calcEngineService.MaximumHeightInCm.Returns(200);
			_calcEngineService.MinimumHeightInCm.Returns(0);

			_test = new BVATest(_ruleEngine)
			{
				Patient = new BVAPatient(_ruleEngine)
				{
					IsAmputee = true,
					Gender = GenderType.Male,
					WeightInKg = 81.6466,
					HeightInCm = 182.88, 
					DeviationFromIdealWeight = 50
				}
			};

			_test.Volumes[BloodType.Measured].RedCellCount = 2207;
			_test.Volumes[BloodType.Measured].PlasmaCount = 3237;
			_test.Status = TestStatus.Pending;
		}

		[TestMethod]
		public void And_the_ideals_correction_factor_is_0_percent_then_it_is_stated_in_the_report_findings()
		{
			//Arrange
			_test.AmputeeIdealsCorrectionFactor = 0;
			_model = new PhysiciansSubReportModel(_test, _calcEngineService, _settingsManager);

			//Act
			var actual = _model.AddReportFindingsData();

			//Assert
			const string expected = "AMPUTEE PATIENT: Ideal TBV & RCV corrected by 0% as per interpreting physician guidance.";
			Assert.IsTrue(actual.StartsWith(expected));
		}

		[TestMethod]
		public void And_the_ideals_correction_factor_is_not_0_then_it_is_stated_in_the_report_findings_with_a_negative_sign()
		{
			//Arrange
			_test.AmputeeIdealsCorrectionFactor = 9;
			_model = new PhysiciansSubReportModel(_test, _calcEngineService, _settingsManager);

			//Act
			var actual = _model.AddReportFindingsData();

			//Assert
			const string expected = "AMPUTEE PATIENT: Ideal TBV & RCV corrected by -9% as per interpreting physician guidance.";
			Assert.IsTrue(actual.StartsWith(expected));
		}
	}
}
