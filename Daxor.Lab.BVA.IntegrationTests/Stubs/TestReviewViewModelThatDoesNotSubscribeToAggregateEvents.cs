﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.IntegrationTests.Stubs
{
    public class TestReviewViewModelThatDoesNotSubscribeToAggregateEvents : TestReviewViewModel
    {
        public TestReviewViewModelThatDoesNotSubscribeToAggregateEvents(ISettingsManager settingsManager)
            : base(MockRepository.GenerateStub<IAppModuleNavigator>(), 
                   MockRepository.GenerateStub<IBloodVolumeTestController>(), 
                   MockRepository.GenerateStub<ITestExecutionController>(),
                   MockRepository.GenerateStub<IMessageBoxDispatcher>(), 
                   MockRepository.GenerateStub<IAuditTrailDataAccess>(), 
                   settingsManager,
                   MockRepository.GenerateStub<IMessageManager>(), 
                   MockRepository.GenerateStub<ILoggerFacade>(), 
                   null, 
                   MockRepository.GenerateStub<IEventAggregator>())
        {
        }

        protected override void SubscribeToAggregateEvents()
        {
        }
    }
}
