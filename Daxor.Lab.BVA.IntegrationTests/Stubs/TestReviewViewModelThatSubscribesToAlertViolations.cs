﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.SubViews;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Database.Interfaces;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.IntegrationTests.Stubs
{
    public class TestReviewViewModelThatSubscribesToAlertViolations : TestReviewViewModel
    {
        public TestReviewViewModelThatSubscribesToAlertViolations(IBloodVolumeTestController controller, IMessageBoxDispatcher dispatcher, 
            ISettingsManager settingsManager, IEventAggregator eventAggregator, IMessageManager messageManager)
            : base(MockRepository.GenerateStub<IAppModuleNavigator>(), controller, MockRepository.GenerateStub<ITestExecutionController>(), dispatcher, MockRepository.GenerateStub<IAuditTrailDataAccess>(), settingsManager,
            messageManager, MockRepository.GenerateStub<ILoggerFacade>(), null, eventAggregator)
        {
        }

        public TestReviewViewModelThatSubscribesToAlertViolations(IBloodVolumeTestController controller, IMessageBoxDispatcher dispatcher,
    ISettingsManager settingsManager, IEventAggregator eventAggregator, IMessageManager messageManager, INavigationCoordinator navigationCoordinator)
            : base(MockRepository.GenerateStub<IAppModuleNavigator>(), controller, MockRepository.GenerateStub<ITestExecutionController>(), dispatcher, MockRepository.GenerateStub<IAuditTrailDataAccess>(), settingsManager,
            messageManager, MockRepository.GenerateStub<ILoggerFacade>(), navigationCoordinator, eventAggregator)
        {
        }

        public bool IsActiveKeyboardClosed { get; private set; }

        protected override void InitializeViewModel()
        {
            SubscribeToAlertViolationChangedEvent();
            InitializeDelegateCommands();
        }

        protected override void CloseActiveKeyboard()
        {
            IsActiveKeyboardClosed = true;
        }

        protected override VerifyTestSetupContent CreateVerifyTestSetupContent()
        {
            return null;
        }
    }
}
