﻿using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.BVA.ViewModels;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Presentation.Events;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.BVA.IntegrationTests.Stubs
{
    public class TestSelectionViewModelThatDoesNotConfigureViewModel : TestSelectionViewModel
    {
        public TestSelectionViewModelThatDoesNotConfigureViewModel(IAppModuleNavigator localNavigator, ITestExecutionController testExecutionController,
                                      IEventAggregator eventAggregator, IMessageBoxDispatcher messageBoxDispatcher,
                                      IBloodVolumeTestController testController,
                                      IUnityContainer container, ILoggerFacade logger, IPatientSpecificFieldCache patientSpecificFieldCache,
                                      ISettingsManager settingsManager, IMessageManager messageManager)
            : base(localNavigator, testExecutionController, eventAggregator, messageBoxDispatcher, testController, container, logger, patientSpecificFieldCache, settingsManager, messageManager)
        {
        }

        protected override void ConfigureViewModel()
        {
        }

        protected override void SubscribeToBvaTestSavedEvent()
        {
            EventAggregator.GetEvent<BvaTestSaved>().Subscribe(OnBvaTestSaved, ThreadOption.PublisherThread, false);
        }
    }
}
