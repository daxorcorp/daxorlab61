using System;
using System.Collections.ObjectModel;
using System.Linq;
using Daxor.Lab.BVA.Common.SearchingAndFiltering;
using Daxor.Lab.BVA.IntegrationTests.Stubs;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.BVA.Interfaces.Events;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;
using Telerik.Windows.Data;

namespace Daxor.Lab.BVA.IntegrationTests.ViewModel_Tests.TestSelectionViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_the_running_test_is_saved
    {
        private EventAggregator _eventAggregator;
        private IBVADataService _stubBvaDataService;
        private ISettingsManager _stubSettingsManager;
        private ITestExecutionController _stubTestExecutionController;
        private IBloodVolumeTestController _stubBloodVolumeTestController;

        [TestInitialize]
        public void TestInitialize()
        {
            _eventAggregator = new EventAggregator();
            _stubBvaDataService = MockRepository.GenerateStub<IBVADataService>();
            _stubBvaDataService.Expect(s => s.AreBvaTestTablesAreOnline()).Return(true);
            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _stubTestExecutionController = MockRepository.GenerateStub<ITestExecutionController>();
            
            _stubBloodVolumeTestController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            _stubBloodVolumeTestController.Expect(c => c.DataService).Return(_stubBvaDataService);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_test_was_not_already_in_the_test_list_then_the_running_test_is_added_to_the_test_list()
        {
            var testItemToReturn = new BVATestItem
            {
                TestID = Guid.NewGuid()
            };

            var expectedTestList = new QueryableCollectionView(new ObservableCollection<BVATestItem> { testItemToReturn });

            _stubBvaDataService.Expect(x => x.SelectBVATestItem(testItemToReturn.TestID)).Return(testItemToReturn);

            var vm = new TestSelectionViewModelThatDoesNotConfigureViewModel(null, _stubTestExecutionController, _eventAggregator, null, 
                                                                _stubBloodVolumeTestController, null, null, null, _stubSettingsManager, null)
            {
                TestInfoItems = new QueryableCollectionView(new ObservableCollection<BVATestItem>())
            };

            // act
            _eventAggregator.GetEvent<BvaTestSaved>().Publish(testItemToReturn.TestID);
            
            Assert.AreEqual(expectedTestList.Cast<BVATestItem>().FirstOrDefault(), vm.TestInfoItems.Cast<BVATestItem>().FirstOrDefault());
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_test_was_not_already_in_the_test_list_then_the_selected_test_becomes_the_running_test()
        {
            var testItemToReturn = new BVATestItem
            {
                TestID = Guid.NewGuid()
            };

            _stubBvaDataService.Expect(x => x.SelectBVATestItem(testItemToReturn.TestID)).Return(testItemToReturn);

            var vm = new TestSelectionViewModelThatDoesNotConfigureViewModel(null, _stubTestExecutionController, _eventAggregator, 
                                                    null, _stubBloodVolumeTestController, null, null, null, _stubSettingsManager, null)
            {
                TestInfoItems = new QueryableCollectionView(new ObservableCollection<BVATestItem>())
            };

            // act
            _eventAggregator.GetEvent<BvaTestSaved>().Publish(testItemToReturn.TestID);

            Assert.AreEqual(testItemToReturn, vm.SelectedTestItem);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_it_is_already_in_the_test_list_then_the_information_is_synched_with_what_is_in_the_database()
        {
            var existingTestItem = new BVATestItem
            {
                TestID = Guid.NewGuid()
            };

            var savedTestItem = new BVATestItem
            {
                CreatedDate = DateTime.Now,
                HasSufficientData = true,
                PatientDOB = new DateTime(1981, 4, 26),
                PatientFullName = "Doober Do",
                PatientHospitalID = "1234",
                PatientPID = Guid.NewGuid(),
                TestID = existingTestItem.TestID,
                TestID2 = "soi soi soi",
                TestModeDescription = "Can Haz Info",
                Status = TestStatus.Running,
                TestStatusDescription = "Running",
                WasSaved = true,
            };

            _stubBvaDataService.Expect(x => x.SelectBVATestItem(savedTestItem.TestID)).Return(savedTestItem);

            var vm = new TestSelectionViewModelThatDoesNotConfigureViewModel(null, _stubTestExecutionController, _eventAggregator,
                                                    null, _stubBloodVolumeTestController, null, null, null, _stubSettingsManager, null)
            {
                TestInfoItems = new QueryableCollectionView(new ObservableCollection<BVATestItem>{existingTestItem})
            };

            // act
            _eventAggregator.GetEvent<BvaTestSaved>().Publish(savedTestItem.TestID);

            Assert.AreEqual(1, vm.TestInfoItems.ItemCount);
            Assert.IsTrue(AreBvaTestItemsEqual(savedTestItem, vm.TestInfoItems.Cast<BVATestItem>().FirstOrDefault()));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_patient_data_has_changed_then_the_patient_information_is_updated_on_all_other_tests_for_the_patient()
        {
            var patientId = Guid.NewGuid();
            var existingTestItem1 = new BVATestItem
            {
                TestID = Guid.NewGuid(),
                PatientFullName = "Phillip Andreason",
                PatientDOB = new DateTime(1956,5,1),
                PatientHospitalID = "9999",
                PatientPID = patientId
            };

            var existingTestItem2 = new BVATestItem
            {
                TestID = Guid.NewGuid(),
                PatientFullName = "Phillip Andreason",
                PatientDOB = new DateTime(1956, 5, 1),
                PatientHospitalID = "9999",
                PatientPID = patientId
            };

            var savedTestItem = new BVATestItem
            {
                CreatedDate = DateTime.Now,
                HasSufficientData = true,
                PatientDOB = new DateTime(1981, 4, 26),
                PatientFullName = "Doober Do",
                PatientHospitalID = "1234",
                PatientPID = patientId,
                TestID = Guid.NewGuid(),
                TestID2 = "soi soi soi",
                TestModeDescription = "Can Haz Info",
                Status = TestStatus.Running,
                TestStatusDescription = "Running",
                WasSaved = true,
            };

            _stubBvaDataService.Expect(x => x.SelectBVATestItem(savedTestItem.TestID)).Return(savedTestItem);

            var vm = new TestSelectionViewModelThatDoesNotConfigureViewModel(null, _stubTestExecutionController, _eventAggregator,
                                                    null, _stubBloodVolumeTestController, null, null, null, _stubSettingsManager, null)
            {
                TestInfoItems = new QueryableCollectionView(new ObservableCollection<BVATestItem> { existingTestItem1, existingTestItem2 })
            };

            // act
            _eventAggregator.GetEvent<BvaTestSaved>().Publish(savedTestItem.TestID);

            foreach (var test in vm.TestInfoItems.Cast<BVATestItem>())
            {
                Assert.AreEqual(savedTestItem.PatientDOB,test.PatientDOB);
                Assert.AreEqual(savedTestItem.PatientFullName,test.PatientFullName);
                Assert.AreEqual(savedTestItem.PatientHospitalID, test.PatientHospitalID);
            }
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_test_is_filtered_out_the_running_test_is_not_added_to_the_test_list_again()
        {
            var testItemToReturn = new BVATestItem
            {
                TestID = Guid.NewGuid(),
            };

            _stubBvaDataService.Expect(x => x.SelectBVATestItem(testItemToReturn.TestID)).Return(testItemToReturn);

            var vm = new TestSelectionViewModelThatDoesNotConfigureViewModel(null, _stubTestExecutionController, _eventAggregator, null,
                                                                _stubBloodVolumeTestController, null, null, null, _stubSettingsManager, null)
            {
                TestInfoItems = new QueryableCollectionView(new ObservableCollection<BVATestItem> { testItemToReturn })
            };

            var searchFilter = new PredicateFilterDescriptor<BVATestItem> {Predicate = item => false};
            vm.TestInfoItems.FilterDescriptors.Clear();
            vm.TestInfoItems.FilterDescriptors.Add(searchFilter);
            
            // act
            _eventAggregator.GetEvent<BvaTestSaved>().Publish(testItemToReturn.TestID);

            Assert.AreEqual(1, vm.TestInfoItems.SourceCollection.Cast<BVATestItem>().Count());
            Assert.AreEqual(0, vm.TestInfoItems.Cast<BVATestItem>().Count());
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_all_results_are_filtered_out_and_the_patient_data_has_changed_then_the_patient_information_is_updated_on_all_other_tests_for_the_patient()
        {
            var patientId = Guid.NewGuid();
            var existingTestItem1 = new BVATestItem
            {
                TestID = Guid.NewGuid(),
                PatientFullName = "Phillip Andreason",
                PatientDOB = new DateTime(1956, 5, 1),
                PatientHospitalID = "9999",
                PatientPID = patientId
            };

            var existingTestItem2 = new BVATestItem
            {
                TestID = Guid.NewGuid(),
                PatientFullName = "Phillip Andreason",
                PatientDOB = new DateTime(1956, 5, 1),
                PatientHospitalID = "9999",
                PatientPID = patientId
            };

            var savedTestItem = new BVATestItem
            {
                CreatedDate = DateTime.Now,
                HasSufficientData = true,
                PatientDOB = new DateTime(1981, 4, 26),
                PatientFullName = "Doober Do",
                PatientHospitalID = "1234",
                PatientPID = patientId,
                TestID = Guid.NewGuid(),
                TestID2 = "soi soi soi",
                TestModeDescription = "Can Haz Info",
                Status = TestStatus.Running,
                TestStatusDescription = "Running",
                WasSaved = true,
            };

            _stubBvaDataService.Expect(x => x.SelectBVATestItem(savedTestItem.TestID)).Return(savedTestItem);

            var vm = new TestSelectionViewModelThatDoesNotConfigureViewModel(null, _stubTestExecutionController, _eventAggregator,
                                                    null, _stubBloodVolumeTestController, null, null, null, _stubSettingsManager, null)
            {
                TestInfoItems = new QueryableCollectionView(new ObservableCollection<BVATestItem> { existingTestItem1, existingTestItem2 })
            };

            var searchFilter = new PredicateFilterDescriptor<BVATestItem> { Predicate = item => false };
            vm.TestInfoItems.FilterDescriptors.Clear();
            vm.TestInfoItems.FilterDescriptors.Add(searchFilter);

            // act
            _eventAggregator.GetEvent<BvaTestSaved>().Publish(savedTestItem.TestID);

            foreach (var test in vm.TestInfoItems.SourceCollection.Cast<BVATestItem>())
            {
                Assert.AreEqual(savedTestItem.PatientDOB, test.PatientDOB);
                Assert.AreEqual(savedTestItem.PatientFullName, test.PatientFullName);
                Assert.AreEqual(savedTestItem.PatientHospitalID, test.PatientHospitalID);
            }
        }

        private bool AreBvaTestItemsEqual(BVATestItem first, BVATestItem second)
        {
            return first.TestID.Equals(second.TestID) &&
                   first.PatientDOB.Equals(second.PatientDOB) &&
                   string.Equals(first.TestID2, second.TestID2) &&
                   string.Equals(first.PatientHospitalID, second.PatientHospitalID) &&
                   first.PatientPID.Equals(second.PatientPID) &&
                   string.Equals(first.PatientFullName, second.PatientFullName) &&
                   first.Status == second.Status &&
                   string.Equals(first.TestModeDescription, second.TestModeDescription) &&
                   first.HasSufficientData.Equals(second.HasSufficientData) &&
                   first.WasSaved.Equals(second.WasSaved) &&
                   string.Equals(first.TestStatusDescription, second.TestStatusDescription);
        }
    }
    // ReSharper restore InconsistentNaming
}
