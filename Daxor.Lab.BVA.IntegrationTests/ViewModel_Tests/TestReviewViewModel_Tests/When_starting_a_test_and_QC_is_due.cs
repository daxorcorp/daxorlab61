using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.IntegrationTests.Stubs;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Alerts;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.IntegrationTests.ViewModel_Tests.TestReviewViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_starting_a_test_and_QC_is_due
    {
        private IEventAggregator _eventAggregator;
        private IMessageBoxDispatcher _dispatcher;

        private static IBVADataService _dataService;
        private static IBloodVolumeTestController _controller;
        private static ISettingsManager _settingsManager;
        private static INavigationCoordinator _navigationCoordinator;
        private static IMessageManager _messageManager;
        private static Message _stubMessage;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _navigationCoordinator = MockRepository.GenerateStub<INavigationCoordinator>();
            _dataService = MockRepository.GenerateStub<IBVADataService>();
            _controller = MockRepository.GenerateStub<IBloodVolumeTestController>();
            _controller.Expect(x => x.DataService).Return(_dataService);

            _settingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _settingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcLastQcTestPassed)).Return(true);
            _settingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            _messageManager = MockRepository.GenerateStub<IMessageManager>();
            _stubMessage = new Message("BVA_DAILY_QC_DUE",
                "{0} QC is due at this time. Do you want to run {0} QC now or continue with this Blood Volume Analysis?",
                "Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", "BVA 51",
                MessageType.Alert);
            _messageManager.Expect(m => m.GetMessage(MessageKeys.BvaDailyQCDue))
                .Return(_stubMessage);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _eventAggregator = new EventAggregator();

            _dispatcher = MockRepository.GenerateMock<IMessageBoxDispatcher>();
            _dispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Error, null, null, new string[] { }))
                .IgnoreArguments()
                .Return(0).Repeat.Once();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_QC_due_message_is_obtained_from_the_message_manager()
        {
            var mockMessageManager = MockRepository.GenerateMock<IMessageManager>();
            mockMessageManager.Expect(m => m.GetMessage(MessageKeys.BvaDailyQCDue))
                .Return(new Message("whatever", "whatever", "{0} whatever", "whatever", MessageType.Alert));

            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, mockMessageManager, _navigationCoordinator)
            {
                CurrentTest = new BVATest(null)
            };
            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload(AlertResolutionPayload.ContaminationQCAlert));

            viewModel.StartTestCommand.Execute(null);

            mockMessageManager.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_full_QC_is_due_then_the_correct_message_is_shown_to_the_user()
        {
            var warningMessage = String.Format(_stubMessage.FormattedMessage, "Full");
            string[] messageBoxChoices = {"Run Full QC", "Continue"};

            _dispatcher.Expect(d => d.ShowMessageBox(MessageBoxCategory.Warning, warningMessage, "Full QC Due", messageBoxChoices)).Return(0);

            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager, _navigationCoordinator)
            {
                CurrentTest = new BVATest(null)
            };
            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload(AlertResolutionPayload.FullQCAlert));

            viewModel.StartTestCommand.Execute(null);

            _dispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_standards_QC_is_due_then_the_correct_message_is_shown_to_the_user()
        {
            var warningMessage = String.Format(_stubMessage.FormattedMessage, "Standards");
            string[] messageBoxChoices = { "Run Standards QC", "Continue" };

            _dispatcher.Expect(d => d.ShowMessageBox(MessageBoxCategory.Warning, warningMessage, "Standards QC Due", messageBoxChoices)).Return(0);

            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager, _navigationCoordinator)
            {
                CurrentTest = new BVATest(null)
            };
            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload(AlertResolutionPayload.StandardsQCAlert));

            viewModel.StartTestCommand.Execute(null);

            _dispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_linearity_QC_is_due_then_the_correct_message_is_shown_to_the_user()
        {
            var warningMessage = String.Format(_stubMessage.FormattedMessage, "Linearity");
            string[] messageBoxChoices = { "Run Linearity QC", "Continue" };

            _dispatcher.Expect(d => d.ShowMessageBox(MessageBoxCategory.Warning, warningMessage, "Linearity QC Due", messageBoxChoices)).Return(0);

            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager, _navigationCoordinator)
            {
                CurrentTest = new BVATest(null)
            };
            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload(AlertResolutionPayload.LinearityQCAlert));

            viewModel.StartTestCommand.Execute(null);

            _dispatcher.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_contamination_QC_is_due_then_the_correct_message_is_shown_to_the_user()
        {
            var warningMessage = String.Format(_stubMessage.FormattedMessage, "Contamination");
            string[] messageBoxChoices = { "Run Contamination QC", "Continue" };

            _dispatcher.Expect(d => d.ShowMessageBox(MessageBoxCategory.Warning, warningMessage, "Contamination QC Due", messageBoxChoices)).Return(0);

            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager, _navigationCoordinator)
            {
                CurrentTest = new BVATest(null)
            };
            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload(AlertResolutionPayload.ContaminationQCAlert));

            viewModel.StartTestCommand.Execute(null);

            _dispatcher.VerifyAllExpectations();
        }

        private static AlertViolationPayload CreatePayload(string type)
        {
            return new AlertViolationPayload(Guid.NewGuid()) { Resolution = type };
        }
    }
    // ReSharper restore InconsistentNaming
}
