﻿using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.IntegrationTests.Stubs;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Alerts;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.IntegrationTests.ViewModel_Tests.TestReviewViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_starting_a_test_and_QC_is_due_and_the_user_decides_to_run_the_BV_test
    {
        private IEventAggregator _eventAggregator;
        private IMessageBoxDispatcher _dispatcher;
        
        private static IMessageManager _messageManager;
        private static IBVADataService _dataService;
        private static IBloodVolumeTestController _controller;
        private static ISettingsManager _settingsManager;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _dataService = MockRepository.GenerateStub<IBVADataService>();
            _controller = MockRepository.GenerateStub<IBloodVolumeTestController>();
            _controller.Expect(x => x.DataService).Return(_dataService);

            _messageManager = MockRepository.GenerateStub<IMessageManager>();
            _messageManager.Expect(x => x.GetMessage(null))
                .IgnoreArguments()
                .Return(new Message("stuff", "n", "stuff", "stuff", MessageType.Undefined));

            _settingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _settingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcLastQcTestPassed)).Return(true);
            _settingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _eventAggregator = new EventAggregator();
            
            _dispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            _dispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Error, null, null, new string[] { }))
                .IgnoreArguments()
                .Return(0).Repeat.Once();
            _dispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Error, null, null, new string[] { }))
               .IgnoreArguments()
               .Return(1);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_QC_due_is_contamination_then_the_test_comments_are_set_correctly()
        {
            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager)
            {
                CurrentTest = new BVATest(null)
            };
            
            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload(AlertResolutionPayload.ContaminationQCAlert));

            viewModel.StartTestCommand.Execute(null);

            Assert.AreEqual(viewModel.CurrentTest.Comments, "Contamination QC due notice overridden by the analyst.");
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_QC_due_is_linearity_then_the_test_comments_are_set_correctly()
        {
            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager)
            {
                CurrentTest = new BVATest(null)
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload(AlertResolutionPayload.LinearityQCAlert));

            viewModel.StartTestCommand.Execute(null);

            Assert.AreEqual(viewModel.CurrentTest.Comments, "Linearity QC due notice overridden by the analyst.");
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_QC_due_is_standards_only_then_the_test_comments_are_set_correctly()
        {
            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager)
            {
                CurrentTest = new BVATest(null)
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload(AlertResolutionPayload.StandardsQCAlert));

            viewModel.StartTestCommand.Execute(null);

            Assert.AreEqual(viewModel.CurrentTest.Comments, "Standards QC due notice overridden by the analyst.");
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_QC_due_is_full_then_the_test_comments_are_set_correctly()
        {
            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager)
            {
                CurrentTest = new BVATest(null)
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload(AlertResolutionPayload.FullQCAlert));

            viewModel.StartTestCommand.Execute(null);

            Assert.AreEqual(viewModel.CurrentTest.Comments, "Full QC due notice overridden by the analyst.");
        }

        private static AlertViolationPayload CreatePayload(string type)
        {
            return new AlertViolationPayload(Guid.NewGuid()) { Resolution = type };
        }
    }
    // ReSharper restore InconsistentNaming
}
