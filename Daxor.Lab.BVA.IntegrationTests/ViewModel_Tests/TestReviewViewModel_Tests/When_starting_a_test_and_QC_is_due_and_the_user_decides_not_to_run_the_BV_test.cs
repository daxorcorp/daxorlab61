using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.IntegrationTests.Stubs;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.Alerts;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.IntegrationTests.ViewModel_Tests.TestReviewViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_starting_a_test_and_QC_is_due_and_the_user_decides_not_to_run_the_BV_test
    {
        private IEventAggregator _eventAggregator;
        private IMessageBoxDispatcher _dispatcher;

        private static IMessageManager _messageManager;
        private static IBVADataService _dataService;
        private static IBloodVolumeTestController _controller;
        private static ISettingsManager _settingsManager;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _dataService = MockRepository.GenerateStub<IBVADataService>();
            _controller = MockRepository.GenerateStub<IBloodVolumeTestController>();
            _controller.Expect(x => x.DataService).Return(_dataService);

            _messageManager = MockRepository.GenerateStub<IMessageManager>();
            _messageManager.Expect(x => x.GetMessage(null))
                .IgnoreArguments()
                .Return(new Message("stuff", "n", "stuff", "stuff", MessageType.Undefined));

            _settingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _settingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcLastQcTestPassed)).Return(true);
            _settingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _eventAggregator = new EventAggregator();

            _dispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            _dispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Error, null, null, new string[] {}))
                .IgnoreArguments()
                .Return(0);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_test_comments_remain_unmodified()
        {
            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager,
                MockRepository.GenerateStub<INavigationCoordinator>())
            {
                CurrentTest = new BVATest(null)
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload("whatever"));

            viewModel.StartTestCommand.Execute(null);

            Assert.AreEqual(viewModel.CurrentTest.Comments, String.Empty);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_test_is_saved()
        {
            var currentTest = new BVATest(null);

            var mockDataService = MockRepository.GenerateMock<IBVADataService>();
            mockDataService.Expect(s => s.SaveTest(currentTest));

            var stubBvTestController = MockRepository.GenerateStub<IBloodVolumeTestController>();
            stubBvTestController.Expect(c => c.DataService).Return(mockDataService);

            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(stubBvTestController, _dispatcher, _settingsManager, _eventAggregator, _messageManager,
                MockRepository.GenerateStub<INavigationCoordinator>())
            {
                CurrentTest = currentTest
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload("whatever"));

            viewModel.StartTestCommand.Execute(null);

            mockDataService.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_busy_indicator_is_disabled()
        {
            var isBusy = true;

            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager,
                MockRepository.GenerateStub<INavigationCoordinator>())
            {
                CurrentTest = new BVATest(null)
            };
            _eventAggregator.GetEvent<BusyStateChanged>().Subscribe(e => isBusy = e.IsBusy);

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload("whatever"));

            viewModel.StartTestCommand.Execute(null);

            Assert.IsFalse(isBusy);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_user_is_navigated_to_the_QC_module()
        {
            const string activeAppModuleKey = "whatever";

            var mockNavCoordinator = MockRepository.GenerateMock<INavigationCoordinator>();
            mockNavCoordinator.Expect(c => c.ActiveAppModuleKey).Return(activeAppModuleKey);
            mockNavCoordinator.Expect(c => c.ActivateAppModule(AppModuleIds.QualityControl, null, activeAppModuleKey)).Return(true);

            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager,
               mockNavCoordinator)
            {
                CurrentTest = new BVATest(null)
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload("whatever"));

            viewModel.StartTestCommand.Execute(null);

            mockNavCoordinator.VerifyAllExpectations();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_active_keyboard_is_closed()
        {
            const string activeAppModuleKey = "whatever";

            var mockNavCoordinator = MockRepository.GenerateMock<INavigationCoordinator>();
            mockNavCoordinator.Expect(c => c.ActiveAppModuleKey).Return(activeAppModuleKey);
            mockNavCoordinator.Expect(c => c.ActivateAppModule(AppModuleIds.QualityControl, null, activeAppModuleKey)).Return(true);

            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager,
               mockNavCoordinator)
            {
                CurrentTest = new BVATest(null)
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(CreatePayload("whatever"));

            viewModel.StartTestCommand.Execute(null);

            Assert.IsTrue(viewModel.IsActiveKeyboardClosed);
        }

        private static AlertViolationPayload CreatePayload(string type)
        {
            return new AlertViolationPayload(Guid.NewGuid()) { Resolution = type };
        }
    }
    // ReSharper restore InconsistentNaming
}
