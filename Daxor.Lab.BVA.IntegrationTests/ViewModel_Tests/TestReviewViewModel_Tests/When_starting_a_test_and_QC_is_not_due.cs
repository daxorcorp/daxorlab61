using System;
using Daxor.Lab.BVA.Core;
using Daxor.Lab.BVA.IntegrationTests.Stubs;
using Daxor.Lab.BVA.Interfaces;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.BVA.IntegrationTests.ViewModel_Tests.TestReviewViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_starting_a_test_and_QC_is_not_due
    {
        private IEventAggregator _eventAggregator;
        private IMessageBoxDispatcher _dispatcher;

        private static IMessageManager _messageManager;
        private static IBVADataService _dataService;
        private static IBloodVolumeTestController _controller;
        private static ISettingsManager _settingsManager;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _dataService = MockRepository.GenerateStub<IBVADataService>();
            _controller = MockRepository.GenerateMock<IBloodVolumeTestController>();
            _controller.Expect(x => x.DataService).Return(_dataService);

            _messageManager = MockRepository.GenerateStub<IMessageManager>();
            _messageManager.Expect(x => x.GetMessage(null))
                .IgnoreArguments()
                .Return(new Message("stuff", "n", "stuff", "stuff", MessageType.Undefined));

            _settingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _settingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcLastQcTestPassed)).Return(true);
            _settingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(false);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _eventAggregator = new EventAggregator();

            _dispatcher = MockRepository.GenerateStub<IMessageBoxDispatcher>();
            _dispatcher.Expect(x => x.ShowMessageBox(MessageBoxCategory.Error, null, null, new string[] { }))
                .IgnoreArguments()
                .Return(0);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_test_comments_remain_unchanged()
        {
            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager,
    MockRepository.GenerateStub<INavigationCoordinator>())
            {
                CurrentTest = new BVATest(null)
            };

            viewModel.StartTestCommand.Execute(null);

            Assert.AreEqual(viewModel.CurrentTest.Comments, String.Empty);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_test_starts()
        {
            _controller.Expect(x => x.StartSelectedTest());
            
            var viewModel = new TestReviewViewModelThatSubscribesToAlertViolations(_controller, _dispatcher, _settingsManager, _eventAggregator, _messageManager,
    MockRepository.GenerateStub<INavigationCoordinator>())
            {
                CurrentTest = new BVATest(null)
            };

            viewModel.StartTestCommand.Execute(null);

            _controller.VerifyAllExpectations();
        }
    }
    // ReSharper restore InconsistentNaming
}
