﻿namespace Daxor.Lab.Domain.Interfaces
{
    /// <summary>
    /// Allows an object to control its configuration.
    /// </summary>
    public interface IConfigurable
    {
        /// <summary>
        /// Saves the configuration.
        /// </summary>
        void SaveConfiguration();

        /// <summary>
        /// Resets the configuration.
        /// </summary>
        void ResetConfiguration();
    }
}
