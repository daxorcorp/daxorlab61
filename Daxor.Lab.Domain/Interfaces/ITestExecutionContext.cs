﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;

namespace Daxor.Lab.Domain.Interfaces
{
    /// <summary>
    /// Provides a context for executing (i.e., processing) of samples on a test
    /// </summary>
    public interface ITestExecutionContext
    {
        #region Properties

        /// <summary>
        /// Gets the ISample that represents the background radiation sample
        /// </summary>
        ISample BackgroundSample { get; }
        
        /// <summary>
        /// Gets the executing test
        /// </summary>
        ITest ExecutingTest { get; }

        /// <summary>
        /// Gets the ID of the test associated with this execution context
        /// </summary>
        Guid ExecutingTestId { get; }
        
        /// <summary>
        /// Gets or sets the status of the executing test
        /// </summary>
        TestStatus ExecutingTestStatus { get; set; }
        
        /// <summary>
        /// Gets the execution context specific metadata
        /// </summary>
        object Metadata { get; }

        /// <summary>
        /// Gets the collection of samples (excluding the background sample), 
        /// ordered by how they should be counted during the test
        /// </summary>
        IEnumerable<ISample> OrderedSamplesExcludingBackground { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Provides the next sample to execute
        /// </summary>
        /// <returns>The sample to execute</returns>
        ISample GetNextSampleToExecute();

        /// <summary>
        /// Performs acquisition of currently running sample and invokes the given delegate
        /// once the acquisition has been completed
        /// </summary>
        /// <param name="sample">Sample to acquire</param>
        /// <param name="acquisitionCompletedCallback">Delegate to invoke once acquisition has completed</param>
        void PerformAcquisition(ISample sample, Action<ISample> acquisitionCompletedCallback);

        /// <summary>
        /// Processes the background position given the counts, acquisition time, count status, 
        /// backgroudnd spectrum, and override status
        /// </summary>
        /// <param name="counts">Counts obtained for the background level</param>
        /// <param name="acqusitionTimeInSeconds">Acquisition time (in seconds)</param>
        /// <param name="isAcquisitionCompleted">Whether or not background acquisition has finished</param>
        /// <param name="spectrum">Background spectrm</param>
        /// <param name="isOverridden">Whether the background value is overridden</param>
        void ProcessBackground(int counts, int acqusitionTimeInSeconds, bool isAcquisitionCompleted, Spectrum spectrum = null, bool isOverridden = false);
        
        /// <summary>
        /// Computes the total acquisition time for a given sample
        /// </summary>
        /// <param name="sample">Sample whose acquisition time is to be computed</param>
        /// <returns>Acquisition time (in milliseconds) for the given sample</returns>
        int ComputeSampleAcquisitionTimeInMilliseconds(ISample sample);

        #endregion
    }
}
