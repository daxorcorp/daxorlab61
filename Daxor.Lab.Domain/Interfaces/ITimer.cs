﻿using System.Timers;

namespace Daxor.Lab.Domain.Interfaces
{
    /// <summary>
    /// Represents behavior of a simple timer
    /// </summary>
    public interface ITimer
    {
        /// <summary>
        /// Occurs when the time interval elapses
        /// </summary>
        event ElapsedEventHandler Elapsed;

        /// <summary>
        /// Gets or sets a value indicating whether the timer is enabled or not
        /// </summary>
        bool Enabled { get; set; }
        
        /// <summary>
        /// Starts the timer
        /// </summary>
        void Start();

        /// <summary>
        /// Stops the timer
        /// </summary>
        void Stop();
    }
}
