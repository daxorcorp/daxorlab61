﻿using System;
using Daxor.Lab.Domain.Entities;

namespace Daxor.Lab.Domain.Interfaces
{
    #region Delegates

    /// <summary>
    /// Represents the method that will handle the AcquisitionCompleted event.
    /// </summary>
    /// <param name="mcaId">Identifier of the MCA that completed its acquisition</param>
    /// <param name="spectrum">Spectrum that was acquired</param>
    public delegate void AcquisitionCompletedHandler(string mcaId, Spectrum spectrum);

    /// <summary>
    /// Represents the method that will handle the AcquisitionStarted event.
    /// </summary>
    /// <param name="mcaId">Identifier of the MCA that started acquiring</param>
    /// <param name="startTime">Time at which acquisition started</param>
    public delegate void AcquisitionStartedHandler(string mcaId, DateTime startTime);

    /// <summary>
    /// Represents the method that will handle the AcquisitionStopped event.
    /// </summary>
    /// <param name="mcaId">Identifier for the MCA that stopped acquiring</param>
    /// <param name="stopTime">Time at which acquisition stopped</param>
    public delegate void AcquisitionStoppedHandler(string mcaId, DateTime stopTime);

    /// <summary>
    /// Represents the method that will handle the Connected event.
    /// </summary>
    /// <param name="mcaId">Identifier of the MCA associated that connected</param>
    public delegate void DetectorConnectedHandler(string mcaId);

    /// <summary>
    /// Represents the method that will handle the Disconnected event.
    /// </summary>
    /// <param name="mcaId">Identifier of the MCA that was disconnected</param>
    /// <param name="reason">Reason the detector was disconnected</param>
    public delegate void DetectorDisconnectedHandler(string mcaId, string reason);

    #endregion

    /// <summary>
    /// Exposes delegates, events, properties, and methods that define the behavior of a 
    /// radiation decay detector.
    /// </summary>
    public interface IDetector
    {
        #region Events

        /// <summary>
        /// Occurs when the spectrum acquisition has completed.
        /// </summary>
        event AcquisitionCompletedHandler AcquisitionCompleted;

        /// <summary>
        /// Occurs when spectrum acquisition has started.
        /// </summary>
        event AcquisitionStartedHandler AcquisitionStarted;

        /// <summary>
        /// Occurs when spectrum acquisition has stopped.
        /// </summary>
        event AcquisitionStoppedHandler AcquisitionStopped;

        /// <summary>
        /// Occurs when the detector is connected to the computer.
        /// </summary>
        event DetectorConnectedHandler Connected;

        /// <summary>
        /// Occurs when the detector has been disconnected from the computer.
        /// </summary>
        event DetectorDisconnectedHandler Disconnected;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the detector's analog-to-digital (ADC) current voltage.
        /// </summary>
        double AdcCurrent { get; }

        /// <summary>
        /// Gets or sets the detector's fine gain.
        /// </summary>
        double FineGain { get; set; }

        /// <summary>
        /// Gets a value indicating whether the spectrum acquisition has completed.
        /// </summary>
        bool HasAcquisitionCompleted { get; }

        /// <summary>
        /// Gets a value indicating whether or not the detector is currently acquiring a spectrum.
        /// </summary>
        bool IsAcquiring { get; }

        /// <summary>
        /// Gets a value indicating whether or not the detector is connected.
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// Gets a value indicating whether or not the gross counts for acquisition have been preset.
        /// </summary>
        bool IsGrossCountsPreset { get; }

        /// <summary>
        /// Gets a value indicating whether or not the live time for acquisition has been preset.
        /// </summary>
        bool IsLiveTimePreset { get; }

        /// <summary>
        /// Gets a value indicating whether or not the real time for acquisition has been preset.
        /// </summary>
        bool IsRealTimePreset { get; }

        /// <summary>
        /// Gets or sets the detector's low level discriminator value.
        /// </summary>
        int LowLevelDiscriminator { get; set; }

        /// <summary>
        /// Gets or sets the detector's noise value
        /// </summary>
        int Noise { get; set; }

        /// <summary>
        /// Gets the detector's serial number.
        /// </summary>
        int SerialNumber { get; }

        /// <summary>
        /// Gets the detector's current spectrum.
        /// </summary>
        Spectrum Spectrum { get; }

        /// <summary>
        /// Gets the length of the detector's current spectrum (in channels).
        /// </summary>
        int SpectrumLength { get; }

        /// <summary>
        /// Gets or sets the detector's current voltage.
        /// </summary>
        int Voltage { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Connects the detector.
        /// </summary>
        void Connect();

        /// <summary>
        /// Disconnects the detector.
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Resets the detector's current spectrum and resets the time values.
        /// </summary>
        void ResetSpectrum();

        /// <summary>
        /// Starts acquiring a spectrum for a given number of seconds.
        /// </summary>
        /// <param name="timeInSeconds">During of acquisition (in seconds)</param>
        /// <param name="isLiveTime">Whether the live time should be timeInSeconds</param>
        void StartAcquiring(int timeInSeconds, bool isLiveTime = true);

        /// <summary>
        /// Starts acquiring until a given number of gross counts have been obtained
        /// within the specified channel range.
        /// </summary>
        /// <param name="grossCounts">Goal counts to acquire</param>
        /// <param name="startChannel">Starting channel</param>
        /// <param name="endChannel">Ending channel</param>
        void StartAcquiring(uint grossCounts, int startChannel, int endChannel);

        /// <summary>
        /// Stops the spectrum acquisition.
        /// </summary>
        void StopAcquiring();

        #endregion
    }
}
