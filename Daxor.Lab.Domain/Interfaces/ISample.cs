﻿using System;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;

namespace Daxor.Lab.Domain.Interfaces
{
	/// <summary>
	/// Defines the behavior of a sample/specimen to be measured using a detector.
	/// </summary>
	public interface ISample
	{
		/// <summary>
		/// Gets or sets the sample's spectrum data.
		/// </summary>
		Spectrum Spectrum { get; set; }

		/// <summary>
		/// Gets or sets the sample's acquisition preset live time (in seconds).
		/// </summary>
		int PresetLiveTimeInSeconds { get; set; }

		/// <summary>
		/// Gets or sets the sample's user-friendly display name.
		/// </summary>
		string DisplayName { get; set; }

		/// <summary>
		/// Gets or sets the sample's execution status
		/// </summary>
		SampleExecutionStatus ExecutionStatus { get; set; }

		/// <summary>
		/// Gets the sample's internal identifier.
		/// </summary>
		Guid InternalId { get; }

		/// <summary>
		/// Gets or sets the sample's position index.
		/// </summary>
		int Position { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether sample is currently being counted.
		/// by the test executioner
		/// </summary>
		bool IsCounting { get; set; }
	}
}
