﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace Daxor.Lab.Domain.Interfaces
{
    /// <summary>
    /// Defines the behavior of a rule engine
    /// </summary>
    public interface IRuleEngine
    {
        #region Properties

        /// <summary>
        /// Gets a collection of rules that are disabled
        /// </summary>
        INotifyCollectionChanged DisabledRules { get; }

        /// <summary>
        /// Gets a collection of rules that the engine manages
        /// </summary>
        IEnumerable<IRule> Rules { get; }

        #endregion

        #region Events

        /// <summary>
        /// Occurs when the rules in this engine have been evaluated
        /// </summary>
        event EventHandler RulesEvaluated;

        #endregion

        #region Methods

        /// <summary>
        /// Adds a given rule to the rule engine's list of rules
        /// </summary>
        /// <param name="rule">Rule to be added</param>
        void AddRule(IRule rule);

        /// <summary>
        /// Clears all broken rules (i.e., resets the evaluation state) on the given model, or
        /// for the given property name on the given model.
        /// </summary>
        /// <param name="model">Model whose broken rules are to be reset</param>
        /// <param name="propertyName">Name of a property on the given model whose broken
        /// rules are to be reset; null by default</param>
        void ClearBrokenRulesOnModel(IEvaluatable model, string propertyName = null);

        /// <summary>
        /// Disables the given rule on the given model
        /// </summary>
        /// <typeparam name="TModel">Evaulatable model type</typeparam>
        /// <param name="model">Evaulatable model that has the given rule</param>
        /// <param name="rule">Rule to be disabled</param>
        void DisableRuleOnModel<TModel>(TModel model, IRule rule) where TModel : IEvaluatable;
                
        /// <summary>
        /// Evaluates rules of a given type on the given model (or properties on the given model).
        /// </summary>
        /// <typeparam name="TRule">Type of rule to be evalauted</typeparam>
        /// <param name="model">Model to be evaluated</param>
        /// <param name="propertyName">Name of a property on the given model whose rules should 
        /// be evaluated</param>
        /// <param name="stopOnFirstBrokenRule">Whether or not evaluation should cease after the 
        /// first broken rule is encountered; false by default</param>
        void EvaluateRules<TRule>(IEvaluatable model, string propertyName = null, bool stopOnFirstBrokenRule = false) where TRule : IRule;
        
        /// <summary>
        /// Enables the given rule on the given model
        /// </summary>
        /// <typeparam name="TModel">Evaulatable model type</typeparam>
        /// <param name="model">Evaulatable model that has the given rule</param>
        /// <param name="rule">Rule to be enabled</param>
        void EnableRuleOnModel<TModel>(TModel model, IRule rule) where TModel : IEvaluatable;
        
        /// <summary>
        /// Returns a collection of broken rules on the given model (or property of the given model)
        /// </summary>
        /// <param name="model">Evaluatable model that may have broken rules</param>
        /// <param name="propertyName">Name of a property on the given model whose rules may be broken</param>
        /// <returns>Collection of broken rules</returns>
        IEnumerable<IRule> GetBrokenRulesOnModel(IEvaluatable model, string propertyName = null);

        /// <summary>
        /// Returns a collection of disabled rules on the given model
        /// </summary>
        /// <typeparam name="TModel">Evaulatable model type</typeparam>
        /// <param name="model">Evaulatable model that has rules</param>
        /// <returns>Collection of disabled rules on the given model</returns>
        IEnumerable<IRule> GetDisabledRulesOnModel<TModel>(TModel model) where TModel : IEvaluatable;

        /// <summary>
        /// Removes a given rule from the rule engine
        /// </summary>
        /// <param name="rule">Rule to be removed</param>
        /// <returns>True if item is successfully removed; otherwise, false. This method also 
        /// returns false if item was not found in collection of rules</returns>
        bool RemoveRule(IRule rule);
        
        #endregion
    }
}
