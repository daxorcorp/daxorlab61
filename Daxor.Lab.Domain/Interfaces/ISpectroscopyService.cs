﻿using Daxor.Lab.Domain.Entities;

namespace Daxor.Lab.Domain.Interfaces
{
    /// <summary>
    /// Defines the behavior of a spectroscopy service, which is used to analyze a 
    /// given spectrum.
    /// </summary>
    public interface ISpectroscopyService
    {
        /// <summary>
        /// Computes an unweighted sliding average of the given spectrum based
        /// on a specified width.
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be smoothed</param>
        /// <param name="width">Width parameter for smoothing</param>
        /// <returns>Array (spectrum) of computed counts</returns>
        uint[] ComputeAverageSmoothSpectrum(uint[] spectrumArray, int width);

        /// <summary>
        /// Computes the centroid of the given spectrum based on the specified bounds.
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be used for computing the centroid</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The computed centroid</returns>
        double ComputeCentroid(uint[] spectrumArray, int leftBound, int rightBound);
        
        /// <summary>
        /// Computes the full width at half maximum (FWHM) of the given spectrum based
        /// on the specified bounds
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be used for computing the FWHM</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The computed full width at half maximum</returns>
        double ComputeFullWidthHalfMax(uint[] spectrumArray, int leftBound, int rightBound);

        /// <summary>
        /// Computes the full width at tenth maximum (FWTM) of the given spectrum based
        /// on the specified bounds.
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be used for computing the FWTM</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The computed full width at tenth maximum</returns>
        double ComputeFullWidthTenthMax(uint[] spectrumArray, int leftBound, int rightBound);

        /// <summary>
        /// Computes the full width at kth maximum (FWKM) of the given Spectrum based
        /// on the specified fractional height and bounds.
        /// </summary>
        /// <param name="spectrum">Spectrum instance to be used for computing the FWKM</param>
        /// <param name="kmaxFraction">Fractional height of interest</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The computed full width at kth maximum</returns>
        double ComputeFullWidthKMax(Spectrum spectrum, float kmaxFraction, int leftBound, int rightBound);

        /// <summary>
        /// Computes the full width at kth maximum (FWKM) of the given spectrum array based
        /// on the specified fractional height and bounds.
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be used for computing the FWKM</param>
        /// <param name="kmaxFraction">Fractional height of interest</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The computed full width at kth maximum</returns>
        double ComputeFullWidthKMax(uint[] spectrumArray, float kmaxFraction, int leftBound, int rightBound);

        /// <summary>
        /// Computes the full width at kth maximum (FWKM) of the given spectrum based
        /// on the specified fractional height and bounds with the Compton background
        /// removed.
        /// </summary>
        /// <param name="spectrum">Spectrum instance to be used for computing the FWKM</param>
        /// <param name="kmaxFraction">Fractional height of interest</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <param name="background">The background level that has been removed</param>
        /// <returns>The computed full width at kth maximum</returns>
        double ComputeFullWidthKMax(Spectrum spectrum, float kmaxFraction, int leftBound, int rightBound, out double background);
        
        /// <summary>
        /// Computes the full width at kth maximum (FWKM) of the given spectrum based
        /// on the specified fractional height and bounds with the Compton background
        /// removed.
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be used for computing the FWKM</param>
        /// <param name="kmaxFraction">Fractional height of interest</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <param name="background">The background level that has been removed</param>
        /// <returns>The computed full width at kth maximum</returns>
        double ComputeFullWidthKMax(uint[] spectrumArray, float kmaxFraction, int leftBound, int rightBound, out double background);

        /// <summary>
        /// Computes the count integral of a given spectrum within the specified bounds.
        /// </summary>
        /// <param name="spectrum">Spectrum instance to be used for computing the integral</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The count integral</returns>
        ulong ComputeIntegral(Spectrum spectrum, int leftBound, int rightBound);

        /// <summary>
        /// Computes the count integral of a given spectrum within the specified bounds.
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be used for computing the integral</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The count integral</returns>
        ulong ComputeIntegral(uint[] spectrumArray, int leftBound, int rightBound);

        /// <summary>
        /// Removes the Compton continuum between the specified bounds from the given 
        /// spectrum.
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be used for continuum removal</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <param name="continuumFreeSpectrum">Array of counts (spectrum) without the continuum</param>
        /// <returns>The background level that has been removed</returns>
        double RemoveContinuumByStraightLine(uint[] spectrumArray, int leftBound, int rightBound, out double[] continuumFreeSpectrum);
    }
}
