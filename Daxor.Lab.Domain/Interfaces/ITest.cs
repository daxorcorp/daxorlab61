﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.Domain.Interfaces
{
    /// <summary>
    /// Defines properties for a test.
    /// </summary>
    public interface ITest
    {
        /// <summary>
        /// Gets or sets the test analyst's name.
        /// </summary>
        string Analyst { get; set; }

        /// <summary>
        /// Gets or sets the test's background count.
        /// </summary>
        int BackgroundCount { get; set; }

        /// <summary>
        /// Gets or sets the test's background count time (in seconds).
        /// </summary>
        double BackgroundTimeInSeconds { get; set; }

        /// <summary>
        /// Gets or sets the test's comments.
        /// </summary>
        string Comments { get; set; }

        /// <summary>
        /// Gets or sets the date when the test was created (for a pending test) or
        /// the date when the test was completed (for non-pending tests).
        /// </summary>
        DateTime Date { get; set; }

        /// <summary>
        /// Gets the test's internal identifier.
        /// </summary>
        Guid InternalId { get; }

        /// <summary>
        /// Gets or sets the location in which the test was performed.
        /// </summary>
        string Location { get; set; }

        /// <summary>
        /// Gets or sets the the acquisition time (in seconds) for each of
        /// the test's samples.
        /// </summary>
        int SampleDurationInSeconds { get; set; }

        /// <summary>
        /// Gets or sets the test's sample layout ID.
        /// </summary>
        int SampleLayoutId { get; set; }

        /// <summary>
        /// Gets the test's collection of samples.
        /// </summary>
        IEnumerable<ISample> Samples { get; }

        /// <summary>
        /// Gets or sets the test's status.
        /// </summary>
        TestStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the system ID on which the test is run.
        /// </summary>
        string SystemId { get; set; }
    }
}
