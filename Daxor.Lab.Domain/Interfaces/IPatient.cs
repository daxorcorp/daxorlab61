﻿using System;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.Domain.Interfaces
{
    /// <summary>
    /// Defines the behavior of a general patient.
    /// </summary>
    public interface IPatient
    {
        #region Properties

        #region ID-related properties

        /// <summary>
        /// Gets or sets the hospital's patient ID.
        /// </summary>
        string HospitalPatientId { get; set; }

        /// <summary>
        /// Gets the patient's internal identifier.
        /// </summary>
        Guid InternalId { get; }

        #endregion

        /// <summary>
        /// Gets or sets the patient's date of birth.
        /// </summary>
        DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the patient's gender.
        /// </summary>
        GenderType Gender { get; set; }

        /// <summary>
        /// Gets or sets the measurement system in which the height and weight
        /// should be used.
        /// </summary>
        MeasurementSystem MeasurementSystem { get; set; }

		/// <summary>
		/// Gets or sets whether the patient is an amputee
		/// </summary>
		bool IsAmputee { get; set; }

        #region Name-related properties

        /// <summary>
        /// Gets or sets the patient's first name.
        /// </summary>
        string FirstName { get; set; }

        /// <summary>
        /// Gets a formatted version of the patient's full name.
        /// </summary>
        string FullName { get; }

        /// <summary>
        /// Gets or sets the patient's middle name.
        /// </summary>
        string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the patient's last name.
        /// </summary>
        string LastName { get; set; }

        #endregion

        #region Height/weight-related propertes

        /// <summary>
        /// Gets the patient's height value in the current measurement system.
        /// </summary>
        double HeightInCurrentMeasurementSystem { get; }

        /// <summary>
        /// Gets the patient's weight value in the current measurement system.
        /// </summary>
        double WeightInCurrentMeasurementSystem { get; }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Gets the number of years that occur between the patient's
        /// date of birth and the given date.
        /// </summary>
        /// <param name="endDate">Ending date</param>
        /// <returns>Number of years that have passed</returns>
        int? GetAgeInYears(DateTime endDate);

        #endregion
    }
}
