﻿namespace Daxor.Lab.Domain.Interfaces
{
    /// <summary>
    /// Defines the behavior of a calibration curve for a detector.
    /// </summary>
    public interface ICalibrationCurve
    {
        #region Properties

        /// <summary>
        /// Gets or sets the identifier for the calibration curve.
        /// </summary>
        int CurveId { get; set; }

        /// <summary>
        /// Gets the y-offset of the calibration curve.
        /// </summary>
        double Offset { get; }

        /// <summary>
        /// Gets the slope of the calibration curve.
        /// </summary>
        double Slope { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Computes the channel the corresponds to the given energy level (in keV)
        /// </summary>
        /// <param name="energyInKeV">Energy value (in keV)</param>
        /// <returns>Channel corresponding to the given energy level</returns>
        int GetChannel(double energyInKeV);

        /// <summary>
        /// Computes the energy (in keV) that corresponds to the given channel.
        /// </summary>
        /// <param name="channel">Channel number</param>
        /// <returns>Energy level (in keV) corresponding to the given channel</returns>
        double GetEnergy(double channel);

        #endregion
    }
}
