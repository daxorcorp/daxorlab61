﻿using System;
using System.Linq;
using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.Domain.Interfaces
{
    /// <summary>
    /// Defines the behavior for a repository of entities.
    /// </summary>
    /// <remarks>
    /// For more information about this pattern see http://martinfowler.com/eaaCatalog/repository.html
    /// or http://blogs.msdn.com/adonet/archive/2009/06/16/using-repository-and-unit-of-work-patterns-with-entity-framework-4-0.aspx.
    /// Using this interface allows us to ensure the persistence ignorance principle
    /// applies within our domain model.
    /// </remarks>
    /// <typeparam name="TEntity">Type of entity for this repository </typeparam>
    public interface IRepository<TEntity> where TEntity : Entity
    {
        /// <summary>
        /// Get all elements of type {T} in repository
        /// </summary>
        /// <returns>List of selected elements</returns>
        IQueryable<TEntity> SelectAll();

        /// <summary>
        /// Get an element of type {T} in repository
        /// </summary>
        /// <param name="testId">Identifier of the item to select</param>
        /// <returns>Selected elements</returns>
        TEntity Select(Guid testId);

        /// <summary>
        /// Adds entity to the repository
        /// </summary>
        /// <param name="item">Item to save</param>
        void Save(TEntity item);

        /// <summary>
        /// Removes entity from the repository
        /// </summary>
        /// <param name="id">Identifier of the item to delete</param>
        void Delete(Guid id);
    }
}
