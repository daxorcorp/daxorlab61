﻿using System;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Eventing;

namespace Daxor.Lab.Domain.Interfaces
{
    /// <summary>
    /// Defines the behavior of a service capable of measuring a background radiation spectrum.
    /// </summary>
    public interface IBackgroundAcquisitionService
    {
        #region Events

        /// <summary>
        /// Occurs when a good background has been obtained.
        /// </summary>
        event EventHandler<BackgroundAttainedEventArgs> GoodBackgroundAttained;

        /// <summary>
        /// Occurs when the background acquisition service restarts counting because
        /// the background was too high for a certain time interval
        /// </summary>
        event EventHandler RestartedBecauseOfHighBackground;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the background acquisition context.
        /// </summary>
        BackgroundAcquisitionContext Context { get; }

        /// <summary>
        /// Gets a value indicating whether or not the background acquisition service is active.
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// Gets a value indicating whether or not the background count value is overridden with some other value.
        /// </summary>
        bool IsBackgroundOverridden { get; }

        /// <summary>
        /// Gets a value indicating whether or not the current background level (in counts per minute) is too high.
        /// </summary>
        bool IsCurrentCpmTooHigh { get; }

        /// <summary>
        /// Gets the status of the background acquisition service.
        /// </summary>
        BackgroundAcquisitionServiceStatus Status { get; }

        /// <summary>
        /// Gets a value indicating whether or not a good background has been attained.
        /// </summary>
        bool WasGoodBackgroundAttained { get; }

        /// <summary>
        /// Gets the number of seconds the background acquisition service has been running.
        /// </summary>
        int RunningBackgroundExecutionTimeInSeconds { get; }

        #region Current state

        /// <summary>
        /// Gets the current background acquisition time in seconds.
        /// </summary>
        int CurrentAcqusitionTimeInSeconds { get; }

        /// <summary>
        /// Gets the current background rate in counts per minute.
        /// </summary>
        int CurrentCountsPerMinute { get; }

        /// <summary>
        /// Gets the current background rate in counts per second.
        /// </summary>
        double CurrentCountsPerSecond { get; }

        /// <summary>
        /// Gets the current background integral (i.e., counts in the ROI).
        /// </summary>
        int CurrentIntegral { get; }

        /// <summary>
        /// Gets the current spectrum for the background.
        /// </summary>
        Spectrum CurrentSpectrum { get; }

        #endregion

        #region Previous state

        /// <summary>
        /// Gets the previous background acquisition time in seconds.
        /// </summary>
        int? PreviousAcquisitionTimeInSeconds { get; }

        /// <summary>
        /// Gets the previous background rate in counts per minute.
        /// </summary>
        int? PreviousCountsPerMinute { get; }

        /// <summary>
        /// Gets the previous background rate in counts per second.
        /// </summary>
        double? PreviousCountsPerSecond { get; }

        /// <summary>
        /// Gets the previous background integral (i.e., counts in the ROI).
        /// </summary>
        int? PreviousIntegral { get; }

        /// <summary>
        /// Gets the previous spectrum for the background.
        /// </summary>
        Spectrum PreviousSpectrum { get; }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Invalidates the previous background level.
        /// </summary>
        void InvalidatePreviousBackground();

        /// <summary>
        /// Overrides the current background level (in counts per minute) with another value.
        /// </summary>
        /// <param name="newCpm">Overriding background level (in counts per minute)</param>
        void OverrideBackground(int newCpm);

        /// <summary>
        /// Starts the background acquisition service asynchronously.
        /// </summary>
        /// <returns>True if the service started successfully; false otherwise</returns>
        bool StartAsync();

        /// <summary>
        /// Stops the background acquisition service asynchronously.
        /// </summary>
        void StopAsync();

        #endregion
    }
}
