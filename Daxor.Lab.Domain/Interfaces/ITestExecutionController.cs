﻿using System;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Eventing;

namespace Daxor.Lab.Domain.Interfaces
{
    /// <summary>
    /// Represents the behavior of a controller that is responsible for executing a test
    /// </summary>
    public interface ITestExecutionController
    {
        #region Events

        /// <summary>
        /// Occurs when an exception has caused an error
        /// </summary>
        event EventHandler<TestExecutionEventArgs> ErrorOccurred;

        /// <summary>
        /// Occurs when the text execution progress has changed
        /// </summary>
        event EventHandler<TestExecutionProgressChangedEventArgs> ProgressChanged;

        /// <summary>
        /// Occurs when the executing test has been aborted
        /// </summary>
        event EventHandler<TestExecutionEventArgs> TestAborted;

        /// <summary>
        /// Occurs when the executing test has completed
        /// </summary>
        event EventHandler<TestExecutionEventArgs> TestCompleted;

        /// <summary>
        /// Occurs when the executing test has started
        /// </summary>
        event EventHandler<TestExecutionEventArgs> TestStarted;

        #endregion

        #region Properties

        /// <summary>
        /// Gets a value indicating whether or not a test is executing
        /// </summary>
        /// <returns>True if a test is executing; false otherwise</returns>
        bool IsExecuting { get; }

        /// <summary>
        /// Gets the executing test
        /// </summary>
        /// <returns></returns>
        ITest ExecutingTest { get; }

        /// <summary>
        /// Gets the execution context metadata.
        /// </summary>
        object ExecutingContextMetadata { get; }

        /// <summary>
        /// Gets the current progress metadata of execution controller
        /// </summary>
        TestExecutionProgressMetadata ProgressMetadata { get; }

        /// <summary>
        /// Gets whether the detector was disconnected or not.
        /// </summary>
        Boolean DidDetectorBecomeDisconnected { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Aborts the execution of a test
        /// </summary>
        void Abort();
        
        /// <summary>
        /// Starts the execution of a given text context.
        /// </summary>
        /// <param name="context">Context used for executing/processing a test</param>
        void Start(ITestExecutionContext context);

        /// <summary>
        /// Returns type of the currently executing test
        /// </summary>
        /// <returns>Type of the currently executing test</returns>
        Type GetExecutingTestType();

        #endregion
    }
}
