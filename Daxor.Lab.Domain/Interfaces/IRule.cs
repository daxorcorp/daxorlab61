﻿using System;

namespace Daxor.Lab.Domain.Interfaces
{
    /// <summary>
    /// Defines the behavior of a a generic business rule
    /// </summary>
    public interface IRule
    {
        /// <summary>
        /// Gets the message for when rule is broken
        /// </summary>
        string BrokenRuleMessage { get; }

        /// <summary>
        /// Gets the type of entity a given rule applies to
        /// </summary>
        Type EntityType { get; }

        /// <summary>
        /// Gets a value indicating whether or not the rule is enabled
        /// </summary>
        bool IsEnabled { get; }

        /// <summary>
        /// Gets the identifier of the rule
        /// </summary>
        string RuleId { get; }

        /// <summary>
        /// Gets the property used in rule evaluation
        /// </summary>
        string PropertyName { get; }

        /// <summary>
        /// Gets the severity of the rule
        /// </summary>
        string SeverityKind { get; }

        /// <summary>
        /// Evaluates the rule with respect to the given model
        /// </summary>
        /// <param name="model">Model to be evaluated</param>
        /// <returns>True if the rule is satisfied, false if the rule is broken</returns>
        bool Evaluate(object model);
    }
}
