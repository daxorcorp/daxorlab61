﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Daxor.Lab.Domain.Interfaces
{
    /// <summary>
    /// Represents an object that supports evaluation/validation
    /// </summary>
    public interface IEvaluatable : IDataErrorInfo, INotifyPropertyChanged
    {
        #region Properties

        /// <summary>
        /// Gets a collection of broken rules on the object
        /// </summary>
        IEnumerable<IRule> BrokenRules { get; }

        /// <summary>
        /// Gets a collection of disabled rules on the object
        /// </summary>
        IEnumerable<IRule> DisabledRules { get; }
        
        /// <summary>
        /// Gets a value indicating whether evaluation is enabled on the object
        /// </summary>
        bool IsEvaluationEnabled { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Disables the given rule on the object
        /// </summary>
        /// <param name="rule">Rule to be disabled</param>
        void DisableRule(IRule rule);

        /// <summary>
        /// Enables the given rule on the object
        /// </summary>
        /// <param name="rule">Rule to be enabled</param>
        void EnableRule(IRule rule);

        /// <summary>
        /// Evaluates all enabled rules on the object
        /// </summary>
        void Evaluate();

        /// <summary>
        /// Evaluates all enabled rules for the given property(ies) on the object
        /// </summary>
        /// <param name="propertyNames">Properties on the object whose corresponding rules should be evaluated</param>
        void EvaluatePropertyByName(params string[] propertyNames);

        #endregion
    }
}
