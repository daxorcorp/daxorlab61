﻿namespace Daxor.Lab.Domain.Interfaces
{
    #region Delegates

    /// <summary>
    /// Represents the method that will handle the ErrorOccurred event.
    /// </summary>
    /// <param name="errorCode">Error code</param>
    /// <param name="message">Error message</param>
    public delegate void ErrorOccurredHandler(int errorCode, string message);

    /// <summary>
    /// Represents the method that will handle the PositionChangeBegin event.
    /// </summary>
    /// <param name="oldPosition">Old position</param>
    /// <param name="newPosition">New position</param>
    public delegate void PositionChangeBeginHandler(int oldPosition, int newPosition);

    /// <summary>
    /// Represents the method that will handle the PositionChangeEndHandler event.
    /// </summary>
    /// <param name="currPosition">Current position (what the position has changed to)</param>
    public delegate void PositionChangeEndHandler(int currPosition);

    /// <summary>
    /// Represents the method that will handle the PositionSeekBegin event.
    /// </summary>
    /// <param name="currPosition">Current position</param>
    /// <param name="goalPosition">Goal position</param>
    public delegate void PositionSeekBeginHandler(int currPosition, int goalPosition);

    /// <summary>
    /// Represents the method that will handle the PositionSeekCancel event.
    /// </summary>
    /// <param name="currPosition">Current position</param>
    /// <param name="goalPosition">Goal position</param>
    public delegate void PositionSeekCancelHandler(int currPosition, int goalPosition);

    /// <summary>
    /// Represents the method that will handle the PositionSeekEnd event.
    /// </summary>
    /// <param name="currPosition">Current position (what the position has changed to)</param>
    public delegate void PositionSeekEndHandler(int currPosition);

    /// <summary>
    /// Represents the method that will handle the SampleChangerConnected event.
    /// </summary>
    public delegate void SampleChangerConnectedHandler();

    /// <summary>
    /// Represents the method that will handle the SampleChangerDisconnected event.
    /// </summary>
    /// <param name="reason">Reason the sample changer was disconnected</param>
    public delegate void SampleChangerDisconnectedHandler(string reason);

    #endregion

    /// <summary>
    /// Exposes events, properties, and methods that define the behavior of
    /// a sample changer device
    /// </summary>
    public interface ISampleChanger
    {
        #region Events

        /// <summary>
        /// Occurs when the sample changer is connected to the computer
        /// </summary>
        event SampleChangerConnectedHandler Connected;

        /// <summary>
        /// Occurs when the sample changer has been disconnected from the computer
        /// </summary>
        event SampleChangerDisconnectedHandler Disconnected;

        /// <summary>
        /// Occurs when the sample changer encounters an error
        /// </summary>
        event ErrorOccurredHandler ErrorOccurred;

        /// <summary>
        /// Occurs when the sample changer starts changing to another position
        /// </summary>
        event PositionChangeBeginHandler PositionChangeBegin;

        /// <summary>
        /// Occurs when the sample changer has finished changing to a position
        /// </summary>
        event PositionChangeEndHandler PositionChangeEnd;

        /// <summary>
        /// Occurs when the sample changer starts seeking a goal position
        /// </summary>
        event PositionSeekBeginHandler PositionSeekBegin;

        /// <summary>
        /// Occurs when the sample changer has cancelled a seek operation
        /// </summary>
        event PositionSeekCancelHandler PositionSeekCancelled;

        /// <summary>
        /// Occurs when the sample changer has finished seeking a goal position
        /// </summary>
        event PositionSeekEndHandler PositionSeekEnd;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the sample changer's current position
        /// </summary>
        int CurrentPosition { get; }

        /// <summary>
        /// Gets a value indicating whether the sample changer is moving
        /// </summary>
        bool IsMoving { get; }

        /// <summary>
        /// Gets the number of positions the changer is capable of accessing
        /// </summary>
        int NumberOfPositions { get; }

        /// <summary>
        /// Gets the number of milliseconds required to move from one position to
        /// the next position
        /// </summary>
        int PositionChangeDelayInMilliseconds { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Connects the sample changer
        /// </summary>
        void Connect();

        /// <summary>
        /// Computes the number moves required to get from the given starting position to
        /// the given ending position
        /// </summary>
        /// <param name="startPosition">Starting position</param>
        /// <param name="endPosition">Ending position</param>
        /// <returns>Number of moves to get from the starting position to the ending position</returns>
        int GetNumberOfMovesBetween(int startPosition, int endPosition);

        /// <summary>
        /// Disconnects the sample changer
        /// </summary>
        void Disconnect();

        /// <summary>
        /// Moves the sample changer to a given goal position
        /// </summary>
        /// <param name="goalPosition">Goal position</param>
        void MoveTo(int goalPosition);

        /// <summary>
        /// Stops the sample changer
        /// </summary>
        void Stop();

        /// <summary>
        /// Checks to see if the sample changer is receiving power
        /// </summary>
        /// <returns>True if the sample changer is receiving power, false otherwise</returns>
        bool IsPoweredUp();

        #endregion
    }
}
