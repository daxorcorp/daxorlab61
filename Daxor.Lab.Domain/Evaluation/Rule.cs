﻿using System;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.Domain.Evaluation
{
    /// <summary>
    /// Represents a base rule capable of being evaluated with respect to some entity
    /// </summary>
    public abstract class Rule : IRule
    {
        /// <summary>
        /// Initializes a new instance of the Rule class based on the given entity
        /// </summary>
        /// <param name="entity">Type of entity a given rule applies to</param>
        protected Rule(Type entity)
        {
            EntityType = entity;
        }

        /// <summary>
        /// Gets or sets the message for when rule is broken.
        /// </summary>
        public virtual string BrokenRuleMessage
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the rule is enabled.
        /// </summary>
        public bool IsEnabled
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the identifier of the rule.
        /// </summary>
        public string RuleId
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the property used in rule evaluation.
        /// </summary>
        public string PropertyName
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the severity of the rule.
        /// </summary>
        public string SeverityKind
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets or sets the type of entity a given rule applies to
        /// </summary>
        public Type EntityType
        {
            get;
            protected set;
        }

        /// <summary>
        /// Evaluates the rule with respect to the given model
        /// </summary>
        /// <param name="model">Model to be evaluated</param>
        /// <returns>True if the rule is satisfied, false if the rule is broken</returns>
        public abstract bool Evaluate(object model);
    }
}
