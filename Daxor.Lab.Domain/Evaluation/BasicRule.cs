﻿using System;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.Domain.Evaluation
{
    /// <summary>
    /// Represents a rule that corresponds to a specific type of entity
    /// </summary>
    /// <typeparam name="TEntity">Type of entity that this rule evaluates</typeparam>
    public class BasicRule<TEntity> : Rule where TEntity : IEvaluatable
    {
        #region Fields

        /// <summary>
        /// Backing field for the rule evaluation delegate
        /// </summary>
        private readonly Func<TEntity, bool> _evaluator;

        #endregion

        #region Ctors

        /// <summary>
        /// Initializes a new instance of the BasicRule class with an empty property name and broken rule message
        /// </summary>
        public BasicRule() : this(string.Empty, string.Empty)
        {
        }

        /// <summary>
        /// Initializes a new instance of the BasicRule class with the given property name and broken rule message
        /// </summary>
        /// <param name="propertyName">Property used in rule evaluation</param>
        /// <param name="brokenRuleMessage">Message for when rule is broken</param>
        public BasicRule(string propertyName, string brokenRuleMessage) : base(typeof(TEntity))
        {
            PropertyName = propertyName;
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            BrokenRuleMessage = brokenRuleMessage;      // ReSharper restore DoNotCallOverridableMethodsInConstructor
            IsEnabled = true;
        }

        /// <summary>
        /// Initializes a new instance of the BasicRule class with the given property name, broken rule message,
        /// and delegate capable of evaluating the entity
        /// </summary>
        /// <param name="propertyName">Property used in rule evaluation</param>
        /// <param name="brokenRuleMessage">Message for when rule is broken</param>
        /// <param name="evaluator">Delegate capable of evaluating the entity</param>
        public BasicRule(string propertyName, string brokenRuleMessage, Func<TEntity, bool> evaluator)
            : this(propertyName, brokenRuleMessage)
        {
            _evaluator = evaluator;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the delegate capable of evaluating this rule
        /// </summary>
        protected Func<TEntity, bool> Evaluator
        {
            get { return _evaluator; }
        }

        #endregion

        #region Rule overrides

        /// <summary>
        /// Evaluates the rule with respect to the given model using an evaluation delegate
        /// </summary>
        /// <param name="model">Model to be evaluated</param>
        /// <returns>True if the rule is satisfied, or if there is no evaluator; 
        /// false if the rule is broken</returns>
        public override bool Evaluate(object model)
        {
            if (Evaluator == null)
                return true;

            return Evaluator((TEntity)model);
        }

        #endregion
    }
}
