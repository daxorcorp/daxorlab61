﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.Domain.Evaluation
{
    /// <summary>
    /// Represents a rule engine that manages rules for specific evaluatable models and
    /// provides a means of evaluating those rules.
    /// </summary>
    public class RuleEngine : IRuleEngine
    {
        #region Fields

        /// <summary>
        /// Dictionary that provides a list of broken rules for a given evaluatable model
        /// </summary>
        private readonly Dictionary<IEvaluatable, List<IRule>> _brokenRulesForModel = new Dictionary<IEvaluatable, List<IRule>>();

        /// <summary>
        /// Backing field for the collection of disabled rules
        /// </summary>
        private readonly ObservableCollection<IRule> _disabledRules = new ObservableCollection<IRule>();

        /// <summary>
        /// Dictionary that provides a list of disabled rules for a given evaluatable model
        /// </summary>
        private readonly Dictionary<IEvaluatable, List<IRule>> _disabledRulesForModel = new Dictionary<IEvaluatable, List<IRule>>();
        
        /// <summary>
        /// Backing field for the collection of all rules
        /// </summary>
        private readonly ObservableCollection<IRule> _rules = new ObservableCollection<IRule>();
       
        #endregion

        #region IRuleEngine events

        /// <summary>
        /// Occurs when the rules in this engine have been evaluated
        /// </summary>
        public event EventHandler RulesEvaluated;

        #endregion

        #region IRuleEngine properties

        /// <summary>
        /// Gets a collection of rules that are disabled
        /// </summary>
        public INotifyCollectionChanged DisabledRules
        {
            get { return _disabledRules; }
        }

        /// <summary>
        /// Gets a collection of rules that the engine manages
        /// </summary>
        public IEnumerable<IRule> Rules
        {
            get { return _rules; }
        }

        #endregion

        #region IRuleEngine methods

        #region Adding/removing rules

        /// <summary>
        /// Adds a given rule to the rule engine's list of rules; if the rule is disabled, the
        /// list of disabled rules is updated accordingly
        /// </summary>
        /// <param name="rule">Rule to be added</param>
        public virtual void AddRule(IRule rule)
        {
            if (rule == null)
                throw new ArgumentNullException("rule");

             _rules.Add(rule);
            
             // Check if rule already is disabled
             if (rule.IsEnabled == false)
                 _disabledRules.Add(rule);
        }

        /// <summary>
        /// Removes a given rule from the rule engine
        /// </summary>
        /// <param name="rule">Rule to be removed</param>
        /// <returns>True if item is successfully removed; otherwise, false. This method also 
        /// returns false if item was not found in collection of rules</returns>
        public virtual bool RemoveRule(IRule rule)
        {
            if (rule == null)
                throw new ArgumentNullException("rule");

            // Enable the rule so that it can be removed from the disabled rules list
            if (!rule.IsEnabled)
                EnableRules(new[] { rule.RuleId });

            return _rules.Remove(rule);
        }

        #endregion

        #region Evaluation/broken rules

        /// <summary>
        /// Clears all broken rules (i.e., resets the evaluation state) on the given model, or
        /// for the given property name on the given model.
        /// </summary>
        /// <param name="model">Model whose broken rules are to be reset</param>
        /// <param name="propertyName">Name of a property on the given model whose broken
        /// rules are to be reset; null by default</param>
        public void ClearBrokenRulesOnModel(IEvaluatable model, string propertyName = null)
        {
            // Clear all the broken rules for the model
            if (propertyName == null)
            {
                if (_brokenRulesForModel.ContainsKey(model))
                {
                    _brokenRulesForModel[model].Clear();
                    _brokenRulesForModel.Remove(model);
                }
            }
            else
            {
                // Only remove broken rules pertaining to the property
                List<IRule> existingBrokenRuleMetadatas;
                if (_brokenRulesForModel.TryGetValue(model, out existingBrokenRuleMetadatas))
                {
                    var brokenRulesOnProperty = (from e in existingBrokenRuleMetadatas where e.PropertyName == propertyName select e).ToList();
                    foreach (var error in brokenRulesOnProperty)
                        _brokenRulesForModel[model].Remove(error);
                }
            }
        }

        /// <summary>
        /// Evaluates rules of a given type on the given model (or properties on the given model)
        /// </summary>
        /// <typeparam name="TRule">Type of rule to be evalauted</typeparam>
        /// <param name="model">Model to be evaluated</param>
        /// <param name="propertyName">Name of a property on the given model whose rules should 
        /// be evaluated</param>
        /// <param name="stopOnFirstBrokenRule">Whether or not evaluation should cease after the 
        /// first broken rule is encountered; false by default</param>
        public virtual void EvaluateRules<TRule>(IEvaluatable model, string propertyName = null, bool stopOnFirstBrokenRule = false) where TRule : IRule
        {
            if (model == null)
                throw new ArgumentNullException("model");

            // Reset all the rule states on the model (or for its property)
            ClearBrokenRulesOnModel(model, propertyName);
            
            // Do the actual evaluation work
            IEnumerable<IRule> brokenRules = InnerEvaluateRules<TRule>(model, propertyName, stopOnFirstBrokenRule);
           
            // Keep track of any rules that were broken
            AddBrokenRulesForModel(model, brokenRules);

            FireRulesEvaluated();
        }

        /// <summary>
        /// Returns a collection of broken rules on the given model (or property
        /// of the given model)
        /// </summary>
        /// <param name="model">Evaluatable model that may have broken rules</param>
        /// <param name="propertyName">Name of a property on the given model whose rules may be broken</param>
        /// <returns>Collection of broken rules</returns>
        public IEnumerable<IRule> GetBrokenRulesOnModel(IEvaluatable model, string propertyName = null)
        {
            if (model != null)
            {
                if (_brokenRulesForModel.ContainsKey(model))
                {
                    // Looking for all rules on this model
                    if (propertyName == null)
                        return _brokenRulesForModel[model].AsReadOnly();

                    // Looking for a specific property on the model
                    var brokenRulesForProperty = from e in _brokenRulesForModel[model]
                                                 where e.EntityType == model.GetType() && e.PropertyName == propertyName
                                                 select e;
                    return brokenRulesForProperty;
                }
            }

            return Enumerable.Empty<IRule>().ToList().AsReadOnly();
        }

        #endregion

        #region Enabling/disabling rules

        /// <summary>
        /// Disables the given rule on the given model
        /// </summary>
        /// <typeparam name="TModel">Evaulatable model type</typeparam>
        /// <param name="model">Evaulatable model that has the given rule</param>
        /// <param name="rule">Rule to be disabled</param>
        public void DisableRuleOnModel<TModel>(TModel model, IRule rule) where TModel : IEvaluatable
        {
            // ReSharper disable CompareNonConstrainedGenericWithNull
            if (model == null)
                throw new ArgumentNullException("model");
            if (rule == null)
                throw new ArgumentNullException("rule");        // ReSharper restore CompareNonConstrainedGenericWithNull

            List<IRule> existingDisabledRules;

            // Disabling a rule for a model that doesn't have disabled rules for it yet
            if (_disabledRulesForModel.TryGetValue(model, out existingDisabledRules))
            {
                // Don't add a rule that already exists
                if (!existingDisabledRules.Contains(rule))
                {
                    existingDisabledRules.Add(rule);
                    model.EvaluatePropertyByName(rule.PropertyName);    // Re-evaluate the rule
                }
            }
            else
            {
                _disabledRulesForModel.Add(model, new List<IRule>(new[] { rule }));
                model.EvaluatePropertyByName(rule.PropertyName);    // Re-evaluate the rule
            }
        }

        /// <summary>
        /// Enables the given rule on the given model
        /// </summary>
        /// <typeparam name="TModel">Evaulatable model type</typeparam>
        /// <param name="model">Evaulatable model that has the given rule</param>
        /// <param name="rule">Rule to be enabled</param>
        public void EnableRuleOnModel<TModel>(TModel model, IRule rule) where TModel : IEvaluatable
        {
            // ReSharper disable CompareNonConstrainedGenericWithNull
            if (model == null)
                throw new ArgumentNullException("model");
            if (rule == null)
                throw new ArgumentNullException("rule");    // ReSharper restore CompareNonConstrainedGenericWithNull

            List<IRule> existingDisabledRules;

            // Only do something if there are disabled rules for this model
            if (_disabledRulesForModel.TryGetValue(model, out existingDisabledRules))
            {
                // Only do something if this rule is in the list of disabled rules
                if (existingDisabledRules.Contains(rule))
                {
                    existingDisabledRules.Remove(rule);

                    // This was the last disabled rule for this model, so wipe out the list
                    if (existingDisabledRules.Count == 0)
                        _disabledRulesForModel.Remove(model);

                    model.EvaluatePropertyByName(rule.PropertyName);  // Re-evaluate the rule
                }
            }
        }

        /// <summary>
        /// Returns a collection of disabled rules on the given model
        /// </summary>
        /// <typeparam name="TModel">Evaulatable model type</typeparam>
        /// <param name="model">Evaulatable model that has rules</param>
        /// <returns>Collection of disabled rules on the given model</returns>
        public IEnumerable<IRule> GetDisabledRulesOnModel<TModel>(TModel model) where TModel : IEvaluatable
        {
            List<IRule> existingDisabledRules;
            _disabledRulesForModel.TryGetValue(model, out existingDisabledRules);

            return existingDisabledRules ?? Enumerable.Empty<IRule>();
        }

        #endregion

        #endregion

        #region Helpers

        /// <summary>
        /// Raises the RulesEvaluated event
        /// </summary>
        private void FireRulesEvaluated()
        {
            var handler = RulesEvaluated;
            if (handler != null)
                handler(this, new EventArgs());
        }

        /// <summary>
        /// Adds broken rule instance(s) for the given model to broken-rule-for-model dictionary.
        /// </summary>
        /// <param name="model">Model for which the rule applies</param>
        /// <param name="brokenRules">Collection of broken rules on the model</param>
        private void AddBrokenRulesForModel(IEvaluatable model, IEnumerable<IRule> brokenRules)
        {
            List<IRule> existingBrokenRules;

            // This model has no list to store rules yet, so create that list.
            if (!_brokenRulesForModel.TryGetValue(model, out existingBrokenRules))
            {
                existingBrokenRules = new List<IRule>();
                _brokenRulesForModel[model] = existingBrokenRules;
            }

            brokenRules.ToList().ForEach(e => existingBrokenRules.Add(e));
        }

        /// <summary>
        /// Disables rules with the given IDs by marking them as disabled and adding
        /// them to the collection of disabled rules.
        /// </summary>
        /// <param name="idsOfRulesToDisable">IDs of rules to disable</param>
        protected virtual void DisableRules(IEnumerable<string> idsOfRulesToDisable)
        {
            // ReSharper disable PossibleMultipleEnumeration
            if (!idsOfRulesToDisable.Any())
                return;

            var rulesToDisable = from r in _rules where idsOfRulesToDisable.Contains(r.RuleId) && r is Rule select r;
            // ReSharper restore PossibleMultipleEnumeration
            foreach (var rule in rulesToDisable)
            {
                if (rule.IsEnabled)
                {
                    ((Rule)rule).IsEnabled = false;     // The cast here is because IRule.IsEnabled_set does not exist. 
                    
                    if (!_disabledRules.Contains(rule)) // Using the field here because DisabledRules is INotifyCollectionChanged.  
                        _disabledRules.Add(rule);
                }
            }
        }

        /// <summary>
        /// Enables rules with the given IDs by marking them as enabled and removing
        /// them from the collection of disabled rules.
        /// </summary>
        /// <param name="idsOfRulesToEnable">IDs of rules to enable</param>
        protected virtual void EnableRules(IEnumerable<string> idsOfRulesToEnable)
        {
            // ReSharper disable PossibleMultipleEnumeration
            if (!idsOfRulesToEnable.Any())
                return;

            var rulesToEnable = from r in _rules where idsOfRulesToEnable.Contains(r.RuleId) && r is Rule select r;
            // ReSharper restore PossibleMultipleEnumeration
            foreach (var rule in rulesToEnable)
            {
                if (!rule.IsEnabled)
                {
                    ((Rule)rule).IsEnabled = true;      // The cast here is because IRule.IsEnabled_set does not exist.

                    if (_disabledRules.Contains(rule))  // Using the field here because DisabledRules is INotifyCollectionChanged.
                        _disabledRules.Remove(rule);
                }
            }
        }

        /// <summary>
        /// Evaluates rules of a given type on the given model (or properties on the given model)
        /// </summary>
        /// <typeparam name="TRule">Type of rule to be evalauted</typeparam>
        /// <param name="model">Model to be evaluated</param>
        /// <param name="propertyName">Name of a property on the given model whose rules should 
        /// be evaluated</param>
        /// <param name="stopOnFirstBrokenRule">Whether or not evaluation should cease after the 
        /// first broken rule is encountered; false by default</param>
        /// <returns>Collection of broken rules that were found</returns>
        /// <remarks>The EvaluateRules method is responsible for helping manage the rule
        /// engine state, while this method simply tells the rules to evaluate themselves.</remarks>
        protected virtual IEnumerable<IRule> InnerEvaluateRules<TRule>(IEvaluatable model, string propertyName, bool stopOnFirstBrokenRule) where TRule : IRule
        {
            if (model == null)
                throw new ArgumentNullException("model");

            var modelRules = from r in _rules
                             where r.EntityType == model.GetType() && r.PropertyName.Equals(propertyName) && r.IsEnabled && !GetDisabledRulesOnModel(model).Contains(r) 
                             select r;

            var brokenRules = new List<IRule>();
            foreach (IRule rule in modelRules)
            {
                if (!rule.Evaluate(model))
                {
                    brokenRules.Add(rule);
                    if (stopOnFirstBrokenRule)
                        break;
                }
            }

            return brokenRules;
        }

        #endregion
    }
}
