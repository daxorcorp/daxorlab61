﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Daxor.Lab.Domain.Entities.Base;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.Domain.Evaluation
{
    /// <summary>
    /// Represents object that can be evaluated by a rule engine
    /// </summary>
    public abstract class EvaluatableObject : ObservableObject, IEvaluatable
    {
        #region Fields

        /// <summary>
        /// Backing field for the evaluation-enabled flag
        /// </summary>
        private bool _isEvaluationEnabled;

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the EvaluatableObject class by enabling
        /// rule evaluation and by beginning listening to the rule engine's
        /// list of disabled rules
        /// </summary>
        /// <param name="ruleEngine">Rule engine that this object should use to 
        /// evaluate itself</param>
        protected EvaluatableObject(IRuleEngine ruleEngine)
        {
            _isEvaluationEnabled = true;
            RuleEngine = ruleEngine;
            if (RuleEngine != null)
                RuleEngine.DisabledRules.CollectionChanged += OnDisabledRulesCollectionChanged;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a rule engine that is capable of evaluating this object
        /// </summary>
        public IRuleEngine RuleEngine { get; set; }

        #endregion

        #region IDataErrorInfo members (from IEvaluatable)

        /// <summary>
        /// Gets the error message for this object (NOT USED; required for IDataErrorInfo) 
        /// </summary>
        public string Error
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// Gets the error message for the property with the given name
        /// </summary>
        /// <param name="columnName">Name of the property whose error message to get</param>
        /// <returns>Error message for the property</returns>
        public string this[string columnName]
        {
            get
            {
                return InternalEvaluatePropertyByName(columnName);
            }
        }

        #endregion

        #region IEvaluatable members

        #region Properties

        /// <summary>
        /// Gets a collection of broken rules on the object
        /// </summary>
        public IEnumerable<IRule> BrokenRules
        {
            get { return RuleEngine != null ? RuleEngine.GetBrokenRulesOnModel(this) : Enumerable.Empty<IRule>(); }
        }

        /// <summary>
        /// Gets a collection of disabled rules on the object
        /// </summary>
        public IEnumerable<IRule> DisabledRules
        {
            get { return RuleEngine != null ? RuleEngine.GetDisabledRulesOnModel(this) : Enumerable.Empty<IRule>(); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether evaluation is enabled on the object. If evaluation is
        /// disabled, all broken rules on this object will be cleared
        /// </summary>
        public bool IsEvaluationEnabled
        {
            get
            {
                return _isEvaluationEnabled;
            }

            set
            {
                _isEvaluationEnabled = value;
                if (!_isEvaluationEnabled && RuleEngine != null)
                    RuleEngine.ClearBrokenRulesOnModel(this);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Disables the given rule on the object
        /// </summary>
        /// <remarks>This method has no effect if this object has no IRuleEngine instance</remarks>
        /// <param name="rule">Rule to be disabled</param>
        public void DisableRule(IRule rule)
        {
            if (RuleEngine != null)
                RuleEngine.DisableRuleOnModel(this, rule);
        }

        /// <summary>
        /// Enables the given rule on the object
        /// </summary>
        /// <remarks>This method has no effect if this object has no IRuleEngine instance</remarks>
        /// <param name="rule">Rule to be enabled</param>
        public void EnableRule(IRule rule)
        {
            if (RuleEngine != null)
                RuleEngine.EnableRuleOnModel(this, rule);
        }

        /// <summary>
        /// Evaluates all enabled rules on the object
        /// </summary>
        /// <remarks>This method has no effect if this object has no IRuleEngine instance</remarks>
        public virtual void Evaluate()
        {
            if (IsEvaluationEnabled && RuleEngine != null)
                RuleEngine.EvaluateRules<IRule>(this);
        }

        /// <summary>
        /// Raises the PropertyChanged event which will ultimately result in a 
        /// call to the indexer
        /// </summary>
        /// <param name="propertyNames">Properties on the object whose corresponding rules should be evaluated</param>
        public virtual void EvaluatePropertyByName(params string[] propertyNames)
        {
            FirePropertyChanged(propertyNames);
        }

        #endregion

        #endregion

        #region IDisposable members (from ObservableObject)

        /// <summary>
        /// Clears all broken rules on this object and unhooked from the disabled rules'
        /// CollectionChanged event
        /// </summary>
        protected override void OnDispose()
        {
            if (RuleEngine != null)
            {
                RuleEngine.ClearBrokenRulesOnModel(this);
                RuleEngine.DisabledRules.CollectionChanged -= OnDisabledRulesCollectionChanged;
            }

            base.OnDispose();
        }

        #endregion

        #region Protected-virtual methods

        /// <summary>
        /// Evaluates all rules associated with this object and the given property name
        /// </summary>
        /// <param name="propertyName">Property whose associated rules should be evaluated</param>
        /// <returns>Message about the broken rule (if any); returns string.Empty if there
        /// are no broken rules</returns>
        protected virtual string InternalEvaluatePropertyByName(string propertyName)
        {
            if (propertyName != null && IsEvaluationEnabled && RuleEngine != null)
            {
                RuleEngine.EvaluateRules<IRule>(this, propertyName, stopOnFirstBrokenRule: true);
                var evaluationError = RuleEngine.GetBrokenRulesOnModel(this, propertyName).FirstOrDefault();
                if (evaluationError != null)
                    return evaluationError.BrokenRuleMessage;
            }

            return string.Empty;
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Handles the event where the collection of disabled rules changes by
        /// re-evaluating the rules being added or removed
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Event arguments</param>
        private void OnDisabledRulesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                // Even though these rules are being disabled, this change of state can
                // affect things, so we re-evaluate the disabled rule. (If you look at
                // RuleEngine.EvaluatePropertyByName(), this ultimately does a re-evaluation
                // by clearing all broken rules and then re-evaluating them. Disabled
                // rules are non-broken by design.) This kind of behavior is necessary
                // because a property on an EvaluatableObject can have multiple types
                // of rules (e.g., name must be 5 chars, must start with a capital letter,
                // must be non-empty) and we can't simply clear *all* of those rules --
                // they each have to be re-evaluated. (GMazeroff; 6/26/2012)
                foreach (var r in e.NewItems)
                {
                    var rule = r as IRule;
                    if (rule != null && rule.EntityType == GetType())
                        EvaluatePropertyByName(rule.PropertyName);
                }
            }
            else if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (var r in e.OldItems)
                {
                    var rule = r as IRule;
                    if (rule != null && rule.EntityType == GetType())
                        EvaluatePropertyByName(rule.PropertyName);
                }
            }
        }

        #endregion
    }
}
