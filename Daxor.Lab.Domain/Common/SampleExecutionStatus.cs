﻿namespace Daxor.Lab.Domain.Common
{
    /// <summary>
    /// Specifies constants defining the status of a sample's execution.
    /// </summary>
    public enum SampleExecutionStatus
    {
        /// <summary>
        /// The sample execution status is undefined.
        /// </summary>
        Undefined,

        /// <summary>
        /// The sample is not doing anything interesting, maybe just initialized.
        /// </summary>
        Idle,

        /// <summary>
        /// The sample is being moved to a position.
        /// </summary>
        InFlight,

        /// <summary>
        /// The sample's spectrum is being acquired (i.e., not finished).
        /// </summary>
        Acquiring,

        /// <summary>
        /// The sample's spectrum has finished acquiring.
        /// </summary>
        Completed,
    }
}