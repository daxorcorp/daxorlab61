﻿namespace Daxor.Lab.Domain.Common
{
    /// <summary>
    /// Specifies constants pertaining to the status of the test execution controller
    /// </summary>
    public enum TestExecutionControllerStatus
    {
        /// <summary>
        /// A test is currently running
        /// </summary>
        TestInProgress,

        /// <summary>
        /// A test has been completed
        /// </summary>
        TestCompleted,

        /// <summary>
        /// A test has been aborted
        /// </summary>
        TestAborted,

        /// <summary>
        /// The controller status is unknown
        /// </summary>
        Unknown,
    }
}
