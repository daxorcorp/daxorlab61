﻿namespace Daxor.Lab.Domain.Common
{
    /// <summary>
    /// Specifies constants defining gender.
    /// </summary>
    public enum GenderType
    {
        /// <summary>
        /// The gender is unknown.
        /// </summary>
        Unknown = -1,

        /// <summary>
        /// The gender is female.
        /// </summary>
        Female = 0,

        /// <summary>
        /// The gender is male.
        /// </summary>
        Male = 1,
    }
}
