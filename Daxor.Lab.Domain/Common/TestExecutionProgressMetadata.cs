﻿using System;

namespace Daxor.Lab.Domain.Common
{
    /// <summary>
    /// Metadata about the progress of an executing test
    /// </summary>
    public class TestExecutionProgressMetadata
    {
        /// <summary>
        /// Gets or sets the amount of time (in seconds) that the test has been executing.
        /// </summary>
        public double ElapsedExecutionTimeInSeconds { get; set; }
        
        /// <summary>
        /// Gets the how much of the test (in percent) has been completed.
        /// </summary>
        public int PercentCompleted
        {
            get
            {
                int percentage = 0;
                if (!double.IsNaN(TotalEstimatedExecutionTimeInSeconds) && TotalEstimatedExecutionTimeInSeconds != 0)
                {
                    percentage = (int)Math.Round((ElapsedExecutionTimeInSeconds / TotalEstimatedExecutionTimeInSeconds) * 100, 0);
                }

                return percentage;
            }
        }

        /// <summary>
        /// Gets the amount of execution time remaining (in seconds) until the test is completed.
        /// </summary>
        public int RemainingExecutionTimeInSeconds
        {
            get
            {
                int timeRemaining = TotalEstimatedExecutionTimeInSeconds - (int)ElapsedExecutionTimeInSeconds;

                if (timeRemaining > 0)
                    return timeRemaining;

                return 0;
            }
        }
 
        /// <summary>
        /// Gets or sets the total esimtated time (in seconds) for the test to complete.
        /// </summary>
        public int TotalEstimatedExecutionTimeInSeconds { get; set; }
    }
}
