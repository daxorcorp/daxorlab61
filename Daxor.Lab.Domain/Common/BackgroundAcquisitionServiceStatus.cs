﻿namespace Daxor.Lab.Domain.Common
{
    /// <summary>
    /// Specifies constants defining the status of background count acquisition status.
    /// </summary>
    public enum BackgroundAcquisitionServiceStatus
    {
        /// <summary>
        /// The background acquisition service status is unknown.
        /// </summary>
        Unknown,

        /// <summary>
        /// The background acquisition service is initializing.
        /// </summary>
        Initializing, 

        /// <summary>
        /// The background acquisition service is moving to the background position.
        /// </summary>
        SeekingBackgroundPosition,

        /// <summary>
        /// The background acquisition service is ready to begin acquiring.
        /// </summary>
        ReadyToBeginAcquisition,

        /// <summary>
        /// The background acquisition service is acquiring.
        /// </summary>
        Acquiring,

        /// <summary>
        /// The background acquisition service is idle.
        /// </summary>
        Idle,

        /// <summary>
        /// The background acquisition service has encountered an error.
        /// </summary>
        Error,
    }
}
