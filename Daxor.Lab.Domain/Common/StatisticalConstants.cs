﻿namespace Daxor.Lab.Domain.Common
{
    /// <summary>
    /// Constants used in the computation of statistical measures.
    /// </summary>
    public static class StatisticalConstants
    {
        /// <summary>
        /// Number of standard deviations to have a certainty of 99.99%
        /// </summary>
        public const double CertaintyFactor = 3.89;
        
        /// <summary>
        /// Default coefficient of variance for determining the difference between two values
        /// </summary>
        public const double DefaultCoefficientOfVariance = 0.5;
    }
}
