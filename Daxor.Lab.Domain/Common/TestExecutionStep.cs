﻿namespace Daxor.Lab.Domain.Common
{
    /// <summary>
    /// Specifies constants definine test execution steps/states
    /// </summary>
    public enum TestExecutionStep
    {
        /// <summary>
        /// The background measurement should be cancelled.
        /// </summary>
        CancelBackground,

        /// <summary>
        /// The sample changer should move to the background position.
        /// </summary>
        MoveToBackground,

        /// <summary>
        /// The background should be counted.
        /// </summary>
        CountBackground,

        /// <summary>
        /// The sample changer should move a specific position.
        /// </summary>
        MoveToSample,

        /// <summary>
        /// The sample changer has reached a given sample.
        /// </summary>
        MoveToSampleCompleted,
        
        /// <summary>
        /// The current sample should be measured/counted.
        /// </summary>
        CountSample,

        /// <summary>
        /// The current sample has finished being measured/counted.
        /// </summary>
        CountSampleCompleted,

        /// <summary>
        /// The execution state is unknown/undefined.
        /// </summary>
        Unknown,
    }
}
