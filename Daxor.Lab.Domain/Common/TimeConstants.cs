﻿namespace Daxor.Lab.Domain.Common
{
    /// <summary>
    /// Defines time unit conversion constants.
    /// </summary>
    public static class TimeConstants
    {
        /// <summary>
        /// Number of microseconds in one second.
        /// </summary>
        public const double MicrosecondsPerSecond = 1000000;

        /// <summary>
        /// Number of milliseconds in one second.
        /// </summary>
        public const double MillisecondsPerSecond = 1000;

        /// <summary>
        /// Number of seconds in one minute.
        /// </summary>
        public const double SecondsPerMinute = 60;
    }
}
