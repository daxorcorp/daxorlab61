﻿namespace Daxor.Lab.Domain.Common
{
    /// <summary>
    /// Specifies constants defining the status of a test.
    /// </summary>
    public enum TestStatus
    {
        /// <summary>
        /// The test status is undefined.
        /// </summary>
        Undefined = -1,

        /// <summary>
        /// The test is pending (i.e., has not been run).
        /// </summary>
        Pending = 0,

        /// <summary>
        /// The test is running.
        /// </summary>
        Running = 1,

        /// <summary>
        /// The test has been aborted.
        /// </summary>
        Aborted = 2,

        /// <summary>
        /// The test has been deleted.
        /// </summary>
        Deleted = 3,

        /// <summary>
        /// The test has been completed.
        /// </summary>
        Completed = 4,
    }
}