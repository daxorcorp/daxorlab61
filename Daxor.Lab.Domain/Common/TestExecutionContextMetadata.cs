﻿namespace Daxor.Lab.Domain.Common
{
    /// <summary>
    /// Represents metadata about an executing test's context
    /// </summary>
    public class TestExecutionContextMetadata
    {
        /// <summary>
        /// Initializes a new instance of the TestExecutionContextMetadata class
        /// </summary>
        /// <param name="appModuleKey">Module key for the app associated with the executing test</param>
        public TestExecutionContextMetadata(string appModuleKey)
        {
            AppModuleKey = appModuleKey;
        }

        /// <summary>
        /// Gets the module key for the app associated with the executing test
        /// </summary>
        public string AppModuleKey { get; private set; }
        
        /// <summary>
        /// Gets or sets the display name of the executing test
        /// </summary>
        public string DisplayTestName { get; set; }
        
        /// <summary>
        /// Gets or sets the display name for the executing sample
        /// </summary>
        public string FriendlyExecutingSampleName { get; set; }

        /// <summary>
        /// Gets or sets the counts obtained for the executing sample
        /// </summary>
        public int UpdatedSampleCounts { get; set; }
    }
}
