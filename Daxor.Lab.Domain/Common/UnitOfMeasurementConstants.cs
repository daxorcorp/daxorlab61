﻿namespace Daxor.Lab.Domain.Common
{
    /// <summary>
    /// Defines unit of measurement conversion constants.
    /// </summary>
    public static class UnitOfMeasurementConstants
    {
        /// <summary>
        /// Number of kilograms per pound.
        /// </summary>
        public const double KilogramsPerPound = 0.45359237;

        /// <summary>
        /// Number of centimeters per inch.
        /// </summary>
        public const double CentimetersPerInch = 2.54;

        #region Metric constants

        /// <summary>
        /// Unit of length used in the metric measurement system.
        /// </summary>
        public const string MetricUnitOfLength = "cm";

        /// <summary>
        /// Unit of mass used in the metric measurement system.
        /// </summary>
        public const string MetricUnitOfMass = "kg";

        #endregion

        #region U.S. constants

        /// <summary>
        /// Unit of length used in the U.S. measurement system.
        /// </summary>
        public const string USUnitOfLength = "in";

        /// <summary>
        /// Unit of mass used in the U.S. measurement system.
        /// </summary>
        public const string USUnitOfMass = "lb";

        #endregion
    }
}
