﻿namespace Daxor.Lab.Domain.Common
{
    /// <summary>
    /// Specifies constants defining a system of measurement.
    /// </summary>
    public enum MeasurementSystem
    {
        /// <summary>
        /// The measurement system is undefined.
        /// </summary>
        Undefined,

        /// <summary>
        /// The measurement system is metric (e.g., kilograms).
        /// </summary>
        Metric,

        /// <summary>
        /// The measurement system is English (e.g., pounds).
        /// </summary>
        English
    }
}
