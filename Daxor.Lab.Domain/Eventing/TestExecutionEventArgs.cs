﻿using System;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.Domain.Eventing
{
    /// <summary>
    /// Provides data for a TestExecution event.
    /// </summary>
    public class TestExecutionEventArgs : EventArgs
    {
        #region Ctor

        /// <summary>
        /// Initializes a new instance of the TestExecutionEventArgs class using the specified
        /// ID of the executing test and an optional error-related exception.
        /// </summary>
        /// <param name="executingTest">Instance of the executing test</param>
        /// <param name="executingTestId">ID of the executing test</param>
        /// <param name="errorException">Exception pertaining to an error (null by default)</param>
        public TestExecutionEventArgs(ITest executingTest, Guid executingTestId, Exception errorException = null)
        {
            ExecutingTestId = executingTestId;
            ErrorException = errorException;
            ExecutingTest = executingTest;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the exception pertaining to a test execution error.
        /// </summary>
        public Exception ErrorException { get; private set; }

        /// <summary>
        /// Gets the ID of the executing test associated with the event.
        /// </summary>
        public Guid ExecutingTestId { get; private set; }

        /// <summary>
        /// Gets the test associated with the event.
        /// </summary>
        public ITest ExecutingTest { get; private set; }

        #endregion
    }
}
