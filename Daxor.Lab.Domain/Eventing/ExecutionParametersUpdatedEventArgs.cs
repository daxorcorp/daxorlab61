﻿using System;

namespace Daxor.Lab.Domain.Eventing
{
    /// <summary>
    /// Provides data for the ExecutionParametersUpdated event.
    /// </summary>
    public class ExecutionParametersUpdatedEventArgs : EventArgs
    {
        #region Ctor

        /// <summary>
        /// Initializes a new instance of the ExecutionParametersUpdatedEventArgs class 
        /// using the specified fine gain, voltage, and integral status. 
        /// </summary>
        /// <param name="fineGain">Detector fine gain</param>
        /// <param name="voltage">Detector voltage</param>
        /// <param name="isDesiredIntegralReached">Whether or not the desired integral
        /// has been reached (false by default)</param>
        public ExecutionParametersUpdatedEventArgs(
            double? fineGain, 
            int? voltage, 
            bool isDesiredIntegralReached = false)
        {
            Voltage = voltage;
            FineGain = fineGain;
            IsDesiredIntegralReached = isDesiredIntegralReached;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the fine gain value that was used by the detector to measure a sample.
        /// </summary>
        public double? FineGain { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the desired integral (i.e., number of counts) has been reached.
        /// </summary>
        public bool IsDesiredIntegralReached { get; private set; }

        /// <summary>
        /// Gets the voltage value that was used by the detector to measure a sample.
        /// </summary>
        public int? Voltage { get; private set; }

        #endregion
    }
}
