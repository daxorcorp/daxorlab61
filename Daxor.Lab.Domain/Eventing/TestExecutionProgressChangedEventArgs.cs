﻿using System;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.Domain.Eventing
{
    /// <summary>
    /// Provides data for the ProgressChanged event.
    /// </summary>
    public class TestExecutionProgressChangedEventArgs : EventArgs
    {
        #region Ctor

        /// <summary>
        /// Initializes a new instance of the TestExecutionProgressChangedEventArgs class
        /// </summary>
        /// <param name="executingTestType">Type of test being executed</param>
        /// <param name="sampleId">ID of the sample on which progress was made</param>
        /// <param name="progressMetadata">Metadata (time values) about the test's progress</param>
        /// <param name="contextMetadata">Metadata (sample name, counts, etc.) about the executing test</param>
        /// <param name="testExecutionStep">Step the test execution controller is currently on</param>
        public TestExecutionProgressChangedEventArgs(
            Type executingTestType, 
            Guid sampleId,
            TestExecutionProgressMetadata progressMetadata,
            TestExecutionContextMetadata contextMetadata,
            TestExecutionStep testExecutionStep)
        {
            ExecutingTestType = executingTestType;
            ExecutingSampleId = sampleId;
            ProgressMetadata = progressMetadata;
            ContextMetadata = contextMetadata;
            CurrentExecutionStep = testExecutionStep;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the test execution context metadata (i.e., information about the executing
        /// test)
        /// </summary>
        public TestExecutionContextMetadata ContextMetadata { get; private set; }

        /// <summary>
        /// Gets the current execution step
        /// </summary>
        public TestExecutionStep CurrentExecutionStep { get; private set; }

        /// <summary>
        /// Gets the identifier of the currently executing sample
        /// </summary>
        public Guid ExecutingSampleId { get; private set; }                
        
        /// <summary>
        /// Gets the type of test being executed
        /// </summary>
        public Type ExecutingTestType { get; private set; }
        
        /// <summary>
        /// Gets the progress metadata (i.e., time values)
        /// </summary>
        public TestExecutionProgressMetadata ProgressMetadata { get; private set; }

        #endregion
    }
}
