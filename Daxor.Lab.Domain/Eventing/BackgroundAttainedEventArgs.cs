﻿using System;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.Domain.Eventing
{
    /// <summary>
    /// Provides data for the BackgroundAttained event.
    /// </summary>
    public class BackgroundAttainedEventArgs : EventArgs
    {
        #region Ctor

        /// <summary>
        /// Initializes a new instance of the BackgroundAttainedEventArgs class using the
        /// specified background count, acquisition time, good-background status, and
        /// override status.
        /// </summary>
        /// <param name="count">Background counts obtained</param>
        /// <param name="acquisitionTimeInSeconds">Length of background acquisition (in seconds)</param>
        /// <param name="isGood">Whether or not a good background was obtained</param>
        /// <param name="isOverridden">Whether or not the background is overridden</param>
        public BackgroundAttainedEventArgs(
            int count, 
            int acquisitionTimeInSeconds, 
            bool isGood,
            bool isOverridden)
        {
            Count = count;
            AcquisitionTimeInSeconds = acquisitionTimeInSeconds;
            IsGood = isGood;
            IsOverridden = isOverridden;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets the background acquisition time (in seconds).
        /// </summary>
        public int AcquisitionTimeInSeconds { get; private set; }

        /// <summary>
        /// Gets the background counts.
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Gets the background rate in CPM (counts per minute).
        /// </summary>
        /// <remarks>Symmetrical rounding is used for fractional counts per minute</remarks>
        public int CountsPerMinute
        {
            get
            {
                if (AcquisitionTimeInSeconds != 0)
                    return (int)Math.Round(Count / (double)AcquisitionTimeInSeconds * TimeConstants.SecondsPerMinute, 0);

                return 0;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the background count is overridden with another value.
        /// </summary>
        public bool IsOverridden { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the obtained background was good. 
        /// </summary>
        public bool IsGood { get; private set; }

        #endregion
    }
}
