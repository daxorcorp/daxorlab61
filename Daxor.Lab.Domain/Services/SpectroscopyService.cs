﻿using System;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.Domain.Services
{
    /// <summary>
    /// Implements algorithms that analyze spectra.
    /// </summary>
    public class SpectroscopyService : ISpectroscopyService
    {
        #region ISpectroscopyService members

        /// <summary>
        /// Computes an unweighted sliding average of the given spectrum based
        /// on a specified width.
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be smoothed</param>
        /// <param name="width">Width parameter for smoothing</param>
        /// <returns>Array (spectrum) of computed counts; null if the
        /// spectrum is invalid</returns>
        public uint[] ComputeAverageSmoothSpectrum(uint[] spectrumArray, int width)
        {
            if (!IsValidSpectrum(spectrumArray))
                return null;

            // Smoothed array
            var smoothedSpectrum = new uint[spectrumArray.Length];

            // Trim off channels for which smoothing there was not enough data
            // In general, for an m-width smooth, there will be (width-1)/2 points at
            // the begining of the spectrum and (width-1)/2 the end of the spectrum
            // for which a complete width smooth can not be calculated.
            Array.Clear(smoothedSpectrum, 0, spectrumArray.Length);
            for (int i = width / 2; i < spectrumArray.Length - (width / 2); i++)
            {
                smoothedSpectrum[i] = spectrumArray[i - 1] + spectrumArray[i] + spectrumArray[i + 1];
                smoothedSpectrum[i] = (uint)Math.Round(smoothedSpectrum[i] / (double)width, 0);
            }

            return smoothedSpectrum;
        }

        /// <summary>
        /// Computes the centroid of the given spectrum based on the specified bounds.
        /// </summary>
        /// <remarks>Using first moment centroid algorithm, as it's the default algorithm 
        /// utilized by Canberra's software.</remarks>
        /// <param name="spectrumArray">Spectrum to be used for computing the centroid</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The computed centroid</returns>
        public double ComputeCentroid(uint[] spectrumArray, int leftBound, int rightBound)
        {
            return CentroidFirstMoment(spectrumArray, leftBound, rightBound);
        }

        /// <summary>
        /// Computes the full width at half maximum (FWHM) of the given spectrum based
        /// on the specified bounds
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be used for computing the FWHM</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The computed full width at half maximum</returns>
        public double ComputeFullWidthHalfMax(uint[] spectrumArray, int leftBound, int rightBound)
        {
            return ComputeFullWidthKMax(spectrumArray, 0.5f, leftBound, rightBound);
        }

        /// <summary>
        /// Computes the full width at kth maximum (FWKM) of the given Spectrum based
        /// on the specified fractional height and bounds.
        /// </summary>
        /// <param name="spectrum">Spectrum instance to be used for computing the FWKM</param>
        /// <param name="kmaxFraction">Fractional height of interest</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The computed full width at kth maximum</returns>
        public double ComputeFullWidthKMax(Spectrum spectrum, float kmaxFraction, int leftBound, int rightBound)
        {
            return ComputeFullWidthKMax(spectrum == null ? null : spectrum.SpectrumArray, kmaxFraction, leftBound, rightBound);
        }

        /// <summary>
        /// Computes the full width at kth maximum (FWKM) of the given spectrum array based
        /// on the specified fractional height and bounds.
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be used for computing the FWKM</param>
        /// <param name="kmaxFraction">Fractional height of interest</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The computed full width at kth maximum</returns>
        public double ComputeFullWidthKMax(uint[] spectrumArray, float kmaxFraction, int leftBound, int rightBound)
        {
            double notNeededBackground;
            return ComputeFullWidthKMax(spectrumArray, kmaxFraction, leftBound, rightBound, out notNeededBackground);
        }

        /// <summary>
        /// Computes the full width at kth maximum (FWKM) of the given Spectrum based
        /// on the specified fractional height and bounds with the Compton background
        /// removed.
        /// </summary>
        /// <param name="spectrum">Spectrum instance to be used for computing the FWKM</param>
        /// <param name="kmaxFraction">Fractional height of interest</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <param name="background">The background level that has been removed</param>
        /// <returns>The computed full width at kth maximum</returns>
        public double ComputeFullWidthKMax(Spectrum spectrum, float kmaxFraction, int leftBound, int rightBound, out double background)
        {
            return ComputeFullWidthKMax(spectrum == null ? null : spectrum.SpectrumArray, kmaxFraction, leftBound, rightBound, out background);
        }

        /// <summary>
        /// Computes the full width at kth maximum (FWKM) of the given spectrum array based
        /// on the specified fractional height and bounds with the Compton background
        /// removed.
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be used for computing the FWKM</param>
        /// <param name="kmaxFraction">Fractional height of interest</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <param name="background">The background level that has been removed</param>
        /// <returns>The computed full width at kth maximum</returns>
        public double ComputeFullWidthKMax(uint[] spectrumArray, float kmaxFraction, int leftBound, int rightBound, out double background)
        {
            background = 0;
            double fwkm = -1;
            int xpeak = int.MinValue;
            double ypeak = double.MinValue;

            var xypairs = new double[4, 2];
            double[] newSpectrumArray;

            if (kmaxFraction <= 0 || kmaxFraction > 1) return 0;

            // Validate the spectrum, return 0 if spectrum is not valid
            if (!IsValidSpectrum(spectrumArray, leftBound - 1, rightBound - 1))
            {
                return fwkm;
            }

            background = RemoveContinuumByStraightLine(spectrumArray, leftBound, rightBound, out newSpectrumArray);

            // 3point average Peak finding

            // Find Max x,y pair
            for (int i = leftBound - 1; i < rightBound; i++)
            {
                if (newSpectrumArray[i] > ypeak)
                {
                    ypeak = newSpectrumArray[i];
                    xpeak = i;
                }
            }

            // Determine if we failed to find our new peak
            bool isInfinity = double.IsInfinity(ypeak);
            bool isNotANumber = double.IsNaN(ypeak);
            // ReSharper disable CompareOfFloatsByEqualityOperator
            bool isMinDouble = ypeak == double.MinValue;    // ReSharper restore CompareOfFloatsByEqualityOperator
            bool isZero = Math.Abs(ypeak - 0) < 1E-300;
            if (isInfinity || isNotANumber || isMinDouble || isZero) return fwkm;

            // Calculate kMax value used for FWKM calculations
            double kmax = ypeak * kmaxFraction;

            // Proceed on the left side of the peak, find pair (x1,y1) and (x2,y2)
            bool leftPairDetected = false;
            for (int ch = xpeak - 1; ch >= leftBound - 1; ch--)
            {
                // Found left pair of (x1,y1) and (x2, y2)
                if (newSpectrumArray[ch] < kmax)
                {
                    // Pair below kMax line to the left of the peak
                    xypairs[0, 0] = ch;
                    xypairs[0, 1] = newSpectrumArray[ch];

                    // Pair above kMax Line
                    xypairs[1, 0] = ch + 1;
                    xypairs[1, 1] = newSpectrumArray[ch + 1];
                    leftPairDetected = true;
                    break;
                }
            }

            // Proceed on the right side of the peak, find pair (x1,y1) and (x2,y2)
            bool rightPairDetected = false;
            for (int ch = xpeak + 1; ch < rightBound; ch++)
            {
                if (Math.Round(newSpectrumArray[ch], 3) < kmax)
                {
                    // Pair below kMax line to the right of the peak
                    xypairs[3, 0] = ch;
                    xypairs[3, 1] = newSpectrumArray[ch];

                    xypairs[2, 0] = ch - 1;
                    xypairs[2, 1] = newSpectrumArray[ch - 1];
                    rightPairDetected = true;
                    break;
                }
            }

            // Final calculations 
            try
            {
                // x coordinate (channel) of intersection of line L with low-energy side of a peak
                double xintersectionLow;
                if (!leftPairDetected)
                {
                    xintersectionLow = leftBound - 1;
                }
                else
                {
                    xintersectionLow = ((kmaxFraction * ypeak) - xypairs[0, 1]) / (xypairs[1, 1] - xypairs[0, 1]);
                    xintersectionLow += xypairs[0, 0];
                }

                // x coordinate (channel) of intersection of line L with high-energy side of a peak
                double xintersectionHigh;
                if (!rightPairDetected)
                {
                    xintersectionHigh = rightBound - 1;
                }
                else
                {
                    xintersectionHigh = (xypairs[2, 1] - (kmaxFraction * ypeak)) / (xypairs[2, 1] - xypairs[3, 1]);
                    xintersectionHigh += xypairs[2, 0];
                }

                fwkm = xintersectionHigh - xintersectionLow;
            }
            catch (Exception)
            {
                fwkm = -1;
            }

            return fwkm;
        }

        /// <summary>
        /// Computes the full width at tenth maximum (FWTM) of the given spectrum based
        /// on the specified bounds.
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be used for computing the FWTM</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The computed full width at tenth maximum</returns>
        public double ComputeFullWidthTenthMax(uint[] spectrumArray, int leftBound, int rightBound)
        {
            return ComputeFullWidthKMax(spectrumArray, 0.1f, leftBound, rightBound);
        }

        /// <summary>
        /// Computes the count integral of a given Spectrum within the specified bounds.
        /// </summary>
        /// <param name="spectrum">Spectrum instance to be used for computing the integral</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The count integral; zero if the spectrum isn't valid</returns>
        /// <exception cref="Exception">The integral could not be computed</exception>
        public ulong ComputeIntegral(Spectrum spectrum, int leftBound, int rightBound)
        {
            return ComputeIntegral(spectrum == null ? null : spectrum.SpectrumArray, leftBound, rightBound);
        }

        /// <summary>
        /// Computes the count integral of a given spectrum within the specified bounds.
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be used for computing the integral</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The count integral; zero if the spectrum isn't valid</returns>
        /// <exception cref="Exception">The integral could not be computed</exception>
        public ulong ComputeIntegral(uint[] spectrumArray, int leftBound, int rightBound)
        {
            ulong integral = 0;
            int zeroBasedLeftBound = leftBound - 1;
            int zeroBasedRightBound = rightBound - 1;

            // If integral is zero if the spectrum is invalid.
            if (!IsValidSpectrum(spectrumArray, zeroBasedLeftBound, zeroBasedRightBound))
                return 0;

            // Get the actual integral for the region of interest (ROI).
            try
            {
                for (int i = zeroBasedLeftBound; i <= zeroBasedRightBound && i >= 0; i++)
                    integral += spectrumArray[i];
            }
            catch
            {
                throw;
            }

            return integral;
        }

        /// <summary>
        /// Removes Compton continuum from the given Spectrum. Algorithm is used by Canbera 3.1 
        /// package and is described in details by J.L Parker p.122
        /// </summary>
        /// <param name="spectrumArray">Spectrum to be used for continuum removal</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <param name="continuumFreeSpectrum">Array of counts (spectrum) without the continuum</param>
        /// <returns>The background level that has been removed</returns>
        public double RemoveContinuumByStraightLine(uint[] spectrumArray, int leftBound, int rightBound, out double[] continuumFreeSpectrum)
        {
            continuumFreeSpectrum = new double[spectrumArray.Length];

            // Use four channels below leftBound and rightBound
            // to establish 
            double xL = 0;
            double yL = 0;
            double xH = 0;
            double yH = 0;
            double background = 0;

            // Find low x and y
            for (int i = leftBound - 2; i > leftBound - 6 && i >= 0; i--)
            {
                xL += i;
                yL += spectrumArray[i];
            }

            xL /= 4.0;    // find channel averages in the left (4 channel) ROI
            yL /= 4.0;    // find average counts in the left(4 channel) ROI

            // Find high x and y
            for (int j = rightBound; j < rightBound + 4 && j < spectrumArray.Length; j++)
            {
                xH += j;
                yH += spectrumArray[j];
            }

            xH /= 4.0;    // find channel averages in the left (4 channel) ROI
            yH /= 4.0;    // find average counts in the left(4 channel) ROI

            // Find slope (m) of the function and intercept (b)
            double m = (yH - yL) / (xH - xL);     // slope of the straight line used in continuum subtraction
            double b = ((xH * yL) - (xL * yH)) / (xH - xL);    // intercept of the straight line used in continuum subtraction

            // Remove the background from each channel based on the line we have calculated
            for (int ch = leftBound - 1; ch < rightBound; ch++)
            {
                if (spectrumArray[ch] < ((m * ch) + b))
                {
                    continuumFreeSpectrum[ch] = 0;
                    background += continuumFreeSpectrum[ch];
                }
                else
                {
                    continuumFreeSpectrum[ch] = spectrumArray[ch] - ((m * ch) + b);
                    background += spectrumArray[ch] - continuumFreeSpectrum[ch];
                }
            }

            return background;
        }

        #endregion

        #region Internal

        #region Private helper method

        /// <summary>
        /// Determines whether the given spectrum array is valid.
        /// </summary>
        /// <param name="spectrumArray">Spectrum array to evaluate</param>
        /// <param name="leftBound">Left ROI bound (default is zero)</param>
        /// <param name="rightBound">Right ROI bound (default is zero)</param>
        /// <returns>True if the array is non-null, has at least one element, and 
        /// left bound is less than or equal to the right bound; false otherwise</returns>
        private bool IsValidSpectrum(uint[] spectrumArray, int leftBound = 0, int rightBound = 0)
        {
            if (spectrumArray == null || spectrumArray.Length == 0 || (rightBound - leftBound) <= 0)
                return false;

            return true;
        }

        #endregion

        #region Centroid Algorithms

        /// <summary>
        /// Determines the peak centroid using the first-moment method.
        /// </summary>
        /// <remarks>
        /// Described by J.L. Parker in "General Topics in Passive Gamma-Ray Assay" ch. 5, p.105.
        /// PeakCentroid = SUM(Xi * Yi)/SUM(Yi)
        /// </remarks>
        /// <param name="spectrum">Spectrum instance to be used for computing the centroid</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The centroid value; zero if the spectrum is invalid</returns>
        /// <exception cref="Exception">The centroid could not be computed</exception>
        /// <exception cref="ArgumentOutOfRangeException">Spectrum array did not have appropriate 
        /// channel between leftBound and rightBound</exception>
        internal double CentroidFirstMoment(Spectrum spectrum, int leftBound, int rightBound)
        {
            return CentroidFirstMoment(spectrum == null ? null : spectrum.SpectrumArray, leftBound, rightBound);
        }

        /// <summary>
        /// Determines the peak centroid using the first-moment method.
        /// </summary>
        /// <remarks>
        /// Described by J.L. Parker in "General Topics in Passive Gamma-Ray Assay" ch. 5, p.105.
        /// PeakCentroid = SUM(Xi * Yi)/SUM(Yi)
        /// </remarks>
        /// <param name="spectrumArray">Spectrum to be used for computing the centroid</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The centroid value; zero if the spectrum is invalid</returns>
        /// <exception cref="Exception">The centroid could not be computed</exception>
        /// <exception cref="ArgumentOutOfRangeException">Spectrum array did not have appropriate 
        /// channel between leftBound and rightBound</exception>
        internal double CentroidFirstMoment(uint[] spectrumArray, int leftBound, int rightBound)
        {
            double peakCentroid = 0;
            
            if (!IsValidSpectrum(spectrumArray, leftBound - 1, rightBound - 1))
                return peakCentroid;

            try
            {
                ulong sumOfChannelsTimesCounts = 0;
                ulong sumOfCounts = 0;
                for (int channel = leftBound - 1; channel < rightBound; channel++)
                {
                    sumOfChannelsTimesCounts += ((ulong)channel + 1) * spectrumArray[channel];
                    sumOfCounts += spectrumArray[channel];
                }

                // Calculate peak Centroid
                if (sumOfCounts == 0)
                    peakCentroid = 0;
                else
                    peakCentroid = (double)sumOfChannelsTimesCounts / sumOfCounts;
            }
            catch
            {
                throw;
            }

            return peakCentroid;
        }

        /// <summary>
        /// Determines the peak centroid using the five-channel method.
        /// </summary>
        /// <remarks>
        /// Described by J.L. Parker in "General Topics in Passive Gamma-Ray Assay" ch. 5, p.105
        /// PeakCentroid = SUM(Xi * Yi)/SUM(Yi)
        /// </remarks>
        /// <param name="spectrum">Spectrum instance to be used for computing the centroid</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The centroid value; zero if the spectrum is invalid</returns>
        /// <exception cref="Exception">The centroid could not be computed</exception>
        /// <exception cref="ArgumentOutOfRangeException">Spectrum array did not have appropriate 
        /// channel between leftBound and rightBound</exception>
        /// <exception cref="DivideByZeroException">A division by zero occurred when the
        /// centroid was computed</exception>
        internal double CentroidFiveChannel(Spectrum spectrum, int leftBound, int rightBound)
        {
            return CentroidFiveChannel(spectrum == null ? null : spectrum.SpectrumArray, leftBound, rightBound);
        }

        /// <summary>
        /// Determines the peak centroid using the five-channel method.
        /// </summary>
        /// <remarks>
        /// Described by J.L. Parker in "General Topics in Passive Gamma-Ray Assay" ch. 5, p.105
        /// PeakCentroid = SUM(Xi * Yi)/SUM(Yi)
        /// </remarks>
        /// <param name="spectrumArray">Spectrum to be used for computing the centroid</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <returns>The centroid value; zero if the spectrum is invalid</returns>
        /// <exception cref="Exception">The centroid could not be computed</exception>
        /// <exception cref="ArgumentOutOfRangeException">Spectrum array did not have appropriate 
        /// channel between leftBound and rightBound</exception>
        /// <exception cref="DivideByZeroException">A division by zero occurred when the
        /// centroid was computed</exception>
        internal double CentroidFiveChannel(uint[] spectrumArray, int leftBound, int rightBound)
        {
            double peakCentroid = 0;
            
            // If spectrum is valid
            if (!IsValidSpectrum(spectrumArray, leftBound, rightBound))
            {
                return peakCentroid;
            }

            try
            {
                uint maxCountsInChannel = 0;
                int maxChannel = int.MaxValue;
                for (int channel = leftBound - 1; channel <= rightBound; channel++)
                {
                    // find max
                    if (maxCountsInChannel < spectrumArray[channel])
                    {
                        maxCountsInChannel = spectrumArray[channel];
                        maxChannel = channel;
                    }
                }

                // Find 2 to the right and 2 to the left
                uint numerator = (spectrumArray[maxChannel + 1] * (maxCountsInChannel - spectrumArray[maxChannel - 2])) -
                                 (spectrumArray[maxChannel - 1] * (spectrumArray[maxChannel] - spectrumArray[maxChannel + 2]));

                uint denominator = (spectrumArray[maxChannel + 1] * (maxCountsInChannel - spectrumArray[maxChannel - 2])) +
                                 (spectrumArray[maxChannel - 1] * (spectrumArray[maxChannel] - spectrumArray[maxChannel + 2]));

                peakCentroid = maxChannel + ((double)numerator / denominator);
            }
            catch 
            {
                throw;
            }

            return Math.Round(peakCentroid, 3);
        }

        #endregion
       
        #region Continuum-removal (float)

        /// <summary>
        /// Removes Compton continuum from the passed spectrum array which is part of Spectrum
        /// object (only between selected ROIs). Algorithm is used by Canbera 3.1 package and
        /// is described in details by J.L Parker p.122
        /// </summary>
        /// <param name="spectrumArray">Spectrum instance to be used for continuum removal</param>
        /// <param name="leftBound">Left ROI bound</param>
        /// <param name="rightBound">Right ROI bound</param>
        /// <param name="continuumFreeSpectrum">Array of counts (spectrum) without the continuum</param>
        /// <returns>The background level that has been removed</returns>
        internal float RemoveContinuumWithStraightLine(float[] spectrumArray, int leftBound, int rightBound, out float[] continuumFreeSpectrum)
        {
            continuumFreeSpectrum = new float[spectrumArray.Length];

            // Use four channels below leftBound and rightBound
            // to establish 
            float xL = 0;
            float yL = 0;
            float xH = 0;
            float yH = 0;
            float background = 0;

            // Find low x and y
            for (int i = leftBound - 2; i > leftBound - 6 && i >= 0; i--)
            {
                xL += i;
                yL += spectrumArray[i];
            }

            xL /= 4.0f;    // find channel averages in the left (4 channel) ROI
            yL /= 4.0f;    // find average counts in the left(4 channel) ROI

            // Find high x and y
            for (int j = rightBound; j < rightBound + 4 && j < spectrumArray.Length; j++)
            {
                xH += j;
                yH += spectrumArray[j];
            }

            xH /= 4.0f;    // find channel averages in the left (4 channel) ROI
            yH /= 4.0f;    // find average counts in the left(4 channel) ROI

            // Find slope (m) of the function and intercept (b)
            float m = (yH - yL) / (xH - xL);   // slope of the straight line used in continuum subtraction
            float b = ((xH * yL) - (xL * yH)) / (xH - xL);   // intercept of the straight line used in continuum subtraction

            // Remove the background from each channel based on the line we have calculated
            for (int ch = leftBound - 1; ch < rightBound; ch++)
            {
                if (spectrumArray[ch] < ((m * ch) + b))
                {
                    continuumFreeSpectrum[ch] = 0;
                }
                else
                {
                    continuumFreeSpectrum[ch] = spectrumArray[ch] - ((m * ch) + b);
                    background += spectrumArray[ch] - continuumFreeSpectrum[ch];
                }
            }

            return background;
        }

        #endregion

        #endregion
    }
}
