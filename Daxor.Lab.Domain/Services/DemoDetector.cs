﻿using System;
using System.Timers;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.Domain.Services
{
    /// <summary>
    /// Represents a demo detector that can be used in place of a hardware-bound detector
    /// </summary>
    public class DemoDetector : IDetector
    {
        #region Static

        /// <summary>
        /// Frequency that the detector will update itself when acquiring
        /// </summary>
        public static readonly int AcquisitionUpdateFrequencyInMilliseconds = (int)TimeConstants.MillisecondsPerSecond;

        /// <summary>
        /// Name of this detector
        /// </summary>
        public static readonly string DemoDetectorName = "Demo_Detector";

        /// <summary>
        /// Serial number
        /// </summary>
        public static readonly int DemoSerialNumber = 123789;

        /// <summary>
        /// Number of channels
        /// </summary>
        public static readonly int DemoSpectrumLength = 1024;

        /// <summary>
        /// Initial fine gain value
        /// </summary>
        public static readonly double InitialFineGain = 1.0;

        /// <summary>
        /// Initial low level discriminator value
        /// </summary>
        public static readonly int InitialLowLevelDiscriminator = 18;

        /// <summary>
        /// Initial noise value
        /// </summary>
        public static readonly int InitialNoise = 10;

        /// <summary>
        /// Initial voltage value
        /// </summary>
        public static readonly int InitialVoltage = 1000;

        /// <summary>
        /// The number of counts to simulate in one minute over the entire spectrum.
        /// </summary>
        public static readonly int TotalSpectrumCounts = 400;

        #endregion

        #region Fields

        /// <summary>
        /// Concurrency lock for the acquisition start time
        /// </summary>
        private readonly object _acquisitionStartTimeLock = new object();

        /// <summary>
        /// Backing field for the acquisition start time
        /// </summary>
        private DateTime? _acquisitionStartTime;

        /// <summary>
        /// Backing field fo the spectrum being acquired
        /// </summary>
        private Spectrum _spectrum;

        /// <summary>
        /// Backing field for the timer used to simulate time elapsing
        /// </summary>
        internal readonly ITimer InternalTimer;

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the DemoDetector class using the default delay between acquisition updates
        /// </summary>
        public DemoDetector()
            : this(new SimpleTimer(AcquisitionUpdateFrequencyInMilliseconds))
        {
        }

        /// <summary>
        /// Initializes a new instance of the DemoDetector class using the given ITimer to simulate the
        /// delay betwwen acquisition updates
        /// </summary>
        /// <param name="delayTimer">ITimer instance used to provide delays between 
        /// acquisition updates</param>
        public DemoDetector(ITimer delayTimer)
        {
            InternalTimer = delayTimer;
            InternalTimer.Elapsed += OnTimerElapsed;

            InitializeDetector(true);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the time that acquisition was started
        /// </summary>
        private DateTime? AcquisitionStartTime
        {
            get
            {
                return _acquisitionStartTime;
            }

            set
            {
                lock (_acquisitionStartTimeLock)
                {
                    _acquisitionStartTime = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the amount of time (in seconds) required for acquisition
        /// </summary>
        private int AcquisitionTimeInSeconds
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the ITimer instance used to simulate delays
        /// </summary>
        private ITimer Timer
        {
            get { return InternalTimer; }
        }

        #endregion

        #region IDetector members

        #region Events

        /// <summary>
        /// Occurs when the spectrum acquisition has completed
        /// </summary>
        public event AcquisitionCompletedHandler AcquisitionCompleted;

        /// <summary>
        /// Occurs when spectrum acquisition has started
        /// </summary>
        public event AcquisitionStartedHandler AcquisitionStarted;

        /// <summary>
        /// Occurs when spectrum acquisition has stopped
        /// </summary>
        public event AcquisitionStoppedHandler AcquisitionStopped;

        /// <summary>
        /// Occurs when the detector is connected to the computer
        /// </summary>
        public event DetectorConnectedHandler Connected;

        /// <summary>
        /// Occurs when the detector has been disconnected from the computer
        /// </summary>
        public event DetectorDisconnectedHandler Disconnected;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the detector's analog-to-digital (ADC) current voltage, which is the same
        /// as the Voltage property for this demo detector
        /// </summary>
        public double AdcCurrent
        {
            get { return Voltage; }
        }

        /// <summary>
        /// Gets or sets the detector's fine gain
        /// </summary>
        public double FineGain
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a value indicating whether or not the spectrum acquisition has completed
        /// </summary>
        public bool HasAcquisitionCompleted
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether or not the detector is currently acquiring a spectrum
        /// </summary>
        public bool IsAcquiring
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether or not the detector is connected
        /// </summary>
        public bool IsConnected
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether or not the gross counts for acquisition have been preset
        /// </summary>
        public bool IsGrossCountsPreset
        {
            get
            {
                return !IsLiveTimePreset;
            }

            private set
            {
                IsLiveTimePreset = !value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether or not the live time for acquisition has been preset
        /// </summary>
        public bool IsLiveTimePreset
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a value indicating whether or not the real time for acquisition has been preset.
        /// (Real time is the same as live time for this demo detector.)
        /// </summary>
        public bool IsRealTimePreset
        {
            get { return IsLiveTimePreset; }
        }

        /// <summary>
        /// Gets or sets the detector's low level discriminator value. (This property does not
        /// affect the behavior of this demo detector.)
        /// </summary>
        public int LowLevelDiscriminator
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the detector's noise value
        /// </summary>
        public int Noise
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the detector's serial number
        /// </summary>
        public int SerialNumber
        {
            get { return DemoSerialNumber; }
        }

        /// <summary>
        /// Gets the detector's current spectrum
        /// </summary>
        public Spectrum Spectrum
        {
            get
            {
                return _spectrum;
            }
        }

        /// <summary>
        /// Gets the length of the detector's current spectrum (in channels)
        /// </summary>
        public int SpectrumLength
        {
            get { return DemoSpectrumLength; }
        }

        /// <summary>
        /// Gets or sets the detector's current voltage
        /// </summary>
        public int Voltage
        {
            get;
            set;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the detector and raises the Connected event
        /// </summary>
        public void Connect()
        {
            InitializeDetector(true);
            FireConnected(DemoDetectorName + SerialNumber);
        }

        /// <summary>
        /// Re-initializes the detector but with disconnected state, then
        /// raises the Disconnected event
        /// </summary>
        public void Disconnect()
        {
            InitializeDetector(false);
            FireDisconnected(DemoDetectorName + SerialNumber, "Disconnect() explicitly called on DemoDetector");
        }

        /// <summary>
        /// Resets the detector's current spectrum and resets the time values
        /// </summary>
        public void ResetSpectrum()
        {
            _spectrum = Spectrum.Empty();
            _spectrum.SpectrumArray = new uint[SpectrumLength];

            _spectrum.LiveTimeInSeconds = 0;
            _spectrum.RealTimeInSeconds = 0;
        }

        /// <summary>
        /// Starts acquiring a spectrum for a given number of seconds
        /// </summary>
        /// <param name="timeInSeconds">During of acquisition (in seconds); zero means
        /// an "infinite" acquisition time</param>
        /// <param name="isLiveTime">Whether the live time should be timeInSeconds</param>
        public void StartAcquiring(int timeInSeconds, bool isLiveTime = true)
        {
            if (IsAcquiring)
                StopAcquiring();

            IsLiveTimePreset = true;
            IsGrossCountsPreset = false;

            ResetSpectrum();

            IsAcquiring = true;
            AcquisitionStartTime = DateTime.Now;
            if (timeInSeconds == 0)
                timeInSeconds = int.MaxValue;
            AcquisitionTimeInSeconds = timeInSeconds;
            Timer.Start();

            FireAcquisitionStarted(DemoDetectorName + SerialNumber, AcquisitionStartTime.Value);
        }

        /// <summary>
        /// Starts acquiring until a given number of gross counts have been obtained
        /// within the specified channel range
        /// </summary>
        /// <param name="grossCounts">Goal counts to acquire</param>
        /// <param name="startChannel">Starting channel</param>
        /// <param name="endChannel">Ending channel</param>
        public void StartAcquiring(uint grossCounts, int startChannel, int endChannel)
        {
            IsGrossCountsPreset = true;

            int numSecondsToCount = 0;
            if (TotalSpectrumCounts != 0)
            {
                double numMinutesToCount = Math.Round(grossCounts / (double)TotalSpectrumCounts);
                numSecondsToCount = (int)Math.Round(numMinutesToCount * TimeConstants.SecondsPerMinute, 0);
            }

            StartAcquiring(numSecondsToCount);
        }

        /// <summary>
        /// Stops the spectrum acquisition, marks the acquisition as completed,
        /// and raises the AcquisitionStopped event
        /// </summary>
        public void StopAcquiring()
        {
            StopAcquiringWithoutRaisingAcquisitionStopped();
            FireAcquisitionStopped(DemoDetectorName + SerialNumber, DateTime.Now);
        }

        #endregion

        #endregion

        #region Event raisers

        /// <summary>
        /// Fires/raises the IDetector.AcquisitionCompleted event
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that completed its acquisition</param>
        /// <param name="spectrum">Spectrum that was acquired</param>
        protected virtual void FireAcquisitionCompleted(string mcaId, Spectrum spectrum)
        {
            var handler = AcquisitionCompleted;
            if (handler != null)
                handler(mcaId, spectrum);
        }

        /// <summary>
        /// Fires/raises the IDetector.AcquisitionStarted event
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that started acquiring</param>
        /// <param name="startTime">Time at which acquisition started</param>
        protected virtual void FireAcquisitionStarted(string mcaId, DateTime startTime)
        {
            var handler = AcquisitionStarted;
            if (handler != null)
                handler(mcaId, startTime);
        }

        /// <summary>
        /// Fires/raises the IDetector.AcquisitionStopped event
        /// </summary>
        /// <param name="mcaId">Identifier for the MCA that stopped acquiring</param>
        /// <param name="stopTime">Time at which acquisition stopped</param>
        protected virtual void FireAcquisitionStopped(string mcaId, DateTime stopTime)
        {
            var handler = AcquisitionStopped;
            if (handler != null)
                handler(mcaId, stopTime);
        }

        /// <summary>
        /// Fires/raises the IDetector.Connected event
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that connected</param>
        protected virtual void FireConnected(string mcaId)
        {
            var handler = Connected;
            if (handler != null)
                handler(mcaId);
        }

        /// <summary>
        /// Fires/raises the IDetector.Disconnected event
        /// </summary>
        /// <param name="mcaId">Identifier of the MCA that was disconnected</param>
        /// <param name="reason">Reason that the detector was disconnected</param>
        protected virtual void FireDisconnected(string mcaId, string reason)
        {
            var handler = Disconnected;
            if (handler != null)
                handler(mcaId, reason);
        }

        #endregion

        #region Private helpers

        /// <summary>
        /// Simulates a uniform distribution of the counts over the entire spectrum by adding
        /// counts to the spectrum in the correct proportion.
        /// </summary>
        private void AddCountsToSpectrum()
        {
            if (Spectrum == null || Spectrum.SpectrumArray == null)
                return;

            double countsPerSecond = TotalSpectrumCounts / TimeConstants.SecondsPerMinute;
            double delayInSeconds = AcquisitionUpdateFrequencyInMilliseconds / TimeConstants.MillisecondsPerSecond;

            var numChannels = (int)Math.Round(countsPerSecond * delayInSeconds, 0);
            var rnd = new Random();
            for (int i = 0; i < numChannels; i++)
            {
                int channelNumber = rnd.Next(SpectrumLength);
                Spectrum.SpectrumArray[channelNumber]++;
            }
        }

        /// <summary>
        /// Initializes this detector by setting the predefined values for fine gain,
        /// LLD, noise, and voltage. The initial boolean states are: acquisition has
        /// completed, no acquisition is in progress, the live time is not preset,
        /// and the detector's connected state is specified as an argument. The
        /// spectrum is also reset.
        /// </summary>
        /// <param name="isConnected">True if the detector state should be marked as
        /// connected; false for disconnected</param>
        private void InitializeDetector(bool isConnected)
        {
            StopTimerIfEnabled();

            FineGain = InitialFineGain;
            LowLevelDiscriminator = InitialLowLevelDiscriminator;
            Noise = InitialNoise;
            Voltage = InitialVoltage;

            HasAcquisitionCompleted = false;
            IsAcquiring = false;
            IsConnected = isConnected;
            IsLiveTimePreset = false;

            ResetSpectrum();
        }

        /// <summary>
        /// Handles the ITimer.Elapsed event by continuing to acquire or completing the
        /// acquisition depending on how much time has elapsed.
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Arguments for the timer event</param>
        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            StopTimerIfEnabled();

            if (Spectrum == null || AcquisitionStartTime == null)
                return;

            double elapsedTime = e.SignalTime.Subtract(AcquisitionStartTime.Value).TotalSeconds;
            if (elapsedTime >= AcquisitionTimeInSeconds)
            {
                Spectrum.LiveTimeInSeconds = AcquisitionTimeInSeconds;
                Spectrum.RealTimeInSeconds = AcquisitionTimeInSeconds;
                StopAcquiringWithoutRaisingAcquisitionStopped();
                HasAcquisitionCompleted = true;
                FireAcquisitionCompleted(DemoDetectorName + SerialNumber, Spectrum);
            }
            else
            {
                AddCountsToSpectrum();
                Spectrum.LiveTimeInSeconds = (int)Math.Round(elapsedTime, 0);
                Spectrum.RealTimeInSeconds = (int)Math.Round(elapsedTime, 0);
                Timer.Start();
            }
        }

        /// <summary>
        /// Stops the spectrum acquisition, marks the acquisition as completed,
        /// but does not raise the AcquisitionStopped event
        /// </summary>
        private void StopAcquiringWithoutRaisingAcquisitionStopped()
        {
            StopTimerIfEnabled();

            HasAcquisitionCompleted = false;
            IsAcquiring = false;
            IsLiveTimePreset = false;
            AcquisitionTimeInSeconds = 0;
            AcquisitionStartTime = null;
        }

        /// <summary>
        /// Stops the acquisition delay timer if it is enabled/running
        /// </summary>
        private void StopTimerIfEnabled()
        {
            if (Timer.Enabled)
                Timer.Stop();
        }

        #endregion
    }
}
