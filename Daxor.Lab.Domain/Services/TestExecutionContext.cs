﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.Domain.Services
{
    /// <summary>
    /// Represents a base test execution context for a particular type of test
    /// </summary>
    /// <typeparam name="T">Type of ITest being executed</typeparam>
    public abstract class TestExecutionContext<T> : ITestExecutionContext where T : ITest
    {
        #region Fields

        /// <summary>
        /// Backing field for the status of the executing test
        /// </summary>
        private TestStatus _executingTestStatus;

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the TestExecutionContext class based on
        /// a given test instance and detector used to acquire counts for the test.
        /// </summary>
        /// <param name="test">Test instance to be executed</param>
        /// <param name="detector">Detector used to acquire counts</param>
        protected TestExecutionContext(T test, IDetector detector)
        {
            // ReSharper disable CompareNonConstrainedGenericWithNull (I'm not sure why I need this because T must be an ITest, which could be null)
            if (test == null)                                       // ReSharper restore CompareNonConstrainedGenericWithNull
                throw new ArgumentNullException("test");
            if (detector == null)
                throw new ArgumentNullException("detector");
            
            ExecutingTestOfSpecificType = test;
            Detector = detector;
            ExecutingSample = null;
            HasAcquisitionStarted = false;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the delegate/callback to be invoked once a sample has
        /// had its counts acquired
        /// </summary>
        protected Action<ISample> AcquisitionCompletedCallback { get; set; }

        /// <summary>
        /// Gets or sets the detector used for obtaining counts
        /// </summary>
        public IDetector Detector { get; protected set; }

        /// <summary>
        /// Gets or sets the executing sample
        /// </summary>
        protected ISample ExecutingSample { get; set; }

        /// <summary>
        /// Gets or sets the strongly-typed executing test instance
        /// </summary>
        protected T ExecutingTestOfSpecificType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not acquisition has started
        /// </summary>
        protected bool HasAcquisitionStarted { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Computes the total acquisition time for a given sample
        /// </summary>
        /// <param name="sample">Sample whose acquisition time is to be computed</param>
        /// <returns>Acquisition time (in milliseconds) for the given sample</returns>
        public abstract int ComputeSampleAcquisitionTimeInMilliseconds(ISample sample);

        /// <summary>
        /// Determines the next sample to execute/process based on the work
        /// performed thus far
        /// </summary>
        /// <returns>Sample to be executed/processed next</returns>
        public abstract ISample GetNextSampleToExecute();

        /// <summary>
        /// Provides a way to handle the scenario when the status of the executing
        /// has changed
        /// </summary>
        protected abstract void OnExecutingTestStatusChanged();

        /// <summary>
        /// Processess the background for the test
        /// </summary>
        /// <param name="integral">Number of counts (i.e., integral over the 
        /// region of interest)</param>
        /// <param name="acqusitionTimeInSeconds">Number of sequence for which
        /// the background was acquired</param>
        /// <param name="isAcquisitionCompleted">Whether or not background 
        /// acquisition has finished acquiring</param>
        /// <param name="spectrum">Spectrum for the background; null by default</param>
        /// <param name="isOverridden">Whether or not the background value has 
        /// been overridden</param>
        public abstract void ProcessBackground(
            int integral,
            int acqusitionTimeInSeconds,
            bool isAcquisitionCompleted,
            Spectrum spectrum = null,
            bool isOverridden = false);

        /// <summary>
        /// Processes the given spectrum for the given sample by assigning the spectrum array,
        /// live time, and real time of the spectrum to the sample. (Extenders of this class 
        /// can use this method to apply additional spectrscopy computations.)
        /// </summary>
        /// <param name="sample">Sample associated with the given spectrum</param>
        /// <param name="spectrum">Spectrum to be processed</param>
        /// <param name="isAcquisitionCompleted">Whether or not the spectrum represents
        /// a complete acquisition</param>
        protected virtual void ProcessSpectrum(ISample sample, Spectrum spectrum, bool isAcquisitionCompleted = false)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");
            if (spectrum == null)
                throw new ArgumentNullException("spectrum");

            sample.Spectrum.SpectrumArray = spectrum.SpectrumArray;
            sample.Spectrum.LiveTimeInSeconds = spectrum.LiveTimeInSeconds;
            sample.Spectrum.RealTimeInSeconds = spectrum.RealTimeInSeconds;
        }

        #endregion

        #region ITestExecutionContext

        #region Properties

        /// <summary>
        /// Gets or sets the ISample that represents the background radiation sample
        /// </summary>
        public ISample BackgroundSample
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the executing test
        /// </summary>
        public ITest ExecutingTest
        {
            get { return ExecutingTestOfSpecificType; }
        }

        /// <summary>
        /// Gets the ID of the test associated with this execution context
        /// </summary>
        public Guid ExecutingTestId
        {
            get { return ExecutingTestOfSpecificType.InternalId; }
        }

        /// <summary>
        /// Gets or sets the status of the executing test
        /// </summary>
        public TestStatus ExecutingTestStatus
        {
            get
            {
                return _executingTestStatus;
            }

            set
            {
                if (_executingTestStatus != value)
                {
                    _executingTestStatus = value;
                    OnExecutingTestStatusChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the execution context specific metadata
        /// </summary>
        public object Metadata
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the collection of samples (excluding the background sample), 
        /// ordered by how they should be counted during the test
        /// </summary>
        public abstract IEnumerable<ISample> OrderedSamplesExcludingBackground { get; }

        #endregion

        #region Methods

        /// <summary>
        /// Performs acquisition of currently running sample and invokes the given delegate
        /// once the acquisition has been completed. If acquisition hasn't yet started, the
        /// detector is used to commence acquiring; otherwise, the spectrum is processed
        /// until acquisition has completed.
        /// </summary>
        /// <param name="sample">Sample to acquire</param>
        /// <param name="acquisitionCompletedCallback">Delegate to invoke once acquisition has completed</param>
        /// <exception cref="ArgumentNullException">If either argument is null</exception>
        /// <exception cref="ArgumentOutOfRangeException">If the given sample's preset live time is less than
        /// or equal to zero seconds</exception>
        public virtual void PerformAcquisition(ISample sample, Action<ISample> acquisitionCompletedCallback)
        {
            if (!HasAcquisitionStarted)
            {
                if (sample == null)
                    throw new ArgumentNullException("sample");
                if (acquisitionCompletedCallback == null)
                    throw new ArgumentNullException("acquisitionCompletedCallback");
                if (sample.PresetLiveTimeInSeconds <= 0)
                    throw new ArgumentOutOfRangeException("sample", "Preset live time must be greater than zero");

                AcquisitionCompletedCallback = acquisitionCompletedCallback;

                Detector.StopAcquiring();
                Detector.ResetSpectrum();
                Detector.StartAcquiring(sample.PresetLiveTimeInSeconds);
                ExecutingSample = sample;

                HasAcquisitionStarted = true;
            }
            else
            {
                // Something went wrong with acquisition (maybe aborted), so call back
                // saying we weren't successful.
                // ------
                // Note from Geoff: Andrey and I put this in as the logic seems sound.
                // However, I think there's a race condition where IsAcquiring is false
                // (because the detector is truly done), but HasAcquisitionCompleted
                // is false. Logging more information shows that AcquisitionCompleted
                // is getting fired, and the implementation of both of these properties
                // relies on hardware calls.
                // ------
                // if (!Detector.IsAcquiring && !Detector.HasAcquisitionCompleted)
                // {
                //     _acquisitionCompletedCallback(acquireSample, successfulAcquisiton);
                //     HasAcquisitionStarted = false;
                //     return;
                // }

                // No problems, process as usual.
                ProcessSpectrum(sample, Detector.Spectrum);

                // Done acquiring, so do final processing and call back saying we're done.
                if (Detector.HasAcquisitionCompleted)
                {
                    ProcessSpectrum(sample, Detector.Spectrum, true);
                    AcquisitionCompletedCallback(sample);
                    HasAcquisitionStarted = false;
                }
            }
        }

        #endregion

        #endregion
    }
}
