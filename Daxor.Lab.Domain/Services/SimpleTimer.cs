﻿using System.Runtime.CompilerServices;
using System.Timers;
using Daxor.Lab.Domain.Interfaces;

[assembly: InternalsVisibleTo("Daxor.Lab.Domain.Tests")]

namespace Daxor.Lab.Domain.Services
{
    /// <summary>
    /// Represents a simple timer that raises an event after a certain time
    /// interval has elapsed.
    /// </summary>
    public class SimpleTimer : ITimer
    {
        #region Fields

        /// <summary>
        /// Internal timer used for implementing this timer class
        /// </summary>
        internal readonly Timer InternalTimer;

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the SimpleTimer class with a specified interval (in milliseconds).
        /// </summary>
        /// <param name="intervalInMilliseconds">Timer interval (in milliseconds)</param>
        /// <exception cref="System.ArgumentException">The value of the interval is less 
        /// than or equal to zero, or greater than System.Int32.MaxValue.</exception>
        public SimpleTimer(int intervalInMilliseconds)
        {
            InternalTimer = new Timer(intervalInMilliseconds);
            InternalTimer.Elapsed += OnTimerElapsed;
        }

        #endregion

        #region ITimer members

        /// <summary>
        /// Occurs when the time interval elapses.
        /// </summary>
        public event ElapsedEventHandler Elapsed;

        /// <summary>
        /// Starts the timer (i.e., enables it).
        /// </summary>
        public void Start()
        {
            InternalTimer.Start();
            Enabled = true;
        }

        /// <summary>
        /// Stops the time (i.e., disables it).
        /// </summary>
        public void Stop()
        {
            InternalTimer.Stop();
            Enabled = false;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the timer is enabled or not.
        /// </summary>
        public bool Enabled { get; set; }

        #endregion

        #region Elapsed event methods

        /// <summary>
        /// Handles the internal timer elapsed by raising SimpleTimer.Elapsed.
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Arguments for the timer event</param>
        internal void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            FireElapsed(sender, e);
        }

        /// <summary>
        /// Raises the Elapsed event.
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Arguments for the event</param>
        internal void FireElapsed(object sender, ElapsedEventArgs e)
        {
            var handler = Elapsed;
            if (handler != null)
                handler(sender, e);
        }

        #endregion
    }
}
