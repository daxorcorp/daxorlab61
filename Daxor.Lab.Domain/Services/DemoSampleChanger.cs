﻿using System.Runtime.CompilerServices;
using System.Timers;
using Daxor.Lab.Domain.Interfaces;

[assembly: InternalsVisibleTo("Daxor.Lab.Domain.Tests")]

namespace Daxor.Lab.Domain.Services
{
    /// <summary>
    /// Represents a demo sample changer that can be used in place of a hardware-bound sample changer
    /// </summary>
    public class DemoSampleChanger : ISampleChanger
    {
        #region Constants

        /// <summary>
        /// Default delay between position changes
        /// </summary>
        private const int PositionChangeDelay = 2000;

        #endregion

        #region Fields

        /// <summary>
        /// Concurrency lock for the current position field
        /// </summary>
        private readonly object _currPositionLock = new object();

        /// <summary>
        /// Concurrency lock for the goal position field
        /// </summary>
        private readonly object _goalPositionLock = new object();

        /// <summary>
        /// Concurrency lock for the is-moving field
        /// </summary>
        private readonly object _isMovingLock = new object();

        /// <summary>
        /// Backing field for the timer used to simulate time elapsing
        /// </summary>
        internal readonly ITimer InternalTimer;

        /// <summary>
        /// Backing field for the sample changer's current position
        /// </summary>
        private int _currPosition;

        /// <summary>
        /// Backing field for the sample changer's goal position
        /// </summary>
        private int _goalPosition;

        /// <summary>
        /// Backing field for the is-moving flag
        /// </summary>
        private bool _isMoving;

        #endregion

        #region Static

        /// <summary>
        /// Default initial position for the sample changer
        /// </summary>
        public static readonly int InitialPosition = 24;

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the DemoSampleChanger class using the default delay between
        /// position changes and default initial position.
        /// </summary>
        public DemoSampleChanger()
            : this(new SimpleTimer(PositionChangeDelay))
        {
        }

        /// <summary>
        /// Initializes a new instance of the DemoSampleChanger class using the given delay between
        /// position changes and default initial position.
        /// </summary>
        /// <param name="delayTimer">ITimer instance used to provide delays
        /// between position changes</param>
        public DemoSampleChanger(ITimer delayTimer)
        {
            _currPosition = InitialPosition;

            InternalTimer = delayTimer;
            InternalTimer.Elapsed += OnTimerElapsed;
        }

        #endregion

        #region Private properties

        /// <summary>
        /// Gets or sets the goal position.
        /// </summary>
        private int GoalPosition
        {
            get
            {
                return _goalPosition;
            }

            set
            {
                lock (_goalPositionLock)
                {
                    _goalPosition = value;
                }
            }
        }

        /// <summary>
        /// Gets the ITimer instance used to simulate delays.
        /// </summary>
        private ITimer Timer
        {
            get { return InternalTimer; }
        }

        #endregion

        #region Helper methods

        /// <summary>
        /// Handles the Elapsed event of the internal timer. This is analogous to
        /// the moving process completing. If the goal position has been reached,
        /// PositionChangeEnd and PositionSeekEnd are raised, and the sample changer
        /// will no longer be moving. Otherwise, PositionChangeEnd is raised and
        /// PositionChangeBegin is raised for the next position.
        /// </summary>
        /// <param name="sender">Sender of the event</param>
        /// <param name="e">Arguments for the event</param>
        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            Timer.Stop();

            // Receiving this event means we've moved from one position to the next.
            CurrentPosition = ComputeNextPosition();

            if (CurrentPosition == GoalPosition)
            {
                // We're now at the goal, so stop moving and fire the appropriate events.
                IsMoving = false;
                FirePositionChangeEnd(CurrentPosition);
                FirePositionSeekEnd(CurrentPosition);
            }
            else
            {
                // We haven't reached the goal, so keep moving and fire the appropriate events.
                IsMoving = true;
                FirePositionChangeEnd(CurrentPosition);
                FirePositionChangeBegin(CurrentPosition, ComputeNextPosition());
                Timer.Start();
            }
        }

        /// <summary>
        /// Determines the next available position based on the current position.
        /// </summary>
        /// <returns>Next available position</returns>
        internal int ComputeNextPosition()
        {
            return ComputeNextPosition(CurrentPosition);
        }

        /// <summary>
        /// Determines the next available position based on a specified position.
        /// </summary>
        /// <param name="currPosition">Current position</param>
        /// <returns>Next available position</returns>
        internal int ComputeNextPosition(int currPosition)
        {
            return (currPosition + 1) % NumberOfPositions;
        }

        #endregion

        #region ISampleChanger members

        #region Events

        /// <summary>
        /// Occurs when the sample changer is connected to the computer.
        /// </summary>
        public event SampleChangerConnectedHandler Connected;

        /// <summary>
        /// Occurs when the sample changer has been disconnected from the computer.
        /// </summary>
        public event SampleChangerDisconnectedHandler Disconnected;

        /// <summary>
        /// Occurs when the sample changer encounters an error.
        /// </summary>
        public event ErrorOccurredHandler ErrorOccurred;

        /// <summary>
        /// Occurs when the sample changer starts changing to another position.
        /// </summary>
        public event PositionChangeBeginHandler PositionChangeBegin;

        /// <summary>
        /// Occurs when the sample changer has finished changing to a position.
        /// </summary>
        public event PositionChangeEndHandler PositionChangeEnd;

        /// <summary>
        /// Occurs when the sample changer starts seeking a goal position.
        /// </summary>
        public event PositionSeekBeginHandler PositionSeekBegin;

        /// <summary>
        /// Occurs when the sample changer has cancelled a seek operation.
        /// </summary>
        public event PositionSeekCancelHandler PositionSeekCancelled;

        /// <summary>
        /// Occurs when the sample changer has finished seeking a goal position.
        /// </summary>
        public event PositionSeekEndHandler PositionSeekEnd;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the sample changer's current position.
        /// </summary>
        public int CurrentPosition
        {
            get
            {
                return _currPosition;
            }

            set
            {
                lock (_currPositionLock)
                {
                    _currPosition = value;
                }
            }
        }

        /// <summary>
        /// Gets a value indicating whether the sample changer is moving.
        /// </summary>
        public bool IsMoving
        {
            get
            {
                return _isMoving;
            }

            private set
            {
                lock (_isMovingLock)
                {
                    _isMoving = value;
                }
            }
        }

        /// <summary>
        /// Gets the number of positions the changer is capable of accessing
        /// </summary>
        public int NumberOfPositions
        {
            get { return 25; }
        }

        /// <summary>
        /// Gets the number of milliseconds required to move from one position to
        /// the adjacent position
        /// </summary>
        public int PositionChangeDelayInMilliseconds
        {
            get { return PositionChangeDelay; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Raises the Connected event, as there's really nothing to connect to.
        /// </summary>
        public void Connect()
        {
            FireConnected();
        }

        /// <summary>
        /// Computes the number moves required to get from the given starting position to
        /// the given ending position based on a carousel-style sample changer that only 
        /// moves clockwise
        /// </summary>
        /// <param name="startPosition">Starting position</param>
        /// <param name="endPosition">Ending position</param>
        /// <returns>Number of moves to get from the starting position to the ending position</returns>
        public int GetNumberOfMovesBetween(int startPosition, int endPosition)
        {
            return endPosition - (startPosition % NumberOfPositions);
        }

        /// <summary>
        /// Raises the Disconnected event, as there's really nothing to disconnect from.
        /// </summary>
        public void Disconnect()
        {
            FireDisconnected("Disconnect() explicitly called on DemoSampleChanger");
        }

        /// <summary>
        /// Moves the sample changer to a specified goal position.
        /// </summary>
        /// <remarks>
        /// If the specified goal position is the same as the current position, the
        /// Seek/Change events will still fire, but no timer will be used.
        /// </remarks>
        /// <param name="goalPosition">Goal position; -1 stops the sample changer; values
        /// outside 1..MaximumPosition will have no effect</param>
        public void MoveTo(int goalPosition)
        {
            GoalPosition = goalPosition;

            if (GoalPosition == -1)
            {
                // Stop moving.
                Stop();
            }
            else if ((GoalPosition < 1) || (GoalPosition > NumberOfPositions))
            {
                // Invalid goal; do nothing.
            }
            else if (GoalPosition == CurrentPosition)
            {
                // Being asked to move to where we already are. Raise the
                // events as usual, but don't involve the timer.
                if (Timer.Enabled)
                    Timer.Stop();

                IsMoving = true;
                FirePositionSeekBegin(CurrentPosition, GoalPosition);
                FirePositionChangeBegin(CurrentPosition, GoalPosition);

                IsMoving = false;
                FirePositionChangeEnd(CurrentPosition);
                FirePositionSeekEnd(CurrentPosition);
            }
            else
            {
                // Being asked to go to a new position. (Do nothing if the timer is already
                // enabled (i.e., moving somewhere already).)
                if (Timer.Enabled == false)
                {
                    IsMoving = true;
                    FirePositionSeekBegin(CurrentPosition, GoalPosition);
                    FirePositionChangeBegin(CurrentPosition, ComputeNextPosition());
                    Timer.Start();
                }
            }
        }

        /// <summary>
        /// Stops the sample changer, fires PositionChangeEnd and PositionSeekCancelled
        /// based on the current position, and stops the timer (if active).
        /// </summary>
        /// <remarks>
        /// If the sample changer is not moving, this method has no effect.
        /// </remarks>
        public void Stop()
        {
            if (IsMoving)
            {
                FirePositionChangeEnd(CurrentPosition);
                FirePositionSeekCancelled(CurrentPosition, GoalPosition);
            }

            IsMoving = false;
            if (Timer.Enabled)
                Timer.Stop();
        }
        /// <summary>
        /// Check to see if the sample changer is receiving power.
        /// </summary>
        /// <returns>True if the sample changer is receiving power, false otherwise</returns>
        /// <remarks>Since the DemoSampleChanger is not actually a sample changer, this should always return true</remarks>
        public bool IsPoweredUp()
        {
            return true;
        }

        #endregion

        #endregion

        #region ISampleChanger event raisers

        /// <summary>
        /// Fires/raises the ISampleChanger.Connected event
        /// </summary>
        protected virtual void FireConnected()
        {
            var handler = Connected;
            if (handler != null)
                handler();
        }

        /// <summary>
        /// Fires/raises the ISampleChanger.Disconnected event
        /// </summary>
        /// <param name="reason">Reason the sample changer was disconnected</param>
        protected virtual void FireDisconnected(string reason)
        {
            var handler = Disconnected;
            if (handler != null)
                handler(reason);
        }

        /// <summary>
        /// Fires/raises the ISampleChanger.ErrorOccurred event
        /// </summary>
        /// <param name="errorCode">Error code associated with event</param>
        /// <param name="message">Message pertaining to the error</param>
        protected virtual void FireErrorOccurred(int errorCode, string message)
        {
            var handler = ErrorOccurred;
            if (handler != null)
                handler(errorCode, message);
        }

        /// <summary>
        /// Fires/raises the ISampleChanger.PositionChangerBegin event
        /// </summary>
        /// <param name="oldPosition">Old (current) position</param>
        /// <param name="newPosition">New (target) position</param>
        protected virtual void FirePositionChangeBegin(int oldPosition, int newPosition)
        {
            var handler = PositionChangeBegin;
            if (handler != null)
                handler(oldPosition, newPosition);
        }

        /// <summary>
        /// Fires/raises the ISampleChanger.PositionChanged event
        /// </summary>
        /// <param name="newPosition">New (current) position</param>
        protected virtual void FirePositionChangeEnd(int newPosition)
        {
            var handler = PositionChangeEnd;
            if (handler != null)
                handler(newPosition);
        }

        /// <summary>
        /// Fires/raises the ISampleChanger.SeekBegin event
        /// </summary>
        /// <param name="currPosition">Current position</param>
        /// <param name="goalPosition">Goal position</param>
        protected virtual void FirePositionSeekBegin(int currPosition, int goalPosition)
        {
            var handler = PositionSeekBegin;
            if (handler != null)
                handler(currPosition, goalPosition);
        }

        /// <summary>
        /// Fires/raises the ISampleChanger.SeekCancelled event
        /// </summary>
        /// <param name="currPosition">Current position</param>
        /// <param name="goalPosition">Prior goal position</param>
        protected virtual void FirePositionSeekCancelled(int currPosition, int goalPosition)
        {
            var handler = PositionSeekCancelled;
            if (handler != null)
                handler(currPosition, goalPosition);
        }

        /// <summary>
        /// Fires/raises the ISampleChanger.PositionSeekEnd event
        /// </summary>
        /// <param name="currPosition">Current position</param>
        protected virtual void FirePositionSeekEnd(int currPosition)
        {
            var handler = PositionSeekEnd;
            if (handler != null)
                handler(currPosition);
        }

        #endregion
    }
}
