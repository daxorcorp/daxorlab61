﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.Domain.Services
{
    /// <summary>
    /// Helper methods for computing statistical measures.
    /// </summary>
    public static class StatisticsHelpers
    {
        /// <summary>
        /// Computes the allowable difference between the two given numbers and a coefficient
        /// of variance.
        /// </summary>
        /// <param name="value1">First value</param>
        /// <param name="value2">Second value</param>
        /// <param name="coeffOfVariance">Coefficient of variance (default is
        /// Daxor.Lab.Domain.Common.StatisticalConstants.DefaultCoefficientOfVariance)
        /// </param> 
        /// <returns>The maximum difference between the two given values that still
        /// guarantees statistical significance</returns>
        public static int ComputeAllowableDifference(int value1, int value2, double coeffOfVariance = StatisticalConstants.DefaultCoefficientOfVariance)
        {
            double higherValue = Math.Max(value1, value2);
            double lowerValue = Math.Min(value1, value2);
            double computedVariance = Math.Sqrt(higherValue + Math.Pow(coeffOfVariance * higherValue / 100, 2) +
                lowerValue + Math.Pow(coeffOfVariance * lowerValue / 100, 2));
            return (int)Math.Round(StatisticalConstants.CertaintyFactor * computedVariance, 0);
        }

        public static int ComputeAllowableDifference(List<int> values, double coeffOfVariance = StatisticalConstants.DefaultCoefficientOfVariance)
        {
            if (values == null)
                throw new ArgumentNullException("values");
            if (values.Count != 2)
                throw new NotSupportedException("Exactly two values are needed for computing the difference");
            return ComputeAllowableDifference(values[0], values[1], coeffOfVariance);
        }
    }
}
