﻿using System;
using System.Globalization;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities.Base;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Domain.Services;

namespace Daxor.Lab.Domain.Entities
{
    /// <summary>
    /// Represents a patient.
    /// </summary>
    public class Patient : Entity, IPatient
    {
        #region Fields

        /// <summary>
        /// Backing field for the patient's first name.
        /// </summary>
        private string _firstName;

        /// <summary>
        /// Backing field for the patient's last name.
        /// </summary>
        private string _lastName;

        /// <summary>
        /// Backing field for the patient's middle name.
        /// </summary>
        private string _middleName;

        /// <summary>
        /// Backing field for ID assigned to the patient by the hospital.
        /// </summary>
        private string _hospitalPatientId;

        /// <summary>
        /// Backing field for the patient's date of birth.
        /// </summary>
        private DateTime? _dateOfBirth;

        /// <summary>
        /// Backing field for the patient's gender.
        /// </summary>
        private GenderType _gender = GenderType.Unknown;

        /// <summary>
        /// Backing field for the patient's height (in centimeters).
        /// </summary>
        private double _heightInCm = -1f;

        /// <summary>
        /// Backing field for the patient's weight (in kilograms).
        /// </summary>
        private double _weightInKg = -1f;

        /// <summary>
        /// Backing field for the patient's desired measurement system (e.g., metric or US).
        /// </summary>
        private MeasurementSystem _measurementSystem = MeasurementSystem.Metric;

		/// <summary>
		/// Indicates whether the patient is an amputee or not
		/// </summary>
	    private bool _isAmputee;

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the Patient class with an internal ID.
        /// </summary>
        public Patient()
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InternalId = IdentityGenerator.NewSequentialGuid();  // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the patient's date of birth.
        /// </summary>
        public DateTime? DateOfBirth
        {
            get { return _dateOfBirth; }
            set { SetValue(ref _dateOfBirth, "DateOfBirth", value); }
        }

        /// <summary>
        /// Gets or sets the patient's gender.
        /// </summary>
        public GenderType Gender
        {
            get { return _gender; }
	        set { SetValue(ref _gender, "Gender", value); }
        }

        /// <summary>
        /// Gets or sets the hospital's patient ID.
        /// </summary>
        public string HospitalPatientId
        {
            get { return _hospitalPatientId; }
            set { SetValue(ref _hospitalPatientId, "HospitalPatientId", value); }
        }

        /// <summary>
        /// Gets or sets the measurement system in which the height and weight
        /// should be used.
        /// </summary>
        public MeasurementSystem MeasurementSystem
        {
            get
            {
                return _measurementSystem;
            }

            set
            {
                if (SetValue(ref _measurementSystem, "MeasurementSystem", value))
                    FirePropertyChanged("HeightInCurrentMeasurementSystem", "WeightInCurrentMeasurementSystem");
            }
        }

		/// <summary>
		/// Gets or sets the patient's amputee status.
		/// </summary>
	    public bool IsAmputee
	    {
			get { return _isAmputee; }
			set
			{
			    SetValue(ref _isAmputee, "IsAmputee", value);
			}
	    }

        #region Name-related properties

        /// <summary>
        /// Gets or sets the patient's first name. Setting the first name also fires
        /// PropertyChanged on FullName.
        /// </summary>
        public string FirstName
        {
            get
            {
                return _firstName;
            }

            set
            {
                if (SetValue(ref _firstName, "FirstName", value))
                    FirePropertyChanged("FullName");
            }
        }

        /// <summary>
        /// Gets a formatted version of the patient's full name.
        /// </summary>
        public string FullName
        {
            get
            {
                if (!string.IsNullOrEmpty(LastName) && !string.IsNullOrEmpty(FirstName))
                {
                    return (LastName + ", " + FirstName + " " + MiddleName).ToString(CultureInfo.InvariantCulture).Trim();
                }

                if (!string.IsNullOrEmpty(LastName) || !string.IsNullOrEmpty(FirstName))
                {
                    if (string.IsNullOrEmpty(LastName))
                        return FirstName;

                    return LastName;
                }
                
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets or sets the patient's last name. Setting the last name also fires
        /// PropertyChanged on FullName.
        /// </summary>
        public string LastName
        {
            get
            {
                return _lastName;
            }

            set
            {
                if (SetValue(ref _lastName, "LastName", value))
                    FirePropertyChanged("FullName");
            }
        }

        /// <summary>
        /// Gets or sets the patient's middle name. Setting the middle name also fires
        /// PropertyChanged on FullName.
        /// </summary>
        public string MiddleName
        {
            get
            {
                return _middleName;
            }

            set
            {
                if (SetValue(ref _middleName, "MiddleName", value))
                    FirePropertyChanged("FullName");
            }
        }

        #endregion

        #region Height-related properties

        /// <summary>
        /// Gets or sets the patient's height (in centimeters). Setting the height
        /// also fires PropertyChanged on HeightInCurrentMeasurementSystem.
        /// </summary>
        public double HeightInCm
        {
            get
            {
                return _heightInCm;
            }

            set
            {
                if (SetValue(ref _heightInCm, "HeightInCm", value))
                    FirePropertyChanged("HeightInCurrentMeasurementSystem");
            }
        }

        /// <summary>
        /// Gets the patient's height value in the current measurement system.
        /// </summary>
        public double HeightInCurrentMeasurementSystem
        {
            get
            {
                if (MeasurementSystem == MeasurementSystem.English)
                    return HeightInInches;

                return HeightInCm;
            }
        }

        /// <summary>
        /// Gets the patient's height (in inches).
        /// </summary>
        public double HeightInInches
        {
            get
            {
                return UnitsOfMeasurementConverter.ConvertHeightToEnglish(_heightInCm);
            }
        }

        #endregion

        #region Weight-related properties

        /// <summary>
        /// Gets the patient's weight value in the current measurement system.
        /// </summary>
        public double WeightInCurrentMeasurementSystem
        {
            get
            {
                if (MeasurementSystem == MeasurementSystem.English)
                    return WeightInLb;

                return WeightInKg;
            }
        }

        /// <summary>
        /// Gets or sets the patient's weight (in kilograms). Setting the weight
        /// also fires PropertyChanged on WeightInCurrentMeasurementSystem.
        /// </summary>
        public double WeightInKg
        {
            get
            {
                return _weightInKg;
            }

            set
            {
                if (SetValue(ref _weightInKg, "WeightInKg", value))
                    FirePropertyChanged("WeightInCurrentMeasurementSystem");
            }
        }

        /// <summary>
        /// Gets the patient's weight (in pounds).
        /// </summary>
        public double WeightInLb
        {
            get
            {
                return UnitsOfMeasurementConverter.ConvertWeightToEnglish(_weightInKg);
            }
        }

        #endregion

        #endregion

        #region Static methods

        /// <summary>
        /// Computes the number of years that occur between the given
        /// date of birth and the given end-date.
        /// </summary>
        /// <param name="dateOfBirth">Beginning date (date of birth)</param>
        /// <param name="endDate">Ending date</param>
        /// <returns>Number of years that have passed; null if the date of
        /// birth is null</returns>
        public static int? GetAgeInYears(DateTime? dateOfBirth, DateTime endDate)
        {
            if (!dateOfBirth.HasValue) return null;

            int age = endDate.Year - dateOfBirth.Value.Year;
            if (endDate.Month < dateOfBirth.Value.Month || (endDate.Month == dateOfBirth.Value.Month && endDate.Day < dateOfBirth.Value.Day)) age--;

            return age;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the number of years that occur between the patient's
        /// date of birth and the given date.
        /// </summary>
        /// <param name="endDate">Ending date</param>
        /// <returns>Number of years that have passed</returns>
        public int? GetAgeInYears(DateTime endDate)
        {
            return GetAgeInYears(DateOfBirth, endDate);
        }

        #endregion
    }
}
