﻿using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.Domain.Entities
{
    /// <summary>
    /// Exposes progress of an executing test.
    /// </summary>
    public class TestProgressMetadata : ValueObject<TestProgressMetadata>
    {
        /// <summary>
        /// Initializes a new instance of the TestProgressMetadata class based on the specified app module key.
        /// </summary>
        /// <param name="appModuleKey">Key for the app module that is executing the test</param>
        public TestProgressMetadata(string appModuleKey)
        {
            AppModuleKey = appModuleKey;
        }

        /// <summary>
        /// Gets the app module key that started the test.
        /// </summary>
        public string AppModuleKey { get; private set; }

        /// <summary>
        /// Gets or sets the friendly display name for the test.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or sets the counts for the currently running sample.
        /// </summary>
        public int UpdatedSampleCounts { get; set; }
    }
}
