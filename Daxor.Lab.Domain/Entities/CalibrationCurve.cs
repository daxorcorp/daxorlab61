﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.Domain.Entities
{
    /// <summary>
    /// Represents a calibration curve that describes the efficiency of a detector
    /// at various energies.
    /// </summary>
    public class CalibrationCurve : ICalibrationCurve
    {
        #region Fields

        /// <summary>
        /// Private backing field for the list of channels
        /// </summary>
        private readonly List<double> _channels  = new List<double>();

        /// <summary>
        /// Private backing field for the list of energies
        /// </summary>
        private readonly List<double> _energies = new List<double>();

        #endregion

        #region Ctors

        /// <summary>
        /// Initializes a new instance of the CalibrationCurve class based on the given ID, 
        /// slope, and y-offset.
        /// </summary>
        /// <param name="id">Identifier for the calibration curve</param>
        /// <param name="slope">Slope of the curve</param>
        /// <param name="offset">y-offset of the curve</param>
        public CalibrationCurve(int id, double slope, double offset)
        {
            CurveId = id;
            Offset = offset;
            Slope = slope;
        }
        
        /// <summary>
        /// Initializes a new instance of the CalibrationCurve class based on the given ID,
        /// list of channels, and list of energies.
        /// </summary>
        /// <param name="id">Identifier for the calibration curve</param>
        /// <param name="channels">List of channels</param>
        /// <param name="energies">List of energies for the channels</param>
        public CalibrationCurve(int id, IEnumerable<double> channels, IEnumerable<double> energies)
        {
            if (channels == null)
                throw new ArgumentNullException("channels");
            if (energies == null)
                throw new ArgumentNullException("energies");
            // ReSharper disable PossibleMultipleEnumeration (The suggested refactoring would have done the same amount of work and would have been less readable)
            if (channels.Count() != energies.Count())
                throw new Exception("Number of channels should match the number of energies"); 

            CurveId = id;

            _channels.AddRange(channels);
            _energies.AddRange(energies); // ReSharper restore PossibleMultipleEnumeration

            BuildLinearCurve();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the identifier for the calibration curve.
        /// </summary>
        public int CurveId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the y-offset of the calibration curve.
        /// </summary>
        public double Offset
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the slope of the calibration curve.
        /// </summary>
        public double Slope
        {
            get;
            private set;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Computes the channel the corresponds to the given energy level (in keV)
        /// </summary>
        /// <param name="energyInKeV">Energy value (in keV)</param>
        /// <returns>Channel corresponding to the given energy level</returns>
        public int GetChannel(double energyInKeV)
        {
            int channel;

            try
            {
                channel = (int)((energyInKeV - Offset) / Slope);
            }
            catch
            {
                channel = -1;
            }

            return channel;
        }

        /// <summary>
        /// Computes the energy (in keV) that corresponds to the given channel.
        /// </summary>
        /// <param name="channel">Channel number</param>
        /// <returns>Energy level (in keV) corresponding to the given channel</returns>
        public double GetEnergy(double channel)
        {
            return (Slope * channel) + Offset;
        }

        #endregion

        #region Internal helper methods

        /// <summary>
        /// Builds the calibration curve, thus producing a slope and offset, using a least-squares fit.
        /// The x-axis represents the channel, and the y-axis represents the energy (in keV).
        /// </summary>
        internal void BuildLinearCurve()
        {
            double m, b;
            int i;

            double x1 = 0.0;
            double y1 = 0.0;
            double xy = 0.0;
            double x2 = 0.0;
            int numPoints = _channels.Count();

            for (i = 0; i < numPoints; i++)
            {
                x1 = x1 + _channels[i];
                y1 = y1 + _energies[i];
                xy = xy + (_channels[i] * _energies[i]);
                x2 = x2 + (_channels[i] * _channels[i]);
            }

            double j = (numPoints * x2) - (x1 * x1);
            
            // Used to be (j != 0), but "0" can't really be represented
            if (Math.Abs(j - 0) > 1E-300)    
            {
                m = ((numPoints * xy) - (x1 * y1)) / j;
                m = Math.Floor((1.0E3 * m) + 0.5) / 1.0E3;
                b = ((y1 * x2) - (x1 * xy)) / j;
                b = Math.Floor((1.0E3 * b) + 0.5) / 1.0E3;
            }
            else
            {
                m = 0;
                b = 0;
            }  

            Slope = m;
            Offset = b;
        }

        #endregion
    }
}
