﻿using System;
using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.Domain.Entities
{
    /// <summary>
    /// Represents a spectrum of acquired counts for a sample.
    /// </summary>
    public class Spectrum : ValueObject<Spectrum>
    {
        #region Fields

        /// <summary>
        /// Backing field for the spectrum's live time (in seconds).
        /// </summary>
        private double _liveTimeInSeconds;

        /// <summary>
        /// Backing field for the spectrum's real time (in seconds).
        /// </summary>
        private double _realtimeInSeconds;

        /// <summary>
        /// Backing field for the spectrum's count array.
        /// </summary>
        private uint[] _spectrumArray;

        #endregion

        #region Properties

        #region Count times

        /// <summary>
        /// Gets the dead time (i.e., the difference between real and live time).
        /// </summary>
        public double DeadTimeInSeconds
        {
            get { return Math.Round(RealTimeInSeconds - LiveTimeInSeconds, 2); }
        }

        /// <summary>
        /// Gets or sets the live time for which a sample was acquired. Setting the live
        /// time also affects DeadTimeInSeconds, so a property-changed notification is sent.
        /// </summary>
        public double LiveTimeInSeconds
        {
            get
            {
                return _liveTimeInSeconds;
            }

            set
            {
                if (SetValue(ref _liveTimeInSeconds, "LiveTimeInSeconds", value))
                    FirePropertyChanged("DeadTimeInSeconds");
            }
        }

        /// <summary>
        /// Gets or sets the real time for which a sample was acquired. Setting the real time
        /// also affects DeadTimeInSeconds, so a property-changed notification is sent.
        /// </summary>
        public double RealTimeInSeconds
        {
            get
            {
                return _realtimeInSeconds;
            }

            set
            {
                if (SetValue(ref _realtimeInSeconds, "RealTimeInSeconds", value))
                    FirePropertyChanged("DeadTimeInSeconds");
            }
        }

        #endregion

        /// <summary>
        /// Gets or sets an array where each element represents the number of counts for
        /// channel i+1 (e.g., SpectrumArray[0] holds the number of counts for channel 1).
        /// </summary>
        public uint[] SpectrumArray
        {
            get { return _spectrumArray; }
            set { SetValue(ref _spectrumArray, "SpectrumArray", value); }
        }

        #endregion

        #region Static methods

        /// <summary>
        /// Returns an empty Spectrum instance (i.e., zero live time, zero real time, and
        /// a null array).
        /// </summary>
        /// <returns>An empty Spectrum instance</returns>
        public static Spectrum Empty()
        {
            return new Spectrum { LiveTimeInSeconds = 0, RealTimeInSeconds = 0, SpectrumArray = null };
        }

        #endregion
    }
}
