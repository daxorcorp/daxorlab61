﻿using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities.Base;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Domain.Services;

namespace Daxor.Lab.Domain.Entities
{
    /// <summary>
    /// Represents a base sample to be measured using a detector.
    /// </summary>
    public abstract class Sample : Entity, ISample
    {
        #region Fields

        /// <summary>
        /// Backing field for the sample's display name.
        /// </summary>
        private string _displayName;

        /// <summary>
        /// Backing field for the sample's execution status.
        /// </summary>
        private SampleExecutionStatus _executionStatus = SampleExecutionStatus.Idle;

        /// <summary>
        /// Backing field for the sample's position/index.
        /// </summary>
        private int _position = -1;

        /// <summary>
        /// Backing field for sample's spectrum.
        /// </summary>
        private Spectrum _spectrum;

        /// <summary>
        /// Backing field for the sample's preset live time (in seconds).
        /// </summary>
        private int _presetLiveTimeInSeconds;

        /// <summary>
        /// Backing field for whether or not the sample is being counted.
        /// </summary>
        private bool _isCounting;
        
        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the Sample class and sets its internal ID.
        /// </summary>
        protected Sample()
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InternalId = IdentityGenerator.NewSequentialGuid();     // ReSharper restore DoNotCallOverridableMethodsInConstructor
            Spectrum = new Spectrum();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the sample's user-friendly display name.
        /// </summary>
        public string DisplayName
        {
            get { return _displayName; }
            set { SetValue(ref _displayName, "DisplayName", value); }
        }

        /// <summary>
        /// Gets or sets the sample's execution status
        /// </summary>
        public SampleExecutionStatus ExecutionStatus
        {
            get { return _executionStatus; }
            set { SetValue(ref _executionStatus, "ExecutionStatus", value); }
        }

        /// <summary>
        /// Gets or sets the sample's position index.
        /// </summary>
        public int Position
        {
            get { return _position; }
            set { SetValue(ref _position, "Position", value); }
        }

        /// <summary>
        /// Gets or sets the Spectrum data.
        /// </summary>
        public Spectrum Spectrum
        {
            get { return _spectrum; }
            set { SetValue(ref _spectrum, "Spectrum", value); }
        }

        /// <summary>
        /// Gets or sets the sample's acquisition preset live time (in seconds).
        /// </summary>
        public int PresetLiveTimeInSeconds
        {
            get { return _presetLiveTimeInSeconds; }
            set { SetValue(ref _presetLiveTimeInSeconds, "PresetLiveTimeInSeconds", value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether or not the sample is being counted.
        /// </summary>
        public bool IsCounting
        {
            get { return _isCounting; }
            set { SetValue(ref _isCounting, "IsCounting", value); }
        }

        #endregion
    }
}
