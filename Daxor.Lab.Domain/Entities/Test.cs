﻿using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities.Base;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.Domain.Services;

namespace Daxor.Lab.Domain.Entities
{
    /// <summary>
    /// Represents a base test that is associated with a given type of sample.
    /// </summary>
    /// <typeparam name="TSample">Type of sample used in this type of test</typeparam>
    public abstract class Test<TSample> : Entity, ITest where TSample : ISample
    {
        #region Fields

        /// <summary>
        /// Backing field for the test's analyst.
        /// </summary>
        private string _analyst;

        /// <summary>
        /// Backing field for the test's background count.
        /// </summary>
        private int _backgroundCount;

        /// <summary>
        /// Backing field for the tests background count time (in seconds).
        /// </summary>
        private double _backgroundTimeInSeconds;

        /// <summary>
        /// Backing field for the test's comments.
        /// </summary>
        private string _comments;

        /// <summary>
        /// Backing field for test's timestamp.
        /// </summary>
        private DateTime _date;

        /// <summary>
        /// Backing field for the test's location.
        /// </summary>
        private string _location;

        /// <summary>
        /// Backing field for the duration (in seconds) for each of the test's samples.
        /// </summary>
        private int _sampleDurationInSeconds;

        /// <summary>
        /// Backing field for the test's sample layout ID.
        /// </summary>
        private int _sampleLayoutId;

        /// <summary>
        /// Backing field for the test's status.
        /// </summary>
        private TestStatus _status;

        /// <summary>
        /// Backing field for the system ID associated with this test.
        /// </summary>
        private string _systemId;

        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the Test class by by initializing the samples list, 
        /// comments, internal ID, status, and date.
        /// </summary>
        protected Test()
        {
            InnerSamples = new List<TSample>();
            Comments = string.Empty;
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            InternalId = IdentityGenerator.NewSequentialGuid();  // ReSharper restore DoNotCallOverridableMethodsInConstructor
            Status = TestStatus.Pending;
            Date = DateTime.Now;              
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the test analyst's name.
        /// </summary>
        public string Analyst
        {
            get { return _analyst; }
            set { SetValue(ref _analyst, "Analyst", value); }
        }

        /// <summary>
        /// Gets or sets the test's background count.
        /// </summary>
        public int BackgroundCount
        {
            get { return _backgroundCount; }
            set { SetValue(ref _backgroundCount, "BackgroundCount", value); }
        }

        /// <summary>
        /// Gets or sets the test's background count time (in seconds).
        /// </summary>
        public double BackgroundTimeInSeconds
        {
            get { return _backgroundTimeInSeconds; }
            set { SetValue(ref _backgroundTimeInSeconds, "BackgroundTimeInSeconds", value); }
        }

        /// <summary>
        /// Gets or sets the test's comments.
        /// </summary>
        public string Comments
        {
            get { return _comments; }
            set { SetValue(ref _comments, "Comments", value); }
        }

        /// <summary>
        /// Gets or sets the date when the test was created (for a pending test) or
        /// the date when the test was completed (for non-pending tests).
        /// </summary>
        public DateTime Date
        {
            get { return _date; }
            set { SetValue(ref _date, "Date", value); }
        }

        /// <summary>
        /// Gets or sets the list of samples that comprise a test that are only exposed
        /// to classes that inherit from TestBase.
        /// </summary>
        protected List<TSample> InnerSamples
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the location in which the test was performed.
        /// </summary>
        public string Location
        {
            get { return _location; }
            set { SetValue(ref _location, "Location", value); }
        }

        /// <summary>
        /// Gets or sets the the acquisition time (in seconds) for each of
        /// the test's samples.
        /// </summary>
        public int SampleDurationInSeconds
        {
            get { return _sampleDurationInSeconds; }
            set
            {
                if (value > 0)
                    SetValue(ref _sampleDurationInSeconds, "SampleDurationInSeconds", value);
                else
                    SetValue(ref _sampleDurationInSeconds, "SampleDurationInSeconds", -1);
            }
        }

        /// <summary>
        /// Gets or sets the test's sample layout ID.
        /// </summary>
        public int SampleLayoutId
        {
            get { return _sampleLayoutId; }
            set { SetValue(ref _sampleLayoutId, "SampleLayoutId", value); }
        }

        /// <summary>
        /// Gets the test's collection of samples.
        /// </summary>
        public virtual IEnumerable<TSample> Samples
        {
            get { return InnerSamples; }
        }

        /// <summary>
        /// Gets or sets the test's status.
        /// </summary>
        public TestStatus Status
        {
            get { return _status; }
            set { SetValue(ref _status, "Status", value); }
        }

        /// <summary>
        /// Gets or sets the system ID on which the test is run.
        /// </summary>
        public string SystemId
        {
            get { return _systemId; }
            set { SetValue(ref _systemId, "SystemId", value); }
        }

        #endregion

        #region ITest.Samples

        /// <summary>
        /// Gets the test's collection of ISamples.
        /// </summary>
        IEnumerable<ISample> ITest.Samples
        {
            get { return InnerSamples.Cast<ISample>(); }
        }

        #endregion
    }
}
