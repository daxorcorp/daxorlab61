﻿using System;
using Daxor.Lab.Domain.Common;

namespace Daxor.Lab.Domain.Entities
{
    /// <summary>
    /// Represents a context for acquiring the background radiation counts.
    /// </summary>
    public class BackgroundAcquisitionContext
    {
        #region Fields

        /// <summary>
        /// Backing field for the present background acquisition time.
        /// </summary>
        private int _presetBackgroundAcquisitionTimeInSeconds = -1;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the position identifier for the background.
        /// </summary>
        public int BackgroundPosition { get; set; }

        /// <summary>
        /// Gets or sets the ID of the detector used for measuring background.
        /// </summary>
        public string DetectorId { get; set; }

        /// <summary>
        /// Gets or sets the upper boundary of counts per minute (CPM) for the background.
        /// </summary>
        public int HighCpmBoundary { get; set; }

        /// <summary>
        /// Gets or sets the number of seconds that the background counts can exceed the 
        /// high CPM (counts per minute) boundary before restarting.
        /// </summary>
        public int HighCpmRestartTimeIntervalInSeconds { get; set; }

        /// <summary>
        /// Gets the preset background acquisition time (in minutes).
        /// </summary>
        public double PresetBackgroundAcquistionTimeInMinutes
        {
            get
            {
                return Math.Round(PresetBackgroundAcquisitionTimeInSeconds / TimeConstants.SecondsPerMinute, 2);
            }
        }

        /// <summary>
        /// Gets or sets the preset background acquisition time (in seconds).
        /// </summary>
        public int PresetBackgroundAcquisitionTimeInSeconds
        {
            get
            {
                return _presetBackgroundAcquisitionTimeInSeconds;
            }

            set
            {
                if (_presetBackgroundAcquisitionTimeInSeconds == value)
                    return;

                _presetBackgroundAcquisitionTimeInSeconds = value;
            }
        }

        /// <summary>
        /// Gets or sets the high bound (i.e., channel) for the region of interest (ROI).
        /// </summary>
        public int RoiHighBound { get; set; }

        /// <summary>
        /// Gets or sets the low bound (i.e., channel) for the region of interest (ROI).
        /// </summary>
        public int RoiLowBound { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the live acquisition time should be used for background counts.
        /// </summary>
        public bool ShouldUseLiveAcquisitionTime { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Performs a shallow copy of this BackgroundAcquisitionContext instance.
        /// </summary>
        /// <returns>Shallow copy of this instance</returns>
        public BackgroundAcquisitionContext ShallowCopy()
        {
            return MemberwiseClone() as BackgroundAcquisitionContext;
        }

        #endregion
    }
}
