﻿using System;
using System.Linq;
using System.Reflection;

namespace Daxor.Lab.Domain.Entities.Base
{
    /// <summary>
    /// Represents a value object whose property value changes can be observed.
    /// </summary>
    /// <remarks>
    /// This abstract class combines ValueObject and ObservableEntity.
    /// The code duplication is due to C# not supporting multiple inheritance.
    /// </remarks>
    /// <typeparam name="TValueObject">Type of value object</typeparam>
    public abstract class ValueObject<TValueObject> : ObservableObject, IEquatable<TValueObject>
        where TValueObject : ValueObject<TValueObject>
    {
        /// <summary>
        /// Index used in the computation of the has code
        /// </summary>
        private const int HashCodeComputationIndex = 1;

        #region Overloaded operators

        /// <summary>
        /// Compares two ValueObject instances for equality.
        /// </summary>
        /// <param name="left">Left-hand side instance</param>
        /// <param name="right">Right-hand side instance</param>
        /// <returns>True if both instances are equal; false otherwise</returns>
        public static bool operator ==(ValueObject<TValueObject> left, ValueObject<TValueObject> right)
        {
            if (Equals(left, null))
            {
                // The 'left' is null, the instances are only equal if 'right' is null, too.
                return Equals(right, null);
            }

            // Rely on IEquatable.Equals().
            return left.Equals(right);
        }

        /// <summary>
        /// Compares two ValueObject instances for inequality.
        /// </summary>
        /// <param name="left">Left-hand side instance</param>
        /// <param name="right">Right-hand side instance</param>
        /// <returns>True if both instances are not equal; false otherwise</returns>
        public static bool operator !=(ValueObject<TValueObject> left, ValueObject<TValueObject> right)
        {
            // Rely on overloaded == operator.
            return !(left == right);
        }

        #endregion

        #region IEquatable method

        /// <summary>
        /// Indicates whether the current object is equal to another object 
        /// of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object</param>
        /// <returns>True if the current object is equal to the other; false
        /// otherwise</returns>
        public bool Equals(TValueObject other)
        {
            // 'this' is not null, so the other can't be either for equality.
            if ((object)other == null)
                return false;

            // Same instance.
            if (ReferenceEquals(this, other))
                return true;

            // Compare all public properties to see if they're the same (via references)
            // or equivalent (via Equals()).
            PropertyInfo[] publicProperties = GetType().GetProperties();

            if (publicProperties.Any())
            {
                return publicProperties.All(p =>
                {
                    var left = p.GetValue(this, null);
                    var right = p.GetValue(other, null);

                    if (left == null && right == null) return true;
                    if (left != null && right == null) return false;
                    if (left == null) return false;

                    // ReSharper disable UseMethodIsInstanceOfType  (This is preferred because TValueObject can be a reference or value type)
                    // ReSharper disable UseIsOperator.1
                    if (typeof(TValueObject).IsAssignableFrom(left.GetType()))
                    {
                        // Left and right have the same reference.
                        return ReferenceEquals(left, right);
                    }
                    // ReSharper restore UseMethodIsInstanceOfType
                    // ReSharper restore UseIsOperator.1

                    // Left and right have a different reference but are equivalent.
                    return left.Equals(right);
                });
            }

            // No properties to compare, so the objects are equal by default.
            return true;
        }

        #endregion

        #region System.Object overrides

        /// <summary>
        /// Determines whether the specified object is equal to the current object.
        /// </summary>
        /// <param name="obj">The object to compare with the current object</param>
        /// <returns>True if the specified object is equal to the current object;
        /// false otherwise</returns>
        public override bool Equals(object obj)
        {
            // 'this' is not null, so the other can't be either for equality.
            if (obj == null)
                return false;

            // Same instance.
            if (ReferenceEquals(this, obj))
                return true;

            var item = obj as ValueObject<TValueObject>;
            if ((object)item != null)
            {
                // Use IEquatable.Equals().
                return Equals((TValueObject)item);
            }

            // The cast failed, so the two types can't be equal.
            return false;
        }

        /// <summary>
        /// Serves as a hash function for a particular type.
        /// </summary>
        /// <returns>A hash code for the current pbject.</returns>
        /// <remarks>See http://blogs.msdn.com/b/cesardelatorre/archive/2011/06/07/implementing-a-value-object-base-class-supertype-pattern-ddd-patterns-related.aspx</remarks>
        public override int GetHashCode()
        {
            int hashCode = 31;
            bool changeMultiplier = false;

            // Use all public properties to create a hash code.
            PropertyInfo[] publicProperties = GetType().GetProperties();

            if (publicProperties.Any())
            {
                foreach (var item in publicProperties)
                {
                    // Property is indexer; simply continue not part of the hashcode
                    if (item.GetIndexParameters().Length > 0)
                    {
                        continue;
                    }

                    object value = item.GetValue(this, null);

                    if (value != null)
                    {
                        hashCode = (hashCode * (changeMultiplier ? 59 : 114)) + value.GetHashCode();
                        changeMultiplier = !changeMultiplier;
                    }
                    else
                        hashCode = hashCode ^ (HashCodeComputationIndex * 13); // only for support {"a",null,null,"a"} <> {null,"a","a",null}
                }
            }

            return hashCode;
        }

        #endregion
    }
}
