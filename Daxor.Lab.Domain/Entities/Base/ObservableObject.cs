﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;

namespace Daxor.Lab.Domain.Entities.Base
{
    /// <summary>
    /// Represents object whose property value changes can be observed.
    /// </summary>
    public abstract class ObservableObject : INotifyPropertyChanged, IDisposable
    {
        #region Ctor/finalize

        /// <summary>
        /// Initializes a new instance of the ObservableObject class.
        /// </summary>
        /// <remarks>Writes information to Debug if in debug release mode.</remarks>
        protected ObservableObject()
        {
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            DebugWriteLine(string.Format(
                "{0} (hashcode='{1}') has been initialized.",
                GetType().Name,
                GetHashCode().ToString(CultureInfo.InvariantCulture))); // ReSharper restore DoNotCallOverridableMethodsInConstructor
        }

        /// <summary>
        /// Finalizes an instance of the ObservableObject class.
        /// </summary>
        /// <remarks>
        /// (1) This method is useful for ensuring that ViewModel objects are properly 
        /// garbage-collected.
        /// (2) Writes information to Debug if in debug release mode.</remarks>
        ~ObservableObject()
        {
            DebugWriteLine(string.Format(
                "{0} (hashcode='{1}') has been finalized.",
                GetType().Name,
                GetHashCode())); 
        }

        #endregion

        #region PropertyChanged event

        /// <summary>
        /// Raised when a property on this object has a new value.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region ThrowExceptionOnInvalidPropertyName property

        /// <summary>
        /// Gets or sets a value indicating whether an exception is thrown, or if a Debug.Fail() is used, 
        /// when an invalid property name is passed to the VerifyPropertyName method.
        /// </summary>
        /// <remarks>
        /// The default value is false, but subclasses used by unit tests might 
        /// override this property's getter to return true.
        /// </remarks>
        protected virtual bool ThrowExceptionOnInvalidPropertyName { get; set; }

        #endregion

        #region SetValue<T> method

        /// <summary>
        /// Sets a new value for a specified field.
        /// </summary>
        /// <typeparam name="T">Type of the field</typeparam>
        /// <param name="propField">Backing field of the property</param>
        /// <param name="propName">Property name</param>
        /// <param name="newValue">New Value</param>
        /// <param name="onlyIfDiffers">Only set the backing field if the new value differs from the old</param>
        /// <returns>Returns true if the value has been changed; false otherwise</returns>
        protected virtual bool SetValue<T>(ref T propField, string propName, T newValue, bool onlyIfDiffers = true)
        {
            // ReSharper disable CompareNonConstrainedGenericWithNull (because ObservableObject can be value or instance types)
            if (onlyIfDiffers && propField != null && propField.Equals(newValue))           // ReSharper restore CompareNonConstrainedGenericWithNull
                return false;

            propField = newValue;
            FirePropertyChanged(propName);
            return true;
        }

        #endregion

        #region INotifyPropertyChanged-related members

        /// <summary>
        /// Raises this object's PropertyChanged event.
        /// </summary>
        /// <param name="propertyName">The property that has a new value.</param>
        protected virtual void FirePropertyChanged(string propertyName)
        {
            VerifyPropertyName(propertyName);
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Raises this object's PropertyChanged event for each of the specified properties.
        /// </summary>
        /// <param name="propertyNames">Properties that have new values</param>
        protected virtual void FirePropertyChanged(params string[] propertyNames)
        {
            if (propertyNames == null)
                throw new ArgumentNullException("propertyNames");
            if (propertyNames.Length == 0)
                throw new ArgumentOutOfRangeException("propertyNames", "propertyNames should contain at least one property");

            foreach (var propertyName in propertyNames)
                FirePropertyChanged(propertyName);
        }

        #endregion // INotifyPropertyChanged Members

        #region IDisposable

        /// <summary>
        /// Invoked when this object is being removed from the application and will 
        /// be subject to garbage collection.
        /// </summary>
        public void Dispose()
        {
            OnDispose();
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// Handles the Dispose() event. Child classes can override this method to 
        /// perform clean-up logic, such as removing event handlers.
        /// </summary>
        protected virtual void OnDispose()
        {
        }

        #endregion

        #region Debug helper methods

        /// <summary>
        /// Warns the developer if this object does not have a public property with 
        /// the specified name. This method does not exist in a Release build.
        /// </summary>
        /// <param name="propertyName">Property name to verify the existence of</param>
        [Conditional("DEBUG")]
        [DebuggerStepThrough]
        public void VerifyPropertyName(string propertyName)
        {
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                string msg = "Invalid property name: " + propertyName;

                if (ThrowExceptionOnInvalidPropertyName)
                    throw new Exception(msg);

                Debug.Fail(msg);
            }
        }

        /// <summary>
        /// Writes a given message to the debug console.
        /// </summary>
        /// <param name="msg">Message to be written</param>
        [Conditional("DEBUG")]
        private void DebugWriteLine(string msg)
        {
            Debug.WriteLine(msg);
        }

        #endregion
    }
}