﻿using System;
using Daxor.Lab.Domain.Evaluation;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.Domain.Entities.Base
{
    /// <summary>
    /// Represents a domain entity with an internal unique identifier.
    /// </summary>
    public abstract class Entity : EvaluatableObject
    {
        /// <summary>
        /// Backing field for the internal ID.
        /// </summary>
        private Guid _id;

        /// <summary>
        /// Initializes a new instance of the Entity class
        /// </summary>
        /// <remarks>The rule engine instance is null</remarks>
        protected Entity() : base(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the Entity class using the given
        /// rule engine instance
        /// </summary>
        /// <param name="ruleEngine">Rule engine instance used for performing
        /// evaluation</param>
        protected Entity(IRuleEngine ruleEngine) : base(ruleEngine)
        {
        }

        /// <summary>
        /// Gets or sets the entity's identifier.
        /// </summary>
        public virtual Guid InternalId
        {
            get
            {
                return _id;
            }

            set
            {
                _id = value;
                OnIdChanged();
            }
        }

        /// <summary>
        /// Determines if the entity is transient (i.e., has no identity).
        /// </summary>
        /// <returns>True if the entity is transient; false otherwise</returns>
        public bool IsTransient()
        {
            return InternalId == Guid.Empty;
        }

        /// <summary>
        /// Gets called when underlying identifier has changed.
        /// </summary>
        protected virtual void OnIdChanged()
        {
        }
    }
}
