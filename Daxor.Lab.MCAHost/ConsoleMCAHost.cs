﻿using System;
using System.ServiceModel;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.MCAService;
using System.Collections.Generic;

namespace Daxor.Lab.MCAHost
{
    /// <summary>
    /// MultiChannelAnalyzer console service host. 
    /// ServiceHost reads config settings such as address, binding and service contract from App.config file.
    /// </summary>
    internal class ConsoleMCAHost
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Title = "MultiChannelAnalyzerService Host";
                Console.ForegroundColor = ConsoleColor.Yellow;

                IMultiChannelAnalyzerServiceContract mcaService = new MultiChannelAnalyzerService();
                using (ServiceHost host = new ServiceHost(mcaService))
                {
                    
                   
                    host.Opened += host_Opened;
                    host.Closing += host_Closing;
                    host.Closed += host_Closed;
                    host.Faulted += host_Faulted;
                    host.Open();

                    Console.WriteLine("MultiChannelAnalyzer Service is up and running " + DateTime.Now.ToLongTimeString());

                    string arg, func, consoleInput;
                    System.Console.Write("command: ");
                    while ((consoleInput = Console.ReadLine()) != null)
                    {
                        if (consoleInput.ToUpper() == "Q")
                            break;

                        int functionLength = consoleInput.Contains("(") ? consoleInput.IndexOf('(') : consoleInput.Length;

                        func = consoleInput.Substring(0, functionLength);
                        string[] lArgs = null;

                        if (functionLength != consoleInput.Length)
                        {
                            if (consoleInput.LastIndexOf(')') - (consoleInput.IndexOf('(') + 1) != 0)
                            {
                                int leftIndex = consoleInput.IndexOf('(') + 1;
                                int length = consoleInput.LastIndexOf(')') - leftIndex;

                                arg = consoleInput.Substring(leftIndex, length);
                                lArgs = arg.Split(',');

                                if (lArgs.Length <= 0)
                                {
                                    Console.WriteLine("bad command [" + func + "]");
                                    return;
                                }
                            }
                        }

                        switch(func){
                            case "?":
                                OutputDirections();
                                break;
                            case "GetSerialNumber":
                                Console.WriteLine("SerialNumber : " + mcaService.GetSerialNumber(lArgs[0]));
                                break;
                            case "GetAllMcaNames":
                                Console.Write("GetAllMcaNames : ");
                                ((List<string>)mcaService.GetAllMultiChannelAnalyzerIds()).ForEach((m) => { Console.WriteLine(m); });
                                break;
                            case "GetVoltage":
                                Console.WriteLine("GetVoltage : " + mcaService.GetHighVoltage(lArgs[0])); ;
                                break;
                            case "SetVoltage":
                                try
                                {
                                    mcaService.SetHighVoltage(lArgs[0], Int32.Parse(lArgs[1]));
                                    Console.WriteLine("Voltage was set to " + lArgs[1]);
                                }
                                catch
                                {
                                    Console.WriteLine("Failed to set voltage to " + lArgs[1]);
                                }
                                break;
                            case "GetFineGain":
                                Console.WriteLine("GetFineGain : " + mcaService.GetFineGain(lArgs[0])); ;
                                break;
                            case "SetFineGain":
                                try
                                {
                                    mcaService.SetFineGain(lArgs[0], Int32.Parse(lArgs[1]));
                                    Console.WriteLine("Fine gain was set to " + lArgs[1]);
                                }
                                catch
                                {
                                    Console.WriteLine("Failed to set fine gain to " + lArgs[1]);
                                }
                                break;

                            default:
                                Console.WriteLine("bad command [" + func + "]");
                                break;
                        }

                        System.Console.Write("command: ");
                    }


                    //Exit
                    foreach (string mcaName in mcaService.GetAllMultiChannelAnalyzerIds())
                    {
                        Console.WriteLine("Shutting down WinTMCA thread. Mca name: " + mcaName);
                        mcaService.ShutDown(mcaName);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception. Message " + ex.Message);
                Console.WriteLine("Press Any Key to continue...");
                Console.ReadKey();
            }
        }

        static void host_Opened(object sender, EventArgs e)
        {
            Console.WriteLine("MultiChannelAnalyzer service channel has opened.");
        }
        static void host_Faulted(object sender, EventArgs e)
        {
            System.Console.WriteLine("MultiChannelAnalyzer service channel has faulted.");
        }
        static void host_Closed(object sender, EventArgs e)
        {
            System.Console.WriteLine("MultiChannelAnalyzer service channel has closed.");
        }
        static void host_Closing(object sender, EventArgs e)
        {
            System.Console.WriteLine("MultiChannelAnalyzer service channel is closing.");
        }


        #region Helpers

        static void OutputDirections()
        {
            Console.WriteLine("Usage:");
            Console.WriteLine("\v GetSerialNumber(mcaName)");
            Console.WriteLine("\v GetAllMcaNames()");
            Console.WriteLine("\v GetVoltage(mcaName)");
            Console.WriteLine("\v SetVoltage(mcaName, voltage)");
            Console.WriteLine("\v GetFineGain(mcaName)");
            Console.WriteLine("\v SetFineGain(mcaName, voltage)");
            Console.WriteLine("\v q");
        }

        #endregion
    }
}
