using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_processing_a_sample
    {
        [TestMethod]
        public void And_the_sample_is_a_source_sample_then_the_count_limit_is_set_to_the_correct_value()
        {
            const int expectedCountLimit = 4000;

            // Arrange
            var setupIsotope = new Isotope(IsotopeType.Cs137, 0.0, 0.0, new List<double>());
            var setupSource = new QcSource(setupIsotope, DateTime.Now, 1, 0.0, 0.0, "1234", expectedCountLimit);
            var setupSample = new QcSample(setupSource) {PresetCountsLimit = setupSource.CountLimit, Position = setupSource.SamplePosition};
            var calibrationTest = new StubQCCalibrationTestThatDoesNotProcessSampleIfRunning();
            
            calibrationTest.BuildSamples();

            var setupSampleFromTest = (from s in calibrationTest.Samples where s.Position == setupSource.SamplePosition select s).FirstOrDefault();
            if (setupSampleFromTest == null)
                Assert.Fail("Cannot find the setup sample in the test");

            // Act
            calibrationTest.ProcessSample(setupSample);

            // Assert
            Assert.AreEqual(expectedCountLimit, setupSampleFromTest.PresetCountsLimit);
        }

        [TestMethod]
        public void And_the_sample_is_not_a_source_sample_then_the_count_limit_is_set_to_the_default_value()
        {
            const int backgroundPosition = 24;

            // Arrange
            var backgroundSample = new QcSample { PresetCountsLimit = 4000, Position = backgroundPosition };
            var calibrationTest = new StubQCCalibrationTestThatDoesNotProcessSampleIfRunning();

            calibrationTest.BuildSamples();

            var backgroundSampleFromTest = (from s in calibrationTest.Samples where s.Position == backgroundPosition select s).FirstOrDefault();
            if (backgroundSampleFromTest == null)
                Assert.Fail("Cannot find the background sample in the test");

            // Act
            calibrationTest.ProcessSample(backgroundSample);

            // Assert
            Assert.AreEqual(-1, backgroundSampleFromTest.PresetCountsLimit);
        }
    }

    // ReSharper restore InconsistentNaming
}
