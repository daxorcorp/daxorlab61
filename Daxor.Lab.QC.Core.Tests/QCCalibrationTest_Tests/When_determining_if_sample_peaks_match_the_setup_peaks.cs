using System.Linq;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Factories;
using Daxor.Lab.QC.Core.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_sample_peaks_match_the_setup_peaks
    {
        private readonly QcCalibrationTest _calibrationTest = new QcCalibrationTest(null);

        [TestMethod]
        public void Then_the_isotope_and_peaks_are_passed_to_the_isotope_matching_service()
        {
            var expectedIsotope = IsotopeFactory.CreateIsotope(IsotopeType.Ba133);
            var expectedPeaks = new[] {new Peak()};
            
            var mockIsotopeMatchingService = MockRepository.GenerateMock<IIsotopeMatchingService>();
            mockIsotopeMatchingService.Expect(s => s.IsMatching(expectedIsotope, expectedPeaks)).Return(false);

            _calibrationTest.IsotopeMatchingService = mockIsotopeMatchingService;

            _calibrationTest.SamplePeaksMatchTheSetupIsotopePeaks(expectedIsotope, expectedPeaks.ToList());

            mockIsotopeMatchingService.VerifyAllExpectations();
        }

        [TestMethod]
        public void Then_the_isotope_matching_service_determines_if_the_peaks_match()
        {
            var expectedIsotope = IsotopeFactory.CreateIsotope(IsotopeType.Ba133);
            var expectedPeaks = new[] { new Peak() };

            var stubIsotopeMatchingService = MockRepository.GenerateMock<IIsotopeMatchingService>();
            stubIsotopeMatchingService.Expect(s => s.IsMatching(expectedIsotope, expectedPeaks)).Return(true).Repeat.Once();
            stubIsotopeMatchingService.Expect(s => s.IsMatching(expectedIsotope, expectedPeaks)).Return(false).Repeat.Once();

            _calibrationTest.IsotopeMatchingService = stubIsotopeMatchingService;

            Assert.IsTrue(_calibrationTest.SamplePeaksMatchTheSetupIsotopePeaks(expectedIsotope, expectedPeaks.ToList()));
            Assert.IsFalse(_calibrationTest.SamplePeaksMatchTheSetupIsotopePeaks(expectedIsotope, expectedPeaks.ToList()));
        }
    }
    // ReSharper restore InconsistentNaming
}
