using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_a_sample
    {
        [TestMethod]
        public void And_the_sample_is_null_then_an_exception_is_thrown()
        {
            var calibrationTest = new QcCalibrationTest(null);
            AssertEx.ThrowsArgumentNullException(()=>calibrationTest.EvaluateSample(null), "sample");
        }
    }
    // ReSharper restore InconsistentNaming
}
