using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_a_sample_and_an_exception_occurs
    {
        private QcCalibrationTest _calibrationTest;

        [TestInitialize]
        public void TestInitialize()
        {
            _calibrationTest = new CalibrationTestThatFailsToInitializeFineGainMetadata(null);
        }

        [TestMethod]
        public void And_an_exception_occurs_during_evaluation_then_the_correct_metadata_is_returned()
        {
            var completedBackgroundSample = new QcSample();
            var expectedMetadata = new QcSampleEvaluationMetadata(completedBackgroundSample.InternalId, 
                QcSampleEvaluationResult.Failed, 
                CalibrationTestThatFailsToInitializeFineGainMetadata.ExceptionMessage);

            completedBackgroundSample.ExecutionStatus = SampleExecutionStatus.Completed;
            completedBackgroundSample.TypeOfSample = SampleType.Background;

            var observedMetadata = _calibrationTest.EvaluateSample(completedBackgroundSample);

            Assert.AreEqual(expectedMetadata, observedMetadata);
        }

        [TestMethod]
        public void And_an_exception_occurs_during_evaluation_then_the_test_evaluation_results_are_correct()
        {
            var completedBackgroundSample = new QcSample();
            
            var expectedMetadata = new QcSampleEvaluationMetadata(completedBackgroundSample.InternalId,
                QcSampleEvaluationResult.Failed,
                CalibrationTestThatFailsToInitializeFineGainMetadata.ExceptionMessage);

            completedBackgroundSample.ExecutionStatus = SampleExecutionStatus.Completed;
            completedBackgroundSample.TypeOfSample = SampleType.Background;

            _calibrationTest.EvaluateSample(completedBackgroundSample);

            var observedEvaluationResults = _calibrationTest.GetEvaluationResultsForSample(completedBackgroundSample.InternalId);
            CollectionAssert.AreEqual(new List<QcSampleEvaluationMetadata>{expectedMetadata}, observedEvaluationResults.ToList());
        }

        [TestMethod]
        public void And_an_exception_occurs_during_evaluation_then_the_sample_is_marked_as_failed()
        {
            var completedBackgroundSample = new QcSample
            {
                ExecutionStatus = SampleExecutionStatus.Completed,
                TypeOfSample = SampleType.Background
            };

            _calibrationTest.EvaluateSample(completedBackgroundSample);

            Assert.AreEqual(false, completedBackgroundSample.IsPassed);
        }

        [TestMethod]
        public void And_an_exception_occurs_during_evaluation_then_the_sample_error_details_are_correct()
        {
            var completedBackgroundSample = new QcSample
            {
                ExecutionStatus = SampleExecutionStatus.Completed,
                TypeOfSample = SampleType.Background
            };

            _calibrationTest.EvaluateSample(completedBackgroundSample);

            Assert.AreEqual(CalibrationTestThatFailsToInitializeFineGainMetadata.ExceptionMessage, completedBackgroundSample.ErrorDetails);
        }

        [TestMethod]
        public void And_an_exception_occurs_during_evaluation_then_the_samples_evaluation_has_completed()
        {
            var completedBackgroundSample = new QcSample
            {
                ExecutionStatus = SampleExecutionStatus.Completed,
                TypeOfSample = SampleType.Background
            };

            _calibrationTest.EvaluateSample(completedBackgroundSample);

            Assert.IsTrue(completedBackgroundSample.IsEvaluationCompleted);
        }
    }
    // ReSharper restore InconsistentNaming
}