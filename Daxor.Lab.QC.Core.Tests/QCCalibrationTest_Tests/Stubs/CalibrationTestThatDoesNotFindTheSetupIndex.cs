﻿namespace Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs
{
    public class CalibrationTestThatDoesNotFindTheSetupIndex : QcCalibrationTest
    {
        public CalibrationTestThatDoesNotFindTheSetupIndex()
            : base(QcSchemaProvider.GetCalibrationSamplesLayoutSchema(), null)
        {
        }

        protected override void FindIndexOfEnergyForSetupPeak()
        {
        }

        public FineGainMetadata FineGainMetaDataWrapper { get { return FineGainMetadata; }}
    }
}
