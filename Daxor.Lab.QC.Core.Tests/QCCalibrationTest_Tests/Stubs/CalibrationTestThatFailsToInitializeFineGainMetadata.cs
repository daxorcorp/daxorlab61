using System;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs
{
    internal class CalibrationTestThatFailsToInitializeFineGainMetadata : QcCalibrationTest
    {
        public const string ExceptionMessage = "Failed to initialize fine gain";

        public CalibrationTestThatFailsToInitializeFineGainMetadata(IRuleEngine ruleEngine)
            : base(QcSchemaProvider.GetCalibrationSamplesLayoutSchema(), ruleEngine)
        {
        }

        protected override void FindIndexOfEnergyForSetupPeak()
        {
        }

        protected override void InitializeFineGainMetadata()
        {
            throw new Exception(ExceptionMessage);
        }
    }
}