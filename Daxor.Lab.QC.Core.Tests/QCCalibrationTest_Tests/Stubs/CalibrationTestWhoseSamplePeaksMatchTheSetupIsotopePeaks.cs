﻿using System.Collections.Generic;
using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs
{
    public class CalibrationTestWhoseSamplePeaksMatchTheSetupIsotopePeaks : QcCalibrationTest
    {
        public CalibrationTestWhoseSamplePeaksMatchTheSetupIsotopePeaks()
            : base(QcSchemaProvider.GetCalibrationSamplesLayoutSchema(), null)
        {
        }

        #region Overrides

        protected override void FindIndexOfEnergyForSetupPeak()
        {
            IndexOfEnergyForSetupPeak = 0;
        }

        protected override void InitializeFineGainMetadata()
        {
        }

        public override bool SamplePeaksMatchTheSetupIsotopePeaks(Isotope setupIsotope, List<Peak> peaksSortedByCentroid)
        {
            return true;
        }
       
        #endregion

        public bool ShouldPeakBeWithinRoi { get; set; }

        public bool ShouldCentroidBeWithinTolerance { get; set; }

        public bool IsFineGainDoneAdjusting
        {
            get { return IsFineGainFinishedAdjusting; }
            set { IsFineGainFinishedAdjusting = value; }
        }

        public int PriorSampleDuration
        {
            get { return PriorSampleDurationInSeconds; }
            set { PriorSampleDurationInSeconds = value; }
        }

        public FineGainMetadata FineGainMetadataWrapper
        {
            get { return FineGainMetadata; }
            set { FineGainMetadata = value; }
        }

        public double SetupCentroid
        {
            get { return SetupActualCentroidInkeV; }
            set { SetupActualCentroidInkeV = value; }
        }

        public int NumberOfFineGainAdjustmentsWrapper
        {
            get { return (int) NumberOfFineGainAdjustments; }
            set { NumberOfFineGainAdjustments = (uint) value; }
        }

        public void AddMetadata(QcSampleEvaluationMetadata metadata)
        {
            AddEvalResult(metadata);
        }
    }
}
