﻿namespace Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs
{
    public class CalibrationTestThatExposesSetupCentroid : QcCalibrationTest
    {
        public CalibrationTestThatExposesSetupCentroid()
            : base(QcSchemaProvider.GetCalibrationSamplesLayoutSchema(), null)
        {
        }

        internal double SetupCentroid
        {
            set { SetupActualCentroidInkeV = value; }
        }
    }
}
