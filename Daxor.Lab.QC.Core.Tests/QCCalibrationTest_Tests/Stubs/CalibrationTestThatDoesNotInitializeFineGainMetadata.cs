using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs
{
    public class CalibrationTestThatDoesNotInitializeFineGainMetadata : QcCalibrationTest
    {
        public CalibrationTestThatDoesNotInitializeFineGainMetadata(IRuleEngine ruleEngine)
            : base(QcSchemaProvider.GetCalibrationSamplesLayoutSchema(), ruleEngine)
        {
        }

        protected override void InitializeFineGainMetadata()
        {
        }

        protected override void FindIndexOfEnergyForSetupPeak()
        {
        }

        public bool IsFineGainDoneAdjusting
        {
            get { return IsFineGainFinishedAdjusting; }
            set { IsFineGainFinishedAdjusting = value; }
        }

        public int PriorSampleDuration
        {
            get { return PriorSampleDurationInSeconds; }
            set { PriorSampleDurationInSeconds = value; }
        }

        public FineGainMetadata FineGainMetadataWrapper
        {
            get { return FineGainMetadata; }
            set { FineGainMetadata = value; }
        }

        public uint NumberOfFineGainAdjustmentsWrapper
        {
            get { return NumberOfFineGainAdjustments; }
            set { NumberOfFineGainAdjustments = value; }
        }
    }
}