﻿using System.Collections.Generic;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.QC.Core.Tests.QCCalibrationTest_Tests.Stubs
{
    public class CalibrationTestWhoseSamplePeaksDoNotMatchTheSetupIsotopePeaks : QcCalibrationTest
    {
        public CalibrationTestWhoseSamplePeaksDoNotMatchTheSetupIsotopePeaks(IRuleEngine ruleEngine)
            : base(QcSchemaProvider.GetCalibrationSamplesLayoutSchema(), ruleEngine)
        {
        }

        protected override void InitializeFineGainMetadata()
        {
        }

        public override bool SamplePeaksMatchTheSetupIsotopePeaks(Isotope setupIsotope, List<Peak> peaksSortedByCentroid)
        {
            return false;
        }

        public bool IsFineGainDoneAdjusting
        {
            get { return IsFineGainFinishedAdjusting; }
            set { IsFineGainFinishedAdjusting = value; }
        }

        internal int PriorSampleDuration
        {
            get { return PriorSampleDurationInSeconds; }
            set { PriorSampleDurationInSeconds = value; }
        }

        public FineGainMetadata FineGainMetadataWrapper
        {
            get { return FineGainMetadata; }
            set { FineGainMetadata = value; }
        }

        public int NumberOfFineGainAdjustmentsWrapper
        {
            get { return (int) NumberOfFineGainAdjustments; }
            set { NumberOfFineGainAdjustments = (uint) value; }
        }
    }
}
