using System;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Payloads;
using Daxor.Lab.QC.Core.SampleEvaluators;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.SampleEvaluator_Tests.BackgroundSampleEvaluator_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_the_sample
    {
        [TestMethod]
        public void And_the_payload_is_null_then_an_exception_is_thrown()
        {
            var evaluator = new BackgroundSampleEvaluator();
            AssertEx.ThrowsArgumentNullException(()=>evaluator.EvaluateSample(null), "payload");
        }

        [TestMethod]
        public void And_the_sample_is_null_then_an_exception_is_thrown()
        {
            var payload = new BackgroundPreEvaluationPayload();
            var evaluator = new BackgroundSampleEvaluator();

            AssertEx.Throws<ArgumentException>(()=>evaluator.EvaluateSample(payload));
        }

        [TestMethod]
        public void And_the_sample_type_in_the_payload_is_not_background_then_an_exception_is_thrown()
        {
            var payload = new BackgroundPreEvaluationPayload
            {
                Sample = new QcSample { TypeOfSample = SampleType.StandardA }
            };

            var evaluator = new BackgroundSampleEvaluator();

            AssertEx.Throws<NotSupportedException>(()=>evaluator.EvaluateSample(payload));
        }
    }
    // ReSharper restore InconsistentNaming
}
