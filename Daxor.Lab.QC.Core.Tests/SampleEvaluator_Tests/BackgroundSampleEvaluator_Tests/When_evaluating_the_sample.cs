using System;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Payloads;
using Daxor.Lab.QC.Core.SampleEvaluators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.SampleEvaluator_Tests.BackgroundSampleEvaluator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_the_sample
    {
        private BackgroundPreEvaluationPayload _prePayload;
        private BackgroundSampleEvaluator _evaluator;

        [TestInitialize]
        public void TestInitialize()
        {
            _prePayload = new BackgroundPreEvaluationPayload
            {
                Sample = new QcSample { TypeOfSample = SampleType.Background, InternalId = new Guid(), IsEvaluationCompleted = false, IsPassed = null}

            };
            _evaluator = new BackgroundSampleEvaluator();
        }

        [TestMethod]
        public void Then_the_sample_evaluation_is_completed()
        {
            _evaluator.EvaluateSample(_prePayload);

            Assert.IsTrue(_prePayload.Sample.IsEvaluationCompleted);
        }

        [TestMethod]
        public void Then_the_sample_is_marked_as_passed()
        {
            _evaluator.EvaluateSample(_prePayload);

            Assert.AreEqual(true, _prePayload.Sample.IsPassed);
        }

        [TestMethod]
        public void Then_the_correct_payload_is_returned()
        {
            var expectedPostPayload = new BackgroundPostEvaluationPayload
            {
                Metadata = new QcSampleEvaluationMetadata(_prePayload.Sample.InternalId, QcSampleEvaluationResult.Pass)
            };
            
            var observedPayload = _evaluator.EvaluateSample(_prePayload);

            Assert.AreEqual(expectedPostPayload, observedPayload);
        }
    }
    // ReSharper restore InconsistentNaming
}
