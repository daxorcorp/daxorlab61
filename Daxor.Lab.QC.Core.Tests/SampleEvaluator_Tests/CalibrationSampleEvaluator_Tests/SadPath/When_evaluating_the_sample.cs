using System;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Payloads;
using Daxor.Lab.QC.Core.SampleEvaluators;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Core.Tests.SampleEvaluator_Tests.CalibrationSampleEvaluator_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_the_sample
    {
        [TestMethod]
        public void And_the_payload_is_null_then_an_exception_is_thrown()
        {
            var evaluator = new CalibrationSampleEvaluator();
            AssertEx.ThrowsArgumentNullException(()=>evaluator.EvaluateSample(null), "payload");
        }

        [TestMethod]
        public void And_the_sample_is_null_then_an_exception_is_thrown()
        {
            var payload = new CalibrationPreEvaluationPayload();
            var evaluator = new CalibrationSampleEvaluator();
            AssertEx.Throws<ArgumentException>(()=>evaluator.EvaluateSample(payload));
        }

        [TestMethod]
        public void And_the_sample_type_in_the_payload_is_not_calibration_then_an_exception_is_thrown()
        {
            var payload = new CalibrationPreEvaluationPayload
            {
                Sample = new QcSample { TypeOfSample = SampleType.StandardA }
            };

            var evaluator = new CalibrationSampleEvaluator();
            AssertEx.Throws<NotSupportedException>(()=>evaluator.EvaluateSample(payload));
        }

        [TestMethod]
        public void And_the_payload_is_not_the_correct_type_then_an_exception_is_thrown()
        {
            var payload = new BackgroundPreEvaluationPayload
            {
                Sample = new QcSample { TypeOfSample = SampleType.QC2 }
            };

            var evaluator = new CalibrationSampleEvaluator();
            AssertEx.Throws<NotSupportedException>(()=>evaluator.EvaluateSample(payload));
        }

        [TestMethod]
        public void And_the_spectroscopy_service_is_null_then_an_exception_is_thrown()
        {
            var payload = new CalibrationPreEvaluationPayload
            {
                Sample = new QcSample{ TypeOfSample = SampleType.QC2},
                SpectroscopyService = null,
                PeakFinder = MockRepository.GenerateStub<IPeakFinder>()
            };

            var evaluator = new CalibrationSampleEvaluator();
            AssertEx.Throws<NotSupportedException>(()=>evaluator.EvaluateSample(payload));
        }

        [TestMethod]
        public void And_the_peak_finder_service_is_null_then_an_exception_is_thrown()
        {
            var payload = new CalibrationPreEvaluationPayload
            {
                Sample = new QcSample { TypeOfSample = SampleType.QC2 },
                SpectroscopyService = MockRepository.GenerateStub<ISpectroscopyService>(),
                PeakFinder = null
            };

            var evaluator = new CalibrationSampleEvaluator();
            AssertEx.Throws<NotSupportedException>(()=>evaluator.EvaluateSample(payload));
        }
    }
    // ReSharper restore InconsistentNaming
}
