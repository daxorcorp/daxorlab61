using System;
using System.Collections.Generic;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Payloads;
using Daxor.Lab.QC.Core.SampleEvaluators;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Core.Tests.SampleEvaluator_Tests.SetupSampleEvaluator_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_evaluating_the_sample
    {
        private SetupSampleEvaluator _evaluator;

        [TestInitialize]
        public void TestInitialize()
        {
            _evaluator = new SetupSampleEvaluator();
        }

        [TestMethod]
        public void And_the_payload_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_evaluator.EvaluateSample(null), "payload");
        }

        [TestMethod]
        public void And_the_payloads_sample_is_null_then_an_exception_is_thrown()
        {
            var payload = new SetupPreEvaluationPayload();
            AssertEx.Throws<ArgumentException>(()=>_evaluator.EvaluateSample(payload));
        }

        [TestMethod]
        public void And_the_sample_is_not_a_setup_sample_is_null_then_an_exception_is_thrown()
        {
            var payload = new SetupPreEvaluationPayload {Sample = new QcSample {TypeOfSample = SampleType.QC4}};
            AssertEx.Throws<NotSupportedException>(()=>_evaluator.EvaluateSample(payload));
        }

        [TestMethod]
        public void And_the_payload_is_not_a_setup_payload_then_an_exception_is_thrown()
        {
            var payload = new BackgroundPreEvaluationPayload { Sample = new QcSample { TypeOfSample = SampleType.QC1 } };
            AssertEx.Throws<NotSupportedException>(()=>_evaluator.EvaluateSample(payload));
        }

        [TestMethod]
        public void And_determining_if_sample_matches_the_setup_isotope_and_the_isotope_is_null_then_an_exception_is_thrown()
        {
            var payload = new SetupPreEvaluationPayload
            {
                Sample = new QcSample { TypeOfSample = SampleType.QC1, Isotope = null },
            };

            AssertEx.Throws<NotSupportedException>(()=>_evaluator.EvaluateSample(payload));
        }

        [TestMethod]
        public void And_peak_finder_returns_a_null_list_then_an_exception_is_thrown()
        {
            var payload = new SetupPreEvaluationPayload
            {
                Sample = new QcSample 
                { 
                    TypeOfSample = SampleType.QC1, 
                    Source = new QcSource(new Isotope(IsotopeType.Cs137, 0, UnitOfTime.Day, new List<double>()), DateTime.Now, 1, 1, 1, "1", 1)
                },
                PeakFinder = MockRepository.GenerateStub<IPeakFinder>(),
                NumberOfFineGainAdjustments = 9,
                MaxAllowableFineGainAdjustments = 10
            };
            payload.PeakFinder.Expect(f => f.FindPeaksSortedByCentroid(null, 0, 0)).IgnoreArguments().Return(null);

            AssertEx.Throws<InvalidOperationException>(()=>_evaluator.EvaluateSample(payload));
        }

        [TestMethod]
        public void And_isotope_matching_service_is_null_then_an_exception_is_thrown()
        {
            var payload = new SetupPreEvaluationPayload
            {
                Sample = new QcSample
                {
                    TypeOfSample = SampleType.QC1,
                    Source = new QcSource(new Isotope(IsotopeType.Cs137, 0, UnitOfTime.Day, new List<double>()), DateTime.Now, 1, 1, 1, "1", 1)
                },
                PeakFinder = MockRepository.GenerateStub<IPeakFinder>(),
                NumberOfFineGainAdjustments = 9,
                MaxAllowableFineGainAdjustments = 10
            };
            payload.PeakFinder.Expect(f => f.FindPeaksSortedByCentroid(null, 0, 0)).IgnoreArguments().Return(new List<Peak>());

            AssertEx.Throws<InvalidOperationException>(()=>_evaluator.EvaluateSample(payload));
        }

        [TestMethod]
        public void And_there_is_no_peak_for_the_index_then_an_exception_is_thrown()
        {
            var payload = new SetupPreEvaluationPayload
            {
                Sample = new QcSample
                {
                    TypeOfSample = SampleType.QC1,
                    Source = new QcSource(new Isotope(IsotopeType.Cs137, 0, UnitOfTime.Day, new List<double>()), DateTime.Now, 1, 1, 1, "1", 1)
                },
                PeakFinder = MockRepository.GenerateStub<IPeakFinder>(),
                NumberOfFineGainAdjustments = 9,
                MaxAllowableFineGainAdjustments = 10,
                IndexOfEnergyForSetupPeak = 83
            };
            payload.PeakFinder.Expect(f => f.FindPeaksSortedByCentroid(null, 0, 0)).IgnoreArguments().Return(new List<Peak>());

            AssertEx.Throws<InvalidOperationException>(()=>_evaluator.EvaluateSample(payload));
        }
    }
    // ReSharper restore InconsistentNaming
}
