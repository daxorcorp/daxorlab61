using Daxor.Lab.QC.Core.SampleEvaluators;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.SampleEvaluator_Tests.SetupSampleEvaluator_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_the_peak_is_in_the_ROI
    {
        [TestMethod]
        public void And_the_given_sample_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>SetupSampleEvaluator.IsSetupPeakWithinRoi(null, new Peak()), "sample");
        }

        [TestMethod]
        public void And_the_given_peak_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>SetupSampleEvaluator.IsSetupPeakWithinRoi(new QcSample(), null), "setupPeak");
        }
    }
    // ReSharper restore InconsistentNaming
}
