using Daxor.Lab.QC.Core.SampleEvaluators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.SampleEvaluator_Tests.SetupSampleEvaluator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_the_centroid_is_within_precision
    {
        [TestMethod]
        public void And_the_peak_is_less_than_the_centroid_and_the_difference_is_below_the_precision_then_the_centroid_is_within_precision()
        {
            const double setupCentroid = 5.0;
            const double setupPeakEnergy = 3.0;
            const int setupPrecisionInkeV = 10;

            Assert.IsTrue(SetupSampleEvaluator.IsCentroidWithinPrecision(setupCentroid,setupPeakEnergy, setupPrecisionInkeV));
        }

        [TestMethod]
        public void And_the_peak_is_less_than_the_centroid_and_the_difference_is_equal_to_the_precision_then_the_centroid_is_within_precision()
        {
            const double setupCentroid = 5.0;
            const double setupPeakEnergy = 3.0;
            const int setupPrecisionInkeV = 2;

            Assert.IsTrue(SetupSampleEvaluator.IsCentroidWithinPrecision(setupCentroid, setupPeakEnergy, setupPrecisionInkeV));
        }

        [TestMethod]
        public void And_the_peak_is_less_than_the_centroid_and_the_difference_is_above_the_precision_then_the_centroid_is_not_within_precision()
        {
            const double setupCentroid = 5.0;
            const double setupPeakEnergy = 3.0;
            const int setupPrecisionInkeV = 1;

            Assert.IsFalse(SetupSampleEvaluator.IsCentroidWithinPrecision(setupCentroid, setupPeakEnergy, setupPrecisionInkeV));
        }

        [TestMethod]
        public void And_the_centroid_is_less_than_the_peak_and_the_difference_is_below_the_precision_then_the_centroid_is_within_precision()
        {
            const double setupCentroid = 3.0;
            const double setupPeakEnergy = 5.0;
            const int setupPrecisionInkeV = 10;

            Assert.IsTrue(SetupSampleEvaluator.IsCentroidWithinPrecision(setupCentroid, setupPeakEnergy, setupPrecisionInkeV));
        }

        [TestMethod]
        public void And_the_centroid_is_less_than_the_peak_and_the_difference_is_equal_to_the_precision_then_the_centroid_is_within_precision()
        {
            const double setupCentroid = 3.0;
            const double setupPeakEnergy = 5.0;
            const int setupPrecisionInkeV = 2;

            Assert.IsTrue(SetupSampleEvaluator.IsCentroidWithinPrecision(setupCentroid, setupPeakEnergy, setupPrecisionInkeV));
        }

        [TestMethod]
        public void And_the_centroid_is_less_than_the_peak_and_the_difference_is_above_the_precision_then_the_centroid_is_not_within_precision()
        {
            const double setupCentroid = 3.0;
            const double setupPeakEnergy = 5.0;
            const int setupPrecisionInkeV = 1;

            Assert.IsFalse(SetupSampleEvaluator.IsCentroidWithinPrecision(setupCentroid, setupPeakEnergy, setupPrecisionInkeV));
        }
    }
    // ReSharper restore InconsistentNaming
}
