using Daxor.Lab.QC.Core.SampleEvaluators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.SampleEvaluator_Tests.SetupSampleEvaluator_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_the_peak_is_in_the_ROI
    {
        private readonly QcSample _setupSample = new QcSample { StartChannel = 50, EndChannel = 100 };

        [TestMethod]
        public void And_the_centroid_is_less_than_the_starting_channel_then_the_peak_is_not_in_the_ROI()
        {
            Assert.IsFalse(SetupSampleEvaluator.IsSetupPeakWithinRoi(_setupSample,
                new Peak {Centroid = _setupSample.StartChannel - 1}));
        }

        [TestMethod]
        public void And_the_centroid_is_at_the_starting_channel_then_the_peak_is_not_in_the_ROI()
        {
            Assert.IsFalse(SetupSampleEvaluator.IsSetupPeakWithinRoi(_setupSample,
                new Peak { Centroid = _setupSample.StartChannel }));
        }

        [TestMethod]
        public void And_the_centroid_is_greater_than_the_ending_channel_then_the_peak_is_not_in_the_ROI()
        {
            Assert.IsFalse(SetupSampleEvaluator.IsSetupPeakWithinRoi(_setupSample,
                new Peak { Centroid = _setupSample.EndChannel + 1 }));
        }

        [TestMethod]
        public void And_the_centroid_is_at_the_ending_channel_then_the_peak_is_not_in_the_ROI()
        {
            Assert.IsFalse(SetupSampleEvaluator.IsSetupPeakWithinRoi(_setupSample,
                new Peak { Centroid = _setupSample.EndChannel }));
        }

        [TestMethod]
        public void And_the_centroid_is_between_the_starting_and_ending_then_the_peak_is_in_the_ROI()
        {
            Assert.IsTrue(SetupSampleEvaluator.IsSetupPeakWithinRoi(_setupSample,
                new Peak { Centroid = _setupSample.StartChannel + 1 }));
        }
    }
    // ReSharper restore InconsistentNaming
}
