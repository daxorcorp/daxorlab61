using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.QCStandardsTest_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_processing_samples
    {
        [TestMethod]
        public void And_the_sample_is_null_then_an_exception_is_thrown()
        {
            var calibrationTest = new QcStandardsTest(null);

            AssertEx.ThrowsArgumentNullException(()=>calibrationTest.ProcessSample(null), "sample");
        }
    }
    // ReSharper restore InconsistentNaming
}
