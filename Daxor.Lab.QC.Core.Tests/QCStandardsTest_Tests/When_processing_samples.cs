using System.Linq;
using Daxor.Lab.QC.Core.Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.QCStandardsTest_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_processing_samples
    {
        [TestMethod]
        public void And_the_sample_is_not_a_source_sample_then_the_count_limit_is_set_to_the_default_value()
        {
            const int standardAPosition = 11;

            // Arrange
            var standardASample = new QcSample { PresetCountsLimit = 4000, Position = standardAPosition };
            var standardsTests = new StubQCStandardsTestThatDoesNotProcessSampleIfRunning();

            standardsTests.BuildSamples();

            var standardASampleFromTest = (from s in standardsTests.Samples where s.Position == standardAPosition select s).FirstOrDefault();
            if (standardASampleFromTest == null)
                Assert.Fail("Cannot find the Standard A sample in the test");

            // Act
            standardsTests.ProcessSample(standardASample);

            // Assert
            Assert.AreEqual(-1, standardASampleFromTest.PresetCountsLimit);
        }
    }
    // ReSharper restore InconsistentNaming
}
