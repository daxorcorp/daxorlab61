﻿namespace Daxor.Lab.QC.Core.Tests.Stubs
{
    public class StubQCStandardsTestThatDoesNotProcessSampleIfRunning : QcStandardsTest
    {
        public StubQCStandardsTestThatDoesNotProcessSampleIfRunning()
            : base(QcSchemaProvider.GetStandardsSamplesLayoutSchema(), null)
        {
            
        }

        protected override void ProcessSampleIfTestRunning(QcSample rSample)
        {

        }
    }
}
