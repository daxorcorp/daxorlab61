﻿namespace Daxor.Lab.QC.Core.Tests.Stubs
{
    public class StubQCFullTestThatDoesNotProcessSampleIfRunning : QcFullTest
    {
        public StubQCFullTestThatDoesNotProcessSampleIfRunning() : base(new QcSchemaProvider(), null)
        {}

    }
}
