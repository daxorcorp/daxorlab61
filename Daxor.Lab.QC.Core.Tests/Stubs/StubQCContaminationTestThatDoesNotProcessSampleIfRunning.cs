﻿namespace Daxor.Lab.QC.Core.Tests.Stubs
{
    public class StubQCContaminationTestThatDoesNotProcessSampleIfRunning : QcContaminationTest
    {
        public StubQCContaminationTestThatDoesNotProcessSampleIfRunning()
            : base(QcSchemaProvider.GetContaminationSamplesLayoutSchema(), null)
        {
            
        }

        protected override void ProcessSampleIfTestRunning(QcSample rSample)
        {
            
        }
    }
}
