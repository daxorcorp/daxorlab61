﻿namespace Daxor.Lab.QC.Core.Tests.Stubs
{
    public class StubQCLinearityWithCalibrationTestThatDoesNotProcessSampleIfRunning : QcLinearityTest
    {
        public StubQCLinearityWithCalibrationTestThatDoesNotProcessSampleIfRunning()
            : base(new QcSchemaProvider(), null)
        {
            
        }
    }
}
