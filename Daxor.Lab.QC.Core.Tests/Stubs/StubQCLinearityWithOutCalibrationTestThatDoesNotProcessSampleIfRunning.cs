﻿
namespace Daxor.Lab.QC.Core.Tests.Stubs
{
    public class StubQCLinearityWithOutCalibrationTestThatDoesNotProcessSampleIfRunning : QcLinearityWithoutCalibrationTest
    {
        public StubQCLinearityWithOutCalibrationTestThatDoesNotProcessSampleIfRunning()
            : base(QcSchemaProvider.GetLinearityWithoutCalibrationSamplesLayoutSchema(), null)
        {
        }

       protected override void ProcessSampleIfTestRunning(QcSample sample)
       {}

    }
}
