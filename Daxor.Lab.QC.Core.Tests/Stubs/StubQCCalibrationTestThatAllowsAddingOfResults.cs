﻿using Daxor.Lab.QC.Core.Common;

namespace Daxor.Lab.QC.Core.Tests.Stubs
{
    public class StubQCCalibrationTestThatAllowsAddingOfResults : QcCalibrationTest
    {
        public StubQCCalibrationTestThatAllowsAddingOfResults()
            : base(QcSchemaProvider.GetCalibrationSamplesLayoutSchema(), null)
        {
            
        }

        public void AddMetadataToEvalResults(QcSampleEvaluationMetadata metadata)
        {
            AddEvalResult(metadata);
        }
    }
}
