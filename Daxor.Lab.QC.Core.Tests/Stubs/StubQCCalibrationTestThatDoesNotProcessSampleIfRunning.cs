﻿namespace Daxor.Lab.QC.Core.Tests.Stubs
{
    public class StubQCCalibrationTestThatDoesNotProcessSampleIfRunning : QcCalibrationTest
    {
        public StubQCCalibrationTestThatDoesNotProcessSampleIfRunning() : base(QcSchemaProvider.GetCalibrationSamplesLayoutSchema(), null)
        {
            
        }

        protected override bool ContainsSample(QcSample sample)
        {
            return true;
        }

        protected override void ProcessSampleIfTestRunning(QcSample rSample)
        {
        }
    }
}
