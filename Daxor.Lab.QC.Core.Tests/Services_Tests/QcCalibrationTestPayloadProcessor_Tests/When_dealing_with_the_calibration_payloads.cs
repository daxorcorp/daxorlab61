using System;
using System.Linq;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Payloads;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.QC.Core.Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Core.Tests.Services_Tests.QcCalibrationTestPayloadProcessor_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_dealing_with_the_calibration_payloads
    {
        private const double expectedSetupActualCentroidInkev = 661.62;
        private const double expectedSetupPeakEnergy = 661.66;
        private const double expectedCalibrationPeakEnergy = 356.12;

        private StubQCCalibrationTestThatAllowsAddingOfResults _qcCalibrationTest;
        private QcSample _qcCalibrationSample;
        private QcCalibrationTestPayloadProcessor _qcCalibrationTestPayloadProcessor;

        [TestInitialize]
        public void TestInitialize()
        {
            _qcCalibrationTestPayloadProcessor = new QcCalibrationTestPayloadProcessor();
            _qcCalibrationTest = new StubQCCalibrationTestThatAllowsAddingOfResults();
            _qcCalibrationTest.BuildSamples();

            _qcCalibrationTest.CalibrationCurve = MockRepository.GenerateStub<ICalibrationCurve>();
            _qcCalibrationTest.PeakFinder = MockRepository.GenerateStub<IPeakFinder>();
            _qcCalibrationTest.SpectroscopyService = MockRepository.GenerateStub<ISpectroscopyService>();
            _qcCalibrationTest.SpectroscopyService.Expect(s => s.ComputeCentroid(null, 0, 0))
                .IgnoreArguments().Return(expectedSetupActualCentroidInkev);
            _qcCalibrationTest.SetupPeakEnergy = expectedSetupPeakEnergy;
            _qcCalibrationTest.CalibrationPeakEnergy = expectedCalibrationPeakEnergy;
            
            _qcCalibrationSample = _qcCalibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.QC2);
        }

        [TestMethod]
        public void Then_the_pre_payload_is_correct()
        {
            var observedPrePayload = _qcCalibrationTestPayloadProcessor.CreatePreEvaluationPayload(_qcCalibrationSample, _qcCalibrationTest);

            var expectedPrePayload = new CalibrationPreEvaluationPayload
            {
                Sample = _qcCalibrationSample,
                PeakFinder = _qcCalibrationTest.PeakFinder,
                SpectroscopyService = _qcCalibrationTest.SpectroscopyService,
                SetupActualCentroidInkeV = expectedSetupActualCentroidInkev,
                SetupPeakEnergy = expectedSetupPeakEnergy,
                CalibrationPeakEnergy = expectedCalibrationPeakEnergy,
            };

            Assert.AreEqual(expectedPrePayload, observedPrePayload);
        }

        [TestMethod]
        public void And_the_result_is_passed_then_processing_the_post_evaluation_payload_clears_the_evaluation_results_on_the_test()
        {
            // Put something in the list to ensure the list is cleared.
            _qcCalibrationTest.AddMetadataToEvalResults(new QcSampleEvaluationMetadata(new Guid(), QcSampleEvaluationResult.FailedButContinue));

            var expectedCalibrationCurve = MockRepository.GenerateStub<ICalibrationCurve>();
            var postPayload = CreateExpectedPayloadResultingFromProcessingTheCalibrationSample(expectedCalibrationCurve, QcSampleEvaluationResult.Pass);

            _qcCalibrationTestPayloadProcessor.ProcessPostEvaluationPayload(postPayload, _qcCalibrationTest);

            Assert.AreEqual(0, _qcCalibrationTest.EvaluationResultCount);
        }

        [TestMethod]
        public void And_the_result_is_passed_then_processing_the_post_evaluation_payload_updates_the_calibration_curve_on_the_test()
        {
            _qcCalibrationTest.CalibrationCurve = null;
            
            var expectedCalibrationCurve = MockRepository.GenerateStub<ICalibrationCurve>();
            var postPayload = CreateExpectedPayloadResultingFromProcessingTheCalibrationSample(expectedCalibrationCurve, QcSampleEvaluationResult.Pass);

            _qcCalibrationTestPayloadProcessor.ProcessPostEvaluationPayload(postPayload, _qcCalibrationTest);

            Assert.IsNotNull(_qcCalibrationTest.CalibrationCurve);
        }

        [TestMethod]
        public void And_the_result_is_not_passed_then_processing_the_post_evaluation_payload_adds_the_evaluation_results_on_the_test()
        {
            var expectedCalibrationCurve = MockRepository.GenerateStub<ICalibrationCurve>();
            var postPayload = CreateExpectedPayloadResultingFromProcessingTheCalibrationSample(expectedCalibrationCurve, QcSampleEvaluationResult.FailedButContinue);

            _qcCalibrationTestPayloadProcessor.ProcessPostEvaluationPayload(postPayload, _qcCalibrationTest);

            Assert.AreEqual(1, _qcCalibrationTest.EvaluationResultCount);
        }

        [TestMethod]
        public void And_the_result_is_not_passed_then_processing_the_post_evaluation_payload_does_not_update_the_calibration_curve_on_the_test()
        {
            var expectedCalibrationCurve = MockRepository.GenerateStub<ICalibrationCurve>();
            _qcCalibrationTest.CalibrationCurve = expectedCalibrationCurve;

            var dummyCalibrationCurveToIgnore = MockRepository.GenerateStub<ICalibrationCurve>();
            var postPayload = CreateExpectedPayloadResultingFromProcessingTheCalibrationSample(dummyCalibrationCurveToIgnore, QcSampleEvaluationResult.FailedButContinue);

            _qcCalibrationTestPayloadProcessor.ProcessPostEvaluationPayload(postPayload, _qcCalibrationTest);

            Assert.AreEqual(expectedCalibrationCurve, _qcCalibrationTest.CalibrationCurve);
        }

        private static CalibrationPostEvaluationPayload CreateExpectedPayloadResultingFromProcessingTheCalibrationSample(ICalibrationCurve expectedCalibrationCurve, QcSampleEvaluationResult result)
        {
            var passingMetadata = new QcSampleEvaluationMetadata(Guid.Empty, result);
            return new CalibrationPostEvaluationPayload {Metadata = passingMetadata, CalibrationCurve = expectedCalibrationCurve };
        }
    }
    // ReSharper restore InconsistentNaming
}
