using System.Linq;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Payloads;
using Daxor.Lab.QC.Core.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.Services_Tests.QcCalibrationTestPayloadProcessor_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_dealing_with_the_background_payloads
    {
        private QcCalibrationTest _qcCalibrationTest;
        private QcSample _qcBackgroundSample;
        private QcCalibrationTestPayloadProcessor _qcCalibrationTestPayloadProcessor;

        [TestInitialize]
        public void TestInitialize()
        {
            _qcCalibrationTestPayloadProcessor = new QcCalibrationTestPayloadProcessor();
            _qcCalibrationTest = new QcCalibrationTest(QcSchemaProvider.GetCalibrationSamplesLayoutSchema(), null);
            _qcCalibrationTest.BuildSamples();
            _qcBackgroundSample = _qcCalibrationTest.Samples.FirstOrDefault(s => s.TypeOfSample == SampleType.Background);
        }

        [TestMethod]
        public void Then_the_prepayload_is_correct()
        {
            var observedPrePayload = _qcCalibrationTestPayloadProcessor.CreatePreEvaluationPayload(_qcBackgroundSample, _qcCalibrationTest);

            var expectedPrePayload = new BackgroundPreEvaluationPayload { Sample = _qcBackgroundSample };

            Assert.IsTrue(ArePrePayloadsEqual(expectedPrePayload, observedPrePayload));
        }

        private static bool ArePrePayloadsEqual(PreEvaluationPayload expectedPrePayload, PreEvaluationPayload observedPrePayload)
        {
            return expectedPrePayload.Sample == observedPrePayload.Sample;
        }
    }
    // ReSharper restore InconsistentNaming
}
