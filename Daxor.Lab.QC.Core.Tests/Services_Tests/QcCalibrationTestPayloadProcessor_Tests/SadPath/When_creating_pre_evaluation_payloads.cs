using System;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.Services_Tests.QcCalibrationTestPayloadProcessor_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_creating_pre_evaluation_payloads
    {
        [TestMethod]
        public void And_the_sample_is_null_then_an_exception_is_thrown()
        {
            var processor = new QcCalibrationTestPayloadProcessor();
            AssertEx.ThrowsArgumentNullException(()=>processor.CreatePreEvaluationPayload(null, new QcCalibrationTest(null)), "sample");
        }

        [TestMethod]
        public void And_the_test_is_null_then_an_exception_is_thrown()
        {
            var processor = new QcCalibrationTestPayloadProcessor();
            AssertEx.ThrowsArgumentNullException(()=>processor.CreatePreEvaluationPayload(new QcSample(), null), "test");
        }

        [TestMethod]
        public void And_there_is_not_a_pre_evaluation_payload_for_the_sample_then_an_exception_is_thrown()
        {
            var processor = new QcCalibrationTestPayloadProcessor();
            AssertEx.Throws<NotSupportedException>(()=>processor.CreatePreEvaluationPayload(new QcSample {TypeOfSample = SampleType.Undefined},
                new QcCalibrationTest(null)));
        }

        [TestMethod]
        public void And_dealing_with_calibration_sample_and_the_test_has_no_setup_sample_then_an_exception_is_thrown()
        {
            var processor = new QcCalibrationTestPayloadProcessor();
            AssertEx.Throws<InvalidOperationException>(()=>processor.CreatePreEvaluationPayload(new QcSample { TypeOfSample = SampleType.QC2 },
                new QcCalibrationTest(null)));
        }
    }
    // ReSharper restore InconsistentNaming
}
