using System;
using Daxor.Lab.QC.Core.Payloads;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.QC.Core.Tests.Services_Tests.QcCalibrationTestPayloadProcessor_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_processing_post_evaluation_payloads
    {
        [TestMethod]
        public void And_the_payload_is_not_for_anything_pertaining_to_a_calibration_test_then_an_exception_is_thrown()
        {
            var badPayload = MockRepository.GeneratePartialMock<PostEvaluationPayload>();
            var processor = new QcCalibrationTestPayloadProcessor();
            AssertEx.Throws<NotSupportedException>(()=>processor.ProcessPostEvaluationPayload(badPayload, new QcCalibrationTest(null)));
        }
    }
    // ReSharper restore InconsistentNaming
}
