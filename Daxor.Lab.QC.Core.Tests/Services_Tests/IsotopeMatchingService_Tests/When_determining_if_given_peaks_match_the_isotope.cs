using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.Services_Tests.IsotopeMatchingService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_given_peaks_match_the_isotope
    {
        private static Isotope _bariumIsotope;
        private static int _energyCount;
        private static IIsotopeMatchingService _isotopeMatchingService;

        [ClassInitialize]
        public static void TestsClassInitialize(TestContext context)
        {
            _bariumIsotope = new Isotope(IsotopeType.Ba133, 10.500, UnitOfTime.Year, new[] { 31.60, 81.00, 356.00 });
            _energyCount = _bariumIsotope.Energies.Count();
            _isotopeMatchingService = new IsotopeMatchingService();
        }

        [TestMethod]
        public void And_there_are_fewer_peaks_than_this_isotopes_energies_then_the_isotopes_do_not_match()
        {
            var peaks = new List<Peak>();
            for (var i = 0; i < _energyCount - 1; i++) peaks.Add(new Peak {Centroid = 10});

            Assert.IsFalse(_isotopeMatchingService.IsMatching(_bariumIsotope, peaks));
        }

        [TestMethod]
        public void And_there_are_more_peaks_than_this_isotopes_energies_then_the_isotopes_do_not_match()
        {
            var peaks = new List<Peak>
            {
                new Peak {Centroid = 31.60},
                new Peak {Centroid = 81.00},
                new Peak {Centroid = 355.00},
                new Peak {Centroid = 356.00}
            };

            Assert.IsFalse(_isotopeMatchingService.IsMatching(_bariumIsotope, peaks));
        }
    }
    // ReSharper restore InconsistentNaming
}
