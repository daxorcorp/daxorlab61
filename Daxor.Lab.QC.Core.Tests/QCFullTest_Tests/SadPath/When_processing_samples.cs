using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.QCFullTest_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_processing_samples
    {
        [TestMethod]
        public void And_the_sample_is_null_then_an_exception_is_thrown()
        {
            var fullTest = new QcFullTest(null);

            AssertEx.ThrowsArgumentNullException(()=>fullTest.ProcessSample(null), "sample");
        }
    }
    // ReSharper restore InconsistentNaming
}
