using System.Linq;
using Daxor.Lab.QC.Core.Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.QCContaminationTest_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_processing_samples
    {
        [TestMethod]
        public void And_the_sample_is_not_a_source_sample_then_the_count_limit_is_set_to_the_default_value()
        {
            const int backgroundPosition = 24;

            // Arrange
            var backgroundSample = new QcSample { PresetCountsLimit = 4000, Position = backgroundPosition };
            var contaminationTest = new StubQCContaminationTestThatDoesNotProcessSampleIfRunning();

            contaminationTest.BuildSamples();

            var backgroundSampleFromTest = (from s in contaminationTest.Samples where s.Position == backgroundPosition select s).FirstOrDefault();
            if (backgroundSampleFromTest == null)
                Assert.Fail("Cannot find the background sample in the test");

            // Act
            contaminationTest.ProcessSample(backgroundSample);

            // Assert
            Assert.AreEqual(-1, backgroundSampleFromTest.PresetCountsLimit);
        }
    }
    // ReSharper restore InconsistentNaming
}
