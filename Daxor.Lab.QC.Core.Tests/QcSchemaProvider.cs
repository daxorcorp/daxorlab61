﻿using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Interfaces;

namespace Daxor.Lab.QC.Core.Tests
{
    public class QcSchemaProvider : ISchemaProvider
    {
        public SamplesSchema GetSamplesLayoutSchema(TestType qcTest)
        {
            switch (qcTest)
            {
                case TestType.Calibration:
                    return GetCalibrationSamplesLayoutSchema();
                case TestType.Contamination:
                    return GetContaminationSamplesLayoutSchema();
                case TestType.Standards:
                    return GetStandardsSamplesLayoutSchema();
                case TestType.LinearityWithoutCalibration:
                    return GetLinearityWithoutCalibrationSamplesLayoutSchema();
                default: return null;
            }
        }

        public static SamplesSchema GetLinearityWithoutCalibrationSamplesLayoutSchema()
        {
            var valuesToInclude = new List<int> { 24, 2, 21, 22 };
            var wheel = GenerateWheel();
            var results = (from x in wheel where valuesToInclude.Contains(x.Key) select x).ToDictionary(pair => pair.Key, pair => pair.Value);

            return new SamplesSchema(2, results);
        }


        public static SamplesSchema GetCalibrationSamplesLayoutSchema()
        {
            var valuesToInclude = new List<int> { 1, 2, 24 };
            var wheel = GenerateWheel();
            var results = (from x in wheel where valuesToInclude.Contains(x.Key) select x).ToDictionary(pair => pair.Key, pair => pair.Value);

            return new SamplesSchema(2, results);
        }

        public static SamplesSchema GetContaminationSamplesLayoutSchema()
        {
            var valuesToExclude = new List<int> {1, 2, 11, 12, 21, 22};
            var wheel = GenerateWheel();
            var results = (from x in wheel where !valuesToExclude.Contains(x.Key) select x).ToDictionary(pair => pair.Key, pair => pair.Value);

            return new SamplesSchema(3, results);
        }

        public static SamplesSchema GetStandardsSamplesLayoutSchema()
        {
            var valuesToInclude = new List<int> { 11, 12, 24 };
            var wheel = GenerateWheel();
            var results = (from x in wheel where valuesToInclude.Contains(x.Key) select x).ToDictionary(pair => pair.Key, pair => pair.Value);

            return new SamplesSchema(4, results);
        }

        private static Dictionary<int, SampleSchemaItem> GenerateWheel()
        {
            return new Dictionary<int, SampleSchemaItem>
            {
                {
                    1, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Cs137,
                        FriendlyName = "QC 1",
                        Position = 1,
                        SerialNumber = "",
                        Type = SampleType.QC1
                    }
                },
                {
                    2, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Ba133,
                        FriendlyName = "QC 2",
                        Position = 2,
                        SerialNumber = "",
                        Type = SampleType.QC2
                    }
                },
                {
                    3, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Standard A",
                        Position = 3,
                        SerialNumber = "",
                        Type = SampleType.StandardA
                    }
                },
                {
                    4, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Baseline A",
                        Position = 4,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    5, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Sample 1A",
                        Position = 5,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    6, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Sample 2A",
                        Position = 6,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    7, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Sample 3A",
                        Position = 7,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    8, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Sample 4A",
                        Position = 8,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    9, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Sample 5A",
                        Position = 9,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    10, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Sample 6A",
                        Position = 10,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    11, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Standard A QC",
                        Position = 11,
                        SerialNumber = "",
                        Type = SampleType.StandardA
                    }
                },
                {
                    12, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Standard B QC",
                        Position = 12,
                        SerialNumber = "",
                        Type = SampleType.StandardB
                    }
                },
                {
                    13, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Sample 6B",
                        Position = 13,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    14, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Sample 5B",
                        Position = 14,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    15, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Sample 4B",
                        Position = 15,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    16, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Sample 3B",
                        Position = 16,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    17, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Sample 2B",
                        Position = 17,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    18, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Sample 1B",
                        Position = 18,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    19, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Baseline B",
                        Position = 19,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    20, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Standard B",
                        Position = 20,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    21, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Ba133,
                        FriendlyName = "QC 3",
                        Position = 21,
                        SerialNumber = "",
                        Type = SampleType.QC3
                    }
                },
                {
                    22, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Ba133,
                        FriendlyName = "QC 4",
                        Position = 22,
                        SerialNumber = "",
                        Type = SampleType.QC4
                    }
                },
                {
                    23, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Empty B",
                        Position = 23,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
                {
                    24, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Background",
                        Position = 24,
                        SerialNumber = "",
                        Type = SampleType.Background
                    }
                },
                {
                    25, new SampleSchemaItem
                    {
                        ContainingIsotope = IsotopeType.Empty,
                        FriendlyName = "Empty A",
                        Position = 25,
                        SerialNumber = "",
                        Type = SampleType.Contamination
                    }
                },
            };
        }
    }
}
