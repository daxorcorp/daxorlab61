using System;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.FineGainAdjustmentService_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_the_fine_gain
    {
        private static IFineGainAdjustmentService _fineGainAdjustmentService;
        private const double TooBigOfAnAdjustment = 0.003;

        // Code that should be executed before running the first test in this class.
        [ClassInitialize]
        public static void TestsClassInitialize(TestContext context)
        {
            _fineGainAdjustmentService = new FineGainAdjustmentService();
            _fineGainAdjustmentService.InitialFineGainAdjustment = TooBigOfAnAdjustment;
        }

        [TestMethod]
        public void And_the_fine_gain_metadata_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_fineGainAdjustmentService.DetermineNewFineGainMetadata(0, 0, null), "currentFineGainMetadata");
        }

        [TestMethod]
        public void And_the_new_fine_gain_is_less_than_the_minimum_fine_gain_then_an_exception_is_thrown()
        {
            var fineGainMetadata = new FineGainMetadata
            {
                CurrentFineGain = 1.0,
                MinFineGain = 0.999,
                MaxFineGain = 1.3,
                IsFirstAdjustment = true
            };
            AssertEx.Throws<InvalidOperationException>(()=>_fineGainAdjustmentService.DetermineNewFineGainMetadata(50, 51, fineGainMetadata));
        }

        [TestMethod]
        public void And_the_new_fine_gain_is_greater_than_the_maximum_fine_gain_then_an_exception_is_thrown()
        {
            var fineGainMetadata = new FineGainMetadata
            {
                CurrentFineGain = 1.0,
                MinFineGain = 0.9,
                MaxFineGain = 1.002,
                IsFirstAdjustment = true
            };
           AssertEx.Throws<InvalidOperationException>(()=>_fineGainAdjustmentService.DetermineNewFineGainMetadata(51, 50, fineGainMetadata));
        }
    }
    // ReSharper restore InconsistentNaming
}
