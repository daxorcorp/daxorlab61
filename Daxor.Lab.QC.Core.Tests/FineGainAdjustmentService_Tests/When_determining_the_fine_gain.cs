﻿using System;
using Daxor.Lab.QC.Core.Interfaces;
using Daxor.Lab.QC.Core.Services;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.FineGainAdjustmentService_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_the_fine_gain
    {
        private IFineGainAdjustmentService _fineGainAdjustmentService;
        private const double MinimumAdjustment = 0.002;

        [TestInitialize]
        public void TestInitialize()
        {
            _fineGainAdjustmentService = new FineGainAdjustmentService {InitialFineGainAdjustment = MinimumAdjustment};
        }

        [TestMethod]
        [RequirementValue(0.001)]
        public void Then_the_smallest_supported_fine_gain_adjustment_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsDouble(), FineGainAdjustmentService.MinimumAdjustment);
        }

        [TestMethod]
        [RequirementValue(0.000001)]
        public void Then_the_centroid_equality_threshold_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsDouble(), FineGainAdjustmentService.CentroidEqualityThreshold);
        }

        [TestMethod]
        public void And_making_the_first_adjustment_and_the_expected_centroid_is_below_the_actual_centroid_then_the_fine_gain_is_lowered_by_the_minimum_amount()
        {
            const double expectedCentroid = 10.0;
            const double actualPeakCentroid = 12.0;
            const double currentFineGain = 1.15;

            const double expectedFineGain = currentFineGain - MinimumAdjustment;

            var metadata = new FineGainMetadata
            {
                CurrentFineGain = currentFineGain,
                MaxFineGain = 1.23,
                MinFineGain = 1.00,
                IsFirstAdjustment = true
            };

            var actualFineGainMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroid,
                actualPeakCentroid, metadata);

            Assert.AreEqual(expectedFineGain, actualFineGainMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_making_the_first_adjustment_and_the_expected_centroid_is_above_the_actual_centroid_then_the_fine_gain_is_raised_by_the_minimum_amount()
        {
            const double actualPeakCentroid = 10.0;
            const double expectedCentroid = 12.0;
            const double currentFineGain = 1.15;

            const double expectedFineGain = currentFineGain + MinimumAdjustment;

            var metadata = new FineGainMetadata
            {
                MaxFineGain = 1.23,
                MinFineGain = 1.00,
                CurrentFineGain = currentFineGain,
                IsFirstAdjustment = true
            };

            var actualFineGainMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroid,
                actualPeakCentroid, metadata);

            Assert.AreEqual(expectedFineGain, actualFineGainMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_making_the_first_adjustment_and_the_expected_centroid_is_equal_to_the_actual_centroid_then_the_fine_gain_is_not_adjusted()
        {
            const double actualPeakCentroid = 12.0;
            const double expectedCentroid = actualPeakCentroid;
            const double currentFineGain = 1.15;

            const double expectedFineGain = currentFineGain;

            var metadata = new FineGainMetadata
            {
                MaxFineGain = 1.23,
                MinFineGain = 1.00,
                IsFirstAdjustment = true,
                CurrentFineGain = 1.15
            };

            var actualFineGainMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroid,
                actualPeakCentroid, metadata);

            Assert.AreEqual(expectedFineGain, actualFineGainMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_making_the_first_adjustment_and_the_centroids_do_not_differ_enough_then_the_fine_gain_is_not_adjusted()
        {
            const double actualPeakCentroid = 12.0;
            const double expectedCentroid =
                actualPeakCentroid + (FineGainAdjustmentService.CentroidEqualityThreshold/2.0);
            const double currentFineGain = 1.15;

            const double expectedFineGain = currentFineGain;

            var metadata = new FineGainMetadata
            {
                MaxFineGain = 1.23,
                MinFineGain = 1.00,
                IsFirstAdjustment = true,
                CurrentFineGain = 1.15
            };

            var actualFineGainMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroid,
                actualPeakCentroid, metadata);

            Assert.AreEqual(expectedFineGain, actualFineGainMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_making_the_first_adjustment_then_the_new_fine_gain_should_be_rounded_down_if_needed()
        {
            const double actualPeakCentroid = 12.0;
            const double expectedCentroid =
                actualPeakCentroid + (FineGainAdjustmentService.CentroidEqualityThreshold/2.0);
            const double expectedFineGain = 1.150;
            const double tooSmallOfAdjustment = 0.0004;

            var metadata = new FineGainMetadata
            {
                MaxFineGain = 1.23,
                MinFineGain = 1.00,
                IsFirstAdjustment = true,
                CurrentFineGain = expectedFineGain + tooSmallOfAdjustment
            };

            _fineGainAdjustmentService.InitialFineGainAdjustment = tooSmallOfAdjustment;

            var actualFineGainMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroid,
                actualPeakCentroid, metadata);

            Assert.AreEqual(expectedFineGain, actualFineGainMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_making_the_first_adjustment_then_the_new_fine_gain_should_be_rounded_up_if_needed()
        {
            const double actualPeakCentroid = 12.0;
            const double expectedCentroid =
                actualPeakCentroid + (FineGainAdjustmentService.CentroidEqualityThreshold/2.0);
            const double expectedFineGain = 1.151;
            const double tooSmallOfAdjustment = 0.0009;

            var metadata = new FineGainMetadata
            {
                MaxFineGain = 1.23,
                MinFineGain = 1.00,
                IsFirstAdjustment = true,
                CurrentFineGain = 1.150 + tooSmallOfAdjustment
            };

            _fineGainAdjustmentService.InitialFineGainAdjustment = tooSmallOfAdjustment;

            var actualFineGainMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroid,
                actualPeakCentroid, metadata);

            Assert.AreEqual(expectedFineGain, actualFineGainMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_making_the_first_adjustment_then_the_new_fine_gain_should_be_rounded_away_from_zero_if_needed()
        {
            const double actualPeakCentroid = 12.0;
            const double expectedCentroid =
                actualPeakCentroid + (FineGainAdjustmentService.CentroidEqualityThreshold/2.0);
            const double expectedFineGain = 1.151;
            const double tooSmallOfAdjustment = 0.0005;
            const double startingFineGain = 1.1505;

            var metadata = new FineGainMetadata
            {
                MaxFineGain = 1.23,
                MinFineGain = 1.00,
                IsFirstAdjustment = true,
                CurrentFineGain = startingFineGain
            };

            _fineGainAdjustmentService.InitialFineGainAdjustment = tooSmallOfAdjustment;

            var actualFineGainMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroid,
                actualPeakCentroid, metadata);

            Assert.AreEqual(expectedFineGain, actualFineGainMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_making_an_adjustment_beyond_the_first_one_and_the_expected_centroid_is_equal_to_the_actual_centroid_then_the_fine_gain_is_not_adjusted()
        {
            const double actualPeakCentroid = 12.0;
            const double expectedCentroid = actualPeakCentroid;
            const double currentFineGain = 1.15;

            const double expectedFineGain = currentFineGain;

            var metadata = new FineGainMetadata
            {
                CurrentFineGain = currentFineGain,
                MaxFineGain = 1.23,
                MinFineGain = 1.00,
                IsFirstAdjustment = false
            };

            var actualFineGainMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroid,
                actualPeakCentroid, metadata);

            Assert.AreEqual(expectedFineGain, actualFineGainMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_making_an_adjustment_beyond_the_first_one_and_the_expected_centroid_is_less_than_the_actual_centroid_then_the_fine_gain_is_found_using_binary_search()
        {
            const double actualPeakCentroid = 12.0;
            const double expectedCentroid = 10;
            const double minFineGain = 0.6;
            const double currentFineGain = 1.1;
            const double expectedFineGain = (minFineGain + currentFineGain)/2.0;

            var metadata = new FineGainMetadata
            {
                CurrentFineGain = 1.1,
                MaxFineGain = 1.4,
                MinFineGain = minFineGain,
                IsFirstAdjustment = false
            };

            var actualFineGainMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroid,
                actualPeakCentroid, metadata);

            Assert.AreEqual(expectedFineGain, actualFineGainMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_making_an_adjustment_beyond_the_first_one_and_the_expected_centroid_is_greater_than_the_actual_centroid_then_the_fine_gain_is_found_using_binary_search()
        {
            const double actualPeakCentroid = 10.0;
            const double expectedCentroid = 12;
            const double maxFineGain = 1.4;
            const double currentFineGain = 1.1;
            const double expectedFineGain = (maxFineGain + currentFineGain)/2.0;

            var metadata = new FineGainMetadata
            {
                CurrentFineGain = currentFineGain,
                MaxFineGain = maxFineGain,
                MinFineGain = 0.6,
                IsFirstAdjustment = false
            };
            _fineGainAdjustmentService.InitialFineGainAdjustment = 0;
            var actualFineGainMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroid,
                actualPeakCentroid, metadata);

            Assert.AreEqual(expectedFineGain, actualFineGainMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_making_an_adjustment_beyond_the_first_one_then_the_minimal_adjustment_should_be_001()
        {
            const double actualPeakCentroid = 10.0;
            const double expectedCentroid = 12;
            const double maxFineGain = 1.001;
            const double currentFineGain = 1.0;
            var expectedFineGain = Math.Round(currentFineGain, 3, MidpointRounding.AwayFromZero);

            var metadata = new FineGainMetadata
            {
                IsFirstAdjustment = false,
                MaxFineGain = maxFineGain,
                MinFineGain = 1.0,
                CurrentFineGain = currentFineGain
            };

            _fineGainAdjustmentService.InitialFineGainAdjustment = 0;

            var actualFineGainMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroid,
                actualPeakCentroid, metadata);

            Assert.AreEqual(expectedFineGain, actualFineGainMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_the_expcected_centroid_is_greater_than_the_actual_peak_centroid_and_making_an_adjustment_beyond_the_first_one_and_the_current_fine_gain_is_not_rounded_then_the_metadata_is_not_affected_by_lack_of_rounding()
        {
            const double actualPeakCentroid = 10.0;
            const double expectedCentroid = 12;
            const double maxFineGain = 1.001;
            const double currentFineGain = 1.000427;
            var expectedFineGain = Math.Round(currentFineGain, 3, MidpointRounding.AwayFromZero);

            var metadata = new FineGainMetadata
            {
                IsFirstAdjustment = false,
                MaxFineGain = maxFineGain,
                MinFineGain = 0,
                CurrentFineGain = currentFineGain
            };

            _fineGainAdjustmentService.InitialFineGainAdjustment = 0;

            var actualFineGainMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroid,
                actualPeakCentroid, metadata);

            Assert.AreEqual(expectedFineGain, actualFineGainMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_the_actual_peak_centroid_is_greater_than_the_expected_centroid_and_making_an_adjustment_beyond_the_first_one_and_the_current_fine_gain_is_not_rounded_then_the_metadata_is_not_affected_also()
        {
            const double actualPeakCentroid = 12.0;
            const double expectedCentroid = 10.0;
            const double maxFineGain = 1.001;
            const double currentFineGain = 1.000427;
            const double expectedFineGain = 0.500;

            var metadata = new FineGainMetadata
            {
                IsFirstAdjustment = false,
                MaxFineGain = maxFineGain,
                MinFineGain = 0,
                CurrentFineGain = currentFineGain
            };

            _fineGainAdjustmentService.InitialFineGainAdjustment = 0;

            var actualFineGainMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(expectedCentroid,
                actualPeakCentroid, metadata);

            Assert.AreEqual(expectedFineGain, actualFineGainMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_the_new_fine_gain_is_equal_to_the_minimum_fine_gain_then_the_new_fine_gain_is_the_minimum()
        {
            var fineGainMetadata = new FineGainMetadata
            {
                CurrentFineGain = 1.0,
                MinFineGain = 1.0 - MinimumAdjustment,
                MaxFineGain = 1.002,
                IsFirstAdjustment = true
            };
            var observedMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(50, 51, fineGainMetadata);

            Assert.AreEqual(fineGainMetadata.MinFineGain, observedMetadata.CurrentFineGain);
        }

        [TestMethod]
        public void And_the_new_fine_gain_is_equal_to_the_maximum_fine_gain_then_the_new_fine_gain_is_the_maximum()
        {
            var fineGainMetadata = new FineGainMetadata
            {
                CurrentFineGain = 1.0,
                MinFineGain = 0.9,
                MaxFineGain = 1.0 + MinimumAdjustment,
                IsFirstAdjustment = true
            };
            var observedMetadata = _fineGainAdjustmentService.DetermineNewFineGainMetadata(51, 50, fineGainMetadata);

            Assert.AreEqual(fineGainMetadata.MaxFineGain, observedMetadata.CurrentFineGain);
        }
    }

    // ReSharper restore InconsistentNaming
}
