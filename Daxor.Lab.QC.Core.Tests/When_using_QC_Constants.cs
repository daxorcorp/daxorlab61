using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_QC_constants
    {
        [TestMethod]
        [RequirementValue(308)]
        public void Then_the_damped_Ba133_lower_bound_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), QcDomainConstants.DampedBa133LowerEnergyBound);
        }

        [TestMethod]
        [RequirementValue(420)]
        public void Then_the_damped_Ba133_upper_bound_matches_requirements()
        {
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), QcDomainConstants.DampedBa133UpperEnergyBound);
        }
    }
    // ReSharper restore InconsistentNaming
}
