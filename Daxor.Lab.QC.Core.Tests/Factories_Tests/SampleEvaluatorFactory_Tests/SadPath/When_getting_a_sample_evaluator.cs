using System;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Factories;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.Factories_Tests.SampleEvaluatorFactory_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_a_sample_evaluator
    {
        [TestMethod]
        public void When_the_sample_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>CalibrationTestSampleEvaluatorFactory.GetSampleEvaluator(null), "sampleToEvaluate");
        }

        [TestMethod]
        public void When_the_type_of_sample_does_not_have_a_corresponding_evaluator_then_an_exception_is_thrown()
        {
            var undefinedSample = new QcSample { TypeOfSample = SampleType.Undefined };
            AssertEx.Throws<NotSupportedException>(()=>CalibrationTestSampleEvaluatorFactory.GetSampleEvaluator(undefinedSample));
        }
    }
    // ReSharper restore InconsistentNaming
}
