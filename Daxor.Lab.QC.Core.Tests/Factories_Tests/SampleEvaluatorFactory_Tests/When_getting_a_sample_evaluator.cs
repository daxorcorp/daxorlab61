using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Factories;
using Daxor.Lab.QC.Core.SampleEvaluators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.Factories_Tests.SampleEvaluatorFactory_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_a_sample_evaluator
    {
        [TestMethod]
        public void And_using_the_background_sample_then_the_correct_sample_evaluator_is_returned()
        {
            var sample = new QcSample {TypeOfSample = SampleType.Background};
            var sampleEvaluator = CalibrationTestSampleEvaluatorFactory.GetSampleEvaluator(sample);

            Assert.IsTrue(sampleEvaluator is BackgroundSampleEvaluator);
        }

        [TestMethod]
        public void And_using_the_setup_sample_then_the_correct_sample_evaluator_is_returned()
        {
            var sample = new QcSample { TypeOfSample = SampleType.QC1 };
            var sampleEvaluator = CalibrationTestSampleEvaluatorFactory.GetSampleEvaluator(sample);

            Assert.IsTrue(sampleEvaluator is SetupSampleEvaluator);
        }

        [TestMethod]
        public void And_using_the_calibration_sample_then_the_correct_sample_evaluator_is_returned()
        {
            var sample = new QcSample { TypeOfSample = SampleType.QC2 };
            var sampleEvaluator = CalibrationTestSampleEvaluatorFactory.GetSampleEvaluator(sample);

            Assert.IsTrue(sampleEvaluator is CalibrationSampleEvaluator);
        }
    }
    // ReSharper restore InconsistentNaming
}
