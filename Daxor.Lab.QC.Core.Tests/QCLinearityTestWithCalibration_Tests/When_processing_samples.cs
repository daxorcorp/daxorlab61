using System;
using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Core.Tests.Stubs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Core.Tests.QCLinearityTestWithCalibration_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_processing_samples
    {
        [TestMethod]
        public void And_the_sample_is_a_source_sample_then_the_count_limit_is_set_to_the_correct_value()
        {
            const int expectedCountLimit = 4000;

            // Arrange
            var qc2Isotope = new Isotope(IsotopeType.Ba133, 0.0, 0.0, new List<double>());
            var qc2Source = new QcSource(qc2Isotope, DateTime.Now, 2, 0.0, 0.0, "1234", expectedCountLimit);
            var qc2Sample = new QcSample(qc2Source) { PresetCountsLimit = qc2Source.CountLimit, Position = qc2Source.SamplePosition };
            var linearityWithCalibrationTest = new StubQCLinearityWithCalibrationTestThatDoesNotProcessSampleIfRunning();

            linearityWithCalibrationTest.BuildSamples();

            var calibrationSampleFromTest = (from s in linearityWithCalibrationTest.Samples where s.Position == qc2Source.SamplePosition select s).FirstOrDefault();
            if (calibrationSampleFromTest == null)
                Assert.Fail("Cannot find the QC2 Source sample in the test");

            // Act
            linearityWithCalibrationTest.ProcessSample(qc2Sample);

            // Assert
            Assert.AreEqual(expectedCountLimit, calibrationSampleFromTest.PresetCountsLimit);
        }

        [TestMethod]
        public void And_the_sample_is_not_a_source_sample_then_the_count_limit_is_set_to_the_default_value()
        {
            const int backgroundPosition = 24;

            // Arrange
            var backgroundSample = new QcSample { PresetCountsLimit = 4000, Position = backgroundPosition };
            var linearityWithCalibrationTest = new StubQCLinearityWithCalibrationTestThatDoesNotProcessSampleIfRunning();

            linearityWithCalibrationTest.BuildSamples();

            var backgroundSampleFromTest = (from s in linearityWithCalibrationTest.Samples where s.Position == backgroundPosition select s).FirstOrDefault();
            if (backgroundSampleFromTest == null)
                Assert.Fail("Cannot find the background sample in the test");

            // Act
            linearityWithCalibrationTest.ProcessSample(backgroundSample);

            // Assert
            Assert.AreEqual(-1, backgroundSampleFromTest.PresetCountsLimit);
        }
    }

    // ReSharper restore InconsistentNaming
}
