﻿using Daxor.Lab.Help.Services.Navigation;
using Daxor.Lab.Help.Views;
using Daxor.Lab.Infrastructure.Constants;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Regions;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.IntegrationTests.Navigation_Tests.HelpModuleNavigator_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_initializing_the_navigator
	{
		private IEventAggregator _stubAggregator;
		private INavigationCoordinator _mockCoordinator;
		private IRegionManager _stubManager;
		private IUnityContainer _mockContainer;

		private HelpModuleNavigator _navigator;

		[TestInitialize]
		public void Initialize()
		{
			_mockCoordinator = Substitute.For<INavigationCoordinator>();
			_stubAggregator = Substitute.For<IEventAggregator>();
			_mockContainer = Substitute.For<IUnityContainer>();
			_stubManager = Substitute.For<IRegionManager>();
		}

        [TestMethod]
        [TestCategory("Integration Test")]
		[DeploymentItem("Daxor.Lab.ResourceDictionaries.dll")]
		public void Then_the_root_view_host_is_added_to_the_collection_of_views_as_the_main_view()
		{
			//Arrange
			var _stubView = new HelpMainView();
			_mockContainer.Resolve<HelpMainView>().Returns(_stubView);
            _navigator = new HelpModuleNavigator(_mockContainer, _stubAggregator, _stubManager, _mockCoordinator);

			//Act
			_navigator.Initialize();

			//Assert
			Assert.AreEqual(2, _navigator.Views.Count);
			Assert.AreEqual(_stubView, _navigator.Views[HelpModuleViewKeys.MainView]);
		}

        [TestMethod]
        [TestCategory("Integration Test")]
		[DeploymentItem("Daxor.Lab.ResourceDictionaries.dll")]
		public void Then_the_root_view_host_is_added_to_the_workspace_region()
		{
			//Arrange
			var _stubView = new HelpMainView();
			var _mockRegion = Substitute.For<IRegion>();
			_stubManager.Regions[RegionNames.WorkspaceRegion].Returns(_mockRegion);
			_mockContainer.Resolve<HelpMainView>().Returns(_stubView);
            _navigator = new HelpModuleNavigator(_mockContainer, _stubAggregator, _stubManager, _mockCoordinator);

			//Act
			_navigator.Initialize();

			//Assert
			_mockRegion.Received(1).Add(_stubView);
		}
	}
	// ReSharper restore InconsistentNaming
}
