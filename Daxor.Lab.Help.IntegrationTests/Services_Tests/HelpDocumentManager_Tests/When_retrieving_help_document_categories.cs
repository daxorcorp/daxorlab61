﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Daxor.Lab.Help.Common;
using Daxor.Lab.Help.Interfaces;
using Daxor.Lab.Help.Models;
using Daxor.Lab.Help.Services;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.Help.IntegrationTests.Services_Tests.HelpDocumentManager_Tests
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_retrieving_help_document_categories
	{
		private IFileManager _mockFileManager;

		private DirectoryInfo _directoryInfo;
		private HelpDocumentManager _documentManager;

		private const string TestPath = "TestPath";
		private const string TestDirectoryPath = @"c:\temp\TestDirectory\";

		[TestInitialize]
		public void Initialize()
		{
			_mockFileManager = Substitute.For<IFileManager>();
			_mockFileManager.DirectoryExists(TestPath).Returns(true);

			_directoryInfo = new DirectoryInfo(TestDirectoryPath);

			_mockFileManager.GetSubDirectoriesInDirectory(TestPath).Returns(_directoryInfo.GetDirectories());
            _documentManager = new HelpDocumentManager(_mockFileManager, Substitute.For<IDocumentGenerator>(), Substitute.For<ILoggerFacade>(), Substitute.For<ISettingsManager>());
		}

        [TestMethod]
        [TestCategory("Integration Test")]
		public void Then_the_category_names_match_the_folder_names_found_in_the_given_path()
		{
			// Arrange
			var expectedDocumentCategories = new List<DocumentCategory>
			{
				new DocumentCategory(@"DirectoryWithNoPdfs", new List<FileInfo>()),
				new DocumentCategory(@"DirectoryWithOnePdf", new List<FileInfo>{new FileInfo("Disclaimer.pdf")}),
				new DocumentCategory(@"Zebra", new List<FileInfo>()),
				new DocumentCategory(@"Disclaimer", new List<FileInfo>{new FileInfo("Disclaimer.pdf")}),
			};

			// Act
			var actualDocumentCategories = _documentManager.RetrieveHelpDocumentCategories(TestPath).ToList();

			// Assert
			var expectedCateogryNames = (from d in expectedDocumentCategories select d.Name).ToList();
			var actualCategoryNames = (from d in actualDocumentCategories select d.Name).ToList();
			CollectionAssert.AreEqual(expectedCateogryNames, actualCategoryNames);
		}

        [TestMethod]
        [TestCategory("Integration Test")]
		public void And_a_directory_contains_files_then_only_PDF_files_are_added_to_the_document_collection_for_that_category()
		{
			// Act
			var results = _documentManager.RetrieveHelpDocumentCategories(TestPath).ToList();

			// Assert
			var pdfDirectoryContents = (from c in results where c.Name == "DirectoryWithOnePdf" select c.Files).FirstOrDefault();
			var pdfFiles = (from f in pdfDirectoryContents where f.Extension == HelpConstants.PdfFileExtension select f).ToList();
			Assert.AreEqual(1, pdfFiles.Count());
		}

        [TestMethod]
        [TestCategory("Integration Test")]
		public void And_a_directory_contains_no_PDF_documents_then_the_document_collection_for_that_category_is_empty()
		{
			// Act
			var results = _documentManager.RetrieveHelpDocumentCategories(TestPath).ToList();

			// Assert
			var pdfDirectoryContents = (from c in results where c.Name == "DirectoryWithNoPdfs" select c.Files).FirstOrDefault();
			// ReSharper disable once AssignNullToNotNullAttribute
			Assert.IsFalse(pdfDirectoryContents.Any());
		}

        [TestMethod]
        [TestCategory("Integration Test")]
		public void And_there_is_a_disclaimer_category_then_it_is_listed_last()
		{
			//Act
			var results = _documentManager.RetrieveHelpDocumentCategories(TestPath).ToList();

			//Assert
			Assert.AreEqual(HelpConstants.DisclaimerName, results.Last().Name);
		}
	}
	// ReSharper restore InconsistentNaming
}
