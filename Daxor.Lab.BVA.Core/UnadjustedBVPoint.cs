﻿using System;
using Daxor.Lab.Domain.Evaluation;

namespace Daxor.Lab.BVA.Core
{
    /// <summary>
    /// Unadjusted Blood Volume Point
    /// </summary>
    public class UnadjustedBVPoint : EvaluatableObject
    {
        public event EventHandler UnadjustedBloodVolumeShouldChange;

        public BVACompositeSample BloodVolumeSample { get; set; }
        public int UnadjustedBloodVolume { get; set; }

        public UnadjustedBVPoint(BVACompositeSample bvSample): base(null)
        {
            BloodVolumeSample = bvSample;
        }

        protected virtual void OnUnadjustedBloodVolumeShouldChanged()
        {
            var handler = UnadjustedBloodVolumeShouldChange;
            if (handler != null)
                handler(this, new EventArgs());
        }
    }
}
