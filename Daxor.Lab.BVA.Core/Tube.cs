﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.BVA.Core
{
    /// <summary>
    /// Tube used by BVATest
    /// </summary>
    public sealed class Tube : Entity
    {
        private string _name;
        private int _id;
        private double _anticoagulantFactor;

        public string Name
        {
            get { return _name; }
            set { base.SetValue(ref _name, "Name", value); }
        }
        public int Id
        {
            get { return _id; }
            set { base.SetValue(ref _id, "Id", value); }
        }
        public double AnticoagulantFactor
        {
            get { return _anticoagulantFactor; }
            set { base.SetValue(ref _anticoagulantFactor, "AnticoagulantFactor", value); }
        }
    }
}
