﻿using System;
using System.Data.Linq;
using System.Linq;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.BVA.Core
{
    /// <summary>
    /// Blood Volume Specific Sample
    /// </summary>
    public sealed class BVASample : Sample
    {
        #region Fields

        string _fullName = String.Empty;                //Pat-1A, Pat-1B, Std-B, Std-A and Ctl-A

        bool _isCountExcluded;                          //True if counts are excluded; false otherwise
        bool _isCountAutoExcluded;                      //True if counts are excluded vai automatic point exclusion; false otherwise
        bool _isHematocritExcluded;                     //True is Hematocrit is excluded; false otherwise
        bool _isExcluded;                               //True if the whole sample is excluded, false otherwise

        double _hematocrit = -1;                        //Hematocrit for a given sample
        double _fwhm = -1;                              //Full Width at Half Max
        double _fwtm = -1;                              //Full Width at Tenth Max
        double _centroid = -1;                          //Centroid
        int _counts = -1;                               //Integral (sum of counts) in ROI
        int _piTimeSeconds = -1;                        //Post Injection Time in seconds

        int _roiLowBound = 308;                         //Low bound in channels
        int _roiHighBound = 420;                        //High bound in channels
        SampleType _sType = SampleType.Undefined;       //Sample type enum
        SampleRange _sRange = SampleRange.Undefined;    //Sample range (A, B)

        #endregion

        #region Ctor

        public BVASample(Guid sampleId, int position, IRuleEngine ruleEngine)
            : this(position, ruleEngine)
        {
            InternalId = sampleId;
        }
        public BVASample(int position, IRuleEngine ruleEngine)
        {
            Position = position;
            RuleEngine = ruleEngine;
        }
        #endregion

        #region Properties

        public Guid TestId
        {
            get;
            set;
        }
        public string FullName
        {
            get { return _fullName; }
            set { SetValue(ref _fullName, "FullName", value); }
        }
        public SampleType Type
        {
            get { return _sType; }
            set { SetValue(ref _sType, "Type", value); }
        }
        public SampleRange Range
        {
            get { return _sRange; }
            set { SetValue(ref _sRange, "Range", value); }
        }
        public bool IsCountExcluded
        {
            get { return _isCountExcluded; }
            set { SetValue(ref _isCountExcluded, "IsCountExcluded", value); }
        }
        public bool IsCountAutoExcluded
        {
            get { return _isCountAutoExcluded; }
            set
            {
                SetValue(ref _isCountAutoExcluded, "IsCountAutoExcluded", value);
                IsCountExcluded = value;
            }
        }
        public bool IsHematocritExcluded
        {
            get { return _isHematocritExcluded; }
            set { SetValue(ref _isHematocritExcluded, "IsHematocritExcluded", value); }
        }
        public bool IsExcluded
        {
            get { return _isExcluded; }
            set { SetValue(ref _isExcluded, "IsExcluded", value); }
        }
        public double Hematocrit
        {
            get { return _hematocrit; }
            set { SetValue(ref _hematocrit, "Hematocrit", value); }
        }

        public double FullWidthHalfMax
        {
            get { return _fwhm; }
            set { SetValue(ref _fwhm, "FullWidthHalfMax", value); }
        }
        public double FullWidthTenthMax
        {
            get { return _fwtm; }
            set { SetValue(ref _fwtm, "FullWidthTenthMax", value); }
        }
        public double Centroid
        {
            get { return _centroid; }
            set { SetValue(ref _centroid, "Centroid", value); }
        }
        public int Counts
        {
            get { return _counts; }
            set { SetValue(ref _counts, "Counts", value); }
        }

        public int ROIHighBound
        {
            get { return _roiHighBound; }
            set { SetValue(ref _roiHighBound, "ROIHighBound", value); }
        }
        public int ROILowBound
        {
            get { return _roiLowBound; }
            set { SetValue(ref _roiLowBound, "ROILowBound", value); }
        }

        public int PostInjectionTimeInSeconds
        {
            get { return _piTimeSeconds; }
            set { SetValue(ref _piTimeSeconds, "PostInjectionTimeInSeconds", value); }
        }

        public Binary SampleHeaderTimestamp { get; set; }
        public Binary BvaSampleTimestamp { get; set; }

        #endregion

        #region FirePropertyChanged override

        protected override void FirePropertyChanged(string propertyName)
        {
            base.FirePropertyChanged(propertyName);
            if (propertyName == "ExecutionStatus")
                base.FirePropertyChanged("Counts");
        }

        protected override void FirePropertyChanged(params string[] propertyNames)
        {
            base.FirePropertyChanged(propertyNames);
            if (propertyNames.Any(n => n == "ExecutionStatus"))
                base.FirePropertyChanged("Counts");
        }

        #endregion

        #region ToString()

        public override string ToString()
        {
            // Obviously this doesn't cover all the properties, but the bug that brought this method
            // into existence pertained to losing counts. [Geoff :: 5/30/2012]
            return DisplayName + " | " + ExecutionStatus + " | " + Counts;
        }

        #endregion
    }
}
