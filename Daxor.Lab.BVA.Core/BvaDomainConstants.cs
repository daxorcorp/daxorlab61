﻿namespace Daxor.Lab.BVA.Core
{
    /// <summary>
    /// Blood Volume Analysis constants.
    /// </summary>
    public static class BvaDomainConstants
    {
        #region Total blood volume ranges

        /// <summary>
        /// Minimum supported whole blood volume (in mL)
        /// </summary>
        public static readonly int MinimumBloodVolumeInmL = 500;

        /// <summary>
        /// Maximum supported whole blood volume (in mL)
        /// </summary>
        public static readonly int MaximumBloodVolumeInmL = 14500;

        #endregion

        #region Sample duration

        public static readonly int MaximumSampleCountDurationInSeconds = 3600;

        #endregion
        
        #region Standard deviation and transudation

        /// <summary>
        /// Maximum supported standard deviation for blood volume points  
        /// </summary>
        public static readonly double MaximumBloodVolumePointStandardDeviation = 0.039;

        /// <summary>
        /// Value at which the transudation rate is considered high (i.e., rates lower
        /// than this are "normal")
        /// </summary>
        public static readonly double MinimumHighTransudationRate = 0.004;

        /// <summary>
        /// Maximum value at which the transudation rate is considered high (i.e., rates
        /// higher than this are "unusually high")
        /// </summary>
        public static readonly double MaximumHighTransudationRate = 0.005;

        #endregion

        #region Post-injection times

        /// <summary>
        /// Highest recommended post-injection time (in seconds)
        /// </summary>
        public static readonly int MaximumPostInjectionTimeInSeconds = 75 * 60;

        /// <summary>
        /// Lowest recommended post-injection time (in seconds)
        /// </summary>
        public static readonly int MinimumPostInjectionTimeInSeconds = 600;

        #endregion

        #region Weight boundaries

        public static readonly string MaximumWeightInPounds = "1430.00";

        public static readonly string MaximumWeightInKilograms = "648.64";

        public static readonly string MinimumWeightInPounds = "0.02";

        public static readonly string MinimumWeightInKilograms = "0.01";

        #endregion

        #region Height boundaries

        public static readonly string MaximumHeightInCentimeters = "253.7";

        public static readonly string MaximumHeightInInches = "99.9";

        public static readonly string MinimumHeightInCentimeters = "0.3";

        public static readonly string MinimumHeightInInches = "0.1";

        #endregion

        #region Weight deviation

        /// <summary>
        /// Minimum Weight deviation
        /// </summary>
        public static readonly int MinimumWeightDeviation = -40;

        /// <summary>
        /// Weight deviation above which is considered high
        /// </summary>
        public static readonly int HighWeightDeviationThreshold = 260;

        /// <summary>
        /// Maximum Weight deviation
        /// </summary>
        public static readonly int MaximumWeightDeviation = 350;

        #endregion

        #region Age

        /// <summary>
        /// Age (in years) above which the user should verify is correct
        /// </summary>
        public static readonly int HighAgeInYearsThreshold = 100;
        
        /// <summary>
        /// Maximum supported age (in years)
        /// </summary>
        public static readonly int MaximumAgeInYears = 150;

        #endregion

        #region Hematocrits

        /// <summary>
        /// Maximum supported amount of difference from a test's average hematocrit
        /// </summary>
        public static readonly int MaximumDifferenceFromAverageHematocrit = 2;

        /// <summary>
        /// Maximum supported hematocrit
        /// </summary>
        public static readonly int MaximumHematocrit = 60;

        /// <summary>
        /// Minimum supported hematocrit
        /// </summary>
        public static readonly int MinimumHematocrit = 20;
        
        #endregion

        #region Counts

        /// <summary>
        /// Minimum number of counts from the reference standards
        /// </summary>
        public static readonly int MinimumStandardCounts = 10000;

        /// <summary>
        /// Sample counts must be at least this many times greater than the baseline counts
        /// </summary>
        public static readonly int MinimumMultipleOfBaselineForSampleCounts = 2;

        /// <summary>
        /// Background counts must not be more than this many times greater than the baseline counts
        /// </summary>
        public static readonly int MaximumMultipleOfBaselineForBackgroundCounts = 2;

        #endregion

        #region Spectroscopy

        public static readonly double I131LowerEnergyBound = 308;

        public static readonly double I131UpperEnergyBound = 420;

        #endregion
    }
}
