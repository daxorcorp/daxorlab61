﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.BVA.Core
{
    public interface ISampleLayoutSchema
    {
        IEnumerable<SampleLayoutItem> SampleLayoutItems { get; }
        int SchemaID { get; }

        int GetPosition(SampleType type, SampleRange range);
        SampleLayoutItem GetLayoutItem(int position);
    }
}
