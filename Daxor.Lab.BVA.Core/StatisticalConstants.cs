﻿
namespace Daxor.Lab.BVA.Core
{
    /// <summary>
    /// Constants used in the computation of statistical measures pertaining to blood volumes.
    /// </summary>
    public static class StatisticalConstants
    {
        /// <summary>
        /// Default coefficient of variance for determining the difference between two 
        /// patient sample count values
        /// </summary>
        public const double PATIENT_SAMPLE_COEFFICIENT_OF_VARIANCE = 0.3;

        /// <summary>
        /// Maximum supported difference (in percentage points) for the two entered
        /// hematocrit values when using the "last two" hematocrit method.
        /// </summary>
        public const double MaximumPercentageDifferenceForLastTwoHematocrits = 1.0;
    }
}
