﻿using System;
using Daxor.Lab.Domain.Evaluation;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.BVA.Core
{
    /// <summary>
    /// Blood Volume used by the test
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public sealed class BVAVolume : EvaluatableObject
    {
        #region Fields

        private int _redCellCount;
        private int _plasmaCount;
	    private int _correctionFactor;

        #endregion

        #region Ctor

        public BVAVolume(IRuleEngine ruleEngine) : base(ruleEngine)
        {
	        _correctionFactor = 0;
        }

        #endregion

        #region Properties

        public BloodType Type
        {
            set;
            get;
        }
        public int RedCellCount
        {
            get { return (int) Math.Round(_redCellCount * (1 - (CorrectionFactor * 0.01)), 0, MidpointRounding.AwayFromZero); }
            set
            {
                if (SetValue(ref _redCellCount, "RedCellCount", value))
                    FirePropertyChanged("WholeCount");
            }
        }
        public int PlasmaCount
        {
			get { return (int) Math.Round(_plasmaCount * (1 - (CorrectionFactor * 0.01)), 0, MidpointRounding.AwayFromZero); }
            set
            {
                if (SetValue(ref _plasmaCount, "PlasmaCount", value))
                    FirePropertyChanged("WholeCount");
            }
        }
        public int WholeCount
        {
	        get
	        {
		        return RedCellCount + PlasmaCount;
	        }
        }

	    public int CorrectionFactor
	    {
		    get { return _correctionFactor; }
		    set
		    {
			    if (Type != BloodType.Ideal || value < -1 || value > 9) return;

			    _correctionFactor = value == -1 ? 0 : value;
				FirePropertyChanged("CorrectionFactor");
				FirePropertyChanged("RedCellCount");
				FirePropertyChanged("PlasmaCount");
				FirePropertyChanged("WholeCount");
		    }
	    }

	    #endregion
    }
}
