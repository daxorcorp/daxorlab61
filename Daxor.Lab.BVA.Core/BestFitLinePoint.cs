﻿using Daxor.Lab.Domain.Entities.Base;

namespace Daxor.Lab.BVA.Core
{
    /// <summary>
    /// Denotes a single best fit point used for the blood volume linear regression
    /// </summary>
    public class BestFitLinePoint : ObservableObject
    {
        #region Fields

        double _iValue;
        double _dValue;

        #endregion

        #region Properties

        public double IndependentValue
        {
            get { return _iValue; }
            set { base.SetValue(ref _iValue, "IndependentValue", value); }
        }

        public double DependentValue
        {
            get { return _dValue; }
            set { base.SetValue(ref _dValue, "DependentValue", value); }
        }

        #endregion
    }
}
