﻿namespace Daxor.Lab.BVA.Core
{
    /// <summary>
    /// Represents metadata about the automatic point exclusion service.
    /// </summary>
    public class AutomaticPointExclusionMetadata
    {
        /// <summary>
        /// Gets or sets metadata about various actions/states that occurred during
        /// the algorithm's execution
        /// </summary>
        public string IntermediateMetadata { get; set; }

        /// <summary>
        /// Gets or sets the last computed residual standard error
        /// </summary>
        public double LastComputedResidualStandardError { get; set; }

        /// <summary>
        /// Gets or sets the last computed standard deviation
        /// </summary>
        public double LastComputedStandardDeviation { get; set; }

        /// <summary>
        /// Gets or sets the last computed transudation rate
        /// </summary>
        public double LastComputedTransudationRate { get; set; }
        
        /// <summary>
        /// Gets or sets the number of points excluded
        /// </summary>
        public int NumberOfPointsExcluded { get; set; }

        /// <summary>
        /// Gets or sets the reason the service was not run to completion
        /// </summary>
        public string ReasonServiceNotRunToCompletion { get; set; }
        
        /// <summary>
        /// Gets or sets whether the service ran to completion
        /// </summary>
        public bool WasServiceRunToCompletion { get; set; }

        /// <summary>
        /// Determines whether this instance is the same as the given object
        /// </summary>
        /// <param name="obj">Object for comparison</param>
        /// <returns>True if the instances are the same reference or semantically equal;
        /// false otherwise</returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((AutomaticPointExclusionMetadata) obj);
        }

        /// <summary>
        /// Determines whether this instance is the same as the given instance
        /// </summary>
        /// <param name="other">Instance for comparison</param>
        /// <returns>True if the instances are semantically equal; false otherwise</returns>
        protected bool Equals(AutomaticPointExclusionMetadata other)
        {
            return NumberOfPointsExcluded == other.NumberOfPointsExcluded &&
                   string.Equals(ReasonServiceNotRunToCompletion, other.ReasonServiceNotRunToCompletion) &&
                   WasServiceRunToCompletion.Equals(other.WasServiceRunToCompletion) &&
                   string.Equals(IntermediateMetadata, other.IntermediateMetadata) &&
                   LastComputedResidualStandardError.Equals(other.LastComputedResidualStandardError) &&
                   LastComputedStandardDeviation.Equals(other.LastComputedStandardDeviation) &&
                   LastComputedTransudationRate.Equals(other.LastComputedTransudationRate);
        }

        /// <summary>
        /// Computes a hashcode for this object's state
        /// </summary>
        /// <returns>Hashcode for this object's state</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = NumberOfPointsExcluded;
                hashCode = (hashCode * 397) ^ (ReasonServiceNotRunToCompletion != null ? ReasonServiceNotRunToCompletion.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ WasServiceRunToCompletion.GetHashCode();
                hashCode = (hashCode * 397) ^ IntermediateMetadata.GetHashCode();
                hashCode = (hashCode * 397) ^ LastComputedResidualStandardError.GetHashCode();
                hashCode = (hashCode * 397) ^ LastComputedStandardDeviation.GetHashCode();
                hashCode = (hashCode * 397) ^ LastComputedTransudationRate.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// Returns a string representation of the properties of this instance
        /// </summary>
        /// <returns>String representation of the properties of this instance</returns>
        public override string ToString()
        {
            return string.Format("NumberOfPointsExcluded: {0}, WasServiceRunToCompletion: {1}, ReasonServiceNotRunToCompletion: {2}, IntermediateMetadata: {3}, LastComputedResidualStandardError: {4}, LastComputedStandardDeviation: {5}, LastComputedTransudationRate: {6}",
                    NumberOfPointsExcluded, WasServiceRunToCompletion, ReasonServiceNotRunToCompletion, IntermediateMetadata, LastComputedResidualStandardError, LastComputedStandardDeviation, LastComputedTransudationRate);
        }
    }
}
