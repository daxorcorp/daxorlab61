﻿using System;
using System.Linq;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.BVA.Core
{ 
    /// <summary>
    /// Blood Volume specific patient
    /// </summary>
    public sealed class BVAPatient : Patient
    {
        #region Fields

        private string _pacNumber;

        #endregion

        #region Ctor

        public BVAPatient(IRuleEngine ruleEngine)
        {
            HeightInCm = 0f;
            WeightInKg = 0f;
            RuleEngine = ruleEngine;
        }

        public BVAPatient(Guid patientId, IRuleEngine ruleEngine)
        {
            InternalId = patientId;
            HeightInCm = 0f;
            WeightInKg = 0f;
            RuleEngine = ruleEngine;
        }

        #endregion

        #region Properties

        public string PacNumber
        {
            get { return _pacNumber; }
            set { SetValue(ref _pacNumber, "PacNumber", value); }
        }
        public double DeviationFromIdealWeight { get; set; }

        #endregion

        #region Overrides

        public override void EvaluatePropertyByName(params string[] propertyName)
        {
            base.EvaluatePropertyByName(propertyName);
            if (propertyName.AsParallel().Any(p => p == "FirstName" || p == "MiddleName" || p == "LastName"))
                base.EvaluatePropertyByName("FullName");
        }

        protected override string InternalEvaluatePropertyByName(string propertyName)
        {
            if (propertyName == "FullName")
            {
                var errors = base.InternalEvaluatePropertyByName("FirstName");
                if (!String.IsNullOrEmpty(errors))
                    return errors;

                errors = base.InternalEvaluatePropertyByName("LastName");
                if (!String.IsNullOrEmpty(errors))
                    return errors;

                errors = base.InternalEvaluatePropertyByName("MiddleName");
                return errors;
            }

            return base.InternalEvaluatePropertyByName(propertyName);
        }

        #endregion
    }
}