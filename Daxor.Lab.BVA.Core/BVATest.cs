﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Entities;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.BVA.Core
{
    /// <summary>
    /// BVATest is a BVA Bounded Context entity.
    /// </summary>
    // ReSharper disable InconsistentNaming
    public class BVATest : Test<BVASample>
    // ReSharper restore InconsistentNaming
    {
        #region Constants

        public const int MaximumProtocolCount = 6;
        public const int MinimumProtocolCount = 1;

        #endregion

        #region Private member fields

        string _refPhysician;       //Referring Physician - a person who is going to be notified about the dtoTest results
        string _ccReportTo;         //CC Report to - another person where the dtoTest results will go to
        string _injectateLot;       //Injectate lot used for the dtoTest
        string _pacsId;             //PACS id
        string _injectateKey;       //Barcode for the injectate
        string _standardAKey;       //Barcode for the standard A
        string _standardBKey;       //Barcode for the standard B
        string _testId2;            //Another id used to identify dtoTest
        string _physiciansSpecialty;

        double _injectateDose;      //Radioactivity does in microCuries used to injectate a patient with.
        int _protocol = -1;         //Number of samples (A&B parts act as a single sample)
        bool _hasGoodBackground;
        double _regStandardDev;

        Tube _tube;
        HematocritFillType _hematocritFill = HematocritFillType.Unknown;
        TestMode _mode;                                 //Mode of the dtoTest: Real, Training, VOPS.
        BVAPatient _patient;                            //Patient that a dtoTest belongs to

		int _considerationId;                           //Amputee, etc.
		int _durationAdjustedBackgroundCounts;          //Duration adjusted background counts
	    private int _amputeeIdealsCorrectionFactor;		//Percentage to reduce ideals if patient is an amputee
		int _refVolume;                                 //Reference volume

        double _transudationRateResult;
        double _stdDevResult;
        double _normalizeHematocritResult;
        string _injectateDoseUnits = "microCuries";

        bool _hasSufficientData;
        readonly List<BVACompositeSample> _compositeSamples;
        readonly Dictionary<BloodType, BVAVolume> _volumes;

        #endregion

        #region Ctors

        public BVATest(IRuleEngine ruleEngine)
        {
            RuleEngine = ruleEngine;
            
            Status = TestStatus.Pending;
            BackgroundCount = -1;
            SampleDurationInSeconds = -1;
            Patient = new BVAPatient(ruleEngine);

            _compositeSamples = new List<BVACompositeSample>();
	        _amputeeIdealsCorrectionFactor = -1;

            //Define dictionary of volumes: Measured and Ideals
            _volumes = new Dictionary<BloodType, BVAVolume>();
            _volumes[BloodType.Measured] = new BVAVolume(ruleEngine) { Type = BloodType.Measured, RedCellCount = 0, PlasmaCount = 0 };
            _volumes[BloodType.Ideal] = new BVAVolume(ruleEngine) { Type = BloodType.Ideal, RedCellCount = 0, PlasmaCount = 0 };
        }

        public BVATest(ISampleLayoutSchema sampleSchema, IRuleEngine ruleEngine)
            : this(ruleEngine)
        {
            if (sampleSchema == null)
                throw new ArgumentNullException("sampleSchema");

            SampleLayoutId = sampleSchema.SchemaID;
            CurrentSampleSchema = sampleSchema;
            BuildBaselineAndStandardSamples();
        }

        #endregion

        #region Properties

        public BVAPatient Patient
        {
            get { return _patient; }
            set
            {
                var prevPatient = _patient;
                if (!SetValue(ref _patient, "Patient", value)) return;

                if (_patient != null) _patient.PropertyChanged += ForwardAmputeePropertyChanged;
                
                // Dispose of prev patient
                if (prevPatient == null) return;
                prevPatient.PropertyChanged -= ForwardAmputeePropertyChanged;
                prevPatient.Dispose();
            }
        }

        public ISampleLayoutSchema CurrentSampleSchema
        {
            get;
            private set;
        }

        public IEnumerable<BVASample> StandardSamples
        {
            get
            {
                return from sample in Samples
                       where sample.Type == SampleType.Standard
                       select sample;
            }
        }

        public IEnumerable<BVASample> BaselineSamples
        {
            get
            {
                return from sample in Samples
                       where sample.Type == SampleType.Baseline
                       select sample;
            }
        }
        public IEnumerable<BVASample> PatientSamples
        {
            get
            {
                return from sample in Samples
                       where (sample.Type != SampleType.Background &&
                              sample.Type != SampleType.Baseline &&
                              sample.Type != SampleType.Standard)
                       select sample;
            }
        }

        public IEnumerable<BVACompositeSample> CompositeSamples
        {
            get { return _compositeSamples; }
        }
        public IEnumerable<BVACompositeSample> PatientCompositeSamples
        {
            get
            {
                return (from cSample in CompositeSamples
                        where cSample.SampleType != SampleType.Baseline &&
                              cSample.SampleType != SampleType.Background &&
                              cSample.SampleType != SampleType.Standard
                        select cSample);
            }
        }

        public BVACompositeSample StandardCompositeSample
        {
            get
            {
                return (from s in CompositeSamples where s.SampleType == SampleType.Standard select s).FirstOrDefault();
            }
        }
        public BVACompositeSample PatientBaselineCompositeSample
        {
            get
            {
                return (from s in CompositeSamples where s.SampleType == SampleType.Baseline select s).FirstOrDefault();
            }
        }
        public BVACompositeSample PatientFirstCompositeSample
        {
            get
            {
                return (from s in PatientCompositeSamples where s.SampleType == SampleType.Sample1 select s).FirstOrDefault();
            }
        }

        public IDictionary<BloodType, BVAVolume> Volumes
        {
	        get
	        {
		        return _volumes;
	        }
        }

        public int DurationAdjustedBackgroundCounts
        {
            get { return _durationAdjustedBackgroundCounts; }
            private set { SetValue(ref _durationAdjustedBackgroundCounts, "DurationAdjustedBackgroundCounts", value); }
        }
        public int RejectedPatientCompositeSamplesCount
        {
            get
            {
                return (from cSample in PatientCompositeSamples
                        where cSample.IsWholeSampleExcluded 
                        select cSample).Count();
            }
        }
        public int BothCountsExcludedPatientCompositeSampleCount
        {
            get
            {
                return (from cSample in PatientCompositeSamples
                        where cSample.AreBothCountsExcluded 
                        select cSample).Count();
            }
        }

	    public int AmputeeIdealsCorrectionFactor
	    {
		    get
		    {
			    return !Patient.IsAmputee ? 0 : _amputeeIdealsCorrectionFactor;
		    }
		    set
		    {
				SetValue(ref _amputeeIdealsCorrectionFactor, "AmputeeIdealsCorrectionFactor", value);

				//Note that while a test's correction factor can be negative, a blood volume's reduction percentage can not be below 0.
				//In that case, the blood volume will set it's reduction percentage to 0.
				_volumes[BloodType.Ideal].CorrectionFactor = _amputeeIdealsCorrectionFactor;
		    }
	    }
        public int Protocol
        {
            get { return _protocol; }
            set
            {
                //Have to build samples based on protocol then fire that sample are built
                if (value != _protocol)
                {
                    BuildPatientSamplesBasedOnProtocol(value);
                    UpdateSamplesHematocritBasedOnFill();
                }

                SetValue(ref _protocol, "Protocol", value);
            }
        }
        public int RefVolume
        {
            get { return _refVolume; }
            set { SetValue(ref _refVolume, "RefVolume", value); }
        }
        public int ConsiderationId
        {
            get { return _considerationId; }
            set { SetValue(ref _considerationId, "ConsiderationId", value); }
        }

        public double TransudationRateResult
        {
            get { return _transudationRateResult; }
            set { SetValue(ref _transudationRateResult, "TransudationRateResult", value); }
        }
        public double StandardDevResult
        {
            get { return _stdDevResult; }
            set { SetValue(ref _stdDevResult, "StandardDevResult", value); }
        }

        public double RegressionStandardDevResult
        {
            get { return _stdDevResult; }
            set { SetValue(ref _regStandardDev, "RegressionStandardDevResult", value); }
        }
        public double NormalizeHematocritResult
        {
            get { return _normalizeHematocritResult; }
            set { SetValue(ref _normalizeHematocritResult, "NormalizeHematocritResult", value); }
        }

        public double PatientHematocritResult
        {
            get
            {
	            if (HematocritFill == HematocritFillType.OnePerTest)
	            {
		            if (PatientFirstCompositeSample != null && PatientFirstCompositeSample.SampleA != null)
			            return PatientFirstCompositeSample.SampleA.Hematocrit;

		            return 0;
	            }

	            // ReSharper disable CompareOfFloatsByEqualityOperator
                var goodHematocritSamples = from s in Samples
                                            where s.Type != SampleType.Background &&
                                                  s.Type != SampleType.Standard &&
                                                  s.Type != SampleType.Baseline &&
                                                  s.IsExcluded == false && s.IsHematocritExcluded == false && s.Hematocrit != -1
                                            select s;
                // ReSharper restore CompareOfFloatsByEqualityOperator

                // ReSharper disable PossibleMultipleEnumeration
                // You need to know if there are things to take averages of, otherwise you'll get an exception.
                if (goodHematocritSamples.Any())
                    return goodHematocritSamples.Average(s => s.Hematocrit);
                // ReSharper restore PossibleMultipleEnumeration

                return 0;
            }
        }

        public double InjectateDose
        {
            get { return _injectateDose; }
            set
            {
                if (SetValue(ref _injectateDose, "InjectateDose", value))
                    if (Status == TestStatus.Pending)
                    {
                        // ReSharper disable CompareOfFloatsByEqualityOperator
                        if (value != -1)
                            SampleDurationInSeconds = value != 0 ? (int)((30.0 / _injectateDose) * TimeConstants.SecondsPerMinute) : -1;
                        // ReSharper restore CompareOfFloatsByEqualityOperator
                    }
            }
        }
        public string RefPhysician
        {
            get { return _refPhysician; }
            set { SetValue(ref _refPhysician, "RefPhysician", value); }
        }
        public string CcReportTo
        {
            get { return _ccReportTo; }
            set { SetValue(ref _ccReportTo, "CcReportTo", value); }
        }

        public string PhysiciansSpecialty
        {
            get { return _physiciansSpecialty; }
            set { SetValue(ref _physiciansSpecialty, "PhysiciansSpecialty", value); }
        }

        public string InjectateLot
        {
            get { return _injectateLot; }
            set { SetValue(ref _injectateLot, "InjectateLot", value); }
        }
        
        // ReSharper disable InconsistentNaming
        public string TestID2
        // ReSharper restore InconsistentNaming
        {
            get { return _testId2; }
            set { SetValue(ref _testId2, "TestID2", value); }
        }
        public string InjectateKey
        {
            get { return _injectateKey; }
            set { SetValue(ref _injectateKey, "InjectateKey", value); }
        }
        public string StandardAKey
        {
            get { return _standardAKey; }
            set { SetValue(ref _standardAKey, "StandardAKey", value); }
        }
        public string StandardBKey
        {
            get { return _standardBKey; }
            set { SetValue(ref _standardBKey, "StandardBKey", value); }
        }
        // ReSharper disable InconsistentNaming
        public string PacsID
        // ReSharper restore InconsistentNaming
        {
            get { return _pacsId; }
            set { SetValue(ref _pacsId, "PacsID", value); }
        }

		public string InjectateDoseUnits
        {
            get { return _injectateDoseUnits; }
            set { SetValue(ref _injectateDoseUnits, "InjectateDoseUnits", value); }
        }
        public HematocritFillType HematocritFill
        {
            get { return _hematocritFill; }
            set
            {
                //Update each sample hematocrit
                if (SetValue(ref _hematocritFill, "HematocritFill", value))
                    UpdateSamplesHematocritBasedOnFill();
            }
        }

        public TestMode Mode
        {
            get { return _mode; }
            set
            {
                SetValue(ref _mode, "Mode", value);
                if (RuleEngine == null) return;

                DisableOrEnableUserRequiredRules(_mode == TestMode.Manual || _mode == TestMode.VOPS);
                DisableOrEnableTestStartRequiredRules(_mode == TestMode.Manual);
                EnsureInjectateVerificationEnabledBasedOnMode(_mode);
            }
        }
        public Tube Tube
        {
            get { return _tube; }
            set { SetValue(ref _tube, "Tube", value); }
        }

        public object ConcurrencyObject { get; set; }

        public bool HasSufficientData
        {
            get { return _hasSufficientData; }
            set { SetValue(ref _hasSufficientData, "HasSufficientData", value); }
        }
        public bool HasGoodBackground
        {
            get { return _hasGoodBackground; }
            set { SetValue(ref _hasGoodBackground, "HasGoodBackground", value); }
        }

	    public bool PatientIsAmputee
	    {
			get { return Patient.IsAmputee; }
		    set
		    {
			    if (!Patient.IsAmputee || !value)
				    AmputeeIdealsCorrectionFactor = -1;

				Patient.IsAmputee = value;

				FirePropertyChanged("AmputeeIdealsCorrectionFactor");
		    }
	    }

        #endregion

        #region PrivateMethods

        //Builds Standard, Baseline, etc.
        public void BuildBaselineAndStandardSamples()
        {
            var baselineAndStandardSamples = (from s in CurrentSampleSchema.SampleLayoutItems
                                              where s.Type == SampleType.Baseline || s.Type == SampleType.Standard
                                              select s).ToList();

            if (baselineAndStandardSamples == null)
                throw new Exception("Failed to build Baseline and/or Standard Samples");

            baselineAndStandardSamples.ForEach(
                sPosition =>
                {
                    var sample = new BVASample(Guid.NewGuid(), sPosition.Position, RuleEngine)
                    {
                        Type = sPosition.Type,
                        Range = sPosition.Range,
                        DisplayName = sPosition.FriendlyName
                    };

                    InnerSamples.Add(sample);
                });


            _compositeSamples.Add(new BVACompositeSample(this, StandardSamples, RuleEngine));
            _compositeSamples.Add(new BVACompositeSample(this, BaselineSamples, RuleEngine));
        }
        void BuildPatientSamplesBasedOnProtocol(int newProtocol)
        {
            if (newProtocol < MinimumProtocolCount) return;
            if (newProtocol > MaximumProtocolCount) return;

            //Current samples
            var groupedCurrentPatientSamples = from p in PatientSamples
                                               group p by p.Type;

            //Number of patient samples groups (Sample1, Sample2, Sample3, etc.)
            // ReSharper disable PossibleMultipleEnumeration
            var numExistingPatientSamplesGroups = groupedCurrentPatientSamples.Count();
            // ReSharper restore PossibleMultipleEnumeration

            //Determine what needs to be done (add more samples or remove some extra ones)
            var numSamplesGroupsRequired = newProtocol - numExistingPatientSamplesGroups;
            if (newProtocol == numExistingPatientSamplesGroups)
                return;

            if (newProtocol > numExistingPatientSamplesGroups)
            {
                if (numSamplesGroupsRequired == newProtocol)
                {
                    //None of the patient samples exist
                    //We need to create numSampleFromHere
                    var sampleTypesToCreate = Enumerable.Range((int)SampleType.Sample1, numSamplesGroupsRequired);
                    sampleTypesToCreate.ToList().ForEach(typeAsInt =>
                    {
                        //Create 2 samples per type, side A and B
                        int actualSamplePositionSideA = CurrentSampleSchema.GetPosition((SampleType)typeAsInt, SampleRange.A);
                        int actualSamplePositionSideB = CurrentSampleSchema.GetPosition((SampleType)typeAsInt, SampleRange.B);
                        
                        var sampleA = new BVASample(actualSamplePositionSideA, RuleEngine)
                        {
                            Type = (SampleType)typeAsInt,
                            Range = SampleRange.A,
                            DisplayName = CurrentSampleSchema.GetLayoutItem(actualSamplePositionSideA).FriendlyName
                        };

                        var sampleB = new BVASample(actualSamplePositionSideB, RuleEngine)
                        {
                            Type = (SampleType)typeAsInt,
                            Range = SampleRange.B,
                            DisplayName = CurrentSampleSchema.GetLayoutItem(actualSamplePositionSideB).FriendlyName
                        };

                        InnerSamples.Add(sampleA);
                        InnerSamples.Add(sampleB);

                        _compositeSamples.Add(new BVACompositeSample(this, sampleA, sampleB, RuleEngine));
                    });
                }
                else
                {
                    //Protocol value has been changed, we need to add additional samples
                    // ReSharper disable PossibleMultipleEnumeration
                    var highestGroup = (from gSample in groupedCurrentPatientSamples
                                        orderby gSample.Key descending
                                        select gSample).FirstOrDefault();
                    // ReSharper restore PossibleMultipleEnumeration
                    if (highestGroup == null)
                        throw new NotSupportedException("Unable to build patient samples based on protocol because the highest group sample couldn't be obtained");
                    var hType = highestGroup.Key;

                    //We need to create numSampleFromHere
                    var sampleTypesToCreate = Enumerable.Range((int)hType + 1, numSamplesGroupsRequired);


                    sampleTypesToCreate.ToList().ForEach(typeAsInt =>
                    {
                        //Create 2 samples per type, side A and B
                        int actualSamplePositionSideA = CurrentSampleSchema.GetPosition((SampleType)typeAsInt, SampleRange.A);
                        int actualSamplePositionSideB = CurrentSampleSchema.GetPosition((SampleType)typeAsInt, SampleRange.B);

                        var sampleA = new BVASample(actualSamplePositionSideA, RuleEngine)
                        {
                            Type = (SampleType)typeAsInt,
                            Range = SampleRange.A,
                            DisplayName = CurrentSampleSchema.GetLayoutItem(actualSamplePositionSideA).FriendlyName
                        };

                        var sampleB = new BVASample(actualSamplePositionSideB, RuleEngine)
                        {
                            Type = (SampleType)typeAsInt,
                            Range = SampleRange.B,
                            DisplayName = CurrentSampleSchema.GetLayoutItem(actualSamplePositionSideB).FriendlyName
                        };

                        InnerSamples.Add(sampleA);
                        InnerSamples.Add(sampleB);

                        _compositeSamples.Add(new BVACompositeSample(this, sampleA, sampleB, RuleEngine));
                    });
                }
            }
            else
            {
                //Start Cutting from the end.
                //Protocol value has been changed, we need to add additional samples
                // ReSharper disable PossibleMultipleEnumeration
                var highestGroup = (from gSample in groupedCurrentPatientSamples
                                    orderby gSample.Key descending
                                    select gSample).FirstOrDefault();
                // ReSharper restore PossibleMultipleEnumeration
                if (highestGroup == null)
                    throw new NotSupportedException("Unable to build patient samples based on protocol because the highest group sample couldn't be obtained");
                var hType = highestGroup.Key;

                //We need to create numSampleFromHere
                var sampleGroupTypesToRemove = Enumerable.Range((int)hType + numSamplesGroupsRequired + 1, Math.Abs(numSamplesGroupsRequired));

                InnerSamples.RemoveAll(sample => sampleGroupTypesToRemove.Contains((int)sample.Type));
                _compositeSamples.RemoveAll(cSample => sampleGroupTypesToRemove.Contains((int)cSample.SampleType));
            }

        }

        public void AssignSampleTypeAndRangeToSample(BVASample sample)
        {
            if (sample == null)
                throw new ArgumentNullException("sample");

            if (CurrentSampleSchema == null)
                throw new Exception("BAssignSampleTypeAndRangeToSample() failed -- CurrentSampleSchema cannot be null");

            var pInfo = (from i in CurrentSampleSchema.SampleLayoutItems
                                      where i.Position == sample.Position
                                      select i).FirstOrDefault();
            
            if (pInfo == null) return;

            sample.Type = pInfo.Type;
            sample.Range = pInfo.Range;
            sample.DisplayName = pInfo.FriendlyName;
        }

        public virtual void DisableOrEnableUserRequiredRules(bool disable)
        {
            //disable user required rules
            var uRequiredRulesToExecuteTest = from r in RuleEngine.Rules.AsParallel()
                                              where r.SeverityKind == "UserRequired"
                                              select r;
            foreach (var rule in uRequiredRulesToExecuteTest)
            {
                if (rule.EntityType == typeof(BVAPatient))
                {
                    if (!disable)
                        Patient.EnableRule(rule);
                    else
                        Patient.DisableRule(rule);
                }
                else if (rule.EntityType == typeof(BVATest))
                {
                    if (!disable)
                        EnableRule(rule);
                    else
                        DisableRule(rule);
                }
            }
        }
        void DisableOrEnableTestStartRequiredRules(bool disable)
        {
            var uRequiredRulesToExecuteTest = from r in RuleEngine.Rules.AsParallel()
                                              where (r.PropertyName == "InjectateLot" || r.PropertyName == "SampleDurationInSeconds") &&
                                              r.SeverityKind == "SystemRequired"
                                              select r;
            foreach (var rule in uRequiredRulesToExecuteTest)
            {
                if (rule.EntityType != GetType()) continue;
                if (!disable)
                    EnableRule(rule);
                else
                    DisableRule(rule);
            }
        }
        void EnsureInjectateVerificationEnabledBasedOnMode(TestMode mode)
        {
            if (mode != TestMode.VOPS)
                return;

            var uInjectateRules = from r in RuleEngine.Rules.AsParallel()
                                  where r.PropertyName == "InjectateKey" || r.PropertyName == "StandardAKey" || r.PropertyName == "StandardBKey"
                                  select r;

            //Add ignore rules
            if (mode == TestMode.VOPS || mode == TestMode.Manual)
                uInjectateRules.ForAll(DisableRule);

            //Remove ignore rules
            else
                uInjectateRules.ForAll(EnableRule);
        }

        private void ForwardAmputeePropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName != "IsAmputee") return;

            if (!PatientIsAmputee)
                AmputeeIdealsCorrectionFactor = -1;

            FirePropertyChanged("PatientIsAmputee");
        }

        #endregion

        #region PublicMethods

        public void UpdateRuleEngine(IRuleEngine ruleEngine)
        {
            RuleEngine = ruleEngine;
            Patient.RuleEngine = ruleEngine;
            foreach (var compositeSample in _compositeSamples)
            {
                compositeSample.RuleEngine = ruleEngine;
                if (compositeSample.SampleA != null) compositeSample.SampleA.RuleEngine = ruleEngine;
                if (compositeSample.SampleB != null) compositeSample.SampleB.RuleEngine = ruleEngine;
            }
            foreach (var volumeKey in _volumes.Keys)
                _volumes[volumeKey].RuleEngine = ruleEngine;

            // Previously, the data mapper set the Mode, which affects rules, but there was
            // no rule engine at that time. These statements can now run given that there 
            // is a live instance of a rule engine.
            DisableOrEnableUserRequiredRules(_mode == TestMode.Manual || _mode == TestMode.VOPS);
            DisableOrEnableTestStartRequiredRules(_mode == TestMode.Manual);
            EnsureInjectateVerificationEnabledBasedOnMode(_mode);
        }

        /// <summary>
        /// Business logic to decide if a test can start
        /// </summary>
        /// <returns></returns>
        public bool CanStartExecution()
        {
            //Cannot start the test if the mode of the test is not either Automatic or VOPS and status is not Pending
            if (Mode != TestMode.Automatic && Mode != TestMode.VOPS && Status != TestStatus.Pending)
                return false;

            //Check test's broken rules
            var hasAnyTestInvalidRequiredRules = BrokenRules.Any(e =>
                e.SeverityKind == "SystemRequired" ||
                e.SeverityKind == "UserRequired");

            if (hasAnyTestInvalidRequiredRules)
                return false;

            //Check Patient's broken rules
            var hasAnyPatientInvalidRequiredRules = Patient.BrokenRules.Any(e =>
                e.SeverityKind == "SystemRequired" ||
                e.SeverityKind == "UserRequired");

            return !hasAnyPatientInvalidRequiredRules;
        }

        /// <summary>
        /// Updates hematocrits of each sample based on the fill
        /// </summary>
        public void UpdateSamplesHematocritBasedOnFill()
        {
            HematocritFillType newHematocritFill = HematocritFill;
            if (newHematocritFill == HematocritFillType.Unknown || newHematocritFill == HematocritFillType.TwoPerSample)
                return;

            //One per sample
            if (newHematocritFill == HematocritFillType.OnePerSample)
            {
                foreach (var compositeSample in CompositeSamples)
                {
                    compositeSample.SampleB.Hematocrit = compositeSample.SampleA.Hematocrit;
                    
                    // Cannot exclude hct for one-per-sample protocol
                    compositeSample.SampleA.IsHematocritExcluded = false;
                    compositeSample.SampleB.IsHematocritExcluded = false;
                }
            }
            //One per Test
            else if (newHematocritFill == HematocritFillType.OnePerTest)
            {

                double patientSample1HematocritA = 0;
                if (PatientFirstCompositeSample != null && PatientFirstCompositeSample.SampleA != null)
                {
                    PatientFirstCompositeSample.SampleB.Hematocrit = PatientFirstCompositeSample.SampleA.Hematocrit;
                    patientSample1HematocritA = PatientFirstCompositeSample.SampleA.Hematocrit;
                }


                //Go through baseline samples
                foreach (var baselineSample in BaselineSamples)
                {
                    baselineSample.Hematocrit = patientSample1HematocritA;
                    baselineSample.IsHematocritExcluded = false;
                }

                //Go through each patient samples
                foreach (var ptnSample in PatientSamples)
                {
                    ptnSample.Hematocrit = patientSample1HematocritA;
                    ptnSample.IsHematocritExcluded = false;
                }
            }
            else if (newHematocritFill == HematocritFillType.LastTwo)
            {
                var orderedPatientSamples = new List<BVACompositeSample>();
                var patientSample = PatientFirstCompositeSample;
                if (patientSample == null) return;
                do
                {
                    orderedPatientSamples.Add(patientSample);
                    patientSample = FindCompositeSampleAfter(patientSample);
                } while (patientSample != null);

                var numPatientSamples = orderedPatientSamples.Count;
                var secondToLastCompositeSample = orderedPatientSamples[numPatientSamples - 2];
                var lastCompositeSample = orderedPatientSamples[numPatientSamples - 1];

                double averageHematocritOfLastTwo = -1;
                // ReSharper disable CompareOfFloatsByEqualityOperator
                if (secondToLastCompositeSample.HematocritA != -1 && lastCompositeSample.HematocritA != -1)
                    averageHematocritOfLastTwo = (secondToLastCompositeSample.HematocritA + lastCompositeSample.HematocritA) / 2;
                // ReSharper restore CompareOfFloatsByEqualityOperator

                foreach (var baselineSample in BaselineSamples)
                {
                    baselineSample.Hematocrit = averageHematocritOfLastTwo;
                    baselineSample.IsHematocritExcluded = false;
                }

                for (var i=0; i < numPatientSamples - 2; i++)
                {
                    orderedPatientSamples[i].SampleA.Hematocrit = averageHematocritOfLastTwo;
                    orderedPatientSamples[i].SampleB.Hematocrit = averageHematocritOfLastTwo;
                    orderedPatientSamples[i].SampleA.IsHematocritExcluded = false;
                    orderedPatientSamples[i].SampleB.IsHematocritExcluded = false;
                }

                // Fill in B-side of composite samples
                secondToLastCompositeSample.SampleB.Hematocrit = secondToLastCompositeSample.SampleA.Hematocrit;
                lastCompositeSample.SampleB.Hematocrit = lastCompositeSample.SampleA.Hematocrit;

                // Prevent last two hematocrits from being excluded
                secondToLastCompositeSample.SampleA.IsHematocritExcluded = false;
                lastCompositeSample.SampleA.IsHematocritExcluded = false;
            }
        }

        public BVACompositeSample FindCompositeSampleAfter(BVACompositeSample currentSample)
        {
            //Return null if null was given
            if (currentSample == null)
                return null;

            if (_compositeSamples == null)
                return null;

            var indexOfCurrent = _compositeSamples.IndexOf(currentSample);
            if (indexOfCurrent < 0)
                return null;

            return _compositeSamples.ElementAtOrDefault(indexOfCurrent + 1);
        }
        public BVACompositeSample FindCompositeSampleBefore(BVACompositeSample currentSample)
        {
            //Return null if null was given
            if (currentSample == null)
                return null;

            if (_compositeSamples == null)
                return null;

            var indexOfCurrent = _compositeSamples.IndexOf(currentSample);
            if (indexOfCurrent <= 0)
                return null;

            return _compositeSamples.ElementAtOrDefault(indexOfCurrent - 1);
        }

        public void ComputeTestAdjustedBackgroundCounts()
        {
            var adjustedCounts = 0;
            if (Mode == TestMode.Manual)
                adjustedCounts = BackgroundCount;
            else
            {
                if (BackgroundTimeInSeconds > 0)
                {
                    var cps = BackgroundCount / BackgroundTimeInSeconds;
                    adjustedCounts = (int)Math.Round(SampleDurationInSeconds * cps, 0);
                }
            }

            DurationAdjustedBackgroundCounts = adjustedCounts;
        }
        public void ClearBarcodes()
        {
            InjectateLot = String.Empty;
            InjectateKey = String.Empty;
            StandardAKey = String.Empty;
            StandardBKey = String.Empty;
        }

        #endregion

        #region Override

        protected override void FirePropertyChanged(string propertyName)
        {
            base.FirePropertyChanged(propertyName);

            //Find if background counts or background acquisition time has changed.
            if (propertyName == "BackgroundCount" || propertyName == "BackgroundTimeInSeconds" || propertyName == "SampleDurationInSeconds")
                ComputeTestAdjustedBackgroundCounts();
            else if (propertyName == "TestStatus" && Status != TestStatus.Pending)
                DisableOrEnableUserRequiredRules(true);
        }
        protected override void OnDispose()
        {
            base.OnDispose();

            if (Patient != null)
                Patient.Dispose();
        }

        public override string ToString()
        {
            // This method was added to aid in debugging a specific issue -- it obviously does not output all
            // of the object state. [Geoff :: 5/30/2012]

            var sb = new StringBuilder();
            sb.Append("-- BVATest --\n");
            sb.Append("Internal ID: " + InternalId + "\n");
            sb.Append("Accession: " + TestID2 + "\n");
            sb.Append("Status: " + Status + "\n");
            sb.Append("Hashcode: " + String.Format("{0:x}", GetHashCode()) + "\n");
            sb.Append("Samples:\n");
            CompositeSamples.ToList().ForEach(s => sb.Append(s.ToString()));
            sb.Append("-------------\n");

            return sb.ToString();
        }
        #endregion
    }
}
