﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Daxor.Lab.Domain.Evaluation;
using Daxor.Lab.Domain.Interfaces;

namespace Daxor.Lab.BVA.Core
{
    /// <summary>
    /// A blood volume sample that is composed of multiple (two) samples. For example,
    /// a Standard composite sample represents Standard A and Standard B; a Sample 1 
    /// composite sample represents Sample 1A and Sample 1B.
    /// </summary>
    public sealed class BVACompositeSample : EvaluatableObject
    {
        #region Private fields

        private const int ValueDoesNotExist = 0;

        private readonly BVATest _test;
        private BVASample _sampleA;
        private BVASample _sampleB;

        private double _unadjustedBloodVolume = ValueDoesNotExist;
        // ReSharper disable InconsistentNaming (already have an uppercase public property with this name)
        private static readonly List<string> _propertiesThatInfluenceUnadjustedBloodVolume = new List<string> { "AverageHematocrit", "PostInjectionTimeInSeconds", "IsWholeSampleExcluded", "AverageCounts" };
        // ReSharper restore InconsistentNaming

        #endregion

        #region Ctor

        public BVACompositeSample(BVATest test, BVASample sampleA, BVASample sampleB, IRuleEngine ruleEngine) : this(test, new[] { sampleA, sampleB }, ruleEngine) { }
        public BVACompositeSample(BVATest test, IEnumerable<BVASample> samples, IRuleEngine ruleEngine) : base(ruleEngine)
        {
            if (samples == null)
                throw new ArgumentNullException("samples");
            
            // ReSharper disable PossibleMultipleEnumeration
            if (!samples.Any())
                throw new ArgumentOutOfRangeException("samples", "Samples collection cannot be empty");
            
            _test = test;
            var sampleA = (from s in samples where s.Range == SampleRange.A select s).FirstOrDefault();
            var sampleB = (from s in samples where s.Range == SampleRange.B select s).FirstOrDefault();
            // ReSharper restore PossibleMultipleEnumeration

            ValidateSampleTypesAndRanges(sampleA, sampleB);

            SampleA = sampleA;
            SampleB = sampleB;
        }

        #endregion

        #region Properties
        
        public BVATest Test
        {
            get { return _test; }
        }

        public BVASample SampleA
        {
            get { return _sampleA; }
            set
            {
                if (_sampleA == value)
                    return;

                ValidateSampleTypeAndRange(value, true);
                
                // Happens the first time the composite sample is constructed
                if (_sampleA != null) 
                    _sampleA.PropertyChanged -= Sample_PropertyChanged;

                _sampleA = value;
                _sampleA.PropertyChanged += Sample_PropertyChanged;

                FirePropertyChanged("SampleA");
            }
        }

        public BVASample SampleB
        {
            get { return _sampleB; }
            set
            {
                if (_sampleB == value)
                    return;

                ValidateSampleTypeAndRange(value, false);

                // Happens the first time the composite sample is constructed
                if (_sampleB != null)
                    _sampleB.PropertyChanged -= Sample_PropertyChanged;

                _sampleB = value;
                _sampleB.PropertyChanged += Sample_PropertyChanged;
                
                FirePropertyChanged("SampleB");
            }
        }

        public double AverageHematocrit
        {
            get
            {
                var goodSamples = 0;
                double totalHematocrit = 0;

                if (!SampleA.IsHematocritExcluded && SampleA.Hematocrit > 0)
                {
                    goodSamples++;
                    totalHematocrit += SampleA.Hematocrit;
                }

                if (!SampleB.IsHematocritExcluded && SampleB.Hematocrit > 0)
                {
                    goodSamples++;
                    totalHematocrit += SampleB.Hematocrit;
                }

                if (goodSamples == 0)
                    return -1;

                // Do not round. Rounding causes the results for unadjusted blood volumes to be different from WinBVA 5.4
                return totalHematocrit / goodSamples;  
            }
        }

        public int PostInjectionTimeInSeconds
        {
            get
            {
                return SampleA.PostInjectionTimeInSeconds;
            }
        }

        public double HematocritA
        {
            get { return SampleA.Hematocrit; }
        }

        public double HematocritB
        {
            get { return SampleB.Hematocrit; }
        }

        public int CountsA
        {
            get { return SampleA.Counts; }
        }

        public int CountsB
        {
            get { return SampleB.Counts; }
        }

        public double UnadjustedBloodVolume
        {
            get { return _unadjustedBloodVolume; }
            set
            {
                if (value > 0)
                    SetValue(ref _unadjustedBloodVolume, "UnadjustedBloodVolume", value);
                else
                    SetValue(ref _unadjustedBloodVolume, "UnadjustedBloodVolume", ValueDoesNotExist);
            }
        }
        public bool IsWholeSampleExcluded
        {
            get
            {
                return SampleA.IsExcluded && SampleB.IsExcluded;
            }
            set
            {
                // Exclude both points
                SampleA.IsExcluded = value;
                SampleB.IsExcluded = value;
            }
        }
        public bool AreBothCountsExcluded
        {
            get
            {
                return SampleA.IsCountExcluded && SampleB.IsCountExcluded;
            }
        }

        public bool AreBothHematocritsExcluded
        {
            get
            {
                return SampleA.IsHematocritExcluded && SampleB.IsHematocritExcluded;
            }
        }

        public double AverageCounts
        {
            get
            {
                var goodSamples = 0;
                double totalCounts = 0;

                if (!SampleA.IsCountExcluded && !SampleA.IsCounting && SampleA.Counts > 0)
                {
                    goodSamples++;
                    totalCounts += SampleA.Counts;
                }

                if (!SampleB.IsCountExcluded && !SampleB.IsCounting && SampleB.Counts > 0)
                {
                    goodSamples++;
                    totalCounts += SampleB.Counts;
                }

                if (goodSamples == 0)
                    return 0;

                // Not rounding here .5 stays. AverageCounts used in calculations without .5 we are off from 5.4 ASK. Don't change.
                return totalCounts / goodSamples;
            }

        }

        /// <summary>
        /// Returns the type of composite sample.
        /// </summary>
        /// <remarks>
        /// By convention, the constructor, SampleA, and SampleB properties verify that the A and B 
        /// samples must be of the same type, and both must be defined (i.e., non-null).
        /// </remarks>
        public SampleType SampleType
        {
            get
            {
                return SampleA.Type;
            }
        }

        #endregion

        #region ToString()

        public override string ToString()
        {
            // This is called by BVATest.ToString(), so the output is tailored to that effect.
            // (Obviously not all of the state for the composite sample is not shown here.)
            // [Geoff :: 5/30/2012]
            return "  Sample A: " + SampleA + "\n  Sample B: " + SampleB + "\n";
        }

        #endregion

        #region Static

        private static void ValidateSampleTypesAndRanges(BVASample sampleA, BVASample sampleB)
        {
            if (sampleA == null)
                throw new NotSupportedException("No A-range sample was found");
            if (sampleB == null)
                throw new NotSupportedException("No B-range sample was found");
            if (sampleA.Type == SampleType.Undefined)
                throw new NotSupportedException("Sample A's type is not supported: " + sampleA.Type);
            if (sampleB.Type == SampleType.Undefined)
                throw new NotSupportedException("Sample B's type is not supported: " + sampleB.Type);
        }

        private static void ValidateSampleTypeAndRange(BVASample sample, bool validatingASample)
        {
            var sampleName = validatingASample ? "A" : "B";
            if (sample == null)
                throw new NotSupportedException("Sample " + sampleName + " cannot be null");
            if (sample.Type == SampleType.Undefined)
                throw new NotSupportedException("Sample " + sampleName + "'s type is not supported: " + sample.Type);
            if (validatingASample && sample.Range != SampleRange.A)
                throw new NotSupportedException("Sample A's range must be A");
            if (!validatingASample && sample.Range != SampleRange.B)
                throw new NotSupportedException("Sample B's range must be B");
        }

        public static List<String> PropertiesThatInfluenceUnadjustedBloodVolume
        {
            get { return _propertiesThatInfluenceUnadjustedBloodVolume; }
        }

        #endregion

        #region Helpers

        private void Sample_PropertyChanged(object sender, PropertyChangedEventArgs args)
        {
            if (args.PropertyName == "Counts")
            {
                FirePropertyChanged("AverageCounts");

                FirePropertyChanged(sender == SampleA ? "CountsA" : "CountsB");
            }
            else if (args.PropertyName == "IsCountExcluded")
            {
                FirePropertyChanged("AverageCounts");
                FirePropertyChanged("AreBothCountsExcluded");
            }
            else if (args.PropertyName == "Hematocrit")
            {
                FirePropertyChanged("AverageHematocrit");

                FirePropertyChanged(sender == SampleA ? "HematocritA" : "HematocritB");
            }
            else if (args.PropertyName == "IsHematocritExcluded")
                FirePropertyChanged("AreBothHematocritsExcluded");
            else if (args.PropertyName == "IsExcluded")
                FirePropertyChanged("IsWholeSampleExcluded");
            else if (args.PropertyName == "PostInjectionTimeInSeconds")
                FirePropertyChanged(args.PropertyName);
        }

        #endregion
    }
}