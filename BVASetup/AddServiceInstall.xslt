<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
 xmlns:wix="http://schemas.microsoft.com/wix/2006/wi"
>
	<!-- Copy all attributes and elements to the output. -->
	<xsl:template match="@*|*">
		<xsl:copy>
			<xsl:apply-templates select="@*" />
			<xsl:apply-templates select="*" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="wix:Component[wix:File[@Source='$(var.MCAServicesDir)\dmca.exe']]">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" />
			<wix:ServiceInstall Id="MCAService" DisplayName="Daxor Multichannel Analyzer Service" Description="WCF service host for the multichannel analyzer service." Name="McaServiceHost" ErrorControl="normal" Start="auto" Vital="yes" Account="LocalSystem" Type="ownProcess" />
			<wix:ServiceControl Id="MCAService" Name="MCAService" Wait="no"  Stop="both" Remove="both" />
		</xsl:copy>
	</xsl:template>
	<xsl:template match="wix:Component[wix:File[@Source='$(var.SCServicesDir)\dsmc.exe']]">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" />
			<wix:ServiceInstall Id="SampleChangerService" DisplayName="Daxor Sample Changer Service" Description="WCF service host for sample changer service." Name="SampleChangerService" ErrorControl="normal" Start="auto" Vital="yes" Account="LocalSystem" Type="ownProcess"/>
			<wix:ServiceControl Id="SampleChangerService" Name="SampleChangerService" Wait="no"  Stop="both" Remove="both" />
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>