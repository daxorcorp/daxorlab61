using System;
using Daxor.Lab.Infrastructure.Alerts;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Daxor.Lab.Footer.IntegrationTests.ViewModel_Tests.AlertsViewModel_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_processing_alert_violation_changed_payload
    {
        private ILoggerFacade _stubLogger;
        private ISettingsManager _stubSettingsManager;
        private IEventAggregator _eventAggregator;
        private StubAlertsViewModelThatDoesNotInitializeSettingObserverOrAlerts _stubAlertsViewModel;


        // Code that should be executed before running each test in this class.
        [TestInitialize]
        public void TestInitialize()
        {
            _stubLogger = MockRepository.GenerateStub<ILoggerFacade>();
            _stubSettingsManager = MockRepository.GenerateStub<ISettingsManager>();
            _eventAggregator = new EventAggregator();

            _stubAlertsViewModel =
                new StubAlertsViewModelThatDoesNotInitializeSettingObserverOrAlerts(_eventAggregator, _stubLogger,
                    _stubSettingsManager)
                {
                    HasBeenAlertedAboutAlertsOnStartupPassthrough = true,
                    HasBeenAlertedAboutFullQCAlertsPassthrough = false,
                    HasBeenAlertedAboutContaminationAlertsPassthrough = false,
                    HasBeenAlertedAboutStandardsAlertsPassthrough = false,
                    HasBeenAlertedAboutLinearityAlertsPassthrough = false,
                };
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_full_QC_test_is_due_then_the_alert_is_expanded()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.FullQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsTrue(_stubAlertsViewModel.IsAlertCollectionExpanded);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_full_QC_test_alert_is_removed_then_the_alert_is_removed()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.FullQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            alertViolationPayload.RemoveAlertFromList = true;

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsTrue(_stubAlertsViewModel.Alerts.Count == 0);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_full_QC_test_is_due_then_the_user_has_been_notified_by_the_alerts_view()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.FullQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsTrue(_stubAlertsViewModel.HasBeenAlertedAboutFullQCAlertsPassthrough);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_contamination_QC_test_is_due_then_the_alert_is_expanded()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.ContaminationQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsTrue(_stubAlertsViewModel.IsAlertCollectionExpanded);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_contamination_QC_test_alert_is_removed_then_the_alert_is_removed()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.ContaminationQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            alertViolationPayload.RemoveAlertFromList = true;

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsTrue(_stubAlertsViewModel.Alerts.Count == 0);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_contamination_QC_test_is_due_then_the_user_has_been_notified_by_the_alerts_view()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.ContaminationQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsTrue(_stubAlertsViewModel.HasBeenAlertedAboutContaminationAlertsPassthrough);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_Standards_QC_test_is_due_then_the_alert_is_expanded()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.StandardsQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsTrue(_stubAlertsViewModel.IsAlertCollectionExpanded);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_standards_QC_test_alert_is_removed_then_the_alert_is_removed()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.StandardsQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            alertViolationPayload.RemoveAlertFromList = true;

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsTrue(_stubAlertsViewModel.Alerts.Count == 0);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_standards_QC_test_is_due_then_the_user_has_been_notified_by_the_alerts_view()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.StandardsQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsTrue(_stubAlertsViewModel.HasBeenAlertedAboutStandardsAlertsPassthrough);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_linearity_QC_test_is_due_then_the_alert_is_expanded()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.LinearityQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsTrue(_stubAlertsViewModel.IsAlertCollectionExpanded);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_linearity_QC_test_alert_is_removed_then_the_alert_is_removed()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.LinearityQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            alertViolationPayload.RemoveAlertFromList = true;

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsTrue(_stubAlertsViewModel.Alerts.Count == 0);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_linearity_QC_test_is_due_then_the_user_has_been_notified_by_the_alerts_view()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.LinearityQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsTrue(_stubAlertsViewModel.HasBeenAlertedAboutLinearityAlertsPassthrough);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_an_alert_payload_is_published_then_the_user_has_been_notified_by_the_alerts_view()
        {
            _stubAlertsViewModel.HasBeenAlertedAboutAlertsOnStartupPassthrough = false;
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.LinearityQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsTrue(_stubAlertsViewModel.HasBeenAlertedAboutAlertsOnStartupPassthrough);
            Assert.IsTrue(_stubAlertsViewModel.HasBeenAlertedAboutLinearityAlertsPassthrough);
            Assert.IsTrue(_stubAlertsViewModel.HasBeenAlertedAboutFullQCAlertsPassthrough);
            Assert.IsTrue(_stubAlertsViewModel.IsAlertCollectionExpanded);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_an_alert_payload_is_published_then_the_alert_is_added_to_the_list()
        {
            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(true);

            var alertId = Guid.NewGuid();
            var alertViolationPayload = new AlertViolationPayload(alertId)
            {
                Resolution = AlertResolutionPayload.LinearityQCAlert
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);
            
            Assert.AreEqual(alertId, _stubAlertsViewModel.Alerts[0].AlertId);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_user_has_been_notified_by_the_alerts_view_and_the_due_notice_is_removed_then_the_alerts_flags_are_reset()
        {
            _stubAlertsViewModel.HasBeenAlertedAboutContaminationAlertsPassthrough = true;
            _stubAlertsViewModel.HasBeenAlertedAboutFullQCAlertsPassthrough = true;
            _stubAlertsViewModel.HasBeenAlertedAboutLinearityAlertsPassthrough = true;
            _stubAlertsViewModel.HasBeenAlertedAboutStandardsAlertsPassthrough = true;

            _stubSettingsManager.Expect(x => x.GetSetting<bool>(SettingKeys.QcTestDue)).Return(false);

            var alertViolationPayload = new AlertViolationPayload(Guid.NewGuid())
            {
                Resolution = AlertResolutionPayload.LinearityQCAlert,
                RemoveAlertFromList = true,
            };

            _eventAggregator.GetEvent<AlertViolationChanged>().Publish(alertViolationPayload);

            Assert.IsFalse(_stubAlertsViewModel.HasBeenAlertedAboutStandardsAlertsPassthrough);
            Assert.IsFalse(_stubAlertsViewModel.HasBeenAlertedAboutLinearityAlertsPassthrough);
            Assert.IsFalse(_stubAlertsViewModel.HasBeenAlertedAboutFullQCAlertsPassthrough);
            Assert.IsFalse(_stubAlertsViewModel.HasBeenAlertedAboutContaminationAlertsPassthrough);
        }
    }
    // ReSharper restore InconsistentNaming
}
