﻿using Daxor.Lab.Footer.ViewModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.NavigationFramework;
using Daxor.Lab.Infrastructure.SystemNotifications;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Rhino.Mocks;

namespace Daxor.Lab.Footer.IntegrationTests.ViewModel_Tests.AlertsViewModel_Tests
{
    class StubAlertsViewModelThatDoesNotInitializeSettingObserverOrAlerts : AlertsViewModel
    {
        public StubAlertsViewModelThatDoesNotInitializeSettingObserverOrAlerts(IEventAggregator eventAggregator, 
                                                        ILoggerFacade logger, 
                                                        ISettingsManager settingsManager) 
            : base(eventAggregator, MockRepository.GenerateStub<INavigationCoordinator>(), logger, settingsManager, 
                   MockRepository.GenerateStub<IMessageBoxDispatcher>(), MockRepository.GenerateStub<ISystemNotificationWatcher>())
        {
        }

        protected override void DealWithExistingAlerts()
        {
        }

        protected override void InitializeSettingObserver()
        {
        }

        public bool HasBeenAlertedAboutAlertsOnStartupPassthrough
        {
            get { return HasBeenAlertedAboutAlertsOnStartup; }
            set { HasBeenAlertedAboutAlertsOnStartup = value; }
        }

        public bool HasBeenAlertedAboutFullQCAlertsPassthrough
        {
            get { return HasBeenAlertedAboutFullQCAlerts; }
            set { HasBeenAlertedAboutFullQCAlerts = value; }
        }

        public bool HasBeenAlertedAboutContaminationAlertsPassthrough
        {
            get { return HasBeenAlertedAboutContaminationAlerts; }
            set { HasBeenAlertedAboutContaminationAlerts = value; }
        }

        public bool HasBeenAlertedAboutStandardsAlertsPassthrough
        {
            get { return HasBeenAlertedAboutStandardsAlerts; }
            set { HasBeenAlertedAboutStandardsAlerts = value; }
        }

        public bool HasBeenAlertedAboutLinearityAlertsPassthrough
        {
            get { return HasBeenAlertedAboutLinearityAlerts; }
            set { HasBeenAlertedAboutLinearityAlerts = value; }
        }
    }
}
