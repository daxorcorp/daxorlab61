﻿using Daxor.Lab.QC.Reports.ReportModels;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;

namespace Daxor.Lab.QC.Reports.ReportControllers
{
    public class ContaminationSubReportController
    {
        #region Fields
        private readonly ContaminationSubReportModel _reportModel;
        #endregion

        #region Ctor

        public ContaminationSubReportController(ContaminationSubReportModel reportModel)
        {
            _reportModel = reportModel;
        }

        #endregion

        #region Public Api

        /// <summary>
        /// Creates and populates the Contamination sub-report based on the QC test associated with the report controller.
        /// </summary>
        public SubReport LoadContaminationData(ref Unit unitX, ref Unit unitY)
        {
            var rpt = new SubReport_Contamination
            {
                textBoxCountTime = {Value = _reportModel.GenerateContaminationCountTime()},
                textBoxContaminationAcceptanceCriteria = {Value = _reportModel.GenerateContaminationAcceptanceCriteria()},
                tableFirstHalf = {DataSource = _reportModel.FirstHalfOfContaminationSamples},
                tableSecondHalf = {DataSource = _reportModel.SecondHalfOfContaminationSamples},
                textBoxContaminationPassFail = {Value = _reportModel.GenerateContaminationPassFailLabel()}
            };

            var subReport = new SubReport { Location = new PointU(unitX, unitY), Size = new SizeU(Unit.Inch(8), rpt.detail.Height) };
            unitY = unitY.Add(rpt.detail.Height);
            subReport.ReportSource = new InstanceReportSource {ReportDocument = rpt};
            return subReport;
        }

        #endregion
    }
}
