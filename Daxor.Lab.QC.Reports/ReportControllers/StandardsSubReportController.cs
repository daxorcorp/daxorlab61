﻿using Daxor.Lab.QC.Reports.ReportModels;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;

namespace Daxor.Lab.QC.Reports.ReportControllers
{
    public class StandardsSubReportController
    {
        #region Fields
        private readonly StandardsSubReportModel _reportModel;
        #endregion

        #region Ctor

        public StandardsSubReportController(StandardsSubReportModel reportModel)
        {
            _reportModel = reportModel;
        }
        #endregion

        #region Public Api

        /// <summary>
        /// Creates and populates the Standards sub-report based on the QC test associated with the report controller.
        /// </summary>
        public SubReport LoadStandardsData(ref Unit unitX, ref Unit unitY)
        {
            var rpt = new SubReport_Standards
            {
                textBoxCountTime = {Value = _reportModel.GenerateStandardsCountTimeLabel()},
                textBoxStandardsAcceptanceCriteria1 = {Value = _reportModel.GenerateStandardsAcceptanceCriteriaLabel1()},
                textBoxStandardsAcceptanceCriteria2 = {Value = _reportModel.GenerateStandardsAcceptanceCriteria2()},
                textBoxStandardsPassFail = {Value = _reportModel.GenerateStandardsPassFailLabel()},
                tableStandardsSamples = {DataSource = _reportModel.StandardSamples},
                textBoxInjectateLot = {Value = _reportModel.InjectateLot}
            };

            var details = _reportModel.GenerateStandardsFailureDetails();
            if (details.Count != 0)
            {
                rpt.panelFailureDetails.Visible = true;
                rpt.tableFailureDetails.DataSource = details;
            }

            var subReport = new SubReport { Location = new PointU(unitX, unitY), Size = new SizeU(Unit.Inch(8), rpt.detail.Height) };
            unitY = unitY.Add(rpt.detail.Height);
            subReport.ReportSource = new InstanceReportSource {ReportDocument = rpt};
            return subReport;
        }

        #endregion
    }
}
