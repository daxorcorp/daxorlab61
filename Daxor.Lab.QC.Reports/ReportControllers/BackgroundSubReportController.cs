﻿using System.Globalization;
using Daxor.Lab.QC.Reports.ReportModels;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;

namespace Daxor.Lab.QC.Reports.ReportControllers
{
    public class BackgroundSubReportController
    {
        #region Fields
        private readonly BackgroundSubReportModel _reportModel;
        #endregion

        #region ctor
        public BackgroundSubReportController(BackgroundSubReportModel reportModel)
        {
            _reportModel = reportModel;
        }
        #endregion

        #region Public Api

        /// <summary>
        /// Creates and populates the Background sub-report based on the QC test associated with the report controller.
        /// </summary>
        public SubReport LoadBackgroundData(ref Unit unitX, ref Unit unitY)
        {
            var rpt = new SubReport_Background
            {
                textBoxBackgroundAcceptanceCriteria = {Value = _reportModel.GenerateBackgroundAcceptanceCriteria()},
                textBoxBackgroundPass =
                {
                    Value =char.ToUpper(_reportModel.GenerateBackgroundState()[0]) +
                        _reportModel.GenerateBackgroundState().Substring(1).ToLower()
                },
                textBoxBackgroundPassFail = {Value = _reportModel.GenerateBackgroundState()},
                textBoxBackgroundRate =
                {
                    Value =_reportModel.GenerateRoundedBackgroundCountsPerMinute().ToString(CultureInfo.InvariantCulture)
                },
                textBoxCountTime = {Value = _reportModel.GenerateBackgroundCountTime()}
            };

            var subReport = new SubReport { Location = new PointU(unitX, unitY), Size = new SizeU(Unit.Inch(8), rpt.detail.Height) };
            unitY = unitY.Add(rpt.detail.Height);
            subReport.ReportSource = new InstanceReportSource {ReportDocument = rpt};
            return subReport;
        }

        #endregion
    }
}
