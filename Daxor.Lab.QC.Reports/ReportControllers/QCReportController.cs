﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Events;
using Daxor.Lab.Infrastructure.Events.Payloads;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.RocketDivisionKey;
using Daxor.Lab.Infrastructure.Threading;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.MessageManager;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.MessageManager.Interfaces;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Reports.Common;
using Daxor.Lab.QC.Reports.ReportModels;
using Daxor.Lab.ReportViewer;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Microsoft.Practices.Composite.Events;
using Microsoft.Practices.Composite.Logging;
using Microsoft.Practices.Composite.Regions;
using RocketDivision.StarBurnX;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;
using Telerik.Reporting.Processing;
using SubReport = Telerik.Reporting.SubReport;

namespace Daxor.Lab.QC.Reports.ReportControllers
{
	public class QcReportController : ReportControllerBase
	{
		#region Fields

		private readonly IMessageBoxDispatcher _messageBoxDispatcher;

		Unit _unitX = Unit.Inch(0);
		Unit _unitY = Unit.Inch(0);

		private QCReport _qcReport;
		private LinearitySubReportController _linearitySubReportController;
		private BackgroundSubReportController _backgroundSubReportController;
		private ResolutionSubReportController _resolutionSubReportController;
		private ContaminationSubReportController _contaminationSubReportController;
		private StandardsSubReportController _standardsSubReportController;
		private readonly QcReportModel _qcTestReportModel;
		private readonly IFolderBrowser _folderBrowser;
		private TaskScheduler _taskScheduler;
		private bool _useIdealDispatcher = true;
		private readonly IMessageManager _messageManager;
		private readonly IStarBurnWrapper _opticalDriveWrapper;
		private readonly QcTest _qcTest;
		public const string ExportMessageBoxCaption = "Export Quality Control Report";


		#endregion

		#region ctor

		public QcReportController(ILoggerFacade customLoggerFacade,
			IMessageBoxDispatcher messageBoxDispatcher,
			IRegionManager regionManager, 
			QcTest test, 
			IEventAggregator eventAggregator, 
			IStarBurnWrapper driveWrapper,
			IFolderBrowser folderBrowser,
			IMessageManager messageManager,
			ISettingsManager settingsManager)
		{
			Logger = customLoggerFacade;
			_messageBoxDispatcher = messageBoxDispatcher;
			RegionManager = regionManager;
			EventAggregator = eventAggregator;
			_opticalDriveWrapper = driveWrapper;
			_folderBrowser = folderBrowser;
			_messageManager = messageManager;
			SettingsManager = settingsManager;

			_qcTestReportModel = new QcReportModel(test, settingsManager);
			_qcTest = test;
		}

		#endregion

		/// <summary>
		/// Gets or sets the scheduler for running asynchronous tasks. By default, this is the regular thread pool,
		/// but is overridable such that other schedulers can be used (i.e., for unit testing).
		/// </summary>
		/// <remarks>
		/// When using a custom scheduler, the IdealDispatcher (for running code on the UI thread) will not be
		/// used, so that the executing code will run under the custom scheduler.
		/// </remarks>
		public TaskScheduler TaskScheduler
		{
			get
			{
				return _taskScheduler ?? (_taskScheduler = TaskScheduler.Default);
			}
			set
			{
				_taskScheduler = value;
				_useIdealDispatcher = false;
			}
		}

		#region IReportController Members

		public override IReportDocument ReportToView
		{
			get { return _qcReport; }
		}

		/// <summary>
		/// Loads a QC report based on the QC test associated with the report contoller and provides the ReportProcessor with
		/// the report that was generated.
		/// </summary>
		public override void PrintReport()
		{
			if (_qcReport == null)
				LoadReportData();

			var standardPrintController = new StandardPrintController();
			var settings = new PrinterSettings();

			var proc = new ReportProcessor {PrintController = standardPrintController};

			proc.PrintReport(new InstanceReportSource {ReportDocument = _qcReport}, settings);                       
		}

		/// <summary>
		/// Loads a QC report based on the QC test associated with the report controller, in both PDF and xml format, and exports 
		/// the reports to the specified location in a zipped, password protected file.
		/// </summary>
		public override void ExportReport()
		{
			var reportProcessor = new ReportProcessor();
			var choices = new[] {"Export", "Cancel"};
			var msgBoxResult = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Undefined, _folderBrowser,
				"Export QC Report", choices);
			
			if (choices[msgBoxResult] != "Export")
				return;

			if (!_folderBrowser.IsPathValid())
			{
				var errorMessage = _messageManager.GetMessage(MessageKeys.QcExportPathInvalid).FormattedMessage;
				_messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error, errorMessage, "Exporting QC Tests",
					MessageBoxChoiceSet.Close);
				Logger.Log(errorMessage, Category.Info, Priority.Low);
				return;
			}

			LoadReportData();

			var exportPath = SettingsManager.GetSetting<string>(SettingKeys.SystemTempFilePath);
			if (!_folderBrowser.IsOpticalDrive)
			{
				var selectedPath = _folderBrowser.CurrentPath;
				if (!selectedPath.EndsWith("\\"))
					selectedPath += "\\";
				exportPath = selectedPath;
			}

			var tokenSource = new CancellationTokenSource();
			var token = tokenSource.Token;
			var exportTask = new Task(() => ExportTask(reportProcessor, exportPath, tokenSource), token);
			exportTask.ContinueWith(result => OnExportTaskFaulted(result.Exception), token,
				TaskContinuationOptions.OnlyOnFaulted, TaskScheduler);
			exportTask.ContinueWith(result => OnExportTaskCompleted(), token,
				TaskContinuationOptions.OnlyOnRanToCompletion, TaskScheduler);
			exportTask.Start(TaskScheduler);
		}

		protected virtual void OnExportTaskFaulted(Exception faultingException)
		{
			var errorMessage = _messageManager.GetMessage(MessageKeys.BvaExportUnsuccessful);

			DisableBusyIndicator();

			if (_useIdealDispatcher)
				IdealDispatcher.BeginInvoke(() => DisplayExportErrorMessage(errorMessage));
			else
				DisplayExportErrorMessage(errorMessage);

			var exceptionToLog = faultingException.InnerException ?? faultingException;
			var logMessage = String.Format("{0}; inner exception: {1}", errorMessage.FormattedMessage, exceptionToLog.Message);
			Logger.Log(logMessage, Category.Exception, Priority.High);
		}


		protected virtual void OnExportTaskCompleted()
		{
			DisableBusyIndicator();

			IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Information,
				"The export has been completed", "Export Complete", MessageBoxChoiceSet.Close));
		}

		protected virtual void ExportTask(ReportProcessor reportProcessor, string tempPath, CancellationTokenSource tokenSource)
		{
			EventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true, "Exporting Report..."));

			var fileName = BuildFileName(_qcTestReportModel.GenerateUnitId());
			SavePdfRenderingToFile(reportProcessor, tempPath, fileName);

			if (!_folderBrowser.IsOpticalDrive)
				return;

			BurnToOpticalMedia(tempPath, fileName, tokenSource);
			CleanupAfterExport(tempPath, fileName);
		}

		protected virtual void CleanupAfterExport(string tempPath, string fileName)
		{
			DisableBusyIndicator();
			SafeFileOperations.Delete(tempPath + fileName + ".pdf");
		}

		protected virtual void SavePdfRenderingToFile(ReportProcessor reportProcessor, string tempPath, string fileName)
		{
			var result = reportProcessor.RenderReport("PDF", new InstanceReportSource {ReportDocument = _qcReport}, null);

			using (var fs = new FileStream(tempPath + fileName + ".pdf", FileMode.Create))
			{
				fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
				fs.Flush();
			}
		}

		private void BurnToOpticalMedia(string tempPath, string fileName, CancellationTokenSource tokenSource)
		{
			var dataBurner = _opticalDriveWrapper.CreateDataBurner();

			dataBurner.OnProgress += dataBurner_OnProgress;

			var dataFolder = dataBurner as IDataFolder;
			dataFolder.AddFile(tempPath + fileName + ".pdf");
			try
			{
				dataBurner.Burn(false, RocketDivisionKey.DEF_VOLUME_NAME, "StarBurnX", "StarBurnX Data Burner");
			}
			catch (Exception ex)
			{
				string message;
				Message errorAlert;
				Logger.Log("QCReportController: Exception in ExportReport: " + ex.Message, Category.Debug,
					Priority.Medium);
				switch (ex.Message)
				{
					case "Device is not ready:Device is not ready":
						errorAlert = _messageManager.GetMessage(MessageKeys.QcExportDeviceNotReady);
						message = errorAlert.FormattedMessage;
						break;
					case "Disk readonly: The media disk is not recordable":
						errorAlert = _messageManager.GetMessage(MessageKeys.QcExportDiscReadonly);
						message = errorAlert.FormattedMessage;
						break;
					case
						"Disc is finalized:The disc is finalized! Note that is not possible to write data on the finalized disc!"
						:
						errorAlert = _messageManager.GetMessage(MessageKeys.QcExportDiscFinalized);
						message = errorAlert.FormattedMessage;
						break;
					default:
						message = ex.Message;
						break;
				}
				if (_useIdealDispatcher)
					IdealDispatcher.BeginInvoke(() => _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error,
						message, "Export Exception", MessageBoxChoiceSet.Close));
				else
					_messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Error,
						message, "Export Exception", MessageBoxChoiceSet.Close);

				Logger.Log(message, Category.Exception, Priority.High);
				dataBurner.OnProgress -= dataBurner_OnProgress;
				tokenSource.Cancel();
			}
			dataBurner.OnProgress -= dataBurner_OnProgress;
			DisableBusyIndicator();
			dataBurner.Drive.Eject();
		}

		#endregion

		#region report loading utilities
		
		/// <summary>
		/// Loads the specified QC report based on the QC test associated with the report controller formatted
		/// according to the type of report requested.
		/// </summary>
		public override void LoadReportData()
		{
			_qcReport = new QCReport();
			LoadHeaderData();

			var subReports = new List<SubReport>();
			Task<List<SubReport>> buildReport;
			_backgroundSubReportController = new BackgroundSubReportController(new BackgroundSubReportModel(_qcTest, SettingsManager));


			// ReSharper disable AccessToModifiedClosure
			switch (_qcTestReportModel.QCType)
			{
				case TestType.Full:
					_qcReport.textBoxTestHeader.Value = QcReportConstants.FullQcTestHeader;
					_linearitySubReportController = new LinearitySubReportController(new LinearitySubReportModel(_qcTest));
					_resolutionSubReportController = new ResolutionSubReportController(new ResolutionSubReportModel(_qcTest, SettingsManager));
					_contaminationSubReportController = new ContaminationSubReportController(new ContaminationSubReportModel(_qcTest, SettingsManager));
					_standardsSubReportController = new StandardsSubReportController(new StandardsSubReportModel(_qcTest, SettingsManager));

					buildReport =
							Task.Factory.StartNew(() =>
								{
									subReports.Add(_backgroundSubReportController.LoadBackgroundData(ref _unitX, ref _unitY));
									return subReports;
								})
							.ContinueWith(h =>
								{
									subReports = h.Result;
									subReports.Add(_contaminationSubReportController.LoadContaminationData(ref _unitX, ref _unitY));
									return subReports;
								})
							.ContinueWith(i =>
								{
									subReports = i.Result;
									subReports.Add(_standardsSubReportController.LoadStandardsData(ref _unitX, ref _unitY));
									return subReports;
								})
							.ContinueWith(j =>
							   {
								   subReports = j.Result;
								   subReports.Add(_resolutionSubReportController.LoadResolutionData(ref _unitX, ref _unitY));
								   return subReports;
							   })
							.ContinueWith(k =>
							   {
								   subReports = k.Result;
								   subReports.Add(_linearitySubReportController.LoadLinearityData(ref _unitX,ref _unitY));
								   return subReports;
							   });

					subReports = buildReport.Result;
					break;

				case TestType.Contamination:
					_qcReport.textBoxTestHeader.Value = QcReportConstants.ContaminationQcTestHeader;
					_contaminationSubReportController = new ContaminationSubReportController(new ContaminationSubReportModel(_qcTest, SettingsManager));
				   
					buildReport =
							Task.Factory.StartNew(() =>
								{
									subReports.Add(_backgroundSubReportController.LoadBackgroundData(ref _unitX, ref _unitY));
									return subReports;
								})
							.ContinueWith(h =>
								{
									subReports = h.Result;
									subReports.Add(_contaminationSubReportController.LoadContaminationData(ref _unitX, ref _unitY));
									return subReports;
								});

					subReports = buildReport.Result;
					break;

				case TestType.Standards:
					_qcReport.textBoxTestHeader.Value = QcReportConstants.StandardsQcTestHeader;
					_standardsSubReportController = new StandardsSubReportController(new StandardsSubReportModel(_qcTest, SettingsManager));


					buildReport =
							Task.Factory.StartNew(() =>
							{
								subReports.Add(_backgroundSubReportController.LoadBackgroundData(ref _unitX, ref _unitY));
								return subReports;
							})
							.ContinueWith(h =>
							{
								subReports = h.Result;
								subReports.Add(_standardsSubReportController.LoadStandardsData(ref _unitX, ref _unitY));
								return subReports;
							});

					subReports = buildReport.Result;
					break;

				case TestType.Linearity:
					_qcReport.textBoxTestHeader.Value = QcReportConstants.LinearityQcTestHeader;
					_linearitySubReportController = new LinearitySubReportController(new LinearitySubReportModel(_qcTest));
					_resolutionSubReportController = new ResolutionSubReportController(new ResolutionSubReportModel(_qcTest, SettingsManager));

					buildReport =
							Task.Factory.StartNew(() =>
							{
								subReports.Add(_backgroundSubReportController.LoadBackgroundData(ref _unitX, ref _unitY));
								return subReports;
							})
							.ContinueWith(h =>
							{
								subReports = h.Result;
								subReports.Add(_resolutionSubReportController.LoadResolutionData(ref _unitX, ref _unitY));
								return subReports;
							})
							.ContinueWith(i =>
							{
								subReports = i.Result;
								subReports.Add(_linearitySubReportController.LoadLinearityData(ref _unitX, ref _unitY));
								return subReports;
							});
				   
						subReports = buildReport.Result;

					break;

				default:
					throw new NotSupportedException("Unsupported QC Test Type: " + _qcTestReportModel.QCType.ToString());
			}
			// ReSharper restore AccessToModifiedClosure

			var rpt = new SubReport_Signature();
			_unitY = _unitY.Add(rpt.detail.Height);
			var subReport = new SubReport
			{
				Location = new PointU(_unitX, _unitY),
				Size = new SizeU(Unit.Inch(8), rpt.detail.Height),
				ReportSource = new InstanceReportSource {ReportDocument = rpt}
			};

			subReports.Add(subReport);
			foreach(var report in subReports)
				_qcReport.detail.Items.Add(report);
		}

		/// <summary>
		/// Populates the header and footer of the report based on the QC test associated with
		/// the report controller.
		/// </summary>
		private void LoadHeaderData()
		{
			_qcReport.textBoxBVAVersion.Value = _qcTestReportModel.GenerateBvaVersion();
			_qcReport.textBoxUnitID.Value = _qcTestReportModel.GenerateUnitId();
			_qcReport.textBoxAnalyst.Value = _qcTestReportModel.Analyst;
			_qcReport.textBoxComment.Value = _qcTestReportModel.Comments;
			_qcReport.textBoxCumulativePassFail.Value = _qcTestReportModel.GenerateCumulativeTestState();
			_qcReport.textBoxDepartment.Value = _qcTestReportModel.Department;
			_qcReport.textBoxDepartmentAddress.Value = _qcTestReportModel.DepartmentAddress;
			_qcReport.textBoxDepartmentDirector.Value = _qcTestReportModel.DepartmentDirector;
			_qcReport.textBoxDepartmentPhoneNumber.Value = _qcTestReportModel.DepartmentPhoneNumber;
			_qcReport.textBoxEndFineGain.Value = _qcTestReportModel.GenerateEndFineGain();
			_qcReport.textBoxHospitalName.Value = _qcTestReportModel.HospitalName;
			_qcReport.textBoxStartFineGain.Value = _qcTestReportModel.GenerateStartFineGain();
			_qcReport.textBoxTestDate.Value = _qcTestReportModel.GenerateTestDate();
			_qcReport.textBoxVoltage.Value = _qcTestReportModel.Voltage;
			_qcReport.textBoxPrintedOn.Value = _qcTestReportModel.GeneratePrintedOnDate();
		}

		#endregion

		#region helper methods

		void dataBurner_OnProgress(int percent, int timeRemaining)
		{
			EventAggregator.GetEvent<BusyStateChanged>().Publish(new BusyPayload(true) { PercentageCompleted = percent, Message = "Burning CD" });
		}

		private void DisableBusyIndicator()
		{
			var busyStateChangedEvent = EventAggregator.GetEvent<BusyStateChanged>();
			if (busyStateChangedEvent != null) busyStateChangedEvent.Publish(new BusyPayload(false));
		}

		private void DisplayExportErrorMessage(Message errorMessage)
		{
			_messageBoxDispatcher.ShowMessageBox(
				MessageBoxCategory.Error,
				errorMessage.FormattedMessage,
				ExportMessageBoxCaption, MessageBoxChoiceSet.Close);
		}

		#endregion
	}
}
