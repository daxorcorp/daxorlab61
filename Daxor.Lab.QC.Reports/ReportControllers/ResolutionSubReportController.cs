﻿using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.QC.Reports.ReportModels;
using Daxor.Lab.QC.Reports.ReportUtilityClasses;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;

namespace Daxor.Lab.QC.Reports.ReportControllers
{
    public class ResolutionSubReportController
    {
        #region Fields

        private readonly ResolutionSubReportModel _reportModel;

        #endregion

        #region Ctor

        public ResolutionSubReportController(ResolutionSubReportModel reportModel)
        {
            _reportModel = reportModel;
        }

        #endregion

        /// <summary>
        /// Creates and populates the Resolution subreport based on the QC test associated with the report controller
        /// </summary>
        public SubReport LoadResolutionData(ref Unit unitX, ref Unit unitY)
        {
            var resolutionSubreport = new SubReport_Resolution
            {
                textBox_AcceptanceCriteriaCalibration = {Value = _reportModel.GenerateCalibrationAcceptanceCriteria()},
                textBoxResolutionAcceptanceCriteria = {Value = _reportModel.GenerateResolutionAcceptanceCriteria()},
                textBoxResolutionPassFail = {Value = _reportModel.GenerateResolutionPassFailLabel()},
                table_LinearityData = {DataSource = _reportModel.ResolutionSamples}
            };
            
            ShowResolutionFailureDetailsIfPresent(_reportModel.GenerateResolutionFailureDetails(), resolutionSubreport);

            var subReport = new SubReport { Location = new PointU(unitX, unitY), Size = new SizeU(Unit.Inch(8), resolutionSubreport.detail.Height) };
            unitY = unitY.Add(resolutionSubreport.detail.Height);
            subReport.ReportSource = new InstanceReportSource {ReportDocument = resolutionSubreport};
            return subReport;
        }

        private static void ShowResolutionFailureDetailsIfPresent(IEnumerable<FailureDetails> resolutionFailureDetails, SubReport_Resolution resolutionSubreport)
        {
            if (!resolutionFailureDetails.Any()) return;

            resolutionSubreport.panelFailureDetails.Visible = true;
            resolutionSubreport.tableFailureDetails.DataSource = resolutionFailureDetails;
        }
    }
}
