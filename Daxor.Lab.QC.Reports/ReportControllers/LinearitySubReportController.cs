﻿using Daxor.Lab.QC.Reports.ReportModels;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;

namespace Daxor.Lab.QC.Reports.ReportControllers
{
    public class LinearitySubReportController
    {
        private readonly LinearitySubReportModel _reportModel;

        #region ctor

        public LinearitySubReportController(LinearitySubReportModel reportModel)
        {
            _reportModel = reportModel;
        }

        #endregion

        #region Public Api

        /// <summary>
        /// Creates and populates the Linearity sub-report based on the QC test associated with the report controller.
        /// </summary>
        public SubReport LoadLinearityData(ref Unit unitX, ref Unit unitY)
        {
            var rpt = new SubReport_Linearity
            {
                tableLinearitySamples = {DataSource = _reportModel.LinearitySamples},
                textBoxLinearityTestPassFail = {Value = _reportModel.GenerateLinearityTestPassFailLabel()}
            };

            if (_reportModel.IsLinearityQcFailed())
            {
                rpt.tableFailureDetails.DataSource = _reportModel.GenerateLinearityFailureDetails();
                rpt.panelFailureDetails.Visible = true;
            }
            
            var subReport = new SubReport { Location = new PointU(unitX, unitY), Size = new SizeU(Unit.Inch(8), rpt.detail.Height) };
            unitY = unitY.Add(rpt.detail.Height);
            subReport.ReportSource = new InstanceReportSource {ReportDocument = rpt};
            return subReport;
        }

        #endregion
    }
}
