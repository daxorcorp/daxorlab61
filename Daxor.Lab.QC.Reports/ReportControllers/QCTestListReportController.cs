﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using Daxor.Lab.QC.Interfaces;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;
using Telerik.Reporting.Processing;
using SubReport = Telerik.Reporting.SubReport;

namespace Daxor.Lab.QC.Reports.ReportControllers
{
	// ReSharper disable once InconsistentNaming
    public class QCTestListReportController
    {
        #region Fields

        private QcTestListReport _qcTestListReport;
        private readonly ISettingsManager _settingsManager;

        #endregion

        #region Ctor

        public QCTestListReportController(ISettingsManager settingsManager)
        {
            _settingsManager = settingsManager;
        }

        #endregion

        #region Public Api

        public void PrintQcTestList(IEnumerable<QcTestItem> testItemsList)
        {
            _qcTestListReport = new QcTestListReport();
            LoadQCTestListHeaderData();

            var qcTestListItemsList = testItemsList.ToList();
            _qcTestListReport.table1.DataSource = qcTestListItemsList;

			var signatureSubReport = new SubReport_Signature();
			//var unitX = new Unit();
			//var unitY = new Unit();
			//unitY = unitY.Add(_qcTestListReport.detail.Height);
	        _qcTestListReport.subReport1.ReportSource = new InstanceReportSource {ReportDocument = signatureSubReport};


            var standardPrintController = new StandardPrintController();
            var settings = new PrinterSettings();

            var proc = new ReportProcessor { PrintController = standardPrintController };
            proc.PrintReport(new InstanceReportSource{ReportDocument = _qcTestListReport}, settings);
        }

        #endregion

        #region Private Methods

        private void LoadQCTestListHeaderData()
        {
            _qcTestListReport.textBoxBVAVersion.Value = _settingsManager.GetSetting<string>(SettingKeys.BvaModuleVersion);
            _qcTestListReport.textBoxUnitID.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemUniqueSystemId);
            _qcTestListReport.textBoxDepartment.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentName);
            _qcTestListReport.textBoxDepartmentAddress.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentAddress);
            _qcTestListReport.textBoxDepartmentDirector.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentDirector);
            _qcTestListReport.textBoxDepartmentPhoneNumber.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentPhoneNumber);
            _qcTestListReport.textBoxHospitalName.Value = _settingsManager.GetSetting<string>(SettingKeys.SystemHospitalName);
            _qcTestListReport.textBoxPrintedOn.Value = "Printed: " + DateTime.Now.ToString("g");
        }

        #endregion
    }
}
