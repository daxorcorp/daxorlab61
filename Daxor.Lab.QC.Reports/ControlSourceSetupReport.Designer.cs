namespace Daxor.Lab.QC.Reports
{
    partial class ControlSourceSetupReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            textBox2 = new Telerik.Reporting.TextBox();
            textBox12 = new Telerik.Reporting.TextBox();
            textBox10 = new Telerik.Reporting.TextBox();
            textBox8 = new Telerik.Reporting.TextBox();
            textBox3 = new Telerik.Reporting.TextBox();
            textBox4 = new Telerik.Reporting.TextBox();
            pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            textBoxDepartmentAddress = new Telerik.Reporting.TextBox();
            textBoxDepartmentDirector = new Telerik.Reporting.TextBox();
            textBoxDepartmentPhoneNumber = new Telerik.Reporting.TextBox();
            textBoxHospitalName = new Telerik.Reporting.TextBox();
            textBox1 = new Telerik.Reporting.TextBox();
            textBoxDepartment = new Telerik.Reporting.TextBox();
            detail = new Telerik.Reporting.DetailSection();
            shape1 = new Telerik.Reporting.Shape();
            tableQCSources = new Telerik.Reporting.Table();
            textBox5 = new Telerik.Reporting.TextBox();
            textBox6 = new Telerik.Reporting.TextBox();
            textBox7 = new Telerik.Reporting.TextBox();
            textBox9 = new Telerik.Reporting.TextBox();
            textBox11 = new Telerik.Reporting.TextBox();
            textBox13 = new Telerik.Reporting.TextBox();
            shape2 = new Telerik.Reporting.Shape();
            textBox14 = new Telerik.Reporting.TextBox();
            shape3 = new Telerik.Reporting.Shape();
            pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
            textBox21 = new Telerik.Reporting.TextBox();
            textBox17 = new Telerik.Reporting.TextBox();
            textBoxBVAVersion = new Telerik.Reporting.TextBox();
            textBox20 = new Telerik.Reporting.TextBox();
            textBoxUnitID = new Telerik.Reporting.TextBox();
            textBoxPrintedOn = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox2
            // 
            textBox2.Name = "textBox2";
            textBox2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.46664029359817505D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox2.Style.Font.Bold = true;
            textBox2.Style.Font.Name = "Times New Roman";
            textBox2.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(11D, Telerik.Reporting.Drawing.UnitType.Point);
            textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox2.Value = "Control Sample ID";
            // 
            // textBox12
            // 
            textBox12.Name = "textBox12";
            textBox12.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.46664029359817505D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox12.Style.Font.Bold = true;
            textBox12.Style.Font.Name = "Times New Roman";
            textBox12.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(11D, Telerik.Reporting.Drawing.UnitType.Point);
            textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox12.StyleName = "";
            textBox12.Value = "Source TOC";
            // 
            // textBox10
            // 
            textBox10.Name = "textBox10";
            textBox10.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.46664029359817505D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox10.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox10.Style.Font.Bold = true;
            textBox10.Style.Font.Name = "Times New Roman";
            textBox10.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(11D, Telerik.Reporting.Drawing.UnitType.Point);
            textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox10.StyleName = "";
            textBox10.Value = "Isotope";
            // 
            // textBox8
            // 
            textBox8.Name = "textBox8";
            textBox8.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.46664029359817505D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox8.Style.Font.Bold = true;
            textBox8.Style.Font.Name = "Times New Roman";
            textBox8.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(11D, Telerik.Reporting.Drawing.UnitType.Point);
            textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox8.StyleName = "";
            textBox8.Value = "Activity (nCi)";
            // 
            // textBox3
            // 
            textBox3.Name = "textBox3";
            textBox3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.46664029359817505D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox3.Style.Font.Bold = true;
            textBox3.Style.Font.Name = "Times New Roman";
            textBox3.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(11D, Telerik.Reporting.Drawing.UnitType.Point);
            textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox3.Value = "Sample Serial No.";
            // 
            // textBox4
            // 
            textBox4.Name = "textBox4";
            textBox4.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.46664029359817505D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            textBox4.Style.Font.Bold = true;
            textBox4.Style.Font.Name = "Times New Roman";
            textBox4.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(11D, Telerik.Reporting.Drawing.UnitType.Point);
            textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox4.Value = "Count Limit";
            // 
            // pageHeaderSection1
            // 
            pageHeaderSection1.Height = new Telerik.Reporting.Drawing.Unit(2D, Telerik.Reporting.Drawing.UnitType.Inch);
            pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBoxDepartmentAddress,
            textBoxDepartmentDirector,
            textBoxDepartmentPhoneNumber,
            textBoxHospitalName,
            textBox1,
            textBoxDepartment});
            pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // textBoxDepartmentAddress
            // 
            textBoxDepartmentAddress.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.1000789403915405D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxDepartmentAddress.Name = "textBoxDepartmentAddress";
            textBoxDepartmentAddress.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9999213218688965D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxDepartmentAddress.Style.Font.Bold = true;
            textBoxDepartmentAddress.Style.Font.Name = "Times New Roman";
            textBoxDepartmentAddress.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxDepartmentAddress.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxDepartmentAddress.Value = "DepartmentAddress";
            // 
            // textBoxDepartmentDirector
            // 
            textBoxDepartmentDirector.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.7003158330917358D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxDepartmentDirector.Name = "textBoxDepartmentDirector";
            textBoxDepartmentDirector.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9999213218688965D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxDepartmentDirector.Style.Font.Bold = true;
            textBoxDepartmentDirector.Style.Font.Name = "Times New Roman";
            textBoxDepartmentDirector.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxDepartmentDirector.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxDepartmentDirector.Value = "DepartmentDirector";
            // 
            // textBoxDepartmentPhoneNumber
            // 
            textBoxDepartmentPhoneNumber.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.300157904624939D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxDepartmentPhoneNumber.Name = "textBoxDepartmentPhoneNumber";
            textBoxDepartmentPhoneNumber.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9999213218688965D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxDepartmentPhoneNumber.Style.Font.Bold = true;
            textBoxDepartmentPhoneNumber.Style.Font.Name = "Times New Roman";
            textBoxDepartmentPhoneNumber.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxDepartmentPhoneNumber.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxDepartmentPhoneNumber.Value = "DepartmentPhone";
            // 
            // textBoxHospitalName
            // 
            textBoxHospitalName.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.90000009536743164D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxHospitalName.Name = "textBoxHospitalName";
            textBoxHospitalName.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9999213218688965D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxHospitalName.Style.Font.Bold = true;
            textBoxHospitalName.Style.Font.Name = "Times New Roman";
            textBoxHospitalName.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxHospitalName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxHospitalName.Value = "HospitalName";
            // 
            // textBox1
            // 
            textBox1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.5D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox1.Name = "textBox1";
            textBox1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9999213218688965D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.30000004172325134D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox1.Style.Font.Bold = true;
            textBox1.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(14D, Telerik.Reporting.Drawing.UnitType.Point);
            textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox1.Value = "Quality Control Calibration Sources Setup Report";
            // 
            // textBoxDepartment
            // 
            textBoxDepartment.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(1.5002368688583374D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxDepartment.Name = "textBoxDepartment";
            textBoxDepartment.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9999213218688965D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.20000004768371582D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxDepartment.Style.Font.Bold = true;
            textBoxDepartment.Style.Font.Name = "Times New Roman";
            textBoxDepartment.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(12D, Telerik.Reporting.Drawing.UnitType.Point);
            textBoxDepartment.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBoxDepartment.Value = "DepartmentName";
            // 
            // detail
            // 
            detail.Height = new Telerik.Reporting.Drawing.Unit(7D, Telerik.Reporting.Drawing.UnitType.Inch);
            detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            shape1,
            tableQCSources,
            shape2,
            textBox14,
            shape3});
            detail.Name = "detail";
            // 
            // shape1
            // 
            shape1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape1.Name = "shape1";
            shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            shape1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9999213218688965D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.1000000610947609D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // tableQCSources
            // 
            tableQCSources.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableQCSources.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableQCSources.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableQCSources.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableQCSources.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableQCSources.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableQCSources.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(new Telerik.Reporting.Drawing.Unit(0.22494754195213318D, Telerik.Reporting.Drawing.UnitType.Inch)));
            tableQCSources.Body.SetCellContent(0, 0, textBox5);
            tableQCSources.Body.SetCellContent(0, 4, textBox6);
            tableQCSources.Body.SetCellContent(0, 5, textBox7);
            tableQCSources.Body.SetCellContent(0, 3, textBox9);
            tableQCSources.Body.SetCellContent(0, 2, textBox11);
            tableQCSources.Body.SetCellContent(0, 1, textBox13);
            tableGroup15.ReportItem = textBox2;
            tableGroup16.Name = "Group3";
            tableGroup16.ReportItem = textBox12;
            tableGroup17.Name = "Group2";
            tableGroup17.ReportItem = textBox10;
            tableGroup18.Name = "Group1";
            tableGroup18.ReportItem = textBox8;
            tableGroup19.ReportItem = textBox3;
            tableGroup20.ReportItem = textBox4;
            tableQCSources.ColumnGroups.Add(tableGroup15);
            tableQCSources.ColumnGroups.Add(tableGroup16);
            tableQCSources.ColumnGroups.Add(tableGroup17);
            tableQCSources.ColumnGroups.Add(tableGroup18);
            tableQCSources.ColumnGroups.Add(tableGroup19);
            tableQCSources.ColumnGroups.Add(tableGroup20);
            tableQCSources.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBox5,
            textBox6,
            textBox7,
            textBox9,
            textBox11,
            textBox13,
            textBox2,
            textBox12,
            textBox10,
            textBox8,
            textBox3,
            textBox4});
            tableQCSources.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.099999986588954926D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.10007890313863754D, Telerik.Reporting.Drawing.UnitType.Inch));
            tableQCSources.Name = "tableQCSources";
            tableGroup21.Groupings.AddRange(new Telerik.Reporting.Grouping[] {
            new Telerik.Reporting.Grouping("")});
            tableGroup21.Name = "DetailGroup";
            tableQCSources.RowGroups.Add(tableGroup21);
            tableQCSources.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.899960994720459D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.69158786535263062D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox5
            // 
            textBox5.Name = "textBox5";
            textBox5.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.22494754195213318D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox5.Style.Font.Name = "Times New Roman";
            textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox5.Value = "=Fields.name";
            // 
            // textBox6
            // 
            textBox6.Name = "textBox6";
            textBox6.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.22494754195213318D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox6.Style.Font.Name = "Times New Roman";
            textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox6.Value = "=Fields.serialNo";
            // 
            // textBox7
            // 
            textBox7.Name = "textBox7";
            textBox7.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.22494754195213318D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox7.Style.Font.Name = "Times New Roman";
            textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox7.Value = "=Fields.countLimit";
            // 
            // textBox9
            // 
            textBox9.Name = "textBox9";
            textBox9.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.22494754195213318D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox9.Style.Font.Name = "Times New Roman";
            textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox9.StyleName = "";
            textBox9.Value = "=Fields.activity";
            // 
            // textBox11
            // 
            textBox11.Name = "textBox11";
            textBox11.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.22494754195213318D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox11.Style.Font.Name = "Times New Roman";
            textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox11.StyleName = "";
            textBox11.Value = "=Fields.isotope";
            // 
            // textBox13
            // 
            textBox13.Format = "{0:d}";
            textBox13.Name = "textBox13";
            textBox13.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.3166601657867432D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.22494754195213318D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox13.Style.Font.Name = "Times New Roman";
            textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            textBox13.StyleName = "";
            textBox13.Value = "=Fields.sourceTOC";
            // 
            // shape2
            // 
            shape2.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.9418537198798731E-05D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.79174548387527466D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape2.Name = "shape2";
            shape2.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            shape2.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(7.9999213218688965D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.1000000610947609D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox14
            // 
            textBox14.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.3000001907348633D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(5.7000002861022949D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox14.Name = "textBox14";
            textBox14.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.30000019073486328D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox14.Style.Font.Name = "Times New Roman";
            textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            textBox14.Value = "Reviewed By:";
            // 
            // shape3
            // 
            shape3.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(4.3000788688659668D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(5.8999996185302734D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape3.Name = "shape3";
            shape3.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            shape3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(3.6998813152313232D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.099999748170375824D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            // 
            // pageFooterSection1
            // 
            pageFooterSection1.Height = new Telerik.Reporting.Drawing.Unit(1D, Telerik.Reporting.Drawing.UnitType.Inch);
            pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            textBox21,
            textBox17,
            textBoxBVAVersion,
            textBox20,
            textBoxUnitID,
            textBoxPrintedOn});
            pageFooterSection1.Name = "pageFooterSection1";
            // 
            // textBox21
            // 
            textBox21.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(7D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.599999725818634D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox21.Name = "textBox21";
            textBox21.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.800000011920929D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.23999999463558197D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox21.Style.Font.Name = "Times New Roman";
            textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBox21.Value = "=\'Page \' + PageNumber + \' of \' + PageCount";
            // 
            // textBox17
            // 
            textBox17.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.099999986588954926D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.599999725818634D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox17.Name = "textBox17";
            textBox17.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.79992109537124634D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2395833283662796D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox17.Style.Font.Name = "Times New Roman";
            textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBox17.Value = "BVA-100:";
            // 
            // textBoxBVAVersion
            // 
            textBoxBVAVersion.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.89999997615814209D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.60000002384185791D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxBVAVersion.Name = "textBoxBVAVersion";
            textBoxBVAVersion.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.91992121934890747D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2395833283662796D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxBVAVersion.Style.Font.Name = "Times New Roman";
            textBoxBVAVersion.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBoxBVAVersion.Value = "v1.2.0.0";
            // 
            // textBox20
            // 
            textBox20.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.8200000524520874D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.60000002384185791D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox20.Name = "textBox20";
            textBox20.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.54158782958984375D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2395833283662796D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox20.Style.Font.Name = "Times New Roman";
            textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBox20.Value = "Unit ID:";
            // 
            // textBoxUnitID
            // 
            textBoxUnitID.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(2.4000000953674316D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.60000002384185791D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxUnitID.Name = "textBoxUnitID";
            textBoxUnitID.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.6456756591796875D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2395833283662796D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxUnitID.Style.Font.Name = "Times New Roman";
            textBoxUnitID.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBoxUnitID.Value = "-";
            // 
            // textBoxPrintedOn
            // 
            textBoxPrintedOn.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(5.0457549095153809D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.599999725818634D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxPrintedOn.Name = "textBoxPrintedOn";
            textBoxPrintedOn.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.9541667699813843D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.2395833283662796D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBoxPrintedOn.Style.Font.Name = "Times New Roman";
            textBoxPrintedOn.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            textBoxPrintedOn.Value = "textBoxDateTimePrinted";
            // 
            // ControlSourceSetupReport
            // 
            Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            pageHeaderSection1,
            detail,
            pageFooterSection1});
            PageSettings.Landscape = false;
            PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(0.20000000298023224D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(0.10000000149011612D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            Style.BackgroundColor = System.Drawing.Color.White;
            Width = new Telerik.Reporting.Drawing.Unit(8D, Telerik.Reporting.Drawing.UnitType.Inch);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        public Telerik.Reporting.TextBox textBoxDepartmentAddress;
        public Telerik.Reporting.TextBox textBoxDepartment;
        public Telerik.Reporting.TextBox textBoxDepartmentDirector;
        public Telerik.Reporting.TextBox textBoxDepartmentPhoneNumber;
        public Telerik.Reporting.TextBox textBoxHospitalName;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        public Telerik.Reporting.Table tableQCSources;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.Shape shape2;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.Shape shape3;
        private Telerik.Reporting.TextBox textBox21;
        public Telerik.Reporting.TextBox textBoxPrintedOn;
        private Telerik.Reporting.TextBox textBox17;
        public Telerik.Reporting.TextBox textBoxBVAVersion;
        private Telerik.Reporting.TextBox textBox20;
        public Telerik.Reporting.TextBox textBoxUnitID;


    }
}