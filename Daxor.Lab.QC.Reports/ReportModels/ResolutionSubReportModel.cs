﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.Domain.Interfaces;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Reports.Common;
using Daxor.Lab.QC.Reports.ReportUtilityClasses;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.QC.Reports.ReportModels
{
    public class ResolutionSubReportModel
    {
        #region Fields

        public List<ResolutionSample> ResolutionSamples = new List<ResolutionSample>();

        private readonly ISettingsManager _settingsManager;
        private readonly QcTest _qcTest;

        #endregion

        #region ctor

        public ResolutionSubReportModel(QcTest test, ISettingsManager settingsManager)
        {
            _settingsManager = settingsManager;
            _qcTest = test;

            PopulateResolutionSamples();
        }

        #endregion

        #region Public API

        public string GenerateResolutionAcceptanceCriteria()
        {
            return String.Format(QcReportConstants.ResolutionAcceptanceCriteriaFormatString, _settingsManager.GetSetting<double>(SettingKeys.QcBa133Centroid),
                _settingsManager.GetSetting<double>(SettingKeys.QcResolutionCentroidTolerance),
                _settingsManager.GetSetting<double>(SettingKeys.QcLinearityMinPassingFwhm),
                _settingsManager.GetSetting<double>(SettingKeys.QcLinearityMaxPassingFwhm),
                _settingsManager.GetSetting<int>(SettingKeys.QcLinearityMinCounts));
        }

        public string GenerateResolutionPassFailLabel()
        {
            if (_qcTest.Status == TestStatus.Aborted)
                return QcReportConstants.TestIncompleteLabel;
            return GenerateResolutionFailureDetails().Count != 0 ? QcReportConstants.CumulativeFailLabel : QcReportConstants.CumulativePassLabel;
        }

        public List<FailureDetails> GenerateResolutionFailureDetails()
        {
            var failureDetails = new List<FailureDetails>();
            foreach (var sample in ResolutionSamples.Where(sample => sample.Status == QcReportConstants.FailLabel))
            {
                AddCountFailureDetails(sample, failureDetails);
                AddFwhmFailureDetails(sample, failureDetails);
                AddCentroidFailureDetails(sample, failureDetails);
            }

            return failureDetails;
        }

        public string GenerateCalibrationAcceptanceCriteria()
        {
            return String.Format(QcReportConstants.CalibrationAcceptanceCriteriaFormatString, _settingsManager.GetSetting<double>(SettingKeys.QcCs137Centroid),
                                    _settingsManager.GetSetting<double>(SettingKeys.QcSetupCentroidPrecisionInkeV),
                                    _settingsManager.GetSetting<int>(SettingKeys.QcCalibrationTestMinPassingFwhm),
                                    _settingsManager.GetSetting<int>(SettingKeys.QcCalibrationTestMaxPassingFwhm),
                                    _settingsManager.GetSetting<int>(SettingKeys.QcLinearityMinCounts));
        }

        #endregion

        #region private methods

        private void PopulateResolutionSamples()
        {
            var sampleList = new[] { 1, 2, 21, 22 };
            var resolutionSamples = from sample in _qcTest.Samples
                                    where sampleList.Contains(sample.Position)
                                    select new ResolutionSample
                                    {
                                        Position = sample.Position,
                                        Status = QcReportHelpers.GeneratePassFailDetails(sample),
                                        ActualCounts = GenerateActualCountsDetails(sample),
                                        SampleName = sample.DisplayName,
                                        RegionOfInterest = GenerateRegionOfInterestDetails(sample),
                                        Centroid = GenerateCentroidDetails(sample),
                                        FWHM = GenerateFullWidthHalfMaxDetails(sample),
                                        SerialNumber = sample.SerialNumber,
                                        CountLimit = sample.Source.CountLimit
                                    };
            ResolutionSamples.AddRange(resolutionSamples.ToList());
        }

        private void AddCountFailureDetails(ResolutionSample sample, ICollection<FailureDetails> failureDetails)
        {
            int parsedCounts;

            var isParsedCountsValueInvalid = !int.TryParse(sample.ActualCounts, out parsedCounts);
            var areParsedCountsGreaterThanOrEqualToMinimum = parsedCounts >= _settingsManager.GetSetting<int>(SettingKeys.QcLinearityMinCounts);

            if (isParsedCountsValueInvalid || areParsedCountsGreaterThanOrEqualToMinimum) return;

            failureDetails.Add(new FailureDetails
            {
                SampleName = sample.SampleName,
                Reason = QcReportConstants.ActualCountsLabel,
                Value = sample.ActualCounts,
                AcceptanceRangeFrom = sample.CountLimit.ToString(CultureInfo.InvariantCulture),
                AcceptanceRangeTo = QcReportConstants.NotApplicableLabel
            });
        }

        private void AddFwhmFailureDetails(ResolutionSample sample, ICollection<FailureDetails> failureDetails)
        {
            var resolutionMaxPassingFwhm = _settingsManager.GetSetting<double>(SettingKeys.QcLinearityMaxPassingFwhm);
            var resolutionMinPassingFwhm = _settingsManager.GetSetting<double>(SettingKeys.QcLinearityMinPassingFwhm);
            var calibrationMaxPassingFwhm = _settingsManager.GetSetting<double>(SettingKeys.QcCalibrationTestMaxPassingFwhm);
            var calibrationMinPassingFwhm = _settingsManager.GetSetting<double>(SettingKeys.QcCalibrationTestMinPassingFwhm);

            var maxPassingFwhmToUse = calibrationMaxPassingFwhm;
            var minPassingFwhmToUse = calibrationMinPassingFwhm;

            if (sample.SampleName != QcReportConstants.Qc1SampleName)
            {
                maxPassingFwhmToUse = resolutionMaxPassingFwhm;
                minPassingFwhmToUse = resolutionMinPassingFwhm;
            }

            double parsedFwhm;
            var isParsedFwhmValueInvalid = !double.TryParse(sample.FWHM, out parsedFwhm);
            var isFwhmInsideAcceptanceRange = !(parsedFwhm > maxPassingFwhmToUse) && !(parsedFwhm < minPassingFwhmToUse);
            if (isParsedFwhmValueInvalid || isFwhmInsideAcceptanceRange) return;

            failureDetails.Add(new FailureDetails
            {
                SampleName = sample.SampleName,
                Reason = QcReportConstants.FwhmLabel,
                Value = sample.FWHM,
                AcceptanceRangeFrom = minPassingFwhmToUse.ToString(CultureInfo.InvariantCulture),
                AcceptanceRangeTo = maxPassingFwhmToUse.ToString(CultureInfo.InvariantCulture)
            });
        }

        private void AddCentroidFailureDetails(ResolutionSample sample, ICollection<FailureDetails> failureDetails)
        {
            var ba133Centroid = _settingsManager.GetSetting<double>(SettingKeys.QcBa133Centroid);
            var cs137Centroid = _settingsManager.GetSetting<double>(SettingKeys.QcCs137Centroid);
            var resolutionCentroidTolerance = _settingsManager.GetSetting<double>(SettingKeys.QcResolutionCentroidTolerance);
            var calibrationCentroidTolerance = _settingsManager.GetSetting<double>(SettingKeys.QcSetupCentroidPrecisionInkeV);

            var centroidToUse = ba133Centroid;
            var centroidToleranceToUse = resolutionCentroidTolerance;

            if (sample.SampleName == QcReportConstants.Qc1SampleName)
            {
                centroidToUse = cs137Centroid;
                centroidToleranceToUse = calibrationCentroidTolerance;
            }

            double parsedCentroid;
            var isCentroidValueInvalid = !double.TryParse(sample.Centroid, out parsedCentroid);
            var isCentroidInsideTolerance = !(Math.Abs(parsedCentroid - centroidToUse) > centroidToleranceToUse);
            if (isCentroidValueInvalid || isCentroidInsideTolerance) return;

            failureDetails.Add(new FailureDetails
            {
                SampleName = sample.SampleName,
                Reason = QcReportConstants.CentroidLabel,
                Value = sample.Centroid,
                AcceptanceRangeFrom = (centroidToUse - centroidToleranceToUse).ToString(CultureInfo.InvariantCulture),
                AcceptanceRangeTo = (centroidToUse + centroidToleranceToUse).ToString(CultureInfo.InvariantCulture)
            });
        }

        private string GenerateActualCountsDetails(QcSample sample)
        {
            if (sample.IsPassed == null || sample.CountsInRegionOfInterest <= 0)
                return QcReportConstants.NotApplicableLabel;

            return sample.CountsInRegionOfInterest.ToString(CultureInfo.InvariantCulture);
        }

        private string GenerateFullWidthHalfMaxDetails(QcSample sample)
        {
            if (sample.IsPassed == null || sample.FullWidthHalfMax <= 0)
                return QcReportConstants.NotApplicableLabel;

            return Math.Round(sample.FullWidthHalfMax, 2, MidpointRounding.AwayFromZero).ToString(QcReportConstants.RoundedToTwoPlacesFormatString);
        }

        private string GenerateCentroidDetails(QcSample sample)
        {
            if (sample.IsPassed == null || sample.Centroid <= 0)
                return QcReportConstants.NotApplicableLabel;

            return Math.Round(sample.Centroid, 2, MidpointRounding.AwayFromZero).ToString(QcReportConstants.RoundedToTwoPlacesFormatString);
        }

        private static string GenerateRegionOfInterestDetails(ISample sample)
        {
            return sample.DisplayName == QcReportConstants.Qc1SampleName ? QcReportConstants.Cs137RegionOfInterest : QcReportConstants.Ba133RegionOfInterest;
        }

        #endregion
    }
}
