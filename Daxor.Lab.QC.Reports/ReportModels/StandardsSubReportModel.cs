﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Daxor.Lab.Domain.Services;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Reports.Common;
using Daxor.Lab.QC.Reports.ReportUtilityClasses;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.QC.Reports.ReportModels
{
    public class StandardsSubReportModel
    {
        #region Fields

        public List<StandardSample> StandardSamples = new List<StandardSample>();

        private readonly int[] _standardsList = { 11, 12 };
        private readonly ISettingsManager _settingsManager;
        private readonly QcTest _qcTest;

        #endregion

        #region ctor

        public StandardsSubReportModel(QcTest test, ISettingsManager settingsManager)
        {
            _settingsManager = settingsManager;
            _qcTest = test;

            PopulateStandardSamples();
        }

        #endregion

        #region Properties

        public string InjectateLot
        {
            get { return _qcTest.InjectateLot; }
        }

        #endregion

        #region Public API

        public string GenerateStandardsCountTimeLabel()
        {
            var standardCountTimeInMinutes = Math.Round(_settingsManager.GetSetting<double>(SettingKeys.QcStandardsCountTimeInSeconds) / 60, 1, MidpointRounding.AwayFromZero);
            return String.Format(QcReportConstants.CountTimeFormatString, standardCountTimeInMinutes);
        }

        public string GenerateStandardsAcceptanceCriteriaLabel1()
        {
            return String.Format(QcReportConstants.StandardsAcceptanceCriteriaFormatString1, _settingsManager.GetSetting<double>(SettingKeys.QcStandardsStdDevDifferenceMultiplier));
        }

        public string GenerateStandardsAcceptanceCriteria2()
        {
            return String.Format(QcReportConstants.StandardsAcceptanceCriteriaFormatString2, _settingsManager.GetSetting<int>(SettingKeys.QcStandardsMinCpm));
        }

        public string GenerateStandardsPassFailLabel()
        {
            var isStandardsTestIncomplete = _qcTest.Samples.Where(s => _standardsList.Contains(s.Position)).All(s => s.IsPassed == null);
            if (isStandardsTestIncomplete)
                return QcReportConstants.TestIncompleteLabel;

            var isStandardsTestPassed = _qcTest.Samples.Where(s => _standardsList.Contains(s.Position)).All(s => s.IsPassed == true);
            return isStandardsTestPassed ? QcReportConstants.CumulativePassLabel : QcReportConstants.CumulativeFailLabel;
        }

        public List<FailureDetails> GenerateStandardsFailureDetails()
        {
            var failureDetails = new List<FailureDetails>();

            foreach (var sample in StandardSamples.Where(sample => sample.Status == QcReportConstants.FailLabel))
            {
                if (SampleCountsAreLessThanMinimum(sample))
                    AddStandardCountsAreLessThanMinimumFailureDetails(sample, failureDetails);
                else
                {
                    var standardsCounts = GetStandardsCountsFromQcTest();
                    var maximumAllowableDifference = StatisticsHelpers.ComputeAllowableDifference(standardsCounts);
                    double actualCountDifference = Math.Abs(standardsCounts[0] - standardsCounts[1]);

                    if (actualCountDifference > maximumAllowableDifference)
                        AddStandardCountsAreGreaterThanAllowableDifferenceFailureDetails(failureDetails, sample,
                            actualCountDifference, maximumAllowableDifference);
                }
            }
            return failureDetails;
        }

        #endregion

        #region private methods

        private bool IsSampleDoneCounting(QcSample sample)
        {
            var desiredLiveTime = _settingsManager.GetSetting<Int32>(SettingKeys.QcStandardsCountTimeInSeconds);
            return Math.Abs(sample.Spectrum.LiveTimeInSeconds - desiredLiveTime) < 1E-10;
        }

        private void PopulateStandardSamples()
        {
            var standardSamples = from sample in _qcTest.Samples
                                  where _standardsList.Contains(sample.Position)
                                  orderby sample.Position
                                  select new StandardSample
                                  {
                                      Position = sample.Position,
                                      Status = QcReportHelpers.GeneratePassFailDetails(sample),
                                      MeasuredRate = GenerateCountsPerMinuteDetails(sample),
                                      SampleName = sample.DisplayName,
                                      ActualCounts = !IsSampleDoneCounting(sample) ? QcReportConstants.NotApplicableLabel : sample.CountsInRegionOfInterest.ToString(CultureInfo.InvariantCulture),
                                      ErrorDetails = sample.ErrorDetails
                                  };

            StandardSamples.AddRange(standardSamples.ToList());
        }

        private string GenerateCountsPerMinuteDetails(QcSample sample)
        {
            if (!IsSampleDoneCounting(sample))
                return QcReportConstants.NotApplicableLabel;

            var countsPerMinute = (int)Math.Round(sample.CountsPerMinute, 0, MidpointRounding.AwayFromZero);
            return countsPerMinute < 0 ? "0" : countsPerMinute.ToString(CultureInfo.InvariantCulture);
        }

        private static void AddStandardCountsAreGreaterThanAllowableDifferenceFailureDetails(ICollection<FailureDetails> failureDetails,
                                StandardSample sample, double actualCountDifference, int maximumAllowableDifference)
        {
            failureDetails.Add(new FailureDetails
            {
                SampleName = sample.SampleName,
                Reason = QcReportConstants.StandardDeviationDifferenceLabel,
                Value = actualCountDifference.ToString(CultureInfo.InvariantCulture),
                AcceptanceRangeFrom = "0",
                AcceptanceRangeTo = maximumAllowableDifference.ToString(CultureInfo.InvariantCulture)
            });
        }

        private List<int> GetStandardsCountsFromQcTest()
        {
            return (from sample in _qcTest.Samples
                    where _standardsList.Contains(sample.Position)
                    orderby sample.Position
                    select sample.CountsInRegionOfInterest).ToList<int>();
        }

        private void AddStandardCountsAreLessThanMinimumFailureDetails(StandardSample sample, ICollection<FailureDetails> failureDetails)
        {
            failureDetails.Add(new FailureDetails
            {
                SampleName = sample.SampleName,
                Reason = QcReportConstants.StandardCountsLabel,
                Value = sample.ActualCounts,
                AcceptanceRangeFrom = _settingsManager.GetSetting<int>(SettingKeys.QcStandardsMinCpm).ToString(CultureInfo.InvariantCulture),
                AcceptanceRangeTo = QcReportConstants.NotApplicableLabel
            });
        }

        private bool SampleCountsAreLessThanMinimum(StandardSample sample)
        {
            return Int32.Parse(sample.ActualCounts) < _settingsManager.GetSetting<int>(SettingKeys.QcStandardsMinCpm);
        }

        #endregion
    }
}
