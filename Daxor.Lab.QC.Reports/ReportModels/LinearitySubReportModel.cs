﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Reports.Common;
using Daxor.Lab.QC.Reports.ReportUtilityClasses;

namespace Daxor.Lab.QC.Reports.ReportModels
{
    public class LinearitySubReportModel
    {
        #region Fields
        public List<LinearitySample> LinearitySamples = new List<LinearitySample>();

        private readonly QcTest _qcTest;
        private readonly int[] _qcSourcesList = { 2, 21, 22 };

        #endregion

        #region ctor

        public LinearitySubReportModel(QcTest test)
        {
            _qcTest = test;

            PopulateLinearitySamples();
        }

        #endregion

        #region Public API

        public List<FailureDetails> GenerateLinearityFailureDetails()
        {
            var linearityFailureDetails = new List<FailureDetails>();
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var sample in LinearitySamples.Where(sample => sample.Status == QcReportConstants.FailLabel && !IsCalibrationSample(sample)))
            {
                linearityFailureDetails.Add(new FailureDetails
                {
                    SampleName = sample.SampleName,
                    Reason = QcReportConstants.RateRatioFailureLabel,
                    Value = sample.MeasuredRatioOfRates,
                    AcceptanceRangeFrom = sample.From,
                    AcceptanceRangeTo = sample.To
                });
            }

            return linearityFailureDetails;
        }

        public bool IsLinearityQcFailed()
        {
            return LinearitySamples.Any(s => s.Status == QcReportConstants.FailLabel) &&
                    _qcTest.Samples.Where(s => _qcSourcesList.Contains(s.Position)).All(AreQcSourcesCompleted);
        }

        public string GenerateLinearityTestPassFailLabel()
        {
            if (IsLinearityQcFailed())
                return QcReportConstants.CumulativeFailLabel;

            return _qcTest.Samples.Where(s => _qcSourcesList.Contains(s.Position))
                                  .All(AreQcSourcesCompleted) ? QcReportConstants.CumulativePassLabel : QcReportConstants.TestIncompleteLabel;
        }

        #endregion

        #region Private Methods

        private void PopulateLinearitySamples()
        {
            var linearitySamples = (from sample in _qcTest.Samples
                                    where _qcSourcesList.Contains(sample.Position) || sample.Position == 1
                                    orderby sample.Position
                                    select new LinearitySample
                                    {
                                        SampleName = sample.DisplayName,
                                        Position = sample.Position,
                                        NominalActivity = sample.Source.Activity.ToString(QcReportConstants.RoundedToTwoPlacesFormatString),
                                        MeasuredRate = Math.Round((sample.CountsInRegionOfInterest / sample.Spectrum.LiveTimeInSeconds * 60), 0, MidpointRounding.AwayFromZero).ToString(CultureInfo.InvariantCulture),
                                        Status = QcReportHelpers.GeneratePassFailDetails(sample),
                                    }).ToArray();

            var startingSourceActivity = ComputeStartingSourceActivity(linearitySamples);
            var startingMeasuredRate = ComputeStartingMeasuredRate(linearitySamples);

            foreach (var linearitySample in linearitySamples)
            {
                SetAcceptableRatioOfRates(linearitySample, startingSourceActivity);
                SetMeasuredRatioOfRates(linearitySample, startingMeasuredRate);
                SetLinearitySamplePassState(linearitySample, startingMeasuredRate);
            }

            LinearitySamples.AddRange(linearitySamples);
        }

        private void SetAcceptableRatioOfRates(LinearitySample sample, double startSourceActivity)
        {
            if (IsQc2Sample(sample))
            {
                sample.From = QcReportConstants.NotApplicableLabel;
                sample.To = QcReportConstants.NotApplicableLabel;
            }

            if (IsQc3OrQc4Sample(sample))
            {
                double ratioLowBound;
                double ratioHighBound;
                CalculateQcRatioBounds(startSourceActivity, double.Parse(sample.NominalActivity), out ratioLowBound, out ratioHighBound);
                sample.From = ratioLowBound.ToString(CultureInfo.InvariantCulture);
                sample.To = ratioHighBound.ToString(CultureInfo.InvariantCulture);
            }
        }

        private static void SetMeasuredRatioOfRates(LinearitySample sample, double startingMeasuredRate)
        {
            if (IsQc2Sample(sample))
                sample.MeasuredRatioOfRates = QcReportConstants.Qc2MeasuredRatioOfRates;
            if (IsQc3OrQc4Sample(sample))
                sample.MeasuredRatioOfRates = Math.Round(startingMeasuredRate / double.Parse(sample.MeasuredRate), 2, MidpointRounding.AwayFromZero).ToString(QcReportConstants.RoundedToTwoPlacesFormatString);
        }

        private static void SetLinearitySamplePassState(LinearitySample sample, double startingMeasuredRate)
        {
            if (!IsQc3OrQc4Sample(sample)) return;

            if (sample.Status == QcReportConstants.NotApplicableLabel) return;

            var ratioLowBound = double.Parse(sample.From);
            var ratioHighBound = double.Parse(sample.To);
            sample.Status = IsMeasuredRateWithinBounds(sample, startingMeasuredRate, ratioLowBound, ratioHighBound) ? QcReportConstants.PassLabel : QcReportConstants.FailLabel;
        }

        private static bool IsQc2Sample(LinearitySample sample)
        {
            return sample.Position == 2;
        }

        private static bool IsQc3OrQc4Sample(LinearitySample sample)
        {
            return sample.Position == 21 || sample.Position == 22;
        }

        private static bool IsMeasuredRateWithinBounds(LinearitySample sample, double startingMeasuredRate, double ratioLowBound, double ratioHighBound)
        {
            return Math.Round(startingMeasuredRate / double.Parse(sample.MeasuredRate), 2, MidpointRounding.AwayFromZero) >= ratioLowBound &&
                   Math.Round(startingMeasuredRate / double.Parse(sample.MeasuredRate), 2, MidpointRounding.AwayFromZero) <= ratioHighBound;
        }

        private static void CalculateQcRatioBounds(double startSourceActivity, double nominalActivity, out double ratioLowBound, out double ratioHighBound)
        {
            var nominalRatio = Math.Round(startSourceActivity / nominalActivity, 2, MidpointRounding.AwayFromZero);
            var adjustmentFactor = Math.Round((nominalRatio * QcReportConstants.QcRatioMultiplier), 2, MidpointRounding.AwayFromZero);

            ratioLowBound = nominalRatio - adjustmentFactor;
            ratioHighBound = nominalRatio + adjustmentFactor;
        }

        private static bool AreQcSourcesCompleted(QcSample s)
        {
            return s.IsPassed != null;
        }

        private static bool IsCalibrationSample(LinearitySample sample)
        {
            return sample.Position == 1;
        }

        private static double ComputeStartingSourceActivity(IEnumerable<LinearitySample> linearitySamples)
        {
            var qc2Sample = GetQc2Sample(linearitySamples);

            double activity;
            if (double.TryParse(qc2Sample.NominalActivity, out activity))
                return activity;

            throw new NotSupportedException("QC 2 sample's nominal activity '" + qc2Sample.NominalActivity + "' is invalid");
        }

        private static double ComputeStartingMeasuredRate(IEnumerable<LinearitySample> linearitySamples)
        {
            var qc2Sample = GetQc2Sample(linearitySamples);
            var measuredRate = double.Parse(qc2Sample.MeasuredRate);
            return measuredRate;
        }

        private static LinearitySample GetQc2Sample(IEnumerable<LinearitySample> linearitySamples)
        {
            var qc2Sample = (from sample in linearitySamples where IsQc2Sample(sample) select sample).FirstOrDefault();
            if (qc2Sample == null)
                throw new NotSupportedException("Cannot find QC 2 linearity sample");

            return qc2Sample;
        }

        #endregion
    }
}
