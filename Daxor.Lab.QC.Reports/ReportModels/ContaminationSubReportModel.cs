﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Reports.Common;
using Daxor.Lab.QC.Reports.ReportUtilityClasses;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.QC.Reports.ReportModels
{
    public class ContaminationSubReportModel
    {
        #region Fields

        private readonly ISettingsManager _settingsManager;
        private readonly QcTest _qcTest;

        private readonly int[] _firstHalfContaminationList = { 25, 3, 4, 5, 6, 7, 8, 9, 10 };
        private readonly int[] _secondHalfContaminationList = { 23, 20, 19, 18, 17, 16, 15, 14, 13 };
        private readonly List<ContaminationSample> _firstHalfOfContaminationSamplesList = new List<ContaminationSample>();
        private readonly List<ContaminationSample> _secondHalfOfContaminationSamplesList = new List<ContaminationSample>();

        #endregion

        #region ctor

        public ContaminationSubReportModel(QcTest test, ISettingsManager settingsManager)
        {
            _settingsManager = settingsManager;
            _qcTest = test;

            PopulateGivenContaminationSamples(_firstHalfContaminationList, FirstHalfOfContaminationSamples);
            PopulateGivenContaminationSamples(_secondHalfContaminationList, SecondHalfOfContaminationSamples);
            SecondHalfOfContaminationSamples.Reverse();
        }

        #endregion

        #region properties

        public List<ContaminationSample> FirstHalfOfContaminationSamples
        {
            get { return _firstHalfOfContaminationSamplesList; }
        }

        public List<ContaminationSample> SecondHalfOfContaminationSamples
        {
            get { return _secondHalfOfContaminationSamplesList; }
        }

        #endregion

        #region Public API

        public string GenerateContaminationPassFailLabel()
        {
            var contaminationList = _firstHalfContaminationList.ToList();
            contaminationList.AddRange(_secondHalfContaminationList);

            var isContaminationTestIncomplete = _qcTest.Samples.Where(s => contaminationList.Contains(s.Position)).Any(s => s.IsPassed == null);
            if (isContaminationTestIncomplete)
                return QcReportConstants.TestIncompleteLabel;

            var isContaminationTestPassed = _qcTest.Samples.Where(s => contaminationList.Contains(s.Position)).All(s => s.IsPassed == true);
            return isContaminationTestPassed ? QcReportConstants.CumulativePassLabel : QcReportConstants.CumulativeFailLabel;
        }

        public string GenerateContaminationCountTime()
        {
            var contaminationCounTimeInMinutes = Math.Round(_settingsManager.GetSetting<double>(SettingKeys.QcContaminationCountTimeInSeconds) / 60, 1, MidpointRounding.AwayFromZero);
            return String.Format(QcReportConstants.CountTimeFormatString, contaminationCounTimeInMinutes);
        }

        public string GenerateContaminationAcceptanceCriteria()
        {
            return String.Format(QcReportConstants.ContaminationAcceptanceCriteriaFormatString,
                _settingsManager.GetSetting<double>(SettingKeys.QcContaminationBackgroundMultiplier),
                _settingsManager.GetSetting<int>(SettingKeys.QcContaminationMaxCpm));
        }

        #endregion

        #region private methods

        private void PopulateGivenContaminationSamples(IEnumerable<int> samplePositions, List<ContaminationSample> listToPopulate)
        {
            var contaminationSamples = from sample in _qcTest.Samples
                                       where samplePositions.Contains(sample.Position)
                                       select new ContaminationSample
                                       {
                                           Position = sample.Position,
                                           Status = QcReportHelpers.GeneratePassFailDetails(sample),
                                           MeasuredRate = GenerateMeasuredRateString(sample),
                                           SampleName = sample.DisplayName
                                       };

            listToPopulate.AddRange(contaminationSamples.ToList());
        }

        private string GenerateMeasuredRateString(QcSample sample)
        {
            if (sample.IsPassed == null)
                return QcReportConstants.NotApplicableLabel;

            return ((int)Math.Round(sample.CountsPerMinute, 0, MidpointRounding.AwayFromZero)).ToString(CultureInfo.InvariantCulture);
        }

        #endregion
    }
}
