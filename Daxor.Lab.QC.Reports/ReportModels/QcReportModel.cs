﻿using System;
using System.Globalization;
using Daxor.Lab.Domain.Common;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.QC.Reports.Common;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.QC.Reports.ReportModels
{
    public class QcReportModel
    {
        #region Fields

        private readonly ISettingsManager _settingsManager;
        private readonly QcTest _qcTest;

        #endregion

        #region Ctor

        public QcReportModel(QcTest test, ISettingsManager settingsManager)
        {
            _qcTest = test;
            _settingsManager = settingsManager;
        }

        #endregion

        #region Properties

        public string Analyst { get { return _qcTest.Analyst; } }

        public string Comments { get { return _qcTest.Comments; } }

        public string Department
        {
            get { return _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentName); }
        }

        public string DepartmentAddress
        {
            get { return _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentAddress); }
        }

        public string DepartmentDirector
        {
            get { return _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentDirector); }
        }

        public string DepartmentPhoneNumber
        {
            get { return _settingsManager.GetSetting<string>(SettingKeys.SystemDepartmentPhoneNumber); }
        }

        public string HospitalName
        {
            get { return _settingsManager.GetSetting<string>(SettingKeys.SystemHospitalName); }
        }

        public string Voltage
        {
            get { return _qcTest.Voltage.ToString(CultureInfo.InvariantCulture); }
        }

        public TestType QCType
        {
            get { return _qcTest.QCType; }
        }

        #endregion

        #region Methods

        public string GenerateBvaVersion()
        {
            return QcReportConstants.VersionPrefix + _settingsManager.GetSetting<string>(SettingKeys.BvaModuleVersion);
        }

        public string GenerateUnitId()
        {
            return _qcTest.SystemId ?? string.Empty;
        }

        public string GenerateCumulativeTestState()
        {
            if (IsTestAborted())
                return QcReportConstants.CumulativeAbortedLabel;

            return _qcTest.IsPassed ? QcReportConstants.CumulativePassLabel : QcReportConstants.CumulativeFailLabel;
        }

        public string GenerateEndFineGain()
        {
            if (IsTestAborted())
                return QcReportConstants.NotApplicableLabel;

            if (TestContainsCalibrationSubtest())
                return Math.Round(_qcTest.EndFineGain, 3, MidpointRounding.AwayFromZero)
                        .ToString(QcReportConstants.RoundedToThreePlacesFormatString);

            return Math.Round(_qcTest.StartFineGain, 3, MidpointRounding.AwayFromZero).ToString(QcReportConstants.RoundedToThreePlacesFormatString);
        }

        public string GenerateStartFineGain()
        {
            return Math.Round(_qcTest.StartFineGain, 3, MidpointRounding.AwayFromZero).ToString(QcReportConstants.RoundedToThreePlacesFormatString);
        }

        public string GenerateTestDate()
        {
            return _qcTest.Date.ToShortDateString() + " " + _qcTest.Date.ToShortTimeString();
        }

        public string GeneratePrintedOnDate()
        {
            return String.Format(QcReportConstants.PrintedOnDateFormatString, DateTime.Now.ToString("g"));
        }

        #endregion

        #region Private Helper Methods

        private bool TestContainsCalibrationSubtest()
        {
            return _qcTest.QCType == TestType.Full || _qcTest.QCType == TestType.Linearity ||
                   _qcTest.QCType == TestType.Calibration;
        }

        private bool IsTestAborted()
        {
            return _qcTest.Status == TestStatus.Aborted;
        }

        #endregion
    }
}
