﻿using System;
using Daxor.Lab.QC.Core;
using Daxor.Lab.QC.Reports.Common;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.SettingsManager.Common;
using Daxor.Lab.SettingsManager.Interfaces;

namespace Daxor.Lab.QC.Reports.ReportModels
{
    public class BackgroundSubReportModel
    {
        #region Fields

        private readonly ISettingsManager _settingsManager;
        private readonly QcTest _qcTest;

        #endregion

        #region ctor

        public BackgroundSubReportModel(QcTest test, ISettingsManager settingsManager)
        {
            _qcTest = test;
            _settingsManager = settingsManager;
        }

        #endregion

        #region Public API

        public int GenerateRoundedBackgroundCountsPerMinute()
        {
            return (int)Math.Round(_qcTest.BackgroundCountsPerMinute(), 0, MidpointRounding.AwayFromZero);
        }

        public string GenerateBackgroundAcceptanceCriteria()
        {
            return String.Format(QcReportConstants.BackgroundAcceptanceCriteriaFormatString, _settingsManager.GetSetting<int>(SettingKeys.SystemHighCpmBoundary));
        }

        public string GenerateBackgroundState()
        {
            return GenerateRoundedBackgroundCountsPerMinute() < _settingsManager.GetSetting<int>(SettingKeys.SystemHighCpmBoundary) ? QcReportConstants.CumulativePassLabel : QcReportConstants.CumulativeFailLabel;
        }

        public string GenerateBackgroundCountTime()
        {
            var backgroundTimeInMinutes = (double)_settingsManager.GetSetting<int>(SettingKeys.SystemBackgroundAcquisitionTimeInSeconds) / 60;
            backgroundTimeInMinutes = Math.Round(backgroundTimeInMinutes, 1, MidpointRounding.AwayFromZero);

            return String.Format(QcReportConstants.CountTimeFormatString, backgroundTimeInMinutes);
        }

        #endregion
    }
}
