namespace Daxor.Lab.QC.Reports
{
    partial class SubReport_Standards
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBoxStandardsPassFail = new Telerik.Reporting.TextBox();
            this.textBoxCountTime = new Telerik.Reporting.TextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBoxStandardsAcceptanceCriteria = new Telerik.Reporting.TextBox();
            this.tableStandardsSamples = new Telerik.Reporting.Table();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.panelFailureDetails = new Telerik.Reporting.Panel();
            this.tableFailureDetails = new Telerik.Reporting.Table();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBoxFailureDetailsLabel = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBoxStandardsAcceptanceCriteria1 = new Telerik.Reporting.TextBox();
            this.textBoxStandardsAcceptanceCriteria2 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBoxInjectateLot = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9603028297424316D), Telerik.Reporting.Drawing.Unit.Inch(0.19583332538604736D));
            this.textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Times New Roman";
            this.textBox2.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "Sample";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7361226081848145D), Telerik.Reporting.Drawing.Unit.Inch(0.19583332538604736D));
            this.textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Times New Roman";
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "Rate (cpm)";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0032565593719482D), Telerik.Reporting.Drawing.Unit.Inch(0.19583332538604736D));
            this.textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Times New Roman";
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.StyleName = "";
            this.textBox8.Value = "Measured Counts";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1002786159515381D), Telerik.Reporting.Drawing.Unit.Inch(0.19583332538604736D));
            this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Times New Roman";
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.Value = "Pass/Fail";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1299891471862793D), Telerik.Reporting.Drawing.Unit.Inch(0.17708329856395721D));
            this.textBox10.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Times New Roman";
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox10.Value = "ID";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1299891471862793D), Telerik.Reporting.Drawing.Unit.Inch(0.17708329856395721D));
            this.textBox11.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Times New Roman";
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox11.Value = "Failed Item";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1537436246871948D), Telerik.Reporting.Drawing.Unit.Inch(0.17708329856395721D));
            this.textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Times New Roman";
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox12.Value = "Value";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0649945735931397D), Telerik.Reporting.Drawing.Unit.Inch(0.17708329856395721D));
            this.textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Times New Roman";
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "From";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0649940967559815D), Telerik.Reporting.Drawing.Unit.Inch(0.17708329856395721D));
            this.textBox18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Name = "Times New Roman";
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "To";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(2.5999999046325684D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBoxStandardsPassFail,
            this.textBoxCountTime,
            this.panel1});
            this.detail.Name = "detail";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.099999986588954926D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
            this.textBox1.Style.BackgroundColor = System.Drawing.Color.Black;
            this.textBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.Color = System.Drawing.Color.White;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Standards";
            // 
            // textBoxStandardsPassFail
            // 
            this.textBoxStandardsPassFail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.1999611854553223D), Telerik.Reporting.Drawing.Unit.Inch(0.11999988555908203D));
            this.textBoxStandardsPassFail.Name = "textBoxStandardsPassFail";
            this.textBoxStandardsPassFail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999990701675415D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBoxStandardsPassFail.Style.Font.Bold = true;
            this.textBoxStandardsPassFail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBoxStandardsPassFail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBoxStandardsPassFail.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxStandardsPassFail.Value = "";
            // 
            // textBoxCountTime
            // 
            this.textBoxCountTime.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.613539457321167D), Telerik.Reporting.Drawing.Unit.Inch(0.12696807086467743D));
            this.textBoxCountTime.Name = "textBoxCountTime";
            this.textBoxCountTime.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBoxCountTime.Style.Font.Name = "Times New Roman";
            this.textBoxCountTime.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxCountTime.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxCountTime.Value = "3.0 Minute Count Time";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxStandardsAcceptanceCriteria,
            this.tableStandardsSamples,
            this.panelFailureDetails,
            this.textBoxStandardsAcceptanceCriteria1,
            this.textBoxStandardsAcceptanceCriteria2,
            this.textBox21,
            this.textBoxInjectateLot});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.340078741312027D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(2.1600000858306885D));
            this.panel1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBoxStandardsAcceptanceCriteria
            // 
            this.textBoxStandardsAcceptanceCriteria.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0599212646484375D), Telerik.Reporting.Drawing.Unit.Inch(0.12992119789123535D));
            this.textBoxStandardsAcceptanceCriteria.Name = "textBoxStandardsAcceptanceCriteria";
            this.textBoxStandardsAcceptanceCriteria.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0399999618530273D), Telerik.Reporting.Drawing.Unit.Inch(0.11999999731779099D));
            this.textBoxStandardsAcceptanceCriteria.Style.Font.Bold = false;
            this.textBoxStandardsAcceptanceCriteria.Style.Font.Name = "Times New Roman";
            this.textBoxStandardsAcceptanceCriteria.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBoxStandardsAcceptanceCriteria.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBoxStandardsAcceptanceCriteria.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBoxStandardsAcceptanceCriteria.Value = "Acceptance Criteria: ";
            // 
            // tableStandardsSamples
            // 
            this.tableStandardsSamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.9603030681610107D)));
            this.tableStandardsSamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.7361228466033936D)));
            this.tableStandardsSamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.0032563209533691D)));
            this.tableStandardsSamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.100278377532959D)));
            this.tableStandardsSamples.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19583337008953095D)));
            this.tableStandardsSamples.Body.SetCellContent(0, 0, this.textBox5);
            this.tableStandardsSamples.Body.SetCellContent(0, 1, this.textBox6);
            this.tableStandardsSamples.Body.SetCellContent(0, 3, this.textBox7);
            this.tableStandardsSamples.Body.SetCellContent(0, 2, this.textBox9);
            tableGroup12.ReportItem = this.textBox2;
            tableGroup13.ReportItem = this.textBox3;
            tableGroup14.Name = "Group1";
            tableGroup14.ReportItem = this.textBox8;
            tableGroup15.ReportItem = this.textBox4;
            this.tableStandardsSamples.ColumnGroups.Add(tableGroup12);
            this.tableStandardsSamples.ColumnGroups.Add(tableGroup13);
            this.tableStandardsSamples.ColumnGroups.Add(tableGroup14);
            this.tableStandardsSamples.ColumnGroups.Add(tableGroup15);
            this.tableStandardsSamples.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox9,
            this.textBox2,
            this.textBox3,
            this.textBox8,
            this.textBox4});
            this.tableStandardsSamples.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.75999999046325684D));
            this.tableStandardsSamples.Name = "tableStandardsSamples";
            tableGroup16.Groupings.Add(new Telerik.Reporting.Grouping(""));
            tableGroup16.Name = "DetailGroup";
            this.tableStandardsSamples.RowGroups.Add(tableGroup16);
            this.tableStandardsSamples.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7999606132507324D), Telerik.Reporting.Drawing.Unit.Inch(0.39166668057441711D));
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9603029489517212D), Telerik.Reporting.Drawing.Unit.Inch(0.19583337008953095D));
            this.textBox5.Style.Font.Name = "Times New Roman";
            this.textBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBox5.Value = "=Fields.SampleName";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.736122727394104D), Telerik.Reporting.Drawing.Unit.Inch(0.19583337008953095D));
            this.textBox6.Style.Font.Name = "Times New Roman";
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox6.Value = "=Fields.MeasuredRate";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.100278377532959D), Telerik.Reporting.Drawing.Unit.Inch(0.19583337008953095D));
            this.textBox7.Style.Font.Name = "Times New Roman";
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Value = "=Fields.Status";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0032563209533691D), Telerik.Reporting.Drawing.Unit.Inch(0.19583337008953095D));
            this.textBox9.Style.Font.Name = "Times New Roman";
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.StyleName = "";
            this.textBox9.Value = "=Fields.ActualCounts";
            // 
            // panelFailureDetails
            // 
            this.panelFailureDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.tableFailureDetails,
            this.textBoxFailureDetailsLabel,
            this.textBox20});
            this.panelFailureDetails.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.1749999523162842D));
            this.panelFailureDetails.Name = "panelFailureDetails";
            this.panelFailureDetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.79992151260376D), Telerik.Reporting.Drawing.Unit.Inch(0.81878966093063354D));
            this.panelFailureDetails.Style.Visible = false;
            // 
            // tableFailureDetails
            // 
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.1299891471862793D)));
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.1299891471862793D)));
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1537435054779053D)));
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0649945735931397D)));
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0649939775466919D)));
            this.tableFailureDetails.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19791695475578308D)));
            this.tableFailureDetails.Body.SetCellContent(0, 0, this.textBox13);
            this.tableFailureDetails.Body.SetCellContent(0, 1, this.textBox14);
            this.tableFailureDetails.Body.SetCellContent(0, 2, this.textBox15);
            this.tableFailureDetails.Body.SetCellContent(0, 3, this.textBox17);
            this.tableFailureDetails.Body.SetCellContent(0, 4, this.textBox19);
            tableGroup17.ReportItem = this.textBox10;
            tableGroup18.ReportItem = this.textBox11;
            tableGroup19.ReportItem = this.textBox12;
            tableGroup20.Name = "Group1";
            tableGroup20.ReportItem = this.textBox16;
            tableGroup21.Name = "Group2";
            tableGroup21.ReportItem = this.textBox18;
            this.tableFailureDetails.ColumnGroups.Add(tableGroup17);
            this.tableFailureDetails.ColumnGroups.Add(tableGroup18);
            this.tableFailureDetails.ColumnGroups.Add(tableGroup19);
            this.tableFailureDetails.ColumnGroups.Add(tableGroup20);
            this.tableFailureDetails.ColumnGroups.Add(tableGroup21);
            this.tableFailureDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox17,
            this.textBox19,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox16,
            this.textBox18});
            this.tableFailureDetails.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.15999999642372131D), Telerik.Reporting.Drawing.Unit.Inch(0.30000001192092896D));
            this.tableFailureDetails.Name = "tableFailureDetails";
            tableGroup22.Groupings.Add(new Telerik.Reporting.Grouping(""));
            tableGroup22.Name = "DetailGroup";
            this.tableFailureDetails.RowGroups.Add(tableGroup22);
            this.tableFailureDetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5437107086181641D), Telerik.Reporting.Drawing.Unit.Inch(0.37500020861625671D));
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1299891471862793D), Telerik.Reporting.Drawing.Unit.Inch(0.1979169100522995D));
            this.textBox13.Style.Font.Bold = false;
            this.textBox13.Style.Font.Name = "Times New Roman";
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox13.Value = "=Fields.SampleName";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1299891471862793D), Telerik.Reporting.Drawing.Unit.Inch(0.1979169100522995D));
            this.textBox14.Style.Font.Bold = false;
            this.textBox14.Style.Font.Name = "Times New Roman";
            this.textBox14.Value = "=Fields.Reason";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1537435054779053D), Telerik.Reporting.Drawing.Unit.Inch(0.1979169100522995D));
            this.textBox15.Style.Font.Bold = false;
            this.textBox15.Style.Font.Name = "Times New Roman";
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox15.Value = "=Fields.Value";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0649945735931397D), Telerik.Reporting.Drawing.Unit.Inch(0.1979169100522995D));
            this.textBox17.Style.Font.Bold = false;
            this.textBox17.Style.Font.Name = "Times New Roman";
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "=Fields.AcceptanceRangeFrom";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0649939775466919D), Telerik.Reporting.Drawing.Unit.Inch(0.1979169100522995D));
            this.textBox19.Style.Font.Bold = false;
            this.textBox19.Style.Font.Name = "Times New Roman";
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox19.StyleName = "";
            this.textBox19.Value = "=Fields.AcceptanceRangeTo";
            // 
            // textBoxFailureDetailsLabel
            // 
            this.textBoxFailureDetailsLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
            this.textBoxFailureDetailsLabel.Name = "textBoxFailureDetailsLabel";
            this.textBoxFailureDetailsLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0999605655670166D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBoxFailureDetailsLabel.Style.Font.Bold = true;
            this.textBoxFailureDetailsLabel.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBoxFailureDetailsLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxFailureDetailsLabel.Style.Visible = true;
            this.textBoxFailureDetailsLabel.Value = "Failure Details";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.0999608039855957D), Telerik.Reporting.Drawing.Unit.Inch(0.050039451569318771D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3999999761581421D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Font.Name = "Times New Roman";
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.Style.Visible = true;
            this.textBox20.Value = "Acceptance Range";
            // 
            // textBoxStandardsAcceptanceCriteria1
            // 
            this.textBoxStandardsAcceptanceCriteria1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.12992119789123535D));
            this.textBoxStandardsAcceptanceCriteria1.Name = "textBoxStandardsAcceptanceCriteria1";
            this.textBoxStandardsAcceptanceCriteria1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.6999998092651367D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
            this.textBoxStandardsAcceptanceCriteria1.Style.Font.Name = "Times New Roman";
            this.textBoxStandardsAcceptanceCriteria1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBoxStandardsAcceptanceCriteria1.Value = "1)Difference between A & B counts to be within 3.89 x the standard deviation of t" +
    "he combined count and systematic errors";
            // 
            // textBoxStandardsAcceptanceCriteria2
            // 
            this.textBoxStandardsAcceptanceCriteria2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(0.27999988198280334D));
            this.textBoxStandardsAcceptanceCriteria2.Name = "textBoxStandardsAcceptanceCriteria2";
            this.textBoxStandardsAcceptanceCriteria2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.6999602317810059D), Telerik.Reporting.Drawing.Unit.Inch(0.16787755489349365D));
            this.textBoxStandardsAcceptanceCriteria2.Style.Font.Name = "Times New Roman";
            this.textBoxStandardsAcceptanceCriteria2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBoxStandardsAcceptanceCriteria2.Value = "2) the minimum rate for each standard is >= 500 counts per minute";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.059999998658895493D), Telerik.Reporting.Drawing.Unit.Inch(0.50999999046325684D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.84007883071899414D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.Font.Name = "Times New Roman";
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.Value = "Injectate Lot:";
            // 
            // textBoxInjectateLot
            // 
            this.textBoxInjectateLot.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Inch(0.50999999046325684D));
            this.textBoxInjectateLot.Name = "textBoxInjectateLot";
            this.textBoxInjectateLot.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5978381633758545D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBoxInjectateLot.Style.Font.Name = "Times New Roman";
            this.textBoxInjectateLot.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBoxInjectateLot.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxInjectateLot.Value = "";
            // 
            // SubReport_Standards
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SubReport_Standards";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        public Telerik.Reporting.TextBox textBoxStandardsAcceptanceCriteria;
        private Telerik.Reporting.TextBox textBox1;
        public Telerik.Reporting.Table tableStandardsSamples;
        public Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox4;
        public Telerik.Reporting.Panel panelFailureDetails;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        public Telerik.Reporting.TextBox textBoxFailureDetailsLabel;
        public Telerik.Reporting.Table tableFailureDetails;
        public Telerik.Reporting.TextBox textBoxCountTime;
        public Telerik.Reporting.TextBox textBoxStandardsPassFail;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox18;
        public Telerik.Reporting.TextBox textBox20;
        public Telerik.Reporting.TextBox textBoxStandardsAcceptanceCriteria1;
        public Telerik.Reporting.TextBox textBoxStandardsAcceptanceCriteria2;
        public Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.TextBox textBox21;
        public Telerik.Reporting.TextBox textBoxInjectateLot;
    }
}