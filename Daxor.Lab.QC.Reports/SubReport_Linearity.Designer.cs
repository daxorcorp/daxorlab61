namespace Daxor.Lab.QC.Reports
{
    partial class SubReport_Linearity
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBoxLinearityTestPassFail = new Telerik.Reporting.TextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBoxContaminationAcceptanceCriteria = new Telerik.Reporting.TextBox();
            this.panelFailureDetails = new Telerik.Reporting.Panel();
            this.tableFailureDetails = new Telerik.Reporting.Table();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBoxAcceptanceRangeLabel = new Telerik.Reporting.TextBox();
            this.textBoxFailureDetailsLabel = new Telerik.Reporting.TextBox();
            this.tableLinearitySamples = new Telerik.Reporting.Table();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1582248210906982D), Telerik.Reporting.Drawing.Unit.Inch(0.18750001490116119D));
            this.textBox25.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Name = "Times New Roman";
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox25.Value = "ID";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1582248210906982D), Telerik.Reporting.Drawing.Unit.Inch(0.18750001490116119D));
            this.textBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Name = "Times New Roman";
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox26.Value = "Fail Item";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.169037938117981D), Telerik.Reporting.Drawing.Unit.Inch(0.18750001490116119D));
            this.textBox27.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Times New Roman";
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox27.Value = "Value";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0791124105453491D), Telerik.Reporting.Drawing.Unit.Inch(0.18750001490116119D));
            this.textBox28.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.Font.Name = "Times New Roman";
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox28.StyleName = "";
            this.textBox28.Value = "From";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0791118144989014D), Telerik.Reporting.Drawing.Unit.Inch(0.18750001490116119D));
            this.textBox29.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Name = "Times New Roman";
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "To";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0056964159011841D), Telerik.Reporting.Drawing.Unit.Inch(0.33207547664642334D));
            this.textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Times New Roman";
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox2.Value = "Control Sample ID";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.97061455249786377D), Telerik.Reporting.Drawing.Unit.Inch(0.33207547664642334D));
            this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Times New Roman";
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox4.Value = "Nominal Activity (nCi)";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.9229161739349365D), Telerik.Reporting.Drawing.Unit.Inch(0.55007869005203247D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1226388216018677D), Telerik.Reporting.Drawing.Unit.Inch(0.33207547664642334D));
            this.textBox18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Name = "Times New Roman";
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "From";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1226388216018677D), Telerik.Reporting.Drawing.Unit.Inch(0.33207547664642334D));
            this.textBox10.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Times New Roman";
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox10.StyleName = "";
            this.textBox10.Value = "To";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1226388216018677D), Telerik.Reporting.Drawing.Unit.Inch(0.33207547664642334D));
            this.textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Times New Roman";
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox12.StyleName = "";
            this.textBox12.Value = "Measured Rate (cpm)";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1226388216018677D), Telerik.Reporting.Drawing.Unit.Inch(0.33207547664642334D));
            this.textBox14.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Times New Roman";
            this.textBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox14.StyleName = "";
            this.textBox14.Value = "Measured Ratio of Rates";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3331335783004761D), Telerik.Reporting.Drawing.Unit.Inch(0.33207547664642334D));
            this.textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Times New Roman";
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "Pass/Fail";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(2.3000001907348633D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBoxLinearityTestPassFail,
            this.panel1});
            this.detail.Name = "detail";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
            this.textBox1.Style.BackgroundColor = System.Drawing.Color.Black;
            this.textBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.Color = System.Drawing.Color.White;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Linearity";
            // 
            // textBoxLinearityTestPassFail
            // 
            this.textBoxLinearityTestPassFail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.31451416015625D), Telerik.Reporting.Drawing.Unit.Inch(0.21305282413959503D));
            this.textBoxLinearityTestPassFail.Name = "textBoxLinearityTestPassFail";
            this.textBoxLinearityTestPassFail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBoxLinearityTestPassFail.Style.Font.Bold = true;
            this.textBoxLinearityTestPassFail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBoxLinearityTestPassFail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBoxLinearityTestPassFail.Value = "";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxContaminationAcceptanceCriteria,
            this.panelFailureDetails,
            this.tableLinearitySamples,
            this.textBox8});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.43999999761581421D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(1.8500000238418579D));
            this.panel1.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.panel1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel1.Style.Color = System.Drawing.Color.Black;
            this.panel1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            // 
            // textBoxContaminationAcceptanceCriteria
            // 
            this.textBoxContaminationAcceptanceCriteria.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.10204410552978516D));
            this.textBoxContaminationAcceptanceCriteria.Name = "textBoxContaminationAcceptanceCriteria";
            this.textBoxContaminationAcceptanceCriteria.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBoxContaminationAcceptanceCriteria.Style.Font.Name = "Times New Roman";
            this.textBoxContaminationAcceptanceCriteria.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBoxContaminationAcceptanceCriteria.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBoxContaminationAcceptanceCriteria.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBoxContaminationAcceptanceCriteria.Value = "Acceptance Criteria:  Measured ratio of rates to lie within range of Acceptable R" +
    "atio of Rates";
            // 
            // panelFailureDetails
            // 
            this.panelFailureDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.tableFailureDetails,
            this.textBoxAcceptanceRangeLabel,
            this.textBoxFailureDetailsLabel});
            this.panelFailureDetails.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.95992130041122437D));
            this.panelFailureDetails.Name = "panelFailureDetails";
            this.panelFailureDetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7999615669250488D), Telerik.Reporting.Drawing.Unit.Inch(0.75D));
            this.panelFailureDetails.Style.Visible = false;
            // 
            // tableFailureDetails
            // 
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.1582248210906982D)));
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.1582248210906982D)));
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.169037938117981D)));
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0791124105453491D)));
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0791118144989014D)));
            this.tableFailureDetails.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19791682064533234D)));
            this.tableFailureDetails.Body.SetCellContent(0, 0, this.textBox20);
            this.tableFailureDetails.Body.SetCellContent(0, 1, this.textBox21);
            this.tableFailureDetails.Body.SetCellContent(0, 2, this.textBox22);
            this.tableFailureDetails.Body.SetCellContent(0, 3, this.textBox23);
            this.tableFailureDetails.Body.SetCellContent(0, 4, this.textBox24);
            tableGroup1.ReportItem = this.textBox25;
            tableGroup2.ReportItem = this.textBox26;
            tableGroup3.ReportItem = this.textBox27;
            tableGroup4.Name = "Group1";
            tableGroup4.ReportItem = this.textBox28;
            tableGroup5.Name = "Group2";
            tableGroup5.ReportItem = this.textBox29;
            this.tableFailureDetails.ColumnGroups.Add(tableGroup1);
            this.tableFailureDetails.ColumnGroups.Add(tableGroup2);
            this.tableFailureDetails.ColumnGroups.Add(tableGroup3);
            this.tableFailureDetails.ColumnGroups.Add(tableGroup4);
            this.tableFailureDetails.ColumnGroups.Add(tableGroup5);
            this.tableFailureDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29});
            this.tableFailureDetails.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.15625D), Telerik.Reporting.Drawing.Unit.Inch(0.20011822879314423D));
            this.tableFailureDetails.Name = "tableFailureDetails";
            tableGroup6.Groupings.Add(new Telerik.Reporting.Grouping(""));
            tableGroup6.Name = "DetailGroup";
            this.tableFailureDetails.RowGroups.Add(tableGroup6);
            this.tableFailureDetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.643712043762207D), Telerik.Reporting.Drawing.Unit.Inch(0.38541683554649353D));
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1582248210906982D), Telerik.Reporting.Drawing.Unit.Inch(0.19791685044765472D));
            this.textBox20.Style.Font.Bold = false;
            this.textBox20.Style.Font.Name = "Times New Roman";
            this.textBox20.Value = "=Fields.SampleName";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1582248210906982D), Telerik.Reporting.Drawing.Unit.Inch(0.19791685044765472D));
            this.textBox21.Style.Font.Bold = false;
            this.textBox21.Style.Font.Name = "Times New Roman";
            this.textBox21.Value = "=Fields.Reason";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.169037938117981D), Telerik.Reporting.Drawing.Unit.Inch(0.19791685044765472D));
            this.textBox22.Style.Font.Bold = false;
            this.textBox22.Style.Font.Name = "Times New Roman";
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox22.Value = "=Fields.Value";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0791124105453491D), Telerik.Reporting.Drawing.Unit.Inch(0.19791685044765472D));
            this.textBox23.Style.Font.Bold = false;
            this.textBox23.Style.Font.Name = "Times New Roman";
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "=Fields.AcceptanceRangeFrom";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0791118144989014D), Telerik.Reporting.Drawing.Unit.Inch(0.19791685044765472D));
            this.textBox24.Style.Font.Bold = false;
            this.textBox24.Style.Font.Name = "Times New Roman";
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox24.StyleName = "";
            this.textBox24.Value = "=Fields.AcceptanceRangeTo";
            // 
            // textBoxAcceptanceRangeLabel
            // 
            this.textBoxAcceptanceRangeLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.6417374610900879D), Telerik.Reporting.Drawing.Unit.Inch(0.061075370758771896D));
            this.textBoxAcceptanceRangeLabel.Name = "textBoxAcceptanceRangeLabel";
            this.textBoxAcceptanceRangeLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0000007152557373D), Telerik.Reporting.Drawing.Unit.Inch(0.13892476260662079D));
            this.textBoxAcceptanceRangeLabel.Style.Font.Bold = true;
            this.textBoxAcceptanceRangeLabel.Style.Font.Name = "Times New Roman";
            this.textBoxAcceptanceRangeLabel.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxAcceptanceRangeLabel.Value = "Acceptance Range";
            // 
            // textBoxFailureDetailsLabel
            // 
            this.textBoxFailureDetailsLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBoxFailureDetailsLabel.Name = "textBoxFailureDetailsLabel";
            this.textBoxFailureDetailsLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0600001811981201D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBoxFailureDetailsLabel.Style.Font.Bold = true;
            this.textBoxFailureDetailsLabel.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBoxFailureDetailsLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxFailureDetailsLabel.Style.Visible = true;
            this.textBoxFailureDetailsLabel.Value = "Failure Details";
            // 
            // tableLinearitySamples
            // 
            this.tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0056965351104736D)));
            this.tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.97061455249786377D)));
            this.tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1226389408111572D)));
            this.tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1226389408111572D)));
            this.tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1226389408111572D)));
            this.tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1226389408111572D)));
            this.tableLinearitySamples.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.3331335783004761D)));
            this.tableLinearitySamples.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.20750784873962402D)));
            this.tableLinearitySamples.Body.SetCellContent(0, 0, this.textBox5);
            this.tableLinearitySamples.Body.SetCellContent(0, 1, this.textBox7);
            this.tableLinearitySamples.Body.SetCellContent(0, 2, this.textBox9);
            this.tableLinearitySamples.Body.SetCellContent(0, 3, this.textBox11);
            this.tableLinearitySamples.Body.SetCellContent(0, 4, this.textBox13);
            this.tableLinearitySamples.Body.SetCellContent(0, 5, this.textBox15);
            this.tableLinearitySamples.Body.SetCellContent(0, 6, this.textBox17);
            tableGroup7.ReportItem = this.textBox2;
            tableGroup8.ReportItem = this.textBox4;
            tableGroup9.Name = "Group1";
            tableGroup9.ReportItem = this.textBox18;
            tableGroup10.Name = "Group2";
            tableGroup10.ReportItem = this.textBox10;
            tableGroup11.Name = "Group3";
            tableGroup11.ReportItem = this.textBox12;
            tableGroup12.Name = "Group4";
            tableGroup12.ReportItem = this.textBox14;
            tableGroup13.Name = "Group5";
            tableGroup13.ReportItem = this.textBox16;
            this.tableLinearitySamples.ColumnGroups.Add(tableGroup7);
            this.tableLinearitySamples.ColumnGroups.Add(tableGroup8);
            this.tableLinearitySamples.ColumnGroups.Add(tableGroup9);
            this.tableLinearitySamples.ColumnGroups.Add(tableGroup10);
            this.tableLinearitySamples.ColumnGroups.Add(tableGroup11);
            this.tableLinearitySamples.ColumnGroups.Add(tableGroup12);
            this.tableLinearitySamples.ColumnGroups.Add(tableGroup13);
            this.tableLinearitySamples.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5,
            this.textBox7,
            this.textBox9,
            this.textBox11,
            this.textBox13,
            this.textBox15,
            this.textBox17,
            this.textBox2,
            this.textBox4,
            this.textBox18,
            this.textBox10,
            this.textBox12,
            this.textBox14,
            this.textBox16});
            this.tableLinearitySamples.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.30212274193763733D));
            this.tableLinearitySamples.Name = "tableLinearitySamples";
            tableGroup14.Groupings.Add(new Telerik.Reporting.Grouping(""));
            tableGroup14.Name = "DetailGroup";
            this.tableLinearitySamples.RowGroups.Add(tableGroup14);
            this.tableLinearitySamples.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7999997138977051D), Telerik.Reporting.Drawing.Unit.Inch(0.53958332538604736D));
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0056964159011841D), Telerik.Reporting.Drawing.Unit.Inch(0.20750784873962402D));
            this.textBox5.Style.Font.Name = "Times New Roman";
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox5.Value = "=Fields.SampleName";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.97061461210250854D), Telerik.Reporting.Drawing.Unit.Inch(0.20750784873962402D));
            this.textBox7.Style.Font.Name = "Times New Roman";
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Value = "=Fields.NominalActivity";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1226388216018677D), Telerik.Reporting.Drawing.Unit.Inch(0.20750784873962402D));
            this.textBox9.Style.Font.Name = "Times New Roman";
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.StyleName = "";
            this.textBox9.Value = "=Fields.From";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1226388216018677D), Telerik.Reporting.Drawing.Unit.Inch(0.20750784873962402D));
            this.textBox11.Style.Font.Name = "Times New Roman";
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox11.StyleName = "";
            this.textBox11.Value = "=Fields.To";
            // 
            // textBox13
            // 
            this.textBox13.Format = "{0:N0}";
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1226388216018677D), Telerik.Reporting.Drawing.Unit.Inch(0.20750784873962402D));
            this.textBox13.Style.Font.Name = "Times New Roman";
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.StyleName = "";
            this.textBox13.Value = "=Fields.MeasuredRate";
            // 
            // textBox15
            // 
            this.textBox15.Format = "{0:N2}";
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1226388216018677D), Telerik.Reporting.Drawing.Unit.Inch(0.20750784873962402D));
            this.textBox15.Style.Font.Name = "Times New Roman";
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox15.StyleName = "";
            this.textBox15.Value = "=Fields.MeasuredRatioOfRates";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3331335783004761D), Telerik.Reporting.Drawing.Unit.Inch(0.20750784873962402D));
            this.textBox17.Style.Font.Name = "Times New Roman";
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "=Fields.Status";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.3000004291534424D), Telerik.Reporting.Drawing.Unit.Inch(0.10212262719869614D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8000005483627319D), Telerik.Reporting.Drawing.Unit.Inch(0.5437501072883606D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Times New Roman";
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.StyleName = "";
            this.textBox8.Value = "Acceptable Ratio of Rates";
            // 
            // SubReport_Linearity
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SubReport_Linearity";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        public Telerik.Reporting.TextBox textBox8;
        public Telerik.Reporting.Panel panelFailureDetails;
        public Telerik.Reporting.Table tableFailureDetails;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        public Telerik.Reporting.TextBox textBoxAcceptanceRangeLabel;
        public Telerik.Reporting.TextBox textBoxFailureDetailsLabel;
        public Telerik.Reporting.Table tableLinearitySamples;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox4;
        public Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox1;
        public Telerik.Reporting.TextBox textBoxLinearityTestPassFail;
        public Telerik.Reporting.TextBox textBoxContaminationAcceptanceCriteria;
        public Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.Panel panel1;
    }
}