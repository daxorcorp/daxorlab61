﻿using Daxor.Lab.QC.Core;

namespace Daxor.Lab.QC.Reports.Common
{
    public class QcReportHelpers
    {
        public static string GeneratePassFailDetails(QcSample sample)
        {
            if (sample.IsPassed == null)
                return QcReportConstants.NotApplicableLabel;

            if ((bool)sample.IsPassed)
                return QcReportConstants.PassLabel;

            return QcReportConstants.FailLabel;
        }
    }
}
