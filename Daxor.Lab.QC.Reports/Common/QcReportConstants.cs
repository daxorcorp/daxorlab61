﻿namespace Daxor.Lab.QC.Reports.Common
{
    public class QcReportConstants
    {
        public const string TestHeaderPrefix = "DAXOR BVA-100 - ";
        public const string TestHeaderSuffix = " - Analysis Report";
        public const string FullQcTestHeader = TestHeaderPrefix + "Full QC Test" + TestHeaderSuffix;
        public const string ContaminationQcTestHeader = TestHeaderPrefix + "Contamination Only Test" + TestHeaderSuffix;
        public const string StandardsQcTestHeader = TestHeaderPrefix + "Standards Only Test" + TestHeaderSuffix;
        public const string LinearityQcTestHeader = TestHeaderPrefix + "Linearity Test" + TestHeaderSuffix;
        public const string VersionPrefix = "v";
        public const string Qc1SampleName = "QC 1";
        public const string CentroidLabel = "Centroid";
        public const string FwhmLabel = "FWHM";
        public const string ActualCountsLabel = "Actual Counts";
        public const string NotApplicableLabel = "N/A";
        public const string PassLabel = "Pass";
        public const string FailLabel = "Fail";
        public const string Cs137RegionOfInterest = "584 to 740";
        public const string Ba133RegionOfInterest = "308 to 420";
        public const string CumulativeAbortedLabel = "Aborted";
        public const string CumulativePassLabel = "PASS";
        public const string CumulativeFailLabel = "FAIL";
        public const string StandardCountsLabel = "Standard Counts";
        public const string StandardDeviationDifferenceLabel = "Standard Deviation Difference";
        public const string PrintedOnDateFormatString = "Printed: {0}";
        public const string BackgroundAcceptanceCriteriaFormatString = "Acceptance Criteria: Rate to be < {0} cpm";
        public const string CountTimeFormatString = "{0:0.0}-minute count time";
        public const string ContaminationAcceptanceCriteriaFormatString = "Acceptance Criteria: <= {0} times background rate or {1} cpm, whichever is larger";
        public const string TestIncompleteLabel = "Test Incomplete";
        public const string CalibrationAcceptanceCriteriaFormatString = "Acceptance Criteria for QC 1:   1) Centroid to be {0} +/- {1:0.0} keV   2) FWHM to be in the range {2} to {3}%   3) counts > {4}";
        public const string ResolutionAcceptanceCriteriaFormatString = "Acceptance Criteria for QC 2 through QC 4:   1) Centroid to be {0} +/- {1:0.0} keV   2) FWHM to be in the range {2} to {3}%   3) counts > {4}";
        public const string StandardsAcceptanceCriteriaFormatString1 = "1) Difference between A & B counts to be within {0} times the standard deviation of the combined count and systematic errors";
        public const string StandardsAcceptanceCriteriaFormatString2 = "2) The minimum rate for each standard is >= {0} counts per minute";
        public const string Qc2MeasuredRatioOfRates = "1.00";
        public const string RateRatioFailureLabel = "Rate Ratio";
        public const string RoundedToTwoPlacesFormatString = "0.00";
        public const string RoundedToThreePlacesFormatString = "0.000";
        public const double QcRatioMultiplier = 0.22;
    }
}
