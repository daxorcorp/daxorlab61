namespace Daxor.Lab.QC.Reports
{
    partial class SubReport_Background
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBoxBackgroundPassFail = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBoxCountTime = new Telerik.Reporting.TextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBoxBackgroundAcceptanceCriteria = new Telerik.Reporting.TextBox();
            this.shape1 = new Telerik.Reporting.Shape();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBoxBackgroundRate = new Telerik.Reporting.TextBox();
            this.textBoxBackgroundPass = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(1.3375786542892456D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxBackgroundPassFail,
            this.textBox1,
            this.textBoxCountTime,
            this.panel1});
            this.detail.Name = "detail";
            this.detail.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            // 
            // textBoxBackgroundPassFail
            // 
            this.textBoxBackgroundPassFail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.9167060852050781D), Telerik.Reporting.Drawing.Unit.Inch(0.011802911758422852D));
            this.textBoxBackgroundPassFail.Name = "textBoxBackgroundPassFail";
            this.textBoxBackgroundPassFail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88333320617675781D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBoxBackgroundPassFail.Style.Font.Bold = true;
            this.textBoxBackgroundPassFail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBoxBackgroundPassFail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBoxBackgroundPassFail.Value = "";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(3.9378803194267675E-05D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.24166667461395264D));
            this.textBox1.Style.BackgroundColor = System.Drawing.Color.Black;
            this.textBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.Color = System.Drawing.Color.White;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Background";
            // 
            // textBoxCountTime
            // 
            this.textBoxCountTime.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.4375393390655518D), Telerik.Reporting.Drawing.Unit.Inch(0.038916110992431641D));
            this.textBoxCountTime.Name = "textBoxCountTime";
            this.textBoxCountTime.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBoxCountTime.Style.Font.Name = "Times New Roman";
            this.textBoxCountTime.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxCountTime.Value = "1.0 Minute Count Time";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxBackgroundAcceptanceCriteria,
            this.shape1,
            this.textBox2,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBoxBackgroundRate,
            this.textBoxBackgroundPass});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.24178481101989746D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8208332061767578D), Telerik.Reporting.Drawing.Unit.Inch(0.95678502321243286D));
            this.panel1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBoxBackgroundAcceptanceCriteria
            // 
            this.textBoxBackgroundAcceptanceCriteria.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.10420616716146469D));
            this.textBoxBackgroundAcceptanceCriteria.Name = "textBoxBackgroundAcceptanceCriteria";
            this.textBoxBackgroundAcceptanceCriteria.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.14000000059604645D));
            this.textBoxBackgroundAcceptanceCriteria.Style.Font.Name = "Times New Roman";
            this.textBoxBackgroundAcceptanceCriteria.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBoxBackgroundAcceptanceCriteria.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBoxBackgroundAcceptanceCriteria.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBoxBackgroundAcceptanceCriteria.Value = "Acceptance Criteria: Rate to be < 100 cpm";
            // 
            // shape1
            // 
            this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.02083333395421505D), Telerik.Reporting.Drawing.Unit.Inch(0.56503981351852417D));
            this.shape1.Name = "shape1";
            this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7999997138977051D), Telerik.Reporting.Drawing.Unit.Inch(0.079166412353515625D));
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.020872751250863075D), Telerik.Reporting.Drawing.Unit.Inch(0.36496090888977051D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0395833253860474D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox2.Style.Font.Name = "Times New Roman";
            this.textBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox2.Value = "Sample";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.268789529800415D), Telerik.Reporting.Drawing.Unit.Inch(0.24428486824035645D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49999985098838806D), Telerik.Reporting.Drawing.Unit.Inch(0.32067617774009705D));
            this.textBox4.Style.Font.Name = "Times New Roman";
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox4.Value = "Rate (cpm)";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7D), Telerik.Reporting.Drawing.Unit.Inch(0.36496090888977051D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7999998927116394D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox5.Style.Font.Name = "Times New Roman";
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox5.Value = "Pass/Fail";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.020872751250863075D), Telerik.Reporting.Drawing.Unit.Inch(0.64428502321243286D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0395833253860474D), Telerik.Reporting.Drawing.Unit.Inch(0.19999997317790985D));
            this.textBox6.Style.Font.Name = "Times New Roman";
            this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox6.Value = "Background";
            // 
            // textBoxBackgroundRate
            // 
            this.textBoxBackgroundRate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.268789529800415D), Telerik.Reporting.Drawing.Unit.Inch(0.64428502321243286D));
            this.textBoxBackgroundRate.Name = "textBoxBackgroundRate";
            this.textBoxBackgroundRate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.49999967217445374D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxBackgroundRate.Style.Font.Name = "Times New Roman";
            this.textBoxBackgroundRate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxBackgroundRate.Value = "textBox7";
            // 
            // textBoxBackgroundPass
            // 
            this.textBoxBackgroundPass.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.0187497138977051D), Telerik.Reporting.Drawing.Unit.Inch(0.64428502321243286D));
            this.textBoxBackgroundPass.Name = "textBoxBackgroundPass";
            this.textBoxBackgroundPass.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.78125D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
            this.textBoxBackgroundPass.Style.Font.Name = "Times New Roman";
            this.textBoxBackgroundPass.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxBackgroundPass.Value = "textBox7";
            // 
            // SubReport_Background
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SubReport_Background";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.TextBox textBox1;
        public Telerik.Reporting.TextBox textBoxBackgroundAcceptanceCriteria;
        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        public Telerik.Reporting.TextBox textBoxBackgroundRate;
        public Telerik.Reporting.TextBox textBoxBackgroundPass;
        public Telerik.Reporting.TextBox textBoxBackgroundPassFail;
        public Telerik.Reporting.TextBox textBoxCountTime;
        public Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.Panel panel1;
    }
}