﻿namespace Daxor.Lab.QC.Reports.ReportUtilityClasses
{
    public class ResolutionSample : ReportSampleBase
    {
        public string SerialNumber { get; set; }

        public string Centroid { get; set; }

        public string FWHM { get; set; }

        public string ActualCounts { get; set; }

        public string RegionOfInterest { get; set; }

        public int CountLimit { get; set; }
    }
}
