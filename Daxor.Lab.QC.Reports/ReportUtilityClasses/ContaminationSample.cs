﻿namespace Daxor.Lab.QC.Reports.ReportUtilityClasses
{
    public class ContaminationSample : ReportSampleBase
    {
        public string MeasuredRate { get; set; }
    }
}
