﻿namespace Daxor.Lab.QC.Reports.ReportUtilityClasses
{
    public class ReportSampleBase
    {
        public string SampleName { get; set; }

        public int Position { get; set; }

        public string Status { get; set; }
    }
}
