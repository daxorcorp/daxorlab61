﻿namespace Daxor.Lab.QC.Reports.ReportUtilityClasses
{
    public class LinearitySample : ReportSampleBase
    {
        public string NominalActivity { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string MeasuredRate { get; set; }

        public string MeasuredRatioOfRates { get; set; }
    }
}
