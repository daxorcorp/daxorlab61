﻿namespace Daxor.Lab.QC.Reports.ReportUtilityClasses
{
    public class StandardSample : ReportSampleBase
    {
        public string MeasuredRate { get; set; }

        public string ActualCounts { get; set; }

        public string ErrorDetails { get; set; }
    }
}
