﻿namespace Daxor.Lab.QC.Reports.ReportUtilityClasses
{
    public class FailureDetails
    {
        public string SampleName { get; set; }

        public string Reason { get; set; }

        public string Value { get; set; }

        public string AcceptanceRangeFrom { get; set; }

        public string AcceptanceRangeTo { get; set; }
    }
}
