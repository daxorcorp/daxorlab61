using Daxor.Lab.QC.Interfaces;

namespace Daxor.Lab.QC.Reports
{
    partial class SubReport_Contamination
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBoxContaminationPassFail = new Telerik.Reporting.TextBox();
            this.textBoxCountTime = new Telerik.Reporting.TextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBoxContaminationAcceptanceCriteria = new Telerik.Reporting.TextBox();
            this.tableFirstHalf = new Telerik.Reporting.Table();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.tableSecondHalf = new Telerik.Reporting.Table();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.shape1 = new Telerik.Reporting.Shape();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6347066164016724D), Telerik.Reporting.Drawing.Unit.Inch(0.359375D));
            this.textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Times New Roman";
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox2.Value = "Sample Carrier";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69794458150863647D), Telerik.Reporting.Drawing.Unit.Inch(0.359375D));
            this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Times New Roman";
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox4.Value = "Rate (cpm)";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4672698974609375D), Telerik.Reporting.Drawing.Unit.Inch(0.359375D));
            this.textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Times New Roman";
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox8.StyleName = "";
            this.textBox8.Value = "Pass/Fail";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.688313364982605D), Telerik.Reporting.Drawing.Unit.Inch(0.35937497019767761D));
            this.textBox14.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Times New Roman";
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox14.Value = "Sample Carrier";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.71035647392272949D), Telerik.Reporting.Drawing.Unit.Inch(0.35937497019767761D));
            this.textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Times New Roman";
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox16.Value = "Rate (cpm)";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4012514352798462D), Telerik.Reporting.Drawing.Unit.Inch(0.35937497019767761D));
            this.textBox17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Times New Roman";
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "Pass/Fail";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(3.2000000476837158D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBoxContaminationPassFail,
            this.textBoxCountTime,
            this.panel1});
            this.detail.Name = "detail";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.11326345056295395D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
            this.textBox1.Style.BackgroundColor = System.Drawing.Color.Black;
            this.textBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.Color = System.Drawing.Color.White;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Contamination";
            // 
            // textBoxContaminationPassFail
            // 
            this.textBoxContaminationPassFail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3000392913818359D), Telerik.Reporting.Drawing.Unit.Inch(0.12419366836547852D));
            this.textBoxContaminationPassFail.Name = "textBoxContaminationPassFail";
            this.textBoxContaminationPassFail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBoxContaminationPassFail.Style.Font.Bold = true;
            this.textBoxContaminationPassFail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBoxContaminationPassFail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBoxContaminationPassFail.Value = "";
            // 
            // textBoxCountTime
            // 
            this.textBoxCountTime.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.4554252624511719D), Telerik.Reporting.Drawing.Unit.Inch(0.1415201872587204D));
            this.textBoxCountTime.Name = "textBoxCountTime";
            this.textBoxCountTime.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBoxCountTime.Style.Font.Name = "Times New Roman";
            this.textBoxCountTime.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxCountTime.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxCountTime.Value = "1.0 Minute Count Time";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxContaminationAcceptanceCriteria,
            this.tableFirstHalf,
            this.tableSecondHalf,
            this.shape1});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.353342205286026D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(2.75D));
            this.panel1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBoxContaminationAcceptanceCriteria
            // 
            this.textBoxContaminationAcceptanceCriteria.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.089921317994594574D));
            this.textBoxContaminationAcceptanceCriteria.Name = "textBoxContaminationAcceptanceCriteria";
            this.textBoxContaminationAcceptanceCriteria.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBoxContaminationAcceptanceCriteria.Style.Font.Name = "Times New Roman";
            this.textBoxContaminationAcceptanceCriteria.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBoxContaminationAcceptanceCriteria.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBoxContaminationAcceptanceCriteria.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBoxContaminationAcceptanceCriteria.Value = "Acceptance Criteria: <= 2 x background rate or 100 cpm, whichever is larger";
            // 
            // tableFirstHalf
            // 
            this.tableFirstHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.6347068548202515D)));
            this.tableFirstHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.69794458150863647D)));
            this.tableFirstHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4672698974609375D)));
            this.tableFirstHalf.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19270837306976318D)));
            this.tableFirstHalf.Body.SetCellContent(0, 0, this.textBox5);
            this.tableFirstHalf.Body.SetCellContent(0, 1, this.textBox7);
            this.tableFirstHalf.Body.SetCellContent(0, 2, this.textBox9);
            tableGroup1.ReportItem = this.textBox2;
            tableGroup2.ReportItem = this.textBox4;
            tableGroup3.Name = "Group1";
            tableGroup3.ReportItem = this.textBox8;
            this.tableFirstHalf.ColumnGroups.Add(tableGroup1);
            this.tableFirstHalf.ColumnGroups.Add(tableGroup2);
            this.tableFirstHalf.ColumnGroups.Add(tableGroup3);
            this.tableFirstHalf.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5,
            this.textBox7,
            this.textBox9,
            this.textBox2,
            this.textBox4,
            this.textBox8});
            this.tableFirstHalf.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D), Telerik.Reporting.Drawing.Unit.Inch(0.28999999165534973D));
            this.tableFirstHalf.Name = "tableFirstHalf";
            tableGroup4.Groupings.Add(new Telerik.Reporting.Grouping(""));
            tableGroup4.Name = "DetailGroup";
            this.tableFirstHalf.RowGroups.Add(tableGroup4);
            this.tableFirstHalf.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7999210357666016D), Telerik.Reporting.Drawing.Unit.Inch(0.55208337306976318D));
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6347066164016724D), Telerik.Reporting.Drawing.Unit.Inch(0.19270837306976318D));
            this.textBox5.Style.Font.Name = "Times New Roman";
            this.textBox5.Value = "=Fields.SampleName";
            // 
            // textBox7
            // 
            this.textBox7.CanGrow = false;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69794458150863647D), Telerik.Reporting.Drawing.Unit.Inch(0.19270837306976318D));
            this.textBox7.Style.Font.Name = "Times New Roman";
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Value = "=Fields.MeasuredRate";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4672698974609375D), Telerik.Reporting.Drawing.Unit.Inch(0.19270837306976318D));
            this.textBox9.Style.Font.Name = "Times New Roman";
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.StyleName = "";
            this.textBox9.Value = "=Fields.Status";
            // 
            // tableSecondHalf
            // 
            this.tableSecondHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.6883131265640259D)));
            this.tableSecondHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.71035635471343994D)));
            this.tableSecondHalf.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4012514352798462D)));
            this.tableSecondHalf.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.1927083432674408D)));
            this.tableSecondHalf.Body.SetCellContent(0, 0, this.textBox10);
            this.tableSecondHalf.Body.SetCellContent(0, 1, this.textBox12);
            this.tableSecondHalf.Body.SetCellContent(0, 2, this.textBox13);
            tableGroup5.ReportItem = this.textBox14;
            tableGroup6.ReportItem = this.textBox16;
            tableGroup7.Name = "Group1";
            tableGroup7.ReportItem = this.textBox17;
            this.tableSecondHalf.ColumnGroups.Add(tableGroup5);
            this.tableSecondHalf.ColumnGroups.Add(tableGroup6);
            this.tableSecondHalf.ColumnGroups.Add(tableGroup7);
            this.tableSecondHalf.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox10,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox16,
            this.textBox17});
            this.tableSecondHalf.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9500789642333984D), Telerik.Reporting.Drawing.Unit.Inch(0.29162684082984924D));
            this.tableSecondHalf.Name = "tableSecondHalf";
            tableGroup8.Groupings.Add(new Telerik.Reporting.Grouping(""));
            tableGroup8.Name = "DetailGroup";
            this.tableSecondHalf.RowGroups.Add(tableGroup8);
            this.tableSecondHalf.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7999212741851807D), Telerik.Reporting.Drawing.Unit.Inch(0.55208331346511841D));
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.688313364982605D), Telerik.Reporting.Drawing.Unit.Inch(0.192708358168602D));
            this.textBox10.Style.Font.Name = "Times New Roman";
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox10.Value = "=Fields.SampleName";
            // 
            // textBox12
            // 
            this.textBox12.CanGrow = false;
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.71035647392272949D), Telerik.Reporting.Drawing.Unit.Inch(0.192708358168602D));
            this.textBox12.Style.Font.Name = "Times New Roman";
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox12.Value = "=Fields.MeasuredRate";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4012514352798462D), Telerik.Reporting.Drawing.Unit.Inch(0.192708358168602D));
            this.textBox13.Style.Font.Name = "Times New Roman";
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.StyleName = "";
            this.textBox13.Value = "=Fields.Status";
            // 
            // shape1
            // 
            this.shape1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.8499999046325684D), Telerik.Reporting.Drawing.Unit.Inch(0.28999999165534973D));
            this.shape1.Name = "shape1";
            this.shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.NS);
            this.shape1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D), Telerik.Reporting.Drawing.Unit.Inch(2.4600000381469727D));
            // 
            // SubReport_Contamination
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SubReport_Contamination";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.Shape shape1;
        public Telerik.Reporting.TextBox textBoxContaminationPassFail;
        public Telerik.Reporting.TextBox textBoxContaminationAcceptanceCriteria;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox14;
        public Telerik.Reporting.Table tableFirstHalf;
        public Telerik.Reporting.Table tableSecondHalf;
        public Telerik.Reporting.TextBox textBoxCountTime;
        public Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.Panel panel1;
    }
}