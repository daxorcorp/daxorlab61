namespace Daxor.Lab.QC.Reports
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;
    using Telerik.Reporting;
    using Telerik.Reporting.Drawing;

    /// <summary>
    /// Summary description for ControlSourceSetupReport.
    /// </summary>
    public partial class ControlSourceSetupReport : Telerik.Reporting.Report
    {
        public ControlSourceSetupReport()
        {
            //
            // Required for telerik Reporting designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
    }
}