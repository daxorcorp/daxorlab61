namespace Daxor.Lab.QC.Reports
{
    partial class SubReport_Resolution
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBoxResolutionPassFail = new Telerik.Reporting.TextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBox_AcceptanceCriteriaCalibration = new Telerik.Reporting.TextBox();
            this.table_LinearityData = new Telerik.Reporting.Table();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.panelFailureDetails = new Telerik.Reporting.Panel();
            this.tableFailureDetails = new Telerik.Reporting.Table();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBoxFailureDetailsLabel = new Telerik.Reporting.TextBox();
            this.textBoxAcceptanceRangeLabel = new Telerik.Reporting.TextBox();
            this.textBoxResolutionAcceptanceCriteria = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1632946729660034D), Telerik.Reporting.Drawing.Unit.Inch(0.17182533442974091D));
            this.textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Times New Roman";
            this.textBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.029999999329447746D);
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox2.Value = "Control Sample ID";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2646439075469971D), Telerik.Reporting.Drawing.Unit.Inch(0.17182533442974091D));
            this.textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Times New Roman";
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox3.StyleName = "";
            this.textBox3.Value = "ROI (keV)";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1110115051269531D), Telerik.Reporting.Drawing.Unit.Inch(0.17182533442974091D));
            this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Times New Roman";
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox4.Value = "Serial Number";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.87573832273483276D), Telerik.Reporting.Drawing.Unit.Inch(0.17182533442974091D));
            this.textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Times New Roman";
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox8.StyleName = "";
            this.textBox8.Value = "Centroid (keV)";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.875737726688385D), Telerik.Reporting.Drawing.Unit.Inch(0.17182533442974091D));
            this.textBox10.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Times New Roman";
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox10.StyleName = "";
            this.textBox10.Value = "FWHM (%)";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2547872066497803D), Telerik.Reporting.Drawing.Unit.Inch(0.17182533442974091D));
            this.textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Times New Roman";
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox12.StyleName = "";
            this.textBox12.Value = "Measured Counts";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2547872066497803D), Telerik.Reporting.Drawing.Unit.Inch(0.17182533442974091D));
            this.textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Times New Roman";
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "Pass/Fail";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1582245826721191D), Telerik.Reporting.Drawing.Unit.Inch(0.1770833283662796D));
            this.textBox25.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Name = "Times New Roman";
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox25.Value = "ID";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1582245826721191D), Telerik.Reporting.Drawing.Unit.Inch(0.1770833283662796D));
            this.textBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Name = "Times New Roman";
            this.textBox26.Value = "Fail Item";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1690378189086914D), Telerik.Reporting.Drawing.Unit.Inch(0.1770833283662796D));
            this.textBox27.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Times New Roman";
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox27.Value = "Value";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0791122913360596D), Telerik.Reporting.Drawing.Unit.Inch(0.1770833283662796D));
            this.textBox28.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.Font.Name = "Times New Roman";
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.StyleName = "";
            this.textBox28.Value = "From";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0791118144989014D), Telerik.Reporting.Drawing.Unit.Inch(0.1770833283662796D));
            this.textBox29.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Name = "Times New Roman";
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "To";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(2.3999998569488525D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBoxResolutionPassFail,
            this.panel1});
            this.detail.Name = "detail";
            this.detail.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.10000002384185791D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0.23999999463558197D));
            this.textBox1.Style.BackgroundColor = System.Drawing.Color.Black;
            this.textBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.Color = System.Drawing.Color.White;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Resolution and Efficiency";
            // 
            // textBoxResolutionPassFail
            // 
            this.textBoxResolutionPassFail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(0.11093020439147949D));
            this.textBoxResolutionPassFail.Name = "textBoxResolutionPassFail";
            this.textBoxResolutionPassFail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBoxResolutionPassFail.Style.Font.Bold = true;
            this.textBoxResolutionPassFail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBoxResolutionPassFail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBoxResolutionPassFail.Value = "";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox_AcceptanceCriteriaCalibration,
            this.table_LinearityData,
            this.panelFailureDetails,
            this.textBoxResolutionAcceptanceCriteria});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.340078741312027D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(1.8599213361740112D));
            this.panel1.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.panel1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox_AcceptanceCriteriaCalibration
            // 
            this.textBox_AcceptanceCriteriaCalibration.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.093710578978061676D));
            this.textBox_AcceptanceCriteriaCalibration.Name = "textBox_AcceptanceCriteriaCalibration";
            this.textBox_AcceptanceCriteriaCalibration.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox_AcceptanceCriteriaCalibration.Style.Font.Name = "Times New Roman";
            this.textBox_AcceptanceCriteriaCalibration.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox_AcceptanceCriteriaCalibration.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBox_AcceptanceCriteriaCalibration.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox_AcceptanceCriteriaCalibration.Value = "Acceptance Criteria: Rate to be < 100 CPM";
            // 
            // table_LinearityData
            // 
            this.table_LinearityData.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1632944345474243D)));
            this.table_LinearityData.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2646439075469971D)));
            this.table_LinearityData.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1110115051269531D)));
            this.table_LinearityData.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.875738263130188D)));
            this.table_LinearityData.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.875737726688385D)));
            this.table_LinearityData.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2547872066497803D)));
            this.table_LinearityData.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.2547872066497803D)));
            this.table_LinearityData.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.18650807440280914D)));
            this.table_LinearityData.Body.SetCellContent(0, 0, this.textBox5);
            this.table_LinearityData.Body.SetCellContent(0, 2, this.textBox7);
            this.table_LinearityData.Body.SetCellContent(0, 3, this.textBox9);
            this.table_LinearityData.Body.SetCellContent(0, 4, this.textBox11);
            this.table_LinearityData.Body.SetCellContent(0, 5, this.textBox13);
            this.table_LinearityData.Body.SetCellContent(0, 6, this.textBox17);
            this.table_LinearityData.Body.SetCellContent(0, 1, this.textBox6);
            tableGroup1.ReportItem = this.textBox2;
            tableGroup2.Name = "Group6";
            tableGroup2.ReportItem = this.textBox3;
            tableGroup3.ReportItem = this.textBox4;
            tableGroup4.Name = "Group1";
            tableGroup4.ReportItem = this.textBox8;
            tableGroup5.Name = "Group2";
            tableGroup5.ReportItem = this.textBox10;
            tableGroup6.Name = "Group3";
            tableGroup6.ReportItem = this.textBox12;
            tableGroup7.Name = "Group5";
            tableGroup7.ReportItem = this.textBox16;
            this.table_LinearityData.ColumnGroups.Add(tableGroup1);
            this.table_LinearityData.ColumnGroups.Add(tableGroup2);
            this.table_LinearityData.ColumnGroups.Add(tableGroup3);
            this.table_LinearityData.ColumnGroups.Add(tableGroup4);
            this.table_LinearityData.ColumnGroups.Add(tableGroup5);
            this.table_LinearityData.ColumnGroups.Add(tableGroup6);
            this.table_LinearityData.ColumnGroups.Add(tableGroup7);
            this.table_LinearityData.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5,
            this.textBox7,
            this.textBox9,
            this.textBox11,
            this.textBox13,
            this.textBox17,
            this.textBox6,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox8,
            this.textBox10,
            this.textBox12,
            this.textBox16});
            this.table_LinearityData.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.49386802315711975D));
            this.table_LinearityData.Name = "table_LinearityData";
            tableGroup8.Groupings.Add(new Telerik.Reporting.Grouping(""));
            tableGroup8.Name = "DetailGroup";
            this.table_LinearityData.RowGroups.Add(tableGroup8);
            this.table_LinearityData.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000001907348633D), Telerik.Reporting.Drawing.Unit.Inch(0.35833337903022766D));
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1632945537567139D), Telerik.Reporting.Drawing.Unit.Inch(0.18650805950164795D));
            this.textBox5.Style.Font.Name = "Times New Roman";
            this.textBox5.Value = "=Fields.SampleName";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1110115051269531D), Telerik.Reporting.Drawing.Unit.Inch(0.18650805950164795D));
            this.textBox7.Style.Font.Name = "Times New Roman";
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Value = "=Fields.SerialNumber";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.875738263130188D), Telerik.Reporting.Drawing.Unit.Inch(0.18650805950164795D));
            this.textBox9.Style.Font.Name = "Times New Roman";
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.StyleName = "";
            this.textBox9.Value = "=Fields.Centroid";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.875737726688385D), Telerik.Reporting.Drawing.Unit.Inch(0.18650805950164795D));
            this.textBox11.Style.Font.Name = "Times New Roman";
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox11.StyleName = "";
            this.textBox11.Value = "=Fields.FWHM";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2547872066497803D), Telerik.Reporting.Drawing.Unit.Inch(0.18650805950164795D));
            this.textBox13.Style.Font.Name = "Times New Roman";
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.StyleName = "";
            this.textBox13.Value = "=Fields.ActualCounts";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2547872066497803D), Telerik.Reporting.Drawing.Unit.Inch(0.18650805950164795D));
            this.textBox17.Style.Font.Name = "Times New Roman";
            this.textBox17.StyleName = "";
            this.textBox17.Value = "=Fields.Status";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2646439075469971D), Telerik.Reporting.Drawing.Unit.Inch(0.18650805950164795D));
            this.textBox6.Style.Font.Name = "Times New Roman";
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox6.StyleName = "";
            this.textBox6.Value = "=Fields.RegionOfInterest";
            // 
            // panelFailureDetails
            // 
            this.panelFailureDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.tableFailureDetails,
            this.textBoxFailureDetailsLabel,
            this.textBoxAcceptanceRangeLabel});
            this.panelFailureDetails.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9339065551757812E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.94595140218734741D));
            this.panelFailureDetails.Name = "panelFailureDetails";
            this.panelFailureDetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.7999610900878906D), Telerik.Reporting.Drawing.Unit.Inch(0.699999988079071D));
            this.panelFailureDetails.Style.Visible = false;
            // 
            // tableFailureDetails
            // 
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.1582248210906982D)));
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.1582248210906982D)));
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.1690378189086914D)));
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0791124105453491D)));
            this.tableFailureDetails.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0791118144989014D)));
            this.tableFailureDetails.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.18750019371509552D)));
            this.tableFailureDetails.Body.SetCellContent(0, 0, this.textBox20);
            this.tableFailureDetails.Body.SetCellContent(0, 1, this.textBox21);
            this.tableFailureDetails.Body.SetCellContent(0, 2, this.textBox22);
            this.tableFailureDetails.Body.SetCellContent(0, 3, this.textBox23);
            this.tableFailureDetails.Body.SetCellContent(0, 4, this.textBox24);
            tableGroup9.ReportItem = this.textBox25;
            tableGroup10.ReportItem = this.textBox26;
            tableGroup11.ReportItem = this.textBox27;
            tableGroup12.Name = "Group1";
            tableGroup12.ReportItem = this.textBox28;
            tableGroup13.Name = "Group2";
            tableGroup13.ReportItem = this.textBox29;
            this.tableFailureDetails.ColumnGroups.Add(tableGroup9);
            this.tableFailureDetails.ColumnGroups.Add(tableGroup10);
            this.tableFailureDetails.ColumnGroups.Add(tableGroup11);
            this.tableFailureDetails.ColumnGroups.Add(tableGroup12);
            this.tableFailureDetails.ColumnGroups.Add(tableGroup13);
            this.tableFailureDetails.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29});
            this.tableFailureDetails.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.15625D), Telerik.Reporting.Drawing.Unit.Inch(0.20011822879314423D));
            this.tableFailureDetails.Name = "tableFailureDetails";
            tableGroup14.Groupings.Add(new Telerik.Reporting.Grouping(""));
            tableGroup14.Name = "DetailGroup";
            this.tableFailureDetails.RowGroups.Add(tableGroup14);
            this.tableFailureDetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.643712043762207D), Telerik.Reporting.Drawing.Unit.Inch(0.36458352208137512D));
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1582248210906982D), Telerik.Reporting.Drawing.Unit.Inch(0.18750019371509552D));
            this.textBox20.Style.Font.Bold = false;
            this.textBox20.Style.Font.Name = "Times New Roman";
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox20.Value = "=Fields.SampleName";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1582248210906982D), Telerik.Reporting.Drawing.Unit.Inch(0.18750019371509552D));
            this.textBox21.Style.Font.Bold = false;
            this.textBox21.Style.Font.Name = "Times New Roman";
            this.textBox21.Value = "=Fields.Reason";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1690378189086914D), Telerik.Reporting.Drawing.Unit.Inch(0.18750019371509552D));
            this.textBox22.Style.Font.Bold = false;
            this.textBox22.Style.Font.Name = "Times New Roman";
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox22.Value = "=Fields.Value";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0791124105453491D), Telerik.Reporting.Drawing.Unit.Inch(0.18750019371509552D));
            this.textBox23.Style.Font.Bold = false;
            this.textBox23.Style.Font.Name = "Times New Roman";
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "=Fields.AcceptanceRangeFrom";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0791118144989014D), Telerik.Reporting.Drawing.Unit.Inch(0.18750019371509552D));
            this.textBox24.Style.Font.Bold = false;
            this.textBox24.Style.Font.Name = "Times New Roman";
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox24.StyleName = "";
            this.textBox24.Value = "=Fields.AcceptanceRangeTo";
            // 
            // textBoxFailureDetailsLabel
            // 
            this.textBoxFailureDetailsLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBoxFailureDetailsLabel.Name = "textBoxFailureDetailsLabel";
            this.textBoxFailureDetailsLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0999606847763062D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBoxFailureDetailsLabel.Style.Font.Bold = true;
            this.textBoxFailureDetailsLabel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBoxFailureDetailsLabel.Style.Visible = true;
            this.textBoxFailureDetailsLabel.Value = "Failure Details";
            // 
            // textBoxAcceptanceRangeLabel
            // 
            this.textBoxAcceptanceRangeLabel.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.64173698425293D), Telerik.Reporting.Drawing.Unit.Inch(0.050039608031511307D));
            this.textBoxAcceptanceRangeLabel.Name = "textBoxAcceptanceRangeLabel";
            this.textBoxAcceptanceRangeLabel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2D), Telerik.Reporting.Drawing.Unit.Inch(0.15000000596046448D));
            this.textBoxAcceptanceRangeLabel.Style.Font.Bold = true;
            this.textBoxAcceptanceRangeLabel.Style.Font.Name = "Times New Roman";
            this.textBoxAcceptanceRangeLabel.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBoxAcceptanceRangeLabel.Value = "Acceptance Range";
            // 
            // textBoxResolutionAcceptanceCriteria
            // 
            this.textBoxResolutionAcceptanceCriteria.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.29378929734230042D));
            this.textBoxResolutionAcceptanceCriteria.Name = "textBoxResolutionAcceptanceCriteria";
            this.textBoxResolutionAcceptanceCriteria.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBoxResolutionAcceptanceCriteria.Style.Font.Name = "Times New Roman";
            this.textBoxResolutionAcceptanceCriteria.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBoxResolutionAcceptanceCriteria.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
            this.textBoxResolutionAcceptanceCriteria.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBoxResolutionAcceptanceCriteria.Value = "Acceptance Criteria: Rate to be < 100 CPM";
            // 
            // SubReport_Resolution
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SubReport_Resolution";
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            this.Style.BackgroundColor = System.Drawing.Color.White;
            this.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(8D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        public Telerik.Reporting.TextBox textBoxResolutionAcceptanceCriteria;
        private Telerik.Reporting.TextBox textBox1;
        public Telerik.Reporting.TextBox textBoxResolutionPassFail;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox16;
        public Telerik.Reporting.Table table_LinearityData;
        public Telerik.Reporting.Panel panelFailureDetails;
        public Telerik.Reporting.Table tableFailureDetails;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        public Telerik.Reporting.TextBox textBoxAcceptanceRangeLabel;
        public Telerik.Reporting.TextBox textBoxFailureDetailsLabel;
        public Telerik.Reporting.TextBox textBox_AcceptanceCriteriaCalibration;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox3;
        public Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.Panel panel1;
    }
}