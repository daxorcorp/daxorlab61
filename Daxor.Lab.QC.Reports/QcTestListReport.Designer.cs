namespace Daxor.Lab.QC.Reports
{
    partial class QcTestListReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
			this.textBoxTestHeader = new Telerik.Reporting.TextBox();
			this.textBoxHospitalName = new Telerik.Reporting.TextBox();
			this.textBoxDepartmentAddress = new Telerik.Reporting.TextBox();
			this.textBoxDepartmentPhoneNumber = new Telerik.Reporting.TextBox();
			this.textBoxDepartment = new Telerik.Reporting.TextBox();
			this.textBoxDepartmentDirector = new Telerik.Reporting.TextBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.table1 = new Telerik.Reporting.Table();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.textBoxPrintedOn = new Telerik.Reporting.TextBox();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBoxUnitID = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBoxBVAVersion = new Telerik.Reporting.TextBox();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.subReport1 = new Telerik.Reporting.SubReport();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// textBox1
			// 
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1041669845581055D), Telerik.Reporting.Drawing.Unit.Inch(0.2291666716337204D));
			this.textBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox1.Style.Font.Bold = true;
			this.textBox1.Style.Font.Name = "Times New Roman";
			this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox1.Value = "Analyst";
			// 
			// textBox2
			// 
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3750002384185791D), Telerik.Reporting.Drawing.Unit.Inch(0.2291666716337204D));
			this.textBox2.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox2.Style.Font.Bold = true;
			this.textBox2.Style.Font.Name = "Times New Roman";
			this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox2.Value = "Result";
			// 
			// textBox7
			// 
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2708358764648438D), Telerik.Reporting.Drawing.Unit.Inch(0.2291666716337204D));
			this.textBox7.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox7.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox7.Style.Font.Bold = true;
			this.textBox7.Style.Font.Name = "Times New Roman";
			this.textBox7.StyleName = "";
			this.textBox7.Value = "Test Type";
			// 
			// textBox9
			// 
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1458346843719482D), Telerik.Reporting.Drawing.Unit.Inch(0.2291666716337204D));
			this.textBox9.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox9.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox9.Style.Font.Bold = true;
			this.textBox9.Style.Font.Name = "Times New Roman";
			this.textBox9.StyleName = "";
			this.textBox9.Value = "Date";
			// 
			// textBox3
			// 
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0937501192092896D), Telerik.Reporting.Drawing.Unit.Inch(0.2291666716337204D));
			this.textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox3.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox3.Style.Font.Bold = true;
			this.textBox3.Style.Font.Name = "Times New Roman";
			this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox3.Value = "Status";
			// 
			// pageHeaderSection1
			// 
			this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1.8000000715255737D);
			this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxTestHeader,
            this.textBoxHospitalName,
            this.textBoxDepartmentAddress,
            this.textBoxDepartmentPhoneNumber,
            this.textBoxDepartment,
            this.textBoxDepartmentDirector});
			this.pageHeaderSection1.Name = "pageHeaderSection1";
			// 
			// textBoxTestHeader
			// 
			this.textBoxTestHeader.KeepTogether = true;
			this.textBoxTestHeader.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.29791674017906189D));
			this.textBoxTestHeader.Name = "textBoxTestHeader";
			this.textBoxTestHeader.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
			this.textBoxTestHeader.Style.Font.Bold = true;
			this.textBoxTestHeader.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
			this.textBoxTestHeader.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxTestHeader.Value = "Quality Control Test - Test List Report";
			// 
			// textBoxHospitalName
			// 
			this.textBoxHospitalName.KeepTogether = true;
			this.textBoxHospitalName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.60000008344650269D));
			this.textBoxHospitalName.Name = "textBoxHospitalName";
			this.textBoxHospitalName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.1999211311340332D));
			this.textBoxHospitalName.Style.Font.Bold = true;
			this.textBoxHospitalName.Style.Font.Name = "Times New Roman";
			this.textBoxHospitalName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBoxHospitalName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxHospitalName.Value = "Hospital Name";
			// 
			// textBoxDepartmentAddress
			// 
			this.textBoxDepartmentAddress.KeepTogether = true;
			this.textBoxDepartmentAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.80833339691162109D));
			this.textBoxDepartmentAddress.Name = "textBoxDepartmentAddress";
			this.textBoxDepartmentAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.19575436413288117D));
			this.textBoxDepartmentAddress.Style.Font.Bold = true;
			this.textBoxDepartmentAddress.Style.Font.Name = "Times New Roman";
			this.textBoxDepartmentAddress.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBoxDepartmentAddress.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxDepartmentAddress.Value = "Department Address";
			// 
			// textBoxDepartmentPhoneNumber
			// 
			this.textBoxDepartmentPhoneNumber.KeepTogether = true;
			this.textBoxDepartmentPhoneNumber.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.99583339691162109D));
			this.textBoxDepartmentPhoneNumber.Name = "textBoxDepartmentPhoneNumber";
			this.textBoxDepartmentPhoneNumber.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.2041667252779007D));
			this.textBoxDepartmentPhoneNumber.Style.Font.Bold = true;
			this.textBoxDepartmentPhoneNumber.Style.Font.Name = "Times New Roman";
			this.textBoxDepartmentPhoneNumber.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBoxDepartmentPhoneNumber.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxDepartmentPhoneNumber.Value = "Department Phone Number";
			// 
			// textBoxDepartment
			// 
			this.textBoxDepartment.KeepTogether = true;
			this.textBoxDepartment.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.1937500238418579D));
			this.textBoxDepartment.Name = "textBoxDepartment";
			this.textBoxDepartment.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.2041667252779007D));
			this.textBoxDepartment.Style.Font.Bold = true;
			this.textBoxDepartment.Style.Font.Name = "Times New Roman";
			this.textBoxDepartment.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBoxDepartment.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxDepartment.Value = "Department";
			// 
			// textBoxDepartmentDirector
			// 
			this.textBoxDepartmentDirector.KeepTogether = true;
			this.textBoxDepartmentDirector.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.4020833969116211D));
			this.textBoxDepartmentDirector.Name = "textBoxDepartmentDirector";
			this.textBoxDepartmentDirector.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.2041667252779007D));
			this.textBoxDepartmentDirector.Style.Font.Bold = true;
			this.textBoxDepartmentDirector.Style.Font.Name = "Times New Roman";
			this.textBoxDepartmentDirector.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBoxDepartmentDirector.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxDepartmentDirector.Value = "Department Director";
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(2.7999999523162842D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.subReport1});
			this.detail.Name = "detail";
			// 
			// table1
			// 
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.104168176651001D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.3750003576278687D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.2708358764648438D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.1458353996276855D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0937502384185791D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.27083349227905273D)));
			this.table1.Body.SetCellContent(0, 0, this.textBox4);
			this.table1.Body.SetCellContent(0, 1, this.textBox5);
			this.table1.Body.SetCellContent(0, 4, this.textBox6);
			this.table1.Body.SetCellContent(0, 2, this.textBox8);
			this.table1.Body.SetCellContent(0, 3, this.textBox10);
			tableGroup13.ReportItem = this.textBox1;
			tableGroup14.ReportItem = this.textBox2;
			tableGroup15.Name = "Group1";
			tableGroup15.ReportItem = this.textBox7;
			tableGroup16.Name = "Group2";
			tableGroup16.ReportItem = this.textBox9;
			tableGroup17.ReportItem = this.textBox3;
			this.table1.ColumnGroups.Add(tableGroup13);
			this.table1.ColumnGroups.Add(tableGroup14);
			this.table1.ColumnGroups.Add(tableGroup15);
			this.table1.ColumnGroups.Add(tableGroup16);
			this.table1.ColumnGroups.Add(tableGroup17);
			this.table1.ColumnHeadersPrintOnEveryPage = true;
			this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox8,
            this.textBox10,
            this.textBox1,
            this.textBox2,
            this.textBox7,
            this.textBox9,
            this.textBox3});
			this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.table1.Name = "table1";
			tableGroup18.Groupings.Add(new Telerik.Reporting.Grouping(""));
			tableGroup18.Name = "DetailGroup";
			this.table1.RowGroups.Add(tableGroup18);
			this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9895896911621094D), Telerik.Reporting.Drawing.Unit.Inch(0.50000017881393433D));
			// 
			// textBox4
			// 
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.104168176651001D), Telerik.Reporting.Drawing.Unit.Inch(0.27083349227905273D));
			this.textBox4.Style.Font.Name = "Times New Roman";
			this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox4.Value = "=Fields.Analyst";
			// 
			// textBox5
			// 
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3750003576278687D), Telerik.Reporting.Drawing.Unit.Inch(0.27083349227905273D));
			this.textBox5.Style.Font.Name = "Times New Roman";
			this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox5.Value = "=Fields.Result";
			// 
			// textBox6
			// 
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0937502384185791D), Telerik.Reporting.Drawing.Unit.Inch(0.27083349227905273D));
			this.textBox6.Style.Font.Name = "Times New Roman";
			this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox6.Value = "=Fields.Status";
			// 
			// textBox8
			// 
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2708358764648438D), Telerik.Reporting.Drawing.Unit.Inch(0.27083349227905273D));
			this.textBox8.Style.Font.Name = "Times New Roman";
			this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox8.StyleName = "";
			this.textBox8.Value = "=Fields.Type";
			// 
			// textBox10
			// 
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1458356380462646D), Telerik.Reporting.Drawing.Unit.Inch(0.27083349227905273D));
			this.textBox10.Style.Font.Name = "Times New Roman";
			this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox10.StyleName = "";
			this.textBox10.Value = "=Fields.TestDate";
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.60000038146972656D);
			this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxPrintedOn,
            this.textBox21,
            this.textBoxUnitID,
            this.textBox20,
            this.textBoxBVAVersion,
            this.textBox17});
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// textBoxPrintedOn
			// 
			this.textBoxPrintedOn.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.1457552909851074D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBoxPrintedOn.Name = "textBoxPrintedOn";
			this.textBoxPrintedOn.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9541667699813843D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxPrintedOn.Style.Font.Name = "Times New Roman";
			this.textBoxPrintedOn.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBoxPrintedOn.Value = "textBoxDateTimePrinted";
			// 
			// textBox21
			// 
			this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.1000003814697266D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79567527770996094D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox21.Style.Font.Name = "Times New Roman";
			this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox21.Value = "=\'Page \' + PageNumber + \' of \' + PageCount";
			// 
			// textBoxUnitID
			// 
			this.textBoxUnitID.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.2977592945098877D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBoxUnitID.Name = "textBoxUnitID";
			this.textBoxUnitID.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.847916841506958D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxUnitID.Style.Font.Name = "Times New Roman";
			this.textBoxUnitID.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBoxUnitID.Value = "-";
			// 
			// textBox20
			// 
			this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7560926675796509D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.54158782958984375D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox20.Style.Font.Name = "Times New Roman";
			this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox20.Value = "Unit ID:";
			// 
			// textBoxBVAVersion
			// 
			this.textBoxBVAVersion.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.64382869005203247D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBoxBVAVersion.Name = "textBoxBVAVersion";
			this.textBoxBVAVersion.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.85609245300292969D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxBVAVersion.Style.Font.Name = "Times New Roman";
			this.textBoxBVAVersion.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBoxBVAVersion.Value = "v1.2.0.0";
			// 
			// textBox17
			// 
			this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9378803194267675E-05D), Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D));
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.64371061325073242D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox17.Style.Font.Name = "Times New Roman";
			this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox17.Value = "BVA-100:";
			// 
			// subReport1
			// 
			this.subReport1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(0.699999988079071D));
			this.subReport1.Name = "subReport1";
			this.subReport1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9895501136779785D), Telerik.Reporting.Drawing.Unit.Inch(0.29992103576660156D));
			// 
			// QcTestListReport
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
			this.Name = "QcTestListReport";
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.25D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

		private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.PageFooterSection pageFooterSection1;
        public Telerik.Reporting.TextBox textBoxTestHeader;
        public Telerik.Reporting.TextBox textBoxHospitalName;
        public Telerik.Reporting.TextBox textBoxDepartmentAddress;
        public Telerik.Reporting.TextBox textBoxDepartmentPhoneNumber;
        public Telerik.Reporting.TextBox textBoxDepartment;
        public Telerik.Reporting.TextBox textBoxDepartmentDirector;
        public Telerik.Reporting.TextBox textBoxPrintedOn;
        private Telerik.Reporting.TextBox textBox21;
        public Telerik.Reporting.TextBox textBoxUnitID;
        private Telerik.Reporting.TextBox textBox20;
        public Telerik.Reporting.TextBox textBoxBVAVersion;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox3;
		public Telerik.Reporting.Table table1;
		private Telerik.Reporting.DetailSection detail;
		public Telerik.Reporting.SubReport subReport1;
    }
}