namespace Daxor.Lab.QC.Reports
{
    partial class SubReport_Signature
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            detail = new Telerik.Reporting.DetailSection();
            shape1 = new Telerik.Reporting.Shape();
            textBox11 = new Telerik.Reporting.TextBox();
            shape3 = new Telerik.Reporting.Shape();
            textBox13 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            detail.Height = new Telerik.Reporting.Drawing.Unit(0.31253936886787415D, Telerik.Reporting.Drawing.UnitType.Inch);
            detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            shape1,
            textBox11,
            shape3,
            textBox13});
            detail.Name = "detail";
            // 
            // shape1
            // 
            shape1.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(1.2000001668930054D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9378803194267675E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape1.Name = "shape1";
            shape1.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            shape1.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(2.0104167461395264D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3125D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox11
            // 
            textBox11.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(0.450000137090683D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9378803194267675E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox11.Name = "textBox11";
            textBox11.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.74791669845581055D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3125D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox11.Style.Font.Name = "Times New Roman";
            textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            textBox11.Value = "Reviewed:";
            // 
            // shape3
            // 
            shape3.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.8666667938232422D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9378803194267675E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
            shape3.Name = "shape3";
            shape3.ShapeType = new Telerik.Reporting.Drawing.Shapes.LineShape(Telerik.Reporting.Drawing.Shapes.LineDirection.EW);
            shape3.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(1.6353384256362915D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3125D, Telerik.Reporting.Drawing.UnitType.Inch));
            // 
            // textBox13
            // 
            textBox13.Location = new Telerik.Reporting.Drawing.PointU(new Telerik.Reporting.Drawing.Unit(3.4708335399627686D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(3.9378803194267675E-05D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox13.Name = "textBox13";
            textBox13.Size = new Telerik.Reporting.Drawing.SizeU(new Telerik.Reporting.Drawing.Unit(0.39375004172325134D, Telerik.Reporting.Drawing.UnitType.Inch), new Telerik.Reporting.Drawing.Unit(0.3125D, Telerik.Reporting.Drawing.UnitType.Inch));
            textBox13.Style.Font.Name = "Times New Roman";
            textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            textBox13.Value = "Date:";
            // 
            // SubReport_Signature
            // 
            Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            detail});
            PageSettings.Landscape = false;
            PageSettings.Margins.Bottom = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Left = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Right = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.Margins.Top = new Telerik.Reporting.Drawing.Unit(0D, Telerik.Reporting.Drawing.UnitType.Inch);
            PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
            Style.BackgroundColor = System.Drawing.Color.White;
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.Shape shape1;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.Shape shape3;
        private Telerik.Reporting.TextBox textBox13;
        public Telerik.Reporting.DetailSection detail;
    }
}