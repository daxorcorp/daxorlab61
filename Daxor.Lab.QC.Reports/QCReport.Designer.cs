namespace Daxor.Lab.QC.Reports
{
    partial class QCReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
			this.textBoxVoltage = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.textBoxEndFineGain = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBoxAnalyst = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBoxStartFineGain = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBoxComment = new Telerik.Reporting.TextBox();
			this.textBoxTestDate = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBoxDepartmentDirector = new Telerik.Reporting.TextBox();
			this.textBoxDepartment = new Telerik.Reporting.TextBox();
			this.textBoxDepartmentPhoneNumber = new Telerik.Reporting.TextBox();
			this.textBoxDepartmentAddress = new Telerik.Reporting.TextBox();
			this.textBoxHospitalName = new Telerik.Reporting.TextBox();
			this.textBoxTestHeader = new Telerik.Reporting.TextBox();
			this.textBoxCumulativePassFail = new Telerik.Reporting.TextBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBoxPageCount = new Telerik.Reporting.TextBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.pageFooterSection1 = new Telerik.Reporting.PageFooterSection();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBoxBVAVersion = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBoxUnitID = new Telerik.Reporting.TextBox();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBoxPrintedOn = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// pageHeaderSection1
			// 
			this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(2.8000001907348633D);
			this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBoxVoltage,
            this.textBox10,
            this.textBoxEndFineGain,
            this.textBox4,
            this.textBox9,
            this.textBoxAnalyst,
            this.textBox8,
            this.textBoxStartFineGain,
            this.textBox6,
            this.textBoxComment,
            this.textBoxTestDate,
            this.textBox3,
            this.textBoxDepartmentDirector,
            this.textBoxDepartment,
            this.textBoxDepartmentPhoneNumber,
            this.textBoxDepartmentAddress,
            this.textBoxHospitalName,
            this.textBoxTestHeader,
            this.textBoxCumulativePassFail,
            this.textBox1,
            this.textBoxPageCount});
			this.pageHeaderSection1.Name = "pageHeaderSection1";
			// 
			// textBoxVoltage
			// 
			this.textBoxVoltage.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.39166784286499D), Telerik.Reporting.Drawing.Unit.Inch(2.1479957103729248D));
			this.textBoxVoltage.Name = "textBoxVoltage";
			this.textBoxVoltage.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70000028610229492D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBoxVoltage.Style.Font.Name = "Times New Roman";
			this.textBoxVoltage.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxVoltage.Value = "";
			// 
			// textBox10
			// 
			this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.4915890693664551D), Telerik.Reporting.Drawing.Unit.Inch(2.1479957103729248D));
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.89999979734420776D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox10.Style.Font.Bold = true;
			this.textBox10.Style.Font.Name = "Times New Roman";
			this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox10.Value = "High Voltage:";
			// 
			// textBoxEndFineGain
			// 
			this.textBoxEndFineGain.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2000007629394531D), Telerik.Reporting.Drawing.Unit.Inch(2.1479957103729248D));
			this.textBoxEndFineGain.Name = "textBoxEndFineGain";
			this.textBoxEndFineGain.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70000028610229492D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBoxEndFineGain.Style.Font.Name = "Times New Roman";
			this.textBoxEndFineGain.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxEndFineGain.Value = "";
			// 
			// textBox4
			// 
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.200000524520874D), Telerik.Reporting.Drawing.Unit.Inch(2.1479957103729248D));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.99992161989212036D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox4.Style.Font.Bold = true;
			this.textBox4.Style.Font.Name = "Times New Roman";
			this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox4.Value = "End Fine Gain:";
			// 
			// textBox9
			// 
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.8000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(1.9479166269302368D));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59158849716186523D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox9.Style.Font.Bold = true;
			this.textBox9.Style.Font.Name = "Times New Roman";
			this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox9.Value = "Analyst:";
			// 
			// textBoxAnalyst
			// 
			this.textBoxAnalyst.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.39166784286499D), Telerik.Reporting.Drawing.Unit.Inch(1.9479166269302368D));
			this.textBoxAnalyst.Name = "textBoxAnalyst";
			this.textBoxAnalyst.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999211072921753D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBoxAnalyst.Style.Font.Name = "Times New Roman";
			this.textBoxAnalyst.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxAnalyst.Value = "";
			// 
			// textBox8
			// 
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.9418537198798731E-05D), Telerik.Reporting.Drawing.Unit.Inch(2.1436710357666016D));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5998822450637817D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox8.Style.Font.Bold = true;
			this.textBox8.Style.Font.Name = "Times New Roman";
			this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox8.Value = "Start Fine Gain:";
			// 
			// textBoxStartFineGain
			// 
			this.textBoxStartFineGain.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6000005006790161D), Telerik.Reporting.Drawing.Unit.Inch(2.1479957103729248D));
			this.textBoxStartFineGain.Name = "textBoxStartFineGain";
			this.textBoxStartFineGain.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.70000028610229492D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBoxStartFineGain.Style.Font.Name = "Times New Roman";
			this.textBoxStartFineGain.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxStartFineGain.Value = "";
			// 
			// textBox6
			// 
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(2.34375D));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0917057991027832D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox6.Style.Font.Bold = true;
			this.textBox6.Style.Font.Name = "Times New Roman";
			this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox6.Value = "Comment:";
			// 
			// textBoxComment
			// 
			this.textBoxComment.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.0917845964431763D), Telerik.Reporting.Drawing.Unit.Inch(2.34375D));
			this.textBoxComment.Name = "textBoxComment";
			this.textBoxComment.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.899803638458252D), Telerik.Reporting.Drawing.Unit.Inch(0.39996051788330078D));
			this.textBoxComment.Style.Font.Name = "Times New Roman";
			this.textBoxComment.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxComment.Value = "";
			// 
			// textBoxTestDate
			// 
			this.textBoxTestDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6000005006790161D), Telerik.Reporting.Drawing.Unit.Inch(1.9479166269302368D));
			this.textBoxTestDate.Name = "textBoxTestDate";
			this.textBoxTestDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999211072921753D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBoxTestDate.Style.Font.Name = "Times New Roman";
			this.textBoxTestDate.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0.05000000074505806D);
			this.textBoxTestDate.Value = "";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.9479166269302368D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5999217033386231D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
			this.textBox3.Style.Font.Bold = true;
			this.textBox3.Style.Font.Name = "Times New Roman";
			this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox3.Value = "Test Completed Date:";
			// 
			// textBoxDepartmentDirector
			// 
			this.textBoxDepartmentDirector.KeepTogether = true;
			this.textBoxDepartmentDirector.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.6458333730697632D));
			this.textBoxDepartmentDirector.Name = "textBoxDepartmentDirector";
			this.textBoxDepartmentDirector.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.2041667252779007D));
			this.textBoxDepartmentDirector.Style.Font.Bold = false;
			this.textBoxDepartmentDirector.Style.Font.Name = "Times New Roman";
			this.textBoxDepartmentDirector.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBoxDepartmentDirector.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxDepartmentDirector.Value = "Department Director";
			// 
			// textBoxDepartment
			// 
			this.textBoxDepartment.KeepTogether = true;
			this.textBoxDepartment.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.4479166269302368D));
			this.textBoxDepartment.Name = "textBoxDepartment";
			this.textBoxDepartment.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.2041667252779007D));
			this.textBoxDepartment.Style.Font.Bold = false;
			this.textBoxDepartment.Style.Font.Name = "Times New Roman";
			this.textBoxDepartment.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBoxDepartment.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxDepartment.Value = "Department";
			// 
			// textBoxDepartmentPhoneNumber
			// 
			this.textBoxDepartmentPhoneNumber.KeepTogether = true;
			this.textBoxDepartmentPhoneNumber.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.2395833730697632D));
			this.textBoxDepartmentPhoneNumber.Name = "textBoxDepartmentPhoneNumber";
			this.textBoxDepartmentPhoneNumber.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.2041667252779007D));
			this.textBoxDepartmentPhoneNumber.Style.Font.Bold = false;
			this.textBoxDepartmentPhoneNumber.Style.Font.Name = "Times New Roman";
			this.textBoxDepartmentPhoneNumber.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBoxDepartmentPhoneNumber.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxDepartmentPhoneNumber.Value = "Department Phone Number";
			// 
			// textBoxDepartmentAddress
			// 
			this.textBoxDepartmentAddress.KeepTogether = true;
			this.textBoxDepartmentAddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.0520833730697632D));
			this.textBoxDepartmentAddress.Name = "textBoxDepartmentAddress";
			this.textBoxDepartmentAddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.19575436413288117D));
			this.textBoxDepartmentAddress.Style.Font.Bold = false;
			this.textBoxDepartmentAddress.Style.Font.Name = "Times New Roman";
			this.textBoxDepartmentAddress.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBoxDepartmentAddress.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxDepartmentAddress.Value = "Department Address";
			// 
			// textBoxHospitalName
			// 
			this.textBoxHospitalName.KeepTogether = true;
			this.textBoxHospitalName.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.84375D));
			this.textBoxHospitalName.Name = "textBoxHospitalName";
			this.textBoxHospitalName.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.1999211311340332D));
			this.textBoxHospitalName.Style.Font.Bold = false;
			this.textBoxHospitalName.Style.Font.Name = "Times New Roman";
			this.textBoxHospitalName.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBoxHospitalName.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxHospitalName.Value = "Hospital Name";
			// 
			// textBoxTestHeader
			// 
			this.textBoxTestHeader.KeepTogether = true;
			this.textBoxTestHeader.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.54166668653488159D));
			this.textBoxTestHeader.Name = "textBoxTestHeader";
			this.textBoxTestHeader.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
			this.textBoxTestHeader.Style.Font.Bold = true;
			this.textBoxTestHeader.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
			this.textBoxTestHeader.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBoxTestHeader.Value = "Daxor BVA-100 - Full QC Test - Analysis Report";
			// 
			// textBoxCumulativePassFail
			// 
			this.textBoxCumulativePassFail.KeepTogether = false;
			this.textBoxCumulativePassFail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.8958334922790527D), Telerik.Reporting.Drawing.Unit.Inch(0.1458333283662796D));
			this.textBoxCumulativePassFail.Name = "textBoxCumulativePassFail";
			this.textBoxCumulativePassFail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0041669607162476D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
			this.textBoxCumulativePassFail.Style.Font.Bold = true;
			this.textBoxCumulativePassFail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
			this.textBoxCumulativePassFail.Value = "";
			// 
			// textBox1
			// 
			this.textBox1.KeepTogether = true;
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5D), Telerik.Reporting.Drawing.Unit.Inch(0.1458333283662796D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9000002145767212D), Telerik.Reporting.Drawing.Unit.Inch(0.30000004172325134D));
			this.textBox1.Style.Font.Bold = true;
			this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
			this.textBox1.Value = "Cumulative Result - ";
			// 
			// textBoxPageCount
			// 
			this.textBoxPageCount.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1041666641831398D), Telerik.Reporting.Drawing.Unit.Inch(0.1458333283662796D));
			this.textBoxPageCount.Name = "textBoxPageCount";
			this.textBoxPageCount.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.80000019073486328D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
			this.textBoxPageCount.Style.Visible = false;
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Inch(3D);
			this.detail.Name = "detail";
			// 
			// pageFooterSection1
			// 
			this.pageFooterSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(0.40000024437904358D);
			this.pageFooterSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17,
            this.textBoxBVAVersion,
            this.textBox20,
            this.textBoxUnitID,
            this.textBox21,
            this.textBoxPrintedOn});
			this.pageFooterSection1.Name = "pageFooterSection1";
			// 
			// textBox17
			// 
			this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.0520833320915699D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.69583338499069214D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox17.Style.Font.Bold = true;
			this.textBox17.Style.Font.Name = "Times New Roman";
			this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox17.Value = "BVA-100:";
			// 
			// textBoxBVAVersion
			// 
			this.textBoxBVAVersion.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.74799537658691406D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
			this.textBoxBVAVersion.Name = "textBoxBVAVersion";
			this.textBoxBVAVersion.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.85609245300292969D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxBVAVersion.Style.Font.Name = "Times New Roman";
			this.textBoxBVAVersion.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBoxBVAVersion.Value = "v1.2.0.0";
			// 
			// textBox20
			// 
			this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.7851823568344116D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.68958348035812378D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox20.Style.Font.Bold = true;
			this.textBox20.Style.Font.Name = "Times New Roman";
			this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox20.Value = "Unit ID:";
			// 
			// textBoxUnitID
			// 
			this.textBoxUnitID.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.4748446941375732D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
			this.textBoxUnitID.Name = "textBoxUnitID";
			this.textBoxUnitID.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.6104946136474609D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxUnitID.Style.Font.Name = "Times New Roman";
			this.textBoxUnitID.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBoxUnitID.Value = "-";
			// 
			// textBox21
			// 
			this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.0522418022155762D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.79567527770996094D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBox21.Style.Font.Name = "Times New Roman";
			this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox21.Value = "=\'Page \' + PageNumber + \' of \' + PageCount";
			// 
			// textBoxPrintedOn
			// 
			this.textBoxPrintedOn.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.0854182243347168D), Telerik.Reporting.Drawing.Unit.Inch(0.10000038146972656D));
			this.textBoxPrintedOn.Name = "textBoxPrintedOn";
			this.textBoxPrintedOn.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.9541667699813843D), Telerik.Reporting.Drawing.Unit.Inch(0.2395833283662796D));
			this.textBoxPrintedOn.Style.Font.Name = "Times New Roman";
			this.textBoxPrintedOn.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBoxPrintedOn.Value = "textBoxDateTimePrinted";
			// 
			// QCReport
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pageHeaderSection1,
            this.detail,
            this.pageFooterSection1});
			this.Name = "QCReport";
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0.37999999523162842D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.10000000149011612D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.Letter;
			this.Style.BackgroundColor = System.Drawing.Color.White;
			this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.9999217987060547D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        public Telerik.Reporting.TextBox textBoxVoltage;
        private Telerik.Reporting.TextBox textBox10;
        public Telerik.Reporting.TextBox textBoxEndFineGain;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox9;
        public Telerik.Reporting.TextBox textBoxAnalyst;
        private Telerik.Reporting.TextBox textBox8;
        public Telerik.Reporting.TextBox textBoxStartFineGain;
        private Telerik.Reporting.TextBox textBox6;
        public Telerik.Reporting.TextBox textBoxComment;
        public Telerik.Reporting.TextBox textBoxTestDate;
        private Telerik.Reporting.TextBox textBox3;
        public Telerik.Reporting.TextBox textBoxDepartmentDirector;
        public Telerik.Reporting.TextBox textBoxDepartment;
        public Telerik.Reporting.TextBox textBoxDepartmentPhoneNumber;
        public Telerik.Reporting.TextBox textBoxDepartmentAddress;
        public Telerik.Reporting.TextBox textBoxHospitalName;
        public Telerik.Reporting.TextBox textBoxCumulativePassFail;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox17;
        public Telerik.Reporting.TextBox textBoxBVAVersion;
        private Telerik.Reporting.TextBox textBox20;
        public Telerik.Reporting.TextBox textBoxUnitID;
        private Telerik.Reporting.TextBox textBox21;
        public Telerik.Reporting.TextBox textBoxPrintedOn;
        public Telerik.Reporting.DetailSection detail;
        public Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        public Telerik.Reporting.TextBox textBoxTestHeader;
        public Telerik.Reporting.PageFooterSection pageFooterSection1;
        public Telerik.Reporting.TextBox textBoxPageCount;
    }
}