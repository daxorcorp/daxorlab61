using Daxor.Lab.QC.Core.Common;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Interfaces.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_QC_test_type_enum_to_a_user_friendly_string
    {
        [TestMethod]
        [RequirementValue("Unknown")]
        public void And_the_value_to_be_converted_is_the_Undefined_enum_then_the_string_returned_matches_requirements()
        {
            var observedResult = QcTestNameConverter.Convert(TestType.Undefined);

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), observedResult);
        }

        [TestMethod]
        [RequirementValue("Linearity")]
        public void And_the_value_to_be_converted_is_the_Linearity_enum_then_the_string_returned_matches_requirements()
        {
            var observedResult = QcTestNameConverter.Convert(TestType.Linearity);

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), observedResult);
        }

        [TestMethod]
        [RequirementValue("Linearity")]
        public void And_the_value_to_be_converted_is_the_Linearity_without_calibration_enum_then_the_string_returned_matches_requirements()
        {
            var observedResult = QcTestNameConverter.Convert(TestType.LinearityWithoutCalibration);

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), observedResult);
        }

        [TestMethod]
        [RequirementValue("Contamination")]
        public void And_the_value_to_be_converted_is_the_Contamination_enum_then_the_string_returned_matches_requirements()
        {
            var observedResult = QcTestNameConverter.Convert(TestType.Contamination);

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), observedResult);
        }

        [TestMethod]
        [RequirementValue("Full QC")]
        public void And_the_value_to_be_converted_is_the_Full_enum_then_the_string_returned_matches_requirements()
        {
            var observedResult = QcTestNameConverter.Convert(TestType.Full);

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), observedResult);
        }

        [TestMethod]
        [RequirementValue("Standards")]
        public void And_the_value_to_be_converted_is_the_Standards_enum_then_the_string_returned_matches_requirements()
        {
            var observedResult = QcTestNameConverter.Convert(TestType.Standards);

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), observedResult);
        }
    }
    // ReSharper restore InconsistentNaming
}
