using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Interfaces.Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_QC_test_type_enum_as_a_string_to_a_user_friendly_string
    {
        [TestMethod]
        public void And_the_string_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>QcTestNameConverter.Convert(null), "value");
        }

        [TestMethod]
        public void And_the_string_is_not_the_string_representation_of_the_enum_then_an_exception_is_thrown()
        {
            AssertEx.Throws<ArgumentOutOfRangeException>(()=>QcTestNameConverter.Convert("This will break"));
        }
    }
    // ReSharper restore InconsistentNaming
}
