using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.QC.Interfaces.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_converting_a_QC_test_type_enum_as_a_string_to_a_user_friendly_string
    {
        [TestMethod]
        [RequirementValue("Unknown")]
        public void And_the_string_is_the_representation_of_the_Undefined_enum_then_unknown_matches_requirements()
        {
            var observedResult = QcTestNameConverter.Convert("Undefined");

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), observedResult);
        }

        [TestMethod]
        [RequirementValue("Linearity")]
        public void And_the_string_is_the_representation_of_the_Linearity_enum_then_Linearity_matches_requirements()
        {
            var observedResult = QcTestNameConverter.Convert(RequirementHelper.GetRequirementValueAsString());

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), observedResult);
        }

        [TestMethod]
        [RequirementValue("Linearity")]
        public void And_the_string_is_the_representation_of_the_Linearity_without_calibration_enum_then_Linearity_matches_requirements()
        {
            var observedResult = QcTestNameConverter.Convert("LinearityWithoutCalibration");

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), observedResult);
        }

        [TestMethod]
        [RequirementValue("Contamination")]
        public void And_the_string_is_the_representation_of_the_Contamination_enum_then_Contamination_matches_requirements()
        {
            var observedResult = QcTestNameConverter.Convert(RequirementHelper.GetRequirementValueAsString());

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), observedResult);
        }

        [TestMethod]
        [RequirementValue("Full QC")]
        public void And_the_string_is_the_representation_of_the_Full_enum_then_Daily_QC_matches_requirements()
        {
            var observedResult = QcTestNameConverter.Convert("Full");

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), observedResult);
        }

        [TestMethod]
        [RequirementValue("Standards")]
        public void And_the_string_is_the_representation_of_the_Standards_enum_then_Standards_matches_requirements()
        {
            var observedResult = QcTestNameConverter.Convert(RequirementHelper.GetRequirementValueAsString());

            Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), observedResult);
        }
    }
    // ReSharper restore InconsistentNaming
}
