using System;
using Daxor.Lab.MCAService.Interfaces;

namespace Daxor.Lab.MCAService.Tests
{
    internal class ThreadPoolForTesting : IThreadPool
    {
        public void QueueUserWorkItem(Action workItem)
        {
            workItem();
        }
    }
}