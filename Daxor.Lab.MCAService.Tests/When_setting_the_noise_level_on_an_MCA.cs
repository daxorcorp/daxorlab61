using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_noise_level_on_an_MCA
    {
        private const string McaId = "abc123";
        private const int ExpectedNoiseLevel = 16;
        private MultiChannelAnalyzerService _mcaService;
        private IMultiChannelAnalyzerController _mockController;
        private IMultiChannelAnalyzer _mockMca;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubLoggerFactory.CreateLogger("a", "b").ReturnsForAnyArgs(_mockLogger);

            _mockMca = Substitute.For<IMultiChannelAnalyzer>();
            _mockController = Substitute.For<IMultiChannelAnalyzerController>();
            _mockController.GetMultiChannelAnalyzerWithId(McaId).Returns(_mockMca);

            var mcaControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            mcaControllerFactory.BuildController().Returns(_mockController);

            _mcaService = new MultiChannelAnalyzerService(stubLoggerFactory,
                mcaControllerFactory,
                Substitute.For<IOperationContext>(),
                Substitute.For<IThreadPool>());
        }

        [TestMethod]
        public void Then_the_controller_is_used()
        {
            _mcaService.SetNoiseLevel(McaId, ExpectedNoiseLevel);

            _mockController.Received().GetMultiChannelAnalyzerWithId(McaId);
        }

        [TestMethod]
        public void Then_the_noise_level_is_set()
        {
            _mcaService.SetNoiseLevel(McaId, ExpectedNoiseLevel);

            Assert.AreEqual(ExpectedNoiseLevel, _mockMca.NoiseLevel);
        }

        [TestMethod]
        public void Then_the_action_is_logged()
        {
            _mcaService.SetNoiseLevel(McaId, ExpectedNoiseLevel);

            _mockLogger.Received().Log("Setting noise level to " + ExpectedNoiseLevel + " on MCA '" + McaId + "'");
        }
    }
    // ReSharper restore InconsistentNaming
}
