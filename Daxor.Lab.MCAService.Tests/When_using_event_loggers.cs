using Daxor.Lab.MCAService.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.MCAService.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_event_loggers
    {
        [TestMethod]
        public void Then_the_hardware_configuration_manager_log_name_matches_requirements()
        {
            Assert.AreEqual("DAXOR Hardware Configuration Manager Log", MultiChannelAnalyzerControllerBuilder.HardwareConfigurationManagerLogName);
        }

        [TestMethod]
        public void Then_the_hardware_configuration_manager_log_source_name_matches_requirements()
        {
            Assert.AreEqual("HardwareConfigurationManagerLog", MultiChannelAnalyzerControllerBuilder.HardwareConfigurationManagerLogSourceName);
        }

        [TestMethod]
        public void Then_the_MCA_service_log_name_matches_requirements()
        {
            Assert.AreEqual("Multi-channel Analyzer Service Log", ServiceLoggerFactory.MultiChannelAnalyzerServiceLogName);
        }

        [TestMethod]
        public void Then_the_MCA_service_log_source_name_matches_requirements()
        {
            Assert.AreEqual("MCAServiceLog", ServiceLoggerFactory.MultiChannelAnalyzerServiceLogSourceName);
        }
    }
    // ReSharper restore InconsistentNaming
}
