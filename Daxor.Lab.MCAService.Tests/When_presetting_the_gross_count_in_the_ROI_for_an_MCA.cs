using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_presetting_the_gross_count_in_the_ROI_for_an_MCA
    {
        private const string McaId = "abc123";
        private const int ExpectedCountPreset = 1235;
        private const int ExpectedLeftChannel = 400;
        private const int ExpectedRightChannel = 800;
        private MultiChannelAnalyzerService _mcaService;
        private IMultiChannelAnalyzerController _mockController;
        private IMultiChannelAnalyzer _mockMca;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubLoggerFactory.CreateLogger("a", "b").ReturnsForAnyArgs(_mockLogger);

            _mockMca = Substitute.For<IMultiChannelAnalyzer>();
            _mockController = Substitute.For<IMultiChannelAnalyzerController>();
            _mockController.GetMultiChannelAnalyzerWithId(McaId).Returns(_mockMca);

            var mcaControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            mcaControllerFactory.BuildController().Returns(_mockController);

            _mcaService = new MultiChannelAnalyzerService(stubLoggerFactory,
                mcaControllerFactory,
                Substitute.For<IOperationContext>(),
                Substitute.For<IThreadPool>());
        }

        [TestMethod]
        public void Then_the_controller_is_used()
        {
            _mcaService.PresetGrossCountInRegionOfInterest(McaId, ExpectedCountPreset, ExpectedLeftChannel, ExpectedRightChannel);

            _mockController.Received().GetMultiChannelAnalyzerWithId(McaId);
        }

        [TestMethod]
        public void Then_the_count_is_preset()
        {
            _mcaService.PresetGrossCountInRegionOfInterest(McaId, ExpectedCountPreset, ExpectedLeftChannel, ExpectedRightChannel);

            _mockMca.Received().PresetGrossCountsInRegionOfInterest(ExpectedCountPreset, ExpectedLeftChannel, ExpectedRightChannel);
        }

        [TestMethod]
        public void Then_the_action_is_logged()
        {
            _mcaService.PresetGrossCountInRegionOfInterest(McaId, ExpectedCountPreset, ExpectedLeftChannel, ExpectedRightChannel);

            _mockLogger.Received().Log("Presetting gross count of " + ExpectedCountPreset + " in ROI " +
                ExpectedLeftChannel + " to " + ExpectedRightChannel + " on MCA '" + McaId + "'");
        }
    }
    // ReSharper restore InconsistentNaming
}
