using System.Collections.Generic;
using System.Linq;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_all_MCA_IDs
    {
        private MultiChannelAnalyzerService _mcaService;
        private IMultiChannelAnalyzerController _mockController;
        private readonly IList<string> ExpectedIds = new List<string> { "abc123", "def567" };

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockController = Substitute.For<IMultiChannelAnalyzerController>();
            _mockController.GetAllMultiChannelAnalyzerIds().Returns(ExpectedIds);

            var mcaControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            mcaControllerFactory.BuildController().Returns(_mockController);
            
            _mcaService = new MultiChannelAnalyzerService(Substitute.For<IServiceLoggerFactory>(),
                mcaControllerFactory, 
                Substitute.For<IOperationContext>(),
                Substitute.For<IThreadPool>());
        }

        [TestMethod]
        public void Then_the_controller_is_used()
        {
            _mcaService.GetAllMultiChannelAnalyzerIds();
            
            _mockController.Received().GetAllMultiChannelAnalyzerIds();
        }

        [TestMethod]
        public void Then_the_correct_IDs_are_returned()
        {
            var observedIds = _mcaService.GetAllMultiChannelAnalyzerIds();
            
            CollectionAssert.AreEqual(ExpectedIds.ToList(), observedIds.ToList());
        }
    }
    // ReSharper restore InconsistentNaming
}
