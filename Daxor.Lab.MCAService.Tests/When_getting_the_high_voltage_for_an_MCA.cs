using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_high_voltage_for_an_MCA
    {
        private const string McaId = "abc123";
        private const int ExpectedHighVoltage = 234;
        private MultiChannelAnalyzerService _mcaService;
        private IMultiChannelAnalyzerController _mockController;

        [TestInitialize]
        public void BeforeEachTest()
        {
            var stubMca = Substitute.For<IMultiChannelAnalyzer>();
            stubMca.HighVoltage.Returns(ExpectedHighVoltage);

            _mockController = Substitute.For<IMultiChannelAnalyzerController>();
            _mockController.GetMultiChannelAnalyzerWithId(McaId).Returns(stubMca);

            var mcaControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            mcaControllerFactory.BuildController().Returns(_mockController);

            _mcaService = new MultiChannelAnalyzerService(Substitute.For<IServiceLoggerFactory>(),
                mcaControllerFactory,
                Substitute.For<IOperationContext>(),
                Substitute.For<IThreadPool>());
        }

        [TestMethod]
        public void Then_the_controller_is_used()
        {
            _mcaService.GetHighVoltage(McaId);

            _mockController.Received().GetMultiChannelAnalyzerWithId(McaId);
        }

        [TestMethod]
        public void Then_the_correct_high_voltage_is_returned()
        {
            Assert.AreEqual(ExpectedHighVoltage, _mcaService.GetHighVoltage(McaId));
        }
    }
    // ReSharper restore InconsistentNaming
}
