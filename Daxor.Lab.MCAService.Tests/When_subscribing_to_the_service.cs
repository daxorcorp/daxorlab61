using System.Linq;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.MCAService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_subscribing_to_the_service
    {
        [TestMethod]
        public void And_there_is_no_callback_channel_from_the_operation_context_then_the_client_list_remains_the_same()
        {
            var stubOperationContext = Substitute.For<IOperationContext>();
            stubOperationContext.GetCallbackChannel<IMultiChannelAnalyzerServiceCallbackEvents>().Returns((IMultiChannelAnalyzerServiceCallbackEvents) null);
            var mcaService = new McaServiceWithExposedMembers(Substitute.For<IServiceLoggerFactory>(),
                Substitute.For<IMultiChannelAnalyzerControllerBuilder>(),
                stubOperationContext, Substitute.For<IThreadPool>());

            Assert.AreEqual(0, mcaService.ExposedClientsList.Count);

            mcaService.Subscribe();

            Assert.AreEqual(0, mcaService.ExposedClientsList.Count);
        }

        [TestMethod]
        public void And_there_is_no_callback_channel_from_the_operation_context_then_a_message_is_logged()
        {
            var stubOperationContext = Substitute.For<IOperationContext>();
            stubOperationContext.GetCallbackChannel<IMultiChannelAnalyzerServiceCallbackEvents>().Returns((IMultiChannelAnalyzerServiceCallbackEvents)null);
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubLoggerFactory.CreateLogger("a", "b").ReturnsForAnyArgs(mockLogger);
            var mcaService = new McaServiceWithExposedMembers(stubLoggerFactory,
                Substitute.For<IMultiChannelAnalyzerControllerBuilder>(),
                stubOperationContext, Substitute.For<IThreadPool>());

            mcaService.Subscribe();

            mockLogger.Received().Log("Client doesn't implement the expected callback events");
        }

        [TestMethod]
        public void And_there_is_a_callback_channel_from_the_operation_context_and_that_channel_is_in_the_list_of_clients_then_the_client_list_remains_the_same()
        {
            var dummyClient = Substitute.For<IMultiChannelAnalyzerServiceCallbackEvents>();
            var stubOperationContext = Substitute.For<IOperationContext>();
            stubOperationContext.GetCallbackChannel<IMultiChannelAnalyzerServiceCallbackEvents>().Returns(dummyClient);
            var mcaService = new McaServiceWithExposedMembers(Substitute.For<IServiceLoggerFactory>(),
                Substitute.For<IMultiChannelAnalyzerControllerBuilder>(),
                stubOperationContext, Substitute.For<IThreadPool>());
            mcaService.ExposedClientsList.Add(dummyClient);

            Assert.AreEqual(1, mcaService.ExposedClientsList.Count);

            mcaService.Subscribe();

            Assert.AreEqual(1, mcaService.ExposedClientsList.Count);
        }

        [TestMethod]
        public void And_there_is_a_callback_channel_from_the_operation_context_and_that_channel_is_in_the_list_of_clients_then_a_message_is_logged()
        {
            var dummyClient = Substitute.For<IMultiChannelAnalyzerServiceCallbackEvents>();
            var stubOperationContext = Substitute.For<IOperationContext>();
            stubOperationContext.GetCallbackChannel<IMultiChannelAnalyzerServiceCallbackEvents>().Returns(dummyClient);
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubLoggerFactory.CreateLogger("a", "b").ReturnsForAnyArgs(mockLogger);
            var mcaService = new McaServiceWithExposedMembers(stubLoggerFactory,
                Substitute.For<IMultiChannelAnalyzerControllerBuilder>(),
                stubOperationContext, Substitute.For<IThreadPool>());
            mcaService.ExposedClientsList.Add(dummyClient);

            mcaService.Subscribe();

            mockLogger.Received().Log("Client is already subscribed");
        }

        [TestMethod]
        public void And_there_is_a_callback_channel_from_the_operation_context_and_that_channel_is_not_subscribed_then_it_is_added_to_the_client_list()
        {
            var dummyClient = Substitute.For<IMultiChannelAnalyzerServiceCallbackEvents>();
            var stubOperationContext = Substitute.For<IOperationContext>();
            stubOperationContext.GetCallbackChannel<IMultiChannelAnalyzerServiceCallbackEvents>().Returns(dummyClient);
            var mcaService = new McaServiceWithExposedMembers(Substitute.For<IServiceLoggerFactory>(),
                Substitute.For<IMultiChannelAnalyzerControllerBuilder>(),
                stubOperationContext, Substitute.For<IThreadPool>());

            mcaService.Subscribe();

            Assert.IsTrue(mcaService.ExposedClientsList.Contains(dummyClient));
        }

        [TestMethod]
        public void And_there_is_a_callback_channel_from_the_operation_context_and_the_channel_is_subscribed_then_a_message_is_logged()
        {
            var dummyClient1 = Substitute.For<IMultiChannelAnalyzerServiceCallbackEvents>();
            var dummyClient2 = Substitute.For<IMultiChannelAnalyzerServiceCallbackEvents>();
            var stubOperationContext = Substitute.For<IOperationContext>();
            stubOperationContext.GetCallbackChannel<IMultiChannelAnalyzerServiceCallbackEvents>().Returns(dummyClient2);
            stubOperationContext.RemoteEndpointIpAddress.Returns("12.34.56.78");
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubLoggerFactory.CreateLogger("a", "b").ReturnsForAnyArgs(mockLogger);
            var mcaService = new McaServiceWithExposedMembers(stubLoggerFactory,
                Substitute.For<IMultiChannelAnalyzerControllerBuilder>(),
                stubOperationContext, Substitute.For<IThreadPool>());
            mcaService.ExposedClientsList.Add(dummyClient1);

            mcaService.Subscribe();

            mockLogger.Received().Log("Client connected from IP 12.34.56.78; total client count is 2");
        }
    }
    // ReSharper restore InconsistentNaming
}
