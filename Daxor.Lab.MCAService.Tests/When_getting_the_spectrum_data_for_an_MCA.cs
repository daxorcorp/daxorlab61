using Daxor.Lab.MCAContracts;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_spectrum_data_for_an_MCA
    {
        private const string McaId = "abc123";
        private readonly SpectrumData ExpectedSpectrumData = new SpectrumData(4, 5, 6, null);
        private MultiChannelAnalyzerService _mcaService;
        private IMultiChannelAnalyzerController _mockController;

        [TestInitialize]
        public void BeforeEachTest()
        {
            var stubMca = Substitute.For<IMultiChannelAnalyzer>();
            stubMca.GetSpectrumData().Returns(ExpectedSpectrumData);

            _mockController = Substitute.For<IMultiChannelAnalyzerController>();
            _mockController.GetMultiChannelAnalyzerWithId(McaId).Returns(stubMca);

            var mcaControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            mcaControllerFactory.BuildController().Returns(_mockController);

            _mcaService = new MultiChannelAnalyzerService(Substitute.For<IServiceLoggerFactory>(),
                mcaControllerFactory,
                Substitute.For<IOperationContext>(),
                Substitute.For<IThreadPool>());
        }

        [TestMethod]
        public void Then_the_controller_is_used()
        {
            _mcaService.GetSpectrumData(McaId);

            _mockController.Received().GetMultiChannelAnalyzerWithId(McaId);
        }

        [TestMethod]
        public void Then_the_correct_spectrum_data_is_returned()
        {
            Assert.AreEqual(ExpectedSpectrumData, _mcaService.GetSpectrumData(McaId));
        }
    }
    // ReSharper restore InconsistentNaming
}
