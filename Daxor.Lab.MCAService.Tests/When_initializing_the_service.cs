﻿using System.Collections.Generic;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Daxor.Lab.MCAService.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_service
    {
        [TestMethod]
        public void Then_a_log_entry_is_made_indicating_that_the_service_is_initializing()
        {
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubLoggerFactory.CreateLogger(ServiceLoggerFactory.MultiChannelAnalyzerServiceLogSourceName, ServiceLoggerFactory.MultiChannelAnalyzerServiceLogName).Returns(mockLogger);

            var stubController = Substitute.For<IMultiChannelAnalyzerController>();
            var stubControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            stubControllerFactory.BuildController().Returns(stubController);

            // ReSharper disable once ObjectCreationAsStatement
            new MultiChannelAnalyzerService(stubLoggerFactory, stubControllerFactory, Substitute.For<IOperationContext>(), Substitute.For<IThreadPool>());

            mockLogger.Received().Log("MCA service is initializing");
        }

        [TestMethod]
        public void Then_the_statuses_of_all_MCAs_are_logged()
        {
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubLoggerFactory.CreateLogger(ServiceLoggerFactory.MultiChannelAnalyzerServiceLogSourceName, ServiceLoggerFactory.MultiChannelAnalyzerServiceLogName).Returns(mockLogger);

            var stubController = Substitute.For<IMultiChannelAnalyzerController>();
            stubController.GetAllMultiChannelAnalyzerIds().Returns(new List<string> {"mca123", "mca456"});
            var connectedMca = Substitute.For<IMultiChannelAnalyzer>();
            connectedMca.IsConnected.Returns(true);
            var disconnectedMca = Substitute.For<IMultiChannelAnalyzer>();
            disconnectedMca.IsConnected.Returns(false);
            stubController.GetMultiChannelAnalyzerWithId("mca123").Returns(connectedMca);
            stubController.GetMultiChannelAnalyzerWithId("mca456").Returns(disconnectedMca);

            var stubControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            stubControllerFactory.BuildController().Returns(stubController);

            // ReSharper disable once ObjectCreationAsStatement
            new MultiChannelAnalyzerService(stubLoggerFactory, stubControllerFactory, Substitute.For<IOperationContext>(), Substitute.For<IThreadPool>());

            mockLogger.Received().Log("MCA 'mca123' is connected");
            mockLogger.Received().Log("MCA 'mca456' is disconnected");
        }

        [TestMethod]
        public void Then_there_are_no_subscribed_clients()
        {
            var mcaService = new McaServiceWithExposedMembers(Substitute.For<IServiceLoggerFactory>(),
                Substitute.For<IMultiChannelAnalyzerControllerBuilder>(),
                Substitute.For<IOperationContext>(),
                Substitute.For<IThreadPool>());

            Assert.AreEqual(0, mcaService.ExposedClientsList.Count);
        }
    }
    // ReSharper restore InconsistentNaming}
}
