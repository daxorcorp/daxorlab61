using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_determining_if_acquisition_has_completed_for_an_MCA
    {
        private const string McaId = "abc123";
        private MultiChannelAnalyzerService _mcaService;
        private IMultiChannelAnalyzerController _mockController;

        [TestInitialize]
        public void BeforeEachTest()
        {
            var stubMca = Substitute.For<IMultiChannelAnalyzer>();
            stubMca.HasAcquisitionCompleted.Returns(true);

            _mockController = Substitute.For<IMultiChannelAnalyzerController>();
            _mockController.GetMultiChannelAnalyzerWithId(McaId).Returns(stubMca);

            var mcaControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            mcaControllerFactory.BuildController().Returns(_mockController);

            _mcaService = new MultiChannelAnalyzerService(Substitute.For<IServiceLoggerFactory>(),
                mcaControllerFactory,
                Substitute.For<IOperationContext>(),
                Substitute.For<IThreadPool>());
        }

        [TestMethod]
        public void Then_the_controller_is_used()
        {
            _mcaService.HasAcquisitionCompleted(McaId);

            _mockController.Received().GetMultiChannelAnalyzerWithId(McaId);
        }

        [TestMethod]
        public void Then_the_correct_acquisition_completed_state_is_returned()
        {
            Assert.IsTrue(_mcaService.HasAcquisitionCompleted(McaId));
        }
    }
    // ReSharper restore InconsistentNaming
}
