﻿using System.Collections.Concurrent;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.MCAService.Interfaces;

namespace Daxor.Lab.MCAService.Tests
{
    internal class McaServiceWithExposedMembers : MultiChannelAnalyzerService
    {
        internal McaServiceWithExposedMembers(IServiceLoggerFactory serviceLoggerFactory,
            IMultiChannelAnalyzerControllerBuilder mcaControllerBuilder,
            IOperationContext operationContext, IThreadPool threadPool) : base(serviceLoggerFactory, mcaControllerBuilder, operationContext, threadPool)
        {
            
        }

        internal ConcurrentBag<IMultiChannelAnalyzerServiceCallbackEvents> ExposedClientsList
        {
            get { return SubscribedClients; }
        }
    }
}
