using System;
using System.ServiceModel;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAContracts.Faults;
using Daxor.Lab.MCACore.Exceptions;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_setting_the_lower_level_discriminator_on_an_MCA
    {
        private const string McaId = "abc123";
        private MultiChannelAnalyzerService _mcaService;
        private IMultiChannelAnalyzerController _mockController;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubLoggerFactory.CreateLogger("a", "b").ReturnsForAnyArgs(_mockLogger);

            _mockController = Substitute.For<IMultiChannelAnalyzerController>();

            var mcaControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            mcaControllerFactory.BuildController().Returns(_mockController);

            _mcaService = new MultiChannelAnalyzerService(stubLoggerFactory,
                mcaControllerFactory,
                Substitute.For<IOperationContext>(),
                Substitute.For<IThreadPool>());
        }

        [TestMethod]
        public void And_the_MCA_could_not_be_resolved_then_an_exception_is_thrown()
        {
            _mockController.GetMultiChannelAnalyzerWithId(McaId).Returns((IMultiChannelAnalyzer)null);

            var wasExceptionThrown = false;
            try
            {
                _mcaService.SetLowerLevelDiscriminator(McaId, 0);
            }
            catch (FaultException<MultiChannelAnalyzerNotFoundFaultContract> ex)
            {
                Assert.AreEqual("SetLowerLevelDiscriminator", ex.Detail.Method);
                Assert.AreEqual("Multichannel analyzer not found", ex.Detail.Message);
                Assert.AreEqual(McaId, ex.Detail.MultiChannelAnalyzerId);
                wasExceptionThrown = true;
            }

            Assert.IsTrue(wasExceptionThrown);
        }

        [TestMethod]
        public void And_setting_the_LLD_fails_then_a_fault_exception_is_thrown()
        {
            var stubMca = Substitute.For<IMultiChannelAnalyzer>();
            stubMca.When(x => x.LowerLevelDiscriminator = 0).Do(x => { throw new Exception("mca exception"); });

            _mockController.GetMultiChannelAnalyzerWithId(McaId).Returns(stubMca);

            var wasExceptionThrown = false;
            try
            {
                _mcaService.SetLowerLevelDiscriminator(McaId, 0);
            }
            catch (FaultException ex)
            {
                Assert.AreEqual("mca exception", ex.Message);
                wasExceptionThrown = true;
            }

            Assert.IsTrue(wasExceptionThrown);
        }

        [TestMethod]
        public void And_setting_the_LLD_fails_then_a_message_is_logged()
        {
            var stubMca = Substitute.For<IMultiChannelAnalyzer>();
            stubMca.When(x => x.LowerLevelDiscriminator = 0).Do(x => { throw new Exception("mca exception"); });
            _mockController.GetMultiChannelAnalyzerWithId(McaId).Returns(stubMca);

            AssertEx.Throws(() => _mcaService.SetLowerLevelDiscriminator(McaId, 0));

            _mockLogger.Received().Log("Unable to set lower-level discriminator on MCA '" + McaId + "': mca exception");
        }

        [TestMethod]
        public void And_setting_the_LLD_fails_because_of_an_out_of_bounds_error_then_a_fault_exception_is_thrown()
        {
            var stubMca = Substitute.For<IMultiChannelAnalyzer>();
            stubMca.When(x => x.LowerLevelDiscriminator = 0).Do(x => { throw new OutOfBoundsException("-1", "1", "0", "bounds message"); });

            _mockController.GetMultiChannelAnalyzerWithId(McaId).Returns(stubMca);

            var wasExceptionThrown = false;
            try
            {
                _mcaService.SetLowerLevelDiscriminator(McaId, 0);
            }
            catch (FaultException ex)
            {
                Assert.AreEqual("bounds message; attempted value is 0, minimum allowed is -1, maximum allowed is 1", ex.Message);
                wasExceptionThrown = true;
            }

            Assert.IsTrue(wasExceptionThrown);
        }

        [TestMethod]
        public void And_setting_the_LLD_fails_because_of_an_out_of_bounds_error_then_a_message_is_logged()
        {
            var stubMca = Substitute.For<IMultiChannelAnalyzer>();
            stubMca.When(x => x.LowerLevelDiscriminator = 0).Do(x => { throw new OutOfBoundsException("-1", "1", "0", "bounds message"); });
            _mockController.GetMultiChannelAnalyzerWithId(McaId).Returns(stubMca);

            AssertEx.Throws(() => _mcaService.SetLowerLevelDiscriminator(McaId, 0));

            _mockLogger.Received().Log("Unable to set lower-level discriminator on MCA '" + McaId + "': attempted value is 0, minimum allowed is -1, maximum allowed is 1 (bounds message)");
        }
    }
    // ReSharper restore InconsistentNaming
}
