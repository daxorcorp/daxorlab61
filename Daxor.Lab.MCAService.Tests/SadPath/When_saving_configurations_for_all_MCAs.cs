using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_saving_configurations_for_all_MCAs
    {
        private MultiChannelAnalyzerService _mcaService;
        private IMultiChannelAnalyzerController _stubController;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _stubController = Substitute.For<IMultiChannelAnalyzerController>();
            _stubController.When(x => x.SaveConfigurationsForAllMultiChannelAnalyzers()).Do(x => { throw new Exception("controller message"); });

            var mcaControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            mcaControllerFactory.BuildController().Returns(_stubController);

            _mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubLoggerFactory.CreateLogger("a", "b").ReturnsForAnyArgs(_mockLogger);

            _mcaService = new MultiChannelAnalyzerService(stubLoggerFactory,
                mcaControllerFactory,
                Substitute.For<IOperationContext>(),
                Substitute.For<IThreadPool>());
        }

        [TestMethod]
        public void And_an_exception_occurs_then_a_message_is_logged()
        {
            _mcaService.SaveConfigurationsForAllMultiChannelAnalyzers();

            _mockLogger.Received().Log("Unable to save configurations: controller message");
        }
    }
    // ReSharper restore InconsistentNaming
}
