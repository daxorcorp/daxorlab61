using System;
using System.ServiceModel;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAContracts.Faults;
using Daxor.Lab.MCAService.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_initializing_the_service
    {
        [TestMethod]
        public void And_the_service_logger_factory_instance_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentNullException>(() => new MultiChannelAnalyzerService(null, 
                Substitute.For<IMultiChannelAnalyzerControllerBuilder>(), 
                Substitute.For<IOperationContext>(),
                Substitute.For<IThreadPool>()));
        }

        [TestMethod]
        public void And_the_controller_builder_instance_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentNullException>(() => new MultiChannelAnalyzerService(Substitute.For<IServiceLoggerFactory>(), 
                null,
                Substitute.For<IOperationContext>(),
                Substitute.For<IThreadPool>()));
        }

        [TestMethod]
        public void And_the_operation_context_instance_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentNullException>(() => new MultiChannelAnalyzerService(Substitute.For<IServiceLoggerFactory>(),
                Substitute.For<IMultiChannelAnalyzerControllerBuilder>(),
                null,
                Substitute.For<IThreadPool>()));
        }

        [TestMethod]
        public void And_the_thread_pool_instance_is_null_then_an_exception_is_thrown()
        {
            // ReSharper disable once ObjectCreationAsStatement
            AssertEx.Throws<ArgumentNullException>(() => new MultiChannelAnalyzerService(Substitute.For<IServiceLoggerFactory>(),
                Substitute.For<IMultiChannelAnalyzerControllerBuilder>(),
                Substitute.For<IOperationContext>(),
                null));
        }

        [TestMethod]
        public void And_the_using_the_controller_factory_throws_an_exception_then_a_controller_fault_exception_is_thrown()
        {
            var stubControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            stubControllerFactory.BuildController().Returns(x => { throw new Exception("message here"); });

            var wasExceptionThrown = false;
            try
            {
                // ReSharper disable once ObjectCreationAsStatement
                new MultiChannelAnalyzerService(Substitute.For<IServiceLoggerFactory>(), stubControllerFactory,
                    Substitute.For<IOperationContext>(), Substitute.For<IThreadPool>());
            }
            catch (FaultException<MultiChannelAnalyzerControllerFaultContract> fault)
            {
                Assert.AreEqual("Unable to create an MCA controller", fault.Message);
                Assert.AreEqual("message here", fault.Detail.Message);
                wasExceptionThrown = true;
            }

            Assert.IsTrue(wasExceptionThrown);
        }

        [TestMethod]
        public void And_the_using_the_controller_factory_throws_an_exception_then_the_exception_is_logged()
        {
            var stubControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            stubControllerFactory.BuildController().Returns(x => { throw new Exception("message here"); });
            var mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubLoggerFactory.CreateLogger("a", "b").ReturnsForAnyArgs(mockLogger);

            var wasExceptionThrown = false;
            try
            {
                // ReSharper disable once ObjectCreationAsStatement
                new MultiChannelAnalyzerService(stubLoggerFactory, stubControllerFactory, Substitute.For<IOperationContext>(), Substitute.For<IThreadPool>());
            }
            catch (FaultException<MultiChannelAnalyzerControllerFaultContract>)
            {
                mockLogger.Received().Log("Unable to create an MCA controller: message here");
                wasExceptionThrown = true;
            }

            Assert.IsTrue(wasExceptionThrown);
        }
    }
    // ReSharper restore InconsistentNaming
}
