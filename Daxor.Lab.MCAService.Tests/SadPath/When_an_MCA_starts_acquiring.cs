using System;
using System.Linq;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_an_MCA_starts_acquiring
    {
        private const int ExpectedClientCount = 3;
        private const string ExpectedMcaId = "abc123";
        private readonly DateTime ExpectedDateTime = DateTime.Now;
        private IMultiChannelAnalyzerController _stubMcaController;
        private McaServiceWithExposedMembers _mcaService;
        private IWindowsEventLogger _mockEventLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _stubMcaController = Substitute.For<IMultiChannelAnalyzerController>();
            var stubMcaControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            stubMcaControllerFactory.BuildController().Returns(_stubMcaController);

            _mockEventLogger = Substitute.For<IWindowsEventLogger>();
            var stubServiceLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubServiceLoggerFactory.CreateLogger("a", "b").ReturnsForAnyArgs(_mockEventLogger);

            _mcaService = new McaServiceWithExposedMembers(stubServiceLoggerFactory,
                stubMcaControllerFactory, Substitute.For<IOperationContext>(), new ThreadPoolForTesting());

            for (var i = 0; i < ExpectedClientCount; i++)
                _mcaService.ExposedClientsList.Add(Substitute.For<IMultiChannelAnalyzerServiceCallbackEvents>());
        }

        [TestMethod]
        public void And_an_exception_happens_when_informing_the_client_then_that_client_is_removed()
        {
            var firstClient = _mcaService.ExposedClientsList.First();
            firstClient.When(x => x.AcquisitionStarted(ExpectedMcaId, ExpectedDateTime)).Do(x => { throw new Exception("client message"); });

            Assert.AreEqual(ExpectedClientCount, _mcaService.ExposedClientsList.Count);

            RaiseAcquisitionStarted();

            Assert.AreEqual(ExpectedClientCount - 1, _mcaService.ExposedClientsList.Count);
            Assert.IsFalse(_mcaService.ExposedClientsList.Contains(firstClient));
        }

        [TestMethod]
        public void And_an_exception_happens_when_informing_the_client_then_a_message_is_logged()
        {
            var firstClient = _mcaService.ExposedClientsList.First();
            firstClient.When(x => x.AcquisitionStarted(ExpectedMcaId, ExpectedDateTime)).Do(x => { throw new Exception("client message"); });

            RaiseAcquisitionStarted();

            _mockEventLogger.Received().Log("Exception occurred when raising AcquisitionStarted on client - will be unsubscribed: client message");
        }

        private void RaiseAcquisitionStarted()
        {
            _stubMcaController.AcquisitionStarted += Raise.Event<Action<string, DateTime>>(ExpectedMcaId, ExpectedDateTime);
        }
    }
    // ReSharper restore InconsistentNaming
}
