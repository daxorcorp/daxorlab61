using System;
using System.ServiceModel;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAContracts.Faults;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_getting_the_high_voltage_for_the_MCA
    {
        private const string McaId = "abc123";
        private MultiChannelAnalyzerService _mcaService;
        private IMultiChannelAnalyzerController _mockController;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubLoggerFactory.CreateLogger("a", "b").ReturnsForAnyArgs(_mockLogger);
            
            _mockController = Substitute.For<IMultiChannelAnalyzerController>();
            
            var mcaControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            mcaControllerFactory.BuildController().Returns(_mockController);
            
            _mcaService = new MultiChannelAnalyzerService(stubLoggerFactory,
                mcaControllerFactory,
                Substitute.For<IOperationContext>(),
                Substitute.For<IThreadPool>());
        }

        [TestMethod]
        public void And_the_MCA_could_not_be_resolved_then_an_exception_is_thrown()
        {
            _mockController.GetMultiChannelAnalyzerWithId(McaId).Returns((IMultiChannelAnalyzer) null);

            var wasExceptionThrown = false;
            try
            {
                _mcaService.GetHighVoltage(McaId);
            }
            catch (FaultException<MultiChannelAnalyzerNotFoundFaultContract> ex)
            {
                Assert.AreEqual("GetHighVoltage", ex.Detail.Method);
                Assert.AreEqual("Multichannel analyzer not found", ex.Detail.Message);
                Assert.AreEqual(McaId, ex.Detail.MultiChannelAnalyzerId);
                wasExceptionThrown = true;
            }

            Assert.IsTrue(wasExceptionThrown);
        }

        [TestMethod]
        public void And_the_high_voltage_could_not_be_obtained_then_a_fault_exception_is_thrown()
        {
            var stubMca = Substitute.For<IMultiChannelAnalyzer>();
            stubMca.HighVoltage.Returns(x => { throw new Exception("mca exception"); });

            _mockController.GetMultiChannelAnalyzerWithId(McaId).Returns(stubMca);

            var wasExceptionThrown = false;
            try
            {
                _mcaService.GetHighVoltage(McaId);
            }
            catch (FaultException ex)
            {
                Assert.AreEqual("mca exception", ex.Message);
                wasExceptionThrown = true;
            }

            Assert.IsTrue(wasExceptionThrown);
        }

        [TestMethod]
        public void And_the_high_voltage_could_not_be_obtained_then_a_message_is_logged()
        {
            var stubMca = Substitute.For<IMultiChannelAnalyzer>();
            stubMca.HighVoltage.Returns(x => { throw new Exception("mca exception"); });
            _mockController.GetMultiChannelAnalyzerWithId(McaId).Returns(stubMca);

            AssertEx.Throws(() => _mcaService.GetHighVoltage(McaId));

            _mockLogger.Received().Log("Unable to obtain high voltage for MCA '" + McaId + "': mca exception");
        }
    }
    // ReSharper restore InconsistentNaming
}
