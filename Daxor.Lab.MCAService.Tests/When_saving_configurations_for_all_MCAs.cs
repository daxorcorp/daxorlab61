using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_saving_configurations_for_all_MCAs
    {
        private MultiChannelAnalyzerService _mcaService;
        private IMultiChannelAnalyzerController _mockController;
        private IWindowsEventLogger _mockLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _mockController = Substitute.For<IMultiChannelAnalyzerController>();

            var mcaControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            mcaControllerFactory.BuildController().Returns(_mockController);

            _mockLogger = Substitute.For<IWindowsEventLogger>();
            var stubLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubLoggerFactory.CreateLogger("a", "b").ReturnsForAnyArgs(_mockLogger);

            _mcaService = new MultiChannelAnalyzerService(stubLoggerFactory,
                mcaControllerFactory,
                Substitute.For<IOperationContext>(),
                Substitute.For<IThreadPool>());
        }

        [TestMethod]
        public void Then_the_controller_is_used()
        {
            _mcaService.SaveConfigurationsForAllMultiChannelAnalyzers();

            _mockController.Received().SaveConfigurationsForAllMultiChannelAnalyzers();
        }

        [TestMethod]
        public void Then_the_action_is_logged()
        {
            _mcaService.SaveConfigurationsForAllMultiChannelAnalyzers();

            _mockLogger.Received().Log("Saving configurations for all MCAs");
        }
    }
    // ReSharper restore InconsistentNaming
}
