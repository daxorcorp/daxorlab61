using System;
using Daxor.Lab.HardwareConfigurationManager;
using Daxor.Lab.MCAContracts;
using Daxor.Lab.MCACore.Interfaces;
using Daxor.Lab.MCAService.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MCAService.Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_an_MCA_starts_acquiring
    {
        private const int ExpectedClientCount = 3;
        private const string ExpectedMcaId = "abc123";
        private readonly DateTime ExpectedDateTime = DateTime.Now;
        private IMultiChannelAnalyzerController _stubMcaController;
        private McaServiceWithExposedMembers _mcaService;
        private IWindowsEventLogger _mockEventLogger;

        [TestInitialize]
        public void BeforeEachTest()
        {
            _stubMcaController = Substitute.For<IMultiChannelAnalyzerController>();
            var stubMcaControllerFactory = Substitute.For<IMultiChannelAnalyzerControllerBuilder>();
            stubMcaControllerFactory.BuildController().Returns(_stubMcaController);

            _mockEventLogger = Substitute.For<IWindowsEventLogger>();
            var stubServiceLoggerFactory = Substitute.For<IServiceLoggerFactory>();
            stubServiceLoggerFactory.CreateLogger("a", "b").ReturnsForAnyArgs(_mockEventLogger);

            _mcaService = new McaServiceWithExposedMembers(stubServiceLoggerFactory,
                stubMcaControllerFactory, Substitute.For<IOperationContext>(), new ThreadPoolForTesting());

            for (var i = 0; i < ExpectedClientCount; i++)
                _mcaService.ExposedClientsList.Add(Substitute.For<IMultiChannelAnalyzerServiceCallbackEvents>());
        }

        [TestMethod]
        public void Then_each_client_is_informed()
        {
            RaiseAcquisitionStarted();

            foreach (var client in _mcaService.ExposedClientsList)
                client.Received().AcquisitionStarted(ExpectedMcaId, ExpectedDateTime);
        }

        private void RaiseAcquisitionStarted()
        {
            _stubMcaController.AcquisitionStarted += Raise.Event<Action<string, DateTime>>(ExpectedMcaId, ExpectedDateTime);
        }
    }
    // ReSharper restore InconsistentNaming
}
