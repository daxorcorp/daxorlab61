﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Daxor.Lab.SCService;
using System.ServiceModel;

namespace Daxor.Lab.SCWindowsServiceHost
{
    public partial class SmcHost : ServiceBase
    {
        public ServiceHost SmcServiceHost { get; protected set; }
        public SampleChangerService SmcService { get; protected set; }

        public SmcHost()
        {
            InitializeComponent();
            this.ServiceName = "Sample Changer Service";
        }
        public static void Main()
        {
            ServiceBase.Run(new SmcHost());
        }

        protected override void OnStart(string[] args)
        {
            if (SmcServiceHost != null)
                TerminateSampleChangerService();
            try
            {
                SmcService = new SampleChangerService();
            }
            catch (Exception ex)
            {
                this.EventLog.WriteEntry("Exception on start: " + ex.Message, EventLogEntryType.Error);
            }

            SmcServiceHost = new ServiceHost(SmcService);
            SmcServiceHost.Open();
            SmcServiceHost.Faulted += new EventHandler(OnServiceFaulted);

            string baseAddresses = "";
            foreach (Uri address in SmcServiceHost.BaseAddresses)
                baseAddresses += " " + address.AbsoluteUri;
            string s = String.Format("{0} listening at {1}", this.ServiceName, baseAddresses);
            this.EventLog.WriteEntry(s, EventLogEntryType.Information);
        }
        protected override void OnStop()
        {
            TerminateSampleChangerService();
        }

        #region Helpers

        private void TerminateSampleChangerService()
        {
            if (SmcServiceHost != null)
            {
                SmcServiceHost.Close();
                SmcServiceHost = null;
            }
        }
        protected void OnServiceFaulted(object sender, EventArgs e)
        {
            this.EventLog.WriteEntry(String.Format("{0} has faulted", this.ServiceName), EventLogEntryType.Error);
        }

        #endregion
    }
}
