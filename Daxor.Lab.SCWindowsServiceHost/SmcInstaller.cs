﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;


namespace Daxor.Lab.SCWindowsServiceHost
{
    [RunInstaller(true)]
    public partial class SmcInstaller : System.Configuration.Install.Installer
    {
        private ServiceProcessInstaller processInstaller;
        private ServiceInstaller serviceInstaller;

        public SmcInstaller()
        {
            InitializeComponent(); processInstaller = new ServiceProcessInstaller();
            processInstaller.Account = ServiceAccount.LocalSystem;

            serviceInstaller = new ServiceInstaller();
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            serviceInstaller.ServiceName = "Sample Changer Service";
            serviceInstaller.DisplayName = "Daxor Sample Changer Service";
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.Description = "WCF service host for sample changer service.";

            Installers.Add(processInstaller);
            Installers.Add(serviceInstaller);

            serviceInstaller.AfterInstall += new InstallEventHandler(serviceInstaller_AfterInstall);

        }

        /// <summary>
        /// Starts the service
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void serviceInstaller_AfterInstall(object sender, InstallEventArgs e)
        {
            ServiceController sc = new ServiceController("Sample Changer Service");
            sc.Start();
        }
    }
}
