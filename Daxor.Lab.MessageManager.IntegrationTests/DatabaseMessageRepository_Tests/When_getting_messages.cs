﻿using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MessageManager.IntegrationTests.DatabaseMessageRepository_Tests
{
    [TestClass]
    // ReSharper disable InconsistentNaming
    public class When_getting_messages
    {
        [TestMethod]
        [TestCategory("Integration Test")]
        [RequirementValue(103)]
        public void Then_the_total_number_of_messages_matches_requirements()
        {
            var dataService = new SettingsManagerDataService();
            var settingsManager = new SystemSettingsManager(Substitute.For<ILoggerFacade>(), dataService);
            var messageRepository = new DatabaseMessageRepository(settingsManager);

            var allMessages = messageRepository.GetMessages();
            
            Assert.AreEqual(RequirementHelper.GetRequirementValueAsInt(), allMessages.Count);
        }
    }
    // ReSharper restore InconsistentNaming
}
