using Daxor.Lab.MessageManager.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MessageManager.IntegrationTests.SystemMessageManager_Tests
{
	// ReSharper disable InconsistentNaming
	[TestClass]
	public class When_initializing_the_message_manager
	{
		[TestMethod]
        [TestCategory("Integration Test")]
		public void Then_the_message_collection_is_retrieved_from_the_message_repository()
		{
			var mockMessageRepository = Substitute.For<IMessageRepository>();
            
		    // ReSharper disable once UnusedVariable
            var ignored = new SystemMessageManager(mockMessageRepository);

            mockMessageRepository.Received(1).GetMessages();
		}
	}
	// ReSharper restore InconsistentNaming
}
