﻿using System;
using System.Collections.Generic;
using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.SettingsManager;
using Daxor.Lab.TestHelpers;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MessageManager.IntegrationTests.SystemMessageManager_Tests.SadPath
{
	[TestClass]
	// ReSharper disable InconsistentNaming
	public class When_getting_a_message
	{
		private ILoggerFacade _stubLogger;

		private DatabaseMessageRepository _messageRepository;
		private SettingsManagerDataService _dataService;
		private SystemSettingsManager _settingsManager;

		private SystemMessageManager _messageManager;

		[TestInitialize]
		public void Initialize()
		{
			_stubLogger = Substitute.For<ILoggerFacade>();
			_dataService = new SettingsManagerDataService();
			_settingsManager = new SystemSettingsManager(_stubLogger, _dataService);
			_messageRepository = new DatabaseMessageRepository(_settingsManager);
			_messageManager = new SystemMessageManager(_messageRepository);
		}

		[TestMethod]
        [TestCategory("Integration Test")]
		[RequirementValue("SystemMessageManager: No such message ID: fake_key")]
		public void And_the_given_key_does_not_exist_then_an_exception_is_thrown()
		{
			try
			{
				_messageManager.GetMessage("fake_key");

				Assert.Fail("Was expecting a KeyNotFoundException to be thrown.  No exception actually thrown.");
			}
			catch (KeyNotFoundException e)
			{
				Assert.AreEqual(RequirementHelper.GetRequirementValueAsString(), e.Message);
			}
			catch (Exception e)
			{
				Assert.Fail("Was expecting a KeyNotFoundException to be thrown.  A(n)" + e.GetType() + " was thrown instead.");
			}
		}
	}
	// ReSharper restore InconsistentNaming
}
