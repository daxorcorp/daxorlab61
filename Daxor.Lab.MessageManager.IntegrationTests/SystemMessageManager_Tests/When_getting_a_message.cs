﻿using Daxor.Lab.Database.DataAccess;
using Daxor.Lab.MessageManager.Common;
using Daxor.Lab.SettingsManager;
using Microsoft.Practices.Composite.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Daxor.Lab.MessageManager.IntegrationTests.SystemMessageManager_Tests
{
    [TestClass]
    // ReSharper disable InconsistentNaming
    public class When_getting_a_message
    {
        #region Fields

        private static SystemMessageManager _messageManager;

        #endregion

        [ClassInitialize]
        public static void Initialize(TestContext ignored)
        {
            var dataService = new SettingsManagerDataService();
            var settingsManager = new SystemSettingsManager(Substitute.For<ILoggerFacade>(), dataService);
            var messageRepository = new DatabaseMessageRepository(settingsManager);
            _messageManager = new SystemMessageManager(messageRepository);
        }

        #region BVA alerts and errors

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_automatic_point_exclusion_completion_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaAutomaticPointExclusionCompletionFailed);

            Assert.AreEqual("Automatic point exclusion did not complete successfully.\n\nThe original exclusion state of the samples has been restored. Please call DAXOR Customer Support at 1-866-548-7282.\n[Error: BVA 34]", message.FormattedMessage);
            Assert.AreEqual("Automatic point exclusion did not complete successfully.", message.Description);
            Assert.AreEqual("BVA 34", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("The original exclusion state of the samples has been restored. Please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_automatic_point_exclusion_could_not_lower_standard_deviation_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaAutomaticPointExclusionCouldNotLowerStdDev);

            Assert.AreEqual("Automatic point exclusion could not find a set of candidate points that, when excluded, brings the standard deviation below 3.9%.\n\nPlease call DAXOR Customer Support at 1-866-548-7282.\n[Alert: BVA 63]", message.FormattedMessage);
            Assert.AreEqual("Automatic point exclusion could not find a set of candidate points that, when excluded, brings the standard deviation below 3.9%.", message.Description);
            Assert.AreEqual("BVA 63", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_automatic_point_exclusion_with_high_residual_squared_error_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaAutomaticPointExclusionHighRse);

            Assert.AreEqual("Samples have been automatically excluded to bring the standard deviation below 3.9%. Although the test results are reportable, DAXOR would like to review the results.\n\nPlease call DAXOR Customer Support at 1-866-548-7282.\n[Alert: BVA 64]", message.FormattedMessage);
            Assert.AreEqual("Samples have been automatically excluded to bring the standard deviation below 3.9%. Although the test results are reportable, DAXOR would like to review the results.", message.Description);
            Assert.AreEqual("BVA 64", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_automatic_point_exclusion_no_further_evaluation_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaAutomaticPointExclusionNoFurtherEvaluation);

            Assert.AreEqual("Re-including a sample that was automatically excluded will disable any further automatic point exclusion on this BVA test.\n\nAre you sure you want to override the exclusion?\n[Alert: BVA 43]", message.FormattedMessage);
            Assert.AreEqual("Re-including a sample that was automatically excluded will disable any further automatic point exclusion on this BVA test.", message.Description);
            Assert.AreEqual("BVA 43", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Are you sure you want to override the exclusion?", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_changing_existing_patient_ID_to_new_patient_ID_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaChangingExistingPatientIdToNewPatientId);

            Assert.AreEqual("The entered patient ID ({0}) does not exist in the database. Do you want to create a new patient with that ID?\n\n(Choosing 'Use Selected' will undo the change to the patient ID.)\n\n\n[Alert: BVA 49]", message.FormattedMessage);
            Assert.AreEqual("The entered patient ID ({0}) does not exist in the database. Do you want to create a new patient with that ID?\n\n(Choosing 'Use Selected' will undo the change to the patient ID.)", message.Description);
            Assert.AreEqual("BVA 49", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_changing_existing_patient_ID_to_other_existing_patient_ID_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaChangingExistingPatientIdToOtherExistingPatientId);

            Assert.AreEqual("The patient ID you have entered is already associated with the selected patient shown below.|Do you want to use the selected patient?\n\n(Choosing 'Use Selected' will discard the patient information already entered and replace it with the selected patient's information.)\n\n\n[Alert: BVA 48]", message.FormattedMessage);
            Assert.AreEqual("The patient ID you have entered is already associated with the selected patient shown below.|Do you want to use the selected patient?\n\n(Choosing 'Use Selected' will discard the patient information already entered and replace it with the selected patient's information.)", message.Description);
            Assert.AreEqual("BVA 48", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_changing_new_patient_ID_to_existing_patient_ID_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaChangingNewPatientIdToExistingPatientId);

            Assert.AreEqual("The patient ID you have entered is already associated with the selected patient shown below.|Do you want to use the selected patient?\n\n(Choosing 'Selected Patient' will discard the patient information already entered and replace it with the selected patient's information.)\n\n\n[Alert: BVA 47]", message.FormattedMessage);
            Assert.AreEqual("The patient ID you have entered is already associated with the selected patient shown below.|Do you want to use the selected patient?\n\n(Choosing 'Selected Patient' will discard the patient information already entered and replace it with the selected patient's information.)", message.Description);
            Assert.AreEqual("BVA 47", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_create_test_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaCreateTestFailed);

            Assert.AreEqual("A new BVA test could not be created.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: BVA 35]", message.FormattedMessage);
            Assert.AreEqual("A new BVA test could not be created.", message.Description);
            Assert.AreEqual("BVA 35", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_daily_QC_due_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaDailyQCDue);

            Assert.AreEqual("{0} QC is due at this time. Do you want to run {0} QC now or continue with this Blood Volume Analysis?\n\nPlease call DAXOR Customer Support at 1-866-548-7282 for any further assistance.\n[Alert: BVA 51]", message.FormattedMessage);
            Assert.AreEqual("{0} QC is due at this time. Do you want to run {0} QC now or continue with this Blood Volume Analysis?", message.Description);
            Assert.AreEqual("BVA 51", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_date_of_birth_more_than_100_years_old_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaDateOfBirth100YearsOld);

            Assert.AreEqual("The entered date of birth indicates that the patient is more than 100 years old.\n\nPlease verify that the date of birth has been entered correctly.\n[Alert: BVA 1]", message.FormattedMessage);
            Assert.AreEqual("The entered date of birth indicates that the patient is more than 100 years old.", message.Description);
            Assert.AreEqual("BVA 1", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify that the date of birth has been entered correctly.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_date_of_birth_after_test_date_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaDateOfBirthAfterTestDate);

            Assert.AreEqual("The date of birth cannot be after the current test date.\n\nPlease correct the date of birth field.\n[Alert: BVA 2]", message.FormattedMessage);
            Assert.AreEqual("The date of birth cannot be after the current test date.", message.Description);
            Assert.AreEqual("BVA 2", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please correct the date of birth field.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_date_of_birth_invalid_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaDateOfBirthInvalid);

            Assert.AreEqual("The entered date of birth is not valid.\n\nPlease correct the date of birth field.\n[Alert: BVA 3]", message.FormattedMessage);
            Assert.AreEqual("The entered date of birth is not valid.", message.Description);
            Assert.AreEqual("BVA 3", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please correct the date of birth field.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_date_of_birth_beyond_max_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaDateOfBirthMax);

            Assert.AreEqual("The entered date of birth indicates that the patient is more than 150 years old.\n\nPlease verify that the date of birth has been entered correctly.\n[Alert: BVA 4]", message.FormattedMessage);
            Assert.AreEqual("The entered date of birth indicates that the patient is more than 150 years old.", message.Description);
            Assert.AreEqual("BVA 4", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify that the date of birth has been entered correctly.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_differing_patient_information_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaDifferingPatientInformation);

            Assert.AreEqual("The patient information entered differs from what's in the database.|Do you want to update existing information about this patient to use the entered information instead?\n\n('Keep Changes' will update this patient's information for this and all prior tests. 'Discard Changes' will restore the patient information for this test to what's in the database and prior tests will not be affected.\n\n\n[Alert: BVA 46]", message.FormattedMessage);
            Assert.AreEqual("The patient information entered differs from what's in the database.|Do you want to update existing information about this patient to use the entered information instead?\n\n('Keep Changes' will update this patient's information for this and all prior tests. 'Discard Changes' will restore the patient information for this test to what's in the database and prior tests will not be affected.", message.Description);
            Assert.AreEqual("BVA 46", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_BVA_export_device_not_ready_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaExportDeviceNotReady);

            Assert.AreEqual("Device is not ready: There does not appear to be a writable disc in the drive.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: BVA 39]", message.FormattedMessage);
            Assert.AreEqual("Device is not ready: There does not appear to be a writable disc in the drive.",
                message.Description);
            Assert.AreEqual("BVA 39", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_BVA_export_disc_not_finalized_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaExportDiscFinalized);

            Assert.AreEqual("A blank disc is required for exporting reports.\n\nPlease try the export operation with another disc.\n[Error: BVA 41]", message.FormattedMessage);
            Assert.AreEqual("A blank disc is required for exporting reports.", message.Description);
            Assert.AreEqual("BVA 41", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please try the export operation with another disc.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_BVA_export_disc_readonly_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaExportDiscReadonly);

            Assert.AreEqual("This disc cannot be written to.\n\nPlease try the export operation with another disc.\n[Error: BVA 40]", message.FormattedMessage);
            Assert.AreEqual("This disc cannot be written to.", message.Description);
            Assert.AreEqual("BVA 40", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please try the export operation with another disc.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_BVA_export_path_invalid_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaExportPathInvalid);

            Assert.AreEqual("The selected location is not valid.\n\nPlease choose another file location.\n[Error: BVA 38]", message.FormattedMessage);
            Assert.AreEqual("The selected location is not valid.", message.Description);
            Assert.AreEqual("BVA 38", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please choose another file location.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_export_unsuccessful_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaExportUnsuccessful);

            Assert.AreEqual("The export did not complete successfully.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 for any further assistance.\n[Error: BVA 65]", message.FormattedMessage);
            Assert.AreEqual("The export did not complete successfully.", message.Description);
            Assert.AreEqual("BVA 65", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_failed_QC_state_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaFailedQCState);

            Assert.AreEqual("A Blood Volume Analysis cannot be performed at this time because the system is in a failed QC state. Please address the QC failure before attempting to proceed.\n\nIf the QC failure cannot be resolved, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: BVA 37]", message.FormattedMessage);
            Assert.AreEqual("A Blood Volume Analysis cannot be performed at this time because the system is in a failed QC state. Please address the QC failure before attempting to proceed.", message.Description);
            Assert.AreEqual("BVA 37", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the QC failure cannot be resolved, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_height_greater_than_the_max_supported_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaHeightMaxSupported);

            Assert.AreEqual("The entered height is greater than the maximum supported height of 86.0 in. or 218.44 cm.\n\nPlease verify the height. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 5]", message.FormattedMessage);
            Assert.AreEqual("The entered height is greater than the maximum supported height of 86.0 in. or 218.44 cm.", message.Description);
            Assert.AreEqual("BVA 5", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the height. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_height_less_than_the_min_supported_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaHeightMinSupported);

            Assert.AreEqual("The entered height is less than the minimum supported height of 48.0 in. or 121.92 cm.\n\nPlease verify the height. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 6]", message.FormattedMessage);
            Assert.AreEqual("The entered height is less than the minimum supported height of 48.0 in. or 121.92 cm.", message.Description);
            Assert.AreEqual("BVA 6", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the height. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_hematocrit_A_exceeds_average_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaHematocritAExceedsAverage);

            Assert.AreEqual("Hematocrit A exceeds the average test hematocrit by more than 2 points.\n\nPlease verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 7]", message.FormattedMessage);
            Assert.AreEqual("Hematocrit A exceeds the average test hematocrit by more than 2 points.", message.Description);
            Assert.AreEqual("BVA 7", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_hematocrit_B_exceeds_average_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaHematocritBExceedsAverage);

            Assert.AreEqual("Hematocrit B exceeds the average test hematocrit by more than 2 points.\n\nPlease verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 8]", message.FormattedMessage);
            Assert.AreEqual("Hematocrit B exceeds the average test hematocrit by more than 2 points.", message.Description);
            Assert.AreEqual("BVA 8", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_hematocrit_exclusion_not_allowed_average_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaHematocritExclusionNotAllowed);

            Assert.AreEqual("The hematocrit cannot be excluded unless the Hematocrit Entry field is set to \"Two Per Sample.\"\n\n\n[Alert: BVA 50]", message.FormattedMessage);
            Assert.AreEqual("The hematocrit cannot be excluded unless the Hematocrit Entry field is set to \"Two Per Sample.\"", message.Description);
            Assert.AreEqual("BVA 50", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_hematocrit_A_unusually_high_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaHematocritHighA);

            Assert.AreEqual("Hematocrit A is unusually high.\n\nPlease verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 9]", message.FormattedMessage);
            Assert.AreEqual("Hematocrit A is unusually high.", message.Description);
            Assert.AreEqual("BVA 9", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_hematocrit_B_unusually_high_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaHematocritHighB);

            Assert.AreEqual("Hematocrit B is unusually high.\n\nPlease verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 10]", message.FormattedMessage);
            Assert.AreEqual("Hematocrit B is unusually high.", message.Description);
            Assert.AreEqual("BVA 10", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_low_hematocrit_A_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaHematocritLowA);

            Assert.AreEqual("Hematocrit A is below 20%.\n\nPlease verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 11]", message.FormattedMessage);
            Assert.AreEqual("Hematocrit A is below 20%.", message.Description);
            Assert.AreEqual("BVA 11", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_low_hematocrit_B_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaHematocritLowB);

            Assert.AreEqual("Hematocrit B is below 20%.\n\nPlease verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 12]", message.FormattedMessage);
            Assert.AreEqual("Hematocrit B is below 20%.", message.Description);
            Assert.AreEqual("BVA 12", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_mismatch_between_hematocrit_A_and_B_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaHematocritMismatchBetweenAandB);

            Assert.AreEqual("Hematocrit A should not differ from Hematocrit B by more than two (2) points.\n\nPlease verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 13]", message.FormattedMessage);
            Assert.AreEqual("Hematocrit A should not differ from Hematocrit B by more than two (2) points.", message.Description);
            Assert.AreEqual("BVA 13", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_mismatch_between_hematocrit_B_and_A_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaHematocritMismatchBetweenBandA);

            Assert.AreEqual("Hematocrit B should not differ from Hematocrit A by more than two (2) points.\n\nPlease verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 14]", message.FormattedMessage);
            Assert.AreEqual("Hematocrit B should not differ from Hematocrit A by more than two (2) points.", message.Description);
            Assert.AreEqual("BVA 14", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the hematocrit. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_injectate_lot_number_invalid_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaInjecateLotNumberInvalid);

            Assert.AreEqual("The lot number for {0} is not valid. Please rescan or manually enter the lot number.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 for any further assistance.\n[Error: BVA 54]", message.FormattedMessage);
            Assert.AreEqual("The lot number for {0} is not valid. Please rescan or manually enter the lot number.", message.Description);
            Assert.AreEqual("BVA 54", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_injectate_dose_in_use_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaInjectateDoseInUse);

            Assert.AreEqual("This injectate dose is in use by another patient.\n\nThe injectate key will now be cleared.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 for any further assistance.\n[Error: BVA 59]", message.FormattedMessage);
            Assert.AreEqual("This injectate dose is in use by another patient.\n\nThe injectate key will now be cleared.", message.Description);
            Assert.AreEqual("BVA 59", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_injectate_key_invalid_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaInjectateKeyInvalid);

            Assert.AreEqual("The {0} key entered {1} is not valid. Please rescan or manually enter the key.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 for any further assistance.\n[Error: BVA 55]", message.FormattedMessage);
            Assert.AreEqual("The {0} key entered {1} is not valid. Please rescan or manually enter the key.", message.Description);
            Assert.AreEqual("BVA 55", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_last_two_hematocrits_too_different_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaLastTwoHematocritsTooDifferent);

            const string expectedMessage = "To use the \"Last Two\" hematocrit entry option, the hematocrit results for the last two samples must agree within {0:0}%.\n\nBoth hematocrit results will now be cleared.\n\nPlease call DAXOR Customer Support at 1-888-774-3268 for any further assistance.\n[Error: BVA 52]";
            const string expectedDescription = "To use the \"Last Two\" hematocrit entry option, the hematocrit results for the last two samples must agree within {0:0}%.\n\nBoth hematocrit results will now be cleared.";

            Assert.AreEqual(expectedMessage, message.FormattedMessage);
            Assert.AreEqual(expectedDescription, message.Description);
            Assert.AreEqual("BVA 52", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-888-774-3268 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_BVA_load_test_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaLoadTestFailed);

            Assert.AreEqual("The requested test could not be loaded from the database.\n\nIf the problem persists, please call DAXOR Customer Support 1-866-548-7282.\n[Error: BVA 36]", message.FormattedMessage);
            Assert.AreEqual("The requested test could not be loaded from the database.", message.Description);
            Assert.AreEqual("BVA 36", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_locked_patient_fields_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaLockedPatientFields);

            Assert.AreEqual("There is currently another BVA test being run for this patient. Until that test completes, patient-specific fields on this test cannot be modified.\n\n\n[Alert: BVA 44]", message.FormattedMessage);
            Assert.AreEqual("There is currently another BVA test being run for this patient. Until that test completes, patient-specific fields on this test cannot be modified.", message.Description);
            Assert.AreEqual("BVA 44", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_lot_numbers_mismatched_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaLotNumbersMismatched);

            Assert.AreEqual("The lot numbers for the {0} do not match.\n\n{1}\n\nPlease check that the lot numbers match and rescan or manually enter\n the {2} keys.\n\nAll product keys will now be cleared.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 for any further assistance.\n[Error: BVA 58]", message.FormattedMessage);
            Assert.AreEqual("The lot numbers for the {0} do not match.\n\n{1}\n\nPlease check that the lot numbers match and rescan or manually enter\n the {2} keys.\n\nAll product keys will now be cleared.", message.Description);
            Assert.AreEqual("BVA 58", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_measured_blood_volume_greater_than_the_max_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaMeasuredBloodVolumeMax);

            Assert.AreEqual("The measured blood volume is greater than 14,500 mL.\n\nPlease call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 15]", message.FormattedMessage);
            Assert.AreEqual("The measured blood volume is greater than 14,500 mL.", message.Description);
            Assert.AreEqual("BVA 15", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_measured_blood_volume_less_than_the_min_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaMeasuredBloodVolumeMin);

            Assert.AreEqual("The measured blood volume is less than 1,700 mL.\n\nPlease call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 16]", message.FormattedMessage);
            Assert.AreEqual("The measured blood volume is less than 1,700 mL.", message.Description);
            Assert.AreEqual("BVA 16", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_no_blank_patient_ID_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaNoBlankPatientId);

            Assert.AreEqual("The patient ID cannot be blank for non-pending tests.\n\n\n[Alert: BVA 45]", message.FormattedMessage);
            Assert.AreEqual("The patient ID cannot be blank for non-pending tests.", message.Description);
            Assert.AreEqual("BVA 45", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_post_injection_time_less_than_previous_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaPostInjTimeLessThanPrev);

            Assert.AreEqual("The post-injection time should not be less than the previous sample's post-injection time.\n\nPlease verify the post-injection time.\n[Alert: BVA 17]", message.FormattedMessage);
            Assert.AreEqual("The post-injection time should not be less than the previous sample's post-injection time.", message.Description);
            Assert.AreEqual("BVA 17", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the post-injection time.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_post_injection_time_too_long_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaPostInjTimeLong);

            Assert.AreEqual("Blood draws greater than 75 minutes post injection may adversely affect test results.\n\nPlease verify the post-injection time. For specimens collected after 75 minutes, please contact Daxor Clinical Support at 1-888-774-3268 for additional guidance.\n[Alert: BVA 18]", message.FormattedMessage);
            Assert.AreEqual("Blood draws greater than 75 minutes post injection may adversely affect test results.", message.Description);
            Assert.AreEqual("BVA 18", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the post-injection time. For specimens collected after 75 minutes, please contact Daxor Clinical Support at 1-888-774-3268 for additional guidance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_post_injection_time_too_short_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaPostInjTimeShort);

            Assert.AreEqual("Blood draws less than 10 minutes post-injection may adversely affect test results.\n\nPlease verify the post-injection time. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 19]", message.FormattedMessage);
            Assert.AreEqual("Blood draws less than 10 minutes post-injection may adversely affect test results.", message.Description);
            Assert.AreEqual("BVA 19", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the post-injection time. If it is correct, call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_post_injection_time_more_than_subsequent_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaPostInjTimeMoreThanSubsequent);

            Assert.AreEqual("The post-injection time should not be more than the subsequent sample's post-injection time.\n\nPlease verify the post-injection time.\n[Alert: BVA 42]", message.FormattedMessage);
            Assert.AreEqual("The post-injection time should not be more than the subsequent sample's post-injection time.", message.Description);
            Assert.AreEqual("BVA 42", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the post-injection time.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_duration_too_long_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaSampleDurationTooLong);

            Assert.AreEqual("An extremely long duration (sample count time) may decrease the accuracy of test results.\n\nPlease verify the duration. If assistance is needed, please call DAXOR Customer Support at 1-866-548-7282.\n[Alert: BVA 20]", message.FormattedMessage);
            Assert.AreEqual("An extremely long duration (sample count time) may decrease the accuracy of test results.", message.Description);
            Assert.AreEqual("BVA 20", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the duration. If assistance is needed, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_A_less_than_baseline_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaSampleALessThanBaseline);

            Assert.AreEqual("The counts for Sample A are less than the average baseline counts.\n\nPlease call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 66]", message.FormattedMessage);
            Assert.AreEqual("The counts for Sample A are less than the average baseline counts.", message.Description);
            Assert.AreEqual("BVA 66", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_B_less_than_baseline_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaSampleBLessThanBaseline);

            Assert.AreEqual("The counts for Sample B are less than the average baseline counts.\n\nPlease call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 67]", message.FormattedMessage);
            Assert.AreEqual("The counts for Sample B are less than the average baseline counts.", message.Description);
            Assert.AreEqual("BVA 67", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_A_close_to_baseline_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaSamplesACloseToBaseline);

            Assert.AreEqual("The counts for Sample A are less than twice the average baseline counts.\n\nPlease call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 21]", message.FormattedMessage);
            Assert.AreEqual("The counts for Sample A are less than twice the average baseline counts.", message.Description);
            Assert.AreEqual("BVA 21", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_B_close_to_baseline_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaSamplesBCloseToBaseline);

            Assert.AreEqual("The counts for Sample B are less than twice the average baseline counts.\n\nPlease call DAXOR Clinical Support at 1-888-774-3268.\n[Alert: BVA 22]", message.FormattedMessage);
            Assert.AreEqual("The counts for Sample B are less than twice the average baseline counts.", message.Description);
            Assert.AreEqual("BVA 22", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please call DAXOR Clinical Support at 1-888-774-3268.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_counts_differ_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaSamplesDiffer);

            Assert.AreEqual("The difference between counts for Sample A and Sample B is higher than expected.\n\nThe difference is most likely due to an error that occurred while pipetting one of the samples. For assistance, please call DAXOR Customer Support at 1-866-548-7282.\n[Alert: BVA 23]", message.FormattedMessage);
            Assert.AreEqual("The difference between counts for Sample A and Sample B is higher than expected.", message.Description);
            Assert.AreEqual("BVA 23", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("The difference is most likely due to an error that occurred while pipetting one of the samples. For assistance, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_save_test_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaSaveTestFailed);

            Assert.AreEqual("The BVA test could not be saved.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: BVA 68]", message.FormattedMessage);
            Assert.AreEqual("The BVA test could not be saved.", message.Description);
            Assert.AreEqual("BVA 68", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_saving_test_will_clear_keys_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaSavingTestWillClearKeys);

            Assert.AreEqual("Injectate/Standard keys have been entered. Leaving this section, will erase the injectate lot and the injectate/standard keys.\n\nAre you sure you want to proceed?\n\nPlease call DAXOR Customer Support at 1-866-548-7282 for any further assistance.\n[Error: BVA 61]", message.FormattedMessage);
            Assert.AreEqual("Injectate/Standard keys have been entered. Leaving this section, will erase the injectate lot and the injectate/standard keys.\n\nAre you sure you want to proceed?", message.Description);
            Assert.AreEqual("BVA 61", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_scanner_input_not_allowed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaScannerInputNotAllowed);

            Assert.AreEqual("This field does not support scanner input.\n\nValid input fields are Injectate Key, Standard A Key, and Standard B Key.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 for any further assistance.\n[Error: BVA 57]", message.FormattedMessage);
            Assert.AreEqual("This field does not support scanner input.\n\nValid input fields are Injectate Key, Standard A Key, and Standard B Key.", message.Description);
            Assert.AreEqual("BVA 57", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_standard_keys_identical_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaStandardKeysIdentical);

            Assert.AreEqual("Do you want to proceed with using the same standard for Standard A and Standard B?\n\nPlease call DAXOR Customer Support at 1-866-548-7282 for any further assistance.\n[Alert: BVA 62]", message.FormattedMessage);
            Assert.AreEqual("Do you want to proceed with using the same standard for Standard A and Standard B?", message.Description);
            Assert.AreEqual("BVA 62", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_standards_counts_differ_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaStandardsDiffer);

            Assert.AreEqual("The difference between the counts for Standard A and Standard B is higher than expected.\n\nPlease centrifuge the standards for approximately 30 seconds and re-run the test. If the issue persists, call DAXOR Customer Support at 1-866-548-7282. \n[Alert: BVA 24]", message.FormattedMessage);
            Assert.AreEqual("The difference between the counts for Standard A and Standard B is higher than expected.", message.Description);
            Assert.AreEqual("BVA 24", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please centrifuge the standards for approximately 30 seconds and re-run the test. If the issue persists, call DAXOR Customer Support at 1-866-548-7282. ", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_low_standard_A_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaStandardsLowA);

            Assert.AreEqual("Standard A is below 10,000 counts.\n\nPlease centrifuge the standards for approximately 30 seconds and re-run the test. If the issue persists, call DAXOR Customer Support at 1-866-548-7282. \n[Alert: BVA 25]", message.FormattedMessage);
            Assert.AreEqual("Standard A is below 10,000 counts.", message.Description);
            Assert.AreEqual("BVA 25", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please centrifuge the standards for approximately 30 seconds and re-run the test. If the issue persists, call DAXOR Customer Support at 1-866-548-7282. ", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_low_standard_B_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaStandardsLowB);

            Assert.AreEqual("Standard B is below 10,000 counts.\n\nPlease centrifuge the standards for approximately 30 seconds and re-run the test. If the issue persists, call DAXOR Customer Support at 1-866-548-7282. \n[Alert: BVA 26]", message.FormattedMessage);
            Assert.AreEqual("Standard B is below 10,000 counts.", message.Description);
            Assert.AreEqual("BVA 26", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please centrifuge the standards for approximately 30 seconds and re-run the test. If the issue persists, call DAXOR Customer Support at 1-866-548-7282. ", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_standard_deviation_is_extreme_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaStdDevExtreme);

            Assert.AreEqual("The standard deviation cannot exceed 3.9%.\n\nPlease examine the test data for outliers. If assistance is needed, call DAXOR Clinical Support at 1-888-774-3268. \n[Alert: BVA 27]", message.FormattedMessage);
            Assert.AreEqual("The standard deviation cannot exceed 3.9%.", message.Description);
            Assert.AreEqual("BVA 27", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please examine the test data for outliers. If assistance is needed, call DAXOR Clinical Support at 1-888-774-3268. ", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_test_deletion_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaTestDeletionFailed);

            Assert.AreEqual("An error occurred while deleting the BVA test.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: BVA 53]", message.FormattedMessage);
            Assert.AreEqual("An error occurred while deleting the BVA test.", message.Description);
            Assert.AreEqual("BVA 53", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_test_mode_changing_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaTestModeChanging);

            Assert.AreEqual("Once the test mode has changed, it cannot be changed again.\n\nAre you sure you want to change the test mode?\n\nPlease call DAXOR Customer Support at 1-866-548-7282 for any further assistance.\n[Alert: BVA 60]", message.FormattedMessage);
            Assert.AreEqual("Once the test mode has changed, it cannot be changed again.\n\nAre you sure you want to change the test mode?", message.Description);
            Assert.AreEqual("BVA 60", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_reverse_transudation_rate_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaTransudationReverse);

            Assert.AreEqual("The transudation rate (slope) is negative.\n\nPlease verify that the patient samples are in the correct positions. If they are correct, call DAXOR Clinical Support at 1-888-774-3268. \n[Alert: BVA 29]", message.FormattedMessage);
            Assert.AreEqual("The transudation rate (slope) is negative.", message.Description);
            Assert.AreEqual("BVA 29", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify that the patient samples are in the correct positions. If they are correct, call DAXOR Clinical Support at 1-888-774-3268. ", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_unexpected_product_key_type_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaUnexpectedProductKeyType);

            Assert.AreEqual("A key for the {0} was entered, but a key for the {1} was expected.\n\n Please select the appropriate input field and rescan or manually enter the {2} key.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 for any further assistance.\n[Error: BVA 56]", message.FormattedMessage);
            Assert.AreEqual("A key for the {0} was entered, but a key for the {1} was expected.\n\n Please select the appropriate input field and rescan or manually enter the {2} key.", message.Description);
            Assert.AreEqual("BVA 56", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_high_weight_deviation_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaWeightHighDev);

            Assert.AreEqual("The deviation from the ideal weight is greater than the supported range of -40% to +260%.\n\nPlease verify the height and weight. If they are correct, call DAXOR Clinical Support at 1-888-774-3268. \n[Alert: BVA 31]", message.FormattedMessage);
            Assert.AreEqual("The deviation from the ideal weight is greater than the supported range of -40% to +260%.", message.Description);
            Assert.AreEqual("BVA 31", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the height and weight. If they are correct, call DAXOR Clinical Support at 1-888-774-3268. ", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_weight_deviation_greater_than_the_max_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaWeightMaxDev);

            Assert.AreEqual("The deviation from the ideal weight is greater than the supported maximum of +350%.\n\nPlease verify the height and weight. If they are correct, call DAXOR Clinical Support at 1-888-774-3268. \n[Alert: BVA 32]", message.FormattedMessage);
            Assert.AreEqual("The deviation from the ideal weight is greater than the supported maximum of +350%.", message.Description);
            Assert.AreEqual("BVA 32", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the height and weight. If they are correct, call DAXOR Clinical Support at 1-888-774-3268. ", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_weight_deviation_below_the_min_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.BvaWeightMinDev);

            Assert.AreEqual("The deviation from the ideal weight is less than the supported minimum of -40%.\n\nPlease verify the height and weight. If they are correct, call DAXOR Clinical Support at 1-888-774-3268. \n[Alert: BVA 33]", message.FormattedMessage);
            Assert.AreEqual("The deviation from the ideal weight is less than the supported minimum of -40%.", message.Description);
            Assert.AreEqual("BVA 33", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("Please verify the height and weight. If they are correct, call DAXOR Clinical Support at 1-888-774-3268. ", message.Resolution);
        }

        #endregion

        #region General alerts and errors

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_automatic_backup_failure_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysAutomaticBackupFailure);

            Assert.AreEqual("The automatic backup failed to complete.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 to resolve this issue.\n[Error: SYS 15]", message.FormattedMessage);
            Assert.AreEqual("The automatic backup failed to complete.", message.Description);
            Assert.AreEqual("SYS 15", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_automatic_backup_location_not_found_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysAutomaticBackupLocationNotFound);

            Assert.AreEqual("The location {0} could not be found. Saving backup to default location.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 to resolve this issue.\n[Error: SYS 16]", message.FormattedMessage);
            Assert.AreEqual("The location {0} could not be found. Saving backup to default location.", message.Description);
            Assert.AreEqual("SYS 16", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_default_backup_location_not_found_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysDefaultBackupLocationNotFound);

            Assert.AreEqual("The location {0} could not be found. Could not find the default location.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 to resolve this issue.\n[Error: SYS 14]", message.FormattedMessage);
            Assert.AreEqual("The location {0} could not be found. Could not find the default location.", message.Description);
            Assert.AreEqual("SYS 14", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_gamma_counter_disconnected_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysGammaCounterDisconnected);

            Assert.AreEqual("The connection to the gamma counter was lost. Any currently running test will be aborted.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing. Choosing 'Reboot' will reboot the system.\n[Error: SYS 4]", message.FormattedMessage);
            Assert.AreEqual("The connection to the gamma counter was lost. Any currently running test will be aborted.", message.Description);
            Assert.AreEqual("SYS 4", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing. Choosing 'Reboot' will reboot the system.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_general_error_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysGeneralError);

            Assert.AreEqual("A general error has occurred and the application will close.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: SYS 2]", message.FormattedMessage);
            Assert.AreEqual("A general error has occurred and the application will close.", message.Description);
            Assert.AreEqual("SYS 2", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_module_failed_to_load_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysModuleLoadFailed);

            Assert.AreEqual("The application could not be started because one or more modules could not be initialized properly.\n\nThe application will now close. If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: SYS 1]", message.FormattedMessage);
            Assert.AreEqual("The application could not be started because one or more modules could not be initialized properly.", message.Description);
            Assert.AreEqual("SYS 1", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("The application will now close. If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_changer_motor_stalled_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysMotorStalled);

            Assert.AreEqual("The sample changer motor has stalled and cannot move. Any currently running test will be aborted.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.\n[Error: SYS 7]", message.FormattedMessage);
            Assert.AreEqual("The sample changer motor has stalled and cannot move. Any currently running test will be aborted.", message.Description);
            Assert.AreEqual("SYS 7", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_no_printers_connected_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysNoPrintersConnected);

            Assert.AreEqual("There are no printers that are properly configured.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.\n[Error: SYS 8]", message.FormattedMessage);
            Assert.AreEqual("There are no printers that are properly configured.", message.Description);
            Assert.AreEqual("SYS 8", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_printer_reset_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysPrinterResetFailed);

            Assert.AreEqual("The printer could not be reset.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.\n[Error: SYS 9]", message.FormattedMessage);
            Assert.AreEqual("The printer could not be reset.", message.Description);
            Assert.AreEqual("SYS 9", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_changer_disconnected_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysSampleChangerDisconnected);

            Assert.AreEqual("The connection to the sample changer was lost. Any currently running test will be aborted.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing. Choosing 'Reboot' will reboot the system.\n[Error: SYS 5]", message.FormattedMessage);
            Assert.AreEqual("The connection to the sample changer was lost. Any currently running test will be aborted.", message.Description);
            Assert.AreEqual("SYS 5", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing. Choosing 'Reboot' will reboot the system.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_sample_changer_unexpectedly_moved_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysSampleChangerUnexpectedMovement);

            Assert.AreEqual("The sample changer made an unexpected move. Any currently running test will be aborted.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.\n[Error: SYS 6]", message.FormattedMessage);
            Assert.AreEqual("The sample changer made an unexpected move. Any currently running test will be aborted.", message.Description);
            Assert.AreEqual("SYS 6", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_invalid_shutdown_password_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysShutdownPasswordInvalid);

            Assert.AreEqual("The password provided is incorrect.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 for any further assistance.\n[Error: SYS 10]", message.FormattedMessage);
            Assert.AreEqual("The password provided is incorrect.", message.Description);
            Assert.AreEqual("SYS 10", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_SQL_connection_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysSqlConnectionFailed);

            Assert.AreEqual("A connection to the database could not be established.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: SYS 3]", message.FormattedMessage);
            Assert.AreEqual("A connection to the database could not be established.", message.Description);
            Assert.AreEqual("SYS 3", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_test_is_executing_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysTestIsExecuting);

            Assert.AreEqual("A test is in progress. {0} will abort the test.\n\n\n[Alert: SYS 13]", message.FormattedMessage);
            Assert.AreEqual("A test is in progress. {0} will abort the test.", message.Description);
            Assert.AreEqual("SYS 13", message.MessageId);
            Assert.AreEqual(MessageType.Alert, message.MessageType);
            Assert.AreEqual("", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_touch_screen_calibration_does_not_exist_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysTouchScreenCalibrationDoesntExist);

            Assert.AreEqual("The touchscreen calibration program could not be found.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: SYS 11]", message.FormattedMessage);
            Assert.AreEqual("The touchscreen calibration program could not be found.", message.Description);
            Assert.AreEqual("SYS 11", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_touch_screen_calibration_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.SysTouchScreenCalibrationFailed);

            Assert.AreEqual("The touchscreen calibration program encountered an error. The calibration has failed.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: SYS 12]", message.FormattedMessage);
            Assert.AreEqual("The touchscreen calibration program encountered an error. The calibration has failed.", message.Description);
            Assert.AreEqual("SYS 12", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        #endregion

        #region QC alerts and errors

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_test_abortion_timeout_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.QcAbortTestTimedOut);

            Assert.AreEqual("The QC test is taking longer than expected to abort.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: QC 9]", message.FormattedMessage);
            Assert.AreEqual("The QC test is taking longer than expected to abort.", message.Description);
            Assert.AreEqual("QC 9", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_export_device_not_ready_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.QcExportDeviceNotReady);

            Assert.AreEqual("Device is not ready: There does not appear to be a writable disc in the drive.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: QC 3]", message.FormattedMessage);
            Assert.AreEqual("Device is not ready: There does not appear to be a writable disc in the drive.", message.Description);
            Assert.AreEqual("QC 3", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_export_disc_finalized_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.QcExportDiscFinalized);

            Assert.AreEqual("A blank disc is required for exporting reports.\n\nPlease try the export operation with another disc.\n[Error: QC 5]", message.FormattedMessage);
            Assert.AreEqual("A blank disc is required for exporting reports.", message.Description);
            Assert.AreEqual("QC 5", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please try the export operation with another disc.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_export_disc_read_only_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.QcExportDiscReadonly);

            Assert.AreEqual("This disc cannot be written to.\n\nPlease try the export operation with another disc.\n[Error: QC 4]", message.FormattedMessage);
            Assert.AreEqual("This disc cannot be written to.", message.Description);
            Assert.AreEqual("QC 4", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please try the export operation with another disc.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_QC_export_path_invalid_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.QcExportPathInvalid);

            Assert.AreEqual("The selected location is not valid.\n\nPlease choose another file location.\n[Error: QC 7]", message.FormattedMessage);
            Assert.AreEqual("The selected location is not valid.", message.Description);
            Assert.AreEqual("QC 7", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please choose another file location.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_QC_load_test_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.QcLoadTestFailed);

            Assert.AreEqual("The QC test could not be loaded.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: QC 6]", message.FormattedMessage);
            Assert.AreEqual("The QC test could not be loaded.", message.Description);
            Assert.AreEqual("QC 6", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_test_execution_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.QcTestExecutionFailed);

            Assert.AreEqual("An error occurred while executing the QC Test.\n\nThe test has been aborted. Please call DAXOR Customer Support at 1-866-548-7282.\n[Error: QC 1]", message.FormattedMessage);
            Assert.AreEqual("An error occurred while executing the QC Test.", message.Description);
            Assert.AreEqual("QC 1", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("The test has been aborted. Please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_test_save_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.QcTestSaveTestFailed);

            Assert.AreEqual("The QC test could not be saved.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: QC 2]", message.FormattedMessage);
            Assert.AreEqual("The QC test could not be saved.", message.Description);
            Assert.AreEqual("QC 2", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_test_start_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.QcTestStartFailed);

            Assert.AreEqual("An error occurred while starting QC Test.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: QC 8]", message.FormattedMessage);
            Assert.AreEqual("An error occurred while starting QC Test.", message.Description);
            Assert.AreEqual("QC 8", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        #endregion

        #region Utility alerts and errors

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_backup_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.UtilityBackupFailed);

            Assert.AreEqual("An error occurred while attempting to backup the databases.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.\n[Error: UTIL 10]", message.FormattedMessage);
            Assert.AreEqual("An error occurred while attempting to backup the databases.", message.Description);
            Assert.AreEqual("UTIL 10", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 to resolve this issue before continuing.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_utility_export_device_not_ready_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.UtilityExportDeviceNotReady);

            Assert.AreEqual("Device is not ready: There does not appear to be a writable disc in the drive.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: UTIL 4]", message.FormattedMessage);
            Assert.AreEqual("Device is not ready: There does not appear to be a writable disc in the drive.", message.Description);
            Assert.AreEqual("UTIL 4", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_utility_export_disc_finalized_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.UtilityExportDiscFinalized);

            Assert.AreEqual("A blank disc is required for exporting reports.\n\nPlease try the export operation with another disc.\n[Error: UTIL 6]", message.FormattedMessage);
            Assert.AreEqual("A blank disc is required for exporting reports.", message.Description);
            Assert.AreEqual("UTIL 6", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please try the export operation with another disc.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_utility_export_disc_read_only_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.UtilityExportDiscReadonly);

            Assert.AreEqual("This disc cannot be written to.\n\nPlease try the export operation with another disc.\n[Error: UTIL 5]", message.FormattedMessage);
            Assert.AreEqual("This disc cannot be written to.", message.Description);
            Assert.AreEqual("UTIL 5", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please try the export operation with another disc.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_utility_export_path_invalid_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.UtilityExportPathInvalid);

            Assert.AreEqual("The selected location is not valid.\n\nPlease choose another file location.\n[Error: UTIL 7]", message.FormattedMessage);
            Assert.AreEqual("The selected location is not valid.", message.Description);
            Assert.AreEqual("UTIL 7", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please choose another file location.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_failed_to_update_the_gamma_counter_settings_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.UtilityGammaCounterSettingsUpdateFailed);

            Assert.AreEqual("An error has occurred when attempting to update the gamma counter settings.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: UTIL 8]", message.FormattedMessage);
            Assert.AreEqual("An error has occurred when attempting to update the gamma counter settings.", message.Description);
            Assert.AreEqual("UTIL 8", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_login_unavailable_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.UtilityLoginUnavailable);

            Assert.AreEqual("This section of Utilities cannot be accessed while a test is in progress.\n\nPlease wait until the current test completes before accessing this section of Utilities.\n[Error: UTIL 9]", message.FormattedMessage);
            Assert.AreEqual("This section of Utilities cannot be accessed while a test is in progress.", message.Description);
            Assert.AreEqual("UTIL 9", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please wait until the current test completes before accessing this section of Utilities.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_ordering_physicians_lookup_failed_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.UtilityOrderingPhysiciansLookupFailed);

            Assert.AreEqual("The list of ordering physicians could not be obtained from the database.\n\nIf the problem persists, please call DAXOR Customer Support at 1-866-548-7282.\n[Error: UTIL 12]", message.FormattedMessage);
            Assert.AreEqual("The list of ordering physicians could not be obtained from the database.", message.Description);
            Assert.AreEqual("UTIL 12", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("If the problem persists, please call DAXOR Customer Support at 1-866-548-7282.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_invalid_password_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.UtilityPasswordInvalid);

            Assert.AreEqual("The password provided is incorrect.\n\nPlease call DAXOR Customer Support at 1-866-548-7282 for any further assistance.\n[Error: UTIL 1]", message.FormattedMessage);
            Assert.AreEqual("The password provided is incorrect.", message.Description);
            Assert.AreEqual("UTIL 1", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please call DAXOR Customer Support at 1-866-548-7282 for any further assistance.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_failed_to_restore_database_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.UtilityRestoreDatebaseFailed);

            Assert.AreEqual("An error occurred when attempting to restore the system databases. This error most likely occurred because the wrong backup file was chosen.\n\nPlease select a backup file containing {0} in the name.\n[Error: UTIL 2]", message.FormattedMessage);
            Assert.AreEqual("An error occurred when attempting to restore the system databases. This error most likely occurred because the wrong backup file was chosen.", message.Description);
            Assert.AreEqual("UTIL 2", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please select a backup file containing {0} in the name.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_failed_to_restore_QC_archives_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.UtilityRestoreQcArchiveFailed);

            Assert.AreEqual("An error occurred when attempting to restore the QC test archives. This error most likely occurred because the wrong backup file was chosen. \n\nPlease select a backup file containing {0} in the name.\n[Error: UTIL 3]", message.FormattedMessage);
            Assert.AreEqual("An error occurred when attempting to restore the QC test archives. This error most likely occurred because the wrong backup file was chosen. ", message.Description);
            Assert.AreEqual("UTIL 3", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please select a backup file containing {0} in the name.", message.Resolution);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void Then_the_database_version_mismatch_message_matches_requirements()
        {
            var message = _messageManager.GetMessage(MessageKeys.UtilityDatabaseVersionMismatch);

            Assert.AreEqual("The backup version you are currently trying to restore is not compatible with this version of the software.\n\nBackup version: {0}\n\nCurrent version: {1}\n\nPlease select another backup.\n[Error: UTIL 11]", message.FormattedMessage);
            Assert.AreEqual("The backup version you are currently trying to restore is not compatible with this version of the software.\n\nBackup version: {0}\n\nCurrent version: {1}", message.Description);
            Assert.AreEqual("UTIL 11", message.MessageId);
            Assert.AreEqual(MessageType.Error, message.MessageType);
            Assert.AreEqual("Please select another backup.", message.Resolution);
        }

        #endregion
    }
    // ReSharper restore InconsistentNaming
}
