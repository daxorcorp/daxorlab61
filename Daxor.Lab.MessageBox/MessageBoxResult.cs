﻿namespace Daxor.Lab.MessageBox
{
    /// <summary>
    /// Specifies constants defining which set of choices to display on an IMessageBox
    /// </summary>
    public enum MessageBoxResult
    {
        /// <summary>
        /// The dialog box return value is undefined/none.
        /// </summary>
        None = 0,
        /// <summary>
        /// The dialog box return value is OK.
        /// </summary>
        OK = 1,
        /// <summary>
        /// The dialog box return value is Cancel.
        /// </summary>
        Cancel = 2,
        /// <summary>
        /// The dialog box return value is Yes.
        /// </summary>
        Yes = 3,
        /// <summary>
        /// The dialog box return value is No.
        /// </summary>
        No = 4
    }
}