﻿using System;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Daxor.Lab.Infrastructure.UserControls;
using Microsoft.Practices.Composite.Logging;

namespace Daxor.Lab.MessageBox
{
    public class FolderChooser : IFolderChooser 
    {
        private readonly IMessageBoxDispatcher _messageBoxDispatcher;
        private readonly IFolderBrowser _folderBrowser;

        public static readonly string ExportText = "Export";
        public static readonly string CancelText = "Cancel";
        public static readonly string MessageBoxCaption = "Export BVA Tests";
        public static readonly string FolderSeparator = "\\";

        public FolderChooser(IMessageBoxDispatcher messageBoxDispatcher, ILoggerFacade logger, 
            IStarBurnWrapper driveWrapper, IDriveAccessorService driveAccessorService, IFolderBrowser folderBrowser = null)
        {
            if (messageBoxDispatcher == null) throw new ArgumentNullException("messageBoxDispatcher");

            _messageBoxDispatcher = messageBoxDispatcher;
            _folderBrowser = folderBrowser ?? new FolderBrowser(logger, driveWrapper, driveAccessorService);
        }

        public FolderChoice GetFolderChoice()
        {
            var messageBoxChoices = new[] { ExportText, CancelText };
            var result = _messageBoxDispatcher.ShowMessageBox(MessageBoxCategory.Undefined, _folderBrowser, MessageBoxCaption, messageBoxChoices);

            if (messageBoxChoices[result] == CancelText)
                return null;
            if (messageBoxChoices[result] == ExportText && !_folderBrowser.IsPathValid())
                return null;

            var folderChoice = new FolderChoice
                {
                    IsOpticalDrive = _folderBrowser.IsOpticalDrive,
                    Path = _folderBrowser.CurrentPath
                };

            if (!folderChoice.Path.EndsWith(FolderSeparator))
                folderChoice.Path += FolderSeparator;
            
            return folderChoice;
        }
    }
}
