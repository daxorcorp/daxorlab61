﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.Infrastructure.Interfaces;

namespace Daxor.Lab.MessageBox
{
    public class MessageBoxUIRegistrar : IMessageBoxUiRegistrar
    {
        private readonly Dictionary<Guid, Func<IModalView>> _modalViews = new Dictionary<Guid, Func<IModalView>>();

        public IMessageBoxUiRegistrar RegisterUi(Guid viewId, Func<IModalView> viewBuilder)
        {
            if (viewId == null || viewId == Guid.Empty)
                throw new ArgumentException("viewID cannot be null or empty");
            if (viewBuilder == null)
                throw new ArgumentNullException("viewBuilder");

            if (_modalViews.ContainsKey(viewId))
                throw new Exception("vuewID has been already used");

            _modalViews.Add(viewId, viewBuilder);

            return this;
        }

        public IModalView GenerateUi(Guid viewId)
        {
            if (viewId == null || viewId == Guid.Empty)
                throw new ArgumentException("viewID cannot be null or empty");

            Func<IModalView> builder;
            if (_modalViews.TryGetValue(viewId, out builder))
                return builder();

            return null;
        }
    }
}
