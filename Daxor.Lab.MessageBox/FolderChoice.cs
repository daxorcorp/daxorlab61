﻿namespace Daxor.Lab.MessageBox
{
    public class FolderChoice
    {
        public string Path { get; set; }
        public bool IsOpticalDrive { get; set; }
    }
}
