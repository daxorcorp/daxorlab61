﻿using System;
//-----------------------------------------------------------------------
// <copyright file="XamlMessageBoxDispatcher.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------
using System.Collections.Generic;
using System.Windows;
using Daxor.Lab.Infrastructure.DomainModels;
using Daxor.Lab.Infrastructure.Interfaces;
using Microsoft.Practices.Unity;

namespace Daxor.Lab.MessageBox
{
    /// <summary>
    /// Handles the showing of a XAML window to display a model message box with specified 
    /// options for the user to choose.
    /// </summary>
    public class XamlMessageBoxDispatcher : IMessageBoxDispatcher
    {
        readonly IMessageBoxUiRegistrar _msgRegistrar;
        readonly IUnityContainer _container;

        public XamlMessageBoxDispatcher(IMessageBoxUiRegistrar msgRegistrar, IUnityContainer container)
        {
            _msgRegistrar = msgRegistrar;
            _container = container;
        }

        /// <summary>
        /// Displays a message box of a specified category with a specified title, message, and
        /// a list of strings corresponding to options for the user to choose.
        /// </summary>
        /// <param name="category">One of the MessageBoxCategory values that categorizes the
        /// information to be displayed in the message box</param>
        /// <param name="messageContent">Content of the message</param>
        /// <param name="caption">The text to display as the title of the message box</param>
        /// <param name="choiceTexts">The strings corresponding to the choices presented to 
        /// the user</param>
        /// <returns>Index of the list item corresponding to the option chosen by the user</returns>
        public int ShowMessageBox(MessageBoxCategory category, object messageContent, string caption, IEnumerable<string> choiceTexts)
        {
            MessageBoxWindow mbWindow = new MessageBoxWindow();
            int result = mbWindow.Show(category, messageContent, caption, choiceTexts);

            mbWindow = null;
            return result;
        }

        /// <summary>
        /// Displays a message box of a specified category with a specified title, message, and
        /// a predefined set of choices from which the user can choose.
        /// </summary>
        /// <param name="category">One of the MessageBoxCategory values that categorizes the
        /// information to be displayed in the message box</param>
        /// <param name="messageContent">Content of the message</param>
        /// <param name="caption">The text to display as the title of the message box</param>
        /// <param name="choiceSet">One of the MessageBoxChoiceSet values that defines the choices
        /// presented to the user</param>
        /// <returns>MessageBoxReturnResult corresponding to the choice made by the user</returns>
        public MessageBoxReturnResult ShowMessageBox(MessageBoxCategory category, object messageContent, string caption, MessageBoxChoiceSet choiceSet)
        {
            MessageBoxWindow mbWindow = new MessageBoxWindow();
            MessageBoxReturnResult result = mbWindow.Show(category, messageContent, caption, choiceSet);

            mbWindow = null;
            return result; 
        }
        
        /// <summary>
        /// Displays a message box of a specified category with a specified title, message, and
        /// a predefined set of choices from which the user can choose.
        /// </summary>
        /// <param name="category">One of the MessageBoxCategory values that categorizes the
        /// information to be displayed in the message box</param>
        /// <param name="messageContent">Content of the message</param>
        /// <param name="caption">The text to display as the title of the message box</param>
        /// <param name="choiceSet">One of the MessageBoxChoiceSet values that defines the choices
        /// presented to the user</param>
        /// <returns>MessageBoxReturnComplexResult corresponding to the choice made by the user based on choiceSet as well as dataContext of messageContent if one is available</returns>
        public MessageBoxReturnResult ShowMessageBox(MessageBoxCategory category, object messageContent, string caption, MessageBoxChoiceSet choiceSet, out object msgContentDataContext)
        {
            MessageBoxWindow mbWindow = new MessageBoxWindow();
            MessageBoxReturnResult result = mbWindow.Show(category, messageContent, caption, choiceSet);
            msgContentDataContext = (messageContent is FrameworkElement) ? ((FrameworkElement)messageContent).DataContext : null;

            mbWindow = null;
            return result;
        }
        public MessageBoxReturnResult ShowMessageBox(MessageBoxCategory category, object messageContent, string caption, MessageBoxChoiceSet choiceSet, Action onClosing)
        {
            MessageBoxWindow mbWindow = new MessageBoxWindow();
            mbWindow.Closing += (s, e) => onClosing();
            MessageBoxReturnResult result = mbWindow.Show(category, messageContent, caption, choiceSet);

            mbWindow = null;
            return result;
        }
        public MessageBoxReturnResult ShowMessageBox(MessageBoxCategory category, Guid viewId, string caption)
        {
            throw new NotImplementedException();
        }

        public void ShowMessageBox<T>(MessageBoxCategory category, string caption, Guid modalViewId, Action<IModalView, T> onClose) where T : class
        {
            ShowMessageBox<T>(category, caption, modalViewId, _container.Resolve<T>(), onClose);
        }
        public void ShowMessageBox<T>(MessageBoxCategory category, string caption, Guid modalViewId, T instanceToBind, Action<IModalView, T> onClose) where T : class
        {
            try
            {
                var view = _msgRegistrar.GenerateUi(modalViewId);
                if (view is IModalView)
                {
                    IModalView modalUI = (IModalView)view;
                    modalUI.State = instanceToBind;

                    MessageBoxWindow mbWindow = new MessageBoxWindow();
                    modalUI.Closing += (sender, e) => mbWindow.Close();
                    mbWindow.Closed += (sender, e) => onClose(modalUI, (T)modalUI.State);

                    int result = mbWindow.Show(category, modalUI, caption, modalUI.Choices);
                }
                else
                {
                    //simple user control
                    throw new NotSupportedException("view must be IModalView");
                }
            }
            catch 
            {
                throw;
            }
        }


        public int ShowMessageBox(MessageBoxCategory category, object messageContent, string caption, IEnumerable<string> choiceTexts, out object msgContentDataContext)
        {
            MessageBoxWindow mbWindow = new MessageBoxWindow();
            var result = mbWindow.Show(category, messageContent, caption, choiceTexts);
            msgContentDataContext = (messageContent is FrameworkElement) ? ((FrameworkElement)messageContent).DataContext : null;

            mbWindow = null;
            return result;
        }
    }
}
