﻿//-----------------------------------------------------------------------
// <copyright file="IMessageBox.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Daxor.Lab.Infrastructure.DomainModels;
using System;

namespace Daxor.Lab.MessageBox
{
    /// <summary>
    /// Shows a message box with specified options for the user to choose.
    /// </summary>
    public interface IMessageBox
    {
        /// <summary>
        /// Displays a message box of a specified category with a specified title, message, and
        /// a list of strings corresponding to options for the user to choose.
        /// </summary>
        /// <param name="category">One of the MessageBoxCategory values that categorizes the
        /// information to be displayed in the message box</param>
        /// <param name="messageContent">Content of the message</param>
        /// <param name="caption">The text to display as the title of the message box</param>
        /// <param name="choiceTexts">The strings corresponding to the choices presented to 
        /// the user</param>
        /// <returns>Index of the list item corresponding to the option chosen by the user</returns>
        int Show(MessageBoxCategory category, object messageContent, string caption, IEnumerable<string> choiceTexts);

        /// <summary>
        /// Displays a message box of a specified category with a specified title, message, and
        /// a predefined set of choices from which the user can choose.
        /// </summary>
        /// <param name="category">One of the MessageBoxCategory values that categorizes the
        /// information to be displayed in the message box</param>
        /// <param name="messageContent">Content of the message</param>
        /// <param name="caption">The text to display as the title of the message box</param>
        /// <param name="choiceSet">One of the MessageBoxChoiceSet values that defines the choices
        /// presented to the user</param>
        /// <returns>MessageBoxReturnResult corresponding to the choice made by the user</returns>
        MessageBoxReturnResult Show(MessageBoxCategory category, object messageContent, string caption, MessageBoxChoiceSet choiceSet);

        /// <summary>
        /// Displays a message box of a specified category with a specified title, message, and
        /// a predefined set of choices from which the user can choose.
        /// </summary>
        /// <param name="category">One of the MessageBoxCategory values that categorizes the
        /// information to be displayed in the message box</param>
        /// <param name="messageContent">Content of the message</param>
        /// <param name="caption">The text to display as the title of the message box</param>
        /// <param name="choiceSet">One of the MessageBoxChoiceSet values that defines the choices
        /// presented to the user</param>
        /// <param name="horizontalAlignment">One of the MessageBoxHorizontalAlignment values that
        /// determines how to align the message box choices.</param>
        /// <returns>MessageBoxReturnResult corresponding to the choice made by the user</returns>
        MessageBoxReturnResult Show(MessageBoxCategory category, object messageContent, string caption, MessageBoxChoiceSet choiceSet,
            MessageBoxHorizontalAlignment horizontalAlignment);

        
    }
}