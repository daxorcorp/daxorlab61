﻿//-----------------------------------------------------------------------
// <copyright file="MessageBoxChoice.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Daxor.Lab.MessageBox
{
    /// <summary>
    /// Represents an option for the user to choose within a message box.
    /// </summary>
    public class MessageBoxChoice
    {
        /// <summary>
        /// The unique identifier of this message box choice
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// The content of this message box choice
        /// </summary>
        public string Content { get; set; }
    }
}