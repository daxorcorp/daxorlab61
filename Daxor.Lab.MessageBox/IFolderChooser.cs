﻿namespace Daxor.Lab.MessageBox
{
    public interface IFolderChooser
    {
        FolderChoice GetFolderChoice();
    }
}
