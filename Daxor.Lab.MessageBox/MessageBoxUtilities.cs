﻿//-----------------------------------------------------------------------
// <copyright file="MessageBoxUtilities.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.MessageBox
{
    /// <summary>
    /// Defines helper methods that pertain to message box choices and return results
    /// </summary>
    public static class MessageBoxUtilities
    {
        #region ChoiceSetToList()
        /// <summary>
        /// Converts a MessageBoxChoiceSet into a list of strings for the Show() method.
        /// </summary>
        /// <param name="choiceSet">One of the MessageBoxChoiceSet values that defines the choices
        /// presented to the user</param>
        /// <returns>The strings corresponding to the choices in the given choice set</returns>
        public static List<string> ChoiceSetToList(MessageBoxChoiceSet choiceSet)
        {
            List<string> choiceList = new List<string>();
            switch (choiceSet)
            {
                case MessageBoxChoiceSet.Ok:
                    choiceList.Add("OK");
                    break;
                case MessageBoxChoiceSet.Close:
                    choiceList.Add("Close");
                    break;
                case MessageBoxChoiceSet.OkCancel:
                    choiceList.Add("OK");
                    choiceList.Add("Cancel");
                    break;
                case MessageBoxChoiceSet.YesNo:
                    choiceList.Add("Yes");
                    choiceList.Add("No");
                    break;
                case MessageBoxChoiceSet.YesNoCancel:
                    choiceList.Add("Yes");
                    choiceList.Add("No");
                    choiceList.Add("Cancel");
                    break;
                case MessageBoxChoiceSet.StartReturn:
                    choiceList.Add("Start");
                    choiceList.Add("Return");
                    break;
            }

            return choiceList;
        }
        #endregion

        #region ChoiceToMessageBoxReturnResult()
        /// <summary>
        /// Converts the index of the item chosen by the user to a MessageBoxReturnResult 
        /// corresponding to the choice from the given MessageBoxChoiceSet.
        /// </summary>
        /// <param name="choiceSet">One of the MessageBoxChoiceSet values that defines the choices
        /// presented to the user</param>
        /// <param name="choiceIndex">Index of the choice made by the user</param>
        /// <returns>MessageBoxReturnResult corresponding to the choice made by the user</returns>
        public static MessageBoxReturnResult ChoiceToMessageBoxReturnResult(MessageBoxChoiceSet choiceSet, int choiceIndex)
        {
            MessageBoxReturnResult result = MessageBoxReturnResult.None;
            switch (choiceSet)
            {
                case MessageBoxChoiceSet.Ok:
                    result = MessageBoxReturnResult.Ok;
                    break;
                
                case MessageBoxChoiceSet.Close:
                    result = MessageBoxReturnResult.Close;
                    break;
                
                case MessageBoxChoiceSet.OkCancel:
                    result = choiceIndex == 0 ? MessageBoxReturnResult.Ok : MessageBoxReturnResult.Cancel;
                    break;
                
                case MessageBoxChoiceSet.YesNo:
                    result = choiceIndex == 0 ? MessageBoxReturnResult.Yes : MessageBoxReturnResult.No;
                    break;

                case MessageBoxChoiceSet.YesNoCancel:
                    if (choiceIndex == 0)
                        result = MessageBoxReturnResult.Yes;
                    else if (choiceIndex == 1)
                        result = MessageBoxReturnResult.No;
                    else
                        result = MessageBoxReturnResult.Cancel;
                    break;

                case MessageBoxChoiceSet.StartReturn:
                    result = choiceIndex == 0 ? MessageBoxReturnResult.Start : MessageBoxReturnResult.Return;
                    break;
            }

            return result;
        }
        #endregion
    }
}
