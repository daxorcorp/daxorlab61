﻿//-----------------------------------------------------------------------
// <copyright file="MessageBoxWindow.xaml.cs" company="DAXOR Corporation">
//     Copyright (c) DAXOR Corporation. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Daxor.Lab.Infrastructure.DomainModels;

namespace Daxor.Lab.MessageBox
{
    /// <summary>
    /// Interaction logic for MessageBoxWindow.xaml
    /// 
    /// NOTES/TODO by Geoff Mazeroff: 
    /// (1) Because Window.ShowDialog() blocks the UI thread, we may need to
    /// consider some other options for displaying a window so that other UI things
    /// can happen behind this message box.
    /// 
    /// (2) The colors of the borders should probably be defined elsewhere instead
    /// of hard-coded in SetMessageBoxBorder().
    /// 
    /// (3) The heights of the buttons should be 20 (horizontal layout) and 60 (vertical
    /// layout). For some reason, they end up larger than that when rendered. Huh???
    /// 
    /// (4) Once this window is closed, you cannot call ShowDialog() on it again. Perhaps
    /// there is a way around this. Otherwise, the caller will need to create a new instance
    /// for every message box.
    /// </summary>
    public partial class MessageBoxWindow : Window, IMessageBox, INotifyPropertyChanged
    {
        #region Private member variables
        /// <summary>
        /// Choices available for the user to select from this message box
        /// </summary>
        private List<MessageBoxChoice> _choices;

        /// <summary>
        /// Orientation of the buttons displaying the choices (as this varies based on message box 
        /// category)
        /// </summary>
        private Orientation _orientation;

        /// <summary>
        /// Width of the buttons displaying the choices (as this varies based on orientation)
        /// </summary>
        private int _choiceWidth;

        /// <summary>
        /// Height of the buttons displaying the choices (as this varies based on orientation)
        /// </summary>
        private int _choiceHeight;

        /// <summary>
        /// Caption (title) of the message box
        /// </summary>
        private string _caption;

        /// <summary>
        /// Message content of the message box
        /// </summary>
        private object _messageContent;

        /// <summary>
        /// Color of the message box border
        /// </summary>
        private Color _borderColor;

        /// <summary>
        /// Category of the message box content
        /// </summary>
        private MessageBoxCategory _category;

        /// <summary>
        /// Horizontal alignment of predefined choices
        /// </summary>
        private MessageBoxHorizontalAlignment _horizontalAlignment;

        /// <summary>
        /// Horizontal alignment of predefined choices
        /// </summary>
        private MessageBoxVerticalAlignment _verticalAlignment;

        private Thickness _buttonMargin;

        #endregion

        #region Constants
        /// <summary>
        /// Preferred width of message box choice buttons when they have predefined button names.
        /// </summary>
        public static readonly int PreferredPredefinedChoiceWidth = 180;

        /// <summary>
        /// Preferred width of message box choice buttons when they have user-defined button names.
        /// </summary>
        public static readonly int PreferredCustomChoiceWidth = 180;

        /// <summary>
        /// Preferred height of message box choice buttons when they have predefined button names.
        /// </summary>
        /// <remarks>This comes from the MinHeight property of the custom Button.</remarks>
        public static readonly int PreferredPredefinedChoiceHeight = 15;

        /// <summary>
        /// Preferred height of message box choice buttons when they have user-defined button names.
        /// </summary>
        /// <remarks>This height allows two lines of text.</remarks>
        public static readonly int PreferredCustomChoiceHeight = 15;

        /// <summary>
        /// Color of the dialog border when an error message is being displayed.
        /// </summary>
        /// <remarks>The RGB values came from projekt202's Dialog.xaml.</remarks>
        public static readonly Color ErrorBorderColor = Color.FromArgb(255, 187, 66, 66);

        /// <summary>
        /// Color of the dialog border when a warning message is being displayed.
        /// </summary>
        /// <remarks>The RGB values came from projekt202's Dialog.xaml.</remarks>
        public static readonly Color WarningBorderColor = Color.FromArgb(255, 216, 182, 89);

        /// <summary>
        /// Color of the dialog border when an information/question message is being displayed.
        /// </summary>
        /// <remarks>The RGB values came from projekt202's Dialog.xaml.</remarks>
        public static readonly Color DefaultBorderColor = Color.FromArgb(255, 0, 0, 0);

        /// <summary>
        /// Horizontal alignment of buttons used when displaying user-defined message box choices.
        /// </summary>
        public static readonly MessageBoxHorizontalAlignment DefaultUserChoiceHorizontalAlignment = MessageBoxHorizontalAlignment.Right;

        /// <summary>
        /// Horizontal alignment of buttons used when displaying predefined message box choices.
        /// </summary>
        public static readonly MessageBoxHorizontalAlignment DefaultPredefinedChoiceHorizontalAlignment = MessageBoxHorizontalAlignment.Right;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes the window and the list of choices available for the user
        /// </summary>
        public MessageBoxWindow()
        {
            InitializeComponent();

            PropertyChanged += MessageBoxPropertyChanged;
            DataContext = this;
            Choices = new List<MessageBoxChoice>();
            ChoiceOrientation = Orientation.Vertical;
            Caption = string.Empty;
            _horizontalAlignment = DefaultUserChoiceHorizontalAlignment;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the orientation of the buttons that display choices to the users
        /// </summary>
        public Orientation ChoiceOrientation
        {
            get
            {
                return _orientation;
            }
            set
            {
                if (_orientation == value) return;

                _orientation = value;
                OnPropertyChanged("ChoiceOrientation");
            }

        }

        /// <summary>
        /// Gets or sets the width of the buttons that display the choices to the user
        /// </summary>
        public int ChoiceWidth
        {
            get
            {
                return _choiceWidth;
            }
            set
            {
                if (_choiceWidth == value) return;

                _choiceWidth = value;
                OnPropertyChanged("ChoiceWidth");
            }
        }

        /// <summary>
        /// Gets or sets the height of the buttons that display the choices to the user
        /// </summary>
        public int ChoiceHeight
        {
            get
            {
                return _choiceHeight;
            }
            set
            {
                if (_choiceHeight == value) return;

                _choiceHeight = value;
                OnPropertyChanged("ChoiceHeight");
            }
        }

        /// <summary>
        /// Gets or sets the caption for this message box window
        /// </summary>
        public string Caption
        {
            get
            {
                return _caption;
            }
            set
            {
                if (_caption == value) return;

                _caption = value;
                OnPropertyChanged("Caption");
            }
        }

        /// <summary>
        /// Gets or sets the horizontal alignment of predefined buttons/choices.
        /// </summary>
        public HorizontalAlignment ChoiceHorizontalAlignment
        {
            get
            {
                return ConvertHorizontalAlignmentToFrameworkEnum(_horizontalAlignment);
            }
            set
            {
                MessageBoxHorizontalAlignment alignment = ConvertFromHorizontalAlignmentFrameworkEnum(value);
                if (alignment == _horizontalAlignment) return;
                _horizontalAlignment = alignment;
                OnPropertyChanged("ChoiceHorizontalAlignment");
            }
        }

        /// <summary>
        /// Gets or sets the horizontal alignment of predefined buttons/choices.
        /// </summary>
        public VerticalAlignment ChoiceVerticalAlignment
        {
            get
            {
                return ConvertVerticalAlignmentToFrameworkEnum(_verticalAlignment);
            }
            set
            {
                MessageBoxVerticalAlignment alignment = ConvertFromVerticalAlignmentFrameworkEnum(value);
                if (alignment == _verticalAlignment) return;
                _verticalAlignment = alignment;
                OnPropertyChanged("ChoiceVerticalAlignment");
            }
        }

        /// <summary>
        /// Gets or sets the message content for this message box window
        /// </summary>
        public object MessageContent
        {
            get
            {
                return _messageContent;
            }
            set
            {
                if (_messageContent == value) return;

                _messageContent = value;
                OnPropertyChanged("MessageContent");
            }
        }

        /// <summary>
        /// Gets or sets the border color for this message box window
        /// </summary>
        public Color BorderColor
        {
            get
            {
                return _borderColor;
            }
            set
            {
                if (_borderColor == value) return;

                _borderColor = value;
                OnPropertyChanged("BorderColor");
            }
        }

        /// <summary>
        /// Gets or sets the category of this message box window
        /// </summary>
        public MessageBoxCategory Category
        {
            get
            {
                return _category;
            }
            set
            {
                if (_category == value) return;

                _category = value;
                OnPropertyChanged("Category");
            }
        }

        /// <summary>
        /// Gets or sets the list of MessageBoxChoices for this message box window
        /// </summary>
        public List<MessageBoxChoice> Choices
        {
            get
            {
                return _choices;
            }
            set
            {
                _choices = value;
                OnPropertyChanged("MessageBoxChoices");
            }
        }

        public Thickness ButtonMargin
        {
            get
            {
                return _buttonMargin;
            }
            set
            {
                if (_buttonMargin == value) return;
                _buttonMargin = value;
                OnPropertyChanged("ButtonMargin");
            }
        }

        #endregion

        #region Show() -- returning int
        /// <summary>
        /// Displays a modal dialog (window) containing the specified caption, text, and choices (displayed
        /// as buttons). The border and icon displayed in the dialog depend on the specified category.
        /// </summary>
        /// <param name="category">One of the MessageBoxCategory values that categorizes the
        /// information to be displayed in the message box</param>
        /// <param name="messageContent">Content of the message</param>
        /// <param name="caption">The text to display as the title of the message box</param>
        /// <param name="choiceTexts">The strings corresponding to the choices presented to 
        /// the user</param>
        /// <returns>Index of the list item corresponding to the option chosen by the user</returns>
        public int Show(MessageBoxCategory category, object messageContent, string caption, IEnumerable<string> choiceTexts)
        {
            ChoiceOrientation = Orientation.Horizontal;
            Category = category;
            Caption = caption;
            MessageContent = messageContent;
            SetMessageBoxChoices(choiceTexts);
            ChoiceHorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            ChoiceVerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            
            ChoiceWidth = PreferredCustomChoiceWidth;
            ChoiceHeight = PreferredCustomChoiceHeight;

            //Don't attempt to set Owner if the window is owner itself
            if (Owner == null || Owner != Application.Current.MainWindow)
            {
                Owner = Application.Current.MainWindow;
                Height = Application.Current.MainWindow.Height;
                Width = Application.Current.MainWindow.Width;
            }
            ShowDialog();
            return (int)Tag;
        }
        #endregion

        #region Show() -- returning MessageBoxReturnResult
        /// <summary>
        /// Displays a message box of a specified category with a specified title, message, and
        /// a predefined set of choices from which the user can choose.
        /// </summary>
        /// <param name="category">One of the MessageBoxCategory values that categorizes the
        /// information to be displayed in the message box</param>
        /// <param name="messageContent">Content of the message</param>
        /// <param name="caption">The text to display as the title of the message box</param>
        /// <param name="choiceSet">One of the MessageBoxChoiceSet values that defines the choices
        /// presented to the user</param>
        /// <returns>MessageBoxReturnResult corresponding to the choice made by the user</returns>
        /// <remarks>The default alignment is used for the choice buttons.</remarks>
        public Daxor.Lab.Infrastructure.DomainModels.MessageBoxReturnResult Show(
            MessageBoxCategory category,
            object messageContent,
            string caption,
            MessageBoxChoiceSet choiceSet)
        {
            ChoiceVerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            return Show(category, messageContent, caption, choiceSet, DefaultPredefinedChoiceHorizontalAlignment);
        }
        #endregion

        #region Show() -- returning MessageBoxReturnResult (but with horiz alignment)
        /// <summary>
        /// Displays a message box of a specified category with a specified title, message, and
        /// a predefined set of choices from which the user can choose.
        /// </summary>
        /// <param name="category">One of the MessageBoxCategory values that categorizes the
        /// information to be displayed in the message box</param>
        /// <param name="messageContent">Content of the message</param>
        /// <param name="caption">The text to display as the title of the message box</param>
        /// <param name="choiceSet">One of the MessageBoxChoiceSet values that defines the choices
        /// presented to the user</param>
        /// <param name="horizontalAlignment">One of the MessageBoxHorizontalAlignment values that
        /// determines how to align the message box choices.</param>
        /// <returns>MessageBoxReturnResult corresponding to the choice made by the user</returns>
        public MessageBoxReturnResult Show(
            MessageBoxCategory category,
            object messageContent,
            string caption,
            MessageBoxChoiceSet choiceSet,
            MessageBoxHorizontalAlignment horizAlignment)
        {
            ChoiceOrientation = Orientation.Horizontal;
            Category = category;
            Caption = caption;
            MessageContent = messageContent;
            SetMessageBoxChoices(MessageBoxUtilities.ChoiceSetToList(choiceSet));
            ChoiceHorizontalAlignment = ConvertHorizontalAlignmentToFrameworkEnum(horizAlignment);
            ChoiceVerticalAlignment = System.Windows.VerticalAlignment.Bottom;

            ChoiceHeight = PreferredPredefinedChoiceHeight;
            ChoiceWidth = PreferredPredefinedChoiceWidth;

            //Don't attempt to set Owner if the window is owner itself
            //  This still doesn't work.  If Owner is null, and the Application.Current.MainWindow 
            //  is the same window as the owner of this, you still get the same exception
            //  Owner = Application.Current.MainWindow;
            if (Owner == null || Owner != Application.Current.MainWindow)
            {
                Owner = Application.Current.MainWindow;
                Height = Application.Current.MainWindow.Height;
                Width = Application.Current.MainWindow.Width;
            }
            
            ShowDialog();
            return MessageBoxUtilities.ChoiceToMessageBoxReturnResult(choiceSet, (int)Tag);
        }
        #endregion

        #region SetMessageBoxChoices()
        /// <summary>
        /// Populates the class variable and UI list of choices based on the given strings.
        /// </summary>
        /// <param name="choiceTexts">The strings corresponding to the choices presented to 
        /// the user</param>
        private void SetMessageBoxChoices(IEnumerable<string> choiceTexts)
        {
            // Clear out any lists of choices (both in the UI and in this class).
            uiButtonList.Items.Clear();
            Choices.Clear();

            // Build out the list of choices in the class variable. Every choice has a
            // unique identifier.
            IEnumerator<string> buttonTextsEnumerator = choiceTexts.GetEnumerator();
            int index = 0;
            while (buttonTextsEnumerator.MoveNext())
            {
                string buttonText = (string)buttonTextsEnumerator.Current;
                Choices.Add(new MessageBoxChoice()
                {
                    ID = index,
                    Content = buttonText
                });
                index++;
            }
        }
        #endregion

        #region SetMessageBoxBorder()
        /// <summary>
        /// Sets the color of the dialog border based on the category for this message box.
        /// </summary>
        /// <param name="category">One of the MessageBoxCategory values that categorizes the
        /// information to be displayed in the message box</param>
        private void SetMessageBoxBorder(MessageBoxCategory category)
        {
            switch (category)
            {
                case MessageBoxCategory.Error:
                    BorderColor = ErrorBorderColor;
                    break;
                case MessageBoxCategory.Warning:
                    BorderColor = WarningBorderColor;
                    break;
                case MessageBoxCategory.Information:
                    BorderColor = DefaultBorderColor;
                    break;
                case MessageBoxCategory.Question:
                    BorderColor = DefaultBorderColor;
                    break;
            }
        }
        #endregion

        #region SetMessageBoxIcon()
        /// <summary>
        /// Sets the icon of the dialog based on the category for this message box.
        /// </summary>
        /// <param name="category">One of the MessageBoxCategory values that categorizes the
        /// information to be displayed in the message box</param>
        private void SetMessageBoxIcon(MessageBoxCategory category)
        {
            uiIconError.Visibility = category == MessageBoxCategory.Error ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;
            uiIconQuestion.Visibility = category == MessageBoxCategory.Question ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;
            uiIconWarning.Visibility = category == MessageBoxCategory.Warning ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;
            uiIconInformation.Visibility = category == MessageBoxCategory.Information ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;
        }
        #endregion

        /// <summary>
        /// Converts a WPF-based horizontal alignment into the custom horizontal alignment enumeration.
        /// </summary>
        /// <param name="alignment">One of the HorizontalAlignment values that describes the
        /// horizontal alignment</param>
        /// <returns>The corresponding enumeration value for the horizontal alignment. Only Left,
        /// Right, and Center are currently supported; "Undefined" is returned otherwise</returns>
        private MessageBoxHorizontalAlignment ConvertFromHorizontalAlignmentFrameworkEnum(HorizontalAlignment alignment)
        {
            MessageBoxHorizontalAlignment toReturn = MessageBoxHorizontalAlignment.Undefined;
            switch (alignment)
            {
                case System.Windows.HorizontalAlignment.Center: toReturn = MessageBoxHorizontalAlignment.Center; break;
                case System.Windows.HorizontalAlignment.Left: toReturn = MessageBoxHorizontalAlignment.Left; break;
                case System.Windows.HorizontalAlignment.Right: toReturn = MessageBoxHorizontalAlignment.Right; break;
            }

            return toReturn;
        }

        /// <summary>
        /// Converts a WPF-based vertical alignment into the custom vertical alignment enumeration.
        /// </summary>
        /// <param name="alignment">One of the VerticalAlignment values that describes the
        /// vertical alignment</param>
        /// <returns>The corresponding enumeration value for the vertical alignment. Only Top,
        /// Center, and Bottom are currently supported; "Undefined" is returned otherwise</returns>
        private MessageBoxVerticalAlignment ConvertFromVerticalAlignmentFrameworkEnum(VerticalAlignment alignment)
        {
            MessageBoxVerticalAlignment toReturn = MessageBoxVerticalAlignment.Undefined;
            switch (alignment)
            {
                case System.Windows.VerticalAlignment.Bottom: toReturn = MessageBoxVerticalAlignment.Bottom; break;
                case System.Windows.VerticalAlignment.Center: toReturn = MessageBoxVerticalAlignment.Center; break;
                case System.Windows.VerticalAlignment.Top: toReturn = MessageBoxVerticalAlignment.Top; break;
            }

            return toReturn;
        }

        /// <summary>
        /// Converts a custom horizontal alignment enumeration into the WPF-based horizontal alignment.
        /// </summary>
        /// <param name="alignment">One of the MessageBoxHorizontalAlignment values that describes the
        /// horizontal alignment</param>
        /// <returns>The corresponding enumeration value for the horizontal alignment. Only Left,
        /// Right, and Center are currently supported; "Left" is returned otherwise</returns>
        private HorizontalAlignment ConvertHorizontalAlignmentToFrameworkEnum(MessageBoxHorizontalAlignment alignment)
        {
            HorizontalAlignment toReturn = System.Windows.HorizontalAlignment.Left;
            switch (alignment)
            {
                case MessageBoxHorizontalAlignment.Center: toReturn = System.Windows.HorizontalAlignment.Center; break;
                case MessageBoxHorizontalAlignment.Left: toReturn = System.Windows.HorizontalAlignment.Left; break;
                case MessageBoxHorizontalAlignment.Right: toReturn = System.Windows.HorizontalAlignment.Right; break;
            }

            return toReturn;
        }

        /// <summary>
        /// Converts a custom vertical alignment enumeration into the WPF-based horizontal alignment.
        /// </summary>
        /// <param name="alignment">One of the MessageBoxVerticalAlignment values that describes the
        /// vertical alignment</param>
        /// <returns>The corresponding enumeration value for the vertical alignment. Only Top,
        /// Center, and Bottom are currently supported; "Top" is returned otherwise</returns>
        private VerticalAlignment ConvertVerticalAlignmentToFrameworkEnum(MessageBoxVerticalAlignment alignment)
        {
            VerticalAlignment toReturn = System.Windows.VerticalAlignment.Top;
            switch (alignment)
            {
                case MessageBoxVerticalAlignment.Bottom: toReturn = System.Windows.VerticalAlignment.Bottom; break;
                case MessageBoxVerticalAlignment.Center: toReturn = System.Windows.VerticalAlignment.Center; break;
                case MessageBoxVerticalAlignment.Top: toReturn = System.Windows.VerticalAlignment.Top; break;
            }

            return toReturn;
        }

        #region Button click event handler
        /// <summary>
        /// Handles the click event for one of the buttons corresponding to the
        /// choices made available to the user.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Click event arguments</param>
        private void uiButtonList_Click(object sender, RoutedEventArgs e)
        {
            // Find out which button was clicked.
            int choiceID = (int)((Button)e.OriginalSource).Tag;
            var sChoice = (from c in Choices
                           where c.ID == choiceID
                           select c).FirstOrDefault();

            // The Tag property is used to inform the caller which choice was made.
            Tag = sChoice.ID;

            // After the choice has been made, close the dialog/window.
            Close();
        }
        #endregion

        #region INotifyPropertyChanged Members
        #region PropertyChanged event
        /// <summary>
        /// Event the fires when a property of this object is changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region OnPropertyChanged()
        /// <summary>
        /// Handles the firing of the PropertyChanged event
        /// </summary>
        /// <param name="propertyName">Name of the property that has changed</param>
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region MessageBoxPropertyChanged()
        /// <summary>
        /// Handles the PropertyChanged event
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event arguments</param>
        void MessageBoxPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // Set the choice width/height based on the orientation
            switch (e.PropertyName)
            {
                case "MessageContent":
                    uiMessageContent.Margin = (MessageContent is string) ? new Thickness(15, 10, 5, 5) : new Thickness(5, 0, 5, 5);
                    break;

                case "Category":
                    SetMessageBoxBorder(Category);
                    SetMessageBoxIcon(Category);
                    break;
            }
        }
        #endregion
        #endregion

    }
}
