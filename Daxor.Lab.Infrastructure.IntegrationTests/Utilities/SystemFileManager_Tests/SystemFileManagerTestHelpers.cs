﻿using System.IO;

namespace Daxor.Lab.Infrastructure.IntegrationTests.Utilities.SystemFileManager_Tests
{
    class SystemFileManagerTestHelpers
    {
        public const string SourceFileAbsolutePath = @"c:\temp\AllTxtFiles1.txt";
        public const string DestinationFileAbsolutePath = @"c:\temp\AllTxtFiles2.txt";
        public const string TargetDirectory = @"c:\temp\foo\";

        public static void DeleteDummyFile()
        {
            DeleteDummyFile(SourceFileAbsolutePath);
        }

        public static void DeleteDummyFile(string path)
        {
            File.Delete(path);
        }

        public static void CreateDummyFile(string path)
        {
            CreateDummyFile(path, "hello world");
        }

        public static void CreateDummyFile(string path, string content)
        {
            using (var outfile = new StreamWriter(path))
            {
                outfile.Write(content);
            }
        }

        public static string ReadDummyFile(string path)
        {
            string content;
            using (var infile = new StreamReader(path))
            {
                content = infile.ReadToEnd();
            }

            return content;
        }

        public static void CleanUpDirectory()
        {
            if (Directory.Exists(TargetDirectory))
                Directory.Delete(TargetDirectory, true);
        }

        public static void CreateTargetDirectory()
        {
            CreateTargetDirectory(TargetDirectory);
        }

        public static void CreateTargetDirectory(string directoryPath)
        {
            Directory.CreateDirectory(directoryPath);
        }

        public static void CleanUpDirectoryAndDummyFiles()
        {
            DeleteDummyFile(SourceFileAbsolutePath);
            DeleteDummyFile(DestinationFileAbsolutePath);
            CleanUpDirectory();
        }
    }
}
