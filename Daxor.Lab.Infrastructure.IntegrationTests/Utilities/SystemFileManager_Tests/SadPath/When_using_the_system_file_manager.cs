﻿using System;
using System.IO;
using Daxor.Lab.Infrastructure.Utilities;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.IntegrationTests.Utilities.SystemFileManager_Tests.SadPath
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_system_file_manager
    {
        private static SystemFileManager _systemFileManager;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _systemFileManager = new SystemFileManager();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            SystemFileManagerTestHelpers.CleanUpDirectoryAndDummyFiles();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_source_file_does_not_exist_then_an_exception_is_thrown()
        {
            SystemFileManagerTestHelpers.DeleteDummyFile();
            AssertEx.Throws<FileNotFoundException>(()=>_systemFileManager.CopyFile(SystemFileManagerTestHelpers.SourceFileAbsolutePath, SystemFileManagerTestHelpers.DestinationFileAbsolutePath));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_directory_to_delete_does_not_exist_then_an_exception_is_thrown()
        {
            SystemFileManagerTestHelpers.CleanUpDirectory();
            AssertEx.Throws<DirectoryNotFoundException>(()=>_systemFileManager.DeleteDirectory(SystemFileManagerTestHelpers.TargetDirectory));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_directory_to_delete_is_invalid_then_an_exception_is_thrown()
        {
            // Cannot delete a file using Directory.Delete()
            AssertEx.Throws<DirectoryNotFoundException>(()=>_systemFileManager.DeleteDirectory(SystemFileManagerTestHelpers.SourceFileAbsolutePath));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_path_for_the_directory_with_files_is_null_then_an_exception_is_thrown()
        {
            AssertEx.ThrowsArgumentNullException(()=>_systemFileManager.GetFilesInDirectory(null, "*.*"), "path");
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_path_for_the_directory_with_files_does_not_exist_then_an_exception_is_thrown()
        {
            AssertEx.Throws<DirectoryNotFoundException>(()=>_systemFileManager.GetFilesInDirectory(SystemFileManagerTestHelpers.TargetDirectory, "*.*"));
        }
        
        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_pattern_for_the_matching_files_is_null_then_an_exception_is_thrown()
        {
            SystemFileManagerTestHelpers.CreateTargetDirectory();
            AssertEx.ThrowsArgumentNullException(()=>_systemFileManager.GetFilesInDirectory(SystemFileManagerTestHelpers.TargetDirectory, null), "searchPattern");
        }
        
        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_path_for_the_directory_with_files_is_actually_a_filename_then_an_exception_is_thrown()
        {
            SystemFileManagerTestHelpers.CreateDummyFile(SystemFileManagerTestHelpers.SourceFileAbsolutePath);
            AssertEx.Throws<IOException>(()=>_systemFileManager.GetFilesInDirectory(SystemFileManagerTestHelpers.SourceFileAbsolutePath, "*.*"));
        }
        
        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_path_for_the_directory_with_files_is_invalid_then_an_exception_is_thrown()
        {
            AssertEx.Throws<DirectoryNotFoundException>(()=>_systemFileManager.GetFilesInDirectory(SystemFileManagerTestHelpers.SourceFileAbsolutePath, "*.*"));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_file_to_remove_the_readonly_property_from_does_not_exist_then_an_exception_is_thrown()
        {
            AssertEx.Throws<FileNotFoundException>(()=>_systemFileManager.RemoveReadOnlyProperty(SystemFileManagerTestHelpers.SourceFileAbsolutePath));
        }
    }
    // ReSharper restore InconsistentNaming
}
