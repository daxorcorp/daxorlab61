﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Daxor.Lab.Infrastructure.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Daxor.Lab.Infrastructure.IntegrationTests.Utilities.SystemFileManager_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_using_the_system_file_manager
    {
        private static SystemFileManager _systemFileManager;

        [ClassInitialize]
        public static void TestClassInitialize(TestContext context)
        {
            _systemFileManager = new SystemFileManager();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            SystemFileManagerTestHelpers.CleanUpDirectoryAndDummyFiles();
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_file_exists_then_FileExist_returns_true()
        {
            SystemFileManagerTestHelpers.CreateDummyFile(SystemFileManagerTestHelpers.SourceFileAbsolutePath);

            Assert.IsTrue(_systemFileManager.FileExists(SystemFileManagerTestHelpers.SourceFileAbsolutePath));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_file_does_not_exist_then_FileExist_returns_false()
        {
            Assert.IsFalse(_systemFileManager.FileExists(SystemFileManagerTestHelpers.SourceFileAbsolutePath));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_file_exists_for_deletion_then_DeleteFile_deletes_it()
        {
            SystemFileManagerTestHelpers.CreateDummyFile(SystemFileManagerTestHelpers.SourceFileAbsolutePath);

            _systemFileManager.DeleteFile(SystemFileManagerTestHelpers.SourceFileAbsolutePath);

            Assert.IsFalse(File.Exists(SystemFileManagerTestHelpers.SourceFileAbsolutePath));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_file_does_not_exist_for_deletion_then_nothing_happens()
        {
            SystemFileManagerTestHelpers.DeleteDummyFile();
            _systemFileManager.DeleteFile(SystemFileManagerTestHelpers.SourceFileAbsolutePath);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_file_is_to_be_copied_to_a_new_destination_that_does_not_exist_then_a_copy_is_created()
        {
            SystemFileManagerTestHelpers.CreateDummyFile(SystemFileManagerTestHelpers.SourceFileAbsolutePath);
            SystemFileManagerTestHelpers.DeleteDummyFile(SystemFileManagerTestHelpers.DestinationFileAbsolutePath);

            _systemFileManager.CopyFile(SystemFileManagerTestHelpers.SourceFileAbsolutePath, SystemFileManagerTestHelpers.DestinationFileAbsolutePath);

            Assert.IsTrue(File.Exists(SystemFileManagerTestHelpers.DestinationFileAbsolutePath));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_file_is_to_be_copied_and_the_destination_file_exists_then_the_destination_is_overwritten()
        {
            SystemFileManagerTestHelpers.CreateDummyFile(SystemFileManagerTestHelpers.SourceFileAbsolutePath);
            SystemFileManagerTestHelpers.CreateDummyFile(SystemFileManagerTestHelpers.DestinationFileAbsolutePath, "goodbye world");

            _systemFileManager.CopyFile(SystemFileManagerTestHelpers.SourceFileAbsolutePath, SystemFileManagerTestHelpers.DestinationFileAbsolutePath);

            Assert.AreEqual(SystemFileManagerTestHelpers.ReadDummyFile(SystemFileManagerTestHelpers.SourceFileAbsolutePath), 
                SystemFileManagerTestHelpers.ReadDummyFile(SystemFileManagerTestHelpers.DestinationFileAbsolutePath));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_directory_is_to_be_deleted_and_the_directory_exists_then_the_directory_is_deleted()
        {
            SystemFileManagerTestHelpers.CleanUpDirectory();
            SystemFileManagerTestHelpers.CreateTargetDirectory();

            _systemFileManager.DeleteDirectory(SystemFileManagerTestHelpers.TargetDirectory);

            Assert.IsFalse(Directory.Exists(SystemFileManagerTestHelpers.TargetDirectory));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_directory_that_has_subdirs_is_to_be_deleted_then_the_entire_directory_is_deleted()
        {
            SystemFileManagerTestHelpers.CleanUpDirectory();
            SystemFileManagerTestHelpers.CreateTargetDirectory();
            SystemFileManagerTestHelpers.CreateTargetDirectory(SystemFileManagerTestHelpers.TargetDirectory + "bar\\");

            _systemFileManager.DeleteDirectory(SystemFileManagerTestHelpers.TargetDirectory);

            Thread.Sleep(300);

            Assert.IsFalse(Directory.Exists(SystemFileManagerTestHelpers.TargetDirectory));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_directory_exists_then_DirectoryExists_returns_true()
        {
            SystemFileManagerTestHelpers.CleanUpDirectory();
            SystemFileManagerTestHelpers.CreateTargetDirectory();
            Assert.IsTrue(_systemFileManager.DirectoryExists(SystemFileManagerTestHelpers.TargetDirectory));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_directory_does_not_exist_then_DirectoryExists_returns_false()
        {
            SystemFileManagerTestHelpers.CleanUpDirectory();
            Assert.IsFalse(_systemFileManager.DirectoryExists(SystemFileManagerTestHelpers.TargetDirectory));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_directory_to_check_for_existence_is_invalid_then_DirectoryExists_returns_false()
        {
            Assert.IsFalse(_systemFileManager.DirectoryExists(SystemFileManagerTestHelpers.SourceFileAbsolutePath));
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_getting_filenames_within_a_directory_based_on_a_pattern_then_the_correct_filenames_are_returned()
        {
            const string firstFile = SystemFileManagerTestHelpers.TargetDirectory + "file1.txt";
            const string secondFile = SystemFileManagerTestHelpers.TargetDirectory + "file2.txt";

            SystemFileManagerTestHelpers.CreateTargetDirectory();
            SystemFileManagerTestHelpers.CreateDummyFile(firstFile);
            SystemFileManagerTestHelpers.CreateDummyFile(secondFile);
            SystemFileManagerTestHelpers.CreateDummyFile(SystemFileManagerTestHelpers.TargetDirectory + "file3.bak");

            var results =
                from r in _systemFileManager.GetFilesInDirectory(SystemFileManagerTestHelpers.TargetDirectory, "*.txt")
                select r.FullName;

            var expectedResults = new List<string> { firstFile, secondFile };
            CollectionAssert.AreEquivalent(expectedResults, results.ToArray());
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        [TestCategory("If fails again, think about redoing this test")]
        public void And_removing_the_readonly_property_from_an_existing_file_then_the_file_is_no_longer_readonly()
        {
            SystemFileManagerTestHelpers.CreateDummyFile(SystemFileManagerTestHelpers.SourceFileAbsolutePath);

            // Make it readonly
            var info = new FileInfo(SystemFileManagerTestHelpers.SourceFileAbsolutePath) { IsReadOnly = true };
            info.Refresh();
            
            _systemFileManager.RemoveReadOnlyProperty(SystemFileManagerTestHelpers.SourceFileAbsolutePath);

            info = new FileInfo(SystemFileManagerTestHelpers.SourceFileAbsolutePath);
            var isReadOnly = info.IsReadOnly;
            info.Refresh();
            Assert.IsFalse(isReadOnly);
            SystemFileManagerTestHelpers.CleanUpDirectoryAndDummyFiles();
        }
    }
    // ReSharper restore InconsistentNaming
}
