﻿using System;
using Daxor.Lab.TestHelpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Daxor.Lab.Infrastructure.Utilities;
using System.IO;
using Ionic.Zip;
using System.Linq;

namespace Daxor.Lab.Infrastructure.IntegrationTests.Utilities.IonicZipWrapper_Tests
{
    // ReSharper disable InconsistentNaming
    [TestClass]
    public class When_saving_a_zip_file
    {
        private const string Directory = @"C:\temp\";
        private const string FileName = "somefile.txt";
        private const string ZipName = "content.zip";
        private IonicZipWrapper _wrapper;
        
        [TestInitialize]
        public void TestInitialize()
        {
            _wrapper = new IonicZipWrapper();
            CreateFile(Directory + FileName);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            File.Delete(Directory + FileName);
            File.Delete(Directory + ZipName);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_file_has_been_added_to_the_root_directory_then_a_zip_folder_is_created()
        {
            _wrapper.AddFileToRoot(Directory + FileName);
            _wrapper.Save(Directory + ZipName);

            var wasFileCreated = File.Exists(Directory + ZipName);

            Assert.IsTrue(wasFileCreated);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_file_has_been_added_to_a_custom_directory_then_a_zip_folder_is_created()
        {
            _wrapper.AddFileToDirectory(Directory + FileName, "directory");
            _wrapper.Save(Directory + ZipName);

            var wasFileCreated = File.Exists(Directory + ZipName);

            Assert.IsTrue(wasFileCreated); 
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_file_has_been_added_to_the_root_directory_then_the_file_is_in_the_root_directory_of_the_zip_folder()
        {
            _wrapper.AddFileToRoot(Directory + FileName);
            _wrapper.Save(Directory + ZipName);

            var wasFileCreated = File.Exists(Directory + ZipName);
            var isFileInCorrectLocation = WasFileInCorrectLocation(Directory + ZipName, FileName);

            Assert.IsTrue(wasFileCreated && isFileInCorrectLocation);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_a_file_has_been_added_to_a_custom_directory_then_the_file_is_in_the_custom_directory_of_the_zip_folder()
        {
            _wrapper.AddFileToDirectory(Directory + FileName, "directory");
            _wrapper.Save(Directory + ZipName);

            var wasFileCreated = File.Exists(Directory + ZipName);
            var isFileInCorrectLocation = WasFileInCorrectLocation(Directory + ZipName, "directory/" + FileName);

            Assert.IsTrue(wasFileCreated && isFileInCorrectLocation);
        }

        [TestMethod]
        [TestCategory("Integration Test")]
        public void And_the_file_has_already_been_save_then_saving_it_again_throws_an_exception()
        {
            _wrapper.AddFileToRoot(Directory + FileName);
            _wrapper.Save(Directory + ZipName);
            AssertEx.Throws<ObjectDisposedException>(()=>_wrapper.Save(Directory + ZipName));
        }

        private void CreateFile(string fullPath)
        {
            using (var writer = new StreamWriter(fullPath))
            {
                writer.WriteLine("stuff");
                writer.Close();
            }
        }

        private bool WasFileInCorrectLocation(string zipFolderPath, string fileName)
        {
            var zipFile = new ZipFile(zipFolderPath);
            var isFileInCorrectLocation = zipFile.Entries.First(x => x.FileName == fileName) != null;
            zipFile.Dispose();

            return isFileInCorrectLocation;
        }
    }
    // ReSharper restore InconsistentNaming
}
