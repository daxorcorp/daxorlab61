﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.SCCore.Utilities;
using System.Collections;
using Daxor.Lab.SCUSBControl;
using Daxor.Lab.SCCore.Interfaces;

namespace Daxor.Lab.SCCore
{
    /// <summary>
    /// Summary description for SMControllerUsb.
    /// Sample Changer abstraction layer, through which we communicate with the sample changer hardware
    ///		
    ///	Originally written by Steve Johnson
    ///	History:
    ///		version 1: ISA PIO card (the DIO-24)
    ///		version 2: LPTWrapper to non-DotNet version of TVich.
    ///		version 3: Direct to DotNet version of TVich.
    /// </summary>
    public class SMControllerUsb: ISampleChangerImplementation
    {
        #region Fields

        private DateTime m_dtLastRead = DateTime.Now;
        private static SMControllerUsb m_SampleChanger;  ///protection against multiple instatiations
        private static bool m_bIsOpen = false;		 ///exposed through a property
        private bool disposed = false;               ///used in implementation of IDisposable
        private bool m_bIsMoving = false;	         ///set on start and stop
        private ushort BOT;
        private ushort AuxIn;
        private ushort Safety;
        private ushort Strobe_LL;

        BvaControl _controller = new BvaControl();
        #endregion

        #region Ctor
       
        /// <summary>
        /// Returns the current usb sample changer (creates one if none exist)
        /// </summary>
        /// <remarks>Follows singleton pattern</remarks>
        /// <returns>Usb implementation of a sample changer</returns>
        public static SMControllerUsb GetSampleChanger()
        {
            if (m_SampleChanger == null)
                m_SampleChanger = new SMControllerUsb();

            return (m_SampleChanger);
        }

        /// <summary>
        /// Actual constructor.  
        /// Doesn't do anything any more, but has to exist so that it can be protected.
        /// </summary>
        protected SMControllerUsb()
        {
            HPTimer.HPTIMER_Init();

            // This eliminates the compiler warning about m_bIsMoving being assigned to but never used.
            // GMazeroff 9/2/2010 3:42pm
            if (m_bIsMoving) m_bIsMoving = true;
        }
        
        #endregion

        #region Properties
     
        public bool Open
        {
            ///Gets the cached status of the connection to TVich.
            get { return m_bIsOpen; }

            ///Opens/closes the connection to TVich, and resets the cached status
            set
            {
                if (value)
                    OpenChanger();
                else
                    CloseChanger();
            }
        }
        public short Position
        {
            get
            {
                return (short)ReadChangerPosition();
                //ReadChangerPosition is protected against DEMO iteself, so that protection is not needed here.
            }
        }
        public ushort LowerLevelSensor
        {
            get
            {
                int n = ((int)Strobe_LL) > 0 ? 0 : 1;
                return (ushort)n;
            }
        }
        public ushort BOTSensor
        {
            get
            {
                int n = ((int)BOT) > 0 ? 0 : 1;
                return (ushort)n;
            }
        }

        #endregion

        #region Methods

        public void StartMotor()
        {
            StartSampleChangerMotor();
        }
        public void StopMotor()
        {
            StopSampleChangerMotor();
        }
        public void PulseMotor()
        {
            PulseSampleChangerMotor();
        }

        public BitArray ReadRawPinData()
        {
            //Index starts at 1 instead of 0
            BitArray bArray = new BitArray(26, false);

            ushort data = _controller.ReadPosition();

            bArray[11] = (data & 16) == 16;
            bArray[10] = (data & 8) == 8;
            bArray[12] = (data & 4) == 4;
            bArray[13] = (data & 2) == 2;
            bArray[15] = (data & 1) == 1;

            return bArray;
        }
        #endregion

        #region Sample Changer Abstraction Functions
        
        public void ReadPorts()
        {
            //	Storage for read data lines.  These will later be fleshed out
            //	For WinBVA 5.4

            byte portbyte = _controller.ReadPorts();

            BOT = (ushort)(portbyte & 1);
            AuxIn = (ushort)(portbyte & 2);
            Safety = (ushort)(portbyte & 4);
            Strobe_LL = (ushort)(portbyte & 8);

        }
        private ushort ReadChangerPosition()
        {
            return _controller.ReadPosition();
        }

        private void OpenChanger()
        {
            m_bIsOpen = _controller.Connect();
        }
        private void CloseChanger()
        {
            if (m_bIsOpen)
                _controller.Disconnect();

            m_bIsOpen = false;
            m_SampleChanger = null;
        }

        private void StartSampleChangerMotor()
        {
            _controller.StartMotor();
            #region Event Logger - only being logged if ran in Debug mode
#if DEBUG
            EventLogger.WriteErrorLogEntry("StartSampleChangerMotor - Sample changer motor being started.");
#endif
            #endregion

            m_dtLastRead = DateTime.Now;
            m_bIsMoving = true;
        }
        private void StopSampleChangerMotor()
        {

            _controller.StopMotor();
            #region Event Logger - only being logged if ran in Debug mode
#if DEBUG
            EventLogger.WriteErrorLogEntry("StopSampleChangerMotor - Sample changer motor stopped.");

#endif
            #endregion
            m_bIsMoving = false;
        }
        public void PulseSampleChangerMotor()
        {
            _controller.PulseMotor(100); //pulse motor for 100us
            EventLogger.WriteErrorLogEntry("Sample Changer Pulsed.");
        }

        #endregion

        #region Destructors

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue 
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the 
        // runtime from inside the finalizer and you should not reference 
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources.
                if (disposing)
                {

                }
                StopSampleChangerMotor();
                CloseChanger();

            }
            disposed = true;
        }
        ~SMControllerUsb()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }
        #endregion

    }
}
