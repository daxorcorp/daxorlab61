﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Daxor.Lab.SCCore.Utilities
{
    public class ErrorCode
    {
        //Error Codes  5XXX indicate fatal system failures, 6XXX are non-fatal errors
        public static readonly int FILE_NOT_FOUND = 5001;
        public static readonly int UNDIFFERENTIATED_EXCEPTION = 5002;
        public static readonly int CANNOT_FIND_INITIAL_POSITION = 5004;
        public static readonly int NOT_INITIALIZED = 5005;
        public static readonly int UNABLE_TO_LOCATE_XML_FILES = 5007;
        public static readonly int UNABLE_TO_CONNECT_TO_SAMPLECHANGER = 5009;

        public static readonly int UNEXPECTED_POSITION_MOVE = 6001;
        public static readonly int INVALID_OPERATION = 6002;
        public static readonly int WATCH_LIMIT_EXCEEDED = 6005;
        public static readonly int UNABLE_TO_FIND_NEXT_POSITION = 6011;
    }
}
