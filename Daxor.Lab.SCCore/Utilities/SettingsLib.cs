﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace Daxor.Lab.SCCore.Utilities
{
    public class SettingsLib
    {
        #region Fields

        static readonly SettingsLib SingletonInstance = new SettingsLib("smcSettings.xml");
        private static string _actualDirectory;
        private static string _fileName;
        private readonly List<Setting> _settings = new List<Setting>();
        private static string _fullFilePath;

        #endregion

        #region Ctor

        private SettingsLib(string file) 
        {
            _fileName = file;
            _actualDirectory = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
        }

        #endregion

        public static SettingsLib FromFile(string filePath)
        {
            _fileName = filePath;
            return SingletonInstance;
        }

        #region Properties

        public static SettingsLib Instance
        {
            get
            {
                if (_fileName != null)
                {
                    SingletonInstance._settings.Clear();
                    SingletonInstance.LoadSettings();
                }

                _fileName = null;
                return SingletonInstance;
            }
        }

        #endregion

        /// <summary>
        /// Loads settings from an xml file
        /// </summary>
        private void LoadSettings()
        {
            _fullFilePath = _actualDirectory + @"\" + _fileName;
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(_fullFilePath);

            var settingsList = xmlDoc.GetElementsByTagName("SETTING");
             
            // Go throug each setting name/value add them to the dictionary
            foreach (XmlNode node in settingsList)
            {
                var settingElement = (XmlElement) node;
                var setting = new Setting
                    {
                        DefaultValue = settingElement.GetElementsByTagName("DEFAULTVALUE")[0].InnerText,
                        Description = settingElement.GetElementsByTagName("DESCRIPTION")[0].InnerText,
                        Name = settingElement.GetElementsByTagName("NAME")[0].InnerText,
                        ValueType = settingElement.GetElementsByTagName("TYPE")[0].InnerText,
                        Value = settingElement.GetElementsByTagName("VALUE")[0].InnerText
                    };

                if (setting.ValueType == "NUMBER")
                {
                    int tempValue;
                    if (Int32.TryParse(setting.Value.ToString(), out tempValue))
                        InternalAddSetting(setting);
                }
                else if (setting.ValueType == "BOOL")
                {
                    bool tempValue;
                    if (Boolean.TryParse(setting.Value.ToString(), out tempValue))
                        InternalAddSetting(setting);
                }
                else if (setting.ValueType == "DOUBLE")
                {
                    double tempValue;
                    if (double.TryParse(setting.Value.ToString(), out tempValue))
                        InternalAddSetting(setting);
                }
                else
                    InternalAddSetting(setting);
            }
        }

        /// <summary>
        /// Saves all of the settings back to the xml file
        /// </summary>
        private void SaveSettings()
        {
            try
            {
                using (var writer = XmlWriter.Create(_fullFilePath))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("SETTINGS");
                    foreach (var setting in _settings)
                    {
                        writer.WriteStartElement("SETTING");
                        writer.WriteElementString("NAME", setting.Name);
                        writer.WriteElementString("VALUE", setting.Value.ToString());
                        writer.WriteElementString("DEFAULTVALUE", setting.DefaultValue);
                        writer.WriteElementString("DESCRIPTION", setting.Description);
                        writer.WriteElementString("TYPE", setting.ValueType);
                        writer.WriteEndElement();
                    }
                    writer.WriteEndDocument();
                }
            }
            catch (Exception ex)
            {
                EventLogger.WriteErrorLogEntry("Error writing the settings back: " + ex.Message, System.Diagnostics.EventLogEntryType.Error);
            }
        }
      
        /// <summary>
        /// Returns value of a given setting, if setting name is valid; default value otherwise.
        /// </summary>
        /// <param name="settingName">Setting Name</param>
        /// 
        public T GetValue<T>(string settingName)
        {
            if (settingName == null)
                return default(T);
            
            var setting = _settings.Find(e => e.Name == settingName);

            if (setting == null)
                return default(T);

            object temp = default(T);
            if (setting.ValueType == "NUMBER")
                temp = Int32.Parse(setting.Value.ToString());
            else if ((string) setting.Value == "BOOL")
                temp = Boolean.Parse(setting.Value.ToString());
            else if ((string) setting.Value == "DOUBLE")
                temp = Double.Parse(setting.Value.ToString());
            else if (setting.ValueType == "STRING")
                temp = setting.Value.ToString();

            return (T)temp;
        }

        /// <summary>
        /// Replaces the value of one setting with another value
        /// </summary>
        /// <param name="settingName">Name of the setting to update</param>
        /// <param name="value">New value to store</param>
        /// <param name="commitChanges">True: updates xml file as well</param>
        /// <remarks>Does not check to see if value makes sense for the setting 
        /// (i.e. if a setting ValueType is int, but you pass a boolean, it will store the value)
        /// </remarks>
        public void SetSetting(string settingName, object value, bool commitChanges = false)
        {
            if (settingName == null)
                throw new ArgumentNullException("settingName");
            if (value == null)
                throw new ArgumentNullException("value");

            var setting = _settings.Find(e => e.Name == settingName);
            if (setting == null)
                return;

            setting.Value = value;
            _settings[_settings.FindIndex(e => e == setting)] = setting;

            if (commitChanges)
                SaveSettings();
        }

        #region Helpers

        void InternalAddSetting(Setting setting)
        {
            //Check if a given setting already contains a value
            if (setting == null)
                return;
            var temp = _settings.Find(e => e.Name == setting.Name);

            if (temp == null)
                _settings.Add(setting);
            else
            {
                var index = _settings.FindIndex(e => e == setting);
                if (temp.Value == null)
                    temp.Value = setting.Value;
                _settings[index] = temp;
            }
        }

        #endregion
    }
}
