﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Daxor.Lab.SCCore.Utilities
{
    /// <summary>
    /// Summary description for HPTimer.
    /// A High Precision (not Hewlett Packard) timer for use with position logging
    ///		in the Position Monitor
    ///	Originally developed by Mike Burkey in Visual Basic
    ///	Translated into C# by Steve Johnson
    /// </summary>
    public class HPTimer
    {
        [StructLayout(LayoutKind.Explicit)]
        public struct LARGE_INTEGER
        {
            [FieldOffset(0)]
            public Int32 LowPart;
            [FieldOffset(4)]
            public Int32 HighPart;
        }
        [DllImport("kernel32.dll", EntryPoint = "QueryPerformanceCounter", ExactSpelling = false, CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern UInt32 QueryPerformanceCounter(ref LARGE_INTEGER lpPerformanceCount);
        [DllImport("kernel32.dll", EntryPoint = "QueryPerformanceFrequency", ExactSpelling = false, CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern UInt32 QueryPerformanceFrequency(ref LARGE_INTEGER li);
        private static bool mvbModuleInitialized = false;
        private static double mvdTicksPerSecond = 0;
        public static double HPTIMER_Resolution
        {
            get
            {
                if (mvdTicksPerSecond != 0)
                {
                    return (1 / mvdTicksPerSecond);
                }
                else
                {
                    return mvdTicksPerSecond;
                }
            }
        }

        public HPTimer()
        {

        }
        public static bool HPTIMER_Init()
        {
            LARGE_INTEGER ticks = new LARGE_INTEGER();


            // if we are already initialized, then go ahead and exit
            if (mvbModuleInitialized)
            {
                return true;
            }

            // check to see if the high performance counter is available
            if (QueryPerformanceFrequency(ref ticks) != 0)
            {
                // save the resolution of the counter in ticks/second
                mvdTicksPerSecond = HPTIMER_LICDbl(ref ticks);
                mvbModuleInitialized = true;
                return true;
            }
            else
            {
                // the timer is not available, return an error
                mvdTicksPerSecond = -1;
                return false;
            }
        }

        public static double HPTIMER_Now()
        {
            double dReturn = -1;
            LARGE_INTEGER ticks = new LARGE_INTEGER();
            if (QueryPerformanceCounter(ref ticks) != 0)
            {
                dReturn = HPTIMER_LICDbl(ref ticks);
            }
            return dReturn;
        }

        public static void HPTIMER_Delay(double delay_time)
        {
            // wait for "delay_time" * .000001 seconds
            double starttime = HPTIMER_Now();
            while (!HPTIMER_Elapsed(starttime, delay_time)) ;
        }

        public static double HPTIMER_Duration(double starttime, double stoptime)
        {
            // MDB -- eventually need to add code to handle roll-over conditions (i.e. starttime > now)
            if (mvdTicksPerSecond != 0)
            {
                return (stoptime / mvdTicksPerSecond) - (starttime / mvdTicksPerSecond);
            }
            else
            {
                return mvdTicksPerSecond;
            }
        }

        public static bool HPTIMER_Elapsed(double starttime, double secs)
        {
            return (HPTIMER_Duration(starttime, HPTIMER_Now()) > secs);
        }

        private static double HPTIMER_LICDbl(ref LARGE_INTEGER LI)
        {
            double Low = 0;
            double dReturn = 0;

            Low = (double)LI.LowPart;
            if (Low < 0)
            {
                Low = Low + Math.Pow(2.0, 32);
            }
            // Low is in the range 0...2^32-1
            dReturn = LI.HighPart * Math.Pow(2.0, 32) + Low;

            return (dReturn);
        }
    }
}
