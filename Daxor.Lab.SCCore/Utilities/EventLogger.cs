﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Daxor.Lab.SCCore.Utilities
{
    /// <summary>
    /// SCLogger - Sample Changer Logger
    /// SCLogger is responsible for logging any operation/error occurred 
    /// during service execution.
    /// </summary>
    public class EventLogger
    {
        #region Fields
        //EventLog used for logging errors/events
        private static EventLog m_Log;

        #endregion

        #region Methods

        /// <summary>
        /// Creates log entry in the SampleChangerEventLog
        /// </summary>
        /// <param name="strMessage">Message that gets written to the log</param>
        public static void WriteErrorLogEntry(string strMessage)
        {
            GetEventLog().WriteEntry(DateTime.Now.ToString() + " " + strMessage);
        }

        /// <summary>
        /// Overload of WriteErrorLogEntry methods.
        /// Creates a log entry including a stack trace in the SampleChangerEventLog
        /// </summary>
        /// <param name="strMessage">Message that gets written to the log</param>
        /// <param name="evtType">Specifies the type of the entry being made</param>
        public static void WriteErrorLogEntry(string strMessage, EventLogEntryType evtType)
        {

            System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace();
            string strStackTrace = string.Empty;
            if (evtType == EventLogEntryType.Error)
            {
                for (int i = 0; i < st.FrameCount; i++)
                {
                    StackFrame sf = st.GetFrame(i);
                    strStackTrace += sf.GetMethod().Name + "\n" + sf.GetFileLineNumber().ToString();
                }
            }

            GetEventLog().WriteEntry(DateTime.Now.ToString() + " " + strMessage + "\nStack trace:\n" + strStackTrace, evtType);
        }


        /// <summary>
        /// Tries to create an EventLog source with the source named SampleChangerServiceLog.
        /// I
        /// </summary>
        /// <returns>EventLog object</returns>
        private static EventLog GetEventLog()
        {

            if (!EventLog.SourceExists("SampleChangerServiceLog"))
            {
                //Registers events source with the log where the entries will be written
                EventLog.CreateEventSource("SampleChangerServiceLog", "SampleChanger Service Log");
            }
            if (m_Log == null)
            {

                m_Log = new EventLog();
                m_Log.Source = "SampleChangerServiceLog";

                //Setting some log properties:
                //1. MaximumKilobytes size = 1GB
                //2. Overwrite log file when necessary
                m_Log.MaximumKilobytes = 1024 * 1024;
                m_Log.ModifyOverflowPolicy(OverflowAction.OverwriteAsNeeded, 0);
            }

            return m_Log;
        }

        #endregion
    }
}
