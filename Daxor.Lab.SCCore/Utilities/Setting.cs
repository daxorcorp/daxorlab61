﻿namespace Daxor.Lab.SCCore.Utilities
{
    /// <summary>
    /// Class used for the SettingsLib. Holds the same information that can be found in smcSettings.xml
    /// </summary>
    public class Setting
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public string DefaultValue { get; set; }
        public string Description { get; set; }
        public string ValueType { get; set; }

        public Setting(string name, object value, string defaultValue, string description, string type)
        {
            Name = name;
            Value = value;
            DefaultValue = defaultValue;
            Description = description;
            ValueType = type;
        }

        public Setting() { }
    }
}
