﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Daxor.Lab.SCCore.Threads;
using Daxor.Lab.SCCore.Utilities;
using System.Diagnostics;
using System.Collections;
using Daxor.Lab.SCCore.Interfaces;

namespace Daxor.Lab.SCCore
{
    /// <summary>
    /// Sample Changer Controller.
    /// </summary>
    public class SMController
    {
        #region Member Fields

        private ManualResetEventSlim m_PositionMonitorInitializationCompletedEvent = new ManualResetEventSlim(false);
        private AutoResetEvent m_MovingMonitorBusyEvent = new AutoResetEvent(false);

        private PositionMonitor m_PositionMonitor;
        private MovingMonitor m_MovingMonitor;
        
        private static SMController m_SampleChangerController;
        private ISampleChangerImplementation m_SampleChanger;
        private DateTime m_dtLastPositionFound;
        protected internal int m_nNewPosition;
        protected internal int m_nPosition;  //Current GOOD Position
        private bool m_IsStopping;            //Set to true if sample changer is in the proccess of stopping

        #endregion

        #region Constructor Definitions

        /// <summary>
        /// Public static classfactory to create or return a SampleChanger object.  
        /// This helps insure that there is only one sample changer controller existant in 
        /// the system.
        /// 		/// </summary>
        /// <returns></returns>
        public static SMController GetSampleChangerControler()
        {

            if (m_SampleChangerController == null)
            {
                //Instantiates sample changer
                m_SampleChangerController = new SMController();
                m_SampleChangerController.m_MovingMonitor = MovingMonitor.GetSampleChangerMovingMonitor(m_SampleChangerController);
                m_SampleChangerController.m_MovingMonitor.ChangerError += new MovingMonitor.ChangerErrorEventHandler(m_SampleChangerController.m_MovingMonitor_ChangerError);
            }

            return (m_SampleChangerController);
        }
        protected SMController()
        {

            this.PositionChangeBegin += new PositionChangeBeginEventHandler(SampleChangerControler_PositionChangeBegin);
            this.PositionChangeEnd += new PositionChangeEndEventHandler(SampleChangerControler_PositionChangeEnd);
            this.PositionSeekCancel += new PositionSeekCancelEventHandler(SampleChangerControler_PositionSeekCancel);
            this.PositionSeekBegin += new PositionSeekBeginEventHandler(SampleChangerControler_PositionSeekBegin);
            this.PositionSeekEnd += new PositionSeekEndEventHandler(SampleChangerControler_PositionSeekEnd);
            this.ChangerError += new ChangerErrorEventHandler(SampleChangerControler_ChangerError);

            try
            {
                //Load Sample Changer setting file before MovingMonitor and PositionMonitor threads
                //as both of them would use those settings
                SettingsLib.FromFile("smcSettings.xml");
                SettingsLib.Instance.GetValue<Int32>("NumConsecutivePositionReads");

                m_SampleChanger = SMControllerUsb.GetSampleChanger();
                m_SampleChanger.Open = true;

                //Applies only to Auto-Stop or ONE_SHOT sample changer, which moves one position
                //and stops.
                m_SampleChanger.ReadPorts();
                if (m_SampleChanger.LowerLevelSensor != 1)
                    m_SampleChanger.PulseSampleChangerMotor();

                m_PositionMonitor = PositionMonitor.GetSampleChangerPositionMonitor(PositionMonitorInitCompletedEvent, SettingsLib.Instance.GetValue<Int32>("SampleChangerType"));
                m_PositionMonitor.PositionChanged += new PositionMonitor.PositionChangedEvent(m_PositionMonitor_PositionChanged);
                m_PositionMonitor.ChangerError += new PositionMonitor.ChangerErrorEvent(m_PositionMonitor_ChangerError);
                m_PositionMonitor.LoggingComplete += new PositionMonitor.LoggingCompleteEvent(m_PositionMonitor_LoggingComplete);

                InitializeChanger();


            }
            catch (SampleChangerInitializationException ex)
            {
                EventLogger.WriteErrorLogEntry(ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                throw;
            }
            catch (Exception ex)
            {
                EventLogger.WriteErrorLogEntry(ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Error);
                throw new SampleChangerInitializationException(Utilities.ErrorCode.NOT_INITIALIZED, ex.Message, ex);
            }
        }

        #endregion

        #region Methods
      
        /// <summary>
        /// Member functions
        /// </summary>
        /// 
        #region Miscellaneous methods

        void InitializeChanger()
        {
            //Wait 10 seconds to get InitializationCompletedEvent from PostionMonitor thread
            if (!PositionMonitorInitCompletedEvent.Wait(10000))
            {
                String error = "InitializeChanger - Unable to find initial position";
                EventLogger.WriteErrorLogEntry(error, EventLogEntryType.Error);
                m_PositionMonitor.Terminate();
                throw new SampleChangerInitializationException(ErrorCode.CANNOT_FIND_INITIAL_POSITION); 
            }

            m_nPosition = m_PositionMonitor.CurrentPosition;
            m_dtLastPositionFound = DateTime.Now;
            if (m_nPosition == 0)
            {
                String error = "InitializeChanger - Unable to connect to sample changer. Check sample changer power.";
                EventLogger.WriteErrorLogEntry(error, EventLogEntryType.Error);
                throw new SampleChangerInitializationException(ErrorCode.UNABLE_TO_CONNECT_TO_SAMPLECHANGER); 
            }

            EventLogger.WriteErrorLogEntry("Initial position = " + m_nPosition.ToString());
        }

        #region Logging

        /// <summary>
        /// Used with the green button in debugging
        /// largely superceded by PM Tests
        /// Retained for future logging needs
        /// </summary>
        public void WriteLog()
        {
#if LOG
			m_PositionMonitor.WriteLog();
#endif
        }
        public void StartLoggingPosition()
        {
            m_MovingMonitor.IsLogging = true;
            m_PositionMonitor.StartLoggingPosition();
        }
        public void StopLoggingPosition()
        {
            m_PositionMonitor.StopLoggingPosition();
            m_MovingMonitor.IsLogging = false;
        }
        
        #endregion

        #endregion

        #region Sample changer control functions
       
        /// <summary>
        /// Raw Read
        /// </summary>
        protected internal short ReadChangerPosition()
        {
            return (m_PositionMonitor.CurrentPosition);
        }
        protected internal void StartSampleChangerControlerMotor()
        {
            m_MovingMonitor.IsMoving = true;
            m_IsStopping = false;
            this.PulseSampleChangerMotor();
        }
        protected internal void StopChangerMotor()
        {
            m_IsStopping = true;
            m_SampleChanger.StopMotor();
            m_MovingMonitor.IsMoving = false;
        }
        public void PulseSampleChangerMotor()
        {
            m_MovingMonitor.IsMoving = true;
            m_IsStopping = false;
            m_SampleChanger.PulseSampleChangerMotor();
        }


        #endregion

        #endregion

        #region Public Interface Properties

        #region Synchronization

        public ManualResetEventSlim PositionMonitorInitCompletedEvent
        {
            get { return m_PositionMonitorInitializationCompletedEvent; }
        }
        public AutoResetEvent MovingMonitorBusyEvent
        {
            get { return m_MovingMonitorBusyEvent; }
        }

        #endregion

        #region Position
        public int Position
        {
            get { return m_nPosition; }
            set
            {
                m_nNewPosition = value;
                if (m_nNewPosition == -1)
                {
                    StopChangerMotor();
                }
                else
                {
                    if ((m_nNewPosition < 1) || ((m_nNewPosition > 25) && (m_nNewPosition != 32)))
                    {
                        OnChangerError(ErrorCode.UNDIFFERENTIATED_EXCEPTION, "Invalid Position " + m_nNewPosition);
                    }
                    else
                    {
                        m_dtLastPositionFound = DateTime.Now;
                        EventLogger.WriteErrorLogEntry("Reset Last Position Attained: " + LastPositionAttained.ToLongTimeString());

                        //Fire Position Seek and Begin Change Events
                        OnPositionSeekBegin(m_nPosition, m_nNewPosition);
                        OnPositionChangeBegin(m_nPosition, m_nNewPosition);

                        // Are we at the position we are seeking?
                        if (m_nNewPosition != m_PositionMonitor.CurrentPosition)
                        {
                            EventLogger.WriteErrorLogEntry("Change position from " + m_PositionMonitor.CurrentPosition.ToString() +
                                                        " to " + m_nNewPosition.ToString(), EventLogEntryType.Information);
                            StartSampleChangerControlerMotor();
                        }
                        else
                        {
                            //Get current position
                            m_nPosition = m_PositionMonitor.CurrentPosition;
                            // Fire End Pos Seek and End Pos Change
                            OnPositionChangeEnd(m_nPosition);
                            OnPositionSeekEnd(m_nPosition);
                        }
                        Thread.Sleep(0);
                    }
                }
            }
        }
        #endregion

        #region IsMoving
        public bool IsMoving
        {
            get
            {
                return m_MovingMonitor.IsMoving;
            }
        }
        #endregion

        #region Closed
        public bool Closed
        {
            get { return !m_SampleChanger.Open; }
            set
            {
                if (value)
                {
                    try
                    {
                        if (this.IsMoving)
                        {
                            StopChangerMotor();
                        }
                        if (m_SampleChanger.Open)
                        {
                            m_SampleChanger.Open = false;
                        }
                        m_PositionMonitor.Terminate();
                        m_MovingMonitor.Terminate();
                        m_SampleChanger.Dispose();
                        m_SampleChanger = null;
                        m_SampleChangerController = null;
                    }
                    catch (ApplicationException ex)
                    {
                        EventLogger.WriteErrorLogEntry(ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Warning);
                    }
                }
                else
                {
                    if (!m_SampleChanger.Open)
                    {
                        m_SampleChanger.Open = true;
                    }
                }
            }
        }
        #endregion

        #region PositionLog
        public double[,] PositionLog
        {
            get { return m_PositionMonitor.m_arLog; }
        }
        #endregion

        #region CurrentPositionRead
        public int CurrentPositionRead
        {
            //	Used by PM Tests
            //	Returns what the port reads
            get
            {
                int nReturn = 0;
                m_SampleChanger.ReadPorts();
                nReturn = m_SampleChanger.Position;
                nReturn += (64 * (m_SampleChanger.LowerLevelSensor));
                nReturn += (128 * (m_SampleChanger.BOTSensor));
                return nReturn;
            }

        }
        #endregion

        #region SleepTime

        public TimeSpan SleepTime
        {
            get { return m_PositionMonitor.SleepTimespan; }
            set
            {
                m_PositionMonitor.SleepTimespan = value;
            }
        }
       
        #endregion

        #region TestDelay
       
        public double TestDelay
        {
            get { return m_PositionMonitor.TestDelay; }
            set
            {
                m_PositionMonitor.TestDelay = value;
            }
        }

        #endregion

        #region TestRecordsRequired
        public int TestRecordsRequired
        {
            get { return m_PositionMonitor.TestRecordsRequired; }
            set
            {
                m_PositionMonitor.TestRecordsRequired = value;
            }
        }
        #endregion

        #region LastPositionAttained
        // Time last good position was found/left
        public DateTime LastPositionAttained
        {
            // Called by Moving Monitor to detect error conditions with the sample changer
            get { return m_dtLastPositionFound; }
            // This should only be called by PM Test when it needs to directly pulse the sample changer.
            set { m_dtLastPositionFound = value; }

        }
        #endregion

        public BitArray ReadRawSensorData()
        {
            return m_SampleChanger.ReadRawPinData();
        }

        /// <summary>
        /// Sets the sample changer type (parallel port or usb)
        /// </summary>
        /// <param name="type">Type of Sample Changer (0 - parallel port, 1 - usb)</param>
        public void SetSampleChangerType(int type)
        {
            SettingsLib.Instance.SetSetting("SampleChangerType", type, commitChanges: true);
        }

        #endregion

        #region Events

        #region Delegate Declarations
        /// <summary>
        /// Delegates to manage SampleChangerControler Events
        /// </summary>
        /// <returns></returns>
        /// 
        public delegate void PositionSeekBeginEventHandler(int CurrentPosition, int SeekingPosition);
        public delegate void PositionSeekEndEventHandler(int CurrentPosition);
        public delegate void PositionSeekCancelEventHandler(int CurrentPosition, int SeekingPosition);
        public delegate void PositionChangeBeginEventHandler(int OldPosition, int NewPosition);
        public delegate void PositionChangeEndEventHandler(int NewPosition);
        public delegate void ChangerErrorEventHandler(int ErrorCode, string Message);
        public delegate void LoggingCompleteEvent();
        #endregion

        /// <summary>
        /// Events
        /// </summary>
        /// 
        #region PositionSeekCancel
        public event PositionSeekCancelEventHandler PositionSeekCancel;

        protected virtual void OnPositionSeekCancel(int CurrentPosition, int SeekingPosition)
        {
            if (PositionSeekCancel != null)
            {

                PositionSeekCancel(CurrentPosition, SeekingPosition);
            }
        }
        #endregion

        #region PositionSeekBegin
        public event PositionSeekBeginEventHandler PositionSeekBegin;
        protected virtual void OnPositionSeekBegin(int CurrentPosition, int SeekingPosition)
        {
            if (PositionSeekBegin != null)
            {

                PositionSeekBegin(CurrentPosition, SeekingPosition);
            }
        }
        #endregion

        #region PositionSeekEnd
        public event PositionSeekEndEventHandler PositionSeekEnd;
        protected virtual void OnPositionSeekEnd(int CurrentPosition)
        {
            if (PositionSeekEnd != null)
            {

                PositionSeekEnd(CurrentPosition);
            }
        }
        #endregion

        #region PositionChangeBegin
        public event PositionChangeBeginEventHandler PositionChangeBegin;

        protected virtual void OnPositionChangeBegin(int OldPosition, int NewPosition)
        {
            if (PositionChangeBegin != null)
            {

                PositionChangeBegin(OldPosition, NewPosition);
            }
        }
        #endregion

        #region PositionChangeEnd
        public event PositionChangeEndEventHandler PositionChangeEnd;

        protected virtual void OnPositionChangeEnd(int NewPosition)
        {
            if (PositionChangeEnd != null)
            {

                PositionChangeEnd(NewPosition);
            }
        }
        #endregion

        #region ChangerError
        public event ChangerErrorEventHandler ChangerError;

        protected virtual void OnChangerError(int ErrorCode, string Message)
        {
            if (ChangerError != null)
            {

                ChangerError(ErrorCode, Message);
            }
        }
        #endregion

        #region LoggingComplete
        public event LoggingCompleteEvent LoggingComplete;
        protected virtual void OnLoggingComplete()
        {
            if (LoggingComplete != null)
            {
                LoggingComplete();
            }
        }
        #endregion

        #endregion

        #region Event handlers

        private void m_PositionMonitor_ChangerError(int ErrorCode, string Message)
        {
            EventLogger.WriteErrorLogEntry("Error " + ErrorCode.ToString() + " " + Message, EventLogEntryType.Error);
            OnChangerError(ErrorCode, Message);
        }
        private void m_PositionMonitor_PositionChanged(int NewPosition)
        {
            //NewPosition is the Current Good Position, as just delivered from the Position Monitor
            EventLogger.WriteErrorLogEntry("Position changed New Position = " + NewPosition.ToString());
            if (NewPosition == 0)
            {
                OnChangerError(ErrorCode.UNEXPECTED_POSITION_MOVE, "Check sample changer power.");
                return;
            }
            if (NewPosition > 0 && NewPosition < 26)
            {
                m_nPosition = NewPosition;
                m_dtLastPositionFound = DateTime.Now;
            }

            if (!m_MovingMonitor.IsMoving && !m_IsStopping)
            {
                // If we have not been told programmatically to move, always fire this error
                // The business logic will decide whether or not to inform the UI
                // The UI will be informed only if counting a position or counting background

                OnChangerError(ErrorCode.UNEXPECTED_POSITION_MOVE, "Unexpected position change. Position " +
                    NewPosition.ToString() + " detected. IsMoving = " + m_MovingMonitor.IsMoving);
            }
            else
            {

                if (NewPosition > 0 && NewPosition < 26)
                {
                    if (m_nPosition == m_nNewPosition)
                    {
                        StopChangerMotor();
                        if (ReadChangerPosition() != m_nNewPosition)
                        {
                            EventLogger.WriteErrorLogEntry("Didn't stop in time.");
                            //consider bubbling an error up here
                        }
                        OnPositionChangeEnd(m_nPosition);  // Always fire position change end event
                        OnPositionSeekEnd(m_nPosition);
                        this.m_IsStopping = false;
                    }
                    else
                    {
                        OnPositionChangeEnd(m_nPosition);  // Always fire position change end event
                        if (!m_IsStopping)
                        {
                            OnPositionChangeBegin(m_nPosition, m_nNewPosition);
                        }
                        m_IsStopping = false;              //Sample changer is stopped

                        if (m_nNewPosition != -1)
                        {
                            //Wait PulseDelay miliseconds before pulsing again.
                            Thread.Sleep(SettingsLib.Instance.GetValue<Int32>("PulseDelay"));
                            this.PulseSampleChangerMotor();
                        }
                        else
                        {
                            m_MovingMonitor.IsMoving = false;
                        }

                    }
                }
            }
        }
        private void m_PositionMonitor_LoggingComplete()
        {
            m_dtLastPositionFound = DateTime.Now;
            m_MovingMonitor.IsLogging = false;
            OnLoggingComplete();
        }
        private void m_MovingMonitor_ChangerError(int ErrorCode, string Message)
        {
            OnChangerError(ErrorCode, Message);
        }
        private void SampleChangerControler_ChangerError(int ErrorCode, string Message)
        {
#if TEST
			BVALibrary.WriteErrorLogEntry("Error " + ErrorCode.ToString() + " " + Message,
				EventLogEntryType.Error);
			if(m_ChangerThread != null)
			{
				m_ChangerThread = null;
			}
#endif
        }
        private void SampleChangerControler_PositionChangeEnd(int CurrentPosition)
        {
#if TEST
			BVALibrary.WriteErrorLogEntry("PositionChangeEnd " + CurrentPosition);
#endif
        }
        private void SampleChangerControler_PositionChangeBegin(int OldPosition, int NewPosition)
        {
#if TEST
			BVALibrary.WriteErrorLogEntry("PositionChangeBegin " + OldPosition + " to " + NewPosition);
#endif

        }
        private void SampleChangerControler_PositionSeekBegin(int CurrentPosition, int SeekingPosition)
        {
#if TEST
			BVALibrary.WriteErrorLogEntry("PositionSeekBegin " + CurrentPosition + " to " + SeekingPosition);
#endif
        }
        private void SampleChangerControler_PositionSeekCancel(int CurrentPosition, int SeekingPosition)
        {
#if TEST
			BVALibrary.WriteErrorLogEntry("PositionSeekCancel " + CurrentPosition + " seeking " + SeekingPosition);
#endif
        }
        private void SampleChangerControler_PositionSeekEnd(int CurrentPosition)
        {
#if TEST
			BVALibrary.WriteErrorLogEntry("PositionSeekEnd " + CurrentPosition);
#endif
        }
     
        #endregion
    }

    #region Exceptions

    public class SampleChangerInitializationException : Exception
    {
        public int ErrorCode { get; private set;}

        public SampleChangerInitializationException() : base() { }
        public SampleChangerInitializationException(string message) : base(message) { }
        public SampleChangerInitializationException(int errorCode): base()
        {
            this.ErrorCode = errorCode;
        }
        public SampleChangerInitializationException(int errorCode, string message, Exception innerException): base(message, innerException)
        {
            this.ErrorCode = errorCode;
        }
    }

    #endregion
}
