﻿using System;
using System.Collections;

namespace Daxor.Lab.SCCore.Interfaces
{
    /// <summary>
    /// Defines all of the methods that a sample changer implementation (hardware platform) should have defined
    /// </summary>
    public interface ISampleChangerImplementation : IDisposable
    {
        #region Properties

        bool Open { get; set; }
        short Position { get; }
        ushort LowerLevelSensor { get; }
        ushort BOTSensor { get; }

        #endregion

        #region Methods

        void StartMotor();
        void StopMotor();
        BitArray ReadRawPinData();
        void PulseSampleChangerMotor();
        void ReadPorts();
        
        #endregion
    }
}
