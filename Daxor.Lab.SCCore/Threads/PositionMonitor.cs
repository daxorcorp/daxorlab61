﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Runtime.CompilerServices;
using Daxor.Lab.SCUSBControl;
using System.Diagnostics;
using Daxor.Lab.SCCore.Interfaces;
using Daxor.Lab.SCCore.Utilities;

namespace Daxor.Lab.SCCore.Threads
{
    /// <summary>
    /// Reads the current position as seen by sample changer.
    /// </summary>
    public class PositionMonitor
    {
        #region Member Fields
       
        private static PositionMonitor m_SampleChangerPositionMonitor;
        private short m_CurrentPosition = -1;
        private Thread m_Thread;
        private bool m_bTerminate = false;
        private short m_sWatchLimit = 50;	//max number of attempts 
        private int m_sMatchCount;	    //number of consecutive identical reads to define a good position; value received from settings file
        private bool m_bInitializing = true;
        private bool m_bLogging = false;
        private static double LastRead;
        private ISampleChangerImplementation m_SampleChanger;


        /*******************************************************
         * Sleep for 25 milliseconds between reads.  
         * This usually creates a pause of 30 ms.  This means 
         * that it requires 240 milliseconds to detect a 
         * position (8 reads * 30 ms between == 240).
         * *****************************************************/
        private TimeSpan m_SleepTimespan = new TimeSpan(0, 0, 0, 0, 25);

        //These member variables used for logging from PM Tests
        public double[,] m_arLog;
        int UBound = 2600000;
        double m_TestDelay = 0.0001;	//=.1 ms

        private ManualResetEventSlim _positionMonitorInitCompletedEvent; //Singals when this tread completes initialization

        #endregion

        #region Events
        
        #region Delegate Declarations
        public delegate void PositionChangedEvent(int NewPosition);
        public delegate void ChangerErrorEvent(int ErrorCode, string Message);
        public delegate void LoggingCompleteEvent();
        #endregion

        #region PositionChanged
        public event PositionChangedEvent PositionChanged;
        protected virtual void OnPositionChanged(int NewPosition)
        {
            if (PositionChanged != null)
            {
                PositionChanged(NewPosition);
            }
        }
        #endregion

        #region ChangerError
        public event ChangerErrorEvent ChangerError;
        protected virtual void OnChangerError(int ErrorCode, string Message)
        {
            if (ChangerError != null)
            {
                ChangerError(ErrorCode, Message);
            }
        }
        #endregion

        #region LoggingComplete
        public event LoggingCompleteEvent LoggingComplete;
        protected virtual void OnLoggingComplete()
        {
            if (LoggingComplete != null)
            {
                LoggingComplete();
            }
        }
        #endregion
     
        #endregion

        #region Public Interface Properties

        public short CurrentPosition
        {
            //makes it thread safe
            [MethodImpl(MethodImplOptions.Synchronized)]
            get { return m_CurrentPosition; }
        }
        // max number of times we can run through and not get a good position
        // this will detect wild wobble, possible problems with parallel port
        // nothing currently calls this property
        public short WatchLimit
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get { return m_sWatchLimit; }
            [MethodImpl(MethodImplOptions.Synchronized)]
            set { m_sWatchLimit = value; }
        }
        public TimeSpan SleepTimespan
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get { return m_SleepTimespan; }
            [MethodImpl(MethodImplOptions.Synchronized)]
            set { m_SleepTimespan = value; }
        }
        public int MatchCount
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get { return m_sMatchCount; }
            [MethodImpl(MethodImplOptions.Synchronized)]
            set { m_sMatchCount = value; }
        }

        // for logging
        public int TestRecordsRequired
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get { return UBound; }
            [MethodImpl(MethodImplOptions.Synchronized)]
            set { UBound = value; }
        }

        public double TestDelay
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get { return m_TestDelay; }
            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                //	Value is in milliseconds
                //	convert to fraction of a second
                m_TestDelay = value / 1000;
            }
        }
        public Thread MonitoringThread
        {
            get { return m_Thread; }
        }

        public double[,] PositionArray
        {
            get { return m_arLog; }
        }

        #endregion

        #region Class Methods
       
        public void Terminate()
        {
            m_bTerminate = true;
            m_SampleChangerPositionMonitor = null;
            m_Thread.Join();

        }

        //local wrapper for the sample position
        private short ReadChangerPosition()
        {
            short nPos = 0;	//returns 0 on failure
            try
            {
                nPos = m_SampleChanger.Position;
            }
            catch (ApplicationException ex)
            {
                EventLogger.WriteErrorLogEntry("ReadChangerPosition " + ex.Message, EventLogEntryType.Warning);
                OnChangerError(ErrorCode.UNDIFFERENTIATED_EXCEPTION, ex.Message);

            }
            return nPos;

        }
        private void ThreadProc()
        {
            LastRead = HPTimer.HPTIMER_Now();
            double currentRead = LastRead;			// v6.0.5.0  Muller changed from CurrentRead...was not recognized
            try
            {
                while (!m_bTerminate)
                {
                    try
                    {

                        #region Initialize counters
                        int sMatchCount = MatchCount;
                        short sWatchLimit;
                        short sInitialCount = ReadChangerPosition();
                        short sCount = sInitialCount;  //it's a position, but we're calling it a count
                        short sLastRead = 0;
                        uint nIndex;
                        #endregion

                        do
                        {
                            sWatchLimit = WatchLimit;

                            if (m_bLogging)
                            {
                            }
                            else
                            {
                                #region Position read loop

                                // This option polls every (m_SleepTimespan, currently 30 milliseconds)
                                // the position sensors (optical or mechanical) // and reports a position
                                // if it reads the same value sMatchCount (currently 8) times.

                                // The routine needs to return 31 and 0 ASAP regardles if LL..

                                nIndex = 0;
                                for (nIndex = 0; nIndex < sMatchCount; )
                                {
                                    sLastRead = ReadChangerPosition();
                                    if (sCount == sLastRead)
                                    {
                                        // If I read the same position as last time through the loop...
                                        nIndex++;
                                    }
                                    else
                                    {
                                        // The position has changed since the last time thorugh the loop
                                        sCount = sLastRead;
                                        nIndex = 0;
                                        if ((sWatchLimit--) == 0)
                                        {
                                            EventLogger.WriteErrorLogEntry("ThreadProc - Unable to find position within watch limit.", EventLogEntryType.Warning);
                                            OnChangerError(ErrorCode.WATCH_LIMIT_EXCEEDED, "Unable to find position within watch limit.");
                                            break;
                                        }
                                    }
                                    System.Threading.Thread.Sleep(m_SleepTimespan);
                                }	// End For
                                #endregion

                                #region Send OnPositionChange if appropriate
                                //m_CurrentPosition is initialized to 0.
                                //the first time through, it will not be equal to the position found
                                //so it will complete initialization, releasing the mutex

                                if (m_CurrentPosition != sCount)
                                {
                                    m_CurrentPosition = sCount;
                                    if (!m_bInitializing)
                                    {
                                        if (sWatchLimit > 0)
                                        {
                                            OnPositionChanged(m_CurrentPosition);
                                        }
                                    }
                                    else
                                    {
                                        //m_InitilizingMutex.ReleaseMutex();
                                        //Wakeup the waiter that has been blocked earlier
                                        m_bInitializing = false;
                                        _positionMonitorInitCompletedEvent.Set();

                                    } 
                                }	
                                #endregion

                            }	
                        } while (sWatchLimit >= 0 && !m_bTerminate);


                    }	
                    catch (ApplicationException ex)
                    {

                        EventLogger.WriteErrorLogEntry("ThreadProc " + ex.Message + "\n" +
                                                       ex.StackTrace, EventLogEntryType.Warning);
                        OnChangerError(ErrorCode.UNDIFFERENTIATED_EXCEPTION, ex.Message);
                    }
                    catch (Exception e)
                    {
                        EventLogger.WriteErrorLogEntry("ThreadProc " + e.Message + "\n" +
                                                       e.StackTrace, EventLogEntryType.Warning);

                    }
                }	
            }	
            finally
            {
                EventLogger.WriteErrorLogEntry("ThreadProc - Exiting position monitor thread.", EventLogEntryType.Warning);
                m_SampleChanger.Dispose();
                m_Thread = null;
            }
        }

        public void StartLoggingPosition()
        {
            m_arLog = new double[2, UBound];
            m_bLogging = true;

        }
        public void StopLoggingPosition()
        {
            m_bLogging = false;
        }

        #endregion

        #region Constructors
        
        public static PositionMonitor GetSampleChangerPositionMonitor(ManualResetEventSlim positionMonitorInitCompletedEvent, int type)//, SampleChangerTypeEnum type)
        {
            // tried this, didn't like it
            if (m_SampleChangerPositionMonitor == null)
                m_SampleChangerPositionMonitor = new PositionMonitor(positionMonitorInitCompletedEvent, type);

            return (m_SampleChangerPositionMonitor);
        }
        protected PositionMonitor(ManualResetEventSlim positionMonitorInitCompletedEvent, int type) //, SampleChangerTypeEnum type)
        {
            //Set number of consecutive identical reads required in order
            //to determine a good position
            m_sMatchCount = SettingsLib.Instance.GetValue<Int32>("NumConsecutivePositionReads");
            _positionMonitorInitCompletedEvent = positionMonitorInitCompletedEvent;

            m_SampleChanger = SMControllerUsb.GetSampleChanger();
            m_SampleChanger.Open = true;
            m_SampleChanger.StopMotor();

            m_Thread = new Thread(new ThreadStart(ThreadProc));
            m_Thread.Priority = ThreadPriority.Highest;
            m_Thread.Name = "PositionMonitorThread";
            m_Thread.Start();

            ///Insures that the worker thread ThreadProc starts execution
            Thread.Sleep(1);
        }
        
        #endregion
    }
}
