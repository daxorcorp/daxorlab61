﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.SCCore.Utilities;
using System.Diagnostics;
using System.Threading;
using System.Runtime.CompilerServices;

namespace Daxor.Lab.SCCore.Threads
{
    /// <summary>
    /// Responsible for monitoring a "well being" of the sample changer.
    /// Restarts samples changer if no position was detected in longer than 15 seconds (setting)
    /// Stops sample changer if no position was detected in longer than 30 seconds (setting)
    /// </summary>
    public class MovingMonitor
    {
        #region Fields

        private static MovingMonitor m_SampleChangerMovingMonitor;
        private Thread m_Thread;
        private bool m_bTerminate = false;
        private bool m_bIsMoving = false;
        private bool m_bIsLogging = false;
        private SMController m_SampleChangerControler;

        #endregion

        #region Ctor

        public static MovingMonitor GetSampleChangerMovingMonitor(SMController sampleChangerController)
        {
            //Single hardware resource; singleton
            if (m_SampleChangerMovingMonitor == null)
                m_SampleChangerMovingMonitor = new MovingMonitor(sampleChangerController);

            return (m_SampleChangerMovingMonitor);
        }
        protected MovingMonitor(SMController sampleChanger)
        {
            if (m_Thread == null)
            {
                m_SampleChangerControler = sampleChanger;
                m_Thread = new Thread(new ThreadStart(ThreadProc));
                m_Thread.Priority = ThreadPriority.Normal;
                m_Thread.Name = "MovingMonitorThread";
                m_Thread.Start();
                Thread.Sleep(1);
            }
            else
                throw new Exception("MovingMonitor: Failed to initialize.");
        }

        #endregion

        #region Properties

        #region IsMoving
        public bool IsMoving
        {
            get { return m_bIsMoving; }
            [MethodImpl(MethodImplOptions.Synchronized)]
            set { m_bIsMoving = value; }
        }
        #endregion

        #region IsLogging
        public bool IsLogging
        {
            get { return m_bIsLogging; }
            set { m_bIsLogging = value; }
        }
        #endregion

        #endregion

        #region Methods
        public void Terminate()
        {
            m_bTerminate = true;
            m_SampleChangerMovingMonitor = null;
            m_Thread.Join();
        }
        #endregion

        #region Thread Start functions

        /// <summary>
        /// ThreadProc for moving sample changer
        /// </summary>
        public void ThreadProc()
        {
            try
            {
                //Waiting for PositionMonitorThread to find its first position 
                if (!m_SampleChangerControler.PositionMonitorInitCompletedEvent.Wait(10000))
                    EventLogger.WriteErrorLogEntry("MovingMonitor: Failed to attain 'PositionMonitorInitializationCompletedEvent' manual event in moving monitor");

                //Start observing
                while (!m_bTerminate)
                {
                    try
                    {

                        Thread.Sleep(1000);
                        if (m_bIsMoving && !m_bIsLogging)
                        {
                            EventLogger.WriteErrorLogEntry("MovingMonitor: MoveToNewPosition " + m_bIsMoving.ToString());
                            TimeSpan ts = (m_SampleChangerControler.LastPositionAttained - DateTime.Now);
                            if (Math.Abs(ts.TotalSeconds) > SettingsLib.Instance.GetValue<Int32>("MaximumSCWaitTimeout"))
                            {
                                m_SampleChangerControler.StopChangerMotor();
                                if (m_SampleChangerControler.ReadChangerPosition() == m_SampleChangerControler.m_nPosition)
                                {
                                    //  This would mean that the  motor never started
                                    EventLogger.WriteErrorLogEntry("MovingMonitor: Unable to start motor. LastPositionAttained " + m_SampleChangerControler.LastPositionAttained.ToLongTimeString());
                                    OnChangerError(ErrorCode.UNABLE_TO_FIND_NEXT_POSITION, "Unable to start motor.");
                                }
                                else
                                {
                                    EventLogger.WriteErrorLogEntry("MovingMonitor: Unable to find next position. LastPositionAttained " + m_SampleChangerControler.LastPositionAttained.ToLongTimeString());
                                    OnChangerError(ErrorCode.UNABLE_TO_FIND_NEXT_POSITION, "Unable to find next position.");
                                }
                            }

                            else if (Math.Abs(ts.TotalSeconds) > SettingsLib.Instance.GetValue<Int32>("WarningSCWaitTimeout"))
                            {
                                if (m_SampleChangerControler.ReadChangerPosition() == 0)
                                    OnChangerError(ErrorCode.UNABLE_TO_FIND_NEXT_POSITION, "Check sample changer power.");

                                if (m_SampleChangerControler.ReadChangerPosition() == m_SampleChangerControler.m_nPosition)
                                {
                                    //  This would mean that the  motor never started
                                    //  We can try to correct this
                                    //	If the motor is in position 31 (which is never assigned to m_nPosition). this will not try to restart it
                                    string err = "MovingMonitor: No new position found. Restarting sample changer.";
                                    EventLogger.WriteErrorLogEntry(err);
                                    m_SampleChangerControler.StartSampleChangerControlerMotor();
                                }
                            }
                        }
                    }
                    catch (ApplicationException ex)
                    {
                        EventLogger.WriteErrorLogEntry("MovingMonitor: " + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Warning);
                        OnChangerError(ErrorCode.UNDIFFERENTIATED_EXCEPTION, ex.Message);
                    }
                }
            }//	End try
            catch (ApplicationException ex)
            {
                EventLogger.WriteErrorLogEntry("MovingMonitor: " + ex.Message + "\n" + ex.StackTrace, EventLogEntryType.Warning);
                OnChangerError(ErrorCode.UNDIFFERENTIATED_EXCEPTION, ex.Message);
            }
        }

        #endregion

        #region ChangerError

        public delegate void ChangerErrorEventHandler(int ErrorCode, string Message);
        public event ChangerErrorEventHandler ChangerError;
        protected virtual void OnChangerError(int ErrorCode, string Message)
        {
            var handler = ChangerError;
            if (handler != null)
                handler(ErrorCode, Message);
        }

        #endregion
    }
}
