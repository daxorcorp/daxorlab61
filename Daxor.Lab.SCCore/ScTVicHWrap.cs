﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Daxor.Lab.SCCore.Utilities;

namespace Daxor.Lab.SCCore
{
    /// <summary>
    /// Summary description for ScTVicHWrap.
    /// Sample Changer abstraction layer,
    ///		through which we communicate with the sample changer hardware
    ///		
    ///	Originally written by Steve Johnson
    ///	History:
    ///		version 1: ISA PIO card (the DIO-24)
    ///		version 2: LPTWrapper to non-DotNet version of TVich.
    ///		version 3: Direct to DotNet version of TVich.
    /// </summary>
    /// 
    public class ScTVicHWrap : IDisposable
    ///interface allowing clean-up code when disposed
    {
        #region Member Fields
        UInt32 HW = 0;		                         ///used to obtain a handle to the parallel port
        //private int m_nPosition = 24;                   // Assigned to but never used; GMazeroff 9/2/2010 3:38pm
        private DateTime m_dtLastRead = DateTime.Now;
        private static ScTVicHWrap m_SampleChanger;  ///protection against multiple instatiations
        private static bool m_bIsOpen = false;		 ///exposed through a property
        private bool disposed = false;               ///used in implementation of IDisposable
        private bool m_bIsMoving = false;	         ///set on start and stop
        private ushort BOT;
        private ushort AuxIn;
        private ushort Safety;
        private ushort Strobe_LL;
        #endregion

        #region Constructors
        ///Returns the sample changer object, or creates one if one has not been created
        public static ScTVicHWrap GetSampleChanger()
        {
            if (m_SampleChanger == null)
            {
                m_SampleChanger = new ScTVicHWrap();
            }

            return (m_SampleChanger);
        }

        /// <summary>
        /// Actual constructor.  
        /// Doesn't do anything any more, but has to exist so that it can be protected.
        /// </summary>
        protected ScTVicHWrap()
        {
            HPTimer.HPTIMER_Init();

            // This eliminates the compiler warning about m_bIsMoving being assigned to but never used.
            // GMazeroff 9/2/2010 3:42pm
            if (m_bIsMoving) m_bIsMoving = true;
        }
        #endregion

        #region Destructors
        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue 
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the 
        // runtime from inside the finalizer and you should not reference 
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources.
                if (disposing)
                {

                }
                StopSampleChangerMotor();
                CloseChanger();

            }
            disposed = true;
        }
        ~ScTVicHWrap()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }
        #endregion

        #region Public Properties

        #region Open
        public bool Open
        {
            ///Gets the cached status of the connection to TVich.
            get { return m_bIsOpen; }
            ///Opens/closes the connection to TVich, and resets the cached status
            set
            {
                if (value)
                {
                    OpenChanger();
                }
                else
                {
                    CloseChanger();
                }
            }
        }
        #endregion

        #region IsMoving
        /// <summary>
        /// Removed 050330 - it would be complicated to support one-shot in this
        /// anyhow, it's not used at the moment
        /// </summary>
        /*public bool IsMoving
        {
            get { return (ReadChangerPosition()==31) ;}
        }
        */
        #endregion

        #region Position
        public short Position
        {
            get
            {
                return (ReadChangerPosition());
                //ReadChangerPosition is protected against DEMO iteself, so that protection is not needed here.
            }
        }
        #endregion

        #region LowerLevelSensor
        public ushort LowerLevelSensor
        {
            get
            {
                int n = ((int)Strobe_LL) > 0 ? 0 : 1;
                return (ushort)n;
            }
        }
        #endregion

        #region BOTSensor
        public ushort BOTSensor
        {
            get
            {
                int n = ((int)BOT) > 0 ? 0 : 1;
                return (ushort)n;
            }
        }
        #endregion

        #endregion

        #region Public Methods
        
        public void StartMotor()
        {
            StartSampleChangerMotor();
        }
        public void StopMotor()
        {
            StopSampleChangerMotor();
        }
        public void PulseMotor()
        {
            PulseSampleChangerMotor();
        }
        
        #endregion

        #region Sample Changer Abstraction Functions

        private void OpenChanger()
        {
            HW = TVicHW32.OpenTVicHW32(HW, "TVICHW32", "TVicDevice0");
            ///possible change - eliminate ActiveHW
            if (TVicHW32.GetActiveHW(HW) != 0)
            {
                TVicHW32.SetLPTNumber(0, 1);
                TVicHW32.SetLPTReadMode(HW); // Set for reading data lines
                m_bIsOpen = true;
            }
            else
            {
                m_bIsOpen = false;
            }
        }
        private short ReadChangerPosition()
        {
            ///read the section of the port we're concerned about
            ///right shift 3 bits (mask off low end)
            ///XOR with 0x10
            short sReturn = 0;
            uint BaseAddr = TVicHW32.GetLPTBasePort(HW);
            ushort nTemp = TVicHW32.GetPortByte(HW, (BaseAddr + 1));
            sReturn = (short)(nTemp >> 3);
            sReturn = (short)(sReturn ^ (short)0x10);

            //Code to return random sample positions for testing WatchLimit
            //Need to uncomment member field above
            //int nReturn = rnd.Next(30) + 1; 
            //sReturn = (short) nReturn;

            //			old code - used to read it a bit at a time
            //current version reads it all at once;
            //avoids any changes while reading
            //			if((TVicHW32.GetPin(HW, 11)== 1))
            //			{
            //				sReturn = 16;
            //			}
            //			sReturn += (short)(TVicHW32.GetPin(HW, 10)*8);
            //			sReturn += (short)(TVicHW32.GetPin(HW, 12)*4);
            //			sReturn += (short)(TVicHW32.GetPin(HW, 13)*2);
            //			sReturn += (short)(TVicHW32.GetPin(HW, 15)*1);

            return (sReturn);
        }

        internal void ReadPorts()
        {
            //	Storage for read data lines.  These will later be fleshed out
            //	For WinBVA 5.4
            uint BaseAddr = TVicHW32.GetLPTBasePort(HW);

            ushort portbyte = TVicHW32.GetPortByte(HW, BaseAddr);

            BOT = (ushort)((int)portbyte & 1);
            AuxIn = (ushort)((int)portbyte & 2);
            Safety = (ushort)((int)portbyte & 4);
            Strobe_LL = (ushort)((int)portbyte & 8);

        }
        private void StartSampleChangerMotor()
        {

            TVicHW32.SetPin(HW, 1, 0);
#if DEBUG
            EventLogger.WriteErrorLogEntry("StartSampleChangerMotor - Sample changer motor being started.");
#endif
            m_dtLastRead = DateTime.Now;
            m_bIsMoving = true;
        }

        private void StopSampleChangerMotor()
        {

            TVicHW32.SetPin(HW, 1, 1);
#if DEBUG
            EventLogger.WriteErrorLogEntry("StopSampleChangerMotor - Sample changer motor stopped.");

#endif
            m_bIsMoving = false;
        }

        public void PulseSampleChangerMotor()
        {
#if true
            // It appears that the TvicHE32 pulse may be too short (25 uS) for the new 
            // input filter on the Mark II and a longer pulse is required.
            TVicHW32.SetPin(HW, 1, 1);
            HPTimer.HPTIMER_Delay((double)(.1 / 1000.0)); // 100 microSeconds = .1 = milliSeconds

            TVicHW32.SetPin(HW, 1, 0);		// Turn Motor On
            HPTimer.HPTIMER_Delay((double)(.1 / 1000.0)); // 100 microSeconds = .1 = milliSeconds
            TVicHW32.SetPin(HW, 1, 1);		// Turn Motor Off
#else
				TVicHW32.LPTStrobe(HW);           
#endif
            EventLogger.WriteErrorLogEntry("Sample Changer Pulsed.");
        }

        private void CloseChanger()
        {

            if (HW != 0)
            {
                HW = TVicHW32.CloseTVicHW32(HW);
                HW = 0;
            }
            m_bIsOpen = false;
            m_SampleChanger = null;
        }

        #endregion

    }
}
